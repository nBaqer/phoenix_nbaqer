<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PromptPrefName.aspx.vb" Inherits="Reports_PromptPrefName" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Save Preference</title>    
    <meta http-equiv="pragma" content="NO-CACHE"/>
    <LINK href="../css/localhost.css" type="text/css" rel="stylesheet"/>
	<base target="_self" />
</head>
<body bgcolor="lightgrey">
    <form id="form1" runat="server">
    <div style="height:100px; vertical-align:middle;">
        <table width="90%" align="center">
        <tr height="20px"><td></td></tr>
        <tr>
            <td class="Label" align="left" valign="middle">Preference name</td>
            <td align="left"><asp:TextBox ID="txtValue" runat="server" CssClass="textbox" /></td>
        </tr>
        <tr align="center">
            <td><asp:Button ID="btnOK" runat="server" Text="Ok"  Width="75px" /></td>
            <td><asp:Button ID="btnCancel" runat="server" Text="Cancel"  Width="75px" /></td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
