' ===============================================================================
' RunReport
' Main entry point to display pre-defined and adhoc reports
' For this version, only adhoc reports are supported
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Parameters that can be passed to this page are:
'   mod         : module id
'   cmpid       : campus id
'   res         : report id
'   isadhoc     : 0 if the report is a pre-defined report, 1 if it is adhoc
'   del         : 1 if the user wants to delete the report after running (can be done when creating the report)
Imports System.Data
Imports System.Xml
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade.Reports
Imports FAME.AdvantageV1.Common.Reports
Imports NHibernate.Properties
Namespace AdvWeb.Reports

    Partial Class RunReport
        Inherits BasePage

#Region "Attributes"
        Private pObj As UserPagePermissionInfo

#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            ' check if we are in "delete after mode" which is set if the param("del")=1 and the user has viewed the report.
            ' Steps to get here are 1) Setup a report and at the end, click Preview.  2) View the Report 3) Click Edit
            If ViewState("redirect") = "1" Then
                Dim url As String = String.Format("~/Reports/SetupAdhocReports.aspx?resid=487&mod={0}&cmpid={1}", _
                                                  Request.Params("mod"), RPTCommon.GetCampusID())
                Response.Redirect(url)
                Return
            End If

            Dim advantageUserState As User
            Dim campusId As String
            Dim resourceId As String
            advantageUserState = AdvantageSession.UserState
            'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
            'ModuleId = HttpContext.Current.Request.Params("Mod").ToString

            resourceId = HttpContext.Current.Request.Params("resid")
            campusId = AdvantageSession.UserState.CampusId.ToString
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageUserState, resourceId, campusId)
            If Not Page.IsPostBack Then
                ' FOR NOW, default to showing only adhoc reports
                radStatus.SelectedIndex = 2
                ' make the "fit to screen" option checked by default


                ViewState("mod") = Request.Params("mod") ' save the module    
                ViewState("del") = Request.Params("del") ' "1" if we need to delete the report after running
                'Enable the edit report button if we are in the system module.
                'If Request.Params("mod").ToUpper = "SY" Then
                'btnEditReport.Enabled = True
                'End If
                SetPageTitle()

                ' add javascript to the delete button
                btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
                ' add javascript to the save button
                Dim js As String = RPTCommon.GetJavsScriptPopup("PromptPrefName.aspx", 300, 150, "hfPrefName")
                btnsave.Attributes.Add("onclick", js)

                ' Advantage specific page permissions stuff
                'pObj = AdvGetPermissions()
                InitButtonsForLoad()
                ' reset the form the the original state
                ResetForm()

                BindReports()

                ' see if the page is requesting a specific report
                Dim reportId As String = Request.Params("resid")
                Dim isAdHoc As Boolean = False
                If Not Request.Params("isadhoc") Is Nothing Then
                    isAdHoc = CType(Request.Params("isadhoc"), Boolean)
                End If

                ' bind the report, if passed to the page
                If Not reportId Is Nothing AndAlso reportId <> "" Then
                    BindReport(reportId, isAdHoc)
                    'MERGEFIX

                    CommonWebUtilities.RestoreItemValues(dlReports, reportId)

                End If

                ' show the warning if we are set to delete the report after running
                If Not ViewState("del") Is Nothing AndAlso ViewState("del") = "1" Then
                    lblWarning.Visible = True
                End If
            End If
            ' Me.ReportWizard.Visible = False
        End Sub

        Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
            BindReports()
        End Sub

#Region "Advantage Permissions"
        Protected Function AdvGetPermissions() As UserPagePermissionInfo
            Dim resourceId As Integer = CInt(HttpContext.Current.Request.Params("resid"))
            Dim campusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
            Dim userId As String = XmlConvert.ToGuid(CType(HttpContext.Current.Session("UserId"), String)).ToString

            Dim fac As New UserSecurityFacade
            Dim urp As UserPagePermissionInfo = fac.GetUserResourcePermissions(userId, resourceId, campusId)
            If urp Is Nothing Then
                'Send the user to the standard error page
                Session("Error") = "You do not have the necessary permissions to access to this report."
                Response.Redirect("../ErrorPage.aspx")
            End If
            Return urp
        End Function

        Private Sub InitButtonsForLoad()
            If pObj Is Nothing Then Return
            If pObj.HasFull Or pObj.HasEdit Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If
            If pObj.HasFull Or pObj.HasDelete Then
                btnDelete.Enabled = True
            Else
                btnDelete.Enabled = False
            End If
            If dlReports.SelectedItem Is Nothing Then
                btnViewReport.Enabled = False
            Else
                btnViewReport.Enabled = True
            End If

        End Sub

        Private Sub InitButtonsForEdit()
            If pObj Is Nothing Then Return
            If pObj.HasFull Or pObj.HasEdit Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If

            If pObj.HasFull Or pObj.HasDelete Then
                btnDelete.Enabled = True
            Else
                btnDelete.Enabled = False
            End If
            btnNew.Enabled = True
        End Sub
#End Region

#Region "Binding Methods"
        ''' <summary>
        ''' Resets the form to its original state
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub ResetForm()
            ' Clear the PrefId
            ViewState("pid") = Nothing
            dlPrefs.SelectedIndex = -1

            lblReportName.Text = ""
            CType(Master.FindControl("contentmain2"), ContentPlaceHolder).FindControl("divReportName").Visible = False
            lblPrefName.Text = ""
            hfPrefName.Value = ""
            txtPref.Text = ""
            ' clear out the wizard
            Dim rpt As Repeater = CType(Step1.ContentTemplateContainer.FindControl("rptFilters"), Repeater)
            rpt.DataSource = Nothing
            rpt.DataBind()

            CType(Step2.ContentTemplateContainer.FindControl("lbSelSort"), ListBox).Items.Clear()
            CType(Step2.ContentTemplateContainer.FindControl("lbSort"), ListBox).Items.Clear()

            CType(Step3.ContentTemplateContainer.FindControl("cbRptFilters"), CheckBox).Enabled = True
            CType(Step3.ContentTemplateContainer.FindControl("cbRptSort"), CheckBox).Enabled = True
            CType(Step3.ContentTemplateContainer.FindControl("cbRptDescrip"), CheckBox).Enabled = True
            CType(Step3.ContentTemplateContainer.FindControl("cbRptInstructions"), CheckBox).Enabled = True
            CType(Step3.ContentTemplateContainer.FindControl("cbRptNotes"), CheckBox).Enabled = True
        End Sub

        ''' <summary>
        ''' Display a list of reports (predef plus adhoc) for the user and module
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub BindReports()
            Dim moduleId As String = CType(ViewState("mod"), String)
            Dim userId As String = RPTCommon.GetCurrentUserId()

            Select Case moduleId
                Case Is = "AD"
                    moduleId = "189"
                Case Is = "AR"
                    moduleId = "26"
                Case Is = "PL"
                    moduleId = "193"
                Case Is = "FA"
                    moduleId = "191"
                Case Is = "HR"
                    moduleId = "192"
                Case Is = "SA"
                    moduleId = "194"
                Case Is = "SY"
                    moduleId = "195"
                Case Is = "FC"
                    moduleId = "300"
            End Select

            If radStatus.SelectedIndex = 0 Then
                dlReports.DataSource = FAME.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetReportsForUser(userId, RPTCommon.GetCampusID(), moduleId, True, True)
            ElseIf radStatus.SelectedIndex = 1 Then
                dlReports.DataSource = FAME.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetReportsForUser(userId, RPTCommon.GetCampusID(), moduleId, True, False)
            Else
                dlReports.DataSource = FAME.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetReportsForUser(userId, RPTCommon.GetCampusID(), moduleId, False, True)
            End If
            dlReports.DataBind()
        End Sub

        ''' <summary>
        ''' Displays all the parameters for the selected report
        ''' </summary>
        ''' <param name="reportId"></param>
        ''' <param name="isAdHoc"></param>
        ''' <remarks></remarks>
        Protected Sub BindReport(ByVal reportId As String, ByVal isAdHoc As Boolean)
            ' display the panel
            pnlRHS.Visible = True
            ' Save the reportid and isadhoc values to the viewstate
            ViewState("rid") = reportId
            ViewState("isadhoc") = isAdHoc

            If isAdHoc Then
                Dim info As AdHocRptInfo = AdHocRptFacade.GetReportInfo(reportId)
                ' set the reportname at the top of the screen
                CType(Master.FindControl("contentmain2"), ContentPlaceHolder).FindControl("divReportName").Visible = True
                lblReportName.Text = info.ReportName
                ' allow the user to edit the report if they are the owner
                ' btnEditReport.Visible = (info.ModUser = RPTCommon.GetCurrentUserId())
            Else
                ' hide these for non-adhoc reports
                CType(Master.FindControl("contentmain2"), ContentPlaceHolder).FindControl("divReportName").Visible = False
                ' btnEditReport.Visible = False
            End If

            ' Bind parameter list
            Dim dsFields As DataSet = FAME.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetReportParams(reportId, IsAdHoc)
            BindFilters(dsFields)
            ' Bind sort orders
            BindSortOrder(dsFields)
            ' Bind header and footer options
            BindDisplayOptions(dsFields)
            ' display the saved prefs for the selected report
            BindPrefs(ReportId, IsAdHoc)

            'Set the mode to NEW
            'If Not pObj Is Nothing Then pObj.SetBtnState(form1, "NEW", pObj)
            'ViewState("MODE") = "NEW"

            'Set page title.
            'PageTitle &= ViewState("Resource")
            'Page.Title = "Run Report - " + rInfo.Resource
            'Page.DataBind()
        End Sub


#Region "Methods for Displaying Forms for Filters"
        ''' <summary>
        ''' Binds all the filters for a report.  The input dataset "ds" is retrieved
        ''' from ReportFacade.GetReportPaarams
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Protected Sub BindFilters(ByVal ds As DataSet)
            ' first, get all the rows with IsParam = 1 and put it in a new datatable called "dt"
            Dim dt As DataTable = ds.Tables(0).Clone()
            Dim drs() As DataRow = ds.Tables(0).Select("IsParam=1")
            For Each dr As DataRow In drs
                dt.Rows.Add(dr.ItemArray)
            Next

            ' Bind the resulting datatable to the repeater
            Dim rpt As Repeater = CType(Step1.ContentTemplateContainer.FindControl("rptFilters"), Repeater)
            rpt.DataSource = dt
            rpt.DataBind()

            'Dim ReportId As String = ViewState("rid")
            'Dim IsAdHoc As Boolean = ViewState("isadhoc")
            For Each ri As RepeaterItem In rpt.Items
                Dim ddlId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
                Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value

                Dim fldType As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)
                'If Not sdfId Is Nothing And fldType = 3 Then fldType = 135 'date field
                If Not ddlId Is Nothing AndAlso ddlId <> "" And (sdfId Is Nothing Or sdfId = "") Then
                    _SetupFilter_DDL(ri)
                ElseIf Not ddlId Is Nothing AndAlso ddlId <> "" And Not sdfId Is Nothing And sdfId <> "" Then
                    _SetupFilter_DDLForSDF(ri)

                ElseIf fldType = FieldType._DataTime Then
                    _SetupFilter_Calendar(ri)
                ElseIf fldType = FieldType._Bit Then
                    _SetupFilter_Bit(ri)
                ElseIf fldType = FieldType._Int Or fldType = FieldType._Smallint Or fldType = FieldType._Decimal Then
                    _SetupFilter_Numeric(ri)
                Else
                    _SetupFilter_RegularInput(ri)
                End If
            Next
            lblNoFilters.Visible = (rpt.Items.Count = 0)
            ' Display a message if there are no filters for this report
            'Step1.ContentTemplateContainer.FindControl("lblNoFilters").Visible = (rpt.Items.Count = 0)
        End Sub

        ''' <summary>
        ''' Dispays controls for entering in a non DDL filter.  The controls
        ''' are simply a dropdown with the list of operators and a textbox with the filter value
        ''' </summary>
        ''' <param name="ri"></param>
        ''' <remarks></remarks>
        Protected Sub _SetupFilter_RegularInput(ByVal ri As RepeaterItem)
            ri.FindControl("txtRegFilter").Visible = True
            Dim ddl As DropDownList = CType(ri.FindControl("ddlRegFilterOp"), DropDownList)
            ddl.Visible = True

            ' Build the dropdownlist with a list of possible operators
            ddl.Items.Add(New ListItem("-- Select --", ""))
            ddl.Items.Add(New ListItem("Equal", "="))
            ddl.Items.Add(New ListItem("Contains", "like"))
            ddl.Items.Add(New ListItem("Between", "BETWEEN"))
            ddl.Items.Add(New ListItem("Not Equal", "<>"))
            ddl.Items.Add(New ListItem("Greater Than", ">"))
            ddl.Items.Add(New ListItem("Less Than", "<"))
            ddl.Items.Add(New ListItem("Greater Than or Equal", ">="))
            ddl.Items.Add(New ListItem("Less Than or Equal", "<="))
        End Sub


        ''' <summary>
        ''' Dispays controls for entering in a non DDL filter.  The controls
        ''' are simply a dropdown with the list of operators and a textbox with the filter value
        ''' </summary>
        ''' <param name="ri"></param>
        ''' <remarks></remarks>
        Protected Sub _SetupFilter_Numeric(ByVal ri As RepeaterItem)
            ri.FindControl("txtRegFilter").Visible = True
            Dim ddl As DropDownList = CType(ri.FindControl("ddlRegFilterOp"), DropDownList)
            ddl.Visible = True

            ' Build the dropdownlist with a list of possible operators
            ddl.Items.Add(New ListItem("-- Select --", ""))
            ddl.Items.Add(New ListItem("Equal", "="))
            ddl.Items.Add(New ListItem("Greater Than", ">"))
            ddl.Items.Add(New ListItem("Less Than", "<"))
            ddl.Items.Add(New ListItem("Between", "BETWEEN"))

        End Sub

   



        ''' <summary>
        ''' Display the calendar controls for this parameter.  Called for each paramter
        ''' in the filter repeater that does not have a DDL defined and has a datetime fieldtype.
        ''' </summary>
        ''' <param name="ri"></param>
        ''' <remarks></remarks>
        Protected Sub _SetupFilter_Calendar(ByVal ri As RepeaterItem)
            ' special case for datetime field.  We need to show controls that allow the user
            ' to enter in an exact date, or a max/min date
            ri.FindControl("lbFilterEqual").Visible = True
            ri.FindControl("txtFilterEqual").Visible = True
            ri.FindControl("ibEqualDate").Visible = True
            ri.FindControl("lbFilterMin").Visible = True
            ri.FindControl("txtFilterMin").Visible = True
            ri.FindControl("ibMinDate").Visible = True
            ri.FindControl("lbFilterMax").Visible = True
            ri.FindControl("txtFilterMax").Visible = True
            ri.FindControl("ibMaxDate").Visible = True

            ' add calendar javascript for the Equal image button
            Dim ctlID As String = ri.FindControl("txtFilterEqual").ClientID
            Dim ibID As String = ri.FindControl("ibEqualDate").ClientID
            Dim img As HtmlImage = CType(ri.FindControl("ibEqualDate"), HtmlImage)
            img.Attributes.Add("onload", String.Format("setup_calendar('{0}','{1}')", ctlID, ibID))

            ' add calendar javascript for the Min image button
            ctlID = ri.FindControl("txtFilterMin").ClientID
            ibID = ri.FindControl("ibMinDate").ClientID
            img = CType(ri.FindControl("ibMinDate"), HtmlImage)
            img.Attributes.Add("onload", String.Format("setup_calendar('{0}','{1}')", ctlID, ibID))

            ' add calendar javascript for the Min image button
            ctlID = ri.FindControl("txtFilterMax").ClientID
            ibID = ri.FindControl("ibMaxDate").ClientID
            img = CType(ri.FindControl("ibMaxDate"), HtmlImage)
            img.Attributes.Add("onload", String.Format("setup_calendar('{0}','{1}')", ctlID, ibID))
        End Sub

        ''' <summary>
        ''' Sets up a filter for a data driven parameter.  This gets called for each item
        ''' in the filter repeater.
        ''' </summary>
        ''' <param name="ri"></param>
        ''' <remarks></remarks>
        Protected Sub _SetupFilter_DDLForSDF(ByVal ri As RepeaterItem)

            Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value
            Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)

            dl.DataSource = FAME.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetValuesForSDFField(sdfId)
            'dl.DataSource = ReportFacade.GetValuesForField(DDLId, ViewState("CampGrpId").ToString)
            dl.DataBind()
            If dl.Items.Count > 4 Then
                ri.FindControl("lblDiv1").Visible = True
                ri.FindControl("lblDiv2").Visible = True
            Else
                ri.FindControl("lblDiv1").Visible = True
            End If
        End Sub
        Protected Sub _SetupFilter_DDL(ByVal ri As RepeaterItem)
            Dim ddlId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
            Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
            'ViewState("CampGrpId") = CType(ri.FindControl("txtCampGrpId"), TextBox).Text
            ' bind the list of possible values for this field                
            'dl.DataSource = ReportFacade.GetValuesForField(DDLId)
            If ViewState("CampGrpId") Is Nothing Then
                If Request.QueryString("CampGrpId") <> "" Then
                    ViewState("CampGrpId") = Request.QueryString("CampGrpId")
                Else
                    ViewState("CampGrpId") = ""
                End If
            End If
            dl.DataSource = FAME.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetValuesForField(ddlId, ViewState("CampGrpId").ToString, AdvantageSession.UserState.UserId.ToString)
            'dl.DataSource = ReportFacade.GetValuesForField(DDLId, ViewState("CampGrpId").ToString)
            dl.DataBind()
            If dl.Items.Count > 4 Then
                ri.FindControl("lblDiv1").Visible = True
                ri.FindControl("lblDiv2").Visible = True
            Else
                ri.FindControl("lblDiv1").Visible = True
            End If
        End Sub
        Protected Sub _SetupFilter_Bit(ByVal ri As RepeaterItem)
            'Dim DDLId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
            Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
            'ViewState("CampGrpId") = CType(ri.FindControl("txtCampGrpId"), TextBox).Text
            ' bind the list of possible values for this field                
            'dl.DataSource = ReportFacade.GetValuesForField(DDLId)

            dl.DataSource = FAME.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetValuesForBitField()
            dl.DataBind()
            If dl.Items.Count > 0 Then
                ri.FindControl("lblDiv1").Visible = True
                ri.FindControl("lblDiv2").Visible = True
            End If
        End Sub

#End Region

#Region "Methods for Displaying Sort Order and Report Options"
        ''' <summary>
        ''' Binds the available sort orders to the list box on the 2nd step of the wizard
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Protected Sub BindSortOrder(ByVal ds As DataSet)
            ' first, get all the rows with IsParam = 1
            Dim dt As DataTable = ds.Tables(0).Clone()
            Dim drs() As DataRow = ds.Tables(0).Select("SortBy=1")
            For Each dr As DataRow In drs
                dt.Rows.Add(dr.ItemArray)
            Next

            ' Add the field info to the lbSort listbox
            Dim lb As ListBox = CType(Step2.ContentTemplateContainer.FindControl("lbSort"), ListBox)
            lb.DataSource = dt
            lb.DataTextField = "Header"
            lb.DataValueField = "ParamId"
            lb.DataBind()
        End Sub

        Protected Sub BindDisplayOptions(ByVal ds As DataSet)
            ' Set status of buttons and checkboxes.
            If ds.Tables.Count = 2 AndAlso ds.Tables("RptProps").Rows.Count > 0 Then
                Dim row As DataRow = ds.Tables("RptProps").Rows(0)
                If Not row.IsNull("AllowParams") AndAlso Not row("AllowParams") Then
                    'Report has no params, then disable Save, New and Delete buttons.
                    'btnAdd.Enabled = False
                    'btnRemove.Enabled = False
                    btnSave.Enabled = False
                    btnNew.Enabled = False
                    btnDelete.Enabled = False
                    ' Disable the Filters and Sort checkboxes.
                    CType(Step3.ContentTemplateContainer.FindControl("cbRptFilters"), CheckBox).Enabled = False
                    CType(Step3.ContentTemplateContainer.FindControl("cbRptSort"), CheckBox).Enabled = False
                End If
                ' Description checkbox.
                If Not row.IsNull("AllowDescrip") Then
                    CType(Step3.ContentTemplateContainer.FindControl("cbRptDescrip"), CheckBox).Enabled = CType(row("AllowDescrip"), Boolean)
                End If
                ' Instructions checkbox.
                If Not row.IsNull("AllowInstruct") Then
                    CType(Step3.ContentTemplateContainer.FindControl("cbRptInstructions"), CheckBox).Enabled = CType(row("AllowInstruct"), Boolean)
                End If
                ' Notes checkbox.
                If Not row.IsNull("AllowNotes") Then
                    CType(Step3.ContentTemplateContainer.FindControl("cbRptNotes"), CheckBox).Enabled = CType(row("AllowNotes"), Boolean)
                End If
            End If
        End Sub
#End Region

#End Region

#Region "Binding Methods for Saved Params"
        ''' <summary>
        ''' Time saver for the BindPrefs with params
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub BindPrefs()
            Dim reportId As String = CType(ViewState("rid"), String)
            Dim isAdHoc As Boolean = CType(ViewState("isadhoc"), Boolean)
            BindPrefs(reportId, isAdHoc)
        End Sub

        ''' <summary>
        ''' Binds all the saved prefs for a report
        ''' </summary>
        ''' <param name="reportId"></param>
        ''' <param name="isAdHoc"></param>
        ''' <remarks></remarks>
        Protected Sub BindPrefs(ByVal reportId As String, ByVal isAdHoc As Boolean)
            dlPrefs.DataSource = FAME.AdvantageV1.BusinessFacade.Reports.RptParamsFacade.GetPrefsForReport(RPTCommon.GetCurrentUserId(), reportId, isAdHoc)
            dlPrefs.DataBind()
        End Sub

        ''' <summary>
        ''' Binds a specific user pref to the form.
        ''' Specifically, the filters are set
        ''' </summary>
        ''' <param name="prefId"></param>
        ''' <remarks></remarks>
        Protected Sub BindPref(ByVal prefId As String)
            Dim info As UserPrefsInfo = FAME.AdvantageV1.BusinessFacade.Reports.RptParamsFacade.GetUserPrefsInfo(prefId)
            lblPrefName.Text = " - " + info.PrefName
            txtPref.Text = info.PrefName
            ' first, clear out all values in the filter form
            Dim rpt As Repeater = CType(Step1.ContentTemplateContainer.FindControl("rptFilters"), Repeater)
            For Each ri As RepeaterItem In rpt.Items
                Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
                For Each dli As DataListItem In dl.Items
                    CType(dli.FindControl("cbValue"), CheckBox).Checked = False
                Next
                CType(ri.FindControl("txtFilterEqual"), TextBox).Text = "" 
                CType(ri.FindControl("txtFilterMax"), TextBox).Text = "" 
                CType(ri.FindControl("txtFilterMin"), TextBox).Text = "" 
                CType(ri.FindControl("txtRegFilter"), TextBox).Text = "" 
            Next

            If Not info.Filters Is Nothing Then
                For Each filter As FilterInfo In info.Filters
                    SetFilter(filter)
                Next
            End If

            'BindSavedSortOrder(dt)
        End Sub


        ''' <summary>
        ''' Sets a specific filter in the form
        ''' This method is a little bit on the brute force side.  It iterates through
        ''' each repeater item looking for a match with the filters paramid (either RptParamId or AdHocFieldId
        ''' depending on if this report is adhoc or not)If the report is adhoc then RptParamId is the TblFldsId (needed to differentiatle between to same type of controls i.e. txtbox/dropdowns).
        ''' Then, it looks in the matching repeater items's 
        ''' datalist and sets the value if it matches the filter's value property.
        ''' </summary>
        ''' <param name="filter"></param>
        ''' <remarks></remarks>
        Protected Sub SetFilter(ByVal filter As FilterInfo)
            Dim rpt As Repeater = CType(Step1.ContentTemplateContainer.FindControl("rptFilters"), Repeater)

            ' first, find the repeateritem with this filters paramid
            For Each ri As RepeaterItem In rpt.Items
                If filter.RptParamId = CType(ri.FindControl("hfParamId"), HiddenField).Value Or _
                   filter.AdHocFieldId = CType(ri.FindControl("hfParamId"), HiddenField).Value Then
                    ' we found the correct repeater item
                    Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
                    ' now check the box with the right value
                    For Each dli As DataListItem In dl.Items
                        If filter.Value = CType(dli.FindControl("lblId"), Label).Text Then
                            CType(dli.FindControl("cbValue"), CheckBox).Checked = True
                        End If
                    Next
                    dim hfTblFldId = CType(ri.FindControl("hfTblFldId"), HiddenField).Value
                    'exact date
                    if filter.OpId = OpType.DateExact Then
                        If filter.RptParamId = hfTblFldId Then
                            CType(ri.FindControl("txtFilterEqual"), TextBox).Text = filter.Value
                        End If
                    End If
                    'min date
                    if filter.OpId = OpType.DateMin Then
                        If filter.RptParamId = hfTblFldId Then
                            CType(ri.FindControl("txtFilterMin"), TextBox).Text = filter.Value
                        End If
                    End If
                    'max date
                    if filter.OpId = OpType.DateMax Then
                        If filter.RptParamId = hfTblFldId Then
                            CType(ri.FindControl("txtFilterMax"), TextBox).Text = filter.Value
                        End If
                    End If
                    ' regular txtbox
                    if filter.OpId = OpType.None Then
                        If filter.RptParamId = hfTblFldId Then
                            CType(ri.FindControl("txtRegFilter"), TextBox).Text = filter.Value
                        End If
                    End If
                    ' regular dropdown
                    if filter.OpId = OpType.IsNull Then
                        If filter.RptParamId = hfTblFldId Then
                            CType(ri.FindControl("ddlRegFilterOp"), DropDownList).SelectedIndex = CType(filter.Value,Integer)
                        End If
                    End If
                End If
            Next
        End Sub

        ' ''' <summary>
        ' ''' NOT USED, will be when merging predef and adhoc reports
        ' ''' </summary>
        ' ''' <param name="dt"></param>
        ' ''' <remarks></remarks>
        'Protected Sub BindSavedSortOrder(ByVal dt As DataTable)
        '    ' Display all the sort orders that were saved
        '    Dim lbSel As ListBox = Step2.ContentTemplateContainer.FindControl("lbSelSort")
        '    lbSel.DataSource = dt
        '    lbSel.DataTextField = "Caption"
        '    lbSel.DataValueField = "RptParamId"
        '    lbSel.DataBind()

        '    'When the page is first loaded we populate the lbxSort control with all the
        '    'sort parameters for the report. When a preference is selected we need to
        '    'make certain that the lbxSort control only displays the sort parameters that
        '    'are not already selected.
        '    Dim lbAll As ListBox = Step2.ContentTemplateContainer.FindControl("lbSort")
        '    For i As Integer = lbAll.Items.Count - 1 To 0 Step -1
        '        If Not lbSel.Items.FindByValue(lbAll.Items(i).Value) Is Nothing Then
        '            lbAll.Items.RemoveAt(i)
        '        End If
        '    Next
        'End Sub
#End Region

#Region "Event Handlers"
        ''' <summary>
        ''' Handles when the user clicks on a report from the left pane
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub dlReports_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlReports.ItemCommand
            ' get the selected id and store it in the viewstate object
            Dim rid As String = e.CommandArgument.ToString
            ViewState("rid") = rid
            If CType(e.Item.FindControl("txtCampGrpDescrip"), HiddenField).Value.ToLower = "all" Then
                ViewState("CampGrpId") = ""
            Else
                ViewState("CampGrpId") = CType(e.Item.FindControl("txtCampGrpId"), HiddenField).Value
            End If

            txtPref.Text = ""
            lblPrefName.Text = ""
            ViewState("pid") = Nothing

            dlReports.SelectedIndex = e.Item.ItemIndex
            If Not (dlReports.SelectedItem Is Nothing) Then
                btnViewReport.Enabled = True
            Else
                btnViewReport.Enabled = False
            End If

            ' do some error checking
            If rid Is Nothing Or rid = "" Then
                RPTCommon.Alert(Me, "There was a problem loading this report.")
                Return
            End If

            ' determine if the report is adhoc by checking if the adhoc image is visible
            Dim isAdHoc As Boolean = e.Item.FindControl("imgAdHoc").Visible
            ' Bind the report to the form
            BindReport(rid, isAdHoc)

            ' Advantage specific stuff()
            'CommonWebUtilities.SetStyleToSelectedItem(dlReports, rid, ViewState)
            CommonWebUtilities.RestoreItemValues(dlReports, rid)

        End Sub

        Private Sub dlPrefs_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlPrefs.ItemCommand
            ' get the selected id and store it in the viewstate object
            Dim pid As String = e.CommandArgument.ToString
            ViewState("pid") = pid
            dlPrefs.SelectedIndex = e.Item.ItemIndex

            ' do some error checking
            If pid Is Nothing Or pid = "" Then
                RPTCommon.Alert(Me, "There was a problem loading the preferences for this report")
                Return
            End If

            ' bind the prefs to the form        
            BindPref(pid)

            ' Advantage specific stuff
            'CommonWebUtilities.SetStyleToSelectedItem(dlPrefs, pid, ViewState)
            CommonWebUtilities.RestoreItemValues(dlPrefs, pid)
            InitButtonsForEdit()
        End Sub

        ''' <summary>
        ''' Checks the form's contents for validity.  If any control is
        ''' not valid, an alert is made and the function returns false.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function ValidateForm() As Boolean
            ' first check that there is a Pref Name
            ' if there isn't then the user hit cancel so just return false
            if dlReports.SelectedIndex = -1 then return False
            If txtPref.Text is Nothing or txtPref.Text = "" then
                RPTCommon.DisplayErrorMessage(Me, "Please give the prefernce name before saving preferences.")
                Return False
            else    
                hfPrefName.Value = txtPref.Text
            End If
            If hfPrefName.Value Is Nothing Or hfPrefName.Value = "" Then
                Return False
            End If

            Dim rid As String = CType(ViewState("rid"), String)
            If rid Is Nothing Or rid = "" Then
                RPTCommon.DisplayErrorMessage(Me, "Please select a report before saving preferences.")
                Return False
            End If
            Return True
        End Function

        ''' <summary>
        ''' User wants to save a report preference
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
            Try
                ' check that the user has entered in all the right values
                If Not ValidateForm() Then Return

                ' make sure there is a 
                Dim rid As String = CType(ViewState("rid"), String)
                Dim isAdHoc As Boolean = CType(ViewState("isadhoc"), Boolean)
                Dim pid As String = CType(ViewState("pid"), String)
                Dim info As New UserPrefsInfo
                ' determine if this is an update or a new by looking
                ' at the ViewState which is set by passing a parameter to the page.
                If pid Is Nothing Or pid = "" Then
                    info.IsInDB = False
                    ViewState("pid") = info.PrefId
                Else
                    info.IsInDB = True
                    info.PrefId = pid
                End If

                info.ReportId = rid
                info.IsAdHoc = isAdHoc
                hfPrefName.Value = txtPref.Text
                info.PrefName = hfPrefName.Value

                Dim rpt As Repeater = CType(Step1.ContentTemplateContainer.FindControl("rptFilters"), Repeater)
                ' First get the total number of parameters
                Dim numParams As Integer = 0
                For Each ri As RepeaterItem In rpt.Items
                    Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
                    For Each dli As DataListItem In dl.Items
                        If CType(dli.FindControl("cbValue"), CheckBox).Checked Then
                            numParams += 1
                        End If
                    Next
                    ' calender 
                    Dim txtEqualDt = CType(ri.FindControl("txtFilterEqual"), TextBox)
                    If  (not(txtEqualDt.Text = "") or not(txtEqualDt.Text = Nothing)) then
                        numParams +=1
                    End If
                    Dim txtMinDt = CType(ri.FindControl("txtFilterMin"), TextBox)
                    If  (not(txtMinDt.Text = "") or not(txtMinDt.Text = Nothing)) then
                        numParams +=1
                    End If
                    Dim txtMaxDt = CType(ri.FindControl("txtFilterMax"), TextBox)
                    If  (not(txtMaxDt.Text = "") or not(txtMaxDt.Text = Nothing)) then
                        numParams +=1
                    End If
                    ' regular text box input
                    Dim txtRegFilter = CType(ri.FindControl("txtRegFilter"), TextBox)
                    If  (not(txtRegFilter.Text = "") or not(txtRegFilter.Text = Nothing)) then
                        numParams +=1
                    End If
                    ' regular dropdown input
                    Dim ddlRegFilterOp = CType(ri.FindControl("ddlRegFilterOp"), DropDownList)
                    If  (ddlRegFilterOp.SelectedIndex > -1) then
                        numParams +=1
                    End If
                Next

                ' now, fill up the array of filters from the form
                Dim i As Integer = 0
                Dim filters(numParams - 1) As FilterInfo
                For Each ri As RepeaterItem In rpt.Items
                    Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
                    For Each dli As DataListItem In dl.Items
                        If CType(dli.FindControl("cbValue"), CheckBox).Checked Then
                            filters(i) = New FilterInfo()
                            filters(i).PrefId = pid
                            If Not isAdHoc Then
                                filters(i).RptParamId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                            Else
                                filters(i).AdHocFieldId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                            End If
                            filters(i).OpId = OpType.Equal
                            filters(i).Value = CType(dli.FindControl("lblId"), Label).Text
                            i += 1
                        End If
                    Next
                    'calender
                    Dim txtExactDt = CType(ri.FindControl("txtFilterEqual"), TextBox)
                    If  (not(txtExactDt.Text = "") or not(txtExactDt.Text = Nothing)) then
                        filters(i) = New FilterInfo()
                        filters(i).PrefId = pid
                        If Not isAdHoc Then
                            filters(i).RptParamId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        Else
                            'If the report is adhoc then RptParamId is the TblFldsId (needed to differentiatle between to same type of controls i.e. txtbox/dropdowns)
                            filters(i).RptParamId = CType(ri.FindControl("hfTblFldId"), HiddenField).Value
                            filters(i).AdHocFieldId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        End If
                        filters(i).OpId = OpType.DateExact
                        filters(i).Value = txtExactDt.Text
                        i +=1
                    End If
                    Dim txtminDt = CType(ri.FindControl("txtFilterMin"), TextBox)
                    If  (not(txtminDt.Text = "") or not(txtminDt.Text = Nothing)) then
                        filters(i) = New FilterInfo()
                        filters(i).PrefId = pid
                        If Not isAdHoc Then
                            filters(i).RptParamId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        Else
                            'If the report is adhoc then RptParamId is the TblFldsId (needed to differentiatle between to same type of controls i.e. txtbox/dropdowns)
                            filters(i).RptParamId = CType(ri.FindControl("hfTblFldId"), HiddenField).Value
                            filters(i).AdHocFieldId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        End If
                        filters(i).OpId = OpType.DateMin
                        filters(i).Value = txtminDt.Text
                        i +=1
                    End If
                    Dim txtMaxDt = CType(ri.FindControl("txtFilterMax"), TextBox)
                    If  (not(txtMaxDt.Text = "") or not(txtMaxDt.Text = Nothing)) then
                        filters(i) = New FilterInfo()
                        filters(i).PrefId = pid
                        If Not isAdHoc Then
                            filters(i).RptParamId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        Else
                            'If the report is adhoc then RptParamId is the TblFldsId (needed to differentiate between to same type of controls i.e. txtbox/dropdowns)
                            filters(i).RptParamId = CType(ri.FindControl("hfTblFldId"), HiddenField).Value
                            filters(i).AdHocFieldId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        End If
                        filters(i).OpId = OpType.DateMax
                        filters(i).Value = txtMaxDt.Text
                        i +=1
                    End If
                    ' regular text box input
                    Dim txtRegFilter = CType(ri.FindControl("txtRegFilter"), TextBox)
                    If  (not(txtRegFilter.Text = "") or not(txtRegFilter.Text = Nothing)) then
                        filters(i) = New FilterInfo()
                        filters(i).PrefId = pid
                        If Not isAdHoc Then
                            filters(i).RptParamId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        Else
                            'If the report is adhoc then RptParamId is the TblFldsId (needed to differentiate between to same type of controls i.e. txtbox/dropdowns)
                            filters(i).RptParamId = CType(ri.FindControl("hfTblFldId"), HiddenField).Value
                            filters(i).AdHocFieldId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        End If
                        filters(i).OpId = OpType.None
                        filters(i).Value = txtRegFilter.Text
                        i +=1
                    End If
                    ' regular drop down 
                    Dim ddlRegFilterOp  = CType(ri.FindControl("ddlRegFilterOp"), DropDownList)
                    If  (ddlRegFilterOp.SelectedIndex > -1) then
                        filters(i) = New FilterInfo()
                        filters(i).PrefId = pid
                        If Not isAdHoc Then
                            filters(i).RptParamId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        Else
                            'If the report is adhoc then RptParamId is the TblFldsId (needed to differentiate between to same type of controls i.e. txtbox/dropdowns)
                            filters(i).RptParamId = CType(ri.FindControl("hfTblFldId"), HiddenField).Value
                            filters(i).AdHocFieldId = CType(ri.FindControl("hfParamId"), HiddenField).Value
                        End If
                        filters(i).OpId = OpType.IsNull
                        filters(i).Value = ddlRegFilterOp.SelectedIndex.ToString()
                        i +=1
                    End If
                Next

                info.Filters = filters

                ' update the list of prefs on the left hand side
                Dim res As String = FAME.AdvantageV1.BusinessFacade.Reports.RptParamsFacade.UpdateUserPref(info, RPTCommon.GetCurrentUserId())
                If res = "" Then
                    RPTCommon.Alert(Page, "User preference was saved successfully")
                Else
                    RPTCommon.Alert(Page, "Errors ocurred while saving the user preference")
                End If

                ' rebind the list of preferences
                BindPrefs()

                ' Advantage specific stuff
                'CommonWebUtilities.SetStyleToSelectedItem(dlPrefs, ViewState("pid"), ViewState)
                CommonWebUtilities.RestoreItemValues(dlPrefs, CType(ViewState("pid"), String))
                InitButtonsForEdit()
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                RPTCommon.DisplayErrorMessage(Page, "Pref could not be saved. Error=" + ex.Message)
            End Try
        End Sub

        ''' <summary>
        ''' Simply reset the form
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnnew.Click
            ViewState("pid") = Nothing
            ResetForm()

            ' Advantage specific stuff
            'Dim objCommon As New CommonUtilities
            'objCommon.SetBtnState(form1, "NEW", Header1.UserPagePermission)
            'Initialize Buttons
            InitButtonsForLoad()
            CommonWebUtilities.RestoreItemValues(dlPrefs, Guid.Empty.ToString)
        End Sub

        ''' <summary>
        ''' Delete a saved pref
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btndelete.Click
            Try
                Dim pid As String = CType(ViewState("pid"), String)
                If Not pid Is Nothing AndAlso pid <> "" Then
                    Dim res As String = FAME.AdvantageV1.BusinessFacade.Reports.RptParamsFacade.DeleteUserPref(pid)
                    If res <> "" Then
                        RPTCommon.DisplayErrorMessage(Me, res)
                    Else
                        ViewState("pid") = Nothing
                        ResetForm()
                        ' show the prefs on the left hand side                    
                        BindPrefs()

                        ' Advantage specific stuff
                        CommonWebUtilities.RestoreItemValues(dlPrefs, Guid.Empty.ToString)
                        InitButtonsForLoad()
                    End If
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                RPTCommon.DisplayErrorMessage(Me, "Error.  Pref could not be deleted.")
            End Try
        End Sub
#End Region

#Region "Helpers for Building the SQL Filter from the Form"
        ''' <summary>
        ''' Retrieves a sql filter string from the list of parameters within the form
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function GetReportFilters() As String
            Dim sb As New StringBuilder()
            Dim rpt As Repeater = CType(Step1.ContentTemplateContainer.FindControl("rptFilters"), Repeater)
            'Dim ReportId As String = ViewState("rid")
            'Dim IsAdHoc As Boolean = ViewState("isadhoc")

            ' iterate through each item in the repeater
            ' each item contains a TblFldId along with a checkbox list of values
            For Each ri As RepeaterItem In rpt.Items
                Dim ddlId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
                Dim fldType As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)
                Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value

                Dim s As String = ""
                'If Not sdfId Is Nothing And fldType = 3 Then
                '    fldType = 135 'date field
                '    CType(ri.FindControl("hfFldType"), HiddenField).Value = fldType
                'End If


                If Not ddlId Is Nothing AndAlso ddlId <> "" And (sdfId Is Nothing Or sdfId = "") Then ' DDL driven form
                    s = _GetReportFilter_DDL(ri)
                ElseIf Not ddlId Is Nothing AndAlso ddlId <> "" And Not sdfId Is Nothing And sdfId <> "" Then
                    s = _GetReportFilter_DDLForSDF(ri)
                ElseIf fldType = FieldType._Bit Then
                    s = _GetReportFilter_BIT(ri)
                ElseIf fldType = FieldType._DataTime And (sdfId Is Nothing Or sdfId = "") Then ' Datetime/Calendar driven form
                    s = _GetReportFilter_Calendar(ri)
                ElseIf fldType = FieldType._DataTime And Not sdfId Is Nothing And sdfId <> "" Then ' Datetime/Calendar driven form
                    s = _GetReportFilter_CalendarForSDF(ri)
                ElseIf fldType = FieldType._Int Or fldType = FieldType._Smallint Or fldType = FieldType._Decimal Then ' numeric driven
                    ' If String.IsNullOrEmpty(sdfId) Then
                    s = _GetReportFilter_NumericInput(ri)
                    'End If
                Else ' The filters came from the user manually entering an operation and a value
                    s = _GetReportFilter_RegularInput(ri)
                End If

                If s.Length > 0 Then
                    sb.Append(String.Format("{0} AND {1}", s, vbCrLf))
                End If
            Next

            ' remove the last "AND"
            If sb.Length > 6 Then sb.Remove(sb.Length - 6, 6)

            Return sb.ToString()
        End Function

        Protected Function _GetReportFilter_RegularInput(ByVal ri As RepeaterItem) As String
            Dim fldTypeId As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)
            Dim txt As TextBox = CType(ri.FindControl("txtRegFilter"), TextBox)
            Dim ddl As DropDownList = CType(ri.FindControl("ddlRegFilterOp"), DropDownList)

            Dim sqlCalculation As String = CType(ri.FindControl("hfCalculationSQL"), HiddenField).Value

            Dim reportId As String = CType(ViewState("rid"), String)
            Dim sb As New StringBuilder
            If ddl.SelectedValue <> "" And txt.Text.Trim <> "" Then
                Dim value As String = txt.Text
                If ddl.SelectedValue = "like" Then
                    value = "%" + value + "%"
                Else If ddl.SelectedValue.Equals("between", StringComparison.InvariantCultureIgnoreCase) Then
                    Dim match As Match = Regex.Match(value,"(\d+\.?\d*)\s(AND|TO){1}\s(\d+\.?\d*)",RegexOptions.IgnoreCase)
                    If match.Success Then
                    value =  match.Groups(1).Value + " AND " +  match.Groups(3).Value
                    Else
                        RPTCommon.Alert(Me, "The value for the Between operator must be in the format: (number and/to number).")
                        Return ""
                    End If
                End If
                value = GetValueForSql(fldTypeId, value)
                ' convert the value to a sql type value (i.e., add single quotes for a string value)

                Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
                Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
                Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value

                Dim sdfFilter As String

                If Not String.IsNullOrEmpty(sqlCalculation) Then


                    Dim ind As Integer = sqlCalculation.ToLower().LastIndexOf(" as ", StringComparison.Ordinal)
                    If ind >= 0 Then
                        sqlCalculation = sqlCalculation.Substring(0, ind)
                    End If

                    Dim s As String = String.Format("{0} {1} {2}", sqlCalculation, ddl.SelectedValue, value)

                    Return s

                Else
                    If tblName = "" Then
                        If ddl.SelectedValue = "like" Then value = "%" + txt.Text + "%"
                        value = GetValueForSql(FieldType._Varchar, value)

                        sdfFilter = AdHocRptFacade.GetColumnSqlFromSDF(reportId, sdfId)
                        Dim cnt As Integer = sdfFilter.Split(CType("$", Char)).Length
                        For i As Integer = 0 To sdfFilter.Split(CType("$", Char)).Length - 1
                            If i = 0 Then sb.AppendFormat("(")
                            If i = cnt - 1 Then
                                sb.AppendFormat(" {0} {1} {2} )", sdfFilter.Split(CType("$", Char))(i), ddl.SelectedValue, value)
                            Else
                                sb.AppendFormat(" {0} {1} {2} or ", sdfFilter.Split(CType("$", Char))(i), ddl.SelectedValue, value)
                            End If

                        Next
                        Return sb.ToString()
                    Else
                        Return String.Format("{0}.{1} {2} {3}", tblName, fldName, ddl.SelectedValue, value)
                    End If

                End If
            End If
            Return "" ' operator was not selected
        End Function


        ''' <summary>
        ''' Returns the sql string when the calendar form is visible
        ''' </summary>
        ''' <param name="ri"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function _GetReportFilter_NumericInput(ByVal ri As RepeaterItem) As String
            Dim fldTypeId As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)
            Dim txt As TextBox = CType(ri.FindControl("txtRegFilter"), TextBox)
            Dim ddl As DropDownList = CType(ri.FindControl("ddlRegFilterOp"), DropDownList)

            Dim sqlCalculation As String = CType(ri.FindControl("hfCalculationSQL"), HiddenField).Value

            Dim reportId As String = CType(ViewState("rid"), String)
            Dim sb As New StringBuilder
            If ddl.SelectedValue <> "" And txt.Text.Trim <> "" Then
                Dim value As String = txt.Text
                If ddl.SelectedValue.Equals("between", StringComparison.InvariantCultureIgnoreCase) Then
                    Dim match As Match = Regex.Match(value,"(\d+\.?\d*)\s(AND|TO){1}\s(\d+\.?\d*)",RegexOptions.IgnoreCase)
                    If match.Success Then
                    value =  match.Groups(1).Value + " AND " +  match.Groups(3).Value
                    Else
                        RPTCommon.Alert(Me, "The value for the Between operator must be in the format: (number and/to number).")
                        Return ""
                    End If
                End If 

                value = GetValueForSql(fldTypeId, value)


                Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
                Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
                Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value

                Dim sdfFilter As String

                If Not String.IsNullOrEmpty(sqlCalculation) Then
                    Dim ind As Integer = sqlCalculation.ToLower().LastIndexOf(" as ", StringComparison.Ordinal)
                    If ind >= 0 Then
                        sqlCalculation = sqlCalculation.Substring(0, ind)
                    End If

                    Dim s As String = String.Format("{0} {1} {2}", sqlCalculation, ddl.SelectedValue, value)

                    Return s

                Else

                    If tblName = "" Then

                        sdfFilter = AdHocRptFacade.GetColumnSqlFromSDF(reportId, sdfId)
                        Dim cnt As Integer = sdfFilter.Split(CType("$", Char)).Length
                        For i As Integer = 0 To sdfFilter.Split(CType("$", Char)).Length - 1
                            If i = 0 Then sb.AppendFormat("(")
                            If i = cnt - 1 Then
                                sb.AppendFormat(" {0} {1} {2} )", sdfFilter.Split(CType("$", Char))(i), ddl.SelectedValue, value)
                            Else
                                sb.AppendFormat(" {0} {1} {2} or ", sdfFilter.Split(CType("$", Char))(i), ddl.SelectedValue, value)
                            End If

                        Next
                        Return sb.ToString()
                    Else
                        Return String.Format("{0}.{1} {2} {3}", tblName, fldName, ddl.SelectedValue, value)
                    End If

                End If
            End If
            Return "" ' operator was not selected

        End Function


    

        ''' <summary>
        ''' Returns the sql string when this is a DDL driven form
        ''' </summary>
        ''' <param name="ri"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function _GetReportFilter_DDL(ByVal ri As RepeaterItem) As String
            Dim fldTypeId As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)
            Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
            Dim sb As New StringBuilder()
            For i As Integer = 0 To dl.Items.Count - 1
                Dim dli As DataListItem = dl.Items(i)
                If CType(dli.FindControl("cbValue"), CheckBox).Checked Then
                    Dim value As String = CType(dli.FindControl("lblId"), Label).Text
                    sb.Append(GetValueForSql(fldTypeId, value) + ",")
                End If
            Next

            ' only add to the filter list if there is length to it
            If sb.Length = 0 Then
                Return ""
            End If

            sb.Remove(sb.Length - 1, 1) ' remove the last comma

            Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
            Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
            Dim s As String = String.Format("{0}.{1} in ({2})", tblName, fldName, sb.ToString())
            Return s
        End Function
        Protected Function _GetReportFilter_DDLForSDF(ByVal ri As RepeaterItem) As String
            Dim fldTypeId As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)
            Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
            Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value
            Dim reportId As String = CType(ViewState("rid"), String)
            Dim sb As New StringBuilder()
            For i As Integer = 0 To dl.Items.Count - 1
                Dim dli As DataListItem = dl.Items(i)
                If CType(dli.FindControl("cbValue"), CheckBox).Checked Then
                    Dim value As String = CType(dli.FindControl("lblId"), Label).Text
                    'if value is a string surround it with tick marks - BHS 11/21/2012
                    'if value is a valid date and the format has slashes do not surround with tick marks - BHS 11/21/2012
                    If IsDate(value) And value.Contains("/") Then
                        sb.Append(GetValueForSql(fldTypeId, value) + ",")
                    Else
                        sb.Append(GetValueForSql(fldTypeId, "'" & value & "'") + ",")
                    End If
                End If
            Next

            ' only add to the filter list if there is length to it
            If sb.Length = 0 Then
                Return ""
            End If
            Dim sdfFilter As String
            sb.Remove(sb.Length - 1, 1) ' remove the last comma

            'Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
            'Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
            sdfFilter = AdHocRptFacade.GetColumnSqlFromSDF(reportId, sdfId)
            Return String.Format("{0} in ({1})", sdfFilter, sb.ToString())

            'Dim s As String = String.Format("{0}.{1} in ({2})", tblName, fldName, sb.ToString())
            'Return s
        End Function
        Protected Function _GetReportFilter_BIT(ByVal ri As RepeaterItem) As String
            Dim fldTypeId As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)


            Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
            Dim sb As New StringBuilder()
            For i As Integer = 0 To dl.Items.Count - 1
                Dim dli As DataListItem = dl.Items(i)
                If CType(dli.FindControl("cbValue"), CheckBox).Checked Then
                    Dim value As String = CType(dli.FindControl("lblId"), Label).Text
                    If value = "True" Then value = "1"
                    If value = "False" Then value = "0"
                    sb.Append(GetValueForSql(fldTypeId, value) + ",")
                End If
            Next

            ' only add to the filter list if there is length to it
            If sb.Length = 0 Then
                Return ""
            End If

            sb.Remove(sb.Length - 1, 1) ' remove the last comma

            Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
            Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
            Dim s As String = String.Format("{0}.{1} in ({2})", tblName, fldName, sb.ToString())
            Return s
        End Function


        ''' <summary>
        ''' Returns the sql string when the calendar form is visible
        ''' </summary>
        ''' <param name="ri"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function _GetReportFilter_Calendar(ByVal ri As RepeaterItem) As String
            ' special case for datetime field.  We need to show controls that allow the user
            ' to enter in an exact date, or a max/min date        
            'Dim txtMin As TextBox = ri.FindControl("txtFilterMin")
            'Dim txtMax As TextBox = ri.FindControl("txtFilterMax")
            Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
            Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
            'Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value
            Dim sqlCalculation As String = CType(ri.FindControl("hfCalculationSQL"), HiddenField).Value

            'Dim sdfFilter As String
            'Dim reportId As String = CType(ViewState("rid"), String)
            Dim sb As New StringBuilder()

        
            'If Not sdfId Is Nothing Then sdfFilter = AdHocRptFacade.GetColumnSqlFromSDF(reportId, sdfId)

            If Not String.IsNullOrEmpty(sqlCalculation) Then
                Dim ind As Integer = sqlCalculation.ToLower().LastIndexOf("as", StringComparison.Ordinal)
                If ind >= 0 Then
                    sqlCalculation = sqlCalculation.Substring(0, ind)
                End If
            End If


            Dim txt As TextBox = CType(ri.FindControl("txtFilterEqual"), TextBox)
            If txt.Text.Length > 0 Then
                Try
                    Dim d As DateTime = CType(txt.Text, DateTime)
                    'Some date fields like adLeads.AssignedDate have the time component on them. In that case we need to return all the records for that day
                    'without taking account of the time component (DE14031). This means that even though the user might enter 7/3/2017
                    'we have to return records between 7/3/2017 00:00:00  and 7/4/2017 00:00:00 in order to get all the records for 7/3/2017.
                    If Not String.IsNullOrEmpty(sqlCalculation) Then
                        sb.AppendFormat("{0} >= '{1}' AND {2} < '{3}' ", sqlCalculation, d.ToShortDateString(), sqlCalculation, DateAdd(DateInterval.Day, 1, d).ToShortDateString)
                    Else
                        sb.AppendFormat("{0}.{1} >= '{2}' AND {3}.{4} < '{5}' ", tblName, fldName, d.ToShortDateString(), tblName, fldName, DateAdd(DateInterval.Day, 1, d).ToShortDateString)
                    End If

                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            End If

            txt = CType(ri.FindControl("txtFilterMin"), TextBox)
            If txt.Text.Length > 0 Then
                Try
                    Dim d As DateTime = CType(txt.Text, DateTime)
                    If sb.Length = 0 Then
                        If Not String.IsNullOrEmpty(sqlCalculation) Then
                            sb.AppendFormat("{0} >= '{1}' ", sqlCalculation, d.ToShortDateString())
                        Else
                            sb.AppendFormat("{0}.{1} >= '{2}' ", tblName, fldName, d.ToShortDateString())
                        End If
                    Else
                        If Not String.IsNullOrEmpty(sqlCalculation) Then
                            sb.AppendFormat(" AND {0} >= '{1}' ", sqlCalculation, d.ToShortDateString())
                        Else
                            sb.AppendFormat(" AND {0}.{1} >= '{2}' ", tblName, fldName, d.ToShortDateString())
                        End If
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            End If

            'When the user enters min = 7/3/2017 and max = 7/4/2017 they are expecting to see all the records between those two dates regardless of the time component.
            'This means the query should return a record with 7/4/2017 08:15:00. 
            txt = CType(ri.FindControl("txtFilterMax"), TextBox)
            If txt.Text.Length > 0 Then
                Try
                    Dim d As DateTime = CType(txt.Text, DateTime)
                    If sb.Length = 0 Then
                        If Not String.IsNullOrEmpty(sqlCalculation) Then
                            sb.AppendFormat("{0} < '{1}' ", sqlCalculation, DateAdd(DateInterval.Day, 1, d).ToShortDateString())
                        Else
                            sb.AppendFormat("{0}.{1} < '{2}' ", tblName, fldName, DateAdd(DateInterval.Day, 1, d).ToShortDateString())
                        End If
                    Else
                        If Not String.IsNullOrEmpty(sqlCalculation) Then
                            sb.AppendFormat(" AND {0} < '{1}' ", sqlCalculation, DateAdd(DateInterval.Day, 1, d).ToShortDateString())
                        Else
                            sb.AppendFormat(" AND {0}.{1} < '{2}' ", tblName, fldName, DateAdd(DateInterval.Day, 1, d).ToShortDateString())
                        End If
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            End If
            Return sb.ToString()

        End Function
        Protected Function _GetReportFilter_CalendarForSDF(ByVal ri As RepeaterItem) As String
            ' special case for datetime field.  We need to show controls that allow the user
            ' to enter in an exact date, or a max/min date        
            'Dim txtMin As TextBox = ri.FindControl("txtFilterMin")
            'Dim txtMax As TextBox = ri.FindControl("txtFilterMax")
            'Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
            'Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
            Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value
            Dim sdfFilter As String
            Dim reportId As String = CType(ViewState("rid"), String)
            Dim sb As New StringBuilder()
            sdfFilter = AdHocRptFacade.GetColumnSqlFromSDFForDate(reportId, sdfId)

            Dim txt As TextBox = CType(ri.FindControl("txtFilterEqual"), TextBox)
            If txt.Text.Length > 0 Then
                Try
                    'Dim d As DateTime = CType(txt.Text, DateTime)
                    If IsDate(txt.Text) Then
                        sb.AppendFormat("{0} = '{1}' ", sdfFilter, txt.Text)
                    End If

                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            End If

            txt = CType(ri.FindControl("txtFilterMin"), TextBox)
            If txt.Text.Length > 0 Then
                Try
                    Dim d As DateTime = CType(txt.Text, DateTime)
                    If sb.Length = 0 Then
                        'sb.AppendFormat("{0} >= '{1}' ", sdfFilter, d.ToShortDateString())
                        sb.AppendFormat("{0} >= '{1}' ", sdfFilter, d.ToString("MM/dd/yyyy"))
                    Else
                        'sb.AppendFormat(" AND {0} >= '{1}' ", sdfFilter, d.ToShortDateString())
                        sb.AppendFormat(" AND {0} >= '{1}' ", sdfFilter, d.ToString("MM/dd/yyyy"))
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            End If

            txt = CType(ri.FindControl("txtFilterMax"), TextBox)
            If txt.Text.Length > 0 Then
                Try
                    Dim d As DateTime = CType(txt.Text, DateTime)
                    If sb.Length = 0 Then
                        'sb.AppendFormat("{0} <= '{1}' ", sdfFilter, d.ToShortDateString())
                        sb.AppendFormat("{0} <= '{1}' ", sdfFilter, d.ToString("MM/dd/yyyy"))
                    Else
                        'sb.AppendFormat(" AND {0} <= '{1}' ", sdfFilter, d.ToShortDateString())
                        sb.AppendFormat(" AND {0} <= '{1}' ", sdfFilter, d.ToString("MM/dd/yyyy"))
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            End If
            Return sb.ToString()
        End Function

        ''' <summary>
        ''' Packages all the TblFldsIds stored in the Sort listbox into an array integers
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function GetTblFldIdsForSort() As Integer()
            Dim lbSel As ListBox = CType(Step2.ContentTemplateContainer.FindControl("lbSelSort"), ListBox)
            Dim res(lbSel.Items.Count - 1) As Integer
            For i As Integer = 0 To lbSel.Items.Count - 1
                res(i) = CType(lbSel.Items(i).Value, Integer)
            Next
            Return res
        End Function

        Protected Function HtmlEncode(ByVal s As String) As String
            s = s.Replace(" ", "&nbsp;")
            s = s.Replace("""", "&quot;")
            s = s.Replace("'", "&#39;")
            Return Server.HtmlEncode(s)
        End Function

        ''' <summary>
        ''' Returns a properly formatted value string for use in a sql statement
        ''' </summary>
        ''' <param name="fldTypeId"></param>
        ''' <param name="value"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function GetValueForSql(ByVal fldTypeId As FieldType, ByVal value As String) As String
            Select Case fldTypeId
                Case FieldType._Bit, 4, 1 'if field type id = 4 or 1 consider it as bit
                    Return value
                Case FieldType._Char
                    Return "'" + value + "'"
                Case FieldType._DataTime
                    Return "'" + value + "'"
                Case FieldType._Decimal
                    Return value
                Case FieldType._Float
                    Return value
                Case FieldType._Int
                    Return value
                Case FieldType._Money
                    Return value
                Case FieldType._Smallint
                    Return value
                Case FieldType._TinyInt
                    Return value
                Case FieldType._Uniqueidentifier
                    Return "'" + value + "'"
                Case FieldType._Unknown
                    Return "'" + value + "'"
                Case FieldType._Varchar
                    Return "'" + value + "'"
            End Select
            Return "'" + value + "'"
        End Function
#End Region

#Region "Event Handlers for Running a Report"
        ''' <summary>
        ''' Checks the filters to see if all required filters have been supplied
        ''' by the end user
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function ValidateFilters() As String
            Dim rpt As Repeater = CType(Step1.ContentTemplateContainer.FindControl("rptFilters"), Repeater)
            For Each ri As RepeaterItem In rpt.Items
                If ri.FindControl("lblReq").Visible Then
                    Dim ddlId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
                    Dim fldType As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)
                    Dim sdfId As String = CType(ri.FindControl("hfSDFId"), HiddenField).Value
                    If Not sdfId Is Nothing And fldType = 3 Then fldType = CType(135, FieldType) 'date field
                    If Not ddlId Is Nothing AndAlso ddlId <> "" Then
                        ' check that at least one value has been specfied in the DDL filter form
                        Dim dl As DataList = CType(ri.FindControl("dlValues"), DataList)
                        Dim bFound As Boolean = False
                        For i As Integer = 0 To dl.Items.Count - 1
                            Dim dli As DataListItem = dl.Items(i)
                            If CType(dli.FindControl("cbValue"), CheckBox).Checked Then
                                bFound = True
                            End If
                        Next
                        If Not bFound Then
                            Return "At least one required filter has not been supplied"
                        End If
                    ElseIf fldType = FieldType._DataTime Then
                        ' check that at least one value has been specfied in the calendar filter form
                        If CType(ri.FindControl("txtFilterEqual"), TextBox).Text = "" AndAlso _
                           CType(ri.FindControl("txtFilterMin"), TextBox).Text = "" AndAlso _
                           CType(ri.FindControl("txtFilterMax"), TextBox).Text = "" Then
                            Return "At least one required filter has not been supplied"
                        End If
                    Else
                        ' check that at least one value has been specfied in the regular filter form
                        Dim txt As TextBox = CType(ri.FindControl("txtRegFilter"), TextBox)
                        Dim ddl As DropDownList = CType(ri.FindControl("ddlRegFilterOp"), DropDownList)
                        If txt.Text = "" Or ddl.SelectedValue = "" Then
                            Return "At least one required filter has not been supplied"
                        End If
                    End If
                End If
            Next
            Return ""
        End Function

        ''' <summary>
        ''' We are "delete after mode".  Hide all controls except for
        ''' the Edit button.  Once in this mode, the user will be redirected back
        ''' to the editor after clicking the edit button
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub SetDeleteAfterMode()
            ViewState("redirect") = "1"
            dlReports.DataSource = Nothing
            dlReports.DataBind()
            ResetForm()
            btnEditReport.Text = "Go Back to Report Editor"
            btnEditReport.Width = Unit.Pixel(180)
            btnFriendlyPrint.Visible = False
            btnViewReport.Visible = False
            lblWarning.Visible = False
            divReportName.Visible = False
            ReportWizard.Visible = False
        End Sub

        ''' <summary>
        ''' User wants to view the report
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewReport.Click
            Dim rid As String = CType(ViewState("rid"), String)
            If rid Is Nothing Or rid = "" Then
                RPTCommon.DisplayErrorMessage(Me, "Please select a report.")
                Return
            End If

            ' ensure that all required filters have been specified
            Dim res As String = ValidateFilters()
            If res <> "" Then
                RPTCommon.DisplayErrorMessage(Me, res)
                Return
            End If

            ' go back to the editor "delete after run" mode
            If Not ViewState("del") Is Nothing AndAlso ViewState("del") = "1" Then
                SetDeleteAfterMode()
            End If

            '   setup the properties of the new window
            Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium
            Const url As String = "AdHocReportViewer.aspx"
            ' Save the parameters to the cache so it can be used on the receiving page
            Session("rptparams") = GetReportFilters()
            Try
                Dim purl As String = String.Format("{0}?rid={1}&del={2}", url, rid, ViewState("del"))
                CommonWebUtilities.OpenChildWindow(Page, purl, "", winSettings)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

        End Sub

        ''' <summary>
        ''' Useer wants a printer friendly version of the report
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub btnFriendlyPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFriendlyPrint.Click
            Dim rid As String = CType(ViewState("rid"), String)
            ' Dim pid As String = ViewState("pid")
            If rid Is Nothing Or rid = "" Then
                RPTCommon.DisplayErrorMessage(Me, "Please select a report.")
                Return
            End If

            ' ensure that all required filters have been specified
            Dim res As String = ValidateFilters()
            If res <> "" Then
                RPTCommon.DisplayErrorMessage(Me, res)
                Return
            End If

            '   setup the properties of the new window
            Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
            Dim url As String = "AdHocReportViewer.aspx"
            ' Save the parameters to the session so it can be used on the receiving page
            Session("rptparams") = GetReportFilters()
            Dim purl As String = String.Format("{0}?print=1&rid={1}&del={2}", url, rid, ViewState("del"))
            CommonWebUtilities.OpenChildWindow(Page, purl, "", winSettings)

            ' go back to the editor "delete after run" mode
            If Not ViewState("del") Is Nothing AndAlso ViewState("del") = "1" Then
                url = String.Format("~/Reports/SetupAdhocReports.aspx?resid=487&mod={0}&cmpid={1}", _
                                    Request.Params("mod"), RPTCommon.GetCampusID())
                Response.Redirect(url, False)
            End If
        End Sub

        ''' <summary>
        ''' Navigates the user back to the maintenance page for editing the report.
        ''' Note: This link is only visible if the current user is the report creator
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub btnEditReport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditReport.Click
            Dim rid As String = CType(ViewState("rid"), String)
            If rid Is Nothing Or rid = "" Then
                RPTCommon.DisplayErrorMessage(Me, "Please select a report before choosing this action.")
                Return
            End If
            Dim url As String = String.Format("~/Reports/SetupAdhocReports.aspx?resid=487&mod={0}&cmpid={1}&id={2}", _
                                              Request.Params("mod"), RPTCommon.GetCampusID(), rid)
            Response.Redirect(url)
        End Sub
#End Region

#Region "Sort Order Event Handlers (Not used right now)"
        ''' <summary>
        ''' User wants to add a sort order
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim lbSel As ListBox = CType(Step2.ContentTemplateContainer.FindControl("lbSelSort"), ListBox)
            Dim lbAll As ListBox = CType(Step2.ContentTemplateContainer.FindControl("lbSort"), ListBox)
            If lbAll.SelectedIndex <> -1 Then
                lbSel.Items.Add(New ListItem(lbAll.SelectedItem.Text, lbAll.SelectedItem.Value))
                lbAll.Items.RemoveAt(lbAll.SelectedIndex)
                If lbAll.SelectedIndex + 1 < lbAll.Items.Count Then
                    lbAll.SelectedIndex = lbAll.SelectedIndex + 1
                End If
            End If
        End Sub

        ''' <summary>
        ''' User wants to remove a sort order
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim lbSel As ListBox = CType(Step2.ContentTemplateContainer.FindControl("lbSelSort"), ListBox)
            Dim lbAll As ListBox = CType(Step2.ContentTemplateContainer.FindControl("lbSort"), ListBox)
            If lbSel.SelectedIndex <> -1 Then
                lbAll.Items.Add(New ListItem(lbSel.SelectedItem.Text, lbSel.SelectedItem.Value))
                lbSel.Items.RemoveAt(lbSel.SelectedIndex)
            End If
        End Sub
#End Region

        Public Overrides Sub SetPageTitle()
            'DE8550 need to override because the reports have their own resource id which isn't the same as the one in syResources
            Header.Title = "AdHoc Reports"
            AdvantageSession.PageBreadCrumb = CurrentPageAsFriendlyPath()
        End Sub
        'EMpty session values.
        Protected Sub Page_Unload(sender As Object, e As EventArgs) Handles Me.Unload
            'Remove Session  dsReport that was created in AdHocreportViewer.aspx.vb
            If Not (Session("dsReport") Is Nothing) Then
                Session.Remove("dsReport")
            End If
        End Sub
    End Class
End NameSpace