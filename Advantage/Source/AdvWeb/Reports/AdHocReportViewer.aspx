<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AdHocReportViewer.aspx.vb" Inherits="Reports_AdHocReportViewer" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/usercontrols/AdvantageRadAjaxLoadingControl.ascx" TagPrefix="uc1" TagName="AdvantageRadAjaxLoadingControl" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Adhoc Report Viewer</title>
    <link href="../css/localhost.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .ReportScroll {
            overflow: auto;
            height: expression(document.body.clientHeight - 100 + "px");
            width: 710px;
            padding: 10px;
            margin-right: 1px;
        }

        .ReportMainBorder {
            border: 1px solid #a5c7e7;
            text-align: center;
            align-content: center;
            width: 98%;
            padding: 4px;
            margin-left: 5px;
            margin-right: 5px;
        }

        .ReportHeader {
            font-family: arial;
            font-size: 12px;
            color: Black;
            font-weight: bold;
        }

        .ReportFilter {
            font-family: arial;
            font-size: 9px;
            color: Black;
            font-weight: bold;
        }

        .ReportHeaderDetails {
            font-family: arial;
            font-size: 9px;
            color: Black;
            font-weight: bold;
        }

        .ReportFooterDetails {
            font-family: arial;
            font-size: 9px;
            color: Black;
            font-weight: bold;
        }

        .ReportDetails {
            font-family: arial;
            font-size: 9px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function getInnerHtml() {
            var element = document.getElementById("divGrid");
            var store = document.getElementById("hfInnerHtml");
            //add the css styles you have used inside the nested GridView
            var css = ""; //"<style type=\"text/css\" id=\"style1\">.textbold {font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #000000;font-weight: bold;text-decoration: none;}.row1 {background-color: #FFFFFF; font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #000000;height: 18px;padding-left: 5px;}.;
            store.value = css + element.innerHTML;
        }


    </script>
</head>
<body style="text-align: center">
    <form id="form1" runat="server">

        <telerik:RadAjaxLoadingPanel ID="AdvantageRadAjaxLoadingControl1" runat="server" InitialDelayTime="300" MinDisplayTime="500">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" UpdateInitiatorPanelsOnly="True">
        </telerik:RadAjaxManager>
        <asp:ScriptManager ID="RadScriptManager1" runat="server">
        </asp:ScriptManager>

        <div>
            <div>

                <asp:Button ID="btnShowSQL" runat="server" CssClass="button" Text="Show SQL" Width="125px" Visible="False" />
            </div>
            <br />
        </div>
        <div style="text-align: center">
            <div>
                <asp:TextBox ID="txtDebug" runat="server" CssClass="TextBox" Width="600px" Rows="10" TextMode="MultiLine" Visible="False" />
            </div>
            <!-- begin Report -->
            <div class="ReportMainBorder">

                <!-- begin Report Contents -->
                <div id='divGrid' runat="server" style="width: 98%; text-align: center">
                    <div align="center" id="divNoRecords" runat="server" visible="False" class="Label">No Records</div>
                    <br />
                    <div align="Right" id="divbuttons" runat="server" class="Label">
                        <asp:LinkButton runat="server" ID="btnExportWord" OnClick="btnExportWord_Click">Export to Word</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton runat="server" ID="btnExportExcel" OnClick="btnExportExcel_Click">Export to Excel</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton runat="server" ID="btnExportPdf" OnClick="btnExportPdf_OnClick">Export to PDF</asp:LinkButton>
                    </div>
                    <div style="text-align: left">
                        <table>
                            <tr>
                                <td>
                                    <asp:Image ID="imgSchoolLogo" runat="server" Visible="false" Width="120px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSchoolName" runat="server" Font-Size="14px" />
                                </td>
                            </tr>
                        </table>

                    </div>

                    <asp:Repeater ID="rptReport" runat="server">
                        <ItemTemplate>
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true" LoadingPanelID="AdvantageRadAjaxLoadingControl1">
                                <div id="AjaxRefresh" style="text-align: left" runat="server">
                                    <telerik:RadGrid ID="gvReport" runat="server" AllowPaging="true" PageSize="25"
                                        OnItemDataBound="gvReport_ItemDataBound" OnNeedDataSource="gvReport_NeedDataSource"
                                                     OnItemCreated="gvReport_OnItemCreated"
                                        ClientSettings-Resizing-AllowColumnResize="true" EnableAjaxSkinRendering="true">
                                        <ExportSettings IgnorePaging="true" OpenInNewWindow="true" >
                                            <Pdf PageHeight="210mm" PageWidth="300mm" DefaultFontFamily="Arial Unicode MS" PageTopMargin="5mm"
                                                PageBottomMargin="5mm" PageLeftMargin="1mm" PageRightMargin="1mm"
                                                BorderStyle="Thin" BorderColor="#2a2a2a" BorderType="TopAndBottom">
                                            </Pdf>
                                            <Excel Format="Xlsx" DefaultCellAlignment="Left" AutoFitImages="True"></Excel>
                                            <Word Format="Docx"></Word>
                                        </ExportSettings>
                                        <MasterTableView CommandItemDisplay="Top" HorizontalAlign="NotSet">
                                            <CommandItemTemplate>
                                                <div id="reportTitle" style="padding-top: 5px">
                                                    <asp:Label ID="lblReportName" runat="server" Font-Size="10pt" Font-Bold="True" Visible="False" />
                                                    <asp:Label ID="lblTitle" runat="server" Font-Size="10pt" Font-Bold="True"></asp:Label>&nbsp; -
                                                    <asp:Label ID="lblRecords" runat="server" Font-Size="10pt"></asp:Label>&nbsp; 
                                                </div>
                                            </CommandItemTemplate>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <br />
                                    <div style="page-break-before: always" clear="all">
                                    </div>
                                </div>
                            </telerik:RadAjaxPanel>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>
                <!-- end Report Contents -->

            </div>
            <!-- end Report -->
        </div>
        <asp:HiddenField ID="hfInnerHtml" runat="server" />
    </form>
</body>
</html>
