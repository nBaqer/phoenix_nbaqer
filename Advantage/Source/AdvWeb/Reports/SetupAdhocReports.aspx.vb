' ===============================================================================
' SetupAdhocReports
' Handles add, edit and delete of reports
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.Reports
Imports FAME.AdvantageV1.Common.Reports
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Partial Class Reports_SetupAdhocReports
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected MyAdvAppSettings As AdvAppSettings

    Private Const COMBOBOX_DEFAULT_SELECT As String = "--Select--"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim mContext As HttpContext
        Dim advantageUserState As User = AdvantageSession.UserState
        ResourceId = HttpContext.Current.Request.Params("resid")
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        RPTCommon.AdvInit(Me)
        If Not Page.IsPostBack Then
            ' add javascript to the delete button
            btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)
            ' load the form with its default values
            ResetForm()
            ' display the AdHoc Reports defined in the system on the left pane
            BindReports()
            ' Bind the task to the form
            Dim Id As String = Request.Params("id")
            If Not Id Is Nothing Then
                BindReport(Id)
            End If

            ' initialize the wizard here
        Else
            InitButtonsForEdit()
        End If

        SetPageTitle()

    End Sub

#Region "Advantage Permissions"
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            'btnnew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub
#End Region

#Region "Common Binding Methods for Maintenance Pages"
    ''' <summary>
    ''' Starts the wizard with the give AdHoc report id
    ''' </summary>
    ''' <param name="Id"></param>
    ''' <remarks></remarks>
    Protected Sub BindReport(ByVal Id As String)
        Try
            Dim info As AdHocRptInfo = AdHocRptFacade.GetReportInfo(Id)
            ViewState("id") = info.ReportId ' just to make sure
            Dim dsFields As DataSet = AdHocRptFacade.GetReportFieldsDS(Id)
            ' save these objects to the viewstate to be used during
            ' the transition from Step 2 to 3
            ViewState("Fields") = dsFields.Tables(0)
            ViewState("Summary") = info.ShowSummary
            ViewState("GroupBySummary") = info.ShowGroupBySummary

            Dim txt As TextBox
            Dim ddl As DropDownList
            Dim cb As CheckBox
            Dim dl As DataList
            Dim lb As Label
            Dim cbLtJoin As CheckBox

            ' -------------------------------------------------------------------------
            ' Bind values for Step 1  
            ' Set the Author or owner name
            lb = CType(Step1.ContentTemplateContainer.FindControl("LbReportCreatorText"), Label)
            If (String.IsNullOrEmpty(info.CreatedBy)) Then
                lb.Text = "Creator not Found"
            Else
                lb.Text = info.CreatedBy
            End If
            'If (Not (String.IsNullOrEmpty(info.ModUser)) AndAlso info.ModUser <> info.CreatedBy  )
            '    lb.Text &= "  Modified by " + info.ModUser
            'End If

            ' Set the Report Name
            txt = CType(Step1.ContentTemplateContainer.FindControl("txtReportName"), TextBox)
            txt.Text = info.ReportName

            ' Set the Report Type
            ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlReportType"), DropDownList)
            ddl.SelectedValue = CType(info.ReportType, String)

            ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlCampGroup"), DropDownList)
            ddl.SelectedValue = info.CampGrpId

            ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlActive"), DropDownList)
            ddl.SelectedValue = CType(info.Active, String)

            cb = CType(Step1.ContentTemplateContainer.FindControl("cbPublic"), CheckBox)
            cb.Checked = info.IsPublic

            cbLtJoin = CType(Step1.ContentTemplateContainer.FindControl("cbLeftJoin"), CheckBox)
            cbLtJoin.Checked = info.UseLeftJoin

            dl = CType(Step1.ContentTemplateContainer.FindControl("dlModules"), DataList)
            RPTCommon.BuildModulesDL(RPTCommon.GetCurrentUserId(), info.ReportId, dl)

            ' -------------------------------------------------------------------------
            ' Bind values for Step 2
            ' Bind the list of fields configured for this report
            Dim dg As DataGrid = CType(Step2.ContentTemplateContainer.FindControl("dgFields"), DataGrid)
            dg.DataSource = dsFields
            dg.DataBind()

            ddl = CType(Step2.ContentTemplateContainer.FindControl("ddlCategories"), DropDownList)
            RPTCommon.BuildCategoriesDDL(info.ReportType, ddl)

            ' -------------------------------------------------------------------------
            ' Bind values for Step 3 and Step 4
            DoStep2Transition()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            RPTCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub


    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist
    ''' In response, this handler should populate the info
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlReports_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlReports.ItemCommand
        ' get the selected resultid and store it in the viewstate object
        Dim id As String = e.CommandArgument.ToString
        ViewState("id") = id
        dlReports.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If String.IsNullOrEmpty(id) Then
            RPTCommon.AlertAjax(Me, "There was a problem displaying this report.", CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager))
            Return
        End If

        ' Clean out any previous form data before loading the new data
        ResetForm()

        ' bind the report to the form
        BindReport(id)

        ' Advantage specific stuff
        'CommonWebUtilities.SetStyleToSelectedItem(dlReports, id, ViewState, Header1)
        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlReports, ViewState("id").ToString)

        SetPageTitle()

    End Sub

    ''' <summary>
    ''' Binds the reports to the datalist on the left pane.
    ''' Depending on the state of the status selector, we show active, inactive or all
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindReports()
        Try
            ' get the filters
            Dim modulexId As String = String.Empty
            Dim entityId As String = String.Empty

            ' do the bind
            ' 10/27/06 - Add support for the "All Campus Group"
            If radStatus.SelectedIndex = 0 Then
                dlReports.DataSource = AdHocRptFacade.GetReports(RPTCommon.GetCurrentUserId(), RPTCommon.GetCampusID(), modulexId, entityId, True, False)
            ElseIf radStatus.SelectedIndex = 1 Then
                dlReports.DataSource = AdHocRptFacade.GetReports(RPTCommon.GetCurrentUserId(), RPTCommon.GetCampusID(), modulexId, entityId, False, True)
            Else
                dlReports.DataSource = AdHocRptFacade.GetReports(RPTCommon.GetCurrentUserId(), RPTCommon.GetCampusID(), modulexId, entityId, True, True)
            End If
            dlReports.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            RPTCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Display active, inactive or all based on the user checking the radio box option
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        ViewState("id") = Nothing
        ResetForm()
        BindReports()
    End Sub


    ''' <summary>
    ''' Resets the form to the intial state.  All controls that are databound are initialized
    ''' from their datasource.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetForm()
        Dim txt As TextBox
        Dim lb As ListBox
        Dim ddl As DropDownList
        Dim cb As CheckBox
        Dim dl As DataList
        dim cbLtJoin As CheckBox
        dim lblBlkFld as Label
        ' reset the view states
        ViewState("Fields") = Nothing
        ViewState("Summary") = Nothing
        ViewState("GroupBySummary") = Nothing

        ' always go to the first step when the form is reset
        ReportWizard.MoveTo(Step1)

        ' Reset the form for Step 1
        txt = CType(Step1.ContentTemplateContainer.FindControl("txtReportName"), TextBox)
        txt.Text = ""

        ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlReportType"), DropDownList)
        RPTCommon.BuildReportTypesDDL(RPTCommon.GetCurrentUserId(), ddl)

        ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlCampGroup"), DropDownList)
        RPTCommon.BuildCampusGroups(ddl)

        ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlActive"), DropDownList)
        RPTCommon.BuildStatusDDL(ddl)
        ddl.SelectedValue = True ' default to Active

        cb = CType(Step1.ContentTemplateContainer.FindControl("cbPublic"), CheckBox)
        cb.Checked = True
        Dim showLeftJoin As String = "no"
        if MyAdvAppSettings.AppSettings("ShowBlankFieldsOptionInAdhoc") = nothing
            showLeftJoin = "no"
        else
            showLeftJoin = CType(MyAdvAppSettings.AppSettings("ShowBlankFieldsOptionInAdhoc"), String)
        End If
        cbLtJoin = CType(Step1.ContentTemplateContainer.FindControl("cbLeftJoin"), CheckBox)
        lblBlkFld = CType(Step1.ContentTemplateContainer.FindControl("lblShowBlankFld"), Label)  
        cbLtJoin.Checked = False
        lblBlkFld.ToolTip = "If you notice a set of students missing from your report, try using this option. *Note* This is a beta feature and may cause issues with the order of your report columns or show duplicate student records. Please check report data after running."
        If(showLeftJoin.ToLower().Equals("no"))
            cbLtJoin.Visible = False
            lblBlkFld.Visible = False
        Else 
            cbLtJoin.Visible = True
            lblBlkFld.Visible = True
        End If
        
        
        
        

        dl = CType(Step1.ContentTemplateContainer.FindControl("dlModules"), DataList)
        RPTCommon.BuildModulesDL(RPTCommon.GetCurrentUserId(), Nothing, dl)

        ' -------------------------------------------------------------------------
        ' Reset the form for Step 2
        ddl = CType(Step2.ContentTemplateContainer.FindControl("ddlCategories"), DropDownList)
        ddl.Items.Clear()

        Dim dg As DataGrid = CType(Step2.ContentTemplateContainer.FindControl("dgFields"), DataGrid)
        dg.DataSource = Nothing
        dg.DataBind()

        lb = CType(Step2.ContentTemplateContainer.FindControl("lbAvailableFields"), ListBox)
        lb.Items.Clear()

        ' -------------------------------------------------------------------------
        ' Reset the form for Step 3
        ddl = CType(Step3.ContentTemplateContainer.FindControl("ddlGroupBy"), DropDownList)
        ddl.Items.Clear()

        ddl = CType(Step3.ContentTemplateContainer.FindControl("ddlSortBy"), DropDownList)
        ddl.Items.Clear()

        cb = CType(Step3.ContentTemplateContainer.FindControl("cbSummary"), CheckBox)
        cb.Checked = False

        cb = CType(Step3.ContentTemplateContainer.FindControl("cbGroupBySummary"), CheckBox)
        cb.Checked = False

        dl = CType(Step3.ContentTemplateContainer.FindControl("dlSumFields"), DataList)
        dl.DataSource = Nothing
        dl.DataBind()


        ' -------------------------------------------------------------------------
        ' Reset the form for Step 4
        dl = CType(Step4.ContentTemplateContainer.FindControl("dlParams"), DataList)
        dl.DataSource = Nothing
        dl.DataBind()


        ' -------------------------------------------------------------------------
        ' Reset the form for Step 5
    End Sub

#End Region

#Region "New, Edit and Save Handlers"
    Protected Sub DoSave()
        Try
            If Not ValidateForm() Then
                Return
            End If

            Dim id As String = CType(ViewState("id"), String)
            Dim info As New AdHocRptInfo
            ' determine if this is an update or a new record
            ' at the ViewState("id") which is set by passing a parameter to the page.
            If String.IsNullOrEmpty(id) Then
                info.IsInDB = False ' It is a new report.....
                ViewState("id") = info.ReportId
            Else
                info.IsInDB = True
                info.ReportId = id
            End If

            UpdateInfoFromStep1(info)
            UpdateInfoFromStep2(info)

            info.ModUser = RPTCommon.GetCurrentUserId()
            info.ModDate = Date.Now

            ' do the add or update 
            Dim opOk As Boolean = AdHocRptFacade.UpdateReport(info, RPTCommon.GetCurrentUserId())
            If opOk Then
                RPTCommon.AlertAjax(Page, "Report was saved successfully", CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager))
            Else
                RPTCommon.AlertAjax(Page, "Errors ocurred while saving the report", CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager))
            End If

            ' update the left pane with the new set of reports
            BindReports()

            ' Advantage specific stuff
            ViewState("id") = info.ReportId ' In the case of an add, the reportid is not avaialble until after
            ' we do the UpdateReport operation.
            ' CommonWebUtilities.SetStyleToSelectedItem(dlReports, id, ViewState, Header1)
            InitButtonsForEdit()
            CommonWebUtilities.RestoreItemValues(dlReports, ViewState("id").ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            RPTCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Updates the AdHocRptInfo object with values found on step 1
    ''' </summary>
    ''' <param name="info"></param>
    ''' <remarks></remarks>
    Protected Sub UpdateInfoFromStep1(ByRef info As AdHocRptInfo)
        Dim txt As TextBox
        Dim ddl As DropDownList
        Dim cb As CheckBox
        Dim dl As DataList
        Dim hf As HiddenField
        dim cbLeftJoin as CheckBox

        txt = Step1.ContentTemplateContainer.FindControl("txtReportName")
        info.ReportName = txt.Text

        ddl = Step1.ContentTemplateContainer.FindControl("ddlReportType")
        info.ReportType = ddl.SelectedValue

        ddl = Step1.ContentTemplateContainer.FindControl("ddlCampGroup")
        info.CampGrpId = ddl.SelectedValue
        ViewState("CampGrpId") = info.CampGrpId
        cb = Step1.ContentTemplateContainer.FindControl("cbPublic")
        info.IsPublic = cb.Checked

        Dim showLeftJoin As String = CType(MyAdvAppSettings.AppSettings("ShowBlankFieldsOptionInAdhoc"), String)
        If(showLeftJoin.ToLower().Equals("no"))
            info.UseLeftJoin = False
        Else 
            cbLeftJoin = Step1.ContentTemplateContainer.FindControl("cbLeftJoin")
            info.UseLeftJoin = cbLeftJoin.Checked
        End If

        ddl = Step1.ContentTemplateContainer.FindControl("ddlActive")
        info.Active = CType(ddl.SelectedValue, Boolean)

        ' update the permissions        
        dl = Step1.ContentTemplateContainer.FindControl("dlModules")
        Dim pInfo(dl.Items.Count - 1) As AdHocPermissionInfo
        For i As Integer = 0 To dl.Items.Count - 1
            hf = CType(dl.Items(i).FindControl("hfModuleId"), HiddenField)
            cb = CType(dl.Items(i).FindControl("cbModule"), CheckBox)
            pInfo(i) = New AdHocPermissionInfo()
            pInfo(i).ReportId = info.ReportId
            pInfo(i).ModuleId = hf.Value
            pInfo(i).Permission = cb.Checked
        Next

        info.Permissions = pInfo
    End Sub

    ''' <summary>
    ''' Updates the AdHocRptInfo object with values found on steps 2 and 3
    ''' </summary>
    ''' <param name="info"></param>
    ''' <remarks></remarks>
    Protected Sub UpdateInfoFromStep2(ByRef info As AdHocRptInfo)
        Dim ddl As DropDownList
        Dim cb As CheckBox
        Dim dl As DataList
        Dim dl2 As DataList

        cb = CType(Step3.ContentTemplateContainer.FindControl("cbSummary"), CheckBox)
        info.ShowSummary = cb.Checked

        Dim cb2 As CheckBox = CType(Step3.ContentTemplateContainer.FindControl("cbGroupBySummary"), CheckBox)
        info.ShowGroupBySummary = cb2.Checked

        Dim dg As DataGrid = CType(Step2.ContentTemplateContainer.FindControl("dgFields"), DataGrid)
        dl = CType(Step4.ContentTemplateContainer.FindControl("dlParams"), DataList)
        dl2 = CType(Step3.ContentTemplateContainer.FindControl("dlSumFields"), DataList)

        ' if data has not been bound to control, then add new fields
        ' this is wrong,we have to disable the Save buton, but for now will leave it
        If dg.Items.Count > 0 And dl2.Items.Count <> dg.Items.Count Then
            DoStep2Transition()
        End If

        ' Save the fields
        Dim i As Integer = dg.Items.Count
        Dim fldInfo(i - 1) As AdHocFieldInfo
        For i = 0 To dg.Items.Count - 1
            Dim dgi As DataGridItem = dg.Items(i)
            fldInfo(i) = New AdHocFieldInfo
            fldInfo(i).ReportId = info.ReportId
            ' get the id of the field and put it in either TblFldsId or SDFId
            ' TblFldsId is an Integer and SDFId is a guid so we are doing a little
            ' hack by checking the length.  We assume that if the length is greater
            ' than 6, then the field is a guid so it belongs to SDFId.
            Dim id As String = CType(dgi.FindControl("hfTblFldsId"), HiddenField).Value
            If id.Length > 6 Then
                fldInfo(i).SDFId = id
            Else
                fldInfo(i).TblFldsId = id
            End If
            fldInfo(i).ColOrder = i
            fldInfo(i).Header = CType(dgi.FindControl("txtHeader"), TextBox).Text
            fldInfo(i).FormatString = CType(dgi.FindControl("ddlFormat"), DropDownList).SelectedValue
            fldInfo(i).Visible = CType(dgi.FindControl("cbVisible"), CheckBox).Checked
            fldInfo(i).Width = CType(CType(dgi.FindControl("txtWidth"), TextBox).Text, Integer)
            fldInfo(i).ShowInSummary = cb.Checked
            fldInfo(i).ShowInGroupBy = cb2.Checked

            ' set IsParam and IsRequireds
            fldInfo(i).IsParam = CType(dl.Items(i).FindControl("cbParam"), CheckBox).Checked
            fldInfo(i).IsRequired = CType(dl.Items(i).FindControl("cbRequired"), CheckBox).Checked

            ' Set the summary stats
            fldInfo(i).SummaryType = 0
            Dim cbCount As CheckBox = CType(dl2.Items(i).FindControl("cbShowCount"), CheckBox)
            Dim cbSum As CheckBox = CType(dl2.Items(i).FindControl("cbShowSum"), CheckBox)
            Dim cbMax As CheckBox = CType(dl2.Items(i).FindControl("cbShowMax"), CheckBox)
            Dim cbMin As CheckBox = CType(dl2.Items(i).FindControl("cbShowMin"), CheckBox)
            Dim cbAvg As CheckBox = CType(dl2.Items(i).FindControl("cbShowAvg"), CheckBox)
            Dim cbMerge As CheckBox = CType(dl2.Items(i).FindControl("cbMerge"), CheckBox)
            If cbCount.Checked Then fldInfo(i).SummaryType = fldInfo(i).SummaryType Or SummaryTypes.Count
            If cbSum.Checked Then fldInfo(i).SummaryType = fldInfo(i).SummaryType Or SummaryTypes.Sum
            If cbMax.Checked Then fldInfo(i).SummaryType = fldInfo(i).SummaryType Or SummaryTypes.Max
            If cbMin.Checked Then fldInfo(i).SummaryType = fldInfo(i).SummaryType Or SummaryTypes.Min
            If cbAvg.Checked Then fldInfo(i).SummaryType = fldInfo(i).SummaryType Or SummaryTypes.Avg
            If cbMerge.Checked Then fldInfo(i).SummaryType = fldInfo(i).SummaryType Or SummaryTypes.Merge

            ddl = CType(Step3.ContentTemplateContainer.FindControl("ddlGroupby"), DropDownList)
            If Not ddl Is Nothing AndAlso Not ddl.SelectedItem Is Nothing AndAlso (ddl.SelectedItem.Value = fldInfo(i).TblFldsId Or ddl.SelectedItem.Value = fldInfo(i).SDFId) Then
                fldInfo(i).GroupBy = True
            Else
                fldInfo(i).GroupBy = False
            End If
            ddl = CType(Step3.ContentTemplateContainer.FindControl("ddlSortby"), DropDownList)
            If Not ddl Is Nothing AndAlso Not ddl.SelectedItem Is Nothing AndAlso (ddl.SelectedItem.Value = fldInfo(i).TblFldsId Or ddl.SelectedItem.Value = fldInfo(i).SDFId) Then
                fldInfo(i).SortBy = True
            Else
                fldInfo(i).SortBy = False
            End If
        Next
        info.Fields = fldInfo
    End Sub


    ''' <summary>
    ''' Adds or updates a Reports
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        DoSave()
    End Sub

    ''' <summary>
    ''' Simply reset the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        ViewState("id") = Nothing
        ResetForm()

        ' Advantage specific stuff
        'CommonWebUtilities.SetStyleToSelectedItem(Me.dlReports, Guid.Empty.ToString, ViewState, Header1)
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlReports, Guid.Empty.ToString)
    End Sub

    ''' <summary>
    ''' Delete the report
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        Dim id As String = CType(ViewState("id"), String)
        If Not id Is Nothing AndAlso id <> "" Then
            Dim res As String = AdHocRptFacade.DeleteReport(id, RPTCommon.GetCurrentUserId())
            If res <> "" Then
                RPTCommon.DisplayErrorMessage(Me, res)
            Else
                ViewState("id") = Nothing
                ResetForm()
                ' update the left pane
                BindReports()

                ' Advantage specific stuff
                'CommonWebUtilities.SetStyleToSelectedItem(Me.dlReports, Guid.Empty.ToString, ViewState, Header1)
                InitButtonsForLoad()
            End If
            CommonWebUtilities.RestoreItemValues(dlReports, Guid.Empty.ToString)
        End If
    End Sub
#End Region

#Region "Transition Handlers"
    ''' <summary>
    ''' Bind controls on steps 3 and 4 that have dependencies on step 2
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub DoStep2Transition()
        ' Retrieve all the params from the viewstate
        Dim dtFields As DataTable = ViewState("Fields")
        Dim ShowSummary As Boolean = False
        Dim ShowGroupBySummary As Boolean = False

        If Not ViewState("Summary") Is Nothing Then
            ShowSummary = ViewState("Summary")
        End If

        If Not ViewState("GroupBySummary") Is Nothing Then
            ShowGroupBySummary = ViewState("GroupBySummary")
        End If

        ' declare some vars to be used to fill up steps 3 and 4
        Dim ddl As DropDownList
        Dim cb As CheckBox
        Dim dl As DataList

        ' GroupBy dropdownlist on Step 3

        ddl = Step3.ContentTemplateContainer.FindControl("ddlGroupBy")
        Dim filteredDtFields As DataTable = AdHocRptFacade.FilterCalculatedFieldFromTable(dtFields)
        ddl.DataSource = filteredDtFields
        ddl.DataTextField = "Header"
        ddl.DataValueField = "TblFldsId"
        ddl.DataBind()
        ddl.Items.Insert(0, COMBOBOX_DEFAULT_SELECT)
        'Dim aInfo As AdHocFieldInfo()
        'aInfo = AdHocRptFacade.GetReportFieldsSDF(ViewState("id"))
        'Select the selected value in combobox
        If Not dtFields Is Nothing Then
            For Each dr As DataRow In filteredDtFields.Rows
                If dr("GroupBy") = True AndAlso dr("TblFldsId").ToString() <> "" Then
                    ddl.SelectedValue = dr("TblFldsId")
                End If
            Next
        End If

        'If Not aInfo Is Nothing Then
        '    ddl.DataSource = aInfo
        '    ddl.DataTextField = "Header"
        '    ddl.DataValueField = "TblFldsId"
        '    ddl.DataBind()
        '    ddl.Items.Insert(0, "--Select--")
        '    For i As Integer = 0 To aInfo.Length - 1
        '        If aInfo(i).GroupBy = True Then
        '            ddl.SelectedValue = aInfo(i).TblFldsId
        '        End If
        '    Next
        'ElseIf Not dtFields Is Nothing Then
        '    ddl.DataSource = dtFields
        '    ddl.DataTextField = "Header"
        '    ddl.DataValueField = "TblFldsId"
        '    ddl.DataBind()
        '    ddl.Items.Insert(0, "--Select--")
        'End If


        ' SortyBy dropdownlist on Step3
        ddl = Step3.ContentTemplateContainer.FindControl("ddlSortBy")

        ddl.DataSource = dtFields
        ddl.DataTextField = "Header"
        ddl.DataValueField = "TblFldsId"
        ddl.DataBind()
        ddl.Items.Insert(0, COMBOBOX_DEFAULT_SELECT)
        If Not dtFields Is Nothing Then
            For Each dr As DataRow In dtFields.Rows
                If dr("SortBy") = True AndAlso dr("TblFldsId").ToString() <> "" Then
                    ddl.SelectedValue = dr("TblFldsId")
                End If
            Next
        End If
        'If Not aInfo Is Nothing Then
        '    ddl.DataSource = aInfo
        '    ddl.DataTextField = "Header"
        '    ddl.DataValueField = "TblFldsId"
        '    ddl.DataBind()
        '    ddl.Items.Insert(0, "--Select--")
        '    For i As Integer = 0 To aInfo.Length - 1
        '        If aInfo(i).SortBy = True Then
        '            ddl.SelectedValue = aInfo(i).TblFldsId
        '        End If
        '    Next
        'ElseIf Not dtFields Is Nothing Then
        '    ddl.DataSource = dtFields
        '    ddl.DataTextField = "Header"
        '    ddl.DataValueField = "TblFldsId"
        '    ddl.DataBind()
        '    ddl.Items.Insert(0, "--Select--")
        'End If


        ' Show Summary checkbox on Step 3
        cb = Step3.ContentTemplateContainer.FindControl("cbSummary")
        cb.Checked = ShowSummary

        ' Show Summary for each Group on Step 3
        cb = Step3.ContentTemplateContainer.FindControl("cbGroupBySummary")
        cb.Checked = ShowGroupBySummary

        ' display all the field for the summary list
        Dim dlSumFields As DataList = Step3.ContentTemplateContainer.FindControl("dlSumFields")
        dlSumFields.DataSource = dtFields
        dlSumFields.DataBind()

        ' Params datalist on Step 4
        dl = Step4.ContentTemplateContainer.FindControl("dlParams")
        dl.DataSource = dtFields
        dl.DataBind()
    End Sub

    ''' <summary>
    ''' Captures the transition from one step to another.    
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Change the message error routine to ajax compatible</remarks>
    Protected Sub OnNextClick(ByVal sender As Object, ByVal e As WizardNavigationEventArgs) Handles ReportWizard.NextButtonClick
        If ReportWizard.ActiveStep.ID = Step1.ID Then
            Dim res As String = ValidateStep1()
            If res <> "" Then ' step not valid

                RPTCommon.AlertAjax(Me, res, Me.Master.FindControl("RadAjaxManager1"))
                '.DisplayErrorMessage(Me, res)
                e.Cancel = True ' Stay on Step 1                
                Return
            End If
        End If

        If ReportWizard.ActiveStep.ID = Step2.ID Then
            Dim res As String = ValidateStep2()
            If res <> "" Then ' step not valid
                RPTCommon.AlertAjax(Me, res, Me.Master.FindControl("RadAjaxManager1"))
                'RPTCommon.DisplayErrorMessage(Me, res)
                e.Cancel = True ' Stay on Step 2
                Return
            End If
            DoStep2Transition() ' Bind controls on steps 3 and 4            
        End If

        If ReportWizard.ActiveStep.ID = Step3.ID Then
            Dim res As String = ValidateStep3()
            If res <> "" Then ' step not valid
                RPTCommon.AlertAjax(Me, res, Me.Master.FindControl("RadAjaxManager1"))
                'RPTCommon.DisplayErrorMessage(Me, res)
                e.Cancel = True ' Stay on Step 3
                Return
            End If
        End If

        If ReportWizard.ActiveStep.ID = Step4.ID Then
            Dim res As String = ValidateStep3()
            If res <> "" Then ' step not valid
                RPTCommon.AlertAjax(Me, res, Me.Master.FindControl("RadAjaxManager1"))
                'RPTCommon.DisplayErrorMessage(Me, res)
                e.Cancel = True ' Stay on Step 4
                Return
            End If
        End If
    End Sub
#End Region

#Region "Validation"
    ''' <summary>
    ''' Validates all the entries in the wizard.
    ''' This is used by the common "DoSave" method.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>JAGG - Changed the Alert Message Routine to work with Ajax.</remarks>
    Protected Function ValidateForm() As Boolean
        Dim res As String

        res = ValidateStep1()
        If res <> "" Then
            RPTCommon.AlertAjax(Me, res, Me.Master.FindControl("RadAjaxManager1"))
            'RPTCommon.DisplayErrorMessage(Me, "Error on Step 1. " + res)
            Return False
        End If

        res = ValidateStep2()
        If res <> "" Then
            RPTCommon.AlertAjax(Me, res, Me.Master.FindControl("RadAjaxManager1"))
            'RPTCommon.DisplayErrorMessage(Me, "Error on Step 2. " + res)
            Return False
        End If

        res = ValidateStep3()
        If res <> "" Then
            RPTCommon.AlertAjax(Me, res, Me.Master.FindControl("RadAjaxManager1"))
            'RPTCommon.DisplayErrorMessage(Me, "Error on Step 3. " + res)
            Return False
        End If

        res = ValidateStep4()
        If res <> "" Then
            RPTCommon.AlertAjax(Me, res, CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager))
            'RPTCommon.DisplayErrorMessage(Me, "Error on Step 4. " + res)
            Return False
        End If

        Return True ' form is valid
    End Function
    Protected Function ValidateStep1() As String
        Dim txt As TextBox
        Dim ddl As DropDownList
        'Dim cbl As CheckBoxList
        Dim dl As DataList
        Dim errorStr As String = String.Empty

        txt = CType(Step1.ContentTemplateContainer.FindControl("txtReportName"), TextBox)
        If txt.Text = "" Then
            errorStr = String.Format("{0}{1}", "- Report Name is required", Environment.NewLine)
        End If

        ' Set the Report Type
        ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlReportType"), DropDownList)
        If ddl.SelectedValue = "" Then
            errorStr += String.Format("{0}{1}", "- Report Type is required", Environment.NewLine)
        End If

        ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlCampGroup"), DropDownList)
        If ddl.SelectedValue = "" Then
            errorStr += String.Format("{0}{1}", "- Campus Group is required", Environment.NewLine)
        End If

        ddl = CType(Step1.ContentTemplateContainer.FindControl("ddlActive"), DropDownList)
        If ddl.SelectedValue = "" Then
            errorStr += String.Format("{0}{1}", "- Status is required", Environment.NewLine)
        End If

        ' JAGG Test that at least one module was selected
        dl = CType(Step1.ContentTemplateContainer.FindControl("dlModules"), DataList)
        Dim isChecked As Boolean = False
        For i As Integer = 0 To dl.Items.Count - 1
            Dim cb As CheckBox = CType(dl.Items(i).FindControl("cbModule"), CheckBox)
            isChecked = isChecked Or cb.Checked
        Next
        If Not isChecked Then
            errorStr += String.Format("{0}{1}", "- Select at least one Module", Environment.NewLine)
        End If

        Return errorStr ' if empty, No errors on this step
    End Function

    Protected Function ValidateStep2() As String
        Dim dg As DataGrid = Step2.ContentTemplateContainer.FindControl("dgFields")
        If dg.Items.Count = 0 Then
            Return "- You must include at least one field in the report"
        End If
        Return ""
    End Function

    Protected Function ValidateStep3() As String
        'Validate if Is group is selected and none function is selected
        Dim ddl As DropDownList
        Dim cb As CheckBox
        Dim dl As DataList

        Dim errorStr As String = String.Empty
        ddl = Step3.ContentTemplateContainer.FindControl("ddlGroupBy")
        If ddl.SelectedValue = COMBOBOX_DEFAULT_SELECT Then
            Return errorStr
        End If

        'A Group by was selected, check if a operation was selected, if not validate false.
        dl = Step3.ContentTemplateContainer.FindControl("dlSumFields")
        Dim isChecked As Boolean = False
        For i As Integer = 0 To dl.Items.Count - 1
            cb = dl.Items(i).FindControl("cbShowCount")
            isChecked = isChecked Or cb.Checked
            cb = dl.Items(i).FindControl("cbShowSum")
            isChecked = isChecked Or cb.Checked
            cb = dl.Items(i).FindControl("cbShowMax")
            isChecked = isChecked Or cb.Checked
            cb = dl.Items(i).FindControl("cbShowMin")
            isChecked = isChecked Or cb.Checked
            cb = dl.Items(i).FindControl("cbShowAvg")
            isChecked = isChecked Or cb.Checked
            cb = dl.Items(i).FindControl("cbMerge")
            isChecked = isChecked Or cb.Checked
        Next
        If Not isChecked Then
            errorStr = String.Format("{0}", "-At least one agregate function (Count, Sum, Max, Min, Avg) is required when choosing a Group By option")
        End If
        Return errorStr
    End Function

    Protected Function ValidateStep4() As String
        Return ""
    End Function
#End Region

#Region "Step 1 Handlers"
    ''' <summary>
    ''' Called when the Report Type is changed on Step 1.
    ''' When the report type is know, we need to
    '''     1) Initialize the list of Field Categories in Step 2
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = Step1.ContentTemplateContainer.FindControl("ddlReportType")
        Dim ReportType As String = ddl.SelectedValue

        ' Initialize the list of Field Categories in Step 2
        If Not ReportType = "" Then
            ddl = Step2.ContentTemplateContainer.FindControl("ddlCategories")
            RPTCommon.BuildCategoriesDDL(ReportType, ddl)

            ' BEN 1/23/07 (Mantis 10201) Remove all fields when the report type changes
            Dim dtFields As DataTable = ViewState("Fields")
            If dtFields IsNot Nothing Then
                dtFields.Rows.Clear()
                Dim dgFields As DataGrid = Step2.ContentTemplateContainer.FindControl("dgFields")
                dgFields.DataSource = dtFields ' clear out the selected fields on step 2
                dgFields.DataBind()
                ViewState("Fields") = dtFields
                DoStep2Transition() ' update the ui on steps 3 and 4
                BindAvailableFields() ' update the Availalbe Fields listbox on step 2
            End If
        End If
    End Sub
#End Region

#Region "Step 2 Handlers"
    ''' <summary>
    ''' Binds the fields currently defined in the report to the step 2 datagrid
    ''' </summary>
    ''' <param name="dtFields"></param>
    ''' <remarks></remarks>
    Protected Sub BindFields(ByVal dtFields As DataTable)
        Try
            Dim dg As DataGrid = CType(Step2.ContentTemplateContainer.FindControl("dgFields"), DataGrid)
            dg.DataSource = dtFields
            dg.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            RPTCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Copies the existing state of the currently selected fields in the datagrid
    ''' over to a datatable
    ''' </summary>
    ''' <param name="dgFields"></param>
    ''' <param name="dtFields"></param>
    ''' <remarks></remarks>
    Protected Sub SaveFieldsToDt(ByVal dgFields As DataGrid, ByVal dtFields As DataTable)
        Try
            For j As Integer = 0 To dgFields.Items.Count - 1
                Dim fld As DataGridItem = dgFields.Items(j)
                Try
                    dtFields.Rows(j)("Header") = CType(fld.FindControl("txtHeader"), TextBox).Text
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
                Try
                    dtFields.Rows(j)("Width") = Convert.ToInt32(CType(fld.FindControl("txtWidth"), TextBox).Text)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
                Try
                    dtFields.Rows(j)("FormatString") = CType(fld.FindControl("ddlFormat"), DropDownList).SelectedValue
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
                Try
                    dtFields.Rows(j)("Visible") = CType(fld.FindControl("cbVisible"), CheckBox).Checked
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    ''' <summary>
    ''' Called when the user clicks UP,DN or DEL from step 2 on the datagrid
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub dgFields_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs)
        Try
            Dim dtFields As DataTable = ViewState("Fields")
            Dim i As Integer = e.Item.ItemIndex
            'BEN: 10/19/06 - Save the current view to the datatable so that hittin up or down keeps the changes
            Dim dgFields As DataGrid = Step2.ContentTemplateContainer.FindControl("dgFields")
            SaveFieldsToDt(dgFields, dtFields)

            Select Case e.CommandName
                Case "Up" ' User wants to move a field up
                    If i = 0 Then Return ' cannot move up when we are on the first field

                    Dim selRow As DataRow = dtFields.Rows(i)
                    ' Create a copy of the selected row
                    Dim CopyRow As DataRow = dtFields.NewRow()
                    For j As Integer = 0 To selRow.ItemArray.Length - 1
                        CopyRow(j) = selRow(j)
                    Next
                    dtFields.Rows.RemoveAt(i)
                    dtFields.Rows.InsertAt(CopyRow, i - 1)
                    ViewState("Fields") = dtFields
                    BindFields(dtFields)

                Case "Dn" ' User wants to move a field down
                    If i = dtFields.Rows.Count - 1 Then Return ' cannot move up when we are on the first field

                    Dim selRow As DataRow = dtFields.Rows(i)
                    ' Create a copy of the selected row
                    Dim CopyRow As DataRow = dtFields.NewRow()
                    For j As Integer = 0 To selRow.ItemArray.Length - 1
                        CopyRow(j) = selRow(j)
                    Next
                    dtFields.Rows.RemoveAt(i)
                    If i = dtFields.Rows.Count - 2 Then
                        dtFields.Rows.Add(CopyRow)
                    Else
                        dtFields.Rows.InsertAt(CopyRow, i + 1)
                    End If
                    ViewState("Fields") = dtFields
                    BindFields(dtFields)

                Case "Del" ' User wants to delete a field
                    dtFields.Rows.RemoveAt(i)
                    ViewState("Fields") = dtFields
                    BindFields(dtFields)
                    BindAvailableFields()
            End Select
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            RPTCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Called for each row being bound to the datasource in dgFields.
    ''' Gives us the opportunity to fill in values
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub dgFields_ItemDataBound(ByVal source As Object, ByVal e As DataGridItemEventArgs)
        If e.Item.DataItem Is Nothing Then Return
        Dim hf As HiddenField = CType(e.Item.FindControl("hfTblFldsId"), HiddenField)
        If Not e.Item.DataItem("TblFldsId") Is Nothing Then
            ' 10/20/06 - BEN: Add support for UDF
            If e.Item.DataItem("TblFldsId").ToString() <> "" Then
                hf.Value = e.Item.DataItem("TblFldsId").ToString()
            Else
                hf.Value = e.Item.DataItem("SDFId").ToString()
            End If
        End If

        ' set the header
        Dim txt As TextBox = CType(e.Item.FindControl("txtHeader"), TextBox)
        If Not e.Item.DataItem("Header") Is Nothing Then
            txt.Text = e.Item.DataItem("Header").ToString()
        End If

        ' display the width
        txt = CType(e.Item.FindControl("txtWidth"), TextBox)
        If Not e.Item.DataItem("Width") Is Nothing Then
            txt.Text = e.Item.DataItem("Width").ToString()
        End If

        ' display visible
        Dim cb As CheckBox = CType(e.Item.FindControl("cbVisible"), CheckBox)
        If Not e.Item.DataItem("Visible") Is Nothing Then
            cb.Checked = CType(e.Item.DataItem("Visible"), Boolean)
        End If

        ' display the formats
        Dim ddl As DropDownList = CType(e.Item.FindControl("ddlFormat"), DropDownList)
        If Not e.Item.DataItem("FldTypeId") Is Nothing Then
            Dim fldType As FieldType = CType(e.Item.DataItem("FldTypeId"), FieldType)
            BuildFormatsDDL(fldType, ddl)

            Try
                Dim fmt As String = CType(e.Item.DataItem("FormatString"), String)
                If fmt <> "" Then
                    ddl.SelectedValue = fmt
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End If
        'Dim s As String = ""
    End Sub

    ''' <summary>
    ''' Build the dropdownlist with a list of format strings.  The format strings
    ''' are dependent on the tyep of field.
    ''' </summary>
    ''' <param name="fldType"></param>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Protected Sub BuildFormatsDDL(ByVal fldType As FieldType, ByVal ddl As DropDownList)
        ddl.Items.Add(New ListItem("---Select---", ""))
        Select Case fldType
            Case FieldType._Bit
                ddl.Items.Add(New ListItem("General", "{0:g}"))

            Case FieldType._Char
                ddl.Items.Add(New ListItem("General", "{0:g}"))

            Case FieldType._DataTime
                ddl.Items.Add(New ListItem("MM/DD/YYYY", "{0:d}"))
                ddl.Items.Add(New ListItem("DD Month YYYY", "{0:D}"))
                ddl.Items.Add(New ListItem("Short Time HH:MM", "{0:t}"))
                ddl.Items.Add(New ListItem("Long Time HH:MM:SS", "{0:T}"))
                ddl.Items.Add(New ListItem("DD Month YYY HH:MM", "{0:f}"))
                ddl.Items.Add(New ListItem("DD Month", "{0:M}"))
                ddl.Items.Add(New ListItem("Month YYYY", "{0:Y}"))
                ddl.SelectedValue = "{0:d}" ' set the default format for a datetime value

            Case FieldType._Decimal
                ddl.Items.Add(New ListItem("General", "{0:g}"))
                ddl.Items.Add(New ListItem("Decimal", "{0:d}"))
                ddl.Items.Add(New ListItem("Exponent", "{0:e}"))
                ddl.Items.Add(New ListItem("Fixed point", "{0:f}"))

            Case FieldType._Float
                ddl.Items.Add(New ListItem("General", "{0:g}"))
                ddl.Items.Add(New ListItem("Decimal", "{0:d}"))
                ddl.Items.Add(New ListItem("Exponent", "{0:e}"))
                ddl.Items.Add(New ListItem("Fixed point", "{0:f}"))

            Case FieldType._Int
                ddl.Items.Add(New ListItem("General", "{0:g}"))

            Case FieldType._Money
                ddl.Items.Add(New ListItem("Currency", "{0:c}"))
                ddl.Items.Add(New ListItem("General", "{0:g}"))
            Case FieldType._Smallint
                ddl.Items.Add(New ListItem("General", "{0:g}"))
            Case FieldType._TinyInt
                ddl.Items.Add(New ListItem("General", "{0:g}"))
            Case FieldType._Uniqueidentifier
                ddl.Items.Add(New ListItem("General", "{0:g}"))
            Case FieldType._Unknown
                ddl.Items.Add(New ListItem("General", "{0:g}"))
            Case FieldType._Varchar
                ddl.Items.Add(New ListItem("General", "{0:g}"))
                'BEN: 10/19/06 - remove these formats as they just cause confusion for right now
                'ddl.Items.Add(New ListItem("Phone number", "{0:g}"))
                'ddl.Items.Add(New ListItem("SSN", "{0:g}"))
        End Select
    End Sub


    ''' <summary>
    ''' Create a datatable with the schema that matches syHocRptFields
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function CreateFieldsDataTable() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("AdHocFieldId", Type.GetType("System.Guid"))
        dt.Columns.Add("ReportId", Type.GetType("System.Int16"))
        dt.Columns.Add("TblId", Type.GetType("System.Int32"))
        dt.Columns.Add("TblFldsId", Type.GetType("System.String"))
        dt.Columns.Add("TableName", Type.GetType("System.String"))
        dt.Columns.Add("FldId", Type.GetType("System.Int32"))
        dt.Columns.Add("FieldName", Type.GetType("System.String"))
        dt.Columns.Add("Header", Type.GetType("System.String"))
        dt.Columns.Add("ColOrder", Type.GetType("System.Int16"))
        dt.Columns.Add("Visible", Type.GetType("System.Boolean"))
        dt.Columns.Add("FormatString", Type.GetType("System.String"))
        dt.Columns.Add("Width", Type.GetType("System.Int32"))
        dt.Columns.Add("ShowInGroupBy", Type.GetType("System.Boolean"))
        dt.Columns.Add("ShowInSummary", Type.GetType("System.Boolean"))
        dt.Columns.Add("SummaryType", Type.GetType("System.Int32"))
        dt.Columns.Add("GroupBy", Type.GetType("System.Boolean"))
        dt.Columns.Add("SortBy", Type.GetType("System.Boolean"))
        dt.Columns.Add("IsParam", Type.GetType("System.Boolean"))
        dt.Columns.Add("IsRequired", Type.GetType("System.Boolean"))
        dt.Columns.Add("FldTypeId", Type.GetType("System.Int32"))
        dt.Columns.Add("FldType", Type.GetType("System.String"))
        dt.Columns.Add("FKColDescrip", Type.GetType("System.String"))
        dt.Columns.Add("SDFId", Type.GetType("System.Guid")) ' UDF support     
        Return dt
    End Function

    ''' <summary>
    ''' User wants to add a field to the list of fields displayed in the report
    ''' 10/20/06 - BEN: Added support for UDF
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddField_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Dim lbFields As ListBox = Step2.ContentTemplateContainer.FindControl("lbFields")
        Dim lbAvailableFields As ListBox = CType(Step2.ContentTemplateContainer.FindControl("lbAvailableFields"), ListBox)
        Dim dtFields As DataTable = CType(ViewState("Fields"), DataTable)
        If dtFields Is Nothing Then
            dtFields = CreateFieldsDataTable()
        End If
        For Each li As ListItem In lbAvailableFields.Items
            If li.Selected = True Then
                Dim dr As DataRow = dtFields.NewRow()
                ' The value of the list item has the following format
                ' {TblFldsId or SDFid},{Caption},{FldTypeId},{FldLen},{FormatMask},{Source table, either sySDF or syTblFlds}
                Dim vals() As String = li.Value.Split(",")

                ' move all the fields creation to a function 
                dr("Header") = vals(1)
                If vals(5) = "syTblFlds" Then
                    'dr("TblFldsId") = System.Convert.ToInt32(vals(0))
                    dr("TblFldsId") = vals(0)
                ElseIf vals(5) = "sySDF" Then
                    dr("SDFId") = vals(0)
                End If
                dr("Visible") = True
                dr("FormatString") = vals(4)
                dr("Width") = Convert.ToInt32(vals(3))
                dr("ShowInGroupBy") = False
                dr("ShowInSummary") = False
                dr("SummaryType") = 0
                dr("GroupBy") = False
                dr("SortBy") = False
                dr("IsParam") = False
                dr("IsRequired") = False
                dr("FldTypeId") = Convert.ToInt32(vals(2))
                If dr("SDFId").ToString <> "" Then dr("TblFldsId") = dr("SDFId").ToString
                dtFields.Rows.Add(dr)
            End If
        Next
        ViewState("Fields") = dtFields
        BindFields(dtFields)
        BindAvailableFields()
    End Sub

    ''' <summary>
    ''' Binds the list of available fields while
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindAvailableFields()
        Dim ddl As DropDownList = Step2.ContentTemplateContainer.FindControl("ddlCategories")
        Dim dg As DataGrid = Step2.ContentTemplateContainer.FindControl("dgFields")
        Dim lbAvailableFields As ListBox = Step2.ContentTemplateContainer.FindControl("lbAvailableFields")

        ' clear out the values if the user selects nothing as the category
        If ddl.SelectedValue = "" Then
            lbAvailableFields.Items.Clear()
            Return
        End If
        ' Bind the list of fields for the selected category
        RPTCommon.BuildFieldsLBEx(ddl.SelectedValue, lbAvailableFields)

        ' remove each field in the report from the list of available fields
        For Each dgi As DataGridItem In dg.Items
            Dim tblFldsId As String = CType(dgi.FindControl("hfTblFldsId"), HiddenField).Value

            ' search for the list item with this tblFldsId.
            ' This is an exhaustive search because of the way the Values are stored in the ListBox
            For i As Integer = lbAvailableFields.Items.Count - 1 To 0 Step -1
                Dim li As ListItem = lbAvailableFields.Items(i)
                Dim vals() As String = li.Value.Split(",")
                If vals(0).ToLower() = tblFldsId.ToLower() Then
                    lbAvailableFields.Items.RemoveAt(i)
                    Exit For ' we found a match, go on to the next item in the datagrid
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' User has selected a category.  The list of fields that the user can choose
    ''' is updated to show only the fields in the selected category
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub OnCategoryChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindAvailableFields()
    End Sub
#End Region

#Region "Step 3 Handlers"
    Protected Sub OnClickFormatField(ByVal sender As Object, ByVal e As EventArgs)

    End Sub
    ''' <sumary>
    ''' Called for each field in the check box list on step 3.
    ''' This checkbox list gives the user the option to choose which summary 
    ''' fields will be computed such as SUM, AVG, COUNT, etc...
    ''' By default, every field can use COUNT
    ''' </sumary>
    Protected Sub Step3_OnItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs)
        Dim strDBVersion As String = CType(MyAdvAppSettings.AppSettings("DBVersion"), String)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim cbShowCount As CheckBox = e.Item.FindControl("cbShowCount")
                Dim cbShowSum As CheckBox = e.Item.FindControl("cbShowSum")
                Dim cbShowMax As CheckBox = e.Item.FindControl("cbShowMax")
                Dim cbShowMin As CheckBox = e.Item.FindControl("cbShowMin")
                Dim cbShowAvg As CheckBox = e.Item.FindControl("cbShowAvg")
                Dim cbMerge As CheckBox = e.Item.FindControl("cbMerge")

                If e.Item.DataItem("Header").ToString().ToLower() = "fund source code" Then
                    cbMerge.Enabled = True
                Else
                    cbMerge.Enabled = False
                End If
                ' 10/24/06 - BEN - Support for SDF.                 
                Dim SDFId As String = e.Item.DataItem("SDFId").ToString()
                If SDFId <> "" Then
                    'cbShowCount.Enabled = False
                    cbShowCount.Enabled = True
                    cbShowSum.Enabled = False
                    cbShowMax.Enabled = False
                    cbShowMin.Enabled = False
                    cbShowAvg.Enabled = False
                    Return ' We cannot perform summary status on an SDF.
                End If

                Dim fldTypeId As FieldType = e.Item.DataItem("FldTypeId")
                Select Case fldTypeId
                    Case FieldType._Bit
                    Case FieldType._Char
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True

                    Case FieldType._DataTime
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True

                    Case FieldType._Decimal
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True
                        cbShowSum.Enabled = True
                        cbShowAvg.Enabled = True

                    Case FieldType._Float
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True
                        cbShowSum.Enabled = True
                        cbShowAvg.Enabled = True

                    Case FieldType._Int
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True
                        cbShowSum.Enabled = True
                        cbShowAvg.Enabled = True
                    Case FieldType._Money
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True
                        cbShowSum.Enabled = True
                        cbShowAvg.Enabled = True

                    Case FieldType._Smallint
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True
                        cbShowSum.Enabled = True
                        cbShowAvg.Enabled = True

                    Case FieldType._TinyInt
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True
                        cbShowSum.Enabled = True
                        cbShowAvg.Enabled = True

                    Case FieldType._Uniqueidentifier
                        ' cbMerge.Enabled = True
                        If strDBVersion = "2000" Then
                            cbShowCount.Enabled = False
                            cbShowSum.Enabled = False
                            cbShowMax.Enabled = False
                            cbShowMin.Enabled = False
                            cbShowAvg.Enabled = False
                        End If
                    Case FieldType._Unknown
                    Case FieldType._Varchar
                        cbShowMax.Enabled = True
                        cbShowMin.Enabled = True
                End Select
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

#End Region

#Region "Step 4 Handlers"
    ''' <sumary>
    ''' Called for each field in the report.
    ''' Allows us to disable the ability to select SDFs as a parameter.
    ''' </sumary>
    Protected Sub Step4_OnItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs)
        'Dim strDBVersion As String = MyAdvAppSettings.AppSettings("DBVersion")
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                ' 10/24/06 - BEN - Support for SDF.                 
                'Dim tblFldsId As String = e.Item.DataItem("TblFldsId").ToString()
                'If tblFldsId = "" Then
                '    CType(e.Item.FindControl("cbParam"), CheckBox).Enabled = False
                '    CType(e.Item.FindControl("cbRequired"), CheckBox).Enabled = False
                'End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
#End Region

#Region "Step 5 Handlers"
    ''' <summary>
    ''' User is at Step 5 and clicked finish.  
    ''' This is equivalent to the user clicking the Save button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub OnFinishClick(ByVal sender As Object, ByVal e As WizardNavigationEventArgs) Handles ReportWizard.FinishButtonClick
        DoSave()
    End Sub

    Protected Sub DoRunReport()
        Dim cbDelete As CheckBox = CType(Step5.ContentTemplateContainer.FindControl("cbDelReport"), CheckBox)
        Dim deleteAfterRun As Boolean = cbDelete.Checked

        ' To run the report, we must first save the settings to the db
        DoSave()

        ' DoSave will put the ReportId into the ViewState
        Dim reportId As String = CType(ViewState("id"), String)
        If reportId Is Nothing Or reportId = "" Then
            RPTCommon.DisplayErrorMessage(Me, "Failed to run the report because of an invalid ID.")
            Return
        End If

        ' run the report.  If DeleteAfterRun is true, the the report gets deleted.
        Dim url As String
        If Not deleteAfterRun Then
            url = String.Format("~/Reports/RunReport.aspx?resid={0}&isadhoc=1&cmpid={1}&mod={2}&CampGrpId={3}", _
                                        reportId, RPTCommon.GetCampusID(), Request.Params("mod"), ViewState("CampGrpId"))
        Else
            url = String.Format("~/Reports/RunReport.aspx?resid={0}&isadhoc=1&cmpid={1}&mod={2}&CampGrpId={3}&del=1", _
                                        reportId, RPTCommon.GetCampusID(), Request.Params("mod"), ViewState("CampGrpId"))
        End If

        Response.Redirect(url)
    End Sub

    ''' <summary>
    ''' User want to run the report
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lbRunReport_Click(ByVal sender As Object, ByVal e As EventArgs)
        DoRunReport()
    End Sub

    Protected Sub ibRunReport_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        DoRunReport()
    End Sub
#End Region

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
    Public Overrides Sub SetPageTitle()
        'DE8550 need to override because the reports have their own resource id which isn't the same as the one in syResources
        Header.Title = "AdHoc Reports"
        AdvantageSession.PageBreadCrumb = CurrentPageAsFriendlyPath()
    End Sub

    Protected Sub ReportWizard_SideBarButtonClick(sender As Object, e As WizardNavigationEventArgs) Handles ReportWizard.SideBarButtonClick

    End Sub

    Protected Sub ReportWizard_PreviousButtonClick(sender As Object, e As WizardNavigationEventArgs) Handles ReportWizard.PreviousButtonClick

    End Sub

End Class

