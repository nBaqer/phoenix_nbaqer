﻿<%@ Page Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ReportHome.aspx.vb" Inherits="Reports_ReportHome" EnableViewState="false" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:content id="content4" contentplaceholderid="additional_head" runat="server">
    
    </asp:content>
<asp:content id="content1" contentplaceholderid="contentmain2" runat="server">
    <style type="text/css">
        a:link {
            color: #2962ff;
        }
        
        a:visited {
            color: #2962ff;
        }
          
        a:hover {
            color: #ff9800;
            text-decoration:none;
        }
        a:active {
            text-decoration:none;
            color: #ff9800; 
        }
    </style>

    <div id='frame' >
		<div>
			<telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
		<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
            <script src="../Scripts/DataSources/reportModulesDataSource.js"></script>
            <script src="../Scripts/common/reportCommon.js"></script>
            <script src="../Scripts/viewModels/reportHome.js"></script>
           <div id='content' style="background-color: white;" >
					<table border ="0" id="searchDocsForm" cellpadding="0" cellspacing="3"  width="100%" style="margin:0px;position:relative;text-align: center;">                       
					    <tr>
					        <td ><input id="reportModulesDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="text"  style="width:200px" /></td>
					    </tr>
				    </table>
               <div id="loading"></div>
               <div id="noReportMessage"></div>
               <br/>
                <div id="reportData">
                    <table width="100%" id="reportLinks" style="background-color: white;margin:10px;" >
                        <tr><td id="AdmissionsLinks" ></td></tr>
                        <tr><td id="AcademicsLinks" ></td></tr>
                        <tr><td id="IPEDSReportsLinks"></td></tr>
                        <% If hdnShowNACCASReports.Value.ToLower.Equals("yes") Then %>
                            <tr><td id="NACCASReportsLinks"></td></tr>
                        <% end if %>
                        <tr><td id="StudentAccountsLinks"></td></tr>
                        <tr><td id="FinancialAidLinks"></td></tr>
                        <tr><td id="FacultyLinks"></td></tr>
                        <tr><td id="PlacementLinks"></td></tr>
                        <tr><td id="HumanResourcesLinks"></td></tr>
                        <tr><td id="SystemLinks"></td></tr>
                    </table>
                </div> 
                 <div id="errorDetails" style="display: none"></div>
                 <input type="hidden" id="hdnUserName" runat="server" class="hdnUserName" ClientIDMode="Static"/>
                <input type="hidden" id="hdnUserId" runat="server" class="hdnUserName" ClientIDMode="Static"/>
               <input type="hidden" id="hdnShowNACCASReports" runat="server" class="hdnUserName" ClientIDMode="Static"/>
			</div>
		</telerik:RadPane>
		</telerik:RadSplitter>
		</div>    
	</div>
</asp:content>      