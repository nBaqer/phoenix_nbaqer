<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="RunReport.aspx.vb" Inherits="AdvWeb.Reports.RunReport" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <title>Run Report</title>
    <link rel="stylesheet" type="text/css" media="all" href="../tm/jscalendar/calendar-blue2.css" title="blue2" />
    <script src="../js/checkall.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
    <script type="text/javascript" src="../tm/jscalendar/calendar.js"></script>
    <script type="text/javascript" src="../tm/jscalendar/lang/calendar-en.js"></script>
    <script type="text/javascript" src="../tm/jscalendar/calendar-setup.js"></script>

    <!-- Popup calendar javascript setup -->
    <script type="text/javascript">
        function setup_calendar(txtID, ibID) {
            Calendar.setup({
                inputField: txtID,   // id of the input field
                ifFormat: "%m/%d/%Y",
                button: ibID   // trigger for the calendar (button ID)
            });
        }
    </script>
    <!-- End Popup calendar javascript setup -->
    <style type="text/css">
        .ReportsDiv {
            overflow-y: auto;
            width: 92%;
            white-space: nowrap;
            padding: 10px;
            height: expression(document.body.clientHeight - 400 + "px");
            text-align: left;
            vertical-align: top;
        }

        .PrefsDiv {
            overflow: auto;
            width: 92%;
            white-space: nowrap;
            padding: 10px;
            height: 325px;
            text-align: left;
            vertical-align: top;
        }

        .EasyLabel {
            font: normal 11px verdana;
            color: #000066;
            background-color: transparent;
            padding: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table style="padding: 0; border: 0; border-collapse: collapse; width: 100%">
                    <tr>
                        <td class="listframetop">
                            <table style="padding: 0; border: 0; border-collapse: collapse; width: 100%">
                                <tr>
                                    <td width="10%" nowrap="nowrap" align="left">
                                        <asp:Label ID="lblshow" runat="server"><b class="tothemeshow">Reports</b></asp:Label>
                                    </td>
                                    <td width="85%" nowrap="nowrap">
                                        <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
                                            RepeatDirection="Horizontal" Visible="False">
                                            <asp:ListItem Text="All" Selected="True" />
                                            <asp:ListItem Text="Predef" />
                                            <asp:ListItem Text="Adhoc" Selected="True" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlReports" DataKeyField="ReportId" RepeatDirection="Vertical" runat="server">
                                    <SelectedItemStyle CssClass="selecteditemstyle" />
                                    <ItemStyle CssClass="itemstyle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgPreDef" ImageUrl="~/images/Reports/predef.jpg" runat="server" Visible='<%# (Not Ctype(Container.DataItem("IsAdHoc"), Boolean)).ToString %>'
                                            CommandArgument='<%# Container.DataItem("ReportId")%>' CausesValidation="False" />
                                        <asp:ImageButton ID="imgAdHoc" ImageUrl="~/images/Reports/adhoc.jpg" runat="server" Visible='<%# Ctype(Container.DataItem("IsAdHoc"), Boolean).ToString %>'
                                            CommandArgument='<%# Container.DataItem("ReportId")%>' CausesValidation="False" />
                                        <asp:LinkButton Text='<%# Container.DataItem("ReportName")%>' runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("ReportId")%>' ID="Linkbutton1" CausesValidation="False" />
                                        <asp:HiddenField ID="txtCampGrpId" runat="server" Value='<%# Container.DataItem("CampGrpId")%>' />
                                        <asp:HiddenField ID="txtCampGrpDescrip" runat="server" Value='<%# Container.DataItem("CampGrpDescrip")%>' />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                    <tr height="5px">
                        <td></td>
                    </tr>
                    <tr>
                        <td class="listframetop"><font class="Label">Saved Report Selections</font></td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="PrefsDiv">
                                <asp:DataList ID="dlPrefs" DataKeyField="PrefId" RepeatDirection="Vertical" runat="server">
                                    <SelectedItemStyle CssClass="selecteditemstyle" />
                                    <ItemStyle CssClass="itemstyle" />
                                    <ItemTemplate>
                                        <asp:LinkButton Text='<%# Container.DataItem("PrefName")%>' runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("PrefId")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="Forward"></telerik:RadSplitBar>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <table style="padding: 0; border: 0; border-collapse: collapse;" class="saveNewDeleteRow">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button>
                            <asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                    </tr>
                </table>
                <table class="maincontenttableV2" style="padding: 0; border: 0; border-collapse: collapse; width: 97%">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <div class="boxContainer">
                                <h3><%=Header.Title  %></h3>
                                <asp:Panel ID="pnlrhs" runat="server">
                                    <div>
                                        <asp:Table ID="divReportName" runat="server"  Width="100%">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblReportName" runat="server" />
                                                    <asp:Label ID="lblPrefName" runat="server" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                        <div style="text-align: left">
                                            <asp:Label ID="lblWarning" runat="server" CssClass="labelbold" ForeColor="Red" Visible="False" Text="Warning: Report will be Deleted after Running" />
                                        </div>
                                        <table style="text-align: center; border-collapse: collapse; padding: 0; width: 100%; vertical-align: central; margin-top: 15px" class="contenttable">
                                            <tr style="vertical-align: bottom">
                                                <td style="width: 102px; vertical-align: bottom">
                                                    <asp:Button ID="btnViewReport" Text="View Report" runat="server"  CssClass="new"/>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblPref">Preference Name: </asp:Label>
                                                    <asp:TextBox runat="server" ID="txtPref" CssClass="textbox"></asp:TextBox>
                                                </td>
                                                <td style="width: 102px">
                                                    <asp:Button ID="btnEditReport" Text="Edit Report" runat="server" Visible="False" Enabled="False"  CssClass="new"/>
                                                </td>
                                                <td align="left">

                                                    <asp:Button ID="btnFriendlyPrint" Visible="False" Text="Printer Friendly" runat="server"  CssClass="new"/>
                                                </td>
                                            </tr>
                                        </table>

                                        <h1>Choose filters</h1>
                                        <%--<div style="text-align: Left; width:100%; vertical-align: top">
                                    </div>--%>
                                        <div style="text-align: left; width: 100%;">
                                            <asp:Label ID="lblNoFilters" runat="server" CssClass="labelbold" Visible="false" Text="There are no filters for this report" />
                                        </div>
                                        <!-- end right side bottom 
                                    <div style="height: expression(document.body.clientHeight - 325 + 'px'); text-align: left; padding-left: 15px">-->
                                        <table style="text-align: center; border-collapse: collapse; padding: 0; width: 100%;" class="contenttable">
                                            <tr>
                                                <td>
                                                    <asp:Wizard ID="ReportWizard" runat="server" Width="100%" ActiveStepIndex="0"
                                                        DisplaySideBar="False"
                                                        DisplayCancelButton="False"
                                                        StartNextButtonType="Link"
                                                        StartNextButtonText=""
                                                        BorderWidth="0" BackColor="White">
                                                        <StepStyle BackColor="White" ForeColor="Black" />
                                                        <NavigationStyle Height="0px" Width="0px" />
                                                        <NavigationButtonStyle Height="0px" Width="0px" />
                                                        <SideBarButtonStyle CssClass="labelbold" />
                                                        <SideBarStyle VerticalAlign="Middle" Width="150px" />
                                                        <HeaderStyle CssClass="listframetop" BorderColor="#ebebeb" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" />
                                                        <WizardSteps>
                                                            <asp:TemplatedWizardStep ID="Step1" runat="server" StepType="Start" Title="Filters">
                                                                <ContentTemplate>
                                                                    <%--<div style="text-align: Left; vertical-align: top">
                                                                    <h3>Choose filters</h3></div><div style="text-align: center">
                                                                    <asp:Label ID="lblNoFilters" runat="server" CssClass="labelbold" Visible="false" Text="There are no filters for this report" />
                                                                </div>--%>
                                                                    <div style="vertical-align: top;">
                                                                        <!-- Repeater with the list of report parameters -->
                                                                        <asp:Repeater ID="rptFilters" runat="server">
                                                                            <HeaderTemplate />
                                                                            <ItemTemplate>
                                                                                <table style="text-align: left; border-collapse: collapse; padding: 0; width: 100%;">
                                                                                    <tr valign="top">
                                                                                        <td nowrap="nowrap" style="border: 1px solid #ebebeb; width: 120px; text-indent: 5px;" valign="top">
                                                                                            <asp:HiddenField ID="hfParamId" runat="server" Value='<%# Container.DataItem("ParamId")%>' />
                                                                                            <asp:HiddenField ID="hfTblFldId" runat="server" Value='<%# Container.DataItem("TblFldsId")%>' />
                                                                                            <asp:HiddenField ID="hfTblName" runat="server" Value='<%# Container.DataItem("TableName")%>' />
                                                                                            <asp:HiddenField ID="hfFldName" runat="server" Value='<%# Container.DataItem("FieldName")%>' />
                                                                                            <asp:HiddenField ID="hfDDLid" runat="server" Value='<%# Container.DataItem("DDLId")%>' />
                                                                                            <asp:HiddenField ID="hfFldType" runat="server" Value='<%# Container.DataItem("FldTypeId")%>' />
                                                                                            <asp:HiddenField ID="hfSDFId" runat="server" Value='<%# Container.DataItem("SDFId")%>' />
                                                                                            <asp:HiddenField ID="hfCalculationSQL" runat="server" Value='<%# Container.DataItem("CalculationSQL")%>' />
                                                                                            <asp:Label ID="lblFldName" runat="server" Text='<%# Container.DataItem("Header")%>' CssClass="easylabel" />
                                                                                            <asp:Label ID="lblReq" runat="server" Text="*" Visible='<%# CType(Container.DataItem("IsRequired"),Boolean) %>' CssClass="easylabel" ForeColor="Red" />
                                                                                        </td>
                                                                                        <td style="border: 1px solid #ebebeb; background-color: #fafafa">
                                                                                            <!-- Displays filter/values that come from the database -->
                                                                                            <asp:Table ID="lblDiv1" runat="server" Height="100px" Visible="false">
                                                                                                <asp:TableRow>
                                                                                                    <asp:TableCell>
                                                                                                        <asp:DataList ID="dlValues" runat="server" RepeatDirection="Vertical" RepeatLayout="Table" RepeatColumns="2" Width="100%" DataKeyField="ValueId">
                                                                                                            <SelectedItemStyle CssClass="selecteditemstyle" />
                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:Label ID="lblId" runat="server" Visible="False" Width="0px" Text='<%# Container.DataItem("ValueId")%>' CssClass="label" Style="padding-top: 0px;" />
                                                                                                                <asp:CheckBox ID="cbValue" runat="server" CssClass="label" Text='<%# Container.DataItem("Descrip")%>' />
                                                                                                            </ItemTemplate>
                                                                                                        </asp:DataList>
                                                                                                    </asp:TableCell>
                                                                                                </asp:TableRow>
                                                                                            </asp:Table>
                                                                                            <%--  <asp:Label ID="lblDiv1" runat="server" Visible="False" Text="<div style='overflow-y:auto; height: 100px;border-collapse:collapse;'>" />--%><asp:Label ID="lblDiv2" runat="server" Visible="False" Text="</div>" />
                                                                                            <!-- End: Displays filter/values that come from the database -->
                                                                                            <!-- Calendar filter -->
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td style="vertical-align: middle; text-align: center">
                                                                                                        <asp:Label ID="lbFilterEqual" runat="server" Visible="False" Text="Exact value" CssClass="easylabel" />
                                                                                                        <asp:TextBox ID="txtFilterEqual" runat="server" Visible="False" CssClass="textbox" Width="80px" />
                                                                                                        <img align="middle" src="../images/TM/PopUpCalendar.gif" id="ibEqualDate" runat="server" visible="False" />
                                                                                                    </td>
                                                                                                    <td style="vertical-align: middle; text-align: center">
                                                                                                        <asp:Label ID="lbFilterMin" runat="server" Visible="False" Text="Min" CssClass="easylabel" />
                                                                                                        <asp:TextBox ID="txtFilterMin" runat="server" Visible="False" CssClass="textbox" Width="80px" />
                                                                                                        <img align="middle" src="../images/TM/PopUpCalendar.gif" id="ibMinDate" runat="server" visible="False" />
                                                                                                    </td>
                                                                                                    <td style="vertical-align: middle; text-align: center">
                                                                                                        <asp:Label ID="lbFilterMax" runat="server" Visible="False" Text="Max" CssClass="easylabel" />
                                                                                                        <asp:TextBox ID="txtFilterMax" runat="server" Visible="False" CssClass="textbox" Width="80px" />
                                                                                                        <img align="middle" src="../images/TM/PopUpCalendar.gif" id="ibMaxDate" runat="server" visible="False" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <!-- End: Calendar filter -->
                                                                                            <!-- Regular user entered via textbox filter -->
                                                                                            <table>
                                                                                                <tr style="vertical-align: middle;">
                                                                                                    <td style="vertical-align: middle; text-align: center">
                                                                                                        <asp:DropDownList ID="ddlRegFilterOp" runat="server" CssClass="dropdownlist" Width="175px" Visible="False" />
                                                                                                        <asp:TextBox ID="txtRegFilter" runat="server" Visible="False" CssClass="textbox" Width="240px" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <!-- End: Regular user entered via textbox filter -->
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr height="10px">
                                                                                        <td></td>
                                                                                    </tr>

                                                                                </table>
                                                                            </ItemTemplate>

                                                                            <FooterTemplate>
                                                                                <%-- </table>--%>
                                                                                <%-- </div>--%>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                        <!-- End: Repeater with the list of report parameters -->
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:TemplatedWizardStep>
                                                            <asp:TemplatedWizardStep ID="Step2" runat="server" Title="Sort Order">
                                                                <ContentTemplate>
                                                                    <div style="text-align: center">
                                                                        <h3>Choose how the report is sorted</h3>
                                                                    </div>
                                                                    <div style="vertical-align: top">
                                                                        <table style="text-align: center; border-collapse: collapse; padding: 0; width: 90%; height: 100%" class="contenttable">
                                                                            <tr valign="top">
                                                                                <td class="labelbold" width="45%" align="center">Available Sort Order</td>
                                                                                <td width="10%">&nbsp;</td>
                                                                                <td class="labelbold" width="45%" align="center">Selected Sort Order</td>
                                                                            </tr>
                                                                            <tr valign="top">
                                                                                <td width="45%" align="center">
                                                                                    <asp:ListBox ID="lbSort" runat="server" Rows="4" Width="200px" CssClass="listboxes" /></td>
                                                                                <td align="center" width="10%">
                                                                                    <div>
                                                                                        <asp:Button ID="btnAdd" Width="90px" Text="Add >" runat="server" CausesValidation="False" OnClick="btnAdd_Click" />
                                                                                    </div>
                                                                                    <div>
                                                                                        <asp:Button ID="btnRemove" Width="90px" Text="< Remove" runat="server" CausesValidation="False" OnClick="btnRemove_Click" />
                                                                                    </div>
                                                                                </td>
                                                                                <td width="45%" align="center">
                                                                                    <asp:ListBox ID="lbSelSort" runat="server" Rows="4" Width="200px" CssClass="listboxes" /></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:TemplatedWizardStep>
                                                            <asp:TemplatedWizardStep ID="Step3" runat="server" Title="Header &amp; Footer">
                                                                <ContentTemplate>
                                                                    <div style="text-align: center">
                                                                        <h3>Additional information to print on the report</h3>
                                                                    </div>
                                                                    <div style="vertical-align: top">
                                                                        <table style="text-align: center; border-collapse: collapse; padding: 5px; width: 100%; height: 100%" class="contenttable">
                                                                            <tr valign="top">
                                                                                <td class="rtcontentreport" align="center">
                                                                                    <table align="center" width="50%">
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <asp:CheckBox ID="cbRptFilters" runat="server" Text="Selected Filter criteria" CssClass="label" Enabled="True" /><br>
                                                                                                <asp:CheckBox ID="cbRptSort" runat="server" Text="Selected Sort Order" CssClass="label" Enabled="True" /><br>
                                                                                                <asp:CheckBox ID="cbRptDescrip" runat="server" Text="Report Description" CssClass="label" Enabled="True" /><br>
                                                                                                <asp:CheckBox ID="cbRptInstructions" runat="server" Text="Report Instructions" CssClass="label" Enabled="True" /><br>
                                                                                                <asp:CheckBox ID="cbRptNotes" runat="server" Text="Report Notes" CssClass="label" Enabled="True" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        H
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:TemplatedWizardStep>
                                                        </WizardSteps>
                                                    </asp:Wizard>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </div>
                            <!--end table content-->
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>

        </telerik:RadSplitter><!-- start validation panel--><asp:Panel ID="panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="customvalidator"
            Display="none"></asp:CustomValidator><asp:Panel ID="pnlrequiredfieldvalidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="true"
            ShowSummary="false"></asp:ValidationSummary>
        <asp:HiddenField ID="txtResourceId" runat="server" />
        <asp:HiddenField ID="hfPrefName" runat="server" />
    </div>
</asp:Content>

