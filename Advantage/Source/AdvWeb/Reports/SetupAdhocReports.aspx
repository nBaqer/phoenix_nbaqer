<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SetupAdhocReports.aspx.vb" Inherits="Reports_SetupAdhocReports" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register Src="~/usercontrols/AdvantageRadAjaxLoadingControl.ascx" TagPrefix="uc1" TagName="AdvantageRadAjaxLoadingControl" %>

<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript">
    </script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

        // check the status of all checkbox in a datalist.
        // true if any of the checkbox is selected otherwise false.
        function ValidateIfAtLeastOneCheckBoxIsChecked(source, arguments) {
            var dl = document.getElementById('dlModules'); //Replace DatList1 with your DataList ID 
            var inputs = dl.getElementsByTagName("input");
            var checked = false;
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    checked = checked | inputs[i].checked;
                }
            }
            arguments.IsValid = checked;
            return checked;
        }
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">

    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>

    <div style="overflow: auto;">
        <uc1:AdvantageRadAjaxLoadingControl runat="server" />
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="AdvantageRadAjaxLoadingControl1">

            <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
                VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
                <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="250" Scrolling="Y">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="listframetop">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="10%" nowrap="nowrap" align="left">
                                            <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                        <td width="85%" nowrap="nowrap">
                                            <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                                <asp:ListItem Text="Active" Selected="true" />
                                                <asp:ListItem Text="Inactive" />
                                                <asp:ListItem Text="All" />
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="listframebottom">
                                <div class="scrollleftfilters">
                                    <asp:DataList ID="dlReports" runat="server" DataKeyField="ReportId" Width="100%">
                                        <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                        <ItemStyle CssClass="itemstyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgInActive" ImageUrl="~/images/Inactive.gif" runat="server"
                                                Visible='<%# (Not Ctype(Container.DataItem("Active"), Boolean)).ToString %>'
                                                CommandArgument='<%# Container.DataItem("ReportId")%>' CausesValidation="False" />
                                            <asp:ImageButton ID="imgActive" ImageUrl="~/images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Active"), Boolean).ToString %>'
                                                CommandArgument='<%# Container.DataItem("ReportId")%>' CausesValidation="False" />
                                            <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("Active")%>' />
                                            <asp:LinkButton Text='<%# Container.DataItem("ReportName")%>' runat="server" CssClass="itemstyle"
                                                CommandArgument='<%# Container.DataItem("ReportId")%>' ID="Linkbutton1" CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </td>
                        </tr>
                    </table>

                </telerik:RadPane>
                <telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="Forward"></telerik:RadSplitBar>
                <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="14px" Scrolling="both" orientation="horizontaltop">
                    <asp:Panel ID="pnlrhs" runat="server">
                        <table cellspacing="0" cellpadding="0" runat="server" class="saveNewDeleteRow">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button>
                                    <asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                    <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button>
                                </td>

                            </tr>
                        </table>
                    <div class="boxContainer full" style="margin-bottom: 10px;">
                        <table style="border: none; width: 98%;">
                            <tr>
                                <td >
                                    <!-- begin content table-->
                                   
                                        <h3><%=Header.Title  %></h3>
                                        <asp:Wizard ID="ReportWizard" runat="server" Width="100%" DisplaySideBar="False"
                                            BorderColor="#ebebeb" BorderStyle="Solid" BorderWidth="0px" ActiveStepIndex="4" FinishCompleteButtonText="Finish" FinishCompleteButtonType="Image">
                                            <NavigationButtonStyle CssClass="label" BackColor="#FFFBFF" BorderColor="#ebebeb"
                                                BorderStyle="Solid" BorderWidth="1px"
                                                ForeColor="#ebebeb" />
                                            <StartNavigationTemplate>
                                                <div style="padding-top: 3px">
                                                    <asp:Button ID="btnStart" runat="server" CssClass="save" Text="Begin" CommandName="MoveNext"
                                                        CausesValidation="true" ValidationGroup="Wizard1" />
                                                </div>
                                            </StartNavigationTemplate>
                                            <FinishNavigationTemplate>
                                                <div style="padding-top: 3px">
                                                    <asp:Button ID="btnPrevious" runat="server" CssClass="new" Text="Previous" CommandName="MovePrevious" />
                                                    <asp:Button ID="btnFinish" runat="server" CssClass="save" Text="Save Report" CommandName="MoveComplete"  />
                                                </div>
                                            </FinishNavigationTemplate>
                                            <StepNavigationTemplate>
                                                <div style="padding-top: 3px">
                                                    <asp:Button ID="btnPrevious" runat="server" CssClass="new" Text="Previous" CommandName="MovePrevious" />
                                                    <asp:Button ID="btnNext" runat="server" CssClass="save" CommandName="MoveNext"
                                                        Text="Next" CausesValidation="true" ValidationGroup="Form" />
                                                </div>
                                            </StepNavigationTemplate>
                                            <StepStyle />
                                            <WizardSteps>
                                                <asp:TemplatedWizardStep ID="Step1" runat="server" StepType="Start" Title="Report Wizard">
                                                    <ContentTemplate>
                                                        <h3>Welcome to the AdHoc Report Wizard. This wizard will guide you through creating
                                                                        and editing a user-defined report.</h3>
                                                        <div>
                                                            <table cellspacing="0" cellpadding="0" width="100%" align="left">
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="LbReportCreatorLabel" CssClass="label" runat="server" Text="Report Creator"></asp:Label>
                                                                    </td>
                                                                    <td class="contentcell4" style="padding-left: 8px; text-align: left">
                                                                        <asp:Label ID="LbReportCreatorText" CssClass="label" runat="server" Text="-"></asp:Label>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lblReportName" runat="server" CssClass="label" ToolTip="Friendly name of the report.">Report Name <span style="color: red">*</span></asp:Label></td>
                                                                    <td class="contentcell4" align="left" style="padding-left: 8px">
                                                                        <asp:TextBox ID="txtReportName" runat="server" CssClass="textbox" MaxLength="100" Width="205px" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorReportName" runat="server" ValidationGroup="Wizard1"
                                                                            ControlToValidate="txtReportName" ErrorMessage="Report Name is a required field." CssClass="errorValidation">
                                                                        </asp:RequiredFieldValidator>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lblReportType" runat="server" CssClass="label">Report Type <span style="color: red">*</span></asp:Label></td>
                                                                    <td class="contentcell4" align="left" style="padding-left: 8px">
                                                                        <asp:DropDownList ID="ddlReportType" runat="server" CssClass="dropdownlist"
                                                                            AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged" Width="230px" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorReportType" runat="server" ValidationGroup="Wizard1"
                                                                            ControlToValidate="ddlReportType" ErrorMessage="Please, select a Report Type." CssClass="errorValidation"
                                                                            InitialValue="">
                                                                        </asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lbCampGroup" runat="server" CssClass="label">Campus Group <span style="color: red">*</span></asp:Label></td>
                                                                    <td class="contentcell4" align="left" style="padding-left: 8px">
                                                                        <asp:DropDownList ID="ddlCampGroup" runat="server" CssClass="dropdownlist" Width="230px" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlCampGroup" runat="server" ValidationGroup="Wizard1"
                                                                            ControlToValidate="ddlCampGroup" ErrorMessage="Please, select a Camp Group." CssClass="errorValidation"
                                                                            InitialValue="">
                                                                        </asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lblActivePrompt" runat="server" CssClass="label">Status <span style="color: red">*</span></asp:Label></td>
                                                                    <td class="contentcell4" align="left" style="padding-left: 8px">
                                                                        <asp:DropDownList ID="ddlActive" runat="server" CssClass="dropdownlist" Width="230px" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lblPrivatePrompt" runat="server" CssClass="label">Make Report Public?</asp:Label></td>
                                                                    <td class="contentcell4" style="text-align: left">
                                                                        <asp:CheckBox ID="cbPublic" runat="server" CssClass="checkbox" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lblShowBlankFld" runat="server" CssClass="label">Show Blank Fields (Beta Feature)</asp:Label></td>
                                                                    <td class="contentcell4" style="text-align: left">
                                                                        <asp:CheckBox ID="cbLeftJoin" runat="server" CssClass="checkbox" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport" valign="top">
                                                                        <asp:Label ID="lblModulesPrompt" runat="server" CssClass="label">Show on these Modules <span style="color: red">*</span></asp:Label></td>
                                                                    <td class="contentcell4" style="white-space: normal; vertical-align: top; text-align: left">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DataList ID="dlModules" DataKeyField="ModuleId" RepeatColumns="2" runat="server" Width="460px"
                                                                                        ClientIDMode="Static">
                                                                                        <SelectedItemStyle CssClass="selecteditemstyle" />
                                                                                        <ItemStyle CssClass="itemstyle" Wrap="true" />
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="hfModuleId" runat="server" Value='<%# Container.DataItem("ModuleId")%>' />
                                                                                            <asp:CheckBox ID="cbModule" runat="server" CssClass="checkbox" Text='<%# Container.DataItem("ModuleName")%>'
                                                                                                Checked='<%# Container.DataItem("Permission")%>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </td>
                                                                                <td style="text-align: left; vertical-align: top">
                                                                                    <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="You must select at least one Module"
                                                                                        ClientValidationFunction="ValidateIfAtLeastOneCheckBoxIsChecked" CssClass="errorValidation"
                                                                                        EnableClientScript="true" ValidationGroup="Wizard1"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ContentTemplate>

                                                </asp:TemplatedWizardStep>
                                                <asp:TemplatedWizardStep ID="Step2" runat="server" Title="Select Fields">
                                                    <ContentTemplate>
                                                        <h3 style="border-top: 0; border-left: 0; border-right: 0; padding: 6px; font-size: 11px">Step 2 of 5 - Select Fields<br />
                                                            Choose the fields that will get displayed on the report.</h3>
                                                        <div class="adhocdiv">
                                                            <table cellspacing="0" cellpadding="0" width="100%" align="center">
                                                                <tr>
                                                                    <td style="vertical-align: top; width: 20%; text-align: left; padding-left: 5px; padding-bottom: 5px; padding-right: 5px">
                                                                        <div style="width: 98%; float: left">
                                                                            <div class="labelbold" style="padding-bottom: 6px">
                                                                                Available Fields
                                                                            </div>
                                                                            <div style="padding-bottom: 5px; min-width: 190px">
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:DropDownList ID="ddlCategories" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                                                                                OnSelectedIndexChanged="OnCategoryChanged" Width="100%" />
                                                                                        </td>
                                                                                        <td align="right" style="width: 60px; padding-right: 0; border-collapse: collapse">
                                                                                            <asp:Button ID="btnAddField" runat="server" Text="Add >" OnClick="btnAddField_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>

                                                                            </div>
                                                                            <div style="min-width: 190px">
                                                                                <asp:ListBox ID="lbAvailableFields" runat="server" CssClass="listboxes" SelectionMode="Multiple"
                                                                                    Height="400px" />

                                                                            </div>

                                                                        </div>

                                                                    </td>
                                                                    <td style="vertical-align: top; height: 460px; width: 80%; padding-left: 5px; padding-bottom: 5px; padding-right: 5px">
                                                                        <div class="labelbold" style="padding-bottom: 6px; width: 100%; height: 21px">
                                                                            Selected Fields
                                                                        </div>
                                                                        <div style="width: 100%; height: 430px; overflow-y: auto; border-color: #ebebeb; border-width: 1px; border-style: solid">
                                                                            <asp:DataGrid ID="dgFields" runat="server" HorizontalAlign="Center" GridLines="Horizontal"
                                                                                AutoGenerateColumns="False" OnItemCommand="dgFields_ItemCommand" OnItemDataBound="dgFields_ItemDataBound"
                                                                                Width="100%" BorderWidth="1px" BorderStyle="Solid" BorderColor="#ebebeb">
                                                                                <AlternatingItemStyle CssClass="datagridalternatingstyle" />
                                                                                <ItemStyle CssClass="datagriditemstyle" />
                                                                                <HeaderStyle CssClass="datagridheader" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderText="" Visible="false" HeaderStyle-Width="0" ItemStyle-Width="0">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="hfTblFldsId" runat="server" Visible="false" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Header">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="30%" />
                                                                                        <ItemStyle CssClass="DataGridItemStyle" />
                                                                                        <ItemTemplate>
                                                                                            <div style="padding-right: 7px">
                                                                                                <asp:TextBox ID="txtHeader" runat="server" CssClass="textbox" Style="width: 100%;" ReadOnly="true" />
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Width">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="10%" />
                                                                                        <ItemStyle CssClass="DataGridItemStyle" />
                                                                                        <ItemTemplate>
                                                                                            <div style="padding-right: 7px">
                                                                                                <asp:TextBox ID="txtWidth" runat="server" CssClass="textbox" />
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Format">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="35%" />
                                                                                        <ItemStyle CssClass="DataGridItemStyle" />
                                                                                        <ItemTemplate>
                                                                                            <div style="padding-right: 7px; padding-top: 2px">
                                                                                                <asp:DropDownList ID="ddlFormat" runat="server" CssClass="dropdownlist" Style="width: 100%" />
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Visible">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="10%" />
                                                                                        <ItemStyle CssClass="DataGridItemStyle" />
                                                                                        <ItemTemplate>
                                                                                            <div class="datagridItemReporter">
                                                                                                <asp:CheckBox ID="cbVisible" runat="server" CssClass="checkbox" Checked="True" />
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="5%" />
                                                                                        <ItemStyle CssClass="DataGridItemStyle" />
                                                                                        <ItemTemplate>
                                                                                            <div class="datagridItemReporter">
                                                                                                <asp:ImageButton ID="btnMoveUp" runat="server" ImageUrl="~/images/Reports/up.gif"
                                                                                                    CommandName="Up" ToolTip="Move Up" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TblFldsId") %>' />
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="5%" />
                                                                                        <ItemStyle CssClass="DataGridItemStyle" />
                                                                                        <ItemTemplate>
                                                                                            <div class="datagridItemReporter">
                                                                                                <asp:ImageButton ID="btnMoveDn" runat="server" ImageUrl="~/images/Reports/dn.gif"
                                                                                                    CommandName="Dn" ToolTip="Move Down" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TblFldsId") %>' />
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="5%" />
                                                                                        <ItemStyle CssClass="DataGridItemStyle" />
                                                                                        <ItemTemplate>
                                                                                            <div class="datagridItemReporter">
                                                                                                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/Reports/delete.gif"
                                                                                                    CommandName="Del" ToolTip="Delete" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TblFldsId") %>' />
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <h3 style="border-bottom: 0; border-left: 0; border-right: 0; padding: 6px; font-size: 11px; margin: 0">Note: On the next step, sorting and grouping options can only be done on fields
                                                                        added in this step.
                                                        </h3>
                                                    </ContentTemplate>
                                                </asp:TemplatedWizardStep>
                                                <asp:TemplatedWizardStep ID="Step3" runat="server" Title="Group by and Summary">
                                                    <ContentTemplate>
                                                        <%--<div style="height: 545px">--%>
                                                        <h3 style="border-top: 0; border-left: 0; border-right: 0; padding: 6px; font-size: 11px; margin: 0">Step 3 of 5 - Grouping and Summary Infomation<br />
                                                            Choose how the report will be sorted and grouped.
                                                        </h3>
                                                        <div class="margin-b-5px"></div>
                                                        <div class="adhocdiv">
                                                            <table cellspacing="0" cellpadding="0" width="100%" align="center">
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lblGroupBy" runat="server" CssClass="label">Group Report By</asp:Label></td>
                                                                    <td class="contentcellAdHocRPad">
                                                                        <asp:DropDownList ID="ddlGroupBy" runat="server" CssClass="dropdownlist" Width="150px" />
                                                                        <div class="margin-b-5px"></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="Label1" runat="server" CssClass="label">Sort Report By</asp:Label></td>
                                                                    <td class="contentcellAdHocRPad">
                                                                        <asp:DropDownList ID="ddlSortby" runat="server" CssClass="dropdownlist" Width="150px" />
                                                                        <div class="margin-b-5px"></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lblShowSummary" runat="server" CssClass="label">Display Summary</asp:Label></td>
                                                                    <td class="contentcellAdHocRNoPad">
                                                                        <asp:CheckBox ID="cbSummary" runat="server" CssClass="checkbox" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="contentcellAdHocReport">
                                                                        <asp:Label ID="lblShowGroupSummary" runat="server" CssClass="label">Display Summary for Each Group</asp:Label></td>
                                                                    <td class="contentcellAdHocRNoPad">
                                                                        <asp:CheckBox ID="cbGroupBySummary" runat="server" CssClass="checkbox" /></td>
                                                                </tr>
                                                            </table>
                                                            <h4 style="font-size: 11px; background-color: #E9EDF2; padding: 5px; border: 1px solid #ebebeb; margin: 20px 0 0 0">Summary Options</h4>
                                                            <div style="height: 380px; overflow-y: auto; border-bottom: 1px solid #ebebeb; border-left: 1px solid #ebebeb; border-right: 1px solid #ebebeb; padding-bottom: 3px">
                                                                <table>
                                                                    <asp:DataList ID="dlSumFields" runat="server" CellSpacing="4" CellPadding="2" Width="100%"
                                                                        OnItemDataBound="Step3_OnItemDataBound">
                                                                        <SelectedItemStyle CssClass="selecteditemstyle" />
                                                                        <ItemStyle CssClass="itemstyle" HorizontalAlign="Left" />

                                                                        <ItemTemplate>

                                                                            <tr style="padding-left: 6px">
                                                                                <td style="width: 200px; overflow-wrap: break-word; text-wrap: normal; padding-left: 6px">
                                                                                    <%# Container.DataItem("Header")%>
                                                                                    <%--<asp:Label ID="lblCaption" runat="server" CssClass="label" Text='<%# Container.DataItem("Header")%>' />--%>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbShowCount" runat="server" Enabled="True" Text="Count" Width="50px"
                                                                                        Checked='<%# (Databinder.Eval(Container.DataItem, "SummaryType") And 1) <> 0 %>' />
                                                                                    <asp:CheckBox ID="cbShowSum" runat="server" Enabled="False" Text="Sum" Width="45px"
                                                                                        Checked='<%# (Databinder.Eval(Container.DataItem, "SummaryType") And 2) <> 0 %>' />
                                                                                    <asp:CheckBox ID="cbShowMax" runat="server" Enabled="False" Text="Max" Width="45px"
                                                                                        Checked='<%# (Databinder.Eval(Container.DataItem, "SummaryType") And 4) <> 0 %>' />
                                                                                    <asp:CheckBox ID="cbShowMin" runat="server" Enabled="False" Text="Min" Width="45px"
                                                                                        Checked='<%# (Databinder.Eval(Container.DataItem, "SummaryType") And 8) <> 0 %>' />
                                                                                    <asp:CheckBox ID="cbShowAvg" runat="server" Enabled="False" Text="Avg" Width="45px"
                                                                                        Checked='<%# (Databinder.Eval(Container.DataItem, "SummaryType") And 16) <> 0 %>' />
                                                                                    <asp:CheckBox ID="cbMerge" runat="server" Enabled="False" Text="Merge" Width="45px"
                                                                                        Checked='<%# (DataBinder.Eval(Container.DataItem, "SummaryType") And 32) <> 0 %>' />

                                                                                </td>
                                                                            </tr>



                                                                        </ItemTemplate>


                                                                    </asp:DataList>
                                                                </table>

                                                            </div>
                                                        </div>

                                                        <%--</div>--%>
                                                    </ContentTemplate>
                                                </asp:TemplatedWizardStep>
                                                <asp:TemplatedWizardStep ID="Step4" runat="server" Title="Select Parameters">
                                                    <ContentTemplate>

                                                        <h3 style="border-top: 0; border-left: 0; border-right: 0; padding: 6px; font-size: 11px; margin: 0">Step 4 of 5 - Select Parameters<br />
                                                            Choose the parameters that will be prompted each time the report is run.
                                                        </h3>
                                                        <div style="height: 545px; overflow-y: auto">
                                                            <div class="adhocdiv">
                                                                <table>
                                                                    <asp:DataList ID="dlParams" runat="server" CellSpacing="4" CellPadding="2" Width="100%"
                                                                        OnItemDataBound="Step4_OnItemDataBound">
                                                                        <SelectedItemStyle CssClass="selecteditemstyle" />
                                                                        <ItemStyle CssClass="itemstyle" />
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td style="width: 190px">
                                                                                    <%# Container.DataItem("Header") %>
                                                                                    <%--<asp:Label ID="lblCaption" runat="server" CssClass="label" Text='<%# Container.DataItem("Header") %>' />--%>
                                                                                </td>
                                                                                <td style="width: 80px">
                                                                                    <asp:CheckBox ID="cbParam" runat="server" Text="Param" Checked='<%# Databinder.Eval(Container.DataItem, "IsParam") %>' />

                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbRequired" runat="server" Text="Required" Checked='<%# Databinder.Eval(Container.DataItem, "IsRequired") %>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:HiddenField ID="hfTblFldsId" runat="server" Value='<%# Databinder.Eval(Container.DataItem, "TblFldsId") %>' />
                                                                                </td>
                                                                            </tr>


                                                                        </ItemTemplate>
                                                                    </asp:DataList>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:TemplatedWizardStep>
                                                <asp:TemplatedWizardStep ID="Step5" runat="server" Title="Finish" StepType="Finish">
                                                    <ContentTemplate>
                                                        <h3 style="border-top: 0; border-left: 0; border-right: 0; padding: 6px; font-size: 11px; margin: 0">Step 5 of 5 - Finished<br />
                                                            Click on Finish or Save at the top right to save the report.</h3>
                                                        <div class="adhocdiv">
                                                            <table cellspacing="4" cellpadding="2" width="80%" align="left">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="cbDelReport" runat="server" Text="Preview / Do Not Save Report"
                                                                            CssClass="checkbox" />

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:ImageButton ID="ibRunReport" runat="server" ToolTip="Run Report Now" ImageUrl="~/images/Reports/runreport.jpg"
                                                                            OnClick="ibRunReport_Click" />
                                                                        <asp:LinkButton ID="lbRunReport" runat="server" Text="Run Report Now"
                                                                            Width="100px" OnClick="lbRunReport_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:TemplatedWizardStep>
                                            </WizardSteps>
                                        </asp:Wizard>
                                  

                                    <!--end table content-->

                                </td>
                            </tr>
                        </table>
                    </div>
                    </asp:Panel>
                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadAjaxPanel>
        <!-- start validation panel-->
        <asp:Panel ID="panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="customvalidator"
            Display="none"></asp:CustomValidator>
        <asp:Panel ID="pnlrequiredfieldvalidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="true"
            ShowSummary="false"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>


    </div>

</asp:Content>


