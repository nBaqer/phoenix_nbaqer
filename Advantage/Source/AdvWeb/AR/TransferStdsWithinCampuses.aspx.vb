﻿Imports FAME.Common
Imports System.Data
Imports Advantage.AFA.Integration
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports FAME.Integration.Adv.Afa.Messages.WebApi.Entities
Imports FAME.Advantage.Api.Library.Models
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.Advantage.Api.Library.Models.AFA.Enrollments

Partial Class AR_TransferStdsWithinCampuses
    Inherits BasePage
    Protected campusId, userId As String
    Private pObj As New UserPagePermissionInfo
    Private MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim advantageUserState As User = AdvantageSession.UserState
        Dim objCommon As New CommonUtilities
        Dim resourceId As Integer
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            'Dim obj common As New CommonUtilities
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"

            BuildToPrgVersionsDDL(campusId)
        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
        End If
    End Sub

    Private Sub PopulateCampusDDL(ByVal ProgramVersion As String, ByVal CurrCampus As String)

        Dim facade As New StuEnrollFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlCampusId
            .DataSource = facade.GetCampuses(ProgramVersion, campusId)
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildToPrgVersionsDDL(ByVal CampusId As String)
        '   From Program Version DDL
        Dim facade As New StuEnrollFacade
        Dim ds As DataSet = facade.GetAllPrgVersionsForCampus(CampusId)

        With ddlFromPrgVerId
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = ds.Tables(0)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

        Session("PrgVersions") = ds.Tables(0)
    End Sub

    Protected Sub ddlCampusId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCampusId.SelectedIndexChanged

    End Sub

    Protected Sub ddlFromPrgVerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFromPrgVerId.SelectedIndexChanged
        If ddlFromPrgVerId.SelectedValue <> Guid.Empty.ToString Then
            PopulateCampusDDL(ddlFromPrgVerId.SelectedValue.ToString, campusId)


            '   show Students grid
            dgrdStuEnrollments.Visible = True

            '   get all students enrolled in selected Program Version
            BindDataGrid()

            '   enable Save button
            InitButtonsForWork()
        End If
    End Sub
    Private Sub BindDataGrid()
        dgrdStuEnrollments.DataSource = (New StuEnrollFacade).GetStudentsInSameProgram(ddlFromPrgVerId.SelectedValue, campusId)
        dgrdStuEnrollments.DataBind()
    End Sub

    Private Sub InitButtonsForWork()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        If ddlFromPrgVerId.SelectedValue = Guid.Empty.ToString Then
            CommonWebUtilities.DisplayWarningInMessageBox(Page, "Please select a Program Version to transfer students to.")
            Exit Sub
        End If

        Dim toPrgVerId As String = ddlFromPrgVerId.SelectedValue
        Dim toCampusId As String
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim isSelected As Boolean
        Dim arrStuEnrollId As New ArrayList
        Dim stuEnrollId As String
        Dim facade As New StuEnrollFacade

        '   Save the datagrid items in a collection.
        iitems = dgrdStuEnrollments.Items

        Try
            If ddlCampusId.SelectedItem.Text = "Select" Then
                CommonWebUtilities.DisplayWarningInMessageBox(Page, "The 'Campus to transfer to' field cannot be empty. Please select a campus to transfer to.")
                Exit Sub
            Else
                toCampusId = ddlCampusId.SelectedValue.ToString
            End If

            '   Loop through the collection to retrieve the StuEnrollId.
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
                    '   set flag to true if option is selected
                    isSelected = True
                    stuEnrollId = CType(iitem.FindControl("lblStuEnrollId"), Label).Text
                    arrStuEnrollId.Add(stuEnrollId)
                End If
            Next

            If isSelected = False Then
                CommonWebUtilities.DisplayWarningInMessageBox(Page, "Please select one or more students to transfer to " & ddlFromPrgVerId.SelectedItem.Text)
                Exit Sub

            Else
                '   proceed to update
                Dim result As String = facade.UpdateCampusAndPrgVersionForStuEnrollments(arrStuEnrollId, toCampusId, toPrgVerId, AdvantageSession.UserState.UserName)
                If result <> "" Then
                    CommonWebUtilities.DisplayWarningInMessageBox(Page, result)
                    Exit Sub
                Else


                    Dim token As TokenResponse

                    token = CType(Session("AdvantageApiToken"), TokenResponse)
                    If (token IsNot Nothing) Then
                        Dim enrollmentRequest As New FAME.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(token.ApiUrl, token.Token)
                        Dim enrollmentDetails = enrollmentRequest.GetAfaEnrollmentDetails(arrStuEnrollId.Cast(Of String).Select(Function(gStr) Guid.Parse(gStr)).ToList())

                        If (enrollmentDetails IsNot Nothing AndAlso enrollmentDetails.Any()) Then
                            'Sync enrollment with AFAFor Each enrollment As AfaEnrollmentDetails In enrollmentDetails
                            Dim enrollmentList As New List(Of AfaEnrollment)
                            For Each enrollment As AfaEnrollmentDetails In enrollmentDetails
                                Dim schedHoursByWeek = (New AttendanceFacade).GetScheduledHoursByWeek(enrollment.StudentEnrollmentId.ToString())
                                Dim afaEnr = New AfaEnrollment With {.StudentEnrollmentId = enrollment.StudentEnrollmentId,
                                        .ProgramVersionId = enrollment.ProgramVersionId, .ProgramName = enrollment.ProgramVersionName,
                                        .StartDate = enrollment.StartDate, .ExpectedGradDate = enrollment.GradDate, .DroppedDate = enrollment.DroppedDate, .EnrollmentStatus = enrollment.EnrollmentStatus,
                                        .AdmissionsCriteria = enrollment.AdmissionsCriteria, .AFAStudentId = enrollment.AfaStudentId, .CmsId = enrollment.CMSId, .LastDateAttendance = enrollment.LastDateAttendance,
                                        .LeaveOfAbsenceDate = enrollment.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollment.ReturnFromLOADate, .StatusEffectiveDate = enrollment.StatusEffectiveDate,
                                        .SSN = enrollment.SSN, .CreatedBy = enrollment.ModUser, .CreatedDate = enrollment.ModDate, .UpdatedBy = enrollment.ModUser, .UpdatedDate = enrollment.ModDate, .AdvStudentNumber = enrollment.AdvStudentNumber, .HoursPerWeek = enrollment.HoursPerWeek, .InSchoolTransferHrs = enrollment.InSchoolTransferHrs, .PriorSchoolHrs = enrollment.PriorSchoolHrs}
                                enrollmentList.Add(afaEnr)
                            Next
                            SyncEnrollmentsWithAFA(enrollmentList)
                        End If
                    End If
                    CommonWebUtilities.DisplayInfoInMessageBox(Page, "Students have been successfully transferred.")
                End If

                '   get list of students after updating DB
                '   bind data-grid
                BindDataGrid()

            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            '   Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    '   Set error condition
    '    Customvalidator1.ErrorMessage = errorMessage
    '    Customvalidator1.IsValid = False

    '    If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If
    'End Sub

    Private Sub SyncEnrollmentsWithAFA(stuEnrollIds As IEnumerable(Of String))
        Try
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            If (wapiSetting IsNot Nothing) Then
                Dim afaEnrollments = New LocationsHelper(connectionString, AdvantageSession.UserState.UserName).SyncEnrollmentsWithAFA(stuEnrollIds)
                If (afaEnrollments.Count > 0) Then
                    Dim enrReq = New FAME.AFA.Api.Library.AcademicRecords.EnrollmentRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    Dim result = enrReq.SendEnrollmentToAFA(afaEnrollments)
                End If
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub


    Private Sub SyncEnrollmentsWithAFA(enrollments As IEnumerable(Of AfaEnrollment))
        Try
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            If (wapiSetting IsNot Nothing) Then

                Dim afaEnrollments = New LocationsHelper(connectionString, AdvantageSession.UserState.UserName).SyncEnrollmentsWithAFA(enrollments)
                If (afaEnrollments.Count > 0) Then

                    Dim enrReq = New FAME.AFA.Api.Library.AcademicRecords.EnrollmentRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    Dim result = enrReq.SendEnrollmentToAFA(afaEnrollments)
                End If

            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub
End Class