<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CoursesInCourseGroup.aspx.vb"
    Inherits="AR_CoursesInCourseGroup" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head runat="server">
    <title>Courses In Course Groups</title>
    <link href="../css/localhost.css" type="text/css" rel="stylesheet">
    <link href="../css/systememail.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <img src="../images/advantage_course_crsgroups.jpg"></td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href=#>X Close</a></td>
            </tr>
        </table>
        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" border="0">
            <tr>
                <td class="twocolumnlabelcell" nowrap>
                    <asp:Label ID="lblStd" runat="server" CssClass="Label">Course Group:</asp:Label></td>
                <td class="twocolumncontentcell" nowrap style="padding-right: 20px">
                    <asp:Label ID="lblStdName" runat="server" CssClass="TextBox"></asp:Label></td>
            </tr>
        </table>
        <!-- begin content table -->
        <!--begin right column-->
        <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%">
            <tbody>
                <tr>
                    <td class="detailsFrame">
                        <div class="scrollwholedocs">
                            <!-- begin content table-->
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdCourses" CellPadding="3" ShowFooter="False" BorderStyle="Solid"
                                            AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false"
                                            Width="100%" runat="server">
                                            <EditItemStyle CssClass="Label"></EditItemStyle>
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                            <FooterStyle CssClass="Label"></FooterStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Code" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRCode" Text='<%# Container.DataItem("Code") %>' CssClass="Label"
                                                            runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Course">
                                                    <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRCourse" CssClass="Label" Text='<%# Container.DataItem("Descrip") %>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Credits">
                                                    <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCredits" CssClass="Label" Text='<%# Container.DataItem("Credits") %>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Hours">
                                                    <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHours" CssClass="Label" Text='<%# Container.DataItem("Hours") %>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid></td>
                                </tr>
                            </table>
                            </div></td></tr></table>
                            <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                            <div id="footer">
                                Copyright  FAME 2005 - 2019. All rights reserved.</div>
    </form>
</body>
</HTML>
