﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="GrdBkWeightings.aspx.vb" Inherits="GrdBkWeightings" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../AuditHist.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

<%--    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstBillingMethods">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstBillingMethods"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstBillingMethods"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstBillingMethods"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="radstatus">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstBillingMethods"  />
                     <telerik:AjaxUpdatedControl ControlID="radstatus"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings> 
    </telerik:RadAjaxManagerProxy>--%>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>
        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="listframetop2">
                    <br />
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="15%" nowrap align="left">
                                <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                            </td>
                            <td width="85%" nowrap>
                                <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Active" Selected="True" />
                                    <asp:ListItem Text="Inactive" />
                                    <asp:ListItem Text="All" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="listframebottom">
                    <div class="scrollleftfilters">
                        <asp:DataList ID="dlstGrdBkWgt" runat="server" DataKeyField="InstrGrdBkWgtId">
                            <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                            <ItemStyle CssClass="itemstyle"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                    CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'>
                                </asp:ImageButton>
                                <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CausesValidation="False"
                                    Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'>
                                </asp:ImageButton>
                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                <asp:LinkButton Text='<%# Container.DataItem("Descrip")%>' runat="server" CssClass="itemstyle"
                                    CommandArgument='<%# Container.DataItem("InstrGrdBkWgtId")%>' ID="Linkbutton1"
                                    CausesValidation="False" />
                            </ItemTemplate>
                        </asp:DataList></div>
                </td>
            </tr>
        </table>


    </telerik:RadPane>


    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
        <asp:Panel ID="pnlRHS" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False">
                        </asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                            CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="scrollright2">
                            <!-- begin content table-->
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblDescrip" runat="server" CssClass="label">Description</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtGrdBkWgtDescrip" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblStatusId" runat="server" CssClass="label">Status</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                <tr>
                                    <td class="contentcell4" style="text-align: center; padding: 10px" colspan="2">
                                        <asp:Label ID="lbllbxTitle" runat="server" CssClass="label">Weight Details</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="93%">
                                        <asp:ListBox ID="lbxInstrGrdBkWgts" runat="server" AutoPostBack="True" CssClass="listboxes"
                                            Height="150px" Width="100%"></asp:ListBox>
                                    </td>
                                    <td width="7%">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tr>
                                                <td style="padding-right: 4px; padding-left: 4px; padding-bottom: 4px; padding-top: 4px;
                                                    background-color: #ffffff">
                                                    <asp:LinkButton ID="btnUp" runat="server" Text="<img border=0 src=../images/up.gif alt=MoveUp>"
                                                        CausesValidation="False" CommandName="Up"></asp:LinkButton>
                                                </td>
                                                <tr>
                                                    <td style="padding-right: 4px; padding-left: 4px; padding-bottom: 4px; padding-top: 4px;
                                                        background-color: #ffffff">
                                                        <asp:LinkButton ID="btnDown" runat="server" Text="<img border=0 src=../images/down.gif alt=MoveDown>"
                                                            CausesValidation="False" CommandName="Down"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                <tr>
                                    <td style="text-align: center; padding: 10px" width="100%" colspan="2">
                                       <%-- <asp:Button ID="btnEdit" runat="server"  Enabled="False" Text="   Edit   ">
                                        </asp:Button>
                                        <asp:Button ID="btnRemove" runat="server"  Enabled="False" Text="Remove">
                                        </asp:Button>--%>
                                        <telerik:RadButton ID="btnEdit" runat="server" Enabled="False" Text=" Edit "></telerik:RadButton>&nbsp;&nbsp;&nbsp;
                                        <telerik:RadButton ID="btnRemove" runat="server" Enabled="False" Text="Remove"></telerik:RadButton>
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="pnlContactInfo" runat="server" Width="100%">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                    <tr>
                                        <td width="100%">
                                            <table cellspacing="0" width="100%" align="center" class="datagriditemstyle">
                                                <tr>
                                                    <td class="datagridheaderstyle" style="border-right: 0px; border-top: 0px; border-left: 0px;
                                                        width: 30%">
                                                        <asp:Label ID="lblGrdComponentType" runat="server" CssClass="labelbold">Grading Component Type</asp:Label>
                                                    </td>
                                                    <td class="datagridheaderstyle" style="border-right: 0px; border-top: 0px; width: 15%">
                                                        <asp:Label ID="lblCode" runat="server" CssClass="labelbold">Code</asp:Label>
                                                    </td>
                                                    <td class="datagridheaderstyle" style="border-right: 0px; border-top: 0px; width: 40%">
                                                        <asp:Label ID="lblDescription" runat="server" CssClass="labelbold">Description</asp:Label>
                                                    </td>
                                                    <td class="datagridheaderstyle" style="width: 15%; border-right: 0px; border-top: 0px">
                                                        <asp:Label ID="lblWeight" runat="server" CssClass="labelbold">Weight(%)</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="datagriditemstyle" style="border: 0px">
                                                        <asp:DropDownList ID="ddlGrdComponentTypeId" runat="server" CssClass="dropdownlist">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="datagriditemstyle" style="border-top: 0; border-right: 0; border-bottom: 0">
                                                        <asp:TextBox ID="txtWgtDetailCode" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox>
                                                    </td>
                                                    <td class="datagriditemstyle" style="border-top: 0; border-right: 0; border-bottom: 0">
                                                        <asp:TextBox ID="txtWgtDetailDescrip" runat="server" CssClass="textbox"></asp:TextBox>
                                                    </td>
                                                    <td class="datagriditemstyle" style="border-top: 0; border-right: 0; border-bottom: 0">
                                                        <ew:NumericBox ID="txtWeight" runat="server" CssClass="textbox" PositiveNumber="True"></ew:NumericBox>
                                                        <asp:RangeValidator ID="GrossAmountRangeValidator" runat="server" MinimumValue="0"
                                                            MaximumValue="100" Type="Double" Display="None" ErrorMessage="Invalid Weight. Pls enter a percentage value between 0 and 100"
                                                            ControlToValidate="txtWeight">Invalid Weight</asp:RangeValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                    <tr>
                                        <td style="text-align: center; padding: 10px" width="100%" colspan="2">
                                           <%-- <asp:Button ID="btnAdd" runat="server"  Text="   Add   "></asp:Button>
                                            <asp:Button ID="btnUpdate" runat="server"  Enabled="False" Text="Update">
                                            </asp:Button>--%>
                                            <telerik:RadButton ID="btnAdd" runat="server" Text=" Add  "></telerik:RadButton>&nbsp;&nbsp;&nbsp;
                                            <telerik:RadButton ID="btnUpdate" runat="server" Enabled="False" Text="Update "></telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:TextBox ID="txtInstrGrdBkWgtId" runat="server" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                            <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
            <!-- Close Panel RHS -->
        </asp:Panel>
    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

