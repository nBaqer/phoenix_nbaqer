﻿Imports Fame.common
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class StdSAPChkResults
    Inherits BasePage


    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected StudentId As String
    Protected resourceId As Integer
    Protected boolStatus As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String
    Protected modCode As String
    Protected state As AdvantageSessionState


    Protected LeadId As String
    Protected boolSwitchCampus As Boolean = False


    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim resourceId As Integer

        'Get the StudentId from the state object associated with this page
        'resourceId = HttpContext.Current.Items("ResourceId")


        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = GetStudentFromStateObject(374) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        'Put user code to initialize the page here
        If radStatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If

        'Always disable the History button. It does not apply to this page.
        'header1.EnableHistoryButton(False)

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            Dim ds As New DataSet
            'Dim ds1 As New DataSet
            'Dim ds2 As New DataSet
            Dim facade As New StdProgressFacade
            '   Dim ProgVerId As String

            'To get the firstname and lastname of student to pass into the 
            'enrollmentid component.
            txtStudentId.Text = StudentId

            'Bind the Grd Bk Weights Datalist
            ds = facade.PopulateDataList(txtStudentId.Text, boolStatus, Master.CurrentCampusId)

            dlstStdProgress.SelectedIndex = -1

            With dlstStdProgress
                .DataSource = ds
                .DataBind()
            End With

            'When the page is first loaded we want to select the first enrollment in the DataList
            Dim stuEnrollId As String

            'display an error if there are no enrollments
            If dlstStdProgress.Items.Count = 0 Then
                DisplayErrorMessage("There are no enrollments")
                Exit Sub
            End If

            stuEnrollId = GetStuEnrollIdForFirstItem()

            ProcessSelectedEnrollment(stuEnrollId)
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            'End If

            MyBase.uSearchEntityControlId.Value = ""

        End If
    End Sub
    Private Function GetStuEnrollIdForFirstItem() As String
        Dim j As Integer


        For j = 0 To dlstStdProgress.Items(0).Controls.Count - 1
            If dlstStdProgress.Items(0).Controls(j).GetType.ToString = "System.Web.UI.WebControls.LinkButton" Then
                Return DirectCast(dlstStdProgress.Items(0).Controls(j), LinkButton).CommandArgument.ToString
            End If
        Next
        Return String.Empty
    End Function

    Private Sub dlstStdProgress_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstStdProgress.ItemCommand
        Dim stuEnrollId As String

        stuEnrollId = dlstStdProgress.DataKeys(e.Item.ItemIndex).ToString()
        ProcessSelectedEnrollment(stuEnrollId)

        CommonWebUtilities.RestoreItemValues(dlstStdProgress, e.CommandArgument)


    End Sub

    Private Sub ProcessSelectedEnrollment(ByVal stuEnrollId As String)
        Dim ds As DataSet
        Dim facade As New LeadFacade
        'Dim CommonUtilities As New CommonUtilities
        'Dim UnschedClosureId As New Guid
        'Dim sDate As String
        'Dim eDate As String
        'Dim dt As New DataTable
        'Dim dt2 As New DataTable
        'Dim start As LinkButton
        'Dim end1 As LinkButton
        'Dim storeddt As DataTable
        'Dim iitems As DataGridItemCollection
        'Dim iitem As DataGridItem
        'Dim i As Integer
        'Dim ClsSectId As String
        Dim strGUID As String

        strGUID = stuEnrollId

        'dt = facade.PopulateDataGridOnItemCmd(strGUID)
        ds = facade.GetSAPResultsByStudent(strGUID)

        With dgrdTransactionSearch
            .DataSource = ds
            .DataBind()
        End With


        'CommonWebUtilities.SetStyleToSelectedItem(dlstStdProgress, stuEnrollId, ViewState, header1)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
