﻿Imports FAME.Common
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Xml
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Web
Imports System.Web.UI.WebControls
Imports Telerik.Web.UI
Imports FAME.Advantage.Common

Partial Class PlaceStdOnSuspension
    Inherits BasePage
    Protected campusId, userid As String
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Put user code to initialize the page here
        'Dim ds As New DataSet
        'Dim facade As New ClassSectionFacade
        Dim objCommon As New CommonUtilities
        'Dim dt As New DataTable

        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        '   Dim m_Context As HttpContext

        'disable history button
        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userid = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not Page.IsPostBack Then
            'Dim objcommon As New CommonUtilities
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")

            btnNew.Enabled = True
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"

            PopulateStatusDDL()
        Else
            'objCommon.PageSetup(Form1, "EDIT")
        End If
    End Sub
    Private Sub PopulateStatusDDL()

        Dim facade As New LOAFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlStatusCodeId
            '.DataSource = facade.GetSuspensionStatuses()

            '' Call fill status
            '' Parameters sysStatusList  String  "11" --> suspension
            ''            Optional ShowActiveOnly = True filter only active, Default false. If is Omited or false should not filter
            ''            Optional CampusId  Current Campus Id, Defalut Null, if is ommited, then get all status for the corp campus group.
            Dim guidCampusId As System.Guid
            guidCampusId = Guid.Parse(campusId)
            .DataSource = facade.GetStatusCodeForAGivenList("11", True, campusId)

            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    'Private Sub lbtSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtSearch.Click

    '    '   setup the properties of the new window
    '    Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px"
    '    Dim name As String = "StudentPopupSearch"
    '    Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx?resid=293&mod=AR&cmpid=" + campusId

    '    '   build list of parameters to be passed to the new window
    '    Dim parametersArray(6) As String

    '    '   set StudentName
    '    parametersArray(0) = txtStudentName.ClientID

    '    '   set StuEnrollment
    '    parametersArray(1) = txtStuEnrollmentId.ClientID
    '    parametersArray(2) = txtStuEnrollment.ClientID

    '    '   set Terms
    '    parametersArray(3) = txtTermId.ClientID
    '    parametersArray(4) = txtTerm.ClientID

    '    '   set AcademicYears
    '    parametersArray(5) = txtAcademicYearId.ClientID
    '    parametersArray(6) = txtAcademicYear.ClientID

    '    '   open new window and pass parameters
    '    CommonWebUtilities.OpenWindowAndPassParameters(Page, url, name, winSettings, parametersArray)

    'End Sub

    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, _
                               ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, _
                               ByVal hourtype As String)


        txtStuEnrollmentId.Text = enrollid
        Session("StuEnrollment") = txtStuEnrollmentId.Text
        Session("StudentName") = fullname
    End Sub

    Private Sub btnLOA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSuspension.Click
        Dim facade As New LOAFacade
        Dim message As String
        Dim student As String
        Dim endMsg As String
        Dim errDates As String
        Dim errSDate As String
        ' Dim HasStartDate As Integer



        If ddlStatusCodeId.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("The Status field cannot be empty. Please select an option.")
            Exit Sub
        Else
            ViewState("Status") = ddlStatusCodeId.SelectedItem.Value.ToString
        End If

        If Session("StudentName") = "" Then
            DisplayErrorMessage("Unable to find data. Student field cannot be empty.")
            Exit Sub
        Else
            student = CType(Session("StudentName"), String)
        End If

        If txtReason.Text = "Select" Then
            DisplayErrorMessage("TThe suspended reason field cannot be empty.  Please enter a reason.")
            Exit Sub
        Else
            ViewState("LOAReason") = txtReason.Text
        End If

        'Validate start and end dates on LOA
        If txtStartDate.SelectedDate IsNot Nothing And txtEndDate.SelectedDate IsNot Nothing Then
            errDates = ValidateStartEndDates(CType(txtStartDate.SelectedDate, Date), CType(txtEndDate.SelectedDate, Date))
            If errDates <> "" Then
                DisplayErrorMessage(errDates)
                Exit Sub
            End If
        End If



        If txtStartDate.SelectedDate IsNot Nothing Then
            'First check if the student has a start date 
            'Validate that start date on this student enrollment is greater 
            errSDate = ValidateStudentStartDate(CType(txtStartDate.SelectedDate, Date))
            If errSDate <> "" Then
                DisplayErrorMessage(errSDate)
                Exit Sub
            End If
        End If

        If facade.CheckAttendanceInLDADates(txtStuEnrollmentId.Text, CType(txtStartDate.SelectedDate, Date), DateAdd("d", 1, txtEndDate.SelectedDate)) Then
            DisplayErrorMessage("Attendance has been already posted between the Suspension dates.")
            Exit Sub
        End If

        message = facade.PlaceStudentOnSuspension(campusId, txtStuEnrollmentId.Text, ddlStatusCodeId.SelectedValue.ToString, ViewState("LOAReason"), txtStartDate.SelectedDate, txtEndDate.SelectedDate, AdvantageSession.UserState.UserName)

        If message <> "" Then
            DisplayErrorMessage(message)
        Else
            endMsg = "Student has been successfully placed on suspension." & vbLf
            'endMsg &= "Please note that the revenue recognition process for this student will be terminated"
            DisplayInfoMessage(endMsg)
            'DisplayErrorMessage("Please note that the revenue recognition for this student will be terminated")
            ClearRHS()
        End If
    End Sub
    Public Function ValidateStartEndDates(ByVal startDate As Date, ByVal endDate As Date) As String

        Dim errMsg As String = ""


        If endDate < startDate Then
            errMsg &= "End Date on the Suspension must be greater than the Start Date. "
        End If

        Return errMsg
    End Function
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If TypeOf ctl Is TextBox Then
                    CType(ctl, TextBox).Text = ""
                End If
                If TypeOf ctl Is CheckBox Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If TypeOf ctl Is DropDownList Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
                If TypeOf ctl Is RadDatePicker Then
                    CType(ctl, RadDatePicker).Clear()
                End If
                If TypeOf ctl Is AdvControls.IAdvControl Then
                    Dim AdvCtrl As AdvControls.IAdvControl = CType(ctl, AdvControls.IAdvControl)
                    AdvCtrl.Clear()
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub DisplayInfoMessage(ByVal infoMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, infoMessage)
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '  Dim sStatusId As String

        Try
            ClearRHS()
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            txtStartDate.SelectedDate = Nothing
            txtEndDate.SelectedDate = Nothing
            ViewState("MODE") = "NEW"
            btnSave.Enabled = False
            'Header1.EnableHistoryButton(False)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Public Function ValidateStudentStartDate(ByVal LOAStartDate As Date) As String

        Dim errMsg As String = ""
        Dim facade As New LOAFacade
        Dim StdstartDate As String

        StdstartDate = facade.GetStudentStartDate(txtStuEnrollmentId.Text)

        'If the student has a start date then validate it against LOA startdate.
        If StdstartDate <> "" Then
            If LOAStartDate < StdstartDate Then
                errMsg &= "Cannot place student with a future start date on Suspension."
            End If
        End If

        Return errMsg
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnSuspension)
        controlsToIgnore.Add(txtStartDate)
        controlsToIgnore.Add(txtEndDate)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        'BIndToolTip()
    End Sub

End Class
