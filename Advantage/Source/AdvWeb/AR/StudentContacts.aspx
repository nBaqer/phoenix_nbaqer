﻿<%@ Page Title="Student Contacts" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentContacts.aspx.vb" Inherits="AdvWeb.AR.ArStudentContacts" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <link href="../css/AdvantageValidator.css" rel="stylesheet" />

    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/FileSaver.js"></script>
    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>
    <script type="text/javascript" src="../Scripts/common/kendoControlSetup.js"></script>
    <script type="text/javascript" src="../Scripts/models/Lead.js"></script>
    <script type="text/javascript" src="../Scripts/viewModels/leadContacts.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain2" Runat="Server">
        <script id="infoTemplate" type="text/x-kendo-template">
        <div class="infoMessageTemplate">
            <img width="32px" height="32px" src=#= src #   />
            <h3>#= title #</h3>
            <p>#= message #</p>
        </div>
    </script>
    <script id="errorTemplate" type="text/x-kendo-template">
        <div class="errorPopup">
            <img width="32px" height="32px" src=#= src #   />
            <h3>#= title #</h3>
            <p>#= message #</p>
        </div>
    </script>
    <div class="kNotification"></div>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Width="100%" Orientation="HorizontalTop" Scrolling="Both">
            <div id="leadContacts">
                <br />
                <div class="updateBlocker" style="display: none; text-align: center; vertical-align: middle">
                    <table style="height: 800px; width: 100%">
                        <tr>
                            <td>
                                <img src="../images/loadingFame.gif" /></td>
                        </tr>
                    </table>
                </div>
                <div class="title-page-button-return">
                    <label class="label-bold-title">Contacts</label>
                    <button id="btnGoBack" class="k-button right returnLeadPage" type="button" style="display:none">Return to Lead Page</button>
                </div>
                <div class="clear"></div>
                <div id="leadContactsGrids">
                    <label class="label-bold"><a href="javascript:void(0)" class="lnkCollapse" title="Collapse/Expand" data-container="leadInformation"><span class="collapse"></span></a>Student Information</label>
                    <br />
                    <div id="leadInformation" class="container" style="width: 1183px; min-width: 800px;">
                        <div style="width: 100%">
                            <div class="title-add-icon">
                                <label><b>Phones </b></label>
                                <a href="javascript:void(0)" id="lnkAddPhone">
                                    <span  class="k-icon k-i-plus-circle font-green"></span>
                                </a>
                            </div>
                            <div class="clear"></div>
                            <div id="leadPhonesGrid" data-bind="source: leadPhonesDataSource" style="width: 625px;"></div>
                            <div class="title-add-icon">
                                <label><b>Emails </b></label>
                                <a href="javascript:void(0)" id="lnkAddEmail">
                                    <span class="k-icon k-i-plus-circle font-green"></span>
                                </a>
                            </div>
                            <div class="clear"></div>
                            <div id="leadEmailGrid" data-bind="source: leadEmailDataSource" style="width: 625px;"></div>
                            <div class="title-add-icon">
                                <label><b>Addresses </b></label>
                                <a href="javascript:void(0)" id="lnkAddAddress">
                                    <span  class="k-icon k-i-plus-circle font-green"></span>
                                </a>
                            </div>
                            <div class="clear"></div>
                            <div id="leadAddressGrid" data-bind="source: leadAddressDataSource" style="width: 950px;"></div>
                            <div class="title-add-icon">
                                <label><b>Comments</b></label>
                            </div>
                            <div class="clear"></div>
                            <div id="leadComents" data-bind="source: leadComentsDataSource" style="width: 99%;">
                                <textarea id="leadCommentsText"></textarea>
                                <div id="leadCommentsAction">
                                    <a href="javascript:void(0)" id="lnkSaveComments">
                                        <span class="k-icon k-i-save font-blue-darken-1"></span>
                                    </a>
                                    <a href="javascript:void(0)" id="lnkDeleteComments">
                                        <span class="k-icon k-i-close font-red"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div id="otherContacts">
                        <div class="title-page-show-all-details">
                            <label class="label-bold left">
                                <a id="lnkCollapOtherContacts" href="javascript:void(0)" class="lnkCollapse" title="Collapse/Expand" data-container="otherContactsCollapsable">
                                    <span class="collapse"></span>
                                </a>Other Contacts <a href="javascript:void(0)" id="lnkAddOtherContacts">
                                    <span  class="k-icon k-i-plus-circle font-green"></span>
                                </a>
                            </label>
                            <button id="btnShowAllDetails" class="k-button left showAllDetails" type="button">Show all details of Other Contacts</button>
                        </div>
                        <div class="clear"></div>
                        <div id="otherContactsCollapsable">
                            <div id="otherContactsGrid" class="container">
                                <div id="leadOtherContactsGrid" data-bind="source: otherContactsDataSource" style="width: 750px; margin-top: 15px;"></div>
                            </div>

                            <br />
                            <div id="otherContactsDetails"></div>
                        </div>
                    </div>

                    <div id="otherContactsDetailsTemplate" class="hidden">
                        <div class="otherContactDetailGrid" style="width: 700px;"></div>
                        <div class="title-add-icon">
                            <label><b>Phones </b></label>
                            <a href="javascript:void(0)" class="lnkAddOtherContactPhone">
                                <span  class="k-icon k-i-plus-circle font-green"></span>
                            </a>
                        </div>
                        <div class="clear"></div>
                        <div class="otherContactPhoneGrid" data-bind="source: otherContactPhoneGrid" style="width: 550px;"></div>
                        <div class="title-add-icon">
                            <label><b>Emails </b></label>
                            <a href="javascript:void(0)" class="lnkAddOtherContactEmails">
                                <span  class="k-icon k-i-plus-circle font-green"></span>
                            </a>
                        </div>
                        <div class="clear"></div>
                        <div class="otherContactEmailsGrid" data-bind="source: otherContactEmailsGrid" style="width: 500px;"></div>
                        <div class="title-add-icon">
                            <label><b>Addresses </b></label>
                            <a href="javascript:void(0)" class="lnkAddOtherContactAddresses">
                                <span  class="k-icon k-i-plus-circle font-green"></span>
                            </a>
                        </div>
                        <div class="clear"></div>
                        <div class="otherContactAddressesGrid" data-bind="source: otherContactAddressesGrid" style="width: 800px;"></div>
                        <div class="title-add-icon">
                            <label><b>Comments</b></label>
                        </div>
                        <div class="clear"></div>
                        <div id="otherContactComments" data-bind="source: leadComentsDataSource" style="width: 99%;">
                            <textarea class="otherContactCommentsText"></textarea>
                            <div class="otherContactCommentsAction">
                                <a href="javascript:void(0)" class="lnkSaveOtherContactComments">
                                    <span class="k-icon k-i-save font-blue-darken-1"></span></a>
                                <a href="javascript:void(0)" class="lnkDeleteOtherContactComments">
                                    <span class="k-icon k-i-close font-red"></span></a>
                            </div>
                        </div>
                    </div>
            </div>

            <div id="phoneForm" style="display: none" class="leadContacts">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 30%;">Phone Type <span class="required">*</span></td>
                            <td>
                                <select id="ddlPhoneType" name="ddlPhoneType" class="k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Phone Type" required></select>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Status <span class="required">*</span></td>
                            <td>
                                <select id="ddlPhoneStatus" name="ddlPhoneStatus" class="k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Phone Status" required></select>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Is International?</td>
                            <td>
                                <input type="checkbox" id="chkIsForeign" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Phone <span class="required">*</span></td>
                            <td>
                                <input type="tel" id="txtPhone" class="k-textbox fieldlabel1" data-labelrequired="Phone" style="width: 200px" required /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Extension</td>
                            <td>
                                <input type="text" id="txtPhoneExtension" class="k-textbox fieldlabel1" style="width: 200px" /></td>
                        </tr>
                        <tr class="hidden">
                                <td style="width: 30%;">Show on Lead Page</td>
                                <td>
                                    <input type="checkbox" id="chkPhoneShowOnLeadPage" />
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Best</td>
                            <td>
                                <input type="checkbox" id="chkPhoneBest" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">&#160;</td>
                            <td>
                                <button id="btnPhoneSave" class="k-button" type="button">Save</button>
                                <button id="btnPhoneCancel" class="k-button" type="button">Cancel</button>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="emailForm" style="display: none" data-role="validator" class="leadContacts">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 30%;">Email Type <span class="required">*</span></td>
                            <td>
                                <select id="ddlEmailType" name="ddlEmailType" class="k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Email Type" required></select></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Status <span class="required">*</span></td>
                            <td>
                                <select id="ddlEmailStatus" name="ddlEmailStatus" class="k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Email Status" required></select></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Email <span class="required">*</span></td>
                            <td>
                                <input type="email" id="txtEmail" name="txtEmail" class="k-textbox" placeholder="e.g. myname@example.net" data-labelrequired="Email" style="width: 200px" required data-email-msg="Email format is not valid" style="width: 200px;" />
                            </td>

                        </tr>
                        <tr class="hidden">
                            <td style="width: 30%;">Show on Lead Page</td>
                            <td>
                                <input type="checkbox" id="chkEmailShowOnLeadPage" />
                            </td>
                       </tr>
                        <tr>
                            <td style="width: 30%;">Best</td>
                            <td>
                                <input type="checkbox" id="chkEmailBest" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">&#160;</td>
                            <td>
                                <button id="btnEmailSave" class="k-button" type="button">Save</button>
                                <button id="btnEmailCancel" class="k-button" type="button">Cancel</button>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="addressForm" style="display: none" data-role="validator" class="leadContacts">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 30%;">Address Type <span class="required">*</span></td>
                            <td>
                                <select id="ddlAddressType" name="ddlAddressType" class="k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Address Type" required></select></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Status <span class="required">*</span></td>
                            <td>
                                <select id="ddlAddressStatus" name="ddlAddressStatus" class="k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Address Status" required></select></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Is International?</td>
                            <td>
                                <input type="checkbox" id="chkIsInternational" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Address 1 <span class="required">*</span></td>
                            <td>
                                <input type="text" id="txtAddress1" class="k-textbox fieldlabel1"  style="width: 200px" data-labelrequired="Address 1" required /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Address 2</td>
                            <td>
                                <input type="text" id="txtAddress2" class="k-textbox fieldlabel1" style="width: 200px" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">City <span class="required">*</span></td>
                            <td>
                                <input type="text" id="txtCity" class="k-textbox fieldlabel1" data-labelrequired="City" style="width: 200px" required /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">State <span class="txtState_required required">*</span></td>
                            <td>
                                <select id="ddlState" name="ddlState" class="k-textbox fieldlabel1" style="width: 200px" data-labelrequired="State" required></select>
                                <input type="text" id="txtState" name="txtState" class="k-textbox fieldlabel1 hidden" style="width: 200px" data-labelrequired="State" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Zip Code <span class="required">*</span></td>
                            <td>
                                <input type="text" id="txtZipCode" class="k-textbox fieldlabel1" data-labelrequired="Zip Code" style="width: 200px" data-validation="zip" maxlength="10" required /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Country <span class="required">*</span></td>
                            <td>
                                <select id="ddlCountry" name="ddlCountry" class="k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Country" required></select>
                                <input type="text" id="txtCountry" name="txtCountry" class="k-textbox fieldlabel1 hidden" style="width: 200px" data-labelrequired="Country" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">County </td>
                            <td>
                                <select id="ddlCounty" name="ddlCounty" class="k-textbox fieldlabel1" style="width: 200px"></select>
                                <input type="text" id="txtCounty" name="txtCounty" class="k-textbox fieldlabel1 hidden" style="width: 200px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Is Mailing Address</td>
                            <td>
                                <input type="checkbox" id="chkIsMaillingAddress" /></td>
                        </tr>
                        <tr  class="hidden">
                            <td style="width: 30%;">Show on Lead Page</td>
                            <td>
                                <input type="checkbox" id="chkAddressShowOnLeadPage" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">&#160;</td>
                            <td>
                                <button id="btnAddressSave" class="k-button" type="button">Save</button>
                                <button id="btnAddressCancel" class="k-button" type="button">Cancel</button>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="otherContactsForm" class="hidden leadContacts">
                    <div id="otherContactInfoForm">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 30%;">Contact Type <span class="required">*</span></td>
                                <td>
                                    <select id="ddlContactType" name="ddlContactType" class="ddlContactType ddl k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Contact Type" required></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Status <span class="required">*</span></td>
                                <td>
                                    <select class="ddlContactStatus ddl k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Status" required></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Relationship<span class="required">*</span></td>
                                <td>
                                    <select class="ddlRelationship ddl k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Relationship" required></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Prefix</td>
                                <td>
                                    <select class="ddlPrefix ddl k-textbox fieldlabel1" style="width: 200px"></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Suffix</td>
                                <td>
                                    <select class="ddlSufix ddl k-textbox fieldlabel1" style="width: 200px"></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">First Name <span class="required">*</span></td>
                                <td>
                                    <input type="text" class="txtContactFirstName k-textbox fieldlabel1" data-labelrequired="First Name " style="width: 200px" required /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Last Name <span class="required">*</span></td>
                                <td>
                                    <input type="text" class="txtContactLastName k-textbox fieldlabel1" data-labelrequired="Last Name" style="width: 200px" required /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Middle Name</td>
                                <td>
                                    <input type="text" class="txtContactMiddleName k-textbox fieldlabel1" style="width: 200px" /></td>
                            </tr>
                        </table>
                    </div>
                    <label class="label-bold left otherContactAddPhone"><a href="javascript:void(0)" class="lnkCollapse" title="Collapse/Expand" data-container="otherContactAddPhone"><span class="collapse"></span></a>Phone Number</label>
                    <div id="otherContactAddPhone">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 30%;">Phone Type <span class="required">*</span></td>
                                <td>
                                    <select class="ddlContactPhoneType ddl k-textbox fieldlabel1" style="width: 200px"></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Status <span class="required">*</span></td>
                                <td>
                                    <select class="ddlContactPhoneStatus ddl k-textbox fieldlabel1" style="width: 200px"></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Is International?</td>
                                <td>
                                    <input type="checkbox" class="chkContactIsForeign" /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Phone <span class="required">*</span></td>
                                <td>
                                    <input type="tel" class="txtContactPhone k-textbox fieldlabel1" /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Extension</td>
                                <td>
                                    <input type="text" class="txtContactPhoneExtension k-textbox fieldlabel1" style="width: 200px" /></td>
                            </tr>
                        </table>

                    </div>
                    <div class="clear"></div>
                    <label class="label-bold left otherContactAddEmail"><a href="javascript:void(0)" class="lnkCollapse" title="Collapse/Expand" data-container="otherContactAddEmail"><span class="collapse"></span></a>Email</label>
                    <div id="otherContactAddEmail">
                        <table style="width: 100%;">

                            <tr>
                                <td style="width: 30%;">Email Type <span class="required">*</span></td>
                                <td>
                                    <select class="ddlContactEmailType ddl k-textbox fieldlabel1" style="width: 200px"></select></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Status <span class="required">*</span></td>
                                <td>
                                    <select class="ddlContactEmailStatus ddl k-textbox fieldlabel1" style="width: 200px"></select></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Email <span class="required">*</span></td>
                                <td>
                                    <input type="email" class="txtContactEmail k-textbox" placeholder="e.g. myname@example.net" style="width: 200px;" />
                                </td>

                            </tr>
                        </table>
                    </div>
                    <div class="clear"></div>
                    <label class="label-bold left otherContactAddAddress"><a href="javascript:void(0)" class="lnkCollapse" title="Collapse/Expand" data-container="otherContactAddAddress"><span class="collapse"></span></a>Address</label>
                    <div id="otherContactAddAddress">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 30%;">Address Type <span class="required hidden">*</span></td>
                                <td>
                                    <select class="ddlContactAddressType ddl k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Address Type"></select></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Status <span class="required hidden">*</span></td>
                                <td>
                                    <select class="ddlContactAddressStatus ddl k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Status"></select></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Is International?</td>
                                <td>
                                    <input type="checkbox" class="chkContactIsInternational" /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Address 1  <span class="required hidden">*</span></td>
                                <td>
                                    <input type="text" class="txtContactAddress1 k-textbox fieldlabel1" data-labelrequired="Address1" style="width: 200px" /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Address 2</td>
                                <td>
                                    <input type="text" class="txtContactAddress2 k-textbox fieldlabel1" style="width: 200px" /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">City  <span class="required hidden">*</span></td>
                                <td>
                                    <input type="text" class="txtContactCity k-textbox fieldlabel1" data-labelrequired="City" style="width: 200px" /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">State  <span class="txtContactState_required required hidden">*</span></td>
                                <td>
                                    <select class="ddlContactState ddl k-textbox fieldlabel1" style="width: 200px" data-labelrequired="State"></select>
                                    <input type="text" class="txtContactState k-textbox fieldlabel1 hidden" style="width: 200px" data-labelrequired="State" style="width: 200px" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Zip Code  <span class="required hidden">*</span></td>
                                <td>
                                    <input type="text" class="txtContactZipCode k-textbox fieldlabel1" data-labelrequired="Zip Code" maxlength="10" data-validation="zip" style="width: 200px" /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Country  <span class="required hidden">*</span></td>
                                <td>
                                    <select class="ddlContactCountry ddl k-textbox fieldlabel1" style="width: 200px" data-labelrequired="Country"></select>
                                    <input type="text" class="txtContactCountry k-textbox fieldlabel1 hidden" style="width: 200px" data-labelrequired="Country" style="width: 200px" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">County </td>
                                <td>
                                    <select class="ddlContactCounty ddl k-textbox fieldlabel1" style="width: 200px"></select>
                                    <input type="text" class="txtContactCounty k-textbox fieldlabel1 hidden" style="width: 200px" style="width: 200px" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">Is Mailing Address</td>
                                <td>
                                    <input type="checkbox" class="chkContactIsMaillingAddress" /></td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 30%;">&#160;</td>
                            <td>
                                <button id="btnOtherContactSave" class="k-button" type="button">Save</button>
                                <button id="btnOtherContactCancel" class="k-button" type="button">Cancel</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <span id="kNotification"></span>
                <asp:HiddenField runat="server" ID="hdnUserId" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="hdnLeadId" ClientIDMode="Static" />
                <input type="hidden" id="hdnLeadPhoneId" />
                <input type="hidden" id="hdnLeadAddressId" />
                <input type="hidden" id="hdnLeadEmailId" />
                <input type="hidden" id="hdnOtherContactId" />
                <input type="hidden" id="hdnOtherContactPhoneId" />
                <input type="hidden" id="hdnOtherContactEmailId" />
                <input type="hidden" id="hdnOtherContactAddressId" />

            </div>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <script type="text/x-kendo-template" id="templateIsPrefered">
         #if(IsPreferred){#
            <span> Yes </span>
         #}else{#
            <span> No </span>
         #}#
    </script>
    <script type="text/x-kendo-template" id="templateIsMailing">
         #if(IsMailingAddress){#
            <span> Yes </span>
         #}else{#
            <span> No </span>
         #}#
    </script>
    <asp:HiddenField runat="server" ID="hfPageTitle" Value="Lead Contacts" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

