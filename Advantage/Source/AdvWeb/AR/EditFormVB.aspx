﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditFormVB.aspx.vb" Inherits="AR_EditFormVB" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reschedule Class</title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <style type="text/css">
        html 
        { 
        	overflow:scroll;
        }
    </style>
      <style type="text/css">
        div.RadWindow_Web20 a.rwCloseButton
            {
	            margin-right:20px;
            }
    </style>
 
</head>
<body>
  <form id="form1" runat="server">
  <script type ="text/javascript" >
      function GetRadWindow() {
          var oWindow = null;
          if (window.radWindow)
              oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog       
          else if (window.frameElement.radWindow)
              oWindow = window.frameElement.radWindow; //IE (and Moz as well)       
          return oWindow;
      }
      function Close() {
          GetRadWindow().Close();
      }
      function returnToParent() {
             
          //create the argument that will be returned to the parent page
          var oArg = new Object();

          //get the selected date from RadDatePicker
          var datePicker = $find("<%= txtSDate.ClientID %>");
          var datePickerEnd = $find("<%= txtEDate.ClientID %>");
        
          //alert(datePickerEnd); 

          oArg.selDate = datePicker.get_selectedDate().format("MM/dd/yyyy"); //.toLocaleDateString(); format("dd/mm/yyyy"); 
          oArg.endDate = datePickerEnd.get_selectedDate().format("MM/dd/yyyy"); //.toLocaleDateString();
          oArg.reschedulemeetingid = document.getElementById("<%= txtrescheduledmeetingid.ClientID %>").value;

//          alert(document.getElementById("<%= ddlContainerRooms.ClientID %>").value);
//          alert(document.getElementById("<%= ddlContainerPeriod.ClientID %>").value);
          


          oArg.roomid = document.getElementById("<%= ddlContainerRooms.ClientID %>").value;
          oArg.clssectionid = "<%= ClsSectionId %>";  //document.getElementById("<%= clssectionid1.ClientID %>").value;
          oArg.periodid = document.getElementById("<%= ddlContainerPeriod.ClientID %>").value;
          oArg.instructiontypeid = "<%= Request.QueryString("instructiontypeid") %>";
          oArg.ismeetingrescheduled = true;

//          alert(oArg.selDate);
//          alert(oArg.endDate);
//          alert(oArg.periodid);
//          alert(oArg.clssectionid);
//          alert(oArg.instructiontypeid);

          //alert(document.getElementById("<%= hiddenflag.ClientID %>").value);
          //alert(oArg.reschedulemeetingid);
          var rowindex = document.getElementById("lblMessage");  
          oArg.rowindex = document.getElementById("lblMessage").value;
          var oWnd = GetRadWindow();

           //oWnd.close(oArg);

          //alert("close");
          //Close the RadWindow and send the argument to the parent page
          if (oArg.selDate) {
            // alert(document.getElementById("<%= hiddenflag.ClientID %>").value);
              if (document.getElementById("<%= hiddenflag.ClientID %>").value == 0) { 
                  oWnd.close(oArg); //Close only when there are no schedule conflicts
              }
          }
          else {
              alert("Please fill both fields");
          }
      }
    </script>
      <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div style=" overflow:auto;">
            <asp:Panel ID="pnl1" runat="server" ScrollBars="Vertical" Height="360px" Width="100%">
              <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                <tr>
                    <td class="ContentCell" align="left">
                        <asp:Label ID="lblTerm" runat="server" Text="Term" CssClass="LabelBold"></asp:Label>
                    </td>
                    <td class="ContentCell4" align="left">
                        <asp:Label ID="lblTermDescrip" runat="server" CssClass="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                     <td class="ContentCell" align="left">
                        <asp:Label ID="lblCourse" runat="server" Text="Course" CssClass="LabelBold"></asp:Label>
                    </td>
                    <td class="ContentCell4" align="left">
                        <asp:Label ID="lblCourseDescrip" runat="server" CssClass="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="ContentCell" align="left">
                        <asp:Label ID="lblInstructor" runat="server" Text="Instructor" CssClass="LabelBold"></asp:Label>
                    </td>
                    <td class="ContentCell4" align="left">
                        <asp:Label ID="lblInstructorDescrip" runat="server" CssClass="Label"></asp:Label>
                        <asp:Label ID="lblInstructionTypeId" runat="server" CssClass="Label" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="ContentCell" align="left">
                        <asp:Label ID="Label1" runat="server" Text="Instruction Type" CssClass="LabelBold"></asp:Label>
                    </td>
                    <td class="ContentCell4" align="left">
                        <asp:Label ID="lblinstructiontype" runat="server" CssClass="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                     <td class="ContentCell" align="left">
                        <asp:Label ID="lblCancelFrom" runat="server" Text="Class Cancelled From - To" CssClass="LabelBold"></asp:Label>
                    </td>
                    <td class="ContentCell4" align="left">
                        <asp:Label ID="lblCancelFromDescrip" runat="server" CssClass="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="ContentCell" align="left">
                        <asp:Label ID="lblRescheduleFrom" Text="Reschedule From" runat="server" CssClass="LabelBold"></asp:Label>
                    </td>
                    <td class="ContentCell4" align="left" nowrap>
                         <telerik:RadDatePicker ID="txtSDate" runat="server" Width="100px" 
                                                                            CssClass="LabelGrid" 
                                                                            Calendar-Font-Size="9px" Calendar-Font-Names="verdana"  DateInput-Font-Size="9px">
                                                                                <Calendar ID="Calendar2" runat="server">
                                                                                    <SpecialDays>
                                                                                        <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday" />
                                                                                    </SpecialDays>
                                                                                </Calendar>
                         </telerik:RadDatePicker>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSDate" Text="Reschedule From date is required" CssClass="LabelBold" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                     <td class="ContentCell" align="left">
                        <asp:Label ID="lblRescheduleTo" Text="Reschedule To" runat="server" CssClass="LabelBold"></asp:Label>
                    </td>
                    <td class="ContentCell4" align="left" nowrap>
                        <telerik:RadDatePicker ID="txtEDate" runat="server" Width="100px" 
                                                                          CssClass="LabelGrid" 
                                                                            Calendar-Font-Size="9px"  DateInput-Font-Size="9px" TabIndex="2">
                                                                                <Calendar ID="Calendar1" runat="server">
                                                                                    <SpecialDays>
                                                                                        <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday" />
                                                                                    </SpecialDays>
                                                                                </Calendar>
                         </telerik:RadDatePicker>
                         <asp:RequiredFieldValidator ID="reqEDate" runat="server" ControlToValidate="txtEDate" Text="Reschedule To date is required" CssClass="LabelBold" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="ContentCell" align="left" >
                        <asp:Label ID="lblPeriod" Text="Period" runat="server" CssClass="LabelBold"></asp:Label>
                    </td>
                    <td class="ContentCell4" align="left" nowrap>
                         <asp:DropDownList ID="ddlContainerPeriod" runat="server" CssClass="DropDownList" Width="230px"></asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlContainerPeriod" Text="Period is required" CssClass="LabelBold" ForeColor="Red"></asp:RequiredFieldValidator>
                     </td>
                </tr>
                <tr>
                     <td class="ContentCell" align="left" >
                        <asp:Label ID="lblAltPeriodId" Text="Alternate Period" runat="server" CssClass="LabelBold"></asp:Label>
                    </td>
                     <td class="ContentCell4" align="left">
                       <asp:DropDownList ID="ddlContainerAltPeriod" runat="server" CssClass="DropDownList" Width="230px"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                     <td class="ContentCell" align="left">
                        <asp:Label ID="lblRoomId" Text="Room" runat="server" CssClass="LabelBold"></asp:Label>
                    </td>
                     <td class="ContentCell4" align="left" nowrap>
                       <asp:DropDownList ID="ddlContainerRooms" runat="server" CssClass="DropDownList" Width="230px" AutoPostBack="true"></asp:DropDownList>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlContainerRooms" Text="Room is required" CssClass="LabelBold" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="ContentCell" align="left">
                        <asp:Label ID="lblInstructionTypeId1" Text="Instruction Type" runat="server" CssClass="LabelBold" Visible="false"></asp:Label>
                    </td>
                     <td class="ContentCell4" align="left" nowrap>
                       <asp:DropDownList ID="ddlInstructionType" runat="server" CssClass="DropDownList" Width="230px" Visible="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="ContentCell" align="left">
                    </td>
                     <td class="ContentCell4" align="left">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Reschedule class" Width="120px" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <%--<asp:Label ID="lblMessage" runat="server" Visible="true"></asp:Label>--%>
                        <input type="hidden" id="lblMessage" runat="server"  />

                        <asp:textbox ID="txtrescheduledmeetingid" runat="server" Visible="true" width="0px"/>
                        <asp:textbox ID="txtTermId" runat="server" Visible="true" width="0px"/>
                        <asp:textbox ID="txtreqid" runat="server" Visible="true" width="0px"/>
                        <asp:HiddenField ID="hiddenflag" runat="server" Value="0" />
                        <asp:textbox ID="roomid" runat="server" Visible="true" width="0px"/>
                        <asp:textbox ID="clssectionid1" runat="server" Visible="true" width="0px"/>
                        <asp:textbox ID="periodid" runat="server" Visible="true" width="0px"/>
                        <asp:textbox ID="startdate" runat="server" Visible="true" width="0px"/>
                        <asp:textbox ID="enddate" runat="server" Visible="true" width="0px"/>
                        <asp:textbox ID="instructiontypeid" runat="server" Visible="true" width="0px"/>
                        <asp:textbox ID="ismeetingrescheduled" runat="server" Visible="true" width="0px"/>
                    </td>
                </tr>
          </table>
              <table width="60%">
            <tr>
                    <td><asp:Label ID="lblScheduleConflicts" runat="server" CssClass="LabelBold" ForeColor="Red"></asp:Label></td>
             </tr>
             <tr>
                <td>
                     <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="False"
                                                        AllowSorting="True" AllowPaging="True" PageSize="8" GridLines="None" 
                                                        ShowGroupPanel="false"  Width="670px" AllowMultiRowSelection="true" Visible="true" CellPadding="0" AllowFilteringByColumn="True">
                                                        <PagerStyle Mode="NextPrevAndNumeric"></PagerStyle>
                                                         <GroupingSettings CaseSensitive="false" />
                                                        <MasterTableView AllowMultiColumnSorting="True" 
                                                        GroupLoadMode="Server" Name="ClassSection" SkinID="Web20" Width="670px" CommandItemSettings-AddNewRecordText="Post new score" CommandItemDisplay="Top" 
                                                        HeaderStyle-Font-Size="10px" HeaderStyle-Font-Bold="true" HeaderStyle-VerticalAlign="Top" AllowFilteringByColumn="True"
                                                        FilterItemStyle-Font-Size="9px" TableLayout="Auto" FilterItemStyle-HorizontalAlign="Left">
                                                        
                                                           <%-- <CommandItemSettings AddNewRecordText="Add New Record"  RefreshText="View All" ShowRefreshButton="false" />--%>
                                                           <CommandItemTemplate>
                                                                <div>
                                                                    <div style="float: left; text-align: left; vertical-align:top; width:400px;height:20px;">
                                                                        <asp:Label ID="Label2" runat="server" Font-Size="12px" Font-Bold="true">Classes with Scheduling Conflicts</asp:Label>
                                                                    </div>
                                                                </div>
                                                            </CommandItemTemplate>
                                                          	<Columns>
                                                                <telerik:GridTemplateColumn HeaderText="Course Description" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" AllowFiltering="false" DataField="CourseDescrip" AutoPostBackOnFilter="true">
                                                                    <ItemTemplate>
                                                                         <asp:Label ID="lblCodeDescrip" runat="server" Text='<%# Container.DataItem("CourseDescrip") %>' CssClass="LabelGrid" Width="150px"></asp:Label>
                                                                    </ItemTemplate> 
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Section" ItemStyle-VerticalAlign="Top" AllowFiltering="false"  ItemStyle-HorizontalAlign="Left"  DataField="ClsSection" AutoPostBackOnFilter="true">
                                                                    <ItemTemplate>
                                                                         <asp:Label ID="lblClsSection" runat="server" Text='<%# Container.DataItem("ClsSection") %>' CssClass="LabelGrid" Width="100px"></asp:Label>
                                                                    </ItemTemplate> 
                                                                </telerik:GridTemplateColumn>
                                                               <telerik:GridTemplateColumn HeaderText="Class Meetings Start-End Dates" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"  AllowFiltering="false" DataField="ClassPeriod">
                                                                    <ItemTemplate>
                                                                         <asp:Label ID="lblSchedule" runat="server" Text='<%# Container.DataItem("ClassPeriod") %>' CssClass="LabelGrid" Width="160px"></asp:Label>
                                                                    </ItemTemplate> 
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Meeting Schedules" ItemStyle-VerticalAlign="Top" AllowFiltering="false" 
                                                                DataField="MeetingSchedule" AutoPostBackOnFilter="true" FilterControlWidth="50px" ItemStyle-HorizontalAlign="Left" >
                                                                    <ItemTemplate>
                                                                         <asp:Label ID="lblMeetingSchedules" runat="server" Text='<%# Container.DataItem("MeetingSchedule") %>' CssClass="LabelGrid" Width="125px"></asp:Label>
                                                                    </ItemTemplate> 
                                                                </telerik:GridTemplateColumn>
															 </Columns>
                                                            </MasterTableView> 
                                                            <ClientSettings EnableRowHoverStyle="true" Scrolling-AllowScroll="true">
                                                                <Selecting AllowRowSelect="True" />
                                                             </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
                                                         <telerik:RadWindowManager ID="ScheduleConflictWindow" runat="server" Behaviors="Default" InitialBehaviors="None"  Visible="false">
            <Windows>
                  <telerik:RadWindow ID="DialogWindow" Behaviors="Close" VisibleStatusbar="false"   
                       ReloadOnShow="true"  Modal="true" runat="server" Height="300px" Width="650px" 
                       VisibleOnPageLoad="false"  Top="0" Left="20">
                  </telerik:RadWindow> 
            </Windows> 
    </telerik:RadWindowManager>
       <telerik:RadWindowManager ID="RadWindowManager1" runat="server"  Title="Schedule Conflicts in Same Class" Font-Bold="true" ForeColor="Red">
    </telerik:RadWindowManager>
                </td>
             </tr>
          </table>
            </asp:Panel>
    </div>
    </form>
</body>
</html>
