﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentEntranceTest.aspx.vb" Inherits="StudentEntranceTest" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/NumberValidations.js" type="text/javascript"></script>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <style type="text/css">
        .style1 {
            width: 100%;
        }

        .style2 {
            width: 19px;
            height: 19px;
        }

        .style3 {
            height: 19px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="80%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <br />
                                    <div class="scrolldatagrid2">
                                        <div class="boxContainer">
                                            <h3><%=Header.Title  %></h3>
                                            <div>
                                                <asp:Label ID="label1" runat="server" CssClass="labelbold" Text="Module"></asp:Label>

                                                <asp:DropDownList ID="ddlModule" CssClass="dropdownlist" Width="200px" AutoPostBack="true"
                                                    runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="boxContainer noBorder">
                                            <asp:DataGrid ID="dgrdTransactionSearch" runat="server" BorderColor="#E0E0E0" AllowSorting="True"
                                                AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="False" BorderWidth="1px"
                                                Width="100%" Style="padding-left: 5px;">
                                                <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>

                                                <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                <FooterStyle CssClass="datagridpadding"></FooterStyle>
                                                <EditItemStyle CssClass="datagridpadding"></EditItemStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Test">
                                                        <HeaderStyle CssClass="k-grid-header" Width="20%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEntrTestDescrip" Text='<%# Container.DataItem("Descrip") %>' CssClass="label"
                                                                runat="server">
                                                            </asp:Label>
                                                            <asp:Label ID="lblEntrTestId" Text='<%# Container.DataItem("adReqId") %>' CssClass="label"
                                                                runat="server" Visible="false">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Required">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRequired" Checked='<%# Container.DataItem("Required") %>' Enabled="False"
                                                                CssClass="checkboxstyle" runat="server"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Pass">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkPass" Checked='<%# Container.DataItem("Pass") %>' Enabled="False"
                                                                CssClass="checkboxstyle" runat="server"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Module">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblModuleName" Text='<%# Container.DataItem("ModuleName") %>' CssClass="label"
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Req for Enrollment">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkEnrlRequired" Checked='<%# Container.DataItem("reqforEnrollment") %>'
                                                                Enabled="False" CssClass="checkboxstyle" runat="server"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Req for Financial Aid">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkFinAidRequired" Checked='<%# Container.DataItem("reqforFinancialAid") %>'
                                                                Enabled="False" CssClass="checkboxstyle" runat="server"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Req for Graduation">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkGradRequired" Checked='<%# Container.DataItem("ReqforGraduation") %>'
                                                                Enabled="False" CssClass="checkboxstyle" runat="server"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Completed Date">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtTestTaken" Text='<%# Container.DataItem("TestTaken") %>' CssClass="textbox"
                                                                runat="server" Width="90px">
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Actual Score">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtActualScore" Text='<%# Container.DataItem("ActualScore") %>'
                                                                Enabled="true" CssClass="textbox" runat="server" Width="90px">
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Min Score">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMinScore" Text='<%# Container.DataItem("MinScore") %>' CssClass="label"
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Comments">
                                                        <HeaderStyle CssClass="k-grid-header" Width="30%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtComments" Text='<%# Container.DataItem("Comments") %>' CssClass="textbox"
                                                                runat="server" Width="90px">
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Override">
                                                        <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkOverRide" Checked='<%# Container.DataItem("OverRide") %>' Enabled="true"
                                                                CssClass="checkboxstyle" runat="server"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

