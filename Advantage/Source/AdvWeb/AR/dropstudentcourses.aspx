﻿<%@ Page Title="Drop Student Courses" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="DropStudentCourses.aspx.vb" Inherits="AR_dropstudentcourses" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <script src="../js/CheckAll.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
             <telerik:AjaxSetting AjaxControlID="btnGetStdCourses" >
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgdDropCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>   
            <telerik:AjaxSetting AjaxControlID="chkAllItems" >
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgdDropCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>   
            <telerik:AjaxSetting AjaxControlID="chkDrop" >
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgdDropCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>    
             
        </AjaxSettings>        
        <%--   --%>  
    </telerik:RadAjaxManagerProxy>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table class="maincontenttable" id="Table1">
                <!-- begin right column -->
                <tr>
                    <td class="detailsframetop">
                        <table class="maincontenttable" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button><asp:Button
                                        ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable">
                            <tr>
                                <td class="detailsframe">
                                    <FAME:StudentSearch ID="StudSearch1" runat="server" ShowEnrollment="True" ShowTerm="True" ShowAcaYr="false" OnTransferToParent="TransferToParent" />
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <div align="center">
                                                <table class="contenttable" width="100%%" align="center">
                                                    <tr>
                                                        <td class="contentcell4">
                                                            <asp:Button ID="btnGetStdCourses" runat="server"  Text="Get Student Courses"
                                                                CausesValidation="False"></asp:Button>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table class="contenttable" align="center" width="100%">
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:DataGrid ID="dgdDropCourse" UniqueId="dgdDropCourse" runat="server" AutoGenerateColumns="False" Width="100%" ClientIDMode="Static"
                                                                BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Code    ">
                                                                        <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Code" runat="server" Text='<%# Container.DataItem("Code")  %>' />
                                                                            <asp:TextBox ID="txtClsSectionId" runat="server" CssClass="ardatalistcontent" Visible="False"
                                                                                Text='<%# Container.DataItem("ClsSectionId") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Course Description  ">
                                                                        <HeaderStyle CssClass="datagridheader" Width="53%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Descrip" runat="server" Text='<%# Container.DataItem("Descrip")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Unschedule">
                                                                        <HeaderStyle CssClass="datagridheader" Width="15%" Wrap="False"></HeaderStyle>
                                                                        <HeaderTemplate>
                                                                            <%--  
                                                                        <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkDrop', document.forms[0].chkAllItems.checked)" />   
                                                                            --%>
                                                                            <asp:CheckBox UniqueId="chkAllItems" ID="chkAllItems" runat="server" ClientIDMode="Static"
                                                                                Text="" AutoPostBack="True" OnCheckedChanged="ToggleSelectedState" />
                                                                            <asp:Label ID="label1" runat="server" Text="Drop  " />
                                                                        </HeaderTemplate>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox UniqueId="chkDrop" ID="chkDrop" runat="server" ClientIDMode="Static"
                                                                                Text="" AutoPostBack="True" OnCheckedChanged="ToggleRowSelection" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Grade ">
                                                                        <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlGrade" runat="server" CssClass="dropdownlist" Width="80px">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Date Determined">
                                                                        <ItemTemplate>
                                                                            <telerik:RadDatePicker ID="txtDateDetermined" MinDate="1/1/1945" runat="server">
                                                                            </telerik:RadDatePicker>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="datagridheader" Width="20%" Wrap="False"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtStuEnrollment" runat="server" Width="0px" TabIndex="2" Visible="false">StuEnrollment</asp:TextBox>
                                                            <asp:TextBox ID="txtStuEnrollmentId" runat="server" Width="0px" Visible="false"></asp:TextBox>
                                                            <asp:TextBox ID="txtAcademicYear" runat="server" Width="0px" TabIndex="8" Visible="false">AcademicYear</asp:TextBox>
                                                            <asp:TextBox ID="txtAcademicYearId" runat="server" Width="0px" Visible="false">AcademicYearId</asp:TextBox>
                                                            <asp:TextBox ID="txtTerm" runat="server" Width="0px" TabIndex="9" Visible="false">Term</asp:TextBox>
                                                            <asp:TextBox ID="txtTermId" runat="server" Width="0px" Visible="false">TermId</asp:TextBox></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                        <!--end table content-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end right column -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

