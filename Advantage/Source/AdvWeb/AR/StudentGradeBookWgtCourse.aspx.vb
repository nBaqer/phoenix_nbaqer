Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections

Imports CrystalDecisions.Shared.Json
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ


Partial Class StudentGradeBookWgtCourse
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''Added by Saraswathi Lakshmanan- on 26-sept-2008 to fix issue 14296
    Dim campusId As String
    Dim instructorId As String
    Dim username As String
    Dim isAcademicAdvisor As Boolean
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim resourceId As Integer
        Dim campusId As String
        Dim userId As String
        Dim objCommon As New CommonUtilities
        Dim fac As New UserSecurityFacade

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not Page.IsPostBack Then
            ViewState("StuEnrollId") = ""
            ViewState("Instructor") = Session("UserId")

            PopulateTermDDL()
            PopulateShiftDDL()
            ''CohortStartDate DDL 
            PopulateCohortStartDateDDL()

            ViewState("MODE") = "NEW"
            txtModUser.Text = Session("UserName")
            txtModDate.Text = Date.MinValue.ToString

            InitButtonsForLoad()

        Else

            If ViewState("StuEnrollId") <> "" Then
                BuildGBWResultsGrid()
            End If
        End If

        ''Added by Saraswathi Lakshmanan
        ''If the UseCohortStartDateforfilter is set to Yes in WebConfigAppSettings 
        ''then the Term DropDownList is made visible false and CohortStartdateDropDown 
        ''is made visible true

        If MyAdvAppSettings.AppSettings("UseCohortStartDateForFilters").ToString.ToLower = "yes" Then
            trTerm.Visible = False
            trTermlbl.Visible = False
            trCohortStartDate.Visible = True
            trCohortStartDatelbl.Visible = True
        Else
            trTerm.Visible = True
            trTermlbl.Visible = True
            trCohortStartDate.Visible = False
            trCohortStartDatelbl.Visible = False
        End If
    End Sub
    Private Sub ddlTerm_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged

        ddlcohortStDate.SelectedIndex = 0
        ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
        ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString

        If (ddlTerm.SelectedItem.Value.ToString() <> "") Then
            PopulateClsSectionDDL(ddlTerm.SelectedItem.Value.ToString())
        End If
    End Sub

    'Private Sub PopulateTermDDL()
    '    'With ddlTerm
    '    '    .DataSource = (New GrdPostingsFacade).GetCurrentAndPastTerms(ViewState("Instructor"))
    '    '    .DataTextField = "TermDescrip"
    '    '    .DataValueField = "TermId"
    '    '    .DataBind()
    '    '    .Items.Insert(0, New ListItem("Select", ""))
    '    '    .SelectedIndex = 0
    '    'End With
    '    With ddlTerm
    '        .DataSource = (New GrdPostingsFacade).GetCurrentAndPastTermsForAnyUser(ViewState("Instructor"), Session("UserName"))
    '        .DataTextField = "TermDescrip"
    '        .DataValueField = "TermId"
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub PopulateTermDDL()
        'With ddlTerm
        '    .DataSource = (New GrdPostingsFacade).GetCurrentAndPastTerms(ViewState("Instructor"))
        '    .DataTextField = "TermDescrip"
        '    .DataValueField = "TermId"
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", ""))
        '    .SelectedIndex = 0
        'End With
        ''Added BY Saraswathi Lakshmanan to fix
        '' issue 14296: Instructor Supervisors and Education Directors(AcademicAdvisors)  
        ''need to be able to post and modify attendance. 
        '' To Find if the User is a Academic Advisors

        Dim facade As New GrdPostingsFacade

        Dim facClsSectAtt As New ClsSectAttendanceFacade

        username = Session("UserName")
        instructorId = ViewState("Instructor")
        'isAcademicAdvisor = facClsSectAtt.GetIsRoleAcademicAdvisor(instructorId)
        isAcademicAdvisor = facClsSectAtt.GetIsRoleAcademicAdvisorORInstructorSuperVisor(instructorId)
        ViewState("isAcademicAdvisor") = isAcademicAdvisor
        campusId = Master.CurrentCampusId
        With ddlTerm
            ''MOdified by Saraswathi Lakshmanan
            .DataSource = facade.GetCurrentAndPastTermsForAnyUserbyRoles(instructorId, username, isAcademicAdvisor, campusId)

            'With ddlTerm
            '    .DataSource = (New GrdPostingsFacade).GetCurrentAndPastTermsForAnyUser(ViewState("Instructor"), Session("UserName"), XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub PopulateShiftDDL()
        Dim dsShift As New DataSet
        Dim facClsSectAtt As New ClsSectAttendanceFacade

        dsShift = facClsSectAtt.GetActiveShiftsByCampus(campusId)

        Dim drShift As DataRow = dsShift.Tables(0).NewRow
        drShift.Item("ShiftDescrip") = "Select"
        drShift.Item("ShiftID") = "00000000-0000-0000-0000-000000000000"
        dsShift.Tables(0).Rows.InsertAt(drShift, 0)

        With ddlShift
            .DataSource = dsShift
            .DataTextField = "ShiftDescrip"
            .DataValueField = "ShiftId"
            .DataBind()
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub PopulateClsSectionDDL(ByVal Term As String, Optional ByVal Shift As String = "00000000-0000-0000-0000-000000000000", Optional ByVal MinimumClassStartDate As Date = Nothing)

        ''Added by Saraswathi Lakshmanan to fix Issue 14296
        username = Session("UserName")
        instructorId = ViewState("Instructor")
        isAcademicAdvisor = ViewState("isAcademicAdvisor")
        campusId = Master.CurrentCampusId

        If Shift = Nothing Then
            Shift = "00000000-0000-0000-0000-000000000000"
        Else
            Shift = ddlShift.SelectedValue.ToString
        End If

        Dim facade As New StdGrdBkFacade

        With ddlClsSection
            .DataSource = facade.GetClsSectionsbyUserandRoles(ViewState("Term"), instructorId, campusId, Shift, rdpMinimumClassStartDate.DbSelectedDate, username, isAcademicAdvisor)
            '.DataSource = facade.GetClsSectionsbyUserandRoles(ViewState("Term"), instructorId, campusId, username, isAcademicAdvisor)
            .DataTextField = "CourseSectDescrip"
            .DataValueField = "ClsSectionId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub btnBuildList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        'If ddlTerm.SelectedItem.Text = "Select" Then
        '    DisplayErrorMessage("Unable to find data. Please select an option from both filters.")
        '    Exit Sub
        'End If
        ''Modified by Saraswathi lakshmanan
        ''To add the cohortStart date 
        ''If term or cohortStDate are not selected then, classSection cannot be fetched.

        If ddlTerm.SelectedItem.Text = "Select" And ddlcohortStDate.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("Unable to find data. Please select an option.")
            dtlClsSectStds.Visible = False
            pnlRHS.Visible = False
            Exit Sub
        ElseIf ddlTerm.SelectedItem.Text <> "Select" Then
            ViewState("Term") = ddlTerm.SelectedItem.Value.ToString
        ElseIf ddlcohortStDate.SelectedItem.Text <> "Select" Then
            ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
        End If
        If ddlClsSection.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("Unable to find data. Please select an option from both filters")
            dtlClsSectStds.Visible = False
            pnlRHS.Visible = False
            Exit Sub
        End If

        dtlClsSectStds.Visible = True
        pnlRHS.Visible = True

        ClearRHS()

        BindDataList()
    End Sub
    Private Sub BindDataList()
        With dtlClsSectStds
            .DataSource = (New StdGrdBkFacade).PopulateDataList(ddlClsSection.SelectedItem.Value.ToString)
            .DataBind()
        End With
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next

            'pnlHeader.Visible = False
            '
            lblCurrentScore.Visible = False
            lblCurrentGrade.Visible = False
            txtFinalScore.Visible = False
            txtFinalGrade.Visible = False
            '
            lblStdName.Text = ""
            lblSecName.Text = ""
            txtFinalScore.Text = ""
            txtFinalGrade.Text = ""


        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim result As String = ""
        Dim ds As DataSet = DirectCast(Session("GBWDetailsAndResultsDS"), DataSet)
        Dim dtCompTypes As DataTable = ds.Tables("GrdComponentTypesDT")
        Dim dtGBWResults As DataTable = ds.Tables("StuGrdBkResultsDT")
        Dim ctrl As Control
        Dim ctrlLabel As Control
        Dim ctrlDt As Control
        Dim tbl As Table
        Dim txt As TextBox
        Dim dtCompleted As RadDatePicker
        Dim dr As DataRow
        Dim arrRows() As DataRow
        Dim facade As New StdGrdBkFacade
        Dim StdGrdObj As New StdGrdBkInfo

        Try
            ctrl = pnlGBWResults.FindControl("tbl" & ViewState("StuEnrollId").ToString)
            tbl = DirectCast(ctrl, Table)

            If Not (tbl Is Nothing) Then

                Dim cols As Integer = dtCompTypes.Rows.Count * 2
                Dim maxRows As Integer = ViewState("MaxRows")
                Dim id As String
                Dim lblCompDescrip As Label

                For rowCounter As Integer = 1 To maxRows
                    For cellCounter As Integer = 1 To cols
                        ctrl = tbl.Rows(rowCounter).Cells(cellCounter).Controls(0)
                        ctrlLabel = tbl.Rows(rowCounter).Cells(cellCounter).Controls(1)
                        If TypeOf ctrl Is TextBox Then
                            ctrlDt = tbl.Rows(rowCounter).Cells(cellCounter + 1).Controls(0)
                            txt = DirectCast(ctrl, TextBox)
                            lblCompDescrip = DirectCast(ctrlLabel, Label)
                            id = txt.ID.Substring(txt.ID.IndexOf("_") + 1)
                            Dim DA As New GradePostingDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
                            Dim enrollment = DA.GetEnrollmentStartDate(New Guid(ViewState("StuEnrollId").ToString()))
                            Dim enrollStartDate As Date = Convert.ToDateTime(enrollment(0))
                            dtCompleted = DirectCast(ctrlDt, RadDatePicker)
                            If txt.Text <> "" Then
                                If (String.IsNullOrEmpty(Convert.ToString(dtCompleted.SelectedDate))) Then
                                    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "Date completed is required.")
                                    Exit Sub
                                ElseIf (dtCompleted.SelectedDate > DateTime.Now.Date Or dtCompleted.SelectedDate < enrollStartDate) Then
                                    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "Date Completed is Invalid. The date must be greater then enrollment Date : " + enrollStartDate + ", and not greater than today's date.")
                                    Exit Sub
                                End If
                            End If

                            'Dim DateCompleted = dtCompleted.DateInput.Text.Substring(0,10)
                            'dim month = DateCompleted.Substring(5,2)
                            'dim dt = DateCompleted.Substring(8,2)
                            'dim year = DateCompleted.Substring(0,4)
                            'DateCompleted = dt+"-"+month+"-"+year
                            cellCounter = cellCounter + 1
                            arrRows = dtGBWResults.Select("InstrGrdBkWgtDetailId='" & id & "' AND ResNum=" & rowCounter)
                            If txt.Text <> "" Then
                                Dim dateCompleted As DateTime = dtCompleted.SelectedDate
                                '********** Validation ******************
                                'Try
                                '    Dim i As Integer = Integer.Parse(txt.Text)
                                'Catch ex As System.Exception
                                '	Dim exTracker = new AdvApplicationInsightsInitializer()
                                '	exTracker.TrackExceptionWrapper(ex)

                                '    DisplayErrorMessage("Incorrect data type for Score.")
                                '    Exit Sub
                                'End Try
                                'If Integer.Parse(txt.Text) > 100 Then
                                If txt.Text = "T" Then
                                    txt.Text = "-1.00"
                                ElseIf txt.Text > 100 Then
                                    DisplayErrorMessage("Score cannot exceed 100.")
                                    Exit Sub
                                End If
                                'If txt.Text > 100 Then
                                '    DisplayErrorMessage("Score cannot exceed 100.")
                                '    Exit Sub
                                'End If
                                '****************************************
                                If arrRows.GetLength(0) = 0 Then
                                    'Row does not exist on dtGBWResults table
                                    'ADD NEW
                                    dr = dtGBWResults.NewRow
                                    dr("GrdBkResultId") = System.Guid.NewGuid
                                    dr("ClsSectionId") = ddlClsSection.SelectedItem.Value
                                    dr("InstrGrdBkWgtDetailId") = id
                                    'dr("Score") = Integer.Parse(txt.Text)
                                    dr("Score") = txt.Text
                                    dr("DateCompleted") = dateCompleted
                                    dr("StuEnrollId") = ViewState("StuEnrollId")
                                    dr("ModUser") = Session("UserName")
                                    dr("ModDate") = Date.Now
                                    dr("ResNum") = rowCounter
                                    dr("IsCompGraded") = True
                                    dr("Comments") = lblCompDescrip.Text
                                    dr("PostDate") = Date.Now
                                    dtGBWResults.Rows.Add(dr)

                                    'PopulateObjStdGrdBkInfo("Insert", )

                                    StdGrdObj.GrdBkResultId = dr("GrdBkResultId").ToString
                                    StdGrdObj.ClassId = dr("ClsSectionId").ToString
                                    StdGrdObj.StuEnrollId = dr("StuEnrollId").ToString
                                    StdGrdObj.GrdBkWgtDetailId = dr("InstrGrdBkWgtDetailId").ToString
                                    StdGrdObj.Score = dr("Score")
                                    StdGrdObj.Comments = dr("Comments").ToString
                                    StdGrdObj.ResNum = dr("ResNum")
                                    StdGrdObj.ModUser = dr("ModUser")
                                    StdGrdObj.IsCompGraded = dr("IsCompGraded")
                                    StdGrdObj.DateCompleted = dr("DateCompleted")


                                    result = facade.InsertStudentGrade(StdGrdObj, Session("UserName"))

                                Else
                                    'Row already exists on dtGBWResults table
                                    'UPDATE
                                    dr = arrRows(0)
                                    'If Not (dr("Score") = Integer.Parse(txt.Text)) Then
                                    Try
                                        If Not (dr("Score") = txt.Text) Then
                                            dr("Score") = txt.Text
                                            dr("ModUser") = Session("UserName")
                                            dr("ModDate") = Date.Now
                                            dr("IsCompGraded") = True
                                            dr("Comments") = lblCompDescrip.Text
                                            dr("DateCompleted") = dateCompleted
                                            dr("PostDate") = Date.Now
                                        Else
                                            dr("Comments") = lblCompDescrip.Text
                                            dr("DateCompleted") = dateCompleted
                                            dr("PostDate") = Date.Now
                                        End If


                                        StdGrdObj.ClassId = dr("ClsSectionId").ToString
                                        StdGrdObj.StuEnrollId = dr("StuEnrollId").ToString
                                        StdGrdObj.GrdBkWgtDetailId = dr("InstrGrdBkWgtDetailId").ToString
                                        StdGrdObj.Score = dr("Score")
                                        StdGrdObj.Comments = dr("Comments").ToString
                                        StdGrdObj.ResNum = dr("ResNum")
                                        StdGrdObj.ModUser = dr("ModUser")
                                        StdGrdObj.IsCompGraded = dr("IsCompGraded")

                                        StdGrdObj.DateCompleted = dr("DateCompleted")

                                        result = facade.UpdateStudentGrade(StdGrdObj, Session("UserName"))

                                    Catch ex As System.Exception
                                        Dim exTracker = New AdvApplicationInsightsInitializer()
                                        exTracker.TrackExceptionWrapper(ex)

                                        If dr("Score") Is System.DBNull.Value Then
                                            dr("Score") = txt.Text
                                            dr("ModUser") = Session("UserName")
                                            dr("ModDate") = Date.Now
                                            dr("IsCompGraded") = True
                                            dr("PostDate") = Date.Now
                                            dr("Comments") = lblCompDescrip.Text
                                            dr("DateCompleted") = dtCompleted.SelectedDate
                                        End If
                                    End Try
                                End If

                            Else
                                If arrRows.GetLength(0) > 0 Then
                                    'Row already exists on dtGBWResults table
                                    'DELETE
                                    dr = arrRows(0)
                                    StdGrdObj.GrdBkResultId = dr("GrdBkResultId").ToString

                                    result = facade.DeleteStudentGrade(StdGrdObj)

                                    dr.Delete()     'Mark row for deletion       
                                End If
                            End If

                        End If
                    Next
                Next

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                    Exit Sub
                End If

                result = facade.UpdateIncompleteGrade(ViewState("StuEnrollId"), ddlClsSection.SelectedItem.Value, chkIsIncomplete.Checked, Session("UserName"))
                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                    Exit Sub
                End If

                ds = (New StdGrdBkFacade).GetGrdBkWgtDetailsAndResultsDS(ddlClsSection.SelectedItem.Value, ViewState("StuEnrollId"))
                Session("GBWDetailsAndResultsDS") = ds

                'Build grid
                BuildGBWResultsGrid()

                'Populate results
                PopulateGBWResults(ddlClsSection.SelectedItem.Value, ViewState("StuEnrollId"))

                'Bind datalist, in case IsIncomplete was updated
                BindDataList()

                'Set buttons' state
                InitButtonsForWork()
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayScoreGrade(ByVal ttlWgt As Decimal, ByVal accumWgt As Decimal)
        lblCurrentScore.Visible = True
        lblCurrentGrade.Visible = True
        txtFinalScore.Visible = True
        txtFinalGrade.Visible = True

        If ttlWgt > 0 Then
            Dim score As Decimal = accumWgt / ttlWgt * 100
            'Commented by balaji for mantis #11119 to remove decimals
            'txtFinalScore.Text = System.Math.Round(score, 2)

            'mantis issue 15210 depending upon the config entry,it rounds up
            Dim scoreToGrade As Decimal
            If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                txtFinalScore.Text = System.Math.Round(score)
                scoreToGrade = System.Math.Round(score, 0)
            Else
                txtFinalScore.Text = System.Math.Round(score, 2)
                scoreToGrade = System.Math.Round(score, 2)
            End If



            'Get Class Section's GrdScaleId
            Dim ds As DataSet = DirectCast(Session("GBWDetailsAndResultsDS"), DataSet)
            Dim dtCompTypes As DataTable = ds.Tables("GrdComponentTypesDT")

            If dtCompTypes.Rows.Count > 0 Then
                txtFinalGrade.Text = ConvertScoreToGrade(scoreToGrade, dtCompTypes.Rows(0)("GrdScaleId").ToString)
            Else
                txtFinalGrade.Text = ""
            End If
        Else
            txtFinalScore.Text = ""
            txtFinalGrade.Text = ""
        End If
    End Sub
    Private Function ConvertScoreToGrade(ByVal score As Decimal, ByVal GrdScaleId As String) As String
        Dim ds As New DataSet
        Dim grade As String = ""
        Dim dtGrdScaleDetails As DataTable
        Dim dtGrdSystemDetails As DataTable

        'Get Grade Scale and Grade System details that are attached to this class section
        ds = (New StdGrdBkFacade).GetGrdScaleSystemDetails(ddlClsSection.SelectedItem.Value, ViewState("Instructor"), ddlTerm.SelectedItem.Value)

        dtGrdScaleDetails = ds.Tables("GradeScaleDetails")
        dtGrdSystemDetails = ds.Tables("GradeSystemDetails")

        'Filter GradeScaleDetails table by GrdScaleId
        Dim rows() As DataRow = dtGrdScaleDetails.Select("GrdScaleId=" + "'" + GrdScaleId + "'")

        Dim boolScoreFallsInGradeRange As Boolean = False

        If rows.Length > 0 Then
            'Loop throughout all the GradeScale
            For i As Integer = 0 To rows.Length - 1
                'Check if the score is between MinVal and MaxVal values
                If score >= CType(rows(i)("MinVal"), Decimal) And score <= CType(rows(i)("MaxVal"), Decimal) Then
                    grade = rows(i).GetParentRow("GradeSystemDetailsGradeScaleDetails")("Grade")
                    boolScoreFallsInGradeRange = True
                    Exit For
                End If
            Next
            'code modified by balaji on 04/04/2009 to fix issue 15792
            'Modification starts here
            If boolScoreFallsInGradeRange = False Then
                If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                    score = System.Math.Round(score, 0)
                Else
                    score = System.Math.Floor(score)
                End If

                For i As Integer = 0 To rows.Length - 1
                    'Check if the score is between MinVal and MaxVal values
                    If score >= CType(rows(i)("MinVal"), Decimal) And score <= CType(rows(i)("MaxVal"), Decimal) Then
                        grade = rows(i).GetParentRow("GradeSystemDetailsGradeScaleDetails")("Grade")
                        boolScoreFallsInGradeRange = True
                        Exit For
                    End If
                Next
            End If
            'code modified by balaji on 04/04/2009 to fix issue 15792
            'Modification Ends here
        End If
        Return grade
    End Function
    Private Sub GetInputMaskValue(ByVal linkbutton As LinkButton)
        Dim facInputMasks As New InputMasksFacade
        Dim ssnMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        If linkbutton.Text <> "" Then
            linkbutton.Text = facInputMasks.ApplyMask(ssnMask, linkbutton.Text)
        End If
    End Sub

    Private Sub dtlClsSectStds_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtlClsSectStds.ItemCommand
        Dim stuEnrollId As String
        Dim stuName As String = CType(e.Item.FindControl("lnkStudentName"), LinkButton).Text
        Dim grade As String = CType(e.Item.FindControl("lnkGrade"), LinkButton).Text
        Dim isIncomplete As String = CType(e.Item.FindControl("lnkIsIncomplete"), LinkButton).Text

        ''Validate Grade Book Weighting Level
        ''This page can only be used when Grade Book Weighting Level is set to Course Level
        'Dim strGBWLevel As String = SingletonAppSettings.AppSettings("GradeBookWeightingLevel")
        'If Not (strGBWLevel.ToLower = "courselevel") Then
        '    DisplayErrorMessage("This page cannot be used to Post Grade By Student." & vbCr & _
        '                            "The Grade Book Weighting must be setup at the Course level.")
        '    Exit Sub
        'End If

        'Validate MaxNoOfGradeBookWeightings in the web.config
        Try
            Dim maxRow As Integer = MyAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings")
            ViewState("MaxRows") = maxRow
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage("Please provide a valid numeric value for the configuration variable 'MaxNoOfGradeBookWeightings'.")
            Exit Sub
        End Try
        '
        '
        stuEnrollId = dtlClsSectStds.DataKeys(e.Item.ItemIndex).ToString

        '*******************************************************************
        pnlHeader.Visible = True
        lblStdName.Text = stuName
        lblSecName.Text = ddlClsSection.SelectedItem.Text
        '
        If isIncomplete = "True" Then
            chkIsIncomplete.Checked = True
        Else
            chkIsIncomplete.Checked = False
        End If
        '
        If Trim(grade) = "I" Then
            DisplayErrorMessage("Student has an Incomplete Grade for the course")

        ElseIf grade = "W" Then
            DisplayErrorMessage("Student has Withdrawn from the course")

        End If
        '*******************************************************************

        pnlGBWResults.Controls.Clear()
        pnlRequiredFieldValidators.Controls.Clear()
        ViewState("StuEnrollId") = ""

        Dim ds As New DataSet
        ds = (New StdGrdBkFacade).GetGrdBkWgtDetailsAndResultsDS(ddlClsSection.SelectedItem.Value, stuEnrollId)

        If ds.Tables.Count > 0 Then
            If ds.Tables("GrdComponentTypesDT").Rows.Count = 0 Then
                DisplayErrorMessage("No Grade Book Weightings were found based on the start date of " & ddlClsSection.SelectedItem.Text & ".")
                Exit Sub
            Else
                Session("GBWDetailsAndResultsDS") = ds
                ViewState("StuEnrollId") = stuEnrollId

                'Build grid
                BuildGBWResultsGrid()

                'Populate results
                PopulateGBWResults(ddlClsSection.SelectedItem.Value, stuEnrollId)

                'Set buttons' state
                InitButtonsForWork()
            End If

        Else

            DisplayErrorMessage("No Grade Book Weightings were found based on the start date of " & ddlClsSection.SelectedItem.Text & ".")
        End If
    End Sub
    Protected Sub dtlClsSectStds_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtlClsSectStds.ItemCreated
        Dim oControl As Control
        If MyAdvAppSettings Is Nothing Then
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
        End If
        For Each oControl In dtlClsSectStds.Controls(0).Controls
            If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
                If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
                    CType(CType(oControl, DataGridItem).FindControl("lblFinalGrade"), Label).Text = "Final Score"
                Else
                    CType(CType(oControl, DataGridItem).FindControl("lblFinalGrade"), Label).Text = "Final Grade"
                End If
            End If
        Next
    End Sub
    Private Sub dtlClsSectStds_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtlClsSectStds.ItemDataBound
        Select Case e.Item.ItemType

            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem

                If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                    dtlClsSectStds.Columns(2).Visible = False

                ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier") = "StudentNumber" Then
                    dtlClsSectStds.Columns(1).Visible = False

                End If
        End Select
    End Sub

    Private Sub InitButtonsForWork()
        If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

    Private Sub BuildGBWResultsGrid()
        Dim ds As DataSet = DirectCast(Session("GBWDetailsAndResultsDS"), DataSet)
        Dim dtCompTypes As DataTable = ds.Tables("GrdComponentTypesDT")

        If dtCompTypes.Rows.Count > 0 Then

            pnlGBWResults.Controls.Clear()
            pnlRequiredFieldValidators.Controls.Clear()

            Dim cols As Integer = dtCompTypes.Rows.Count
            Dim tbl As New Table
            Dim cellWidth As Integer = 100 / (cols + 1)
            tbl.ID = "tbl" & ViewState("StuEnrollId")
            tbl.Width = Unit.Percentage(100)
            tbl.CellPadding = "0"
            tbl.CellSpacing = "0"
            tbl.CssClass = "gradebooktable"

            'Header Row 0
            'Cell(0,0)
            Dim cell As New TableCell
            Dim dtCell As New TableCell

            Dim lbl As New Label
            Dim row As New TableRow
            Dim lblComments As New Label
            lbl.CssClass = "label"
            lbl.Text = "<strong>Num</strong>"
            cell.VerticalAlign = VerticalAlign.Bottom
            cell.HorizontalAlign = HorizontalAlign.Center
            cell.CssClass = "datagridheader10"
            cell.Controls.Add(lbl)
            row.Cells.Add(cell)

            For Each dr As DataRow In dtCompTypes.Rows
                cell = New TableCell
                lbl = New Label
                lbl.ID = dr("InstrGrdBkWgtDetailId").ToString
                lbl.CssClass = "label"
                lbl.Text = "<strong>" & dr("Code") & "</strong>"
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Center
                cell.CssClass = "datagridheader10"
                cell.Controls.Add(lbl)
                row.Cells.Add(cell)
                cell = New TableCell
                lbl = New Label
                lbl.ID = dr("InstrGrdBkWgtDetailId").ToString + "DateCompled"
                lbl.CssClass = "label"
                lbl.Text = "<strong>Date Completed</strong>"
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Center
                cell.CssClass = "datagridheader10"
                cell.Controls.Add(lbl)
                row.Cells.Add(cell)

            Next

            'Add Header Row to table
            tbl.Rows.Add(row)
            '
            '
            Dim txt As TextBox
            Dim dtCompleted As RadDatePicker
            Dim lblCompDescrip As Label
            '
            'Data Rows
            For i As Integer = 1 To ViewState("MaxRows")
                row = New TableRow
                cell = New TableCell
                lbl = New Label
                lbl.ID = "Num" & i
                lbl.CssClass = "label"
                lbl.Text = i
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Center
                cell.Controls.Add(lbl)
                cell.CssClass = "datagriditemstyle"
                row.Cells.Add(cell)

                For j As Integer = 0 To cols - 1

                    cell = New TableCell

                    If dtCompTypes.Rows(j).IsNull("MinNumber") Then
                        'Add Score textbox
                        txt = New TextBox
                        txt.ID = "txt" & i & "_" & dtCompTypes.Rows(j)("InstrGrdBkWgtDetailId").ToString
                        txt.CssClass = "textbox"
                        cell.Controls.Add(txt)

                        lblCompDescrip = New Label
                        lblCompDescrip.ID = "lblCompDescrip" & i & "_" & dtCompTypes.Rows(j)("Descrip").ToString
                        lblCompDescrip.Text = dtCompTypes.Rows(j)("Descrip").ToString
                        lblCompDescrip.CssClass = "label"
                        lblCompDescrip.Visible = False
                        cell.Controls.Add(lblCompDescrip)
                        cell.CssClass = "datagriditemstyle"
                        'Dim cv As New CompareValidator
                        'With cv
                        '    ' Dim newGuid As System.Guid
                        '    'MOdified by Saraswathi on DEc 16 2008 to fix controlduplication issue 
                        '    '.ID = "cv" & i & j
                        '    .ID = "cv" & i & "_" & j & "_"
                        '    .ControlToValidate = txt.ID
                        '    .Display = ValidatorDisplay.None
                        '    .ErrorMessage = "Invalid Data"
                        '    .ValueToCompare = "32767"
                        '    .Operator = ValidationCompareOperator.NotEqual
                        '    .EnableClientScript = True
                        '    .Type = ValidationDataType.Double
                        '    '.Operator = ValidationCompareOperator.LessThan
                        'End With
                        'cell.Controls.Add(cv)
                        lblCompDescrip = New Label
                        lblCompDescrip.ID = "dtlblCompDescrip" & i & "_" & dtCompTypes.Rows(j)("Descrip").ToString
                        lblCompDescrip.CssClass = "label"
                        lblCompDescrip.Text = dtCompTypes.Rows(j)("Descrip").ToString
                        lblCompDescrip.Visible = False
                        row.Cells.Add(cell)

                        dtCell = New TableCell()
                        dtCompleted = New RadDatePicker()
                        dtCompleted.ID = "dtCompleted" & i & "_" & dtCompTypes.Rows(j)("InstrGrdBkWgtDetailId").ToString
                        dtCompleted.MinDate = "1/1/1945"
                        dtCompleted.Visible = True
                        dtCell.Controls.Add(dtCompleted)
                        dtCell.CssClass = "datagriditemstyle"
                        row.Cells.Add(dtCell)

                    Else
                        If i <= dtCompTypes.Rows(j)("MinNumber") Then
                            'Add Score textbox
                            txt = New TextBox
                            txt.ID = "txt" & i & "_" & dtCompTypes.Rows(j)("InstrGrdBkWgtDetailId").ToString
                            txt.CssClass = "textbox"
                            cell.Controls.Add(txt)
                            lblCompDescrip = New Label
                            lblCompDescrip.ID = "lblCompDescrip" & i & "_" & dtCompTypes.Rows(j)("Descrip").ToString
                            lblCompDescrip.CssClass = "label"
                            lblCompDescrip.Text = dtCompTypes.Rows(j)("Descrip").ToString
                            lblCompDescrip.Visible = False
                            cell.Controls.Add(lblCompDescrip)
                            cell.CssClass = "datagriditemstyle"
                            'Dim cv As New CompareValidator
                            'With cv
                            '    'Dim newGuid As System.Guid
                            '    'MOdified by Saraswathi on DEc 16 2008 to fix controlduplication issue 
                            '    '.ID = "cv" & i & j
                            '    .ID = "cv" & i & "_" & j & "_"
                            '    .ControlToValidate = txt.ID
                            '    .Display = ValidatorDisplay.None
                            '    .ErrorMessage = "Invalid Data"
                            '    .ValueToCompare = "32767"
                            '    .Operator = ValidationCompareOperator.NotEqual
                            '    .EnableClientScript = True
                            '    .Type = ValidationDataType.Double
                            '    '.Operator = ValidationCompareOperator.LessThan
                            'End With
                            'cell.Controls.Add(cv)
                            lblCompDescrip = New Label
                            lblCompDescrip.ID = "dtlblCompDescrip" & i & "_" & dtCompTypes.Rows(j)("Descrip").ToString
                            lblCompDescrip.CssClass = "label"
                            lblCompDescrip.Text = dtCompTypes.Rows(j)("Descrip").ToString
                            lblCompDescrip.Visible = False
                            row.Cells.Add(cell)

                            dtCell = New TableCell()
                            dtCompleted = New RadDatePicker()
                            dtCompleted.ID = "dtCompleted" & i & "_" & dtCompTypes.Rows(j)("InstrGrdBkWgtDetailId").ToString
                            dtCompleted.MinDate = "1/1/1945"
                            dtCompleted.Visible = True
                            dtCell.Controls.Add(dtCompleted)
                            dtCell.CssClass = "datagriditemstyle"
                            row.Cells.Add(dtCell)
                        Else
                            lbl = New Label
                            lbl.CssClass = "label"
                            cell.Controls.Add(lbl)
                            cell.CssClass = "datagriditemstyle"
                            lblCompDescrip = New Label
                            lblCompDescrip.ID = "lblCompDescrip" & i & "_" & dtCompTypes.Rows(j)("Descrip").ToString
                            lblCompDescrip.CssClass = "label"
                            lblCompDescrip.Text = dtCompTypes.Rows(j)("Descrip").ToString
                            lblCompDescrip.Visible = False
                            cell.Controls.Add(lblCompDescrip)
                            cell.CssClass = "datagriditemstyle"
                            row.Cells.Add(cell)
                            dtCell = New TableCell()
                            lbl = New Label
                            lbl.CssClass = "label"
                            dtCell.Controls.Add(lbl)
                            dtCell.CssClass = "datagriditemstyle"
                            lblCompDescrip = New Label
                            lblCompDescrip.ID = "lblCompDescrip_" & i & "_" & dtCompTypes.Rows(j)("Descrip").ToString
                            lblCompDescrip.CssClass = "label"
                            lblCompDescrip.Text = dtCompTypes.Rows(j)("Descrip").ToString
                            lblCompDescrip.Visible = False
                            dtCell.Controls.Add(lblCompDescrip)
                            dtCell.CssClass = "datagriditemstyle"
                            row.Cells.Add(dtCell)


                        End If
                    End If
                    cell.VerticalAlign = VerticalAlign.Bottom
                    cell.HorizontalAlign = HorizontalAlign.Center


                Next
                tbl.Rows.Add(row)
            Next
            '
            '
            '
            'Weight Row
            cell = New TableCell
            lbl = New Label
            row = New TableRow
            lbl.CssClass = "label"
            lbl.Text = "<strong>Grade Book Weight</strong>"
            cell.VerticalAlign = VerticalAlign.Bottom
            cell.HorizontalAlign = HorizontalAlign.Left
            cell.Controls.Add(lbl)
            cell.CssClass = "datagriditemstyle"
            row.Cells.Add(cell)

            For Each dr As DataRow In dtCompTypes.Rows
                cell = New TableCell
                lbl = New Label
                lbl.ID = "lblWgt" & dr("InstrGrdBkWgtDetailId").ToString
                If Not dr("Weight") Is System.DBNull.Value Then
                    lbl.Text = dr("Weight")
                Else
                    lbl.Text = ""
                End If

                lbl.CssClass = "label"
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Left
                cell.Controls.Add(lbl)
                cell.CssClass = "datagriditemstyle"
                row.Cells.Add(cell)
                dtCell = New TableCell
                dtCell.CssClass = "datagriditemstyle"
                row.Cells.Add(dtCell)
            Next

            'Add Weight Row to table
            tbl.Rows.Add(row)
            '
            '
            '
            'Actual Weight Row
            cell = New TableCell
            lbl = New Label
            row = New TableRow
            lbl.CssClass = "label"
            lbl.Text = "<strong>Actual Weight</strong>"
            cell.VerticalAlign = VerticalAlign.Bottom
            cell.HorizontalAlign = HorizontalAlign.Left
            cell.Controls.Add(lbl)
            cell.CssClass = "datagriditemstyle"
            row.Cells.Add(cell)

            For Each dr As DataRow In dtCompTypes.Rows
                cell = New TableCell
                lbl = New Label
                lbl.ID = "lblWeight" & dr("InstrGrdBkWgtDetailId").ToString
                lbl.CssClass = "label"
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Left
                cell.Controls.Add(lbl)
                cell.CssClass = "datagriditemstyle"
                row.Cells.Add(cell)
                dtCell = New TableCell
                dtCell.CssClass = "datagriditemstyle"
                row.Cells.Add(dtCell)
            Next

            'Add Score Row to table
            tbl.Rows.Add(row)

            pnlGBWResults.Controls.Add(tbl)
        End If
    End Sub

    Private Sub PopulateGBWResults(Optional ByVal ClsSectId As String = "", Optional ByVal StuEnrollid As String = "")
        Dim ds As DataSet = DirectCast(Session("GBWDetailsAndResultsDS"), DataSet)
        Dim dtCompTypes As DataTable = ds.Tables("GrdComponentTypesDT")
        Dim dtGBWResults As DataTable = ds.Tables("StuGrdBkResultsDT")
        Dim ctrl As Control
        Dim tbl As Table
        Dim txt As TextBox
        Dim dtCompleted As RadDatePicker
        Dim lbl As Label
        Dim cols As Integer = dtCompTypes.Rows.Count
        Dim maxRows As Integer = ViewState("MaxRows")
        Dim rowCounter As Integer
        Dim arrRows() As DataRow
        Dim rows() As DataRow
        Dim dr As DataRow
        Dim accumWgt As Decimal = 0
        Dim ttlWgt As Decimal = 0
        Dim gbwUtil As GrdBkWgtUtility = Nothing
        Dim boolGradePosted As Boolean = False
        Dim intGradePostedCount As Integer = 0

        ctrl = pnlGBWResults.FindControl("tbl" & ViewState("StuEnrollId").ToString)
        tbl = DirectCast(ctrl, Table)

        If Not (tbl Is Nothing) Then
            '
            'Populate score textboxes from the dtGBWResults table
            For Each dr In dtGBWResults.Rows
                ctrl = pnlGBWResults.FindControl("txt" & dr("ResNum").ToString & "_" & dr("InstrGrdBkWgtDetailId").ToString)
                If Not (ctrl Is Nothing) Then
                    txt = DirectCast(ctrl, TextBox)
                    Try
                        txt.Text = If(dr("Score").ToString = "-1.00", "T", dr("Score").ToString)
                        ' txt.Text = CType(dr("Score"), String)
                        ctrl = pnlGBWResults.FindControl("dtCompleted" & dr("ResNum").ToString & "_" & dr("InstrGrdBkWgtDetailId").ToString)
                        If Not (ctrl Is Nothing) Then
                            dtCompleted = DirectCast(ctrl, RadDatePicker)
                            dtCompleted.SelectedDate = CType(dr("DateCompleted"), Date)
                        End If
                        intGradePostedCount += 1
                    Catch ex As System.Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                        txt.Text = ""
                    End Try
                End If
            Next
            If intGradePostedCount >= 1 Then
                boolGradePosted = True
            End If
            '
            '
            '
            'Loop thru the table header to read the Grading Component Types.  
            'Compute, retrieve and populate the Score and Weight labels per Grading Component Type
            rowCounter = 0
            For cellCounter As Integer = 1 To cols
                lbl = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), Label)

                'Use the dtCompTypes table to get the Grading Policy 
                rows = dtCompTypes.Select("InstrGrdBkWgtDetailId='" & lbl.ID & "'")

                If rows.GetLength(0) > 0 Then
                    '
                    'Get dtCompTypes's first row 
                    dr = rows(0)
                    '
                    'Get results for Grading Component Type
                    arrRows = dtGBWResults.Select("InstrGrdBkWgtDetailId='" & lbl.ID & "'")
                    '
                    If arrRows.GetLength(0) > 0 Then

                        ''Find if all the rows are null or Zero value is posted
                        ''Added by Saraswathi Lakshmanan on April 03 2009
                        ''If a zero value is posted then , that value is considered and weight is evaluated
                        ''if no value is posted, then those gradeweights are not considered 
                        If fnAddScoretoWeight(arrRows) = True Then
                            If Not dr("Weight") Is System.DBNull.Value Then
                                ttlWgt += dr("Weight")
                            End If

                            'Compute score using Strategy pattern
                            If Not (dr.IsNull("GrdPolicyId")) Then
                                gbwUtil = New GrdBkWgtUtility(arrRows, dr("Weight"), dr("GrdPolicyId"), dr("Param"))
                            Else
                                If Not dr("Weight") Is System.DBNull.Value Then
                                    gbwUtil = New GrdBkWgtUtility(arrRows, dr("Weight"))
                                End If
                            End If
                            'Use gbwUtil.Weight property
                            If Not dr("Weight") Is System.DBNull.Value Then
                                DirectCast(tbl.Rows(ViewState("MaxRows") + 2).Cells(cellCounter).Controls(0), Label).Text = gbwUtil.Weight
                                accumWgt += gbwUtil.Weight
                            End If
                            ''Modified by saraswathi lakshmanan to fix issue 15467
                            ''The current score is calculated based on the number of grade books entered.
                            ''if a grade is posted, then Actualvalue/ gradebook weight,
                            ''Changed on march 04 2009
                            'If gbwUtil.Weight = 0 Then
                            '    ttlWgt += 0
                            'Else
                            '    ttlWgt += dr("Weight")
                            'End If
                        End If
                    End If
                    '
                End If
                '
            Next
            '
        End If

        'Dim boolIsScorePosted As Boolean
        'boolIsScorePosted = (New StdGrdBkFacade).boolIsScorePosted(ClsSectId, StuEnrollid)

        'If score is posted for any one of the components go to displayscoregrade function
        If boolGradePosted = True Then
            DisplayScoreGrade(ttlWgt, accumWgt)
        Else
            txtFinalScore.Visible = True
            txtFinalGrade.Visible = True
            lblCurrentScore.Visible = True
            lblCurrentGrade.Visible = True
            txtFinalScore.Text = ""
            txtFinalGrade.Text = ""
        End If
    End Sub

    ''Added by Saraswathi on April 3 2009
    ''To find if the values are all null or a zero value is posted
    Public Function fnAddScoretoWeight(ByVal arrRows() As DataRow) As Boolean
        Dim flag As Boolean
        For Each dr As DataRow In arrRows
            If Not dr("Score") Is DBNull.Value Then
                If dr("Score").ToString <> "" Then
                    flag = True
                    Return True
                Else
                    flag = False
                End If
            Else
                flag = False
            End If
        Next
        Return flag
    End Function

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnBuildList)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip("ddlTerm")
        BindToolTip("ddlClsSection")


    End Sub
    ''Added by Saraswathi Lakshmanan to show the cohort Start Date on the LHS of the Screen to buuild the list based on cohortstdate and class sections 
    ''On Oct- 3- 2008

    ''Added by Saraswathi Lakshmanan to show the cohort Start Date on the LHS of the Screen to buuild the list based on cohortstdate and class sections 
    ''On Oct - 3-2008
    Private Sub PopulateClsSectionDDLbasedonCohortStDate(ByVal CohortStartDate As String)

        ''Added by Saraswathi Lakshmanan to fix Issue 14296
        username = Session("UserName")
        instructorId = ViewState("Instructor")
        isAcademicAdvisor = ViewState("isAcademicAdvisor")
        campusId = Master.CurrentCampusId
        Dim facade As New StdGrdBkFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlClsSection
            .DataSource = facade.GetClsSectionsbyUserandRolesandCohortStDt(ViewState("CohortStartDate"), instructorId, campusId, username, isAcademicAdvisor)
            .DataTextField = "CourseSectDescrip"
            .DataValueField = "ClsSectionId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        dtlClsSectStds.DataSource = Nothing
        dtlClsSectStds.DataBind()
        ClearRHS()

    End Sub

    Private Sub PopulateCohortStartDateDDL()

        Dim facade As New GrdPostingsFacade
        username = Session("UserName")
        instructorId = ViewState("Instructor")
        isAcademicAdvisor = ViewState("isAcademicAdvisor")
        campusId = Master.CurrentCampusId

        With ddlcohortStDate
            .DataSource = facade.GetCohortStartDateforCurrentAndPastTermsForAnyUserbyRoles(instructorId, username, isAcademicAdvisor, campusId)
            .DataTextField = "CohortStartDate"
            .DataValueField = "CohortStartDate"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub ddlcohortStDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcohortStDate.SelectedIndexChanged
        ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
        ddlTerm.SelectedIndex = 0
        ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
        If (ViewState("CohortStartDate") <> "") Then
            PopulateClsSectionDDLbasedonCohortStDate(ViewState("CohortStartDate"))
        End If

    End Sub


    Protected Sub ddlShift_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlShift.SelectedIndexChanged
        ddlcohortStDate.SelectedIndex = 0
        ViewState("Shift") = ddlShift.SelectedItem.Value.ToString()
        If (ViewState("Shift") <> "") Then
            PopulateClsSectionDDL(ViewState("Term"), ViewState("Shift"), ViewState("MinimumClassStartDate"))
        End If
    End Sub

    Protected Sub rdpMinimumClassStartDate_SelectedDateChanged(sender As Object, e As Calendar.SelectedDateChangedEventArgs) Handles rdpMinimumClassStartDate.SelectedDateChanged
        ddlcohortStDate.SelectedIndex = 0
        ViewState("MinimumClassStartDate") = rdpMinimumClassStartDate.DbSelectedDate
        PopulateClsSectionDDL(ViewState("Term"), ViewState("Shift"), ViewState("MinimumClassStartDate"))
    End Sub





    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
