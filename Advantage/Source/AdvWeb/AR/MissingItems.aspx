﻿<%@ Page Title="Missing Items" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="MissingItems.aspx.vb" Inherits="MissingItems" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
       <%-- <script type="text/javascript" language="javascript">
            function SetHiddenText() {
                document.forms[0].HiddenText.value = "YES";

            }
    </script>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
            <!-- begin rightcolumn -->
            <tr>
                <td class="detailsframetop">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False">
                                </asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                        align="center">
                        <tr>
                            <td class="detailsframe">
                                <div class="scrollsingleframe">
                                    <!-- begin table content-->

                                    
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="45%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblPrgVerId" runat="server" CssClass="label">Program<span style="COLOR: red">*</span></asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlPrgVerId" Width="350px" runat="server" CssClass="dropdownlist"
                                                    TabIndex="1">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStudentGrpId" runat="server" CssClass="label">Student Group<span style="COLOR: red">*</span></asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlStudentGrpId" Width="350px" runat="server" CssClass="dropdownlist"
                                                    TabIndex="2">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusCodeId" runat="server" CssClass="label">Enrollment Status</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlStatusCodeId" Width="350px" runat="server" CssClass="dropdownlist" TabIndex="3">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblShiftId" runat="server" CssClass="label">Shift</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlShiftId" Width="350px" runat="server" CssClass="dropdownlist" TabIndex="4">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblreqType" runat="server" CssClass="label">Required For</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlreqType" Width="350px" runat="server" CssClass="dropdownlist" TabIndex="4">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                &nbsp;
                                            </td>
                                            <td class="contentcell4">
                                                <asp:Button ID="btnBuildList" runat="server" Text="Get List"  TabIndex="5">
                                                </asp:Button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding-top: 12px; text-align: center">
                                                <asp:Label ID="lblMessage" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- begin content table-->
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td style="padding-top: 20px">
                                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="StudentId"
                                                    CellPadding="0" BorderWidth="1px" BorderColor="#E0E0E0" Width="100%">
                                                    <HeaderStyle CssClass="datagridheader" Wrap="True"></HeaderStyle>
                                                    <RowStyle CssClass="datagriditemstyle" />
                                                    <AlternatingRowStyle CssClass="datagridalternatingstyle" />
                                                    <Columns>
                                                        <asp:BoundField DataField="FullName" HeaderText="Student Name" SortExpression="FirstName"
                                                            HeaderStyle-CssClass="datagridheader" ItemStyle-CssClass="datagriditemstyle">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="SSN" HeaderText="SSN" HeaderStyle-CssClass="datagridheader"
                                                            ItemStyle-CssClass="datagriditemstyle" />
                                                        <asp:BoundField DataField="PrgVerDescrip" HeaderText="Program Version" HeaderStyle-CssClass="datagridheader"
                                                            ItemStyle-CssClass="datagriditemstyle" />
                                                        <asp:BoundField DataField="StatusCodeDescrip" HeaderText="Status" HeaderStyle-CssClass="datagridheader"
                                                            ItemStyle-CssClass="datagriditemstyle" />
                                                        <asp:BoundField DataField="ShiftDescrip" HeaderText="Shift" HeaderStyle-CssClass="datagridheader"
                                                            ItemStyle-CssClass="datagriditemstyle" />
                                                        <asp:BoundField DataField="PreviousEducation" HeaderText="Previous Education" HeaderStyle-CssClass="datagridheader"
                                                            ItemStyle-CssClass="datagriditemstyle" />
                                                        <asp:TemplateField HeaderText="Missing Requirements" HeaderStyle-CssClass="datagridheader"
                                                            ItemStyle-CssClass="datagriditemstyle">
                                                            <ItemStyle CssClass="missingitemstyle" />
                                                            <HeaderStyle CssClass="datagridheader" />
                                                            <ItemTemplate>
                                                                <asp:BulletedList ID="blRequirements" DataTextField="Descrip" CausesValidation="False"
                                                                    BulletStyle="Square" runat="server">
                                                                </asp:BulletedList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Missing Requirement Group(s)" HeaderStyle-CssClass="datagridheader"
                                                            ItemStyle-CssClass="datagriditemstyle">
                                                            <ItemTemplate>
                                                                <asp:DataList ID="dlReqGroup" runat="server" Width="100%">
                                                                    <ItemTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="label5" Font-Bold="true" runat="server" Text='<%# Container.dataitem("Descrip") %>'
                                                                                        CssClass="labelboldreq"></asp:Label>
                                                                                    <asp:Label ID="lblReqGrpId" Font-Bold="true" Visible="false" runat="server" Text='<%# Container.dataitem("ReqGrpId") %>'
                                                                                        CssClass="labelboldreq"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 100%; padding: 0 40px">
                                                                                    <asp:DataGrid ID="dgReq" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                                        ShowFooter="true" BorderColor="#d4ebfb" BorderStyle="solid" BorderWidth="0px">
                                                                                        <Columns>
                                                                                            <asp:TemplateColumn>
                                                                                                <ItemStyle CssClass="itemstyle" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:BulletedList ID="blReqWithinReqGroup" runat="Server" DataTextField="Descrip"
                                                                                                        CausesValidation="False" DisplayMode="Text" BulletStyle="Circle" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateColumn>
                                                                                        </Columns>
                                                                                    </asp:DataGrid>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>No records found with the selected criteria</EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
								<!-- end content table -->
                        <%--<table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblMissingItems" runat="server" Font-Bold="true" CssClass="labelheaders" Visible="false">Missing Items</asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlDocReqs" CssClass="label" runat="server" Width="100%">
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="HiddenText" Style="display: none" runat="server" CssClass="label"></asp:TextBox>
                                </td>
                            </tr>
                        </table>--%>


                                    <!--end table content-->
                                </div>
                            </td>
                        </tr>
                    </table>
   </table>


             <!--end table content-->

            
            <asp:panel id="Panel1" runat="server" CssClass="validationsummary">
            </asp:panel>
            <asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator">
                </asp:customvalidator>
            <asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True">
                </asp:validationsummary>
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

