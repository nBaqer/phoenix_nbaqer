<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="GradeSystems.aspx.vb" Inherits="GradeSystems" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>

    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstGradeSystems" runat="server" DataKeyField="GrdSystemId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.GrdSystemId") %>' Text='<%# DataBinder.Eval(Container, "DataItem.Descrip") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table id="Table2" cellspacing="0" cellpadding="0" width="60%" border="0" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDescrip" CssClass="label" runat="server">Description</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtDescrip" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Description can not be blank"
                                                    Display="None" ControlToValidate="txtDescrip">Description can not be blank</asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist"></asp:DropDownList><asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ControlToValidate="ddlCampGrpId"
                                                    Display="None" ErrorMessage="Must Select a Campus Group" ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual">Must Select a Campus Group</asp:CompareValidator></td>
                                        </tr>
                                    </table>
                                    <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:DataGrid ID="dgrdGradeSystemDetails" runat="server" ShowFooter="True" BorderStyle="Solid"
                                                    DataKeyField="GrdSysDetailId" AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false"
                                                    Width="100%" CellPadding="4" BorderColor="#E0E0E0" BorderWidth="1px">
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Grade">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrade" Text='<%# Container.DataItem("Grade") %>' runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtGrade" CssClass="textbox" runat="server" MaxLength="10" Width="40px"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditGrade" Text='<%# Ctype(Container.DataItem("Grade"), String).Trim() %>' runat="server" MaxLength="10" CssClass="textbox">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="GradeEditRequiredFieldValidator" runat="server" ControlToValidate="txtEditGrade"
                                                                    Display="None" ErrorMessage="Grade can not be blank">Grade can not be blank</asp:RequiredFieldValidator>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="GPA">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGPA" Text='<%# DataBinder.Eval(Container.DataItem, "GPA", "{0}") %>' runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtGPA" CssClass="textbox" runat="server" MaxLength="6" Width="40px"></asp:TextBox>
                                                                <asp:CompareValidator ID="GPACompareValidator" runat="server" ControlToValidate="txtGPA" Display="None"
                                                                    ErrorMessage="Invalid GPA value" Operator="GreaterThanEqual" ValueToCompare="0.0" Type="Double"></asp:CompareValidator>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditGPA" Text='<%# DataBinder.Eval(Container.DataItem, "GPA", "{0}") %>' CssClass="textbox" runat="server" Width="40px" MaxLength="6">
                                                                </asp:TextBox>
                                                                <asp:CompareValidator ID="GPAEditCompareValidator" runat="server" ControlToValidate="txtEditGPA" Display="None"
                                                                    ErrorMessage="Invalid GPA value" Operator="GreaterThanEqual" ValueToCompare="0.0" Type="Double"></asp:CompareValidator>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Credits Earned?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkItemIsCreditsEarned" Checked='<%# Container.DataItem("IsCreditsEarned") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="chkIsCreditsEarned" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkEditIsCreditsEarned" Checked='<%# Container.DataItem("IsCreditsEarned") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Credits Attempted?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkItemIsCreditsAttempted" Checked='<%# Container.DataItem("IsCreditsAttempted") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="ChkIsCreditsAttempted" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkEditIsCreditsAttempted" Checked='<%# Container.DataItem("IsCreditsAttempted") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Credits Awarded?" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkItemIsCreditsAwarded" Checked='<%# Container.DataItem("IsCreditsAwarded") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="ChkIsCreditsAwarded" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkEditIsCreditsAwarded" Checked='<%# Container.DataItem("IsCreditsAwarded") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Transfer Grade?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkItemIsTransferGrade" Checked='<%# Container.DataItem("IsTransferGrade") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="chkIsTransferGrade" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="ChkEditIsTransferGrade" Checked='<%# Container.DataItem("IsTransferGrade") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="In GPA?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkItemIsInGPA" Checked='<%# Container.DataItem("IsInGPA") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="chkIsInGPA" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkEditIsInGPA" Checked='<%# Container.DataItem("IsInGPA") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="In SAP?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkItemIsInSAP" Checked='<%# Container.DataItem("IsInSAP") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="chkIsInSAP" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="ChkEditIsInSAP" Checked='<%# Container.DataItem("IsInSAP") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Pass?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkItemIsPass" Checked='<%# Container.DataItem("IsPass") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="chkIsPass" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="ChkEditIsPass" Checked='<%# Container.DataItem("IsPass") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Incomplete?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkItemIsIncomplete" Checked='<%# Container.DataItem("IsIncomplete") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="chkIsIncomplete" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkEditIsIncomplete" Checked='<%# Container.DataItem("IsIncomplete") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Default?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkItemIsDefault" Checked='<%# Container.DataItem("IsDefault") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="chkIsDefault" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkEditIsDefault" Checked='<%# Container.DataItem("IsDefault") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Drop?" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkItemIsDrop" Checked='<%# Container.DataItem("IsDrop") %>' runat="server" Enabled="False"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:CheckBox ID="chkIsDrop" runat="server"></asp:CheckBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkEditIsDrop" Checked='<%# Container.DataItem("IsDrop") %>' runat="server"></asp:CheckBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderStyle CssClass="datagridheader" Width="12%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle_gradesystem"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images/im_edit.gif alt= edit>"
                                                                    CausesValidation="False" runat="server" CommandName="Edit">
																		<img border="0" src="../images//im_edit.gif" alt="edit"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkButUp" Text="<img border=0 src=../images/up.gif alt=MoveUp>" CausesValidation="False"
                                                                    runat="server" CommandName="Up"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkButDown" Text="<img border=0 src=../images/down.gif alt=MoveDown>" CausesValidation="False"
                                                                    runat="server" CommandName="Down"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle_gradesystem"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>" runat="server"
                                                                    CommandName="Update" />
                                                                <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>" CausesValidation="False"
                                                                    runat="server" CommandName="Delete" />
                                                                <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images/im_delete.gif alt=Cancel>" CausesValidation="False"
                                                                    runat="server" CommandName="Cancel" />
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn Visible="False">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtGrdSysDetailid" Text='<%# Container.DataItem("GrdSysDetailId") %>' runat="server" Visible="false" CssClass="textbox">
                                                                </asp:TextBox>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditGrdSysDetailId" Text='<%# Container.DataItem("GrdSysDetailId") %>' runat="server" Visible="False" CssClass="textbox">
                                                                </asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <!--end table content-->
    </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
    <asp:Panel ID="panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
        ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowSummary="False"
        ShowMessageBox="True"></asp:ValidationSummary>
    <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    <!--end validation panel-->
    </div>
</asp:Content>


