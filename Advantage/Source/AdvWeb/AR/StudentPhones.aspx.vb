﻿Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Collections
Imports System.Collections.Generic
Imports Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports System.Activities.Statements
Imports Telerik.OpenAccess

Partial Class StudentPhones
    Inherits BasePage
    <DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Protected StudentId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected LeadId As String = Guid.Empty.ToString

    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False



    Public Enum PageEntityType
        [None] = 0
        [Lead] = 1
        [Student] = 2
        [Employer] = 3
        [Employee] = 4
    End Enum

    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub


    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As StudentMRU


        Dim objStudentState As New StudentMRU

        MyBase.GlobalSearchHandler(0)

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
            StudentId = Guid.Empty.ToString()
        Else
            StudentId = AdvantageSession.MasterStudentId
        End If

        If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
            LeadId = Guid.Empty.ToString()
        Else
            LeadId = AdvantageSession.MasterLeadId
        End If


        With objStudentState
            .StudentId = New Guid(StudentId)
            .LeadId = New Guid(LeadId)
            .Name = AdvantageSession.MasterName
        End With

        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        Master.ShowHideStatusBarControl(True)

        Return objStudentState


    End Function


#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim sdfControls As New SDFComponent

        Page.Title = "Student Phones"


        resourceId = CInt(Request.QueryString("resid")) ' CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        'userId = AdvantageSession.UserState.UserId.ToString
        'ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU
        objStudentState = getStudentFromStateObject(159) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''



        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'mContext = HttpContext.Current
        ' txtResourceId.Text = CInt(m_Context.Items("ResourceId"))



        txtResourceId.Text = resourceId

        Dim advantageUserState As User = AdvantageSession.UserState


        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        Try
            If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                If boolSwitchCampus = True Then
                    CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
                End If
                'objCommon.PopulatePage(Form1)
                'Disable the new and delete buttons
                'objCommon.SetBtnState(Form1, "NEW", pObj)
                ViewState("MODE") = "NEW"

                '   build dropdownlists
                BuildDropDownLists()

                '   bind datalist
                BindDataList(StudentId)

                '   bind an empty new AcademicYearInfo
                BindStudentPhoneData(New StuPhoneInfo)

                '   initialize buttons
                InitButtonsForLoad()
                '16521: ENH: Galen: FERPA: Compliance Issue 
                'added by Theresa G on May 7th 2010
                'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
                '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
                'End If
                MyBase.uSearchEntityControlId.Value = ""
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                'Master.PageObjectId = txtStudentPhoneId.Text
                'Master.PageResourceId = resourceId

            End If
            GetInputMaskValue()
            'Header1.EnableHistoryButton(False)

            'change text on the default checkbox
            chkdefault1.Text = "Show this phone on the student info section"

            'Check If any UDF exists for this resource
            Dim intSDFExists As Integer = sdfControls.GetSDFExists(resourceId, ModuleId)
            If intSDFExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If


            If Not Trim(txtPhone.Text) = "" Then
                sdfControls.GenerateControlsEditSingleCell(pnlSDF, resourceId, txtStudentPhoneId.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, ModuleId)
            End If



        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub GetInputMaskValue()
        'Dim facInputMasks As New InputMasksFacade
        ''  Dim correctFormat As Boolean
        'Dim strMask As String
        ''Dim zipMask As String
        ''Dim errorMessage As String
        'Dim strPhoneReq As String
        ''Dim strZipReq As String
        ''Dim strFaxReq As String
        'Dim objCommon As New CommonUtilities
        ''  Dim ssnMask As String

        'If chkForeignPhone.Checked = False Then
        '    'Get The Input Mask for Phone/Fax and Zip
        '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        '    'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        '    'accepts only certain characters as mask characters
        '    txtPhone.Mask = Replace(strMask, "#", "9")
        '    'Get The Format Of the input masks and display it next to caption
        '    'labels
        '    lblPhone.ToolTip = strMask
        'Else
        '    txtPhone.Mask = ""
        '    lblPhone.ToolTip = ""
        'End If

        ''Get The RequiredField Value
        ''        Dim strSSNReq As String

        'strPhoneReq = objCommon.SetRequiredColorMask("Phone")

        ''If The Field Is Required Field Then Color The Masked
        ''Edit Control

        'If strPhoneReq = "Yes" Then
        '    txtPhone.BackColor = Color.FromName("#ffff99")
        'End If
    End Sub
    Private Sub BindDataList(ByVal studentId As String)
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind AcademicYears datalist
        dlstStdPhone.DataSource = New DataView((New StdPhoneFacade).GetAllStudentPhones(studentId).Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstStdPhone.DataBind()
    End Sub
    Private Sub BuildDropDownLists()
        'BuildStatusDDL()
        'BuildPhoneTypeDDL()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Status DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing))

        'PhoneTypes DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneTypeID, AdvantageDropDownListName.Phone_Types, campusId, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub

    Private Sub BuildPhoneTypeDDL()
        Dim PhoneType As New LeadFacade
        With ddlPhoneTypeID
            .DataTextField = "PhoneTypeDescrip"
            .DataValueField = "PhoneTypeId"
            .DataSource = PhoneType.GetPhoneTypes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindStudentPhoneData(ByVal StudentPhone As StuPhoneInfo)
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        With StudentPhone
            If .ForeignPhone = True Then
                chkForeignPhone.Checked = True
                txtPhone.Text = ""
                txtPhone.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtPhone.DisplayMask = ""
                txtPhone.DisplayPromptChar = ""
            Else
                txtPhone.Mask = "(###)-###-####"
                txtPhone.DisplayMask = "(###)-###-####"
                txtPhone.DisplayPromptChar = ""
                chkForeignPhone.Checked = False
            End If

            'Get Phone
            txtPhone.Text = .Phone
            'If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
            '    'txtPhone.Text = facInputMasks.ApplyMask(strMask, txtPhone.Text)
            'End If

            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'Get Address2
            txtExt.Text = .Extension

            'Get City
            txtBestTime.Text = .BestTime

            txtStudentId.Text = StudentId
            txtStudentPhoneId.Text = .StdPhoneId

            'AddressType
            'ddlPhoneTypeID.SelectedValue = .PhoneType
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneTypeID, .PhoneTypeId, .PhoneType)

            'AddressStatus
            If (.PhoneStatus <> Guid.Empty.ToString()) Then
                ddlStatusId.SelectedValue = .PhoneStatus
            End If

            If .Default1 = True Then
                chkdefault1.Checked = True
            Else
                chkdefault1.Checked = False
            End If
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub

    Private Sub dlstStdPhone_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstStdPhone.ItemCommand

        '   get the AcademicYearId from the backend and display it
        GetStudentPhone(e.CommandArgument)

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = resourceId
        Master.setHiddenControlForAudit()
        'CType(Master.FindControl("hdnObjectId"), HiddenField).Value = e.CommandArgument
        'CType(Master.FindControl("hdnPageResourceId"), HiddenField).Value = txtResourceId.Text

        'DISABLE - DD
        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStdPhone, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEditSingleCell(pnlSDF, resourceId, e.CommandArgument, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstStdPhone, e.CommandArgument)

    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub GetStudentPhone(ByVal StdPhoneId As String)

        '   bind AcademicYear properties
        BindStudentPhoneData((New StdPhoneFacade).GetStudentPhoneInfo(StdPhoneId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList(txtStudentId.Text)
        BindStudentPhoneData(New StuPhoneInfo)

        'initialize buttons
        InitButtonsForLoad()


        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, ModuleId)

        CommonWebUtilities.RestoreItemValues(dlstStdPhone, Guid.Empty.ToString)

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim result As String
        Dim errorMessage As String
        Dim facade As New StdPhoneFacade
        Dim doesDefaultExist As Integer

        If Not chkForeignPhone.Checked And txtPhone.Text.Length < 10 Then
            DisplayErrorMessage("Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone field)")
            Exit Sub
        End If

        doesDefaultExist = facade.DoesDefaultPhoneExist(txtStudentId.Text, txtStudentPhoneId.Text)
        If doesDefaultExist >= 1 And chkdefault1.Checked Then
            DisplayErrorMessage("A default Phone already exists for this student")
            Exit Sub
        End If
        'Check If Mask is successful only if Foreign Is not checked
        If chkForeignPhone.Checked = False Then
            errorMessage = ValidateFieldsWithInputMasks()
        Else
            errorMessage = ""
        End If

        If errorMessage = "" Then
            With New StdPhoneFacade
                '   update AcademicYear Info 
                result = .UpdateStudentPhone(BuildStdPhoneInfo(txtStudentPhoneId.Text), AdvantageSession.UserState.UserName)
            End With

            '   bind the datalist
            BindDataList(txtStudentId.Text)

            'DISABLE - DD
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstStdPhone, txtStudentPhoneId.Text, ViewState, Header1)

            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get the AcademicYearId from the backend and display it
                GetStudentPhone(txtStudentPhoneId.Text)
                'Results.Visible = True
                chkIsInDB.Checked = True
                InitButtonsForEdit()
            End If

            ''   if there are no errors bind a new entity and init buttons
            'If Page.IsValid Then

            '    '   set the property IsInDB to true in order to avoid an error if the user
            '    '   hits "save" twice after adding a record.
            '    chkIsInDB.Checked = True

            '    'note: in order to display a new page after "save".. uncomment next lines
            '    '   bind an empty new AcademicYearInfo
            '    'BindAcademicYearData(New AcademicYearInfo)

            '    '   initialize buttons
            '    'InitButtonsForLoad()
            '    InitButtonsForEdit()

            'End If


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim sdfid As ArrayList
            Dim sdfidValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim sdfControl As New SDFComponent
            Try
                sdfControl.DeleteSDFValue(txtStudentPhoneId.Text)
                sdfid = sdfControl.GetAllLabels(pnlSDF)
                sdfidValue = sdfControl.GetAllValues(pnlSDF)
                For z = 0 To sdfid.Count - 1
                    sdfControl.InsertValues(txtStudentPhoneId.Text, Mid(sdfid(z).id, 5), sdfidValue(z))
                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ' US3037 5/4/2012 Janet Robinson
            CommonWebUtilities.RestoreItemValues(dlstStdPhone, txtStudentPhoneId.Text)

        Else
            DisplayErrorMessageMask(errorMessage)
        End If



    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Function ValidateFieldsWithInputMasks() As String
        'Dim facInputMasks As New InputMasksFacade
        'Dim correctFormat As Boolean
        'Dim strMask As String
        ''        Dim zipMask As String
        'Dim errorMessage As String = ""
        ''   Dim ssnMask As String

        'strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        ''Validate the phone field format. If the field is empty we should not apply the mask
        ''agaist it.
        'If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for phone field." & vbCr
        '    End If
        'End If

        'Return errorMessage
        Return String.Empty
    End Function
    Private Function BuildStdPhoneInfo(ByVal StdPhoneId As String) As StuPhoneInfo
        'instantiate class
        Dim studentPhone As New StuPhoneInfo
        Dim facInputMask As New InputMasksFacade
        Dim phoneMask As String
        'Dim zipMask As String
        'Dim ssnMask As String

        With studentPhone
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get StudentMasterID
            .StudentID = txtStudentId.Text

            'get AcademicYearId
            .StdPhoneId = StdPhoneId

            .Phone = txtPhone.Text

            .Extension = txtExt.Text

            .BestTime = txtBestTime.Text

            'Foreign Phone
            If chkForeignPhone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If

            If chkdefault1.Checked = True Then
                .Default1 = 1
            Else
                .Default1 = 0
            End If

            'Get Phone
            phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
            If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
                '.Phone = facInputMask.RemoveMask(phoneMask, txtPhone.Text)
            Else
                .Phone = txtPhone.Text
            End If

            'Get PhoneType
            .PhoneType = ddlPhoneTypeID.SelectedValue

            'Get Phone Status
            .PhoneStatus = ddlStatusId.SelectedValue

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)
        End With
        'return data
        Return studentPhone
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        '   bind an empty new AcademicYearInfo
        BindStudentPhoneData(New StuPhoneInfo)

        'DISABLE - DD
        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStdPhone, Guid.Empty.ToString, ViewState, Header1)

        'initialize buttons
        InitButtonsForLoad()

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, ModuleId)

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstStdPhone, Guid.Empty.ToString)

    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (txtStudentPhoneId.Text = Guid.Empty.ToString) Then
            'update AcademicYear Info 
            Dim result As String = (New StdPhoneFacade).DeleteStudentPhone(txtStudentPhoneId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList(txtStudentId.Text)

                '   bind an empty new AcademicYearInfo
                BindStudentPhoneData(New StuPhoneInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim SDFControl As New SDFComponent
            SDFControl.DeleteSDFValue(txtStudentPhoneId.Text)
            SDFControl.GenerateControlsNewSingleCell(pnlSDF, resourceId, ModuleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        End If

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstStdPhone, Guid.Empty.ToString)

    End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignPhone.CheckedChanged
        'Dim objCommon As New CommonWebUtilities
        If chkForeignPhone.Checked Then
            txtPhone.Text = ""
            txtPhone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtPhone.DisplayMask = ""
            txtPhone.DisplayPromptChar = ""
        Else
            txtPhone.Mask = "(###)-###-####"
            txtPhone.DisplayMask = "(###)-###-####"
            txtPhone.DisplayPromptChar = ""
        End If
        'CommonWebUtilities.SetFocus(Me.Page, txtPhone)
        txtPhone.Focus()
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'If pObj.HasFull Or pObj.HasDelete Then
        '    btnDelete.Enabled = True
        '    'Set the Delete Button so it prompts the user for confirmation when clicked
        '    btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        'Else
        '    btnDelete.Enabled = False
        'End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ' ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class