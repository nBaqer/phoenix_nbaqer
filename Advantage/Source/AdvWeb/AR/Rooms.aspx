<%@ Page Language="vb" AutoEventWireup="false" Inherits="Rooms" CodeFile="Rooms.aspx.vb" %>
<%@ Register TagPrefix="fame" TagName="footer" Src="../UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
    <HEAD>
        <title>Rooms</title>
        <<link href="../css/localhost.css" type="text/css" rel="stylesheet" />
        <LINK href="../CSS/systememail.css" type="text/css" rel="stylesheet">
        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <script language="javascript" src="../AuditHist.js"></script>
    </HEAD>
    <body runat="server" leftMargin="0" topMargin="0" ID="Body1" NAME="Body1">
        <form id="Form1" method="post" runat="server">
            <!-- beging header -->
            
            
             <table style="width:100%;">
                <tr>
                   <td colspan="2">
                        <table cellSpacing="0" cellPadding="0" width="100%" border="0">
                            <tr>
                                <td><IMG src="../images/advantage_building_rooms.jpg"></td>
                                <td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
                            </tr>
                        </table>
                   </td> 
                </tr>
                <tr >
                    <td style="width:20%;height:400px;vertical-align: top;">
                        <table><tr><td><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
                            <tr>
                                <td class="listframe">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="15%" nowrap align="left"><asp:Label ID="lblShow" Runat="server"><b class="label">Show</b></asp:Label></td>
                                            <td width="85%" nowrap>
                                                <asp:radiobuttonlist id="radStatus" CssClass="Label" AutoPostBack="true" Runat="Server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Active" Selected="True" />
                                                    <asp:ListItem Text="Inactive" />
                                                    <asp:ListItem Text="All" />
                                                </asp:radiobuttonlist>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="listframebottom"><div class="scrollleftpopup">
                                        <asp:datalist id="dlstRooms" runat="server" DataKeyField="RoomId">
                                            <SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
                                            <ItemStyle CssClass="NonSelectedItem"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton id="imgInActive" ImageUrl="../images/Inactive.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>' CausesValidation="False">
                                                </asp:ImageButton>
                                                <asp:ImageButton id="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>' CausesValidation="False">
                                                </asp:ImageButton>
                                                <asp:Label ID="lblId" Runat="server" Visible="false" text='<%# Container.DataItem("StatusId")%>' />
                                                <asp:LinkButton text='<%# Container.DataItem("Descrip")%>' Runat="server" CssClass="NonSelectedItem" CommandArgument='<%# Container.DataItem("RoomId")%>' ID="Linkbutton1" CausesValidation="False" />
                                            </ItemTemplate>
                                        </asp:datalist>
                                    </div>
                                </td>
                            </tr>
                        </table></td></tr></table>
                    </td>
                    <td style="width:80%;vertical-align:top; ">
                        <table cellpadding="0" cellspacing="0" border="1px" >
                            <tr>
                                <td class="menuframe" align="right" valign="top"  >
                                    <asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Height="25px"   ></asp:button>
                                    <asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Height="25px"></asp:button>
                                    <asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Height="25px"></asp:button>
                                </td>
                            </tr>
                        </table><br/>
                            
                   
                        <table style="width:100%;">
                            
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table5">
                                        <tr>
                                            <TD class="detailsframe" style="vertical-align:middle;">
                                    
                                                <asp:panel id="pnlRHS" Runat="server">
                                                    
                                                        <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="60%" align="center">
                                                            <TR>
                                                                <TD class="twocolumnlabelcell" style="WIDTH: 60%" colspan="2">
                                                                    <asp:Label id="lblNote" Runat="server" CssClass="Label" Font-Bold="true"></asp:Label></TD>
                                                            </TR>
                                                            <TR>
                                                                <TD class="twocolumnlabelcell">
                                                                    <asp:label id="lblBldgDescrip" runat="server" CssClass="Label">Building</asp:label></TD>
                                                                <TD class="twocolumncontentcell">
                                                                    <asp:textbox id="txtBldgDescrip" runat="server" CssClass="TextBox" ReadOnly="True"></asp:textbox></TD>
                                                            </TR>
                                                            <TR>
                                                                <TD class="twocolumnlabelcell">
                                                                    <asp:label id="lblCode" Runat="server" CssClass="Label"></asp:label></TD>
                                                                <TD class="twocolumncontentcell">
                                                                    <asp:textbox id="txtCode" Runat="server" CssClass="TextBox" MaxLength="128"></asp:textbox></TD>
                                                            </TR>
                                                            <TR>
                                                                <TD class="twocolumnlabelcell">
                                                                    <asp:label id="lblDescrip" Runat="server" CssClass="Label"></asp:label></TD>
                                                                <TD class="twocolumncontentcell">
                                                                    <asp:textbox id="txtDescrip" Runat="server" CssClass="TextBox" MaxLength="128"></asp:textbox></TD>
                                                            </TR>
                                                            <TR>
                                                                <TD class="twocolumnlabelcell">
                                                                    <asp:label id="lblCapacity" Runat="server" CssClass="Label"></asp:label></TD>
                                                                <TD class="twocolumncontentcell">
                                                                    <asp:textbox id="txtCapacity" Runat="server" CssClass="TextBox" MaxLength="128"></asp:textbox>
                                                                    <asp:CustomValidator id="Customvalidator4" Runat="server" OnServerValidate="TextValidate" Display="None"
                                                                        ErrorMessage="Negative Values Not Allowed" ControlToValidate="txtCapacity"></asp:CustomValidator>
                                                                </TD>
                                                            </TR>
                                                            <TR>
                                                                <TD class="twocolumnlabelcell">
                                                                    <asp:label id="lblStatusId" Runat="server" CssClass="Label"></asp:label></TD>
                                                                <TD class="twocolumncontentcell">
                                                                    <asp:dropdownlist id="ddlStatusId" runat="server" CssClass="DropDownList"></asp:dropdownlist></TD>
                                                            </TR>
                                                            <TR>
                                                                <TD class="twocolumnlabelcell" colspan="2">
                                                                    <asp:textbox id="txtRoomId" Runat="server" MaxLength="128" Visible="false"></asp:textbox>
                                                                    <asp:textbox id="txtBldgId" Runat="server" MaxLength="128" Visible="false"></asp:textbox></TD>
                                                                <td>
                                                                <asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
                                                                <asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
                                                                </TD>
                                                                </tr>
                                                         </table> 
                                                    
                                            </asp:panel>
                                        </td>
                                    </TR>
                                </TABLE>
                            </td>
                        </tr>
                    </table>
                </td>
                </tr>
            </table>
            
            <asp:panel id="pnlRequiredFieldValidators" runat="server" Width="50px"></asp:panel>&nbsp;&nbsp;
            <asp:validationsummary id="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary>
            <div id="footer">Copyright  FAME 2005 - 2019. All rights reserved.</div>
        </form>
    </body>
</HTML>
