﻿<%@ Page Title="Enter Class Sections With Periods" Language="VB" MasterPageFile="~/NewSite.master"
    AutoEventWireup="false" CodeFile="ClassSectionsWithPeriods_New.aspx.vb" Inherits="ClassSectionsWithPeriods_New" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

    <script language="javascript" src="../js/AuditHist.js" type="text/jscript"></script>
    <script type="text/javascript">
        window.onload = function () {
            var strCook = document.cookie;
            if (strCook.indexOf("!~") != 0) {
                var intS = strCook.indexOf("!~");
                var intE = strCook.indexOf("~!");
                var strPos = strCook.substring(intS + 2, intE);
                document.getElementById("grdWithScroll").scrollTop = strPos;
            }

        }
    </script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }



        function RowDblClick(sender, eventArgs) {
            sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
        }
    </script>
    <style type="text/css">
        div.RadWindow_Web20 a.rwCloseButton {
            margin-right: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrdClassMeetings">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrdClassMeetings" />
                    <telerik:AjaxUpdatedControl ControlID="ScheduleConflictWindow" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="300" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table cellspacing="0" cellpadding="0" width="400px" border="0" runat="server" id="LeftMenu">
                <tr>
                    <td class="listframeclasssections">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td class="listframetop2">
                                    <table id="AutoNumber6" cellspacing="0" cellpadding="2" width="100%">
                                        <tr>
                                            <td class="employersearch">
                                                <asp:Label ID="lblTerm" Width="80px" runat="server" CssClass="label">Term</asp:Label>
                                            </td>
                                            <td class="employersearch2">
                                                <telerik:RadComboBox ID="ddlTerm" Style="margin-left: -30px;" runat="server" DataTextField="TermDescrip"
                                                    DataValueField="TermId" AutoPostBack="True" Filter="Contains" CausesValidation="False" AppendDataBoundItems="true"
                                                    Width="200px" DropDownWidth="300px">
                                                </telerik:RadComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch" style="height: 22px">
                                                <asp:Label ID="lblCourse" Width="80px" runat="server" CssClass="label">Course</asp:Label>
                                            </td>
                                            <td class="employersearch2" style="height: 22px">
                                                <telerik:RadComboBox ID="ddlCourse" Style="margin-left: -30px;" runat="server" DataTextField="Descrip"
                                                    DataValueField="Reqid" AppendDataBoundItems="true" Filter="Contains" Width="200px" DropDownWidth="300px">
                                                </telerik:RadComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch">
                                                <asp:Label ID="lblInstructor" Width="80px" runat="server" CssClass="label">Instructor</asp:Label>
                                            </td>
                                            <td class="employersearch2">
                                                <telerik:RadComboBox ID="ddlInstructor" Style="margin-left: -30px;" runat="server"
                                                    DataTextField="InstructorDescrip" DataValueField="InstructorId" Filter="Contains" AppendDataBoundItems="true"
                                                    Width="200px" DropDownWidth="300px">
                                                </telerik:RadComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch">
                                                <asp:Label ID="lblShiftId2" Width="80px" runat="server" CssClass="label">Shift</asp:Label>
                                            </td>
                                            <td class="employersearch2">
                                                <telerik:RadComboBox ID="ddlShiftId2" Style="margin-left: -30px;" runat="server"
                                                    DataTextField="ShiftDescrip" DataValueField="ShiftId" Filter="Contains" AppendDataBoundItems="true"
                                                    Width="200px">
                                                </telerik:RadComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch">
                                                <asp:Label ID="lblFilterStartDate" CssClass="label" runat="server">Start Date</asp:Label>
                                            </td>
                                            <td class="employersearch2">
                                                <telerik:RadDatePicker ID="radFilterStartDate" runat="server" Width="200px" Style="margin-left: -30px;">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch">
                                                <asp:Label ID="lblFilterEndDate" CssClass="label" runat="server">End Date</asp:Label>
                                            </td>
                                            <td class="employersearch2">
                                                <telerik:RadDatePicker ID="radFilterEndDate" runat="server" Width="200px" Style="margin-left: -30px;">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch"></td>
                                            <td class="employersearch2" style="text-align: left">
                                                <div style="width: 200px; text-align: center">
                                                    <asp:Button ID="btnBuildList" runat="server" Text="Build List" CausesValidation="False"></asp:Button>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="listframebottom">
                                    <div id="grdWithScroll">
                                        <table id="AutoNumber5" cellspacing="0" cellpadding="0" width="300px" border="0">
                                            <tr>
                                                <td>
                                                    <telerik:RadGrid ID="dtlClsSectList" runat="server" Width="300px" AutoGenerateColumns="False"
                                                        AllowSorting="True" Visible="false">
                                                        <MasterTableView DataKeyNames="ClsSectionId" ShowHeadersWhenNoRecords="false" Width="300px"
                                                            ItemStyle-Wrap="True">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="TermDescrip" HeaderText="Term" SortExpression="TermDescrip"
                                                                    UniqueName="TermDescrip" ItemStyle-Wrap="True">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="CodeAndDescrip" HeaderText="Course" SortExpression="CodeAndDescrip"
                                                                    UniqueName="CodeAndDescrip" ItemStyle-Wrap="True">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="ClsSection" HeaderText="Section" SortExpression="ClsSection"
                                                                    UniqueName="ClsSection" ItemStyle-Wrap="True">
                                                                </telerik:GridBoundColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                        <PagerStyle Mode="NumericPages"></PagerStyle>
                                                        <ClientSettings EnableRowHoverStyle="true" EnablePostBackOnRowClick="true">
                                                            <Selecting AllowRowSelect="True" />
                                                        </ClientSettings>
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both">

            <asp:Panel ID="pnlRHS" runat="server" style ="overflow: auto;">

                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                            <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                            <asp:Button ID="btnCloneClass" runat="server" CssClass="copy" Text="Copy" CausesValidation="False" ToolTip="Copy Class"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <div class="boxContainer">
                                <h3>
                                    <asp:Label ID="headerTitle" runat="server"></asp:Label></h3>
                                <!-- begin content table-->

                                <!-- Adjust width to 95% to keep entire contents of panel on the screen  -->
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <asp:Label ID="lblErrMsg" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnClone" runat="server" Text="Clone"
                                                ToolTip="Copy classes to same term"
                                                Visible="false" /></td>
                                    </tr>
                                    <tr>
                                        <td class="classsectionlabelcell">
                                            <asp:Label ID="lblTermId" runat="server" CssClass="label">Term</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <asp:DropDownList ID="ddlTermId" runat="server" DataTextField="TermDescrip" DataValueField="TermId" Width="200px" CssClass="dropdownlist"
                                                AutoPostBack="True" CausesValidation="False">
                                            </asp:DropDownList>

                                        </td>
                                        <td class="employersearch">
                                            <asp:Label ID="lblStartDate" CssClass="label" runat="server">Start Date</asp:Label>
                                        </td>
                                        <td class="employersearch2">
                                            <telerik:RadDatePicker ID="txtStartDate" runat="server" Width="200px">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="classsectionlabelcell">
                                            <asp:Label ID="lblCourseId" runat="server" CssClass="label">Course<snap style="color: #b71c1c;">*</snap></asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <asp:DropDownList ID="ddlReqId" runat="server" DataTextField="Descrip" DataValueField="ReqId" Width="200px" CssClass="dropdownlist"
                                                AutoPostBack="True" CausesValidation="False">
                                            </asp:DropDownList>

                                        </td>
                                        <td class="classsectionlabelcell">
                                            <asp:Label ID="lblEndDate" CssClass="label" runat="server">End Date</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <telerik:RadDatePicker ID="txtEndDate" runat="server" Width="200px">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="employersearch">
                                            <asp:Label ID="lblSection" runat="server" CssClass="label">Section<snap style="color: #b71c1c;">*</snap></asp:Label>
                                        </td>
                                        <td class="employersearch">
                                            <asp:TextBox ID="txtClsSection" runat="server" CssClass="textbox" Width="176px"></asp:TextBox>
                                        </td>
                                        <td class="classsectionlabelcell" style="white-space: nowrap;">
                                            <asp:Label ID="lblMaxStud" runat="server" CssClass="label">Max Students</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <asp:TextBox ID="txtMaxStud" runat="server" CssClass="textbox" Width="176px"></asp:TextBox>
                                            <asp:HiddenField ID="hdnMaxStud" runat="server" />
                                            <asp:CustomValidator ID="Customvalidator4" runat="server" ControlToValidate="txtMaxStud"
                                                ErrorMessage="Negative Values Not Allowed" Display="None" OnServerValidate="TextValidate"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="employersearch">
                                            <asp:Label ID="lblInstr" runat="server" CssClass="label">Instructor</asp:Label>
                                        </td>
                                        <td class="employersearch2">
                                            <asp:DropDownList ID="ddlInstructorId" runat="server" DataTextField="InstructorDescrip" CssClass="dropdownlist"
                                                DataValueField="InstructorId" CausesValidation="False" Width="200px">
                                            </asp:DropDownList>

                                            <asp:TextBox ID="hdnGrdWeights" runat="server" Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="hdnInstructor" runat="server" Visible="false"></asp:TextBox>
                                        </td>
                                        <td class="employersearch" style="white-space: nowrap;">
                                            <asp:Label ID="lblCampusId" runat="server" CssClass="label">Campus</asp:Label>
                                        </td>
                                        <td class="employersearch2">
                                            <asp:DropDownList ID="ddlCampus" runat="server" DataTextField="CampDescrip" DataValueField="CampusId" Width="200px" CssClass="dropdownlist"
                                                CausesValidation="False">
                                            </asp:DropDownList>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="employersearch">
                                            <asp:Label ID="label1" runat="server" CssClass="label">Shift</asp:Label>
                                        </td>
                                        <td class="employersearch2">
                                            <asp:DropDownList ID="ddlShiftId" runat="server" DataTextField="ShiftDescrip" DataValueField="ShiftId" Width="200px" CssClass="dropdownlist"
                                                CausesValidation="False">
                                            </asp:DropDownList>

                                        </td>
                                        <td class="employersearch">
                                            <asp:Label ID="lblGrdScaleId" runat="server" CssClass="label">Grade Scale</asp:Label>
                                        </td>
                                        <td class="employersearch2">
                                            <asp:DropDownList ID="ddlGrdScaleId" runat="server" DataTextField="Descrip" DataValueField="GrdScaleId" Width="200px" CssClass="dropdownlist"
                                                CausesValidation="False">
                                            </asp:DropDownList>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="employersearch" style="white-space: nowrap;">
                                            <asp:Label ID="lblStudentStartDate" CssClass="label" runat="server">Student Start Date</asp:Label>
                                        </td>
                                        <td class="employersearch2">
                                            <telerik:RadDatePicker ID="txtStudentStartDate" runat="server" MinDate="1/1/1945" Width="200px">
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td class="employersearch" style="white-space: nowrap;">
                                            <asp:Label ID="label3" runat="server" CssClass="label">Student Group</asp:Label>
                                        </td>
                                        <td class="employersearch2">
                                            <asp:DropDownList ID="ddlLeadGrp" runat="server" DataTextField="Descrip" DataValueField="LeadGrpId" CssClass="dropdownlist"
                                                CausesValidation="False" Width="200px">
                                            </asp:DropDownList>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="classsectionlabelcell" style="white-space: nowrap;">
                                            <asp:Label ID="lblAllowEarlyIn" CssClass="Label" runat="server">Allow Early In</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <asp:CheckBox ID="chkAllowEarlyIn" runat="server" CssClass="checkbox" TextAlign="Right"
                                                CausesValidation="false" AutoPostBack="true" />
                                        </td>
                                        <td class="classsectionlabelcell" style="white-space: nowrap;">
                                            <asp:Label ID="lblAllowLateOut" runat="server" CssClass="Label">Allow Late Out</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <asp:CheckBox ID="chkAllowLateOut" runat="server" CssClass="checkbox" TextAlign="Right"
                                                CausesValidation="false" AutoPostBack="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="classsectionlabelcell" style="white-space: nowrap;">
                                            <asp:Label ID="lblAllowExtraHours" runat="server" CssClass="Label">Allow Extra Hours</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <asp:CheckBox ID="chkAllowExtraHours" runat="server" CssClass="checkbox" TextAlign="Right"
                                                Enabled="false" />
                                        </td>
                                        <td class="classsectionlabelcell" style="white-space: nowrap;">
                                            <asp:Label ID="lblCheckTardyIn" runat="server" CssClass="Label">Check Tardy In</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <asp:CheckBox ID="chkCheckTardyIn" runat="server" CssClass="checkbox" TextAlign="Right"
                                                CausesValidation="false" AutoPostBack="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="classsectionlabelcell" style="white-space: nowrap;">
                                            <asp:Label ID="lblMaxInBeforeTardy" runat="server" CssClass="Label">Max In Before Tardy</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <telerik:RadTimePicker ID="radMaxInBeforeTardy" runat="server" ZIndex="30001" Enabled="false" Width="200px"
                                                AutoPostBack="true">
                                                <TimeView ID="TimeView1" TimeFormat="t" runat="server" StartTime="06:00:00" EndTime="22:15:00"
                                                    Interval="00:15:00" Columns="5" RenderDirection="Vertical">
                                                </TimeView>
                                            </telerik:RadTimePicker>
                                            <asp:RequiredFieldValidator ID="rqdMaxTardy" runat="server" ControlToValidate="radMaxInBeforeTardy"
                                                ErrorMessage="Max In Before Tardy is required" Display="None" Enabled="false">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td class="classsectionlabelcell" style="white-space: nowrap;">
                                            <asp:Label ID="lblAssignTardyInTime" runat="server" CssClass="Label">Assign Tardy In Time</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <telerik:RadTimePicker ID="radAssignTardyInTime" runat="server" ZIndex="30001" Width="200px">
                                                <TimeView ID="TimeView2" TimeFormat="t" runat="server" StartTime="06:00:00" EndTime="22:15:00"
                                                    Interval="00:15:00" Columns="5" RenderDirection="Vertical">
                                                </TimeView>
                                            </telerik:RadTimePicker>
                                            <asp:RequiredFieldValidator ID="rqdAssignTardy" runat="server" ControlToValidate="radAssignTardyInTime"
                                                ErrorMessage="Assign Tardy In Time is required" Display="None" Enabled="false">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="classsectionlabelcell">
                                            <asp:Label ID="label2" runat="server" CssClass="label" Visible="false">Period</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell">
                                            <asp:DropDownList ID="ddlPeriodId" runat="server" CssClass="dropdownlist" AutoPostBack="true" Width="200px"
                                                Visible="false">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="classsectionlabelcell"></td>
                                        <td class="classsectioncontentcell"></td>
                                    </tr>
                                    <tr>
                                        <td class="classsectionlabelcell"></td>
                                        <td class="classsectioncontentcell">
                                            <asp:CheckBox ID="chkAllowConflicts" AutoPostBack="True" CssClass="checkbox" Text="Allow Override of Conflicts"
                                                runat="server" Checked="False" Visible="false"></asp:CheckBox>
                                        </td>
                                        <td class="classsectionlabelcell"></td>
                                        <td class="classsectioncontentcell"></td>
                                    </tr>
                                    <tr>
                                        <td class="classsectionlabelcell">
                                            <asp:Label ID="lblConflictCourses" runat="server" CssClass="label" Visible="False">Override Conflicts With</asp:Label>
                                        </td>
                                        <td class="classsectioncontentcell" colspan="3">
                                            <asp:ListBox ID="lbxConflictCourses" runat="server" CssClass="listboxclasssection"
                                                Visible="False" SelectionMode="Multiple" Rows="5"></asp:ListBox>
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 100%">
                                    <tr>
                                        <td colspan="4">
                                            <asp:CheckBox ID="chkAllowScheduleConflicts" runat="server" CssClass="checkbox" Checked="false" Text="Override Scheduling Conflicts" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><telerik:RadGrid ID="RadGrdClassMeetings" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                                                    AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True"
                                                    AutoGenerateColumns="False" Width="100%" AllowMultiRowEdit="false">
                                                    <PagerStyle Mode="NextPrevAndNumeric" />
                                                    <MasterTableView Width="100%" CommandItemDisplay="Bottom" DataKeyNames="ClsSectMeetingId"
                                                        HorizontalAlign="NotSet" AutoGenerateColumns="False" EditMode="InPlace" PageSize="5">
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" CancelImageUrl="~/images/icon/icon_cancel.png" UpdateImageUrl="~/images/icon/icon_save.png" InsertImageUrl="~/images/icon/icon_add.png"  EditImageUrl="~/images/icon/icon_edit.png">
                                                                <%--<ItemStyle CssClass="MyImageButton" Width="300px" />--%>
                                                            </telerik:GridEditCommandColumn>
                                                        
                                                            <telerik:GridBoundColumn UniqueName="ClsSectMeetingId" DataField="ClsSectMeetingId"
                                                                Visible="false" />
                                                            <telerik:GridBoundColumn UniqueName="InstructionTypeId" DataField="InstructionTypeId"
                                                                Visible="false" />
                                                            <telerik:GridTemplateColumn UniqueName="ContainerPeriod" HeaderStyle-Width="20%"
                                                                ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label1" CssClass="title" Text="Period" runat="server"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label CssClass="label" ID="lblContainer1" runat="server" Text='<%# Eval("PeriodDescrip") %>'></asp:Label>
                                                                    <asp:Label CssClass="label" ID="lblPeriodContainer" runat="server" Text='<%# Eval("PeriodId") %>'
                                                                        Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList CssClass="dropdownlist" ID="ddlContainerPeriod" runat="server"
                                                                        DataSource='<%# dtPeriods %>' DataTextField="PeriodDescrip" DataValueField="PeriodId">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <HeaderStyle Width="20%" />
                                                                <ItemStyle Width="20%" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="ContainerAltPeriod" HeaderStyle-Width="20%"
                                                                ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label2" CssClass="title" Text="Alternate Period" runat="server"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label CssClass="label" ID="lblContainer2" runat="server" Text='<%# Eval("AltPeriodDescrip") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList CssClass="dropdownlist" ID="ddlContainerAltPeriod" runat="server"
                                                                        DataSource='<%# dtAltPeriods %>' DataTextField="PeriodDescrip" DataValueField="PeriodId">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <HeaderStyle Width="20%" />
                                                                <ItemStyle Width="20%" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="ContainerRooms" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label3" CssClass="title" Text="Room" runat="server"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label CssClass="label" ID="lblContainer3" runat="server" Text='<%# Eval("RoomDescrip") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList CssClass="dropdownlist" ID="ddlContainerRooms" runat="server" DataSource='<%# dtRooms %>'
                                                                        DataTextField="Descrip" DataValueField="RoomId">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <HeaderStyle Width="20%" />
                                                                <ItemStyle Width="20%" />
                                                            </telerik:GridTemplateColumn>

                                                            <telerik:GridTemplateColumn HeaderText="Start Date" UniqueName="ContainerStartDate"
                                                                HeaderStyle-Width="16%" ItemStyle-Width="16%">
                                                                <ItemTemplate>
                                                                    <asp:Label CssClass="label" runat="server" ID="Sdate" Text='<%# Eval("StartDate", "{0:d}") %>'>
                                                                    </asp:Label>
                                                                    &nbsp;
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadDatePicker ID="pickerStartDate" runat="server" DbSelectedDate='<%# Bind("StartDate") %>'
                                                                        Width="130px" MinDate="1/1/1945">
                                                                    </telerik:RadDatePicker>
                                                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Enter between the range"
                                                                        ControlToValidate="pickerStartDate" OnServerValidate="CustomValidator1_ServerValidate"
                                                                        Display="None"></asp:CustomValidator>
                                                                    <asp:RequiredFieldValidator ID="rqd2" runat="server" ControlToValidate="pickerStartDate"
                                                                        ErrorMessage="StartDate required" Display="None"></asp:RequiredFieldValidator>
                                                                </EditItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="End Date" UniqueName="ContainerEndDate" HeaderStyle-Width="16%"
                                                                ItemStyle-Width="16%" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="Edate" CssClass="label" Text='<%# Eval("EndDate", "{0:d}") %>'>
                                                                    </asp:Label>
                                                                    &nbsp;
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadDatePicker ID="pickerEndDate" runat="server" DbSelectedDate='<%# Bind("EndDate") %>'
                                                                        Width="130px">
                                                                    </telerik:RadDatePicker>
                                                                    <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Enter between the range"
                                                                        ControlToValidate="pickerEndDate" OnServerValidate="CustomValidator1_ServerValidate"
                                                                        Display="None"></asp:CustomValidator>
                                                                    <asp:RequiredFieldValidator ID="rqd1" runat="server" ControlToValidate="pickerEndDate"
                                                                        ErrorMessage="EndDate required" Display="None"></asp:RequiredFieldValidator>
                                                                </EditItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="ContainerInstruction" HeaderStyle-Width="20%"
                                                                ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="label4" CssClass="title" Text="Instruction Type" runat="server"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label CssClass="label" ID="lblInstructionType" runat="server" Text='<%# Eval("InstructionTypeDescrip") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList CssClass="dropdownlist" ID="ddlInstructionType" runat="server"
                                                                        DataSource='<%# dtInstructionTypes %>' DataTextField="InstructionTypeDescrip"
                                                                        DataValueField="InstructionTypeId" Width="90">
                                                                    </asp:DropDownList>
                                                                    <asp:CompareValidator ID="cfvInstructionType" runat="server" ControlToValidate="ddlInstructionType"
                                                                        ValueToCompare="00000000-0000-0000-0000-000000000000" Type="String" Operator="GreaterThan"
                                                                        ErrorMessage="Instruction Type can not be blank" Display="None">
                                                                    </asp:CompareValidator>
                                                                </EditItemTemplate>
                                                                <HeaderStyle Width="20%" />
                                                                <ItemStyle Width="20%" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="ContainerBreakDuration" HeaderStyle-Width="20%"
                                                                ItemStyle-Width="20%">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblbreak" CssClass="title" Text="Break Duration in Mins" runat="server"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label CssClass="label" ID="lblBreakDuration" runat="server" Text='<%# Eval("BreakDuration") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtBreakDuration" runat="server" CssClass="TextBox" Text='<%# Eval("BreakDuration") %>'>
                                                                    </asp:TextBox>
                                                                </EditItemTemplate>
                                                                <HeaderStyle Width="20%" />
                                                                <ItemStyle Width="20%" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="ContainerHidden" Visible="false">
                                                                <ItemTemplate>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:Label runat="server" ID="lblCmdType" Text='<%# Eval("CmdType") %>' Visible="false">
                                                                    </asp:Label>
                                                                </EditItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridButtonColumn ConfirmText="Delete this Class Section meeting?" ConfirmDialogType="RadWindow"
                                                                ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" ImageUrl="~/images/icon/icon_cancel.png"
                                                                UniqueName="DeleteColumn">
                                                                <%--<ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />--%>
                                                            </telerik:GridButtonColumn>
                                                            <telerik:GridBoundColumn UniqueName="AttUsedCnt" DataField="AttUsedCnt" Visible="false" />
                                                        </Columns>
                                                        <EditFormSettings ColumnNumber="2" CaptionDataField="EmployeeID" CaptionFormatString="Edit properties of Product {0}">
                                                            <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                                                            <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
                                                            <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" BackColor="White"
                                                                Width="100%" />
                                                            <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px" BackColor="White" />
                                                            <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                                                          <%--  <EditColumn ButtonType="ImageButton" InsertText="Insert Order" UpdateText="Update record"
                                                                UniqueName="EditCommandColumn1" CancelText="Cancel edit">
                                                            </EditColumn>--%>
                                                            <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                    <ClientSettings>
                                                        <ClientEvents OnRowDblClick="RowDblClick" />
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                        </td>
                                    </tr>

                                </table>
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" style="text-align: center;">
                                    <asp:TextBox ID="txtClsSectionId" runat="server" CssClass="textbox" Visible="False">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtOriginalTermId" runat="server" CssClass="textbox" Visible="false">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtClsSectMeetingId1" runat="server" CssClass="textbox" Visible="False">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate
                                    </asp:TextBox>
                                    <asp:CheckBox ID="chkfetchedFromDB" runat="server" CssClass="label" Text="Satisfactory/UnSatisfactory"
                                        Visible="False"></asp:CheckBox>
                                </table>
                                <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="6">
                                                <asp:Label ID="lblSDF" CssClass="label" runat="server" Font-Bold="true">School Defined Fields</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                    </table>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="contentcell2" colspan="6">
                                                <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false">
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>

                                <!-- end content table-->
                            </div>
                        </td>
                    </tr>
                </table>

            </asp:Panel>

        </telerik:RadPane>
    </telerik:RadSplitter>






    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>





    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <asp:CompareValidator ID="cmv123" runat="server" ErrorMessage="End date must be greater than the start date"
        Operator="GreaterThanEqual" ControlToValidate="txtEndDate" Display="None" ControlToCompare="txtStartDate"
        Type="Date"></asp:CompareValidator>
    <telerik:RadWindowManager ID="ScheduleConflictWindow" runat="server" Behaviors="Default" InitialBehaviors="None" Visible="false">
        <Windows>
            <telerik:RadWindow ID="DialogWindow" Behaviors="Close" VisibleStatusbar="false"
                ReloadOnShow="true" Modal="true" runat="server" Height="400px" Width="700px"
                VisibleOnPageLoad="false" Top="0" Left="20">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Title="Schedule Conflicts in Same Class" Font-Bold="true" ForeColor="Red">
    </telerik:RadWindowManager>
</asp:Content>
