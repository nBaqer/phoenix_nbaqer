﻿Imports Fame.Common
Imports System.Data
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Xml
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic

Partial Class CoursePrereqs
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo

    Protected campusId As String


    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim ds As New DataSet
        'Dim objCommon As New CommonUtilities
        'Dim campusId As String
        'Dim userId As String
        '        Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        'disable history button
        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            'Dim objcommon As New CommonUtilities
            'Populate the Activities Datagrid with the person who is logged in Activities
            'Default the Selection Criteria for the activity records
            ViewState("Term") = ""
            ViewState("MODE") = "NEW"
            ViewState("ClsSectIdGuid") = ""
            ViewState("SelectedIndex") = "-1"

            BuildDropDownLists()
            'BuildCourseDDL()

            ViewState("MODE") = "NEW"
        Else
            'If Session("JustClosed") = "Closed" Then
            '    MoveRecordToRHS(Session("ActivityAssignmentGuid"))
            '    Session("JustClosed") = ""
            'End If

            ''Added to fix issue 17642-- Modified on sept 23 2009
            ''Refresh on page causes the disabled items to be enabled again.
            Dim iSelectedrows As Integer

            For iSelectedrows = 0 To lstSelectCourses.Items.Count - 1
                If lstSelectCourses.Items(iSelectedrows).Text.EndsWith("(All Program Versions)") Then
                    'lstSelectCourses.Items(iSelectedrows).Text = lstSelectCourses.Items(iSelectedrows).Text.Remove(lstSelectCourses.Items(iSelectedrows).Text.ToString.Length - 4, 4)
                    If ddlPrgverID.SelectedItem.Value <> Guid.Empty.ToString Then
                        'lstSelectCourses.Items(iSelectedrows).Attributes("Disabled") = True
                        lstSelectCourses.Items(iSelectedrows).Attributes.Add("disabled", "true")
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub BuildCourseDDL()
        Dim facade As New CoursePrereqFacade
        With ddlCourseId
            .DataTextField = "CodeAndDescrip"
            .DataValueField = "ReqId"
            .DataSource = facade.GetAllCourses(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDropDownLists()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        ''Added by saraswathi on July 29 2009 to fix isue 
        ''16753: BUG: Cannot apply pre-req to just one program. 
        ''Build Programversion DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrgverID, AdvantageDropDownListName.ProgramVersions, campusId, True, False, Guid.Empty.ToString))


        'Courses DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCourseId, AdvantageDropDownListName.Courses, campusId, True, False, Guid.Empty.ToString))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
        ddlPrgverID.Items(0).Text = "All Program Versions"

    End Sub
    Private Sub ddlCourseId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCourseId.SelectedIndexChanged
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim facade As New CoursePrereqFacade

        ViewState("Course") = ddlCourseId.SelectedItem.Value.ToString
        ViewState("ProgVer") = ddlPrgverID.SelectedItem.Value.ToString
        If ddlCourseId.SelectedItem.Value.ToString = "00000000-0000-0000-0000-000000000000" Then
        Else
            ds = facade.GetAvailSelectedCourses(ViewState("Course"), campusId, ViewState("ProgVer"))
        End If

        'If SingletonAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
        '    Try
        '        For Each drRow As DataRow In ds.Tables("AvailCourses").Rows
        '            If drRow("Code").ToString.Substring(0, 2).ToLower = "sh" Then
        '                drRow.Delete()
        '            End If
        '        Next

        '        For Each drRow As DataRow In ds.Tables("SelectedCourses").Rows
        '            If drRow("Code").ToString.Substring(0, 2).ToLower = "sh" Then
        '                drRow.Delete()
        '            End If
        '        Next
        '    Catch ex As System.Exception
         '    	Dim exTracker = new AdvApplicationInsightsInitializer()
        '    	exTracker.TrackExceptionWrapper(ex)

        '    End Try

        '    ds.AcceptChanges()
        'End If

        'Bind the Campuses List Box
        BindCoursesListBox(ds)

        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If


        Session("SelectedCourses") = ds.Tables("SelectedCourses")
    End Sub

    Private Sub BindCoursesListBox(ByRef ds As DataSet)
        If ds.Tables.Count = 0 Then
            lstAvailCourses.Items.Clear()
            lstSelectCourses.Items.Clear()
        Else
            lstAvailCourses.DataSource = New DataView(ds.Tables("AvailCourses"), "", "Descrip", DataViewRowState.CurrentRows)
            lstAvailCourses.DataTextField = "CodeAndDescrip"
            lstAvailCourses.DataValueField = "ReqId"
            lstAvailCourses.DataBind()


            lstSelectCourses.DataSource = New DataView(ds.Tables("SelectedCourses"), "", "Descrip", DataViewRowState.CurrentRows)
            lstSelectCourses.DataTextField = "CodeAndDescrip"
            lstSelectCourses.DataValueField = "PreCoReqId"
            lstSelectCourses.DataBind()

            Dim iSelectedrows As Integer

            For iSelectedrows = 0 To lstSelectCourses.Items.Count - 1
                If lstSelectCourses.Items(iSelectedrows).Text.EndsWith("(All Program Versions)") Then
                    'lstSelectCourses.Items(iSelectedrows).Text = lstSelectCourses.Items(iSelectedrows).Text.Remove(lstSelectCourses.Items(iSelectedrows).Text.ToString.Length - 4, 4)
                    If ddlPrgverID.SelectedItem.Value <> Guid.Empty.ToString Then
                        'lstSelectCourses.Items(iSelectedrows).Attributes("Disabled") = True
                        lstSelectCourses.Items(iSelectedrows).Attributes.Add("disabled", "true")
                    End If
                End If
            Next
        End If
        btnRemove.Enabled = True
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If lstAvailCourses.SelectedIndex >= 0 Then
            Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim objListGen As New DataListGenerator
            Dim Reqid As String



            'Add selected item to the relevant assigned list. 
            Reqid = lstAvailCourses.SelectedItem.Value.ToString
            lstSelectCourses.Items.Add(New ListItem(lstAvailCourses.SelectedItem.Text, lstAvailCourses.SelectedItem.Value))
            dt = Session("SelectedCourses")
            dr = dt.NewRow()
            dr("PreCoReqId") = lstAvailCourses.SelectedItem.Value.ToString
            dt.Rows.Add(dr)
            Session("SelectedCourses") = dt
            'Remove the item from the lstAvailCourses list
            SelIndex = lstAvailCourses.SelectedIndex
            lstAvailCourses.Items.RemoveAt(SelIndex)
            ''Get the prereqs for the selected Item.
            ''After the selected item is moved delete all the prereqs for that item in the available list.
            Dim facade As New CoursePrereqFacade
            Dim dsPreReqsforSelecteditem As New DataSet

            Dim PrgVerid As String
            Dim I As Integer
            PrgVerid = ddlPrgverID.SelectedItem.Value.ToString
            dsPreReqsforSelecteditem = facade.getPrereqsandReqsForSelectediteminAvailListBox(campusId, Reqid, PrgVerid)
            If dsPreReqsforSelecteditem.Tables.Count > 0 Then
                If dsPreReqsforSelecteditem.Tables(0).Rows.Count > 0 Then
                    If lstAvailCourses.Items.Count > 0 Then
                        For I = lstAvailCourses.Items.Count - 1 To 0 Step -1
                            If dsPreReqsforSelecteditem.Tables(0).Select("ReqId='" + lstAvailCourses.Items(I).Value.ToString + "'").Length > 0 Then
                                lstAvailCourses.Items.RemoveAt(I)

                            End If
                        Next
                    End If
                End If
            End If



            'ds = objListGen.SummaryListGenerator()
            'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
            'PopulateDataList(ds)
        End If


        Dim iSelectedrows As Integer
        For iSelectedrows = 0 To lstSelectCourses.Items.Count - 1
            If lstSelectCourses.Items(iSelectedrows).Text.EndsWith("(All Program Versions)") Then
                'lstSelectCourses.Items(iSelectedrows).Text = lstSelectCourses.Items(iSelectedrows).Text.Remove(lstSelectCourses.Items(iSelectedrows).Text.ToString.Length - 4, 4)
                If ddlPrgverID.SelectedItem.Value <> Guid.Empty.ToString Then
                    'lstSelectCourses.Items(iSelectedrows).Attributes("Disabled") = True
                    lstSelectCourses.Items(iSelectedrows).Attributes.Add("disabled", "true")
                End If
            End If
        Next
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click

        Dim SelIndex As Integer
        Dim iCourseId As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator
        Dim ReqId As String

        If lstSelectCourses.SelectedIndex >= 0 Then
            'Add selected item to the lstAvailCourses list. In order to do this we
            'have to use the FldsSelectCamps to find out the 'FldId for the selected item.
            ReqId = lstSelectCourses.SelectedItem.Value.ToString
            lstAvailCourses.Items.Add(New ListItem(lstSelectCourses.SelectedItem.Text, lstSelectCourses.SelectedItem.Value))
            dt = Session("SelectedCourses")
            dr = dt.Rows.Find(lstSelectCourses.SelectedItem.Value.ToString)
            iCourseId = dr("PreCoReqId").ToString
            If dr.RowState <> DataRowState.Added Then
                'Mark the row as deleted
                dr.Delete()
            Else
                dt.Rows.Remove(dr)
            End If
            'Remove the item from the lstSelectCourses list
            SelIndex = lstSelectCourses.SelectedIndex
            lstSelectCourses.Items.RemoveAt(SelIndex)
            Session("SelectedCourses") = dt


            ''''''''''''''''
            ''Get the prereqs for the selected Item.
            ''After the selected item is moved delete all the prereqs for that item in the available list.
            Dim facade As New CoursePrereqFacade
            Dim dsPreReqsforSelecteditem As New DataSet

            Dim PrgVerid As String
            Dim I As Integer
            Dim InnerloopforListitems As Integer
            Dim SelecteditemExists As Boolean = False
            PrgVerid = ddlPrgverID.SelectedItem.Value.ToString
            dsPreReqsforSelecteditem = facade.getPrereqsandReqsForSelectediteminAvailListBox(campusId, ReqId, PrgVerid)
            If dsPreReqsforSelecteditem.Tables.Count > 0 Then
                If dsPreReqsforSelecteditem.Tables(0).Rows.Count > 0 Then

                    For I = 0 To dsPreReqsforSelecteditem.Tables(0).Rows.Count - 1
                        SelecteditemExists = False
                        For InnerloopforListitems = 0 To lstAvailCourses.Items.Count - 1
                            If lstAvailCourses.Items(InnerloopforListitems).Value.ToString = dsPreReqsforSelecteditem.Tables(0).Rows(I)("ReqId").ToString Then
                                SelecteditemExists = True
                                Exit For
                            End If
                        Next
                        If SelecteditemExists = False Then
                            If ddlCourseId.SelectedValue.ToString <> dsPreReqsforSelecteditem.Tables(0).Rows(I)("ReqId").ToString Then
                                lstAvailCourses.Items.Add(New ListItem(dsPreReqsforSelecteditem.Tables(0).Rows(I)("CodeAndDescrip").ToString, dsPreReqsforSelecteditem.Tables(0).Rows(I)("ReqId").ToString))
                            End If
                        End If
                    Next

                End If
            End If




            'ds = objListGen.SummaryListGenerator()
            'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
            'PopulateDataList(ds)

        End If
        Dim iSelectedrows As Integer
        For iSelectedrows = 0 To lstSelectCourses.Items.Count - 1
            If lstSelectCourses.Items(iSelectedrows).Text.EndsWith("(All Program Versions)") Then
                'lstSelectCourses.Items(iSelectedrows).Text = lstSelectCourses.Items(iSelectedrows).Text.Remove(lstSelectCourses.Items(iSelectedrows).Text.ToString.Length - 4, 4)
                If ddlPrgverID.SelectedItem.Value <> Guid.Empty.ToString Then
                    '                    lstSelectCourses.Items(iSelectedrows).Attributes("Disabled") = True
                    lstSelectCourses.Items(iSelectedrows).Attributes.Add("disabled", "true")
                End If
            End If
        Next
    End Sub

    Private Sub btnAddAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAll.Click
        Dim i As Integer
        '        Dim SelIndex As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator

        'Loop thru the available campus list and add all the entries to the selected campus list
        'And simultaneously remove all the campuses from avail camp list
        'The code in this sub should only execute if an item is selected
        'in the lstAvailCourses listbox.

        For i = 0 To lstAvailCourses.Items.Count - 1

            lstSelectCourses.Items.Add(New ListItem(lstAvailCourses.Items(i).Text, lstAvailCourses.Items(i).Value))
            dt = Session("SelectedCourses")
            dr = dt.NewRow()
            dr("PreCoReqId") = lstAvailCourses.Items(i).Value
            dt.Rows.Add(dr)
            Session("SelectedCourses") = dt
        Next
        'Remove all the items from the lstAvailCourses list
        lstAvailCourses.Items.Clear()

        'ds = objListGen.SummaryListGenerator()
        'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
        'PopulateDataList(ds)

    End Sub

    Private Sub btnRemoveAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAll.Click
        Dim i As Integer
        Dim iCourseId As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator

        For i = 0 To lstSelectCourses.Items.Count - 1

            lstAvailCourses.Items.Add(New ListItem(lstSelectCourses.Items(i).Text, lstSelectCourses.Items(i).Value))
            dt = Session("SelectedCourses")
            dr = dt.Rows.Find(lstSelectCourses.Items(i).Value)
            iCourseId = dr("PreCoReqId").ToString
            If dr.RowState <> DataRowState.Added Then
                'Mark the row as deleted
                dr.Delete()
            Else
                dt.Rows.Remove(dr)
            End If

        Next
        'Remove all the items from the lstSelectCourses list
        lstSelectCourses.Items.Clear()

        'ds = objListGen.SummaryListGenerator()
        'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
        'PopulateDataList(ds)

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ' Dim i As Integer
        ' Dim strGuid As String
        Dim dt1, dt2 As DataTable
        Dim dr As DataRow
        Dim objCommon As New CommonUtilities
        Dim PreReqId As String
        Dim ds1 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim dv2 As New DataView
        ' Dim storedGuid As String
        Dim sReqId As String
        Dim facade As New CoursePrereqFacade
        Dim CourseReqTyp As Integer
        ' Dim msg As String
        Dim Progverid As String

        Try



            'Run getchanges method on the dataset to see which rows have changed.
            'This section handles the changes to the Advantage Required fields
            dt1 = Session("SelectedCourses")
            dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted)
            If Not IsNothing(dt2) Then
                For Each dr In dt2.Rows
                    If dr.RowState = DataRowState.Added Then
                        PreReqId = dr("PreCoReqId").ToString

                        sReqId = ddlCourseId.SelectedItem.Value.ToString

                        CourseReqTyp = 1
                        ''Modified by Saraswathi lakshmanan to fix issue 
                        ''16753: BUG: Cannot apply pre-req to just one program. 
                        Progverid = ddlPrgverID.SelectedItem.Value.ToString

                        'Insert GrdBkWgt into arClsSections Table
                        facade.InsertCoursePrereq(Progverid, sReqId, PreReqId, CourseReqTyp, AdvantageSession.UserState.UserName)


                    ElseIf dr.RowState = DataRowState.Deleted Then
                        PreReqId = dr("PreCoReqId", DataRowVersion.Original).ToString
                        sReqId = ddlCourseId.SelectedItem.Value.ToString()
                        ''Modified by Saraswathi lakshmanan to fix issue 
                        ''16753: BUG: Cannot apply pre-req to just one program. 
                        Progverid = ddlPrgverID.SelectedItem.Value.ToString

                        facade.DeleteCoursePrereq(sReqId, PreReqId, Progverid)
                        'If msg <> "" Then
                        '    DisplayErrorMessage(msg)
                        '    Exit Sub
                        'End If
                        'Delete row from ResTblFlds table. It is important to remember that when a row
                        'is marked as deleted we cannot access the value from the current version of the
                        'row. We have to use the DataRowVersion enumeration to obtain the original value
                        'stored in the row.

                    End If
                Next
            End If


            dt1.AcceptChanges()
            Session("SelectedCourses") = dt1

            'End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnAddAll)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(btnRemoveAll)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip("ddlPrgverID")
        BindToolTip("ddlCourseId")
        BindToolTip("lstAvailCourses")
        BindToolTip("lstSelectCourses")
    End Sub

    Protected Sub ddlPrgverID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrgverID.SelectedIndexChanged
        Dim ds As New DataSet
        Dim facade As New CoursePrereqFacade

        ViewState("ProgVer") = ddlPrgverID.SelectedItem.Value.ToString
        If ddlPrgverID.SelectedItem.Value = Guid.Empty.ToString Then
            ds = facade.GetAllCourses(campusId)
        Else
            ds = facade.GetTheCoursesforProgverid(ViewState("ProgVer"), campusId)
        End If
        With ddlCourseId
            .DataTextField = "CodeAndDescrip"
            .DataValueField = "ReqId"
            .DataSource = ds.Tables(0)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
        Dim dsCourselistBox As New DataSet
        BindCoursesListBox(dsCourselistBox)
    End Sub
    Protected Sub lstSelectCourses_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSelectCourses.SelectedIndexChanged
        'lstSelectCourses.Items(iSelectedrows).Text.EndsWith("(All Programs)")
        If lstSelectCourses.SelectedItem.Text.EndsWith("(All Program Versions)") Then
            btnRemove.Enabled = False
        Else
            btnRemove.Enabled = True
        End If
    End Sub

End Class
