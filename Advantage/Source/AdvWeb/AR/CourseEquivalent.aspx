<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="CourseEquivalent.aspx.vb" Inherits="AR_CourseEquivalent" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>

</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
        Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" orientation="horizontaltop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="table3">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="98%" border="0" cellpadding="0" cellspacing="0" id="table5">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button>
                                    <asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"
                                        Enabled="false"></asp:Button>
                                    <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3>
                                            <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                        </h3>
                                        <asp:Panel ID="pnlrhs" runat="server">
                                            <!--begin content table-->
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                                <tr>
                                                    <td class="spacertables"></td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell" align="right">
                                                        <asp:Label ID="lblreqid" runat="server" CssClass="label">Equivalent Course</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" align="left">
                                                        <asp:DropDownList ID="ddlreqid" TabIndex="2" runat="server" CssClass="dropdownlist"
                                                            AutoPostBack="true" Width="400px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <p>
                                            </p>
                                            <div class="scrolldatagridright">
                                                <asp:Panel ID="pnlgrid" runat="server" Visible="true">
                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                        <tr>
                                                            <td class="spacertables"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="contentcellheadercourseequivalent" style="margin: 5px 50px 50px 40px; padding-left: 60px;">
                                                                <asp:Label ID="label1" CssClass="label" runat="server">select courses from the list below </asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <p>
                                                </p>
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-left: 40px;">
                                                    <tr>
                                                        <td width="100%">
                                                            <asp:DataGrid ID="dgrdcourseequivalent" runat="server" Width="100%" BorderWidth="1px"
                                                                ShowFooter="false" AutoGenerateColumns="false" AllowSorting="true"
                                                                Height="70px">
                                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                                <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="center"></ItemStyle>
                                                                <HeaderStyle HorizontalAlign="center" CssClass="datagridheaderstyle"></HeaderStyle>
                                                                <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                                <EditItemStyle CssClass="datagriditemstyle"></EditItemStyle>
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="all">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <HeaderTemplate>
                                                                            <input id="chkallitems" type="checkbox" value="check all" onclick="checkalldatagridcheckboxes('chkreqselected',
        document.forms[0].chkallitems.checked)" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkreqselected" runat="server" Checked='<%# checkcourse(container.dataitem("selectedequivreqid")) %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Code">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblcode" Text='<%# container.dataitem("code") %>' CssClass="label"
                                                                                runat="server">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Description">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="60%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldescrip" Text='<%# container.dataitem("descrip") %>' CssClass="label"
                                                                                runat="server">
                                                                            </asp:Label>
                                                                            <asp:Label ID="lbladreqid" Text='<%# container.dataitem("reqid") %>' CssClass="label"
                                                                                runat="server" Visible="false">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- end content table -->
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- start validation panel-->
            <!-- start validation panel-->
            <asp:Panel ID="panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary"
                ErrorMessage="customvalidator" Display="none"></asp:CustomValidator>
            <asp:Panel ID="pnlrequiredfieldvalidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary"
                ShowMessageBox="true" ShowSummary="false"></asp:ValidationSummary>
            <asp:TextBox ID="txtresourceid" runat="server" Visible="false"></asp:TextBox>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
