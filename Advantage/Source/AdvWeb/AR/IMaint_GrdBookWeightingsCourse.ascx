<%@ Control Language="VB" AutoEventWireup="false" CodeFile="IMaint_GrdBookWeightingsCourse.ascx.vb"
    Inherits="AR_IMaint_GrdBookWeightingsCourse" %>
<%@ Register TagPrefix="ew" Assembly="eWorld.UI" Namespace="eWorld.UI" %>
<style>
    .datagriditemstyleweighting
    {
        padding:10px;
    }
    .datagridheaderweighting
    {
        padding:10px;
    }

    .label
{
	font-size: 11px;
	font-style: normal;
	color: #000066;
	font-family: verdana;
	background-color: transparent;
	text-decoration: none;
	padding-left: 2px;
	padding-right: 2px;
	padding-top: 4px;
}
</style>
<div style="padding: 16px 0 16px 16px;height:100%">

    <table width="50%" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td class="twocolumnlabelcell" style="text-align: right">
                <span class="label">Course:</span>
            </td>
            <td class="twocolumncontentcell" style="text-align: left">
                <asp:Label ID="lblCourse" runat="server" CssClass="label" />
                 <asp:HiddenField ID="hfCourseCredits" runat="server" />
                 
                </td>
        </tr>
        <tr>
            <td class="twocolumnlabelcell" style="text-align: right">
                <asp:Label ID="lblEfectiveDate" runat="server" CssClass="label" Text="Effective Date: " /></td>
            <td class="twocolumncontentcell" style="text-align: left">
                <asp:TextBox ID="txtEffectiveDate" CssClass="textboxdate" Width="160px" runat="server"
                     />
                <img height="1" src="../images/1x1.gif" width="10">
                <a id="A1" onclick="javascript:OpenCalendar('Form1', 'txtStartDate', false, 1945)"
                    runat="server"></a>
            </td>
        </tr>
    </table>
</div>
<div style="overflow: scroll; height: 300px; margin: 0; padding: 0 16px 16px 16px;">
    <table width="95%" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
                <asp:Repeater ID="rptDetails" runat="server">
                    <HeaderTemplate>
                        <table class="weightingtable" width="100%" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td class="datagridheaderweighting" style="width: 37%"> 
                                    Grading Component</td>
                                <td class="datagridheaderweighting" style="width: 37%"> 
                                    Type</td>
                                <td class="datagridheaderweighting" style="width: 12%">
                                    Number</td>
                                <td class="datagridheaderweighting" style="width: 13%">
                                    Weight</td>
                                <% If MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then%>
                                 <td class="datagridheaderweighting" style="width: 25%; white-space: nowrap">
                                    Credits per service</td>
                                <% end if %>
                                <td class="datagridheaderweighting" style="width: 25%; white-space: nowrap">
                                    Grading Policy</td>
                                <td class="datagridheaderweighting" style="width: 12%">
                                    Param</td>
                                <% If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Then%>
                                <td class="datagridheaderweighting" style="width: 37%">
                                   Required</td>
                                <td class="datagridheaderweighting" style="width: 37%">
                                    Must Pass</td>
                                <% end if %>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="datagriditemstyleweighting">
                                <asp:HiddenField ID="hfGrdBkWgtId" runat="server" />
                                <asp:HiddenField ID="hfGrdCompTypeId" runat="server" />
                                <asp:HiddenField ID="hfSysGrdCompTypeId" runat="server" />
                                <asp:HiddenField ID="hfModUser" runat="server" />
                                <!--<asp:ImageButton ID="ibToMaintPage_GrdComp" runat="server" ImageUrl="~/images/AR/to_maintpage.gif"
                                    CommandName="ToMaintGrdComps" ToolTip="Go to maintenance page" />-->
                                <asp:Label ID="lblDescrip" runat="server" CssClass="label" /></td>
                            <td class="datagriditemstyleweighting">
                                <asp:Label ID="lblType" runat="server" CssClass="label" /></td>
                            <td class="datagriditemstyleweighting">
                                <ew:NumericBox runat="server" ID="txtNumber" PositiveNumber="True" CssClass="textbox" /></td>
                            <td class="datagriditemstyleweighting">
                                <ew:NumericBox runat="server" ID="txtWeight" PositiveNumber="True" CssClass="textbox" />
                            </td>
                            <% If MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then%>
                            <td class="datagriditemstyleweighting">
                                <ew:NumericBox runat="server" ID="txtCreditsPerService" PositiveNumber="True" CssClass="textbox"/>
                            </td>
                            <% end if %>
                            <td class="datagriditemstyleweighting">
                                <asp:DropDownList ID="ddlGrdPolicy" CssClass="dropdownlistff" runat="server" /></td>
                            <td class="datagriditemstyleweighting">
                                <ew:NumericBox runat="server" ID="txtParameter" DecimalPlaces="0" PositiveNumber="True" /></td>
                            <% If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Then%> 
                            <td class="datagriditemstyleweighting">
                                <asp:CheckBox ID="chkRequired" CssClass="checkbox" runat="server" />
                             </td>
                             <td class="datagriditemstyleweighting">
                                <asp:CheckBox ID="chkMustPass" CssClass="checkbox" runat="server" />
                             </td>
                            <% end if %>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</div>
