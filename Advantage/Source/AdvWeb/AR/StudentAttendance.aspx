﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentAttendance.aspx.vb" Inherits="StudentAttendance" %>


<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

    <style>
        .AdvantageMainBody {
            overflow-x: auto !important
        }
    </style>
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script src="../js/AuditHist.js" type="text/javascript"></script>

    <script type="text/javascript">
        //    function SetHiddenText() {
        //        document.Content2_forms[0].HiddenText.value = "YES";

        //    }
        // window.print();

        function preventBackspace(e) {
            var evt = e || window.event;
            if (evt) {
                var keyCode = evt.charCode || evt.keyCode;
                if (keyCode === 8) {
                    if (evt.preventDefault) {
                        evt.preventDefault();
                    }
                    else {
                        evt.returnValue = false;
                    }
                }
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <asp:Panel runat="server" ID="MainPanel">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" id="Table1" style="border: none !important;">
            <!-- begin rightcolumn -->
            <tr>
                <td>
                    <table width="99%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->
                    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                        align="center">
                        <tr>
                            <td>
                                <div>
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" border="0" width="500" align="center">
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblEnrollmentId" CssClass="Label" runat="server">Enrollment</asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell">
                                                    <asp:DropDownList ID="ddlStuEnrollId" Width="325px" runat="server" CssClass="dropdownlist" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtPrgVerTrackAttendance" runat="Server" Visible="false"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="emptycell" colspan="2"></td>
                                            </tr>

                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblTerms" runat="server" CssClass="Label">Term</asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell">
                                                    <asp:DropDownList ID="ddlTermId" Width="325px" runat="server" CssClass="dropdownlist" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="emptycell" colspan="2"></td>
                                            </tr>

                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblClsSections" runat="server" CssClass="Label">Class Section</asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell">
                                                    <asp:DropDownList ID="ddlClsSectionId" Width="325px" runat="server" CssClass="dropdownlist">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="twocolumnlabelcell">&nbsp;</td>
                                                <td class="twocolumncontentcell">
                                                    <asp:Button ID="btnBuildList" runat="server" Text="Show Attendance"></asp:Button></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="boxContainer noBorder">
                                        <table class="contenttable" cellspacing="5" border="0" style="width: 100%;" runat="server"
                                            id="tblOverallTotals" visible="false">
                                            <tr>
                                                <td colspan="6" style="text-align: left;">
                                                    <asp:Label ID="lblMessage" runat="server" CssClass="Label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top">
                                                <td class="Label" align="left" valign="middle" style="width: 20%;">Actual Totals
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 20%;">Present:
                                                      <asp:Label ID="LblActualPresent" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">Absent:
                                                      <asp:Label ID="LblActualAbsent" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">
                                                    <asp:Label ID="lblScheduledORTardy" runat="server"></asp:Label>
                                                    <asp:Label ID="LblScheduled" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">
                                                    <asp:Label ID="lblMakeUpORTardy" runat="server"></asp:Label>
                                                    <asp:Label ID="lblMakeUp" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">
                                                    <asp:Label ID="lblActualExcused" runat="server" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label" align="left" valign="middle" style="width: 20%;">Adjusted Totals
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 20%;">Present:
                                                      <asp:Label ID="LblAdjPresent" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">Absent:
                                                      <asp:Label ID="LblAdjAbsent" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">
                                                    <asp:Label ID="lblAdjScheduledORTardy" runat="server"></asp:Label>
                                                    <asp:Label ID="LblAdjScheduled" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">
                                                    <asp:Label ID="lblAdjMakeUpORTardy" runat="server"></asp:Label>
                                                    <asp:Label ID="lblAdjMakeUp" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">
                                                    <asp:Label ID="lblAdjExcused" runat="server" Visible="false"></asp:Label>
                                                </td>
                                            </tr>

                                            <tr runat="server" id="rowConverted" visible="false">
                                                <td class="Label" align="left" valign="middle" style="width: 20%;">Converted Totals
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 20%;">Present:
                                                      <asp:Label ID="LblConvPresent" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">Absent:
                                                      <asp:Label ID="LblConvAbsent" runat="server"></asp:Label>
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">Scheduled:
                                                    <asp:Label ID="LblConvScheduled" runat="server"></asp:Label>

                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">&nbsp;
                                                </td>
                                                <td class="Label" align="left" valign="middle" style="width: 15%;">&nbsp;
                                                </td>
                                            </tr>



                                            <tr>
                                                <td class="Label" align="left" valign="middle" colspan="6">* Indicates an adjustment:
                                                      <asp:Label ID="lblTardy" runat="server"></asp:Label>
                                                    Tardies make an Absence
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label" align="left" valign="middle" colspan="6">Percentage of Present:
                                                      <asp:Label ID="lblOverallAttendance" runat="server"></asp:Label>%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; padding-top: 12px" class="Label" colspan="6">
                                                    <strong><font size='2'><asp:label id="lblAttendance" runat="server" text="Post Attendance"></asp:label></font></strong>
                                                </td>
                                            </tr>
                                            <%--  <tr>
                                                <td class="spacertables" colspan="6"></td>
                                            </tr>          --%>
                                        </table>

                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="pnlAttendance" CssClass="Label" runat="server" Width="100%">
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="HiddenText" Style="display: none" runat="server" CssClass="Label"></asp:TextBox>
                                                    <asp:CheckBox ID="chkFutureStart" Style="display: none" runat="server" CssClass="CheckBox"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    <table id="tblLegend" runat="server" visible="false">
                                                        <tr align="center">
                                                            <td style="text-align: right" class="LegendLabel">Legend :
                                                            </td>
                                                            <td style="height: 5px; vertical-align: middle; background-color: White; border: solid 1px black; width: 90px">
                                                                <span class="LegendLabel">Entry</span>
                                                            </td>
                                                            <td style="height: 5px; vertical-align: middle; background-color: Aquamarine; border: solid 1px black; width: 90px">
                                                                <span class="LegendLabel">Scheduled</span>
                                                            </td>
                                                            <td style="height: 5px; vertical-align: middle; background-color: LightGrey; border: solid 1px black; width: 90px">
                                                                <span class="LegendLabel">Disabled</span>
                                                            </td>
                                                            <td style="height: 5px; vertical-align: middle; background-color: LightSalmon; border: solid 1px black; width: 90px">
                                                                <span class="LegendLabel">Makeup</span>
                                                            </td>
                                                            <td style="height: 5px; vertical-align: middle; background-color: LightBlue; border: solid 1px black; width: 90px">
                                                                <span class="LegendLabel">Absent</span>
                                                            </td>
                                                            <td style="height: 5px; vertical-align: middle; background-color: BlanchedAlmond; border: solid 1px black; width: 90px">
                                                                <span class="LegendLabel">Tardy</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </td>
                        </tr>

                    </table>
                </td>
                <!-- end rightcolumn -->
            </tr>
        </table>
    </asp:Panel>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
        ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
        ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

