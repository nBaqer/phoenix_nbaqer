﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="TermProgress.aspx.vb" Inherits="TermProgress" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td nowrap="nowrap" align="left" width="15%">
                                    <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                </td>
                                <td nowrap="nowrap" width="85%" align="left" style="padding-right: 175px;">
                                    <asp:RadioButtonList ID="radStatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true"
                                        CssClass="label">
                                        <asp:ListItem Text="Active" Selected="True" />
                                        <asp:ListItem Text="Inactive" />
                                        <asp:ListItem Text="All" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfilters">
                            <asp:DataList ID="dlstStdProgress" runat="server" DataKeyField="StuEnrollId" Width="100%">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                        Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'
                                        CausesValidation="False"></asp:ImageButton>
                                    <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'
                                        CausesValidation="False"></asp:ImageButton>
                                    <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                    <asp:LinkButton Text='<%# Container.DataItem("PrgVerDescrip")%>' runat="server" CssClass="nonselecteditem"
                                        CommandArgument='<%# Container.DataItem("StuEnrollId")%>' ID="Linkbutton1" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                        <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="scrollright2">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlRHS" runat="server">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td>
                                            <telerik:RadGrid ID="RadUnschedule" runat="server"
                                                BorderWidth="1px"
                                                BorderColor="#E0E0E0"
                                                Width="100%" Visible="true"
                                                GridLines="Both"
                                                CellPadding="3"
                                                ShowStatusBar="True" PageSize="10"
                                                AllowPaging="True"
                                                AllowSorting="True"
                                                AllowMultiRowEdit="false"
                                                AutoGenerateColumns="False"
                                                AllowAutomaticInserts="False"
                                                AllowAutomaticUpdates="True"
                                                AllowAutomaticDeletes="False">
                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                <PagerStyle Mode="NumericPages" AlwaysVisible="False" />
                                                <MasterTableView Width="100%" CommandItemDisplay="None"
                                                    HorizontalAlign="NotSet"
                                                    EditMode="InPlace"
                                                    AutoGenerateColumns="False"
                                                    AllowAutomaticInserts="False"
                                                    DataKeyNames="TermId"
                                                    BorderWidth="0px"
                                                    BorderColor="transparent"
                                                    NoMasterRecordsText="No Increment Periods to display">
                                                    <Columns>
                                                        <telerik:GridTemplateColumn UniqueName="TermId"
                                                            ReadOnly="True"
                                                            Display="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTermId" runat="server"
                                                                    Text='<%# Eval("TermId")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblTermId" runat="server"
                                                                    Text='<%# Eval("TermId")%>'></asp:Label>
                                                            </EditItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="Term"
                                                            ReadOnly="true"
                                                            SortExpression="Term"
                                                            AllowFiltering="false"
                                                            ItemStyle-Font-Size="11px"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <HeaderStyle Width="25%" />
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblTerm" runat="server"
                                                                    Text="Term"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTerm" runat="server"
                                                                    Text='<%# Eval("TermDescrip")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>

                                                        <telerik:GridTemplateColumn UniqueName="DescripXTranscript"
                                                            AllowFiltering="false"
                                                            ItemStyle-Font-Size="11px"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <HeaderStyle Width="23%" />
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblDescripXTranscript" runat="server"
                                                                    Text="Term Description for Transcript"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDescripXTranscript" runat="server"
                                                                    Text='<%# Eval("DescripXTranscript")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtDescripXTranscript" runat="server" Width="95%"
                                                                    Text='<%# Eval("DescripXTranscript")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridEditCommandColumn  UniqueName="EditCommandColumn"
                                                            HeaderStyle-Width="7%">
                                                            <HeaderStyle Width="7%" />
                                                            <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" Width="7%" />
                                                        </telerik:GridEditCommandColumn>
                                                        <telerik:GridTemplateColumn UniqueName="CreditsAttempted"
                                                            AllowFiltering="false"
                                                            HeaderStyle-Width="10%"
                                                            ItemStyle-Font-Size="11px"
                                                            ItemStyle-HorizontalAlign="right">
                                                            <HeaderStyle Width="10%" />
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblHeaderCreditsAttempted" runat="server"
                                                                    Text="Credits Attempted"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="CreditsAttempted" runat="server"
                                                                    Text='<%# Eval("CreditsAttempted")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="CreditsEarned"
                                                            AllowFiltering="false"
                                                            HeaderStyle-Width="10%"
                                                            ItemStyle-Font-Size="11px"
                                                            ItemStyle-HorizontalAlign="right">
                                                            <HeaderStyle Width="10%" />
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblHeaderCreditsEarned" runat="server"
                                                                    Text="Credits Earned"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="CreditsEarned" runat="server"
                                                                    Text='<%# Eval("CreditsEarned")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="TermGPA"
                                                            AllowFiltering="false"
                                                            HeaderStyle-Width="10%"
                                                            ItemStyle-Font-Size="11px"
                                                            ItemStyle-HorizontalAlign="right">
                                                            <HeaderStyle Width="10%" />
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblHeaderTermGPA" runat="server"
                                                                    Text="Current GPA"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="TermGPA" runat="server"
                                                                    Text='<%# Eval("TermGPA")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="CumGPA"
                                                            AllowFiltering="false"
                                                            HeaderStyle-Width="10%"
                                                            ItemStyle-Font-Size="11px"
                                                            ItemStyle-HorizontalAlign="right">
                                                            <HeaderStyle Width="10%" />
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblHeaderCumGPA" runat="server"
                                                                    Text="Cumulative GPA"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="CumGPA" runat="server"
                                                                    Text='<%# Eval("GPA")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>

                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>

                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:TextBox ID="txtStudentId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

