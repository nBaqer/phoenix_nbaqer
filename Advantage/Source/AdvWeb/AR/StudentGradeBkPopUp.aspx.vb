Imports FAME.common
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data

Partial Class StudentGradeBkPopUp
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dtlClsSectStds As System.Web.UI.WebControls.DataGrid
    Private requestContext As HttpContext
    Protected WithEvents grdBkGradeScaleDetailsTable As System.Data.DataTable
    Protected WithEvents grdBkGradeSystemDetailsTable As System.Data.DataTable
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim StdEnrollId As String = ""
        Dim ClsSectId As String = ""
        Dim StudentId As String = ""

        Try
            'StuEnrollId
            If Request.QueryString("StuEnrollId") <> "" Then
                StdEnrollId = Request.QueryString("StuEnrollId")
                ViewState("StuEnrollId") = StdEnrollId
            End If
            'ClsSectionId
            If Request.QueryString("TestId") <> "" Then
                If Left(Request.QueryString("TestId"), 6) <> "000000" Then
                    ClsSectId = Request.QueryString("TestId")
                    ViewState("ClsSection") = ClsSectId
                Else
                    DisplayErrorMessage("This is a transfer grade. Hence, there is no grade book available to display.")
                    Exit Sub
                End If
            Else
                DisplayErrorMessage("This is a transfer grade. Hence, there is no grade book available to display.")
                Exit Sub
            End If
            'StudentId
            If Request.QueryString("StudentId") <> "" Then
                StudentId = Request.QueryString("StudentId")
            End If

            DisplayContent(ClsSectId, StudentId, StdEnrollId)

            If Not Page.IsPostBack Then
                ChkIsInDB.Checked = True
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '   Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub DisplayContent(ByVal ClsSectionId As String, ByVal StudentId As String, ByVal StuEnrollId As String)
        Dim facade As New StdGrdBkFacade
        Dim CommonUtilities As New CommonUtilities
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim strFirstName, strLastName, strInstructor, FullName As String

        Dim GrdBkInfo() As String = Split(facade.GetStdGrdBkPopupInfo(ClsSectionId, StudentId, ViewState("StuEnrollId")), ";")
        strInstructor = GrdBkInfo(0)
        ViewState("CourseName") = GrdBkInfo(1)
        ViewState("Term") = GrdBkInfo(2)
        strFirstName = GrdBkInfo(3)
        strLastName = GrdBkInfo(4)
        chkIsIncomplete.Checked = GrdBkInfo(6)

        FullName = strLastName & ", " & strFirstName
        PopulateHeader(FullName)

        'Dim gbwLevel As String = SingletonAppSettings.AppSettings("GradeBookWeightingLevel")
        Dim gbwLevel As String = facade.GetGradeBookWeightingLevel(ClsSectionId, ViewState("StuEnrollId"))
        If gbwLevel.ToLower = "instructorlevel" Then
            'Instructor Level
            If strInstructor = "" Then
                DisplayErrorMessage("This class section has no instructor set up. Hence, the grades cannot be viewed.")
                Exit Sub
            End If

            ds = facade.PopulateDataGridOnItemCmd(StuEnrollId, ClsSectionId, FullName, strInstructor, Session("UserName"))

            dt = ds.Tables("GrdBkCriteria")

            With dgdStdGrdBk
                .DataSource = dt
                .DataBind()
            End With

            ViewState("Instructor") = strInstructor
            Session("GrdBkCriteria") = ds

            DisplayScoreGrade()

            pnlInstrGBW.Visible = True

        Else

            'Course Level
            Try
                Dim maxRow As Integer = MyAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings")
                ViewState("MaxRows") = maxRow
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                DisplayErrorMessage("A valid numeric value for the configuration variable 'MaxNoOfGradeBookWeightings' is missing. Hence, the grades cannot be viewed.")
                Exit Sub
            End Try

            ds = (New StdGrdBkFacade).GetGrdBkWgtDetailsAndResultsDS(ViewState("ClsSection"), ViewState("StuEnrollId"))

            If ds.Tables.Count > 0 Then

                If ds.Tables("GrdComponentTypesDT").Rows.Count = 0 Then
                    DisplayErrorMessage("No Grade Book Weightings were found based on the start date of " & ViewState("CourseName") & ".")
                    Exit Sub

                Else
                    Session("GBWDetailsAndResultsDS") = ds
                    'Build grid
                    BuildGBWResultsGrid()

                    'Populate results
                    PopulateGBWResults()

                    pnlCourseGBW.Visible = True
                End If
            Else
                DisplayErrorMessage("No Grade Book Weightings were found based on the start date of " & ViewState("CourseName") & ".")
            End If
        End If
    End Sub
    Private Sub DisplayScoreGrade()
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable
        Dim totalscore As Decimal
        Dim facade As New StdGrdBkFacade
        Dim grdsclId As String
        Dim weightsum As Decimal
        Dim gradeRounding As String = MyAdvAppSettings.AppSettings("GradeRounding").ToString.ToLower()
        Label1.Visible = True
        Label2.Visible = True
        txtFinalScore.Visible = True
        txtFinalGrade.Visible = True
        Dim scoreToGrade As Decimal

        ''Get Grade Scale and Grade System details that are attached to this class section
        'ds = facade.GetGrdScaleSystemDetails(ViewState("ClsSection"), ViewState("Instructor"), ViewState("Term"))

        'grdBkGradeScaleDetailsTable = ds.Tables("GradeScaleDetails")
        'grdBkGradeSystemDetailsTable = ds.Tables("GradeSystemDetails")

        ds1 = Session("GrdBkCriteria")

        dt1 = ds1.Tables("GrdBkCriteria")

        Dim row As DataRow
        For Each row In dt1.Rows
            If Not (row.IsNull("Score")) Then
                weightsum += row("Weight")
                totalscore += (CType(row("Score"), Decimal) * (CType(row("Weight"), Decimal) / 100))
            End If
        Next

        '   add score
        If weightsum <> 0 Then
            If gradeRounding = "yes" Then
                txtFinalScore.Text = Math.Round((totalscore / weightsum) * 100) 'Math.Round((totalscore / weightsum) * 100, 2)
                scoreToGrade = Math.Round((totalscore / weightsum) * 100)
            Else
                txtFinalScore.Text = Math.Round((totalscore / weightsum) * 100, 2)
                'scoreToGrade = Math.Floor(Math.Round((totalscore / weightsum) * 100))
                scoreToGrade = Math.Floor(Math.Round((totalscore / weightsum) * 100, 2))
            End If
        Else
            txtFinalScore.Text = ""
        End If

        '     get gradescale id
        If txtFinalScore.Text <> "" Then
            grdsclId = facade.GetGrdScaleId(ViewState("ClsSection"))
            txtFinalGrade.Text = ConvertScoreToGrade(scoreToGrade, grdsclId)
        Else
            txtFinalGrade.Text = ""
        End If
    End Sub
    Private Function ConvertScoreToGrade(ByVal score As Decimal, ByVal GrdScaleId As String) As String
        'Dim gbwLevel As String = SingletonAppSettings.AppSettings("GradeBookWeightingLevel")
        Dim grade As String = ""

        'If gbwLevel.ToLower = "instructorlevel" Then
        '    'Instructor Level GBW
        '    '   filter GradeScaleDetails table by GrdScaleId
        '    Dim rows() As DataRow = grdBkGradeScaleDetailsTable.Select("GrdScaleId=" + "'" + GrdScaleId + "'")

        '    '   loop only if you have rows
        '    Dim i As Integer
        '    If rows.Length > 0 Then
        '        '   loop throughout all the GradeScale
        '        For i = 0 To rows.Length - 1
        '            '   check if the score is between MinVal and MaxVal values
        '            If score >= CType(rows(i)("MinVal"), Decimal) And score <= CType(rows(i)("MaxVal"), Decimal) Then
        '                '   return corresponding Grade
        '                grade = rows(i).GetParentRow("GradeSystemDetailsGradeScaleDetails")("Grade")
        '            End If
        '        Next
        '    End If

        'Else
        '    'Course Level GBW
        Dim ds As New DataSet
        Dim dtGrdScaleDetails As DataTable
        Dim dtGrdSystemDetails As DataTable

        'Get Grade Scale and Grade System details that are attached to this class section
        ds = (New StdGrdBkFacade).GetGrdScaleSystemDetails(ViewState("ClsSection"), ViewState("Instructor"), ViewState("Term"))

        dtGrdScaleDetails = ds.Tables("GradeScaleDetails")
        dtGrdSystemDetails = ds.Tables("GradeSystemDetails")

        'Filter GradeScaleDetails table by GrdScaleId
        Dim rows() As DataRow = dtGrdScaleDetails.Select("GrdScaleId=" + "'" + GrdScaleId + "'")

        If rows.Length > 0 Then
            'Loop throughout all the GradeScale
            For i As Integer = 0 To rows.Length - 1
                'Check if the score is between MinVal and MaxVal values
                If score >= CType(rows(i)("MinVal"), Decimal) And score <= CType(rows(i)("MaxVal"), Decimal) Then
                    grade = rows(i).GetParentRow("GradeSystemDetailsGradeScaleDetails")("Grade")
                    Exit For
                End If
            Next
        End If
        'End If

        Return grade
    End Function
    Private Sub PopulateHeader(ByVal fullname As String)
        'pnlHeader.Visible = True
        lblStdName.Text = fullname
        lblSecName.Text = ViewState("CourseName")
    End Sub

    Private Sub dgdStdGrdBk_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdStdGrdBk.ItemDataBound
        Dim ds As New DataSet
        Dim Incomplete As String

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem

                'Incomplete = CType(iitem.FindControl("txtIncomplete"), TextBox).Text

                Incomplete = CType(e.Item.FindControl("txtIncomplete"), TextBox).Text

                If Incomplete = "True" Then
                    chkIsIncomplete.Checked = True
                End If
        End Select
    End Sub

    Private Sub BuildGBWResultsGrid()
        Dim ds As DataSet = DirectCast(Session("GBWDetailsAndResultsDS"), DataSet)
        Dim dtCompTypes As DataTable = ds.Tables("GrdComponentTypesDT")

        If dtCompTypes.Rows.Count > 0 Then

            pnlCourseGBW.Controls.Clear()

            Dim cols As Integer = dtCompTypes.Rows.Count
            Dim tbl As New Table
            Dim cellWidth As Integer = 100 / (cols + 1)
            tbl.ID = "tbl" & ViewState("StuEnrollId")
            tbl.Width = Unit.Percentage(100)
            tbl.CellPadding = "0"
            tbl.CellSpacing = "0"
            tbl.CssClass = "gradebooktable"

            'Header Row 0
            'Cell(0,0)
            Dim cell As New TableCell
            Dim lbl As New Label
            Dim row As New TableRow
            lbl.CssClass = "Label"
            lbl.Text = "<strong>Num</strong>"
            cell.VerticalAlign = VerticalAlign.Bottom
            cell.HorizontalAlign = HorizontalAlign.Center
            cell.CssClass = "DataGridHeader10"
            cell.Controls.Add(lbl)
            row.Cells.Add(cell)

            For Each dr As DataRow In dtCompTypes.Rows
                cell = New TableCell
                lbl = New Label
                lbl.ID = dr("InstrGrdBkWgtDetailId").ToString
                lbl.CssClass = "Label"
                lbl.Text = "<strong>" & dr("Code") & "</strong>"
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Center
                cell.CssClass = "DataGridHeader10"
                cell.Controls.Add(lbl)
                row.Cells.Add(cell)
            Next

            'Add Header Row to table
            tbl.Rows.Add(row)
            '
            '
            Dim txt As TextBox
            '
            'Data Rows
            For i As Integer = 1 To ViewState("MaxRows")
                row = New TableRow
                cell = New TableCell
                lbl = New Label
                lbl.ID = "Num" & i
                lbl.CssClass = "Label"
                lbl.Text = i
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Center
                cell.Controls.Add(lbl)
                cell.CssClass = "DataGridItemStyle"
                row.Cells.Add(cell)

                For j As Integer = 0 To cols - 1

                    cell = New TableCell

                    If dtCompTypes.Rows(j).IsNull("MinNumber") Then
                        'Add Score textbox
                        txt = New TextBox
                        txt.ID = "txt" & i & "_" & dtCompTypes.Rows(j)("InstrGrdBkWgtDetailId").ToString
                        txt.CssClass = "textbox"
                        txt.ReadOnly = True
                        cell.Controls.Add(txt)
                        cell.CssClass = "DataGridItemStyle"
                    Else
                        If i <= dtCompTypes.Rows(j)("MinNumber") Then
                            'Add Score textbox
                            txt = New TextBox
                            txt.ID = "txt" & i & "_" & dtCompTypes.Rows(j)("InstrGrdBkWgtDetailId").ToString
                            txt.CssClass = "textbox"
                            txt.ReadOnly = True
                            cell.Controls.Add(txt)
                            cell.CssClass = "DataGridItemStyle"
                        Else
                            lbl = New Label
                            lbl.CssClass = "label"
                            cell.Controls.Add(lbl)
                            cell.CssClass = "DataGridItemStyle"
                        End If
                    End If

                    cell.VerticalAlign = VerticalAlign.Bottom
                    cell.HorizontalAlign = HorizontalAlign.Center
                    row.Cells.Add(cell)
                Next

                tbl.Rows.Add(row)
            Next
            '
            '
            '
            'Weight Row
            cell = New TableCell
            lbl = New Label
            row = New TableRow
            lbl.CssClass = "Label"
            lbl.Text = "<strong>Grade Book Weight</strong>"
            cell.VerticalAlign = VerticalAlign.Bottom
            cell.HorizontalAlign = HorizontalAlign.Left
            cell.Controls.Add(lbl)
            cell.CssClass = "DataGridItemStyle"
            row.Cells.Add(cell)

            For Each dr As DataRow In dtCompTypes.Rows
                cell = New TableCell
                lbl = New Label
                lbl.ID = "lblWgt" & dr("InstrGrdBkWgtDetailId").ToString
                lbl.Text = dr("Weight")
                lbl.CssClass = "Label"
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Left
                cell.Controls.Add(lbl)
                cell.CssClass = "DataGridItemStyle"
                row.Cells.Add(cell)
            Next

            'Add Weight Row to table
            tbl.Rows.Add(row)
            '
            '
            '
            'Actual Weight Row
            cell = New TableCell
            lbl = New Label
            row = New TableRow
            lbl.CssClass = "Label"
            lbl.Text = "<strong>Actual Weight</strong>"
            cell.VerticalAlign = VerticalAlign.Bottom
            cell.HorizontalAlign = HorizontalAlign.Left
            cell.Controls.Add(lbl)
            cell.CssClass = "DataGridItemStyle"
            row.Cells.Add(cell)

            For Each dr As DataRow In dtCompTypes.Rows
                cell = New TableCell
                lbl = New Label
                lbl.ID = "lblWeight" & dr("InstrGrdBkWgtDetailId").ToString
                lbl.CssClass = "Label"
                cell.VerticalAlign = VerticalAlign.Bottom
                cell.HorizontalAlign = HorizontalAlign.Left
                cell.Controls.Add(lbl)
                cell.CssClass = "DataGridItemStyle"
                row.Cells.Add(cell)
            Next

            'Add Score Row to table
            tbl.Rows.Add(row)

            pnlCourseGBW.Controls.Add(tbl)
        End If
    End Sub

    Private Sub PopulateGBWResults()
        Dim ds As DataSet = DirectCast(Session("GBWDetailsAndResultsDS"), DataSet)
        Dim dtCompTypes As DataTable = ds.Tables("GrdComponentTypesDT")
        Dim dtGBWResults As DataTable = ds.Tables("StuGrdBkResultsDT")
        Dim ctrl As Control
        Dim tbl As Table
        Dim txt As TextBox
        Dim lbl As Label
        Dim cols As Integer = dtCompTypes.Rows.Count
        Dim maxRows As Integer = ViewState("MaxRows")
        Dim rowCounter As Integer
        Dim arrRows() As DataRow
        Dim rows() As DataRow
        Dim dr As DataRow
        Dim accumWgt As Decimal = 0
        Dim ttlWgt As Decimal = 0
        Dim gbwUtil As GrdBkWgtUtility
        Dim boolGradePosted As Boolean = False
        Dim intGradePostedCount As Integer = 0

        ctrl = pnlCourseGBW.FindControl("tbl" & ViewState("StuEnrollId").ToString)
        tbl = DirectCast(ctrl, Table)

        If Not (tbl Is Nothing) Then
            '
            'Populate score textboxes from the dtGBWResults table
            For Each dr In dtGBWResults.Rows

                ctrl = pnlCourseGBW.FindControl("txt" & dr("ResNum").ToString & "_" & dr("InstrGrdBkWgtDetailId").ToString)
                If Not (ctrl Is Nothing) Then
                    txt = DirectCast(ctrl, TextBox)
                    Try
                        txt.Text = If(dr("Score").ToString = "-1.00", "T", dr("Score").ToString)
                        intGradePostedCount += 1
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        txt.Text = ""
                    End Try
                End If

            Next
            If intGradePostedCount >= 1 Then
                boolGradePosted = True
            End If
            '
            '
            '
            'Loop thru the table header to read the Grading Component Types.  
            'Compute, retrieve and populate the Score and Weight labels per Grading Component Type
            rowCounter = 0
            For cellCounter As Integer = 1 To cols
                lbl = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), Label)

                'Use the dtCompTypes table to get the Grading Policy 
                rows = dtCompTypes.Select("InstrGrdBkWgtDetailId='" & lbl.ID & "'")

                If rows.GetLength(0) > 0 Then
                    '
                    'Get dtCompTypes's first row 
                    dr = rows(0)
                    '
                    'Get results for Grading Component Type
                    arrRows = dtGBWResults.Select("InstrGrdBkWgtDetailId='" & lbl.ID & "'")
                    '
                    If arrRows.GetLength(0) > 0 Then

                        'Compute score using Strategy pattern
                        If Not (dr.IsNull("GrdPolicyId")) Then
                            gbwUtil = New GrdBkWgtUtility(arrRows, dr("Weight"), dr("GrdPolicyId"), dr("Param"))
                        Else
                            gbwUtil = New GrdBkWgtUtility(arrRows, dr("Weight"))
                        End If
                        'Use gbwUtil.Weight property
                        DirectCast(tbl.Rows(ViewState("MaxRows") + 2).Cells(cellCounter).Controls(0), Label).Text = gbwUtil.Weight
                        accumWgt += gbwUtil.Weight
                        ''Modified by saraswathi lakshmanan to fix issue 15467
                        ''The current score is calculated based on the number of grade books entered.
                        ''if a grade is posted, then Actualvalue/ gradebook weight,
                        ''Changed on march 04 2009
                        'modified by Theresa G on sep 20 2010
                        '19745: The grade on the transcript tab shows different from what it posts on grade book page. 
                        If arrRows(0)("Score").ToString = "-1.00" Then
                            ttlWgt += 0
                        ElseIf gbwUtil.Weight = 0 And (arrRows(0)("Score").ToString = "" Or arrRows(0)("Score").ToString = "-1.00") Then
                            ttlWgt += 0
                        Else
                            ttlWgt += dr("Weight")
                        End If
                    End If
                    '
                End If
                '
            Next
            '
        End If

        'Dim boolIsScorePosted As Boolean
        'boolIsScorePosted = (New StdGrdBkFacade).boolIsScorePosted(Request.Params("testid"), ViewState("StuEnrollId").ToString)

        'If score is posted for any one of the components go to displayscoregrade function
        If boolGradePosted = True Then
            DisplayScoreGrade(ttlWgt, accumWgt)
        Else
            txtFinalScore.Visible = True
            txtFinalGrade.Visible = True
            Label1.Visible = True
            Label2.Visible = True
            txtFinalScore.Text = ""
            txtFinalGrade.Text = ""
        End If

        'DisplayScoreGrade(ttlWgt, accumWgt)
    End Sub
    Private Sub DisplayScoreGrade(ByVal ttlWgt As Decimal, ByVal accumWgt As Decimal)
        Label1.Visible = True
        Label2.Visible = True
        txtFinalScore.Visible = True
        txtFinalGrade.Visible = True

        If ttlWgt > 0 Then
            Dim score As Decimal = accumWgt / ttlWgt * 100
            Dim scoreToGrade As Decimal
            'Code commented by balaji for mantis #11119 to get rid of decimals
            'txtFinalScore.Text = System.Math.Round(score, 2)
            'modified by Theresa G on sep 20 2010
            '19745: The grade on the transcript tab shows different from what it posts on grade book page. 


            If MyAdvAppSettings.AppSettings("GradeRounding").ToString.ToLower = "yes" Then
                txtFinalScore.Text = System.Math.Round(score, 0)
                scoreToGrade = System.Math.Round(score, 0)
            Else
                'txtFinalScore.Text = System.Math.Floor(score)
                scoreToGrade = System.Math.Floor(score)
                txtFinalScore.Text = Math.Round(score, 2)
                'scoreToGrade = Math.Floor(Math.Round(score, 2))
            End If

            'Round off score before converting to grade. (for example: round off 89.75 to 90)


            'Get Class Section's GrdScaleId
            Dim ds As DataSet = DirectCast(Session("GBWDetailsAndResultsDS"), DataSet)
            Dim dtCompTypes As DataTable = ds.Tables("GrdComponentTypesDT")

            If dtCompTypes.Rows.Count > 0 Then

                txtFinalGrade.Text = ConvertScoreToGrade(scoreToGrade, dtCompTypes.Rows(0)("GrdScaleId").ToString)
            Else
                txtFinalGrade.Text = ""
            End If
        Else
            txtFinalScore.Text = ""
            txtFinalGrade.Text = ""
        End If
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
End Class
