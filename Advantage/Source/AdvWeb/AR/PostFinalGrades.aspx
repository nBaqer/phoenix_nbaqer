﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostFinalGrades.aspx.vb" Inherits="AdvWeb.AR.PostFinalGrades" %>

<%@ Import Namespace="AdvWeb.AR" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script type="text/javascript">

        function OldPageResized(sender) {
            $telerik.repaintChildren(sender);
        }

    </script>
    <script src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized(this)">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>

            <table class="maincontenttable" id="Table2">
                <tr>
                    <td class="listframetop">
                        <table class="maincontenttable" align="center">
                            <tr id="trTerm" runat="server">
                                <td style="width: 22%; padding: 3px;">
                                    <asp:Label ID="lblTerm" runat="server" CssClass="label">Term</asp:Label>
                                </td>
                                <td style="width: 22%; padding: 3px;">
                                    <asp:DropDownList ID="ddlTerm" Width="270px" runat="server" AutoPostBack="True" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trCohortStartDate" runat="server">
                                <td>
                                    <asp:Label ID="label3" runat="server" CssClass="label">Cohort Start Date</asp:Label>
                                </td>
                                <td style="width: 22%">
                                    <asp:DropDownList ID="ddlcohortStDate" runat="server" AutoPostBack="True" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 22%; padding: 3px;">
                                    <asp:Label ID="lblInstructor" runat="server" CssClass="label">Instructor</asp:Label>
                                </td>
                                <td style="width: 22%">
                                    <asp:DropDownList ID="ddlInstructor" Width="270px" runat="server" AutoPostBack="True" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 22%; padding: 3px;">
                                    <asp:Label ID="lblClsSection" runat="server" CssClass="label">Class</asp:Label>
                                </td>
                                <td style="padding: 3px;">
                                    <asp:DropDownList ID="ddlClsSection" Width="270px" runat="server" AutoPostBack="False" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right;">
                                    <asp:Button ID="btnBuildList" runat="server" Text="Build List"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfltr4rows">
                            &nbsp;
                        </div>
                    </td>
                </tr>
            </table>




        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table class="maincontenttable">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="false"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" style="width: 98%">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3>
                                <asp:Label ID="headerTitle" runat="server"></asp:Label></h3>
                            <!-- begin content table-->

                            <!-- Adjust width to 95% to keep entire contents of panel on the screen  -->
                            <table class="contenttable" width="100%" align="center">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <asp:Label ID="lblDatecompleted" runat="server">Date Completed field is required when final Grade is posted.</asp:Label>
                                </tr>
                                <tr>
                                    <td class="contenttable" align="right">
                                        <asp:Button ID="btnExportToExcell" runat="server"
                                            Text="Export to Excel" Visible="false"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadAjaxLoadingPanel ID="radAjaxLoader" runat="server"></telerik:RadAjaxLoadingPanel>
                                        <telerik:RadAjaxPanel LoadingPanelID="radAjaxLoader" runat="server">
                                            <asp:DataGrid ID="dgdFinalGrades" runat="server" AutoGenerateColumns="False" Width="100%"
                                                BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                                <EditItemStyle CssClass="label"></EditItemStyle>
                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <FooterStyle CssClass="label"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Student">
                                                        <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                        <ItemStyle Width="33%" CssClass="datalistar"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtStuEnrollId" runat="server" CssClass="ardatalistcontent" Visible="False"
                                                                Text='<%# Container.DataItem("StuEnrollId") %>' />
                                                            <asp:Label ID="Student" runat="server" CssClass="ardatalistcontent" Text='<%# Container.DataItem("LastName") & ", " & Container.DataItem("FirstName") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Status" Visible="True">
                                                        <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                        <ItemStyle Width="20%" CssClass="datalistar"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="StatusId" runat="server" CssClass="ardatalistcontent" Visible="false"
                                                                Text='<%# Container.DataItem("SysStatusId")  %>' />
                                                            <asp:Label ID="Status" runat="server" CssClass="ardatalistcontent" Text='<%# Container.DataItem("StatusCodeDescrip")  %>' Visible="True" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="StudentIdentifier">
                                                        <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                        <ItemStyle Width="15%" CssClass="datalistar"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStudentIdentifier" runat="server" CssClass="ardatalistcontent"
                                                                Text='<%# Container.DataItem("StudentIdentifier") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Score">
                                                        <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                        <ItemStyle CssClass="datalistar" Width="15%"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtScore" runat="server" CssClass="textbox" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Grade">
                                                        <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                        <ItemStyle CssClass="datalistar" Width="15%"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlGrade" runat="server" CssClass="dropdownlist" Width="80px">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="txtGrade" runat="server" Visible="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Date Completed">
                                                        <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                        <ItemStyle CssClass="datalistar" Width="15%"></ItemStyle>
                                                        <ItemTemplate>
                                                            <telerik:RadDatePicker ID="dateCompletedDP" runat="server" Width="200" daynameformat="Short" MinDate="1/1/1900" MaxDate="<%#DateTime.Today%>" AutoPostBack="False" Calendar-ShowRowHeaders="false" SelectedDate='<%#TryParseDateTime(Convert.ToString(Container.DataItem("DateCompleted"))) %>'>
                                                            </telerik:RadDatePicker>
                                                            <%--<asp:RequiredFieldValidator ID="DateCompletedFieldValidator" runat="server"
                                                                ControlToValidate="dateCompletedDP"
                                                                ErrorMessage="Date Completed is a required field."
                                                                ForeColor="Red">
                                                            </asp:RequiredFieldValidator>--%>
                                                            <asp:CustomValidator ID="dateCompletedDpCustomValidator"
                                                                ControlToValidate="dateCompletedDP"
                                                                Display="Static"
                                                                OnServerValidate="ValidateSelectedDate"
                                                                ErrorMessage="Selected Date is not valid."
                                                                ForeColor="Red" ValidateEmptyText="True"
                                                                runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Start Date" Visible="False">
                                                        <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                        <ItemStyle CssClass="datalistar" Width="15%"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStartDate" runat="server" CssClass="ardatalistcontent"
                                                                Text='<%# Container.DataItem("StartDate") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Graduated Date" Visible="False">
                                                        <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                        <ItemStyle CssClass="datalistar" Width="15%"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGraduatedDate" runat="server" CssClass="ardatalistcontent"
                                                                Text='<%# Container.DataItem("GraduatedDate") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </telerik:RadAjaxPanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contenttable" align="left">
                                        <%-- <table id="tblLegend" runat="server" width="41%" style="text-align: right" visible="false">
                            <tr>
                                <td style="background-color: #C0C0C0; width: 10%; text-align: right">
                                </td>
                                <td style="width: 90%; text-align: left">
                                    <asp:Label ID="lblLegend" Text="Out of school status students" runat="Server" CssClass="labelbold"></asp:Label>
                                </td>
                            </tr>
                        </table>--%>
                                    </td>
                                </tr>
                                <asp:TextBox ID="txtClsSectionId1" runat="server" CssClass="label" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

