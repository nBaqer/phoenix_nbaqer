Imports FAME.Common
Imports System.Xml
'Imports eWorld.UI
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common
Imports System.Text


Partial Class SAPPolicyDetails
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator5 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Customvalidator4 As System.Web.UI.WebControls.CustomValidator
    Protected MyAdvAppSettings As AdvAppSettings


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Private validatetxt As Boolean = False
    Dim campusId As String
    Dim insTypeCount As Integer = 0
    Dim dtInstructionTypes As DataTable
    Dim isTitleIVSap = False
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        '        Dim sStatusId As String
        Dim facade As New SAPFacade

        Dim userId As String
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim m_Context As HttpContext
        Dim facSAP As New SAPFacade

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'If Request.QueryString("CampusId") <> "" Then
        '    ViewState("CampusId") = Request.QueryString("CampusId")
        '    campusId = ViewState("CampusId")
        'End If
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        'If SingletonAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then

        'End If
        PopulatepnlInstructionTypeAttendance()
        If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then

            lblAttendance.Visible = True
            radAttendance.Visible = True
            pnlAttendance.Visible = True
        Else
            lblAttendance.Visible = False
            radAttendance.Visible = False
            pnlAttendance.Visible = False
        End If

        m_Context = HttpContext.Current
        txtResourceId.Text = CInt(m_Context.Items("ResourceId"))

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
            RadPanelBar1.Visible = True
            CType(pnlRHS.FindControl("lblAccuracy"), Label).Visible = True
            CType(pnlRHS.FindControl("txtAccuracy"), eWorld.UI.NumericBox).Visible = True
            pnlpercent.Visible = False
        Else
            RadPanelBar1.Visible = False
            CType(pnlRHS.FindControl("lblAccuracy"), Label).Visible = False
            CType(pnlRHS.FindControl("txtAccuracy"), eWorld.UI.NumericBox).Visible = False
        End If

        If Not Page.IsPostBack Then

            'txtSAPId.Text = "05B1AF69-5398-46CD-A4A6-39DC45CB68E0"

            'Get the SAP Policy Id from the previous page.
            'Response.Write(Request.QueryString("SAPPolicy"))
            'PopulatepnlInstructionTypeAttendance()
            Dim a As String = Trim(Request.QueryString("SAPId"))
            If a <> "" Then
                txtSAPId.Text = Trim(Request.QueryString("SAPId"))
            End If
            'Set the text box to a new guid
            txtSAPDetailId.Text = System.Guid.NewGuid.ToString

            ViewState("SAPId") = txtSAPId.Text
            'Commented out by Troy 3/2/2005.If there is a special character such as #
            'in the sap policy name then whatever comes after it will be lost. For example,
            'if the name is "SAP Policy # 1 Test" only "SAP Policy" will be retrieved
            'from the query string.
            'If Trim(Request.QueryString("SAPPolicy")) <> "" Then
            '    txtSAPPolicy.Text = Trim(Request.QueryString("SAPPolicy"))
            'End If
            txtSAPPolicy.Text = facSAP.GetSAPPolicyName(txtSAPId.Text)
            ViewState("SAPPolicy") = txtSAPPolicy.Text

            'Disable the new and delete buttons
            objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"

            objCommon.PageSetup(Form1, "NEW")


            'Bind the Grd Bk Weights Datalist 
            ds = facade.PopulateDataList(txtSAPId.Text)
            Session("DLValues") = ds

            dlstIncrements.SelectedIndex = -1

            With dlstIncrements
                .DataSource = ds
                .DataBind()
            End With
            ds.WriteXml(sw)
            ViewState("ds") = sw.ToString()


            'Populate and bind ListBoxes
            PopulateTrigUnitDDL()
            PopulateTrigOffTypDDL()
            'If we are dealing with a school that uses numeric grading system then
            'we need to use average of as the qualitative min type. Else we can use
            'the regular CGA of.
            If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                PopulateQualTypDDL(2)
            Else
                PopulateQualTypDDL(1)
            End If

            PopulateQuantMinUnitTypDDL()
            PopulateConsequenceDDL()

            If Request("TrigUnit") = 3 And Request("TrigOffset") = 3 Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 3
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                txtMinCredsCompltd.Text = "0"
                txtMinCredsCompltd.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "Credits"
            ElseIf Request("TrigUnit") = 3 And Request("TrigOffset") = 2 Then
                ddlTrigOffsetTyp.SelectedValue = 2
                ddlTrigUnit.SelectedValue = 3
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                txtMinCredsCompltd.Text = "0"
                txtMinCredsCompltd.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "Credits"
            ElseIf Request("TrigUnit") = 1 And Request("TrigOffset") = 2 Then
                ddlTrigOffsetTyp.SelectedValue = 2
                ddlTrigUnit.SelectedValue = 1
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "DaysEndTerm"
            ElseIf Request("TrigUnit") = 1 And Request("TrigOffset") = 3 Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 1
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "DaysSDate"
            ElseIf Request("TrigUnit") = 2 And Request("TrigOffset") = 2 Then
                ddlTrigOffsetTyp.SelectedValue = 2
                ddlTrigUnit.SelectedValue = 2
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "WeeksEndTerm"
            ElseIf Request("TrigUnit") = 2 And Request("TrigOffset") = 3 Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 2
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "WeeksSDate"
            ElseIf Request("TrigUnit") = 4 And Request("TrigOffset") = 3 Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 4
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                ddlQuantMinUnitType.SelectedValue = 3
                ddlQuantMinUnitType.Enabled = False
                txtMinAttendanceValue.Text = 0
                txtMinAttendanceValue.Enabled = False
                txtMinCredsCompltd.Text = 0
                txtMinCredsCompltd.Enabled = False
                pnlAttendance.Visible = True
                ViewState("Type") = "ClockActHoursSDate"

            ElseIf Request("TrigUnit") = 5 And Request("TrigOffset") = 3 Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 5
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                ddlQuantMinUnitType.SelectedValue = 3
                ddlQuantMinUnitType.Enabled = False
                txtMinAttendanceValue.Text = 0
                txtMinAttendanceValue.Enabled = False
                txtMinCredsCompltd.Text = 0
                txtMinCredsCompltd.Enabled = False
                pnlAttendance.Visible = True
                ViewState("Type") = "ClockSchHoursSDate"
            ElseIf Request("TrigUnit") = 6 And (Request("TrigOffset") = 2 Or Request("TrigOffset") = 3) Then
                'ddlTrigOffsetTyp.SelectedValue = 2
                ddlTrigOffsetTyp.SelectedValue = Request("TrigOffset")
                ddlTrigUnit.SelectedValue = 6
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    txtQuantMinValue.Text = 0
                    txtQuantMinValue.Enabled = False
                    ddlQuantMinUnitType.SelectedIndex = 0
                    ddlQuantMinUnitType.Enabled = False
                End If
                txtMinAttendanceValue.Text = 0
                txtMinAttendanceValue.Enabled = True
                txtMinCredsCompltd.Enabled = True
                ViewState("Type") = "MonthSDate"
            End If
            If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                BuildShortHandSkillLevelSection()
                pnlpercent.Visible = False
            End If
        Else
            objCommon.PageSetup(Form1, "EDIT")
            objCommon.SetBtnState(Form1, "EDIT", pObj)
            If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                BuildShortHandSkillLevelSection()
                pnlpercent.Visible = False
            End If
        End If

        'Special case: change datatype of the TrigValue validator
        If pnlRequiredFieldValidators.Controls.Count > 0 Then
            Dim ctrl As Control = pnlRequiredFieldValidators.FindControl("cmvTrigValue1096")
            Dim val As CompareValidator = CType(ctrl, CompareValidator)
            val.Type = ValidationDataType.Double
        End If
        'Special case: change maximum length of txtTrigValue(smallint)
        'Up to 3 digits of whole number, 2 decimal digits and the decimal point
        Me.txtTrigValue.MaxLength = 6


        isTitleIVSap = Boolean.Parse(Request("isTitleIVSap"))

        If (isTitleIVSap) Then
            HideNonTitleIVFields()
        End If

    End Sub

    Private Sub PopulateTrigUnitDDL()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim SAPDetails As New SAPFacade

        ' Bind the Dataset to the CheckBoxList
        ddlTrigUnit.DataSource = SAPDetails.GetTrigUnits()
        ddlTrigUnit.DataTextField = "TrigUnitTypDescrip"
        ddlTrigUnit.DataValueField = "TrigUnitTypId"
        ddlTrigUnit.DataBind()

    End Sub

    Private Sub PopulateTrigOffTypDDL()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim SAPDetails As New SAPFacade

        ' Bind the Dataset to the CheckBoxList
        ddlTrigOffsetTyp.DataSource = SAPDetails.GetTrigOffTyps()
        ddlTrigOffsetTyp.DataTextField = "TrigOffTypDescrip"
        ddlTrigOffsetTyp.DataValueField = "TrigOffsetTypId"
        ddlTrigOffsetTyp.DataBind()

    End Sub

    Private Sub PopulateConsequenceDDL()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim SAPDetails As New SAPFacade

        ' Bind the Dataset to the CheckBoxList
        ddlConsequence.DataTextField = "ConseqTypDesc"
        ddlConsequence.DataValueField = "ConsequenceTypId"
        ddlConsequence.DataSource = SAPDetails.GetConsequences()
        ddlConsequence.DataBind()

    End Sub

    Private Sub PopulateQuantMinUnitTypDDL()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim SAPDetails As New SAPFacade

        ' Bind the Dataset to the CheckBoxList
        ddlQuantMinUnitType.DataTextField = "QuantMinTypDesc"
        ddlQuantMinUnitType.DataValueField = "QuantMinUnitTypId"
        ddlQuantMinUnitType.DataSource = SAPDetails.GetQuantMinUnitTyps()
        ddlQuantMinUnitType.DataBind()

    End Sub

    Private Sub PopulateQualTypDDL(Optional ByVal qualMinType As Integer = 1)

        Dim SAPDetails As New SAPFacade

        ' Bind the Dataset to the CheckBoxList
        ddlQualType.DataTextField = "QualMinTypDesc"
        ddlQualType.DataValueField = "QualMinTypId"
        ddlQualType.DataSource = SAPDetails.GetQualTyps(qualMinType)
        ddlQualType.DataBind()

    End Sub
    Private Sub PopulatepnlInstructionTypeAttendance()

        dtInstructionTypes = (New ClassSectionPeriodsFacade).GetInstructionTypes_SP(True, campusId)
        insTypeCount = dtInstructionTypes.Rows.Count
        Dim tbl As New Table
        Dim drow As TableRow
        Dim c1 As TableCell
        Dim c2 As TableCell
        Dim rnv As New RangeValidator
        If dtInstructionTypes.Rows.Count > 0 Then

            For Each dr As DataRow In dtInstructionTypes.Rows
                drow = New TableRow
                Dim lbl As New Label
                Dim txt As New TextBox
                txt.ID = "txt" & dr("InstructionTypeId").ToString
                c1 = New TableCell
                c1.HorizontalAlign = HorizontalAlign.Left
                c1.VerticalAlign = VerticalAlign.Top
                c1.CssClass = "twocolumnlabelcell"
                lbl.CssClass = "Label"
                lbl.Text = "% of " & dr("InstructionTypeDescrip") & " Hours"
                c1.Controls.Add(lbl)
                drow.Cells.Add(c1)
                c2 = New TableCell
                c2.HorizontalAlign = HorizontalAlign.Left
                c2.VerticalAlign = VerticalAlign.Top
                c2.CssClass = "twocolumnlabelcell"
                txt.CssClass = "TextBoxsap"
                txt.Width = 95

                c2.Controls.Add(txt)
                drow.Cells.Add(c2)


                'Allow only decimal values 




                pnlInstructionTypeAttendance.Controls.Add(drow)
                rnv.ControlToValidate = txt.ID
                rnv.Type = ValidationDataType.Double
                rnv.MinimumValue = 0
                rnv.Display = ValidatorDisplay.None

                rnv.ErrorMessage = "Enter a value greater than 0."
                pnlRequiredFieldValidators.Controls.Add(rnv)
            Next





        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Try
            dlstIncrements.SelectedIndex = -1
            ClearRHS()
            txtSAPPolicy.Text = ViewState("SAPPolicy")
            ds.ReadXml(sr)
            BindDataList(ds)

            'Set the text box to a new guid
            txtSAPDetailId.Text = System.Guid.NewGuid.ToString

            'disable new and delete buttons.
            objcommon.SetBtnState(Form1, "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            txtMinAttendanceValue.Text = "0"

            If ViewState("Type") = "Credits" Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 3
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                txtMinCredsCompltd.Text = "0"
                txtMinCredsCompltd.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "Credits"
            ElseIf ViewState("Type") = "DaysEndTerm" Then
                ddlTrigOffsetTyp.SelectedValue = 2
                ddlTrigUnit.SelectedValue = 1
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "DaysEndTerm"
            ElseIf ViewState("Type") = "DaysSDate" Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 1
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "DaysSDate"
            ElseIf ViewState("Type") = "WeeksEndTerm" Then
                ddlTrigOffsetTyp.SelectedValue = 2
                ddlTrigUnit.SelectedValue = 2
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "WeeksEndTerm"
            ElseIf ViewState("Type") = "WeeksSDate" Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 2
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                'ddlQuantMinUnitType.SelectedValue = 1
                'ddlQuantMinUnitType.Enabled = False
                ViewState("Type") = "WeeksSDate"
            ElseIf ViewState("Type") = "ClockSchHoursSDate" Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 5
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                ddlQuantMinUnitType.SelectedValue = 3
                ddlQuantMinUnitType.Enabled = False
                txtMinAttendanceValue.Text = 0
                txtMinAttendanceValue.Enabled = False
                txtMinCredsCompltd.Text = 0
                txtMinCredsCompltd.Enabled = False
                ViewState("Type") = "ClockSchHoursSDate"
            ElseIf ViewState("Type") = "ClockActHoursSDate" Then
                ddlTrigOffsetTyp.SelectedValue = 3
                ddlTrigUnit.SelectedValue = 4
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                ddlQuantMinUnitType.SelectedValue = 3
                ddlQuantMinUnitType.Enabled = False
                txtMinAttendanceValue.Text = 0
                txtMinAttendanceValue.Enabled = False
                txtMinCredsCompltd.Text = 0
                txtMinCredsCompltd.Enabled = False
                ViewState("Type") = "ClockActHoursSDate"
            ElseIf Request("TrigUnit") = 6 And (Request("TrigOffset") = 2 Or Request("TrigOffset") = 3) Then
                'ddlTrigOffsetTyp.SelectedValue = 2
                ddlTrigOffsetTyp.SelectedValue = Request("TrigOffset")
                ddlTrigUnit.SelectedValue = 6
                ddlTrigUnit.Enabled = False
                ddlTrigOffsetTyp.Enabled = False
                txtTrigOffsetSeq.Enabled = False
                If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    txtQuantMinValue.Text = 0
                    txtQuantMinValue.Enabled = False
                    ddlQuantMinUnitType.SelectedIndex = 0
                    ddlQuantMinUnitType.Enabled = False
                End If
                txtMinAttendanceValue.Text = 0
                txtMinAttendanceValue.Enabled = True
                txtMinCredsCompltd.Enabled = True
                ViewState("Type") = "MonthSDate"
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub


    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim Facade As New SAPFacade
        Dim SAPDetailId As Guid

        Try
            SAPDetailId = XmlConvert.ToGuid(ViewState("SAPDetailId"))
            Dim result As String = Facade.DeleteSAPDetails(SAPDetailId)
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                ClearRHS()
                dlstIncrements.SelectedIndex = -1
                ds = Facade.PopulateDataList(ViewState("SAPId"))
                dlstIncrements.SelectedIndex = -1
                BindDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                txtSAPPolicy.Text = ViewState("SAPPolicy")
                'Set the text box to a new guid
                txtSAPDetailId.Text = System.Guid.NewGuid.ToString

                objCommon.SetBtnState(Form1, "NEW", pObj)
                ViewState("MODE") = "NEW"

                If ViewState("Type") = "Credits" Then
                    ddlTrigOffsetTyp.SelectedValue = 3
                    ddlTrigUnit.SelectedValue = 3
                    ddlTrigUnit.Enabled = False
                    ddlTrigOffsetTyp.Enabled = False
                    txtTrigOffsetSeq.Enabled = False
                    txtMinCredsCompltd.Text = "0"
                    txtMinCredsCompltd.Enabled = False
                    'ddlQuantMinUnitType.SelectedValue = 1
                    'ddlQuantMinUnitType.Enabled = False
                    ViewState("Type") = "Credits"
                ElseIf ViewState("Type") = "DaysEndTerm" Then
                    ddlTrigOffsetTyp.SelectedValue = 2
                    ddlTrigUnit.SelectedValue = 1
                    ddlTrigUnit.Enabled = False
                    ddlTrigOffsetTyp.Enabled = False
                    txtTrigOffsetSeq.Enabled = False
                    'ddlQuantMinUnitType.SelectedValue = 1
                    'ddlQuantMinUnitType.Enabled = False
                    ViewState("Type") = "DaysEndTerm"
                ElseIf ViewState("Type") = "DaysSDate" Then
                    ddlTrigOffsetTyp.SelectedValue = 3
                    ddlTrigUnit.SelectedValue = 1
                    ddlTrigUnit.Enabled = False
                    ddlTrigOffsetTyp.Enabled = False
                    txtTrigOffsetSeq.Enabled = False
                    'ddlQuantMinUnitType.SelectedValue = 1
                    'ddlQuantMinUnitType.Enabled = False
                    ViewState("Type") = "DaysSDate"
                ElseIf ViewState("Type") = "WeeksEndTerm" Then
                    ddlTrigOffsetTyp.SelectedValue = 2
                    ddlTrigUnit.SelectedValue = 2
                    ddlTrigUnit.Enabled = False
                    ddlTrigOffsetTyp.Enabled = False
                    txtTrigOffsetSeq.Enabled = False
                    'ddlQuantMinUnitType.SelectedValue = 1
                    'ddlQuantMinUnitType.Enabled = False
                    ViewState("Type") = "WeeksEndTerm"
                ElseIf ViewState("Type") = "WeeksSDate" Then
                    ddlTrigOffsetTyp.SelectedValue = 3
                    ddlTrigUnit.SelectedValue = 2
                    ddlTrigUnit.Enabled = False
                    ddlTrigOffsetTyp.Enabled = False
                    txtTrigOffsetSeq.Enabled = False
                    'ddlQuantMinUnitType.SelectedValue = 1
                    'ddlQuantMinUnitType.Enabled = False
                    ViewState("Type") = "WeeksSDate"
                ElseIf ViewState("Type") = "ClockSchHoursSDate" Then
                    ddlTrigOffsetTyp.SelectedValue = 3
                    ddlTrigUnit.SelectedValue = 5
                    ddlTrigUnit.Enabled = False
                    ddlTrigOffsetTyp.Enabled = False
                    txtTrigOffsetSeq.Enabled = False
                    ddlQuantMinUnitType.SelectedValue = 3
                    ddlQuantMinUnitType.Enabled = False
                    txtMinAttendanceValue.Text = 0
                    txtMinAttendanceValue.Enabled = False
                    txtMinCredsCompltd.Text = 0
                    txtMinCredsCompltd.Enabled = False
                    ViewState("Type") = "ClockSchHoursSDate"
                ElseIf ViewState("Type") = "ClockActHoursSDate" Then
                    ddlTrigOffsetTyp.SelectedValue = 3
                    ddlTrigUnit.SelectedValue = 4
                    ddlTrigUnit.Enabled = False
                    ddlTrigOffsetTyp.Enabled = False
                    txtTrigOffsetSeq.Enabled = False
                    ddlQuantMinUnitType.SelectedValue = 3
                    ddlQuantMinUnitType.Enabled = False
                    txtMinAttendanceValue.Text = 0
                    txtMinAttendanceValue.Enabled = False
                    txtMinCredsCompltd.Text = 0
                    txtMinCredsCompltd.Enabled = False
                    ViewState("Type") = "ClockActHoursSDate"
                ElseIf ViewState("Type") = "MonthSDate" Then
                    ddlTrigOffsetTyp.SelectedValue = Request("TrigOffset")
                    ddlTrigUnit.SelectedValue = 6
                    ddlTrigUnit.Enabled = False
                    ddlTrigOffsetTyp.Enabled = False
                    txtTrigOffsetSeq.Enabled = False
                    If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        txtQuantMinValue.Text = 0
                        txtQuantMinValue.Enabled = False
                        ddlQuantMinUnitType.SelectedIndex = 0
                        ddlQuantMinUnitType.Enabled = False
                    End If
                    txtMinAttendanceValue.Text = 0
                    txtMinAttendanceValue.Enabled = True
                    txtMinCredsCompltd.Enabled = True
                    ViewState("Type") = "MonthSDate"
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
                If ctl.GetType Is GetType(eWorld.UI.NumericBox) Then
                    CType(ctl, eWorld.UI.NumericBox).Text = ""
                End If
            Next
            If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                For Each ctl In DirectCast(RadPanelBar1.FindItemByValue("Skill").FindControl("pnlSkill"), Panel).Controls
                    If ctl.GetType Is GetType(DropDownList) Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                    End If
                Next
                pnlpercent.Visible = False
            End If
            Dim txtId As String = String.Empty
            For i As Integer = 0 To dtInstructionTypes.Rows.Count - 1
                txtId = "txt" & dtInstructionTypes.Rows(i)("InstructionTypeid").ToString
                CType(pnlInstructionTypeAttendance.FindControl(txtId), TextBox).Text = ""
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstIncrements_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstIncrements.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim facade As New SAPFacade
        Dim dt As New DataTable
        Dim SAPInfoObject As New SAPInfo
        Try
            strGUID = dlstIncrements.DataKeys(e.Item.ItemIndex).ToString()
            txtSAPDetailId.Text = strGUID
            ViewState("SAPDetailId") = strGUID
            SAPInfoObject = facade.GetSAPInfoOnItemCmd(strGUID)

            txtSAPPolicy.Text = ViewState("SAPPolicy")

            txtSeq.Text = SAPInfoObject.Seq
            'Display TrigValue with two decimal places
            txtTrigValue.Text = SAPInfoObject.TrigValue / 100
            objCommon.SelTextInDDL(ddlTrigUnit, SAPInfoObject.TrigUnit.ToString)
            objCommon.SelTextInDDL(ddlTrigOffsetTyp, SAPInfoObject.TrigOffsetTyp.ToString)

            If SAPInfoObject.TrigOffsetSeq = 0 Then
                txtTrigOffsetSeq.Text = ""
            Else
                txtTrigOffsetSeq.Text = SAPInfoObject.TrigOffsetSeq
            End If

            'Display always at least a zero
            txtQualMinValue.Text = SAPInfoObject.QualMinValue.ToString("0.##")
            If SAPInfoObject.OverallAttendance = False Then
                radAttendance.SelectedValue = 1
                pnlOverallAttendance.Visible = False
                pnlInstructionTypeAttendance.Visible = True
                Dim txtId As String = String.Empty
                For i As Integer = 0 To SAPInfoObject.QuantMinValueByInsTypes.Count - 1
                    txtId = "txt" & SAPInfoObject.QuantMinValueByInsTypes(i).InstructionTypeId.ToString
                    CType(pnlInstructionTypeAttendance.FindControl(txtId), TextBox).Text = SAPInfoObject.QuantMinValueByInsTypes(i).QuantMinValue
                Next
                txtQuantMinValue.Text = ""

            Else
                radAttendance.SelectedValue = 0
                txtQuantMinValue.Text = SAPInfoObject.QuantMinValue.ToString
                pnlOverallAttendance.Visible = True
                pnlInstructionTypeAttendance.Visible = False
                Dim txtId As String = String.Empty
                For i As Integer = 0 To dtInstructionTypes.Rows.Count - 1
                    txtId = "txt" & dtInstructionTypes.Rows(i)("InstructionTypeid").ToString
                    CType(pnlInstructionTypeAttendance.FindControl(txtId), TextBox).Text = ""
                Next
            End If

            'Display always at least a zero
            txtMinAttendanceValue.Text = SAPInfoObject.MinAttendanceValue.ToString("0.##")
            txtMinCredsCompltd.Text = SAPInfoObject.MinCredsCompltd
            txtValidationSequence.Text = SAPInfoObject.ValidationSequence
            CType(pnlRHS.FindControl("txtAccuracy"), eWorld.UI.NumericBox).Text = SAPInfoObject.Accuracy

            objCommon.SelTextInDDL(ddlQuantMinUnitType, SAPInfoObject.QuantMinUnitType.ToString)
            objCommon.SelTextInDDL(ddlQualType, SAPInfoObject.QualType.ToString)
            'ddlQuantMinUnitType.SelectedValue = SAPInfoObject.QuantMinUnitType.ToString
            'ddlQualType.SelectedValue = SAPInfoObject.QualType.ToString
            objCommon.SelTextInDDL(ddlConsequence, SAPInfoObject.Consequence.ToString)



            objCommon.SetBtnState(Form1, "EDIT", pObj)
            ViewState("MODE") = "EDIT"

            'Enable the save text box only if this policy is not being used.
            btnSave.Enabled = Not SAPInfoObject.IsSAPPolicyUsed

            selIndex = e.Item.ItemIndex
            dlstIncrements.SelectedIndex = selIndex
            ds.ReadXml(sr)
            BindDataList(ds)

            'Disable the offsetseq textbox if we are dealing with start date
            If ddlTrigOffsetTyp.SelectedItem.Value = 3 Then
                txtTrigOffsetSeq.Enabled = False
            Else
                txtTrigOffsetSeq.Enabled = True
            End If
            If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                BindShortHandSkills(ViewState("SAPDetailId"))
                pnlpercent.Visible = False
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstReqTypes_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstReqTypes_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub


    Private Sub BindDataList(ByVal ds As DataSet)
        If ds.Tables.Count > 0 Then
            With dlstIncrements
                .DataSource = ds
                .DataBind()
            End With
        End If

    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim SAPObject As New SAPInfo
        Dim sw As New System.IO.StringWriter
        Dim objListGen As New DataListGenerator
        Dim facade As New SAPFacade
        Dim TrigVals As New DataSet
        Dim msg As String = ""
        '        Dim increment As Integer
        Dim result As String



        If validatetxt = True Then
            Exit Sub
        End If

        If Not InputFieldsAreValid() Then
            DisplayErrorMessage(GetInputFieldErrors())
            Exit Sub
        End If

        'Troy:9/30/2005. The html page initially had a required field validator for the trigoffsetseq.
        'In addition the trigoffsetseq was also a required field in the db so the pagesetup component
        'was also creating another required field validator as well. The field has been changed to
        'allow nulls since if the trigoffsettyp is Start Date there wont be any offsetseq. The required
        'field validator in the html page has therefore been removed as well. However, we still need to
        'validate that there is an offsetseq when the offsettyp is not Start Date.
        If ddlTrigOffsetTyp.SelectedItem.Value <> 3 Then
            If txtTrigOffsetSeq.Text = "" Then
                DisplayErrorMessage("You must specify which term to use for this increment.")
                Exit Sub
            End If
        End If

        TrigVals = Session("DLValues")

        'increment = IIf(txtSeq.Text = "", 0, CInt(txtSeq.Text)) 'evaluate this statement.

        'If txtSeq.Text = "" Then
        'increment = 999999
        'Else
        '    increment = CInt(txtSeq.Text)
        'End If

        Try
            'msg = facade.ValidateIncrement(increment, TrigVals)
            '**********************************************************
            'If msg = "" Then 'If Increment doesnt exist already
            '**********************************************************
            'Move the values in the text boxes over to the object
            SAPObject = PopulateSAPObject()

            If ViewState("Type") = "ClockActHoursSDate" Or ViewState("Type") = "ClockSchHoursSDate" Then
                If radAttendance.SelectedValue = 1 Then
                    If noValueForInsTypes(SAPObject) Then
                        DisplayErrorMessage(" Please enter in percentage of hours for the Instruction Type(s).")
                        Exit Sub
                    End If
                End If
            End If

            If ViewState("MODE") = "NEW" Then
                'We want to validate the trig offset sequence if we are not dealing with start date.
                'If ddlTrigOffsetTyp.SelectedItem.Value <> 3 Then
                '    msg = ValidateSequence((New SAPFacade).GetValidationSequence(SAPObject.SAPId.ToString), increment, txtTrigValue.Text, ddlTrigUnit.SelectedItem.Value, ddlTrigOffsetTyp.SelectedItem.Value, txtTrigOffsetSeq.Text, True)
                'Else
                '    msg = ValidateSequence((New SAPFacade).GetValidationSequence(SAPObject.SAPId.ToString), increment, txtTrigValue.Text, ddlTrigUnit.SelectedItem.Value, ddlTrigOffsetTyp.SelectedItem.Value, "0", True)
                'End If

                If msg = "" Then 'If Increment doesnt exist already

                    dlstIncrements.SelectedIndex = -1
                    Dim SAPData As New SAPFacade
                    result = SAPData.AddSAPDetails(SAPObject)
                    If Not result = "" Then
                        '   Display Error Message
                        DisplayErrorMessage(result)
                    Else
                        SAPObject = facade.GetSAPInfoOnItemCmd(SAPObject.SAPDetailId.ToString)
                        txtSeq.Text = SAPObject.Seq
                    End If

                    ds = facade.PopulateDataList(ViewState("SAPId"))
                    dlstIncrements.SelectedIndex = -1
                    If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        AddShortHandSkillRequirement(SAPObject.SAPDetailId.ToString)
                        pnlpercent.Visible = False
                    End If
                    BindDataList(ds)
                    dlstIncrements.SelectedIndex = GetIndexOfThisItem(SAPObject.SAPDetailId.ToString)
                    'BindDataList(ds)
                    ds.WriteXml(sw)
                    ViewState("ds") = sw.ToString()
                    ' ds = ActivityData.PopulateDataList(XmlConvert.ToGuid(ViewState("EmployeeId")), ViewState("FromStartDate"), ViewState("ToStartDate"), ViewState("ActivityStatus"))
                    'ViewState("StoredDs") = ds
                    'Set Mode to EDIT since a record has been inserted into the db.
                    objCommon.SetBtnState(Form1, "EDIT", pObj)
                    ViewState("MODE") = "EDIT"
                Else
                    DisplayErrorMessage(msg)

                End If

            ElseIf ViewState("MODE") = "EDIT" Then
                'We want to validate the trig offset sequence if we are not dealing start date. 
                'If ddlTrigOffsetTyp.SelectedItem.Value <> 3 Then
                '    msg = ValidateSequence((New SAPFacade).GetValidationSequence(SAPObject.SAPId.ToString), SAPObject.Seq, txtTrigValue.Text, ddlTrigUnit.SelectedItem.Value, ddlTrigOffsetTyp.SelectedItem.Value, txtTrigOffsetSeq.Text, False)
                'Else
                '    msg = ValidateSequence((New SAPFacade).GetValidationSequence(SAPObject.SAPId.ToString), SAPObject.Seq, txtTrigValue.Text, ddlTrigUnit.SelectedItem.Value, ddlTrigOffsetTyp.SelectedItem.Value, "0", False)
                'End If

                If Not msg = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(msg)
                    Exit Sub
                End If

                dlstIncrements.SelectedIndex = -1
                Dim SAPData As New SAPFacade
                result = SAPData.UpdateSAPDetails(SAPObject)
                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    SAPObject = facade.GetSAPInfoOnItemCmd(SAPObject.SAPDetailId.ToString)
                    txtSeq.Text = SAPObject.Seq
                End If

                dlstIncrements.SelectedIndex = -1
                ds = facade.PopulateDataList(ViewState("SAPId"))

                If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    AddShortHandSkillRequirement(SAPObject.SAPDetailId.ToString)
                    pnlpercent.Visible = False

                End If
                BindDataList(ds)
                dlstIncrements.SelectedIndex = GetIndexOfThisItem(SAPObject.SAPDetailId.ToString)


                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
            End If

            ' Else 'Validation failed i.e Increment already exists.
            'Display Error Msg.
            'DisplayErrorMessage(msg)
            '  End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Function PopulateSAPObject() As SAPInfo
        Dim SAPObject As New SAPInfo
        Dim ReqTypes As New ReqTypesFacade
        '        Dim ReqTypId As Integer
        'Generate a new guid to insert the record

        If ViewState("MODE") = "NEW" Then

            'This code is not necessary since we set the textbox to a newguid in
            'page setup. This was changed to fix the browser refresh problem #01390 
            'SAPObject.SAPDetailId = Guid.NewGuid

            SAPObject.SAPDetailId = XmlConvert.ToGuid(txtSAPDetailId.Text)
            ViewState("SAPDetailId") = SAPObject.SAPDetailId.ToString
        Else
            SAPObject.SAPDetailId = XmlConvert.ToGuid(ViewState("SAPDetailId"))
        End If

        SAPObject.SAPId = XmlConvert.ToGuid(ViewState("SAPId"))

        'SAPObject.Seq = IIf(txtSeq.Text = "", 0, CInt(txtSeq.Text))
        If txtSeq.Text = "" Then
            SAPObject.Seq = 0
        Else
            SAPObject.Seq = CInt(txtSeq.Text)
        End If

        If txtTrigOffsetSeq.Text <> "" And ddlTrigOffsetTyp.SelectedValue <> AdvantageCommonValues.StartDateGuid Then
            SAPObject.TrigOffsetSeq = txtTrigOffsetSeq.Text
        End If
        SAPObject.TrigOffsetTyp = (ddlTrigOffsetTyp.SelectedItem.Text)
        SAPObject.TrigOffsetTypId = ddlTrigOffsetTyp.SelectedItem.Value
        SAPObject.TrigUnit = (ddlTrigUnit.SelectedItem.Text)
        SAPObject.TrigUnitTypId = ddlTrigUnit.SelectedItem.Value
        'Store TrigValue always as an integer
        If txtTrigValue.Text <> "" Then
            SAPObject.TrigValue = Decimal.Parse(txtTrigValue.Text) * 100
        End If
        If ViewState("Type") = "ClockActHoursSDate" Or ViewState("Type") = "ClockSchHoursSDate" Then
            If radAttendance.SelectedValue = 0 Then
                SAPObject.QuantMinValue = Decimal.Parse(txtQuantMinValue.Text)
                SAPObject.OverallAttendance = True
            Else
                SAPObject.QuantMinValue = Nothing
                SAPObject.OverallAttendance = False
                Dim _QuantMinValueByInsTypes(insTypeCount) As QuantMinValueByInsType
                Dim txtId As String = String.Empty
                For i As Integer = 0 To insTypeCount - 1
                    _QuantMinValueByInsTypes(i) = New QuantMinValueByInsType
                    txtId = "txt" & dtInstructionTypes.Rows(i)("InstructionTypeid").ToString
                    _QuantMinValueByInsTypes(i).InstructionTypeId = dtInstructionTypes.Rows(i)("InstructionTypeid")
                    _QuantMinValueByInsTypes(i).QuantMinValue = IIf(CType(pnlInstructionTypeAttendance.FindControl(txtId), TextBox).Text = "", 0, CType(pnlInstructionTypeAttendance.FindControl(txtId), TextBox).Text)
                    _QuantMinValueByInsTypes(i).SAPQuantInsTypeID = Guid.NewGuid()
                    _QuantMinValueByInsTypes(i).moddate = Date.Now
                    _QuantMinValueByInsTypes(i).SAPDetailId = SAPObject.SAPDetailId
                    SAPObject.AddQuantMinValueByInsType(_QuantMinValueByInsTypes(i))
                Next

                '_QuantMinValueByInsType = New QuantMinValueByInsType()
                '_QuantMinValueByInsType.moddate = Date.Now()
                'SAPObject.QuantMinValueByInsTypes = _QuantMinValueByInsTypes


            End If
        Else
            SAPObject.OverallAttendance = True
            SAPObject.QuantMinValue = Decimal.Parse(txtQuantMinValue.Text)
        End If


        SAPObject.QuantMinUnitType = (ddlQuantMinUnitType.SelectedItem.Text)
        SAPObject.QuantMinUnitTypId = (ddlQuantMinUnitType.SelectedItem.Value)
        SAPObject.QualMinValue = Decimal.Parse(txtQualMinValue.Text)
        SAPObject.MinAttendanceValue = Decimal.Parse(txtMinAttendanceValue.Text)
        SAPObject.QualMinTypId = ddlQualType.SelectedItem.Value
        SAPObject.Consequence = ddlConsequence.SelectedItem.Text
        SAPObject.ConsequenceTypId = ddlConsequence.SelectedItem.Value
        If txtMinCredsCompltd.Text.Equals(String.Empty) Then
            SAPObject.MinCredsCompltd = 0
        Else
            SAPObject.MinCredsCompltd = Decimal.Parse(txtMinCredsCompltd.Text)
        End If

        Try
            SAPObject.Accuracy = CType(pnlRHS.FindControl("txtAccuracy"), eWorld.UI.NumericBox).Text
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            SAPObject.Accuracy = 0
        End Try

        Return SAPObject
    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        If errorMessage <> "" Then CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Public Sub TextValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        Dim strMinus As String = Mid(args.Value, 1, 1)
        If strMinus = "-" Then
            DisplayErrorMessage("The value cannot be negative")
            validatetxt = True
        End If
    End Sub
    Private Function ValidateSequence(ByVal validationSequence As String, ByVal seq As Integer,
                                            ByVal trigValue As Long, ByVal trigUnitTypId As Long,
                                            ByVal trigOffsetTypId As Long, ByVal trigOffsetSeq As Long,
                                            ByVal insert As Boolean) As String
        Dim trigUnitTypIdMessageIncluded As Boolean = False
        Dim lng As Long

        '   get an array with the validation sequence of each item
        Dim valSeq As String() = validationSequence.Split(";")
        Array.Sort(valSeq)

        Dim topLength As Integer = valSeq.Length - 1

        Dim sb As New StringBuilder

        '   if there are no items exit validation
        If valSeq.Length = 1 Then Return ""

        For i As Integer = 0 To topLength
            'Dim c1 As Boolean = False
            'Dim c2 As Boolean = False
            Dim c3 As Boolean = False
            Dim c4 As Boolean = False

            '   validate type of unit. They must use same type of unit (Days, Weeks, etc.)
            If Not GetTrigUnitTypIdFromValSeq(valSeq(i)) = trigUnitTypId Then
                If Not trigUnitTypIdMessageIncluded Then
                    sb.Append("All increments must use same Type of Unit." + vbCrLf)
                    trigUnitTypIdMessageIncluded = True
                End If
            End If

            '   validate seq
            '   there should not be duplicates sequences during insertions
            'If valSeq(i) = CType(trigOffsetSeq * 100000 + trigOffsetTypId * 10000 + trigValue * 10 + trigUnitTypId, String).ToString Then
            lng = trigOffsetSeq * 1000000000 + trigOffsetTypId * 100000000 + trigValue * 100000 + trigUnitTypId
            If valSeq(i) = CType(lng, String).ToString Then
                If insert Then
                    sb.Append("Increment " + txtSeq.Text + " already exists." + vbCrLf)
                End If
            End If

            '   validate sequence of term, afterTerm and numberOfUnits
            If insert Then
                '' check seq of previous item
                'If i > 0 Then
                '    If seq > GetSeqFromValSeq(valSeq(i)) Then c1 = True
                'Else
                '    c1 = True
                'End If
                ''   check seq of next item
                'If i < topLength - 1 Then
                '    If seq < GetSeqFromValSeq(valSeq(i + 1)) Then c2 = True
                'Else
                '    c2 = True
                'End If

                '   check sequence of previous item
                'Dim s As Integer = trigOffsetSeq * 10000 + trigOffsetTypId * 1000 + trigValue
                lng = trigOffsetSeq * 100000000 + trigOffsetTypId * 10000000 + trigValue * 10000
                If i > 0 Then
                    If lng > GetLongSequenceFromValSeq(valSeq(i)) Then c3 = True
                Else
                    c3 = True
                End If
                If i < topLength - 1 Then
                    If lng < GetLongSequenceFromValSeq(valSeq(i + 1)) Then c4 = True
                Else
                    c4 = True
                End If
            Else
                '   this is the update case
                '   check that the sequence exists
                'If i > 0 Then
                '    If seq = GetSeqFromValSeq(valSeq(i - 1)) Then
                '        c1 = True
                '        c2 = True
                '    End If
                'Else
                '    c1 = True
                '    c2 = True
                'End If

                '   there should not be duplicates sequences during updates
                'If valSeq(i) = CType(trigOffsetSeq * 100000 + trigOffsetTypId * 10000 + trigValue * 10 + trigUnitTypId, String).ToString Then
                lng = trigOffsetSeq * 1000000000 + trigOffsetTypId * 100000000 + trigValue * 100000 + trigUnitTypId
                If valSeq(i) = CType(lng, String).ToString Then
                    If i + 1 <> seq Then sb.Append("Increment " + txtSeq.Text + " already exists." + vbCrLf)
                End If

                '   check sequence of previous item
                'Dim s As Integer = trigOffsetSeq * 10000 + trigOffsetTypId * 1000 + trigValue
                lng = trigOffsetSeq * 100000000 + trigOffsetTypId * 10000000 + trigValue * 10000
                If i > 0 Then
                    If lng > GetLongSequenceFromValSeq(valSeq(i - 1)) Then c3 = True
                Else
                    c3 = True
                End If
                '   check sequence of next item
                If i < topLength - 1 Then
                    If lng < GetLongSequenceFromValSeq(valSeq(i + 1)) Then c4 = True
                Else
                    c4 = True
                End If
            End If
            'If (c1 And c2 And c3 And c4) Then Return sb.ToString
            'If (c3 And c4) Then Return sb.ToString
        Next

        '   this increment is out of sequence
        'Return sb.Append("This increment is out of sequence." + vbCrLf).ToString
        Return sb.ToString

    End Function
    Private Function GetSeqFromValSeq(ByVal valSeq As String) As Integer
        'Dim int As Integer = CType(valSeq, Integer)
        'GetSeqFromValSeq = int / 10000000
        Dim lng As Long = CType(valSeq, Long)
        GetSeqFromValSeq = lng / 100000000000
    End Function
    Private Function GetTrigUnitTypIdFromValSeq(ByVal valSeq As String) As Integer
        'Dim int As Integer = CType(valSeq, Integer)
        'GetTrigUnitTypIdFromValSeq = int Mod 10
        Dim lng As Long = CType(valSeq, Long)
        GetTrigUnitTypIdFromValSeq = lng Mod 10
    End Function
    Private Function GetLongSequenceFromValSeq(ByVal valSeq As String) As Integer
        'Dim int As Integer = CType(valSeq, Integer)
        'GetLongSequenceFromValSeq = (int - (CType(int / 10000000, Integer) * 10000000)) / 10
        Dim lng As Long = CType(valSeq, Long)
        GetLongSequenceFromValSeq = (lng - (CType(lng / 100000000000, Integer) * 100000000000)) / 100000
    End Function

    Private Sub ddlTrigOffsetTyp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrigOffsetTyp.SelectedIndexChanged
        Dim SDateGuid As New AdvantageCommonValues

        If ddlTrigOffsetTyp.SelectedValue = AdvantageCommonValues.StartDateGuid Then
            txtTrigOffsetSeq.Enabled = False
            txtTrigOffsetSeq.Text = ""
        Else
            txtTrigOffsetSeq.Enabled = True
        End If
    End Sub
    Private Function GetIndexOfThisItem(ByVal keyValue As String) As Integer
        If dlstIncrements.Items.Count = 0 Then Return 0
        For i As Integer = 0 To dlstIncrements.Items.Count - 1
            Dim item As DataListItem = dlstIncrements.Items(i)
            Select Case item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    If CType(item.FindControl("Linkbutton1"), LinkButton).CommandArgument = keyValue Then Return item.ItemIndex
            End Select
        Next
        Return 0
    End Function

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim SDateGuid As New AdvantageCommonValues

        If ddlTrigOffsetTyp.SelectedValue = AdvantageCommonValues.StartDateGuid Then
            txtTrigOffsetSeq.Enabled = False
        Else
            txtTrigOffsetSeq.Enabled = True
        End If
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Private Function InputFieldsAreValid() As Boolean
        Try
            Dim d As Decimal = Decimal.Parse(txtQualMinValue.Text)
            If txtQuantMinValue.Visible = True Then
                Dim i As Decimal = Decimal.Parse(txtQuantMinValue.Text)
                If i > 100 Or i < 0 Then Return False
            End If
            If ddlQualType.SelectedValue = 2 Then
                If d > 100 Or d < 0 Then Return False
            Else
                If d > 4.0 Or d < 0 Then Return False
            End If

            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function GetInputFieldErrors() As String
        Try
            Dim d As Decimal = Decimal.Parse(txtQualMinValue.Text)
            If ddlQualType.SelectedValue = 2 Then
                If d > 100 Or d < 0 Then Return "Percentage must be between 1 and 100"
            Else
                If d > 4.0 Or d < 0 Then Return "GPA must be between 0 and 4.00"

            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return "Invalid GPA"
        End Try
        If txtQuantMinValue.Visible = True Then
            Try
                Dim i As Decimal = Decimal.Parse(txtQuantMinValue.Text)
                If i > 100 Or i < 1 Then Return "Percentage must be between 1 and 100"
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Return "Invalid Percentage"
            End Try
        End If
        Return ""
    End Function
    Private Sub BuildShortHandSkillLevelSection(Optional ByVal strclearvalue As String = "")

        Dim intRowCount As Integer = 0
        Dim intLoop As Integer
        Dim pnlSkill As Panel
        Dim dynLabel As Label
        Dim dynDropDownList, dynOperatorDropDownList As DropDownList

        Dim dsTopSpeedTestCategoryDetails As New DataSet
        Dim getScores As New SetDictationTestRulesFacade

        intRowCount = getScores.GetShortHandCourses
        dsTopSpeedTestCategoryDetails = getScores.GetSAPShortHandSkillLevel()

        pnlSkill = DirectCast(RadPanelBar1.FindItemByValue("Skill").FindControl("pnlSkill"), Panel)
        If intRowCount >= 1 Then
            pnlSkill.Controls.Add(New LiteralControl("<table class=""contenttable"" cellSpacing=""0"" cellPadding=""0"" width=""500px"" align=""center"">"))
            pnlSkill.Controls.Add(New LiteralControl("<tr height=""25""><td>&nbsp;</td></tr>"))
        End If
        For intLoop = 1 To intRowCount
            'Create Labels For Ranges
            dynLabel = New Label
            With dynLabel
                .ID = "lbl" & intLoop
                .Text = intLoop.ToString & "."
                .CssClass = "Label"
                .Style("left") = "500px"
            End With

            'Create TextBoxes For Ranges
            dynDropDownList = New DropDownList
            With dynDropDownList
                .ID = "ddl" & intLoop
                .CssClass = "DropDownList"
                .Style("width") = "300px"
                .Style("left") = "500px"
                .DataSource = dsTopSpeedTestCategoryDetails
                .DataTextField = "Descrip"
                .DataValueField = "ValueId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select  words per minute", "Select"))
                If strclearvalue = "clearvalues" Then
                    .SelectedIndex = 0
                End If
            End With

            dynOperatorDropDownList = New DropDownList
            With dynOperatorDropDownList
                .ID = "ddlOperator" & intLoop
                .CssClass = "DropDownList"
                .Style("width") = "75px"
                .Style("left") = "500px"
                .Items.Insert(0, New ListItem("Select", "Select"))
                .Items.Insert(1, New ListItem("Or", "Or"))
                .Items.Insert(2, New ListItem("And", "And"))
                If strclearvalue = "clearvalues" Then
                    .SelectedIndex = 0
                End If
            End With

            pnlSkill.Controls.Add(New LiteralControl("<tr><td width=""25px"">"))
            pnlSkill.Controls.Add(dynLabel)
            pnlSkill.Controls.Add(New LiteralControl("</td><td  width=""250px"">"))
            pnlSkill.Controls.Add(dynDropDownList)
            pnlSkill.Controls.Add(New LiteralControl("</td><td width=""25px"">&nbsp;</td><td  width=""200px"">"))
            If intLoop < intRowCount Then 'if there are 3 rows then the operator dropdown should only show up in first two rows.
                pnlSkill.Controls.Add(dynOperatorDropDownList)
            End If
            pnlSkill.Controls.Add(New LiteralControl("</td></tr><tr height=""10px""><td></td></tr>"))
        Next
        Dim dynLabelExample As New Label
        Dim strText As String = "     Example: The following is one of the SAP Shorthand Skill level requirement. " & " <br /><br />" & " At the end of 12 months, students in machine shorthand classes must pass a  five-minute dictation test at 95% accuracy or higher at" & " a shorthand speed of atleast 80 words per minute(wpm) in QA, 60 wpm in jury charge, or 60wpm in Literary."
        With dynLabelExample
            .CssClass = "Label"
            .ID = "dynLabelEx"
            .Text = strText
        End With

        Dim dynSelectionText As New Label
        With dynSelectionText
            .CssClass = "LabelBold"
            .ID = "dynSelectEx"
            .Visible = False
        End With

        If intRowCount >= 1 Then
            pnlSkill.Controls.Add(New LiteralControl("<tr height=""10px""><td></td></tr></table>"))
            pnlSkill.Controls.Add(New LiteralControl("<table class=""contenttable"" cellSpacing=""0"" cellPadding=""0"" width=""500px"" align=""center"">"))
            pnlSkill.Controls.Add(New LiteralControl("<tr height=""50px""><td>"))
            pnlSkill.Controls.Add(dynLabelExample)
            pnlSkill.Controls.Add(New LiteralControl("</td></tr>"))
            pnlSkill.Controls.Add(New LiteralControl("<tr height=""50px""><td>"))
            pnlSkill.Controls.Add(dynSelectionText)
            pnlSkill.Controls.Add(New LiteralControl("</td></tr>"))
            pnlSkill.Controls.Add(New LiteralControl("</table>"))
        End If
    End Sub
    Private Sub BindShortHandSkills(Optional ByVal SAPDetailId As String = "")
        Dim intRowCount As Integer = 0
        Dim intLoop As Integer
        Dim pnlSkill As Panel
        '        Dim dynLabel As Label
        Dim dynDropDownList, dynOperatorDropDownList As DropDownList

        Dim dsTopSpeedTestCategoryDetails As New DataSet
        Dim getScores As New SetDictationTestRulesFacade

        intRowCount = getScores.GetShortHandCourses
        pnlSkill = DirectCast(RadPanelBar1.FindItemByValue("Skill").FindControl("pnlSkill"), Panel)
        Dim ds As New DataSet()
        'ds = Nothing
        Try
            ds = getScores.BindShortHandSkillRequirement(SAPDetailId) 'getScores.GetSAPShortHandSkillLevel()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ds = Nothing
        End Try

        'The accuracy column has been added to arSAP_ShortHandSkillRequirement instead of arSAPDetails table, as 
        'there is an expectation that the school may want to tie the accuracy with the short hand skills
        'If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count >= 1 Then
        '    If Not ds.Tables(0).Rows(0)("Accuracy") Is System.DBNull.Value Then CType(pnlRHS.FindControl("txtAccuracy"), eWorld.UI.NumericBox).Text = ds.Tables(0).Rows(0)("Accuracy").ToString
        'End If

        For intLoop = 1 To intRowCount
            Dim ctrlDDLName As String = "ddl" + intLoop.ToString
            Dim ctrlDDLOperator As String = "ddlOperator" + intLoop.ToString
            dynDropDownList = DirectCast(pnlSkill.FindControl(ctrlDDLName), DropDownList)
            dynOperatorDropDownList = DirectCast(pnlSkill.FindControl(ctrlDDLOperator), DropDownList)
            SelectMentorRequirementValues(dynDropDownList, dynOperatorDropDownList, ds, intLoop)
        Next
        Dim strMentorText As String = ""
        strMentorText = FormatMentorRequirementString(SAPDetailId)
        If Not strMentorText.Trim = "" Then
            If pnlSkill.FindControl("dynLabelEx") IsNot Nothing Then
                DirectCast(pnlSkill.FindControl("dynLabelEx"), Label).Visible = False
                DirectCast(pnlSkill.FindControl("dynSelectEx"), Label).Visible = True
                DirectCast(pnlSkill.FindControl("dynSelectEx"), Label).Text = "The following is the selected shorthand skill level requirement: " & "<br><br>" & strMentorText
            End If

        Else
            If pnlSkill.FindControl("dynLabelEx") IsNot Nothing Then
                DirectCast(pnlSkill.FindControl("dynLabelEx"), Label).Visible = True
                DirectCast(pnlSkill.FindControl("dynSelectEx"), Label).Visible = False

            End If

        End If
    End Sub
    Private Sub SelectMentorRequirementValues(ByRef ddlMentorRequirementList As DropDownList,
                                              ByRef ddlMentorOperatorList As DropDownList,
                                              ByVal ds As DataSet,
                                              ByVal intLoopCounter As Integer)
        If Not ddlMentorOperatorList Is Nothing Then
            If Not ds Is Nothing Then
                Try
                    ddlMentorRequirementList.SelectedValue = ds.Tables(0).Rows(intLoopCounter - 1)("MentorValue")
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlMentorRequirementList.SelectedIndex = 0
                End Try

                Try
                    ddlMentorOperatorList.SelectedValue = ds.Tables(0).Rows(intLoopCounter - 1)("Operator")
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlMentorOperatorList.SelectedIndex = 0
                End Try
            Else
                ddlMentorRequirementList.SelectedIndex = 0
                ddlMentorOperatorList.SelectedIndex = 0
            End If
        Else
            If Not ds Is Nothing Then
                Try
                    ddlMentorRequirementList.SelectedValue = ds.Tables(0).Rows(intLoopCounter - 1)("MentorValue")
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlMentorRequirementList.SelectedIndex = 0
                End Try
            End If
        End If
    End Sub
    Private Function SetupShortHandSkill(ByVal SAPDetailId As String) As DataSet
        Dim intRowCount As Integer = 0
        Dim intLoop As Integer
        Dim pnlSkill As Panel
        '        Dim dynLabel As Label
        Dim dynDropDownList, dynOperatorDropDownList As DropDownList

        Dim dsTopSpeedTestCategoryDetails As New DataSet
        Dim getScores As New SetDictationTestRulesFacade

        intRowCount = getScores.GetShortHandCourses
        pnlSkill = DirectCast(RadPanelBar1.FindItemByValue("Skill").FindControl("pnlSkill"), Panel)

        Dim tblMentor As New DataTable("SetupShortHandSkill")
        With tblMentor
            'Define table schema
            .Columns.Add("ShortHandSkillRequirementId", GetType(System.Guid))
            .Columns.Add("SAPDetailId", GetType(System.Guid))
            .Columns.Add("GrdComponentTypeId", GetType(System.Guid))
            .Columns.Add("Speed", GetType(System.Decimal))
            .Columns.Add("Operator", GetType(System.String))
            .Columns.Add("OperatorSequence", GetType(System.Int32))
        End With

        For intLoop = 1 To intRowCount
            Dim ctrlDDLName As String = "ddl" + intLoop.ToString
            Dim ctrlDDLOperator As String = "ddlOperator" + intLoop.ToString
            dynDropDownList = DirectCast(pnlSkill.FindControl(ctrlDDLName), DropDownList)
            dynOperatorDropDownList = DirectCast(pnlSkill.FindControl(ctrlDDLOperator), DropDownList)
            If Not dynOperatorDropDownList Is Nothing Then
                If Not dynDropDownList.SelectedValue = "" AndAlso dynOperatorDropDownList.SelectedValue = "" Then
                    dynOperatorDropDownList.SelectedValue = "or "
                End If
                Dim instrColon As Integer = 0
                If Not dynDropDownList.SelectedValue = "" Then
                    instrColon = InStr(dynDropDownList.SelectedValue, ":")
                    If instrColon >= 1 Then
                        Dim GrdComponentTypeId As String = dynDropDownList.SelectedValue.Substring(0, instrColon - 1)
                        Dim speed As Decimal = dynDropDownList.SelectedValue.Substring(instrColon)
                        tblMentor.LoadDataRow(New Object() {System.Guid.NewGuid, SAPDetailId, GrdComponentTypeId, speed, dynOperatorDropDownList.SelectedValue, intLoop}, False)
                    End If
                End If
            Else
                If Not dynDropDownList.SelectedValue = "" Then
                    Dim instrColon As Integer = 0
                    If Not dynDropDownList.SelectedValue = "" Then
                        instrColon = InStr(dynDropDownList.SelectedValue, ":")
                        If instrColon >= 1 Then
                            Dim GrdComponentTypeId As String = dynDropDownList.SelectedValue.Substring(0, instrColon - 1)
                            Dim speed As Decimal = dynDropDownList.SelectedValue.Substring(instrColon)
                            tblMentor.LoadDataRow(New Object() {System.Guid.NewGuid, SAPDetailId, GrdComponentTypeId, speed, "Select", intLoop}, False)
                        End If
                    End If
                End If
            End If
        Next
        Dim ds As DataSet = New DataSet()
        If tblMentor.Rows.Count >= 1 Then
            ds.Tables.Add(tblMentor)
        End If
        Return ds
    End Function
    Private Sub AddShortHandSkillRequirement(ByVal SAPDetailId As String) 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document
        Dim dsRules As New DataSet
        Dim grdFacade As New SetDictationTestRulesFacade
        dsRules = SetupShortHandSkill(SAPDetailId)
        Try
            If Not dsRules Is Nothing AndAlso dsRules.Tables(0).Rows.Count >= 1 Then
                For Each lcol As DataColumn In dsRules.Tables(0).Columns
                    lcol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                Dim strXML As String = dsRules.GetXml
                Dim strModUser As String = Session("UserName")
                Dim strModDate As DateTime = Date.Now
                Dim dtOldEffectiveDate As DateTime = #1/1/1900#
                If ViewState("btnState") = "new" Then
                    grdFacade.SetupSAPShortHandSkillRequirement(SAPDetailId, strXML, strModUser, strModDate)
                    Exit Sub
                End If
                grdFacade.SetupSAPShortHandSkillRequirement(SAPDetailId, strXML, strModUser, strModDate)
                Dim strMentorText As String
                strMentorText = FormatMentorRequirementString(SAPDetailId)
                Dim pnlSkill As Panel = Nothing
                If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    pnlSkill = DirectCast(RadPanelBar1.FindItemByValue("Skill").FindControl("pnlSkill"), Panel)
                    pnlpercent.Visible = False
                End If
                If Not strMentorText.Trim = "" Then
                    DirectCast(pnlSkill.FindControl("dynLabelEx"), Label).Visible = False
                    DirectCast(pnlSkill.FindControl("dynSelectEx"), Label).Visible = True
                    DirectCast(pnlSkill.FindControl("dynSelectEx"), Label).Text = "The following is the selected shorthand skill level requirement: " & "<br><br>" & strMentorText
                Else
                    DirectCast(pnlSkill.FindControl("dynLabelEx"), Label).Visible = True
                    DirectCast(pnlSkill.FindControl("dynSelectEx"), Label).Visible = False
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Private Function FormatMentorRequirementString(ByVal SAPDetailId As String) As String
        Dim dsMentorProctored As New DataSet
        Dim strMentorText As String = ""
        Dim intOperatorLoop As Integer = 0
        Dim strPreviousOperator As String = ""
        Dim intLoop As Integer = 0
        dsMentorProctored = (New SetDictationTestRulesFacade).GetSAPRequirement(SAPDetailId)
        'If Not dsMentorProctored Is Nothing AndAlso dsMentorProctored.Tables(0).Rows.Count >= 1 Then
        '    strMentorText = "("
        '    For Each drReq As DataRow In dsMentorProctored.Tables(0).Rows
        '        If Not drReq("MentorOperator").ToString.ToLower.Trim = "select" Then
        '            If drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "or" Then
        '                strMentorText &= drReq("MentorRequirement")   'If the operator is AND, then the text will be Text1
        '            ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso Not strPreviousOperator = "or" Then
        '                strMentorText &= "(" & drReq("MentorRequirement") & ")"  'If the operator is AND, then the text will be (Text1)
        '            Else
        '                strMentorText &= drReq("MentorRequirement") 'if Operator is OR, then text will be just Text1
        '            End If
        '            If drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "or" Then
        '                strMentorText = strMentorText & ")  " & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
        '            ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "and" Then
        '                strMentorText = strMentorText & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
        '            Else
        '                strMentorText = strMentorText & " " & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
        '            End If
        '        End If
        '        If drReq("MentorOperator").ToString.ToLower.Trim = "select" AndAlso strPreviousOperator = "and" Then
        '            strMentorText = strMentorText & " (" & drReq("MentorRequirement") & ")"
        '        ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "select" AndAlso strPreviousOperator = "or" Then
        '            strMentorText = strMentorText & " " & drReq("MentorRequirement") & ")"
        '        End If
        '        strPreviousOperator = drReq("MentorOperator").ToString.ToLower.Trim
        '    Next
        '    Return strMentorText
        'End If
        If Not dsMentorProctored Is Nothing AndAlso dsMentorProctored.Tables(0).Rows.Count >= 1 Then
            If strPreviousOperator.Trim = "" Then
                strMentorText = "("
            End If
            For Each drReq As DataRow In dsMentorProctored.Tables(0).Rows
                If Not drReq("MentorOperator").ToString.ToLower.Trim = "select" Then
                    intLoop += 1
                    If drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "or" Then
                        strMentorText &= drReq("MentorRequirement")   'If the operator is AND, then the text will be Text1
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator.Trim = "" Then
                        strMentorText &= drReq("MentorRequirement")   'If the operator is AND, then the text will be Text1
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso (strPreviousOperator = "or" Or strPreviousOperator = "select") Then
                        strMentorText &= "(" & drReq("MentorRequirement") & ")"  'If the operator is AND, then the text will be (Text1)
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "or" AndAlso (strPreviousOperator = "and" Or strPreviousOperator = "select") Then
                        strMentorText &= drReq("MentorRequirement") & ")"  'If the operator is AND, then the text will be (Text1)
                    Else
                        strMentorText &= drReq("MentorRequirement") 'if Operator is OR, then text will be just Text1
                    End If
                    If drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "or" Then
                        strMentorText = strMentorText & ")  " & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "or" AndAlso strPreviousOperator = "and" Then
                        strMentorText = strMentorText & " <font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "and" Then
                        strMentorText = strMentorText & " <font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
                    Else
                        strMentorText = strMentorText & " " & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
                    End If
                End If
                If drReq("MentorOperator").ToString.ToLower.Trim = "select" AndAlso strPreviousOperator = "and" Then
                    If InStr(strMentorText, "OR") < 1 Then
                        strMentorText = strMentorText & drReq("MentorRequirement") & ")"
                    Else
                        strMentorText = strMentorText & " (" & drReq("MentorRequirement") & ")"
                    End If
                ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "select" AndAlso strPreviousOperator = "or" Then
                    If InStr(strMentorText, "AND") >= 1 Then
                        strMentorText = strMentorText & " (" & drReq("MentorRequirement") & ")"
                    Else
                        strMentorText = strMentorText & drReq("MentorRequirement") & ")"
                    End If
                End If
                strPreviousOperator = drReq("MentorOperator").ToString.ToLower.Trim
            Next
            If strMentorText.Length = 1 Then strMentorText = ""
            If Mid(strMentorText, strMentorText.Length - 10, 2).ToLower = "or" Then strMentorText = Replace(Replace(Mid(strMentorText, 1, strMentorText.Length - 11).Trim & "</font>", "(", ""), ")", "")
            Return strMentorText
        End If
        If strMentorText.Length = 1 Then strMentorText = ""
        Return strMentorText
    End Function

    Protected Sub radAttendance_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles radAttendance.SelectedIndexChanged
        'Dim txtId As String = String.Empty
        'For i As Integer = 0 To dtInstructionTypes.Rows.Count - 1
        '    txtId = "txt" & dtInstructionTypes.Rows(i)("InstructionTypeid").ToString
        '    CType(pnlInstructionTypeAttendance.FindControl(txtId), TextBox).Text = ""
        'Next
        txtQuantMinValue.Text = ""
        If radAttendance.SelectedValue = 0 Then
            pnlOverallAttendance.Visible = True
            pnlInstructionTypeAttendance.Visible = False

        Else
            pnlOverallAttendance.Visible = False
            pnlInstructionTypeAttendance.Visible = True

        End If
    End Sub
    Private Function noValueForInsTypes(SAPObject As SAPInfo) As Boolean
        Dim rtn As Boolean = True

        For i As Integer = 0 To SAPObject.QuantMinValueByInsTypes.Count - 1
            If SAPObject.QuantMinValueByInsTypes(i).QuantMinValue > 0 Then
                rtn = False
                Return rtn
            End If
        Next
        Return rtn
    End Function
    Private Sub ShowNonTitleIVFields()
        lblAttendance.Visible = True
        radAttendance.Visible = True
        lblMinAttendance.Visible = True
        txtMinAttendanceValue.Visible = True
        percentSpan.Visible = True
        lblMinCredsCompltd.Visible = True
        txtMinCredsCompltd.Visible = True
        creditSpan.Visible = True
        consequenceLabel.Visible = True
        ddlConsequence.Visible = True
    End Sub
    Private Sub HideNonTitleIVFields()
        lblAttendance.Visible = False
        radAttendance.Visible = False
        lblMinAttendance.Visible = False
        txtMinAttendanceValue.Visible = False
        percentSpan.Visible = False
        lblMinCredsCompltd.Visible = False
        txtMinCredsCompltd.Visible = False
        creditSpan.Visible = False
        consequenceLabel.Visible = False
        ddlConsequence.Visible = False
    End Sub
End Class
