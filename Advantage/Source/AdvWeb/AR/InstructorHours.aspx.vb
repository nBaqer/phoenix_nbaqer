﻿Imports FAME.common
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Xml
Imports BO = Advantage.Business.Objects

Partial Class InstructorHours
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Public campusId, userId As String
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim ds As New DataSet
        'Dim objCommon As New CommonUtilities
        Dim campusId As String
        Dim userId As String
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        'disable history button
        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not Page.IsPostBack Then
            btnExportToExcell.Visible = True
            btnExportToExcell.Attributes.Add("onClick", "exporttoexcel();")

            'Populate the campus dropdown list box with the campuses that the current user
            'has permission to. The special "sa" account should have access to all campuses.
            Dim cgFac As New CampusGroupsFacade

            With ddlCampus
                .DataSource = cgFac.GetCampusesByUser(userId)
                .DataTextField = "CampDescrip"
                .DataValueField = "CampusId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            'btnBuildList_Click(sender, e)
        End If
    End Sub
    Private Function Validate_Dates(ByVal StrDate As String, ByVal EndDate As String) As String
        Dim errorMessage As String = ""

        'validate start date
        If Not IsDate(StrDate) Then
            errorMessage += "'Start Date' is invalid." + vbCrLf
            Return errorMessage
        End If

        'validate end date
        If Not IsDate(EndDate) Then
            errorMessage += "'End Date' is invalid." + vbCrLf
            Return errorMessage
        End If

        'end date must be later that start date
        If Date.Parse(StrDate).CompareTo(Date.Parse(EndDate)) > 0 Then
            errorMessage += "'Start Date' must be earlier than 'End Date'." + vbCrLf
        End If
        Return errorMessage
    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        Dim fac As New ClassSchedulesFacade
        Dim strmess As String
        ''Date validation added by Saraswathi lakshmanan on march 2009 17
        ''Added while fixing 15378
        ''
        strmess = Validate_Dates(txtStart.SelectedDate, txtEnd.SelectedDate)
        If strmess = "" Then

            dgrdInstructorHours.DataSource = fac.GetInstructorHours(txtStart.SelectedDate, txtEnd.SelectedDate, ddlCampus.SelectedValue, "")
            dgrdInstructorHours.DataBind()
        Else
            DisplayErrorMessage(strmess)
        End If

    End Sub

End Class
