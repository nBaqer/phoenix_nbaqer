Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections
' ===============================================================================
'
' FAME AdvantageV1
'
' FERPACategory.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Partial Class FERPACategory
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Panel4 As System.Web.UI.WebControls.Panel
    Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator3 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lbtBankAccts As System.Web.UI.WebControls.LinkButton
    Protected errorMessage As String


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then

            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            BuildDropDownLists()
            BindDataList(campusId)
            BindFERPAData(New FERPAInfo)
            BuildFERPAPages()

        Else
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        End If
        headerTitle.Text = Header.Title
    End Sub

    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub BindDataList(ByVal campusid As String)

        Dim sShowActive As String = ""

        If radstatus.SelectedItem.Text.ToLower = "active" Then
            sShowActive = "True"
        ElseIf radstatus.SelectedItem.Text.ToLower = "inactive" Then
            sShowActive = "False"
        Else
            sShowActive = "All"
        End If

        '   bind BankCodes datalist
        With New StudentFERPA
            dlstFERPACat.DataSource = .GetFERPACategories(sShowActive, campusid)
            dlstFERPACat.DataBind()
        End With

    End Sub

    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()

    End Sub

    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub dlstFERPACat_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstFERPACat.ItemCommand

        Dim strId As String = dlstFERPACat.DataKeys(e.Item.ItemIndex).ToString()

        GetFERPACodeId(strId)
        BindFERPAPageList(strId)
        txtOldCampGrpId.Text = ddlCampGrpId.SelectedValue

        '   initialize buttons
        InitButtonsForEdit(e.CommandArgument)
        CommonWebUtilities.RestoreItemValues(dlstFERPACat, strId)

    End Sub

    Private Sub BindFERPAData(ByVal ferpa As FERPAInfo)

        With ferpa
            chkIsInDB.Checked = .IsInDB
            txtFERPAId.Text = .FERPAId
            txtCode.Text = .Code
            ddlStatusId.SelectedValue = .StatusId
            txtFERPADescrip.Text = .Name
            ddlCampGrpId.SelectedValue = .CampGrpId
        End With

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        '   instantiate component

        Dim chkFERPAPages As String() = GetFERPAPages()
        If chkFERPAPages Is Nothing Then
            DisplayErrorMessage("Select at least one student FERPA page")
            Exit Sub
        End If
        If Not errorMessage = "" Then
            DisplayErrorMessage(errorMessage)
            Exit Sub
        End If
        If Trim(txtOldCampGrpId.Text) <> "" Then
            If Trim(txtOldCampGrpId.Text) <> Trim(ddlCampGrpId.SelectedValue) Then
                Dim intClsSectionExists As Integer = (New TermProgressFacade).IsThereAnyFERPACategory(txtFERPAId.Text, txtOldCampGrpId.Text, ddlCampGrpId.SelectedValue)
                If intClsSectionExists >= 1 Then
                    DisplayErrorMessage("Cannot change the campus group because there are students with FERPA Policy that are associated with this campus group")
                    Exit Sub
                End If
            End If
        End If

        'Check If Mask is successful only if Foreign Is not checked
        If errorMessage = "" Then

            Dim result As String
            With New StudentFERPA
                '   update bank Info 
                result = .UpdateFERPAInfo(BuildFERPAInfo(txtFERPAId.Text), chkFERPAPages)
            End With

            BindDataList(campusId)

            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get the BankCode from the backend and display it
                GetFERPACodeId(txtFERPAId.Text)
            End If

            '   if there are no errors bind a new entity and init buttons
            If Page.IsValid Then

                '   set the property IsInDB to true in order to avoid an error if the user
                '   hits "save" twice after adding a record.
                chkIsInDB.Checked = True

                'note: in order to display a new page after "save".. uncomment next lines
                '   bind an empty new BankInfo
                'BindBankData(New BankInfo)

                '   initialize buttons
                'InitButtonsForLoad()
                InitButtonsForEdit(txtFERPAId.Text)

            End If
        End If
        CommonWebUtilities.RestoreItemValues(dlstFERPACat, txtFERPAId.Text)
    End Sub

    Private Function BuildFERPAInfo(ByVal ferpaID As String) As FERPAInfo

        '   instantiate class
        Dim ferpaInfo As New FERPAInfo

        With ferpaInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get bankId
            .FERPAId = ferpaID

            '   get Code
            .Code = txtCode.Text.Trim

            '   get StatusId

            .StatusId = ddlStatusId.SelectedValue

            '   get bank's name
            .Name = txtFERPADescrip.Text.Trim

            '   get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue
            .ModUser = HttpContext.Current.Session("UserName").ToString

        End With

        '   return data
        Return ferpaInfo

    End Function

    Private Sub BuildFERPAPages()
        Dim facade As New StudentFERPA
        With ChkFERPAPages
            .DataTextField = "Resource"
            .DataValueField = "ResourceId"
            .DataSource = facade.GetStudentPagesByModulePageName()
            .DataBind()
        End With
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        '   bind an empty new BankInfo
        BindFERPAData(New FERPAInfo)
        BuildFERPAPages()
        txtOldCampGrpId.Text = ""
        '   Reset Style in the Datalist
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstFERPACat, Guid.Empty.ToString, ViewState)

        '   initialize buttons
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlstFERPACat, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click

        If Not (txtFERPAId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim saf As New StudentFERPA

            '   update bank Info 
            Dim result As String = saf.DeleteFERPAInfo(txtFERPAId.Text)

            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                BindDataList(campusId)
                '   bind an empty new BankInfo
                BindFERPAData(New FERPAInfo)
                BuildFERPAPages()
                '   initialize buttons
                InitButtonsForLoad()
                txtOldCampGrpId.Text = ""
            End If

        End If
        CommonWebUtilities.RestoreItemValues(dlstFERPACat, Guid.Empty.ToString)
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False


    End Sub

    Private Sub InitButtonsForEdit(ByVal bankId As String)
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = True
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        '   add the campusId and the bankId to the URL

    End Sub

    Private Sub GetFERPACodeId(ByVal FERPACodeId As String)
        '   Create a StudentsFacade Instance
        Dim saf As New StudentFERPA

        '   bind BankCode properties
        BindFERPAData(saf.GetFERPAInfo(FERPACodeId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged

        BindFERPAData(New FERPAInfo)
        txtOldCampGrpId.Text = ""
        InitButtonsForLoad()
        BindDataList(campusId)

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button
        controlsToIgnore.Add(btnsave)

        'Add javascript code to warn the user about non saved changes
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    Private Function GetFERPAPages() As String()
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        '    Dim selectedDegrees() As String = selectedDegrees.CreateInstance(GetType(String), chkLeadGrpId.Items.Count)
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), ChkFERPAPages.Items.Count)
        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In ChkFERPAPages.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        'resize the array
        If i > 0 Then
            ReDim Preserve selectedDegrees(i - 1)
        Else
            Return Nothing
        End If

        Return selectedDegrees

    End Function

    Private Sub BindFERPAPageList(ByVal FERPACategoryID As String)
        'Get Degrees data to bind the CheckBoxList
        Dim Facade As New StudentFERPA

        'Clear the current selection
        ChkFERPAPages.ClearSelection()
        Dim dt As DataTable = Facade.GetFERPAPages(FERPACategoryID)

        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim row As DataRow
                row = dt.Rows(i)
                Dim item As ListItem
                For Each item In ChkFERPAPages.Items
                    If item.Value.ToString = CType(row("ResourceId"), Integer) Then
                        item.Selected = True
                        Exit For
                    End If
                Next
            Next
        End If

    End Sub
End Class
