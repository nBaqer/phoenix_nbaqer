﻿
Imports System.Xml
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common
Imports System.Data

Partial Class TransferGrade
    Inherits BasePage

    Dim selectedClsSection() As String
    Dim isChecked As Boolean
    Dim isCheckedsuccess As Boolean = False

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ' ReSharper disable InconsistentNaming
    Protected WithEvents lblSkillGroups As Label
    Protected WithEvents lblPossibleSkills As Label
    Protected WithEvents lstPossibleSkills As ListBox
    Protected WithEvents txtSkillGrpId As TextBox
    Protected WithEvents btnAdd As Button
    Protected WithEvents btnRemove As Button
    Protected WithEvents lblStudentSkills As Label
    Protected WithEvents lblError As Label
    Protected WithEvents btnAddAll As Button
    Protected WithEvents btnRemoveAll As Button
    Protected WithEvents txtcheck As HtmlInputText
    Protected WithEvents txtStudentId As TextBox
    Protected WithEvents btnhistory As Button
    Protected WithEvents lstGrpName1 As ListBox
    Protected WithEvents lblSkillGroups1 As Label
    Protected WithEvents lblPossibleSkills1 As Label
    Protected WithEvents lblStudentSkills1 As Label
    Protected WithEvents lstPossibleSkills1 As ListBox
    Protected WithEvents lstStudentSkills1 As ListBox
    Protected WithEvents lstGrpName As ListBox
    Protected WithEvents lstStudentSkills As ListBox
    Protected WithEvents txtLeadId As TextBox
    Protected WithEvents pnlTerm1 As Panel
    ' ReSharper restore InconsistentNaming
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings
    Private StrMessageReturn As String
    Protected campusId As String


    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        'Dim campusId As String
        'Dim userId As String
        ' Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        'userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As User = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

        'Check if this page still exists in the menu while switching campus
        Dim boolSwitchCampus As Boolean = False
        Dim boolPageStillPartofMenu As Boolean
        Dim previousCampusId As String

        If Not Session("PreviousCampus") Is Nothing Then
            previousCampusId = Session("PreviousCampus").ToString
            If previousCampusId.Trim <> Request.QueryString("cmpid").ToString.Trim Then
                boolSwitchCampus = True 'User switched campus
            End If
            Session("PreviousCampus") = Request.QueryString("cmpid").ToString.Trim
            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If
        End If

        If Not IsPostBack Then
            PopulateTermDDL()
            pnlTerm.Visible = False
        End If

        headerTitle.Text = Header.Title
    End Sub
    Private Sub PopulateTermDDL()
        Dim facade As New PostFinalGrdsFacade
        With dlstTerm
            .DataSource = facade.GetCurrentAndPastTerms(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
            .DataBind()
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim strClsSectId As String
        Dim intArrCount As Integer
        Dim z As Integer
        Dim htmlString As StringBuilder = New StringBuilder()

        Dim transferFacade As New TransferGradeFacade
        Dim strClsSectionDescrip As String
        Dim strCourseCode As String
        ' Save the data-grid items in a collection.
        iitems = dgrdTransactionSearch.Items
        Try
            'Loop through the collection to get the Array Size
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True) Then
                    intArrCount += 1
                End If
            Next

            ' Finish if none is selected
            If intArrCount < 1 Then
                Exit Sub
            End If


            selectedClsSection = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedCourseCode() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedClsSectionDescrip() As String = Array.CreateInstance(GetType(String), intArrCount)

            'Loop Through The Collection To Get ClassSection
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                'retrieve class-section id from the data-grid
                strClsSectId = CType(iitem.FindControl("txtClsSectionId"), TextBox).Text()
                strClsSectionDescrip = CType(iitem.FindControl("Label3"), Label).Text
                strCourseCode = CType(iitem.FindControl("lblCode"), Label).Text
                If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True) Then
                    selectedClsSection.SetValue(strClsSectId, z)
                    selectedClsSectionDescrip.SetValue(strClsSectionDescrip, z)
                    selectedCourseCode.SetValue(strCourseCode, z)
                    z += 1
                End If
            Next

            'Flag for checkedBox
            If z = 0 Then isChecked = False
            'resize the array
            If z > 0 Then ReDim Preserve selectedClsSection(z - 1)

            isChecked = True


            'Transfer Grade In The BackGround
            Dim strGradeOutput As String
            Dim strGrdBkLevel As String
            strGrdBkLevel = "CourseLevel"
            strGradeOutput = transferFacade.TransferGrades(txtTermId.Text, strGrdBkLevel, selectedClsSection, selectedClsSectionDescrip, campusId, AdvantageSession.UserState.UserName, MyAdvAppSettings.AppSettings("GradesFormat").ToString(), selectedCourseCode)
            txtHint.Value = strGradeOutput

            If Not strGradeOutput = "" Then

                StrMessageReturn = " Grades were transferred successfully for student who completed all components."
                Dim scriptstring As String = strGradeOutput   '" \n\n" 
                scriptstring = scriptstring.Replace(vbCr, "\r").Replace(vbLf, "\n")
                scriptstring = scriptstring.Replace(Environment.NewLine, String.Empty)
                scriptstring = scriptstring.Replace("\n", "</BR>")

                With htmlString
                    .Append("<div> ")
                    .Append("   <p> <strong>" + StrMessageReturn + "</strong> </p> ")
                    .Append("<div style=' height: 350px; width: 525px; overflow-y:auto;'> ")
                    .Append("<p style=padding-left:20px;> ")
                    .Append(scriptstring)
                    .Append("</p> ")
                    .Append("</div> ")
                    .Append("</div> ")
                End With
                MessagePopUp(htmlString.ToString())
                isCheckedsuccess = True
            End If
            BindDataList()
        Finally
        End Try

    End Sub

    Private Sub dlstTerm_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstTerm.ItemCommand
        txtTermId.Text = CType(e.CommandArgument, String)
        BindDataList()
        pnlTerm.Visible = True
        CommonWebUtilities.RestoreItemValues(dlstTerm, txtTermId.Text)
        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            Button1.Enabled = True
        Else
            Button1.Enabled = False
        End If
        TransferGradesWindow.Visible = false
    End Sub
    Private Function DataChangeOnClickbutton(ByVal classArray() As String, ByVal ds As DataSet) As String
        Dim strReturnMsg As New StringBuilder
        Dim strSc As New StringBuilder
        Dim dt As DataTable = CType(Session("Source"), DataTable)
        Dim searchedValueNow As String
        Dim searchedValueBefore As String
        Dim searchCode As New List(Of String)()

        For Each item In classArray

            Dim rows() As DataRow = dt.Select("Testcls='" & item & "'")
            Dim dtRow() As DataRow = ds.Tables(0).Select("Testcls='" & item & "'")
            If rows.Count > 0 Then
                searchedValueNow = rows(0).Item("Grade").ToString()
                searchedValueBefore = dtRow(0).Item("Grade").ToString()
                'Comparing what was before and now
                If searchedValueNow = "No" And searchedValueBefore = "No" Then

                    searchCode.Add(CType(rows(0).Item("Code"), String))

                ElseIf searchedValueNow = "Yes" And searchedValueBefore = "No" Then
                    Dim strCompare As Boolean = InStr(strReturnMsg.ToString(), " Grades were not transferred successfully. ")

                    If strCompare = False Then
                        strReturnMsg.Append(" Grades were not transferred successfully. ")
                    End If
                ElseIf searchedValueNow = "No" And searchedValueBefore = "Yes" Then

                    Dim strCompare As Boolean = InStr(strReturnMsg.ToString(), " Grades were transferred successfully for student who completed all components. ")

                    If strCompare = False Then
                        strReturnMsg.Append(" Grades were transferred successfully for student who completed all components. ")
                    End If
                ElseIf searchedValueNow = "Yes" And searchedValueBefore = "Yes" Then


                    Dim strCompare As Boolean = InStr(strReturnMsg.ToString(), " Grades were transferred successfully for students who completed all components. ")

                    If strCompare = False Then
                        strReturnMsg.Append(" Grades were transferred successfully for students who completed all components. ")
                    End If

                End If
            End If

        Next
        If searchCode.Count > 1 Then
            For Each cls In searchCode
                strSc.Append(cls & "; ")
            Next
            strReturnMsg.Append("Grades were not transferred successfully for the following codes " & strSc.ToString() & " No students are registered for the class.")
        ElseIf searchCode.Count = 1 Then

            strReturnMsg.Append("Grades were not transferred successfully for the code " & searchCode(0).ToString() & " No students are registered for the class.")
        End If
        Return strReturnMsg.ToString()

    End Function



    Private Sub MessagePopUp(Optional ByVal scriptstring As String = "")
        Session("scriptstring") = scriptstring
        Dim strUrl As String = "TransferGradesAlert.aspx"
        TransferGradesWindow.Visible = True
        TransferGradesWindow.Windows(0).NavigateUrl = strUrl
        TransferGradesWindow.Windows(0).VisibleOnPageLoad = True

    End Sub
    Private Sub BindDataList()
        Dim transferFacade As New TransferGradeFacade
        'Dim campusId As String
        Dim ds As DataSet


        campusId = HttpContext.Current.Request.Params("cmpid")

        Dim strGrdBkLevel As String
        Dim facade As New StdGrdBkFacade
        'strGrdBkLevel = facade.GetGradeBookWeightingLevel(ClsSectionId.GetValue(x).ToString)
        strGrdBkLevel = CType(MyAdvAppSettings.AppSettings("GradeBookWeightingLevel"), String)
        If strGrdBkLevel = "InstructorLevel" Then

            ds = transferFacade.GetGradedByTermId(txtTermId.Text, campusId)

            If isChecked = True And isCheckedsuccess = False Then

                StrMessageReturn = DataChangeOnClickbutton(selectedClsSection, ds)
                MessagePopUp(StrMessageReturn)
            End If

            Session("Source") = ds.Tables(0)

            dgrdTransactionSearch.DataSource = ds
            dgrdTransactionSearch.DataBind()
        Else
            ds = transferFacade.GetGradedByTermIdCourseLevel(txtTermId.Text, campusId)

            If isChecked = True And isCheckedsuccess = False Then
                StrMessageReturn = DataChangeOnClickbutton(selectedClsSection, ds)
                MessagePopUp(StrMessageReturn)
            End If

            Session("Source") = ds.Tables(0)

            dgrdTransactionSearch.DataSource = ds
            dgrdTransactionSearch.DataBind()
        End If

    End Sub

    Public Function Conv(ByVal val As Integer) As String
        If val = 0 Then
            Return "No"
        Else
            Return "Yes"
        End If
    End Function

    Private Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim txtCheckIsGraded As TextBox

        iitems = dgrdTransactionSearch.Items
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            txtCheckIsGraded = CType(iitem.FindControl("txtIsGraded"), TextBox)
        Next
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False

        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(Button1)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub

    'Private Sub MessagePopUp(Optional ByVal scriptstring As String = "")
    '    'Dim strOpen As String = String.Format(" var my_window = window.open('','test','modal=yes, title=""Transferred Grades"", toolbar=no ,menubar=no, location=center, status=1, scrollbars=1, resizable=no ,height=450px, width=425px, screenX=140px, screenY=140px, top=422px, left=440px');")
    '    'Dim strWrite As String = String.Format(" my_window.document.write('")
    '    'Dim strclose As String = String.Format(" <button onclick=""self.close();"">Close Window</button> ")
    '    'Dim script As String = String.Format(strOpen + strWrite + StrMessageReturn + htmLstring + strclose + "'); ")
    '    'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radconfirm", script, True)
    '    'Dim message As String = StrMessageReturn + htmLstring
    '    'Dim strOpen As String = String.Format(" 
    '    'Dim strWrite As String = String.Format(" my_window.document.write('")
    '    'Dim strclose As String = String.Format(" <button onclick=""self.close();"">Close Window</button> ")

    '    'Dim script As String = String.Format(StrMessageReturn + htmLstring)
    '    'RadWindowManager1.RegisterWithScriptManager = True
    '    'RadWindowManager1.RadAlert(script, 450, 100, "Transferred Grades", "alertCallBackFn", "")
    '    '

    '    ' Dim strclose As String = String.Format("<button onclick=""self.close();"">Close Window</button>")
    '    ' Dim script As String = String.Format("var my_window = window.open('','test','toolbar=no,menubar=no,location=center,status=1,scrollbars=1,resizable=no,height=450px,width=425px,screenX=140px,screenY=140px,top=422px,left=440px,modal=yes'); my_window.document.write('" + StrMessageReturn + htmLstring + strclose + "'); ")
    '    'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radconfirm", script, True)
    '    'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", script, True)
    '    'RadWindowManager1.RadAlert(message, 450, 100, "Transferred Grades", "alertCallBackFn", "")
    '    '' ("RadAlert is called from the server", 330, 180, "Server RadAlert", "alertCallBackFn", choices.SelectedValue)
    '    'RadWindowManager1.RadAlert(message, 450, 100, "Transferred Grades", "alertCallBackFn", "")

    '    '   this is the beginning of the javascript code  
    '    'Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=PassData();function PassData(){"

    '    '   this is the middle of the javascript
    '    '   build the parameter list to be returned to the parent page
    '    ' Dim scriptMiddle As String = ""


    '    'Dim sb As New StringBuilder
    '    Session("scriptstring") = scriptstring  '     htmLstring   '

    '    'Const name As String = "TransferGrades001" '       "ViewScheduleConflicts" ' 
    '    'Dim csType As Type = Page.GetType
    '    'Dim url As String = "../AR/" + name + ".aspx?resid=630&mod=AR&cmpid=" + campusId
    '    'Dim fullUrl As String = "window.open('" + url + "', '_blank', 'status=yes,resizable=yes,scrollbars=yes,width=450px,height=400px,fullscreen=no, modal=yes' );"

    '    'ScriptManager.RegisterStartupScript(Page, csType, "OPEN_WINDOW", fullUrl, True)



    '    Dim strUrl As String = "TransferGrades001.aspx?resid=630&mod=AR&cmpid=" + campusId
    '    TransferGradesWindow.Visible = True
    '    TransferGradesWindow.Windows(0).NavigateUrl = strUrl
    '    TransferGradesWindow.Windows(0).VisibleOnPageLoad = True

    'End Sub


    'Dim htmLstring1 As String = "<HTML>\n<HEAD> \n<TITLE>New Document</TITLE> \n</HEAD> \n<BODY bgColor=#E9EDF2> \n<style=""height:150px; overflow-y: scroll;""> \n<P style=padding-left:20px;> "
    'htmLstring1 = htmLstring1 & scriptstring & "</P> \n</div> \n</BODY>\n</HTML>"

    'Dim script As String = String.Format("var my_window = window.open('','test','toolbar=no,menubar=no,location=center,status=1,scrollbars=1,resizable=no,height=450px,width=425px,screenX=140px,screenY=140px,top=422px,left=440px,modal=yes'); my_window.document.write('" + htmLstring + "'); ")
    'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", script, True)


End Class
