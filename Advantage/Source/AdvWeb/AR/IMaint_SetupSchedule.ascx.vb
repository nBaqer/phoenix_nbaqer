' ===============================================================================
' Form_SetupSchedule
' RHS for the SetupSchedule maintenance page
' Uses IMaintFormBase interface
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.Common.AR

Partial Class AR_IMaint_SetupSchedule
    Inherits System.Web.UI.UserControl
    Implements IMaintFormBase

#Region "IMaintFormBase"
    Property Title() As String Implements IMaintFormBase.Title
        Get
            If ParentId IsNot Nothing AndAlso ParentId <> "" Then
                Dim info As PrgVersionInfo = PrgVersionsFacade.GetPrgVersionInfo(ParentId)
                If info IsNot Nothing AndAlso info.PrgVerDescrip <> "" Then
                    Return "Setup Schedules for " + info.PrgVerDescrip
                Else
                    Return "Setup Schedules for [Unknown Program Version]"
                End If
            End If
            Return "Setup Schedules"
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Property ObjId() As String Implements IMaintFormBase.ObjId
        Get
            Return ViewState("objId_SetupSchedule")
        End Get
        Set(ByVal value As String)
            ViewState("objId_SetupSchedule") = value
        End Set
    End Property

    Property ParentId() As String Implements IMaintFormBase.ParentId
        Get
            Return ViewState("parentId_SetupSchedule")
        End Get
        Set(ByVal value As String)
            ViewState("parentId_SetupSchedule") = value
        End Set
    End Property

    Property ModDate() As DateTime Implements IMaintFormBase.ModDate
        Get
            Return ViewState("moddate_Schedule")
        End Get
        Set(ByVal value As DateTime)
            ViewState("moddate_Schedule") = value
        End Set
    End Property

    ''' <summary>
    ''' Binds the form given an ID.
    ''' In this case, the ID is a ScheduleId referencing arProgSchedules
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BindForm(ByVal ID As String, Optional ByVal campusId As String = "", Optional ByVal Permission As String = "", Optional ByVal InSchoolEnrollmentStatus As String = "", Optional ByVal OutSchoolEnrollmentStatus As String = "") As String Implements IMaintFormBase.BindForm
        Try
            Dim info As ScheduleInfo = SchedulesFacade.GetScheduleInfo(ID)
            ObjId = ID
            ModDate = info.ModDate

            Me.txtSchCode.Text = info.Code
            Me.txtSchDescrip.Text = info.Descrip
            Me.cbSchFlextime.Checked = info.UseFlexTime

            ' Display whether this program is setup to use a timeclcok
            ' Do this before we bind the repeater because lblUseTimeClock value
            ' is used by the repeaters OnItemDataBound event.


            If info.UseTimeClock Then
                Me.lblUseTimeClock.Text = "Yes"
            Else
                Me.lblUseTimeClock.Text = "No"
            End If

            Me.rptSchedule.DataSource = info.DetailsDT
            Me.rptSchedule.DataBind()

            ' only allow flex time if this program is using a timeclock
            lblSchFlextime.Visible = info.UseTimeClock
            cbSchFlextime.Visible = info.UseTimeClock


            ' display the total number of hours for this schedule
            DisplayTotalScheduledHours()

            ' return an info message if this schedule is currently used
            If info.NumStudentsUsing > 0 And Permission <> "readonly" Then
                Return "Notice: Changes to this schedule cannot be made because it is used by 1 or more students."
            End If
            Return "" ' Success
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Function Handle_Save() As String Implements IMaintFormBase.Handle_Save
        Try
            Dim res As String = ValidateSchedule()
            If res <> "" Then Return res

            If ParentId = "" Then
                Return "Critical error.  Program Version has not been set."
            End If

            Dim scheduleid As String = ObjId
            Dim info As New ScheduleInfo
            ' determine if this is an update or a new record by looking
            If scheduleid Is Nothing Or scheduleid = "" Then
                info.IsInDB = False
                ObjId = info.ScheduleId
            Else
                info.IsInDB = True
                info.ScheduleId = ObjId
            End If
            info.PrgVerId = ParentId
            info.UseFlexTime = Me.cbSchFlextime.Checked
            info.Active = Me.ddlSchActive.SelectedValue

            info.Code = Me.txtSchCode.Text
            info.Descrip = Me.txtSchDescrip.Text
            info.ModUser = ARCommon.GetCurrentUserId()

            ' determine if we are using a timeclock
            Dim useTimeClock As Boolean = lblUseTimeClock.Text.ToLower = "yes"

            ' build up the array of schedule details
            Dim c As Integer = Me.rptSchedule.Items.Count - 1
            Dim details(c) As ScheduleDetailInfo
            For i As Integer = 0 To c
                Dim rpi As RepeaterItem = Me.rptSchedule.Items(i)
                If CType(rpi.FindControl("cbAllowExtraHours"), CheckBox).Checked Then
                    If CType(rpi.FindControl("cbAllowEarlyIn"), CheckBox).Checked = False And
                        CType(rpi.FindControl("cbAllowLateOut"), CheckBox).Checked = False Then
                        Dim strDay As String = ""
                        Select Case i
                            Case 0
                                strDay = "Sunday"
                            Case 1
                                strDay = "Monday"
                            Case 2
                                strDay = "Tuesday"
                            Case 3
                                strDay = "Wednesday"
                            Case 4
                                strDay = "Thursday"
                            Case 5
                                strDay = "Friday"
                            Case 6
                                strDay = "Saturday"
                        End Select
                        Return "Selection of Allow Extra Hours on " & strDay & " requires either Allow Early In or Allow Late In to be selected on " & strDay
                        Exit Function
                    End If
                End If
                details(i) = New ScheduleDetailInfo()
                details(i).ScheduleId = info.ScheduleId
                details(i).dw = i
                details(i).TimeIn = FixDateTime(CType(rpi.FindControl("tpTimeIn"), eWorld.UI.TimePicker).PostedTime)
                details(i).LunchOut = FixDateTime(CType(rpi.FindControl("tpLunchOut"), eWorld.UI.TimePicker).PostedTime)
                details(i).LunchIn = FixDateTime(CType(rpi.FindControl("tpLunchIn"), eWorld.UI.TimePicker).PostedTime)
                details(i).TimeOut = FixDateTime(CType(rpi.FindControl("tpTimeOut"), eWorld.UI.TimePicker).PostedTime)
                ''Changed from FixNUmber to FixDecimal for MaxNoLunch
                details(i).MaxNoLunch = FixDecimal(CType(rpi.FindControl("txtMaxNoHrsLunch"), TextBox).Text)
                details(i).Allow_EarlyIn = CType(rpi.FindControl("cbAllowEarlyIn"), CheckBox).Checked
                details(i).Allow_LateOut = CType(rpi.FindControl("cbAllowLateOut"), CheckBox).Checked
                details(i).Allow_ExtraHours = CType(rpi.FindControl("cbAllowExtraHours"), CheckBox).Checked
                details(i).Check_TardyIn = CType(rpi.FindControl("cbCheckTardyIn"), CheckBox).Checked
                details(i).MaxBeforeTardy = FixDateTime(CType(rpi.FindControl("tpMaxInTardy"), eWorld.UI.TimePicker).PostedTime)
                details(i).TardyInTime = FixDateTime(CType(rpi.FindControl("tpTardyInTime"), eWorld.UI.TimePicker).PostedTime)
                details(i).MinHoursConsideredPresent = FixDecimal(CType(rpi.FindControl("txtMinHoursPresent"), TextBox).Text)

                ' if we are not using a timeclock or the user want to use flex time, then the total is whatever they entered
                If Not useTimeClock Or Me.cbSchFlextime.Checked = True Then
                    details(i).Total = FixDecimal(CType(rpi.FindControl("txtTotHours"), TextBox).Text)
                Else
                    ' otherwise, the total must be calculated
                    details(i).Total = details(i).CalculatedTotal
                End If
            Next
            info.Details = details

            ' perform the update and record the result
            Dim UserId As String = ARCommon.GetCurrentUserId()
            res = SchedulesFacade.UpdateSchedule(info, ARCommon.GetUserNameFromUserId(UserId))

            ' if the update was successful, rebind the form so that everything display correctly.
            ' Also, this will update the ModDate so that clicking Save followed by Delete works properly
            If res = "" Then
                BindForm(ObjId)
            End If

            Return res ' return the result of the update
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Function Handle_New() As String Implements IMaintFormBase.Handle_New
        Try
            ObjId = Nothing
            ModDate = Nothing

            ARCommon.BuilStatusesDDL(Me.ddlSchActive)
            Me.txtSchCode.Text = ""
            Me.txtSchDescrip.Text = ""
            Me.cbSchFlextime.Checked = False
            Dim bUseTimeClock As Boolean = PrgVersionsFacade.IsUsingTimeClock(Me.ParentId)
            If bUseTimeClock Then
                Me.lblUseTimeClock.Text = "Yes"
            Else
                Me.lblUseTimeClock.Text = "No"
            End If

            lblTotHours.Text = ""

            Me.rptSchedule.DataSource = SchedulesFacade.GetBlankSchedule()
            Me.rptSchedule.DataBind()

            ' only allow flex time if this program is using a timeclock
            lblSchFlextime.Visible = bUseTimeClock
            cbSchFlextime.Visible = bUseTimeClock

            Return "" ' Success
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Function Handle_Delete() As String Implements IMaintFormBase.Handle_Delete
        Try
            If ObjId Is Nothing Or ObjId = "" Then Return "No record to delete"
            Dim res As String = SchedulesFacade.DeleteSchedule(ObjId, ModDate)
            If res <> "" Then
                Return res
            End If

            ObjId = ""
            ModDate = Date.MinValue
            Return "" ' Success
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Function GetLHSDataSet(ByVal Id As String, ByVal bShowActive As Boolean, ByVal bShowInactive As Boolean) As DataSet Implements IMaintFormBase.GetLHSDataSet
        If Id Is Nothing Or Id = "" Then Return Nothing
        Return SchedulesFacade.GetSchedules(Id, bShowActive, bShowInactive)
    End Function

    ReadOnly Property LSHImageUrl() As String Implements IMaintFormBase.LHSImageUrl
        Get
            Return "~/images/AR/lhs_setup_schedule.gif"
        End Get
    End Property
#End Region

#Region "Helpers"
    ''' <summary>
    ''' Handle when the user clicks the Flex Time checkbox
    ''' If flextime is enabled, then the user can enter the scheduled daily hours column for each day
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub cbSchFlextime_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSchFlextime.CheckedChanged
        For Each rpi As RepeaterItem In Me.rptSchedule.Items
            CType(rpi.FindControl("txtTotHours"), TextBox).Enabled = cbSchFlextime.Checked
        Next
    End Sub

    ''' <summary>
    ''' Validates the schedule pane
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateSchedule() As String
        Dim sbRes As New StringBuilder()

        If Me.txtSchCode.Text.Length = 0 Then sbRes.AppendFormat("Code is missing.{0}", vbCrLf)
        If Me.txtSchDescrip.Text.Length = 0 Then sbRes.AppendFormat("Description is missing.{0}", vbCrLf)

        For Each rpi As RepeaterItem In Me.rptSchedule.Items
            Dim bDOWError As Boolean = False ' flag if day of week has an error
            Dim lblDOW As Label = CType(rpi.FindControl("lblDOW"), Label)
            Dim tpTimeIn As eWorld.UI.TimePicker = CType(rpi.FindControl("tpTimeIn"), eWorld.UI.TimePicker)
            Dim tpLunchOut As eWorld.UI.TimePicker = CType(rpi.FindControl("tpLunchOut"), eWorld.UI.TimePicker)
            Dim tpLunchIn As eWorld.UI.TimePicker = CType(rpi.FindControl("tpLunchIn"), eWorld.UI.TimePicker)
            Dim tpTimeOut As eWorld.UI.TimePicker = CType(rpi.FindControl("tpTimeOut"), eWorld.UI.TimePicker)
            Dim TardyChecked As CheckBox = CType(rpi.FindControl("cbCheckTardyIn"), CheckBox)
            Dim Assigntardytime As eWorld.UI.TimePicker = CType(rpi.FindControl("tpTardyInTime"), eWorld.UI.TimePicker)
            Dim maxinbeforetardy As eWorld.UI.TimePicker = CType(rpi.FindControl("tpMaxInTardy"), eWorld.UI.TimePicker)

            Dim maxhrswithoutlunch As eWorld.UI.NumericBox = CType(rpi.FindControl("txtMaxNoHrsLunch"), eWorld.UI.NumericBox)


            ' check that both Time In and Time Out have been entered (user cannot only enter one of them)
            If tpTimeIn.SelectedTime = Date.MinValue AndAlso tpTimeOut.SelectedTime <> Date.MinValue Then
                sbRes.AppendFormat("On {0}, Time Out was entered without a Time In.{1}", lblDOW.Text, vbCrLf)
                bDOWError = True
            End If
            If tpTimeIn.SelectedTime <> Date.MinValue AndAlso tpTimeOut.SelectedTime = Date.MinValue Then
                sbRes.AppendFormat("On {0}, Time In was entered without a Time Out.{1}", lblDOW.Text, vbCrLf)
                bDOWError = True
            End If
            ' check that both Lunch In and Lunch Out have been entered (user cannot only enter one of them)
            If tpLunchOut.SelectedTime = Date.MinValue AndAlso tpLunchIn.SelectedTime <> Date.MinValue Then
                sbRes.AppendFormat("On {0}, Lunch Out was entered without a Lunch In.{1}", lblDOW.Text, vbCrLf)
                bDOWError = True
            End If
            If tpLunchOut.SelectedTime <> Date.MinValue AndAlso tpLunchIn.SelectedTime = Date.MinValue Then
                sbRes.AppendFormat("On {0}, Lunch In was entered without a Lunch Out.{1}", lblDOW.Text, vbCrLf)
                bDOWError = True
            End If

            If tpTimeIn.SelectedTime <> Date.MinValue Then
                If (tpLunchOut.SelectedTime <> Date.MinValue AndAlso tpTimeIn.SelectedTime > tpLunchOut.SelectedTime) Or
                    (tpLunchIn.SelectedTime <> Date.MinValue AndAlso tpTimeIn.SelectedTime > tpLunchIn.SelectedTime) Or
                    (tpTimeOut.SelectedTime <> Date.MinValue AndAlso tpTimeIn.SelectedTime > tpTimeOut.SelectedTime) Then
                    sbRes.AppendFormat("On {0}, Time In must be less than Lunch Out, Lunch In and Time Out.{1}", lblDOW.Text, vbCrLf)
                    bDOWError = True
                End If
            End If
            If tpLunchOut.SelectedTime <> Date.MinValue Then
                If (tpLunchIn.SelectedTime <> Date.MinValue AndAlso tpLunchOut.SelectedTime > tpLunchIn.SelectedTime) Or
                    (tpTimeOut.SelectedTime <> Date.MinValue AndAlso tpLunchOut.SelectedTime > tpTimeOut.SelectedTime) Then
                    sbRes.AppendFormat("On {0}, Lunch Out must be less than Lunch In and Time Out.{1}", lblDOW.Text, vbCrLf)
                    bDOWError = True
                End If
            End If
            If tpLunchIn.SelectedTime <> Date.MinValue Then
                If (tpTimeOut.SelectedTime <> Date.MinValue AndAlso tpLunchIn.SelectedTime > tpTimeOut.SelectedTime) Then
                    sbRes.AppendFormat("On {0}, Lunch In must be less than Time Out.{1}", lblDOW.Text, vbCrLf)
                    bDOWError = True
                End If
            End If
            ''Added by saraswathi to validate that maxxhrs without lunch cannot be entered when there is no entry for that day.
            ''March 10 2009
            If tpTimeIn.SelectedTime = Date.MinValue And tpTimeOut.SelectedTime = Date.MinValue And tpLunchIn.SelectedTime = Date.MinValue And tpLunchOut.SelectedTime = Date.MinValue And maxhrswithoutlunch.Text <> "" Then
                sbRes.AppendFormat("On {0}, Max hrs without lunch cannot be entered when there is no time in, time out, lunch in and lunch out.{1}", lblDOW.Text, vbCrLf)
                bDOWError = True
            End If

            ' check that the entered # of hours fits within the time window
            If Me.cbSchFlextime.Checked Then
                Dim sTotal As String = CType(rpi.FindControl("txtTotHours"), TextBox).Text
                If sTotal <> "" Then
                    Dim flextotal As Decimal = sTotal
                    Dim calctotal As Decimal = SchedulesFacade.GetDailyScheduledHours(tpTimeIn.SelectedTime, tpTimeOut.SelectedTime,
                                                                                      tpLunchOut.SelectedTime, tpLunchIn.SelectedTime)
                    If flextotal > calctotal Then
                        sbRes.AppendFormat("On {0}, Sched Hours is greater than the time windows allocated by Time In, Time Out, Lunch In and Lunch Out.{1}", lblDOW.Text, vbCrLf)
                        bDOWError = True
                    End If
                End If
            End If

            ''Added by Saraswathi Lakshmanan
            If TardyChecked.Checked = True Then
                If maxinbeforetardy.SelectedTime = Date.MinValue Or Assigntardytime.SelectedTime = Date.MinValue Then
                    sbRes.AppendFormat("On {0}, Max In Before Tardy and Assigned Tardy In Time should be entered.{1}", lblDOW.Text, vbCrLf)
                    bDOWError = True
                End If
                If maxinbeforetardy.SelectedTime <> Date.MinValue Then
                    If (tpTimeIn.SelectedTime <> Date.MinValue AndAlso maxinbeforetardy.SelectedTime < tpTimeIn.SelectedTime) Or
                        (tpLunchOut.SelectedTime <> Date.MinValue AndAlso maxinbeforetardy.SelectedTime > tpLunchOut.SelectedTime) Or
                        (tpLunchIn.SelectedTime <> Date.MinValue AndAlso maxinbeforetardy.SelectedTime > tpLunchIn.SelectedTime) Or
                        (tpTimeOut.SelectedTime <> Date.MinValue AndAlso maxinbeforetardy.SelectedTime > tpTimeOut.SelectedTime) Then
                        sbRes.AppendFormat("On {0}, Max In Before Tardy must be greater than Time In and  less than Lunch Out, Lunch In and Time Out.{1}", lblDOW.Text, vbCrLf)
                        bDOWError = True
                    End If
                End If
                If Assigntardytime.SelectedTime <> Date.MinValue Then
                    If (tpTimeIn.SelectedTime <> Date.MinValue AndAlso Assigntardytime.SelectedTime < tpTimeIn.SelectedTime) Or
                        (maxinbeforetardy.SelectedTime <> Date.MinValue AndAlso Assigntardytime.SelectedTime < maxinbeforetardy.SelectedTime) Or
                        (tpLunchOut.SelectedTime <> Date.MinValue AndAlso Assigntardytime.SelectedTime > tpLunchOut.SelectedTime) Or
                        (tpLunchIn.SelectedTime <> Date.MinValue AndAlso Assigntardytime.SelectedTime > tpLunchIn.SelectedTime) Or
                        (tpTimeOut.SelectedTime <> Date.MinValue AndAlso Assigntardytime.SelectedTime > tpTimeOut.SelectedTime) Then
                        sbRes.AppendFormat("On {0}, Assigned Tardy In Time must be greater than Time In,Max Tardy In Time and  less than Lunch Out, Lunch In and Time Out.{1}", lblDOW.Text, vbCrLf)
                        bDOWError = True
                    End If
                End If
            ElseIf TardyChecked.Checked = False Then
                If maxinbeforetardy.SelectedTime <> Date.MinValue Or Assigntardytime.SelectedTime <> Date.MinValue Then
                    sbRes.AppendFormat("On {0}, Max In Before Tardy and Assigned Tardy In Time cannot be entered without selecting Tardy Checkbox.{1}", lblDOW.Text, vbCrLf)
                    bDOWError = True
                End If
            End If

            ' Set the text color to red for this DOW if there was an error
            If bDOWError Then
                lblDOW.ForeColor = Drawing.Color.DarkRed
                lblDOW.Font.Bold = True
            Else
                lblDOW.ForeColor = Drawing.ColorTranslator.FromHtml("#000066")
                lblDOW.Font.Bold = False
            End If
        Next
        Return sbRes.ToString()
    End Function

    ''' <summary>
    ''' Updates the label control lblTotHours with the total number of hours in the schedule.
    ''' This should be called only after the repeater for the daily schedule has been bound
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub DisplayTotalScheduledHours()
        Dim tot As Decimal = 0.0
        For Each rpi As RepeaterItem In Me.rptSchedule.Items
            Dim strTot_day As String = CType(rpi.FindControl("txtTotHours"), TextBox).Text
            If strTot_day <> "" Then
                Try
                    tot += CType(strTot_day, Decimal)
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                End Try
            End If
        Next
        lblTotHours.Text = tot.ToString("#.##")
        lblTotHours.ToolTip = String.Format("Total hours: {0}{1}Total minutes: {2}", tot, vbCrLf, tot * 60.0)
    End Sub

    Protected Function GetDayOfWeek(ByVal dw As String) As String
        Select Case dw
            Case "0"
                Return "Sun"
            Case "1"
                Return "Mon"
            Case "2"
                Return "Tue"
            Case "3"
                Return "Wed"
            Case "4"
                Return "Thu"
            Case "5"
                Return "Fri"
            Case "6"
                Return "Sat"
        End Select
        Return "N/A"
    End Function

    Protected Function GetTime(ByVal o As Object) As String
        Try
            If DBNull.Value.Equals(o) Then Return ""
            Dim d As DateTime = o
            If d = DateTime.MinValue Then Return ""
            Return d.ToShortTimeString()
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ""
        End Try
    End Function

    Protected Function GetSafeTime(ByVal o As Object) As DateTime
        Try
            If DBNull.Value.Equals(o) Then Return DateTime.MinValue
            Return CType(o, DateTime)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return DateTime.MinValue
        End Try
    End Function

    Protected Function FixNumber(ByVal s As String) As Integer
        Try
            If s Is Nothing Or s = "" Then Return Integer.MinValue
            Return CType(s, Integer)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return Integer.MinValue
        End Try
    End Function

    Protected Function FixDecimal(ByVal s As String) As Decimal
        Try
            If s Is Nothing Or s = "" Then Return Decimal.MinValue
            Return CType(s, Decimal)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return Decimal.MinValue
        End Try
    End Function

    Protected Function FixDateTime(ByVal s As String) As DateTime
        Try
            If s Is Nothing Or s = "" Then Return DateTime.MinValue
            Dim d As DateTime = "1/1/1990 " + s
            Return d
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return DateTime.MinValue
        End Try
    End Function
#End Region

#Region "Schedule Repeater Helpers"
    ''' <summary>
    ''' bShow is True if program version is using timeclock and false otherwise
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ShowScheduleDetails(ByVal e As RepeaterItemEventArgs)
        Dim bShow As Boolean = Me.lblUseTimeClock.Text.ToLower = "yes"
        If e.Item.ItemType = ListItemType.Header Then
            e.Item.FindControl("sch0").Visible = bShow
            e.Item.FindControl("sch1").Visible = bShow
            e.Item.FindControl("sch2").Visible = bShow
            e.Item.FindControl("sch3").Visible = bShow
            e.Item.FindControl("sch4").Visible = bShow
            'e.Item.FindControl("sch5").Enabled = bShow ' Always show daily hours
            e.Item.FindControl("sch6").Visible = bShow
            e.Item.FindControl("sch7").Visible = bShow
            e.Item.FindControl("sch8").Visible = bShow
            e.Item.FindControl("sch9").Visible = bShow
            e.Item.FindControl("sch10").Visible = bShow
            e.Item.FindControl("sch11").Visible = bShow
            e.Item.FindControl("sch12").Visible = bShow

        ElseIf e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.FindControl("schd0").Visible = bShow
            e.Item.FindControl("schd1").Visible = bShow
            e.Item.FindControl("schd2").Visible = bShow
            e.Item.FindControl("schd3").Visible = bShow
            e.Item.FindControl("schd4").Visible = bShow

            ' Total daily schedule hours is enabled if we are not using a timeclcok
            ' or Flex time has been checked
            CType(e.Item.FindControl("txtTotHours"), TextBox).Enabled = Not bShow Or Me.cbSchFlextime.Checked

            e.Item.FindControl("schd6").Visible = bShow
            e.Item.FindControl("schd7").Visible = bShow
            e.Item.FindControl("schd8").Visible = bShow
            e.Item.FindControl("schd9").Visible = bShow
            e.Item.FindControl("schd10").Visible = bShow
            e.Item.FindControl("schd11").Visible = bShow
            e.Item.FindControl("schd12").Visible = bShow

            ''Added by SAraswathi to hide the footer caption "time should be entred in this format"
        ElseIf e.Item.ItemType = ListItemType.Footer Then
            e.Item.FindControl("lblFooterText").Visible = bShow
        End If
    End Sub

    ''' <summary>
    ''' Fills in the scheduled hours box for each day.
    ''' This value is determined in different ways depending on if we are using
    ''' a time clock and if we are using flex time
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub DisplayScheduledHoursForDay(ByVal e As RepeaterItemEventArgs)
        Dim timein As DateTime = GetSafeTime(e.Item.DataItem("timein"))
        Dim timeout As DateTime = GetSafeTime(e.Item.DataItem("timeout"))
        Dim lunchout As DateTime = GetSafeTime(e.Item.DataItem("lunchout"))
        Dim lunchin As DateTime = GetSafeTime(e.Item.DataItem("lunchin"))
        Dim bUseClockHours As Boolean = Me.lblUseTimeClock.Text.ToLower = "yes"

        If bUseClockHours Then
            ' Handle the case when we are using a timeclock
            ' If flex time is enabled, then we use whatever value is stored in the database.
            ' If no flex time, then we calculate based on timein, timout, lunchout, lunchin
            Dim tot As Decimal = 0.0
            If Me.cbSchFlextime.Checked Then
                If Not DBNull.Value.Equals(e.Item.DataItem("total")) Then
                    tot = CType(e.Item.DataItem("total"), Decimal)
                End If
            Else
                tot = SchedulesFacade.GetDailyScheduledHours(timein, timeout, lunchout, lunchin).ToString()
            End If

            If tot > 0.0 Then
                CType(e.Item.FindControl("txtTotHours"), TextBox).Text = String.Format("{0:#.##}", tot)
            Else
                CType(e.Item.FindControl("txtTotHours"), TextBox).Text = ""
            End If
        Else
            ' Handle the case when we are not using a timeclock
            ' This is always the value stored in the database
            If Not DBNull.Value.Equals(e.Item.DataItem("total")) Then
                Dim tot As Decimal = CType(e.Item.DataItem("total"), Decimal)
                If tot > 0.0 Then CType(e.Item.FindControl("txtTotHours"), TextBox).Text = tot.ToString("#.##")
            Else
                CType(e.Item.FindControl("txtTotHours"), TextBox).Text = ""
            End If
        End If
    End Sub

    ''' <summary>
    ''' Called for each item in the daily schedule repeater control.
    ''' Gives us a chance to fix up the ui
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptSchedule_OnItemBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptSchedule.ItemDataBound
        ' Hide or shows parts of the schedule depending on if flex time and/or use time clock is enabled.
        ShowScheduleDetails(e)

        If e.Item.ItemType = ListItemType.Header Then
        ElseIf e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            ' convert the integer dw to a day of week string like "Mon", "Tue", etc...
            CType(e.Item.FindControl("lblDOW"), Label).Text = GetDayOfWeek(e.Item.DataItem("dw").ToString())

            ' Set the total daily schedule hours for each dow
            DisplayScheduledHoursForDay(e)

            ' set the correct value for all the time pickers from the data source
            If e.Item.DataItem("allow_earlyin").ToString() = "" Then
                CType(e.Item.FindControl("cbAllowEarlyIn"), CheckBox).Checked = False
            Else
                CType(e.Item.FindControl("cbAllowEarlyIn"), CheckBox).Checked = e.Item.DataItem("allow_earlyin")
            End If
            If e.Item.DataItem("allow_lateout").ToString() = "" Then
                CType(e.Item.FindControl("cbAllowLateOut"), CheckBox).Checked = False
            Else
                CType(e.Item.FindControl("cbAllowLateOut"), CheckBox).Checked = e.Item.DataItem("allow_lateout")
            End If
            If e.Item.DataItem("allow_extrahours").ToString() = "" Then
                CType(e.Item.FindControl("cbAllowExtraHours"), CheckBox).Checked = False
            Else
                CType(e.Item.FindControl("cbAllowExtraHours"), CheckBox).Checked = e.Item.DataItem("allow_extrahours")
            End If
            If e.Item.DataItem("check_tardyin").ToString() = "" Then
                CType(e.Item.FindControl("cbCheckTardyIn"), CheckBox).Checked = False
            Else
                CType(e.Item.FindControl("cbCheckTardyIn"), CheckBox).Checked = e.Item.DataItem("check_tardyin")
            End If

            SelectTimeInTimePicker(CType(e.Item.FindControl("tpTimeIn"), eWorld.UI.TimePicker), e.Item.DataItem("timein"))
            SelectTimeInTimePicker(CType(e.Item.FindControl("tpLunchOut"), eWorld.UI.TimePicker), e.Item.DataItem("lunchout"))
            SelectTimeInTimePicker(CType(e.Item.FindControl("tpLunchIn"), eWorld.UI.TimePicker), e.Item.DataItem("lunchin"))
            SelectTimeInTimePicker(CType(e.Item.FindControl("tpTimeOut"), eWorld.UI.TimePicker), e.Item.DataItem("timeout"))
            SelectTimeInTimePicker(CType(e.Item.FindControl("tpMaxInTardy"), eWorld.UI.TimePicker), e.Item.DataItem("max_beforetardy"))
            SelectTimeInTimePicker(CType(e.Item.FindControl("tpTardyInTime"), eWorld.UI.TimePicker), e.Item.DataItem("tardy_intime"))
            'Dim minHours As Decimal = 0.0
            'If Not DBNull.Value.Equals(e.Item.DataItem("MinimumHoursToBePresent")) Then
            '    minHours = CType(e.Item.DataItem("MinimumHoursToBePresent"), Decimal)
            'End If
            'If minHours > 0.0 Then
            '    CType(e.Item.FindControl("txtMinHoursPresent"), TextBox).Text = String.Format("{0:#.##}", minHours)
            'Else
            '    CType(e.Item.FindControl("txtMinHoursPresent"), TextBox).Text = ""
            'End If

        End If
    End Sub

    ''' <summary>
    ''' Simple helper function that selects the correct time value in an eWorld.UI.TimePicker object.
    ''' If datetime param is null, it selects nothing
    ''' </summary>
    ''' <param name="tp"></param>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Protected Sub SelectTimeInTimePicker(ByVal tp As eWorld.UI.TimePicker, ByVal dt As Object)
        If Not DBNull.Value.Equals(dt) Then
            tp.SelectedTime = GetTime(dt)
        Else
            tp.SelectedTime = Nothing
        End If
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
End Class
