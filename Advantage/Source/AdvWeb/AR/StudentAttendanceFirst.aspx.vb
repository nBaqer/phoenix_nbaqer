﻿Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common

Namespace AdvWeb.AR

    Partial Class StudentAttendance
        Inherits BasePage

#Region "Variables"
        Protected State As AdvantageSessionState
        Protected LeadId As String
        Protected WithEvents Btnhistory As Button
        Protected StudentId As String
        Protected StudentName As String
        Protected CampusId As String
        Protected Userid As String
        Protected MyAdvAppSettings As AdvAppSettings
        'Private populateAttendanceGri As String = String.Empty
        Protected boolSwitchCampus As Boolean = False



#End Region

#Region "MRU Routines"
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer. Do not modify it using the code editor.
            InitializeComponent()
        End Sub



        Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As Advantage.Business.Objects.StudentMRU

            Dim objStudentState As New Advantage.Business.Objects.StudentMRU

            Try

                MyBase.GlobalSearchHandler(0)

                boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

                If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                    StudentId = Guid.Empty.ToString()
                Else
                    StudentId = AdvantageSession.MasterStudentId
                End If

                If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                    LeadId = Guid.Empty.ToString()
                Else
                    LeadId = AdvantageSession.MasterLeadId
                End If


                With objStudentState
                    .StudentId = New Guid(StudentId)
                    .LeadId = New Guid(LeadId)
                    .Name = AdvantageSession.MasterName
                End With

                HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
                HttpContext.Current.Items("Language") = "En-US"

                Master.ShowHideStatusBarControl(True)





            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Dim strSearchUrl As String = ""
                strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
                Response.Redirect(strSearchUrl)
            End Try

            Return objStudentState

        End Function


#End Region

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme
        End Sub
#End Region

#Region "Page Load"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            CampusId = Master.CurrentCampusId
            Userid = AdvantageSession.UserState.UserId.ToString

            '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
            'Get StudentId and LeadId
            Dim objStudentState As Advantage.Business.Objects.StudentMRU
            objStudentState = getStudentFromStateObject(327) 'Pass resourceid so that user can be redirected to same page while swtiching students

            If objStudentState Is Nothing Then
                RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If

            With objStudentState
                StudentId = .StudentId.ToString
                LeadId = .LeadId.ToString
                studentName = .Name

            End With

            If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
                ViewState("MODE") = "NEW"

                If boolSwitchCampus = True Then
                    CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, String.Empty)
                End If

                MyBase.uSearchEntityControlId.Value = ""
            Else

            End If
        End Sub

#End Region

    End Class
End Namespace