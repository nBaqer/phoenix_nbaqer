﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StuStatusChangeHist.aspx.vb" Inherits="AR_StuStatusChangeHist" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Status Change History</title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <link href="../css/systememail.css" type="text/css" rel="stylesheet" />
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR" />
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <style type="text/css">
        .toptemp {
            font-family: verdana;
            font-size: 14px;
            font-weight: bold;
            color: #FFFFFF;
            background-color: #164D7F;
            text-align: left;
            padding: 6px;
            width: 50%;
            vertical-align: bottom;
        }

        .tablecontent {
            border: 0;
            padding: 0;
            width: 100%;
            margin: 0;
            border-spacing: 0;
        }

        #footer{
            position: relative !important;
        }
    </style>
</head>
<body runat="server" id="Body1" name="Body1" style="text-align: left;">
    <form id="Form1" method="post" runat="server">
    <telerik:RadScriptManager ID="rsmMain" runat="server" AsyncPostBackTimeout="360000">
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="ramStuStatusChangeHist" runat="server" EnableAJAX="true">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgStuStatusChangeHist">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" LoadingPanelID="ralpStuStatusChangeHist" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralpStuStatusChangeHist" runat="server" >
    </telerik:RadAjaxLoadingPanel>
    <table class="tablecontent">
        <tr>
            <td>
                    <table class="tablecontent">
                    <tr>
                            <td class="toptemp">Status Change History
                        </td>
                        <td class="topemail">
                            <a class="close" onclick="top.close()" href="#">X Close</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
                <td style="width: 5px">&nbsp;
            </td>
        </tr>
        <tr style="vertical-align: top; text-align: center">
                <td>
                    <table class="tablecontent" id="Table1">
                    <tr>
                            <td style="vertical-align: top; width: 100%">
                            <asp:Panel ID="pnlRHS" runat="server">
                                <!--begin right column-->
                                    <table class="tablecontent" id="Table5">
                                    <tr>
                                        <td class="DetailsFrame" style="vertical-align: top; text-align: center">
                                            <div class="scrollsingleframe" style="left: 0; width: 100%; height: expression(document.body.clientHeight - 128 + 'px')">
                                                <!-- begin table content-->
                                                    <table class="contenttable" style="vertical-align: middle">
                                                    <tr>
                                                            <td>
                                                                <asp:Label ID="lblStudentName" runat="server" Text="Student Name: " Font-Size="12px"></asp:Label>
                                                       </td>
                                                    </tr>
                                                       <tr>
                                                            <td>
                                                                <asp:Label ID="lblEnrollmentName" runat="server" Text="Enrollment Name: " Font-Size="12px"></asp:Label>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                            <td style="text-align: right; padding-top: 15px;">
                                                                
                                                                <asp:Button ID="btnPrint" OnClick="btnExportToPdf_Click" runat="server" Text="Print" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                            <td style="vertical-align: middle; text-align: center; padding-top: 15px;">
                                                            <telerik:RadGrid ID="rgStuStatusChangeHist" runat="server" Width="90%" ShowStatusBar="true"
                                                                AutoGenerateColumns="False" AllowSorting="True" AllowMultiRowSelection="False"
                                                                AllowFilteringByColumn="true" GroupingSettings-CaseSensitive="false" AllowPaging="True"
                                                                 OnNeedDataSource="rgStuStatusChangeHist_NeedDataSource">
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                    <ExportSettings IgnorePaging="true" OpenInNewWindow="True">
                                                                </ExportSettings>
                                                                <MasterTableView Width="100%" Name="StuStatusChangeHist" AllowMultiColumnSorting="false">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="DateOfChange" HeaderText="Effective Date"
                                                                                HeaderButtonType="TextButton" DataField="DateOfChange" DataFormatString="{0:MM/dd/yyyy}">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="StatusCodeDescrip"
                                                                            HeaderText="Status" HeaderButtonType="TextButton" DataField="Status">
                                                                        </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn AllowFiltering="true" SortExpression="Details" HeaderText="Reason"
                                                                            HeaderButtonType="TextButton" DataField="Details">
                                                                        </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn AllowFiltering="true" SortExpression="RequestedBy" HeaderText="Requested By"
                                                                                HeaderButtonType="TextButton" DataField="RequestedBy">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn AllowFiltering="true" SortExpression="ModUser" HeaderText="Changed By"
                                                                            HeaderButtonType="TextButton" DataField="ModUser">
                                                                        </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn AllowFiltering="true" SortExpression="CaseNumber" HeaderText="Case #"
                                                                                HeaderButtonType="TextButton" DataField="CaseNumber">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn AllowFiltering="true" SortExpression="ModDate" HeaderText="Date of Change" ItemStyle-HorizontalAlign="Center"
                                                                                HeaderButtonType="TextButton" DataField="ModDate" DataFormatString="{0:MM/dd/yyyy}">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!--end table content-->
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                            <td></td>
                        <!-- end rightcolumn -->
                    </tr>
                </table>
            </td>
        </tr>
    </table>
        <div id="footer" runat="server">
            <asp:Label ID="lblFooterText" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
