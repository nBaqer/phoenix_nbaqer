﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Transcript.aspx.vb" Inherits="Transcript" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
    <style type="text/css">
        .transcript a {
            text-decoration: #333 underline;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table class="maincontenttable">
                <tr>
                    <td class="listframetop">
                        <asp:Label ID="lblFilter" runat="server" CssClass="label" Text="Filter Not Applicable"></asp:Label></td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftnofilter">
                            <asp:DataList ID="dlstLeadNames" runat="server" DataKeyField="StuEnrollId" Width="100%">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <SelectedItemTemplate>
                                    <asp:Label ID="label11" runat="server" CssClass="selecteditemstyle" Text='<%# Container.DataItem("PrgVerDescrip")%>' ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                    </asp:Label>
                                </SelectedItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Container.DataItem("PrgVerDescrip")%>' runat="server" CssClass="itemstyle" CommandName='<%# Container.DataItem("PrgVerId")%>' CommandArgument='<%# Container.DataItem("StuEnrollId")%>' ID="Linkbutton1" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table class="maincontenttable hidden">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable">
                <tr>
                    <td class="detailsframe">
                        <div class="scrollright2">
                            <!-- begin content table-->


                            <table class="maincontenttable transcript">
                                <tr>
                                    <td>
                                        <!-- Transcript table -->
                                        <table class="maincontenttable">
                                            <tr>
                                                <td >
                                                    <asp:DataGrid ID="dgrdTransactionSearch" runat="server" Width="100%" EditItemStyle-Wrap="false"
                                                        HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False" BorderWidth="1px"
                                                        BorderStyle="Solid" BorderColor="#ebebeb" CellPadding="3">
                                                        <FooterStyle CssClass="label"></FooterStyle>
                                                        <EditItemStyle Wrap="False" CssClass="label"></EditItemStyle>
                                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Term Start Date">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStartDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'
                                                                        ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                    </asp:Label>
                                                                    <asp:Label ID="lbloldTermId" runat="server" Text='<%# Container.DataItem("TermId") %>'
                                                                        Visible="False"></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="ddlEditClsSectionId" runat="server" CssClass="dropdownlist">
                                                                    </asp:DropDownList>
                                                                    <asp:Label ID="lblTermId" runat="server" Text='<%# Container.DataItem("TermId") %>'
                                                                        Visible="False"></asp:Label>
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Code">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                        runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Course">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="GrdBkLinkButton" Text='<%# Container.DataItem("Descrip")%>' CommandArgument='<%# Container.DataItem("TestId")%>'
                                                                        runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Credits">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCredits" Text='<%# Container.DataItem("Credits") %>' CssClass="label"
                                                                        runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Hours">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHours" Text='<%# Container.DataItem("Hours") %>' CssClass="label"
                                                                        runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Score">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblScore" Text='<%# getGradeRounding(Container.DataItem("Score")) %>'
                                                                        CssClass="label" runat="server" Visible="true" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'></asp:Label>
                                                                    <asp:Label ID="lblGrade" Text='<%# Container.DataItem("Grade") %>' CssClass="label"
                                                                        runat="server" Visible="true" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'></asp:Label>
                                                                    <asp:Label ID="lblscoreReqId" runat="Server" Text='<%# Container.DataItem("ReqId")%>'
                                                                        Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblIsTransferGrade" runat="Server" Text='<%# Container.DataItem("IsTransferGrade")%>'
                                                                        Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Grade">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="label3" Text='<%# Container.DataItem("Grade") %>' runat="server" CssClass="label"
                                                                        ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="ddlEditGrdSysDetailId" runat="server" CssClass="dropdownlist">
                                                                    </asp:DropDownList>
                                                                    <asp:Label ID="lblTestId" runat="server" Text='<%# Container.DataItem("TestId") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lblResultId" runat="server" Text='<%# Container.DataItem("ResultId") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lblReqId" runat="Server" Text='<%# Container.DataItem("ReqId")%>'
                                                                        Visible="false"></asp:Label>
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Edit">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnlButEdit" runat="server" Text="<img border=0 src=../images/im_edit.gif alt= edit/>"
                                                                        CausesValidation="False" CommandName="Edit" Visible="True">
                                                                         <img src="../images/im_edit.gif" alt="edit" style="border: 0"/>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="lnkbutUpdate" runat="server" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                                        CommandName="Update"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkbutDelete" runat="server" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                                        CausesValidation="False" CommandName="Delete"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkbutCancel" runat="server" Text="<img border=0 src=../images/im_delete.gif alt=Cancel>"
                                                                        CausesValidation="False" CommandName="Cancel"></asp:LinkButton>
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 15px;">
                                        <asp:Panel ID="pnlSummary" runat="server" Visible="False">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                                                <tr>
                                                    <td class="contentcell" width="100%">
                                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblClassesAttemptedTitle" runat="server" CssClass="label"
                                                                        Text="Total Classes Attempted:" Font-Bold="True">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCasessAttemptedValue" runat="server" CssClass="label">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCreditsAttemptedTitle" runat="server" CssClass="label"
                                                                        Text="Total Credits Attempted:" Font-Bold="True">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCreditsAttemptedValue" runat="server" CssClass="label">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblHoursSchecluedTitle" runat="server" CssClass="label"
                                                                        Text="Total Hours Scheduled:" Font-Bold="True">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblHoursSchecluedValue" runat="server" CssClass="label">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblGPAAverageTitle" runat="server" CssClass="label"
                                                                        Text="" Font-Bold="True">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblGPAAverageValue" runat="server" CssClass="label">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCreditsEarnedTitle" runat="server" CssClass="label"
                                                                        Text="Total Credits Earned:" Font-Bold="True ">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCreditsEarnedValue" runat="server" CssClass="label">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblHoursEarnedTitle" runat="server" CssClass="label"
                                                                        Text="Total Hours Earned:" Font-Bold="True">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblHoursEarnedValue" runat="server" CssClass="label">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblGradePointsTitle" runat="server" CssClass="label" Visible="False"
                                                                        Text="Total Classes Attempted:" Font-Bold="True">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblGradePointsValue" runat="server" CssClass="label" Visible="False">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell"></td>
                                                                <td class="contentcell"></td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCompletedSAPRequirementsTitle" runat="server" CssClass="label"
                                                                        Text="Completed SAP Requirements:" Font-Bold="True">
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCompletedSAPRequirementsValue" runat="server" CssClass="label">
                                                                    </asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <!-- Scheduled Courses table -->
                                <tr>
                                    <td style="padding-top: 15px;">
                                        <asp:Panel ID="pnlScheduledCourses" runat="server" Visible="false">
                                            <table class="maincontenttable">
                                                <tr>
                                                    <td class="contentcellheader">
                                                        <asp:Label ID="lblSchedCourses" runat="server" Font-Bold="true" CssClass="label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 10px; padding-bottom: 10px">
                                                        <asp:DataGrid ID="dgrdScheduledCourses" runat="server" Width="100%" EditItemStyle-Wrap="false"
                                                            HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False" BorderWidth="1px"
                                                            BorderStyle="Solid" BorderColor="#ebebeb" CellPadding="3" OnItemCommand="drgTransactionSearch_Click"
                                                            ShowFooter="False">
                                                            <EditItemStyle CssClass="label"></EditItemStyle>
                                                            <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <FooterStyle CssClass="label"></FooterStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="Term Start">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTermStartDate" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Class Start">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblClassStartDate" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"ClassStart", "{0:d}") %>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Code" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSchedCCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Course">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lblSchedCourse" Text='<%# Container.DataItem("Descrip")%>' CommandArgument='<%# Container.DataItem("TestId")%>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Credits" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Credits" Text='<%# Container.DataItem("Credits") %>' CssClass="label"
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Meetings (Room)">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTimeAndRoom" CssClass="label" Text='<%# Container.DataItem("TimeAndRoom") %>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Instructor">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblInstructor" CssClass="label" Text='<%# Container.DataItem("InstructorName") %>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Campus">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCampus" CssClass="label" Text='<%# Container.DataItem("CampDescrip") %>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 15px;">
                                        <asp:Panel ID="pnlRemainingCourses" runat="server" Visible="false">
                                            <table class="maincontenttable">
                                                <tr>
                                                    <td class="contentcellheader">
                                                        <asp:Label ID="lblRemainingCourses" runat="server" Font-Bold="true" CssClass="label">Remaining Courses</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 10px">
                                                        <asp:DataGrid ID="dgrdRemainingCourses" runat="server" Width="100%" EditItemStyle-Wrap="false"
                                                            HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False" BorderWidth="1px"
                                                            BorderStyle="Solid" BorderColor="#ebebeb" CellPadding="3" ShowFooter="False">
                                                            <EditItemStyle CssClass="label"></EditItemStyle>
                                                            <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <FooterStyle CssClass="label"></FooterStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="Code" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Course">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRCourse" CssClass="label" Text='<%# Container.DataItem("Req") %>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Credits">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCredits" CssClass="label" Text='<%# Container.DataItem("Credits") %>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Hours">
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblHours" CssClass="label" Text='<%# Container.DataItem("Hours") %>'
                                                                            runat="server" ForeColor='<%# IsCoursebelongtoPrgVersion(CType(DataBinder.Eval(Container.DataItem, "ReqId").tostring(), String)) %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>

                            <asp:TextBox ID="txtStudentId" CssClass="label" runat="server" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="txtStEmploymentId" CssClass="label" runat="server" Visible="false"></asp:TextBox>
                            <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                            <asp:TextBox ID="txtStudentDocs" runat="server" Visible="False"></asp:TextBox>

                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

