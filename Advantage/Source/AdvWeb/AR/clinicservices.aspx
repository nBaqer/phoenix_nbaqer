﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ClinicServices.aspx.vb" Inherits="ClinicServices" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="../js/Print.js"></script>
 <script language="javascript"  type="text/javascript" >
     function exporttoexcel() {
         var stuenrollid = "<%=txtStuEnrollId.text %>";
         var required = "<%=txtRequired.text %>";
         var completed = "<%=txtCompleted.text %>";
         var remaining = "<%=txtRemaining.text %>";
         var program = "<%=ddlStuEnrollId.SelectedItem.Text %>";
         var course = "<%=ddlCourses.SelectedItem.Text %>";
         var source = "Clinic Services";
         var turl = 'ExportToExcel.aspx?stuenrollid=' + stuenrollid + '&required=' + required + '&completed=' + completed + '&remaining=' + remaining + '&program=' + program + '&course=' + course + '&source=' + source;
         var strReturn = window.open(turl, 'clinicservice', 'resizable:yes;status:no;dialogWidth:800px;dialogHeight:290px;dialogHide:true;help:no;scroll:yes');
     }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

  
   
     </asp:Content>
     <asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<TR>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right">
                                    <asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button>
                                    <asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
							align="center">
							<tr>
								<td class="detailsframe"><div class="scrollsingleframe">
										<!-- begin table content-->
                                        <div>
            <asp:Table runat="server" ID="tbSelectFile" Width="65%" HorizontalAlign="center">
                <asp:TableRow>
                    <asp:TableCell width="25%" HorizontalAlign="right">
                        <asp:Label ID="lblEnrollment" Text="Enrollment" runat="Server" CssClass="labelbold"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="40%">
                         <asp:DropDownList ID="ddlStuEnrollId" runat="server" CssClass="dropdownlist" AutoPostBack="true" Width="400px">
                         </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell width="25%" HorizontalAlign="right">
                        <asp:Label ID="label7" Text="Courses" runat="Server" CssClass="labelbold"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="40%">
                         <asp:DropDownList ID="ddlCourses" runat="server" CssClass="dropdownlist" Width="400px">
                         </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                <asp:TableCell>
                </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow >
                        <asp:TableCell width="20%" HorizontalAlign="right">
                        </asp:TableCell>
                        <asp:TableCell ID="TableCell1" width="40%" runat="server" Wrap=false>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            <telerik:RadButton ID="btnBuildList" Text="Show Clinic Services" runat="Server" ></telerik:RadButton>
                            <telerik:RadButton id="btnPrint" runat="server" Text="Print Preview"  Width="100px"></telerik:RadButton>
							<telerik:RadButton ID="btnExportToExcel" runat="Server" Text="Export to Excel"  > </telerik:RadButton>
                    </asp:TableCell>    
                </asp:TableRow>
            </asp:Table>
             <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td>
                                                    <div id="print_area" runat="server">
                                                        <asp:panel ID="pnlStudentInfo" runat="server" visible=true Width="80%" HorizontalAlign="center">
                                                              <table cellpadding="0" cellspacing="0" width="70%" align="center">
                                        <tr>
                                            <td style="width: 80%; padding: 16px">
                                                <fieldset style="margin:0; padding: 0 12px 12px 12px; border: 1px solid #ebebeb">
                                                    <legend style="font: bold 11px verdana; color: #000066; margin-bottom: 12px">Student Information</legend>
                                                     <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="twocolumnlabelcell" nowrap align="left">
                                                                <asp:Label ID="lblStudent" runat="Server" 
                                                                style="font: normal 11px verdana;color: #000066;
                                                                background-color: transparent;width: 100%;padding: 2px;
                                                                vertical-align: bottom;
                                                                text-align:left;"
                                                                >Student:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap align="left">
                                                                <asp:Label ID="lblStudentName" runat="server"  
                                                                style="font: normal 11px verdana;color: #333333;width: 100%;	
                                                                text-align: left;	background-color: #FAFAFA;
                                                                border: 1px solid #ebebeb;padding: 0px 3px;text-align:left;">
                                                                </asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td class="twocolumnlabelcell" nowrap align="left">
                                                                <asp:Label ID="lblEnrollment1" runat="Server"
                                                                style="font: normal 11px verdana;color: #000066;
                                                                background-color: transparent;width: 100%;padding: 2px;
                                                                vertical-align: bottom;
                                                                text-align:left;"
                                                                >Enrollment:</asp:Label>
                                                            </td>
                                                          <td class="twocolumncontentcell" nowrap>
                                                                <asp:TextBox ID="lblEnrollmentName" runat="server" 
                                                                style="font: normal 11px verdana;color: #333333;width: 300px;	
                                                                text-align: left;	background-color: #FAFAFA;
                                                                border: 1px solid #ebebeb;padding: 0px 3px;text-align:left;"
                                                                contentEditable="False" CssClass="noclass"></asp:TextBox>
                                                            </td>
                                                            <td></td>
                                                             <td class="twocolumnlabelcell" nowrap align="left">
                                                                <asp:Label ID="lblStatus" runat="Server" style="font: normal 11px verdana;color: #000066;
                                                                background-color: transparent;width: 100%;padding: 2px 0px 0px 20px;
                                                                vertical-align: bottom;
                                                                text-align:left;">Enrollment Status:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap align="left">
                                                                <asp:Label ID="lblEnrollmentStatus" runat="Server"  
                                                                style="font: normal 11px verdana;color: #333333;width: 100%;	
                                                                text-align: left;	background-color: #FAFAFA;
                                                                border: 1px solid #ebebeb;padding: 0px 3px;text-align:left;"
                                                               ></asp:Label>
                                                            </td>
                                                        </tr>
                                                       <tr>
                                                       <td class="spacertables">
                                                       </td></tr>
                                                        <tr>
                                                             <td class="twocolumnlabelcell" nowrap align="left">
                                                                <asp:Label ID="lblStudentStartDate" runat="Server" 
                                                                style="font: normal 11px verdana;color: #000066;
                                                                background-color: transparent;width: 100%;padding: 2px;
                                                                vertical-align: bottom;
                                                                text-align:left;">Start Date:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap align="left">
                                                                <asp:Label ID="lblStartDate" runat="Server" CssClass="TextBox" 
                                                                style="font: normal 11px verdana;color: #333333;width: 100%;	
                                                                text-align: left;	background-color: #FAFAFA;
                                                                border: 1px solid #ebebeb;padding: 0px 3px;text-align:left;"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td class="twocolumnlabelcell" nowrap align="left">
                                                                <asp:Label ID="lblGradDate" runat="Server" 
                                                                style="font: normal 11px verdana;color: #000066;
                                                                background-color: transparent;width: 100%;padding: 2px;
                                                                vertical-align: bottom;
                                                                text-align:left;">Graduation Date:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap align="left">
                                                                <asp:Label ID="lblGD" runat="Server" style="font: normal 11px verdana;color: #333333;width: 100%;	
                                                                text-align: left;	background-color: #FAFAFA;
                                                                border: 1px solid #ebebeb;padding: 0px 3px;text-align:left;"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                    </asp:panel>
                                                <p></p>    
                <asp:DataGrid ID="dgClinicServices" CellPadding="0" BorderWidth="1px" BorderStyle="Solid" 
                BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal" Width="100%" runat="server">
                <EditItemStyle Wrap="False"></EditItemStyle>
                <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="11px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066"></ItemStyle>
                <HeaderStyle CssClass="datagridheaderstyle" BackColor="#FAFAFA" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066"></HeaderStyle>
                <AlternatingItemStyle CssClass="datagridalternatingstyle" BackColor="#FAFAFA"></AlternatingItemStyle>
                <Columns>
                    <asp:TemplateColumn HeaderText="Clinic Service" HeaderStyle-Font-Bold=true>
                        <HeaderStyle CssClass="datagridheaderstyle" width="55%" Font-Names="verdana" Font-Size="12px" Font-Bold="true" BorderColor="#E0E0E0" ForeColor="#000066"></HeaderStyle>
                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblClinicService" runat="Server" Text='<%# Container.DataItem("ClinicService") %>' CssClass="label"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Required">
                        <HeaderStyle CssClass="datagridheaderstyle" Width="15%" Font-Names="verdana" Font-Size="12px" Font-Bold="true" BorderColor="#E0E0E0" ForeColor="#000066"></HeaderStyle>
                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblRequired" Text='<%# DataBinder.Eval(Container, "DataItem.Required", "{0:#.00}") %>' CssClass="label"
                                runat="server">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Completed">
                        <HeaderStyle CssClass="datagridheaderstyle" width="15%" Font-Names="verdana" Font-Size="12px" Font-Bold="true" BorderColor="#E0E0E0" ForeColor="#000066"></HeaderStyle>
                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCompleted" Text='<%# Container.DataItem("Completed") %>' CssClass="label"
                                runat="server">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Remaining" >
                        <HeaderStyle CssClass="datagridheaderstyle" width="15%" Font-Names="verdana" Font-Size="12px" Font-Bold="true" BorderColor="#E0E0E0" ForeColor="#000066"></HeaderStyle>
                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblRemaining" Text='<%# Container.DataItem("Remaining") %>' CssClass="label"
                                runat="server">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>                                            
            </div>
                                            </td>
          </tr>
          </table> 
            
            </div>
                                        </div>
										
								</td>
							</tr>
						</table>
					</td>
					<!-- end rightcolumn --></TR>
			</table>
			<!-- start validation panel-->
              <asp:TextBox ID="txtStuEnrollId" runat="server" Width="0px" CssClass="donothing"  style="VISIBILITY: hidden"></asp:TextBox>
            <asp:TextBox ID="txtRequired" runat="server" Width="0px" CssClass="donothing" style="VISIBILITY: hidden"></asp:TextBox>
            <asp:TextBox ID="txtCompleted" runat="server" Width="0px" CssClass="donothing" style="VISIBILITY: hidden"></asp:TextBox>
            <asp:TextBox ID="txtRemaining" runat="server" Width="0px" CssClass="donothing" style="VISIBILITY: hidden"></asp:TextBox>
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel>
            <asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>



