
Partial Class AR_TardyMinutes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim decAdjustAbsent As Decimal = 0.0
            Dim decActualAbsent As Decimal = 0.0
            Label4.Text = Request.Params("numbermakestardy") & ")"
            'lblActPresent.Text = (Request.Params("actpresent") * 60) & " minutes (" & Request.Params("actpresent") & " hours)"
            'lblActAbsent.Text = (Request.Params("actabsent") * 60) & " minutes (" & Request.Params("actabsent") & " hours)"
            'lblActTardy.Text = (Math.Round(CType(Request.Params("acttardy"), Decimal), 2) * 60) & " minutes (" & Math.Round(CType(Request.Params("acttardy"), Decimal), 2) & " hours)"
            'lblAdjPresent.Text = (Request.Params("adjpresent") * 60) & " minutes (" & Request.Params("adjpresent") & " hours)"
            'lblAdjAbsent.Text = (Request.Params("adjabsent") * 60 + Request.Params("adjpresent") * 60) & " minutes (" & Request.Params("adjabsent") + Request.Params("adjpresent") & " hours)"
            'lblAdjTardy.Text = (Math.Round(CType(Request.Params("adjtardy"), Decimal), 2) * 60) & " minutes (" & Math.Round(CType(Request.Params("adjtardy"), Decimal), 2) & " hours)"

            lblActPresent.Text = Request.Params("actpresent")
            lblActAbsent.Text = Request.Params("actabsent")
            If Not Request.Params("acttardy") = "" Then
                lblActTardy.Text = Math.Round(CType(Request.Params("acttardy"), Decimal), 2)
            Else
                lblActTardy.Text = ""
            End If
            If Not Request.Params("adjpresent") = "" Then
                lblAdjPresent.Text = Request.Params("adjpresent")
            Else
                lblAdjPresent.Text = ""
            End If
            If Not Request.Params("actabsent") = "" Then
                decActualAbsent = Math.Round(CType(Request.Params("actabsent"), Decimal), 2)
            End If
            If Not Request.Params("adjabsent") = "" Then
                decAdjustAbsent = Math.Round(CType(Request.Params("adjabsent"), Decimal), 2)
            End If
            If decActualAbsent > 0 And decAdjustAbsent > 0 Then
                lblAdjAbsent.Text = decActualAbsent + decAdjustAbsent
            End If
            If Not Request.Params("adjtardy") = "" Then
                lblAdjTardy.Text = Math.Round(CType(Request.Params("adjtardy"), Decimal), 2)
            End If
            'If Request.Params("adjabsent") > 0 Then
            '    lblTardyPenalty.Text = Math.Abs(Request.Params("adjabsent") * 60 - Math.Round(CType(Request.Params("actabsent"), Decimal), 2) * 60).ToString & " minutes"
            'Else
            '    lblTardyPenalty.Text = ""
            'End If
        End If
    End Sub
End Class
