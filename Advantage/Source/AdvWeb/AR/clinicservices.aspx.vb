﻿Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class ClinicServices
    Inherits BasePage
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough> Private Sub InitializeComponent()

    End Sub
    Protected StudentId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String

    Private mruProvider As MRURoutines
    Protected state As AdvantageSessionState
    Protected LeadId As String


    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

#Region "MRU Routines"
    Protected boolSwitchCampus As Boolean = False
    Private Sub Page_Init(sender As System.Object, e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)






        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region


    Private Sub BuildEnrollment()
        With ddlStuEnrollId
            .DataTextField = "Descrip"
            .DataValueField = "StuEnrollId"
            .DataSource = (New ClinicHoursFacade).GetAllEnrollments(StudentId)
            .DataBind()
        End With
    End Sub
    Private Sub BuildCourses()
        With ddlCourses
            .DataTextField = "Descrip"
            .DataValueField = "ReqId"
            .DataSource = (New ClinicHoursFacade).GetAllCoursesByEnrollment(ddlStuEnrollId.SelectedValue)
            .DataBind()
            .Items.Insert(0, New ListItem("All Courses", "All Courses"))
            .SelectedIndex = 0
        End With
    End Sub



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Dim objCommon As New CommonUtilities
        Dim dt As DataTable
        'Dim resourceId As Integer

        'resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(534) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If
        End With


        '''''''''''''''''' Call to get student and lead ends here '''''''''''''''''



        If Not Page.IsPostBack Or MyBase.uSearchEntityControlId IsNot Nothing Then
            BuildEnrollment()
            BuildCourses()
            txtStuEnrollId.Text = ddlStuEnrollId.SelectedValue
            txtRequired.Text = "500"
            txtCompleted.Text = ""
            txtRemaining.Text = "All"
            Dim strMessage As String
            strMessage = "Please check to see if courses has been defined for the program version" & vbCrLf
            strMessage &= "using program version definition page" & vbCrLf
            Try
                dt = (New ExamsFacade).GetClinicServicesAndHoursByStudent(ddlStuEnrollId.SelectedValue, 500, "", "All") '(New ClinicHoursFacade).GetAllClinicServices(ddlStuEnrollId.SelectedValue, "", "All", "Lab Work")
                Dim stuEnrollInfo As New StudentEnrollmentInfo
                stuEnrollInfo = (New StuEnrollFacade).GetStudentDetails(ddlStuEnrollId.SelectedValue)
                lblStudentName.Text = (New StuEnrollFacade).StudentName(StudentId)
                With stuEnrollInfo
                    lblEnrollmentName.Text = .ProgramVersion
                    lblEnrollmentStatus.Text = .StatusCode
                    lblStartDate.Text = .StartDate
                    lblGD.Text = .ExpGradDate
                End With
                pnlStudentInfo.Visible = True
                btnPrint.Visible = True
                btnExportToExcel.Visible = True
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                DisplayErrorMessage(strMessage)
                btnPrint.Visible = False
                btnExportToExcel.Visible = False
                pnlStudentInfo.Visible = False
                Exit Sub
            End Try
            For Each dr As DataRow In dt.Rows
                If dr("Completed") Is System.DBNull.Value Then
                    dr("Completed") = "0.00"
                    dr("Remaining") = dr("Required") - dr("Completed")
                Else
                    If dr("Remaining") = "0" Then
                        dr("Remaining") = "0.00"
                    End If
                End If
                dr("Required") = CType(dr("Required"), Decimal)
            Next
            Try
                dgClinicServices.DataSource = dt
                dgClinicServices.DataBind()
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'For Ross Demo Do Nothing
            End Try
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, studentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, studentId, resourceId.ToString)
            'End If
        End If
        btnPrint.Attributes.Add("onClick", "getPrint('ContentMain2_print_area');")
        btnExportToExcel.Attributes.Add("onClick", "exporttoexcel();")
    End Sub
    Protected Sub ddlStuEnrollId_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlStuEnrollId.SelectedIndexChanged
        BuildCourses()
    End Sub
    Private Sub DisplayErrorMessage(errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub btnBuildList_Click(sender As Object, e As System.EventArgs) Handles btnBuildList.Click
        Dim dt As New DataTable
        Dim strMessage As String
        strMessage = "Please check to see if courses has been defined for the program version" & vbCrLf
        strMessage &= "using program version definition page" & vbCrLf
        Try
            If ddlCourses.SelectedValue = "All Courses" Then
                txtStuEnrollId.Text = ddlStuEnrollId.SelectedValue
                txtRequired.Text = "500"
                txtCompleted.Text = ""
                txtRemaining.Text = "All"
                Try
                    dt = (New ExamsFacade).GetClinicServicesAndHoursByStudent(ddlStuEnrollId.SelectedValue, 500, "", "All")
                    Dim StuEnrollInfo As New StudentEnrollmentInfo
                    StuEnrollInfo = (New StuEnrollFacade).GetStudentDetails(ddlStuEnrollId.SelectedValue)
                    lblStudentName.Text = (New StuEnrollFacade).StudentName(StudentId) 'CType(Header1.FindControl("txtStudentName"), TextBox).Text
                    With StuEnrollInfo
                        lblEnrollmentName.Text = .ProgramVersion
                        lblEnrollmentStatus.Text = .StatusCode
                        lblStartDate.Text = .StartDate
                        lblGD.Text = .ExpGradDate
                    End With
                    btnPrint.Visible = True
                    btnExportToExcel.Visible = True
                    pnlStudentInfo.Visible = True
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    DisplayErrorMessage(strMessage)
                    btnPrint.Visible = False
                    btnExportToExcel.Visible = False
                    pnlStudentInfo.Visible = False
                    Exit Sub
                End Try
            Else
                'ds = (New ClinicHoursFacade).GetAllClinicServices(ddlStuEnrollId.SelectedValue, ddlCourses.SelectedValue, "", "Lab Work")
                txtStuEnrollId.Text = ddlStuEnrollId.SelectedValue
                txtRequired.Text = "500"
                txtCompleted.Text = ddlCourses.SelectedValue
                txtRemaining.Text = ""
                Try
                    dt = (New ExamsFacade).GetClinicServicesAndHoursByStudent(ddlStuEnrollId.SelectedValue, 500, ddlCourses.SelectedValue, "")
                    Dim StuEnrollInfo As New StudentEnrollmentInfo
                    StuEnrollInfo = (New StuEnrollFacade).GetStudentDetails(ddlStuEnrollId.SelectedValue)
                    'lblStudentName.Text = CType(Header1.FindControl("txtStudentName"), TextBox).Text
                    lblStudentName.Text = (New StuEnrollFacade).StudentName(StudentId)
                    With StuEnrollInfo
                        lblEnrollmentName.Text = .ProgramVersion
                        lblEnrollmentStatus.Text = .StatusCode
                        lblStartDate.Text = .StartDate
                        lblGD.Text = .ExpGradDate
                    End With
                    btnPrint.Visible = True
                    btnExportToExcel.Visible = True
                    pnlStudentInfo.Visible = True
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    DisplayErrorMessage(strMessage)
                    btnPrint.Visible = False
                    btnExportToExcel.Visible = False
                    pnlStudentInfo.Visible = False
                    Exit Sub
                End Try
            End If
            For Each dr As DataRow In dt.Rows
                If dr("Completed") Is System.DBNull.Value Then
                    dr("Completed") = "0.00"
                    dr("Remaining") = dr("Required") - dr("Completed")
                Else
                    If dr("Remaining") = "0" Then
                        dr("Remaining") = "0.00"
                    End If
                End If
            Next
            dgClinicServices.DataSource = dt
            dgClinicServices.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            dgClinicServices.DataSource = Nothing
            dgClinicServices.DataBind()
        End Try
    End Sub
    Protected Sub btnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles btnExportToExcel.Click
    End Sub
    Protected Sub dgClinicServices_ItemCreated(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgClinicServices.ItemCreated
        Dim oControl As Control
        For Each oControl In dgClinicServices.Controls(0).Controls
            If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
                CType(oControl, DataGridItem).Cells(0).Text = "Clinic Services-" & ddlStuEnrollId.SelectedItem.Text & "-" & ddlCourses.SelectedItem.Text
            End If
        Next
    End Sub
    Protected Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
        'pnlStudentInfo.Visible = True
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        BindToolTip("ddlStuEnrollId")
        BindToolTip("ddlCourses")
    End Sub
End Class
