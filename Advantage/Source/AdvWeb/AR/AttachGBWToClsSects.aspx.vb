﻿Imports System.Diagnostics
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections

Partial Class AttachGBWToClsSects
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents chkStatus As CheckBox
    Protected WithEvents dlstCampGrps As DataList
    Protected WithEvents btnhistory As Button
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim campusId As String
        Dim userId As String
        '        Dim m_Context As HttpContext
        Dim resourceId As Integer

        Dim advantageUserState As User = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

        'Check if this page still exists in the menu while switching campus
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        '   disable History button at all time
        'Header1.EnableHistoryButton(False)

        If Not Page.IsPostBack Then
            'Dim objcommon As New CommonUtilities
            'Populate the Activities Datagrid with the person who is logged in Activities
            'Default the Selection Criteria for the activity records
            'ViewState("Term") = ""
            ViewState("MODE") = "NEW"
            ViewState("ClsSectIdGuid") = ""
            ViewState("SelectedIndex") = "-1"
            'ViewState("Instructor") = "E62FE198-18D9-4244-821D-E89A740F4160"
            ViewState("Instructor") = userId
            PopulateTermDDL()
            PopulateStatusDDL()
            ViewState("MODE") = "NEW"
            ddlStatus.SelectedIndex = 1
            ViewState("Status") = ddlStatus.SelectedItem.Value.ToString()
            If (ViewState("Status") <> "") Then
                PopulateGrdBKWgtsDDL()
            End If
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
        Else
            'If Session("JustClosed") = "Closed" Then
            '    MoveRecordToRHS(Session("ActivityAssignmentGuid"))
            '    Session("JustClosed") = ""
            'End If
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
        End If

    End Sub
    'Private Sub PopulateTermDDL()
    '    '
    '    '   Get Degrees data to bind the CheckBoxList
    '    '
    '    Dim facade As New AttchGBWToClsSectsFacade

    '    ' Bind the Dataset to the CheckBoxList
    '    With ddlTerm
    '        .DataSource = facade.GetAllTerms()
    '        .DataTextField = "TermDescrip"
    '        .DataValueField = "TermId"
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub PopulateTermDDL()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New AttchGBWToClsSectsFacade
        ' Bind the Dataset to the CheckBoxList
        With ddlTerm
            .DataSource = facade.GetAllTerms(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTerm.SelectedIndexChanged
        'ViewState("Status") = ddlStatus.SelectedItem.Value.ToString()
        'If (ViewState("Status") <> "") Then
        '    PopulateGrdBKWgtsDDL()
        'End If
    End Sub

    Private Sub PopulateStatusDDL()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New AttchGBWToClsSectsFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlStatus
            .DataSource = facade.GetAllStatuses()
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        ViewState("Status") = ddlStatus.SelectedItem.Value.ToString()
        If (ViewState("Status") <> "") Then
            PopulateGrdBKWgtsDDL()
            lbxAvailClsSects.Items.Clear()
            lbxSelectedClsSects.Items.Clear()
        End If
    End Sub

    Private Sub PopulateGrdBkWgtsDDL()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New AttchGBWToClsSectsFacade
        ' Bind the Dataset to the CheckBoxList
        With ddlDescrip
            .DataSource = facade.GetGrdBkWgts(CType(ViewState("Status"), String), CType(ViewState("Instructor"), String))
            .DataTextField = "Descrip"
            .DataValueField = "InstrGrdBkWgtId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub ddlDescrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDescrip.SelectedIndexChanged
        'Dim facade As New AttchGBWToClsSectsFacade
        'ViewState("Descrip") = ddlDescrip.SelectedItem.Value.ToString()
        'If (ViewState("Descrip") <> "") Then
        '    facade.GetInstrClsSects()
        'End If

    End Sub
    Private Sub btnGetClsSects_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetClsSects.Click
        Dim ds As DataSet
        'Dim ds2 As New DataSet
        Dim facade As New AttchGBWToClsSectsFacade
        Dim campusId As String

        campusId = HttpContext.Current.Request.Params("cmpid")
        lbxAvailClsSects.Items.Clear()
        lbxSelectedClsSects.Items.Clear()
        'ViewState("Term")       = ddlTerm.SelectedItem.Value.ToString
        'ViewState("ClsSection") = ddlStatus.SelectedItem.Value.ToString
        'ViewState("GrdBkWgt")   = ddlDescrip.SelectedItem.Value.ToString

        Dim term As String = ddlTerm.SelectedItem.Value.ToString
        'Dim clsSection As String = ddlStatus.SelectedItem.Value.ToString
        Dim gradebook As String = ddlDescrip.SelectedItem.Value.ToString

        'Validations
        If Guid.TryParse(term, New Guid()) = False Then
            DisplayErrorMessage("You must select a Term")
            Debug.WriteLine("Term has the following bas format: {0}", term)
            Exit Sub
        End If

        If Guid.TryParse(gradebook, New Guid()) = False Then
            DisplayErrorMessage("You must select a Grade Book. " + Environment.NewLine + " If you had not setup a Grade Book, please go to option: 'Set Up Grade Book Weightings' and create at least one, before you use this page.")
            Debug.WriteLine("Term has the following bas format: {0}", term)
            Exit Sub
        End If

        ds = facade.GetAvailSelectedClsSections(CType(ViewState("Instructor"), String), term, gradebook, campusId)

        'Bind the Campuses List Box
        BindClsSectsListBox(ds)

        If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If


        Session("SelectedClsSects") = ds.Tables("SelectedClsSects")
    End Sub

    Private Sub BindClsSectsListBox(ByRef ds As DataSet)
        lbxAvailClsSects.DataSource = ds.Tables("AvailClsSects")
        lbxAvailClsSects.DataTextField = "CourseSectDescrip"
        lbxAvailClsSects.DataValueField = "ClsSectionId"
        lbxAvailClsSects.DataBind()

        lbxSelectedClsSects.DataSource = ds.Tables("SelectedClsSects")
        lbxSelectedClsSects.DataTextField = "CourseSectDescrip"
        lbxSelectedClsSects.DataValueField = "ClsSectionId"
        lbxSelectedClsSects.DataBind()
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub


    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        If lbxAvailClsSects.SelectedIndex >= 0 Then
            Dim selIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            'Dim ds As New DataSet
            ' Dim objListGen As New DataListGenerator

            'Add selected item to the relevant assigned list. 

            lbxSelectedClsSects.Items.Add(New ListItem(lbxAvailClsSects.SelectedItem.Text, lbxAvailClsSects.SelectedItem.Value))
            dt = CType(Session("SelectedClsSects"), DataTable)
            dr = dt.NewRow()
            dr("ClsSectionId") = lbxAvailClsSects.SelectedItem.Value.ToString
            dt.Rows.Add(dr)
            Session("SelectedClsSects") = dt
            'Remove the item from the lbxAvailClsSects list
            selIndex = lbxAvailClsSects.SelectedIndex
            lbxAvailClsSects.Items.RemoveAt(selIndex)
            'ds = objListGen.SummaryListGenerator()
            'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
            'PopulateDataList(ds)
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click

        Dim selIndex As Integer
        'Dim iClsSectId As String
        Dim dt As DataTable
        Dim dr As DataRow
        'Dim ds As New DataSet
        'Dim objListGen As New DataListGenerator

        If lbxSelectedClsSects.SelectedIndex >= 0 Then
            'Add selected item to the lbxAvailClsSects list. In order to do this we
            'have to use the FldsSelectCamps to find out the 'FldId for the selected item.
            lbxAvailClsSects.Items.Add(New ListItem(lbxSelectedClsSects.SelectedItem.Text, lbxSelectedClsSects.SelectedItem.Value))
            dt = CType(Session("SelectedClsSects"), DataTable)
            dr = dt.Rows.Find(lbxSelectedClsSects.SelectedItem.Value.ToString)
            'iClsSectId = dr("ClsSectionId").ToString
            If dr.RowState <> DataRowState.Added Then
                'Mark the row as deleted
                dr.Delete()
            Else
                dt.Rows.Remove(dr)
            End If
            'Remove the item from the lbxSelectedClsSects list
            selIndex = lbxSelectedClsSects.SelectedIndex
            lbxSelectedClsSects.Items.RemoveAt(selIndex)
            Session("SelectedClsSects") = dt

            'ds = objListGen.SummaryListGenerator()
            'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
            'PopulateDataList(ds)

        End If
    End Sub

    Private Sub btnAddAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddAll.Click
        Dim i As Integer
        '        Dim SelIndex As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        'Dim ds As New DataSet
        'Dim objListGen As New DataListGenerator

        'Loop thru the available campus list and add all the entries to the selected campus list
        'And simultaneously remove all the campuses from avail camp list
        'The code in this sub should only execute if an item is selected
        'in the lbxAvailClsSects listbox.

        For i = 0 To lbxAvailClsSects.Items.Count - 1

            lbxSelectedClsSects.Items.Add(New ListItem(lbxAvailClsSects.Items(i).Text, lbxAvailClsSects.Items(i).Value))
            dt = CType(Session("SelectedClsSects"), DataTable)
            dr = dt.NewRow()
            dr("ClsSectionId") = lbxAvailClsSects.Items(i).Value
            dt.Rows.Add(dr)
            Session("SelectedClsSects") = dt
        Next
        'Remove all the items from the lbxAvailClsSects list
        lbxAvailClsSects.Items.Clear()

    End Sub

    Private Sub btnRemoveAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemoveAll.Click
        Dim i As Integer
        'Dim iClsSectId As String
        Dim dt As DataTable
        Dim dr As DataRow
        'Dim ds As New DataSet
        'Dim objListGen As New DataListGenerator

        For i = 0 To lbxSelectedClsSects.Items.Count - 1

            lbxAvailClsSects.Items.Add(New ListItem(lbxSelectedClsSects.Items(i).Text, lbxSelectedClsSects.Items(i).Value))
            dt = CType(Session("SelectedClsSects"), DataTable)
            dr = dt.Rows.Find(lbxSelectedClsSects.Items(i).Value)
            'iClsSectId = dr("ClsSectionId").ToString
            If dr.RowState <> DataRowState.Added Then
                'Mark the row as deleted
                dr.Delete()
            Else
                dt.Rows.Remove(dr)
            End If

        Next
        'Remove all the items from the lbxSelectedClsSects list
        lbxSelectedClsSects.Items.Clear()

        'ds = objListGen.SummaryListGenerator()
        'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
        'PopulateDataList(ds)

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        '        Dim i As Integer
        '   Dim strGuid As String
        Dim dt1, dt2 As DataTable
        Dim dr As DataRow
        'Dim objCommon As New CommonUtilities
        Dim iClsSectId As String
        'Dim ds1 As New DataSet
        'Dim objListGen As New DataListGenerator
        'Dim sw As New StringWriter
        'Dim dv2 As New DataView
        '   Dim storedGuid As String
        Dim sGrdBkWgtId As String
        Dim facade As New AttchGBWToClsSectsFacade



        Try
            '' CODE BELOW HANDLES A FRESH INSERT INTO THE TABLE(S)
            'If ViewState("MODE") = "NEW" Then


            '    For i = 0 To lbxSelectedClsSects.Items.Count - 1
            '        'INSERT GrdBkWgtId into arClassSections using the clssect id and the Term id

            '        ID = lbxSelectedClsSects.Items(i).Value


            '        'Insert GrdBkWgt into arClsSections Table
            '        facade.UpdateClsSection(sGrdBkWgtId, ID, ViewState("Term"))


            '    Next
            '    dt1 = Session("FldsSelectCamps")
            '    dt1.AcceptChanges()

            '    objCommon.SetBtnState(Form1, "EDIT")
            '    ViewState("MODE") = "EDIT"

            '    ' CODE BELOW HANDLES AN UPDATE INTO THE TABLE(S)
            'ElseIf viewstate("MODE") = "EDIT" Then


            'Run getchanges method on the dataset to see which rows have changed.
            'This section handles the changes to the Advantage Required fields
            dt1 = CType(Session("SelectedClsSects"), DataTable)
            dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted)
            If Not IsNothing(dt2) Then
                For Each dr In dt2.Rows
                    If dr.RowState = DataRowState.Added Then
                        iClsSectId = dr("ClsSectionId").ToString

                        sGrdBkWgtId = ddlDescrip.SelectedItem.Value.ToString


                        'Insert GrdBkWgt into arClsSections Table
                        facade.UpdateClsSection(sGrdBkWgtId, iClsSectId)


                    ElseIf dr.RowState = DataRowState.Deleted Then
                        iClsSectId = dr("ClsSectionId", DataRowVersion.Original).ToString
                        sGrdBkWgtId = ""
                        facade.UpdateClsSection(sGrdBkWgtId, iClsSectId)
                        'Delete row from ResTblFlds table. It is important to remember that when a row
                        'is marked as deleted we cannot access the value from the current version of the
                        'row. We have to use the DataRowVersion enumeration to obtain the original value
                        'stored in the row.

                    End If
                Next
            End If
            dt1.AcceptChanges()
            Session("FldsSelectCamps") = dt1

            'End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnGetClsSects)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnAddAll)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(btnRemoveAll)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip("ddlTerm")
        BindToolTip("ddlStatus")
        BindToolTip("ddlDescrip")
    End Sub


End Class