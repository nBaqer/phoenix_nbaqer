﻿
Imports System.Collections
Imports System.Data
Imports System.Xml
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.Common


Namespace AdvWeb.AR

    Partial Class ArRegisterStdsManually
        Inherits BasePage
        Protected CampusId As String
        Protected UserId As String
        Protected MyAdvAppSettings As AdvAppSettings

        Private pObj As New UserPagePermissionInfo

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim advantageUserState As User = AdvantageSession.UserState

            'Dim fac As New UserSecurityFacade
            Dim resourceId As String

            'disable history button
            'Header1.EnableHistoryButton(False)
            'txtAcademicYearId.Attributes.Add("onClick", "needToConfirm = false;__doPostBack('lbtHasDisb','')")
            resourceId = HttpContext.Current.Request.Params("resid")
            CampusId = AdvantageSession.UserState.CampusId.ToString
            UserId = AdvantageSession.UserState.UserId.ToString

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If


            ' US3776 2/25/2013 Janet Robinson popup showing if it had shown previously
            ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False

            If Not Page.IsPostBack Then
                'Dim obj common As New CommonUtilities

            Else

            End If
            If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If
            btnDelete.Enabled = False
            btnNew.Enabled = False
        End Sub
        Private Sub lbtHasDisb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtHasDisb.Click
            PopulateTermDdl()
        End Sub
        Private Sub PopulateTermDdl()
            '
            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New RegFacade
            If Not facade.IsStudentHold(CType(Session("StuEnrollment"), String)) = "1" Then
                ''
                With lbxTerms
                    .DataSource = nothing
                    .DataBind()
                    .Items.Clear()
                    .DataSource = facade.GetCurrentAndFutureTermsForStudent(CType(Session("StuEnrollment"), String), CampusId)
                    .DataTextField = "TermDescrip"
                    .DataValueField = "TermId"
                    .DataBind()
                    .Items.Insert(0, New ListItem("All Terms", ""))
                End With
                if (lbxTerms.Items.Count > 0)
                    lbxTerms.SelectedIndex = 0
                End If
            Else
                DisplayErrorMessage("Cannot display student information as the student group registration is put on hold")
            End If
        End Sub

        Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, _
                                       ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, _
                                       ByVal hourtype As String)

            If enrollid = "" Then enrollid = CType(Session("StuEnrollment"), String)
            If fullname = "" Then fullname = CType(Session("StudentName"), String)

            txtStuEnrollmentId.Value = enrollid
            Session("StuEnrollment") = enrollid
            Session("StudentName") = fullname

            Call PopulateTermDdl()

            pnlGrid.Visible = False

        End Sub

        Private Sub btnGetStdCourses_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetStdCourses.Click
            Try


                Dim facade As New RegStdManuallyFacade
                Dim dt As DataTable
                Dim strTerms As String = String.Empty
                For i As Integer = 0 To lbxTerms.Items.Count - 1
                    If lbxTerms.Items(i).Selected = True And lbxTerms.Items(i).Value <> String.Empty Then
                        strTerms += If(strTerms = String.Empty, String.Format("'{0}'", lbxTerms.Items(i).Value), String.Format(", '{0}' ", lbxTerms.Items(i).Value))
                    End If
                Next

                If Session("StudentName") Is Nothing Then
                    DisplayErrorMessage("Unable to find data. Student field cannot be empty.")
                    Exit Sub
                End If
                ViewState("StuEnrollId") = txtStuEnrollmentId.Value

                'We should not allow registration for drop students as well as any other out-of-school statuses
                If Not facade.IsEnrollmentInSchoolStatus(txtStuEnrollmentId.Value) Then
                    DisplayErrorMessage("This enrollment cannot be scheduled for classes because it is in an out-of-school status")
                Else
                    If strTerms = String.Empty Then ' A Term or Terms was/were selected

                        ''To fix mantis issue 17516: QA: "Bypass prerequisites" on register students manually is not showing all the class sections.
                        dt = facade.GetRemainingClassSections(txtStuEnrollmentId.Value, chkReqs.Checked, CampusId, ChkAllCourses.Checked, UserId)
                        With dgdTransferGrade
                            If New DataView(dt, "", "CampDescrip,TermDescrip,TermStartDate,Code,Descrip,Shift,Instructor", DataViewRowState.CurrentRows).Count > 0 Then
                                .DataSource = New DataView(dt, "", "CampDescrip,TermDescrip,TermStartDate,Code,Descrip,Shift,Instructor", DataViewRowState.CurrentRows)
                                .DataBind()
                                trNoData.Visible = False
                            Else
                                .DataSource = Nothing
                                .DataBind()
                                DisplayErrorMessage("There are no classes scheduled for this Term.")
                                Exit Sub
                            End If
                        End With
                    Else
                        'A Term or Terms was/where selected in the list of Term
                        dt = facade.GetRemainingClassSections(txtStuEnrollmentId.Value, strTerms, chkReqs.Checked, CampusId, ChkAllCourses.Checked, UserId)

                        'dt = FilterClassesWithOutQuorum(dt)

                        Dim drRows() As DataRow
                        Dim drSingleRow As DataRow
                        Dim dtCourse As DataTable
                        dtCourse = dt.Clone
                        drRows = dt.Select("TermID in (" + strTerms + ")")

                        For Each drSingleRow In drRows
                            dtCourse.ImportRow(drSingleRow)
                        Next

                        ' Binding with the Table in the interface
                        With dgdTransferGrade
                            If New DataView(dt, "", "CampDescrip,TermDescrip,TermStartDate,Code,Descrip,Shift,Instructor", DataViewRowState.CurrentRows).Count > 0 Then
                                .DataSource = New DataView(dt, "", "CampDescrip,TermDescrip,TermStartDate,Code,Descrip,Shift,Instructor", DataViewRowState.CurrentRows)
                                .DataBind()
                                trNoData.Visible = False
                            Else
                                .DataSource = Nothing
                                .DataBind()
                                'trNoData.Visible = True
                                'lblNoData.Text = "No Data Found"
                                DisplayErrorMessage("There are no classes scheduled for this Term.")
                                Exit Sub
                            End If
                        End With
                    End If

                    pnlGrid.Visible = True

                    Dim sb As StringBuilder = New StringBuilder()

                    For Each dr In dt.Rows
                        Dim stuEnrollId As String = dr("StuEnrollId").ToString()
                        Dim clsSectionId As String = dr("ClsSectionId").ToString()


                        If New RegFacade().GetTransfersForStudent_SP(stuEnrollId, clsSectionId) = True Then
                            Dim sofar As String = sb.ToString()
                            If sofar.IndexOf(dr("Descrip").ToString(), StringComparison.Ordinal) = -1 Then
                                sb.Append(dr("Descrip").ToString() & ", ")
                            End If
                        End If
                    Next

                    If sb.Length > 0 Then

                        Dim st As String = sb.ToString()
                        If st.Substring(st.Length - 2, 2) = ", " Then
                            st = st.Substring(0, st.Length - 2)
                        End If

                        Dim sb2 As StringBuilder = New StringBuilder()
                        sb2.Append("This student has at least one enrollment with a transfer grade for the following courses. ")
                        sb2.Append("If you want to apply that transfer grade to the existing student enrollment, navigate to Academics/Common Tasks/Grade Posting/Record Transfer Grades for Student" & vbCrLf)
                        DisplayErrorMessage(sb2.ToString() & vbCrLf & st)
                    End If
                End If





            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                CommonWebUtilities.DisplayErrorInMessageBox(Page, ex.Message)
            End Try
        End Sub

        Private Sub GetRemainingStdClassSections()
            ''To fix issue 15901
            ''The courses are filtered when , a term is selected in the list.  Else, all the Courses are listed.

            Dim facade As New RegStdManuallyFacade
            Dim dt As DataTable

            Dim strTerms As String = String.Empty
            For i As Integer = 0 To lbxTerms.Items.Count - 1
                If lbxTerms.Items(i).Selected = True And lbxTerms.Items(i).Value <> "" Then
                    If strTerms = String.Empty Then
                        strTerms = "'" & lbxTerms.Items(i).Value & "'"
                    Else
                        strTerms = strTerms & ",'" & lbxTerms.Items(i).Value & "'"
                    End If
                End If
            Next

            If strTerms = String.Empty Then
                ''To fix mantis issue 17516: QA: "Bypass prerequisites" on register students manually is not showing all the class sections.
                dt = facade.GetRemainingClassSections(txtStuEnrollmentId.Value, chkReqs.Checked, CampusId, ChkAllCourses.Checked, UserId)
                With dgdTransferGrade
                    If New DataView(dt, "", "CampDescrip,TermDescrip,TermStartDate,Code,Descrip,Shift,Instructor", DataViewRowState.CurrentRows).Count > 0 Then
                        .DataSource = New DataView(dt, "", "CampDescrip,TermDescrip,TermStartDate,Code,Descrip,Shift,Instructor", DataViewRowState.CurrentRows)
                        .DataBind()
                        trNoData.Visible = False
                    Else
                        .DataSource = Nothing
                        .DataBind()
                        DisplayErrorMessage("There are no classes scheduled for this Term.")
                        Exit Sub
                    End If
                End With
            Else

                dt = facade.GetRemainingClassSections(txtStuEnrollmentId.Value, strTerms, chkReqs.Checked, CampusId, ChkAllCourses.Checked, UserId)
                Dim drRows() As DataRow, drSingleRow As DataRow
                Dim dtCourse As DataTable
                dtCourse = dt.Clone
                drRows = dt.Select("TermID in (" + strTerms + ")")

                For Each drSingleRow In drRows
                    dtCourse.ImportRow(drSingleRow)
                Next

                With dgdTransferGrade
                    If New DataView(dtCourse, "", "CampDescrip,TermDescrip,TermStartDate,Code,Descrip,Shift,Instructor", DataViewRowState.CurrentRows).Count > 0 Then
                        .DataSource = New DataView(dtCourse, "", "CampDescrip,TermDescrip,TermStartDate,Code,Descrip,Shift,Instructor", DataViewRowState.CurrentRows)
                        .DataBind()
                        trNoData.Visible = False
                    Else
                        .DataSource = Nothing
                        .DataBind()
                        'trNoData.Visible = True
                        'lblNoData.Text = "No Data Found"
                        DisplayErrorMessage("There are no classes scheduled for this Term.")
                        Exit Sub
                    End If
                End With
            End If
        End Sub

        Private Sub DisplayErrorMessage(ByVal errorMessage As String)

            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End Sub
        Private Sub DisplayInfoInMessageBox(ByVal message As String)

            '   Display error in message box in the client
            CommonWebUtilities.DisplayInfoInMessageBox(Page, message)
        End Sub
        ' US3776 2/25/2013 Janet Robinson added field to indicate calling program
        Protected Sub ShowScheduleConflict(ByVal clsSectId As String, ByVal stuEnrollId As String, ByVal classes As String, ByVal termId As String, ByVal boolconflictinsideterm As Boolean, Optional ByVal buttonState As String = "", Optional ByVal boolConflictClassIgnoreRoom As Boolean = False, Optional ByVal boolRegStdManPage As Boolean = True)
            Dim studentId As String = String.Empty
            Dim strUrl As String = "ViewScheduleConflicts.aspx?resid=909&mod=AR" + "&ClsSectionId=" + clsSectId.ToString + "&StuEnrollId=" + stuEnrollId + "&Student=" + studentId + "&buttonState=" + buttonState + "&Classes=" + Server.UrlEncode(classes) + "&TermId=" + termId.ToString + "&boolconflictinsideterm=" + boolconflictinsideterm.ToString + "&CampusId=" + XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString + "&boolConflictClassIgnoreRoom=" + boolConflictClassIgnoreRoom.ToString + "&boolRegStdManPage=" + boolRegStdManPage.ToString
            ScheduleConflictWindow.Visible = True
            ScheduleConflictWindow.Windows(0).NavigateUrl = strUrl
            ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = True
        End Sub
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            '   if GrdSystem is nothing do an insert. Else do an update
            Dim iitems As DataGridItemCollection
            Dim iitem As DataGridItem
            Dim i As Integer
            Dim sId As String
            Dim courseErrStr As String
            Dim code As String
            Dim descrip As String
            Dim sectionId As String
            Dim termId As String
            Dim previousTermId As String = String.Empty
            Dim courseId As String
            Dim previousCourseId As String = String.Empty
            Dim selected As Boolean = False
            Const boolconflictinsideterm As Boolean = False
            Const boolConflictClassIgnoreRoom As Boolean = False
            Dim sameCourseError As Boolean = False
            Dim sameCourseDup As Boolean = False
            courseErrStr = "Cannot register Student because of the following conflicts: " & vbCrLf & vbCrLf
            Dim allClsSections As String = String.Empty
            Dim dsScheduleDupConflicts As DataSet

            ' Save the data-grid items in a collection.
            iitems = dgdTransferGrade.Items
            Try
                sId = txtStuEnrollmentId.Value

                Dim dsScheduleConflicts As DataSet
                Dim strClsSectionList As String = ""
                Dim strCourseDescrip As String = ""
                Dim strTermId As String = ""



                For i = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)



                    If (CType(iitem.FindControl("chkDrop"), CheckBox).Checked = True) Then
                        code = CType(iitem.FindControl("Code"), Label).Text
                        descrip = CType(iitem.FindControl("Descrip"), Label).Text
                        sectionId = CType(iitem.FindControl("txtClsSectionId"), TextBox).Text
                        'Shift = CType(iitem.FindControl("Shift"), Label).Text
                        'Section = CType(iitem.FindControl("Section"), Label).Text
                        courseId = CType(iitem.FindControl("lblReqId"), Label).Text
                        termId = CType(iitem.FindControl("lblTermId"), Label).Text
                        selected = True
                        allClsSections &= sectionId & ","

                        'Test if the selected class id has vacant
                        'Dim info As ClassQuorumInfo = RegStdManuallyFacade.GetClassQuorum(sectionId)
                        'If (info.IsSeatAvailable = False) Then
                        '    Dim err As String = String.Format("The course ""{0}"" has reached the maximum number of students allowed. Advantage can not register the student",
                        '                                        descrip)
                        '    DisplayErrorMessage(err)
                        '    Exit Sub

                        'End If



                        dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(sectionId, txtStuEnrollmentId.Value.ToString.Trim)
                        If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 And MyAdvAppSettings.AppSettings("TimeClockClassSection", CampusId).ToString.ToLower = "yes" Then
                            strClsSectionList &= sectionId & ","
                            strCourseDescrip &= descrip & ","
                            strTermId &= termId & ","
                        End If

                        If lbxTerms.SelectedIndex = 0 Then
                            If (courseId = previousCourseId) Then
                                If sameCourseDup = False Then
                                    courseErrStr = courseErrStr + code + " " + descrip + " selected more than once" & vbCrLf & vbCrLf
                                    sameCourseDup = True
                                End If
                                sameCourseError = True
                            Else
                                sameCourseDup = False
                            End If

                            If (iitems.Count > 0) Then
                                If (CType(iitems.Item(i).FindControl("chkDrop"), CheckBox).Checked = True) Then
                                    previousCourseId = CType(iitems.Item(i).FindControl("lblReqId"), Label).Text
                                End If
                            End If

                        Else
                            If (courseId = previousCourseId) And (termId = previousTermId) Then
                                If sameCourseDup = False Then
                                    courseErrStr = courseErrStr + code + " " + descrip + " selected more than once" & vbCrLf & vbCrLf
                                    sameCourseDup = True
                                End If
                                sameCourseError = True
                            Else
                                sameCourseDup = False
                            End If

                            If (iitems.Count > 0) Then
                                If (CType(iitems.Item(i).FindControl("chkDrop"), CheckBox).Checked = True) Then
                                    previousCourseId = CType(iitems.Item(i).FindControl("lblReqId"), Label).Text
                                    previousTermId = CType(iitems.Item(i).FindControl("lblTermId"), Label).Text
                                End If
                            End If
                        End If
                    End If
                Next



                If Not strClsSectionList.ToString.Trim = "" Then
                    strClsSectionList = Mid(strClsSectionList, 1, InStrRev(strClsSectionList, ",") - 1)
                    strCourseDescrip = Mid(strCourseDescrip, 1, InStrRev(strCourseDescrip, ",") - 1)
                    strTermId = Mid(strTermId, 1, InStrRev(strTermId, ",") - 1)
                End If

                'If none of the classes are selected then display a message
                If selected = False Then
                    DisplayErrorMessage("Please select one or more class sections to register the students for.")
                    Exit Sub
                End If

                If MyAdvAppSettings.AppSettings("TimeClockClassSection", CampusId).ToString.ToLower = "yes" Then
                    allClsSections = Mid(allClsSections, 1, InStrRev(allClsSections, ",") - 1)
                    dsScheduleDupConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistrationMan(allClsSections)
                    If Not dsScheduleDupConflicts Is Nothing AndAlso dsScheduleDupConflicts.Tables(0).Rows.Count >= 1 Then
                        For j As Integer = 0 To dsScheduleDupConflicts.Tables(0).Rows.Count - 1
                            courseErrStr = CType((courseErrStr + dsScheduleDupConflicts.Tables(0).Rows.Item(j).Item(0) + " is scheduled at the same start and end date and same start and end time as another selected course" & vbCrLf & vbCrLf), String)
                        Next
                        sameCourseError = True
                    End If
                End If

                If sameCourseError = True Then
                    DisplayErrorMessage(courseErrStr)
                    Exit Sub

                ElseIf Not strClsSectionList.ToString.Trim = "" And MyAdvAppSettings.AppSettings("TimeClockClassSection", CampusId).ToString.ToLower = "yes" Then
                    ShowScheduleConflict(strClsSectionList.ToString, txtStuEnrollmentId.Value.ToString.Trim, strCourseDescrip, strTermId, boolconflictinsideterm, "", boolConflictClassIgnoreRoom) 'Show the radWindow
                Else
                    'Add Class with no schedule conflicts
                    AddCls(iitems, sId, strClsSectionList)
                    DisplayInfoInMessageBox("Student have been successfully registered.")
                    GetRemainingStdClassSections()
                    DataBind()
                    ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
                End If


            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

        ''' <summary>
        ''' The routine scan if the check-box is checked if checked proceed to register the enrollment
        ''' The new modification check all enrollment for the student that can register the curse.
        ''' </summary>
        ''' <param name="iitems">
        ''' Item from the list of class listed in the application 
        ''' </param>
        ''' <param name="sid">Enrollment Id</param>
        ''' <param name="strClsSectionList">
        ''' 
        ''' </param>
        ''' <remarks>
        ''' Added logic to register in all student enrollments that need the same class
        ''' </remarks>
        Private Sub AddCls(ByVal iitems As DataGridItemCollection, ByVal sid As String, ByVal strClsSectionList As String)
            Dim iitem As DataGridItem
            'Dim facade As New RegFacade
            Dim i As Integer
            Dim sectionId As String

            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkDrop"), CheckBox).Checked = True) Then

                    sectionId = CType(iitem.FindControl("txtClsSectionId"), TextBox).Text
                    'Comment added by Balaji on Oct 15 2011
                    'If class has schedule conflicts do not register the class
                    'if class has no schedule conflicts then register
                    If InStr(strClsSectionList, sectionId) >= 1 Then
                        Continue For
                    End If
                    ' Register class in all enrollments of the student that need the class.
                    Dim register As ArRegisterClass = New ArRegisterClass(sid, sectionId)
                    register.UserName = CType(Session("UserName"), String)
                    register.CampusId = CampusId
                    If register.ValidateRegistrationAdd() Then
                        register.RegisterClassWithEnrollments()

                        'facade.AddStdToClsSect(sectionId, sid, Session("UserName"), CampusId)
                        ' if this ClassSection is a OnLine On-line Course then update OnLine Enrollment
                        If OnLineLib.IsOnLineClassSection(sectionId) Then
                            Dim onLineInfo As OnLineInfo = (New OnLineFacade).GetOnLineInfoByStuEnrollId(sid)
                            OnLineLib.UpdateStudentOnLineEnrollment(onLineInfo, CType(Session("UserName"), String))
                        End If
                    End If
                End If
            Next
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnGetStdCourses)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
            BindToolTip()
        End Sub



        Private Function CheckIfStudentHasMetCcrSpeedRequirements(ByVal stuEnrollId As String, _
                                                                  ByVal courseId As String, _
                                                                  ByVal descrip As String) As String
            Dim boolIsStudentEligibleForRegisteration As Boolean
            Dim ccrErrorMessage As String = ""
            boolIsStudentEligibleForRegisteration = (New RegisterStudentsBR).CCR_CheckIfStudentMetRequirements(stuEnrollId, courseId)
            If boolIsStudentEligibleForRegisteration = False Then
                ccrErrorMessage &= "Student cannot be registered in course " & descrip & ", as the Student has not satisfied " & vbCrLf
                ccrErrorMessage &= "the prerequisites, mentor/proctored test requirements,  the student has exceeded the minimum course repetition limit (" & MyAdvAppSettings.AppSettings("CCR_RepeatSHCourses").ToString.Trim & " times)" & vbCrLf
                ccrErrorMessage &= "set for SH Level courses or the student failed the prerequisite course."
                Return ccrErrorMessage
            Else
                Return ""
            End If
        End Function

        Protected Sub dgdTransferGrade_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgdTransferGrade.ItemCreated
            Dim oControl As Control
            If MyAdvAppSettings Is Nothing Then
                MyAdvAppSettings = AdvAppSettings.GetAppSettings()
            End If
            For Each oControl In dgdTransferGrade.Controls(0).Controls
                If CType(oControl, DataGridItem).ItemType = ListItemType.Header Or CType(oControl, DataGridItem).ItemType = ListItemType.Item Or _
                   CType(oControl, DataGridItem).ItemType = ListItemType.AlternatingItem Then
                    If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        CType(oControl, DataGridItem).Cells(9).Visible = True
                    Else
                        CType(oControl, DataGridItem).Cells(9).Visible = False
                    End If
                End If
            Next
        End Sub
        Protected Sub dgdTransferGrade_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgdTransferGrade.ItemDataBound
            If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                Dim iitems As DataGridItemCollection
                Dim iitem As DataGridItem
                Dim descrip As String
                Dim courseId As String
                Dim intLoop As Integer
                'Dim errorMessage As String = ""
                Dim lblRegistrationDescrip As Label
                iitems = dgdTransferGrade.Items
                For intLoop = 0 To iitems.Count - 1
                    iitem = iitems.Item(intLoop)
                    courseId = CType(iitem.FindControl("lblReqId"), Label).Text
                    lblRegistrationDescrip = CType(iitem.FindControl("lblRegistration"), Label)
                    descrip = CType(iitem.FindControl("Descrip"), Label).Text
                    Dim ccrErrorMessage As String = CheckIfStudentHasMetCcrSpeedRequirements(txtStuEnrollmentId.Value, courseId, descrip)
                    If Not ccrErrorMessage.Trim = "" Then
                        lblRegistrationDescrip.Text = "No. " & ccrErrorMessage
                        CType(iitem.FindControl("chkDrop"), CheckBox).Enabled = False
                    Else
                        lblRegistrationDescrip.Text = "Yes"
                        CType(iitem.FindControl("chkDrop"), CheckBox).Enabled = True
                    End If
                Next

            End If

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim facade As New RegStdManuallyFacade
                Dim periodClassRoomList As List(Of PeriodClassRoom)
                Dim clsSectionId As String = DirectCast(e.Item.DataItem, DataRowView)("ClsSectionId").ToString()
                Dim dgdGrid As DataGrid = DirectCast(e.Item.FindControl("dgdPeriodClassRoom"), DataGrid)
                periodClassRoomList = facade.GetPeriodClassRoom(clsSectionId)

                dgdGrid.DataSource = periodClassRoomList
                dgdGrid.DataBind()
            End If

        End Sub


    End Class
End Namespace