﻿
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common.Tables

Namespace AdvWeb.AR
    Partial Class ArTransferPendingDialog
        Inherits Page

        Protected MyAdvAppSettings As AdvAppSettings

        Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

            'Get necessary values
            Dim transferByCopy As List(Of ArTransferGrades) = CType(AdvantageSession.TpTransferByCopyList, List(Of ArTransferGrades))
            Dim registerByCopy As List(Of ArResultsClass) = CType(AdvantageSession.TpRegisterByCopyList, List(Of ArResultsClass))
            Dim readyToTransfer As List(Of TransferRequirement) = CType(AdvantageSession.TpReadyToTransferList, List(Of TransferRequirement))
            Dim weirdList As List(Of TransferRequirement) = CType(AdvantageSession.TpWeirdThingList, List(Of TransferRequirement))

            If transferByCopy Is Nothing Then
                Return
            End If

            'Determine the configuration of Grade
            MyAdvAppSettings = AdvAppSettings.GetAppSettings()

            'Configure the page
            'Determine what buttons are show
            If (transferByCopy.Count > 0 Or registerByCopy.Count > 0) Then
                ButtonType1.Visible = True
                ButtonType2.Visible = False
            Else
                ButtonType1.Visible = False
                ButtonType2.Visible = True
            End If


            TransferredSection.Visible = (readyToTransfer.Count > 0)

            'Grid to register the transferred
            If transferByCopy.Count > 0 Then
                TransferByOne.Visible = True
                GridTransfer.DataSource = transferByCopy
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString().ToLower() = "numeric" Then
                    GridTransfer.Columns(2).Visible = False
                    GridTransfer.Columns(3).Visible = True
                Else
                    GridTransfer.Columns(2).Visible = True
                    GridTransfer.Columns(3).Visible = False

                End If
            Else
                TransferByOne.Visible = False
            End If
            


            'Grid of Register and Grades
            If registerByCopy.Count > 0 Then

                'They are register and grades. configure grid
                GradePostedByOne.Visible = True
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString().ToLower() = "numeric" Then
                    RadGridPosted.Columns(2).Visible = False
                    RadGridPosted.Columns(3).Visible = True
                Else
                    RadGridPosted.Columns(2).Visible = True
                    RadGridPosted.Columns(3).Visible = False
                End If

                RadGridPosted.DataSource = registerByCopy
            Else
                GradePostedByOne.Visible = False
            End If

            If weirdList.Count > 0 Then
                WeirdSection.Visible = True
                RadGridWeird.DataSource = weirdList
            Else
                WeirdSection.Visible = False
            End If




        End Sub




    End Class
End Namespace