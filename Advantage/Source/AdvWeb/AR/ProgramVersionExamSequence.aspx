﻿<%@ Page Language="vb" AutoEventWireup="false" Inherits="ProgramVersionExamSequence" CodeFile="ProgramVersionExamSequence.aspx.vb" %>

<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register TagPrefix="uc1" TagName="DialogTemplates" Src="~/usercontrols/DialogTemplates.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>



<head>
    <title>Program Version Definitions</title>
    <%--<link href="../css/systememail.css" type="text/css" rel="stylesheet">--%>

    <script type="text/javascript">
        // Variables for MasterPageUserOptionsPanelBar
        var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL = '<%= Page.ResolveUrl("~")%>';
        var XMASTER_PAGE_USER_OPTIONS_USERNAME = "<%=Session("UserName") %>";
        var XMASTER_PAGE_USER_OPTIONS_USERID = '<%=Session("UserId")%>';
        var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID = '';
        function ShowCurrentTime() {

        }
        function OnSuccess(response, userContext, methodName) {
            alert(response);
        }
    </script>
    <%: Styles.Render("~/bundles/popupstyle") %>
    <%: Scripts.Render("~/bundles/popupscripts") %>
    <script src="../Kendo/js/jszip.min.js"></script>
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>

    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <%--<link rel="stylesheet" type="text/css" href="../CSS/ImportLeads.css" />
    <link href="../css/AR/font-awesome.css" rel="stylesheet" />--%>
    <style type="text/css">
        .mainDiv {
            width: 100%;
            margin-top: 10px;
        }

        .k-grid tbody tr {
            cursor: move;
        }

        #btnSave {
            float: right;
            margin-right: 20px;
            margin-bottom: 20px;
        }

        #ddlRequirements {
            margin: 20px;
        }

        #programVersionExamSequenceTable {
            margin: 20px;
        }

        .toppopupheader {
            font-family: verdana;
            font-size: xx-small;
            color: #FFFFFF;
            background-color: #164D7F;
            padding: 6px;
            vertical-align: top;
        }

            .toppopupheader > span {
                font-size: 20px;
            }

        .topemail {
            font-family: verdana;
            font-size: xx-small;
            color: #FFFFFF;
            background-color: #164D7F;
            text-align: right;
            padding: 20px;
            width: 20%;
            vertical-align: top;
        }

        A.close:visited, A.close:link {
            color: white
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnSave").on('click',
                function () {
                    
                    sessionStorage.setItem('programVersionDefinitionFormHasChanged', false);
                });
            //set session storage key to indicate changes on inputs
            $("#mainDiv").on("change",".examPositionTextBox input", function () {
                sessionStorage.setItem('programVersionDefinitionFormHasChanged', true);
            });

            $("#ddlRequirements").on('change',
                function () {
                    if (sessionStorage.getItem('programVersionDefinitionFormHasChanged') === "true") {
                        showWindow('#confirmationTemplate',
                            "Do you want to save your changes for current subject?",
                            false,
                            'btnSave', 'btnUpdateGrid');
                    } else {
                        $("#btnUpdateGrid").click();
                    }
                });

            
        });

      
        //close event function
        function CloseEvent(closeWindow, btnName) {
            showWindow('#confirmationTemplate', "Do you want to save your changes?", closeWindow, btnName, '');
        }

        //show window message
        function showWindow(template, message, closeWindow, btnName, btnAfterSave) {
            if (sessionStorage.getItem('programVersionDefinitionFormHasChanged') === "true") {
                var dfd = $.Deferred();
                var result = false;
                var win = $('<div id="popupWindow"></div>')
                    .appendTo("body")
                    .kendoWindow({
                        width: 400,
                        resizable: false,
                        title: false,
                        modal: true,
                        visible: false,
                        scrollable: false,
                        close: function (e) {
                            this.destroy();
                            dfd.resolve(result);
                        }
                    })
                    .data("kendoWindow");

                win.content($(template).html()).open().center();

                $('.popupMessage').html(message);

                $("#popupWindow .confirm_yes").val('Yes');
                $("#popupWindow .confirm_no").val('No');

                $("#popupWindow .confirm_no").click(() => {

                    if (btnAfterSave !== '') {
                        sessionStorage.setItem('programVersionDefinitionFormHasChanged', false);
                        win.close()
                        $("#" + btnAfterSave).click();

                    } else {
                        win.close();
                        if (closeWindow) {
                            close();

                        }
                    }
                    
                });

                $("#popupWindow .confirm_yes").click(() => {

                    win.close();
                    if (btnName) {
                        sessionStorage.setItem('programVersionDefinitionFormHasChanged', false);
                        $("#" + btnName).click();

                        if (btnAfterSave !== '') {
                            $("#" + btnAfterSave).click();
                        }
                    }
                    if (closeWindow) {
                        close();
                    }
                });

                return dfd.promise();
            } else {
                if (btnName) {
                    sessionStorage.setItem('programVersionDefinitionFormHasChanged', false);
                    $("#" + btnName).click();

                    if (btnAfterSave !== '') {
                        $("#" + btnAfterSave).click();
                    }
                }
                if (closeWindow) {
                    close();

                }
            }

        };
    </script>

</head>
<body runat="server">
    <uc1:DialogTemplates runat="server" ID="dialogTemplates" />

    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="scriptmanager1" runat="server" EnablePageMethods="True"></asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="toppopupheader">
                    <img src="../images/maint_popup_header.jpg">
                    <asp:Label ID="lblheader" runat="server" Text="Exam Sequence" />
                </td>

                <td class="topemail">
                    <a class="close" onclick="CloseEvent(true, 'btnSave')" href="#">X Close</a>
                </td>
            </tr>
        </table>
        <div class="mainDiv">
            <div class="DetailsFrameTop">
                <div class="">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel4">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" OnClick="btnSave_OnClick"></asp:Button>
                            <asp:button ID="btnUpdateGrid" runat="server" Text="Save" CssClass="save hidden" OnClick="ddlRequirements_OnSelectedIndexChanged"></asp:button>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="content">
                    <asp:DropDownList ID="ddlRequirements" runat="server" CssClass="dropdownlistff" EnableViewState="True" >
                    </asp:DropDownList>
                    <telerik:RadAjaxManager ID="RadAjaxManagerPosition" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="examPositionTextBox">
                                <UpdatedControls>
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>
                    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                        <ContentTemplate>



                            <telerik:RadGrid ID="programVersionExamSequenceTable" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                                HeaderStyle-HorizontalAlign="Center" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                                AllowPaging="False" AutoGenerateColumns="False" Width="90%" AllowMultiRowEdit="false" Height="500">
                                <MasterTableView Width="100%" CommandItemDisplay="top" DataKeyNames="Name,Position"
                                    AutoGenerateColumns="False" EditMode="InPlace" NoMasterRecordsText="No exams to display." PageSize="10">
                                    <CommandItemSettings ShowAddNewRecordButton="False" ShowRefreshButton="False" />
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="Position" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderText="Order">
                                            <ItemTemplate>
                                                <asp:TextBox TextMode="Number" CssClass="text-box examPositionTextBox" data-examId='<%# Eval("GradeBookDetailId") %>' data-requirementId='<%# Eval("SubjectId") %>' ID="examPositionTextBox" runat="server" Text='<%# Convert.ToInt32(Eval("Position")).ToString() %>' OnTextChanged="examPositionTextBox_OnTextChanged" AutoPostBack="True" Width="50"></asp:TextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Name" UniqueName="Name" DataType="System.String" HeaderText="Exam"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="SubjectDescription" UniqueName="SubjectDescription" DataType="System.String" HeaderText="Subject"></telerik:GridBoundColumn>
                                    </Columns>
                                    <EditFormSettings>
                                    </EditFormSettings>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="False" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
