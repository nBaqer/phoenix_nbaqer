﻿<%@ Page Title="Instructor Hours" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="InstructorHours.aspx.vb" Inherits="InstructorHours" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/JavaScript">
        function exporttoexcel() {
            var start = "<%=txtStart.SelectedDate %>";
            var end = "<%=txtEnd.SelectedDate %>";
            var campus = "<%=ddlCampus.selectedvalue %>";
            var turl = 'ExportToExcelClsSchedule.aspx?Start=' + start + '&end=' + end + '&campus=' + campus + '&source=noclassschedule';
            var strReturn = window.open(turl, 'classschedule', 'resizable:yes;status:no;dialogWidth:800px;dialogHeight:290px;dialogHide:true;help:no;scroll:yes');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->

                                        <table width="40%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblStart" runat="server" Text="Start Date" CssClass="label">Start Date <font color="red">*</font></asp:Label>
                                                </td>
                                                <td class="contentcell4" align="left" style="width: 60%">

                                                    <telerik:RadDatePicker ID="txtStart" runat="server" MinDate="1/1/1945" Width="200px">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblEnd" runat="server" Text="End" CssClass="label">End Date <font color="red">*</font></asp:Label>
                                                </td>
                                                <td class="contentcell4" align="left" style="width: 60%">

                                                    <telerik:RadDatePicker ID="txtEnd" runat="server" MinDate="1/1/1945" Width="200px">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblCampus" runat="server" Text="Campus" CssClass="label">Campus</asp:Label>
                                                </td>
                                                <td class="contentcell" style="width: 60%">
                                                    <asp:DropDownList ID="ddlCampus" runat="server" AutoPostBack="False" CssClass="dropdownlist" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td class="contentcell"></td>
                                                <td class="contentcell4" align="left" style="width: 68%" nowrap>
                                                    <br />
                                                    <asp:Button ID="btnBuildList" runat="server" Text="Build List"></asp:Button>
                                                    <asp:Button ID="btnExportToExcell" runat="server" 
                                                        Text="Export to Excel" Visible="True" CausesValidation="false"></asp:Button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell"></td>
                                                <td class="contentcell4" align="left" style="width: 68%"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell"></td>
                                                <td class="contentcell4" style="width: 68%">
                                                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dgrdInstructorHours" runat="server" Width="100%" HorizontalAlign="Center"
                                                        BorderStyle="Solid" AutoGenerateColumns="True" AllowSorting="False" BorderColor="#E0E0E0">
                                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>


                                    </div>
                                    <asp:TextBox ID="txtClsSectionId1" runat="server" CssClass="label" Visible="False"></asp:TextBox>
                                </td>
                                <!-- end rightcolumn -->
                            </tr>
                        </table>
                </tr>
            </table>

            <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStart"
                Display="None" ErrorMessage="You must enter a start meet date to search"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="txtEnd"
                Display="None" ErrorMessage="You must enter a end meet date to search"></asp:RequiredFieldValidator>

            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

