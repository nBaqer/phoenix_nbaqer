<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="StudentAttendanceFromConversion.aspx.vb" Inherits="AR_StudentAttendanceFromConversion" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<TR>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right">
                                    <asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button>
                                    <asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						
					</td>
					<!-- end rightcolumn --></TR>
                    <TR>
					<td class="detailsframe">
						<div class="scrollsingleframe">
							<!-- begin table content-->
							<table class="contenttable" cellSpacing="0" cellPadding="0" width="45%" align="center">
								<tr>
									<TD class="contentcell"><asp:label id="lblEnrollmentId" CssClass="Label" Runat="server">Enrollment</asp:label></TD>
									<TD class="contentcell4">
									    <asp:dropdownlist id="ddlStuEnrollId" runat="server" CssClass="DropDownList" ></asp:dropdownlist>
									    
									</TD>
								</tr>
								<tr>
									<TD class="contentcell" style="height: 19px"><asp:label id="lblTerms" runat="server" CssClass="Label">Start</asp:label></TD>
									<TD class="contentcell4" style="height: 19px"><asp:TextBox ID="txtStartDate" runat="server" CssClass="TextBox" ></asp:TextBox></TD>
								</tr>
								<tr>
									<TD class="contentcell"><asp:label id="lblClsSections" runat="server" CssClass="Label">End</asp:label></TD>
									<TD class="contentcell4"><asp:TextBox ID="txtEndDate" runat="server" CssClass="TextBox" ></asp:TextBox><TD class="contentcell4"></TD>
								<TR>
									<TD class="contentcell">&nbsp;</TD>
									<TD class="contentcell4"><asp:button id="btnBuildList" runat="server" Text="Show Attendance" ></asp:button></TD>
								</TR>
								
							</table>
							
							<table class="contenttable" cellSpacing="0" cellPadding="0" width="100%" border="0">
							    <tr>
							        <td colspan=4 align="left">
                                        <asp:Label ID="lblHours" runat="server" CssClass="Label"></asp:Label></td>
							    </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdStudentAttendance" runat="server" Width="100%" HorizontalAlign="Center"
                                        BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0">
                                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                        <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="MeetDate" HeaderText="Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="25%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Actual" ReadOnly="True" HeaderText="Hours">
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="25%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Tardy" >
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="25%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:checkbox ID="chkTardy" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Tardy") %>'>
                                                    </asp:checkbox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Excused" >
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="25%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:checkbox ID="chkExcused" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Excused") %>'>
                                                    </asp:checkbox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            
                                        </Columns>
                                    </asp:DataGrid>
                                    </td>
                                </tr>
								<tr>
									<td><asp:panel id="pnlAttendance" CssClass="Label" Runat="server" Width="100%"></asp:panel></td>
								</tr>
								
							</table>
							<!--end table content--></div>
					</td>
				</TR>
			</table>
            <!-- start validation panel--><asp:panel id="Panel1" runat="server" CssClass="ValidationSummary">
			<asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDate" ErrorMessage="Start date is required" Display="None"></asp:RequiredFieldValidator>
			<asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="txtEndDate" ErrorMessage="End date is required" Display="None"></asp:RequiredFieldValidator>
			<asp:RequiredFieldValidator ID="rfvEnrollment" runat="server" ControlToValidate="ddlStuEnrollId" ErrorMessage="Enrollment is required" Display="None"></asp:RequiredFieldValidator>
			<asp:CompareValidator ID="cmvStartDate" runat="server" ControlToValidate="txtStartDate" Type="Date" Operator="DataTypeCheck" ErrorMessage="You must enter a valid date for the start date" Display="none"></asp:CompareValidator>
			<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtEndDate" Type="Date" Operator="DataTypeCheck" ErrorMessage="You must enter a valid date for the end date" Display="none"></asp:CompareValidator>
			</asp:panel><asp:customvalidator id="Customvalidator1" runat="server" CssClass="ValidationSummary" ErrorMessage="CustomValidator"
				Display="None"></asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
				ShowSummary="False"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>

