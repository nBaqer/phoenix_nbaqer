<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Programs.aspx.vb" Inherits="Programs" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script src="../js/checkall.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>


    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350"
                Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server"
                                            RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstPrograms" runat="server" DataKeyField="ProgId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server"
                                            Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>'
                                            CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>'
                                            CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server"
                                            CommandArgument='<%# Container.DataItem("ProgId")%>' Text='<%# getDesc(Container.DataItem("ProgDescrip"),Container.DataItem("ShiftDescrip")) %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both"
                orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button>
                        </td>
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="maincontenttable" width="70%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblProgCode" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtProgCode" runat="server" MaxLength="128" Height="20px" Width="250px" CssClass="textbox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" CssClass="dropdownlist" runat="server" Width="278px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblProgDescrip" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="contentcell4" nowrap="nowrap">
                                                <asp:TextBox ID="txtProgDescrip" runat="server" MaxLength="128" Height="20px" Width="250px" CssClass="textbox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <asp:Panel ID="pnlPeriods" runat="server" Visible="false">
                                                <td class="contentcell">
                                                    <asp:Label ID="lblNumPmtPeriods" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtNumPmtPeriods" runat="server" MaxLength="128" Visible="false"
                                                        Width="275px"></asp:TextBox>
                                                </td>
                                            </asp:Panel>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="contentcell4" nowrap="nowrap">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" Width="278px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblACId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="contentcell4" nowrap="nowrap">
                                                <asp:DropDownList ID="ddlACId" runat="server" Width="278px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblShiftId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="contentcell4" nowrap="nowrap">
                                                <asp:DropDownList ID="ddlShiftId" runat="server" Width="278px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCipCode" runat="server" CssClass="Label">CIP Code</asp:Label>
                                            </td>
                                            <td class="contentcell4" nowrap="nowrap">
                                                <asp:TextBox ID="txtCipCode" runat="server" MaxLength="6" Height="20px" Width="250px" CssClass="textbox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCredentialLevel" runat="server" CssClass="Label">Credential Level</asp:Label>
                                            </td>
                                            <td class="contentcell4" nowrap="nowrap">
                                                <asp:DropDownList ID="ddlCredentialLevel" runat="server" Width="278px">
                                                </asp:DropDownList>
                                                <%--<asp:DropDownList ID="ddlCredentialLevel" runat="server" Width="272px">
                                                    <asp:ListItem Value="08">Select</asp:ListItem>
                                                    <asp:ListItem Value="01">Undergraduate certificate</asp:ListItem>
                                                    <asp:ListItem Value="02">Associate's degree</asp:ListItem>
                                                    <asp:ListItem Value="03">Bachelor's degree</asp:ListItem>
                                                    <asp:ListItem Value="04">Post baccalaureate certificate</asp:ListItem>
                                                    <asp:ListItem Value="05">Master's degree</asp:ListItem>
                                                    <asp:ListItem Value="06">Doctoral degree</asp:ListItem>
                                                    <asp:ListItem Value="07">First professional degree</asp:ListItem>
                                                </asp:DropDownList>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4" nowrap="nowrap">
                                                <label style="padding: 5px; display: block">
                                                    <asp:CheckBox ID="chkGEProgram" runat="server" Text="Gainful Employment Program"
                                                        CssClass="checkbox" Style="margin-right: 10px" />
                                                </label>
                                                <label style="padding: 5px">
                                                    <asp:CheckBox ID="chk1098T" runat="server" Text="Include in 1098T Extract"
                                                        CssClass="checkbox" />
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4" nowrap></td>
                                            <tr>
                                                <td class="contentcell"></td>
                                                <td class="contentcell4" nowrap="nowrap">
                                                    <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="label new ">View Versions Of This Program</asp:LinkButton><br />
                                                    <br />
                                                    <asp:LinkButton ID="Linkbutton1" runat="server" Visible="false" CssClass="label">Set Up Non-Term Start Dates</asp:LinkButton>
                                                    <br />
                                                </td>
                                            </tr>
                                    </table>
                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6">
                                                    <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcell2" colspan="6">
                                                    <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false">
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </asp:Panel>
                            <!--end table content-->
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <asp:TextBox ID="txtProgId" runat="server" MaxLength="128" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtCampusId" runat="server" MaxLength="128" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtOldCampusGroups" runat="Server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtOldCampusGroupsText" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" Width="0"></asp:TextBox>
        <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" Width="0"></asp:TextBox>
        <asp:CompareValidator ID="cvNumPmtPeriods" runat="server" ControlToValidate="txtNumPmtPeriods"
            Display="None" ErrorMessage="Value should be 2 or greater" Operator="GreaterThanEqual"
            ValueToCompare="2" Type="Integer"></asp:CompareValidator>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            Display="None" ErrorMessage="CustomValidator">
        </asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>
</asp:Content>
