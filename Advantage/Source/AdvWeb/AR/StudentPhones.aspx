﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentPhones.aspx.vb" Inherits="StudentPhones" EnableEventValidation="false" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
<%--<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstStdPhone">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                 <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                    <telerik:AjaxUpdatedControl ControlID="dlstStdPhone"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
           <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstStdPhone"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstStdPhone"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="radstatus">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstStdPhone"  />
                     <telerik:AjaxUpdatedControl ControlID="radstatus"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings> 
</telerik:RadAjaxManagerProxy>
--%>



    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="listframetop">
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td nowrap="nowrap" align="left" width="15%">
                                            <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                        <td nowrap="nowrap" width="85%">
                                            <asp:RadioButtonList ID="radStatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                CssClass="label">
                                                <asp:ListItem Text="Active" Selected="True" />
                                                <asp:ListItem Text="Inactive" />
                                                <asp:ListItem Text="All" />
                                            </asp:RadioButtonList></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="listframebottom">
                                <div class="scrollleftfilters">
                                    <asp:DataList ID="dlstStdPhone" runat="server" Width="100%" DataKeyField="StudentPhoneId">
                                        <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                        <ItemStyle CssClass="itemstyle"></ItemStyle>
                                        <ItemTemplate>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td nowrap="nowrap" width="30%">
                                                        <asp:ImageButton ID="imgInActive" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
                                                            CommandArgument='<%# Container.DataItem("StudentPhoneId")%>' CausesValidation="False"
                                                            ImageUrl="../images/Inactive.gif" />
                                                        <asp:ImageButton ID="imgActive" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>'
                                                            CommandArgument='<%# Container.DataItem("StudentPhoneId")%>' CausesValidation="False"
                                                            ImageUrl="../images/Active.gif" />
                                                        <asp:LinkButton Text='<%# Container.DataItem("Descrip") %>' runat="server" CssClass="itemstyle"
                                                            CommandArgument='<%# Container.DataItem("StudentPhoneId")%>' ID="Linkbutton2"
                                                            CausesValidation="False" />
                                                    </td>
                                                    <td nowrap="nowrap" width="70%">
                                                        <asp:LinkButton Text='<%# Container.DataItem("Phone")%>' runat="server" CssClass="itemstyle"
                                                            CommandArgument='<%# Container.DataItem("StudentPhoneId")%>' ID="Linkbutton1"
                                                            CausesValidation="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList></div>
                            </td>
                        </tr>
                    </table>



    </telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
      <asp:Panel ID="pnlRHS" runat="server">

                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2">
        <!-- begin content table-->


                                    <!-- begin content table-->
                                  
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                            <tr>
                                                <td class="contentcell" nowrap="nowrap">
                                                </td>
                                                <td class="contentcell4" nowrap="nowrap">
                                                    <asp:CheckBox ID="chkForeignPhone" TabIndex="1" runat="server" CssClass="checkboxinternational"
                                                        AutoPostBack="true" Text="International"></asp:CheckBox></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap="nowrap">
                                                    <asp:Label ID="lblPhone" runat="server" CssClass="label">Phone</asp:Label></td>
                                                <td class="contentcell4">
                                                    <telerik:RadMaskedTextBox ID="txtPhone" TabIndex="2" runat="server" CssClass="textbox"
                                                        Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="" Width="100px">
                                                    </telerik:RadMaskedTextBox>                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblExt" runat="server" CssClass="label">Extension</asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtExt" TabIndex="3" runat="server" CssClass="textbox" Columns="3"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblBestTime" runat="server" CssClass="label">BestTime</asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtBestTime" TabIndex="4" runat="server" CssClass="textbox" Columns="9"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap="nowrap">
                                                    <asp:Label ID="lblPhoneTypeID" runat="server" CssClass="label">Type</asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:DropDownList ID="ddlPhoneTypeID" TabIndex="5" runat="server" CssClass="dropdownlist">
                                                    </asp:DropDownList>
                                                    <%--<asp:CompareValidator ID="cvPhoneTypeID" runat="server" ValueToCompare="00000000-0000-0000-0000-000000000000"
                                                        Operator="NotEqual" ControlToValidate="ddlPhoneTypeID" Display="None" ErrorMessage="Must Select a Phone Type">Must Select a Phone Type</asp:CompareValidator>--%></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap="nowrap">
                                                    <asp:Label ID="lblStatusId" runat="server" CssClass="label">Status</asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:DropDownList ID="ddlStatusId" TabIndex="6" runat="server" CssClass="dropdownlist">
                                                    </asp:DropDownList>
                                                    <asp:CompareValidator ID="cvStatusId" runat="server" ValueToCompare="00000000-0000-0000-0000-000000000000"
                                                        Operator="NotEqual" ControlToValidate="ddlStatusId" Display="None" ErrorMessage="Must Select a Status">Must Select a Status</asp:CompareValidator></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap="nowrap">
                                                </td>
                                                <td class="contentcell4date">
                                                    <asp:CheckBox ID="chkdefault1" TabIndex="7" runat="server" CssClass="checkboxinternational"
                                                        Text="Default Phone"></asp:CheckBox></td>
                                            </tr>
                                        </table>
                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td class="contentcell" nowrap="nowrap">
                                                    <asp:TextBox ID="txtStudentPhoneId" runat="server" Visible="false"></asp:TextBox>
                                                    <asp:CheckBox ID="chkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                                    <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                                    <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox></td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="label"
                                                        Width="10%"></asp:TextBox>
                                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="label"
                                                        Width="10%"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtStudentId" runat="server" Visible="false"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="spacertables">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap" colspan="6">
                                                        <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="spacertables">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcell2" colspan="6">
                                                    <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false">
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                   
                                    <!-- end content table-->
                             


        </div>
        </td>
        </tr>
        </table>
         </asp:Panel>
    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>
