<%@ Page Language="vb" Title="Exams" AutoEventWireup="false" MasterPageFile="~/NewSite.master" Inherits="StudentExams" CodeFile="StudentExams.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" type="text/javascript" src="../js/Print.js"></script>
    <link rel="stylesheet" type="text/css" href="../FameLink/PrintStyle.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="False"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="False"></asp:Button>

                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin table content -->
                            <table width="100%" align="center">
                                <tr>
                                    <td width="60%">&nbsp;</td>
                                    <td width="20%">&nbsp;</td>
                                    <td>
                                        <asp:Button ID="btnPrint" runat="server" Text="Print Preview"></asp:Button></td>
                                </tr>
                            </table>
                            <div id="print_area" runat="server">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblEnrollmentId"
                                                runat="server">Enrollment</asp:Label>
                                        </td>
                                        <td width="40%">
                                            <asp:DropDownList ID="ddlEnrollmentId" runat="server" CssClass="dropdownlist"
                                                AutoPostBack="true" Width="250px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertable"></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblOverAllAverageText" runat="server" Text="Overall Average Score"></asp:Label>
                                        </td>
                                        <td width="40%">
                                            <asp:Label ID="lOverallAverage" runat="Server"
                                                Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <p></p>
                                <asp:Panel ID="pnlStudentInfo" runat="server" Visible="true">
                                    <table cellpadding="0" cellspacing="0" width="60%" align="center">
                                        <tr>
                                            <td style="width: 80%; padding: 16px">
                                                <fieldset>
                                                    <legend>Student Information</legend>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="twocolumnlabelcell" nowrap>
                                                                <asp:Label ID="lblStudent" runat="Server">Student:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap>
                                                                <asp:Label ID="lblStudentName" runat="server">
                                                                </asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td class="twocolumnlabelcell" nowrap>
                                                                <asp:Label ID="lblEnrollment1" runat="Server">Enrollment:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap>
                                                                <asp:TextBox ID="lblEnrollmentName" runat="server" CssClass="textbox"
                                                                    contentEditable="False"></asp:TextBox>
                                                            </td>
                                                            <td></td>
                                                            <td class="twocolumnlabelcell" nowrap>
                                                                <asp:Label ID="lblStatus" runat="Server">Enrollment Status:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap>
                                                                <asp:Label ID="lblEnrollmentStatus" runat="Server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacertables"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="twocolumnlabelcell" nowrap>
                                                                <asp:Label ID="lblStudentStartDate" runat="Server">Start Date:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap>
                                                                <asp:Label ID="lblStartDate" runat="Server" CssClass="TextBox"></asp:Label>
                                                            </td>
                                                            <td></td>
                                                            <td class="twocolumnlabelcell" nowrap>
                                                                <asp:Label ID="lblGradDate" runat="Server">Graduation Date:</asp:Label>
                                                            </td>
                                                            <td class="twocolumncontentcell" nowrap>
                                                                <asp:Label ID="lblGD" runat="Server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>

                                <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                                <tr>
                                                    <td class="contentcellheader" nowrap>
                                                        <div style="width: 30%; float: left">
                                                            <asp:Label ID="lLeft" runat="server"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <div class="scrollleft">
                                                            <asp:DataGrid ID="dgWrittenExams" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                                                BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal" Width="100%" runat="server">
                                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                                <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="11px" Font-Bold="false" BorderColor="#E0E0E0"></ItemStyle>
                                                                <HeaderStyle CssClass="datagridheaderstyle" BackColor="#FAFAFA" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></HeaderStyle>
                                                                <AlternatingItemStyle CssClass="datagridalternatingstyle" BackColor="#FAFAFA"></AlternatingItemStyle>
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Name">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="70%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblClinicService" runat="Server" Text='<%# Container.DataItem("Descrip") %>' CssClass="label"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Score">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="15%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRequired" Text='<%# ConvertToStringForT(DataBinder.Eval(Container, "DataItem.Score", "{0:#.00}")) %>' CssClass="label"
                                                                                runat="server">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Pass/Fail">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="15%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCompleted" Text='<%#ConvertIsPassToString(DataBinder.Eval(Container.DataItem, "IsPass"))%>' CssClass="label"
                                                                                runat="server">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="20px">&nbsp;</td>
                                        <td valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                                <tr>
                                                    <td class="contentcellheader" nowrap>
                                                        <div style="width: 30%; float: left">
                                                            <asp:Label ID="lRight" runat="server"></asp:Label>
                                                        </div>
                                                    </td>
                                                    <td width="20px">&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="scrollleft">
                                                            <asp:DataGrid ID="dgPracticalExam" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                                                BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal" Width="100%" runat="server">
                                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                                <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="11px" Font-Bold="false" BorderColor="#E0E0E0"></ItemStyle>
                                                                <HeaderStyle CssClass="datagridheaderstyle" BackColor="#FAFAFA" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></HeaderStyle>
                                                                <AlternatingItemStyle CssClass="datagridalternatingstyle" BackColor="#FAFAFA"></AlternatingItemStyle>
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Name">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="70%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblClinicService" runat="Server" Text='<%# Container.DataItem("Descrip") %>' CssClass="label"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Score">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="15%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></ItemStyle>
                                                                        <ItemTemplate>
                                                                              <asp:Label ID="lblRequired" Text='<%#  ConvertToStringForT(DataBinder.Eval(Container, "DataItem.Score", "{0:#.00}")) %>' CssClass="label"
                                                                                runat="server">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Pass/Fail">
                                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="15%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCompleted" Text='<%#ConvertIsPassToString(DataBinder.Eval(Container.DataItem, "IsPass"))%>' CssClass="label"
                                                                                runat="server">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:Repeater ID="rWrittenExams" runat="server" Visible="false">
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "Descrip")%>
                                    <%#DataBinder.Eval(Container.DataItem, "Score")%>
                                    <%#ConvertIsPassToString(DataBinder.Eval(Container.DataItem, "IsPass"))%>
                                </ItemTemplate>
                            </asp:Repeater>


                            <asp:Repeater ID="rPracticalExams" runat="server" Visible="false">
                                <ItemTemplate>
                                    <tr>

                                        <%#DataBinder.Eval(Container.DataItem, "Descrip")%>

                                        <%#DataBinder.Eval(Container.DataItem, "Score")%>

                                        <%#ConvertIsPassToString(DataBinder.Eval(Container.DataItem, "IsPass"))%>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:TextBox ID="txtSkillGrpId" runat="server" Visible="False"></asp:TextBox>
    <asp:Button ID="btncheck" runat="server" Text="Check" Visible="false" />
    <input id="Hidden1" type="hidden" name="txtcheck" runat="server" />
    <asp:TextBox ID="txtStudentId" runat="server" Width="0px" Visible="true" />
</asp:Content>

