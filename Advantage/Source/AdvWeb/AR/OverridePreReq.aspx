﻿<%@ Page Title="Override Prerequisites" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="OverridePreReq.aspx.vb" Inherits="OverridePreReq" %>
<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
            <!-- begin rightcolumn -->
            <tr>
                <td class="detailsframetop">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False">
                                </asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->

                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                        align="center">
                        <tr>
                            <td class="detailsframe">
                                <fame:StudentSearch id="StudSearch1" runat="server" ShowTerm="false" ShowAcaYr="false" OnTransferToParent="TransferToParent"/>
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <!-- begin table content-->
                                    <asp:Button ID="btnGetCourses" runat="server"  Text="Get Courses"
                                                CausesValidation="False"></asp:Button>
                                   
                                    <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" align="center" style="width: 100%; margin-top: 10px;">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtStuEnrollment" TabIndex="2" runat="server" Width="0px" Visible="false">StuEnrollment</asp:TextBox>
                                                <asp:TextBox ID="txtStuEnrollmentId" runat="server" Width="0px" Visible="false">StuEnrollmentId</asp:TextBox>
                                                <asp:TextBox ID="txtAcademicYear" TabIndex="8" runat="server" Width="0px" Visible="false">AcademicYear</asp:TextBox>
                                                <asp:TextBox ID="txtAcademicYearId" runat="server" Width="0px" Visible="false">AcademicYearId</asp:TextBox>
                                                <asp:TextBox ID="txtTerm" TabIndex="9" runat="server" Width="0px" Visible="false">Term</asp:TextBox>
                                                <asp:TextBox ID="txtTermId" runat="server" Width="0px" Visible="false">TermId</asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="threecolumnheader1">
                                                <asp:Label ID="lblAvailCourses" CssClass="labelbold" Font-Bold="True" runat="server">Available Courses</asp:Label>
                                            </td>
                                            <td class="threecolumnspacer1">
                                            </td>
                                            <td class="threecolumnheader1">
                                                <asp:Label ID="lblSelCourses" CssClass="labelbold" Font-Bold="True" runat="server">Selected Courses</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="threecolumncontent">
                                                <asp:ListBox ID="lbxAvailCourses" runat="server" CssClass="listboxes" AutoPostBack="False"
                                                    Rows="12"></asp:ListBox>
                                            </td>
                                            <td class="threecolumnbuttons">
                                               <telerik:RadButton  ID="btnAdd" CssClass="buttons1" Text="Add >" Enabled="False" runat="server" >
                                                </telerik:RadButton>
                                                <br>
                                               <telerik:RadButton  ID="btnAddAll" CssClass="buttons1" Text="Add All >>" Enabled="False"
                                                    runat="server" ></telerik:RadButton>
                                                <br>
                                                <br>
                                                <telerik:RadButton  ID="btnRemove" CssClass="buttons1" Text="< Remove" Enabled="False" runat="server" >
                                                </telerik:RadButton>
                                                <br>
                                                <telerik:RadButton  ID="btnRemoveAll" runat="server" CssClass="buttons1" Text="<< Remove All"
                                                    Enabled="False" ></telerik:RadButton>
                                            </td>
                                            <td class="threecolumncontent">
                                                <asp:ListBox ID="lbxSelCourses" runat="server" CssClass="listboxes" Rows="12" AutoPostBack="False">
                                                </asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>


                                    <!--end table content-->
                                </div>
                            </td>
                        </tr>
                    </table>
        </table>
             <!--end table content-->


                <asp:TextBox id="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                <asp:TextBox id="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
            
                <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
                </asp:Panel>
                <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                    Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
                <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
                </asp:Panel>
                <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                    ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
                </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

