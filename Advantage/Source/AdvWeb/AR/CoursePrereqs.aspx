﻿<%@ Page Title="Enter Course Prerequisites" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="CoursePrereqs.aspx.vb" Inherits="CoursePrereqs" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
            <!-- begin rightcolumn -->
            <tr>
                <td class="detailsframetop">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False">
                                </asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                        align="center">
                        <tr>
                            <td class="detailsframe">
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <!-- begin table content-->

                                    <table width="40%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="label1" runat="server" CssClass="label">Program Version</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlPrgverID" Width="350px"  runat="server" CssClass="dropdownlist" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCourse" runat="server" CssClass="label">Course</asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCourseId" Width="350px" runat="server" CssClass="dropdownlist" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <table width="900px" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                        <asp:Label ID="lblError" CssClass="label" runat="server" Visible="false" ForeColor="red"></asp:Label>
                                        <tr>
                                            <td class="threecolumnheader1">
                                                <asp:Label ID="lblAvailCourses" runat="server" CssClass="label" Font-Bold="True">Available course(s)</asp:Label>
                                            </td>
                                            <td class="threecolumnspacer1">
                                            </td>
                                            <td class="threecolumnheader1">
                                                <asp:Label ID="lblSelectCourses" runat="server" CssClass="label" Font-Bold="True">Selected Prereq course(s)</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="threecolumncontent">
                                                <asp:ListBox ID="lstAvailCourses" runat="server" CssClass="listboxes" Rows="15">
                                                </asp:ListBox>
                                            </td>
                                            <td class="threecolumnbuttons">
                                               <telerik:RadButton  ID="btnAdd" Text="Add >" runat="server" CssClass="buttons1" width="100px"></telerik:RadButton ><br />
                                                <telerik:RadButton  ID="btnRemove" Text="< Remove" runat="server" CssClass="buttons1" width="100px" ></telerik:RadButton ><br />
                                                <telerik:RadButton  ID="btnAddAll" Visible="false" Text="Add All >>" runat="server" CssClass="buttons1" width="100px">
                                                </telerik:RadButton ><br />
                                                <br />
                                                <telerik:RadButton  Visible="false" ID="btnRemoveAll" Text="<< Remove All" runat="server"
                                                    CssClass="buttons1"></telerik:RadButton>
                                            </td>
                                            <td class="threecolumncontent">
                                                <asp:ListBox ID="lstSelectCourses" runat="server" CssClass="listboxes" Rows="15"
                                                    AutoPostBack="True"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>


                                    <!--end table content-->
                                </div>
                            </td>
                        </tr>
                    </table>
        </table>
             <!--end table content-->


                <asp:TextBox id="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                <asp:TextBox id="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
            
                <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
                </asp:Panel>
                <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                    Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
                <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
                </asp:Panel>
                <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                    ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
                </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

