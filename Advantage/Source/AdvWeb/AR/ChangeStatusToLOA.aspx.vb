﻿Imports System.Diagnostics
Imports FAME.Advantage.Common.AdvControls
Imports FAME.Common
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports System.Collections
Imports System.Collections.Generic

Partial Class ChangeStatusToLOA
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected campusId, userId As String


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it usi ng the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        ' Me.SetPageTitle("Change Status to LOA")
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities

        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As User = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

       
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        If Not Page.IsPostBack Then
            'Dim objcommon As New CommonUtilities
            ' objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            BuildDropDownLists()

            PopulateStatusDdl()

        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")

        End If

    End Sub
    Private Sub PopulateStatusDdl()
        Dim facade As New LOAFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlStatusCodeId
            .DataSource = facade.GetLOAStatuses()
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, _
                             ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, _
                             ByVal hourtype As String)


        txtStuEnrollmentId.Text = enrollid
        ' txtStudentName.Text = fullname
        txtTermId.Text = termid
        'txtTerm.Text = termdescrip
        'txtAcademicYearId.Text = academicyearid
        'txtAcademicYear.Text = academicyeardescrip

        Session("StuEnrollment") = txtStuEnrollmentId.Text
        Session("StudentName") = fullname
        Session("TermId") = txtTermId.Text
        'Session("Term") = txtTerm.Text
        'Session("AcademicYearID") = txtAcademicYearId.Text
        'Session("AcademicYear") = txtAcademicYear.Text
        'DE7886
        ClearRHS("NO")
    End Sub



    Private Sub btnLOA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLOA.Click
        Dim facade As New LOAFacade
        Dim message As String
        Dim student As String
        Dim endMsg As String
        Dim errDates As String
        Dim errSDate As String
        '        Dim HasStartDate As Integer
        If Not Session("StudentName") Is Nothing Then
            If Session("StudentName") = "" Then
                DisplayErrorMessage("Unable to find data. Student field cannot be empty.")
                Exit Sub
            Else
                student = Session("StudentName")
            End If
        Else
            DisplayErrorMessage("Unable to find data. Student field cannot be empty.")
            Exit Sub
        End If


        If ddlStatusCodeId.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("The Status and LOA Reason fields cannot be empty. Please select an option.")
            Exit Sub
        Else
            ViewState("Status") = ddlStatusCodeId.SelectedItem.Value.ToString()
        End If

        If ddlLOAReasonId.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("The Status and LOA Reason fields cannot be empty. Please select an option.")
            Exit Sub
        Else
            ViewState("LOAReason") = ddlLOAReasonId.SelectedItem.Value.ToString()
        End If

        'Validate start and end dates on LOA
        If txtStartDate.SelectedDate.ToString <> "" And txtEndDate.SelectedDate.ToString <> "" Then
            errDates = ValidateStartEndDates(txtStartDate.SelectedDate.ToString, txtEndDate.SelectedDate.ToString)
            If errDates <> "" Then
                DisplayErrorMessage(errDates)
                Exit Sub
            End If
        End If



        If txtStartDate.SelectedDate.ToString <> "" Then
            'First check if the student has a start date
            'Validate that start date on this student enrollment is greater
            errSDate = ValidateStudentStartDate(txtStartDate.SelectedDate.ToString)
            If errSDate <> "" Then
                DisplayErrorMessage(errSDate)
                Exit Sub
            End If
        End If
        If txtStartDate.SelectedDate.ToString <> "" Then
            If CDate(txtStartDate.SelectedDate.ToString) > Date.Now.ToShortDateString Then
                errSDate = StudentLOACount()
                If errSDate <> "" Then
                    DisplayErrorMessage(errSDate)
                    Exit Sub
                End If
            End If
        End If

        ' US2135 01/05/2012 Janet Robinson added RequestDate validation
        If txtLOARequestDate.SelectedDate.ToString = "" Then
            DisplayErrorMessage("LOA Request Date must be entered.")
            Exit Sub
        End If
        ' DE7126 02/7/2012 Janet Robinson - make sure RequestDate is valid and before or equal to start date
        Dim result As String = ""
        If Not CommonWebUtilities.IsValidDate(txtLOARequestDate.SelectedDate.ToString) Then
            DisplayErrorMessage("Invalid LOA Request Date.")
            Exit Sub
        End If
        If Date.Parse(txtLOARequestDate.SelectedDate.ToString) > Date.Parse(txtEndDate.SelectedDate.ToString) Then
            DisplayErrorMessage("LOA Request Date cannot be greater than LOA End Date.")
            Exit Sub
        End If

        If facade.CheckAttendanceInLDADates(txtStuEnrollmentId.Text, txtStartDate.SelectedDate, DateAdd("d", 1, txtEndDate.SelectedDate)) Then
            DisplayErrorMessage("Student Attendance has been posted between the selected LOA dates.")
            Exit Sub
        End If


        message = facade.PlaceStudentOnLOA(campusId, txtStuEnrollmentId.Text, ddlLOAReasonId.SelectedValue.ToString, txtStartDate.SelectedDate.ToString, txtEndDate.SelectedDate.ToString, ddlStatusCodeId.SelectedValue.ToString, AdvantageSession.UserState.UserName, txtLOARequestDate.SelectedDate.ToString)

        If message <> "" Then
            DisplayErrorMessage(message)
        Else
            ' Add a new task
            endMsg = AddNewTask()
            endMsg &= "The student will be placed on an LOA on the selected Start Date." & vbLf
            If facade.GetStudentTutionEarningMethod_Sp(txtStuEnrollmentId.Text) = 0 Then
                endMsg &= "Please note that the revenue recognition process for this student will be terminated"
            End If
            CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, endMsg)
            ClearRHS("ALL")
        End If
    End Sub

    Private Function AddNewTask() As String
        Dim utInfo As New UserTaskInfo
        Dim rtn As String = String.Empty
        ' determine if this is an update or a new task by looking
        ' at the ViewState("usertaskid") which is set by passing a parameter to
        ' the page.

        utInfo.IsInDB = False
        utInfo.AssignedById = TMCommon.GetCurrentUserId()
        utInfo.EndDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.StartDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.Message = "The student will be placed on an LOA on the selected Start Date." & vbLf
        utInfo.Priority = 1
        'utInfo.OwnerId = TMCommon.GetCurrentUserId()
        'utInfo.ReId = txtStuEnrollmentId.Text
        utInfo.ModUser = TMCommon.GetCurrentUserId()
        utInfo.Status = TaskStatus.Pending
        utInfo.ResultId = txtStuEnrollmentId.Text


        ' Update the task and check the result
        ' Asked to remove the message.
        If Not UserTasksFacade.AddTaskFromStatusCode(ddlStatusCodeId.SelectedValue, utInfo) Then
            '    rtn = "Unable to add a reminder to the Task Manager. " & vbLf
            rtn = String.Empty
        End If
        Return rtn
    End Function

    Public Function ValidateStartEndDates(ByVal startDate As Date, ByVal endDate As Date) As String

        Dim errMsg As String = ""


        If endDate < startDate Then
            errMsg &= "End date on LOA must be greater than the Start Date. "
        End If

        Return errMsg
    End Function
    Private Sub ClearRHS(ByVal ClearType As String)
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If TypeOf ctl Is TextBox Then
                    CType(ctl, TextBox).Text = ""
                End If
                If TypeOf ctl Is CheckBox Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If TypeOf ctl Is DropDownList Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
                If TypeOf ctl Is RadDatePicker Then
                    CType(ctl, RadDatePicker).Clear()
                End If
                If ClearType = "ALL" Then
                    If TypeOf ctl Is IAdvControl Then
                        Dim AdvCtrl As IAdvControl = CType(ctl, IAdvControl)
                        AdvCtrl.Clear()
                    End If
                End If
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        Dim objCommon As New CommonUtilities

        Try
            ClearRHS("ALL")
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            btnSave.Enabled = False
            ' Header1.EnableHistoryButton(False)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message()
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Public Function ValidateStudentStartDate(ByVal LOAStartDate As Date) As String

        Dim errMsg As String = ""
        Dim facade As New LOAFacade
        Dim stdstartDate As String

        stdstartDate = facade.GetStudentStartDate(txtStuEnrollmentId.Text)

        'If the student has a start date then validate it against LOA startdate.
        If stdstartDate <> "" Then
            If LOAStartDate < stdstartDate Then
                ''   errMsg &= "Cannot place student with a future start date on LOA."
                errMsg &= "   Cannot place a future start student on LOA. "
            End If
        End If

        Return errMsg
    End Function

    Public Function StudentLOACount() As String

        Dim errMsg As String = ""
        Dim facade As New LOAFacade
        Dim lOAcnt As Integer

        lOAcnt = facade.StudentLOACount(txtStuEnrollmentId.Text)

        'If the student has a start date then validate it against LOA start-date.
        If lOAcnt > 0 Then

            errMsg &= "   An active LOA exists for this student.."
        End If
        Return errMsg
    End Function
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnLOA)
        'controlsToIgnore.Add(CalButton1)
        'controlsToIgnore.Add(CalButton2)
        'Add javascript code to warn the user about non saved changes

        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        '        Dim SchoolItem As String

        'Countries DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlLOAReasonId, AdvantageDropDownListName.LOA_Reasons, campusId, True, True, String.Empty))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub

End Class

