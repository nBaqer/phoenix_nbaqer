﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ChangeStatusToLOA.aspx.vb" Inherits="ChangeStatusToLOA" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Change Status to LOA</title>

    <script language="javascript" src="../AuditHist.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">

        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">

            <table id="Table1" class="maincontenttable">
                <!-- begin right column -->
                <tr>
                    <td class="DetailsFrameTop">
                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Enabled="False" Text="Save"></asp:Button><asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                                        Enabled="False" CausesValidation="False"></asp:Button>
                                </td>

                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable">
                            <tr>
                                <td class="detailsframe">
                                    <FAME:StudentSearch ID="StudSearch1" runat="server" ShowEnrollment="True" ShowTerm="True" ShowAcaYr="false"
                                        OnTransferToParent="TransferToParent" />
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <table class="contenttable" style="padding: 5px; width: 80%">

                                                <tr>
                                                    <td>

                                                        <table width="100%">
                                                            <tr>
                                                                <td class="contentcell" style="width: 300px">
                                                                    <asp:Label ID="lblStartDate" CssClass="label" runat="server"></asp:Label>
                                                                </td>
                                                                <td class="contentcell4">

                                                                    <telerik:RadDatePicker ID="txtStartDate" MinDate="1/1/1945" runat="server" Width="325px">
                                                                    </telerik:RadDatePicker>

                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblEndDate" CssClass="label" runat="server"></asp:Label>
                                                                </td>
                                                                <td class="contentcell4">

                                                                    <telerik:RadDatePicker ID="txtEndDate" MinDate="1/1/1945" runat="server" Width="325px">
                                                                    </telerik:RadDatePicker>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblStatusCodeId" CssClass="label" runat="server"></asp:Label>
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:DropDownList ID="ddlStatusCodeId" runat="server" CssClass="dropdownlist" Width="325px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblLOAReasonId" CssClass="label" runat="server"></asp:Label>
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:DropDownList ID="ddlLOAReasonId" runat="server" CssClass="dropdownlist" Width="325px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblLOARequestDate" CssClass="label" runat="server">LOA Request Date<font color="red">*</font></asp:Label>
                                                                </td>
                                                                <td class="contentcell4">

                                                                    <telerik:RadDatePicker ID="txtLOARequestDate" MinDate="1/1/1945" runat="server" Width="325px">
                                                                    </telerik:RadDatePicker>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">&nbsp;
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:Button ID="btnLOA" runat="server" Text="Change to LOA" CausesValidation="True"></asp:Button>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>

                                            </table>
                                        </asp:Panel>
                                        <!--end table content-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>

            <asp:TextBox ID="txtStuEnrollment" TabIndex="2" runat="server" Visible="false"
                Width="0%" BorderStyle="None" BorderWidth="0">StuEnrollment</asp:TextBox><asp:TextBox ID="txtStuEnrollmentId" runat="server" Visible="false"
                    Width="0%" BorderStyle="None" BorderWidth="0">StuEnrollmentId</asp:TextBox>
            <asp:TextBox ID="txtAcademicYear" TabIndex="8" runat="server" Width="0" BorderStyle="None" BorderWidth="0" Visible="false">AcademicYear</asp:TextBox><asp:TextBox ID="txtAcademicYearId" runat="server" Visible="false"
                Width="0%" BorderStyle="None" BorderWidth="0">AcademicYearId</asp:TextBox>
            <asp:TextBox ID="txtTerm" TabIndex="9" runat="server" Width="0" BorderStyle="None" BorderWidth="0" Visible="false">Term</asp:TextBox><asp:TextBox
                ID="txtTermId" runat="server" Width="0" BorderStyle="None" BorderWidth="0" Visible="false">TermId</asp:TextBox>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <!-- end footer -->
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
</asp:Content>
