﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class StudentEntranceTest
    Inherits BasePage

    Protected StudentId As String
    Protected UserId, campusId As String
    Protected pObj As New UserPagePermissionInfo
    Protected resourceId As Integer
    Protected boolSwitchCampus As Boolean = False



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected state As AdvantageSessionState
    Protected LeadId As String
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim fac As New UserSecurityFacade


        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        UserId = AdvantageSession.UserState.UserId.ToString

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(372) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With


        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'pObj = Header1.UserPagePermission

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        End If

        'Delete Should Always Be Disabled
        btnDelete.Enabled = False

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            BuildModuleDDL()
            BindDataGrid()

            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            'End If
            MyBase.uSearchEntityControlId.Value = ""

        End If
        Dim objMRUFac As New MRUFacade
        InitializeComponent()


    End Sub
    Private Sub BindDataGrid()
        Dim getDB As New LeadEntranceFacade
        Dim DsEntrancetest As New DataSet

        DsEntrancetest = getDB.GetStudentGridRequirementDetailsByEffectiveDates(StudentId, campusId)
        ViewState("DsEntranceTest") = DsEntrancetest
        dgrdTransactionSearch.DataSource = DsEntrancetest
        dgrdTransactionSearch.DataBind()


        ' ClearActualScore()
    End Sub
    Private Sub BuildModuleDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlModule
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = DocumentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
        End With

    End Sub

    Public Sub SetUpEntranceTest()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        'Dim strEntrTestId As String
        'Dim intRequired As Integer
        Dim intArrCount As Integer
        '  Dim intOverRideCount As Integer
        Dim intResult As Integer
        Dim z As Integer
        Dim Facade As New LeadFacade
        Dim sMessage As String

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        Try
            'Loop thru the collection to get the Array Size
            Dim strAlpha As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            strAlpha = strAlpha.ToLower.ToString
            sMessage = ""
            Dim sMessageCharacters As String = ""

            Dim strActualValue As String
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                Dim z1 As Integer
                If Not Trim(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = "" Then
                    For z1 = 1 To CType(iitem.FindControl("txtActualScore"), TextBox).Text.Length
                        strActualValue = Mid(CType(iitem.FindControl("txtActualScore"), TextBox).Text.ToLower.ToString, z1, 1)
                        If InStr(strAlpha, strActualValue) >= 1 Then
                            sMessageCharacters &= "No characters are allowed in Actual Score for " & CType(iitem.FindControl("lblEntrTestDescrip"), Label).Text & vbLf
                            Exit For
                        End If
                    Next
                End If
            Next
            If Not sMessageCharacters = "" Then
                DisplayErrorMessage(sMessageCharacters)
                Exit Sub
            End If

            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                ' If Not CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True Then
                If Not Trim(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = "" Then
                    If CInt(CType(iitem.FindControl("txtActualScore"), TextBox).Text) >= 1 Then
                        If CType(iitem.FindControl("txtTestTaken"), TextBox).Text = "" Then
                            sMessage &= "Completed date is required for " & CType(iitem.FindControl("lblEntrTestDescrip"), Label).Text & vbLf
                        End If
                    End If
                End If
                'End If
            Next

            If Not sMessage = "" Then
                DisplayErrorMessage(sMessage)
                Exit Sub
            End If

            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                ' If Not (CType(iitem.FindControl("txtTestTaken"), TextBox).Text) = "" Or CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True Then
                intArrCount += 1
                'End If
            Next

            'For i = 0 To iitems.Count - 1
            '    iitem = iitems.Item(i)
            '    intOverRideCount += 1
            'Next

            'If intOverRideCount >= 1 Then
            '    Dim overrideselectedEntranceTest() As String = overrideselectedEntranceTest.CreateInstance(GetType(String), intOverRideCount)
            '    Dim overselectedOverRide() As String = selectedOverRide.CreateInstance(GetType(String), intArrCount)
            'End If

            If intArrCount >= 1 Then
                Dim selectedEntranceTest() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedRequired() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedPass() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedTestTaken() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedActualScore() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedMinScore() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedComments() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedOverRide() As String = Array.CreateInstance(GetType(String), intArrCount)

                'Loop Through The Collection To Get ClassSection

                For i = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    'retrieve clssection id from the datagrid

                    'Modified by balaji on 10/13/2005
                    '  If Not (CType(iitem.FindControl("txtTestTaken"), TextBox).Text) = "" Or CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True Then
                    selectedEntranceTest.SetValue(CType(iitem.FindControl("lblEntrTestId"), Label).Text, z)
                    If (CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True) Then
                        selectedRequired.SetValue("Yes", z)
                    Else
                        selectedRequired.SetValue("No", z)
                    End If
                    If (CType(iitem.FindControl("chkPass"), CheckBox).Checked = True) Then
                        selectedPass.SetValue("Yes", z)
                    Else
                        selectedPass.SetValue("No", z)
                    End If
                    selectedTestTaken.SetValue(CType(iitem.FindControl("txtTestTaken"), TextBox).Text, z)
                    selectedActualScore.SetValue(CType(iitem.FindControl("txtActualScore"), TextBox).Text, z)
                    selectedMinScore.SetValue(CType(iitem.FindControl("lblMinscore"), Label).Text, z)
                    selectedComments.SetValue(CType(iitem.FindControl("txtComments"), TextBox).Text, z)
                    If (CType(iitem.FindControl("chkPass"), CheckBox).Checked = True) Then
                        selectedOverRide.SetValue("No", z)
                    Else
                        If (CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True) Then
                            selectedOverRide.SetValue("Yes", z)
                        Else
                            selectedOverRide.SetValue("No", z)
                        End If
                    End If
                    z += 1
                    ' End If
                Next

                'resize the array
                'If z > 0 Then ReDim Preserve selectedClsSection(z - 1)
                'If z > 0 Then ReDim Preserve selectedRequired(z - 1)
                intResult = Facade.InsertStudentEntranceTest(StudentId, selectedEntranceTest, selectedRequired, selectedPass, selectedTestTaken, selectedActualScore, selectedMinScore, selectedComments, selectedOverRide, Session("UserName"))
            End If
            BindDataGrid()
        Finally
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SetUpEntranceTest()

        If Not ViewState("DsEntranceTest") Is Nothing Then
            Dim dsentrancetest As New DataSet
            dsentrancetest = ViewState("DsEntranceTest")
            Dim dtentrancetest As DataTable = dsentrancetest.Tables(0)
            If ddlModule.SelectedItem.Text = "Select" Then
                dgrdTransactionSearch.DataSource = dsentrancetest
                dgrdTransactionSearch.DataBind()
            Else
                Dim query = _
            From EntranceTable In dtentrancetest.AsEnumerable() _
            Where EntranceTable.Field(Of String)("ModuleName") = ddlModule.SelectedItem.Text _
            Select EntranceTable

                dgrdTransactionSearch.DataSource = query
                dgrdTransactionSearch.DataBind()
            End If
        Else
            BindDataGrid()
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub ClearActualScore()

        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer

        iitems = dgrdTransactionSearch.Items
        Try
            'Loop thru the collection to get the Array Size

            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If Trim(CType(iitem.FindControl("txtTestTaken"), TextBox).Text) = "" Then
                    If CInt(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = 0 Then
                        CType(iitem.FindControl("txtTestTaken"), TextBox).Text = ""
                    End If
                End If
            Next
        Finally
        End Try
    End Sub

    Private Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        iitems = dgrdTransactionSearch.Items
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            If Trim(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = "0.00" Then
                ''If Trim(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = "0" Then
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                CType(iitem.FindControl("txtActualScore"), TextBox).Text = ""
            End If
        Next
        ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim txtActualScore As TextBox = CType(e.Item.FindControl("txtActualScore"), TextBox)
            txtActualScore.Attributes.Add("onkeypress", "return DecimalNumbers(" + txtActualScore.ClientID + ", 3)")
        End If
        ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Protected Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged

        If Not ViewState("DsEntranceTest") Is Nothing Then
            Dim dsentrancetest As New DataSet
            dsentrancetest = ViewState("DsEntranceTest")
            Dim dtentrancetest As DataTable = dsentrancetest.Tables(0)
            If ddlModule.SelectedItem.Text = "Select" Then
                dgrdTransactionSearch.DataSource = dsentrancetest
                dgrdTransactionSearch.DataBind()
            Else
                Dim query = _
            From EntranceTable In dtentrancetest.AsEnumerable() _
            Where EntranceTable.Field(Of String)("ModuleName") = ddlModule.SelectedItem.Text _
            Select EntranceTable

                dgrdTransactionSearch.DataSource = query
                dgrdTransactionSearch.DataBind()
            End If
        End If

    End Sub
End Class