﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="GraduateAuditGroup.aspx.vb" Inherits="AdvWeb.AR.GraduateAuditGroup" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register Src="~/usercontrols/AdvantageRadAjaxLoadingControl.ascx" TagPrefix="uc1" TagName="AdvantageRadAjaxLoadingControl" %>



<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <uc1:AdvantageRadAjaxLoadingControl runat="server" ID="AdvantageRadAjaxLoadingControl1" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="AdvantageRadAjaxLoadingControl1">
        <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
            BorderWidth="0px">
            <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">

                <table width="99%" border="0" cellpadding="0" cellspacing="0" id="Table1">

                    <!-- begin rightcolumn -->
                    <tr>
                        <td>
                            <div class="boxContainer">
                                <h3><%=Header.Title  %></h3>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                                    <!-- begin top menu (save,new,reset,delete,history)-->
                                    <tr>
                                        <td class="menuframe" align="right">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                            <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                                Enabled="False"></asp:Button></td>
                                    </tr>

                                </table>
                                <table style="width: 40%;">
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblEnrollmentId" CssClass="label" runat="server">Enrollment</asp:Label></td>
                                        <td class="contentcell4date">
                                            <asp:DropDownList ID="ddlEnrollmentId" runat="server" CssClass="dropdownlist" AutoPostBack="True"></asp:DropDownList></td>
                                        <td>
                                            <telerik:RadButton ID="btnExportPdf" runat="server" Text="Export To PDF" ImageUrl="~/Images/Pdf.png" OnClick="BtnExportToPdfClick"></telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                            <div class="boxContainer noBorder">
                            <!-- end top menu (save,new,reset,delete,history)-->
                            <!--begin right column-->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="" align="center">
                                <tr>
                                    <td>
                                        <!-- begin table content-->
                                        <div>
                                            <!-- begin table content-->
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td>
                                                        <telerik:RadGrid ID="masterGrid" runat="server" OnItemDataBound="MasterGridItemCreated">
                                                            <MasterTableView AutoGenerateColumns="False">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="Descrip" HeaderText="" UniqueName="Descrip" />
                                                                    <telerik:GridBoundColumn DataField="EquivCourseDesc" HeaderText="" UniqueName="EquivCourseDesc" ItemStyle-HorizontalAlign="Left" />
                                                                    <telerik:GridBoundColumn DataField="Credits" HeaderText="" UniqueName="Credits" ItemStyle-HorizontalAlign="Right" />
                                                                    <telerik:GridBoundColumn DataField="Hours" HeaderText="" UniqueName="Hours" ItemStyle-HorizontalAlign="Right" />
                                                                    <telerik:GridBoundColumn DataField="Grade" HeaderText="" UniqueName="Grade" ItemStyle-HorizontalAlign="Right" />
                                                                    <telerik:GridBoundColumn DataField="CreditsEarned" HeaderText="" UniqueName="CreditsEarned" ItemStyle-HorizontalAlign="Right" />
                                                                    <telerik:GridBoundColumn DataField="CreditsRemaining" HeaderText="" UniqueName="CreditsRemained" ItemStyle-HorizontalAlign="Right" />
                                                                    <telerik:GridBoundColumn DataField="HoursCompleted" HeaderText="" UniqueName="HoursCompleted" ItemStyle-HorizontalAlign="Right" />
                                                                    <telerik:GridBoundColumn DataField="HoursRemaining" HeaderText="" UniqueName="HoursRemaining" ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </MasterTableView>
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>

                                            </table>

                                            <asp:TextBox ID="txtStudentId" runat="server" Visible="false"></asp:TextBox><asp:TextBox ID="txtStEmploymentId" runat="server" Visible="false"></asp:TextBox><asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox><asp:TextBox ID="txtStudentDocs" runat="server" Visible="False"></asp:TextBox><!--end table content-->
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </td>
                        <!-- end rightcolumn -->
                    </tr>
                </table>
                <!-- start validation panel-->
                <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
                <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                    ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
                <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                    ShowMessageBox="True"></asp:ValidationSummary>
                <!--end validation panel-->

            </telerik:RadPane>
        </telerik:RadSplitter>
    </telerik:RadAjaxPanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>



