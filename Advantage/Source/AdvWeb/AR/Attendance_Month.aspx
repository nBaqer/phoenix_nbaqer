<%@ Page Language="VB" AutoEventWireup="true" CodeFile="Attendance_Month.aspx.vb" Inherits="Attendance_Month" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Calendar [Monthly View]</title>
     <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <style>
        .calendar
 {
  border:none;
}
.day
{
  width:100%;
  height:70px;
  text-align:right;
  vertical-align:top;
  font-family:Verdana;
  font-style:normal;
  font-size:10px;
  color:Black;
  background-color:#FFFFFF;
  border:solid 1px #ebebeb;
}
.daytext
{
  width:100%; 
  height:100%;
 text-align:left;
  vertical-align:top;
  font-family:Verdana;
  font-style:bold;
  font-size:10px;
  color:Black;
  background-color:#FFFFFF;
  white-space: nowrap;
}
.othermonthday
{
  background-color:#E9EDF2;
  width:90px;
  height:70px;
  text-align:right;
  vertical-align:top;
  font-family:Arial;
  font-size:11px;
  color:Black;
  border:solid 1px #ebebeb;
}
.month
{
  border-collapse:collapse;
  border:solid 1px black;
}
.dayheader
{
  background-color:#E9EDF2;
  background-image:url(images/header_bg.gif);
  color:#000066;
  font-family:Verdana;
  font-size:11px;
  text-align:center;
  border-top:solid 1px #ebebeb;
  border-left:solid 1px #ebebeb;
  border-bottom:solid 1px #ebebeb;
  border-right:solid 1px #ebebeb;
  padding: 4px;
}
.title
{
  background-color:#E9EDF2;
  color:#000066;
  background-image:url(images/title_bg.gif);
  border-bottom-width: 0px;
  border-top:solid 0px #ebebeb;
  border-left:solid 0px #ebebeb;
  border-bottom:solid 1px #ebebeb;
  border-right:solid 0px #ebebeb;
}

.title TD
{
  font-family:verdana;
  font-size:11px;
  font-weight:bold;
  color:000066;
  padding-top:1px;
  padding-bottom:1px;
}
.nextprev
{
  cursor:none;
  font-family:verdana;
  font-size:11px;
  font-weight:bold;
  color:000066;
  padding-top:1px;
  padding-bottom:1px;
  color:#000066;
}
.pageheader
{
   background-color:#E9EDF2;
  background-image:url(images/header_bg.gif);
  color:#504C39;
  font-family:Verdana;
  font-size:11px;
  text-align:center;
  border-top:solid 1px #ebebeb;
  border-left:solid 1px #ebebeb;
  border-bottom:solid 1px #ebebeb;
  border-right:solid 1px #ebebeb;
  padding: 2px;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		
		<table id="Table3" width="100%" cellpadding="0" cellspacing="0" class="pageheader">
        	<tr>
		        <th style="text-align: left" width="60%" class="label" nowrap>&nbsp;&nbsp;&nbsp;Attendance of <asp:Label ID="lblStudentName" runat="server"></asp:Label> - <asp:Label ID="lblPrgVersion" runat="server"></asp:Label></th>
		        <th style="text-align: center" width="30%" class="label" nowrap><a class="close" href="#" onclick="window.print()">Print Screen</a></th>
		        <th style="text-align: right" width="10%" nowrap class="label"><a class="close" href="#" onclick="top.close()">X Close</a></th>
		    </tr>
		</table>
		<p></p>
 <table id="Table1" width="90%" cellpadding="0" cellspacing="0">		
       <tr>
                <td colspan=6 bgcolor="#cccccc">
                    <asp:DropDownList id="drpCalMonth" Runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" cssClass="dropdownlist" Width="100px"></asp:DropDownList>
                    <asp:DropDownList id="drpCalYear" Runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" cssClass="dropdownlist" Width="80px"></asp:DropDownList>
                </td>
        </tr>
</table>
 <table id="Table2" width="100%" cellpadding="0" cellspacing="0" height="150px">		
		<tr>
		    <td colspan=6>
            <asp:Calendar ID="Calendar1" runat="server"  Width="90%"
                NextPrevFormat="FullMonth" 
                BorderColor="#83929f" 
                BorderStyle="Solid" 
                BorderWidth="1px" 
                DayNameFormat="Full" >
                <TodayDayStyle CssClass="day" BorderStyle="Solid" />
                <DayStyle CssClass="day" Font-Underline=false />
                <OtherMonthDayStyle CssClass="othermonthday" Font-Underline=false/>
                <NextPrevStyle CssClass="nextprev" />
                <DayHeaderStyle CssClass="dayheader" />
                <TitleStyle CssClass="title"  />                      
            </asp:Calendar>
            </td>
        </tr>
        </table>  
        <!--</div>      -->
    </div>
    </form>
</body>
</html>
