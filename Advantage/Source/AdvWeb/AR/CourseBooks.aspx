<%@ Page Language="vb" AutoEventWireup="false" Inherits="CourseBooks" CodeFile="CourseBooks.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Setup Course Books</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>

    <link href="../css/localhost.css" type="text/css" rel="stylesheet">
    <link href="../css/systememail.css" type="text/css" rel="stylesheet">
</head>
<body runat="server" leftmargin="0" topmargin="0">
    <form id="Form1" method="post" runat="server">
        <!-- beging header -->
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <img src="../images/advantage_setup_books.jpg"></td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href=#>X Close</a></td>
            </tr>
        </table>
        <!-- end header -->
        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" height="100%">
            <tr>
                <td class="DetailsFrameTop">
                    
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="MenuFrame" align="right">
								    <asp:button id="btnSave" runat="server" Text="Save" CssClass="save"></asp:button>
                                    <asp:button id="btnNew" runat="server" Text="New" CssClass="new" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False"></asp:button>
								</td>
							</tr>
						</table>
			
			<table id="Table5" cellSpacing="0" cellPadding="0" width="90%" style="margin:25px;"
				border="0">
                            <tr>
                                <td class="detailsframeff">
                                    <div><asp:Panel ID="pnlRHS" runat="server">
                                        <table cellspacing="0" cellpadding="0" width="50%" border="0" align="center">
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblCourseDescrip" runat="server" CssClass="Label">Course</asp:Label></td>
                                                <td class="twocolumncontentcell">
                                                    <asp:TextBox ID="txtCourseDescrip" runat="server" ReadOnly="true" CssClass="TextBox"
                                                        MaxLength="128"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                <td class="twocolumncontentcell" style="text-align: left; padding-bottom: 30px">
                                                    <asp:CheckBox ID="chkIsReq" Text="Required" runat="server" CssClass="Label"></asp:CheckBox></td>
                                            </tr>
                                        </table>
                                        <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" align="center">
											<TR>
												<td nowrap class="threecolumnheader"><asp:Label ID="lblAvailBooks" runat="server" CssClass="Labelbold">Books Not Assigned</asp:Label></TD>
												<td nowrap class="threecolumnspacer"></td>
												<td nowrap class="threecolumnheader"><asp:Label ID="lblSelBooks" runat="server" CssClass="Labelbold">Books Assigned</asp:Label></TD>
											</TR>
											<TR>
												<td nowrap class="threecolumncontent"><asp:ListBox ID="lbxAvailBooks" runat="server" SelectionMode="Multiple" CssClass="listboxes" Height="250px"
                                                      AutoPostBack="True"  Rows="10"></asp:ListBox></TD>
												<td nowrap class="threecolumnbuttons"><asp:Button ID="btnAdd" runat="server" Text="Add >>"  Enabled="false" Width="100%"></asp:Button><br />
                                                                <asp:Button ID="btnRemove" runat="server" Text="Remove <<"  Enabled="False" Width="100%"></asp:Button><br />
                                                                <asp:Button ID="btnEdit" runat="server" Text="Edit"  Enabled="False" Visible =false Width="100%"></asp:Button>
												</TD>
												<td nowrap class="threecolumncontent"><asp:ListBox ID="lbxSelectBooks" runat="server" SelectionMode="Multiple" CssClass="listboxes" Height="250px" AutoPostBack="True"
                                                        Rows="10"></asp:ListBox></TD>
											</TR>
										</TABLE>
                                        <table cellspacing="0" cellpadding="0" width="60%" border="0" align="center">
                                            <tr>
                                                <td class="contentcell" nowrap>
                                                </td>
                                                <td class="emptycell">
                                                </td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtCourseBkId" runat="server" CssClass="TextBox" MaxLength="128"
                                                        Visible="false"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap>
                                                </td>
                                                <td class="emptycell">
                                                </td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtCourseId" runat="server" CssClass="TextBox" MaxLength="128" Visible="false"></asp:TextBox></td>
                                            </tr>
                                        </table></asp:Panel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    
                </td>
            </tr>
        </table>
        <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
    </form>
</body>
</HTML>
