Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class DocStatuses
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then

            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList()

            '   bind an empty new DocStatusInfo
            BindDocStatusData(New DocStatusInfo)

            '   initialize buttons
            InitButtonsForLoad()

            'Header1.EnableHistoryButton(False)

        Else
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Call the procedure for edit
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BindDataList()

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radstatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind DocStatuss datalist
        dlstDocStatuses.DataSource = New DataView((New DocStatusesFacade).GetAllDocStatuses().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstDocStatuses.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
        BuildSysStatusDocsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildSysStatusDocsDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlSysDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "SysDocStatusId"
            .DataSource = statuses.GetAllSysDocStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", 0))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dlstDocStatuses_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstDocStatuses.ItemCommand

        '   get the DocStatusId from the backend and display it
        GetDocStatusId(e.CommandArgument)

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.SetHiddenControlForAudit()

        '   initialize buttons
        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlstDocStatuses, e.CommandArgument.ToString)
    End Sub
    Private Sub BindDocStatusData(ByVal DocStatus As DocStatusInfo)
        With DocStatus
            chkIsInDB.Checked = .IsInDB
            txtDocStatusId.Text = .DocStatusId
            txtDocStatusCode.Text = .Code
            If Not (DocStatus.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = DocStatus.StatusId
            ddlSysDocStatusId.SelectedValue = DocStatus.SysDocStatusId
            txtDocStatusDescrip.Text = .Description
            If Not (DocStatus.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = DocStatus.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        '   instantiate component
        Dim result As String
        With New DocStatusesFacade
            '   update DocStatus Info 
            result = .UpdateDocStatusInfo(BuildDocStatusInfo(txtDocStatusId.Text), Session("UserName")) 'context.User.Identity.Name
        End With

        '   bind datalist
        BindDataList()

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the DocStatusId from the backend and display it
            GetDocStatusId(txtDocStatusId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new DocStatusInfo
            'BindDocStatusData(New DocStatusInfo)

            '   initialize buttons
            InitButtonsForEdit()
        End If
        CommonWebUtilities.RestoreItemValues(dlstDocStatuses, txtDocStatusId.Text)
    End Sub
    Private Function BuildDocStatusInfo(ByVal DocStatusId As String) As DocStatusInfo
        'instantiate class
        Dim DocStatusInfo As New DocStatusInfo

        With DocStatusInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get DocStatusId
            .DocStatusId = DocStatusId

            'get Code
            .Code = txtDocStatusCode.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get DocStatus's name
            .Description = txtDocStatusDescrip.Text.Trim

            'get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            'get SysDocStatusId
            .SysDocStatusId = ddlSysDocStatusId.SelectedValue

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return DocStatusInfo
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click

        '   bind an empty new DocStatusInfo
        BindDocStatusData(New DocStatusInfo)

        'initialize buttons
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlstDocStatuses, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If Not (txtDocStatusId.Text = Guid.Empty.ToString) Then

            'update DocStatus Info 
            Dim result As String = (New DocStatusesFacade).DeleteDocStatusInfo(txtDocStatusId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new DocStatusInfo
                BindDocStatusData(New DocStatusInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If
            CommonWebUtilities.RestoreItemValues(dlstDocStatuses, Guid.Empty.ToString)
        End If
    End Sub

    Private Sub InitButtonsForLoad()

        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False

    End Sub

    Private Sub InitButtonsForEdit()

        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    End Sub

    Private Sub GetDocStatusId(ByVal DocStatusId As String)

        '   bind DocStatus properties
        BindDocStatusData((New DocStatusesFacade).GetDocStatusInfo(DocStatusId))
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        '   bind datalist
        BindDataList()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

End Class
