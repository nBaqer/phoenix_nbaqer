<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TardyMinutes.aspx.vb" Inherits="AR_TardyMinutes" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Absence Summary</title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <div>
        <asp:Panel ID="pnlReportingAgenciesHeader" runat="server" Visible="True">
            <table class="contenttable" cellspacing="0" cellpadding="0" width="90%" align="center"
                style="border: 1px solid #ebebeb;">
                <tr>
                    <td class="contentcellheader" nowrap colspan="8" style="border-top: 0px; border-right: 0px;
                        border-left: 0px" align="left">
                        <asp:Label ID="lblReportingAgencies" runat="server" Font-Bold="true" CssClass="label">Absence Summary</asp:Label>
                        <asp:Label ID="Label3" runat="Server" CssClass="labelbold">(Number of tardies that equals 1 absence:</asp:Label>
                        <asp:Label ID="Label4" runat="Server" CssClass="labelbold"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="close" href="#" onclick="top.close()">X
                            Close Window</a>
                    </td>
                </tr>
                <tr>
                    <td class="contentcellcitizenship" colspan="6" style="padding-top: 12px">
                        <asp:Panel ID="pnlReportingAgencies" TabIndex="12" runat="server" EnableViewState="false">
                            <table>
                                <tr>
                                    <td width="40%">
                                        &nbsp;
                                    </td>
                                    <td width="20%" nowrap>
                                        <asp:Label ID="lblPresent" runat="server" CssClass="label">Present</asp:Label>
                                    </td>
                                    <td width="20%" nowrap>
                                        <asp:Label ID="lblAbsent" runat="server" CssClass="label">Absent</asp:Label>
                                    </td>
                                    <td width="20%" nowrap>
                                        <asp:Label ID="lblTardy" runat="server" CssClass="label">Tardy</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblActualTotals" runat="server" CssClass="labelbold">Overall Actual Totals</asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="lblActPresent" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="lblActAbsent" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="lblActTardy" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblAdjTotals" runat="server" CssClass="labelbold">Overall Adjusted Totals</asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="lblAdjPresent" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="lblAdjAbsent" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="lblAdjTardy" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" CssClass="labelbold">Tardy Penalty</asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="Label2" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="lblTardyPenalty" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td nowrap>
                                        <asp:Label ID="Label6" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr height="20">
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="left">
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
