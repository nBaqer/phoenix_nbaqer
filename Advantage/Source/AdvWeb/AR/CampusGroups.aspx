<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="CampusGroups.aspx.vb" Inherits="CampusGroups" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstCampGrps" runat="server" DataKeyField="CampGrpId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("CampGrpId")%>' Text='<%# container.dataitem("CampGrpDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>

                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtModUser" runat="server" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtModDate" runat="server" Visible="false"></asp:TextBox>
                                <asp:DropDownList ID="ddlCampusId" runat="server" Visible="false"></asp:DropDownList>
                                <asp:CheckBox ID="chkIsAllCampusGrp" runat="server" Visible="false" />
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpCode" runat="server" CssClass="label">Code</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCampGrpCode" runat="server" CssClass="textbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpDescrip" runat="server" CssClass="label">Description</asp:Label></td>
                                            <td class="contentcell4" style="padding-bottom: 20px">
                                                <asp:TextBox ID="txtCampGrpDescrip" runat="server" CssClass="textbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="Label1" runat="server" CssClass="label" Visible="false">Description</asp:Label></td>
                                            <td class="contentcell4" style="padding-bottom: 20px">
                                                <asp:TextBox ID="txtCampGrpId" runat="server" CssClass="textbox" Visible="False"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td class="threecolumnheader1">
                                                <asp:Label ID="lblAvailCamps" runat="server" CssClass="labelbold">Available Campuses</asp:Label></td>
                                            <td class="threecolumnspacer1"></td>
                                            <td class="threecolumnheader1">
                                                <asp:Label ID="lblSelCamps" runat="server" CssClass="labelbold">Selected Campuses</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="threecolumncontent">
                                                <asp:ListBox ID="lbxAvailCamps" runat="server" CssClass="listboxes" Rows="12"></asp:ListBox></td>
                                            <td class="threecolumnbuttons">
                                                <telerik:RadButton ID="btnAdd" runat="server" CssClass="buttons1" Text="Add >>" CausesValidation="False" Width="100px"></telerik:RadButton>
                                                <br>

                                                <telerik:RadButton ID="btnAddAll" runat="server" CssClass="buttons1" Text="Add All >>" CausesValidation="False" Width="100px"></telerik:RadButton>
                                                <br>
                                                <br>

                                                <telerik:RadButton ID="btnRemove" runat="server" CssClass="buttons1" Text="Remove <<" CausesValidation="False" Width="100px"></telerik:RadButton>
                                                <br>

                                                <telerik:RadButton ID="btnRemoveAll" runat="server" CssClass="buttons1" Text="Remove all <<" CausesValidation="False" Width="100px"></telerik:RadButton>

                                            </td>
                                            <td class="threecolumncontent">
                                                <asp:ListBox ID="lbxSelectCamps" runat="server" CssClass="listboxes" Rows="12"></asp:ListBox></td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6">
                                                    <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcell2" colspan="6">
                                                    <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false"></asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>

                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>

    </div>

</asp:Content>


