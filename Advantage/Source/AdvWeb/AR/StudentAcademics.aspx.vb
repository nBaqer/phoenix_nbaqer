﻿Imports System.Diagnostics
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common

Partial Class StudentAcademics
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected StudentId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    'Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String
    'Private writtenExamsCount As Integer
    'Private practicalExamsCount As Integer
    'Private writtenExamsAverage As Decimal
    'Private practicalExamsAverage As Decimal
    Private sumOfWeightsIsZero As Boolean = False

    Protected state As AdvantageSessionState
    Protected LeadId As String
    Protected boolSwitchCampus As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings



    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region


    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Dim objCommon As New CommonUtilities

        campusid = Master.CurrentCampusId

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU
        objStudentState = GetStudentFromStateObject(538) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString


        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        'Always disable the History button. It does not apply to this page.
        'Header1.EnableHistoryButton(False)
        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            CleanCache()
            BuildStudentEnrollmentsDDL(StudentId)
            BindTreeView()
            If tvAcademics.Nodes.Count > 0 Then
                tvAcademics.Nodes(0).Selected = True
                tvAcademics_SelectedNodeChanged(Me, New EventArgs())
            End If
            StudentInfo()
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            'End If

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        End If
        btnPrint.Attributes.Add("onClick", "getPrint('ContentMain2_print_area');")
    End Sub
    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade

        With ddlEnrollmentId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()
            'If Not .Items.Count = 1 Then
            '    .Items.Insert(0, New ListItem("All Enrollments", Guid.Empty.ToString))
            'End If
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindTreeView()
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Session("xmlDocTree") = xmlDoc.OuterXml
        XmlDataSource1.Data = xmlDoc.OuterXml
        tvAcademics.Nodes.Clear()
        tvAcademics.DataSource = XmlDataSource1
        If Not String.IsNullOrEmpty(XmlDataSource1.Data) Then tvAcademics.DataBind()

    End Sub
    Private Sub StudentInfo()
        Try
            Dim stuEnrollInfo As StudentEnrollmentInfo
            stuEnrollInfo = (New StuEnrollFacade).GetStudentDetails(ddlEnrollmentId.SelectedValue)
            lblStudentName.Text = (New StuEnrollFacade).StudentName(StudentId) 'CType(Header1.FindControl("txtStudentName"), TextBox).Text
            With stuEnrollInfo
                lblEnrollmentName.Text = .ProgramVersion
                lblEnrollmentStatus.Text = .StatusCode
                lblStartDate.Text = .StartDate
                lblGD.Text = .ExpGradDate
            End With
            pnlStudentInfo.Visible = True
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            btnPrint.Visible = False
            pnlStudentInfo.Visible = False
            Exit Sub
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Function ConvertIsPassToString(ByVal isPass As Boolean) As String
        If isPass = 0 Then Return "F" Else Return "P"
    End Function
    Protected Sub ddlEnrollmentId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
        BindTreeView()
        rightPanel.Visible = False
        StudentInfo()
    End Sub
    Private Function GetGradeBookResults(ByVal stuEnrollId As String) As XmlDocument
        Dim xmlDoc As XmlDocument
        xmlDoc = Cache.Item(stuEnrollId)
        If xmlDoc Is Nothing Then
            Dim gradeBookResults As String = (New ExamsFacade).GetGradeBookResultsByStudent_SP(stuEnrollId)
            xmlDoc = New XmlDocument()
            If Not (gradeBookResults Is Nothing) Then
                xmlDoc.LoadXml(gradeBookResults)
                CalculateAverages(xmlDoc.DocumentElement)
            End If
            Cache.Add(stuEnrollId, xmlDoc, Nothing, DateTime.MaxValue, New TimeSpan(1, 0, 0), CacheItemPriority.Default, Nothing)
        End If
        Return xmlDoc
    End Function

    Protected Sub tvAcademics_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles tvAcademics.SelectedNodeChanged
        rightPanel.Visible = True
        Dim sn As TreeNode = tvAcademics.SelectedNode
        Select Case tvAcademics.SelectedNode.Depth
            Case 0  'Program Version
                'Name
                SetName(True, sn.Text)
                'Average
                SetAverage(True, "ProgramVersion[@PrgVerId='" + sn.Value + "']")
                'Score
                SetScore(False)
                'PostDate
                SetPostDate(False, Nothing)
                'RequiredCompletedRemaining
                SetRequiredCompletedRemaining(False, Nothing)
                'RequirementsMet
                SetRequirementsMet(True, sn.ValuePath)
                'Posted Date
                SetPostedDate(False, Nothing)
                'Dates
                SetDates(False, Nothing)
                'Passing Score
                SetPassingScore(False, Nothing)
                'Weight
                SetWeight(False, Nothing)
                'Required
                SetRequired(False, Nothing)
                'MustPass
                SetMustPass(False, Nothing)
                'Credits Panel
                SetCredits(True, sn.ValuePath)
            Case 1  'Module
                'name
                SetName(True, sn.Text)
                'average
                SetAverage(True, "ProgramVersion/Module[@GrpId='" + sn.Value + "']")
                'Score
                SetScore(False)
                'PostDate
                SetPostDate(False, Nothing)
                'RequiredCompletedRemaining
                SetRequiredCompletedRemaining(False, Nothing)
                'RequirementsMet
                SetRequirementsMet(True, sn.ValuePath)
                'Posted Date
                SetPostedDate(False, Nothing)
                'Dates
                SetDates(True, sn.ValuePath)
                'Passing Score
                SetPassingScore(False, Nothing)
                'Weight
                SetWeight(False, Nothing)
                'Required
                SetRequired(False, Nothing)
                'MustPass
                SetMustPass(False, Nothing)
                'Credits Panel
                SetCredits(True, sn.ValuePath)
            Case 2  'Course
                'name
                SetName(True, sn.Text)
                'average
                SetAverage(False, Nothing)
                'Score
                SetScore(True)
                rScore.Text = GetAverage(GetXPathForCourseFromTreeviewPath(sn.ValuePath)).ToString("###.00")
                'PostDate
                If rScore.Text = 0 Then
                    rScore.Text = GetAverageNew(GetXPathForCourseFromTreeviewPath(sn.ValuePath))

                End If

                SetPostDate(False, Nothing)
                'RequiredCompletedRemaining
                SetRequiredCompletedRemaining(False, sn.ValuePath)
                'RequirementsMet
                SetRequirementsMet(True, sn.ValuePath)
                'Posted Date
                SetPostedDate(False, Nothing)
                'Dates
                SetDates(False, Nothing)
                'Passing Score
                SetPassingScore(False, Nothing)
                'Weight
                'lWeight.Visible = True
                'rWeight.Visible = True
                'rWeight.Text = GetCourseWeight(sn.ValuePath).ToString("####.00")
                lWeight.Visible = False
                rWeight.Visible = False
                'Required
                SetRequired(False, Nothing)
                'MustPass
                SetMustPass(False, Nothing)
                'Credits Panel
                SetCredits(True, sn.ValuePath)
            Case 3  'GradeBookResult
                'name
                SetName(True, sn.Text)
                'average
                SetAverage(False, Nothing)
                Dim sysComponentTypeId As Integer = Integer.Parse(GetValue(sn.ValuePath, "SysComponentTypeId"))
                Select Case sysComponentTypeId
                    Case 500, 503
                        'Score
                        SetScore(False)
                        rScore.Text = GetValue(sn.ValuePath, "Score")
                        'PostDate
                        SetPostDate(True, sn.ValuePath)
                        'RequiredCompletedRemaining
                        SetRequiredCompletedRemaining(True, sn.ValuePath)
                    Case 501, 499, 502
                        'Score
                        SetScore(True)
                        rScore.Text = GetValue(sn.ValuePath, "Score")
                        'PostDate
                        SetPostDate(True, sn.ValuePath)
                        'RequiredCompletedRemaining
                        SetRequiredCompletedRemaining(False, sn.ValuePath)
                    Case 533
                        'Score
                        SetScore(True)
                        rScore.Text = GetValue(sn.ValuePath, "Score")
                        'PostDate
                        SetPostDate(True, sn.ValuePath)
                        'RequiredCompletedRemaining
                        SetRequiredCompletedRemaining(False, sn.ValuePath)
                    Case 544
                        'Score
                        SetScore(False)
                        'PostDate
                        SetPostDate(True, sn.ValuePath)
                        'RequiredCompletedRemaining
                        SetRequiredCompletedRemaining(True, sn.ValuePath)
                    Case Else
                        'Score
                        SetScore(False)
                        'PostDate
                        SetPostDate(False, Nothing)
                        'RequiredCompletedRemaining
                        SetRequiredCompletedRemaining(True, sn.ValuePath)
                End Select
                'RequirementsMet
                SetRequirementsMet(True, sn.ValuePath)
                'Posted Date
                SetPostedDate(False, sn.ValuePath)
                'Dates
                SetDates(False, Nothing)
                'Passing Score
                SetPassingScore(False, Nothing)
                'Weight
                SetWeight(True, sn.ValuePath)
                'Required
                SetRequired(True, sn.ValuePath)
                'MustPass
                SetMustPass(True, sn.ValuePath)
                'Credits Panel
                SetCredits(False, sn.ValuePath)
        End Select
    End Sub
    Private Function GetValue(ByVal path As String, ByVal attribute As String) As String
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']/Module[@GrpId='")
            .Append(part(1))    'GrpId
            .Append("']/Course[@ReqId='")
            .Append(part(2))    'ReqId
            .Append("']/GradeBook[@GrdBkResultId='")
            .Append(part(3))    'GrdBkResultId
            .Append("']/@")
            .Append(attribute)
        End With
        Dim node As XmlNode = xmlDoc.SelectSingleNode(xpath.ToString())
        If node Is Nothing Then
            Return String.Empty
        Else
            Return node.InnerText
        End If
    End Function
    Private Function GetDate(ByVal path As String, ByVal attribute As String) As String
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']/Module[@GrpId='")
            .Append(part(1))    'GrpId
            .Append("']/Course[@ReqId='")
            .Append(part(2))    'ReqId
            .Append("']/GradeBook[@GrdBkResultId='")
            .Append(part(3))    'GrdBkResultId
            .Append("']/@")
            .Append(attribute)
        End With
        Dim node As XmlNode = xmlDoc.SelectSingleNode(xpath.ToString())
        If node Is Nothing Then
            Return String.Empty
        Else
            Return Date.Parse(node.InnerText).ToShortDateString
        End If
    End Function
    Private Function GetAverage(ByVal xpath As String) As Decimal
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim node As XmlNode = xmlDoc.SelectSingleNode(xpath + "/@Average")
        If node Is Nothing Then

            Return 0.0
        Else
            Return Decimal.Parse(node.InnerText)
        End If
    End Function
    Private Function GetAverageNew(ByVal xpath As String) As String
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim node As XmlNode = xmlDoc.SelectSingleNode(xpath + "/@Average")
        If node Is Nothing Then
            node = xmlDoc.SelectSingleNode(xpath + "/@Score")
            If node Is Nothing Then
                node = xmlDoc.SelectSingleNode(xpath + "/@Grade")
                If node Is Nothing Then
                    Return 0.0
                Else
                    lScore.Text = "Grade"
                    Return node.InnerText
                End If
            Else
                lScore.Text = "Score"
                Return Decimal.Parse(node.InnerText)
            End If
        Else
            Return Decimal.Parse(node.InnerText)
        End If
    End Function
    Private Function GetScoreAverage(ByVal xpath As String) As String
        Dim xmlDoc As New XmlDocument()
        xmlDoc.LoadXml(CType(Session("xmlDocTree"), String))
        Dim node As XmlNode = xmlDoc.SelectSingleNode(xpath + "/@Score")
        If node Is Nothing Then
            Return 0.0
        Else
            Return Decimal.Parse(node.InnerText)
        End If
    End Function
    Private Function GetAverageForCourse(ByVal xpath As String) As Decimal
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim nodeList As XmlNodeList = xmlDoc.SelectNodes(xpath)
        Return GetAverageForNodeList(nodeList)
    End Function
    Private programSumOfCourseGrades As Decimal
    Private programNumberOfCourses As Decimal
    Private moduleSumOfCourseGrades As Decimal
    Private moduleNumberOfCourses As Decimal
    Private courseSumOfComponentWeightedGrades As Decimal
    Private courseSumOfComponentWeights As Decimal
    Private Sub CalculateAverages(ByVal node As XmlNode)
        Dim gradeRounding As String = MyAdvAppSettings.AppSettings("GradeRounding").ToString.ToLower()
        If node.HasChildNodes Then
            For Each childNode As XmlNode In node.ChildNodes
                CalculateAverages(childNode)
            Next
            Dim deep As Integer = GetNodeDeep(node)
            Select Case deep
                Case 1  'Program node
                    Dim average As XmlAttribute = node.OwnerDocument.CreateAttribute("Average")
                    If Not (programNumberOfCourses = 0.0) Then
                        Dim avg As Decimal = Math.Round(programSumOfCourseGrades / programNumberOfCourses, 2)
                        average.Value = avg.ToString()
                        node.Attributes.Append(average)
                    End If
                Case 2  'Module node
                    Dim average As XmlAttribute = node.OwnerDocument.CreateAttribute("Average")
                    If Not (moduleSumOfCourseGrades = 0.0) Then
                        Dim avg As Decimal = Math.Round(moduleSumOfCourseGrades / moduleNumberOfCourses, 2)
                        average.Value = avg.ToString()
                        node.Attributes.Append(average)
                        'programSumOfModuleGrades += avg
                        'programNumberOfModules += 1
                    End If
                    moduleSumOfCourseGrades = 0.0
                    moduleNumberOfCourses = 0.0
                Case 3  'Course node
                    Dim average As XmlAttribute = node.OwnerDocument.CreateAttribute("Average")
                    If Not (courseSumOfComponentWeights = 0.0) Then
                        If Not node.Attributes("Score") Is Nothing Then

                            Dim avg As Decimal
                            'commented by Theresa G on 7/22/09 for mantis 16909: QA: Average needs to be fixed on the Term level and Program level on the student academics tab. 
                            If gradeRounding = "yes" Then
                                avg = Math.Round(courseSumOfComponentWeightedGrades / courseSumOfComponentWeights)
                            Else
                                avg = Math.Round(courseSumOfComponentWeightedGrades / courseSumOfComponentWeights, 2)
                            End If
                            If gradeRounding = "yes" Then
                                avg = Math.Round(CType(node.Attributes("Score").Value, Decimal))
                            Else
                                avg = Math.Round(CType(node.Attributes("Score").Value, Decimal), 2)
                            End If
                            average.Value = avg.ToString()
                            node.Attributes.Append(average)
                            moduleSumOfCourseGrades += avg
                            moduleNumberOfCourses += 1
                            programSumOfCourseGrades += avg
                            programNumberOfCourses += 1
                        End If
                    End If
                    courseSumOfComponentWeightedGrades = 0.0
                    courseSumOfComponentWeights = 0.0
            End Select
        Else
            'this is a component node
            GetScoreAndWeightFromGradeBookNode(node, courseSumOfComponentWeightedGrades, courseSumOfComponentWeights)
        End If

    End Sub
    Private Function GetNodeDeep(ByVal node As XmlNode) As Integer
        Dim i As Integer = 0
        While Not (node.ParentNode Is Nothing)
            node = node.ParentNode
            i += 1
        End While
        Return i
    End Function
    Private Sub GetScoreAndWeightFromGradeBookNode(ByVal gradeBookNode As XmlNode, ByRef sumOfScores As Decimal, ByRef sumOfWeights As Decimal)
        If Not gradeBookNode.Attributes("SysComponentTypeId") Is Nothing Then
            Dim sysComponentTypeId As String = gradeBookNode.Attributes("SysComponentTypeId").Value
            Select Case Integer.Parse(sysComponentTypeId)
                Case 501, 533
                    Dim score As Decimal = 0.0
                    Dim obj As Object = gradeBookNode.Attributes("Score")
                    If Not obj Is Nothing Then
                        If Decimal.TryParse(CType(obj, XmlAttribute).Value, score) Then
                            If Not (gradeBookNode.Attributes("Weight")) Is Nothing Then
                                Dim weight As Decimal = 0.0
                                If Decimal.TryParse(gradeBookNode.Attributes("Weight").Value, weight) Then
                                    sumOfScores += score * weight
                                    sumOfWeights += weight
                                End If
                            End If
                        End If
                    End If
            End Select
        End If
    End Sub
    Private Function GetAverageForNodeList(ByVal nodeList As XmlNodeList) As Decimal
        Dim sumOfScores As Decimal = 0.0
        Dim sumOfWeights As Decimal = 0.0
        For Each gradeBookNode As XmlNode In nodeList
            GetScoreAndWeightFromGradeBookNode(gradeBookNode, sumOfScores, sumOfWeights)
        Next
        If sumOfWeights > 0.0 Then
            sumOfWeightsIsZero = False
            Return Math.Round(sumOfScores / sumOfWeights, 2)
        Else
            sumOfWeightsIsZero = True
            Return 0.0
        End If
    End Function
    Private Function GetXPathForCourseFromTreeviewPath(ByVal path As String) As String
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']/Module[@GrpId='")
            .Append(part(1))    'GrpId
            .Append("']/Course[@ReqId='")
            .Append(part(2))    'ReqId
            '.Append("']/GradeBook")
            .Append("']")
        End With
        Return xpath.ToString()
    End Function
    Private Function CheckRequirements(ByVal path As String) As Boolean
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']")
            .Append("/Module")
            If part.Length > 1 Then
                .Append("[@GrpId='")
                .Append(part(1))    'GrpId
                .Append("']")
            End If
            .Append("/Course")
            If part.Length > 2 Then
                .Append("[@ReqId='")
                .Append(part(2))    'ReqId
                .Append("']")
            End If
            .Append("/GradeBook")
            If part.Length > 3 Then
                .Append("[@GrdBkResultId='")
                .Append(part(3))    'GrdBkResultId
                .Append("']")
            End If
        End With
        Dim nodeList As XmlNodeList = xmlDoc.SelectNodes(xpath.ToString())
        For Each gradeBookNode As XmlNode In nodeList
            If Not CheckGradeBookNode(gradeBookNode) Then Return False
        Next
        'check atleast one component is attempted for a course
        '19751: QA: Issue with a course whose components are not required on the Academics tab. 
        'added by Theresa G on Sep 21 2010
        If part.Length = 3 Then
            Dim notAttempted As Boolean = False
            For Each gradeBookNode As XmlNode In nodeList
                Dim node As XmlNode = gradeBookNode.SelectSingleNode("@Score")
                If node Is Nothing Then
                    notAttempted = True
                Else
                    notAttempted = False
                    Exit For
                End If
            Next
            If notAttempted = True Then Return False
        End If
        If Not CheckCoursesRequirements(xmlDoc, xpath.ToString()) Then Return False
        If part.Length > 1 Then
            Return True
        Else
            Return CheckProgramCredits(xmlDoc, path)
        End If
    End Function
    Private Function CheckProgramCredits(ByVal xmlDoc As XmlDocument, ByVal path As String) As Boolean
        Dim credits(,) As Decimal = GetCredits(path)
        Dim node As XmlNode = xmlDoc.SelectSingleNode("/ProgramVersion")
        Dim programCredits As String = node.Attributes("ProgramCredits").Value
        If Decimal.Parse(programCredits) > credits(0, 2) Then
            Return False
        End If
        Return True
    End Function
    Private Function GetPostedDate(ByVal path As String) As Date
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']/Module[@GrpId='")
            .Append(part(1))    'GrpId
            .Append("']/Course[@ReqId='")
            .Append(part(2))    'ReqId
            .Append("']/GradeBook[@GrdBkResultId='")
            .Append(part(3))    'GrdBkResultId
            .Append("']/@PostDate")
        End With
        Dim node As XmlNode = xmlDoc.SelectSingleNode(xpath.ToString())
        If node Is Nothing Then
            Return Date.MinValue
        Else
            Return Date.Parse(node.InnerText)
        End If
    End Function
    Private Function GetStartDate(ByVal path As String) As Date
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']/Module[@GrpId='")
            .Append(part(1))    'GrpId
            .Append("']/@StartDate")
        End With
        Dim node As XmlNode = xmlDoc.SelectSingleNode(xpath.ToString())
        If node Is Nothing Then
            Return Date.MinValue
        Else
            Return Date.Parse(node.InnerText)
        End If
    End Function
    Private Function GetEndDate(ByVal path As String) As Date
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']/Module[@GrpId='")
            .Append(part(1))    'GrpId
            .Append("']/@EndDate")
        End With
        Dim node As XmlNode = xmlDoc.SelectSingleNode(xpath.ToString())
        If node Is Nothing Then
            Return Date.MinValue
        Else
            Return Date.Parse(node.InnerText)
        End If
    End Function
    Private Function GetCourseWeight(ByVal path As String) As Decimal
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']/Module[@GrpId='")
            .Append(part(1))    'GrpId
            .Append("']/Course[@ReqId='")
            .Append(part(2))    'ReqId
            .Append("']/GradeBook/@Weight")
        End With
        Dim nodeList As XmlNodeList = xmlDoc.SelectNodes(xpath.ToString())
        Dim sum As Decimal = 0.0
        For Each scoreNode As XmlNode In nodeList
            sum += Decimal.Parse(scoreNode.InnerText)
        Next
        If nodeList.Count > 0 Then
            Return sum / nodeList.Count
        Else
            Return 0.0
        End If
    End Function
    Private Function GetCredits(ByVal path As String) As Decimal(,)
        Dim xmlDoc As XmlDocument = GetGradeBookResults(ddlEnrollmentId.SelectedValue)
        Dim part() As String = path.Split("/")
        Dim xpath As New StringBuilder("ProgramVersion[@PrgVerId='")
        With xpath
            .Append(part(0))    'PrgVerId
            .Append("']")
            .Append("/Module")
            If part.Length > 1 Then
                .Append("[@GrpId='")
                .Append(part(1))    'GrpId
                .Append("']")
            End If
            .Append("/Course")
            If part.Length > 2 Then
                .Append("[@ReqId='")
                .Append(part(2))    'ReqId
                .Append("']")
            End If
        End With
        Dim nodeList As XmlNodeList = xmlDoc.SelectNodes(xpath.ToString())
        Dim credits(1, 2) As Decimal
        For Each courseNode As XmlNode In nodeList
            Dim node As XmlNode = courseNode.SelectSingleNode("@Credits")
            If Not (node Is Nothing) Then
                credits(0, 0) += Decimal.Parse(node.InnerText)
                If IsAttempted(courseNode) Then
                    credits(0, 1) += Decimal.Parse(node.InnerText)
                End If
                If IsEarned(courseNode) Then
                    credits(0, 2) += Decimal.Parse(node.InnerText)
                End If
            End If
            node = courseNode.SelectSingleNode("@FinAidCredits")
            If Not (node Is Nothing) Then
                credits(1, 0) += Decimal.Parse(node.InnerText)
                If IsAttempted(courseNode) Then
                    credits(1, 1) += Decimal.Parse(node.InnerText)
                End If
                If IsEarned(courseNode) Then
                    credits(1, 2) += Decimal.Parse(node.InnerText)
                End If
            End If
        Next
        Return credits
    End Function
    Private Function IsAttempted(ByVal node As XmlNode) As Boolean
        Dim nodeList As XmlNodeList = node.SelectNodes("child::*[@Score]")
        If nodeList.Count > 0 Then
            Return True
        Else
            If node.Attributes("Score") Is Nothing Then
                If Not node.Attributes("Grade") Is Nothing Then Return True
            Else
                Return True
            End If
        End If
        'select externship node
        nodeList = node.SelectNodes("child::*[@SysComponentTypeId=544]")
        If nodeList.Count = 0 Then Return False
        Return CheckIfExternshipIsAttempted(nodeList(0))
    End Function
    Private Function CheckIfExternshipIsAttempted(ByVal externshipNode As XmlNode) As Boolean
        If Not (externshipNode.Attributes("HoursCompleted") Is Nothing) Then
            Dim hoursCompleted As String = externshipNode.Attributes("HoursCompleted").Value
            If Decimal.Parse(hoursCompleted) > 0.0 Then Return True
        End If
        Return False
    End Function
    Private Function IsEarned(ByVal rnode As XmlNode) As Boolean
        Dim nodeList As XmlNodeList = rnode.ChildNodes
        For Each gradeBookNode As XmlNode In nodeList
            If Not CheckGradeBookNode(gradeBookNode) Then Return False
        Next
        Return CheckCoursesRequirements(rnode)
    End Function
    Private Function CheckGradeBookNode(ByVal gradeBookNode As XmlNode) As Boolean
        'this is only for testing
        'If 1 = 1 Then Return True
        Dim node As XmlNode = gradeBookNode.SelectSingleNode("@Score")
        Dim isAttempted As Boolean = False
        If Not (node Is Nothing) Then isAttempted = True
        Dim score As Decimal = 0.0
        Dim sysComponentTypeId As String = gradeBookNode.Attributes("SysComponentTypeId").Value
        'Dim isPracticalExam As Boolean = False
        'If sysComponentTypeId = "533" Then isPracticalExam = True
        If isAttempted Then
            score = Decimal.Parse(node.InnerText)
        End If
        node = gradeBookNode.SelectSingleNode("@PassingGrade")
        Dim passingScore As Decimal = 0.0
        If Not (node Is Nothing) Then
            passingScore = Decimal.Parse(node.InnerText)
        End If
        node = gradeBookNode.SelectSingleNode("@Weight")
        Dim weight As Decimal = 0.0
        If Not (node Is Nothing) Then
            weight = Decimal.Parse(node.InnerText)
        End If
        node = gradeBookNode.SelectSingleNode("@Required")
        Dim required As Boolean = False
        If Not (node Is Nothing) Then
            Dim requiredInt As Integer
            If Integer.TryParse(node.InnerText, requiredInt) Then
                If requiredInt > 0 Then required = True Else required = False
            End If
        End If
        node = gradeBookNode.SelectSingleNode("@MustPass")
        Dim mustPass As Boolean = False
        If Not (node Is Nothing) Then
            Dim mustPassInt As Integer
            If Integer.TryParse(node.InnerText, mustPassInt) Then
                If mustPassInt > 0 Then mustPass = True Else mustPass = False
            End If
        End If
        If Not (Not required And Not mustPass) Then
            If mustPass And (score < passingScore) And (passingScore > 0.0) Then Return False
            Select Case sysComponentTypeId
                Case "533"
                    If required And Not mustPass And Not isAttempted Then Return False
                    'If score = 0.0 Then Return False
                Case "544"
                    'Dim hoursRequired As String = gradeBookNode.SelectSingleNode("@HoursRequired").Value
                    Dim hoursCompletedNode As XmlNode = gradeBookNode.SelectSingleNode("@HoursCompleted")
                    If Not (hoursCompletedNode Is Nothing) Then
                        If Decimal.Parse(gradeBookNode.SelectSingleNode("@HoursRequired").Value) > Decimal.Parse(gradeBookNode.SelectSingleNode("@HoursCompleted").Value) Then Return False
                    Else
                        Return False
                    End If
                    'If score > 0.0 And Not (Decimal.Parse(hoursRequired) = score) Then Return False
                Case Else
                    If required And Not mustPass And Not isAttempted Then Return False
                    'If (required And mustPass And (score < passingScore)) Or (score = 0.0) Then Return False
                    If (required And mustPass And (score < passingScore)) Then Return False
            End Select
        End If
        Return True
    End Function
    Private Sub SetName(ByVal state As Boolean, ByVal text As String)
        lName.Visible = state
        rName.Visible = state
        If state Then rName.Text = text
    End Sub
    Private Sub SetAverage(ByVal state As Boolean, ByVal xpath As String)
        lAverage.Visible = state
        rAverage.Visible = state
        If state Then rAverage.Text = GetAverage(xpath).ToString("###.00")
    End Sub
    Private Sub SetScore(ByVal state As Boolean)
        lScore.Visible = state
        rScore.Visible = state
    End Sub
    Private Sub SetPostDate(ByVal state As Boolean, ByVal path As String)
        lPostDate.Visible = state
        rPostDate.Visible = state
        If state Then rPostDate.Text = GetDate(path, "PostDate")
    End Sub
    Private Sub SetRequiredCompletedRemaining(ByVal state As Boolean, ByVal valuePath As String)
        lRequiredHours.Visible = state
        rRequiredHours.Visible = state
        lCompletedHours.Visible = state
        rCompletedHours.Visible = state
        lRemainingHours.Visible = state
        rRemainingHours.Visible = state
        If state Then
            rRequiredHours.Text = Decimal.Parse(GetValue(valuePath, "HoursRequired")).ToString("####.00")
            rCompletedHours.Text = GetValue(valuePath, "HoursCompleted")
            If String.IsNullOrEmpty(rCompletedHours.Text) Then rCompletedHours.Text = "  0.00"
            Dim remainingHours As Decimal = Decimal.Parse(rRequiredHours.Text) - Decimal.Parse(rCompletedHours.Text)
            If remainingHours < 0.0 Then remainingHours = 0.0
            rRemainingHours.Text = remainingHours.ToString("####.00")
        End If
    End Sub
    Private Sub SetRequirementsMet(ByVal state As Boolean, ByVal valuePath As String)
        lRequirementsMet.Visible = state
        rRequirementsMet.Visible = state
        If state Then
            rRequirementsMet.Text = YesOrNo(CheckRequirements(valuePath))
        End If
    End Sub
    Private Sub SetPostedDate(ByVal state As Boolean, ByVal valuePath As String)
        lDate.Visible = state
        rDate.Visible = state
        If state Then rDate.Text = GetPostedDate(valuePath)
    End Sub
    Private Sub SetDates(ByVal state As Boolean, ByVal valuePath As String)
        lDates.Visible = state
        rDates1.Visible = state
        rDates2.Visible = state
        lTo.Visible = state
        If state Then rDates1.Text = GetStartDate(valuePath)
        If state Then rDates2.Text = GetEndDate(valuePath)
    End Sub
    Private Sub SetPassingScore(ByVal state As Boolean, ByVal valuePath As String)
        lPassingScore.Visible = state
        rPassingScore.Visible = state
    End Sub
    Private Sub SetWeight(ByVal state As Boolean, ByVal valuePath As String)
        lWeight.Visible = state
        rWeight.Visible = state
        If state Then rWeight.Text = GetValue(valuePath, "Weight")
        If rWeight.Text.Length = 0 Then rWeight.Text = "0"
    End Sub
    Private Sub SetRequired(ByVal state As Boolean, ByVal valuePath As String)
        lRequired.Visible = state
        rRequired.Visible = state
        If state Then rRequired.Text = YesOrNo(GetValue(valuePath, "Required"))
    End Sub
    Private Sub SetMustPass(ByVal state As Boolean, ByVal valuePath As String)
        lMustPass.Visible = state
        rMustPass.Visible = state
        If state Then rMustPass.Text = YesOrNo(GetValue(valuePath, "MustPass"))
    End Sub
    Private Sub SetCredits(ByVal state As Boolean, ByVal valuePath As String)
        '=====Credits===========
        CreditsPanel.Visible = state
        Label22.Visible = state
        Dim credits(0, 0) As Decimal
        If state Then credits = GetCredits(valuePath)
        'Credits
        lCredits.Visible = state
        r1Credits.Visible = state
        If state Then r1Credits.Text = CType(credits(0, 0), String)
        r2Credits.Visible = state
        If state Then r2Credits.Text = CType(credits(1, 0), String)
        'Attempted
        lAttempted.Visible = state
        r1Attempted.Visible = state
        If state Then r1Attempted.Text = CType(credits(0, 1), String)
        r2Attempted.Visible = state
        If state Then r2Attempted.Text = CType(credits(1, 1), String)
        'Earned
        lEarned.Visible = state
        r1Earned.Visible = state
        If state Then r1Earned.Text = CType(credits(0, 2), String)
        If state Then r2Earned.Visible = state
        If state Then r2Earned.Text = CType(credits(1, 2), String)
    End Sub

    Private Function YesOrNo(ByVal val As Object) As String
        If val Is DBNull.Value Then Return String.Empty
        If String.IsNullOrEmpty(val) Then Return String.Empty
        Dim valBool As Boolean = CType(val, Boolean)
        If valBool Then Return "Yes" Else Return "No"
    End Function
    Private Sub CleanCache()
        Dim cachedItems As IDictionaryEnumerator = Cache.GetEnumerator()
        While cachedItems.MoveNext
            Dim key As String = CType(cachedItems.Key, String)
            'remove only Guids (StuEnrollId)
            If CommonWebUtilities.IsValidGuid(key) Then
                Cache.Remove(key)
            End If
        End While
    End Sub
    Private Function CheckCoursesRequirements(ByVal xmlDoc As XmlDocument, ByVal xpath As String) As Boolean
        ' this is a Program
        If xpath.IndexOf("/Module/Course/GradeBook", StringComparison.Ordinal) + 24 = xpath.Length Then
            'select all the courses of the program
            Dim newXpath As String = xpath.Substring(0, xpath.Length - 10)
            Dim nodeList As XmlNodeList = xmlDoc.SelectNodes(newXpath)
            For Each courseNode As XmlNode In nodeList
                If Not CheckCoursesRequirements(courseNode) Then Return False
            Next
            Return True
        End If
        ' this is a Module
        If xpath.IndexOf("/Course/GradeBook", StringComparison.Ordinal) + 17 = xpath.Length Then
            'select all the courses of the module
            Dim newXpath As String = xpath.Substring(0, xpath.Length - 10)
            Dim nodeList As XmlNodeList = xmlDoc.SelectNodes(newXpath)
            For Each courseNode As XmlNode In nodeList
                If Not CheckCoursesRequirements(courseNode) Then Return False
            Next
            Return True
        End If
        ' this is a course
        If xpath.IndexOf("/GradeBook", StringComparison.Ordinal) + 10 = xpath.Length Then
            Dim score As Decimal = GetAverageForCourse(xpath)
            Dim passingGrade As String = String.Empty
            Dim courseNode As XmlNode
            If Not xmlDoc.SelectSingleNode(xpath) Is Nothing Then
                courseNode = xmlDoc.SelectSingleNode(xpath).ParentNode
                passingGrade = courseNode.Attributes("PassingGrade").InnerXml

            Else
                courseNode = xmlDoc.SelectSingleNode(xpath.Substring(0, xpath.Length - 10))
                If Not courseNode.Attributes("Score") Is Nothing Then
                    passingGrade = courseNode.Attributes("PassingGrade").InnerXml
                    score = CType(courseNode.Attributes("Score").InnerXml, Decimal)
                    If Not String.IsNullOrEmpty(passingGrade) Then
                        If score < Decimal.Parse(passingGrade) Then
                            Return False
                        Else
                            Return True
                        End If
                    End If
                ElseIf Not courseNode.Attributes("Grade") Is Nothing Then
                    'score = courseNode.Attributes("Grade").InnerXml
                    passingGrade = courseNode.Attributes("PassingGrade").InnerXml
                    If passingGrade = 1 Then
                        Return True
                    Else
                        Return False
                    End If

                End If

            End If
            If Not String.IsNullOrEmpty(passingGrade) Then
                If score < Decimal.Parse(passingGrade) And Not sumOfWeightsIsZero Then
                    Return False
                Else
                    Return True
                End If
            End If

        End If
        ' this is a component
        Return True
    End Function
    Private Function CheckCoursesRequirements(ByVal courseNode As XmlNode) As Boolean
        Dim nodeList As XmlNodeList = courseNode.SelectNodes("child::*[@Score]")
        If nodeList.Count = 0 Then
            If Not courseNode.Attributes("Score") Is Nothing Then
                Dim score As Decimal = Decimal.Parse(courseNode.Attributes("Score").InnerXml)
                Dim passingGrade As String = courseNode.Attributes("PassingGrade").InnerXml
                If Not String.IsNullOrEmpty(passingGrade) Then
                    If score < Decimal.Parse(passingGrade) Then
                        Return False
                    Else
                        Return True
                    End If
                End If
            ElseIf Not courseNode.Attributes("Grade") Is Nothing Then
                Dim passingGrade As String = courseNode.Attributes("PassingGrade").InnerXml
                If passingGrade = 1 Then
                    Return True
                Else
                    Return False
                End If
                'added by Theresa G on Sep 08 2010
                '19696: QA: Graduating students without course completion against Ross type schools. 
                'commented by Theresa G on Sep 20 2010
                'Else
                '    Return True
            End If

        Else
            Dim score As Decimal = GetAverageForNodeList(nodeList)
            Dim passingGrade As String = courseNode.Attributes("PassingGrade").InnerXml
            If Not String.IsNullOrEmpty(passingGrade) Then
                If score < Decimal.Parse(passingGrade) And Not sumOfWeightsIsZero Then
                    Return False
                Else
                    Return True
                End If
            End If
        End If
        Return False
    End Function

    'Protected Sub tvAcademics_TreeNodeDataBound(ByVal sender As Object, ByVal e As TreeNodeEventArgs) Handles tvAcademics.TreeNodeDataBound
    '    Dim str As String = String.Empty
    'End Sub
#End Region

End Class