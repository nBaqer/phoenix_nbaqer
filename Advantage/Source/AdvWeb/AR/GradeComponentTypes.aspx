<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="GradeComponentTypes.aspx.vb" Inherits="GradeComponentTypes" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstDegree">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstDegree" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstDegree" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstDegree" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstDegree" runat="server" DataKeyField="GrdComponentTypeId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("GrdComponentTypeId")%>' Text='<%# container.dataitem("Descrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                        <tr>
                                            <td colspan="6" nowrap align="center">
                                                <asp:Label ID="lblMessageStatus" CssClass="LabelBold" runat="server" MaxLength="128" ForeColor="white" Visible="true" Style="border: solid 1px #6788be; background-color: #6788be;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">

                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCode" CssClass="label" runat="server"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" CssClass="textbox" runat="server" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDescrip" CssClass="label" runat="server"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtDescrip" CssClass="textbox" runat="server" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblSysComponentTypeId" CssClass="label" runat="server">System Component Type
														<font color="red">*</font></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlSysComponentTypeId" runat="server" CssClass="dropdownlist"
                                                    AutoPostBack="true">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtGrdComponentTypeId" CssClass="textbox" runat="server" MaxLength="128" Visible="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblSelectCourses" CssClass="label" runat="server">Select Courses</asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <telerik:RadGrid ID="RadGrid1" runat="server"
                                                    AutoGenerateColumns="False" Width="600px" AllowSorting="false"
                                                    Height="250px" AllowMultiRowSelection="True" GridLines="None">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <AlternatingItemStyle HorizontalAlign="Left" />
                                                    <MasterTableView>
                                                        <Columns>
                                                            <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" />
                                                            <telerik:GridTemplateColumn Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:TextBox runat="server" ID="txtReqId" Text='<%# Bind("ReqId") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridBoundColumn
                                                                DataField="Code" HeaderText="Course Code"
                                                                SortExpression="Code"
                                                                UniqueName="Code">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn
                                                                DataField="Descrip" HeaderText="Course Description"
                                                                SortExpression="Descrip"
                                                                UniqueName="Descrip">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings>
                                                        <Scrolling AllowScroll="True" />
                                                        <Selecting AllowRowSelect="True"></Selecting>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <telerik:RadAjaxLoadingPanel ID="AjaxLoadingPanel" runat="server" Transparency="30"
                                        MinDisplayTime="25" BackgroundPosition="BottomRight">
                                        <img src="../Images/loading.gif" alt="Loading..." style="border: 0; margin-top: 45px;" />
                                    </telerik:RadAjaxLoadingPanel>
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="label"
                                        Width="10%"></asp:TextBox>
                                    <asp:Panel ID="pnlRegent" runat="server" Visible="false">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center" style="border: 1px solid #ebebeb;">
                                            <tr>
                                                <td class="contentcellheader" nowrap
                                                    style="border-top: 0px; border-right: 0px; border-left: 0px">
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="label">Map To Regent</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellcitizenship" style="padding-top: 12px">
                                                    <asp:Panel ID="pnlRegentAwardChild" TabIndex="12" runat="server" Width="80%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="35%" nowrap class="contentcell">
                                                                    <asp:Label ID="lblregentterm" runat="server" CssClass="label">Degree Code<span style="color: red">*</span></asp:Label></td>
                                                                <td width="65%" nowrap class="contentcell4">
                                                                    <asp:DropDownList ID="ddlRegentCode" runat="server" CssClass="textbox" TabIndex="6"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>

                                </div>
                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>

</asp:Content>


