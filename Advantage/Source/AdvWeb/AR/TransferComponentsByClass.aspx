﻿<%@ Page Title="Transfer Components By Class" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="TransferComponentsByClass.aspx.vb" Inherits="TransferComponentsByClass" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Transfer Course Components By Class</title>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable" align="center">
                            <tr>
                                <td class="detailsframe" align="center">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td colspan="2" class="contentcell">
                                                    <asp:Label ID="lblFrom" runat="server" CssClass="headerLabel">Transfer Components From Course</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTerm" runat="server" CssClass="label">Term</asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <telerik:RadComboBox ID="rcbTerm" runat="server" DataTextField="TermDescrip"
                                                        DataValueField="TermId" AutoPostBack="True" Filter="Contains" CausesValidation="False" AppendDataBoundItems="true"
                                                        Width="325px" DropDownWidth="325px">
                                                    </telerik:RadComboBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblCourse" runat="server" CssClass="label">Course</asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <telerik:RadComboBox ID="rcbCourse" runat="server" DataTextField="Descrip" AutoPostBack="true"
                                                        DataValueField="Reqid" AppendDataBoundItems="true" Filter="Contains" Width="325px" DropDownWidth="325px">
                                                    </telerik:RadComboBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="contentcell">
                                                    <asp:Label ID="lblTo" runat="server" CssClass="headerLabel">Register in New Class</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblClass" runat="server" CssClass="label" ToolTip="Classes available for new registration in selected course">Available Classes</asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <telerik:RadComboBox ID="rcbAvailableClasses" runat="server" AutoPostBack="true"
                                                        DataTextField="Display" DataValueField="ClsSectionId" Filter="Contains" AppendDataBoundItems="true"
                                                        Width="325px" DropDownWidth="325px" ToolTip="Classes available for new registration in selected course">
                                                    </telerik:RadComboBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0"
                                            align="center">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" style="margin-left: 5px; margin-right: 5px;">
                                                        <tr>
                                                            <td align="left" width="40%">
                                                                <asp:Label runat="server" ID="lblGridTitle" Text="Students with Incomplete Course" CssClass="headerLabel"></asp:Label>
                                                            </td>
                                                            <td width="20%" align="center">
                                                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td width="40%" align="right">
                                                                <telerik:RadButton ID="btnTransferComponents" runat="server" Text="Transfer Components" SkinID="LargeButton"></telerik:RadButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <telerik:RadGrid ID="rgStudents" runat="server" AllowMultiRowSelection="true" AllowSorting="true" EnableLinqExpressions="false" AutoGenerateColumns="False">
                                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                                        <Selecting AllowRowSelect="true" UseClientSelectColumnOnly="false" EnableDragToSelectRows="true" />
                                                                    </ClientSettings>
                                                                    <MasterTableView DataKeyNames="ResultId" Name="Students" HierarchyLoadMode="Client" AllowFilteringByColumn="true">
                                                                        <SortExpressions>
                                                                            <telerik:GridSortExpression FieldName="ValidStudent" SortOrder="Descending" />
                                                                        </SortExpressions>
                                                                        <DetailTables>
                                                                            <telerik:GridTableView Name="CompletedComponents" DataKeyNames="InstrGrdBkWgtId" Width="100%">
                                                                                <ParentTableRelation>
                                                                                    <telerik:GridRelationFields DetailKeyField="ResultId" MasterKeyField="ResultId"></telerik:GridRelationFields>
                                                                                </ParentTableRelation>
                                                                                <Columns>
                                                                                    <telerik:GridBoundColumn DataField="Description" HeaderText="Completed Component"></telerik:GridBoundColumn>
                                                                                    <telerik:GridBoundColumn DataField="ComponentType" HeaderText="Component Type"></telerik:GridBoundColumn>
                                                                                    <telerik:GridBoundColumn DataField="Score" HeaderText="Score"></telerik:GridBoundColumn>
                                                                                </Columns>
                                                                            </telerik:GridTableView>
                                                                        </DetailTables>
                                                                        <Columns>
                                                                            <telerik:GridClientSelectColumn UniqueName="CheckboxSelectColumn" ItemStyle-Width="25px" AutoPostBackOnFilter="true"></telerik:GridClientSelectColumn>
                                                                            <telerik:GridBoundColumn UniqueName="FullName" DataField="FullName" HeaderText="Student" ReadOnly="true" AllowFiltering="true">
                                                                                <ItemStyle Wrap="false" />
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn UniqueName="Program" DataField="Program" HeaderText="Program" ReadOnly="true" AllowFiltering="true">
                                                                                <ItemStyle Wrap="false" />
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn UniqueName="CurrentClass" DataField="ClsSection" HeaderText="Current Class" ReadOnly="true" AllowFiltering="true">
                                                                                <ItemStyle Wrap="false" />
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridDateTimeColumn UniqueName="StartDate" DataField="StartDate" HeaderText="Start Date" AllowFiltering="true"
                                                                                DataType="System.DateTime" DataFormatString="{0:MM/dd/yyyy}" PickerType="DatePicker">
                                                                            </telerik:GridDateTimeColumn>
                                                                            <telerik:GridDateTimeColumn UniqueName="ExpGradDate" DataField="ExpGradDate" HeaderText="Exp Grad Date" AllowFiltering="true"
                                                                                DataType="System.DateTime" DataFormatString="{0:MM/dd/yyyy}" PickerType="DatePicker">
                                                                            </telerik:GridDateTimeColumn>
                                                                            <telerik:GridBoundColumn UniqueName="StuEnrollId" DataField="StuEnrollId" Display="false"></telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn UniqueName="TermId" DataField="TermId" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn UniqueName="ClsSectionId" DataField="ClsSectionId" Display="false" ReadOnly="true"></telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn UniqueName="ValidStudent" DataField="ValidStudent" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
                                                                        </Columns>
                                                                    </MasterTableView>
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
