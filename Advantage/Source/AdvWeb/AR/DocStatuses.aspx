<%@ page title="" language="vb" MasterPageFile="~/NewSite.master" autoeventwireup="false" codefile="DocStatuses.aspx.vb" inherits="DocStatuses" %>
<%@ mastertype virtualpath="~/NewSite.master"%>
<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters" style="position: relative; top: 5px; height: expression(document.body.clientHeight - 179 + 'px')">
                                <asp:DataList ID="dlstDocStatuses" runat="server" DataKeyField="DocStatusId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" runat="server" CausesValidation="False" CommandArgument='<%# Container.DataItem("DocStatusId")%>' Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' ImageUrl="../images/Inactive.gif"></asp:ImageButton>
                                        <asp:ImageButton ID="imgActive" runat="server" CausesValidation="False" CommandArgument='<%# Container.DataItem("DocStatusId")%>' Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' ImageUrl="../images/Active.gif"></asp:ImageButton>
                                        <asp:LinkButton ID="Linkbutton1" CssClass="itemstyle" CausesValidation="False" runat="server" CommandArgument='<%# Container.DataItem("DocStatusId")%>' Text='<%# Container.DataItem("DocStatusDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="98%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>

                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table width="60%" align="center">
                                        <asp:TextBox ID="txtDocStatusId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                        <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDocStatusCode" runat="server" CssClass="label">Code</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtDocStatusCode" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="revCode" runat="server" ControlToValidate="txtDocStatusCode" Display="None"
                                                    ErrorMessage="too many characters" ValidationExpression=".{0,10}"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDocStatusDescrip" CssClass="label" runat="server">Description</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtDocStatusDescrip" CssClass="textbox" MaxLength="50" runat="server" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblSysDocStatusId" CssClass="label" runat="server">System Document Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlSysDocStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                <asp:CompareValidator ID="Comparevalidator1" runat="server" ErrorMessage="Must Select a System Document Status"
                                                    Display="None" ControlToValidate="ddlSysDocStatusId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a System Document Status</asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                <asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ErrorMessage="Must Select a Campus Group"
                                                    Display="None" ControlToValidate="ddlCampGrpId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Campus Group</asp:CompareValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:TextBox ID="txtRowIds" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                    <!--end table content-->
                                </div>
                            </asp:Panel>
                            <!--end table content-->
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>
</asp:Content>


