﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="FASAPCheck.aspx.vb" Inherits="AR_FASAPCheck" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script type="text/javascript">

       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }

   </script>
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
 <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>   
     
           
			<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
				<!-- begin leftcolumn -->
				<tr>
					<td class="listframe">
						<table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td class="listframetop2">
									<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
										<tr>
											<td class="twocolumnlabelcell">
                                                <asp:label id="lblCampusId"  Runat="server">Campus</asp:label>
                                             </td>
											<td class="twocolumncontentcell" align="left"  style="padding:3px;">
                                                <asp:dropdownlist id="ddlCampusId" Width="280px" Runat="server" AutoPostBack="False" cssClass="dropdownlist" Enabled="True"></asp:dropdownlist>
                                             </td>
										</tr>
										<tr>
											<td class="twocolumnlabelcell">
                                                <asp:label id="lblPrgVerId"  Runat="server">Program</asp:label>
                                            </td>
											<td class="twocolumncontentcell" style="padding:3px;" align="left">
                                                <asp:dropdownlist id="ddlPrgVerId" Width="280px" Runat="server" AutoPostBack="False" cssClass="dropdownlist"></asp:dropdownlist></td>
										</tr>
										<tr>
											<td class="twocolumnlabelcell"></td>
											<td class="twocolumncontentcell" style="padding:3px;">
                                                <asp:button id="btnGetList"  Runat="server" Width="140px" Text="Preview FA SAP" ToolTip="Preview FA SAP"></asp:button>
                                            </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom">
									<div class="scrollleftfltr3rows"></div>
								</td>
							</tr>
						</table>
					</td>
                 </tr>
             </table>



    </telerik:RadPane>


    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="false"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" Enabled="false" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" Enabled="false" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2">
        <!-- begin content table-->


            <table>
                <tr>
                    <td align="left">
                        <br />
                        <asp:Button ID="btnBuildList" Width="170px" runat="server" Text="Commit FA SAP"
                            Visible="false"></asp:Button>
                        <br />
                    </td>
                    <td>
                        <br />
                        <br />

                    </td>
                    <td align="right">
                        <br />
                        <asp:Button ID="btnExportToExcell" runat="server" Width="170px"
                            Text="Export List to Excel" Visible="false"></asp:Button>
                        <br />
                    </td>
                </tr>
            </table>


            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="left">
    <tr>
    <td>
   <asp:DataGrid ID="dgrdTransactionSearch" runat="server" 
                        Width="97%" GridLines="both" EditItemStyle-Wrap="false" HeaderStyle-Wrap="true"
                        AllowSorting="True" AutoGenerateColumns="False" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#E0E0E0" cellpadding="0">
                        <EditItemStyle Wrap="False"></EditItemStyle>
                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Student Name">
                                <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="EnrollLinkButton" Text='<%# Container.DataItem("FirstName") & " " & Container.DataItem("LastName") %>'
                                        CommandName='<%# Container.DataItem("PrgVerId")%>' runat="server" CommandArgument='<%# Container.DataItem("StuEnrollId")%>'/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Enrollment">
                                <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPrgVerDescrip" Text='<%# Container.DataItem("PrgVerDescrip")%>'
                                        CssClass="label" runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FA SAP Increment">
                                <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="label2" Text='<%# Container.DataItem("Period")%>' CssClass="label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FA SAP Check Date">
                                <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="label1" Text='<%# Container.DataItem("ModDate")%>' CssClass="label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Making FA SAP?" Visible="false">
                                <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblMakingSAP" Text='<%# Container.DataItem("MakingSAP")%>' CssClass="label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FA SAP Check Result">
                                <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSAPResult" Text='<%# Container.DataItem("StudentStatus") %>' CssClass="label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Reason">
                                <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblComments" runat="server" Visible="true" Text='<%# Container.DataItem("Comments")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Can be Terminated?">
                                <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkTerminate" runat="server" Checked='<%# GetTerminateEnabled(eval("ProbationCount"),eval("TerminationProbationCount")) %>'
                                        Enabled="false" Text='<%# GetTerminateEnabledDesc(eval("ProbationCount"),eval("TerminationProbationCount")) %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Unschedule">
                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <HeaderTemplate>
                                    <!--<asp:Label Id="label1" cssclass = "Label" Runat="server" Text='Copy' /><br>-->
                                    <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkCopy',
																	 document.forms[0].chkAllItems.checked)" />Check
                                    All
                                </HeaderTemplate>
                                 
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkCopy" runat="server" Text='' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                           
                            <asp:TemplateColumn HeaderText="Terminate" Visible="false">
                                <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblConsequenceTypId" runat="server" Visible="false" Text='<%# Container.DataItem("ConsequenceTypId")%>'>																</asp:Label>
                                    <asp:Label ID="lblProbationCount" runat="server" Visible="false" Text='<%# eval("ProbationCount")%>'>				
                                    </asp:Label>
                                    <asp:Label ID="lblTerminationProbationCount" runat="server" Visible="false" Text='<%#eval("TerminationProbationCount")%>'>																</asp:Label>
                                    <asp:Label ID="lblSAPTerminationCnt" runat="server" Visible="false" Text='<%# eval("SAPTerminationCnt")%>'>				
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
</td>
    </tr>

                 

            </table>



        <!-- end content table-->
        </div>
        </td>
        </tr>
        </table>

    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>


