Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Drawing
Imports Telerik.Web.UI

Partial Class GradeComponentTypes
    Inherits BasePage
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents btnClose As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim cmpCallReportingAgencies As New ReportingAgencies
    Protected intRptFldId, intResultsAffected As Integer
    Protected boolRptAgencyAppToSchool As Boolean
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = CInt(m_Context.Items("ResourceId"))

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        Try
            If Not Page.IsPostBack Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Set the text box to a new guid
                txtGrdComponentTypeId.Text = System.Guid.NewGuid.ToString

                ds = objListGen.SummaryListGenerator(userId, campusId)

                PopulateDataList(ds)

                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                BuildSystemComponentTypes()
                ShowHideGrid(0)
                HideStatusMessage()
                radstatus.SelectedIndex = 0
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub

    Private Sub BuildSystemComponentTypes()
        ddlSysComponentTypeId.DataSource = (New GradesFacade).GetSysGradeBookComponents()
        ddlSysComponentTypeId.DataTextField = "Resource"
        ddlSysComponentTypeId.DataValueField = "ResourceId"
        ddlSysComponentTypeId.DataBind()
        ddlSysComponentTypeId.Items.Insert(0, New ListItem("Select", ""))
        ddlSysComponentTypeId.SelectedIndex = 0
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim msg As String
        Dim grdFacade As New GradesFacade
        Dim dsCourses As New DataSet

        Try
            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtGrdComponentTypeId.Text
                txtRowIds.Text = objCommon.PagePK
                msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstDegree.SelectedIndex = -1
                PopulateDataList(ds)

                'Get the contents of datatable and pass it as xml document
                If ddlSysComponentTypeId.SelectedValue = 612 Then
                    dsCourses = GetListofCheckedCourses()
                    If Not dsCourses Is Nothing Then
                        For Each lcol As DataColumn In dsCourses.Tables(0).Columns
                            lcol.ColumnMapping = System.Data.MappingType.Attribute
                        Next
                        Dim strXML As String = dsCourses.GetXml
                        grdFacade.UpdateSysComponentTypesandAddCourses(txtGrdComponentTypeId.Text, strXML)
                    End If
                Else
                    grdFacade.UpdateSysComponentTypes(txtGrdComponentTypeId.Text, ddlSysComponentTypeId.SelectedValue)
                End If

                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"

            ElseIf ViewState("MODE") = "EDIT" Then
                msg = objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                If ddlSysComponentTypeId.SelectedValue = 612 Then
                    dsCourses = GetListofCheckedCourses()
                    If Not dsCourses Is Nothing Then
                        For Each lcol As DataColumn In dsCourses.Tables(0).Columns
                            lcol.ColumnMapping = System.Data.MappingType.Attribute
                        Next
                        Dim strXML As String = dsCourses.GetXml
                        grdFacade.UpdateSysComponentTypesandAddCourses(txtGrdComponentTypeId.Text, strXML)
                    End If
                Else
                    grdFacade.UpdateSysComponentTypes(txtGrdComponentTypeId.Text, ddlSysComponentTypeId.SelectedValue)
                End If
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstDegree.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
            End If

            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstDegree, txtGrdComponentTypeId.Text)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            HideStatusMessage()
            Response.Redirect("../ErrorPage.aspx")

        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim msg As String
        Try
            msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

            If msg <> "" Then
                If InStr(msg, "constraint") > 0 Then
                    lblMessageStatus.Text = "Error:Delete operation failed as there are dependent records for the selected grade component type"
                    Exit Sub
                Else
                    DisplayErrorMessage(msg)
                    Exit Sub
                End If
            End If

            ClearRHS()
            ds = objListGen.SummaryListGenerator(userId, campusId)
            dlstDegree.SelectedIndex = -1
            PopulateDataList(ds)
            ds.WriteXml(sw)
            ViewState("ds") = sw.ToString()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtGrdComponentTypeId.Text = System.Guid.NewGuid.ToString
            CommonWebUtilities.RestoreItemValues(dlstDegree, Guid.Empty.ToString)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
            HideStatusMessage()
            ShowHideGrid(0)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Try
            ClearRHS()
            ds.ReadXml(sr)
            PopulateDataList(ds)
            ddlSysComponentTypeId.SelectedValue = ""
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtGrdComponentTypeId.Text = System.Guid.NewGuid.ToString

            'Reset Style in the Datalist
            CommonWebUtilities.RestoreItemValues(dlstDegree, Guid.Empty.ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstDegree_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstDegree.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim intSysComponentTypeId As Integer
        Try

            strGUID = dlstDegree.DataKeys(e.Item.ItemIndex).ToString()
            txtRowIds.Text = e.CommandArgument.ToString
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstDegree.SelectedIndex = selIndex
            ds.ReadXml(sr)
            PopulateDataList(ds)
            intSysComponentTypeId = (New GradesFacade).GetSystemComponents(strGUID.ToString)
            If intSysComponentTypeId = 0 Then
                ddlSysComponentTypeId.SelectedValue = ""
            Else
                ddlSysComponentTypeId.SelectedValue = intSysComponentTypeId
            End If

            If intSysComponentTypeId = 612 Then
                ddlSysComponentTypeId_SelectedIndexChanged(sender, Nothing)
                CheckItemsInGrid()
            End If

            ShowHideGrid(intSysComponentTypeId)
            HideStatusMessage()

            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstDegree, strGUID)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstDegree_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstDegree_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtGrdComponentTypeId.Text = System.Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/20/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radstatus.SelectedItem.Text.ToLower = "active") Or (radstatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radstatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "Descrip", DataViewRowState.CurrentRows)

        With dlstDegree
            .DataSource = dv
            .DataBind()
        End With
        dlstDegree.SelectedIndex = -1

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtGrdComponentTypeId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    Protected Sub ddlSysComponentTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSysComponentTypeId.SelectedIndexChanged
        If ddlSysComponentTypeId.SelectedValue = "612" Then
            'Show the course list
            RadGrid1.Visible = True
            lblSelectCourses.Visible = True
            Dim strStatusId As String = AdvantageCommonValues.ActiveGuid
            RadGrid1.DataSource = (New GradesFacade).GetCoursesByCampusIdandStatusId(campusId, strStatusId)
            RadGrid1.DataBind()
        Else
            RadGrid1.Visible = False
            lblSelectCourses.Visible = False
        End If
    End Sub

    Protected Sub ToggleSelectedState(ByVal sender As Object, ByVal e As EventArgs)
        If (CType(sender, CheckBox)).Checked Then
            For Each dataItem As GridDataItem In RadGrid1.MasterTableView.Items
                CType(dataItem.FindControl("rowChkBox"), CheckBox).Checked = True
                dataItem.Selected = True
                Dim backColor As Color = ColorTranslator.FromHtml("#e4e4e4")
                RadGrid1.ItemStyle.BackColor = backColor
                RadGrid1.AlternatingItemStyle.BackColor = backColor
                RadGrid1.GridLines = GridLines.None
            Next
        Else
            For Each dataItem As GridDataItem In RadGrid1.MasterTableView.Items
                CType(dataItem.FindControl("rowChkBox"), CheckBox).Checked = False
                dataItem.Selected = False
                RadGrid1.ItemStyle.BackColor = Color.White
                RadGrid1.AlternatingItemStyle.BackColor = Color.White
                RadGrid1.GridLines = GridLines.Both
            Next
        End If
    End Sub

    Protected Sub ToggleRowSelection(ByVal sender As Object, ByVal e As EventArgs)
        CType(CType(sender, CheckBox).Parent.Parent, GridItem).Selected = CType(sender, CheckBox).Checked
    End Sub

    Private Function GetListofCheckedCourses() As DataSet
        Dim tbl As New DataTable("GradeComponentTypes_Courses")
        Dim strCourseId As String = ""
        Dim strGradeComponentTypeId As String = ""
        Dim strGrdComponentTypeId_ReqId As String = ""
        Dim intSelectedCount As Integer = 0
        '        Dim item As GridDataItem

        With tbl
            'Define table schema
            .Columns.Add("GrdComponentTypeId_ReqId", GetType(Guid))
            .Columns.Add("GrdComponentTypeId", GetType(Guid))
            .Columns.Add("ReqId", GetType(Guid))  'ReqId refers to course id

            'set field properties
            .Columns("GrdComponentTypeId_ReqId").AllowDBNull = False
            .Columns("GrdComponentTypeId").AllowDBNull = False
            .Columns("ReqId").AllowDBNull = False

            'set primary key
            .PrimaryKey = New DataColumn() {.Columns("GrdComponentTypeId"), .Columns("ReqId")}
        End With

        'GetSelectedItems method brings in a collection of rows that were selected
        For Each dataItem As GridDataItem In RadGrid1.MasterTableView.GetSelectedItems
            'BR: If the course was selected, add the course to the datatable using LoadDataRow Method
            'Benefits:LoadDataRow method helps get control RowState for the new DataRow
            'First Parameter: Array of values, the items in the array correspond to columns in the table. 
            'Second Parameter: AcceptChanges, enables to control the value of the RowState property of the new DataRow.
            '                  Passing a value of False for this parameter causes the new row to have a RowState of Added
            strGrdComponentTypeId_ReqId = System.Guid.NewGuid.ToString
            strGradeComponentTypeId = txtGrdComponentTypeId.Text
            strCourseId = CType(dataItem.FindControl("txtReqId"), TextBox).Text.ToString
            tbl.LoadDataRow(New Object() {strGrdComponentTypeId_ReqId, strGradeComponentTypeId, strCourseId}, False)
        Next

        Dim ds As DataSet = New DataSet()
        ds.Tables.Add(tbl)
        Return ds
    End Function

    Private Sub ShowHideGrid(ByVal intSysComponentTypeId As Integer)
        'Just for demo
        If intSysComponentTypeId = 612 Then
            RadGrid1.Visible = True
            lblSelectCourses.Visible = True
        Else
            RadGrid1.Visible = False
            lblSelectCourses.Visible = False
        End If
    End Sub

    Private Sub CheckItemsInGrid()
        Dim ds As New DataSet
        ds = (New GradesFacade).GetCoursesByGrdComponentTypeId(txtGrdComponentTypeId.Text)

        For Each dr As DataRow In ds.Tables(0).Rows
            For Each dataitem As GridDataItem In RadGrid1.MasterTableView.Items
                If dr("ReqId").ToString.Trim = CType(dataitem.FindControl("txtReqId"), TextBox).Text.ToString.Trim Then
                    dataitem.Selected = True
                    Exit For
                End If
            Next
        Next
    End Sub

    Private Sub ShowStatusMessage()
        'lblMessageStatus.Visible = True
        'lblMessageStatus.Text = "Record was saved successfully"
        'DisplayErrorMessage("Record was saved successfully")
    End Sub

    Private Sub HideStatusMessage()
        lblMessageStatus.Visible = False
    End Sub
End Class
