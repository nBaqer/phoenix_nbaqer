﻿Imports System.Diagnostics
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports Telerik.Web.UI

Partial Class TermProgress
    Inherits BasePage

    Protected WithEvents btnhistory As Button
    'Protected WithEvents dataGridTable As System.Data.DataTable
    Private Property dataGridTable() As System.Data.DataTable
        Get
            If (Me.ViewState("dataGridTable") Is Nothing) Then
                Return New System.Data.DataTable()
            Else
                Return DirectCast(Me.ViewState("dataGridTable"), System.Data.DataTable)
            End If
        End Get
        Set(value As System.Data.DataTable)
            Me.ViewState.Add("dataGridTable", value)
        End Set
    End Property
    Private Property StuEnrollId() As String
        Get
            If (Me.ViewState("StuEnrollId") Is Nothing) Then
                Return GetStuEnrollIdForFirstItem()
            Else
                Return DirectCast(Me.ViewState("StuEnrollId"), String)
            End If
        End Get
        Set(value As String)
            Me.ViewState.Add("StuEnrollId", value)
        End Set
    End Property
    Protected boolStatus As String
    'Private requestContext As HttpContext
    Protected StudentId As String
    Protected UserId As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

#End Region


    Protected state As AdvantageSessionState
    Protected LeadId As String
    'Dim resourceId As Integer
    Protected boolSwitchCampus As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings


    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        'dataNavigationProvider = New NavigationRoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
        'mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim resourceId As Integer
        Dim campusId As String
        'Get the StudentId from the state object associated with this page
        UserId = AdvantageSession.UserState.UserId.ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU
        objStudentState = getStudentFromStateObject(200) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''

        'Put user code to initialize the page here
        If radStatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If

        'Always disable the History button. It does not apply to this page.
        'Header1.EnableHistoryButton(False)

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            Dim ds As DataSet
            Dim facade As New StdProgressFacade

            'To get the first name and last name of student to pass into the 
            'enrollmentid component.
            txtStudentId.Text = StudentId

            'Bind the Grd Bk Weights Data list
            'ds = facade.PopulateDataList(txtStudentId.Text, boolStatus, HttpContext.Current.Request.Params("cmpid"))
            ds = facade.PopulateDataList(txtStudentId.Text, boolStatus, AdvantageSession.UserState.CampusId.ToString())

            dlstStdProgress.SelectedIndex = -1

            With dlstStdProgress
                .DataSource = ds
                .DataBind()
            End With

            'display an error if there are no enrollments
            If dlstStdProgress.Items.Count = 0 Then
                ThereAreNoEnrollments()
                'DisplayInfoMessage("There are no enrollments")
                MyBase.uSearchEntityControlId.Value = ""
                Exit Sub
            End If

            'When the page is first loaded we want to select the first enrollment in the DataList
            StuEnrollId = GetStuEnrollIdForFirstItem()
            ProcessSelectedEnrollment(StuEnrollId)
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            'End If
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If
            MyBase.uSearchEntityControlId.Value = ""
        End If

        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False

    End Sub

    Private Sub dlstStdProgress_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlstStdProgress.ItemCommand
        StuEnrollId = dlstStdProgress.DataKeys(e.Item.ItemIndex).ToString()
        ProcessSelectedEnrollment(StuEnrollId)
        CommonWebUtilities.RestoreItemValues(dlstStdProgress, StuEnrollId)
    End Sub

    Private Sub ProcessSelectedEnrollment(ByVal StuEnrollId As String)
        Dim dt As DataTable
        Dim gradeReps As String = CType(MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod"), String)
        Dim strIncludeHours As String = CType(MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade"), String)
        Dim includeHours As Boolean
        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If

        'dt = (New GraduateAuditFacade).GetTermProgressByEnrollment(stuEnrollId, gradeReps)
        dt = (New GraduateAuditFacade).GetTermProgressFromResults(StuEnrollId, gradeReps, includeHours)

        If dt.Rows.Count = 0 Then
            DisplayInfoMessage("This enrollment has not completed any course yet.")
        End If

        For Each dtRow As DataRow In dt.Rows
            'Dim boolIsClinicCompletlySatisfied1 As Boolean = (New ExamsFacade).isClinicCourseCompletlySatisfiedByTermId(stuEnrollId, 500, "", "All", CType(dtRow("TermId"), Guid).ToString)

            'Get info DescripXTranscript or Create record from arTermEnrollSummary table
            Dim dtRowTESummary As DataRow
            dtRowTESummary = GetTermEnrollSummaryRecord(dtRow("TermId").ToString(), StuEnrollId)

            dtRow("DescripXTranscript") = dtRowTESummary("DescripXTranscript")
            '
            If Not dtRow("CreditsAttempted") Is DBNull.Value Then
                Try
                    dtRow("CreditsAttempted") = Math.Round(dtRow("CreditsAttempted"), 2)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    dtRow("CreditsAttempted") = dtRow("CreditsAttempted")
                End Try
            End If
            If Not dtRow("CreditsEarned") Is DBNull.Value Then
                Try
                    dtRow("CreditsEarned") = Math.Round(dtRow("CreditsEarned"), 2)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    dtRow("CreditsEarned") = dtRow("CreditsEarned")
                End Try
            End If
        Next
        dataGridTable = dt
        'BuildGridRadUnschedule()
        RadUnschedule.DataSource = dataGridTable
        RadUnschedule.DataBind()

    End Sub
    Private Sub ThereAreNoEnrollments()
        'There Are No Enrollments so there are not terms in progress
        Dim ds1 As DataSet = New DataSet()
        Dim dt1 As DataTable = New DataTable()
        dt1.Rows.Clear()
        ds1.Tables.Add(dt1)
    End Sub
    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        'Bind the Grd Bk Weights Datalist
        Dim ds As DataSet
        Dim facade As New StdProgressFacade
        ds = facade.PopulateDataList(txtStudentId.Text, boolStatus)

        dlstStdProgress.SelectedIndex = -1

        With dlstStdProgress
            .DataSource = ds
            .DataBind()
        End With
    End Sub

    Private Function GetStuEnrollIdForFirstItem() As String
        Dim j As Integer

        For j = 0 To dlstStdProgress.Items(0).Controls.Count - 1
            If dlstStdProgress.Items(0).Controls(j).GetType.ToString = "System.Web.UI.WebControls.LinkButton" Then
                Return DirectCast(dlstStdProgress.Items(0).Controls(j), LinkButton).CommandArgument.ToString
            End If
        Next
        Return String.Empty
    End Function

    'Private Sub BindDataGrid()
    '    '   bind datagrid to dataGridTable
    '    'dgrdLeadGroups.DataSource = New DataView(dataGridTable, "ReqGrpId=" + "'" + CType(Session("ReqGrpId"), String) + "'", "LeadGrpDescrip ASC", DataViewRowState.CurrentRows)
    '    'dgrdLeadGroups.DataBind()
    'End Sub

    Private Function GetTermEnrollSummaryRecord(ByVal TermId As String, ByVal StuEnrollId As String) As DataRow
        'Get info DescripXTranscript or Create record from arTermEnrollSummary table
        '    
        Dim dsTermEnrollSummary As DataSet
        Dim dtRowTESummary As DataRow
        dsTermEnrollSummary = (New GraduateAuditFacade).GetTermEnrollSummary(TermId, StuEnrollId, AdvantageSession.UserState.UserName)
        dtRowTESummary = dsTermEnrollSummary.Tables(0).Rows(0)
        Return dtRowTESummary
    End Function
    Private Sub BuildGridRadUnschedule()
        RadUnschedule.DataSource = dataGridTable    '(New GraduateAuditFacade).GetTermProgressByEnrollment(stuEnrollId)
    End Sub
    Protected Sub RadUnschedule_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadUnschedule.NeedDataSource
        BuildGridRadUnschedule()
    End Sub
    Protected Sub RadUnschedule_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles RadUnschedule.ItemDataBound
        'If (TypeOf e.Item Is GridHeaderItem) Then
        'End If
        'If (TypeOf e.Item Is GridDataItem) AndAlso (e.Item.IsInEditMode = False) Then
        'End If
        'If (TypeOf e.Item Is GridDataItem) AndAlso e.Item.IsInEditMode Then
        'End If
        'If (TypeOf e.Item Is GridDataItem) AndAlso e.Item.IsInEditMode Then
        'End If
    End Sub
 
    Protected Sub RadUnschedule_UpdateCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadUnschedule.UpdateCommand

        Dim editedItem = DirectCast(e.Item, GridEditableItem)
        Dim termEnrollSummary As TermEnrollSummary = New TermEnrollSummary()
        Dim result As String = String.Empty
        Dim index As Integer = e.Item.DataSetIndex
        With termEnrollSummary
            .TermId = DirectCast(editedItem("TermId").Controls(1), Label).Text()
            .StuEnrollId = StuEnrollId
            .DescripXTranscript = DirectCast(editedItem("DescripXTranscript").Controls(1), TextBox).Text
            .ModUser = AdvantageSession.UserState.UserName
            .ModDate = DateTime.Now

        End With

        'Save TermEnrollsummary
        result = (New GraduateAuditFacade).UpdateTermEnrollSummary(termEnrollSummary)

        If Not (result = String.Empty) Then
            'Display Error Message
            CommonWebUtilities.DisplayErrorInMessageBox(Page, result)
            'DisplayInfoMessage(result)
        Else
            'update dataGridTable
            dataGridTable(index).Item(2) = termEnrollSummary.DescripXTranscript
        End If
        e.Canceled = True
        RadUnschedule.MasterTableView.ClearEditItems()
        BuildGridRadUnschedule()
        RadUnschedule.MasterTableView.Rebind()
    End Sub

    Private Sub DisplayInfoMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayInfoInMessageBox(Page, errorMessage)
        End If
    End Sub
End Class
