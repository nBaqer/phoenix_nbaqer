﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportToExcel.aspx.vb" Inherits="AR_ExportToExcel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Export to Excel</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <asp:DataGrid ID="dgCS" CellPadding="0" BorderWidth="1px" BorderStyle="Solid" 
                BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal" Width="80%" runat="server">
                <EditItemStyle Wrap="False"></EditItemStyle>
                <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="11px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066"></ItemStyle>
                <HeaderStyle CssClass="datagridheaderstyle" BackColor="#FAFAFA" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066"></HeaderStyle>
                <AlternatingItemStyle CssClass="datagridalternatingstyle" BackColor="#FAFAFA"></AlternatingItemStyle>
                <Columns>
                    <asp:TemplateColumn HeaderText="Clinic Service" HeaderStyle-Font-Bold=true>
                        <HeaderStyle CssClass="datagridheaderstyle" width="50%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066" HorizontalAlign="left"></HeaderStyle>
                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066" HorizontalAlign="left"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblClinicService" runat="Server" Text='<%# Container.DataItem("ClinicService") %>' CssClass="label"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Required">
                        <HeaderStyle CssClass="datagridheaderstyle" Width="10%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066" HorizontalAlign="center"></HeaderStyle>
                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066" HorizontalAlign="center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblRequired" Text='<%# DataBinder.Eval(Container, "DataItem.Required", "{0:#.00}") %>' CssClass="label"
                                runat="server">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Completed">
                        <HeaderStyle CssClass="datagridheaderstyle" width="10%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066" HorizontalAlign="center"></HeaderStyle>
                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066" HorizontalAlign="center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCompleted" Text='<%# Container.DataItem("Completed") %>' CssClass="label"
                                runat="server">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Remaining" >
                        <HeaderStyle CssClass="datagridheaderstyle" width="10%" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066" HorizontalAlign="center"></HeaderStyle>
                        <ItemStyle CssClass="datagriditemstyle" Font-Names="verdana" Font-Size="12px" Font-Bold="false" BorderColor="#E0E0E0" ForeColor="#000066" HorizontalAlign="center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblRemaining" Text='<%# Container.DataItem("Remaining") %>' CssClass="label"
                                runat="server">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
    </div>
    </form>
</body>
</html>
