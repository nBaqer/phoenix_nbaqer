﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="StudentEnrollments.aspx.vb" Inherits="StudentEnrollments" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%--<%@ Register TagPrefix="fame" TagName="StudentBar" Src="~/UserControls/StudentBar.ascx" %>--%>
<%@ Register Src="~/usercontrols/AdvantageRadAjaxLoadingControl.ascx" TagPrefix="uc1" TagName="AdvantageRadAjaxLoadingControl" %>
<%@ Register Src="~/usercontrols/AR/StudentSchedule.ascx" TagPrefix="fame" TagName="StudentSchedule" %>

<%@ Reference VirtualPath="~/usercontrols/StudentInfoBar.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Student Enrollments</title>
    <script type="text/javascript">
        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
        function ConfirmAttendance(sender, args) {
            args.set_cancel(!window.confirm("Removing classes will remove the attendance for this student. Do you want to continue?"));
        }
        function OpenWnd() {
            radopen(null, "StudentScheduleWindow");
        }
        function OnClientClose(sender, eventArgs) {
            __doPostBack('ClosedScheduleDetailsWindow');
        }
        function savedAndReloadComboBox() {
            
            window.alert('Information was saved successfully');

            var ddlProgVer = document.getElementById("ContentMain2_ddlPrgVerId"); 
            var selectecProgVer = ddlProgVer.options[ddlProgVer.selectedIndex];

            var combo = $find('ctl00_statusBarControl_rcbActiveEnrollments');
            combo.trackChanges();
            var comboItem = new Telerik.Web.UI.RadComboBoxItem();
            comboItem.set_text(selectecProgVer.text);
            comboItem.set_value(selectecProgVer.value);
            combo.get_items().add(comboItem);
            comboItem.select();
            combo.commitChanges(); 
        }

        function matchLSSelectionAndStudentBar() {
            var ddlProgVer = document.getElementById("ContentMain2_ddlPrgVerId"); 
            var selectecProgVer = ddlProgVer.options[ddlProgVer.selectedIndex];
            var combo = $find('ctl00_statusBarControl_rcbActiveEnrollments');
            combo.trackChanges();
            var comboItem = combo.findItemByText(selectecProgVer.text);
            comboItem.select();
            combo.commitChanges(); 
        }

        var countCheckChangeHeight = 0
        function checkForChanges() {
            var advBodyHeigth = document.getElementsByClassName("AdvantageMainBody")[0].scrollHeight;
            var innerDivHeight = $("#RAD_SPLITTER_PANE_CONTENT_ctl00_ContentMain2_OldMenuPane").height();
            if (advBodyHeigth != innerDivHeight) {
                $("#RAD_SPLITTER_PANE_CONTENT_ctl00_ContentMain2_OldMenuPane").css('height', advBodyHeigth + 'px');
            } else {
                countCheckChangeHeight++;
            }

            if (countCheckChangeHeight < 10) {
                setTimeout(checkForChanges, 500);

            }
        }
        $(document).ready(function () {
            checkForChanges();

            $('tbody').on("change", ".writtenPartsDropdown", function (e) {
                if (this.value == 1) {
                    $('.OptionalLicensingExamDetailsPanel').removeAttr('style');
                }else {
                    $('.OptionalLicensingExamDetailsPanel').css('display','none');
                }
            })
        });
    </script>
    <style>
        .button-margin .rfdSkinnedButton {
            margin-left: 2px;
            margin-bottom: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <uc1:AdvantageRadAjaxLoadingControl ID="AdvantageRadAjaxLoadingControl1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="AdvantageRadAjaxLoadingControl1">

      <telerik:RadWindow ID="StudentScheduleWindow" VisibleOnPageLoad="False"  Width="980" Height="524" runat="server">
            <ContentTemplate>
                <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
                            <fame:StudentSchedule ID="studentSchedule" runat="server" />
                            </contenttemplate>
                </telerik:RadAjaxPanel>
            </ContentTemplate>
        </telerik:RadWindow>

        <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
            Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
            <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#fafafa" Width="350" Scrolling="Y">
                <%-- Add class ListFrameTop2 to the table below --%>
                <%--LEFT PANE CONTENT HERE--%>
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td nowrap align="left" width="15%">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td nowrap width="85%">
                                        <asp:RadioButtonList ID="radStatus" runat="Server" CssClass="label" AutoPostBack="true"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom" style="white-space: normal">
                            <div class="scrollleftfilters" style="white-space: normal">
                                <asp:DataList ID="dlstStdEnroll" runat="server" DataKeyField="StuEnrollId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                            Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'
                                            CausesValidation="False"></asp:ImageButton>
                                        <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'
                                            CausesValidation="False"></asp:ImageButton>
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                        <asp:LinkButton Text='<%# Container.DataItem("PrgVerDescrip")%>' runat="server" Style="font-weight: normal !important;"
                                            CommandArgument='<%# Container.DataItem("StuEnrollId")%>' ID="Linkbutton1" CausesValidation="False"/>
                                    </ItemTemplate>
                                </asp:DataList>
                                <div id="trackhistmess" class="trackHistory" runat="server" visible="false">
                                    <h1>Student Transfer Track History</h1>
                                    <div class="trackHistoryLabel">
                                        <p>
                                            <asp:Label ID="lblTrackMessage" runat="Server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="Forward"></telerik:RadSplitBar>
            <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
                Orientation="HorizontalTop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" style="text-align: right;">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                            <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                </table>

                <div style="padding: 20px 20px 20px 20px;">
                    <table class="contenttable" cellspacing="0" cellpadding="0" style="text-align: left; width: 90%;">
                        <tr>
                            <td class="contentcell" style="text-align: left; width: 15%">
                                <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="label" Enabled="False"
                                    Visible="True">LOAs</asp:LinkButton>
                            </td>
                            <td class="contentcell" style="text-align: left; width: 15%">
                                <asp:LinkButton ID="Linkbutton3" runat="server" CssClass="label" Enabled="False">Probations / Warning</asp:LinkButton>
                            </td>
                            <%-- <td class="contentcell" style="text-align: left; width: 15%">
                                <asp:LinkButton ID="Linkbutton4" runat="server" CssClass="label" Enabled="False">Warnings</asp:LinkButton>
                            </td>--%>
                            <td class="contentcell" style="text-align: left; width: 15%">
                                <asp:LinkButton ID="Linkbutton5" runat="server" CssClass="label" Enabled="False"
                                    Visible="True">Suspensions</asp:LinkButton>
                            </td>
                            <td class="contentcell" style="text-align: left; width: 15%">
                                <asp:LinkButton ID="lnkbtnReschReason" runat="server" CssClass="label" Enabled="False">Reschedules</asp:LinkButton>
                            </td>
                            <td class="contentcell" style="text-align: left; width: 25%">
                                <asp:LinkButton ID="lnkbtnStatusChangeHistory" runat="server" CssClass="label" Enabled="False"
                                    CausesValidation="false">Status Change History</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                <hr />
                <table class="maincontenttable">
                    <tr>
                        <td class="detailsframe">
                            <div class="scrollright2">
                                <!-- begin content table-->

                                <asp:Panel ID="pnlRHS" runat="server">
                                    <table class="contenttable">
                                        <tr>
                                            <td colspan="5" style="text-align: center;">
                                                <asp:Label ID="lblCurrentProb" runat="server" CssClass="label" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal; height: 25px;">
                                                <asp:Label ID="lblEnrollmentId" runat="server" CssClass="label">Enrollment Id</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="height: 25px">
                                                <asp:TextBox ID="txtEnrollmentId" runat="server" CssClass="textbox" ReadOnly="True"
                                                    MaxLength="128"></asp:TextBox>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal; height: 25px;">
                                                <asp:Label ID="lblFAAdvisorId" runat="server" CssClass="label">Financial Advisor</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="height: 25px">
                                                <asp:DropDownList ID="ddlFAAdvisorId" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="emptycell" style="height: 25px"></td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblPrgVerId" runat="server" CssClass="label">Program Version</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlPrgVerId" runat="server" AutoPostBack="True" CssClass="dropdownlist" Enabled="False" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblStatusCodeId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlStatusCodeId" runat="server" CssClass="dropdownlist" AutoPostBack="True" CausesValidation="False" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblSchedule" runat="server" CssClass="label">Schedule</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell button-margin">
                                                <asp:DropDownList ID="ddlSchedules" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                                                                          
                                                <asp:LinkButton ID="ibToMaintPage_Schedule" runat="server">
                                                    <span class="k-icon k-i-edit"></span>
                                                </asp:LinkButton>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal"></td>
                                            <td class="stuenrollcontentcell"></td>
                                            <td class="stuenrollheadercell" style="white-space: normal; height: 25px;">
                                                <asp:Label ID="lblDateDetermined" runat="server" CssClass="label" Visible="False" Text="Date Determined"></asp:Label>
                                                <asp:RequiredFieldValidator ID="rfvDateDetermined" runat="server" ControlToValidate="txtDateDetermined" Enabled="False"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="txtDateDetermined" runat="server" CssClass="textbox" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="SuvaStuff" runat="server">
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblSubaVersion" runat="server" CssClass="label">Program Version Type<span style="color: #b71c1c">*</span></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlSuvaVersion" runat="server" AutoPostBack="False" CssClass="dropdownlist">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal; height: 25px;">
                                                <asp:Label ID="lblDropReasonId" runat="server" CssClass="label" Visible="False" Text="Drop Reason"></asp:Label>
                                                <asp:RequiredFieldValidator ID="rfvDropReason" runat="server" ControlToValidate="ddlDropReasonId" Enabled="False"></asp:RequiredFieldValidator>

                                            </td>
                                            <td class="stuenrollcontentcell" style="height: 25px">
                                                <asp:DropDownList ID="ddlDropReasonId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblEnrollDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtEnrollDate" runat="server" Width="200" daynameformat="Short" MinDate="1/1/1945" AutoPostBack="true"
                                                    OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblCampusId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlCampusId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblExpStartDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtExpStartDate" runat="server" Width="200" daynameformat="Short" MinDate="1/1/1945" AutoPostBack="true"
                                                    OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblEmpId" runat="server" CssClass="label">Academic Advisor</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlEmpId" runat="server" CssClass="dropdownlist" Width="200">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblStartDate" runat="server" CssClass="label">Version Start Date</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtStartDate" runat="server" Width="200" daynameformat="Short" MinDate="1/1/1945" AutoPostBack="true"
                                                    OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblAdminRep" runat="server" CssClass="label">Admissions Rep</asp:Label><span style="color: #b71c1c">*</span>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlAdminRep" runat="server" CssClass="dropdownlist" Width="200">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblMidPtDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtMidPtDate" runat="server" Width="200" daynameformat="Short" MinDate="1/1/1945" AutoPostBack="true"
                                                    OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblShiftId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlShiftId" runat="server" CssClass="dropdownlist" Width="200">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblExpGradDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtExpGradDate" runat="server" Width="200" daynameformat="Short" MinDate="1/1/1945" AutoPostBack="true"
                                                    OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblEdLvlId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlEdLvlId" runat="server" CssClass="dropdownlist" Width="200">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblContractedGradDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtContractedGradDate" runat="server" Width="200" daynameformat="Short" MinDate="1/1/1945" AutoPostBack="true"
                                                    OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
                                                </telerik:RadDatePicker>
                                                <asp:Button ID="btnCalculateContractedGradDate" runat="server" Text="Calculate Grad Date" CausesValidation="False"></asp:Button>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblBillingMethodId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlBillingMethodId" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="disableAutoChargeContainer" runat="server">
                                            <td colspan="2"></td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblDisableAutoCharge" runat="server" CssClass="label" Text="Disable Auto Charge"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlDisableAutoCharge" runat="server" CssClass="dropdownlist" Width="200px">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblTransferDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="txtTransferDate" runat="server" CssClass="textbox" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblDegCertSeekingId" runat="server" CssClass="label" Visible="true"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlDegCertSeekingId" runat="server" CssClass="dropdownlist" Width="200">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblAttendTypeId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlAttendTypeId" runat="server" CssClass="dropdownlist" Width="200">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblSAPId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlSAPId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblTuitionCategoryId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlTuitionCategoryId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblDisabled" runat="server" CssClass="label" Text="Disabled"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlIsDisabled" runat="server" CssClass="dropdownlist" DefaultMessage="Select...">
                                                    <asp:ListItem Value="-1">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblLDA" runat="server" CssClass="label" Visible="True"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtLDA" runat="server" Width="200" daynameformat="Short"
                                                    Enabled="false">
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblEntranceInterview" runat="server" CssClass="label" Text="Entrance Interview"></asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <telerik:RadDatePicker ID="EntranceInterviewDate" runat="server" Calendar-ShowRowHeaders="false" MinDate="1/1/1945" Width="200px">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="Label2" runat="server" CssClass="label">Re-Enroll Date</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="txtReEnrollmentDate" runat="server" CssClass="textbox" Enabled="false" Width="176px"></asp:TextBox>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal; height: 25px;">
                                                <asp:Label ID="lblbadgenumber" runat="server" CssClass="label">Badge Number</asp:Label>

                                            </td>

                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="txtbadgenumber" runat="server" CssClass="textbox"></asp:TextBox>&nbsp&nbsp
 
                                                <asp:Button ID="btnNewBadgeNum" runat="server" Text="Generate Badge ID"></asp:Button>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="Label4" runat="server" CssClass="label">Cohort Start Date</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtCohortStartDate" runat="server" Width="200" daynameformat="Short">
                                                </telerik:RadDatePicker>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="Label5" runat="server" CssClass="label">H.S. Graduation/GED</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="text-align: left;">
                                                <telerik:RadDatePicker ID="txtGraduatedOrReceivedDate" runat="server" Width="200"
                                                    daynameformat="Short" MinDate="01/01/1900" maxdate="01/01/4000">
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal; height: 25px;">
                                                <asp:Label ID="lblnewPrgVersion" Visible="false" runat="server" CssClass="label">New Program version</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlnewPrgVersion" runat="server" AutoPostBack="false" Visible="false"
                                                    CssClass="dropdownlist" Width="200">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal; vertical-align: middle;">
                                                <asp:Label ID="lblDistanceEd" runat="server" CssClass="label">Distance Ed Status</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell" style="">
                                                <asp:RadioButtonList ID="rblDistanceEducation" runat="server" AutoPostBack="true" CssClass="radiobutton" Height="25px"
                                                    RepeatDirection="Horizontal" ToolTip="Select the Distance Education status">
                                                    <asp:ListItem Value="Only">Only</asp:ListItem>
                                                    <asp:ListItem Value="Some">Some</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="None">None</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td></td>
                                            <td class="stuenrollcontentcell">
                                                <asp:Button runat="server" ID="btnReschedule" Text="Reschedule"
                                                    Visible="false" Width="200" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="stuenrollcontentcell label">
                                                <asp:CheckBox ID="chkIsFirstTimeInSchool" runat="server" Checked="False" Visible="true" />
                                                <asp:Label ID="Label9" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal; height: 25px;">
                                                <asp:Label ID="lblReschReason" Visible="false" runat="server" CssClass="label">Reschedule Reason</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="txtReschReason" Visible="false" runat="server" CssClass="textbox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="stuenrollcontentcell label">
                                                <asp:CheckBox ID="chkIsFirstTimePostSecSchool" runat="server" Checked="False" Visible="true" />
                                                <asp:Label ID="Label10" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal; height: 25px;"></td>
                                            <td class="stuenrollcontentcell">
                                                <asp:Button ID="bAddToOnLine" runat="server" CausesValidation="false" Text="Add Student to OnLine" Visible="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblThirdPartyContract" runat="server" CssClass="label" Text="Attending through a contract with a third party"></asp:Label>
                                            </td>

                                            <td class="stuenrollcontentcell">
                                                <asp:DropDownList ID="ddlThirdPartyContract" AppendDataBoundItems="true" CssClass="dropdownlist" runat="server" Width="200">
                                                    <asp:ListItem Text="No" Value="0" />
                                                    <asp:ListItem Text="Yes" Value="1" />
                                                </asp:DropDownList>

                                            </td>
                                        </tr>
                                        <asp:TextBox ID="txtDistanceEdStatus" runat="server" Visible="false">
                                        </asp:TextBox>
                                    </table>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" runat="server"
                                        id="trackmessagecontrols">
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="LblTransferFromCampus" runat="server" CssClass="label">Transferred From Campus</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="lblTransferFromCampusValue" runat="server" ReadOnly="True" CssClass="textbox"></asp:TextBox>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="LblTransferFromProgram" runat="server" CssClass="label">Transferred From Program</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="LblTransferFromProgramValue" runat="server" ReadOnly="True" CssClass="textbox"></asp:TextBox>
                                            </td>
                                            <td class="emptycell"></td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="LblTransfertoCampus" runat="server" CssClass="label">Transferred To Campus</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="LblTransferToCampusValue" runat="server" ReadOnly="True" CssClass="textbox"></asp:TextBox>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="LblTransferToProgram" runat="server" CssClass="label">Transferred To Program</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="LblTransferToProgramValue" runat="server" ReadOnly="True" CssClass="textbox"></asp:TextBox>
                                            </td>
                                            <td class="emptycell"></td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="lblTransferedDate" runat="server" CssClass="label">Transferred Date</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="lblTransferDateValue" runat="server" ReadOnly="True" CssClass="textbox"></asp:TextBox>
                                            </td>
                                            <td class="stuenrollheadercell" style="white-space: normal">
                                                <asp:Label ID="Label3" runat="server" CssClass="label">Transferred By</asp:Label>
                                            </td>
                                            <td class="stuenrollcontentcell">
                                                <asp:TextBox ID="txtTransferedBy" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                            </td>
                                            <td class="emptycell"></td>
                                        </tr>
                                    </table>
                                    <table class="contenttable" width="100%">
                                        <tbody>
                                            <tr>
                                                <td class="stuenrollheadercell" style="white-space: normal; height: 25px;">
                                                    <asp:Label ID="lbltransferHours" runat="server" Visible="false" CssClass="label">Transfer Hours</asp:Label>
                                                </td>
                                                <td class="stuenrollcontentcell">
                                                    <asp:TextBox ID="txtTransferHours" Visible="false" runat="server" CssClass="textbox"></asp:TextBox>
                                                </td>

                                                <td class="stuenrollheadercell" style="white-space: normal; height: 25px;"></td>
                                                <td class="stuenrollcontentcell"></td>
                                                <td class="emptycell"></td>
                                            </tr>

                                            <tr id="rowTotalTransferHours" runat="server">

                                                <td class="stuenrollheadercell" style="white-space: normal">
                                                    <asp:Label ID="lblTotalTransferHours" runat="server" CssClass="label">Total Transfer Hours</asp:Label>
                                                </td>
                                                <td class="stuenrollcontentcell">
                                                    <asp:TextBox ID="txtTotalTransferHours" runat="server" CssClass="textbox" 
                                                        MaxLength="128" TextMode="Number" AutoPostBack="True" OnTextChanged="txtTotalTransferHours_OnTextChanged" on></asp:TextBox>
                                                </td>
                                                <td class="stuenrollheadercell" style="white-space: normal; height: 25px;"></td>
                                                <td class="stuenrollcontentcell"></td>
                                                <td class="emptycell"></td>

                                            </tr>
                                        
                                            <tr id="rowTotalTransferHoursFromThisSchool" runat="server">
                                                <td class="stuenrollheadercell" style="white-space: normal">
                                                    <asp:Label ID="lblTotalTransferHoursFromThisSchool" runat="server" CssClass="label">Transfer Hours From This School</asp:Label>
                                                </td>
                                                <td class="stuenrollcontentcell">
                                                    <asp:TextBox ID="txtTotalTransferHoursFromThisSchool" runat="server" CssClass="textbox" AutoPostBack="True"
                                                                 MaxLength="128" TextMode="Number" OnTextChanged="txtTotalTransferHoursFromThisSchool_OnTextChanged"></asp:TextBox>
                                                </td>
                                                <td class="stuenrollheadercell" style="white-space: normal; height: 25px;"></td>
                                                <td class="stuenrollcontentcell"></td>
                                                <td class="emptycell"></td>
                                            </tr>
                                        
                                            <tr id="rowTotalTransferHoursFromProgram" runat="server">
                                                <td class="stuenrollheadercell" style="white-space: normal">
                                                    <asp:Label ID="lblTotalTransferHoursFromProgram" runat="server" CssClass="label">From Program</asp:Label>
                                                </td>
                                                <td class="stuenrollcontentcell">
                                                    <asp:DropDownList ID="ddTransferHourFromProgram" runat="server" CssClass="dropdownlist" AutoPostBack="True" Width="200">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="stuenrollheadercell" style="white-space: normal; height: 25px;"></td>
                                                <td class="stuenrollcontentcell"></td>
                                                <td class="emptycell"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr />
                                 <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6" style="border-left: 0 !important; border-right: 0 !important; border-top: 0">
                                                    <asp:Label ID="LabelLED" runat="server" CssClass="label" Font-Bold="true">Licensure Exam Details</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleft" nowrap style="padding-top: 16px !important; padding-bottom: 16px !important;">
                                                    <asp:Label ID="LabelWrittenAllParts" runat="server" CssClass="label" Text="All parts of exam completed?"></asp:Label>
                                                    <font color="red">*</font>
                                                </td>

                                                <td class="contentcell4column" style="padding-top: 16px !important; padding-bottom: 16px !important;">
                                                    <asp:DropDownList ID="ddlWrittenAllParts" TabIndex="41" CssClass="dropdownlist writtenPartsDropdown" runat="server" >
                                                        <asp:ListItem Text="No" Value="0" />
                                                        <asp:ListItem Text="Yes" Value="1" />
                                                    </asp:DropDownList>

                                                </td>
                                            </tr>

                                            <tbody runat="server" id="OptionalLicensingExamDetails" class="OptionalLicensingExamDetailsPanel">
                                                <tr>

                                                    <td class="leadcell4columnleft">
                                                        <asp:Label ID="LabelLastPart" runat="server" CssClass="label" Text="Date Completed"></asp:Label>
                                                        <font color="red">*</font>
                                                    </td>
                                                    <td class="contentcell4column" nowrap>
                                                        <telerik:RadDatePicker ID="dpLastPart" runat="server" Width="200" daynameformat="Short" CssClass="required">
                                                        </telerik:RadDatePicker>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap style="padding-top: 16px">
                                                        <asp:Label ID="LabelPassedAllParts" runat="server" CssClass="label" Text="Passed all parts of exam?"></asp:Label>
                                                        <font color="red">*</font>
                                                    </td>
                                                    <td class="contentcell4column" style="padding-top: 16px !important; padding-bottom: 16px">
                                                        <asp:DropDownList ID="ddlPassedAllParts" TabIndex="41" AppendDataBoundItems="true" CssClass="dropdownlist" runat="server">
                                                            <asp:ListItem Text="No" Value="0" />
                                                            <asp:ListItem Text="Yes" Value="1" />
                                                        </asp:DropDownList>

                                                    </td>
                                                    <td class="leadcell4column" nowrap style="padding-bottom: 16px">&nbsp;</td>
                                                    <td class="contentcell4columnright" style="padding-bottom: 16px">&nbsp;</td>
                                                </tr>

                                                <asp:CustomValidator ID="LicensingExamDetailsValidator"
                                                    OnServerValidate="ValidateLicensingExamDetails"
                                                    ValidateEmptyText="True"
                                                    runat="server"> 

                                                </asp:CustomValidator>
                                            </tbody>

                                        </table>
                                <hr />
                                    <table class="contenttable">
                                        <tr>
                                            <td class="stuenrollheadercellheader">
                                                <asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="label">Select Student Groups<span style="color: #b71c1c" >*</span></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="leadcell2">
                                                <asp:CheckBoxList ID="chkLeadGrpId" TabIndex="5" runat="Server" RepeatColumns="7" CssClass="checkboxstyle">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                    </table>

                                </asp:Panel>
                                <!-- end table content-->
                                <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                    <table class="contenttable">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="stuenrollheadercellheader" nowrap>
                                                <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="contenttable">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%;">
                                                <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" Width="100%" EnableViewState="false">
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:TextBox ID="txtStudentId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox
                                    ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                <asp:TextBox ID="txtStuEnrollId" runat="server" Visible="false"></asp:TextBox><asp:CheckBox
                                    ID="chkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                <asp:CheckBox
                                    ID="chkIsDisabled" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                <asp:TextBox ID="TxtLeadId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtPrgVersionTypeId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtEntranceInterviewDate" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                <!-- end content table-->
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
        </telerik:RadSplitter>
    </telerik:RadAjaxPanel>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

<asp:Content runat="server" ID="Content5" ContentPlaceHolderID="BottomScriptContentPlaceHolder">
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        var $elemId;

        prm.add_endRequest(function (s, e) {
            if ($elemId == null || $elemId.indexOf("dlstStdEnroll") == -1) {
                var pageLoading = $(document).find(".page-loading");
                if (pageLoading.length !== 0)
                    kendo.ui.progress($(".page-loading"), false);
            }
        });
        
        prm.add_beginRequest(function (s, e) {
            $elemId = e.get_postBackElement().id;
            if ($elemId.indexOf("btnNew") > -1 ||
                $elemId.indexOf("btnSave") > -1 ||
                $elemId.indexOf("btnDelete") > -1 ||
                $elemId.indexOf("dlstStdEnroll") > -1 || 
                $elemId.indexOf("radStatus") > -1|| 
                $elemId.indexOf("ddlPrgVerId") > -1) {
                var show = true
                var pageLoading = $(document).find(".page-loading");
                if (pageLoading.length === 0)
                    $(document).find("body").append('<div class="page-loading"></div>');
                kendo.ui.progress($(".page-loading"), show);
            }
        });
    </script>
</asp:Content>
