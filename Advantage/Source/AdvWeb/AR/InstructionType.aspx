<%@ Page Language="vb" AutoEventWireup="false" Inherits="InstructionType" CodeFile="InstructionType.aspx.vb"
    MasterPageFile="~/NewSite.master" Title="Instuction Types" %>
    <%@ mastertype virtualpath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
   
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
            Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radStatus" CssClass="radiobutton" AutoPostBack="true" runat="Server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframetop2">
                            <div id="grdWithScroll" class="scrollleftfilters" onscroll="SetDivPosition()">
                                <asp:DataList ID="dlstInstructionTypes" DataKeyField="InstructionTypeId" runat="server"
                                    Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" runat="server" CausesValidation="False" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
                                            CommandArgument='<%# Container.DataItem("InstructionTypeId")%>' ImageUrl="../images/Inactive.gif" />
                                        <asp:ImageButton ID="imgActive" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>'
                                            CommandArgument='<%# Container.DataItem("InstructionTypeId")%>' ImageUrl="../images/Active.gif" />
                                        <asp:LinkButton ID="Linkbutton1" CssClass="itemstyle" CausesValidation="False" runat="server"
                                            CommandArgument='<%# Container.DataItem("InstructionTypeId")%>' Text='<%# Container.DataItem("InstructionTypeDescrip")%>' />
                                    </ItemTemplate>
                                </asp:DataList></div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
                Orientation="HorizontalTop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False">
                            </asp:Button>
                            <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
                <!-- end top menu (save,new,reset,delete,history)-->
                <!--begin right column-->
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <div class="scrollright2">
                                <!-- begin table content-->
                                <asp:Panel ID="pnlRHS" runat="server">
                                    <table class="contenttable" align="center">
                                        <tr>
                                            <td class="contentcell" nowrap colspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblInstructionTypeCode" CssClass="Label" runat="server">Code<font color="red">*</font></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtInstructionTypeCode" CssClass="textbox" runat="server" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqInstructionTypeCode" runat="server" ControlToValidate="txtInstructionTypeCode"
                                                    Display="None" ErrorMessage="Code is Required">Code is Required</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revInstructionTypeCode" runat="server" ControlToValidate="txtInstructionTypeCode"
                                                    Display="None" ErrorMessage="too many characters" ValidationExpression=".{0,10}"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblStatusId" CssClass="Label" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblInstructionTypeDescrip" CssClass="Label" runat="server">Description<font color="red">*</font></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtInstructionTypeDescrip" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqInstructionTypeDescrip" runat="server" ControlToValidate="txtInstructionTypeDescrip"
                                                    Display="None" ErrorMessage="Description is Required">Description is Required</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revInstructionTypeDescrip" runat="server" ControlToValidate="txtInstructionTypeDescrip"
                                                    Display="None" ErrorMessage="too many characters" ValidationExpression=".{0,50}"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:CheckBox ID="chkIsDefault" runat="server" CssClass="checkbox" Text="Make Default"
                                                    TextAlign="Right" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtInstructionTypeId" CssClass="Label" runat="server" MaxLength="128"
                                                    Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtOrigInstructionTypeId" CssClass="Label" runat="server" MaxLength="128"
                                                    Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtCampGrpId" CssClass="Label" runat="server" MaxLength="128" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                                <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtRowIds" runat="server" Visible="false"></asp:TextBox>
                                                <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                                <asp:TextBox ID="txtOrigCode" runat="server" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtOrigDesc" runat="server" Visible="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <!--end table content-->
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
    </div>
</asp:Content>
