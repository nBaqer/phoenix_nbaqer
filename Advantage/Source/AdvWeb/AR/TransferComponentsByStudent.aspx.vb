﻿Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports Advantage.Business.Objects
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Partial Class TransferComponentsByStudent
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


#End Region

    Protected ResourceId As Integer

    Protected campusId, userId As String
    Protected ModuleId As String
    Dim pobj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

#Region "Page Events"

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SetVariables()
        If Not IsPostBack Then
            Session("StuEnrollmentId") = Nothing
        End If

    End Sub

#End Region

#Region "Grid Events"

    Protected Sub rgIncompleteCourses_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles rgIncompleteCourses.NeedDataSource
        If Not e.IsFromDetailTable Then
            BindIncompleteCourseGrid(Session("StuEnrollmentId"))
        End If
    End Sub

    Protected Sub rgIncompleteCourses_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles rgIncompleteCourses.ItemDataBound
        'populate filter terms drop down
        If TypeOf e.Item Is GridFilteringItem Then
            Dim facade As New TransferComponentsFacade
            Dim dtTerms As New DataTable
            If Not Session("StuEnrollmentId") Is Nothing Then
                dtTerms = facade.GetTermsForPartiallyCompletedCourse(Session("StuEnrollmentId"))
            End If
            Dim filterItem As GridFilteringItem = DirectCast(e.Item, GridFilteringItem)
            Dim combo As RadComboBox = TryCast(filterItem.FindControl("rcbTerm"), RadComboBox)
            If combo IsNot Nothing Then
                With combo
                    .DataSource = dtTerms
                    .DataBind()
                    .Items.Insert(0, New RadComboBoxItem("All Terms"))
                End With
            End If
        End If

        'populate available classes drop down
        If TypeOf e.Item Is GridDataItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            If item.OwnerTableView.Name = "IncompleteCourses" Then
                Dim facade As New TransferComponentsFacade
                Dim dtAvailableClasses As New DataTable
                Dim ResultId As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("ResultId").ToString()
                dtAvailableClasses = facade.GetAvailableClassesForPartialTransferByStudent(ResultId)
                Dim combo As RadComboBox = TryCast(item.FindControl("rcbAvailableClasses"), RadComboBox)
                If combo IsNot Nothing Then
                    With combo
                        .DataSource = dtAvailableClasses
                        .DataBind()
                        .Items.Insert(0, New RadComboBoxItem("Please select a class to register", 0))
                    End With
                End If
            End If
        End If

    End Sub

    Protected Sub rgIncompleteCourses_DetailTableDataBind(sender As Object, e As GridDetailTableDataBindEventArgs) Handles rgIncompleteCourses.DetailTableDataBind
        Dim dataItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)

        Select Case e.DetailTableView.Name
            Case "CompletedComponents"
                Dim ResultId As String = dataItem.GetDataKeyValue("ResultId").ToString()
                Dim dsCompletedComponents As New DataSet
                Dim facade As New TransferComponentsFacade
                dsCompletedComponents = facade.GetCompletedComponents(ResultId)
                If dsCompletedComponents.Tables.Count > 0 Then
                    e.DetailTableView.DataSource = dsCompletedComponents.Tables(0)
                End If
        End Select
    End Sub

    Protected Sub rcbTerm_PreRender(sender As Object, e As EventArgs)
        Dim combo As RadComboBox = TryCast(sender, RadComboBox)
        If combo IsNot Nothing AndAlso ViewState(combo.ClientID) IsNot Nothing Then
            Dim item As RadComboBoxItem = combo.FindItemByValue(ViewState(combo.ClientID))
            If item Is Nothing Then
                combo.SelectedValue = ""
                ViewState(combo.ClientID) = Nothing
            Else
                combo.SelectedValue = ViewState(combo.ClientID).ToString()
            End If
        End If
    End Sub

    Protected Sub rcbTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        Dim combo As RadComboBox = TryCast(sender, RadComboBox)
        If combo IsNot Nothing Then
            ViewState(combo.ClientID) = e.Value
        End If

        Dim filterExpression As String = ""
        If Not e.Value = "" Then
            filterExpression = "([TermId] = '" + e.Value + "')"
        End If
        rgIncompleteCourses.MasterTableView.FilterExpression = filterExpression
        rgIncompleteCourses.Rebind()
    End Sub

#End Region

#Region "Button Click Events"

    Protected Sub btnTransferComponents_Click(sender As Object, e As EventArgs) Handles btnTransferComponents.Click
        Dim RegistrationProcessFacade As New RegistrationProcessFacade
        Dim facade As New TransferComponentsFacade
        Dim ClassList As String = String.Empty
        Dim classConflicts As String = String.Empty
        Dim CourseCount As Integer = GetCourseCount()
        If CourseCount < 1 Then
            DisplayRADAlert(CallbackType.Postback, "Error1", "No courses are available to transfer.", "Unable to Transfer")
        Else
            Dim StudentRegistrationList As List(Of StudentRegistration.Lib.Student) = GetStudentRegistrationList()
            If StudentRegistrationList.Count < 1 Then
                DisplayRADAlert(CallbackType.Postback, "Error2", "Please select a course to transfer.", "Unable to Transfer")
            Else
                ClassList = getClassSectionsList(StudentRegistrationList)
                classConflicts = facade.GetScheduleConflictsTransferPartialForStudent(ClassList)
                If classConflicts = String.Empty Then
                    RegistrationProcessFacade.RegisterStudentIncompleteCourses(StudentRegistrationList, AdvantageSession.UserState.UserName)
                    DisplayRADAlert(CallbackType.Postback, "Complete", CourseCount & " transfer" & IIf(CourseCount <> 1, "s", "") & " completed.",  "Transfer Complete")
                    rgIncompleteCourses.Rebind()
                Else
                    DisplayRADAlertCustomSize(CallbackType.Postback, "Conflict", "There are conflicts in these classes " & classConflicts & " . No classes have been transferred.", 600, 300, "Schedule Conflicts")
                End If

            End If
        End If
    End Sub

#End Region

#Region "Private Functions"
    Private Function getClassSectionsList(StudentRegistrationList As List(Of StudentRegistration.Lib.Student)) As String
        Dim rtn As String = String.Empty
        For i As Integer = 0 To StudentRegistrationList.Count - 1
            For j As Integer = 0 To StudentRegistrationList(i).Classes.Count - 1
                If rtn = String.Empty Then
                    rtn = StudentRegistrationList(i).Classes(j).newClassId
                Else
                    rtn = rtn & "," & StudentRegistrationList(i).Classes(j).newClassId
                End If

            Next
        Next
        Return rtn

    End Function
    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, _
                           ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, _
                           ByVal hourtype As String)

        If enrollid = "" Then enrollid = Session("StuEnrollmentId")
        If fullname = "" Then fullname = Session("StudentName")

        Session("StuEnrollmentId") = enrollid
        Session("StudentName") = fullname

        SetTransferButton(enrollid)

        rgIncompleteCourses.MasterTableView.FilterExpression = ""
        rgIncompleteCourses.Rebind()
    End Sub

    Private Sub BindIncompleteCourseGrid(ByVal stuEnrollId As String)
        Dim intConfigConsolidate As String = MyAdvAppSettings.AppSettings("ConsolidateCourseComponents")
        If intConfigConsolidate.ToString.ToLower() = "true" AndAlso stuEnrollId <> "" Then
            Dim facade As New TransferComponentsFacade
            Dim dsIncompleteCourses As New DataSet
            dsIncompleteCourses = facade.GetPartiallyCompletedCourses(stuEnrollId)
            If dsIncompleteCourses.Tables.Count > 0 Then
                rgIncompleteCourses.DataSource = dsIncompleteCourses.Tables(0)
            Else
                rgIncompleteCourses.DataSource = String.Empty
            End If
        Else
            rgIncompleteCourses.DataSource = String.Empty
        End If
    End Sub

    Private Function GetCourseCount() As Integer
        Dim count As Integer = 0
        For Each item As GridDataItem In rgIncompleteCourses.Items
            If item.OwnerTableView.Name = "IncompleteCourses" Then
                Dim combo As RadComboBox = TryCast(item.FindControl("rcbAvailableClasses"), RadComboBox)
                If combo IsNot Nothing Then
                    With combo
                        If .SelectedIndex > 0 Then
                            count += 1
                        End If
                    End With
                End If

            End If
        Next
        Return count
    End Function

    Private Function GetSelectedClassList() As List(Of StudentRegistration.Lib.Class)
        Dim ClassList As New List(Of StudentRegistration.Lib.Class)
        For Each item As GridDataItem In rgIncompleteCourses.Items
            If item.OwnerTableView.Name = "IncompleteCourses" Then
                'Dim oldClassId As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("ResultId").ToString()
                Dim oldClassId = item("ClsSectionId").Text
                Dim combo As RadComboBox = TryCast(item.FindControl("rcbAvailableClasses"), RadComboBox)
                If combo IsNot Nothing Then
                    With combo
                        If .SelectedIndex > 0 Then
                            Dim oClass As New StudentRegistration.Lib.Class
                            oClass.newClassId = .SelectedValue
                            oClass.oldClassId = oldClassId
                            ClassList.Add(oClass)
                        End If
                    End With
                End If
            End If
        Next

        Return ClassList
    End Function

    Private Function GetStudentRegistrationList() As List(Of StudentRegistration.Lib.Student)
        Dim StudentRegistrationList As New List(Of StudentRegistration.Lib.Student)
        Dim ClassList As List(Of StudentRegistration.Lib.Class) = GetSelectedClassList()
        If ClassList.Count > 0 Then
            Dim StudentRegistration As New StudentRegistration.Lib.Student
            StudentRegistration.Classes = ClassList
            StudentRegistration.StudentEnrollmentId = Session("StuEnrollmentId")
            StudentRegistrationList.Add(StudentRegistration)
        End If
        Return StudentRegistrationList
    End Function

    Private Sub SetTransferButton(ByVal stuEnrollId As String)
        Dim facade As New AdStudentGroupsFacade
        If facade.IsStudentOnRegistrationHold(stuEnrollId) Then
            btnTransferComponents.Enabled = False
        Else
            btnTransferComponents.Enabled = True
        End If
    End Sub

    Private Sub SetVariables()
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ResourceId = Trim(Request.QueryString("resid"))

        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        pobj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pobj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

    End Sub

#End Region

End Class