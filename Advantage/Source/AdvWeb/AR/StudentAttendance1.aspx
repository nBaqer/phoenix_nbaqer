﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentAttendance1.aspx.vb" Inherits="AdvWeb.AR.StudentAttendance" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script src="../js/AuditHist.js" type="text/javascript"></script>
    <script type="text/javascript">
        function preventBackspace(e) {
            var evt = e || window.event;
            if (evt) {
                var keyCode = evt.charCode || evt.keyCode;
                if (keyCode === 8) {
                    if (evt.preventDefault) {
                        evt.preventDefault();
                    }
                    else {
                        evt.returnValue = false;
                    }
                }
            }
        }
    </script>
    <script>
        var StudentAttendanceStudentId = "<%=StudentId %>";

    </script>
    <script type="text/javascript">

        // Call javascript support code.

        $(document).ready(function () {
          
            var top = $('#StudentAttendanceContainer').offset().top;
            var height = $(window).height() - 240;
            $('#StudentAttendanceContainer').css("height", height + "px");
            //$(window).resize();
            var studentAttendance = new StudentAttendance1.StudentAttendance1();

        });

    
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server" >
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="../Scripts/Advantage.Client.AR.js"></asp:ScriptReference>
        </Scripts>

    </asp:ScriptManagerProxy>
<div id="StudentAttendanceContainer" class="k-content" style="display:block; border:0; padding: 0; margin:0; width: 100%; height:90%; overflow:scroll">
    <div id="StudentAttendanceViewModelRegion"  style="width: 100%; height:100%; ">
        <div id="StudentAttendancetabstrip" style="width: 99%; height: 100%; padding: 0; margin:0; " >
            <ul>
                <li>Attendance Course
                </li>
                <li class="k-state-active">Attendance Student
                </li>
            </ul>
            <!-- Student Attendance Course Attendance Pane -->
            <div id="coursepanel">
                <div id="CourseVertical">
                    <div id="CourseLeftPane">
                        <div id="CourseLeftPaneContent" style="border: 2px solid red; ">
                           <h3>Inner splitter / left pane</h3>
                        </div>
                    </div>
                    <div id="CourseRightPane" style="border: 2px solid green;" >
                        <div id="pane-content" style="border: 2px solid red;">
                            Inner splitter / center pane
                            <p>Resizable only.</p>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Student Attendance Simgle Student Panes -->
            <div style="width: 99%; height:90%;  padding: 0; margin:0; " >
                <table id="SingleVertical" style="width: 100%; height:100% ">
                    <tr style="width: 100%; height:100%; border: 1px solid green;">
                        <td style="width: 400px; height:100%; border: 1px solid black; vertical-align: top">
                            <div id="SingleStudentLeft" style="width: 400px; height: 100%; border: 3px solid red;">
                                <!-- Panel Bar Student Single Attendance -->
                                <ul id="StudentAttendancePanelBar" style="margin: 0;">
                                    <li class="k-state-active">
                                        <span class="k-link k-state-selected">Select Filters:</span>
                                        <div style="margin-left: 5px; margin-right: 5px; margin-top: 10px; padding-bottom: 5px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>Enrollment:</td>
                                                </tr>
                                                <tr style="height: 30px">

                                                    <td>
                                                        <input id="StudentAttendanceFilterEnrollment" data-bind="value: selectedEnrollmentId" style="width: 100%;" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Term:</td>
                                                </tr>
                                                <tr style="height: 30px">
                                                    <td>
                                                        <input id="StudentAttendanceFilterTerm" value="1" style="width: 100%;" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Class:</td>
                                                </tr>
                                                <tr style="height: 30px">

                                                    <td>
                                                        <input id="StudentAttendanceFilterClass" value="1" style="width: 100%;"
                                                            data-role="dropdownlist" /></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </li>
                                    <li id="StudentAttendanceStudentInfo">Info from Student:&nbsp; <span id="selectedStudent"></span>
                                        <div style="margin-left: 5px; margin-right: 5px; margin-top: 10px; padding-bottom: 5px">
                                            <table>
                                                 <tr><td>Student</td><td></td></tr>
                                                 <tr><td>Student Id</td><td></td></tr>
                                                 <tr><td>Program Version</td><td></td></tr>
                                                 <tr><td>Status</td><td></td></tr>
                                                 <tr><td>Start Date</td><td></td></tr>
                                                 <tr><td>Graduation Date</td><td></td></tr>
                                            </table>
                                            

                                        </div>
                                    </li>
                                    <li id="StudentAttendanceSelectStudent"><span>Change Student</span>
                                        <div>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td><span>Select Student: </span>
                                                    </td>
                                                </tr>
                                                <tr style="height: 30px;">

                                                    <td style="height: 34px;">
                                                        <input id="StudentttendanceStudentSelect" value="1" style="width: 100%;" />
                                                    </td>
                                                </tr>
                                                <tr style="height: 30px; text-align: right">
                                                    <td>
                                                        <button id="GetStudentAttendanceStudent" type="button" style="width: 100px">Go! </button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </li>
                                    <li><span>Reports</span>
                                        <div style="margin-left: 10px; margin-right: 10px; margin-top: 10px; padding-bottom: 5px">

                                            <table style="width: 100%">
                                                <tr>
                                                    <td><span>Select Report: </span>
                                                    </td>
                                                </tr>
                                                <tr style="height: 30px;">

                                                    <td style="height: 34px;">
                                                        <input id="StudentAttendanceFilterReport" value="1" style="width: 100%;" />
                                                    </td>
                                                </tr>
                                                <tr style="height: 30px; text-align: right">
                                                    <td>
                                                        <button id="StudentAttendanceReportButton" type="button" style="width: 100px">Go! </button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </li>

                                </ul>

                            </div>
                        </td>
                        <td class="k-header" style="cursor: pointer; width:5px " >
                            <
                        </td>
                        <td style="height:100%; border: 1px solid black; vertical-align: top">
                            <!-- End LEft Panel Student Attendance Individual -->
                            <div id="SingleStudentRight" style="width: 100%; height:100%; border: 1px solid black;">
                                <!-- Right Pane content -->
                                <div id="StudentAttendancePopUp" style="text-align: center;">
                                    <div style="margin-left: 10px; margin-right: 10px; margin-top: 10px; padding-bottom: 5px">
                                        <br />
                                        <div style="background-color: #DAECF4; width: 100%; height: 30px; border: 1px solid #94C0D2;">LOA DAYS</div>
                                        <div id="StudentAttendanceLoaGrid"></div>
                                        <br />
                                        <div style="background-color: #DAECF4; width: 100%; height: 30px; border: 1px solid #94C0D2;">ENROLLMENT STATUS CHANGES</div>
                                        <div id="StudentAttendanceEnrollmentChanges"></div>
                                        <br />
                                        <div style="background-color: #DAECF4; width: 100%; height: 30px; border: 1px solid #94C0D2;">SUSPENDED DAYS</div>
                                        <div id="StudentAttendanceSuspendedDays"></div>
                                        <p />
                                    </div>
                                </div>

                                <div id="lowPane" style="width: 100%; height: 90%; align-content: center; margin: 0 auto; visibility: hidden">
                                    <table class="k-header">
                                        <tr>
                                            <th style="text-align: left">Class Summary:</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="StudentAttendanceGridCourseSummary" style="margin-top: 2px; display: none;"></div>

                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table class="k-header">
                                        <tr>
                                            <th style="text-align: left">Post Attendance by Periods: </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="StudentAttendanceGrid" style="margin-top: 5px; min-height: 100px; display: none"></div>
                                                <div id="StudentAttendanceGridWeek" style="margin-top: 5px; min-height: 100px; display: none"></div>
                                                <div id="StudentAttendanceAbsentPresentGrid" style="margin-top: 5px; min-height: 100px; display: none"></div>
                                                <div id="StudentAttendanceAbsentPresentGridWeek" style="margin-top: 5px; min-height: 100px; display: none"></div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>
    <!--end validation panel-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>


