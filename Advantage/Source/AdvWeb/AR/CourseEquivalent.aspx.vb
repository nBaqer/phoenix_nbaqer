Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects

Partial Class AR_CourseEquivalent
    Inherits BasePage
    Private Sub BuildCourses()
        With ddlReqId
            .DataTextField = "CodeAndDescrip"
            .DataValueField = "ReqId"
            .DataSource = (New CoursesFacade).GetAllCourses(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private pObj As New UserPagePermissionInfo
    Dim objCommon As New CommonUtilities
    Dim resourceId As Integer
    Dim campusId As String
    Dim userId, ModuleId As String
    Dim m_Context As HttpContext
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtresourceid.Text = CInt(m_Context.Items("ResourceId"))

        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        'Dim ds As New DataSet
        'Dim sw As New System.IO.StringWriter
        'Dim ds2 As New DataSet
        '   Dim sStatusId As String

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtresourceid.Text = resourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtresourceid.Text, campusId)
        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then
                m_Context = HttpContext.Current
                objCommon.PageSetup(CType(Master.FindControl("contentmain2"), ContentPlaceHolder), "NEW")
                objCommon.SetBtnState(CType(Master.FindControl("contentmain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                'Header1.EnableHistoryButton(False)
                btnnew.Enabled = False
                btndelete.Enabled = False
                BuildCourses()
                pnlgrid.Visible = False
                'Call InitButtonsForLoad
                InitButtonsForLoad()
            Else
                objCommon.PageSetup(CType(Master.FindControl("contentmain2"), ContentPlaceHolder), "EDIT")
                'Call the procedure for edit
                InitButtonsForEdit()
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub
    Protected Sub ddlReqId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReqId.SelectedIndexChanged
        If Not ddlReqId.SelectedValue = "" Then
            pnlGrid.Visible = True
            Label1.Text = "Select courses for " & ddlReqId.SelectedItem.Text & " from the list below"
            BindDataList()
        End If
    End Sub
    Private Sub BindDataList()
        With dgrdCourseEquivalent
            .DataSource = (New CoursesFacade).GetCourses(campusId, ddlReqId.SelectedValue)
            .DataBind()
        End With
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim intArrCount As Integer
        '        Dim intGridRowCount As Integer
        iitems = dgrdCourseEquivalent.Items
        'Loop thru the collection to get the Array Size
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            If (CType(iitem.FindControl("chkReqSelected"), CheckBox).Checked = True) Then
                intArrCount += 1
            End If
        Next
        'Dim oControl As Control
        'If intArrCount = iitems.Count Then
        '    For Each oControl In dgrdCourseEquivalent.Controls(0).Controls
        '        If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
        '            CType(FindControl("chkAllItems"), HtmlInputCheckBox).Checked = True
        '        End If
        '    Next
        'End If
    End Sub
    Public Function CheckCourse(ByVal intReq As Integer) As Boolean
        If intReq >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        'Dim strLeadGrpId As String
        'Dim intRequired As Integer
        Dim intArrCount As Integer
        Dim intResult As String
        Dim z As Integer
        'Dim intReqCount As Integer
        'Dim strMessage As String

        iitems = dgrdCourseEquivalent.Items
        Try
            'Loop thru the collection to get the Array Size
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkReqSelected"), CheckBox).Checked = True) Then
                    intArrCount += 1
                End If
            Next

            If intArrCount >= 1 Then
                Dim selectedRequirement() As String = Array.CreateInstance(GetType(String), intArrCount)
                'Loop Through The Collection To Get ClassSection
                For i = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    'retrieve clssection id from the datagrid
                    If (CType(iitem.FindControl("chkReqSelected"), CheckBox).Checked = True) Then
                        selectedRequirement.SetValue(CType(iitem.FindControl("lbladReqId"), Label).Text, z)
                        z += 1
                    End If
                Next
                intResult = (New CoursesFacade).AddCourseEquivalent(ddlReqId.SelectedValue, selectedRequirement, Session("UserName"))
                If Not intResult = "" Then
                    DisplayErrorMessage(intResult)
                    Exit Sub
                End If
            Else
                intResult = (New CoursesFacade).DeleteCourseEquivalent(ddlReqId.SelectedValue)
                If Not intResult = "" Then
                    DisplayErrorMessage(intResult)
                    Exit Sub
                End If
            End If
            BindDataList()
        Finally
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
End Class
