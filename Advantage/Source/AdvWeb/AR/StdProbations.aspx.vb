Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects

Partial Class StdProbations
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected campusId As String

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Protected resourceId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim ds As New DataSet
        Dim facade As New ProbationFacade
        Dim objCommon As New CommonUtilities
        Dim dt As New DataTable
        '   Dim m_Context As HttpContext
        Dim StdEnrollId As String = String.Empty

        'disable history button
        'Header1.EnableHistoryButton(False)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = HttpContext.Current.Request.Params("resid")
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Not Page.IsPostBack Then
            If Request.QueryString("StuEnrollId") <> "" Then
                StdEnrollId = Request.QueryString("StuEnrollId")
                ViewState("StuEnrollId") = StdEnrollId
            End If

            'Dim objcommon As New CommonUtilities
            ds = facade.GetStudentProbations(StdEnrollId)

            PopulateProbationTypeDdl()

            With dgdDropCourse
                .DataSource = ds
                .DataBind()
            End With
            ViewState("Drop") = ds.Tables("Drop")
        Else

        End If
    End Sub
    Private Sub PopulateProbationTypeDdl()

        Dim facade As New ProbationFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlProbWarningTypeId
            .DataSource = facade.GetProbationTypes()
            .DataTextField = "Descrip"
            .DataValueField = "ProbWarningTypeId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select All", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub ddlProbWarningTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProbWarningTypeId.SelectedIndexChanged
        Dim ds As New DataSet
        Dim facade As New ProbationFacade
        Dim StdEnrollId As String = String.Empty

        If Request.QueryString("StuEnrollId") <> "" Then
            StdEnrollId = Request.QueryString("StuEnrollId")
            ViewState("StuEnrollId") = StdEnrollId
        End If
        If (ddlProbWarningTypeId.SelectedIndex > 0) Then

            ds = facade.GetStudentProbations(StdEnrollId, Integer.Parse(ddlProbWarningTypeId.SelectedValue))
        Else
            ds = facade.GetStudentProbations(StdEnrollId)
        End If

        With dgdDropCourse
            .DataSource = ds
            .DataBind()
        End With
        ViewState("Drop") = ds.Tables("Drop")
    End Sub
End Class
