﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentAcademics.aspx.vb" Inherits="StudentAcademics" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript">
    </script>
    <script language="javascript" type="text/javascript" src="../js/Print.js"></script>
    <link rel="stylesheet" type="text/css" href="../FameLink/PrintStyle.css" />
    <style>
        @media only screen and ( max-height: 800px) {
            .scrollleft {
                height: 350px !important;
            }
        }

        @media only screen and ( min-height: 800px ) and ( max-height: 1900px) {
            .scrollleft {
                height: 500px !important;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="375" Scrolling="Y">
            <asp:TreeView ID="tvAcademics" runat="server" SelectedNodeStyle-Font-Bold="true" ExpandDepth="1">
                <DataBindings>
                    <asp:TreeNodeBinding DataMember="ProgramVersion" Depth="0" TextField="PrgVerDescrip"
                        ValueField="PrgVerId" />
                    <asp:TreeNodeBinding DataMember="Module" Depth="1" TextField="Grpdescrip" ValueField="GrpId" />
                    <asp:TreeNodeBinding DataMember="Course" Depth="2" TextField="CourseCodeDescrip" ValueField="ReqId" />
                    <asp:TreeNodeBinding DataMember="GradeBook" Depth="3" TextField="Comments" ValueField="GrdBkResultId" />
                </DataBindings>
            </asp:TreeView>
            <asp:XmlDataSource ID="XmlDataSource1" runat="server" EnableCaching="false"></asp:XmlDataSource>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="99%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                                <!-- begin top menu (save,new,reset,delete,history)-->
                                <tr>
                                    <td class="menuframe" align="right">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="false"></asp:Button>
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                            Enabled="False"></asp:Button></td>
                                </tr>
                            </table>
                            <!-- end top menu (save,new,reset,delete,history)-->
                            <!--begin right column-->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                                align="center">
                                <tr>
                                    <td class="detailsframe">
                                        <div class="scrollsingleframe">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center" style="margin-bottom: 10px;">
                                                <tr>
                                                    <td colspan="4" align="left">
                                                        <asp:Label ID="lblEnrollmentId"
                                                            runat="server">Enrollment</asp:Label>&nbsp;&nbsp;&nbsp;
                                               
                                                    <asp:DropDownList ID="ddlEnrollmentId" runat="server" CssClass="dropdownlistno100"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList></td>
                                                    <td>
                                                        <asp:Button ID="btnPrint" runat="server" Text="Print Preview"></asp:Button></td>
                                                </tr>
                                            </table>
                                            <div id="print_area" runat="server">
                                                <!-- begin table content -->

                                                <asp:Panel ID="pnlStudentInfo" runat="server" Visible="true">
                                                    <table cellpadding="0" cellspacing="0" width="60%" align="center">
                                                        <tr>
                                                            <td style="width: 80%; padding: 16px">
                                                                <fieldset >
                                                                    <legend >Student Information</legend>
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td class="contentcell" nowrap>
                                                                                <asp:Label ID="lblStudent" runat="Server">Student:</asp:Label>
                                                                            </td>
                                                                            <td class="contentcell4" nowrap>
                                                                                <asp:Label ID="lblStudentName" runat="server">
                                                                                </asp:Label>
                                                                            </td>
                                                                            <td></td>
                                                                            <td class="contentcell" nowrap>
                                                                                <asp:Label ID="lblEnrollment1" runat="Server">Enrollment:</asp:Label>
                                                                            </td>
                                                                            <td class="contentcell4" nowrap>
                                                                                <asp:TextBox ID="lblEnrollmentName" runat="server" CssClass="textbox"
                                                                                    contentEditable="False"></asp:TextBox>
                                                                            </td>
                                                                            <td></td>
                                                                            <td class="contentcell" nowrap>
                                                                                <asp:Label ID="lblStatus" runat="Server" >Enrollment Status:</asp:Label>
                                                                            </td>
                                                                            <td class="contentcell4" nowrap>
                                                                                <asp:Label ID="lblEnrollmentStatus" runat="Server"
                                                                                    ></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="contentcell" nowrap>
                                                                                <asp:Label ID="lblStudentStartDate" runat="Server"
                                                                                    >Start Date:</asp:Label>
                                                                            </td>
                                                                            <td class="contentcell4" nowrap>
                                                                                <asp:Label ID="lblStartDate" runat="Server" CssClass="textbox"
                                                                                    ></asp:Label>
                                                                            </td>
                                                                            <td></td>
                                                                            <td class="contentcell" nowrap>
                                                                                <asp:Label ID="lblGradDate" runat="Server"
                                                                                   >Graduation Date:</asp:Label>
                                                                            </td>
                                                                            <td class="contentcell4" nowrap>
                                                                                <asp:Label ID="lblGD" runat="Server" ></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td style="width: 100%; vertical-align: top;">
                                                            <table border="0px">
                                                                <tr>
                                                                    <td class="contentcell" align="center">
                                                                        <asp:Panel ID="rightPanel" runat="server" Width="375px" Visible="false">
                                                                            <table border="0px">
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lName" runat="server" CssClass="label">Name</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rName" runat="server" CssClass="textbox"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lAverage" runat="server" CssClass="label">Average</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rAverage" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lScore" runat="server" CssClass="label">Score</asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="rScore" runat="server" CssClass="textbox" Width="50px"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lPostDate" runat="server" CssClass="label">Post Date</asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="rPostDate" runat="server" CssClass="textbox" Width="65px"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lRequiredHours" runat="server" CssClass="label">Required</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rRequiredHours" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lCompletedHours" runat="server" CssClass="label">Completed</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rCompletedHours" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lRemainingHours" runat="server" CssClass="label">Remaining</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rRemainingHours" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lRequirementsMet" runat="server" CssClass="label">Requirements Met?</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rRequirementsMet" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lDate" runat="server" CssClass="label">Date</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rDate" runat="server" CssClass="textbox" Width="65px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lDates" runat="server" CssClass="label">Dates</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rDates1" runat="server" CssClass="textbox" Width="65px"></asp:Label><asp:Label ID="lTo" runat="server" Visible="false" Style="font: normal 11px verdana; color: #000066; background-color: transparent; padding: 2px; vertical-align: middle"> To </asp:Label>
                                                                                        <asp:Label ID="rDates2" runat="server" CssClass="textbox" Width="65px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lPassingScore" runat="server" CssClass="label">Passing Score</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rPassingScore" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lWeight" runat="server" CssClass="label">Weight</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rWeight" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lRequired" runat="server" CssClass="label">Required</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rRequired" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="contentcell">
                                                                                        <asp:Label ID="lMustPass" runat="server" CssClass="label">Must pass</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:Label ID="rMustPass" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                </tr>
                                                                                <asp:Panel ID="CreditsPanel" runat="server">
                                                                                    <tr>
                                                                                        <td class="contentcell">
                                                                                            <asp:Label ID="label21" runat="server" CssClass="label"></asp:Label></td>
                                                                                        <td>
                                                                                            <asp:Label ID="label22" runat="server" CssClass="label">Academic   &nbsp;&nbsp; Financial Aid</asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="contentcell">
                                                                                            <asp:Label ID="lCredits" runat="server" CssClass="label">Credits</asp:Label></td>
                                                                                        <td>
                                                                                            <asp:Label ID="r1Credits" runat="server" CssClass="textbox" Width="50px"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                           
                                                                                        <asp:Label ID="r2Credits" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="contentcell">
                                                                                            <asp:Label ID="lAttempted" runat="server" CssClass="label">Attempted</asp:Label></td>
                                                                                        <td>
                                                                                            <asp:Label ID="r1Attempted" runat="server" CssClass="textbox" Width="50px"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                           
                                                                                        <asp:Label ID="r2Attempted" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="contentcell">
                                                                                            <asp:Label ID="lEarned" runat="server" CssClass="label">Earned</asp:Label></td>
                                                                                        <td>
                                                                                            <asp:Label ID="r1Earned" runat="server" CssClass="textbox" Width="50px"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                           
                                                                                        <asp:Label ID="r2Earned" runat="server" CssClass="textbox" Width="50px"></asp:Label></td>
                                                                                    </tr>
                                                                                </asp:Panel>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="contentcell"></td>
                                                    </tr>
                                                </table>
                                                <!--end table content-->
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>



