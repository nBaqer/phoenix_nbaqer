﻿<%@ Page Title="Copy Class Sections" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="CopyClassSections.aspx.vb" Inherits="CopyClsSections" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">

            <telerik:RadWindowManager ID="ScheduleConflictWindow" runat="server" Behaviors="Default" InitialBehaviors="None" Visible="false">
                <Windows>
                    <telerik:RadWindow ID="DialogWindow" Behaviors="Close" VisibleStatusbar="false"
                        ReloadOnShow="true" Modal="true" runat="server" Height="400px" Width="700px"
                        VisibleOnPageLoad="false" Top="20" Left="20">
                    </telerik:RadWindow>
                </Windows>
            </telerik:RadWindowManager>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <table width="40%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="Label1" runat="server" CssClass="label">From Term<font color="red">*</font></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" style="width: 68%">
                                                    <asp:DropDownList ID="ddlTerm1" runat="server" CssClass="dropdownlist" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="Label2" runat="server" CssClass="label"> To Term<font color="red">*</font></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" style="width: 68%">
                                                    <asp:DropDownList ID="ddlTerm2" runat="server" CssClass="dropdownlist" AutoPostBack="True" Enabled="False">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell"></td>
                                                <td class="twocolumncontentcell" align="left" style="width: 68%">
                                                    <asp:CheckBox ID="chkIgnoreConflicts" runat="server" Text='Ignore Conflicts' CssClass="label" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell"></td>
                                                <td class="twocolumncontentcell" style="width: 68%">
                                                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="Label">
                                                    <asp:Label ID="lblNote" runat="server" Visible="false">
                                                <b>* Please note that the class sections meeting dates will be  set to new term's start and end date.You may need to adjust these dates in the new term.You also need to assign the student's start date to all the new class sections.</b>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dgdCopyClsSect" runat="server" Width="100%" AutoGenerateColumns="False"
                                                        BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                        <FooterStyle CssClass="DataGridItemStyle"></FooterStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Term">
                                                                <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                                
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtTermId" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("TermId") %>' />
                                                                    <asp:TextBox ID="txtMaxStud" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("MaxStud") %>' />
                                                                    <asp:TextBox ID="txtStartDate" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("StartDate") %>' />
                                                                    <asp:TextBox ID="txtEndDate" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("EndDate") %>' />
                                                                    <asp:Label ID="Term" runat="server" CssClass="Label" Text='<%# Container.DataItem("TermDescrip")  %>' />
                                                                    <asp:CheckBox ID="chkAllowEarlyIn" runat="server" CssClass="checkbox" Visible="false" Checked='<%# Container.DataItem("AllowEarlyIn") %>' />
                                                                    <asp:CheckBox ID="chkAllowLateOut" runat="server" CssClass="checkbox" Visible="false" Checked='<%# Container.DataItem("AllowLateOut") %>' />
                                                                    <asp:CheckBox ID="chkAllowExtraHours" runat="server" CssClass="checkbox" Visible="false" Checked='<%# Container.DataItem("AllowExtraHours") %>' />
                                                                    <asp:CheckBox ID="chkCheckTardyIn" runat="server" CssClass="checkbox" Visible="false" Checked='<%# Container.DataItem("CheckTardyIn") %>' />
                                                                    <asp:TextBox ID="txtMaxInBeforeTardy" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("MaxInBeforeTardy") %>' />
                                                                    <asp:TextBox ID="txtAssignTardyInTime" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("AssignTardyInTime") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Course Code">
                                                                <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                                
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CourseCode" CssClass="Label" runat="server" Text='<%# Container.DataItem("Code")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Section">
                                                                <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                                
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtClsSectionId" CssClass="Label" runat="server" Visible="False"
                                                                        Text='<%# Container.DataItem("ClsSectionId") %>' />
                                                                    <asp:Label ID="lblClsSection" CssClass="Label" runat="server" Text='<%# Container.DataItem("ClsSection") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Course Description">
                                                                <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                                
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCourseId" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("ReqId") %>' />
                                                                    <asp:Label ID="Label3" CssClass="Label" runat="server" Text='<%# Container.DataItem("Descrip") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Instructor">
                                                                <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                                
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtEmpId" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("InstructorId") %>' />
                                                                    <asp:Label ID="Label4" CssClass="Label" runat="server" Text='<%# Container.DataItem("FullName") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Grade Scale" Visible="False">
                                                                <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                                
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtGrdScaleId" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("GrdScaleId") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Shift" Visible="False">
                                                                <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                                
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtShiftId" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("ShiftId") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Unschedule">
                                                                <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                                
                                                                <HeaderTemplate>
                                                                    <!--<asp:Label Id="Label1" cssclass = "Label" Runat="server" Text='Copy' /><br>-->
                                                                    <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkCopy',
																	 document.forms[0].chkAllItems.checked)" />Check All
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkCopy" runat="server" Text='' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--end table content-->
                                    </div>
                                    <asp:TextBox ID="txtClsSectionId1" runat="server" CssClass="Label" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>


            <!--end table content-->

            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator">
            </asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
        </telerik:RadPane>
    </telerik:RadSplitter>


</asp:Content>
