﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="RegisterStdsManually.aspx.vb" Inherits="AdvWeb.AR.ArRegisterStdsManually" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <script src="../js/CheckAll.js" type="text/javascript"></script>

    <%-- ReSharper disable Asp.Entity --%>
    <%-- 
		 <script language="JavaScript" type="text/JavaScript">
		  function openwin() {

			  var campusid = "<%=campusId%>";
			  var param1 = "<%=txtStudentName.clientID%>";
			  var param2 = "<%=txtStuEnrollmentId.clientID%>";
			  var param3 = "<%=txtStuEnrollment.clientID%>";
			  var param4 = "<%=txtTermId.clientID%>";
			  var param5 = "<%=txtTerm.clientID%>";
			  var param6 = "<%=txtAcademicYearId.clientID%>";
			  var param7 = "<%=txtAcademicYear.clientID%>";
			  var userid = "<%=userId%>";

			  var winSettings = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px";
			  var name = "StudentPopupSearch2";

			  theURL = "../SY/StudentPopupSearch2.aspx?resid=357&mod=SA&cmpid=" + campusid;
			  theURL += "&" + param1 + "=" + "&" + param2 + "=" + "&" + param3 + "=" + "&" + param4 + "=" + "&" + param5 + "=" + "&" + param6 + "=" + "&" + param7 + "=" + "&userid=" + userid;
			  window.open(theURL, name, winSettings);
		  }
	</script>
    --%>
    <%-- ReSharper restore Asp.Entity --%>
    <style type="text/css">
        div.RadWindow_Web20 a.rwCloseButton {
            margin-right: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="LeftMenu">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="LeftMenu" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dtlClsSectList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>


        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table id="Table1" class="Table100">
                <!-- begin rightcolumn -->
                <tr style="width: 100%">
                    <td class="detailsframetop" style="height: 61px">
                        <table id="Table4" class="Table100">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" style="text-align: right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                        ID="btnNew" runat="server" CssClass="new" Text="New" Enabled="false" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                                            CausesValidation="False" Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable">
                            <tr>
                                <td class="detailsframe">
                                    <div">
                                        <FAME:StudentSearch ID="StudSearch1" runat="server" ShowTerm="false" ShowAcaYr="false" OnTransferToParent="TransferToParent" />
                                        <!-- begin table content-->
                                        <asp:Panel ID="pnlRHS" runat="server" CssClass="boxContainer">
                                            <h3><%=Header.Title  %></h3>
                                            <table class="contenttable" style="padding: 0; width: 40%; min-width: 400px; text-align: center;">
                                                <asp:Panel ID="pnlDate" runat="server" Visible="True">
                                                    <tr>
                                                        <td class="contentcell4search">

                                                            <div id="StudSearch">

                                                                <asp:LinkButton ID="lbtHasDisb"
                                                                    runat="server" Width="0px">
                                                                </asp:LinkButton>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td class="threecolumnheaderreg">
                                                            <asp:Label ID="lblAvailStuds" runat="server" CssClass="label" Font-Bold="True">Terms</asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="threecolumncontentreg" style="text-align: center">
                                                            <asp:ListBox ID="lbxTerms" runat="server" Height="140px" SelectionMode="Single"
                                                                CssClass="listboxes"></asp:ListBox>
                                                        </td>

                                                    </tr>
                                                    <tr>

                                                        <td class="threecolumncontentreg" style="text-align: left">
                                                            <asp:CheckBox ID="chkReqs" runat="server" Text="Bypass Prerequisites" CssClass="label" />
                                                            &nbsp;
                                                        </td>

                                                    </tr>
                                                    <tr style="visibility: hidden">

                                                        <td class="threecolumncontentreg" style="text-align: left">
                                                            <asp:CheckBox ID="ChkAllCourses" runat="server" Text='View All Class Sections' CssClass="label" />
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell4" style="text-align: right">
                                                            <asp:Button ID="btnGetStdCourses" runat="server" Text="Get Student Courses"
                                                                CausesValidation="False"></asp:Button>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trNoData" visible="false">
                                                        <td class="contentcell4" style="text-align: center; padding-left: 2em">
                                                            <asp:Label runat="server" ID="lblNoData"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </asp:Panel>
                                            </table>
                                            <table class="contenttable, Table100" style="text-align: center">
                                                <asp:Panel ID="pnlGrid" runat="server">
                                                    <tr>
                                                        <td style="padding-top: 10px">
                                                            <asp:DataGrid ID="dgdTransferGrade" runat="server" BorderColor="#ebebeb" BorderStyle="Solid"
                                                                BorderWidth="1px" Width="100%" AutoGenerateColumns="False">
                                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Select">
                                                                        <HeaderStyle CssClass="datagridheader" Width="5%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <HeaderTemplate>
                                                                            <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkDrop',
        document.forms[0].chkAllItems.checked)" />
                                                                            <asp:Label ID="label2" runat="server" Text='Select' />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkDrop" runat="server" Text='' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Campus">
                                                                        <HeaderStyle CssClass="datagridheader" Width="12%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCampus" runat="server" Text='<%# Container.DataItem("CampDescrip")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Term">
                                                                        <HeaderStyle CssClass="datagridheader" Width="12%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTerm" runat="server" Text='<%# Container.DataItem("TermDescrip")%>' />
                                                                            <asp:Label ID="lblTermid" runat="server" Visible="false" Text='<%# Container.DataItem("TermId")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Code">
                                                                        <HeaderStyle CssClass="datagridheader" Width="5%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Code" runat="server" Text='<%# Container.DataItem("Code")  %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Course Description">
                                                                        <HeaderStyle CssClass="datagridheader" Width="12%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Descrip" runat="server" Text='<%# Container.DataItem("Descrip")%>' />
                                                                            <asp:Label ID="lblReqId" runat="server" Visible="false" Text='<%# Container.DataItem("ReqId")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Credits">
                                                                        <HeaderStyle CssClass="datagridheader" Width="4%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCredits" CssClass="label" Text='<%# Container.DataItem("Credits") %>'
                                                                                runat="server"> </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Hours">
                                                                        <HeaderStyle CssClass="datagridheader" Width="4%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblHours" CssClass="label" Text='<%# Container.DataItem("Hours") %>'
                                                                                runat="server"> </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Class Section">
                                                                        <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Section" runat="server" Text='<%# Container.DataItem("ClsSection")%>' />
                                                                            <asp:TextBox ID="txtClsSectionId" runat="server" CssClass="ardatalistcontent" Visible="False"
                                                                                Text='<%# Container.DataItem("ClsSectionId") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Shift">
                                                                        <HeaderStyle CssClass="datagridheader" Width="5%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Shift" runat="server" Text='<%# Container.DataItem("Shift")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Eligible for Registration?">
                                                                        <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRegistration" runat="server" CssClass="label" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Period - Class Room">
                                                                        <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                                                        <HeaderTemplate>
                                                                            <div align="left" style="float: left">
                                                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" />
                                                                            </div>
                                                                            <div align="right" style="float: right">
                                                                                <asp:Label ID="lblClassRoom" runat="server" Text="Classroom" />
                                                                            </div>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:DataGrid ID="dgdPeriodClassRoom" runat="server" AllowPaging="False"
                                                                                CellPadding="3" CellSpacing="2"
                                                                                BorderStyle="None"
                                                                                BorderWidth="0px"
                                                                                Width="100%" AutoGenerateColumns="False"
                                                                                AllowSorting="False" ShowHeader="False">
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="PeriodDescrip" ItemStyle-BorderWidth="0px" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="RoomDescrip" ItemStyle-BorderWidth="0px" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Instructor">
                                                                        <HeaderStyle CssClass="datagridheader" Width="12%"></HeaderStyle>
                                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Instructor" runat="server" Text='<%# Container.DataItem("Instructor")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell4">
                                                            <asp:HiddenField ID="txtStuEnrollment" runat="server" />
                                                            <asp:HiddenField ID="txtStuEnrollmentId" runat="server" />
                                                            <asp:HiddenField ID="txtAcademicYear" runat="server" />
                                                            <asp:TextBox ID="txtAcademicYearId" runat="server" Visible="false" />
                                                            <asp:HiddenField ID="txtTerm" runat="server" />
                                                            <asp:HiddenField ID="txtTermId" runat="server" />
                                                        </td>
                                                    </tr>
                                                </asp:Panel>
                                            </table>
                                        </asp:Panel>
                                        <!--end table content-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                    runat="server">
                </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server"
                ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
            <!--end validation panel-->

        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadWindowManager ID="ScheduleConflictWindow" runat="server" Behaviors="Default" InitialBehaviors="None" Visible="false">
        <Windows>
            <telerik:RadWindow ID="DialogWindow" Behaviors="Close" VisibleStatusbar="false"
                ReloadOnShow="true" Modal="true" runat="server" Height="400px" Width="700px"
                VisibleOnPageLoad="false" Top="0" Left="20">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Title="Schedule Conflicts in Same Class" Font-Bold="true" ForeColor="Red">
    </telerik:RadWindowManager>
</asp:Content>

