﻿
Imports System.Collections
Imports System.Data
Imports System.Xml
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.Common.Tables
Imports BO = Advantage.Business.Objects

Namespace AdvWeb.AR

    Partial Class TransferGrades
        Inherits BasePage
        Private pObj As New UserPagePermissionInfo

        Protected WithEvents Label5 As Label
        Protected WithEvents DDLTermId As DropDownList
        Protected CampusId, UserId As String
        Protected MyAdvAppSettings As AdvAppSettings
        Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme
        End Sub

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

            MyAdvAppSettings = AdvAppSettings.GetAppSettings()

            Dim resourceId As Integer
            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            CampusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
            UserId = AdvantageSession.UserState.UserId.ToString


            Dim advantageUserState As New BO.User()
            advantageUserState = AdvantageSession.UserState

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
            'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If

            'Attention to processing window information for process Transfer or Graded.
            If (IsPostBack) Then
                Dim parameter As String = Request("__EVENTARGUMENT")
                Dim control As String = Request("__EVENTTARGET")
                If (control.ToString() = "RadTransferPending") Then

                    ' Hidden windows.....
                    RadWindowManager1.Visible = False
                    RadWindowManager1.Windows(0).VisibleOnPageLoad = False

                    'See if you need to execute some actions
                    Dim param As String() = Split(parameter, ",")
                    If (param(0) = "transfer") Then


                        ' See if we need to register transfer special case
                        If (param(1).ToLower() = "true") Then
                            Dim list As IList(Of ArTransferGrades) = AdvantageSession.TpTransferByCopyList
                            TransferGradesBo.TransferEnrollmentsByCopyOne(list)
                        End If

                        'See if we need to register special case
                        If (param(2).ToLower() = "true") Then
                            Dim list As IList(Of ArResultsClass) = AdvantageSession.TpRegisterByCopyList
                            TransferGradesBo.RegisterEnrollmentsByCopyOne(list)
                        End If

                    End If

                    'Reload Grid.
                    btnGetStdCourses_Click(Nothing, Nothing)

                    'If exists Values in weird list scan the grid to disable its combo box
                    Dim weirds As List(Of TransferRequirement) = CType(AdvantageSession.TpWeirdThingList, List(Of TransferRequirement))
                    For Each weird As TransferRequirement In weirds

                        For Each item As DataGridItem In dgdTransferGrade.Items
                            Dim code As String = DirectCast(item.Cells(0).Controls.Item(1), Label).Text
                            If code = weird.ReqCode Then
                                item.Cells(5).Enabled = False
                                item.Cells(6).Enabled = False
                            End If

                        Next
                    Next

                    'Clean Session
                    AdvantageSession.TpTransferByCopyList = Nothing
                    AdvantageSession.TpRegisterByCopyList = Nothing
                    AdvantageSession.TpReadyToTransferList = Nothing
                    AdvantageSession.TpWeirdThingList = Nothing
                End If
            End If

        End Sub

        Private Sub btnGetStdCourses_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetStdCourses.Click
            Dim fac As New TranscriptFacade
            Dim dt As DataTable

            If Session("StudentName") = "" Then
                btnSave.Enabled = False
                DisplayErrorMessage("Unable to find data. Student field cannot be empty.")
                Exit Sub
            Else
                If pObj.HasFull Or pObj.HasAdd Then
                    btnSave.Enabled = True
                Else
                    btnSave.Enabled = False
                End If
            End If

            ViewState("StuEnrollId") = txtStuEnrollmentId.Text
            ViewState("TermsDS") = Nothing

            dt = fac.GetCoursesWithoutPassingGradeForStudentEnrollment(txtStuEnrollmentId.Text, True)
            Dim dv As New DataView(dt, "Grd = '1'", "Code", DataViewRowState.CurrentRows)
            With dgdTransferGrade
                .DataSource = dv
                .DataBind()
            End With

        End Sub

        Private Sub BuildGradesDDL(ByVal ddl As DropDownList)

            Dim ddlDS As DataSet
            '   save TuitionCategories in the View state for subsequent use
            If ViewState("GradesDS") Is Nothing Then
                ddlDS = (New TransferGradesFacade).GetGrades(CType(ViewState("StuEnrollId"), String))
                ViewState("GradesDS") = ddlDS
            Else
                ddlDS = CType(ViewState("GradesDS"), DataSet)
            End If

            '   bind the TuitionCategories DDL
            With ddl
                .DataTextField = "Grade"
                .DataValueField = "GrdSysDetailId"
                .DataSource = ddlDS
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                '.SelectedIndex = 0
            End With

        End Sub

        Private Sub BuildTermsDDL(ByVal stuEnrollId As String, ByVal ddl As DropDownList)
            Dim ddlDS As DataSet

            '   save TuitionCategories in the View state for subsequent use
            If ViewState("TermsDS") Is Nothing Then

                ''The term drop down list should populate the terms belonging to the students prgVersion and those mapped to all.
                ddlDS = (New RegFacade).GetTermsFromStudentsProgramandStartDate(stuEnrollId, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
                ViewState("TermsDS") = ddlDS

            Else

                ddlDS = CType(ViewState("TermsDS"), DataSet)
            End If

            '   bind the Terms DDL
            With ddl
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = ddlDS
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                '.SelectedIndex = 0
            End With

        End Sub


        Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, _
                                  ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, _
                                  ByVal hourtype As String)


            txtStuEnrollmentId.Text = enrollid
            Session("StuEnrollment") = txtStuEnrollmentId.Text
            Session("StudentName") = fullname

        End Sub
        Protected Sub dgdTransferGrade_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgdTransferGrade.ItemCreated
            Dim oControl As Control
            If MyAdvAppSettings Is Nothing Then
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If
            End If
            For Each oControl In dgdTransferGrade.Controls(0).Controls
                If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        'CType(oControl, DataGridItem).Cells(5).Visible = False
                        CType(oControl, DataGridItem).Cells(5).HorizontalAlign = HorizontalAlign.Center
                        CType(oControl, DataGridItem).Cells(5).Text = "Transfer Grade?"
                    Else
                        CType(oControl, DataGridItem).Cells(4).Visible = False
                    End If
                End If
                If (CType(oControl, DataGridItem).ItemType = ListItemType.Item Or _
                    CType(oControl, DataGridItem).ItemType = ListItemType.AlternatingItem) Then
                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        'CType(oControl, DataGridItem).Cells(5).Visible = False
                        CType(oControl, DataGridItem).Cells(5).HorizontalAlign = HorizontalAlign.Center

                    Else
                        CType(oControl, DataGridItem).Cells(4).Visible = False
                    End If
                End If
            Next
        End Sub

        Private Sub dgdTransferGrade_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgdTransferGrade.ItemDataBound
            '  Dim i As Integer
            Dim ddl As DropDownList
            Dim chk As CheckBox
            Dim ddl2 As DropDownList
            Dim count As Integer
            Dim stuEnrollId As String = txtStuEnrollmentId.Text
            count = CInt(ViewState("Count"))

            If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                Select Case e.Item.ItemType
                    Case ListItemType.Item, ListItemType.AlternatingItem
                        count += 1
                        ViewState("Count") = count.ToString
                        ddl = CType(e.Item.FindControl("ddlGrade"), DropDownList)
                        ddl.Visible = False
                        chk = CType(e.Item.FindControl("chkGrade"), CheckBox)
                        chk.Visible = True
                        ddl2 = CType(e.Item.FindControl("ddlTerm"), DropDownList)
                        BuildGradesDDL(ddl)
                        BuildTermsDDL(stuEnrollId, ddl2)
                End Select
            Else

                Select Case e.Item.ItemType
                    Case ListItemType.Item, ListItemType.AlternatingItem
                        count += 1
                        ViewState("Count") = count.ToString
                        ddl = CType(e.Item.FindControl("ddlGrade"), DropDownList)
                        chk = CType(e.Item.FindControl("chkGrade"), CheckBox)
                        chk.Visible = False
                        ddl2 = CType(e.Item.FindControl("ddlTerm"), DropDownList)
                        BuildGradesDDL(ddl)
                        BuildTermsDDL(stuEnrollId, ddl2)
                End Select
            End If
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            '   if GrdSystem is nothing do an insert. Else do an update
            'Dim iitems As DataGridItemCollection
            'Dim iitem As DataGridItem
            'Dim i As Integer
            'Dim sId As String
            'Dim sGrade As String
            'Dim chkGrd As Boolean = False
            'Dim dt As DataTable
            'Dim finalGradeObject As FinalGradeInfo = Nothing
            'Dim updateERR As String = ""
            'Dim facade As New TransferGradesFacade
            'Dim termId As String
            'Dim reqId As String
            'Dim fac As New TranscriptFacade
            'Dim decScore As Decimal
            'Dim errMsg As String = ""
            'Dim useTimeClock As Boolean = False
            'Dim dtfacade As New GetDateFromHoursBR
            'Dim dtExpectedDate As DateTime
            'Dim upDateExpGradDatefacade As New StudentEnrollmentFacade
            'Dim passingScore As Decimal
            Dim errStr As String

            Try


                'Save all data grid to a list
                Dim listToTransfer As List(Of TransferRequirement) = New List(Of TransferRequirement)()
                Dim isNumeric As Boolean = (MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric")

                For Each item As DataGridItem In dgdTransferGrade.Items
                    Dim transfer As TransferRequirement = New TransferRequirement()
                    transfer.Grade = CType(item.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString
                    transfer.ReqCode = CType(item.FindControl("Code"), Label).Text
                    transfer.CourseDescription = CType(item.FindControl("Descrip"), Label).Text

                    transfer.TermId = CType(item.FindControl("ddlTerm"), DropDownList).SelectedItem.Value.ToString()
                    If (String.IsNullOrEmpty(transfer.Grade) = False Or (String.IsNullOrEmpty(transfer.TermId) = False)) Then
                        transfer.IsChecked = CType(item.FindControl("ChkGrade"), CheckBox).Checked
                        Decimal.TryParse(CType(item.FindControl("txtScore"), TextBox).Text, transfer.Score)
                        transfer.ReqId = CType(item.FindControl("txtReqId"), TextBox).Text
                        transfer.ModName = AdvantageSession.UserState.UserName
                        transfer.StuEnrollId = txtStuEnrollmentId.Text
                        listToTransfer.Add(transfer)
                    End If
                Next

                'Begin validations
                If listToTransfer.Count = 0 Then
                    DisplayErrorMessage("No item was selected")
                    Return
                End If

                'Validate conditions
                errStr = TransferGradesBo.ValidateUserInput(listToTransfer, isNumeric)

                ' Check for validation messages
                If (String.IsNullOrEmpty(errStr) = False) Then
                    DisplayErrorMessage(errStr)
                    Return
                End If

                'Classifying Cases
                Dim bo = New TransferGradesBo(listToTransfer)

                'Store values in session
                AdvantageSession.TpReadyToTransferList = bo.ListReadyToTransferGrade
                AdvantageSession.TpRegisterByCopyList = bo.ListOfRegisterCopyOne
                AdvantageSession.TpTransferByCopyList = bo.ListTransferByCopyOne
                AdvantageSession.TpWeirdThingList = bo.ListOfRareSituation

                ' Transfer requirements that haven't problem.
                'Troy: 7/1/2016 DE12863. Director or Academics or SA should be able to modify academics for an out of school status.
                'We therefore need to override the default behavior if the user is Director of Academics or SA.
                Dim usFacade As New UserSecurityFacade

                If usFacade.DoesUserBelongToRoleForCampus(UserId, 13, CampusId) Or usFacade.DoesUserBelongToRoleForCampus(UserId, 1, CampusId) Then
                    bo.TransferRequirements(True)
                Else
                    bo.TransferRequirements(False)
                End If

                'Test if exists some weird situations
                'First situation: the requirement(s) was/where transferred in only one enrollment for the student
                If bo.ListTransferByCopyOne.Count > 0 Then
                    MessagePopUpTransferPendingDialog()
                    Return
                End If

                'Register in the case that one of then was registered and graded
                If bo.ListOfRegisterCopyOne.Count > 0 Then
                    MessagePopUpTransferPendingDialog()
                    Return
                End If

                'Show the messages in the case of rare situation
                If bo.ListOfRareSituation.Count > 0 Then
                    MessagePopUpTransferPendingDialog()
                    Return
                End If

                DisplayInfoMessage("Transfer grade(s) has been posted successfully for this student")

                'Reload Grid.
                btnGetStdCourses_Click(Nothing, Nothing)

                'Clean Session
                AdvantageSession.TpTransferByCopyList = Nothing
                AdvantageSession.TpRegisterByCopyList = Nothing
                AdvantageSession.TpReadyToTransferList = Nothing
                AdvantageSession.TpWeirdThingList = Nothing

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try




            '' Save the data grid items in a collection.
            'iitems = dgdTransferGrade.Items
            'Try
            '    sId = txtStuEnrollmentId.Text
            '    'Loop through the collection to retrieve the StudentId,score & Comments.

            '    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
            '        passingScore = (New StuProgressReportObject).GetMinPassingScore_SP()
            '    End If
            '    For i = 0 To iitems.Count - 1
            '        iitem = iitems.Item(i)
            '        If CType(iitem.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString <> "" Or CType(iitem.FindControl("ddlTerm"), DropDownList).SelectedItem.Value <> "" Then
            '            Try
            '                termId = CType(iitem.FindControl("ddlTerm"), DropDownList).SelectedItem.Value
            '                If termId = "" Then
            '                    errStr &= "Please select a term for Row " & i + 1 & vbCrLf
            '                    Exit Try
            '                End If
            '                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
            '                    Try
            '                        decScore = CType(CType(iitem.FindControl("txtScore"), TextBox).Text, Decimal)
            '                    Catch ex As Exception
             '                    	Dim exTracker = new AdvApplicationInsightsInitializer()
            '                    	exTracker.TrackExceptionWrapper(ex)

            '                        decScore = 0
            '                    End Try
            '                    If termId <> "" Then
            '                        chkGrd = CType(iitem.FindControl("ChkGrade"), CheckBox).Checked
            '                    End If

            '                    If (decScore = 0 And chkGrd = False) Then
            '                        errStr &= "Please enter a score/check transfer grade check box for Row " & i + 1 & vbCrLf
            '                        Exit Try
            '                    End If

            '                    If chkGrd = True And decScore <> 0 Then
            '                        If chkGrd Then
            '                            If decScore < passingScore Then
            '                                errStr &= "For transfer grades,please enter a passing score for Row " & i + 1 & vbCrLf
            '                                Exit Try
            '                            End If
            '                        End If
            '                    End If
            '                Else
            '                    If CType(iitem.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString = "" Then
            '                        errStr &= "Please enter a grade for Row " & i + 1 & vbCrLf
            '                        Exit Try
            '                    End If

            '                End If


            '                reqId = CType(iitem.FindControl("txtReqId"), TextBox).Text
            '                sGrade = CType(iitem.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString
            '                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
            '                    If termId <> "" Then
            '                        If chkGrd Then
            '                        End If

            '                        finalGradeObject = PopulateFinalGrdObject(sId, sGrade, reqId, termId, decScore)
            '                        updateERR &= CType(facade.UpdateTransferGrade(finalGradeObject, AdvantageSession.UserState.UserName), String)

            '                        If updateERR = "" Then
            '                            If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
            '                                dtExpectedDate = dtfacade.GetDateFromHours(finalGradeObject.StuEnrollId, errMsg, useTimeClock, campusId)
            '                                If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And useTimeClock = True Then
            '                                    upDateExpGradDatefacade.UpdateExpectedGraduationDate(dtExpectedDate, finalGradeObject.StuEnrollId)
            '                                End If
            '                            End If
            '                        End If

            '                    End If
            '                Else
            '                    sGrade = CType(iitem.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString
            '                    If sGrade <> "" And termId <> "" Then
            '                        finalGradeObject = PopulateFinalGrdObject(sId, sGrade, reqId, termId, decScore)
            '                        updateERR &= CType(facade.UpdateTransferGrade(finalGradeObject, AdvantageSession.UserState.UserName), String)

            '                        If updateERR = "" Then
            '                            If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
            '                                dtExpectedDate = dtfacade.GetDateFromHours(finalGradeObject.StuEnrollId, errMsg, useTimeClock, campusId)
            '                                If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And useTimeClock = True Then
            '                                    upDateExpGradDatefacade.UpdateExpectedGraduationDate(dtExpectedDate, finalGradeObject.StuEnrollId)
            '                                End If
            '                            End If
            '                        End If

            '                    End If
            '                End If
            '            Catch ex As Exception
             '            	Dim exTracker = new AdvApplicationInsightsInitializer()
            '            	exTracker.TrackExceptionWrapper(ex)

            '            End Try
            '        End If
            '    Next
            '    If updateERR = "" Then
            '        If Not finalGradeObject Is Nothing Then
            '            If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
            '                If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And useTimeClock = True Then
            '                    If errStr = "" Then
            '                        DisplayErrorMessage(CType(("Transfer grade(s) has been posted successfully for this student" + IIf(Not dtExpectedDate = Nothing, vbCrLf + "Revised Expected Graduation Date is " + dtExpectedDate.ToString("MM/dd/yyyy"), "")), String))
            '                    Else
            '                        DisplayErrorMessage(CType(("Some Transfer grade(s) has been posted successfully for this student" + IIf(Not dtExpectedDate = Nothing, vbCrLf + "Revised Expected Graduation Date is " + dtExpectedDate.ToString("MM/dd/yyyy") & vbCrLf & "Some are missing either a term or grade or score or passing score for true transfer", "")), String))
            '                    End If

            '                Else
            '                    If errStr = "" Then
            '                        DisplayErrorMessage("Transfer grade(s) has been posted successfully for this student" & vbCrLf & errMsg)
            '                    Else
            '                        DisplayErrorMessage("Some Transfer grade(s) has been posted successfully for this student" & vbCrLf & errMsg & vbCrLf & "Some are missing either a term or grade or score or passing score for true transfer")
            '                    End If

            '                End If
            '            Else
            '                If errStr = "" Then
            '                    DisplayErrorMessage("Transfer grade(s) has been posted successfully for this student")
            '                Else
            '                    DisplayErrorMessage("Some Transfer grade(s) has been posted successfully for this student" & vbCrLf & "Some are missing either a term or grade or score or passing score for true transfer")
            '                End If
            '            End If
            '            dt = fac.GetCoursesWithoutPassingGradeForStudentEnrollment(txtStuEnrollmentId.Text, True)
            '            Dim dv As New DataView(dt, "Grd = '1'", "Code", DataViewRowState.CurrentRows)
            '            With dgdTransferGrade
            '                .DataSource = dv
            '                .DataBind()
            '            End With
            '        End If

            '    Else
            '        DisplayErrorMessage(updateERR)
            '    End If

            '    If errStr <> "" Then
            '        DisplayErrorMessage(errStr)
            '        Exit Sub
            '    End If

            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    'Redirect to error page.
            '    If ex.InnerException Is Nothing Then
            '        Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            '    Else
            '        Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            '    End If
            '    Response.Redirect("../ErrorPage.aspx")
            'End Try

        End Sub
        'Private Function PopulateFinalGrdObject(ByVal StuEnrollId As String, ByVal Grade As String, ByVal ReqId As String, ByVal Termid As String, ByVal score As Decimal) As FinalGradeInfo
        '    Dim GrdPostingObj As New FinalGradeInfo

        '    GrdPostingObj.ReqId = ReqId
        '    GrdPostingObj.TermId = Termid
        '    GrdPostingObj.StuEnrollId = StuEnrollId
        '    GrdPostingObj.Grade = Grade
        '    GrdPostingObj.Score = score
        '    GrdPostingObj.ModDate = DateTime.Now

        '    Return GrdPostingObj
        'End Function
        Private Sub DisplayErrorMessage(ByVal errorMessage As String)

            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End Sub
          Private Sub DisplayInfoMessage(ByVal errorMessage As String)

            '   Display error in message box in the client
            CommonWebUtilities.DisplayInfoInMessageBox(Page, errorMessage)
        End Sub



        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnGetStdCourses)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        End Sub

        Private Sub MessagePopUpTransferPendingDialog()
            Const strUrl As String = "TransferPendingDialog.aspx"
            RadWindowManager1.Visible = True
            RadWindowManager1.Windows(0).NavigateUrl = strUrl
            RadWindowManager1.Windows(0).Visible = True
            RadWindowManager1.Windows(0).VisibleOnPageLoad = True
        End Sub

        Protected Sub btnCheckPreviousGrade_Click(sender As Object, e As EventArgs) Handles btnCheckPreviousGrade.Click
            Try
                'Save all data grid to a list
                Dim listToTransfer As List(Of TransferRequirement) = New List(Of TransferRequirement)()

                For Each item As DataGridItem In dgdTransferGrade.Items
                    Dim transfer As TransferRequirement = New TransferRequirement()
                    transfer.ReqCode = CType(item.FindControl("Code"), Label).Text
                    transfer.CourseDescription = CType(item.FindControl("Descrip"), Label).Text
                    Decimal.TryParse(CType(item.FindControl("txtScore"), TextBox).Text, transfer.Score)
                    transfer.ReqId = CType(item.FindControl("txtReqId"), TextBox).Text
                    transfer.ModName = AdvantageSession.UserState.UserName
                    transfer.StuEnrollId = txtStuEnrollmentId.Text
                    listToTransfer.Add(transfer)
                Next

                'Check if they courses to test
                If listToTransfer.Count = 0 Then
                    DisplayErrorMessage("Please Get Student Courses first")
                    Return
                End If

                'Classifying Cases
                Dim bo = New TransferGradesBo(listToTransfer)

                'Check if exists some graded courses
                If bo.ListTransferByCopyOne.Count = 0 And bo.ListOfRegisterCopyOne.Count = 0 And bo.ListOfRareSituation.Count = 0 Then
                    DisplayErrorMessage("There are no previous grades assigned to these courses")
                    Return
                End If

                'Store values in session
                bo.ListReadyToTransferGrade.Clear()
                AdvantageSession.TpReadyToTransferList = bo.ListReadyToTransferGrade ' should be in this case always empty
                AdvantageSession.TpRegisterByCopyList = bo.ListOfRegisterCopyOne
                AdvantageSession.TpTransferByCopyList = bo.ListTransferByCopyOne
                AdvantageSession.TpWeirdThingList = bo.ListOfRareSituation

                'Test if exists some weird situations
                'First situation: the requirement(s) was/where transferred in only one enrollment for the student
                If bo.ListTransferByCopyOne.Count > 0 Or bo.ListOfRegisterCopyOne.Count > 0 Or bo.ListOfRareSituation.Count > 0 Then
                    MessagePopUpTransferPendingDialog()
                    Return
                End If

                'Clean Session
                AdvantageSession.TpTransferByCopyList = Nothing
                AdvantageSession.TpRegisterByCopyList = Nothing
                AdvantageSession.TpReadyToTransferList = Nothing
                AdvantageSession.TpWeirdThingList = Nothing

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try

        End Sub
    End Class
End Namespace