Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class AR_StudentGradeBookResultsFromConversion
    Inherits BasePage
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected studentId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String
    Protected modCode As String
    Protected state As AdvantageSessionState

    Protected LeadId As String
    Dim boolSwitchCampus As Boolean = False


    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim fac As New UserSecurityFacade
        '        Dim m_Context As HttpContext



        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = GetStudentFromStateObject(531) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            studentId = .StudentId.ToString
            LeadId = .LeadId.ToString
        End With


        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''
        resourceId = HttpContext.Current.Items("ResourceId")
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString
        pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        'This is for testing purposes only.
        'studentId = CType("{71B81B39-B5FD-4BE2-A662-D1F75B871673}", String)

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            '   build dropdownlists
            BuildDropDownLists()

            '   disable History button the first time
            ' Header1.EnableHistoryButton(False)

            ' pObj = Header1.UserPagePermission
            MyBase.uSearchEntityControlId.Value = ""

        End If

    End Sub

    Private Sub BuildDropDownLists()
        BuildStudentEnrollmentsDDL(studentId)

    End Sub

    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade
        Dim fac As New StudentGradeBookResultsFromConversionFacade

        With ddlStuEnrollId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()
            If Not .Items.Count = 1 Then
                .Items.Insert(0, New ListItem("Select", "Select"))
            End If
            .SelectedIndex = 0

            'If there is only one item in the enrollment dropdown list we should automatically
            'bring the corresponding courses.
            If ddlStuEnrollId.Items.Count = 1 And Not ddlStuEnrollId.SelectedValue = "Select" Then
                With ddlReqId
                    .DataTextField = "Descrip"
                    .DataValueField = "ReqId"
                    .DataSource = fac.GetCoursesWithResultsForEnrollment(ddlStuEnrollId.SelectedValue)
                    .DataBind()
                    If Not .Items.Count = 1 Then
                        .Items.Insert(0, New ListItem("Select", "Select"))
                    End If
                    .SelectedIndex = 0
                End With
            Else
                With ddlReqId
                    .Items.Insert(0, New ListItem("Select", "Select"))
                    .SelectedIndex = 0
                End With
            End If
        End With
    End Sub

    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        'Get DataTable with attendance for the selected enrollment
        Dim fac As New StudentGradeBookResultsFromConversionFacade

        dgrdStudentResults.DataSource = fac.GetCourseResultsForEnrollment(ddlStuEnrollId.SelectedValue, ddlReqId.SelectedValue)
        dgrdStudentResults.DataBind()


    End Sub
    Protected Sub ddlStuEnrollId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStuEnrollId.SelectedIndexChanged
        Dim fac As New StudentGradeBookResultsFromConversionFacade
        If Not ddlStuEnrollId.SelectedValue = "Select" Then
            Try
                With ddlReqId
                    .DataTextField = "Descrip"
                    .DataValueField = "ReqId"
                    .DataSource = fac.GetCoursesWithResultsForEnrollment(ddlStuEnrollId.SelectedValue)
                    .DataBind()
                    If Not .Items.Count = 1 Then
                        .Items.Insert(0, New ListItem("Select", "Select"))
                    End If
                    .SelectedIndex = 0
                End With
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                With ddlReqId
                    .Items.Clear()
                    .Items.Insert(0, New ListItem("Select", "Select"))
                    .SelectedIndex = 0
                End With
            End Try
        End If
    End Sub
End Class
