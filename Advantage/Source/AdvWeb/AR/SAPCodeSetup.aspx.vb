Imports System.Diagnostics
Imports Fame.Common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.IO
Imports Advantage.Business.Objects
Imports System.Collections
Imports Fame.Advantage.Common

Partial Class SAPCodeSetup
    Inherits BasePage
    Protected WithEvents ChkStatus As CheckBox
    Protected WithEvents Btnhistory As Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private validatetxt As Boolean = False
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected CampusId, UserId As String
    Protected MyAdvAppSettings As AdvAppSettings
    Protected DefaultMessages As Dictionary(Of String, String)
    Const PASSED As String = "Passed"
    Const WARNING As String = "Warning"
    Const INELIGIBLE As String = "Ineligible"
    Const PROBATION As String = "Probation"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        MyAdvAppSettings = AdvAppSettings.GetAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        'Put user code to initialize the page here
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        btnsave.Visible = True
        btndelete.Visible = True
        btnnew.Visible = True

        'Linkbutton2.Attributes.Add("onclick", "OpenSAPDetails();return false;")

        Dim mContext As HttpContext
        mContext = HttpContext.Current
        txtResourceId.Text = CType(mContext.Items("ResourceId"), String)

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New StringWriter
        'Dim campusId As String
        'Dim userId As String
        Dim sapObj As New SAPInfo

        Dim advantageUserState As User = AdvantageSession.UserState
        ResourceId = HttpContext.Current.Request.Params("resid")
        CampusId = AdvantageSession.UserState.CampusId.ToString
        UserId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        If (MyAdvAppSettings.AppSettings("TrackFASAP") = "True") Then

            chkFaSapPolicy.Visible = True
        Else
            chkFaSapPolicy.Visible = False
        End If
        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, CampusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                btnViewSapDetails.Visible = False

                'Populate the trig unit types and trig offset types
                PopulateTrigUnitTyps()
                PopulateTrigOffsetTyps()
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Set the text box to a new guid
                txtSAPId.Text = Guid.NewGuid.ToString

                ds = objListGen.SummaryListGenerator(UserId, CampusId)

                PopulateDataList(ds)

                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()

                txtModUser.Text = sapObj.ModUser
                txtModDate.Text = sapObj.ModDate.ToString

                If MyAdvAppSettings.AppSettings("GradesFormat").ToLower = "letter" Then TermVisibleTrue()
                chkTransferHours.Visible = False
                Dim facade As New SAPFacade
                Session("DefaultMessages") = facade.GetSapNoticeDefaultMessages()
                DefaultMessages = CType(Session("DefaultMessages"), Dictionary(Of String, String))
                ResetSAPNotices()
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                DefaultMessages = CType(Session("DefaultMessages"), Dictionary(Of String, String))
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub

    Private Sub DisableForNonSupport()
        If (Not AdvantageSession.UserState.IsUserSupport) Then
            btnsave.Visible = False
            btndelete.Visible = False
            btnViewSapDetails.Visible = False
            btnViewSapDetails.Attributes.Remove("onclick")
        Else
            btnViewSapDetails.Visible = True
        End If

    End Sub
    Private Sub ResetSAPNotices()
        Dim val As String = ""
        PassedTextBox.Text = If(DefaultMessages.TryGetValue(PASSED, val), val, "")
        WarningTextBox.Text = If(DefaultMessages.TryGetValue(WARNING, val), val, "")
        IneligibleTextBox.Text = If(DefaultMessages.TryGetValue(INELIGIBLE, val), val, "")
        ProbationTextBox.Text = If(DefaultMessages.TryGetValue(PROBATION, val), val, "")
    End Sub
    Private Sub PopulateTrigUnitTyps()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim sapDetails As New SAPFacade

        ' Bind the Dataset to the CheckBoxList
        ddlTrigUnitTypId.DataSource = sapDetails.GetTrigUnits()
        ddlTrigUnitTypId.DataTextField = "TrigUnitTypDescrip"
        ddlTrigUnitTypId.DataValueField = "TrigUnitTypId"
        ddlTrigUnitTypId.DataBind()
        ddlTrigUnitTypId.Items.Insert(0, New ListItem("Select", CType(0, String)))
        ddlTrigUnitTypId.SelectedIndex = 0
    End Sub

    Private Sub PopulateTrigOffsetTyps()


        ' Bind the Dataset to the CheckBoxList
        Dim sapDetails As New SAPFacade

        ' Bind the Dataset to the CheckBoxList
        ddlTrigOffsetTypId.DataSource = sapDetails.GetTrigOffTyps()
        ddlTrigOffsetTypId.DataTextField = "TrigOffTypDescrip"
        ddlTrigOffsetTypId.DataValueField = "TrigOffsetTypId"
        ddlTrigOffsetTypId.DataBind()
        ddlTrigOffsetTypId.Items.Insert(0, New ListItem("Select", CType(0, String)))
        ddlTrigOffsetTypId.SelectedIndex = 0
    End Sub
    Private Sub PopulateTrigOffsetTyps(TrigUnitTypId As Integer)


        ' Bind the Dataset to the CheckBoxList
        Dim sapDetails As New SAPFacade

        ' Bind the Dataset to the CheckBoxList
        ddlTrigOffsetTypId.DataSource = sapDetails.GetTrigOffTyps(CType(ddlTrigUnitTypId.SelectedValue, Integer))
        ddlTrigOffsetTypId.DataTextField = "TrigOffTypDescrip"
        ddlTrigOffsetTypId.DataValueField = "TrigOffsetTypId"
        ddlTrigOffsetTypId.DataBind()
        ddlTrigOffsetTypId.Items.Insert(0, New ListItem("Select", CType(0, String)))
        ddlTrigOffsetTypId.SelectedIndex = 0
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New StringWriter
        Dim msg As String
        Dim facade As New SAPFacade
        Try
            If validatetxt = True Then
                Exit Sub
            End If
            If (txtTerminateProbCnt.Text <> "") Then
                If (Not IsNumeric(txtTerminateProbCnt.Text)) Then
                    DisplayErrorMessage("Only Numeric Values Allowed For Termination Probation Count") 'Page - Maintenance TrackFASAP
                    Exit Sub
                End If
            End If
            'If (txtTerminateProbCnt.Text.Trim <> "") Then
            '    If chkFaSapPolicy.Checked = True And Convert.ToInt32(txtTerminateProbCnt.Text.Trim) > 1 Then
            '        DisplayErrorMessage("A FASap policy can not have a value greater than 1 for the 'Probation(s) allowed before termination' field. Changes were not saved")
            '        Exit Sub
            '    ElseIf chkFaSapPolicy.Checked = True And IsNumeric(txtTerminateProbCnt.Text) Then
            '        If Convert.ToInt32(txtTerminateProbCnt.Text.Trim) = 0 Then
            '            DisplayErrorMessage("Please Enter value greater than 0, value 1 allowed For Termination Probation Count")
            '            Exit Sub
            '        End If
            '    End If
            '    'ElseIf chkFaSapPolicy.Checked = True And Len(txtTerminateProbCnt.Text) = 0 Then
            '    '    DisplayErrorMessage("Please Enter value greater than 0, value 1 allowed For Termination Probation Count")
            '    '    Exit Sub
            'End If

            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method 

                objCommon.PagePK = txtSAPId.Text
                txtRowIds.Text = objCommon.PagePK

                Dim sapObj As SAPInfo = PopulateSAPObject(txtSAPId.Text)

                'Insert SAP policy into arSAP table
                msg = facade.InsertSAPPolicy(sapObj, CType(Session("Username"), String))

                If sapObj.FaSapPolicy = 1 Then
                    Dim listOfCustomVerbiage As New List(Of SAPCustomVerbiage)
                    If (PassedTextBox.Text <> DefaultMessages.Item(PASSED)) Then
                        listOfCustomVerbiage.Add(New SAPCustomVerbiage With {.Code = PASSED, .Message = PassedTextBox.Text, .SapId = sapObj.SAPId})
                    End If
                    If (WarningTextBox.Text <> DefaultMessages.Item(WARNING)) Then
                        listOfCustomVerbiage.Add(New SAPCustomVerbiage With {.Code = WARNING, .Message = WarningTextBox.Text, .SapId = sapObj.SAPId})
                    End If
                    If (IneligibleTextBox.Text <> DefaultMessages.Item(INELIGIBLE)) Then
                        listOfCustomVerbiage.Add(New SAPCustomVerbiage With {.Code = INELIGIBLE, .Message = IneligibleTextBox.Text, .SapId = sapObj.SAPId})
                    End If
                    If (ProbationTextBox.Text <> DefaultMessages.Item(PROBATION)) Then
                        listOfCustomVerbiage.Add(New SAPCustomVerbiage With {.Code = PROBATION, .Message = ProbationTextBox.Text, .SapId = sapObj.SAPId})
                    End If
                    msg = facade.InsertSAPCustomVerbiage(listOfCustomVerbiage, UserId)
                End If

                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If

                If Not msg = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(msg)
                    Exit Sub
                Else
                    GetSAPPolicy(txtSAPId.Text)
                End If

                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstSAP.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"


            ElseIf ViewState("MODE") = "EDIT" Then

                Dim sapObj As SAPInfo = PopulateSAPObject(txtSAPId.Text)
                Dim prevCustomVerbiage As New List(Of SAPCustomVerbiage)
                If Not Session("CustomVerbiage") Is Nothing Then
                    prevCustomVerbiage = CType(Session("CustomVerbiage"), List(Of SAPCustomVerbiage))
                End If
                'Insert SAP policy into arSAP table
                msg = facade.UpdateSAPPolicy(sapObj, CType(Session("Username"), String))

                ' If has custom verbiage but is no longer a Title IV sap policy, remove custom verbiage
                If (prevCustomVerbiage.Count > 0 AndAlso sapObj.FaSapPolicy = 0) Then
                    facade.DeleteSAPCustomVerbiage(txtSAPId.Text)
                    'If TitleIV SAP Policy 
                ElseIf (sapObj.FaSapPolicy = 1) Then
                    If (prevCustomVerbiage.Any(Function(i As SAPCustomVerbiage) i.Code = PASSED AndAlso i.SapId = sapObj.SAPId)) Then

                        ' Text was reset to default
                        If (PassedTextBox.Text = DefaultMessages.Item(PASSED)) Then
                            facade.SetSAPCustomVerbiageStatus(New SAPCustomVerbiage With {.Code = PASSED, .Message = PassedTextBox.Text, .SapId = sapObj.SAPId})
                        Else
                            Dim customVerb = prevCustomVerbiage.Where(Function(i As SAPCustomVerbiage) i.Code = PASSED AndAlso i.SapId = sapObj.SAPId).Select(Function(i As SAPCustomVerbiage) i.Message).FirstOrDefault()
                            ' Text changed to another custom text
                            If (PassedTextBox.Text <> customVerb) Then
                                facade.UpdateSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = PASSED, .Message = PassedTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                            End If
                        End If

                    Else
                        If (PassedTextBox.Text <> DefaultMessages.Item(PASSED)) Then
                            facade.InsertSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = PASSED, .Message = PassedTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                        End If
                    End If

                    If (prevCustomVerbiage.Any(Function(i As SAPCustomVerbiage) i.Code = WARNING AndAlso i.SapId = sapObj.SAPId)) Then

                        ' Text was reset to default
                        If (WarningTextBox.Text = DefaultMessages.Item(WARNING)) Then
                            facade.SetSAPCustomVerbiageStatus(New SAPCustomVerbiage With {.Code = WARNING, .Message = WarningTextBox.Text, .SapId = sapObj.SAPId})
                        Else
                            Dim customVerb = prevCustomVerbiage.Where(Function(i As SAPCustomVerbiage) i.Code = WARNING AndAlso i.SapId = sapObj.SAPId).Select(Function(i As SAPCustomVerbiage) i.Message).FirstOrDefault()
                            ' Text changed to another custom text
                            If (WarningTextBox.Text <> customVerb) Then
                                facade.UpdateSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = WARNING, .Message = WarningTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                            End If
                        End If

                    Else
                        If (WarningTextBox.Text <> DefaultMessages.Item(WARNING)) Then
                            facade.InsertSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = WARNING, .Message = WarningTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                        End If
                    End If

                    If (prevCustomVerbiage.Any(Function(i As SAPCustomVerbiage) i.Code = INELIGIBLE AndAlso i.SapId = sapObj.SAPId)) Then

                        ' Text was reset to default
                        If (IneligibleTextBox.Text = DefaultMessages.Item(INELIGIBLE)) Then
                            facade.SetSAPCustomVerbiageStatus(New SAPCustomVerbiage With {.Code = INELIGIBLE, .Message = IneligibleTextBox.Text, .SapId = sapObj.SAPId})
                        Else
                            Dim customVerb = prevCustomVerbiage.Where(Function(i As SAPCustomVerbiage) i.Code = INELIGIBLE AndAlso i.SapId = sapObj.SAPId).Select(Function(i As SAPCustomVerbiage) i.Message).FirstOrDefault()
                            ' Text changed to another custom text
                            If (IneligibleTextBox.Text <> customVerb) Then
                                facade.UpdateSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = INELIGIBLE, .Message = IneligibleTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                            End If
                        End If

                    Else
                        If (IneligibleTextBox.Text <> DefaultMessages.Item(INELIGIBLE)) Then
                            facade.InsertSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = INELIGIBLE, .Message = IneligibleTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                        End If
                    End If

                    If (prevCustomVerbiage.Any(Function(i As SAPCustomVerbiage) i.Code = PROBATION AndAlso i.SapId = sapObj.SAPId)) Then

                        ' Text was reset to default
                        If (ProbationTextBox.Text = DefaultMessages.Item(PROBATION)) Then
                            facade.SetSAPCustomVerbiageStatus(New SAPCustomVerbiage With {.Code = PROBATION, .Message = ProbationTextBox.Text, .SapId = sapObj.SAPId})
                        Else
                            Dim customVerb = prevCustomVerbiage.Where(Function(i As SAPCustomVerbiage) i.Code = PROBATION AndAlso i.SapId = sapObj.SAPId).Select(Function(i As SAPCustomVerbiage) i.Message).FirstOrDefault()
                            ' Text changed to another custom text
                            If (ProbationTextBox.Text <> customVerb) Then
                                facade.UpdateSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = PROBATION, .Message = ProbationTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                            End If
                        End If

                        ' Text was reset to default
                        If (ProbationTextBox.Text = DefaultMessages.Item(PROBATION)) Then
                            facade.SetSAPCustomVerbiageStatus(New SAPCustomVerbiage With {.Code = PROBATION, .Message = ProbationTextBox.Text, .SapId = sapObj.SAPId})
                        Else
                            Dim customVerb = prevCustomVerbiage.Where(Function(i As SAPCustomVerbiage) i.Code = PROBATION AndAlso i.SapId = sapObj.SAPId).Select(Function(i As SAPCustomVerbiage) i.Message).FirstOrDefault()
                            ' Text changed to another custom text
                            If (ProbationTextBox.Text <> customVerb) Then
                                facade.UpdateSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = PROBATION, .Message = ProbationTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                            End If
                        End If

                    Else
                        If (ProbationTextBox.Text <> DefaultMessages.Item(PROBATION)) Then
                            facade.InsertSAPCustomVerbiage((New List(Of SAPCustomVerbiage) From {New SAPCustomVerbiage With {.Code = PROBATION, .Message = ProbationTextBox.Text, .SapId = sapObj.SAPId}}), UserId)
                        End If
                    End If
                End If
                If Not msg = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(msg)
                Else
                    GetSAPPolicy(txtSAPId.Text)
                End If

                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstSAP.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
            End If

            'enable links to SAP Details
            btnViewSapDetails.Visible = True
            btnViewSapDetails.Enabled = True
            CommonWebUtilities.RestoreItemValues(dlstSAP, txtSAPId.Text)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub GetSAPPolicy(ByVal sapid As String)
        Dim facade As New SAPFacade
        BindSAPInfoExist(facade.GetSAPPolicy(sapid))
        Session("FaSapPolicy") = facade.GetSAPPolicy(sapid).FaSapPolicy
    End Sub

    Private Sub BindSAPInfoExist(ByVal sapInfo As SAPInfo)

        'Bind The StudentInfo Data From The Database
        With sapInfo
            txtSAPId.Text = .SAPId.ToString
            ddlStatusId.SelectedValue = .StatusId.ToString
            ddlCampGrpId.SelectedValue = .CampGrpId.ToString
            txtSAPCode.Text = .Code
            txtSAPDescrip.Text = .Descrip

            Try
                ddlTrigUnitTypId.SelectedValue = CType(.TrigUnitTypId, String)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                'Do nothing
            End Try

            PopulateTrigOffsetTyps(CType(ddlTrigUnitTypId.SelectedValue, Integer))
            'If (.TrigUnitTypId = "4" Or .TrigUnitTypId = "5") Then
            '    ddlTrigOffsetTypId.Items.RemoveAt(1)
            'End If

            Try
                ddlTrigOffsetTypId.SelectedValue = CType(.TrigOffsetTypId, String)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                'Do nothing
            End Try

            txtModDate.Text = .ModDate.ToString
            txtModUser.Text = .ModUser
            If (.TerminateProbCnt = 0) Then
                txtTerminateProbCnt.Text = ""
            Else
                txtTerminateProbCnt.Text = CType(.TerminateProbCnt, String)
            End If
            If (.MinTermGPA = 0) Then
                txtMinTermGPA.Text = ""
            Else
                txtMinTermGPA.Text = CType(.MinTermGPA, String)
            End If
            If (.TermGPAOver = 0) Then
                txtTermGPAOver.Text = ""
            Else
                txtTermGPAOver.Text = CType(.TermGPAOver, String)
            End If
            If (.TrigOffsetTypId = "2" And MyAdvAppSettings.AppSettings("GradesFormat").ToLower = "letter") Then
                TermVisibleTrue()
                'TermVisibleFalse()
            Else
                TermVisibleFalse()
            End If
            ShowProbationTermination()

            If (MyAdvAppSettings.AppSettings("TrackFASAP") = "True") Then
                Session("CustomVerbiage") = .TitleIVSAPCustomVerbiage
                ResetSAPNotices()
                chkFaSapPolicy.Visible = True
                If (.FaSapPolicy = 1) Then
                    chkFaSapPolicy.Checked = True
                    SapVerbiageContainer.Visible = True
                    chkPayOnProbation.Visible = True
                    chkPayOnProbation.Checked = .PayOnProbation.HasValue AndAlso .PayOnProbation.Value
                    TermVisibleFalse()
                    HideProbationTermination()
                    DisableForNonSupport()
                    If (.TitleIVSAPCustomVerbiage.Count > 0) Then
                        For Each scv As SAPCustomVerbiage In .TitleIVSAPCustomVerbiage
                            Select Case scv.Code
                                Case PASSED
                                    PassedTextBox.Text = scv.Message
                                Case WARNING
                                    WarningTextBox.Text = scv.Message
                                Case INELIGIBLE
                                    IneligibleTextBox.Text = scv.Message
                                Case PROBATION
                                    ProbationTextBox.Text = scv.Message
                                Case Else
                            End Select
                        Next


                    End If
                Else
                    chkFaSapPolicy.Checked = False
                    SapVerbiageContainer.Visible = False
                    chkPayOnProbation.Visible = False
                    chkPayOnProbation.Checked = False
                End If
            Else
                chkFaSapPolicy.Visible = False
            End If
            chkTrackExtern.Checked = .TrackExternAttendance

            chkTransferHours.Checked = .IncludeTransferHours


        End With
    End Sub

    Private Function PopulateSAPObject(ByVal sguid As String) As SAPInfo
        Dim sapObj As New SAPInfo

        sapObj.SAPId = XmlConvert.ToGuid(txtSAPId.Text)
        sapObj.Code = txtSAPCode.Text
        sapObj.Descrip = txtSAPDescrip.Text
        sapObj.TrigUnitTypId = CType(ddlTrigUnitTypId.SelectedItem.Value, Integer)
        sapObj.TrigOffsetTypId = CType(ddlTrigOffsetTypId.SelectedItem.Value, Integer)

        sapObj.StatusId = XmlConvert.ToGuid(ddlStatusId.SelectedItem.Value)
        sapObj.CampGrpId = ddlCampGrpId.SelectedItem.Value
        sapObj.TrackExternAttendance = chkTrackExtern.Checked
        sapObj.IncludeTransferHours = chkTransferHours.Checked ' Include Transfer Hours

        If txtModDate.Text = "" Then
            sapObj.ModDate = Date.Now
        Else
            sapObj.ModDate = CType(txtModDate.Text, Date)
        End If
        If (txtTerminateProbCnt.Text = "") Then
            sapObj.TerminateProbCnt = 0
        Else
            sapObj.TerminateProbCnt = CType(txtTerminateProbCnt.Text, Integer)
        End If
        If (txtMinTermGPA.Text = "") Then
            sapObj.MinTermGPA = 0
        Else
            sapObj.MinTermGPA = CType(txtMinTermGPA.Text, Decimal)
        End If
        If (txtTermGPAOver.Text = "") Then
            sapObj.TermGPAOver = 0
        Else
            sapObj.TermGPAOver = CType(txtTermGPAOver.Text, Decimal)
        End If

        If chkFaSapPolicy.Checked = True And (MyAdvAppSettings.AppSettings("TrackFASAP") = "True") Then
            sapObj.FaSapPolicy = 1
            sapObj.PayOnProbation = chkPayOnProbation.Checked
        ElseIf sapObj.FaSapPolicy = 0 And chkFaSapPolicy.Visible = True Then
            sapObj.Descrip = TrimSAPDescrip(txtSAPDescrip.Text.Trim)
        End If


        Return sapObj
    End Function

    Private Function TrimSAPDescrip(ByVal item As String) As String
        Dim myPos = InStr(item, "(")
        If myPos > 0 Then
            Dim leftString = Left(item, myPos - 1)

            Return leftString
        Else
            Return item
        End If

    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))
        Try
            ClearRhs()
            ds.ReadXml(sr)
            PopulateDataList(ds)
            'Set the text box to a new guid
            txtSAPId.Text = Guid.NewGuid.ToString
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Reset Style in the Data-list
            btnViewSapDetails.Visible = False
            TermVisibleTrue()
            ShowProbationTermination()
            chkTransferHours.Visible = False

            chkFaSapPolicy.Checked = (New SAPCheckFacade).CheckForFASAPDependency(txtSAPId.Text)
            'If (New SAPCheckFacade).CheckForSAPDependency(txtSAPId.Text) = False Then
            '    chkFaSapPolicy.Checked = True

            'Else
            '    chkFaSapPolicy.Checked = False

            'End If
            CommonWebUtilities.RestoreItemValues(dlstSAP, Guid.Empty.ToString)
            ResetSAPNotices()
            chkPayOnProbation.Checked = False

            If (chkFaSapPolicy.Checked) Then
                SapVerbiageContainer.Visible = True
                chkPayOnProbation.Visible = True
                TermVisibleFalse()
                HideProbationTermination()
            Else
                SapVerbiageContainer.Visible = False
                chkPayOnProbation.Visible = False
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btndelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New StringWriter
        Dim msg As String
        Dim facade As New SAPFacade
        Try
            msg = facade.DeleteSAPCustomVerbiage(txtSAPId.Text)

            If msg <> "" Then
                DisplayErrorMessage(msg)
            End If

            msg = facade.DeleteSAPPolicy(txtSAPId.Text)

            If msg <> "" Then
                DisplayErrorMessage(msg)
            End If
            ClearRhs()
            ds = objListGen.SummaryListGenerator(userId, campusId)
            dlstSAP.SelectedIndex = -1
            PopulateDataList(ds)
            ds.WriteXml(sw)
            ViewState("ds") = sw.ToString()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtSAPId.Text = Guid.NewGuid.ToString
            ' Header1.EnableHistoryButton(False)
            btnViewSapDetails.Visible = False
            TermVisibleFalse()
            chkTransferHours.Visible = False
            CommonWebUtilities.RestoreItemValues(dlstSAP, Guid.Empty.ToString)
            ResetSAPNotices()
            SapVerbiageContainer.Visible = False
            chkPayOnProbation.Visible = False
            chkPayOnProbation.Checked = False

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    'Private saFaArray() As String
    'Private ReadOnly Property MyArray() As String()
    '    Get
    '        Return saFaArray
    '    End Get
    'End Property


    Private Function ValidateSaPorFa(ByVal strGUID As String) As String()
        Dim saFaArray(2) As String
        'ReDim saFaArray(2)

        Dim facade As SAPCheckFacade = New SAPCheckFacade()

        If facade.CheckForSAPDependency(strGUID) = True Then
            saFaArray(0) = "False"
        Else
            saFaArray(0) = "True"
        End If
        If facade.CheckForFASAPDependency(strGUID) = True Then
            saFaArray(1) = "False"
        Else
            saFaArray(1) = "False"
        End If

        Return saFaArray

    End Function
    Private Sub dlstSAP_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlstSAP.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim faSapPolicy As Integer = 0
        Try
            Master.PageObjectId = CType(e.CommandArgument, String)
            Master.PageResourceId = CType(ResourceId, Integer)
            Master.SetHiddenControlForAudit()

            strGUID = dlstSAP.DataKeys(e.Item.ItemIndex).ToString()
            GetSAPPolicy(strGUID)
            DisableForNonSupport()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstSAP.SelectedIndex = selIndex

            ds.ReadXml(sr)
            PopulateDataList(ds)
            If Not IsNothing(Session("FaSapPolicy")) Then
                faSapPolicy = CType(Session("FaSapPolicy"), Integer)
            End If

            'Set visibility of the transfer hour control
            SetTransferHoursVisibility()



            ''Validation on SAP or FA SAP
            If faSapPolicy >= 0 Then
                Dim mValidate() As String = ValidateSaPorFa(strGUID)
                CommonWebUtilities.RestoreItemValues(dlstSAP, strGUID)
                For i As Integer = 0 To UBound(mValidate)

                    If mValidate(0) = "True" Then
                        chkFaSapPolicy.Enabled = True
                        Exit Sub
                    ElseIf mValidate(1) = "True" Then
                        chkFaSapPolicy.Enabled = True
                        Exit Sub
                    Else
                        chkFaSapPolicy.Enabled = False
                    End If

                Next
            End If


        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstSAP_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstSAP_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub chkFaSapPolicy_Clicked(ByVal sender As Object, ByVal e As EventArgs) Handles chkFaSapPolicy.CheckedChanged
        If (chkFaSapPolicy.Checked) Then
            SapVerbiageContainer.Visible = True
            chkPayOnProbation.Visible = True
            If chkTransferHours.Visible = True Then
                chkTransferHours.Checked = True
            End If
            HideProbationTermination()
            If MyAdvAppSettings.AppSettings("GradesFormat").ToLower = "letter" Then TermVisibleFalse()
            If (MyAdvAppSettings.AppSettings("TrackFASAP") = "True") Then
                btnsave.Visible = True
                DisableForNonSupport()
            Else
                btnsave.Visible = False
            End If
        Else
            SapVerbiageContainer.Visible = False
            chkPayOnProbation.Visible = False
            If chkTransferHours.Visible = True Then
                chkTransferHours.Checked = False
            End If
            SetTransferHoursVisibility()
            ShowProbationTermination()
            If MyAdvAppSettings.AppSettings("GradesFormat").ToLower = "letter" Then TermVisibleTrue()
            btnsave.Visible = True
        End If

    End Sub

    Public Sub resetPassedButton_Clicked(ByVal sender As Object, ByVal e As EventArgs) Handles ResetPassedButton.Click
        Dim val As String = ""
        PassedTextBox.Text = If(DefaultMessages.TryGetValue(PASSED, val), val, "")
    End Sub
    Private Sub resetWarningButton_Clicked(ByVal sender As Object, ByVal e As EventArgs) Handles ResetWarningButton.Click
        Dim val As String = ""
        WarningTextBox.Text = If(DefaultMessages.TryGetValue(WARNING, val), val, "")
    End Sub
    Private Sub resetIneligibleButton_Clicked(ByVal sender As Object, ByVal e As EventArgs) Handles ResetIneligible.Click
        Dim val As String = ""
        IneligibleTextBox.Text = If(DefaultMessages.TryGetValue(INELIGIBLE, val), val, "")
    End Sub
    Private Sub resetProbationButton_Clicked(ByVal sender As Object, ByVal e As EventArgs) Handles ResetProbation.Click
        Dim val As String = ""
        ProbationTextBox.Text = If(DefaultMessages.TryGetValue(PROBATION, val), val, "")
    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkStatus.CheckedChanged
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtSAPId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
            btnViewSapDetails.Visible = False
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was re factored by AVP on 6/19/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the data-table
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radstatus.SelectedItem.Text.ToLower = "active") Or (radstatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radstatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "SAPDescrip", DataViewRowState.CurrentRows)

        With dlstSAP
            .DataSource = dv
            .DataBind()
        End With
        dlstSAP.SelectedIndex = -1
    End Sub

    Public Sub TextValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs)
        Dim strMinus As String = Mid(args.Value, 1, 1)
        If strMinus = "-" Then
            DisplayErrorMessage("The value cannot be negative")
            validatetxt = True
        End If
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtSAPId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub

    Protected Sub ddlTrigOffsetTypId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTrigOffsetTypId.SelectedIndexChanged
        If (ddlTrigOffsetTypId.SelectedValue = "2" And MyAdvAppSettings.AppSettings("GradesFormat").ToLower = "letter" And Not chkFaSapPolicy.Checked) Then
            TermVisibleTrue()
        Else
            TermVisibleFalse()
        End If
    End Sub

    Private Sub TermVisibleTrue()
        lblMinTermGPA.Visible = True
        txtMinTermGPA.Visible = True
        lblTermGPAOver.Visible = True
        txtTermGPAOver.Visible = True
    End Sub

    Private Sub TermVisibleFalse()
        lblMinTermGPA.Visible = False
        txtMinTermGPA.Visible = False
        lblTermGPAOver.Visible = False
        txtTermGPAOver.Visible = False
    End Sub
    Private Sub HideProbationTermination()
        lblTerminate.Visible = False
        txtTerminateProbCnt.Visible = False
        chkTrackExtern.Visible = False
    End Sub
    Private Sub ShowProbationTermination()
        lblTerminate.Visible = True
        txtTerminateProbCnt.Visible = True
        chkTrackExtern.Visible = True
    End Sub

    Protected Sub ddlTrigUnitTypId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTrigUnitTypId.SelectedIndexChanged
        SetTransferHoursVisibility()
        PopulateTrigOffsetTyps(CType(ddlTrigUnitTypId.SelectedValue, Integer))
        TermVisibleFalse()
        If (chkTransferHours.Visible) Then
            chkTransferHours.Checked = chkFaSapPolicy.Checked

        Else
            chkTransferHours.Checked = False
        End If
    End Sub

    Private Sub SetTransferHoursVisibility()
        Dim sapAttendance As String = MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower
        Dim isvis As Boolean = ((sapAttendance = "byday" Or sapAttendance = "byclass") And ddlTrigUnitTypId.SelectedValue = "4")
        chkTransferHours.Visible = isvis
        Return
    End Sub
End Class