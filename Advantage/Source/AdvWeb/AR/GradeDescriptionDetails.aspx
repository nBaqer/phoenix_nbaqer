<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master" Inherits="GradeDescriptionDetails" CodeFile="GradeDescriptionDetails.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content5" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstBankCodes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstBankCodes" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstBankCodes" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstBankCodes" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframe">
                            <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="listframetop">
                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td nowrap align="left" width="15%">
                                                    <asp:Label ID="lblshow" runat="server"><b class="tothemeshow">Grade System</b></asp:Label></td>
                                                <td nowrap width="70%">
                                                    <asp:DropDownList ID="ddlgrdsystemfilterid" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td nowrap width="15%">
                                                    <div style="margin: 10px;">
                                                        <asp:Button ID="btnfilter" runat="server" Text="Show Grades"></asp:Button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="listframebottom">
                                        <div class="scrollleftfilters">
                                            <asp:DataList ID="dlstBankCodes" runat="server" DataKeyField="grdsysdetailid" Width="100%">
                                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("grdsysdetailid")%>' Text='<%# container.dataitem("grade")%>'>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="new" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <!-- begin table content-->
                                    <asp:TextBox ID="txtbankid" runat="server" Visible="false"></asp:TextBox><asp:CheckBox ID="chkisindb" runat="server" Visible="false"></asp:CheckBox><asp:TextBox ID="txtmoduser" runat="server" Visible="false">moduser</asp:TextBox><asp:TextBox ID="txtmoddate" runat="server" Visible="false">moddate</asp:TextBox>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="label1" runat="server" CssClass="label">Grade System</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlgrdsystemid" TabIndex="1" runat="server" CssClass="dropdownlist" AutoPostBack="true"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblgrade" runat="server" CssClass="label">Grade</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlgrade" TabIndex="2" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblgradedescrip" runat="server" CssClass="label">Description</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtgradedescrip" TabIndex="3" runat="server" CssClass="textbox" MaxLength="100"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblquality" runat="server" CssClass="label">Quality</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtquality" TabIndex="4" runat="server" CssClass="textbox" MaxLength="100"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                    <!--end table content-->
                                </div>
                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="customvalidator"
            Display="none"></asp:CustomValidator>
        <asp:Panel ID="pnlrequiredfieldvalidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="true"
            ShowSummary="false"></asp:ValidationSummary>
    </div>
    <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
</asp:Content>
