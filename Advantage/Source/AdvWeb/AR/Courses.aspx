<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Courses.aspx.vb" Inherits="Courses" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResizedn(sender) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <div style="overflow: auto;">
        <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResizedn(this)" Style="overflow: auto;">
            <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table class="maincontenttable">
                    <tr>
                        <td class="listframetop">
                            <table class="maincontenttable">
                                <tr>
                                    <td style="width: 10%; text-align: left; text-wrap: avoid">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td style="width: 85%; text-wrap: avoid">
                                        <asp:RadioButtonList ID="radStatus" CssClass="radiobutton" AutoPostBack="true" runat="Server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframetop2">
                            <div id="grdWithScroll" class="scrollleftfilters" onscroll="SetDivPosition()">
                                <asp:DataList ID="dlstCourses" runat="server" DataKeyField="ReqId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>' CausesValidation="False"></asp:ImageButton>
                                        <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>' CausesValidation="False"></asp:ImageButton>
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                        <asp:LinkButton ID="Linkbutton1" CssClass="itemstyle" CausesValidation="False" runat="server" CommandArgument='<%# Container.DataItem("ReqId")%>' Text='<%# Container.DataItem("CodeAndDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
                <asp:Panel ID="pnlRHS" runat="server">
                    <table style="width: 100%; border: 0; border-spacing: 0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td style="padding: 0; text-align: right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                                <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <table class="maincontenttable">
                        <tr>
                            <td class="detailsframe">
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <!-- begin content table-->

                                    <table class="contenttable" style="width: 70%;">
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblCode" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" runat="server" CssClass="textbox"></asp:TextBox></td>
                                            <tr>
                                                <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                    <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                            </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblDescrip" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtDescrip" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblCredits" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCredits" runat="server" CssClass="textbox"></asp:TextBox>
                                                <asp:CustomValidator ID="Customvalidator4" runat="server" OnServerValidate="TextValidate" Display="None"
                                                    ErrorMessage="Negative Values Not Allowed" ControlToValidate="txtCredits"></asp:CustomValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblFinAidCredits" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtFinAidCredits" runat="server" CssClass="textbox"></asp:TextBox>
                                                <asp:CustomValidator ID="Customvalidator3" runat="server" OnServerValidate="TextValidate" Display="None"
                                                    ErrorMessage="Negative Values Not Allowed" ControlToValidate="txtFinAidCredits"></asp:CustomValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblHours" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtHours" runat="server" CssClass="textbox"></asp:TextBox>
                                                <asp:CustomValidator ID="Customvalidator2" runat="server" OnServerValidate="TextValidate" Display="None"
                                                    ErrorMessage="Negative Values Not Allowed" ControlToValidate="txtHours"></asp:CustomValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblGrdLvlId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlGrdLvlId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblCourseCategoryId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCourseCategoryId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblCourseCatalog" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCourseCatalog" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td style="text-align: left">
                                                <asp:CheckBox ID="chkSU" runat="server" CssClass="label" Text="Satisfactory/UnSatisfactory" Visible="False"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td style="text-align: left">
                                                <asp:RadioButtonList ID="radProgress" CssClass="label" runat="Server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Satisfactory/UnSatisfactory" />
                                                    <asp:ListItem Text="Pass/Fail" />
                                                    <asp:ListItem Text="None" Selected="True" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:Label ID="label1" runat="server" CssClass="label">Departments</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlDeptId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="white-space: nowrap;">
                                                <asp:Label ID="lblUnitTypeId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlUnitTypeId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td class="contentcell">
                                                <asp:CheckBox ID="chkExternship" runat="server" CssClass="checkbox" Text="Externship" AutoPostBack="True" /></td>
                                        </tr>

                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td class="contentcell" style="display: block;">
                                                <asp:CheckBox ID="chkTrackTardies" runat="server" Text="Track Tardies" CssClass="checkbox" AutoPostBack="True" />
                                            </td>

                                        </tr>
                                        <tr>
                                            <asp:Panel ID="pnlTardies" runat="server" Visible="False">
                                                <td class="contentcell" style="white-space: nowrap">
                                                    <asp:Label ID="lblTardiesMakingAbsence" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                    <asp:TextBox ID="txtTardiesMakingAbsence" runat="server" CssClass="textbox"></asp:TextBox>
                                                    <asp:RangeValidator ID="rvTardiesMakingAbsence" runat="server" ControlToValidate="txtTardiesMakingAbsence" Display="none" MinimumValue="0" MaximumValue="365" ErrorMessage="Number of Tardies Making An Absence must be between 0 and 365." Type="integer"></asp:RangeValidator>
                                                </td>
                                            </asp:Panel>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="white-space: nowrap;"></td>
                                            <td class="contentcell">
                                                <asp:CheckBox ID="chkAttendanceOnly" runat="server" CssClass="checkbox" Text="Use this Course to Track Attendance by Program" AutoPostBack="True" Visible="true" /></td>
                                        </tr>

                                        <tr>
                                            <td class="contentcell" style="white-space: nowrap;"></td>
                                            <td class="contentcell">
                                                <asp:CheckBox ID="chkAllowRetakes" runat="server" CssClass="checkbox" Text="Allow Course Retakes for Completed Courses" /></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell"></td>
                                            <td class="twocolumncontentcelldate">
                                                <asp:CheckBox ID="chkUseTimeClock" runat="server" CssClass="label" Text="Use Time Clock" /></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td class="contentcell">
                                                <asp:CheckBox ID="cbIsOnLine" runat="server" CssClass="label" Text="Is Online?" Visible="false" Checked="false" /></td>
                                        </tr>

                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCourseComments" runat="server" CssClass="label">Comments</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCourseComments" runat="server" MaxLength="300" CssClass="tocommentsnowrap"
                                                    Columns="60" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:LinkButton ID="lbtSetUpFees" runat="server" CssClass="label" Enabled="False">Set Up Fees for this Course</asp:LinkButton></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:LinkButton ID="lbtSetupBooks" runat="server" CssClass="label" Enabled="True">Set Up Books for this Course</asp:LinkButton></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:LinkButton ID="lbtSetUpGrdBkWgt" runat="server" CssClass="label" Enabled="False">Set Up Grade Book Weightings for this Course</asp:LinkButton></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;">
                                                <asp:LinkButton ID="lbtSetupRules" runat="server" CssClass="label" Enabled="False">Set Up Dictation/Speed Test Rules for this Course</asp:LinkButton></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap;"></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtReqId" runat="server" CssClass="" MaxLength="128" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtModUser" runat="server" CssClass="" Visible="False">ModUser</asp:TextBox>
                                                <asp:TextBox ID="txtModDate" runat="server" CssClass="" Visible="False">ModDate</asp:TextBox>
                                                <asp:TextBox ID="txtRowIds" Visible="False" CssClass="" Style="visibility: hidden" runat="server"></asp:TextBox>
                                                <asp:TextBox ID="txtResourceId" Visible="False" CssClass="" Style="visibility: hidden" runat="server"></asp:TextBox>
                                                <asp:TextBox ID="txtAttUnitType" runat="server" CssClass="" Visible="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>

                                    <!--end table content-->
                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="maincontenttable">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" colspan="6">
                                                    <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table class="maincontenttable">
                                            <tr>
                                                <td class="contentcell2" colspan="6">
                                                    <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false"></asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass=""></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="" ErrorMessage="CustomValidator"
            Display="None"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="" ShowMessageBox="True"
            ShowSummary="False"></asp:ValidationSummary>
    </div>

</asp:Content>


