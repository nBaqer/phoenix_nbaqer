' ===============================================================================
'
' FAME AdvantageV1
'
' EmployeeInfo.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports Telerik.Web.UI

Partial Class SetupTestRules
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents ddlScannedId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtStudentDocs As System.Web.UI.WebControls.TextBox
    Protected strCount As String
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblDocumentModule As System.Web.UI.WebControls.Label
    Protected WithEvents ddlDocumentTypeId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStudentName As System.Web.UI.WebControls.Label
    Protected WithEvents ddlStudentName As System.Web.UI.WebControls.DropDownList
    Protected StudentId As String '= "14094BD8-18CA-4B95-9195-069D47774306"


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

    Private pObj As New UserPagePermissionInfo
    Protected selectedModule As Integer
    Protected campusId As String
    Protected campus As String
    Protected courseid As String
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ' 05/2/2012 Janet Robinson telerik bug caused dynamic end year to blow up page so it was moved here
        lblFooterText.Text = " Copyright @ FAME 2005 - " + Year(DateTime.Now).ToString + ". All rights reserved."

        Dim userId As String
        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim objCommon As New CommonUtilities
        'Put user code to initialize the page here
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        'resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        'campus = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campus = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campus)
        If Not Page.IsPostBack Then
            objCommon.SetCaptionsAndColorRequiredFieldsPopUps(Form1)
            objCommon.SetBtnState(Form1, "NEW", pObj)


            txtReqId.Text = Request.Params("CourseId")
            txtReqDescrip.Text = Request.Params("Course")
            BindRadGrid()
            BindDataList()
            BuildPrereqDDL()
            BuildCategoryCombinationDDL()
            BuildMentorSection()
            ViewState("MODE") = "NEW"
        Else
            objCommon.SetCaptionsAndColorRequiredFieldsPopUps(Form1)
            BuildMentorSection()
        End If
        'pObj = fac.GetUserResourcePermissions(userId, resourceId, Trim(campus))

        SetPermissions()

        If Session("UserName").ToString.Trim = "" Then
            Session("Error") = "Session Expired"
            Response.Redirect("../ErrorPage.aspx")
        End If
    End Sub
    Private Sub SetPermissions()
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
        If pObj.HasFull Then
            btnNew.Enabled = True
            btnSave.Enabled = True
            btnDelete.Enabled = True
        End If
        If pObj.HasAdd Then
            btnNew.Enabled = True
            btnSave.Enabled = True
        End If
        If pObj.HasDelete Then
            btnDelete.Enabled = True
        End If
        If pObj.HasEdit Then
            btnSave.Enabled = True
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If Page is free of errors Show Edit Buttons
        If Page.IsValid Then
            InitButtonsForEdit()
        End If

        AddRules()
        AddPrerequisites()
        AddMentorProctored()

        BindDataList()
        txtOldEffectiveDate.Text = CType(txtEffectiveDate, RadDatePicker).DbSelectedDate

    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        InitButtonsForLoad()
        BindRadGrid()
        ClearPreRequisitesSection()
        HideStatusMessage()
        BindMentorProctored("1/1/1900", "")
        rfvEffectiveDate.Enabled = False
        ViewState("btnState") = "new"
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        CType(txtEffectiveDate, RadDatePicker).Clear()
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim ruleFacade As New SetDictationTestRulesFacade
        Dim dtOldEffectiveDate As DateTime = #1/1/1900#
        If Len(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate) = 0 Then
            Exit Sub
        End If
        If Not txtOldEffectiveDate.Text.ToString.Trim = "" Then
            dtOldEffectiveDate = Convert.ToDateTime(txtOldEffectiveDate.Text)
        End If

        ruleFacade.DeleteRulesByEffectiveDateAndCourse(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate, txtReqId.Text)
        ruleFacade.DeletePreRequisitesByEffectiveDateAndCourse(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate, txtReqId.Text)
        ruleFacade.DeleteMentorTestByEffectiveDateAndCourse(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate, txtReqId.Text)

        InitButtonsForLoad()
        ClearPreRequisitesSection()
        BindMentorProctored("1/1/1900", "")
        BindRadGrid()
        BindDataList()
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub BindRadGrid()

        Dim radTestDetailsGrid As RadGrid
        radTestDetailsGrid = DirectCast(RadPanelBar1.FindItemByValue("TopSpeedTestingCategory").FindControl("RadTestDetailsGrid"), RadGrid)
        radTestDetailsGrid.DataSource = (New SetDictationTestRulesFacade).GetTestByCourse(txtReqId.Text)
        radTestDetailsGrid.DataBind()

    End Sub
    Private Sub BuildMentorSection(Optional ByVal strclearvalue As String = "")
        Dim radTestDetailsGrid As RadGrid
        radTestDetailsGrid = DirectCast(RadPanelBar1.FindItemByValue("TopSpeedTestingCategory").FindControl("RadTestDetailsGrid"), RadGrid)
        Dim intRowCount As Integer = radTestDetailsGrid.Items.Count
        Dim intLoop As Integer
        Dim pnlMentor As Panel
        Dim dynLabel As Label
        Dim dynDropDownList, dynOperatorDropDownList As DropDownList
        pnlMentor = DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("pnlMentor"), Panel)
        If intRowCount >= 1 Then
            pnlMentor.Controls.Add(New LiteralControl("<table class=""contenttable"" cellSpacing=""0"" cellPadding=""0"" width=""500px"" align=""center"">"))
            pnlMentor.Controls.Add(New LiteralControl("<tr height=""25""><td>&nbsp;</td></tr>"))
        End If
        For intLoop = 1 To intRowCount
            'Create Labels For Ranges
            dynLabel = New Label
            With dynLabel
                .ID = "lbl" & intLoop
                .Text = intLoop.ToString & "."
                .CssClass = "Label"
                .Style("left") = "500px"
            End With

            'Create TextBoxes For Ranges
            dynDropDownList = New DropDownList
            With dynDropDownList
                .ID = "ddl" & intLoop
                .CssClass = "DropDownList"
                .Style("width") = "300px"
                .Style("left") = "500px"
                .DataSource = (New SetDictationTestRulesFacade).GetMentorProctoredTestRequirement("", "") '(strReqId, strPreReqId)
                .DataTextField = "MentorProctoredRequirement"
                .DataValueField = "MentorValue"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", "Select"))
                If strclearvalue = "clearvalues" Then
                    .SelectedIndex = 0
                End If
            End With

            dynOperatorDropDownList = New DropDownList
            With dynOperatorDropDownList
                .ID = "ddlOperator" & intLoop
                .CssClass = "DropDownList"
                .Style("width") = "75px"
                .Style("left") = "500px"
                .Items.Insert(0, New ListItem("Select", "Select"))
                .Items.Insert(1, New ListItem("Or", "Or"))
                .Items.Insert(2, New ListItem("And", "And"))
                If strclearvalue = "clearvalues" Then
                    .SelectedIndex = 0
                End If
            End With

            pnlMentor.Controls.Add(New LiteralControl("<tr><td width=""25px"">"))
            pnlMentor.Controls.Add(dynLabel)
            pnlMentor.Controls.Add(New LiteralControl("</td><td  width=""250px"">"))
            pnlMentor.Controls.Add(dynDropDownList)
            pnlMentor.Controls.Add(New LiteralControl("</td><td width=""25px"">&nbsp;</td><td  width=""200px"">"))
            If intLoop < intRowCount Then 'if there are 3 rows then the operator dropdown should only show up in first two rows.
                pnlMentor.Controls.Add(dynOperatorDropDownList)
            End If
            pnlMentor.Controls.Add(New LiteralControl("</td></tr><tr height=""10px""><td></td></tr>"))
        Next
        If intRowCount >= 1 Then
            pnlMentor.Controls.Add(New LiteralControl("<tr height=""10px""><td></td></tr></table>"))
        End If
    End Sub
    Private Function GetListofCourses(ByVal ReqId As String, Optional ByVal PreReqId As String = "") As DataSet
        Dim tblCourses As New DataTable("Courses")
        With tblCourses
            .Columns.Add("ReqId", GetType(Guid))
        End With
        tblCourses.LoadDataRow(New Object() {ReqId, PreReqId}, False)
        Dim dsCourses As DataSet = New DataSet()
        dsCourses.Tables.Add(tblCourses)
        Return dsCourses
    End Function
    Private Function GetListofRules() As DataSet
        Dim tbl As New DataTable("SetupTestRules")
        Dim strGrdComponentTypeId_ReqId As String = ""
        Dim strEffectiveDate As String = ""
        Dim intNumberofTests As Integer = 0
        Dim decMinimumScore As Decimal = 0.0
        Dim strModUser As String = ""
        Dim strModDate As DateTime = Date.Now
        Dim intSelectedCount As Integer = 0
        '        Dim item As GridDataItem

        With tbl
            'Define table schema
            .Columns.Add("GrdComponentTypeId_ReqId", GetType(Guid))
            .Columns.Add("NumberofTests", GetType(Integer))
            .Columns.Add("MinimumScore", GetType(Decimal))
            .Columns.Add("ModUser", GetType(String))

            'set field properties
            .Columns("GrdComponentTypeId_ReqId").AllowDBNull = False
            .Columns("NumberofTests").AllowDBNull = False
            .Columns("MinimumScore").AllowDBNull = False


            'set primary key
            '.PrimaryKey = New DataColumn() {.Columns("GrdComponentTypeId"), .Columns("ReqId")}
        End With

        strEffectiveDate = CType(txtEffectiveDate, RadDatePicker).DbSelectedDate

        Dim radTestDetailsGrid As RadGrid
        radTestDetailsGrid = DirectCast(RadPanelBar1.FindItemByValue("TopSpeedTestingCategory").FindControl("RadTestDetailsGrid"), RadGrid)

        'GetSelectedItems method brings in a collection of rows that were selected
        For Each dataItem As GridDataItem In radTestDetailsGrid.MasterTableView.Items
            'BR: If the rules to the datatable using LoadDataRow Method
            'Benefits:LoadDataRow method helps get control RowState for the new DataRow
            'First Parameter: Array of values, the items in the array correspond to columns in the table. 
            'Second Parameter: AcceptChanges, enables to control the value of the RowState property of the new DataRow.
            '                  Passing a value of False for this parameter causes the new row to have a RowState of Added
            strGrdComponentTypeId_ReqId = CType(dataItem.FindControl("txtGrdComponentTypeid_ReqId"), TextBox).Text.ToString

            'Number of Test required and Minimum Score required don't allow nulls so add the following validation
            If Not CType(dataItem.FindControl("txtNumberofTest"), eWorld.UI.NumericBox).Text.ToString = "" And _
                Not CType(dataItem.FindControl("txtMinimumScore"), eWorld.UI.NumericBox).Text.ToString = "" Then
                intNumberofTests = CType(dataItem.FindControl("txtNumberofTest"), eWorld.UI.NumericBox).Text.ToString
                decMinimumScore = CType(dataItem.FindControl("txtMinimumScore"), eWorld.UI.NumericBox).Text.ToString
                strModUser = Session("UserName")
                'tbl.LoadDataRow(New Object() {strGrdComponentTypeId_ReqId, Convert.ToDateTime(txtEffectiveDate.Text), intNumberofTests, decMinimumScore, strModUser, Convert.ToDateTime(strModDate)}, False)
                tbl.LoadDataRow(New Object() {strGrdComponentTypeId_ReqId, intNumberofTests, decMinimumScore, strModUser}, False)
            End If
        Next

        Dim ds As DataSet = New DataSet()
        ds.Tables.Add(tbl)
        Return ds
    End Function
    Private Sub AddRules() 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document
        Dim dsRules As New DataSet
        Dim grdFacade As New SetDictationTestRulesFacade
        dsRules = GetListofRules()
        If Not dsRules Is Nothing Then
            For Each lcol As DataColumn In dsRules.Tables(0).Columns
                lcol.ColumnMapping = System.Data.MappingType.Attribute
            Next
            Dim strXML As String = dsRules.GetXml

            Dim dtOldEffectiveDate As DateTime = #1/1/1900#
            If ViewState("btnState") = "new" Then ' if new button was clicked then dtOldEffectiveDate should carry default value 1/1/1900
                grdFacade.SetupRules(Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate), strXML, dtOldEffectiveDate)
                Exit Sub
            End If
            If Not txtOldEffectiveDate.Text.ToString.Trim = "" Then
                dtOldEffectiveDate = Convert.ToDateTime(txtOldEffectiveDate.Text)
            End If
            grdFacade.SetupRules(Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate), strXML, dtOldEffectiveDate)
            ShowStatusMessage("Record was saved successfully")
        End If
    End Sub
    Private Sub AddPrerequisites(Optional ByVal dtOldEffectiveDate As DateTime = #1/1/1900#)
        Dim intMinPerCategory As Integer = 0
        Dim intMaxPerCategory As Integer = 0
        Dim intMinPerCombination As Integer = 0
        Dim intMaxPerCombination As Integer = 0
        Dim strPreReqId As String = ""
        Dim boolMentorProctored As Boolean = False

        'intMinPerCategory = Convert.ToInt64(DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("txtMinCategory"), TextBox).Text)
        'intMaxPerCategory = Convert.ToInt64(DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("txtMaxCategory"), TextBox).Text)
        'intMinPerCombination = Convert.ToInt64(DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("txtMinCombination"), TextBox).Text)
        'intMaxPerCombination = Convert.ToInt64(DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("txtMaxCombination"), TextBox).Text)

        intMinPerCategory = Convert.ToInt64(DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCategory"), DropDownList).SelectedValue)
        intMaxPerCategory = Convert.ToInt64(DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCategory"), DropDownList).SelectedValue)
        intMinPerCombination = Convert.ToInt64(DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCombination"), DropDownList).SelectedValue)
        intMaxPerCombination = Convert.ToInt64(DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCombination"), DropDownList).SelectedValue)

        strPreReqId = DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlPrereq"), DropDownList).SelectedValue
        boolMentorProctored = DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMentor"), DropDownList).SelectedValue

        Dim grdFacade As New SetDictationTestRulesFacade
        If Not txtOldEffectiveDate.Text.ToString.Trim = "" Then
            dtOldEffectiveDate = Convert.ToDateTime(txtOldEffectiveDate.Text)
        End If

        If Not strPreReqId.Trim = "" Then
            grdFacade.SetupPreRequisites(Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate), txtReqId.Text, _
                                     intMinPerCategory, intMaxPerCategory, intMinPerCombination, intMaxPerCombination, _
                                     strPreReqId, boolMentorProctored, Session("UserName"), Date.Now.ToShortDateString, dtOldEffectiveDate)
        Else
            grdFacade.ResetPreRequisites(txtReqId.Text, Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate))
        End If
    End Sub
    Private Sub BindDataList()
        dlstEffectiveDate.DataSource = (New SetDictationTestRulesFacade).GetEffectiveDates(txtReqId.Text)
        dlstEffectiveDate.DataBind()
    End Sub
    Private Sub BindDataGridByEffectiveDate(ByVal EffectiveDate As DateTime, _
                                            ByVal ReqId As String)
        Dim radTestDetailsGrid As RadGrid
        radTestDetailsGrid = DirectCast(RadPanelBar1.FindItemByValue("TopSpeedTestingCategory").FindControl("RadTestDetailsGrid"), RadGrid)

        radTestDetailsGrid.DataSource = (New SetDictationTestRulesFacade).GetRulesByEffectiveDateAndCourse(EffectiveDate, ReqId)
        radTestDetailsGrid.DataBind()

    End Sub
    Protected Sub dlstEffectiveDate_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstEffectiveDate.ItemCommand
        Dim ds As New DataSet
        Dim strEffectiveDate As DateTime
        Dim objCommon As New CommonUtilities

        ViewState("btnState") = "edit"
        Try

            'Dim stopWatch As New Stopwatch()
            'stopWatch.Start()
            strEffectiveDate = e.CommandArgument
            CType(txtEffectiveDate, RadDatePicker).DbSelectedDate = strEffectiveDate

            'For Edit Purpose Need to keep track of the Old Effective Date
            txtOldEffectiveDate.Text = strEffectiveDate

            BindDataGridByEffectiveDate(strEffectiveDate, txtReqId.Text)

            BindPrerequisites(strEffectiveDate)

            BindMentorProctored(strEffectiveDate, txtReqId.Text)
            Dim strMentorText As String = ""
            strMentorText = FormatMentorRequirementString()
            If Not strMentorText.Trim = "" Then
                DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Visible = True
                DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblExample"), Label).Visible = False
                DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Text = "The following is the selected mentor/proctored test requirement: " & "<br><br>" & strMentorText
            Else
                DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Visible = False
                DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblExample"), Label).Visible = True
            End If
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstEffectiveDate, e.CommandArgument, ViewState, Nothing)
            HideStatusMessage()
            InitButtonsForEdit()

            'stopWatch.Stop()
            ' Get the elapsed time as a TimeSpan value.
            'Dim ts As TimeSpan = stopWatch.Elapsed
            '' Format and display the TimeSpan value.
            'Dim elapsedTime As String = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10)
            'lblTime.Text = elapsedTime
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstDegree_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstDegree_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub ShowStatusMessage(ByVal message As String)
        lblMessageStatus.Visible = True
        lblMessageStatus.Text = message
        'Dim scr As String = "<script language='javascript'>function f(){alert('Welcome to RadWindow ASP.NET AJAX!');}</script>"
        'Dim scriptstring As String = "radalert('Welcome to Rad<strong>Window</strong>!', 330, 210);"
        ' ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert1", scr, True)
        'Dim radalertscript As String = "<script language='javascript'>function f(){radalert('Welcome to RadWindow <strong>ASP.NET AJAX</strong>!', 330, 210); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>"
        'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", radalertscript, True)

    End Sub
    Private Sub HideStatusMessage()
        lblMessageStatus.Visible = False
    End Sub
    Private Sub BuildPrereqDDL()
        Dim ddlPrerequisites As DropDownList
        ddlPrerequisites = DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlPrereq"), DropDownList)

        With ddlPrerequisites
            .DataSource = (New GradesFacade).GetCCRCoursesByCampusIdandStatusId(campus, AdvantageCommonValues.ActiveGuid)
            .DataTextField = "Descrip"
            .DataValueField = "ReqId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
        End With
    End Sub
    Private Sub BuildCategoryCombinationDDL()
        Dim ddlMinCategory, ddlMaxCategory, ddlMinCombination, ddlMaxCombination As DropDownList
        Dim i As Integer
        Dim intMaxCategory, intMaxCombination As Integer

        ddlMinCategory = DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCategory"), DropDownList)
        ddlMaxCategory = DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCategory"), DropDownList)
        ddlMinCombination = DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCombination"), DropDownList)
        ddlMaxCombination = DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCombination"), DropDownList)

        Try
            intMaxCategory = MyAdvAppSettings.AppSettings("CCR_TopSpeedTestingCategory")
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            intMaxCategory = 10
        End Try
        Try
            intMaxCombination = MyAdvAppSettings.AppSettings("CCR_TopSpeedTestingCombination")
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            intMaxCombination = 10
        End Try

        With ddlMinCategory
            .Items.Insert(0, New ListItem("Select", 0))
            For i = 1 To intMaxCategory
                .Items.Insert(i, New ListItem(i, i))
            Next
        End With

        With ddlMaxCategory
            .Items.Insert(0, New ListItem("Select", 0))
            For i = 1 To intMaxCategory
                .Items.Insert(i, New ListItem(i, i))
            Next
        End With

        With ddlMinCombination
            .Items.Insert(0, New ListItem("Select", 0))
            For i = 1 To intMaxCombination
                .Items.Insert(i, New ListItem(i, i))
            Next
        End With

        With ddlMaxCombination
            .Items.Insert(0, New ListItem("Select", 0))
            For i = 1 To intMaxCombination
                .Items.Insert(i, New ListItem(i, i))
            Next
        End With
    End Sub
    Private Sub BindPrerequisites(ByVal dtEffectiveDate As Date)
        Dim grdFacade As New SetDictationTestRulesFacade
        Dim ds As New DataSet
        ds = (New SetDictationTestRulesFacade).GetPreRequisitesByEffectiveDateAndCourse(dtEffectiveDate, txtReqId.Text)

        Dim dr As DataRow
        If Not ds Is Nothing Then
            Try
                dr = ds.Tables(0).Rows(0)
                If Not dr("MinperCategory") Is System.DBNull.Value Then DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCategory"), DropDownList).SelectedValue = dr("MinperCategory")
                If Not dr("MaxperCategory") Is System.DBNull.Value Then DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCategory"), DropDownList).SelectedValue = dr("MaxperCategory")
                If Not dr("MinperCombination") Is System.DBNull.Value Then DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCombination"), DropDownList).SelectedValue = dr("MinperCombination")
                If Not dr("MaxperCombination") Is System.DBNull.Value Then DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCombination"), DropDownList).SelectedValue = dr("MaxperCombination")
                If Not dr("PreReqId") Is System.DBNull.Value Then DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlPreReq"), DropDownList).SelectedValue = CType(dr("PreReqId"), Guid).ToString
                If Not dr("mentorproctored") Is System.DBNull.Value Then
                    If dr("mentorproctored") = True Then
                        DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMentor"), DropDownList).SelectedValue = 1
                    Else
                        DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMentor"), DropDownList).SelectedValue = 0
                    End If
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCategory"), DropDownList).SelectedIndex = 0
                DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCategory"), DropDownList).SelectedIndex = 0
                DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCombination"), DropDownList).SelectedIndex = 0
                DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCombination"), DropDownList).SelectedIndex = 0
                DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlPreReq"), DropDownList).SelectedIndex = 0
                DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMentor"), DropDownList).SelectedValue = 0
            End Try
        End If
    End Sub
    Private Sub ClearPreRequisitesSection()
        DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCategory"), DropDownList).SelectedIndex = 0
        DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCategory"), DropDownList).SelectedIndex = 0
        DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMinCombination"), DropDownList).SelectedIndex = 0
        DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMaxCombination"), DropDownList).SelectedIndex = 0
        DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlMentor"), DropDownList).SelectedIndex = 0
        DirectCast(RadPanelBar1.FindItemByValue("Prerequisites").FindControl("ddlPreReq"), DropDownList).SelectedIndex = 0
    End Sub
    Private Function SetupMentorProctored() As DataSet
        Dim radTestDetailsGrid As RadGrid
        radTestDetailsGrid = DirectCast(RadPanelBar1.FindItemByValue("TopSpeedTestingCategory").FindControl("RadTestDetailsGrid"), RadGrid)
        Dim intRowCount As Integer = radTestDetailsGrid.Items.Count
        Dim intLoop As Integer
        Dim pnlMentor As Panel
        Dim dynLabel As Label = Nothing
        Dim dynDropDownList As DropDownList = Nothing
        Dim dynOperatorDropDownList As DropDownList = Nothing
        Dim strMentorText As String = ""
        Dim strMentorOr As String = ""
        Dim strMentorRequirement As String = ""

        pnlMentor = DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("pnlMentor"), Panel)


        Dim tblMentor As New DataTable("SetupMentor")
        With tblMentor
            'Define table schema
            .Columns.Add("MentorId", GetType(System.Guid))
            .Columns.Add("MentorRequirement", GetType(System.String))
            .Columns.Add("MentorOperator", GetType(System.String))
            .Columns.Add("GrdComponentTypeId", GetType(System.Guid))
            .Columns.Add("Speed", GetType(System.Decimal))
            .Columns.Add("ReqId", GetType(System.Guid))
            .Columns.Add("OperatorSequence", GetType(System.Int32))
            .Columns.Add("ModUser", GetType(System.String))
            .Columns.Add("ModDate", GetType(System.DateTime))
            'set field properties
            .Columns("MentorRequirement").AllowDBNull = False
            .Columns("MentorOperator").AllowDBNull = False

        End With

        For intLoop = 1 To intRowCount
            Dim ctrlDDLName As String = "ddl" + intLoop.ToString
            Dim ctrlDDLOperator As String = "ddlOperator" + intLoop.ToString
            dynDropDownList = DirectCast(pnlMentor.FindControl(ctrlDDLName), DropDownList)
            dynOperatorDropDownList = DirectCast(pnlMentor.FindControl(ctrlDDLOperator), DropDownList)
            If Not dynOperatorDropDownList Is Nothing Then
                If Not dynDropDownList.SelectedValue = "" AndAlso dynOperatorDropDownList.SelectedValue = "" Then
                    dynOperatorDropDownList.SelectedValue = "or "
                End If
                Dim instrColon As Integer = 0
                If Not dynDropDownList.SelectedValue = "" Then
                    instrColon = InStr(dynDropDownList.SelectedValue, ":")
                    If instrColon >= 1 Then
                        Dim GrdComponentTypeId As String = dynDropDownList.SelectedValue.Substring(0, instrColon - 1)
                        Dim speed As Decimal = dynDropDownList.SelectedValue.Substring(instrColon)
                        tblMentor.LoadDataRow(New Object() {System.Guid.NewGuid, dynDropDownList.SelectedItem.Text, dynOperatorDropDownList.SelectedValue, GrdComponentTypeId, speed, txtReqId.Text, intLoop}, False)
                    End If
                End If
            Else
                If Not dynDropDownList.SelectedValue = "" Then
                    Dim instrColon As Integer = 0
                    If Not dynDropDownList.SelectedValue = "" Then
                        instrColon = InStr(dynDropDownList.SelectedValue, ":")
                        If instrColon >= 1 Then
                            Dim GrdComponentTypeId As String = dynDropDownList.SelectedValue.Substring(0, instrColon - 1)
                            Dim speed As Decimal = dynDropDownList.SelectedValue.Substring(instrColon)
                            tblMentor.LoadDataRow(New Object() {System.Guid.NewGuid, dynDropDownList.SelectedItem.Text, "Select", GrdComponentTypeId, speed, txtReqId.Text, intLoop}, False)
                        End If
                    End If
                End If
            End If
        Next
        Dim ds As DataSet = New DataSet()
        If tblMentor.Rows.Count >= 1 Then
            ds.Tables.Add(tblMentor)
        End If
        Return ds
    End Function
    Private Sub AddMentorProctored(Optional ByVal dtOldEffectiveDate As DateTime = #1/1/1900#) 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document
        Dim dsRules As New DataSet
        Dim grdFacade As New SetDictationTestRulesFacade
        dsRules = SetupMentorProctored()
        If Not dsRules Is Nothing Then
            Try
                Try
                    If dsRules.Tables(0).Rows.Count = 0 Then
                        grdFacade.ResetMentorRequirement(txtReqId.Text, Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate))
                        DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Visible = False
                        DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblExample"), Label).Visible = True
                        Exit Sub
                    End If
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    grdFacade.ResetMentorRequirement(txtReqId.Text, Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate))
                    DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Visible = False
                    DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblExample"), Label).Visible = True
                    Exit Sub
                End Try

                For Each lcol As DataColumn In dsRules.Tables(0).Columns
                    lcol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                Dim strXML As String = dsRules.GetXml
                Dim strModUser As String = Session("UserName")
                Dim strModDate As DateTime = Date.Now
                If ViewState("btnState") = "new" Then ' if new button was clicked then dtOldEffectiveDate should carry default value 1/1/1900
                    grdFacade.SetupMentorProctoredTestRequirement(Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate), strXML, dtOldEffectiveDate, strModUser, strModDate)
                    Exit Sub
                End If
                If Not txtOldEffectiveDate.Text.ToString.Trim = "" Then
                    dtOldEffectiveDate = Convert.ToDateTime(txtOldEffectiveDate.Text)
                End If
                grdFacade.SetupMentorProctoredTestRequirement(Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate), strXML, dtOldEffectiveDate, strModUser, strModDate)
                Dim strMentorText As String = ""
                strMentorText = FormatMentorRequirementString()
                If Not strMentorText.Trim = "" Then
                    DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Visible = True
                    DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblExample"), Label).Visible = False
                    DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Text = ""
                    DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Text = "The following is the selected mentor/proctored test requirement: " & "<br><br>" & strMentorText
                Else
                    DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Visible = False
                    DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblExample"), Label).Visible = True
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End If
    End Sub
    Private Function FormatMentorRequirementString() As String
        Dim dsMentorProctored As New DataSet
        Dim strMentorText As String = ""
        Dim intOperatorLoop As Integer = 0
        Dim strPreviousOperator As String = ""
        dsMentorProctored = (New SetDictationTestRulesFacade).GetMentorRequirementText(Convert.ToDateTime(CType(txtEffectiveDate, RadDatePicker).DbSelectedDate), txtReqId.Text)
        If Not dsMentorProctored Is Nothing AndAlso dsMentorProctored.Tables(0).Rows.Count >= 1 Then
            strMentorText = "("
            For Each drReq As DataRow In dsMentorProctored.Tables(0).Rows
                If Not drReq("MentorOperator").ToString.ToLower.Trim = "select" Then
                    If drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "or" Then
                        strMentorText &= drReq("MentorRequirement")   'If the operator is AND, then the text will be Text1
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator.Trim = "" Then
                        strMentorText &= drReq("MentorRequirement")   'If the operator is AND, then the text will be Text1
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso Not strPreviousOperator = "or" Then
                        strMentorText &= "(" & drReq("MentorRequirement") & ")"  'If the operator is AND, then the text will be (Text1)
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "or" AndAlso Not strPreviousOperator = "and" Then
                        strMentorText &= "(" & drReq("MentorRequirement") & ")"  'If the operator is AND, then the text will be (Text1)
                    Else
                        strMentorText &= drReq("MentorRequirement") 'if Operator is OR, then text will be just Text1
                    End If
                    If drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "or" Then
                        strMentorText = strMentorText & ")  " & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "or" AndAlso strPreviousOperator = "and" Then
                        strMentorText = strMentorText & ")  " & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
                    ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "and" AndAlso strPreviousOperator = "and" Then
                        strMentorText = strMentorText & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
                    Else
                        strMentorText = strMentorText & " " & "<font color=""red"">" & drReq("MentorOperator").ToString.ToUpper & "</font>  "
                    End If
                End If
                If drReq("MentorOperator").ToString.ToLower.Trim = "select" AndAlso strPreviousOperator = "and" Then
                    strMentorText = strMentorText & " (" & drReq("MentorRequirement") & ")"
                ElseIf drReq("MentorOperator").ToString.ToLower.Trim = "select" AndAlso strPreviousOperator = "or" Then
                    strMentorText = strMentorText & " (" & drReq("MentorRequirement") & ")"
                End If
                strPreviousOperator = drReq("MentorOperator").ToString.ToLower.Trim
            Next
            Return strMentorText
        End If
        Return strMentorText
    End Function
    Private Sub BindMentorProctored(ByVal dtEffectiveDate As Date, ByVal ReqId As String)
        Dim radTestDetailsGrid As RadGrid
        radTestDetailsGrid = DirectCast(RadPanelBar1.FindItemByValue("TopSpeedTestingCategory").FindControl("RadTestDetailsGrid"), RadGrid)
        Dim intRowCount As Integer = radTestDetailsGrid.Items.Count
        Dim intLoop As Integer
        Dim pnlMentor As Panel
        Dim dynLabel As Label = Nothing
        Dim dynDropDownList As DropDownList = Nothing
        Dim dynOperatorDropDownList As DropDownList = Nothing
        pnlMentor = DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("pnlMentor"), Panel)

        Dim ds As New DataSet
        Try
            ds = (New SetDictationTestRulesFacade).BindMentorProctoredTestRequirement(dtEffectiveDate, ReqId)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ds = Nothing
        End Try

        For intLoop = 1 To intRowCount
            Dim ctrlDDLName As String = "ddl" + intLoop.ToString
            Dim ctrlDDLOperator As String = "ddlOperator" + intLoop.ToString
            dynDropDownList = DirectCast(pnlMentor.FindControl(ctrlDDLName), DropDownList)
            dynOperatorDropDownList = DirectCast(pnlMentor.FindControl(ctrlDDLOperator), DropDownList)
            SelectMentorRequirementValues(dynDropDownList, dynOperatorDropDownList, ds, intLoop)
        Next
        DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblUserSelection"), Label).Visible = False
        DirectCast(RadPanelBar1.FindItemByValue("Mentor").FindControl("lblExample"), Label).Visible = True
    End Sub
    Private Sub SelectMentorRequirementValues(ByRef ddlMentorRequirementList As DropDownList, _
                                              ByRef ddlMentorOperatorList As DropDownList, _
                                              ByVal ds As DataSet, _
                                              ByVal intLoopCounter As Integer)

        If Not ddlMentorOperatorList Is Nothing Then
            If Not ds Is Nothing Then
                Try
                    ddlMentorRequirementList.SelectedValue = ds.Tables(0).Rows(intLoopCounter - 1)("MentorValue")
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlMentorRequirementList.SelectedIndex = 0
                End Try

                Try
                    ddlMentorOperatorList.SelectedValue = ds.Tables(0).Rows(intLoopCounter - 1)("MentorOperator")
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlMentorOperatorList.SelectedIndex = 0
                End Try
            Else
                ddlMentorRequirementList.SelectedIndex = 0
                ddlMentorOperatorList.SelectedIndex = 0
            End If
        Else
            If Not ds Is Nothing Then
                Try
                    ddlMentorRequirementList.SelectedValue = ds.Tables(0).Rows(intLoopCounter - 1)("MentorValue")
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlMentorRequirementList.SelectedIndex = 0
                End Try
            Else
                ddlMentorRequirementList.SelectedIndex = 0
            End If
        End If
    End Sub
End Class
