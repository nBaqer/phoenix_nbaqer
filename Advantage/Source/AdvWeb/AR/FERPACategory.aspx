<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="FERPACategory.aspx.vb" Inherits="FERPACategory" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>

    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstFERPACat" runat="server" DataKeyField="FERPACategoryID" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("FERPACategoryID")%>' Text='<%# container.dataitem("FERPACategoryDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <!-- begin table content-->
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCode" runat="server" CssClass="label">Code<font color=red>*</font></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" TabIndex="1" runat="server" CssClass="textbox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvStudent" runat="server" Display="None" ErrorMessage="Must Enter the Code" ControlToValidate="txtCode"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblBankDescrip" runat="server" CssClass="label">Description<font color=red>*</font></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtFERPADescrip" TabIndex="2" runat="server" CssClass="textbox" MaxLength="100"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="None" ErrorMessage="Must Enter the Description" ControlToValidate="txtFERPADescrip"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label">Status<font color=red>*</font></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" TabIndex="3" runat="server" CssClass="dropdownlist"></asp:DropDownList><asp:CompareValidator ID="StatusCompareValidator" runat="server" Display="None" ErrorMessage="Must Select a Status"
                                                    ControlToValidate="ddlStatusId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Status</asp:CompareValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label">Campus Group<font color=red>*</font></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" TabIndex="4" runat="server" CssClass="dropdownlist"></asp:DropDownList><asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" Display="None" ErrorMessage="Must Select a Campus Group"
                                                    ControlToValidate="ddlCampGrpId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Campus Group</asp:CompareValidator></td>
                                        </tr>



                                    </table>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="left">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" nowrap colspan="8">
                                                <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="label" Style="text-align: left;">Select Student page(s) for FERPA</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="leadcell2" colspan="5">
                                                <asp:CheckBoxList ID="ChkFERPAPages" TabIndex="5" runat="Server" RepeatColumns="4" CssClass="checkboxstyle"></asp:CheckBoxList></td>

                                        </tr>
                                    </table>
                                    <p></p>
                                    <p></p>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td colspan="5" style="white-space: nowrap;">
                                                <asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="label" Style="text-align: left;">
													Note: Some of the student pages may not apply to your particular school
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>



                                    <!--end table content-->
                                </div>

                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtFERPAId" runat="server" Visible="false"></asp:TextBox><asp:CheckBox ID="chkIsInDB" runat="server" Visible="false"></asp:CheckBox>
        <asp:TextBox ID="txtOldCampGrpId" runat="server" Visible="false"></asp:TextBox>
        <!--end validation panel-->
    </div>

</asp:Content>


