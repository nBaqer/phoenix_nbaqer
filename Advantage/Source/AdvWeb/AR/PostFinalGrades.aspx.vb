﻿Imports System.Collections
Imports System.Data
Imports System.Diagnostics
Imports System.Drawing
Imports System.IO
Imports System.Runtime.CompilerServices
Imports System.Xml
Imports Advantage.Business.Objects
Imports Fame.Advantage.Api.Library.AcademicRecords
Imports Fame.Advantage.Api.Library.Models
Imports Fame.Advantage.Api.Library.Models.Common
Imports Fame.Advantage.Common
Imports Fame.Advantage.Domain.MultiTenant.Infrastructure.API
Imports Fame.AdvantageV1.BusinessFacade
Imports Fame.AdvantageV1.Common
Imports NHibernate.Util
Imports Telerik.Web.UI
Imports Telerik.Web.UI.Calendar

Namespace AdvWeb.AR

    Partial Class PostFinalGrades
        Inherits BasePage
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected MyAdvAppSettings As AdvAppSettings

        Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme

        End Sub
        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private pObj As New UserPagePermissionInfo
        Public Class GlobalVariables
            Public Shared StudentEnrollmentsList As List(Of Guid) = New List(Of Guid)()
        End Class
        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Put user code to initialize the page here
            Dim campusId As String
            Dim resourceId As Integer


            Dim advantageUserState As User = AdvantageSession.UserState

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            campusId = Master.CurrentCampusId
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If


            'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
            ''Added by Saraswathi Lakshmanan
            ''If the UseCohortStartDateforfilter is set to Yes in WebConfigAppSettings 
            ''then the Term DropDownList is made visible false and CohortStartdateDropDown 
            ''is made visible true

            If MyAdvAppSettings.AppSettings("UseCohortStartDateForFilters").ToString.ToLower = "yes" Then
                trTerm.Visible = False
                trCohortStartDate.Visible = True
            Else
                trTerm.Visible = True
                trCohortStartDate.Visible = False
            End If

            If Not Page.IsPostBack Then
                'Populate the Activities Data-grid with the person who is logged in Activities
                'Default the Selection Criteria for the activity records
                ViewState("Term") = ""
                ViewState("Class") = ""
                ViewState("GrdCriteria") = ""
                ViewState("MODE") = "NEW"
                ViewState("ClsSectIdGuid") = ""
                ViewState("SelectedIndex") = "-1"
                Session("UserId") = Session("UserId")
                Session("UserRole") = "Academic Advisors"

                If Session("UserRole") = "Instructor" Then
                    ddlInstructor.SelectedItem.Value = CType(Session("UserId"), String)
                    ddlInstructor.Enabled = False
                ElseIf Session("UserRole") = "Academic Advisors" Then
                    ddlInstructor.Enabled = True
                End If

                PopulateTermDDL()
                ''CohortStartDate DDL 
                PopulateCohortStartDateDDL()

                ViewState("MODE") = "NEW"

                ''dgdFinalGrades.Columns(1).HeaderText = SingletonAppSettings.AppSettings("StudentIdentifier")
                dgdFinalGrades.Columns(2).HeaderText = CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String)
                ''''''

                dgdFinalGrades.Visible = False
                btnExportToExcell.Visible = False
                ' tblLegend.Visible = False
            End If
            headerTitle.Text = Header.Title
        End Sub
        Private Sub PopulateTermDDL()
            '
            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New PostFinalGrdsFacade

            ' Bind the Dataset to the CheckBoxList
            With ddlTerm
                .DataSource = facade.GetCurrentAndPastTerms(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTerm.SelectedIndexChanged
            ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
            If (ViewState("Term") <> "") Then
                PopulateInstructorDDL()
                'Unselect the class dropdown upon changing the term filter
                If ddlClsSection.Items.Count <> 0 Then
                    ddlClsSection.SelectedIndex = 0
                End If
            End If
            Dim programRegistrationType As Boolean = False
            Dim termsFacade As New PostFinalGrdsFacade
            Dim termsDs As DataSet = termsFacade.GetCurrentAndPastTerms(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)

            If (termsDs.Tables.Count > 0) Then
                If (termsDs.Tables(0).Rows.Count > 0) Then
                    If (Not termsDs.Tables(0).Rows(ddlTerm.SelectedIndex - 1).Item("ProgramRegistrationType") Is DBNull.Value) Then
                        programRegistrationType = CType(termsDs.Tables(0).Rows(ddlTerm.SelectedIndex - 1).Item("ProgramRegistrationType"), Boolean)
                    End If
                End If
            End If

            If (programRegistrationType) then 
                PopulateClsSectionDDL()
            End If

            dgdFinalGrades.Visible = False
            btnExportToExcell.Visible = False
            'tblLegend.Visible = False
        End Sub
        Private Sub ddlInstructor_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlInstructor.SelectedIndexChanged
            ViewState("Instructor") = ddlInstructor.SelectedItem.Value.ToString()

            If (ViewState("Instructor") <> "") Then
                If trTerm.Visible = True Then
                    PopulateClsSectionDDL()
                ElseIf trCohortStartDate.Visible = True Then
                    PopulateClsSectionDdLbasedonCohortStDate(CType(ViewState("CohortStartDate"), String))
                End If

            Else
                'Unselected the class drop down upon un-selecting a value from the instructor filter
                If ddlClsSection.Items.Count <> 0 Then
                    ddlClsSection.SelectedIndex = 0
                End If
            End If
            dgdFinalGrades.Visible = False
            btnExportToExcell.Visible = False
            'tblLegend.Visible = False
        End Sub
        Private Sub PopulateClsSectionDDL()
            '
            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New PostFinalGrdsFacade
            Dim campusId As String

            campusId = HttpContext.Current.Request.Params("cmpid")

            Dim instructor as string = string.Empty
            if (Not ViewState("Instructor") Is Nothing )
                instructor = CType(ViewState("Instructor"), String)
            End If

            ' Bind the Dataset to the CheckBoxList
            With ddlClsSection
                .DataSource = facade.GetClsSections(CType(ViewState("Term"), String), instructor, campusId)
                .DataTextField = "CourseSectDescrip"
                .DataValueField = "ClsSectionId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub PopulateInstructorDDL()
            '
            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New PostFinalGrdsFacade
            Dim campusId As String

            campusId = HttpContext.Current.Request.Params("cmpid")

            ' Bind the Dataset to the CheckBoxList
            With ddlInstructor
                .DataSource = facade.GetInstructors(CType(ViewState("Term"), String), campusId)
                .DataTextField = "FullName"
                .DataValueField = "InstructorId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
            Dim facade As New PostFinalGrdsFacade
            Dim ds As DataSet

            If ddlTerm.SelectedItem.Text = "Select" And ddlcohortStDate.SelectedItem.Text = "Select" Then
                DisplayErrorMessage("Unable to find data. Please select an option.")
                Exit Sub
            ElseIf ddlTerm.SelectedItem.Text <> "Select" Then
                ViewState("Term") = ddlTerm.SelectedItem.Value.ToString
            ElseIf ddlcohortStDate.SelectedItem.Text <> "Select" Then
                ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
            End If

            Dim termsFacade As New PostFinalGrdsFacade
            Dim programRegistrationType As Boolean = False

            Dim termsDs As DataSet = termsFacade.GetCurrentAndPastTerms(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)

            If (termsDs.Tables.Count > 0) Then
                If (termsDs.Tables(0).Rows.Count > 0) Then
                    If (Not termsDs.Tables(0).Rows(ddlTerm.SelectedIndex - 1).Item("ProgramRegistrationType") Is DBNull.Value) Then
                        programRegistrationType = CType(termsDs.Tables(0).Rows(ddlTerm.SelectedIndex - 1).Item("ProgramRegistrationType"), Boolean)
                    End If
                End If
            End If


            If ddlInstructor.SelectedItem.Text = "Select" Then
                If (programRegistrationType = False) Then
                    DisplayErrorMessage("Unable to find data. Please select an option from Instructor")
                    Exit Sub
                End If
            Else
                ViewState("Instructor") = ddlInstructor.SelectedItem.Value.ToString
            End If

            If ddlClsSection.SelectedItem.Text = "Select" Then
                DisplayErrorMessage("Unable to find data. Please select an option from Class Section")
                Exit Sub
            Else
                ViewState("ClsSection") = ddlClsSection.SelectedItem.Value.ToString
            End If

            ds = facade.GetStdInfoFrDatagrid(CType(ViewState("ClsSection"), String), AdvantageSession.UserId.ToString())

            ViewState("Count") = 0

            If ds.Tables("ClsSectStds").Rows.Count > 0 Then
                Session("ClsSectStds") = ds.Tables("ClsSectStds")
                Session("ClsSectGrds") = ds.Tables("ClsSectGrds")

                dgdFinalGrades.Visible = True
                btnExportToExcell.Visible = True
                ' tblLegend.Visible = True

                If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
                    btnSave.Enabled = True
                End If


                With dgdFinalGrades
                    .DataSource = ds.Tables("ClsSectStds")
                    .DataBind()
                End With
            Else
                dgdFinalGrades.Visible = False
                btnExportToExcell.Visible = False
                'tblLegend.Visible = False
                If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
                    btnSave.Enabled = True
                End If
                DisplayErrorMessage("Unable to find a registered student for this Class Section")
            End If
        End Sub
        Private Sub GetFinalGrades()
            Dim facade As New PostFinalGrdsFacade
            Dim ds As DataSet
            ds = facade.GetStdInfoFrDatagrid(CType(ViewState("ClsSection"), String), AdvantageSession.UserId.ToString())

            ViewState("Count") = 0

            If ds.Tables("ClsSectStds").Rows.Count > 0 Then
                Session("ClsSectStds") = ds.Tables("ClsSectStds")
                Session("ClsSectGrds") = ds.Tables("ClsSectGrds")

                dgdFinalGrades.Visible = True
                btnExportToExcell.Visible = True
                'tblLegend.Visible = True
                If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
                    btnSave.Enabled = True
                End If


                With dgdFinalGrades
                    .DataSource = ds.Tables("ClsSectStds")
                    .DataBind()
                End With
            Else
                dgdFinalGrades.Visible = False
                btnExportToExcell.Visible = False
                'tblLegend.Visible = False
                If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
                    btnSave.Enabled = True
                End If
                DisplayErrorMessage("Unable to find a registered student for this Class Section")
            End If
        End Sub


        Private Sub DisplayErrorMessage(ByVal errorMessage As String)

            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End Sub

        Protected Sub dgdFinalGrades_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgdFinalGrades.ItemCreated
            Dim oControl As Control
            If MyAdvAppSettings Is Nothing Then
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If
            End If
            For Each oControl In dgdFinalGrades.Controls(0).Controls
                If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
                    ''''' Code changes by Kamalesh Ahuja on 09/08/2010 to resolve mantis issue id 07251''
                    'If SingletonAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    '    CType(oControl, DataGridItem).Cells(3).Visible = False
                    'Else
                    '    CType(oControl, DataGridItem).Cells(2).Visible = False
                    'End If
                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        CType(oControl, DataGridItem).Cells(4).Visible = False
                    Else
                        CType(oControl, DataGridItem).Cells(3).Visible = False
                    End If
                End If
                If (CType(oControl, DataGridItem).ItemType = ListItemType.Item Or
                    CType(oControl, DataGridItem).ItemType = ListItemType.AlternatingItem) Then

                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        CType(oControl, DataGridItem).Cells(4).Visible = False
                    Else
                        CType(oControl, DataGridItem).Cells(3).Visible = False
                    End If
                End If
            Next
        End Sub
        Private Sub dgdFinalGrades_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgdFinalGrades.ItemDataBound
            Dim ddl As DropDownList
            Dim tbl1 As DataTable
            Dim tbl2 As DataTable
            Dim count As Integer

            tbl1 = CType(Session("ClsSectStds"), DataTable)
            tbl2 = CType(Session("ClsSectGrds"), DataTable)
            count = CInt(ViewState("Count"))

            'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            '    Dim lbl As Label = CType(e.Item.FindControl("StatusId"), Label)
            '    If Not lbl.Text = "True" Then
            '        e.Item.BackColor = Color.LightGray
            '    End If
            'End If
            '''''''''''''

            If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                Select Case e.Item.ItemType
                    Case ListItemType.Item, ListItemType.AlternatingItem
                        count += 1
                        ViewState("Count") = count.ToString
                        ddl = CType(e.Item.FindControl("ddlGrade"), DropDownList)
                        ddl.Visible = False
                        Dim txt As TextBox = CType(e.Item.FindControl("txtScore"), TextBox)
                        If tbl1.Rows.Count > 0 Then
                            With tbl2
                                .PrimaryKey = New DataColumn() { .Columns("StuEnrollId")}
                            End With
                            'For Each row In tbl1.Rows
                            If (tbl2.Rows.Count > 0) Then
                                Dim row2 As DataRow = tbl2.Rows.Find(tbl1.Rows(count - 1)("StuEnrollId"))
                                If Not (row2 Is Nothing) Then

                                    If Not row2("Score") Is DBNull.Value Then
                                        If MyAdvAppSettings.AppSettings("GradeRounding").ToLower() = "yes" Then
                                            txt.Text = Math.Round(CType(row2("Score"), Decimal)).ToString
                                        Else
                                            txt.Text = CType(row2("Score"), Decimal).ToString
                                        End If
                                    Else

                                        txt.Text = ""
                                    End If
                                End If
                            End If
                            Dim txtGrd As Label = CType(e.Item.FindControl("txtGrade"), Label)
                            txtGrd.Text = txt.Text
                            'Next
                        End If
                End Select
            Else
                Select Case e.Item.ItemType
                    Case ListItemType.Item, ListItemType.AlternatingItem
                        count += 1
                        ViewState("Count") = count.ToString
                        ddl = CType(e.Item.FindControl("ddlGrade"), DropDownList)

                        If tbl1.Rows(count - 1)("IsContinuingEd") = True Then
                            BuildContinuingEDGradesDDL(ddl, tbl1.Rows(count - 1)("PrgVerId").ToString)
                        Else
                            BuildGradesDDL(ddl)
                        End If

                        'Select DDL if grade exists for student in "ClsSectStds" datatable.
                        If tbl1.Rows.Count > 0 Then
                            'Load a datatable with ChildId, ChildTyp and ChildDescrip
                            'Create a datatable with a new column called ChildId, make this the primary key.
                            'Make the ChildId column the primary key
                            With tbl2
                                .PrimaryKey = New DataColumn() { .Columns("StuEnrollId")}
                            End With
                            'For Each row In tbl1.Rows

                            If (tbl2.Rows.Count > 0) Then
                                Dim row2 As DataRow = tbl2.Rows.Find(tbl1.Rows(count - 1)("StuEnrollId"))

                                If Not (row2 Is Nothing) Then
                                    'loop thru the items in the ddl, find the particular 
                                    'grade and then select it.
                                    Dim item As ListItem
                                    'In For Loop Check The Number of Items Selected For Reassign Admission Reps
                                    For Each item In ddl.Items
                                        If (item.Value) = row2("GrdSysDetailId").ToString Then
                                            If Not row2("GrdSysDetailId") Is DBNull.Value Then ddl.SelectedValue = row2("GrdSysDetailId").ToString Else ddl.SelectedValue = ""
                                        End If
                                    Next
                                    Dim txtGrd As Label = CType(e.Item.FindControl("txtGrade"), Label)
                                    txtGrd.Text = ddl.SelectedValue
                                End If
                            End If
                            'Next
                        End If
                End Select
            End If
        End Sub

        Private Sub BuildGradesDDL(ByVal ddl As DropDownList)
            Dim ddlDS As DataSet
            '   save TuitionCategories in the View state for subsequent use
            If ViewState("GradesDS") Is Nothing Then
                ddlDS = (New PostFinalGrdsFacade).GetAllGrades(CType(ViewState("ClsSection"), String))
                ViewState("GradesDS") = ddlDS
            Else
                ddlDS = CType(ViewState("GradesDS"), DataSet)
            End If
            '   bind the TuitionCategories DDL
            With ddl
                .DataTextField = "Grade"
                .DataValueField = "GrdSysDetailId"
                .DataSource = ddlDS
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                '.SelectedIndex = 0
            End With
        End Sub
        Private Sub BuildContinuingEDGradesDDL(ByVal ddl As DropDownList, ByVal prgVerId As String)
            Dim ddlDS As DataSet
            '   save TuitionCategories in the Viewstate for subsequent use

            ddlDS = (New PostFinalGrdsFacade).GetContinuingEdGrades(prgVerId)

            '   bind the TuitionCategories DDL
            With ddl
                .DataTextField = "Grade"
                .DataValueField = "GrdSysDetailId"
                .DataSource = ddlDS
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                '.SelectedIndex = 0
            End With
        End Sub
        Private Sub ddlClsSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlClsSection.SelectedIndexChanged
            ViewState("ClsSection") = ddlClsSection.SelectedItem.Value.ToString()
        End Sub
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            '   if GrdSystem is nothing do an insert. Else do an update
            'Dim iitems As DataGridItemCollection
            'Dim iitem As DataGridItem
            'Dim i As Integer
            'Dim sId As String
            'Dim sGrade As String
            'Dim decScore As Decimal = CType(0.0, Decimal)
            'Dim finalGradeObject As FinalGradeInfo
            'Dim errStr As String = ""
            'Dim facade As New PostFinalGrdsFacade
            'Dim scoreIsNull As Boolean = False

            Try
                'Create the list of screen
                Dim listScreenOperations As List(Of GrdRecordsPoco) = New List(Of GrdRecordsPoco)()
                Dim classId As String = ddlClsSection.SelectedValue.ToString()
                Dim isNumeric As Boolean = (MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric")

                Dim itemsList As List(Of DataGridItem) = New List(Of DataGridItem)

                For Each item As DataGridItem In dgdFinalGrades.Items
                    itemsList.Add(item)
                Next
                If (Not (itemsList.Any(Function(x As DataGridItem) CType(x.FindControl("dateCompletedDpCustomValidator"), CustomValidator).IsValid))) Then
                    Return
                End If
                For Each item As DataGridItem In dgdFinalGrades.Items

                    Dim scoreText = CType(item.FindControl("txtScore"), TextBox).Text
                    Dim gradetext = String.Empty
                    Dim previousScore = CType(item.FindControl("txtGrade"), Label).Text
                    Dim dcDP = CType(item.FindControl("dateCompletedDP"), RadDatePicker)
                    Dim ds As Date
                    If (dcDP.SelectedDate.HasValue) Then
                        ds = dcDP.SelectedDate.Value
                    Else
                        ds = dcDP.MinDate
                    End If
                    Dim stuEnrollId = CType(item.FindControl("txtStuEnrollId"), TextBox).Text
                    Dim studentData = CType(Session("ClsSectStds"), DataTable)

                    Dim studentRow As DataRow = studentData.Select("StuEnrollId = '" & stuEnrollId & "'").FirstOrDefault()
                    Dim previousCompletedDate As Date
                    If Not IsDBNull(studentRow("DateCompleted")) Then
                        previousCompletedDate = CType(studentRow("DateCompleted"), Date)
                    End If
                    ' Analyze if some change in values, if not does not save the entity
                    If isNumeric Then
                        If String.IsNullOrWhiteSpace(previousScore) And String.IsNullOrWhiteSpace(scoreText) Then Continue For
                        If previousScore = scoreText AndAlso previousCompletedDate = ds Then Continue For
                    Else
                        gradetext = CType(item.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString
                        If String.IsNullOrWhiteSpace(previousScore) And String.IsNullOrWhiteSpace(gradetext) Then Continue For
                        If previousScore = gradetext AndAlso previousCompletedDate = ds Then Continue For
                    End If
                    Dim poco As GrdRecordsPoco = New GrdRecordsPoco()
                    poco.StuEnrollId = CType(item.FindControl("txtStuEnrollId"), TextBox).Text

                    poco.Score = Nothing
                    Dim dScore As Decimal
                    Decimal.TryParse(scoreText, dScore)
                    If dScore > 0 Then
                        poco.Score = dScore.ToString("F")
                    End If

                    poco.UserName = AdvantageSession.UserState.UserName
                    poco.ClsSectionId = classId
                    poco.Grade = gradetext
                    poco.IsFormatNumeric = isNumeric
                    poco.DateCompleted = ds

                    listScreenOperations.Add(poco)
                    If stuEnrollId IsNot Nothing And stuEnrollId IsNot String.Empty Then
                        GlobalVariables.StudentEnrollmentsList.Add(New Guid(stuEnrollId))
                    End If
                Next
                Dim gradeObject As ArFinalGradeClass = New ArFinalGradeClass(listScreenOperations, classId)
                if (Page.IsValid) Then
                    gradeObject.ExecuteOperations()
                    GetFinalGrades()
                end If

                '' Save the data-grid items in a collection.
                'iitems = dgdFinalGrades.Items


                '    'Loop through the collection to retrieve the StudentId,score & Comments.
                '    For i = 0 To iitems.Count - 1
                '        iitem = iitems.Item(i)
                '        Try
                '            If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                '                'modified by Theresa G on Sep 10 2010 for fixing the Grades when we change from a grade to a null or a score to a null value
                '                If CType(iitem.FindControl("txtScore"), TextBox).Text = "" And CType(iitem.FindControl("txtGrade"), Label).Text = "" Then
                '                    Exit Try
                '                End If
                '                If CType(iitem.FindControl("txtScore"), TextBox).Text = CType(iitem.FindControl("txtGrade"), Label).Text Then
                '                    Exit Try
                '                End If
                '            Else
                '                'modified by Theresa G on Sep 10 2010 for fixing the Grades when we change from a grade to a null or a score to a null value
                '                If CType(iitem.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString = "" And CType(iitem.FindControl("txtGrade"), Label).Text = "" Then
                '                    Exit Try
                '                End If
                '                If CType(iitem.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString = CType(iitem.FindControl("txtGrade"), Label).Text Then
                '                    Exit Try
                '                End If
                '            End If
                '            sId = CType(iitem.FindControl("txtStuEnrollId"), TextBox).Text
                '            If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                '                If CType(iitem.FindControl("txtScore"), TextBox).Text = "" Then
                '                    decScore = 0
                '                    scoreIsNull = True
                '                Else
                '                    decScore = CType(iitem.FindControl("txtScore"), TextBox).Text
                '                End If

                '                finalGradeObject = PopulateFinalGrdObject(sId, "", decScore)
                '            Else
                '                sGrade = CType(iitem.FindControl("ddlGrade"), DropDownList).SelectedItem.Value.ToString
                '                finalGradeObject = PopulateFinalGrdObject(sId, sGrade, decScore)
                '            End If
                '            'modified by Theresa G on Sep 10 2010 for fixing the Grades when we change from a grade to a null or a score to a null value
                '            errStr &= facade.UpdateFinalGrade(finalGradeObject, AdvantageSession.UserState.UserName, scoreIsNull)
                '            If errStr = "" Then
                '                DisplayErrorMessage("Grades have been posted for these students successfully")
                '            Else
                '                DisplayErrorMessage(errStr)
                '            End If
                '        Catch ex As Exception
                 '        	Dim exTracker = new AdvApplicationInsightsInitializer()
                '        	exTracker.TrackExceptionWrapper(ex)

                '        End Try
                '    Next


            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
            If GlobalVariables.StudentEnrollmentsList.Count > 0 Then
                TitleIVSapUpdate(GlobalVariables.StudentEnrollmentsList)
            End If

        End Sub

        Private Sub TitleIVSapUpdate(StudentEnrollmentsList)
            If (Not Session("AdvantageApiToken") Is Nothing) Then
                Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
                Dim TitleIVSAPRequest As New TitleIVSAPRequest(tokenResponse.ApiUrl, tokenResponse.Token)
                Dim pass As Boolean? = TitleIVSAPRequest.TitleIVSapCheck(StudentEnrollmentsList)
            End If
        End Sub
        'Private Function PopulateFinalGrdObject(ByVal StuEnrollId As String, Optional ByVal Grade As String = "", Optional ByVal score As Decimal = 0.0) As FinalGradeInfo
        '    Dim GrdPostingObj As New FinalGradeInfo

        '    GrdPostingObj.ClsSectId = ViewState("ClsSection")
        '    GrdPostingObj.StuEnrollId = StuEnrollId
        '    GrdPostingObj.Score = score
        '    'If Grade <> Guid.Empty.ToString Or Grade <> "" Then
        '    GrdPostingObj.Grade = Grade
        '    'get ModUser
        '    GrdPostingObj.ModUser = txtModUser.Text
        '    GrdPostingObj.ClsSectId = ddlClsSection.SelectedValue
        '    GrdPostingObj.TermId = ddlTerm.SelectedValue

        '    ''get ModDate
        '    'GrdPostingObj.ModDate = Date.Parse(txtModDate.Text)
        '    'End If

        '    Return GrdPostingObj
        'End Function
        Private Sub btnExportToExcell_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcell.Click

            Dim oStringWriter As StringWriter = New StringWriter
            Dim oHtmlTextWriter As HtmlTextWriter = New HtmlTextWriter(oStringWriter)

            'delete from the datagrid all controls that are not literals
            HideAllDropDownLists()
            'ClearControls(dgdFinalGrades)

            'render datagrid
            dgdFinalGrades.RenderControl(oHtmlTextWriter)

            'convert html to string
            Dim enc As Encoding = Encoding.UTF8
            Dim s As String = oStringWriter.ToString

            'move html content to a memorystream
            Dim documentMemoryStream As New MemoryStream
            documentMemoryStream.Write(enc.GetBytes(s), 0, s.Length)

            '   save document and document type in sessions variables
            Session("DocumentMemoryStream") = documentMemoryStream
            Session("ContentType") = "application/vnd.ms-excel"

            '   Register a javascript to open the report in another window
            Const javascript As String = "<script>var origwindow=window.self;window.open('../sa/PrintAnyReport.aspx');</script>"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NewWindow", javascript, False)

            'Recall the button click event so that the datagrid shows up with Textbox or dropdownlist controls
            btnBuildList_Click(sender, e)
        End Sub
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnBuildList)

            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
            BindToolTip("ddlTerm")
            BindToolTip("ddlClsSection")
            BindToolTip("ddlInstructor")


        End Sub
        Private Sub HideAllDropDownLists()
            For i As Integer = 0 To dgdFinalGrades.Items.Count - 1
                Dim dp As RadDatePicker = CType(dgdFinalGrades.Items(i).FindControl("dateCompletedDP"), RadDatePicker)
                'Dim dpReqValidator As RequiredFieldValidator = CType(dgdFinalGrades.Items(i).FindControl("DateCompletedFieldValidator"), RequiredFieldValidator)
                Dim dpCustomValidator As CustomValidator = CType(dgdFinalGrades.Items(i).FindControl("dateCompletedDpCustomValidator"), CustomValidator)

                If MyAdvAppSettings.AppSettings("GradesFormat").ToLower.ToString = "letter" Then
                    Dim ddl As DropDownList = CType(dgdFinalGrades.Items(i).FindControl("ddlGrade"), DropDownList)
                    'Dim val As String = ddl.SelectedItem.Text
                    Dim literal As LiteralControl = New LiteralControl
                    ddl.Parent.Controls.Add(literal)
                    literal.Text = ddl.SelectedItem.Text
                    ddl.Visible = False
                Else
                    Dim txtDecScore As TextBox = CType(dgdFinalGrades.Items(i).FindControl("txtScore"), TextBox)
                    'Dim val As String = txtDecScore.Text
                    Dim literal As LiteralControl = New LiteralControl
                    txtDecScore.Parent.Controls.Add(literal)
                    literal.Text = txtDecScore.Text
                    txtDecScore.Visible = False
                End If

                Dim literalDP As LiteralControl = New LiteralControl
                dp.Parent.Controls.Add(literalDP)
                literalDP.Text = dp.SelectedDate.ToString()
                dp.Visible = False
                'dpReqValidator.Visible = False
                dpCustomValidator.Visible = False
            Next
        End Sub

        '' show the cohort Start Date on the LHS of the Screen to build the list based on cohort-date and class sections 
        ''On Oct - 14-2008
        Private Sub PopulateClsSectionDdLbasedonCohortStDate(ByVal cohortStartDate As String)

            Dim facade As New PostFinalGrdsFacade
            Dim campusId As String

            campusId = HttpContext.Current.Request.Params("cmpid")
            ' Bind the Dataset to the CheckBoxList
            With ddlClsSection
                .DataSource = facade.GetClsSectionsbyCohortStartDate(cohortStartDate, CType(ViewState("Instructor"), String), campusId)
                .DataTextField = "CourseSectDescrip"
                .DataValueField = "ClsSectionId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With

        End Sub

        Private Sub PopulateCohortStartDateDDL()

            Dim facade As New PostFinalGrdsFacade
            Dim campusId As String
            campusId = Master.CurrentCampusId

            With ddlcohortStDate
                .DataSource = facade.GetCohortStartDateforCurrentAndPastTerms(campusId)
                .DataTextField = "CohortStartDate"
                .DataValueField = "CohortStartDate"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Protected Sub ddlcohortStDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlcohortStDate.SelectedIndexChanged
            ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
            ddlTerm.SelectedIndex = 0
            ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
            If (ViewState("CohortStartDate") <> "") Then
                PopulateInstructorDdLbasedonCohortStDate(CType(ViewState("CohortStartDate"), String))
            End If

        End Sub

        '' show the cohort Start Date on the LHS of the Screen to build the list based on cohort date and class sections 
        Private Sub PopulateInstructorDdLbasedonCohortStDate(ByVal cohortStartDate As String)

            Dim facade As New PostFinalGrdsFacade
            Dim campusId As String

            campusId = HttpContext.Current.Request.Params("cmpid")

            ' Bind the Dataset to the CheckBoxList
            With ddlInstructor
                .DataSource = facade.GetInstructorbyCohortStartDate(cohortStartDate, campusId)
                .DataTextField = "FullName"
                .DataValueField = "InstructorId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Public Sub ValidateSelectedDate(source As Object, args As ServerValidateEventArgs)
            Dim cv As CustomValidator = CType(source, CustomValidator)
            Dim parentRow As DataGridItem = CType(cv.NamingContainer, DataGridItem)
            Dim message = String.Empty
            If (parentRow IsNot Nothing) Then
                dim grade as String = CType(parentRow.FindControl("txtGrade"),Label).Text
                Dim startDate As DateTime? = DateTime.Parse(CType(parentRow.FindControl("lblStartDate"), Label).Text)
                If(CType(parentRow.FindControl("lblGraduatedDate"), Label).Text="")
                    cv.IsValid = False
                    args.IsValid = cv.IsValid
                    cv.ErrorMessage = "Revised GradDate Date is null."
                Else 
                    Dim graduatedDate As DateTime? = DateTime.Parse(CType(parentRow.FindControl("lblGraduatedDate"), Label).Text)
                    Dim dp As RadDatePicker = CType(parentRow.FindControl("dateCompletedDP"), RadDatePicker)
                    Dim selectedDate = dp.SelectedDate
                    cv.IsValid = ValidateDate(grade,startDate, graduatedDate, selectedDate, message)
                    cv.ErrorMessage = message
                    args.IsValid = cv.IsValid
                End If
                'Return
            else
                cv.IsValid = False
                args.IsValid = cv.IsValid
            End If
        End Sub

        Private Shared Function ValidateDate(grade As String, startDate As Date?, graduatedDate As Date?, selectedDate As Date?,
                                             ByRef message As String) As Boolean
            If (selectedDate Is Nothing) Then
                If(grade.Equals(String.Empty)) Then
                    Return true
                    Else 
                        message = "Date Completed is required."
                        Return False
                End If
            End If
            If (startDate Is Nothing AndAlso graduatedDate Is Nothing) Then
                message = "Date validation requires start date and graduation date."
                Return False
            End If
            If (selectedDate < startDate) Then
                message = "Date Completed cannot be less than enrollment start date("& startDate &")."
                Return False
            ElseIf (selectedDate > graduatedDate) Then
                message = "Date Completed cannot be greater than graduated date("& graduatedDate &")."
                Return False
            End If
            Return True
        End Function

        Protected Sub dateCompletedDP_SelectedDateChanged(ByVal sender As Object, ByVal e As SelectedDateChangedEventArgs)
            Dim dp As RadDatePicker = CType(sender, RadDatePicker)
            Dim parentRow As DataGridItem = CType(dp.NamingContainer, DataGridItem)
            Dim selectedDate = dp.SelectedDate
            Dim message = String.Empty

            If (parentRow IsNot Nothing) Then
                Dim startDate As DateTime? = DateTime.Parse(CType(parentRow.FindControl("lblStartDate"), Label).Text)
                Dim graduatedDate As DateTime? = DateTime.Parse(CType(parentRow.FindControl("lblGraduatedDate"), Label).Text)
                Dim cv = CType(parentRow.FindControl("dateCompletedDpCustomValidator"), CustomValidator)
                dim grade as String = CType(parentRow.FindControl("txtGrade"),Label).Text
                cv.IsValid = ValidateDate(grade,startDate, graduatedDate, selectedDate, message)
                cv.ErrorMessage = message
                Return
            End If
        End Sub

        Public Function TryParseDateTime(ByVal text As String) As DateTime?
            Dim validDate As DateTime
            If (DateTime.TryParse(text, validDate)) Then
                Return validDate
            End If
            Return Nothing
        End Function
    End Class
End Namespace