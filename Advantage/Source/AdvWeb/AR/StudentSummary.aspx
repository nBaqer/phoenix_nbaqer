<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master"
    Inherits="StudentSummary" CodeFile="StudentSummary.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">


        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }


    </script>
    <script type="text/javascript">
		    function openschedule()
                {
                    var cmpid = "<%=Session("CampusId") %>";
                    var mod = "<%=Session("mod") %>";
                    var pid = "<%=txtPrgVerId.Text %>";
                    var objid = "<%=Session("ObjId") %>";
                    var ascx = '~/AR/IMaint_SetupSchedule.ascx';
                    var turl = '../MaintPopup.aspx?mod=' + mod +
                            '&resid=511' +
                            '&cmpid=' + cmpid +
                            '&pid=' + pid + 
		                    '&objid=' + objid + 
                            '&ascx=' + ascx + 
                            '&perm=readonly' + '&source=studentsummary';
                    window.showModalDialog(turl,null,'resizable:no;status:no;dialogWidth:850px;dialogHeight:600px;dialogHide:true;help:no;scroll:yes');
                    }
            function openmonthsummary(){
                var stuenrollid = "<%=txtStuEnrollId.text %>";
                var studentname= "<%=Session("StudentName") %>";
                var prgversion = "<%=Session("PrgVersion") %>";
                var unittype = "<%=Session("UnitType") %>";
                var turl = 'Attendance_Month.aspx?stuenrollid=' + stuenrollid + '&studentname=' + studentname + '&prgversion=' + prgversion + '&unittype=' + unittype;
                window.open(turl,"calendar","width=800px,height=550px;");
            }
            function tracktardy(){
                var prgverid = "<%=txtPrgVerId.Text %>";
                var actualdaysabsent = "<%=Session("actualdaysabsent") %>";
                var numberoftardydays = "<%=Session("numberoftardy") %>";
                var studenttardy = "<%=Session("studenttardy") %>";
                var numberofabsentdays = "<%=Session("numberofabsentdays") %>";
                var numbermakestardy = "<%=Session("tardiesmakingabsence") %>";
                var permission="readonly";
                var unittype = "<%=Session("UnitType") %>";
                var turl = 'tardy.aspx?prgverid=' + prgverid + '&numberoftardydays=' + numberoftardydays + '&numberofabsentdays=' + numberofabsentdays + '&actualdaysabsent=' + actualdaysabsent + '&numbermakestardy=' + numbermakestardy + '&studenttardy=' + studenttardy + '&unittype=' + unittype;
                window.open(turl,"tardy","width=500px,height=250px;");
            }
             function tracktardyminutes(){
                var prgverid = "<%=txtPrgVerId.Text %>";
                var actualdaysabsent = "<%=Session("actualdaysabsent") %>";
                var numberoftardydays = "<%=Session("numberoftardy") %>";
                var studenttardy = "<%=Session("studenttardy") %>";
                var numberofabsentdays = "<%=Session("numberofabsentdays") %>";
                var numbermakestardy = "<%=Session("tardiesmakingabsence") %>";
                var actpresent = "<%=Session("actualhourspresent")%>";
                var actabsent = "<%=Session("actualhoursabsent")%>";
                var acttardy = "<%=Session("actualtardyhours")%>";
                var adjpresent = "<%=Session("adjactual")%>";
                var adjabsent = "<%=Session("adjabsent")%>";
                var adjtardy = "<%=Session("adjtardy")%>";
                
                var turl = 'tardyminutes.aspx?prgverid=' + prgverid + '&numberoftardydays=' + numberoftardydays + '&numberofabsentdays=' + numberofabsentdays + '&actualdaysabsent=' + actualdaysabsent + '&numbermakestardy=' + numbermakestardy + '&studenttardy=' + studenttardy + '&actpresent=' + actpresent + '&actabsent=' + actabsent + '&acttardy=' + acttardy + '&adjpresent=' + adjpresent + '&adjabsent=' + adjabsent + '&adjtardy=' + adjtardy;
                window.open(turl,"tardyminutes","width=900px,height=225px;");
            }
    </script>
    <style>
        h1
        {
            font: bold 11px verdana;
            color: #000066;
            margin: 0;
            padding: 6px;
            text-align: center;
            border-bottom: 1px solid #666;
            background-color: #edecec;
        }
        .trackHistory
        {
            position: absolute;
            bottom: 10px;
            border: 1px solid #000;
            background-color: #fefefe;
            width: 100%;
            height: 100px;
            white-space: normal;
        }
        .trackHistory p
        {
            font: normal 11px verdana;
            color: #333;
            margin: 0;
            padding: 6px;
            text-align: left;
            white-space: normal;
        }
        .trackHistoryLabel
        {
            width: 100%;
            height: 100px;
            overflow: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <table cellspacing="0" cell="0" width="100%" border="0">
                            <tr>
                                <td nowrap align="left" width="15%">
                                    <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                </td>
                                <td nowrap width="85%">
                                    <asp:RadioButtonList ID="radStatus" runat="Server" CssClass="label" AutoPostBack="true"
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Active" Selected="True" />
                                        <asp:ListItem Text="Inactive" />
                                        <asp:ListItem Text="All" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom" style="white-space: normal">
                        <div class="scrollleftfilters" style="white-space: normal">
                            <asp:DataList ID="dlstStdEnroll" runat="server" DataKeyField="StuEnrollId" Width="100%">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                        Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'
                                        CausesValidation="False"></asp:ImageButton>
                                    <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'
                                        CausesValidation="False"></asp:ImageButton>
                                    <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                    <asp:LinkButton Text='<%# Container.DataItem("PrgVerDescrip")%>' runat="server" CssClass="itemstyle"
                                        CommandArgument='<%# Container.DataItem("StuEnrollId")%>' ID="Linkbutton1" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                            <% If Not Trim(Session("TrackingMessage")) = "" Then%>
                            <div class="trackHistory">
                                <h1>
                                    Student Transfer Track History</h1>
                                <div class="trackHistoryLabel">
                                    <p>
                                        <asp:Label ID="lblTrackMessage" runat="Server"></asp:Label></p>
                                </div>
                            </div>
                            <% End If%>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False">
                        </asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                            CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="scrollright">
                            <!-- begin table content -->
                            <asp:Panel ID="pnlRHS" runat="server">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td colspan="6" align="center">
                                            <asp:Label ID="lblCurrentProb" runat="server" CssClass="label" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" style="white-space: normal" nowrap>
                                            <asp:Label ID="lblExpStartDate" runat="server" CssClass="label">Expected Start Date</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtExpStartDate" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" style="white-space: normal">
                                            <asp:Label ID="lblStartDate" runat="server" CssClass="label">Start Date</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtStartDate" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" style="white-space: normal">
                                            <asp:Label ID="lblEnrollDate" runat="server" CssClass="label">Enrolled Date</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtEnrollDate" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" style="white-space: normal">
                                            <asp:Label ID="lblReEnrollDate" runat="server" CssClass="label">Reenroll Date</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtReEnrollDate" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" style="white-space: normal" nowrap>
                                            <asp:Label ID="lblExpGradDate" runat="server" CssClass="label">Expected Grad Date</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtExpGradDate" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" style="white-space: normal" nowrap>
                                            <asp:Label ID="lblStudentGroup" runat="server" CssClass="label">Student Group</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:ListBox ID="lstStudentGroup" runat="server" CssClass="label" Rows="3"></asp:ListBox>
                                        </td>
                                    </tr>
                                </table>
                                <p>
                                </p>
                                <asp:CheckBoxList ID="chkLeadGrpId" TabIndex="5" runat="server" RepeatColumns="7"
                                    CssClass="CheckBoxStyle" Visible="false">
                                </asp:CheckBoxList>
                                <p>
                                </p>
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="7">
                                            <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="LabelButton">Attendance</asp:Label>
                                            <input type="button" id="Button2" runat="server" class="MonthDetail" language="javascript"
                                                onclick="openmonthsummary();" value="Month Detail ..." />
                                        </td>
                                    </tr>
                                    <tr height="10px">
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblTotalHours" runat="Server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtTotalHours" runat="Server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            <asp:Label ID="lblSchedHours" runat="Server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtSchedHours" runat="Server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblTotalHoursAbsent" runat="Server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4" nowrap>
                                            <asp:TextBox ID="txtTotalHoursAbsent" runat="Server" CssClass="textbox" Enabled="false"
                                                Width="100px"></asp:TextBox>
                                            <input type="button" id="Button1" runat="server" class="MonthDetail" language="javascript"
                                                onclick="tracktardy();" value="View Detail ..." />
                                            <input type="button" id="btntardyminutes" runat="server" class="MonthDetail" language="javascript"
                                                onclick="tracktardyminutes();" value="View Detail ..." />
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            <asp:Label ID="lblSchedHoursByWeek" runat="Server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4" nowrap>
                                            <asp:TextBox ID="txtSchedHoursByWeek" runat="Server" CssClass="textbox" Width="90px"
                                                Enabled="false"></asp:TextBox>
                                            <input type="button" id="btnSchedule" runat="server" class="ScheduleDetail" language="javascript"
                                                onclick="openschedule();" value="View Schedule ..." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblMakeUpHours" runat="Server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtMakeUpHours" runat="Server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            <asp:Label ID="lblBadgeId" runat="server" CssClass="label">Badge ID</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtBadgeId" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblLDA" runat="Server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtLDA" runat="Server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" style="white-space: normal">
                                            <asp:Label ID="lblClockSchedule" runat="server" CssClass="label">Schedule</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtClockSchedule" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblAttPercentage" runat="Server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtAttPercentage" runat="Server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltransferHours" runat="server" CssClass="label" Visible="false">Transfer Hours</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTransferHours" runat="server" CssClass="textbox" Enabled="false"
                                                Visible="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <p>
                                </p>
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="lblReportingAgencies" runat="server" Font-Bold="true" CssClass="label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            &nbsp;
                                        </td>
                                        <td width="20%" nowrap>
                                            <asp:Label ID="lblPresent" runat="server" CssClass="label"><u>Present</u> (in hrs)</asp:Label>
                                        </td>
                                        <td width="20%" nowrap>
                                            <asp:Label ID="lblAbsent" runat="server" CssClass="label"><u>Absent</u> (in hrs) </asp:Label>
                                        </td>
                                        <td width="20%" nowrap>
                                            <asp:Label ID="lblTardy" runat="server" CssClass="label"><u>Tardy</u> (in hrs)</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="40%" nowrap>
                                            <asp:Label ID="lblActualTotals" runat="server" CssClass="LabelBold">Overall Actual Totals</asp:Label>
                                        </td>
                                        <td nowrap width="20%">
                                            <asp:Label ID="lblActPresent" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td nowrap width="20%">
                                            <asp:Label ID="lblActAbsent" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td nowrap width="20%">
                                            <asp:Label ID="lblActTardy" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="40%" nowrap>
                                            <asp:Label ID="lblAdjTotals" runat="server" CssClass="LabelBold">Overall Adjusted Totals</asp:Label>
                                        </td>
                                        <td nowrap width="20%">
                                            <asp:Label ID="lblAdjPresent" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td nowrap width="20%">
                                            <asp:Label ID="lblAdjAbsent" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td nowrap width="20%">
                                            <asp:Label ID="lblAdjTardy" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- end table content-->
                        </div>
                    </td>
                </tr>
            </table>           
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="contentcell" style="white-space: normal">
                        <asp:TextBox ID="txtStuEnrollId" runat="server" Visible="false"></asp:TextBox><asp:CheckBox
                            ID="chkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="contentcell" style="white-space: normal">
                    </td>
                    <td class="contentcell4">
                        <asp:TextBox ID="txtStudentId" runat="server" CssClass="TextBox" Visible="false"></asp:TextBox><asp:TextBox
                            ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox
                                ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                    </td>
                </tr>
                <asp:TextBox ID="txtPrgVerId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtActualAbsentHours" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
            </table>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False"></asp:ValidationSummary>
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
