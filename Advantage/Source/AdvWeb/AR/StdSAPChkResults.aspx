﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StdSAPChkResults.aspx.vb" Inherits="StdSAPChkResults" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script type="text/javascript">
</script>

    <style>
        .AlignCenter {
            text-align: center !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <%--LEFT PANE CONTENT HERE--%>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td nowrap align="left" width="15%">
                                    <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                <td nowrap width="85%">
                                    <asp:RadioButtonList ID="radStatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true" CssClass="label">
                                        <asp:ListItem Text="Active" Selected="True" />
                                        <asp:ListItem Text="Inactive" />
                                        <asp:ListItem Text="All" />
                                    </asp:RadioButtonList></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfilters">
                            <asp:DataList ID="dlstStdProgress" runat="server" DataKeyField="StuEnrollId" Width="100%">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>' CausesValidation="False"></asp:ImageButton>
                                    <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>' CausesValidation="False"></asp:ImageButton>
                                    <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                    <asp:LinkButton Text='<%# Container.DataItem("PrgVerDescrip")%>' runat="server" CssClass="nonselecteditem" CommandArgument='<%# Container.DataItem("StuEnrollId")%>' ID="Linkbutton1" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>


        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0" class="hidden">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="false"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none;">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->

                            <%-- MAIN CONTENT HERE--%>
                            <asp:DataGrid ID="dgrdTransactionSearch" runat="server" BorderColor="#ebebeb" BorderStyle="Solid"
                                BorderWidth="1px" CellPadding="3" Width="100%" AutoGenerateColumns="False">
                                <EditItemStyle CssClass="label"></EditItemStyle>
                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                <FooterStyle CssClass="label"></FooterStyle>
                                <Columns>

                                    <asp:TemplateColumn HeaderText="Increment">
                                        <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="label2" Text='<%# Container.DataItem("Period")%>' CssClass="label" runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Triggered at">
                                        <HeaderStyle CssClass="datagridheader" Width="25%"></HeaderStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPrgVerDescrip" Text='<%# Container.DataItem("TrigDescrip")%>' CssClass="label" runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="SAP Check Date">
                                        <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="label1" Text='<%# Eval("CheckPointDate", "{0:MM/dd/yyyy}")%>' CssClass="label" runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Making SAP?">
                                        <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMakingSAP" Text='<%# Container.DataItem("MakingSAP")%>' CssClass="label" runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Reason">
                                        <HeaderStyle CssClass="datagridheader" Width="25%"></HeaderStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblComments" runat="server" Visible="true" Text='<%# Container.DataItem("Comments")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:TextBox ID="txtStudentId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

