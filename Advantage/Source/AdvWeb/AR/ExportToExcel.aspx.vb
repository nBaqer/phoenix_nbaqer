﻿
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade

Partial Class AR_ExportToExcel
    Inherits BasePage

    Protected Sub dgCS_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCS.ItemCreated
        Dim oControl As Control
        For Each oControl In dgCS.Controls(0).Controls
            If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
                CType(oControl, DataGridItem).Cells(0).Text = Request.Params("Source") & " - " & Request.Params("Program") & " - " & Request.Params("course")
            End If
        Next
    End Sub
    Protected Sub dgCS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgCS.Load
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim stuEnrollId As String = Request.Params("StuEnrollId")
            Dim Required As String = Request.Params("Required")
            Dim Completed As String = Request.Params("Completed")
            Dim Remaining As String = Request.Params("Remaining")
            Dim program As String = Request.Params("Program")
            Dim course As String = Request.Params("course")

            Dim dtTable As New DataTable
            dtTable = (New ExamsFacade).GetClinicServicesAndHoursByStudent(stuEnrollId, Required, Completed, Remaining)

            'need to do this for ms word
            'For Each dr As DataRow In dtTable.Rows
            '    If InStr(dr("ClinicService"), "&") >= 1 Then
            '        dr("ClinicService") = Replace(dr("ClinicService"), "&", " and ")
            '    End If
            'Next

            dgCS.DataSource = dtTable
            dgCS.DataBind()

            Response.ContentType = "application/vnd.ms-excel"
            ' Remove the charset from the Content-Type header.
            Response.Charset = ""
            ' Turn off the view state.
            Me.EnableViewState = False

            Dim tw As New System.IO.StringWriter()
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            ' Get the HTML for the control.
            dgCS.RenderControl(hw)
            ' Write the HTML back to the browser.
            Response.Write(tw.ToString())
            ' End the response.
            Response.End()

            'Code to export to word
            'Response.Clear()

            'Response.AddHeader("content-disposition", "attachment;filename=Yearwise Yield and Expenses.doc")

            'Response.Charset = ""

            'Response.Cache.SetCacheability(HttpCacheability.NoCache)

            'Response.ContentType = "application/vnd.word"

            'Dim stringWrite As New System.IO.StringWriter()

            'Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

            'dgCS.ForeColor = Color.Black

            'dgCS.HeaderStyle.BackColor = Color.White

            'dgCS.HeaderStyle.Font.Bold = True

            'dgCS.HeaderStyle.Font.Size = 11

            'dgCS.ItemStyle.ForeColor = Color.Black

            'dgCS.ItemStyle.Font.Size = 11

            'dgCS.ItemStyle.VerticalAlign = System.Web.UI.WebControls.VerticalAlign.Top

            'dgCS.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left


            ''Table1.Font.Size = 9

            ''Table1.BorderWidth = 0

            ''Table1.RenderControl(htmlWrite)

            'htmlWrite.WriteLine("<p align=center>")

            'dgCS.RenderControl(htmlWrite)

            'htmlWrite.WriteLine("</p>")

            'Response.Write(stringWrite.ToString())

            'Response.[End]()


        End If
    End Sub
End Class
