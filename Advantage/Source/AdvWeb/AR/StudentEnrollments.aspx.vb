﻿Imports System.Diagnostics
Imports Fame.AdvantageV1.BusinessFacade.AR
Imports Fame.Common
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports System.Collections
Imports System.Collections.Generic
Imports System.Drawing
Imports System.Text
Imports Advantage.AFA.Integration
Imports Fame.Advantage.Common
Imports Advantage.Business.Objects
Imports Fame.Advantage.Api.Library.Models
Imports Fame.Advantage.Api.Library.Models.Common
Imports Fame.Advantage.DataAccess.LINQ
Imports Fame.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports Fame.Advantage.MultiTenantHost.Lib.Infrastructure.API
Imports Fame.Advantage.Reporting.ReportService2005
Imports Fame.AdvantageV1.Common.AR
Imports Fame.AdvantageV1.DataAccess.AR
Imports Fame.Integration.Adv.Afa.Messages.WebApi.Entities
Imports FluentNHibernate.Utils
Imports Newtonsoft.Json.Linq
Imports Telerik.Web.Data.Extensions

Partial Class StudentEnrollments
    Inherits BasePage
    <DebuggerStepThrough> Private Sub InitializeComponent()

    End Sub
    Protected StudentId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected ShowStatusLevel As AdvantageCommonValues.IncludeStatus
    Private stuMasterInfo As New StudentEnrollmentInfo
    'To get the first name and last name of student to pass into the enrollmentid component.
    Dim strFirstName, strLastName As String
    ReadOnly enrollmentId As New EnrollmentComponent
    Dim facade As New StuEnrollFacade
    ReadOnly sdfControls As New SDFComponent
    'Private dataNavigationProvider As NavigationRoutines
    'Private mruProvider As MRURoutines
    Protected state As AdvantageSessionState
    Protected LeadId As String = Guid.Empty.ToString
    Protected boolSwitchCampus As Boolean = False
    Dim dateError As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings
    Protected BillingMethods As DataSet


    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(sender As Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        facade = New StuEnrollFacade()
    End Sub

#End Region

#Region "Page Events"
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MyAdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim objCommon As New CommonUtilities

        'set the onLine button according to web.config 
        bAddToOnLine.Visible = False

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        ViewState("CampusId") = campusId

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU
        objStudentState = GetObjStudentState(0)
        Master.ShowHideStatusBarControl(True)
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            'LeadId = .LeadId.ToString
            LeadId = .ShadowLead.ToString()

        End With


        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'Set the Reschedule Button so it prompts the user for confirmation when clicked
        btnReschedule.Attributes.Add("onclick", "if(confirm('Are you sure you want to reschedule this student?')){}else{return false}")

        Dim advantageUserState As Advantage.Business.Objects.User = AdvantageSession.UserState
        ViewState("IsUserSa") = advantageUserState.IsUserSA
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId.ToString(), campusId)

        If radStatus.SelectedItem.Text = "Active" Then
            ShowStatusLevel = AdvantageCommonValues.IncludeStatus.ActiveOnly
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            ShowStatusLevel = AdvantageCommonValues.IncludeStatus.InactiveOnly
        Else
            ShowStatusLevel = AdvantageCommonValues.IncludeStatus.ActiveAndInactive
        End If

        Try
            If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(uSearchEntityControlId.Value)) Then
                'The default behavior is not to show the date determined text box to the user
                txtDateDetermined.Attributes.Add("style", "display:none")

                'The default behavior is not to show the date drop reason ddl to the user
                ddlDropReasonId.Attributes.Add("style", "display:none")

                'Get the setting that decide if it a SUVA School
                Dim majorMinorActive = GetSettingString("MajorsMinorsConcentrations")
                ddlSuvaVersion.DataTextField = "Description"
                ddlSuvaVersion.DataValueField = "IdSuva"
                If majorMinorActive.ToUpper() = "YES" Then
                    lblSubaVersion.Visible = True
                    ddlSuvaVersion.Visible = True
                    'SuvaStuff.Style("visibility") = "visible"
                    ddlSuvaVersion.DataSource = SuvaTableFacade.GetItemsFromSuvaCatalogDB()
                Else
                    lblSubaVersion.Visible = False
                    ddlSuvaVersion.Visible = False
                    ddlSuvaVersion.DataSource = SuvaTableFacade.GetMockItemsFromSuvaCatalogDB()
                    'SuvaStuff.Style("visibility") = "collapse"
                End If
                ddlSuvaVersion.DataBind()
                '............................................

                'To get the first name and last-name of student to pass into the enrollment-id component.
                txtStudentId.Text = StudentId
                Dim fullName() As String = Split(facade.StudentName(StudentId))
                strFirstName = fullName(0)
                strLastName = fullName(1)

                'To Generate EnrollmentID 
                txtEnrollmentId.Text = enrollmentId.GenerateEnrollmentID(strLastName, strFirstName)
                BuildEnrollStatusDDL()

                'objCommon.PopulatePage(Form1)
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                disableAutoChargeContainer.Visible = False

                BuildDropDownLists()
                txtStuEnrollId.Text = Guid.NewGuid.ToString
                BindEnrollmentDataList(txtStudentId.Text)
                SelectFirstEnrollmentWhenPageFirstLoads()
                BuildLeadGroupsDDL()
                BindLeadGroupList()

                'Show link for Autit History
                'if there are no enrollments there is no way to select the first one
                If dlstStdEnroll.DataKeys.Count > 0 Then
                    Master.PageObjectId = dlstStdEnroll.DataKeys(0).ToString()
                    Master.PageResourceId = resourceId
                    Master.SetHiddenControlForAudit()

                End If

                'Clear the Session before looking out for Transfered Enrollment
                Session("TrackingMessage") = ""
                trackhistmess.Visible = False
                getTransferToolTip()

                If boolSwitchCampus = True Then
                    CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
                End If

                uSearchEntityControlId.Value = ""

                'to  be replace with value coming from db
                ViewState("curTotalTransferHrs") = 0
                ViewState("curTotalTransferHrsFromThisSchool") = 0

                BindTimeClockSchedules()
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                getTransferToolTip()
                'Enable the Reschedule Button if the school is configured for this.
                If MyAdvAppSettings.AppSettings("ShowRescheduleButtonOnEnrollmentTab").ToLower = "true" Then
                    btnReschedule.Visible = True
                    ''Reschedule Reason label and textbox are added
                    lblReschReason.Visible = True
                    txtReschReason.Visible = True
                    ddlnewPrgVersion.Visible = True
                    lblnewPrgVersion.Visible = True
                    SetRescheduleButtonState(txtStuEnrollId.Text)
                End If
            End If
            'moduleId = HttpContext.Current.Request.Params("Mod").ToString
            ModuleId = AdvantageSession.UserState.ModuleCode.ToString

            'Check If any UDF exists for this resource
            Dim intSdfExists As Integer = sdfControls.GetSDFExists(resourceId, ModuleId)
            If intSdfExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If

            If Trim(txtStuEnrollId.Text) <> "" Then
                sdfControls.GenerateControlsEdit(pnlSDF, resourceId, txtStuEnrollId.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
            End If
            If AdvantageSession.UserState.IsUserSA Then txtDateDetermined.ReadOnly = False
            If MyAdvAppSettings.AppSettings("ManuallySetLDA").ToString.ToLower = "yes" Then
                txtLDA.Enabled = True
            End If
            ToggleBaddgeNumberSection()

            'Toggle the licensing exam details section
            ToggleLicensingExamDetails()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

#End Region
    'Protected Sub ViewScheduleDetailsWindow(sender As Object, e As EventArgs) Handles ViewScheduleDetails.Click
    '    Dim script As String = "function f(){$find(""" + StudentScheduleWindow.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
    '    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
    'End Sub
    Private Sub UpdateLdaForManuallySetLdaAsNo(stuEnrollid As String)
        If MyAdvAppSettings.AppSettings("ManuallySetLDA").ToString.ToLower = "no" Then
            'Dim facade As New StuEnrollFacade
            facade.UpdateLDAForManuallySetLDAAsNO(stuEnrollid)

        End If
    End Sub
    Private Sub BuildPrgVersionDDL()
        ''to save the contents of the program version and use it in newprogrVersionDDL

        Dim ds As DataSet = facade.GetAllPrgVersions()
        ViewState("ProgVersions") = ds
        With ddlPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0

        End With
    End Sub
    Private Sub BuildAcadAdvisorDDL(CampId As String, Optional ByVal advisorID As String = "")
        'Dim facade As New StuEnrollFacade
        With ddlEmpId
            .DataTextField = "FullName"
            .DataValueField = "UserId"
            .DataSource = facade.GetAcadAdvisors(CampId, advisorID)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAdminRepDDL(Optional ByVal repID As String = "")
        'Dim facade As New StuEnrollFacade
        With ddlAdminRep
            .DataTextField = "FullName"
            .DataValueField = "UserId"
            Try
                .DataSource = facade.GetAdminReps(CType(ViewState("CampusId"), String), repID)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End Try
        End With
    End Sub
    Protected Sub ibToMaintPage_Schedule_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ibToMaintPage_Schedule.Click
        BindTimeClockSchedules()
    End Sub

    Public Sub BindTimeClockSchedules()
        Dim Show_ibToMaintPage_Schedule As Boolean = False
        Dim stuEnrollId As String = txtStuEnrollId.Text
        Dim prgVerId As String = ddlPrgVerId.SelectedValue
        If prgVerId = "" Or prgVerId = Guid.Empty.ToString() Then
            ' no prgverid?, just clear out the info
            ddlSchedules.DataSource = Nothing
            ddlSchedules.DataBind()
            ddlSchedules.Items.Clear()
            txtBadgeNumber.Text = ""
            ibToMaintPage_Schedule.Visible = False
            Return
        End If

        Show_ibToMaintPage_Schedule = SchedulesFacade.IsPrgVerUsingTimeClockSchedule(prgVerId)
        If Show_ibToMaintPage_Schedule Then

            ' Retrieve the student's active schedule info and bind it to the form
            Dim info As StudentScheduleInfo
            info = SchedulesFacade.GetActiveStudentScheduleInfo(stuEnrollId)

            Try
                If ddlSchedules.Items.FindByValue(info.ScheduleId) IsNot Nothing Then
                    ddlSchedules.SelectedValue = info.ScheduleId

                End If

                ' add javascript to show the schedule maintenance page
                Dim parentId As String = prgVerId
                Dim objId As String = info.ScheduleId
                Dim url As String = String.Format("../MaintPopup.aspx?mod={0}&resid=511&cmpid={1}&pid={2}&objid={3}&ascx={4}",
                                                  Session("mod"), campusId, parentId, objId, "~/AR/IMaint_SetupSchedule.ascx")
                Dim js As String = GetJavsScriptPopup(url, 900, 600, Nothing)
                ibToMaintPage_Schedule.Visible = True
                ibToMaintPage_Schedule.Attributes.Add("onclick", js)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try
        Else
            ddlSchedules.DataSource = Nothing
            ddlSchedules.DataBind()
            ddlSchedules.Items.Clear()
            ddlSchedules.Items.Insert(0, New ListItem("---Select---", ""))
            ddlSchedules.SelectedIndex = 0
            ibToMaintPage_Schedule.Visible = False
        End If
    End Sub

    Public Shared Function GetJavsScriptPopup(ByVal popupUrl As String,
                                              ByVal width As Integer,
                                              ByVal height As Integer,
                                              ByVal retControlId As String) As String
        Dim js As New StringBuilder
        js.Append("var strReturn; " & vbCrLf)
        js.Append("strReturn=window.open('")
        js.Append(popupUrl)
        js.Append("',null,'re-sizable:yes;status:no;scrollbars=yes;minimize:yes; maximize:yes; dialogWidth:")
        js.Append(width.ToString())
        js.Append("px;dialogHeight:")
        js.Append(height.ToString())
        js.Append("px;dialogHide:true;help:no;scroll:yes'); " & vbCrLf)
        If Not retControlId Is Nothing Then
            If retControlId.Length <> 0 Then
                js.Append("if (strReturn != null) " & vbCrLf)
                js.Append("     document.getElementById('")
                js.Append(retControlId)
                js.Append("').value=strReturn;" & vbCrLf)
            End If
        End If
        Return js.ToString()
    End Function
    Private Sub BuildEnrollStatusDDL()
        'Dim facade As New StuEnrollFacade
        With ddlStatusCodeId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = facade.GetEnrollStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildDisableAutoChargeDDL()
        With ddlDisableAutoCharge
            .Items.Insert(0, New ListItem("No", "0"))
            .Items.Insert(1, New ListItem("Yes", "1"))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub FillTransferFromProgramDDL(leadId As String, Optional enrollmentId As String = Nothing)
        Dim enrollmentPrograms = (New StudentEnrollmentDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetTransferProgramsForLead(leadId, enrollmentId)
        With ddTransferHourFromProgram
            .DataTextField = "ProgramDescription"
            .DataValueField = "EnrollmentId"
            .DataSource = enrollmentPrograms
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildBillingMethodDataSet()
        If (Not ViewState("CampusId") Is Nothing) Then
            Me.BillingMethods = (New StuEnrollFacade).GetBillingMethodForChargingMethods(CType(ViewState("CampusId"), String))
            ViewState("BillingMethods") = Me.BillingMethods
        End If

    End Sub
    Private Sub BuildEnrollStatusDDL(enumType As StateEnum)
        '   bind DDL
        Dim fac As New BatchStatusChangeFacade
        With ddlStatusCodeId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = fac.GetStatuses(enumType, CType(ViewState("CampusId"), String))
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildFaAdvisorIdDDL(Optional ByVal advisorId As String = "")

        With ddlFAAdvisorId
            .DataTextField = "FullName"
            .DataValueField = "UserId"
            .DataSource = (New StuEnrollFacade).GetFinancialAdvisorsPerCampus(CType(ViewState("CampusId"), String), advisorId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCampusDDL(prgVerId As String, currentCampus As String)
        With ddlCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataSource = (New StuEnrollFacade).GetCampuses("", currentCampus)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BindEnrollmentDataList(stuId As String)
        Dim ds As DataSet
        ds = (New StuEnrollFacade).PopulateDataList(stuId, ShowStatusLevel, ViewState("CampusId").ToString)
        With dlstStdEnroll
            .DataSource = ds
            .DataBind()
        End With
    End Sub

    Protected Sub ChargingMethodChanged(sender As Object, e As EventArgs) Handles ddlBillingMethodId.SelectedIndexChanged

        BillingMethods = (CType(ViewState("BillingMethods"), DataSet))
        If Not BillingMethods Is Nothing Then
            Dim selected = ddlBillingMethodId.SelectedValue
            Dim billingDataRow As DataRow = BillingMethods.Tables(0).Select("BillingMethodId = '" + selected + "'").FirstOrDefault()
            Dim billingCode = -1
            If Not billingDataRow Is Nothing Then
                Dim code = -1
                If Not billingDataRow.Item("BillingMethod") Is Nothing Then
                    Integer.TryParse(billingDataRow.Item("BillingMethod").ToString(), code)
                End If
                billingCode = code
            End If
            If (billingCode = Fame.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then
                disableAutoChargeContainer.Visible = True
            Else
                disableAutoChargeContainer.Visible = False
            End If
        End If

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        SaveRecord()
        CommonWebUtilities.RestoreItemValues(dlstStdEnroll, txtStuEnrollId.Text)
    End Sub



    Private Function BuildStuEnrollObj() As StudentEnrollmentInfo
        Dim stuEnrollInfo As New StudentEnrollmentInfo
        'Dim facade As New StuEnrollFacade
        With stuEnrollInfo
            .ShiftId = ddlShiftId.SelectedValue
            .CampusId = ddlCampusId.SelectedValue
            .AdmissionsRep = ddlAdminRep.SelectedValue
            .StudentID = StudentId 'Guid.NewGuid.ToString
            .StatusCodeId = ddlStatusCodeId.SelectedValue
            .GrdLvlId = ddlEdLvlId.SelectedValue
            .BillingMethodId = ddlBillingMethodId.SelectedValue

            ' Extra values........................
            .IsDisabled = CType(ddlIsDisabled.SelectedValue, Integer)

            .EntranceInterviewDate = EntranceInterviewDate.SelectedDate
            .IsFirstTimeInSchool = chkIsFirstTimeInSchool.Checked
            .IsFirstTimePostSecSchool = chkIsFirstTimePostSecSchool.Checked
            '....................................


            If txtEnrollDate.SelectedDate Is Nothing Then
                .EnrollDate = ""
            Else
                .EnrollDate = CType(Date.Parse(CType(txtEnrollDate.SelectedDate, String)), String)
            End If
            If txtExpStartDate.SelectedDate Is Nothing Then
                .ExpStartDate = ""
            Else
                .ExpStartDate = CType(Date.Parse(CType(txtExpStartDate.SelectedDate, String)), String)
            End If
            If txtStartDate.SelectedDate Is Nothing Then
                .StartDate = ""
            Else
                .StartDate = CType(Date.Parse(CType(txtStartDate.SelectedDate, String)), String)
            End If

            If txtLDA.SelectedDate Is Nothing Then
                .LDA = ""
            Else
                .LDA = CType(Date.Parse(CType(txtLDA.SelectedDate, String)), String)
            End If

            If txtDateDetermined.Text = "" Then
                .DateDetermined = ""
            Else
                .DateDetermined = CType(Date.Parse(txtDateDetermined.Text), String)
            End If
            If txtMidPtDate.SelectedDate Is Nothing Then
                .MidPtDate = ""
            Else
                .MidPtDate = CType(Date.Parse(CType(txtMidPtDate.SelectedDate, String)), String)
            End If
            If txtExpGradDate.SelectedDate Is Nothing Then
                .ExpGradDate = ""
            Else
                .ExpGradDate = CType(Date.Parse(CType(txtExpGradDate.SelectedDate, String)), String)
            End If
            If txtContractedGradDate.SelectedDate Is Nothing Then
                .ContractedGradDate = ""
            Else
                .ContractedGradDate = CType(Date.Parse(CType(txtContractedGradDate.SelectedDate, String)), String)
            End If
            If txtTransferDate.Text = "" Then
                .TransferDate = ""
            Else
                .TransferDate = CType(Date.Parse(txtTransferDate.Text), String)
            End If
            If (txtReEnrollmentDate.Text = "") Then
                .ReenrollDate = ""
            Else
                .ReenrollDate = txtReEnrollmentDate.Text
            End If

            If txtCohortStartDate.SelectedDate Is Nothing Then
                .CohortStartDate = CType(txtExpStartDate.SelectedDate, String)
            Else
                .CohortStartDate = CType(txtCohortStartDate.SelectedDate, String)
            End If
            If txtGraduatedOrReceivedDate.SelectedDate Is Nothing Then
                .GraduatedOrReceivedDate = ""
            Else
                .GraduatedOrReceivedDate = CType(Date.Parse(CType(txtGraduatedOrReceivedDate.SelectedDate, String)), String)
            End If

            If ddlSAPId.SelectedValue = "" Then
                .SAPId = ""
            Else
                .SAPId = ddlSAPId.SelectedValue
            End If

            .AcadamicAdvisor = ddlEmpId.SelectedValue
            .FaAdvisor() = ddlFAAdvisorId.SelectedValue
            .ShiftId = ddlShiftId.SelectedValue
            .GrdLvlId = ddlEdLvlId.SelectedValue
            .BillingMethodId = ddlBillingMethodId.SelectedValue
            .ProgramVersionID = ddlPrgVerId.SelectedValue
            .EnrollmentId = txtEnrollmentId.Text
            .TuitionCategory = ddlTuitionCategoryId.SelectedValue
            .DropReasonId = ddlDropReasonId.SelectedValue
            .AttendTypeId = ddlAttendTypeId.SelectedValue
            .DegCertSeekingId = ddlDegCertSeekingId.SelectedValue
            '' Suva Stuff jguirado...
            If ddlSuvaVersion.SelectedValue.ToString = String.Empty Then
                .ProgramVersionTypeId = 0
            Else
                .ProgramVersionTypeId = CType(ddlSuvaVersion.SelectedValue, Integer)
            End If

            If txtTransferHours.Text.Trim = "" Then
                .TransferHours = 0
            Else
                .TransferHours = CType(txtTransferHours.Text.Trim, Decimal)
            End If
            .BadgeNumber = txtbadgenumber.Text.Trim
            'US3153
            .DistanceEdStatus = rblDistanceEducation.SelectedValue.ToString
            If (disableAutoChargeContainer.Visible) Then
                If ddlDisableAutoCharge.SelectedValue.ToString() = "0" Then
                    .DisableAutoCharge = False
                ElseIf ddlDisableAutoCharge.SelectedValue.ToString() = "1" Then
                    .DisableAutoCharge = True
                Else
                    .DisableAutoCharge = False
                End If

            End If
            .ThirdPartyContract = ddlThirdPartyContract.SelectedValue.Equals("1")

            Dim licensureExamSave = ddlWrittenAllParts.SelectedValue.Equals("1")
            'Licensure Exam Details
            .LicensureWrittenAllParts = ddlWrittenAllParts.SelectedValue.Equals("1")
            .LicensureLastPartWrittenOn = If(licensureExamSave, dpLastPart.SelectedDate, Nothing)
            .LicensurePassedAllParts = licensureExamSave AndAlso ddlPassedAllParts.SelectedValue.Equals("1")

        End With
        Return stuEnrollInfo
    End Function
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Try
            ClearRhs()
            StudentId = txtStudentId.Text
            BindEnrollmentDataList(txtStudentId.Text)
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)

            ddlPrgVerId.Enabled = True
            ViewState("MODE") = "NEW"
            txtStuEnrollId.Text = Guid.NewGuid.ToString
            'To Generate EnrollmentID 
            Dim fullName() As String = Split((New StuEnrollFacade).StudentName(StudentId))
            strFirstName = fullName(0)
            strLastName = fullName(1)
            txtEnrollmentId.Text = enrollmentId.GenerateEnrollmentID(strLastName, strFirstName)
            rblDistanceEducation.SelectedValue = "None"

            ddlStatusCodeId.Enabled = True
            Linkbutton2.Enabled = False
            Linkbutton3.Enabled = False
            'Linkbutton4.Enabled = False
            ' Set ComboBox Major,Muns to None
            ddlSuvaVersion.SelectedIndex = 0

            ''the linkRescheduleReasonButton is disabled when in New mode
            lnkbtnReschReason.Enabled = False
            lnkbtnStatusChangeHistory.Enabled = False
            BuildLeadGroupsDDL()
            FillTransferFromProgramDDL(LeadId)

            chkLeadGrpId.ClearSelection()

            sdfControls.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
            BuildEnrollStatusDDL(StateEnum.NoStart)
            CommonWebUtilities.RestoreItemValues(dlstStdEnroll, Guid.Empty.ToString)

            With ddlSchedules
                .Items.Clear()
                .SelectedValue = Nothing
            End With
            ibToMaintPage_Schedule.Visible = False
            BuildCampusDDL("", campusId)

            disableAutoChargeContainer.Visible = False
            ddlThirdPartyContract.SelectedIndex = 0
            'The drop reason and date determined fields should be hidden
            lblDropReasonId.Visible = False
            lblDateDetermined.Visible = False
            txtDateDetermined.Attributes.Add("style", "display:none")
            ddlDropReasonId.Attributes.Add("style", "display:none")

            trackmessagecontrols.Visible = False
            lblTransferFromCampusValue.Text = ""
            LblTransferFromProgramValue.Text = ""
            LblTransferToCampusValue.Text = ""
            LblTransferToProgramValue.Text = ""
            lblTransferDateValue.Text = ""
            txtTransferedBy.Text = ""
            txtTransferHours.Text = ""
            txtTotalTransferHours.Text = ""
            txtTotalTransferHoursFromThisSchool.Text = ""
            ddTransferHourFromProgram.SelectedIndex = 0
            'ViewScheduleDetails.Visible = False
            EnableDisableTotalTransferHours()
            dpLastPart.Clear()
            ddlWrittenAllParts.SelectedIndex = 0
            ddlPassedAllParts.SelectedIndex = 0
            ToggleLicensingExamDetails()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim fac As New StuEnrollFacade
        Dim msg As String
        Try
            'objCommon.DoDelete(Form1)
            msg = fac.DeleteStuEnrollment(txtStuEnrollId.Text, Date.Parse(txtModDate.Text))
            If msg <> "" Then
                DisplayErrorMessage(msg)
                Exit Sub
            End If
            ClearRhs()

            dlstStdEnroll.SelectedIndex = -1
            StudentId = txtStudentId.Text
            BindEnrollmentDataList(txtStudentId.Text)

            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtStuEnrollId.Text = Guid.NewGuid.ToString

            Dim FullName() As String = Split(fac.StudentName(StudentId))
            strFirstName = FullName(0)
            strLastName = FullName(1)
            ddlStatusCodeId.Enabled = True
            disableAutoChargeContainer.Visible = False
            ddlThirdPartyContract.SelectedIndex = 0
            sdfControls.DeleteSDFValue(txtStuEnrollId.Text)
            sdfControls.GenerateControlsNew(pnlSDF, resourceId, ModuleId)

            chkLeadGrpId.ClearSelection()

            'To Generate EnrollmentID 
            txtEnrollmentId.Text = enrollmentId.GenerateEnrollmentID(strLastName, strFirstName)
            BuildEnrollStatusDDL(StateEnum.NoStart)
            CommonWebUtilities.RestoreItemValues(dlstStdEnroll, Guid.Empty.ToString)
            trackmessagecontrols.Visible = False
            lblTransferFromCampusValue.Text = ""
            LblTransferFromProgramValue.Text = ""
            LblTransferToCampusValue.Text = ""
            LblTransferToProgramValue.Text = ""
            lblTransferDateValue.Text = ""
            txtTransferedBy.Text = ""
            txtTransferHours.Text = ""
            txtTotalTransferHours.Text = ""
            txtTotalTransferHoursFromThisSchool.Text = ""
            FillTransferFromProgramDDL(LeadId)
            EnableDisableTotalTransferHours()
            ddTransferHourFromProgram.SelectedIndex = 0
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control
        'Dim objCommon As New CommonWebUtilities
        Dim ddl As DropDownList
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If (ctl.GetType Is GetType(RadDatePicker)) Then
                    CType(ctl, RadDatePicker).SelectedDate = Nothing
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = -1
                    ddl = CType(ctl, DropDownList)
                    CommonWebUtilities.CleanAdvantageListItemCollection(ddl.Items)
                End If
            Next
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstStdEnroll_ItemCommand(sender As Object, e As DataListCommandEventArgs) Handles dlstStdEnroll.ItemCommand
        Dim selIndex As Integer
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim probCount As Integer
        Dim loaCount As Integer
        'Dim warnCount As Integer
        Dim suspCount As Integer
        Dim reschReasonCount As Integer
        Dim statusChangeHist As Integer
        ddlPrgVerId.Enabled = False
        Try
            Master.PageObjectId = CType(e.CommandArgument, String)
            Master.PageResourceId = resourceId
            Master.SetHiddenControlForAudit()
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add("updatehdnObjectId('" + Master.PageObjectId + "');")
            BuildEnrollStatusDDL()
            strGUID = dlstStdEnroll.DataKeys(e.Item.ItemIndex).ToString()

            'To populate the Program Version Id, EmpId fields
            Dim stuEnrollID As String
            stuEnrollID = strGUID 'This Statement Passes the Primary Key Value To Variable
            txtStuEnrollId.Text = stuEnrollID

            'to update the LDA in enrollments table always from the attendance table
            If MyAdvAppSettings.AppSettings("ManuallySetLDA").ToString.ToLower = "no" Then
                UpdateLdaForManuallySetLdaAsNo(txtStuEnrollId.Text)
            End If
            BuildAdmissionRepsDDL()

            'Get Student Probation, LOA and Warning count. And 
            'enable the links accordingly
            probCount = facade.GetProbationCount(stuEnrollID)
            loaCount = facade.GetLOACount(stuEnrollID)
            suspCount = facade.GetSuspensionCount(stuEnrollID)
            reschReasonCount = facade.GetRescheduleReasonCount(stuEnrollID)
            statusChangeHist = facade.GetStatusChangeHistoryCount(stuEnrollID)

            If probCount > 0 Then
                Linkbutton3.Enabled = True
                Linkbutton3.Text = "<span style='text-decoration:underline'>Probations / Warnings</span>"
            Else
                Linkbutton3.Enabled = False
            End If

            If loaCount > 0 Then
                Linkbutton2.Enabled = True
                Linkbutton2.Text = "<span style='text-decoration:underline'>LOAs</span>"
            Else
                Linkbutton2.Enabled = False
            End If
            If suspCount > 0 Then
                Linkbutton5.Enabled = True
                Linkbutton5.Text = "<span style='text-decoration:underline'>Suspensions</span>"
            Else
                Linkbutton5.Enabled = False
            End If
            ''If records are there in RescheduleReason then The linkbutton is enabled
            If reschReasonCount > 0 Then
                lnkbtnReschReason.Enabled = True
                lnkbtnReschReason.Text = "<span style='text-decoration:underline'>Reschedules</span>"
            Else
                lnkbtnReschReason.Enabled = False
            End If
            If statusChangeHist > 0 Then
                lnkbtnStatusChangeHistory.Enabled = True
                lnkbtnStatusChangeHistory.Text = "<span style='text-decoration:underline'>Status Change History</span>"
            Else
                lnkbtnStatusChangeHistory.Enabled = False
            End If
            GetStudentInfo(strGUID)
            BuildAdminRepDDL(stuMasterInfo.AdmissionsRep)
            BuildAcadAdvisorDDL(CType(ViewState("CampusId"), String), stuMasterInfo.AcadamicAdvisor)
            BuildFaAdvisorIdDDL(stuMasterInfo.FaAdvisor())
            BuildScheduleDDL(stuMasterInfo.ProgramVersionID)

            FillTransferFromProgramDDL(LeadId, stuEnrollID)

            BindStudentInfoExist(stuMasterInfo)


            If stuMasterInfo.SAPId = "" Then
                ddlSAPId.SelectedIndex = 0
            Else
                ddlSAPId.SelectedValue = stuMasterInfo.SAPId
            End If
            txtStuEnrollId.Text = stuEnrollID
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstStdEnroll.SelectedIndex = selIndex
            BindEnrollmentDataList(txtStudentId.Text)

            BindLeadGroupList()

            ddlIsDisabled.SelectedValue = If(stuMasterInfo.IsDisabled Is Nothing, "-1", stuMasterInfo.IsDisabled.ToString())
            chkIsFirstTimeInSchool.Checked = stuMasterInfo.IsFirstTimeInSchool
            chkIsFirstTimePostSecSchool.Checked = stuMasterInfo.IsFirstTimePostSecSchool
            If stuMasterInfo.EntranceInterviewDate Is Nothing Then
                EntranceInterviewDate.Clear()
            Else
                EntranceInterviewDate.SelectedDate = stuMasterInfo.EntranceInterviewDate
            End If

            BuildBillingMethodDataSet()
            ddlDisableAutoCharge.SelectedValue = If(stuMasterInfo.DisableAutoCharge, "1", "0")
            ChargingMethodChanged(ddlBillingMethodId, EventArgs.Empty)

            ddlThirdPartyContract.SelectedValue = If(stuMasterInfo.ThirdPartyContract, "1", "0")

            sdfControls.GenerateControlsEdit(pnlSDF, resourceId, stuEnrollID, ModuleId)

            ddlStatusCodeId.Enabled = False
            If strGUID <> "" Then
                If MyAdvAppSettings.AppSettings("ShowRescheduleButtonOnEnrollmentTab").ToLower = "true" Then
                    btnReschedule.Visible = True
                    lblReschReason.Visible = True
                    txtReschReason.Visible = True
                    ddlnewPrgVersion.Visible = True
                    lblnewPrgVersion.Visible = True

                    SetRescheduleButtonState(strGUID)
                End If
            End If

            Dim IsClockHourProgram As Boolean
            IsClockHourProgram = facade.IsStudentProgramClockHourType(stuEnrollID)
            If IsClockHourProgram = True Then
                lbltransferHours.Visible = True
                txtTransferHours.Visible = True
            End If
            If stuMasterInfo.DistanceEdStatus.Length > 0 Then
                Select Case stuMasterInfo.DistanceEdStatus.ToString
                    Case "Some"
                        rblDistanceEducation.SelectedValue = "Some"
                    Case "Only"
                        rblDistanceEducation.SelectedValue = "Only"
                    Case Else
                        rblDistanceEducation.SelectedValue = "None"
                End Select
            Else
                rblDistanceEducation.SelectedValue = "None"
            End If
            CommonWebUtilities.RestoreItemValues(dlstStdEnroll, strGUID)


            EnableDisableTotalTransferHours(Nothing, stuMasterInfo.TracksTransfer)
            BindTimeClockSchedules()

            txtBadgeNumber.Text = SchedulesFacade.GetBadgeNumberforStudentEnrollment(stuEnrollID)

            ToggleLicensingExamDetails()

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "matchLSSelectionAndStudentBarScript", "matchLSSelectionAndStudentBar();", True)

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstStdEnroll_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstStdEnroll_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        Dim fac As New TranscriptFacade
        If Not AdvantageSession.UserState.IsUserSA And fac.IsStudentCurrentlyInSchool(txtStuEnrollId.Text) = False Then
            btnSave.Enabled = False
            DisplayErrorMessage("Data cannot be saved for out of school enrollments")
        End If
    End Sub

    Private Sub GetStudentInfo(stuEnrollID As String)
        Dim studentinfo As New StuEnrollFacade
        stuMasterInfo = studentinfo.GetStudentDetails(stuEnrollID)
    End Sub

    Public Function IsDropStatus(gradeDS As DataSet, status As String) As Boolean
        Dim isdrop = False

        'Make the ChildId column the primary key
        With gradeDS.Tables(0)
            .PrimaryKey = New DataColumn() { .Columns("StatusCodeId")}
        End With

        If (gradeDS.Tables(0).Rows.Count > 0) Then
            Dim row2 As DataRow = gradeDS.Tables(0).Rows.Find(status)
            isdrop = Not (row2 Is Nothing)
        End If

        Return isdrop
    End Function

    Private Sub BindStudentInfoExist(StudentInfo As StudentEnrollmentInfo)
        Dim fac As New TerminateFacade
        Dim gradesDS As DataSet
        'Bind The StudentInfo Data From The Database
        Try
            With StudentInfo
                If .ProgramVersionID <> Guid.Empty.ToString Then
                    'ddlPrgVerId.SelectedValue = .ProgramVersionID
                    'set selected value in ddl
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrgVerId, .ProgramVersionID.ToString, .ProgramVersion)
                    'ddlPrgVerId.Enabled = (New StudentEnrollmentFacade).IsThisEnrollmentBeingUsed(StudentInfo.StuEnrollmentId)

                    btnCalculateContractedGradDate.Visible = facade.IsProgramVersionTimeClock(.ProgramVersionID)
                Else
                    BuildPrgVersionDDL()
                End If
                If (.ScheduleId <> "" And Not (ddlSchedules.Items.FindByValue(.ScheduleId)) Is Nothing) Then
                    ddlSchedules.SelectedValue = .ScheduleId
                End If
                If .AcadamicAdvisor <> Guid.Empty.ToString Or .AcadamicAdvisor <> "" Then
                    'ddlEmpId.SelectedValue = .AcadamicAdvisor
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlEmpId, .AcadamicAdvisor.ToString, .AcadadvisorText)
                Else
                    BuildAcadAdvisorDDL(CType(ViewState("CampusId"), String))
                End If

                If .FaAdvisor() <> Guid.Empty.ToString Or .FaAdvisor() <> "" Then
                    'ddlFAAdvisorId.SelectedValue = .FAAdvisor
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFAAdvisorId, .FaAdvisor().ToString, .FAadvisorText)
                Else
                    BuildFaAdvisorIdDDL()
                End If

                txtTotalTransferHours.Text = stuMasterInfo.TransferHours
                txtTotalTransferHoursFromThisSchool.Text = stuMasterInfo.TotalTransferHoursFromThisSchool

                Dim indexTransferFromrogram = ddTransferHourFromProgram.Items.FindByValue(stuMasterInfo.TransferHoursFromProgramId)
                ddTransferHourFromProgram.SelectedIndex = ddTransferHourFromProgram.Items.IndexOf(indexTransferFromrogram)
                EnableDisableTotalTransferHours(Nothing, stuMasterInfo.TracksTransfer)
                BuildAdmissionRepsDDL()
                Dim exists As ListItem = ddlAdminRep.Items.FindByValue(.AdmissionsRep.ToString)
                If exists Is Nothing Then
                    ddlAdminRep.SelectedIndex = 0
                Else
                    ddlAdminRep.SelectedValue = .AdmissionsRep.ToString
                End If

                txtEnrollmentId.Text = .EnrollmentId
                If .BillingMethodId <> Guid.Empty.ToString Or .BillingMethodId <> "" Then
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlBillingMethodId, .BillingMethodId.ToString, .BillingMethod)
                End If
                Try
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTuitionCategoryId, .TuitionCategory.ToString, .TuitionCatText)
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlTuitionCategoryId.SelectedIndex = 0
                End Try

                If .ShiftId <> Guid.Empty.ToString Or .ShiftId <> "" Then
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlShiftId, .ShiftId.ToString, .Shift)
                End If
                If .GrdLvlId <> Guid.Empty.ToString Or .GrdLvlId <> "" Then
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlEdLvlId, .GrdLvlId.ToString, .GradeLevel)
                End If
                If Not String.IsNullOrEmpty(.DropReasonId) And .DropReasonId <> Guid.Empty.ToString() Then
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDropReasonId, .DropReasonId.ToString, .DropReason)
                End If
                If .StatusCodeId <> Guid.Empty.ToString Or .StatusCodeId <> "" Then
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusCodeId, .StatusCodeId.ToString, .StatusCode)
                End If
                If .CampusId <> Guid.Empty.ToString Or .CampusId <> "" Then
                    BuildCampusDDL("", .CampusId)
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCampusId, .CampusId.ToString, .Campus)
                End If
                If .AttendTypeId <> Guid.Empty.ToString Or .AttendTypeId <> "" Then
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAttendTypeId, .AttendTypeId.ToString, .AttendType)
                End If

                If .DegCertSeekingId <> Guid.Empty.ToString Or .DegCertSeekingId <> "" Then
                    CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDegCertSeekingId, .DegCertSeekingId.ToString, .DegCertSeekingText)
                End If

                If .EnrollDate <> "" Then
                    txtEnrollDate.SelectedDate = CType(.EnrollDate, Date?)
                Else
                    txtEnrollDate.SelectedDate = Nothing
                End If

                If .LDA <> "" Then
                    txtLDA.SelectedDate = CType(.LDA, Date?)
                Else
                    txtLDA.SelectedDate = Nothing
                End If
                If .DateDetermined <> String.Empty Then
                    Dim index As Integer = .DateDetermined.IndexOf(" ", StringComparison.Ordinal)
                    txtDateDetermined.Text = If(index = -1, .DateDetermined, .DateDetermined.Remove(index))
                Else
                    txtDateDetermined.Text = String.Empty
                End If

                If .ExpStartDate <> "" Then
                    txtExpStartDate.SelectedDate = CType(.ExpStartDate, Date?)
                Else
                    txtExpStartDate.SelectedDate = Nothing
                End If

                If .TransferDate <> String.Empty Then
                    Dim index As Integer = .TransferDate.IndexOf(" ", StringComparison.Ordinal)
                    txtTransferDate.Text = If(index = -1, .TransferDate, .TransferDate.Remove(index))


                End If

                If .MidPtDate <> "" Then
                    txtMidPtDate.SelectedDate = CType(.MidPtDate, Date?)
                Else
                    txtMidPtDate.SelectedDate = Nothing
                End If

                If .ExpGradDate <> "" Then
                    txtExpGradDate.SelectedDate = CType(.ExpGradDate, Date?)
                Else
                    txtExpGradDate.SelectedDate = Nothing
                End If

                If .ContractedGradDate <> "" Then
                    txtContractedGradDate.SelectedDate = CType(.ContractedGradDate, Date?)
                Else
                    txtContractedGradDate.SelectedDate = Nothing
                End If

                If .StartDate <> "" Then
                    txtStartDate.SelectedDate = CType(.StartDate, Date?)
                Else
                    txtStartDate.SelectedDate = Nothing
                End If


                ddlWrittenAllParts.SelectedValue = If(.LicensureWrittenAllParts, "1", "0")
                dpLastPart.SelectedDate = .LicensureLastPartWrittenOn
                ddlPassedAllParts.SelectedValue = If(.LicensurePassedAllParts, "1", "0")

                'Set the Delete Button so it prompts the user for confirmation when clicked
                txtStartDate.Attributes.Add("onChange", "alert('Warning: changing Start Date could make student data inconsistent.')")
                txtModUser.Text = .ModUser
                txtModDate.Text = CType(.ModDate, String)

                'If the student has a drop status then make the lda and date determined

                'txt boxes to true.
                gradesDS = fac.GetDropStatuses()
                'Check if the selected grade exists in the drop grades
                If (IsDropStatus(gradesDS, ddlStatusCodeId.SelectedValue.ToString)) = True Then
                    lblDateDetermined.Visible = True
                    lblLDA.Visible = True
                    'txtDateDetermined.Visible = True

                    txtDateDetermined.Attributes.Remove("style")
                    txtDateDetermined.Attributes.Add("style", "display:inline")
                    txtDateDetermined.Enabled = True
                    'Mark the date determined label with red * if it does not contain one
                    If lblDateDetermined.Text.Contains("*") Then
                        'Do nothing
                    Else
                        lblDateDetermined.Text = lblDateDetermined.Text & "<font color=""red"">*</font>"
                    End If

                    txtLDA.Visible = True
                    If .DropReasonId <> Guid.Empty.ToString Or .DropReasonId <> "" Then
                        'ddlDropReasonId.SelectedValue = .DropReasonId
                        CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDropReasonId, .DropReasonId.ToString, .DropReason)
                        lblDropReasonId.Visible = True
                        'ddlDropReasonId.Visible = True
                        ddlDropReasonId.Attributes.Remove("style")
                        ddlDropReasonId.Attributes.Add("style", "display:inline")
                        'Mark the drop reason label with red * if it does not contain one
                        If lblDropReasonId.Text.Contains("*") Then
                            'Do nothing
                        Else
                            lblDropReasonId.Text = lblDropReasonId.Text & "<font color=""red"">*</font>"
                        End If
                        ddlDropReasonId.Enabled = True
                    End If
                Else
                    lblDateDetermined.Visible = False
                    lblLDA.Visible = False
                    'txtDateDetermined.Visible = False
                    txtDateDetermined.Attributes.Remove("style")
                    txtDateDetermined.Attributes.Add("style", "display:none")
                    txtDateDetermined.Enabled = False
                    txtLDA.Visible = False
                    lblDropReasonId.Visible = False
                    'ddlDropReasonId.Visible = False
                    ddlDropReasonId.Attributes.Remove("style")
                    ddlDropReasonId.Attributes.Add("style", "display:none")
                    ddlDropReasonId.Enabled = False
                End If


                'for traditional schools get the LDA from the different attendance tables
                '                If SingletonAppSettings.AppSettings("SchedulingMethod", CampusId) = "Traditional" Then
                lblLDA.Visible = True
                txtLDA.Visible = True
                Dim lda As Date = (New ClsSectAttendanceFacade).GetLDA(txtStuEnrollId.Text)
                If Not (lda = Date.MinValue) Then
                    If txtLDA.SelectedDate Is Nothing Then
                        txtLDA.SelectedDate = CType(lda.ToShortDateString(), Date?)
                    Else
                        If Date.Parse(CType(txtLDA.SelectedDate, String)) < lda.Date Then txtLDA.SelectedDate = CType(lda.ToShortDateString(), Date?)
                    End If

                End If

                If .ReenrollDate <> "" Then
                    txtReEnrollmentDate.Text = .ReenrollDate
                Else
                    txtReEnrollmentDate.Text = ""
                End If
                If .CohortStartDate <> "" Then
                    txtCohortStartDate.SelectedDate = CType(.CohortStartDate, Date?)
                ElseIf .ExpStartDate <> "" Then
                    txtCohortStartDate.SelectedDate = CType(.ExpStartDate, Date?)
                Else
                    txtCohortStartDate.SelectedDate = Nothing
                End If

                txtTransferHours.Text = CType(.TransferHours, String)

                If .GraduatedOrReceivedDate <> "" Then
                    Dim gord = CType(.GraduatedOrReceivedDate, Date?)
                    txtGraduatedOrReceivedDate.SelectedDate = gord
                Else
                    txtGraduatedOrReceivedDate.SelectedDate = Nothing
                End If

                If .BadgeNumber <> "" Then
                    txtbadgenumber.Text = .BadgeNumber
                End If

                'If school tracks attendance ByDay and if program the student is enrolled in uses time clock then we should enable this field for them on the enrollments tab. 
                If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" AndAlso .UseTimeClock = True Then
                    txtbadgenumber.Enabled = True
                ElseIf MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byclass" And MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                    txtbadgenumber.Enabled = True
                Else
                    txtbadgenumber.Enabled = False
                End If

                txtDistanceEdStatus.Text = .DistanceEdStatus
            End With

            ''To Clear the contents of RescheduleReason
            txtReschReason.Text = ""

            ''Fill ComboBox Program Version Type. Only visible in SUVA
            If ddlSuvaVersion.Items.Count > 2 Then
                ddlSuvaVersion.SelectedValue = CType(StudentInfo.ProgramVersionTypeId, String)
            End If

            ''To bind the active programs except the one in the program version ddl
            BuildNewPrgVersionDdl()


        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error Binding student information: " & ex.Message & " "
            Else
                Session("Error") = "Error Binding student information: " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub radStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radStatus.SelectedIndexChanged
        Dim objCommon As New CommonUtilities
        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtStuEnrollId.Text = Guid.NewGuid.ToString
            BindEnrollmentDataList(txtStudentId.Text)
            txtEnrollmentId.Text = enrollmentId.GenerateEnrollmentID(strLastName, strFirstName)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ToggleBaddgeNumberSection()
        Dim programVersionDA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim programVersionUsesTimeClock = programVersionDA.DoesProgramVersionUseTimeClock(ddlPrgVerId.SelectedValue)

        If (programVersionUsesTimeClock IsNot Nothing) Then
            lblbadgenumber.Visible = programVersionUsesTimeClock
            txtbadgenumber.Visible = programVersionUsesTimeClock
            btnNewBadgeNum.Visible = programVersionUsesTimeClock
        End If
    End Sub

    Private Sub ddlPrgVerId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrgVerId.SelectedIndexChanged
        Dim prgVerId As String
        'Dim Facade As New StuEnrollFacade
        Dim billingMethod As String
        prgVerId = ddlPrgVerId.SelectedValue.ToString
        If prgVerId <> "" Then
            billingMethod = facade.GetPrgVerBillingMethod(prgVerId, CType(ViewState("CampusId"), String))
            If billingMethod <> Guid.Empty.ToString Or billingMethod <> "" Then
                ddlBillingMethodId.SelectedValue = billingMethod
            End If

            ToggleBaddgeNumberSection()


            BuildScheduleDDL(prgVerId)

            btnCalculateContractedGradDate.Visible = facade.IsProgramVersionTimeClock(prgVerId)

            Dim prgVerIsClockHour = facade.IsProgramVersionClockHour(prgVerId)

            If (prgVerIsClockHour) Then
                EnableDisableTotalTransferHours(True)
            Else
                EnableDisableTotalTransferHours(False)
            End If
        End If
    End Sub
    Private Sub BuildScheduleDDL(ByVal prgVerId As String)

        Dim ds As DataSet = facade.GetScheduleByProgramVersion(prgVerId)
        ViewState("Schedules") = ds
        'ddlSchedules
        'ddlSchedules
        If ds.Tables.Item(0).Rows.Count = 0 Then
            With ddlSchedules
                .Items.Clear()
            End With
        Else
            With ddlSchedules
                .Items.Clear()
                .SelectedValue = Nothing
                .DataTextField = "Descrip"
                .DataValueField = "ScheduleId"
                .DataSource = ds
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If

    End Sub
    Private Sub SaveScheduleForEnrollment(ByVal StuEnrollId As String, ByVal ScheduleId As String, ByVal User As String, ByVal PrgVerId As String)
        Dim errorMsg As String = facade.SaveScheduleForEnrollment(StuEnrollId, ScheduleId, User, PrgVerId)
        If (errorMsg <> "") Then
            DisplayErrorMessage(errorMsg)
        End If
    End Sub
    Private Sub DisplayErrorMessage(errorMessage As String)
        CommonWebUtilities.AlertAjax(errorMessage, CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager))
    End Sub

    Private Sub Linkbutton2_Click(sender As Object, e As EventArgs) Handles Linkbutton2.Click
        Dim sb As New StringBuilder
        sb.Append("&StuEnrollId=" + txtStuEnrollId.Text)

        'Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name = "StdLOAs"
        Dim csType As Type = Page.GetType
        Dim url As String = "../AR/" + name + ".aspx?resid=345&mod=AR&cmpid=" + campusId + sb.ToString
        Dim fullUrl As String = "window.open('" + url + "', '_blank', 'status=yes,resizable=yes,scrollbars=yes,width=900px,height=550px,fullscreen=no' );"

        ScriptManager.RegisterStartupScript(Page, csType, "OPEN_WINDOW", fullUrl, True)

    End Sub

    Private Sub Linkbutton3_Click(sender As Object, e As EventArgs) Handles Linkbutton3.Click
        Dim sb As New StringBuilder
        sb.Append("&StuEnrollId=" + txtStuEnrollId.Text)

        'Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Const name = "StdProbations"
        Dim csType As Type = Page.GetType
        Dim url As String = "../AR/" + name + ".aspx?resid=346&mod=AR&cmpid=" + campusId + sb.ToString
        Dim fullUrl As String = "window.open('" + url + "', '_blank', 'status=yes,resizable=yes,scrollbars=yes,width=900px,height=550px,fullscreen=no' );"

        ScriptManager.RegisterStartupScript(Page, csType, "OPEN_WINDOW", fullUrl, True)

    End Sub
    Private Sub btnCalculateContractedGradDate_Click(sender As Object, e As EventArgs) Handles btnCalculateContractedGradDate.Click
        Try

            Dim token As TokenResponse

            token = CType(Session("AdvantageApiToken"), TokenResponse)

            Dim graduationCalculator As New Fame.Advantage.Api.Library.Common.GraduationCalculator(token.ApiUrl, token.Token)

            If (Not String.IsNullOrEmpty(ddlSchedules.SelectedValue) And (txtExpStartDate.SelectedDate.HasValue)) Then
                Dim result = graduationCalculator.CalculateContractedGraduationDate(Guid.Parse(ddlSchedules.SelectedValue), txtExpStartDate.SelectedDate.Value, Guid.Parse(campusId))
                If (result.ResultStatus = Enums.ResultStatus.Success) Then
                    txtContractedGradDate.SelectedDate = result.Result
                Else
                    DisplayErrorMessage(result.ResultStatusMessage)
                End If
            Else
                DisplayErrorMessage("Schedule and expected start date must be provided to calculate graduation date.")
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage("Unable to calculate contracted grad date. An exception has occurred: " + ex.Message)
        End Try

    End Sub
    Private Sub btnNewBadgeNum_Click(sender As Object, e As EventArgs) Handles btnNewBadgeNum.Click
        Try
            Dim campusId = ddlCampusId.SelectedValue
            Dim token As TokenResponse
            token = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim enrollmentRequest As New Fame.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(token.ApiUrl, token.Token)
            Dim result = enrollmentRequest.GetNewBadgeNumber(Guid.Parse(campusId))
            If (result <> "") Then
                txtbadgenumber.Text = result
            Else
                DisplayErrorMessage("Unable to calculate a new badge number.")
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage("Unable to calculate a new badge number. An exception has occurred: " + ex.Message)
        End Try

    End Sub

    Private Sub SelectFirstEnrollmentWhenPageFirstLoads()
        Dim selIndex As Integer
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim probCount As Integer
        Dim loaCount As Integer
        'Dim warnCount As Integer
        Dim suspCount As Integer
        Dim reschReasonCount As Integer
        Dim statusChangeHist As Integer
        Try

            strGUID = Session("CurrentEnrollmentId")
            If strGUID = "" Then
                'if there are no enrollments there is no way to select the first one
                If dlstStdEnroll.DataKeys.Count = 0 Then Return
                strGUID = dlstStdEnroll.DataKeys(0).ToString()
            End If
            rblDistanceEducation.SelectedIndex = -1
            If strGUID <> "" Then
                If MyAdvAppSettings.AppSettings("ShowRescheduleButtonOnEnrollmentTab").ToLower = "true" Then
                    btnReschedule.Visible = True
                    ''REschedule Reason label and textbox are added
                    lblReschReason.Visible = True
                    txtReschReason.Visible = True
                    ddlnewPrgVersion.Visible = True
                    lblnewPrgVersion.Visible = True
                    SetRescheduleButtonState(strGUID)
                End If
            End If

            Dim stuEnrollID As String
            stuEnrollID = strGUID 'This Statement Passes the Primary Key Value To Variable
            txtStuEnrollId.Text = stuEnrollID
            'to update the LDA in enrollments table always from the attendance table
            If MyAdvAppSettings.AppSettings("ManuallySetLDA").ToString.ToLower = "no" Then
                UpdateLdaForManuallySetLdaAsNo(txtStuEnrollId.Text)
            End If
            BuildAdmissionRepsDDL()
            'Get Student Probation, LOA and Warning count. And 
            'enable the links accordingly
            probCount = facade.GetProbationCount(stuEnrollID)
            loaCount = facade.GetLOACount(stuEnrollID)
            'warnCount = facade.GetWarningCount(stuEnrollID)
            suspCount = facade.GetSuspensionCount(stuEnrollID)
            reschReasonCount = facade.GetRescheduleReasonCount(stuEnrollID)
            statusChangeHist = facade.GetStatusChangeHistoryCount(stuEnrollID)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim currentProb As List(Of String) = facade.GetCurrentProbations(stuEnrollID)

            If ((currentProb IsNot Nothing) And (currentProb.Count > 0)) Then
                lblCurrentProb.Visible = True
                lblCurrentProb.Text = "On Disciplinary Probation from " & currentProb.Item(0) & " to " & currentProb.Item(1)
            End If

            If probCount > 0 Then
                Linkbutton3.Enabled = True
                Linkbutton3.Text = "<span style='text-decoration:underline'>Probations / Warnings</span>"
            Else
                Linkbutton3.Enabled = False
            End If

            If loaCount > 0 Then
                Linkbutton2.Enabled = True
                Linkbutton2.Text = "<span style='text-decoration:underline'>LOAs</span>"
            Else
                Linkbutton2.Enabled = False
            End If

            If suspCount > 0 Then
                Linkbutton5.Enabled = True
                Linkbutton5.Text = "<span style='text-decoration:underline'>Suspensions</span>"
            Else
                Linkbutton5.Enabled = False
            End If
            ''If records are there in RescheduleReason then The link-button is enabled
            If reschReasonCount > 0 Then
                lnkbtnReschReason.Enabled = True
                lnkbtnReschReason.Text = "<span style='text-decoration:underline'>Reschedules</span>"
            Else
                lnkbtnReschReason.Enabled = False
            End If
            If statusChangeHist > 0 Then
                lnkbtnStatusChangeHistory.Enabled = True
                lnkbtnStatusChangeHistory.Text = "<span style='text-decoration:underline'>Status Change History</span>"
            Else
                lnkbtnStatusChangeHistory.Enabled = False
            End If
            GetStudentInfo(strGUID)
            BuildAdminRepDDL(stuMasterInfo.AdmissionsRep)
            BuildAcadAdvisorDDL(CType(ViewState("CampusId"), String), stuMasterInfo.AcadamicAdvisor)
            BuildFaAdvisorIdDDL(stuMasterInfo.FaAdvisor())
            BuildScheduleDDL(stuMasterInfo.ProgramVersionID)
            FillTransferFromProgramDDL(LeadId, stuEnrollID)
            BindStudentInfoExist(stuMasterInfo)
            txtStuEnrollId.Text = stuEnrollID

            ddlIsDisabled.SelectedValue = If(stuMasterInfo.IsDisabled Is Nothing, "-1", stuMasterInfo.IsDisabled.ToString())
            chkIsFirstTimeInSchool.Checked = stuMasterInfo.IsFirstTimeInSchool
            chkIsFirstTimePostSecSchool.Checked = stuMasterInfo.IsFirstTimePostSecSchool
            If stuMasterInfo.EntranceInterviewDate Is Nothing Then
                EntranceInterviewDate.Clear()
            Else
                EntranceInterviewDate.SelectedDate = stuMasterInfo.EntranceInterviewDate
            End If

            BuildBillingMethodDataSet()
            ddlDisableAutoCharge.SelectedValue = If(stuMasterInfo.DisableAutoCharge, "1", "0")
            ChargingMethodChanged(ddlBillingMethodId, EventArgs.Empty)

            ddlThirdPartyContract.SelectedValue = If(stuMasterInfo.ThirdPartyContract, "1", "0")

            If stuMasterInfo.DistanceEdStatus.Length > 0 Then
                Select Case stuMasterInfo.DistanceEdStatus.ToString
                    Case "Some"
                        rblDistanceEducation.SelectedValue = "Some"
                    Case "Only"
                        rblDistanceEducation.SelectedValue = "Only"
                    Case Else
                        rblDistanceEducation.SelectedValue = "None"
                End Select
            Else
                rblDistanceEducation.SelectedValue = "None"
            End If

            If stuMasterInfo.SAPId = "" Then
                ddlSAPId.SelectedIndex = 0
            Else
                ddlSAPId.SelectedValue = stuMasterInfo.SAPId
            End If

            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = 0
            dlstStdEnroll.SelectedIndex = selIndex

            EnableDisableTotalTransferHours(Nothing, stuMasterInfo.TracksTransfer)
            txtTransferHours.Text = CType(stuMasterInfo.TransferHours, String)
            BindEnrollmentDataList(txtStudentId.Text)
            BindLeadGroupList()
            ddlStatusCodeId.Enabled = False

            CommonWebUtilities.RestoreItemValues(dlstStdEnroll, strGUID)

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error selecting the first enrollment when page is first loaded: " & ex.Message & " "
            Else
                Session("Error") = "Error selecting the first enrollment when page is first loaded: " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

        ''Non Sa users should not be allowed to modify the Enrollments, which are out of school
        Dim fac As New TranscriptFacade
        If Not AdvantageSession.UserState.IsUserSA And fac.IsStudentCurrentlyInSchool(txtStuEnrollId.Text) = False Then
            btnSave.Enabled = False
            DisplayErrorMessage("Data cannot be saved for out of school enrollments")
        End If
    End Sub
    Private Sub BuildLeadGroupsDDL()
        Dim fac As New AdReqsFacade
        With chkLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = fac.GetAllLeadGroupsForEnrollment(campusId, txtStuEnrollId.Text) 'facade.GetAllLeadGroups(campusId, "All")
            .DataBind()
        End With
    End Sub
    Private Function UpdateLeadGroups(stuEnrollId As String) As Integer
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        '    Dim selectedDegrees() As String = selectedDegrees.CreateInstance(GetType(String), chkLeadGrpId.Items.Count)
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), chkLeadGrpId.Items.Count)
        Dim i = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkLeadGrpId.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        If i <= 0 Then
            Return -1
        End If

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim leadGrpFacade As New AdReqsFacade
        Dim lef As New LeadEnrollmentFacade

        Dim lId As String = lef.GetLeadIdForEnrollment(stuEnrollId)
        If leadGrpFacade.UpdateLeadGroupsAndStudentEnrollments(lId, AdvantageSession.UserState.UserName, selectedDegrees, txtStuEnrollId.Text) < 0 Then
            DisplayErrorMessage("A related record exists,you can not perform this operation")
        End If
        Return 0
    End Function

    Private Sub BindLeadGroupList()
        'Get Degrees data to bind the CheckBoxList
        Dim fac As New AdReqsFacade
        'Clear the current selection
        chkLeadGrpId.ClearSelection()
        Dim ds As DataSet = fac.GetLeadgroupsSelectedByStudentEnrollment(txtStuEnrollId.Text)
        Dim jobCount As Integer = fac.GetLeadGroupCountByStudentEnrollment(txtStuEnrollId.Text)

        If jobCount >= 1 Then
            'Select the items on the CheckBoxList
            Dim i As Integer
            If Not (ds.Tables(0).Rows.Count < 0) Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim row As DataRow
                    row = ds.Tables(0).Rows(i)
                    Dim item As ListItem
                    For Each item In chkLeadGrpId.Items
                        If item.Value.ToString = DirectCast(row("LeadGrpId"), Guid).ToString Then
                            item.Selected = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
    End Sub

    Private Function CheckLeadGroups() As Integer
        Dim strGroups As String = String.Empty
        ' Dim dsUseScheduled As DataSet
        'In For Loop Check The Number of Items Selected
        For Each item As ListItem In chkLeadGrpId.Items
            If item.Selected Then
                If strGroups = String.Empty Then
                    strGroups = item.Value.ToString
                Else
                    strGroups &= "," & item.Value.ToString
                End If

            End If
        Next

        If strGroups = String.Empty Then
            Return -1
        ElseIf strGroups.Split(CType(",", Char)).Length = 1 Then
            Return 1
        Else
            Return (New StudentEnrollmentFacade).GetSchedLeadGrpCnt_SP(strGroups)
        End If
    End Function

    Private Function OkToAddEnrollment() As Boolean

        'When we are creating a new enrollment we need to ensure that the student does not already
        'has an active enrollment for the same program version. It is ok if it is the same program
        'version if it has a system status of no start or dropped.
        'If the DataList does not contain any item then it means that this is the first enrollment
        'for the student so we don't have to waste time querying the database.
        Dim blnOk As Boolean
        Dim fac As New StuEnrollFacade

        'If there are no items in the DataList then it is okay.
        If dlstStdEnroll.Items.Count = 0 Then
            blnOk = True
        Else

            blnOk = fac.OKToAddEnrollmentWithSameProgramVersion(dlstStdEnroll.DataKeys(0).ToString(), ddlPrgVerId.SelectedItem.Value)
        End If

        Return blnOk
    End Function

    Private Sub Linkbutton5_Click(sender As Object, e As EventArgs) Handles Linkbutton5.Click
        Dim sb As New StringBuilder
        sb.Append("&StuEnrollId=" + txtStuEnrollId.Text)

        'Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name = "StdSuspensions"
        Dim csType As Type = Page.GetType
        Dim url As String = "../AR/" + name + ".aspx?resid=376&mod=AR&cmpid=" + campusId + sb.ToString
        Dim fullUrl As String = "window.open('" + url + "', '_blank', 'status=yes,resizable=yes,scrollbars=yes,width=900px,height=550px,fullscreen=no' );"

        ScriptManager.RegisterStartupScript(Page, csType, "OPEN_WINDOW", fullUrl, True)

    End Sub
    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(Linkbutton2)
        controlsToIgnore.Add(Linkbutton3)
        controlsToIgnore.Add(Linkbutton5)
        controlsToIgnore.Add(lnkbtnStatusChangeHistory)

        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub


    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList = New List(Of AdvantageDDLDefinition)()

        ddlList.Add(New AdvantageDDLDefinition(ddlDropReasonId, AdvantageDropDownListName.Drop_Reasons, campusId, True, True))

        ddlList.Add(New AdvantageDDLDefinition(ddlPrgVerId, AdvantageDropDownListName.ProgramVersions, campusId, True, True))

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlEmpId, AdvantageDropDownListName.Academic_Advisors, campusId, True, True, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlFAAdvisorId, AdvantageDropDownListName.Financial_Advisors, campusId, True, True, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlAdminRep, AdvantageDropDownListName.Admission_Reps, campusId, True, True))

        'Shifts DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlShiftId, AdvantageDropDownListName.Shifts, campusId, True, True, String.Empty))

        'Shifts DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusCodeId, AdvantageDropDownListName.Status_Codes, campusId, True, True))

        'Campuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCampusId, AdvantageDropDownListName.Campus, Nothing, True, True))

        'Billing Methods DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlBillingMethodId, AdvantageDropDownListName.Billing_Methods, campusId, True, True))

        'Attendance Types DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlAttendTypeId, AdvantageDropDownListName.AttendanceType, campusId, True, True, String.Empty))

        'Degree Certificate Seeking Types DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlDegCertSeekingId, AdvantageDropDownListName.DegCertSeekingType, campusId, True, True, String.Empty))

        'Tuition Cats DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlTuitionCategoryId, AdvantageDropDownListName.Tuition_Cats, campusId, True, True, String.Empty))

        'Education Level DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlEdLvlId, AdvantageDropDownListName.EducationLvl, campusId, True, True, String.Empty))


        ddlList.Add(New AdvantageDDLDefinition(ddlSAPId, AdvantageDropDownListName.SAPs, campusId, True, True))
        ''Added by Saraswathi Lakshmanan on Oct- 1 - 2008
        ''Option to Change the program version from one to another and then reschedule
        ''DropDown List with the Program versions which are active
        '' and is not the  current Program version.
        ddlList.Add(New AdvantageDDLDefinition(ddlnewPrgVersion, AdvantageDropDownListName.ProgramVersions, campusId, True, True, String.Empty))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        ''Remove the Current Program version details from the new Program Version DropDownList

    End Sub
    Private Sub BuildAdmissionRepsDDL()
        Dim leadAdmissionReps As New StuEnrollFacade
        'Dim DirectorAdmissionsCheck As New UserSecurityFacade
        'Dim boolDirCheck As Boolean = DirectorAdmissionsCheck.IsDirectorOfAdmissions(Session("UserId"))
        With ddlAdminRep
            .DataTextField = "fullname"
            .DataValueField = "userid"
            .DataSource = leadAdmissionReps.GetAllAdmissionRepsByCampusAndUserIdAndUserRoles(campusId, CType(Session("UserId"), String), 8, txtStuEnrollId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Public Sub getTransferToolTip()
        'Dim dgItems1 As DataListItemCollection
        'Dim dgList As New DataList
        Dim transferFacade As New TransferStudentsToAnotherProgramFacade
        Dim ds As DataSet

        'Dim StudentName As String = (New StuEnrollFacade).StudentName(StudentId)
        'dgList = CType(Master.FindControl("dlstStdEnroll"), DataList)
        'dgItems1 = dlstStdEnroll.Items
        'dgItems1 = dgList.Items
        Session("TrackingMessageControls") = ""
        ds = transferFacade.GetTransferResultDS(txtStuEnrollId.Text)
        If ds.Tables(0).Rows.Count >= 1 Then
            Dim dr As DataRow
            Session("TrackingMessageControls") = "Yes"
            For Each dr In ds.Tables(0).Rows
                lblTransferFromCampusValue.Text = dr("SourceCampus").ToString
                LblTransferToCampusValue.Text = dr("TargetCampus").ToString
                LblTransferFromProgramValue.Text = dr("SourcePrgVersion").ToString
                LblTransferToProgramValue.Text = dr("TargetPrgVersion").ToString
                lblTransferDateValue.Text = dr("ModDate").ToString.Substring(0, InStr(dr("ModDate").ToString, " ") - 1)
                txtTransferedBy.Text = dr("Users").ToString
                trackmessagecontrols.Visible = True
            Next
        Else
            Session("TrackingMessageControls") = ""
            trackmessagecontrols.Visible = False
        End If
        Session("TrackingMessage") = ""
        trackhistmess.Visible = False
    End Sub
    Protected Sub dlstStdEnroll_ItemCreated(sender As Object, e As DataListItemEventArgs) Handles dlstStdEnroll.ItemCreated
    End Sub
    Protected Sub dlstStdEnroll_vItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlstStdEnroll.ItemDataBound
        getTransferToolTip()
    End Sub
    Protected Sub bAddToOnLine_Click(sender As Object, e As EventArgs) Handles bAddToOnLine.Click
        Dim result As String = OnLineLib.create_user((New OnLineFacade).GetOnLineInfoByStuEnrollId(txtStuEnrollId.Text), AdvantageSession.UserState.UserName)
        If Not (result = String.Empty) Then
            DisplayErrorMessage(result)
        End If
    End Sub
    Private Sub btnReschedule_Click(sender As Object, e As EventArgs) Handles btnReschedule.Click
        Dim fac As New TranscriptFacade
        Dim msg As String
        Dim expGradDate As String
        Dim msg1 As String
        Dim studentEnrollment As New StuEnrollFacade
        Dim stuEnrollId As String
        Dim rescheduleReason As String
        rescheduleReason = txtReschReason.Text

        Dim oldStuEnrollId As String
        oldStuEnrollId = txtStuEnrollId.Text

        If Not ddlnewPrgVersion.SelectedItem.Text = "Select" Then

            'this code handles the shift id data issue in the arPrograms table
            Dim shiftId As String
            shiftId = studentEnrollment.GetShiftIdforProgramVersion(ddlnewPrgVersion.SelectedValue)
            If shiftId = String.Empty Then
                DisplayErrorMessage("The " & ddlnewPrgVersion.SelectedItem.ToString & " program does not have a proper shift assigned to it." & vbCrLf &
                                    "The student cannot be rescheduled in a program without a shift assignment.")
                Exit Sub
            End If

            TransferStudentsToNewProgramVersion()
            stuEnrollId = CType(ViewState("StuEnrollId"), String)

            studentEnrollment.UpdateCohortStartDateinStudentEnrollment(stuEnrollId, txtCohortStartDate.SelectedDate, AdvantageSession.UserState.UserName)

            GetStudentInfo(stuEnrollId)
            BindStudentInfoExist(stuMasterInfo)
            BindEnrollmentDataList(txtStudentId.Text)
            msg = fac.ReScheduleStudentWithCohortStartDate(stuEnrollId, CType(txtCohortStartDate.SelectedDate, String), AdvantageSession.UserState.UserName, ViewState("CampusId").ToString, oldStuEnrollId)

            If msg <> "" Then
                DisplayErrorMessage("An error occurred while trying to reschedule the student:" & msg)
            Else
                'We need to upate the grad date shown in the header control. Remember that this sub
                'gets executed after the header control has been loaded so it will still display the
                'previous expected grad date. 
                expGradDate = fac.GetExpGradDate(stuEnrollId)

                'Need to do the same thing for the Expected Grad date field on the form itself.
                txtExpGradDate.SelectedDate = CType(expGradDate, Date?)
                ''ReascheduleReason added to a new table
                ''Reschedule Reason Saved to the Old StudentEnrollid
                If Not rescheduleReason = "" Then
                    msg1 = fac.AddRescheduleReason(oldStuEnrollId, AdvantageSession.UserState.UserName, rescheduleReason)
                    If msg1 <> "" Then
                        DisplayErrorMessage("An error occurred while trying to save reschedule reason :" & msg)
                    End If
                End If

                'Display message that the process ran okay.
                DisplayErrorMessage("The rescheduling process was successful")


            End If

        Else
            'We need to unregister the student for any classes not yet graded and then register him/her
            'for all the classes of the new cohort start date that they have NOT already passed.
            'We want to wrap all of this in a transaction so that if anything fails everything gets rolled back.

            'Call save record so the record gets updated without the user having to manually click the
            'save button after changing the cohort start date.
            SaveRecord()

            msg = fac.ReScheduleStudentWithCohortStartDate(txtStuEnrollId.Text, CType(txtCohortStartDate.SelectedDate, String), AdvantageSession.UserState.UserName, ViewState("CampusId").ToString)

            If msg <> "" Then
                DisplayErrorMessage("An error occurred while trying to reschedule the student:" & msg)
            Else
                'We need to update the grad date shown in the header control. Remember that this sub
                'gets executed after the header control has been loaded so it will still display the
                'previous expected grad date. 
                expGradDate = fac.GetExpGradDate(txtStuEnrollId.Text)

                'Need to do the same thing for the Expected Grad date field on the form itself.
                txtExpGradDate.SelectedDate = CType(expGradDate, Date?)

                ''ReascheduleReason added to a new table
                If Not rescheduleReason = "" Then
                    msg1 = fac.AddRescheduleReason(txtStuEnrollId.Text, AdvantageSession.UserState.UserName, rescheduleReason)
                    If msg1 <> "" Then
                        DisplayErrorMessage("An error occurred while trying to save reschedule reason :" & msg)
                    End If
                End If

                'Display message that the process ran okay.
                DisplayErrorMessage("The rescheduling process was successful")
            End If

        End If

    End Sub
    Private Sub SaveRecord()
        Dim objCommon As New CommonUtilities
        Dim errMessage As String = String.Empty
        Dim facadeTerminate As New TerminateFacade
        Dim gradesDS As DataSet


        Dim j As Integer


        Dim programVersionDA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim programVersionUsesTimeClock = programVersionDA.DoesProgramVersionUseTimeClock(ddlPrgVerId.SelectedValue)

        Dim startDate = txtStartDate.SelectedDate
        If (startDate Is Nothing) Then
            DisplayErrorMessage("Start Date is a required field")
            Exit Sub
        End If

        Dim transferHoursValue = Convert.ToDecimal(If(txtTotalTransferHours.Text.Trim() = "", "0", txtTotalTransferHours.Text))
        Dim transferHoursFromThisSchoolValue = Convert.ToDecimal(If(txtTotalTransferHoursFromThisSchool.Text.Trim() = "", "0", txtTotalTransferHoursFromThisSchool.Text))

        If (transferHoursFromThisSchoolValue > transferHoursValue) Then
            DisplayErrorMessage("Total Transfer Hours must be greater than Total Transfer Hours from this School.")
            Exit Sub
        End If

        If (transferHoursFromThisSchoolValue > 0 AndAlso ddTransferHourFromProgram.SelectedValue = Guid.Empty.ToString()) Then
            DisplayErrorMessage("You must specify From Program if Transfer Hours from this School is greater than zero(0).")
            Exit Sub
        End If

        If OptionalLicensingExamDetails.Attributes.Item("style") = "" Then
            If (Not (CType(OptionalLicensingExamDetails.FindControl("LicensingExamDetailsValidator"), CustomValidator)).IsValid) Then
                DisplayErrorMessage("Please specify required values for Licensure Exam Details")
                Exit Sub
            End If

        End If

        j = CheckLeadGroups()
        If j = -1 Then
            DisplayErrorMessage("Please select a student group")
            Exit Sub
        ElseIf j >= 2 Then
            DisplayErrorMessage("Please select only one Student Group that is to be used for scheduling")
            Exit Sub
        End If

        Const maxlength = 9

        If programVersionUsesTimeClock Then
            Dim scheduleId = ddlSchedules.SelectedValue
            If (scheduleId Is "") Then
                DisplayErrorMessage("Time clock programs must have a schedule.")
                Exit Sub
            End If
            If txtbadgenumber.Text.Length > 0 AndAlso
               SchedulesFacade.IsBadgeNumberInUse(txtbadgenumber.Text, txtStuEnrollId.Text, campusId) Then
                DisplayErrorMessage("The entered badge number is currently used by another student.  Please choose another badge number.")
                Exit Sub
            End If
        End If

        If GetSettingString("MajorsMinorsConcentrations").ToUpper = "YES" Then
            If (ddlSuvaVersion.SelectedValue = String.Empty OrElse ddlSuvaVersion.SelectedValue = 0) Then
                DisplayErrorMessage("Program Version Type is required")
                Exit Sub
            End If
        End If

        dateError = False
        SetConGradDate()
        If dateError Then
            Exit Sub
        End If

        gradesDS = facadeTerminate.GetDropStatuses()
        If (IsDropStatus(gradesDS, ddlStatusCodeId.SelectedValue.ToString)) = True Then
            If txtDateDetermined.Text = "" Then
                DisplayErrorMessage("Date Determined is required")
                Exit Sub
            End If
        End If

        Try

            If ViewState("MODE") = "NEW" Then
                If OkToAddEnrollment() Then
                    Try
                        objCommon.PagePK = txtStuEnrollId.Text

                        Dim studentEnrollment As New StuEnrollFacade
                        Dim stuEnrollObj As StudentEnrollmentInfo

                        stuEnrollObj = BuildStuEnrollObj()
                        studentEnrollment.InsertFields(txtStuEnrollId.Text, stuEnrollObj, AdvantageSession.UserState.UserName)
                        UpdateLeadGroups(txtStuEnrollId.Text)
                        UpdateTransferHours(txtStuEnrollId.Text, transferHoursValue, transferHoursFromThisSchoolValue, ddTransferHourFromProgram.SelectedValue)
                        SaveScheduleForEnrollment(txtStuEnrollId.Text, ddlSchedules.SelectedValue, AdvantageSession.UserState.UserName, stuEnrollObj.ProgramVersionID)
                        studentEnrollment.updateLeadBadgeNumber(txtStuEnrollId.Text, stuEnrollObj.BadgeNumber)
                        GetStudentInfo(txtStuEnrollId.Text)
                        BindStudentInfoExist(stuMasterInfo)
                        BindEnrollmentDataList(txtStudentId.Text)
                        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                        ViewState("MODE") = "EDIT"

                    Catch ex As Exception
                        DisplayErrorMessage("Unable to save new enrollment. The following error have occurred: " + ex.Message)
                        Exit Sub
                    End Try
                Else
                    errMessage &= "You cannot create another enrollment with the same program version" & vbCr
                    errMessage &= "unless the enrollment has a system drop status or no show status."
                    DisplayErrorMessage(errMessage)
                    Exit Sub
                End If


            ElseIf ViewState("MODE") = "EDIT" Then
                Dim studentEnrollment As New StuEnrollFacade
                Dim stuEnrollObj As StudentEnrollmentInfo
                Dim lda As Date = (New ClsSectAttendanceFacade).GetLDA(txtStuEnrollId.Text)
                If Not (lda = Date.MinValue) Then
                    If txtLDA.SelectedDate IsNot Nothing Then
                        If Date.Parse(CType(txtLDA.SelectedDate, String)) < lda.Date Then
                            DisplayErrorMessage("Please select a LDA date greater than the last day of attendance")
                        End If
                    End If
                End If

                GetStudentInfo(txtStuEnrollId.Text)
                Dim oldExpGradDate = stuMasterInfo.ExpGradDate
                Dim oldStartDate = stuMasterInfo.StartDate
                Dim oldBadgeNumber = stuMasterInfo.BadgeNumber
                Dim oldTransferHoursValue = stuMasterInfo.TransferHours
                Dim oldTransferHoursFromThisSchoolValue = stuMasterInfo.TotalTransferHoursFromThisSchool
                Dim schedule = SchedulesFacade.GetActiveStudentScheduleInfo(txtStuEnrollId.Text)
                Dim scheduleChanged = False
                If schedule IsNot Nothing And ddlSchedules.SelectedValue IsNot "" And schedule.ScheduleId <> ddlSchedules.SelectedValue Then
                    scheduleChanged = True
                End If

                stuEnrollObj = BuildStuEnrollObj()
                studentEnrollment.UpdateFields(txtStuEnrollId.Text, stuEnrollObj, AdvantageSession.UserState.UserName)
                UpdateLeadGroups(txtStuEnrollId.Text)
                UpdateTransferHours(txtStuEnrollId.Text, transferHoursValue, transferHoursFromThisSchoolValue, ddTransferHourFromProgram.SelectedValue)
                SaveScheduleForEnrollment(txtStuEnrollId.Text, ddlSchedules.SelectedValue, AdvantageSession.UserState.UserName, stuEnrollObj.ProgramVersionID)
                GetStudentInfo(txtStuEnrollId.Text)
                BindStudentInfoExist(stuMasterInfo)
                BindEnrollmentDataList(txtStudentId.Text)

                If (oldExpGradDate <> stuEnrollObj.ExpGradDate OrElse oldStartDate <> stuEnrollObj.StartDate OrElse scheduleChanged OrElse oldBadgeNumber <> stuEnrollObj.BadgeNumber OrElse transferHoursValue <> oldTransferHoursValue OrElse transferHoursFromThisSchoolValue <> oldTransferHoursFromThisSchoolValue) Then

                    If oldBadgeNumber <> stuEnrollObj.BadgeNumber Then
                        studentEnrollment.updateLeadBadgeNumber(txtStuEnrollId.Text, stuEnrollObj.BadgeNumber)
                    End If

                    Dim gPrgVerId As New Guid(stuEnrollObj.ProgramVersionID.Trim)
                    Dim gstudentId As New Guid(stuEnrollObj.StudentID.Trim)

                    Dim token As TokenResponse

                    token = CType(Session("AdvantageApiToken"), TokenResponse)

                    If (token IsNot Nothing) Then
                        Dim ds = (New StuEnrollFacade).PopulateDataList(txtStudentId.Text, ShowStatusLevel, ViewState("CampusId").ToString)
                        If (Not ds Is Nothing) Then
                            If (ds.Tables.Count > 0) Then
                                If ds.Tables(0).Rows.Count > 0 Then
                                    For Each dr As Object In ds.Tables(0).Rows
                                        Dim afaEnr As AfaEnrollment
                                        Dim enrollmentRequest As New Fame.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(token.ApiUrl, token.Token)
                                        Dim enrollmentDetails = enrollmentRequest.GetAfaEnrollmentDetails(New Guid(dr("StuEnrollId").ToString()))

                                        If (enrollmentDetails IsNot Nothing) Then
                                            If txtStuEnrollId.Text = enrollmentDetails.StudentEnrollmentId.ToString() Then
                                                Dim schedHoursByWeek = (New AttendanceFacade).GetScheduledHoursByWeek(txtStuEnrollId.Text)
                                                afaEnr = New AfaEnrollment With {.StudentEnrollmentId = Guid.Parse(txtStuEnrollId.Text.Trim), .StudentId = Guid.Parse(txtStudentId.Text),
                                                        .ProgramVersionId = Guid.Parse(stuEnrollObj.ProgramVersionID.Trim), .ProgramName = ddlPrgVerId.SelectedItem.Text, .campusId = Guid.Parse(stuEnrollObj.CampusId),
                                                        .startDate = txtStartDate.SelectedDate, .ExpectedGradDate = txtExpGradDate.SelectedDate, .DroppedDate = enrollmentDetails.DroppedDate, .EnrollmentStatus = enrollmentDetails.EnrollmentStatus,
                                                        .AdmissionsCriteria = enrollmentDetails.AdmissionsCriteria, .AFAStudentId = enrollmentDetails.AfaStudentId, .CmsId = enrollmentDetails.CMSId, .LastDateAttendance = enrollmentDetails.LastDateAttendance,
                                                        .LeaveOfAbsenceDate = enrollmentDetails.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollmentDetails.ReturnFromLOADate, .StatusEffectiveDate = enrollmentDetails.StatusEffectiveDate,
                                                        .SSN = enrollmentDetails.SSN, .CreatedBy = enrollmentDetails.ModUser, .CreatedDate = enrollmentDetails.ModDate, .UpdatedBy = enrollmentDetails.ModUser, .UpdatedDate = enrollmentDetails.ModDate, .AdvStudentNumber = txtbadgenumber.Text, .HoursPerWeek = schedHoursByWeek}
                                            Else
                                                afaEnr = New AfaEnrollment With {.StudentEnrollmentId = enrollmentDetails.StudentEnrollmentId, .StudentId = Guid.Parse(txtStudentId.Text),
                                                    .ProgramVersionId = enrollmentDetails.ProgramVersionId, .ProgramName = enrollmentDetails.ProgramVersionName, .campusId = Guid.Parse(stuEnrollObj.CampusId),
                                                    .startDate = enrollmentDetails.StartDate, .ExpectedGradDate = enrollmentDetails.GradDate, .DroppedDate = enrollmentDetails.DroppedDate, .EnrollmentStatus = enrollmentDetails.EnrollmentStatus,
                                                    .AdmissionsCriteria = enrollmentDetails.AdmissionsCriteria, .AFAStudentId = enrollmentDetails.AfaStudentId, .CmsId = enrollmentDetails.CMSId, .LastDateAttendance = enrollmentDetails.LastDateAttendance,
                                                    .LeaveOfAbsenceDate = enrollmentDetails.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollmentDetails.ReturnFromLOADate, .StatusEffectiveDate = enrollmentDetails.StatusEffectiveDate,
                                                    .SSN = enrollmentDetails.SSN, .CreatedBy = enrollmentDetails.ModUser, .CreatedDate = enrollmentDetails.ModDate, .UpdatedBy = enrollmentDetails.ModUser, .UpdatedDate = enrollmentDetails.ModDate, .AdvStudentNumber = txtbadgenumber.Text, .HoursPerWeek = enrollmentDetails.HoursPerWeek, .InSchoolTransferHrs = enrollmentDetails.InSchoolTransferHrs, .PriorSchoolHrs = enrollmentDetails.PriorSchoolHrs - enrollmentDetails.InSchoolTransferHrs}
                                            End If
                                            SyncEnrollmentsWithAFA(afaEnr)
                                        End If

                                    Next
                                End If
                            End If
                        End If

                    End If

                End If
            End If

            ddlStatusCodeId.Enabled = False

            Dim sdfid As ArrayList
            Dim sdfidValue As ArrayList
            Dim z As Integer
            Try
                sdfControls.DeleteSDFValue(txtStuEnrollId.Text)
                sdfid = sdfControls.GetAllLabels(pnlSDF)
                sdfidValue = sdfControls.GetAllValues(pnlSDF)
                For z = 0 To sdfid.Count - 1
                    sdfControls.InsertValues(txtStuEnrollId.Text, Mid(CType(sdfid(z).id, String), 5), CType(sdfidValue(z), String))
                Next
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "savedAndReloadComboBoxScript", "savedAndReloadComboBox();", True)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub UpdateTransferHours(studentEnrollmentId As String, transferHoursValue As Decimal, transferHoursFromThisSchoolValue As Decimal, transferHoursFromProgramId As String)
        Dim DA = New StudentEnrollmentDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        DA.UpdateTransferHours(studentEnrollmentId, transferHoursValue, transferHoursFromThisSchoolValue, transferHoursFromProgramId)
    End Sub

    Private Sub SetRescheduleButtonState(stuEnrollId As String)
        'The reschedule button should only be enabled for students who are currently attending,
        'on academic probation or future start.
        Dim fac As New TranscriptFacade

        If fac.IsStudentCurrentlyInSchool(stuEnrollId) Then
            ''Reschedule reason label and textbox are added
            ''by Saraswathi Lakshmanan 09-30-2008
            txtReschReason.Enabled = True
            lblReschReason.Enabled = True
            btnReschedule.Enabled = True
            ddlnewPrgVersion.Enabled = True
            lblnewPrgVersion.Enabled = True
        Else
            txtReschReason.Enabled = False
            lblReschReason.Enabled = False
            btnReschedule.Enabled = False
            ddlnewPrgVersion.Enabled = False
            lblnewPrgVersion.Enabled = False
        End If


    End Sub
    Protected Sub lnkbtnReschReason_Click(sender As Object, e As EventArgs) Handles lnkbtnReschReason.Click
        Dim sb As New StringBuilder
        sb.Append("&StuEnrollId=" + txtStuEnrollId.Text)

        'Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name = "StudReschReason"
        Dim csType As Type = Page.GetType
        Dim url As String = "../AR/" + name + ".aspx?resid=345&mod=AR&cmpid=" + campusId + sb.ToString
        Dim fullURL As String = "window.open('" + url + "', '_blank', 'status=yes,resizable=yes,scrollbars=yes,width=900px,height=550px,fullscreen=no' );"

        ScriptManager.RegisterStartupScript(Page, csType, "OPEN_WINDOW", fullURL, True)

    End Sub

    ''NewProgram Version DDL is added
    Private Sub BuildNewPrgVersionDdl()
        ' Dim facade As New StuEnrollFacade
        Dim ds As DataSet
        ds = CType(ViewState("ProgVersions"), DataSet)

        With ddlnewPrgVersion
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = ds
            .DataBind()
            .Items.Remove(ddlPrgVerId.SelectedItem)
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub TransferStudentsToNewProgramVersion()
        Dim ds As DataSet
        Dim transferFacade As New TransferStudentsToAnotherProgramFacade
        Dim stuEnrollId As String = transferFacade.GetStudentEnrollmentByProgramVersion(txtStudentId.Text, ddlnewPrgVersion.SelectedValue)
        If stuEnrollId = "" Then
            stuEnrollId = Guid.NewGuid.ToString
        End If
        ViewState("StuEnrollId") = stuEnrollId

        Dim studentEnrollment As New StuEnrollFacade
        Dim shiftId As String
        shiftId = studentEnrollment.GetShiftIdforProgramVersion(ddlnewPrgVersion.SelectedValue)

        '  Dim strMessage As String
        Dim enrollId As New EnrollmentComponent
        Dim enrollFacade As New StuEnrollFacade
        Dim strAlertMessage = ""

        Dim fullName() As String = Split(enrollFacade.StudentName(txtStudentId.Text))
        Dim strFirstName1 As String = fullName(0)
        Dim strLastName1 As String = fullName(1)

        'To Generate EnrollmentID 
        Dim strEnrollmentId As String = enrollId.GenerateEnrollmentID(strLastName1, strFirstName1)
        ds = (New TransferStudentsToAnotherProgramFacade).GetCommonCoursesAndGrades(txtStudentId.Text, ddlPrgVerId.SelectedValue, ddlnewPrgVersion.SelectedValue)
        Dim enrollDateStr = CType(txtEnrollDate.SelectedDate, String)
        Dim expStartDateStr = CType(txtExpStartDate.SelectedDate, String)
        Dim expGradDateStr = CType(txtExpGradDate.SelectedDate, String)
        If ds.Tables(0).Rows.Count >= 1 Then

            transferFacade.InsertStudentEnrollment(stuEnrollId, txtStudentId.Text, campusId, ddlnewPrgVersion.SelectedValue, AdvantageSession.UserState.UserName, ddlPrgVerId.SelectedValue, enrollDateStr, expStartDateStr, expGradDateStr, campusId, shiftId, "")            ' , strEnrollmentId
            TransferGradesForAllPrograms(stuEnrollId, ds)
            Dim strGradeMessage As String = "Student Enrollment, Ledger and Attendance information has been successfully transferred to " & ddlnewPrgVersion.SelectedItem.Text
            DisplayErrorMessage(strGradeMessage)
        Else

            Dim strEnrollmentMessage As Object = transferFacade.InsertStudentEnrollment(stuEnrollId, txtStudentId.Text, campusId, ddlnewPrgVersion.SelectedValue, AdvantageSession.UserState.UserName, ddlPrgVerId.SelectedValue, enrollDateStr, expStartDateStr, expGradDateStr, strEnrollmentId, campusId, shiftId)
            If Not strEnrollmentMessage = "" Then
                strAlertMessage &= CType(strEnrollmentMessage, String)
            End If
            Dim strTransferGradeMessage As Object = transferFacade.TransferGrades(stuEnrollId, Nothing, Nothing, Nothing, AdvantageSession.UserState.UserName, Nothing, Nothing, Nothing, txtStudentId.Text, ddlPrgVerId.SelectedValue, campusId)      ' , ""
            If Not strAlertMessage = "" Then
                strAlertMessage &= ","
            End If
            Dim strGradeMessage = ""
            If Not strAlertMessage = "" Then
                strAlertMessage &= CType(strTransferGradeMessage, String)
            End If
            strGradeMessage = strAlertMessage & " information were successfully transferred to " & ddlnewPrgVersion.SelectedItem.Text
            DisplayErrorMessage(strGradeMessage)
        End If
    End Sub

    Protected Sub TransferGradesForAllPrograms(stuEnrollmentId As String, ds As DataSet)

        Dim strCourseId, strNewGradeId, strTermId, strResultId As String
        Dim intArrCount As Integer
        Dim intIsCourseCompleted As Integer
        Dim z As Integer
        Dim transferFacade As New TransferStudentsToAnotherProgramFacade

        If ds.Tables(0).Rows.Count >= 1 Then
            intArrCount = ds.Tables(0).Rows.Count
            Dim selectedCourse() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedGrade() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedTerm() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedResult() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedIsCourseCompleted() As Integer = Array.CreateInstance(GetType(Integer), intArrCount)

            For Each dr As DataRow In ds.Tables(0).Rows
                strCourseId = dr("ReqId").ToString
                If Not MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
                    strNewGradeId = dr("GrdSysDetailId").ToString
                Else
                    strNewGradeId = dr("Grade").ToString
                End If
                strTermId = dr("termId").ToString
                strResultId = dr("ResultId").ToString '
                intIsCourseCompleted = dr("IsCourseCompleted")

                selectedCourse.SetValue(strCourseId, z)
                selectedGrade.SetValue(strNewGradeId, z)
                selectedTerm.SetValue(strTermId, z)
                selectedResult.SetValue(strResultId, z)
                selectedIsCourseCompleted.SetValue(intIsCourseCompleted, z)
                z += 1
            Next

            'resize the array
            If z > 0 Then ReDim Preserve selectedCourse(z - 1)
            If z > 0 Then ReDim Preserve selectedGrade(z - 1)
            If z > 0 Then ReDim Preserve selectedTerm(z - 1)
            If z > 0 Then ReDim Preserve selectedResult(z - 1)
            transferFacade.TransferGrades(stuEnrollmentId, selectedCourse, selectedGrade, selectedTerm, AdvantageSession.UserState.UserName, selectedResult, selectedIsCourseCompleted, txtStudentId.Text, ddlPrgVerId.SelectedValue, campusId) '       , ""
        End If
    End Sub

    Protected Sub lnkbtnStatusChangeHistory_Click(sender As Object, e As EventArgs) Handles lnkbtnStatusChangeHistory.Click
        Dim sb As New StringBuilder
        '   append PrgVersion to the querystring
        '   append ProgVerId Description to the querystring
        sb.Append("&StuEnrollId=" + txtStuEnrollId.Text)

        '   setup the properties of the new window
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name = "StuStatusChangeHist"
        Dim csType As Type = Page.GetType

        Dim url As String = "../AR/" + name + ".aspx?resid=630&mod=AR&cmpid=" + campusId + sb.ToString
        Dim fullUrl = String.Format("window.open('{0}', '_blank','{1}');", url, winSettings)
        ScriptManager.RegisterStartupScript(Page, csType, "OPEN_WINDOW", fullUrl, True)
    End Sub

    Protected Sub SetConGradDate()

        Dim esd, ed, sd, mpd, rgd, cgd As Date

        esd = Date.MinValue
        ed = Date.MinValue
        sd = Date.MinValue
        rgd = Date.MinValue
        mpd = Date.MinValue

        cgd = Date.MinValue

        If Not txtExpStartDate.SelectedDate Is Nothing Then esd = CType(txtExpStartDate.SelectedDate, Date)

        If Not txtEnrollDate.SelectedDate Is Nothing Then ed = CType(txtEnrollDate.SelectedDate, Date)

        If Not txtStartDate.SelectedDate Is Nothing Then sd = CType(txtStartDate.SelectedDate, Date)

        If Not txtMidPtDate.SelectedDate Is Nothing Then mpd = CType(txtMidPtDate.SelectedDate, Date)

        If Not txtExpGradDate.SelectedDate Is Nothing Then rgd = CType(txtExpGradDate.SelectedDate, Date)

        If Not txtContractedGradDate.SelectedDate Is Nothing Then cgd = CType(txtContractedGradDate.SelectedDate, Date)

        'DE8603 10/10/2012 Janet Robinson

        If cgd <> Date.MinValue Then
            If esd >= cgd OrElse ed >= cgd OrElse sd >= cgd OrElse mpd >= cgd Then
                DisplayErrorMessage("The Revised Graduation Date and Contracted Graduation Date must be greater then the Enrollment, Expected Start, Start, and Mid-Point dates")
                dateError = True
                Exit Sub
            End If
        End If

        If rgd <> Date.MinValue Then
            If esd >= rgd OrElse ed >= rgd OrElse sd >= rgd OrElse mpd >= rgd Then
                DisplayErrorMessage("The Revised Graduation Date and Contracted Graduation Date must be greater then the Enrollment, Expected Start, Start, and Mid-Point dates")
                dateError = True
                Exit Sub
            End If
        End If
    End Sub

    ''' <summary>
    ''' Get a setting as String
    ''' </summary>
    ''' <param name="settingName">The name of the setting</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetSettingString(settingName As String) As String
        Dim advAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            advAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            advAppSettings = New AdvAppSettings
        End If
        Dim result As String = advAppSettings.AppSettings(settingName).ToString()
        Return result
    End Function

    Protected Sub OldMenuPane_Load(sender As Object, e As EventArgs) Handles OldMenuPane.Load

    End Sub





    Protected Sub ddlStatusCodeId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatusCodeId.SelectedIndexChanged
        'Check if Drop Was Selected and show the Drop reason and Date Determined Fiels
        'Otherwise hide them
        Try
            Dim fac As New TerminateFacade
            Dim gradesDS As DataSet
            gradesDS = fac.GetDropStatuses()
            With stuMasterInfo
                'Check if the selected grade exists in the drop grades
                If (IsDropStatus(gradesDS, ddlStatusCodeId.SelectedValue.ToString)) = True Then
                    lblDateDetermined.Visible = True
                    lblLDA.Visible = True
                    'txtDateDetermined.Visible = True

                    txtDateDetermined.Attributes.Remove("style")
                    txtDateDetermined.Attributes.Add("style", "display:inline")
                    txtDateDetermined.Enabled = True
                    'Mark the date determined label with red * if it does not contain one
                    If lblDateDetermined.Text.Contains("*") Then
                        'Do nothing
                    Else
                        lblDateDetermined.Text = lblDateDetermined.Text & "<font color=""red"">*</font>"
                    End If

                    txtLDA.Visible = True
                    If .DropReasonId <> Guid.Empty.ToString Or .DropReasonId <> "" Then
                        'ddlDropReasonId.SelectedValue = .DropReasonId
                        CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDropReasonId, .DropReasonId.ToString, .DropReason)
                        lblDropReasonId.Visible = True
                        'ddlDropReasonId.Visible = True
                        ddlDropReasonId.Attributes.Remove("style")
                        ddlDropReasonId.Attributes.Add("style", "display:inline")
                        'Mark the drop reason label with red * if it does not contain one
                        If lblDropReasonId.Text.Contains("*") Then
                            'Do nothing
                        Else
                            lblDropReasonId.Text = lblDropReasonId.Text & "<font color=""red"">*</font>"
                        End If
                        ddlDropReasonId.Enabled = True
                    End If
                Else
                    lblDateDetermined.Visible = False
                    lblLDA.Visible = False
                    'txtDateDetermined.Visible = False
                    txtDateDetermined.Attributes.Remove("style")
                    txtDateDetermined.Attributes.Add("style", "display:none")
                    txtDateDetermined.Enabled = False
                    txtLDA.Visible = False
                    lblDropReasonId.Visible = False
                    'ddlDropReasonId.Visible = False
                    ddlDropReasonId.Attributes.Remove("style")
                    ddlDropReasonId.Attributes.Add("style", "display:none")
                    ddlDropReasonId.Enabled = False
                End If
            End With
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub


    Private Sub SyncEnrollmentsWithAFA(enrollment As AfaEnrollment)
        Try
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            If (wapiSetting IsNot Nothing) Then

                Dim afaEnrollments = New LocationsHelper(connectionString, AdvantageSession.UserState.UserName).SyncEnrollmentsWithAFA(enrollment)
                If (afaEnrollments.Count > 0) Then

                    Dim enrReq = New Fame.AFA.Api.Library.AcademicRecords.EnrollmentRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    Dim result = enrReq.SendEnrollmentToAFA(afaEnrollments)
                End If

            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try




    End Sub

    Private Sub EnableDisableTransferHoursFromProgram(Optional ByVal disabled As Boolean = False)
        Dim txtBox As TextBox = txtTotalTransferHoursFromThisSchool

        Dim numValue = Convert.ToDecimal(If(txtBox.Text.Trim() = "", "0", txtBox.Text))

        ddTransferHourFromProgram.Enabled = numValue > 0 AndAlso disabled = False
    End Sub

    Private Sub EnableDisableTotalTransferHours(Optional ByVal isClockHour As Boolean? = Nothing, Optional ByVal tracksTransfer As Boolean = False)
        Dim IsClockHourProgram As Boolean
        IsClockHourProgram = If(isClockHour, facade.IsStudentProgramClockHourType(txtStuEnrollId.Text))
        rowTotalTransferHoursFromThisSchool.Visible = IsClockHourProgram
        rowTotalTransferHoursFromProgram.Visible = IsClockHourProgram
        rowTotalTransferHours.Visible = IsClockHourProgram
        txtTotalTransferHours.Enabled = IsClockHourProgram
        txtTotalTransferHoursFromThisSchool.Enabled = IsClockHourProgram
        ddTransferHourFromProgram.Enabled = IsClockHourProgram
        EnableDisableTransferHoursFromProgram(tracksTransfer)
    End Sub

    Protected Sub txtTotalTransferHoursFromThisSchool_OnTextChanged(sender As Object, e As EventArgs)

        Dim textBox As TextBox = sender
        Dim newVal = Convert.ToDecimal(If(textBox.Text.Trim() = "", "0", textBox.Text))
        Dim oldValue = 0

        If (ViewState("curTotalTransferHrsFromThisSchool") IsNot Nothing) Then
            oldValue = Convert.ToDecimal(ViewState("curTotalTransferHrsFromThisSchool").ToString())
        End If

        Dim totalTransferHoursValue = Convert.ToDecimal(If(txtTotalTransferHours.Text.Trim() = "", "0", txtTotalTransferHours.Text))

        'If total transfer hours is greater or equal than transfer hours from this school
        If (totalTransferHoursValue >= newVal) Then


            ViewState("curTotalTransferHrsFromThisSchool") = newVal

        Else  'Else reset value to old
            txtTotalTransferHoursFromThisSchool.Text = oldValue
            ViewState("curTotalTransferHrsFromThisSchool") = oldValue

            'Show Message on client side
            DisplayErrorMessage("Total Transfer Hours From this School cannot be greater than Total Transfer Hours")
        End If

        EnableDisableTransferHoursFromProgram()


    End Sub

    Protected Sub WrittenAllPartsChanged(sender As Object, e As EventArgs) Handles ddlWrittenAllParts.SelectedIndexChanged
        ToggleLicensingExamDetails()
    End Sub

    Private Sub ToggleLicensingExamDetails()
        Dim writtenAllParts = ddlWrittenAllParts.SelectedValue.Equals("1")
        If (writtenAllParts) Then
            OptionalLicensingExamDetails.Attributes.Remove("style") '.Visible = True
        Else
            OptionalLicensingExamDetails.Attributes.Add("style", "display:none") '.Visible = False
        End If
    End Sub

    Public Sub ValidateLicensingExamDetails(source As Object, args As ServerValidateEventArgs)
        Dim cv As CustomValidator = CType(source, CustomValidator)
        Dim parent = cv.Parent
        Dim message = String.Empty
        If (parent IsNot Nothing) Then
            For Each control As Control In parent.Controls
                If TypeOf control Is RadDatePicker Then
                    Dim dp = CType(control, RadDatePicker)
                    Dim selectedDate = dp.SelectedDate
                    Dim isValid = dp.CssClass.Contains("required") AndAlso selectedDate.HasValue
                    Dim dpInput = dp.Controls.Cast(Of RadDateInput).Where(Function(x) TypeOf (x) Is RadDateInput).FirstOrDefault()
                    If (Not isValid) Then

                        If (Not dpInput Is Nothing) Then
                            dpInput.BackColor = Color.Yellow
                        End If
                        cv.IsValid = isValid
                    Else
                        dpInput.BackColor = Color.White
                    End If


                End If
                'Return
            Next
        End If

        args.IsValid = cv.IsValid
    End Sub

    Protected Sub txtTotalTransferHours_OnTextChanged(sender As Object, e As EventArgs)
        Dim textBox As TextBox = sender
        Dim newVal = Convert.ToDecimal(If(textBox.Text.Trim() = "", "0", textBox.Text))
        Dim oldValue = 0

        If (ViewState("curTotalTransferHrs") IsNot Nothing) Then
            oldValue = Convert.ToDecimal(ViewState("curTotalTransferHrs").ToString())
        End If

        Dim totalTransferHoursFromThisSchoolValue = Convert.ToDecimal(If(txtTotalTransferHoursFromThisSchool.Text.Trim() = "", "0", txtTotalTransferHoursFromThisSchool.Text))

        'if new value of total transfer hours is still greater or same as total hours from this program
        If (newVal >= totalTransferHoursFromThisSchoolValue) Then
            ViewState("curTotalTransferHrs") = newVal
        Else 'reset to old value
            ViewState("curTotalTransferHrs") = oldValue

            'Show Message on client side
            DisplayErrorMessage("Total Transfer Hours cannot be less than Total Transfer Hours from this School")
        End If






    End Sub

End Class

