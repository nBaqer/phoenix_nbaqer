Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class Buildings
    Inherits BasePage
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents ZipFormat As System.Web.UI.WebControls.Label
    Protected ResourceId As String
    Protected ModuleId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Private validatetxt As Boolean = False

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        txtAddress1.Attributes.Add("onChange", "DoTitleCase('txtAddress1');")
        txtAddress2.Attributes.Add("onChange", "DoTitleCase('txtAddress2');")
        txtCity.Attributes.Add("onChange", "DoTitleCase('txtCity');")
        txtBldgName.Attributes.Add("onChange", "DoTitleCase('txtBldgName');")
        txtBldgTitle.Attributes.Add("onChange", "DoTitleCase('txtBldgTitle');")
        'txtBldgEmail.Attributes.Add("onChange", "DoTitleCase('txtBldgEmail');")
        'Linkbutton2.Attributes.Add("onclick", "OpenRooms();return false;")
        Dim objCommon As New CommonUtilities
        '        Dim sStatusId As String
        '    Dim m_Context As HttpContext

        Dim sdfcontrols As New SDFComponent

        Dim advantageUserState As New BO.User()
        Dim mContext As HttpContext
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        'ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString

        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        'Check if this page still exists in the menu while switching campus
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                '   build dropdownlists
                BuildDropDownLists()

                '   bind datalist
                BindDataList()

                '   bind an empty new AcademicYearInfo
                BindBuildingData(New BuildingInfo)

                '   initialize buttons
                InitButtonsForLoad()
                Linkbutton2.Enabled = False
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                InitButtonsForEdit()
            End If

            If chkforeignzip.Checked = True Then
                chkforeignzip.Checked = True
                txtotherstate.Visible = True
                txtotherstate.Enabled = True
                lblotherstate.Visible = True
                ddlstateid.Enabled = False
            Else
                chkforeignzip.Checked = False
                txtotherstate.Visible = False
                txtotherstate.Visible = False
                lblotherstate.Visible = False
                ddlstateid.Enabled = True
            End If



            'Check If any UDF exists for this resource
            Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(ResourceId, ModuleId)
            If intSDFExists >= 1 Then
                pnludfheader.Visible = True
            Else
                pnludfheader.Visible = False
            End If

            If Trim(txtbldgcode.Text) <> "" Then
                sdfcontrols.GenerateControlsEditSingleCell(pnlsdf, ResourceId, txtbldgid.Text, ModuleId)
            Else
                sdfcontrols.GenerateControlsNewSingleCell(pnlsdf, ResourceId, ModuleId)
            End If

            GetInputMaskValue()

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnNew.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind AcademicYears datalist
        dlstBldgs.DataSource = New DataView((New BuildingFacade).GetAllBuildings().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstBldgs.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        'BuildStatusDDL()
        'BuildCampGrpDDL()
        'BuildStatesDDL()

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, True, True))

        'Campus Groups DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCampGrpId, AdvantageDropDownListName.CampGrps, Nothing, True, True))

        'Campus Groups DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCampusId, AdvantageDropDownListName.Campuses, Nothing, True, True))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStateId, AdvantageDropDownListName.States, Nothing, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub

    Private Function GetInputMaskValue() As String

        GetInputMaskValue = ""

        If chkforeignphone.Checked = False Then
            txtphone.Mask = "(###)-###-####"
            txtphone.DisplayMask = "(###)-###-####"
            lblphone.ToolTip = "(###)-###-####"
            txtphone.DisplayPromptChar = ""

        Else
            txtphone.Text = ""
            txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtphone.DisplayMask = ""
            lblphone.ToolTip = ""
            txtphone.DisplayPromptChar = ""
        End If

        If chkforeignfax.Checked = False Then
            txtfax.Mask = "(###)-###-####"
            txtfax.DisplayMask = "(###)-###-####"
            lblfax.ToolTip = "(###)-###-####"
            txtfax.ToolTip = "(###)-###-####"
            txtfax.DisplayPromptChar = ""
        Else
            txtfax.Text = ""
            txtfax.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtfax.DisplayMask = ""
            lblfax.ToolTip = ""
            txtfax.DisplayPromptChar = ""
        End If

        If chkforeignzip.Checked = False Then
            txtzip.Mask = "#####"
            txtzip.DisplayMask = "#####"
            lblzip.ToolTip = "#####"
        Else
            txtzip.Mask = "aaaaaaaaaa"
            txtzip.DisplayMask = ""
            lblzip.ToolTip = ""
        End If

        lblbldgopen.ToolTip = "hh:MM:ss AM/PM"
        lblbldgclose.ToolTip = "hh:MM:ss AM/PM"

    End Function

    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlstatusid
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildCampGrpDDL()
        'Bind the Status DropDownList
        Dim statuses As New CampusGroupsFacade
        With ddlcampgrpid
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = statuses.GetAllCampusGroups()
            .DataBind()
        End With
    End Sub
    Private Sub BuildStatesDDL()
        'Bind the Status DropDownList
        Dim states As New StatesFacade
        With ddlstateid
            .DataTextField = "StateDescrip"
            .DataValueField = "StateID"
            .DataSource = states.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Function ValidateFieldsWithInputMasks() As String
        'Dim facInputMasks As New InputMasksFacade
        'Dim correctFormat As Boolean
        ''Dim strMask As String
        'Dim zipMask As String
        Dim errorMessage As String = ""

        'zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        ' ''Validate the zip field format. If the field is empty we should not apply the mask
        ' ''against it.
        'If txtzip.Text <> "" And chkforeignzip.Checked = False Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtzip.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for zip field."
        '    End If
        'End If

        Return errorMessage

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        '        Dim msg As String
        Dim errorMessage As String
        'Bind The EmployerInfo Data From The Database
        Dim facInputMask As New InputMasksFacade
        'Dim phoneMask As String
        'Dim zipMask As String
        Dim Result As String
        Dim facade As New BuildingFacade

        'Get the mask for phone numbers and zip
        'phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        'zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        errorMessage = ValidateFieldsWithInputMasks()

        Dim selectedCampusId = ddlcampusid.SelectedItem.Value
        If selectedCampusId = Guid.Empty.ToString() Or selectedCampusId = Nothing Then
            CommonWebUtilities.DisplayWarningInMessageBox(Me.Page, "Please Select a Campus")
            ' DisplayErrorMessage("Please Select a Campus")
            Return
        End If

        If errorMessage = "" Then
            If validatetxt = True Then
                Exit Sub
            End If

            'Call Update Function in the plEmployerInfoFacade
            Result = facade.UpdateBuildingInfo(BuildBuildingInfo(txtbldgid.Text), AdvantageSession.UserState.UserName)

            '   bind the datalist
            BindDataList()

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstBldgs, txtBldgId.Text, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstbldgs, txtbldgid.Text)
            If Not Result = "" Then
                '   Display Error Message
                DisplayErrorMessage(Result)
            Else
                '   get the AcademicYearId from the backend and display it
                GetBuildingInfo(txtbldgid.Text)
                'Results.Visible = True
                chkisindb.Checked = True
                'btnNew.Enabled = False
                'btnDelete.Enabled = True
            End If
            ''   if there are no errors bind a new entity and init buttons
            'If Page.IsValid Then

            '    '   set the property IsInDB to true in order to avoid an error if the user
            '    '   hits "save" twice after adding a record.
            '    chkIsInDB.Checked = True

            '    'note: in order to display a new page after "save".. uncomment next lines
            '    '   bind an empty new AcademicYearInfo
            '    'BindAcademicYearData(New AcademicYearInfo)

            '    '   initialize buttons
            '    'InitButtonsForLoad()
            '    InitButtonsForEdit()

            'End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtbldgid.Text)
                SDFID = SDFControl.GetAllLabels(pnlsdf)
                SDFIDValue = SDFControl.GetAllValues(pnlsdf)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtbldgid.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Else
            DisplayErrorMessageMask(errorMessage)
        End If

        'If txtzip.Text <> "" And chkforeignzip.Checked = False Then
        '    txtzip.Text = facInputMask.RemoveMask(zipMask, txtzip.Text)
        'End If

    End Sub

    Private Function BuildBuildingInfo(ByVal BldgId As String) As BuildingInfo
        'instantiate class
        Dim Building As New BuildingInfo
        Dim facInputMask As New InputMasksFacade
        'Dim zipMask As String
        '        Dim ssnMask As String

        With Building
            'get IsInDB
            .IsInDB = chkisindb.Checked

            'get AcademicYearId
            .BldgId = BldgId

            'Get Address1
            .Address1 = txtaddress1.Text

            'Get Address2
            .Address2 = txtaddress2.Text

            'Get City
            .City = txtcity.Text

            'Get StateId
            .StateId = ddlstateid.SelectedValue
            .State = ddlstateid.SelectedItem.Text

            .NoOfRooms = txtbldgrooms.Text

            .Email = txtbldgemail.Text

            .Name = txtbldgname.Text

            .Comments = txtbldgcomments.Text

            .Title = txtbldgtitle.Text

            .Code = txtbldgcode.Text

            .Descrip = txtbldgdescrip.Text

            .OtherState = txtotherstate.Text

            .BldgOpen = txtbldgopen.Text

            .BldgClose = txtbldgclose.Text

            .Zip = txtzip.Text

            If chksat.Checked = True Then
                .Sat = 1
            Else
                .Sat = 0
            End If

            If chksun.Checked = True Then
                .Sun = 1
            Else
                .Sun = 0
            End If

            If chkmon.Checked = True Then
                .Mon = 1
            Else
                .Mon = 0
            End If

            If chktue.Checked = True Then
                .Tue = 1
            Else
                .Tue = 0
            End If

            If chkwed.Checked = True Then
                .Wed = 1
            Else
                .Wed = 0
            End If
            If chkthu.Checked = True Then
                .Thur = 1
            Else
                .Thur = 0
            End If
            If chkfri.Checked = True Then
                .Fri = 1
            Else
                .Fri = 0
            End If

            'Get Country 
            .CampGrpId = ddlcampgrpid.SelectedValue

            .CampusId = ddlcampusid.SelectedValue

            'Get AddressType
            .StateId = ddlstateid.SelectedValue
            .State = ddlstateid.SelectedItem.Text

            'Address Status
            .StatusId = ddlstatusid.SelectedValue

            'Foreign Zip
            If chkforeignzip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            'Foreign Zip
            If chkforeignphone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If
            'Foreign Zip
            If chkforeignfax.Checked = True Then
                .ForeignFax = 1
            Else
                .ForeignFax = 0
            End If

            'Get Phone
            .Phone = txtphone.Text

            'Get zip
            .Fax = txtfax.Text

            'get ModUser
            .ModUser = txtmoduser.Text

            'get ModDate
            .ModDate = Date.Parse(txtmoddate.Text)

        End With
        'return data
        Return Building
    End Function
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client

        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        '   bind an empty new AcademicYearInfo
        BindBuildingData(New BuildingInfo)

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstBldgs, Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstbldgs, Guid.Empty.ToString)
        'initialize buttons
        InitButtonsForLoad()

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlsdf, ResourceId, ModuleId)



    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If Not (txtbldgid.Text = Guid.Empty.ToString) Then
            'update AcademicYear Info 
            Dim result As String = (New BuildingFacade).DeleteBuilding(txtbldgid.Text, Date.Parse(txtmoddate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new AcademicYearInfo
                BindBuildingData(New BuildingInfo)

                '   initialize buttons
                InitButtonsForLoad()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtbldgid.Text)
                SDFControl.GenerateControlsNewSingleCell(pnlsdf, ResourceId, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End If

        End If
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next

            'For Each ctl In pnlDays.Controls
            '    If ctl.GetType Is GetType(TextBox) Then
            '        CType(ctl, TextBox).Text = ""
            '    End If
            '    If ctl.GetType Is GetType(CheckBox) Then
            '        CType(ctl, CheckBox).Checked = False
            '    End If
            '    If ctl.GetType Is GetType(DropDownList) Then
            '        CType(ctl, DropDownList).SelectedIndex = 0
            '    End If
            'Next

            'For Each ctl In pnlContactInfo.Controls
            '    If ctl.GetType Is GetType(TextBox) Then
            '        CType(ctl, TextBox).Text = ""
            '    End If
            '    If ctl.GetType Is GetType(CheckBox) Then
            '        CType(ctl, CheckBox).Checked = False
            '    End If
            '    If ctl.GetType Is GetType(DropDownList) Then
            '        CType(ctl, DropDownList).SelectedIndex = 0
            '    End If
            'Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub dlstBldgs_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstbldgs.ItemCommand
        '   get the AcademicYearId from the backend and display it
        GetBuildingInfo(e.CommandArgument)
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()
        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstBldgs, e.CommandArgument, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstbldgs, e.CommandArgument)
        '   initialize buttons
        InitButtonsForEdit()

        linkbutton2.Enabled = True

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEditSingleCell(pnlsdf, ResourceId, txtbldgid.Text, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub
    Private Sub GetBuildingInfo(ByVal BldgId As String)

        '   bind AcademicYear properties
        BindBuildingData((New BuildingFacade).GetBldgInfo(BldgId))

    End Sub

    'Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged
    '    Dim objListGen As New DataListGenerator
    '    Dim ds As New DataSet
    '    Dim ds2 As New DataSet
    '    Dim sw As New System.IO.StringWriter
    '    Dim objCommon As New CommonUtilities
    '    Dim dv2 As New DataView
    '    '        Dim sStatusId As String
    '    Try
    '        ClearRHS()
    '        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
    '        ViewState("MODE") = "NEW"
    '        'Set the text box to a new guid
    '        txtbldgid.Text = System.Guid.NewGuid.ToString
    '        ds = objListGen.SummaryListGenerator()
    '        PopulateDataList(ds)
    '        linkbutton2.Enabled = False
    '    Catch ex As System.Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try
    'End Sub

    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtotherstate.TextChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtzip)
    End Sub
    'Private Sub PopulateDataList(ByVal ds As DataSet)
    '    Dim ds2 As New DataSet
    '    Dim dv2 As New DataView
    '    Dim objListGen As New DataListGenerator
    '    Dim sStatusId As String
    '    Dim facInputMasks As New InputMasksFacade
    '    'Dim strMask As String
    '    'Dim zipMask As String
    '    'Dim dr As DataRow

    '    ds2 = objListGen.StatusIdGenerator()
    '    'Set up the primary key on the datatable
    '    ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}


    '    If (radstatus.SelectedItem.Text = "Active") Then   'Show Active Only
    '        Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
    '        sStatusId = row("StatusId").ToString()
    '        'Create dataview which displays active records only
    '        Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "BldgDescrip", DataViewRowState.CurrentRows)
    '        dv2 = dv
    '    ElseIf (radstatus.SelectedItem.Text = "Inactive") Then   'Show InActive Only
    '        Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
    '        sStatusId = row("StatusId").ToString()
    '        'Create dataview which displays inactive records only
    '        Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "BldgDescrip", DataViewRowState.CurrentRows)
    '        dv2 = dv
    '    Else  'Show All
    '        ds = objListGen.SummaryListGeneratorSort()
    '        Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
    '        sStatusId = row("StatusId").ToString()
    '        'Create dataview which displays inactive records only
    '        Dim dv As New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows)
    '        dv2 = dv
    '    End If



    '    With dlstbldgs
    '        .DataSource = dv2
    '        .DataBind()
    '    End With
    '    'dlstBldgs.SelectedIndex = -1


    'End Sub
    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        'Try
        '    ClearRHS()
        '    objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
        '    Viewstate("MODE") = "NEW"
        '    txtBldgId.Text = Guid.NewGuid.ToString
        '    ds = objListGen.SummaryListGenerator()
        '    PopulateDataList(ds)
        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    'Redirect to error page.
        '    If ex.InnerException Is Nothing Then
        '        Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " "
        '    Else
        '        Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " " & ex.InnerException.Message
        '    End If
        '    Response.Redirect("../ErrorPage.aspx")
        'End Try

        BindBuildingData(New BuildingInfo)
        BindDataList()
        'Header1.EnableHistoryButton(False)
        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlsdf, ResourceId, ModuleId)

        CommonWebUtilities.RestoreItemValues(dlstbldgs, Guid.Empty.ToString)

        'initialize buttons
        InitButtonsForLoad()


    End Sub
    Public Sub TextValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        Dim strMinus As String = Mid(args.Value, 1, 1)
        If strMinus = "-" Then
            DisplayErrorMessage("The Value Cannot be Negative")
            validatetxt = True
        End If
    End Sub

    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtcity.TextChanged
        Dim objCommon As New CommonWebUtilities
        If chkforeignzip.Checked = True Then
            CommonWebUtilities.SetFocus(Me.Page, txtotherstate)
        End If
    End Sub

    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkforeignphone.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtphone)
        GetInputMaskValue()
    End Sub
    Private Sub chkForeignFax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkforeignfax.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtfax)
        GetInputMaskValue()
    End Sub
    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkforeignzip.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtaddress1)
        If chkforeignzip.Checked = True Then
            ddlstateid.Enabled = False
            ddlstateid.SelectedIndex = 0
            txtotherstate.Visible = True
            lblotherstate.Visible = True
            txtzip.Mask = "aaaaaaaaaa"
            txtzip.DisplayMask = ""
            lblzip.ToolTip = ""
        Else
            ddlstateid.Enabled = True
            txtotherstate.Text = ""
            txtotherstate.Visible = False
            lblotherstate.Visible = False
            txtzip.Mask = "#####"
            txtzip.DisplayMask = "#####"
            lblzip.ToolTip = "#####"
        End If
    End Sub
    Private Sub BindBuildingData(ByVal BldgInfo As BuildingInfo)
        Dim facInputMasks As New InputMasksFacade
        'Dim zipMask As String
        'Dim strMask As String

        ''Get the mask for phone numbers and zip
        'zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        'strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        With BldgInfo
            If .ForeignZip = True Then
                chkforeignzip.Checked = True
                txtzip.Mask = "aaaaaaaaaa"
                txtzip.DisplayMask = "aaaaaaaaaa"
            Else
                chkforeignzip.Checked = False
                txtzip.Mask = "#####"
                txtzip.DisplayMask = "#####"
            End If

            If .ForeignPhone = True Then
                chkforeignphone.Checked = True
                txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtphone.DisplayMask = ""
                lblphone.ToolTip = ""
            Else
                chkforeignphone.Checked = False
                txtphone.Mask = "(###)-###-####"
                txtphone.DisplayMask = "(###)-###-####"
                lblphone.ToolTip = "(###)-###-####"
            End If

            If .ForeignFax = True Then
                chkforeignfax.Checked = True
                txtfax.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtfax.DisplayMask = ""
                lblfax.ToolTip = ""
            Else
                chkforeignfax.Checked = False
                txtfax.Mask = "(###)-###-####"
                txtfax.DisplayMask = "(###)-###-####"
                lblfax.ToolTip = "(###)-###-####"
            End If

            If .Sun = True Then
                chksun.Checked = True
            Else
                chksun.Checked = False
            End If

            If .Mon = True Then
                chkmon.Checked = True
            Else
                chkmon.Checked = False
            End If
            If .Tue = True Then
                chktue.Checked = True
            Else
                chktue.Checked = False
            End If

            If .Wed = True Then
                chkwed.Checked = True
            Else
                chkwed.Checked = False
            End If
            If .Thur = True Then
                chkthu.Checked = True
            Else
                chkthu.Checked = False
            End If
            If .Fri = True Then
                chkfri.Checked = True
            Else
                chkfri.Checked = False
            End If
            If .Sat = True Then
                chksat.Checked = True
            Else
                chksat.Checked = False
            End If

            'get IsInDB
            chkisindb.Checked = .IsInDB
            'Get Address1
            txtaddress1.Text = .Address1

            'Get Address2
            txtaddress2.Text = .Address2

            txtcity.Text = .City

            txtbldgcode.Text = .Code

            txtbldgdescrip.Text = .Descrip

            txtbldgrooms.Text = .NoOfRooms

            txtbldgopen.Text = .BldgOpen

            txtbldgclose.Text = .BldgClose

            txtbldgname.Text = .Name

            txtbldgtitle.Text = .Title

            txtbldgemail.Text = .Email

            txtbldgcomments.Text = .Comments

            txtbldgid.Text = .BldgId

            'Get State
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlstateid, .StateId, .State)

            'Try
            '    ddlStateId.SelectedValue = .State
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    BuildStatesDDL()
            'End Try

            'Get Zip
            txtzip.Text = .Zip

            'Get Phone
            txtphone.Text = .Phone

            'Get Fax
            txtfax.Text = .Fax

            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlcampgrpid, .CampGrpId, .CampGrp)

            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlcampusid, .CampusId, .CampusId)

            'Try
            '    ddlCampGrpId.SelectedValue = .CampGrpId
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    BuildCampGrpDDL()
            'End Try

            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlstatusid, .StatusId, "Invalid Status")

            'Try
            '    ddlStatusId.SelectedValue = .StatusId
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    BuildStatusDDL()
            'End Try

            txtotherstate.Text = .OtherState

            'Get Foreign Zip
            If .ForeignZip = True Then
                chkforeignzip.Checked = True
                txtotherstate.Visible = True
                txtotherstate.Enabled = True
                lblotherstate.Visible = True
                ddlstateid.Enabled = False
                lblzip.ToolTip = ""
                ddlstateid.CausesValidation = False
            Else
                chkforeignzip.Checked = False
                txtotherstate.Visible = False
                txtotherstate.Visible = False
                lblotherstate.Visible = False
                ddlstateid.Enabled = True
                lblzip.ToolTip = "#####"
                ddlstateid.CausesValidation = True
            End If

            'If .Default1 = True Then
            '    chkdefault1.Checked = True
            'Else
            '    chkdefault1.Checked = False
            'End If
            txtmoduser.Text = .ModUser
            txtmoddate.Text = .ModDate.ToString

        End With
    End Sub

    Private Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkbutton2.Click
        Dim sb As New StringBuilder
        '   append PrgVerId to the querystring
        sb.Append("&BldgId=" + txtbldgid.Text)
        '   append Course Description to the querystring
        sb.Append("&BldgName=" + txtbldgdescrip.Text)
        '   append Bldg Rooms to the querystring
        sb.Append("&BldgRooms=" + txtbldgrooms.Text)


        '   setup the properties of the new window
        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "Rooms"
        Dim url As String = "../AR/" + name + ".aspx?resid=284&mod=AR&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        'theChild = window.open('../AR/Rooms.aspx?resid=284&BldgName=' + BuildingName + '&BldgId=' + BuildingId + '&CampusId=' + CampId,'Rooms','Width=850,height=500,ScreenX=0,ScreenY=0,toolbar=yes');

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(linkbutton2)
        controlsToIgnore.Add(chkforeignfax)
        controlsToIgnore.Add(chkforeignphone)
        controlsToIgnore.Add(chkforeignzip)

        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub


End Class
