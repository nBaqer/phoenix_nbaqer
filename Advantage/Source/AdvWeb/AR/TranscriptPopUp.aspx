<%@ Reference Page="~/AR/Transcript.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="TranscriptPopUp" CodeFile="TranscriptPopUp.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Transcript</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />  
    <link rel="stylesheet" type="text/css" href="../css/systememail.css" />  
   
</head>
<body leftmargin="0" topmargin="0" runat="server" id="Body1">
    <form id="Form1" method="post" runat="server">    
    <asp:Panel id="pnlTop" runat="server" ScrollBars="Auto" Width="90%" Height="700px">
   <table cellspacing="0" cellpadding="0" width="90%" border="0" class="maincontenttable">    
        <tr>
            <td class="detailsframe">
                <div class="scrollpopupresume">
                    <table cellspacing="0" cellpadding="0" width="90%" border="0">
                        <tr>
                            <td class="arcontentcells" nowrap="nowrap">
                                <asp:Label ID="lblStd" runat="server" CssClass="label">Student</asp:Label>
                            </td>
                            <td class="arcontentcells2" nowrap="nowrap" style="padding-right: 20px">
                                <asp:Label ID="lblStdName" runat="server" CssClass="textbox"></asp:Label>
                            </td>
                            <td class="arcontentcells" nowrap="nowrap">
                                <asp:Label ID="lblStudentID" runat="server" CssClass="label">Student ID</asp:Label>
                            </td>
                            <td class="arcontentcells2" nowrap="nowrap" style="padding-right: 20px">
                                <asp:Label ID="lblStdID" runat="server" CssClass="textbox"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <!-- begin content table -->
                    <table cellspacing="0" cellpadding="0" width="90%" border="0">
                        <tr>
                            <td style="padding-top: 10px">
                                <!-- Transcript table -->
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td>
                                            <asp:DataGrid ID="dgrdTransactionSearch" runat="server" Width="100%" EditItemStyle-Wrap="false" 
                                                HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid"
                                                ShowFooter="False" CellPadding="3" BorderWidth="1px" BorderColor="#E0E0E0">
                                                <EditItemStyle CssClass="label"></EditItemStyle>
                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <FooterStyle CssClass="label"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Term Start Date">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStartDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Code">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Course">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label1" CssClass="label" Text='<%# Container.DataItem("Descrip") %>'
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Grade">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label3" CssClass="label" Text='<%# Container.DataItem("Grade") %>'
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Grade">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label4" CssClass="label" Text='<%# Container.DataItem("NumGrade") %>'
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 16px">
                                <asp:Panel ID="pnlSummary" runat="server" Visible="False">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="left"
                                        border="0">
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblNumber" runat="server" CssClass="label">Number Of Classes</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblTotalClasses" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label4" runat="server" CssClass="label">Credits Attempted</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label5" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label6" runat="server" CssClass="label">Credits Earned</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label7" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label8" runat="server" CssClass="label">GPA</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label9" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label10" runat="server" CssClass="label">Completed SAP Requirements ?</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label12" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <!-- Scheduled Courses table -->
                        <tr>
                            <td style="padding-top: 16px">
                                <asp:Panel ID="pnlScheduledCourses" runat="server" Visible="false">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="contentcellheader">
                                                <asp:Label ID="lblSchedCourses" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 10px">
                                                <asp:DataGrid ID="dgrdScheduledCourses" CellPadding="3" ShowFooter="False" BorderStyle="Solid"
                                                    AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false"
                                                    Width="100%" runat="server" BorderWidth="1px" BorderColor="#E0E0E0">
                                                    <EditItemStyle CssClass="label"></EditItemStyle>
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                    <FooterStyle CssClass="label"></FooterStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Code">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSchedCCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Course">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSchedCourse" CssClass="label" Text='<%# Container.DataItem("Descrip") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Meetings (Room)">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTimeAndRoom" CssClass="label" Text='<%# Container.DataItem("TimeAndRoom") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Instructor">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInstructor" CssClass="label" Text='<%# Container.DataItem("InstructorName") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Term Start">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTermStartDate" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 16px">
                                <asp:Panel ID="pnlRemainingCourses" runat="server" Visible="false">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="contentcellheader">
                                                <asp:Label ID="lblRemainingCourses" runat="server" CssClass="label" Font-Bold="true">Remaining Courses</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 10px">
                                                <asp:DataGrid ID="dgrdRemainingCourses" CellPadding="3" ShowFooter="False" BorderStyle="Solid"
                                                    AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false"
                                                    Width="100%" runat="server">
                                                    <EditItemStyle CssClass="label"></EditItemStyle>
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                    <FooterStyle CssClass="label"></FooterStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Code">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Course">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRCourse" CssClass="label" Text='<%# Container.DataItem("Req") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Credits">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCredits" CssClass="label" Text='<%# Container.DataItem("Credits") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Hours">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHours" CssClass="label" Text='<%# Container.DataItem("Hours") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <!-- end content table -->
                </div>
            </td>
        </tr>
    </table>
   
    
   <%-- <table cellspacing="0" cellpadding="0" width="100%" border="0" class="maincontenttable">    
        <tr>
            <td class="detailsframe">
                <div class="scrollpopupresume">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="arcontentcells" nowrap>
                                <asp:Label ID="lblStd" runat="server" CssClass="label">Student</asp:Label>
                            </td>
                            <td class="arcontentcells2" nowrap style="padding-right: 20px">
                                <asp:Label ID="lblStdName" runat="server" CssClass="textbox"></asp:Label>
                            </td>
                            <td class="arcontentcells" nowrap>
                                <asp:Label ID="lblStudentID" runat="server" CssClass="label">Student ID</asp:Label>
                            </td>
                            <td class="arcontentcells2" nowrap style="padding-right: 20px">
                                <asp:Label ID="lblStdID" runat="server" CssClass="textbox"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <!-- begin content table -->
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="padding-top: 10px">
                                <!-- Transcript table -->
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td>
                                            <asp:DataGrid ID="dgrdTransactionSearch1" runat="server" Width="100%" EditItemStyle-Wrap="false" 
                                                HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid"
                                                ShowFooter="False" CellPadding="3" BorderWidth="1px" BorderColor="#E0E0E0">
                                                <EditItemStyle CssClass="label"></EditItemStyle>
                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <FooterStyle CssClass="label"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Term Start Date">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStartDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Code">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Course">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label1" CssClass="label" Text='<%# Container.DataItem("Descrip") %>'
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Grade">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label3" CssClass="label" Text='<%# Container.DataItem("Grade") %>'
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Grade">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label4" CssClass="label" Text='<%# Container.DataItem("NumGrade") %>'
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 16px">
                                <asp:Panel ID="pnlSummary" runat="server" Visible="False">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="left"
                                        border="0">
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblNumber" runat="server" CssClass="label">Number Of Classes</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblTotalClasses" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label4" runat="server" CssClass="label">Credits Attempted</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label5" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label6" runat="server" CssClass="label">Credits Earned</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label7" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label8" runat="server" CssClass="label">GPA</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label9" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label10" runat="server" CssClass="label">Completed SAP Requirements ?</asp:Label>
                                            </td>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="label12" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <!-- Scheduled Courses table -->
                        <tr>
                            <td style="padding-top: 16px">
                                <asp:Panel ID="pnlScheduledCourses1" runat="server" Visible="false">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="contentcellheader">
                                                <asp:Label ID="lblSchedCourses" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 10px">
                                                <asp:DataGrid ID="dgrdScheduledCourses1" CellPadding="3" ShowFooter="False" BorderStyle="Solid"
                                                    AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false"
                                                    Width="100%" runat="server" BorderWidth="1px" BorderColor="#E0E0E0">
                                                    <EditItemStyle CssClass="label"></EditItemStyle>
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                    <FooterStyle CssClass="label"></FooterStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Code">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSchedCCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Course">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSchedCourse" CssClass="label" Text='<%# Container.DataItem("Descrip") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Meetings (Room)">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTimeAndRoom" CssClass="label" Text='<%# Container.DataItem("TimeAndRoom") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Instructor">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInstructor" CssClass="label" Text='<%# Container.DataItem("InstructorName") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Term Start">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTermStartDate" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 16px">
                                <asp:Panel ID="pnlRemainingCourses1" runat="server" Visible="false">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="contentcellheader">
                                                <asp:Label ID="lblRemainingCourses" runat="server" CssClass="label" Font-Bold="true">Remaining Courses</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 10px">
                                                <asp:DataGrid ID="dgrdRemainingCourses1" CellPadding="3" ShowFooter="False" BorderStyle="Solid"
                                                    AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false"
                                                    Width="100%" runat="server">
                                                    <EditItemStyle CssClass="label"></EditItemStyle>
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="datagridheader"></ItemStyle>
                                                    <HeaderStyle CssClass="datagriditemstyle"></HeaderStyle>
                                                    <FooterStyle CssClass="label"></FooterStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Code">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Course">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRCourse" CssClass="label" Text='<%# Container.DataItem("Req") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Credits">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCredits" CssClass="label" Text='<%# Container.DataItem("Credits") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Hours">
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHours" CssClass="label" Text='<%# Container.DataItem("Hours") %>'
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>--%>
                    <!-- end content table -->
              <%--  </div>
            </td>
        </tr>
    </table>
    </div>--%>
    <asp:TextBox ID="txtStudentId" CssClass="label" runat="server" Visible="false"></asp:TextBox><asp:TextBox
        ID="txtStEmploymentId" CssClass="label" runat="server" Visible="false"></asp:TextBox><asp:CheckBox
            ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox><asp:TextBox
                ID="txtStudentDocs" runat="server" Visible="False"></asp:TextBox>
    <!-- end rightcolumn -->
   </asp:Panel> 
    </form>
</body>
</html>
