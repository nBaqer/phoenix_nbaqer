﻿
Imports Fame.Common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports BO = Advantage.Business.Objects
Imports Fame.Advantage.Common
Imports System.Text
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports Fame.Advantage.Common.FAME.Advantage.Common
Imports Fame.Advantage.DataAccess.LINQ.Common
Imports Fame.Advantage.DataAccess.LINQ.PostExams

Partial Class postexamresults
    Inherits BasePage


    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected StudentId, campusid As String
    Protected ResourceID As Integer
    Private dtOriginal As DataTable
    Private grdBkCount As Integer


    Protected state As AdvantageSessionState
    Protected LeadId As String
    Protected boolSwitchCampus As Boolean = False
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings



    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Dim objCommon As New CommonUtilities
        'Get the StudentId from the state object associated with this page
        campusid = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        ResourceID = CInt(HttpContext.Current.Request.Params("resid"))


        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(542) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With


        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        grdBkCount = Int32.Parse(MyAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings").ToString)
        'Always disable the History button. It does not apply to this page.
        'Header1.EnableHistoryButton(False)
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceID, campusid)

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            BuildStudentEnrollmentsDDL(StudentId)
            BindRepeater(ddlTerms.SelectedValue)


            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""


        Else
            BindRepeater(ddlTerms.SelectedValue)
        End If

    End Sub
    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade
        With ddlEnrollmentId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()

            .SelectedIndex = 0
        End With
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub DisplaySaveMessage(ByVal saveMessage As String)
        '   Display Save in message box in the client
        CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, saveMessage)
    End Sub
    Protected Sub ddlEnrollmentId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
        'disable all items before the binding
        DisableAllItemsInListItemCollection(ddlTerms.Items)
        ddlTerms.SelectedIndex = 0
        BindRepeater(ddlTerms.SelectedValue)
    End Sub
    Private Sub BindRepeater(ByVal termId As String)
        'bind repeater
        Dim originalDataSet = (New PostExamsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetPostExamsData(ddlEnrollmentId.SelectedValue, ddlTypes.SelectedValue)
        dtOriginal = DataTableFunctions.ConvertToDataTable(originalDataSet)
        Dim dt As DataTable = dtOriginal
        Select Case ddlTerms.SelectedValue
            Case Guid.Empty.ToString()
                rPostExamResults.DataSource = New DataView(dt, Nothing, "", DataViewRowState.CurrentRows)
            Case Else
                rPostExamResults.DataSource = New DataView(dt, "TermId='" + termId + "'", "", DataViewRowState.CurrentRows)
        End Select

        rPostExamResults.DataBind()
    End Sub
    Private Function DeleteDuplicatedRows1(ByVal dt As DataTable) As DataTable
        If dt.Rows.Count = 0 Then Return dt
        Dim previous As Guid = Guid.Empty
        Dim previousclssection As Guid = Guid.Empty
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("clsSectionId") Is System.DBNull.Value Then
                If (dt.Rows(i)("GRDComponentTypeId") = previous Or dt.Rows(i)("GrpDescrip") Is System.DBNull.Value) Then
                    dt.Rows(i).Delete()
                Else
                    previous = dt.Rows(i)("GRDComponentTypeId")
                End If
            Else
                If (dt.Rows(i)("GRDComponentTypeId") = previous And dt.Rows(i)("clsSectionId") = previousclssection) Or dt.Rows(i)("GrpDescrip") Is System.DBNull.Value Then
                    dt.Rows(i).Delete()
                Else
                    previous = dt.Rows(i)("GRDComponentTypeId")
                    previousclssection = dt.Rows(i)("clsSectionId")
                End If
            End If

        Next
        dt.AcceptChanges()
        Return dt
    End Function

    Private Sub GetPostedScoresObj(ByRef postExamListFinal As List(Of postExamResultsObject))
        Dim list As RepeaterItemCollection = rPostExamResults.Items
        Dim PostExamList As New List(Of postExamResultsObject)

        For Each item As RepeaterItem In list
            If item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.Item Then

                For i As Integer = 1 To CType(CType(item.FindControl("txtNumber"), TextBox).Text, Integer)
                    Dim ctrlScore As String = "tbScore_" & i & "_row" & item.ItemIndex + 1
                    Dim ctrlPostDate As String = "tbPostDate_" & i & "_row" & item.ItemIndex + 1
                    Dim ctrlDBID As String = "hdnResultId_" & i & "_row" & item.ItemIndex + 1
                    Dim tbControl As TextBox = CType(item.FindControl(ctrlScore), TextBox)
                    Dim tbPostDate As TextBox = CType(item.FindControl(ctrlPostDate), TextBox)
                    Dim ctrDBIdName As String = "hdnDBIdName_" & i & "_row" & item.ItemIndex + 1

                    If (Not tbControl.Text.Trim() = String.Empty AndAlso tbControl.Text.Trim().ToLower() = "t") Then
                        tbControl.Text = "-1"
                    End If

                    Dim score As Decimal = 0.0
                    Dim originalScore As Decimal = 0.0
                    Dim postDate As Date = Date.MinValue
                    Dim originalPostDate As Date = Date.MinValue
                    Dim hasNonBlankScore As Boolean = Decimal.TryParse(tbControl.Text, score)
                    Dim hasNonBlankPostDate As Boolean = Date.TryParse(tbPostDate.Text, postDate)

                    Dim updateScore As Boolean = False
                    Dim updateDate As Boolean = False
                    Dim deleteScore As Boolean = False
                    Dim deleteDate As Boolean = False
                    Dim updateWithTodaysDate As Boolean = False
                    Dim updateWithOriginalPostDate As Boolean = False

                    Dim type As Integer = Integer.Parse(CType(item.FindControl("tbType"), TextBox).Text)
                    Dim dbId As String = CType(item.FindControl(ctrlDBID), HiddenField).Value
                    Dim DBIdName As String = CType(item.FindControl(ctrDBIdName), HiddenField).Value
                    Dim clssectionId As String = CType(item.FindControl("tbClsSectionId"), TextBox).Text
                    Dim InstrGrdBkWgtDetailId As String = CType(item.FindControl("tbInstrGrdBkWgtDetailId"), TextBox).Text
                    Dim ComponentDescription As String = CType(item.FindControl("lblCompDescrip"), Label).Text
                    Dim resnum As Integer = i

                    'update score and update date
                    If hasNonBlankScore And hasNonBlankPostDate Then
                        updateScore = True
                        updateDate = True
                    Else
                        deleteScore = True
                        deleteDate = True
                    End If

                    Dim examResults As New postExamResultsObject()
                    examResults.dbIdName = DBIdName
                    examResults.dbId = dbId
                    examResults.clssectionId = clssectionId
                    examResults.instrGrdBkWgtDetailId = InstrGrdBkWgtDetailId
                    If updateScore Then examResults.score = score
                    If deleteScore Then examResults.deleteScore = True
                    If updateDate Then examResults.postDate = postDate
                    If deleteDate Then examResults.deleteDate = True
                    If updateWithTodaysDate Then examResults.updateWithTodaysDate = True
                    If updateWithOriginalPostDate Then examResults.originalPostDate = originalPostDate
                    Select Case type
                        Case 1

                            examResults.componentDescription = ComponentDescription.Replace(",", " ")
                            examResults.resnum = resnum

                    End Select
                    PostExamList.Add(examResults)
                Next
            End If

        Next
        postExamListFinal = PostExamList

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim postExamListFinal As New List(Of postExamResultsObject)
        If Not IsInputValid() Then
            DisplayErrorMessage(GetErrorMessage())
            Exit Sub
        End If
        GetPostedScoresObj(postExamListFinal)
        Dim resultObj As String = (New ExamsFacade).PostExamResultsObj(postExamListFinal, ddlEnrollmentId.SelectedValue, Session("UserName"))

        If Not resultObj = "" Then
            '   Display Error Message
            DisplayErrorMessage(resultObj)
            Exit Sub
        Else
            BindRepeater(ddlTerms.SelectedValue)
        End If
        DisplaySaveMessage("Grades saved successfully for student.")
    End Sub
    Private Function IsInputValid() As Boolean
        Dim list As RepeaterItemCollection = rPostExamResults.Items
        For Each item As RepeaterItem In list
            If item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.Item Then

                Dim tbMinScore As TextBox = CType(item.FindControl("tbMinScore"), TextBox)
                Dim tbMaxScore As TextBox = CType(item.FindControl("tbMaxScore"), TextBox)
                Dim endDate As Date = Date.Parse(CType(item.FindControl("tbEndDate"), TextBox).Text)
                Dim startDate As Date = Date.Parse(CType(item.FindControl("tbStartDate"), TextBox).Text)
                Dim objPanel As PlaceHolder = DirectCast(item.FindControl("pllDynamic"), PlaceHolder)
                For i As Integer = 1 To CType(CType(item.FindControl("txtNumber"), TextBox).Text, Integer)
                    Dim ctrlScore As String = "tbScore_" & i & "_row" & item.ItemIndex + 1
                    Dim ctrlPostDate As String = "tbPostDate_" & i & "_row" & item.ItemIndex + 1
                    Dim tbControl As TextBox = CType(item.FindControl(ctrlScore), TextBox)
                    Dim tbPostDate As TextBox = CType(item.FindControl(ctrlPostDate), TextBox)

                    Dim score As Decimal = 0.0
                    Dim originalScore As Decimal = 0.0
                    Dim postDate As Date = Date.MinValue
                    'Dim originalPostDate As Date = Date.MinValue
                    Dim hasNonBlankScore As Boolean = (Not tbControl.Text.Trim() = String.Empty AndAlso (tbControl.Text.Trim().ToLower() = "t" OrElse Decimal.TryParse(tbControl.Text, score)))
                    Dim hasNonBlankPostDate As Boolean = Date.TryParse(tbPostDate.Text, postDate)
                    'Dim hasNonBlankOriginalScore As Boolean = Decimal.TryParse(tbOriginalScore.Text, originalScore)
                    'Dim hasNonBlankOriginalPostDate As Boolean = Date.TryParse(tbOriginalPostDate.Text, originalPostDate)
                    'commented by balaji, the following validation is not neccessary as Post Exam results pulls
                    'scores posted on Post Grade by Student (Course Level) ' 07/17/2008
                    'If hasNonBlankOriginalScore Xor hasNonBlankOriginalPostDate Then Return False
                    'If (Not hasNonBlankOriginalScore And Not hasNonBlankOriginalPostDate And Not hasNonBlankScore And hasNonBlankPostDate) Then Return False
                    If hasNonBlankScore And Not hasNonBlankPostDate Then Return False
                    If Not hasNonBlankScore And hasNonBlankPostDate Then Return False

                    If Not tbPostDate.Text.Trim() = String.Empty And Not Date.TryParse(tbPostDate.Text, postDate) Then Return False

                    If postDate < startDate And Not (postDate = Date.MinValue) Then Return False
                    'If postDate > endDate Then Return False
                    If postDate > Date.Now Then Return False
                    'If (Not hasNonBlankOriginalScore And Not hasNonBlankOriginalPostDate And hasNonBlankScore And Not hasNonBlankPostDate And Date.Now > endDate) Then Return False
                    If (Not tbControl.Text.Trim() = String.Empty AndAlso Not tbControl.Text.Trim().ToLower() = "t" AndAlso Not Decimal.TryParse(tbControl.Text, score)) Then Return False
                    If score < Integer.Parse(tbMinScore.Text) Then Return False
                    If score > Integer.Parse(tbMaxScore.Text) Then Return False
                Next
            End If
        Next
        Return True
    End Function
    Private Function GetErrorMessage() As String
        Dim sb As New StringBuilder()
        Dim list As RepeaterItemCollection = rPostExamResults.Items
        For Each item As RepeaterItem In list
            If item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.Item Then


                Dim tbMinScore As TextBox = CType(item.FindControl("tbMinScore"), TextBox)
                Dim tbMaxScore As TextBox = CType(item.FindControl("tbMaxScore"), TextBox)
                Dim endDate As Date = Date.Parse(CType(item.FindControl("tbEndDate"), TextBox).Text)
                Dim startDate As Date = Date.Parse(CType(item.FindControl("tbStartDate"), TextBox).Text)

                For i As Integer = 1 To CType(CType(item.FindControl("txtNumber"), TextBox).Text, Integer)
                    Dim ctrlScore As String = "tbScore_" & i & "_row" & item.ItemIndex + 1
                    Dim ctrlPostDate As String = "tbPostDate_" & i & "_row" & item.ItemIndex + 1
                    Dim tbControl As TextBox = CType(item.FindControl(ctrlScore), TextBox)
                    Dim tbPostDate As TextBox = CType(item.FindControl(ctrlPostDate), TextBox)
                    Dim score As Decimal = 0.0
                    Dim postDate As Date = Date.MinValue
                    Dim hasNonBlankScore As Boolean = (Not tbControl.Text.Trim() = String.Empty AndAlso (tbControl.Text.Trim().ToLower() = "t" OrElse Decimal.TryParse(tbControl.Text, score)))
                    Dim hasNonBlankPostDate As Boolean = Date.TryParse(tbPostDate.Text, postDate)

                    If Not tbPostDate.Text.Trim() = String.Empty And Not Date.TryParse(tbPostDate.Text, postDate) Then
                        AppendErrorMessage(sb, tbPostDate.Text, "Invalid date.")
                    End If

                    If postDate < startDate And Not (postDate = Date.MinValue) Then
                        AppendErrorMessage(sb, tbPostDate.Text, "Date is earlier than start date.")
                    End If

                    If postDate > Date.Now Then
                        AppendErrorMessage(sb, tbPostDate.Text, "Date is a future date.")
                    End If

                    If (Not tbControl.Text.Trim() = String.Empty AndAlso Not tbControl.Text.Trim().ToLower() = "t" AndAlso Not Decimal.TryParse(tbControl.Text, score)) Then
                        AppendErrorMessage(sb, tbControl.Text, "Score is invalid.")
                    End If
                    If score < Integer.Parse(tbMinScore.Text) Then
                        AppendErrorMessage(sb, tbControl.Text, "Score is below minimum.")
                    End If
                    If score > Integer.Parse(tbMaxScore.Text) Then
                        AppendErrorMessage(sb, tbControl.Text, "Score is above maximum.")
                    End If
                    If hasNonBlankScore And Not hasNonBlankPostDate Then
                        AppendErrorMessage(sb, tbControl.Text, "Score without Date.")
                    End If

                    If Not hasNonBlankScore And hasNonBlankPostDate Then
                        AppendErrorMessage(sb, tbPostDate.Text, "Date without score.")
                    End If

                Next
            End If
        Next
        Return sb.ToString()
    End Function

    Private Sub AppendErrorMessage(ByVal sb As StringBuilder, ByVal value As String, ByVal errorMessage As String)
        sb.Append(" '")
        sb.Append(value)
        sb.Append("' ")
        sb.Append(errorMessage)
        sb.Append(vbCrLf)
    End Sub
    Protected Sub ddlTypes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTypes.SelectedIndexChanged, ddlTerms.SelectedIndexChanged
        BindRepeater(ddlTerms.SelectedValue)
    End Sub

    Protected Sub rPostExamResults_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rPostExamResults.ItemCreated

        If e.Item.ItemType = ListItemType.Header Then
            Dim objHeader As PlaceHolder = DirectCast(e.Item.FindControl("pllHeader"), PlaceHolder)
            Dim tblHeader As New Table

            tblHeader.Width = Unit.Percentage(100)
            tblHeader.CellPadding = "0"
            tblHeader.CellSpacing = "0"
            tblHeader.CssClass = "gradebooktable"
            tblHeader.BorderWidth = "0"
            Dim cell As New TableCell
            Dim row As New TableRow


            For i As Integer = 1 To grdBkCount
                For j As Integer = 1 To 2
                    cell = New TableCell
                    cell.HorizontalAlign = HorizontalAlign.Center
                    If j Mod 2 = 1 Then
                        cell.Text = "Score"
                        cell.Width = "45"
                    Else
                        cell.Text = "Date"
                        cell.Width = "72"
                    End If
                    row.Cells.Add(cell)
                    cell = New TableCell
                    cell.HorizontalAlign = HorizontalAlign.Center
                    cell.Width = "5"
                    row.Cells.Add(cell)
                    tblHeader.Rows.Add(row)
                Next
            Next

            'Add Header Row to table

            objHeader.Controls.Add(tblHeader)

        End If


        If (e.Item.ItemType = ListItemType.AlternatingItem) OrElse (e.Item.ItemType = ListItemType.Item) Then

            Dim objPanel As PlaceHolder = DirectCast(e.Item.FindControl("pllDynamic"), PlaceHolder)
            If objPanel IsNot Nothing Then
                Dim tbl As New Table

                tbl.ID = "tbl" & ddlEnrollmentId.SelectedValue.ToString 'ViewState("StuEnrollId")
                tbl.Width = Unit.Percentage(100)
                tbl.CellPadding = "0"
                tbl.CellSpacing = "0"
                tbl.CssClass = "gradebooktable"
                tbl.BorderWidth = "0"
                Dim cell As New TableCell

                Dim row As New TableRow


                For i As Integer = 1 To grdBkCount
                    Dim hdnDBID As New HiddenField
                    hdnDBID.ID = "hdnResultId_" & i & "_row" & e.Item.ItemIndex + 1
                    cell = New TableCell
                    cell.Controls.Add(hdnDBID)
                    row.Controls.Add(cell)

                    Dim hdnDBIDName As New HiddenField
                    hdnDBIDName.ID = "hdnDBIDName_" & i & "_row" & e.Item.ItemIndex + 1
                    cell = New TableCell
                    cell.Controls.Add(hdnDBIDName)
                    row.Controls.Add(cell)

                    For j As Integer = 1 To 2
                        cell = New TableCell
                        cell.HorizontalAlign = HorizontalAlign.Center
                        Dim rblist As New TextBox
                        rblist.CssClass = "TextBox"
                        If j Mod 2 = 1 Then
                            rblist.ID = "tbScore_" & i & "_row" & e.Item.ItemIndex + 1
                            rblist.Width = "55"
                            rblist.ClientIDMode = UI.ClientIDMode.Static
                        Else
                            rblist.ID = "tbPostDate_" & i & "_row" & e.Item.ItemIndex + 1
                            rblist.Width = "72"
                            rblist.ClientIDMode = UI.ClientIDMode.Static
                        End If
                        cell.Controls.Add(rblist)
                        row.Cells.Add(cell)

                        row.Cells.Add(cell)
                        tbl.Rows.Add(row)
                    Next
                Next
                'Add Row to table
                objPanel.Controls.Add(tbl)

            End If
        End If


    End Sub
    Protected Sub rPostExamResults_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rPostExamResults.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item

                Dim termId As String = CType(e.Item.DataItem("TermId"), Guid).ToString()
                Dim li As ListItem = ddlTerms.Items.FindByValue(termId)
                If li Is Nothing And Not (e.Item.DataItem("GrpDescrip") Is System.DBNull.Value) Then
                    li = New ListItem(e.Item.DataItem("GrpDescrip"), termId)
                    ddlTerms.Items.Add(li)
                End If
                If CType(e.Item.DataItem("StuEnrollId"), Guid).ToString().ToLower = ddlEnrollmentId.SelectedValue.ToLower Then
                    li.Enabled = True
                Else
                    li.Enabled = False
                End If

                Dim ctrlName As String
                Dim ctrlName1 As String
                Dim hdnCtrlName As String
                Dim hdnDBIDName As String
                Dim cnt As Integer
                If Not IsDBNull(e.Item.DataItem("Number")) Then
                    cnt = CType(e.Item.DataItem("Number"), Integer)
                End If
                For i As Integer = 1 To grdBkCount
                    ctrlName = "tbScore_" & i & "_row" & e.Item.ItemIndex + 1
                    ctrlName1 = "tbPostDate_" & i & "_row" & e.Item.ItemIndex + 1
                    If i > cnt Then
                        Dim txt As TextBox = CType(e.Item.FindControl(ctrlName), TextBox)
                        txt.Enabled = False
                        txt.BackColor = Color.LightGray
                        Dim txt1 As TextBox = CType(e.Item.FindControl(ctrlName1), TextBox)
                        txt1.Enabled = False
                        txt1.BackColor = Color.LightGray

                    End If
                Next
                Dim aRows() As DataRow = dtOriginal.Select("ClsSectionId='" & e.Item.DataItem("ClsSectionid").ToString & "' and InstrGrdBkWgtDetailId='" & e.Item.DataItem("InstrGrdBkWgtDetailId").ToString & "'", "Row")

                For i As Integer = 0 To aRows.Length - 1
                    ctrlName = "tbScore_" & i + 1 & "_row" & e.Item.ItemIndex + 1
                    ctrlName1 = "tbPostDate_" & i + 1 & "_row" & e.Item.ItemIndex + 1
                    hdnCtrlName = "hdnResultId_" & i + 1 & "_row" & e.Item.ItemIndex + 1
                    hdnDBIDName = "hdnDBIDName_" & i + 1 & "_row" & e.Item.ItemIndex + 1

                    Dim txt As TextBox = CType(e.Item.FindControl(ctrlName), TextBox)
                    Dim txt1 As TextBox = CType(e.Item.FindControl(ctrlName1), TextBox)
                    Dim hdn As HiddenField = CType(e.Item.FindControl(hdnCtrlName), HiddenField)
                    Dim hdnDBName As HiddenField = CType(e.Item.FindControl(hdnDBIDName), HiddenField)
                    If Not aRows(i).IsNull("PostDate") Then
                        txt.Text = If(aRows(i)("Score").ToString = "-1.00", "T", aRows(i)("Score").ToString)
                        txt1.Text = CDate(aRows(i)("PostDate"))
                    End If
                    hdn.Value = aRows(i)("DBId").ToString
                    hdnDBName.Value = aRows(i)("DBIdName").ToString
                Next

                'cnt = CType(e.Item.DataItem("Number"), Integer)
        End Select
    End Sub
    Private Sub DisableAllItemsInListItemCollection(ByVal lic As ListItemCollection)
        For Each item As ListItem In lic
            If item.Value <> Guid.Empty.ToString Then item.Enabled = False
        Next
    End Sub

    Protected Function GetDefaultDate() As String
        Return Date.Today.ToString
    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BindToolTip()
    End Sub
End Class