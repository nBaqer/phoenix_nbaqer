<%@ Control Language="VB" AutoEventWireup="false" CodeFile="IMaint_SetupSchedule.ascx.vb"
    Inherits="AR_IMaint_SetupSchedule" %>
<%@ Register TagPrefix="ew" Assembly="eWorld.UI" Namespace="eWorld.UI" %>
<div style="width: 90%; margin: 20px">
    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="left">
        <tr>
            <td class="ClassSectionLabelCell" nowrap>
                <asp:Label ID="lblSchCode" runat="server" CssClass="Label">Code <span style="color: red">*</span></asp:Label>
            </td>
            <td class="ClassSectionContentCell">
                <asp:TextBox ID="txtSchCode" runat="server" Width="50px" CssClass="textbox" />
            </td>
        </tr>
        <tr>
            <td class="ClassSectionLabelCell" nowrap>
                <asp:Label ID="lblSchDescrip" runat="server" CssClass="Label">Schedule Name <span style="color: red">*</span></asp:Label>
            </td>
            <td class="ClassSectionContentCell">
                <asp:TextBox ID="txtSchDescrip" runat="server" CssClass="textbox" />
            </td>

        </tr>
        <tr>
            <td class="ClassSectionLabelCell" nowrap>
                <asp:Label ID="lblSchActivePrompt" runat="server" CssClass="Label">Status <span style="color: red">*</span></asp:Label>
            </td>
            <td class="ClassSectionContentCell">
                <asp:DropDownList ID="ddlSchActive" runat="server" CssClass="dropdownlistff" />
            </td>
            <td class="ClassSectionLabelCell" nowrap>
                <asp:Label ID="lblSchFlextime" runat="server" CssClass="Label" Text="Flextime schedule"
                    ToolTip="When checked, a student can satisfy the scheduled daily hours anytime between the specified Time In and Time Out" />
            </td>
            <td class="ClassSectionContentCell">
                <asp:CheckBox ID="cbSchFlextime" runat="server" TextAlign="Left" AutoPostBack="True" />
            </td>
        </tr>
        <tr>
            <td class="ClassSectionLabelCell" nowrap>
                <asp:Label ID="lblSchTotHours" runat="server" CssClass="Label" Text="Total Hours:"
                    ToolTip="Total number of hours in the schedule" />
            </td>
            <td class="ClassSectionContentCell">
                <asp:Label ID="lblTotHours" runat="server" CssClass="Label" />
            </td>
        </tr>
        <tr>
            <td class="ClassSectionLabelCell" nowrap>
                <asp:Label ID="Label2" runat="server" CssClass="Label">Using time clock?</asp:Label>
            </td>
            <td class="ClassSectionContentCell">
                <asp:Label ID="lblUseTimeClock" runat="server" CssClass="Label" />
            </td>
        </tr>
    </table>
    <div style="width: 100%; text-align: center; margin-top: 20px; clear: both">
        <asp:Repeater ID="rptSchedule" runat="server">
            <HeaderTemplate>
                <table cellspacing="0" cellpadding="0" style="border: 1px solid #ebebeb; border-right: 0; border-bottom: 0">
                    <tr>
                        <td class="SchedHeader">
                            <asp:Label ID="lblHeadDay" runat="server" Text="Day" ToolTip="Day of week" /></td>
                        <td class="SchedHeader" style="width: 70px" id="sch0" runat="server">
                            <asp:Label ID="lblHeadTimeIn" runat="server" Text="Time In" ToolTip="Time In" /></td>
                        <td class="SchedHeader" style="width: 70px" id="sch1" runat="server">
                            <asp:Label ID="lblHeadLunchOut" runat="server" Text="Lunch Out" ToolTip="Lunch Out" /></td>
                        <td class="SchedHeader" style="width: 70px" id="sch2" runat="server">
                            <asp:Label ID="lblHeadLunchIn" runat="server" Text="Lunch In" ToolTip="Lunch In" /></td>
                        <td class="SchedHeader" style="width: 70px" id="sch3" runat="server">
                            <asp:Label ID="lblHeadTimeOut" runat="server" Text="Time Out" ToolTip="Time Out" /></td>
                        <td class="SchedHeader" id="sch4" runat="server">
                            <asp:Label ID="lblHeadMaxHrsNoLunch" runat="server" Text="Max Hrs w/o Lunch" ToolTip="Maximum hours without lunch" /></td>
                        <td class="SchedHeader" id="sch5" runat="server">
                            <asp:Label ID="lblHeadSchedHours" runat="server" Text="Sched Hours" ToolTip="Daily Scheduled Hours" /></td>
                        <td class="SchedHeader" id="sch6" runat="server">
                            <asp:Label ID="lblHeadAllowEarlyIn" runat="server" Text="Allow Early In" ToolTip="Check to allow early in" /></td>
                        <td class="SchedHeader" id="sch7" runat="server">
                            <asp:Label ID="lblHeadAllowLateIn" runat="server" Text="Allow Late Out" ToolTip="Check to allow late out" /></td>
                        <td class="SchedHeader" id="sch8" runat="server">
                            <asp:Label ID="lblHeadAllowExtraHours" runat="server" Text="Allow Extra Hours" ToolTip="Check to allow extra hours" /></td>
                        <td class="SchedHeader" id="sch9" runat="server">
                            <asp:Label ID="Label1" runat="server" Text="Check Tardy In" ToolTip="Check tardy in" /></td>
                        <td class="SchedHeader" style="width: 70px" id="sch10" runat="server">
                            <asp:Label ID="lblHeadCheckTardyIn" runat="server" Text="Max In Before Tardy" ToolTip="Max In Before Tardy" /></td>
                        <td class="SchedHeader" style="width: 70px" id="sch11" runat="server">
                            <asp:Label ID="lblHeadAssignedTardyInTime" runat="server" Text="Assigned Tardy In Time"
                                ToolTip="Assigned Tardy In Time" /></td>
                        <td class="SchedHeader" style="width: 70px" id="sch12" runat="server">
                            <asp:Label ID="lblMinHoursConsideredPresent" runat="server" Text="Min Hours Considered Present"
                                ToolTip="Minimum number of hours to be considered present" /></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="SchedItem" align="center">
                        <asp:Label ID="lblDOW" runat="server" Text='<%#Container.DataItem("dw") %>' /></td>
                    <td class="SchedItem" id="schd0" runat="server">
                        <ew:TimePicker ID="tpTimeIn" ToolTip="Time In" runat="server" ImageUrl="../images/ico_clock.gif"
                            Nullable="True" PopupWidth="370px" PopupHeight="175px" NumberOfColumns="8" MinuteInterval="OneMinute"
                            DisplayUnselectableTimes="False" PopupLocation="Bottom" ControlDisplay="TextBoxImage"
                            TextboxLabelStyle-CssClass="SchedTextBox" Enabled="true" LowerBoundTime="6AM" DisableTextboxEntry="false">
                            <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                            <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                            <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                        </ew:TimePicker>
                    </td>
                    <td class="SchedItem" id="schd1" runat="server">
                        <ew:TimePicker ID="tpLunchOut" ToolTip="Lunch Out" runat="server" ImageUrl="../images/ico_clock.gif"
                            Nullable="True" PopupWidth="254px" PopupHeight="175px" NumberOfColumns="8" MinuteInterval="OneMinute"
                            DisplayUnselectableTimes="True" DisableTextboxEntry="False" PopupLocation="Bottom" ControlDisplay="TextBoxImage"
                            TextboxLabelStyle-CssClass="SchedTextBox">
                            <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                            <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                            <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                        </ew:TimePicker>
                    </td>
                    <td class="SchedItem" id="schd2" runat="server">
                        <ew:TimePicker ID="tpLunchIn" ToolTip="Lunch In" runat="server" ImageUrl="../images/ico_clock.gif"
                            Nullable="True" PopupWidth="254px" PopupHeight="175px" NumberOfColumns="8" MinuteInterval="OneMinute"
                            DisplayUnselectableTimes="True" DisableTextboxEntry="False" PopupLocation="Bottom" ControlDisplay="TextBoxImage"
                            TextboxLabelStyle-CssClass="SchedTextBox">
                            <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                            <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                            <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                        </ew:TimePicker>
                    </td>
                    <td class="SchedItem" id="schd3" runat="server">
                        <ew:TimePicker ID="tpTimeOut" ToolTip="Time Out" runat="server" ImageUrl="../images/ico_clock.gif"
                            Nullable="True" PopupWidth="254px" PopupHeight="175px" NumberOfColumns="8" MinuteInterval="OneMinute"
                            DisplayUnselectableTimes="True" DisableTextboxEntry="False" PopupLocation="Bottom" ControlDisplay="TextBoxImage"
                            TextboxLabelStyle-CssClass="SchedTextBox">
                            <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                            <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                            <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                        </ew:TimePicker>
                    </td>
                    <td class="SchedItem" id="schd4" runat="server">
                        <ew:NumericBox runat="server" ID="txtMaxNoHrsLunch" Width="38px" PositiveNumber="True"
                            CssClass="SchedTextBox" Text='<%#Container.DataItem("maxnolunch") %>' /></td>
                    <td class="SchedItem" id="schd5" runat="server">
                        <ew:NumericBox runat="server" ID="txtTotHours" Width="38px" PositiveNumber="True"
                            CssClass="SchedTextBox" Text='<%#Container.DataItem("total") %>' /></td>
                    <td class="SchedItem" id="schd6" runat="server">
                        <asp:CheckBox ID="cbAllowEarlyIn" runat="server" /></td>
                    <td class="SchedItem" id="schd7" runat="server">
                        <asp:CheckBox ID="cbAllowLateOut" runat="server" /></td>
                    <td class="SchedItem" id="schd8" runat="server">
                        <asp:CheckBox ID="cbAllowExtraHours" runat="server" /></td>
                    <td class="SchedItem" id="schd9" runat="server">
                        <asp:CheckBox ID="cbCheckTardyIn" runat="server" /></td>
                    <td class="SchedItem" id="schd10" runat="server">
                        <ew:TimePicker ID="tpMaxInTardy" ToolTip="Max Time In Before Tardy" runat="server"
                            ImageUrl="../images/ico_clock.gif" Nullable="True" PopupWidth="254px" PopupHeight="175px"
                            NumberOfColumns="8" MinuteInterval="OneMinute" DisplayUnselectableTimes="True" DisableTextboxEntry="False"
                            PopupLocation="left" ControlDisplay="TextBoxImage" TextboxLabelStyle-CssClass="SchedTextBox">
                            <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                            <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                            <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                        </ew:TimePicker>
                    </td>
                    <td class="SchedItem" id="schd11" runat="server">
                        <ew:TimePicker ID="tpTardyInTime" ToolTip="Max Time In Before Tardy" runat="server"
                            ImageUrl="../images/ico_clock.gif" Nullable="True" PopupWidth="254px" PopupHeight="175px"
                            NumberOfColumns="8" MinuteInterval="OneMinute" DisplayUnselectableTimes="True" DisableTextboxEntry="False"
                            PopupLocation="left" ControlDisplay="TextBoxImage" TextboxLabelStyle-CssClass="SchedTextBox">
                            <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                            <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                            <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                        </ew:TimePicker>
                        <td class="SchedItem" id="schd12" runat="server">
                            <ew:NumericBox runat="server" ID="txtMinHoursPresent" Width="38px" PositiveNumber="True"
                                CssClass="SchedTextBox" Text='<%#Container.DataItem("MinimumHoursToBePresent") %>' /></td>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
           
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td class="SchedHeader"></td>
                    </tr>
                    <tr>
                        <td class="SchedHeader">
                            <asp:Label ID="lblFooterText" runat="server" Visible="false" Text="Time should be input in the following format (hh:mm AM or hh:mm PM). Examples: 10:15 AM  4:20 PM" /></td>
                    </tr>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
