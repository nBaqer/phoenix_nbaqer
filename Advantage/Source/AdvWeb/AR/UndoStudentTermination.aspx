﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UndoStudentTermination.aspx.vb" Inherits="AdvWeb.AR.AR_UndoStudentTermination" MasterPageFile="~/NewSite.master" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register Src="~/usercontrols/AR/UndoTermination/UndoStudentSearch.ascx" TagPrefix="FAME" TagName="UndoStudentSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>
    <link href="../css/AR/StudentTermination.css" rel="stylesheet" />
    <link href="../css/AR/UndoTermination.css" rel="stylesheet" />
    <link href="../css/AdvantageValidator.css" rel="stylesheet" />
    <link href="../css/AR/font-awesome.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>
    <script lang="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script src="../Scripts/AR/StudentTermination/UndoTermination.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
    <asp:HiddenField runat="server" ID="hdnCampusId" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <div class="boxContainer full">
        <h3><%=Header.Title  %></h3>
        <div class="undo-browser">
            <div id="tabsForm">
                <br>
                <div id="divTerminationDetail" class="m-detailbox-body">
                    <FAME:UndoStudentSearch runat="server" ID="UndoStudentSearch" />
                </div>
                <div class="buttonDiv">
                    <hr class="top-margin20px">
                    <div class="inLineBlock top-margin20px">
                        <span id="spnUndo" class="disabled">
                            <button type="button" id="btnUndoTermination" disabled  style="color: rgba(255, 255, 255, 0.9) !important;background-color: #1976d2 !important;">
                                Undo Termination
                            </button>
                        </span>
                    </div>
                    <div class="inlineBtn">

                        <span id="spnCancel" class="disabled">
                            <button type="button" id="btnCancelUndoTermination" disabled>
                                Cancel
                            </button>
                        </span>
                    </div>
                    <div class="disabled inLineBlock top-margin7px">
                        <input type="hidden" id="hdnTerminationId">
                        <input type="hidden" id="hdnEnrollmentId">
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
