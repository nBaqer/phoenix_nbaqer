<%--<%@ Reference Page="~/CM/Documents.aspx" %>
<%@ Reference Page="~/CM/DocumentStatus.aspx" %>
<%@ Reference Page="~/AR/Term.aspx" %>--%>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SetupTestRules" CodeFile="SetupTestRules.aspx.vb" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Set Up Dictation/Speed Test Rules for this Course</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../css/systememail.css" type="text/css" rel="stylesheet" />
    <link href="../css/localhost.css" type="text/css" rel="stylesheet" />
    <%--<script language="javascript" src="ViewDocument.js"></script>--%>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        function closewindow() {
            typeof (theChild);
            if (typeof (theChild) != "undefined") {
                if (theChild.open && !theChild.closed) {
                    theChild.close();
                }
            }
        }
    </script>
    <script type="text/javascript">
        function ToggleSecondPopup() {
            $find("<%= txtEffectiveDate.ClientID %>").showPopup();
        }
    </script>
</head>
<body runat="server" leftmargin="0" topmargin="0" id="Body1" name="Body1">
    <form id="Form1" method="post" runat="server">
    <div>
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <asp:ScriptManager ID="scriptmanager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="radStatus">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dlstEffectiveDate" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dlstEffectiveDate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="pnlRHS" LoadingPanelID="AjaxLoadingPanel" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dlstEffectiveDate" LoadingPanelID="AjaxLoadingPanel" />
                        <telerik:AjaxUpdatedControl ControlID="lblUserSelection" LoadingPanelID="AjaxLoadingPanel" />
                        <telerik:AjaxUpdatedControl ControlID="lblMessageStatus" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnNew">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnDelete">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dlstEffectiveDate" />
                        <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadGrid1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="background-color: #164D7F;" nowrap>
                    <h3>
                        <asp:Label ID="lblCaption" runat="server" Text="Set Up Dictation/Speed Test Rules for this Course"></asp:Label></h3>
                </td>
                <td class="topemail">
                    <a class="close" href="#" onclick="top.close()">X Close</a>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <!-- begin
        leftcolumn -->
                <td class="listframe">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="listframetop" nowrap>
                                <asp:Label ID="lblEffectiveDatesText" runat="server" CssClass="labelbold">Effective Date</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="listframebottom">
                                <div class="scrollleft">
                                    <asp:DataList ID="dlstEffectiveDate" runat="server">
                                        <SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
                                        <SelectedItemTemplate>
                                        </SelectedItemTemplate>
                                        <ItemStyle CssClass="NonSelectedItem"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton Text='<%# DataBinder.Eval(Container.DataItem,"EffectiveDate",
        "{0:d}") %>' runat="server" CssClass="NonSelectedItem" CommandArgument='<%# Container.DataItem("EffectiveDate")%>' ID="Linkbutton2"
                                                CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:DataList></div>
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- end leftcolumn -->
                <!-- begin rightcolumn -->
                <td class="leftside">
                    <table cellspacing="0" cellpadding="0" width="9" border="0">
                    </table>
                </td>
                <td class="DetailsFrameTop">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="MenuFrame" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="true">
                                </asp:Button><asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" Enabled="true">
                                </asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                                    CausesValidation="true" Enabled="true"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <div>
                                    <!--begin content table-->
                                    <asp:Panel ID="pnlRHS" runat="server" ScrollBars="Vertical">
                                        <br />
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                            <tr>
                                                <td nowrap align="center">
                                                    <asp:Label ID="lblMessageStatus" CssClass="labelbold" runat="server" MaxLength="128"
                                                        ForeColor="white" Visible="false" Style="border: solid 1px #6788be; background-color: #6788be;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <br />
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblReqDescrip" runat="server" Text="Course" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell">
                                                    <asp:TextBox ID="txtReqDescrip" runat="server" CssClass="textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblEffectiveDate" runat="server" CssClass="label" Text="Effective Date"></asp:Label>
                                                </td>
                                                <td align="left" class="twocolumncontentcell">
                                                    <telerik:RadDatePicker ID="txtEffectiveDate" runat="server" BorderColor="#E0E0E0"
                                                        cellpadding="4" Culture="English (United States)" daynameformat="Short" FocusedDate='<%# Date.Now %>'
                                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Width="300px" >
                                                        <DateInput runat="server" BackColor="#ffff99">
                                                        </DateInput>
                                                    </telerik:RadDatePicker>
                                                    <asp:RequiredFieldValidator ID="rfvEffectiveDate" runat="server" ControlToValidate="txtEffectiveDate"
                                                        ErrorMessage="Effective Date is required" Style="font: normal 11px verdana;"></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td width="20px">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                                                        <telerik:RadPanelBar runat="server" ID="RadPanelBar1" ExpandMode="SingleExpandedItem"
                                                            Width="640px" >
                                                            <Items>
                                                                <telerik:RadPanelItem Enabled="True" Text="Top-Speed Testing Category" runat="server"
                                                                    Font-Bold="true" Selected="true" Expanded="True">
                                                                    <Items>
                                                                        <telerik:RadPanelItem Value="TopSpeedTestingCategory" runat="server" Font-Bold="true"
                                                                            Selected="true">
                                                                            <ItemTemplate>
                                                                                <div class="text">
                                                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                                                                        <tr>
                                                                                            <td class="twocolumnlabelcell">
                                                                                            </td>
                                                                                            <td class="twocolumncontentcell">
                                                                                                <asp:Panel ID="pnlRadGridScroll" runat="server" Height="200px" ScrollBars="Vertical">
                                                                                                    <telerik:RadGrid ID="RadTestDetailsGrid" runat="server" AutoGenerateColumns="False"
                                                                                                        AllowSorting="True"  AllowMultiRowSelection="True" GridLines="None"
                                                                                                        Width="600px">
                                                                                                        <MasterTableView>
                                                                                                            <Columns>
                                                                                                                <telerik:GridTemplateColumn Visible="false">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:TextBox runat="server" ID="txtGrdComponentTypeid_ReqId" Text='<%# Bind("GrdComponentTypeid_ReqId")
        %>'></asp:TextBox>
                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>
                                                                                                                <telerik:GridTemplateColumn HeaderText="Category #">
                                                                                                                    <ItemStyle Width="10%" />
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label runat="server" ID="lblRowNumber" Text='<%# Bind("RowNumber") %>'></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>
                                                                                                                <telerik:GridTemplateColumn HeaderText="Test">
                                                                                                                    <ItemStyle Width="30%" />
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label runat="server" ID="lblDescrip" Text='<%# Bind("Descrip") %>'></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>
                                                                                                                <telerik:GridTemplateColumn Visible="true" HeaderText="Number of Tests" ItemStyle-Width="20%">
                                                                                                                    <ItemStyle Width="20%" />
                                                                                                                    <ItemTemplate>
                                                                                                                        <ew:NumericBox ID="txtNumberofTest" runat="server" CssClass="TextBox" Text='<%#
        Bind("NumberofTests") %>' PositiveNumber="True" RealNumber="False"></ew:NumericBox>
                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>
                                                                                                                <telerik:GridTemplateColumn Visible="true" HeaderText="Minimum number of words per minute"
                                                                                                                    ItemStyle-Width="20%">
                                                                                                                    <ItemStyle Width="20%" />
                                                                                                                    <ItemTemplate>
                                                                                                                        <ew:NumericBox ID="txtMinimumScore" runat="server" CssClass="TextBox" Text='<%# Bind("MinimumScore") %>'></ew:NumericBox>
                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>
                                                                                                            </Columns>
                                                                                                        </MasterTableView>
                                                                                                        <ClientSettings>
                                                                                                            <Scrolling AllowScroll="True" />
                                                                                                            <Selecting AllowRowSelect="True"></Selecting>
                                                                                                        </ClientSettings>
                                                                                                    </telerik:RadGrid>
                                                                                                </asp:Panel>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </telerik:RadPanelItem>
                                                                    </Items>
                                                                </telerik:RadPanelItem>
                                                                <telerik:RadPanelItem Text="Prerequisites" runat="server" Font-Bold="true">
                                                                    <Items>
                                                                        <telerik:RadPanelItem Value="Prerequisites" runat="server">
                                                                            <ItemTemplate>
                                                                                <div class="text">
                                                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                                                                        <tr>
                                                                                            <td class="contentcell" nowrap>
                                                                                                <asp:Label ID="Label1" runat="server" CssClass="Label">Prerequisite Course:</asp:Label>
                                                                                            </td>
                                                                                            <td class="contentcell4" nowrap>
                                                                                                <asp:DropDownList ID="ddlPrereq" runat="server" CssClass="dropdownlist">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="contentcell" nowrap>
                                                                                                <asp:Label ID="lblMinCategory" runat="server" CssClass="Label">Minimum required in each category:</asp:Label>
                                                                                            </td>
                                                                                            <td class="contentcell4" nowrap>
                                                                                                <asp:DropDownList ID="ddlMinCategory" runat="server" CssClass="dropdownlist" Width="100px">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="contentcell" nowrap>
                                                                                                <asp:Label ID="lblMaxCategory" runat="server" CssClass="Label">Maximum
        required in each category:</asp:Label>
                                                                                            </td>
                                                                                            <td class="contentcell4" nowrap>
                                                                                                <asp:DropDownList ID="ddlMaxCategory" runat="server" CssClass="dropdownlist" Width="100px">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="contentcell" nowrap>
                                                                                                <asp:Label ID="lblMinCombination" runat="server" CssClass="Label">Minimum required in each combination:</asp:Label>
                                                                                            </td>
                                                                                            <td class="contentcell4" nowrap>
                                                                                                <asp:DropDownList ID="ddlMinCombination" runat="server" CssClass="dropdownlist" Width="100px">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="contentcell" nowrap>
                                                                                                <asp:Label ID="lblMaxCombination" runat="server" CssClass="Label">Maximum required in each combination:</asp:Label>
                                                                                            </td>
                                                                                            <td class="contentcell4" nowrap>
                                                                                                <asp:DropDownList ID="ddlMaxCombination" runat="server" CssClass="dropdownlist" Width="100px">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="contentcell" nowrap>
                                                                                                <asp:Label ID="lblMentor" runat="server" CssClass="Label">Mentor/Proctored Test
        Requirement:</asp:Label>
                                                                                            </td>
                                                                                            <td class="contentcell4" nowrap>
                                                                                                <asp:DropDownList ID="ddlMentor" runat="server" CssClass="dropdownlist" Width="100px">
                                                                                                    <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                    <asp:ListItem Value="1">Required</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                        <asp:Label ID="lblExample" runat="server" Font-Bold="true" CssClass="labelbold"> Example: The following is the prerequisite requirement for
        course SH220. <br /><br /> A minimum of 1 and maximum of 2 in each category for
        a combination of 5 of the 6 requirements listed under top-speed testing category
        + mentor\proctored test requirement for course SH200. </asp:Label>
                                                                                    </p>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </telerik:RadPanelItem>
                                                                    </Items>
                                                                </telerik:RadPanelItem>
                                                                <telerik:RadPanelItem Enabled="True" Text="Mentor/Proctored Test Requirement" runat="server"
                                                                    Selected="true" Font-Bold="true">
                                                                    <Items>
                                                                        <telerik:RadPanelItem Value="Mentor" runat="server">
                                                                            <ItemTemplate>
                                                                                <div class="text">
                                                                                    <asp:Panel ID="pnlMentor" runat="server" ScrollBars="Vertical">
                                                                                    </asp:Panel>
                                                                                    <asp:Label ID="lblUserSelection" runat="server" CssClass="labelbold" Visible="false">
                                                                                    </asp:Label>
                                                                                    <br />
                                                                                    <asp:Label ID="lblExample" runat="server" Font-Bold="true" CssClass="labelbold">
        Example: The following is the mentor/proctored test requirement for course SH240.
        <br /><br /> (1 mentor/proctored test in 180L or 1 mentor/proctored test in 200JC)
        AND (1 mentor/proctored test in 225QA) </asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </telerik:RadPanelItem>
                                                                    </Items>
                                                                </telerik:RadPanelItem>
                                                            </Items>
                                                            <CollapseAnimation Duration="100" Type="None" />
                                                            <ExpandAnimation Duration="100" Type="None" />
                                                        </telerik:RadPanelBar>
                                                        <br />
                                                        <asp:Button runat="server" ID="backButton" Visible="false" CssClass="qsfButton" Text="Back"
                                                            Style="margin: 10px 0 30px 25px" />
                                                    </telerik:RadAjaxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <telerik:RadAjaxLoadingPanel ID="AjaxLoadingPanel" runat="server" Transparency="30"
                                        MinDisplayTime="25" BackgroundPosition="BottomRight">
                                        <img src="../Images/loading.gif" alt="Loading..." style="border: 0; margin-top: 45px;" />
                                    </telerik:RadAjaxLoadingPanel>
                                </div></td> </tr> </table>
                                <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                <asp:TextBox ID="txtReqId" runat="server" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtOldEffectiveDate" runat="server" Visible="false"></asp:TextBox>
                                <asp:Panel ID="pnlRequiredFieldValidators" runat="server" Visible="False">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Visible="False" ShowMessageBox="True"
                                        ShowSummary="False"></asp:ValidationSummary>
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" Visible="False"></asp:CustomValidator>
                                </asp:Panel>
                                <asp:TextBox ID="txtStudentDocId" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                                    ID="txtDocumentName" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtStudentName"
                                        runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtPath" runat="server"
                                            Visible="False"></asp:TextBox><asp:TextBox ID="txtDocumentType" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                                                ID="txtExtension" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtStudentId"
                                                    runat="server" Visible="False"></asp:TextBox>
                                <!--<table width="60%" align="center">
        <TR> <TD class="contentcell" nowrap><asp:Button id="btnGetDoc" Runat="server" Text="
        Liberty "  Visible="true"></asp:Button></TD> <TD class="contentcell4"
        nowrap><asp:Button id="btnSchoolDocs" Runat="server" Text="SchoolDocs" 
        Visible="true"></asp:Button></TD> </TR> </table>-->
         </td>
    <!-- end rightcolumn -->
    </tr> </table>
    </div>
    
    <div id="footer" runat="server"><asp:Label ID="lblFooterText" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
