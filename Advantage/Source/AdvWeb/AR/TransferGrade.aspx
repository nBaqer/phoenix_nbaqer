﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="TransferGrade.aspx.vb" Inherits="TransferGrade" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

        window.onload = function () {
            var strCook = document.cookie;
            if (strCook.indexOf("!~") != 0) {
                var intS = strCook.indexOf("!~");
                var intE = strCook.indexOf("~!");
                var strPos = strCook.substring(intS + 2, intE);
                document.getElementById("grdWithScroll").scrollTop = strPos;
            }
        }
        function SetDivPosition() {
            var intY = document.getElementById("grdWithScroll").scrollTop;
            document.cookie = "yPos=!~" + intY + "~!";
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <script language="JavaScript" type="text/JavaScript">

        function confirmCallBackFn(arg) {
            if (arg == true) {

                var oButton = document.getElementById('<%=Button1.ClientID%>');
                // alert(oButton);
                oButton.click();
            }
        }
        function alertCallBackFn() {
            var oButton = document.getElementById('<%=Button1.ClientID%>');
            // alert(oButton);
            oButton.click();
        }
    </script>

    <asp:HiddenField ID="txtHint" runat="server" />

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>

            <table width="100%" border="0" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="listframe">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="listframetop">
                                    <asp:Label ID="lblFilter" CssClass="label" runat="server" Text="Filter Not Applicable" />
                                </td>
                            </tr>
                            <tr>
                                <td class="listframebottom">
                                    <div id="grdWithScroll" class="scrollleftnofilter"
                                        onscroll="SetDivPosition()">
                                        <asp:DataList ID="dlstTerm" runat="server">
                                            <SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
                                            <SelectedItemTemplate>
                                            </SelectedItemTemplate>
                                            <ItemStyle CssClass="nonselecteditem"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton Text='<%# Container.DataItem("TermDescrip")%>' runat="server" CssClass="itemstyle"
                                                    CommandArgument='<%# Container.DataItem("TermId")%>' ID="Linkbutton2" CausesValidation="False" />
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3> <asp:Label ID="headerTitle" runat="server"></asp:Label></h3>
                            <!-- begin content table-->


                            <asp:Panel ID="pnlTerm" runat="server" ScrollBars="auto">
                                <table width="100%" align="center">
                                    <tr>
                                        <td >
                                            <asp:DataGrid ID="dgrdTransactionSearch" runat="server" Width="100%" GridLines="Horizontal"
                                                EditItemStyle-Wrap="false" HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False"
                                                BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0" CellPadding="0" DataKeyField="TestCls">
                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="All">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <HeaderTemplate>
                                                            <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkUnschedule',
        document.forms[0].chkAllItems.checked)" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkUnschedule" runat="server" Text='' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Code">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Course">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label2" Text='<%# Container.DataItem("Descrip") %>' CssClass="label"
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Section">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label3" Text='<%# Container.DataItem("ClsSection") %>' CssClass="label"
                                                                runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Grading Complete">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label1" runat="server" CssClass="label" Text='<%# Container.DataItem("GradesComplete") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Transfer Complete">
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblIsGraded" runat="server" CssClass="label" Text='<%# Container.DataItem("Grade") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn Visible="False">
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtIsGraded" runat="server" Visible="False" Text='<%# Container.DataItem("Grade") %>'>
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn Visible="False">
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtClsSectionId" runat="server" Visible="False" Text='<%# Container.DataItem("TestCls") %>'>
                                                            </asp:TextBox>
                                                        </ItemTemplate>

                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                        <td style="width: 10%; min-width: 200px;">
                                            <asp:Button ID="Button1" Text="Transfer Grade" runat="server"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>

                            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Overlay="true"
                                ClientIDMode="static" VisibleStatusbar="false" Style="z-index: 10003;">
                            </telerik:RadWindowManager>

                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

            <asp:TextBox ID="txtTermId" runat="server" Visible="False"></asp:TextBox>
            <asp:Button ID="btncheck" runat="server" Text="Check" Visible="false" />

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>

    <telerik:RadWindowManager ID="TransferGradesWindow" runat="server"
        Behaviors="None" InitialBehaviors="None" Visible="false" DestroyOnClose="True">
        <Windows>
            <telerik:RadWindow ID="DialogWindow" runat="server" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" DestroyOnClose="True"
                ReloadOnShow="true"
                VisibleOnPageLoad="false" Top="0" Left="20"
                Width="550px" Height="450px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
</asp:Content>
