<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Buildings.aspx.vb" Inherits="Buildings" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">
        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
       
    </script>
    <style type="text/css">
        .RadForm_Material.rfdLabel label, .RadForm_Material.rfdLabel .rfdAspLabel {
            vertical-align: bottom !important;
            line-height: 20px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div id="grdwithscroll" class="scrollleftfilters" onscroll="setdivposition()">
                                <asp:DataList ID="dlstbldgs" runat="server" DataKeyField="bldgid" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("bldgid")%>' Text='<%# container.dataitem("bldgdescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                       
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">

                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="scrollright2" style="overflow: auto;">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblbldgcode" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgcode" TabIndex="1" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblstatusid" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlstatusid" TabIndex="2" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblbldgrooms" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgrooms" TabIndex="3" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox>
                                                <asp:CustomValidator ID="customvalidator4" runat="server" OnServerValidate="textvalidate"
                                                    Display="none" ErrorMessage="negative values not allowed" ControlToValidate="txtbldgrooms"></asp:CustomValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblbldgdescrip" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgdescrip" TabIndex="4" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblcampgrpid" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlcampgrpid" TabIndex="5" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblcampusid" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlcampusid" TabIndex="5" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="lblcorpaddress" runat="server" CssClass="label" Font-Bold="true">Address</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="padding-top: 10px">&nbsp;</td>
                                            <td class="contentcell4">
                                                <asp:CheckBox ID="chkforeignzip" TabIndex="6" runat="server" CssClass="checkboxinternational"
                                                    AutoPostBack="true"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lbladdress1" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtaddress1" TabIndex="7" runat="server" CssClass="textbox" MaxLength="128" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lbladdress2" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtaddress2" TabIndex="8" runat="server" CssClass="textbox" MaxLength="128" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblcity" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtcity" TabIndex="9" runat="server" CssClass="textbox" AutoPostBack="true" MaxLength="128" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblstateid" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlstateid" TabIndex="10" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblotherstate" runat="server" CssClass="label" Visible="false"></asp:Label></td>
                                            <td class="contentcell4" nowrap>
                                                <asp:TextBox ID="txtotherstate" runat="server" CssClass="textbox" AutoPostBack="true"
                                                    Enabled="False" Visible="true"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblzip" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <telerik:RadMaskedTextBox ID="txtzip" TabIndex="11" runat="server" CssClass="textbox"
                                                    Mask="#####" DisplayMask="#####" DisplayPromptChar="" AutoPostBack="false"
                                                    Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="label1" runat="server" CssClass="label" Font-Bold="true">Phones</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap; vertical-align: top; padding-top: 10px;">
                                                <asp:Label ID="lblphone" runat="server" CssClass="label"></asp:Label></td>

                                            <td class="contentcell4">
                                                <telerik:RadMaskedTextBox ID="txtphone" TabIndex="13" runat="server" CssClass="textbox"
                                                    Mask="(###)###-####" DisplayMask="(###)###-####" DisplayPromptChar="" Width="200px">
                                                </telerik:RadMaskedTextBox>
                                                <asp:CheckBox ID="chkforeignphone" TabIndex="12" runat="server"
                                                    CssClass="checkboxinternational"
                                                    AutoPostBack="true"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblfax" runat="server" CssClass="label" Style="text-align: left; white-space: nowrap; vertical-align: top;"></asp:Label></td>
                                            <td class="contentcell4">
                                                <telerik:RadMaskedTextBox ID="txtfax" TabIndex="15" runat="server" CssClass="textbox"
                                                    Mask="(###)-###-####" DisplayMask="(###)###-####" DisplayPromptChar="" Width="200px">
                                                </telerik:RadMaskedTextBox>
                                                <asp:CheckBox ID="chkforeignfax" TabIndex="14" runat="server" CssClass="checkboxinternational"
                                                    AutoPostBack="true"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblbldgopen" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgopen" TabIndex="16" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regularexpressionvalidator1" runat="server" ValidationExpression="^((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [ap]m))$|^([01]\d|2[0-3])(:[0-5]\d){1,2}$"
                                                    ControlToValidate="txtbldgopen" ErrorMessage="invalid time input" Display="none">invalid time input</asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblbldgclose" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgclose" TabIndex="17" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="label3" runat="server" CssClass="label" Font-Bold="true">Open on the following days</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding-top: 10px; padding-bottom: 10px">
                                                <table class="contenttable" cellspacing="0" cellpadding="2" width="100%" align="center">
                                                    <tr>
                                                        <td width="15%">
                                                            <asp:CheckBox ID="chksun" TabIndex="18" runat="server" CssClass="label"></asp:CheckBox></td>
                                                        <td width="14%">
                                                            <asp:CheckBox ID="chkmon" TabIndex="19" runat="server" CssClass="label"></asp:CheckBox></td>
                                                        <td width="14%">
                                                            <asp:CheckBox ID="chktue" TabIndex="20" runat="server" CssClass="label"></asp:CheckBox></td>
                                                        <td width="14%">
                                                            <asp:CheckBox ID="chkwed" TabIndex="21" runat="server" CssClass="label"></asp:CheckBox></td>
                                                        <td width="14%">
                                                            <asp:CheckBox ID="chkthu" TabIndex="22" runat="server" CssClass="label"></asp:CheckBox></td>
                                                        <td width="14%">
                                                            <asp:CheckBox ID="chkfri" TabIndex="23" runat="server" CssClass="label"></asp:CheckBox></td>
                                                        <td width="15%">
                                                            <asp:CheckBox ID="chksat" TabIndex="24" runat="server" CssClass="label"></asp:CheckBox></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="label2" runat="server" CssClass="label" Font-Bold="true">Contact Information</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="text-align: left; white-space: nowrap; vertical-align: top; padding-bottom: 10px;">
                                                <asp:Label ID="lblbldgname" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgname" TabIndex="25" runat="server" CssClass="textbox" MaxLength="128" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblbldgtitle" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgtitle" TabIndex="26" runat="server" CssClass="textbox" MaxLength="128" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblbldgemail" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgemail" TabIndex="27" runat="server" CssClass="textbox" MaxLength="128" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblbldgcomments" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtbldgcomments" TabIndex="28" runat="server" CssClass="textbox" Width="200px" 
                                                    MaxLength="128" Visible="true" TextMode="multiline"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">&nbsp;</td>
                                            <td class="contentcell4">
                                                <asp:LinkButton ID="linkbutton2" runat="server" CssClass="label">set up rooms for this building</asp:LinkButton></td>
                                        </tr>
                                        <asp:CheckBox ID="chkisindb" runat="server" Visible="false" Checked="false"></asp:CheckBox>
                                        <asp:TextBox ID="txtmoduser" runat="server" Visible="false">moduser</asp:TextBox>
                                        <asp:TextBox ID="txtmoddate" runat="server" Visible="false">moddate</asp:TextBox>
                                        <asp:TextBox ID="txtrowids" Style="visibility: hidden" CssClass="donothing" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtresourceid" Style="visibility: hidden" CssClass="donothing" runat="server"></asp:TextBox>
                                    </table>
                                    <asp:Panel ID="pnludfheader" runat="server" Visible="false">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6">
                                                    <asp:Label ID="lblsdf" runat="server" Font-Bold="true" CssClass="label">school defined fields</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcell2" colspan="6">
                                                    <asp:Panel ID="pnlsdf" TabIndex="12" runat="server" EnableViewState="false">
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:TextBox ID="txtbldgid" runat="server" MaxLength="128" Visible="true" CssClass="hidden"></asp:TextBox><asp:TextBox
            ID="txtcampusid" runat="server" MaxLength="128" Visible="true" CssClass="hidden"></asp:TextBox>
        <asp:Panel ID="panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="customvalidator"
            Display="none"></asp:CustomValidator>
        <asp:Panel ID="pnlrequiredfieldvalidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="true"
            ShowSummary="false"></asp:ValidationSummary>
    </div>
</asp:Content>






