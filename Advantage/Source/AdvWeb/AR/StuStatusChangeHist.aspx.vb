﻿
Imports System.Data
Imports FAME.Advantage.Common
Imports FAME.Advantage.Domain.Student.Enrollments
Imports Telerik.Web.UI
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports FAME.Advantage.Reporting

Partial Class AR_StuStatusChangeHist
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected campusId As String

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    'Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Protected resourceId As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        ' 05/2/2012 Janet Robinson telerik bug caused dynamic end year to blow up page so it was moved here
        lblFooterText.Text = " Copyright @ FAME 2005 - " + Year(DateTime.Now).ToString + ". All rights reserved."

        'Put user code to initialize the page here
        'Dim objCommon As New CommonUtilities
        '        Dim m_Context As HttpContext
        Dim stdEnrollId As String
        'disable history button
        'Header1.EnableHistoryButton(False)
        Dim advantageUserState As BO.User = AdvantageSession.UserState
        resourceId = HttpContext.Current.Request.Params("resid")
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Not Page.IsPostBack Then
            'btnPrint.Attributes.Add("onclick", "window.print();")
            If Request.QueryString("StuEnrollId") <> "" Then
                stdEnrollId = Request.QueryString("StuEnrollId")
                ViewState("StuEnrollId") = stdEnrollId
            End If
            BindStuStatusChangeHistory()

        Else
        End If
    End Sub
    Protected Sub BindStuStatusChangeHistory()
        Dim facade As New StatusChangeHistoryFacade
        Dim list As IList(Of StatusChangeHistoryObj)
        list = facade.GetStatusChangeHistory(CType(ViewState("StuEnrollId"), String))
        rgStuStatusChangeHist.DataSource = list
        rgStuStatusChangeHist.DataBind()
        ViewState("StuHist") = list
        'Get enrollment name and Student Id
        Dim names As String = StatusChangeHistoryFacade.GetEnrollmentNameAndStudentName(CType(ViewState("StuEnrollId"), String))
        Dim name As String() = names.Split(CType(",", Char()), 2, StringSplitOptions.None)
        lblEnrollmentName.Text = String.Format("Enrollment: {0}", name(0))
        lblStudentName.Text = String.Format("Student:    {0}", name(1))
    End Sub

    Protected Sub rgStuStatusChangeHist_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles rgStuStatusChangeHist.NeedDataSource
        If Not e.IsFromDetailTable Then
            If Not ViewState("StuHist") Is Nothing Then
                Dim ds As IList(Of StatusChangeHistoryObj) = CType(ViewState("StuHist"), IList(Of StatusChangeHistoryObj))
                rgStuStatusChangeHist.MasterTableView.DataSource = ds
            End If
        End If
    End Sub
    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        'create temp table
        Dim facade As New StatusChangeHistoryFacade
        facade.CreateTempStatusChangeHistoryTable()

        Dim getReportAsBytes As [Byte]()
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim dtPrint = New DataTable
        For Each col As GridColumn In rgStuStatusChangeHist.Columns
            Dim colString As New DataColumn(col.UniqueName)
            dtPrint.Columns.Add(colString)
        Next
        Dim pageCount As Integer = rgStuStatusChangeHist.PageCount
        Dim pageCurrentIndex As Integer = rgStuStatusChangeHist.CurrentPageIndex
        Dim i As Integer = 0
        While i < pageCount
            rgStuStatusChangeHist.CurrentPageIndex = i
            rgStuStatusChangeHist.Rebind()

            For Each row As GridDataItem In rgStuStatusChangeHist.Items
                Dim dr As DataRow = dtPrint.NewRow
                For Each col As GridColumn In rgStuStatusChangeHist.Columns
                    If row(col.UniqueName).Text = "&nbsp;" Then
                        dr(col.UniqueName) = ""
                    Else
                        dr(col.UniqueName) = row(col.UniqueName).Text
                    End If
                Next
                'dtPrint.Rows.Add(dr) 'no need to put rows here
                'to do for each in the table
                facade.InsertInTempStatusChangeHistory(CType(ViewState("StuEnrollId"), String), CType(dr("DateOfChange"), DateTime), CType(dr("Status"), String), CType(dr("Details"), String), CType(dr("RequestedBy"), String), CType(dr("ModUser"), String), CType(dr("CaseNumber"), String), CType(dr("ModDate"), DateTime))
            Next
            i += 1
        End While

        'rebind to the previous page
        rgStuStatusChangeHist.CurrentPageIndex = pageCurrentIndex
        rgStuStatusChangeHist.Rebind()
        
        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder")
        Dim getReportParameterObj = New Info.StatusChangeHistorySelectionValues
        getReportParameterObj.StudentEntrollmentId = CType(ViewState("StuEnrollId"), String)
        getReportAsBytes = (New Logic.StatusChangeHistoryReportGenerator).RenderReport("pdf", getReportParameterObj, strReportPath)
        ExportReport("pdf", getReportAsBytes)
    End Sub

    Private Sub ExportReport(ByVal exportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case exportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'Dim script As String = String.Format("window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        Dim script As String = String.Format("window.open('../SY/DisplaySSRSReport.aspx','_self','resizable=yes,scrollbars=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub
End Class
