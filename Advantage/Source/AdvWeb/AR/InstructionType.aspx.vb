Imports FAME.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Imports System.Collections
Imports System.Data

Partial Class InstructionType
    Inherits BasePage

    ' 08/02/2011 JRobinson Clock Hour Project - maintenance page for Instruction Types

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected resourceId As String
    Protected userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities



        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        Try

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            campusId = AdvantageSession.UserState.CampusId.ToString
            userId = AdvantageSession.UserState.UserId.ToString

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If


            If Not IsPostBack Then
                'objCommon.PageSetup(Form1, "NEW")
                'objCommon.PopulatePage(Form1)
                'Disable the new and delete buttons
                'objCommon.SetBtnState(Form1, "NEW", pObj)
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                '   build dropdownlists
                BuildDropDownLists()

                '   bind datalist
                BindDataList()

                '   bind an empty new InstructionTypeInfo
                BindInstructionTypeData(New InstructionTypeInfo)

                'Header1.EnableHistoryButton(False)
                InitButtonsForLoad()
            Else
                'objCommon.PageSetup(Form1, "EDIT")
                InitButtonsForEdit()
            End If

            'txtInstructionTypeCode.BackColor = Color.FromName("#ffff99")
            'txtInstructionTypeDescrip.BackColor = Color.FromName("#ffff99")

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind Instruction Type datalist
        dlstInstructionTypes.DataSource = New DataView((New InstructionTypeFacade).GetAllInstructionTypeIds().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstInstructionTypes.DataBind()

    End Sub

    Private Sub BindInstructionTypeData(ByVal InstructionTypeData As InstructionTypeInfo)
        With InstructionTypeData
            chkIsInDB.Checked = .IsInDB
            txtInstructionTypeCode.Text = .InstructionTypeCode
            txtOrigCode.Text = .InstructionTypeCode
            txtInstructionTypeId.Text = .InstructionTypeId
            txtOrigInstructionTypeId.Text = .OrigInstructionTypeId
            If Not (InstructionTypeData.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = InstructionTypeData.StatusId
            txtInstructionTypeDescrip.Text = .InstructionTypeDescrip
            txtOrigDesc.Text = .InstructionTypeDescrip
            txtCampGrpId.Text = .CampGrpId
            chkIsDefault.Checked = .IsDefault
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim facInstructionTypes As New InstructionTypeFacade

        Try

            If Not Page.IsValid Then Exit Sub

            Dim result As String


            With New InstructionTypeFacade
                result = .CheckInstructionTypeDup(txtOrigInstructionTypeId.Text, txtInstructionTypeCode.Text, txtInstructionTypeDescrip.Text)
            End With
            If result <> "" Then
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error1", result, "Save Error")
                Exit Sub
            End If

            'do not allow user to change code or desc of Theory record
            If txtOrigInstructionTypeId.Text <> "00000000-0000-0000-0000-000000000000" Then
                If (txtOrigCode.Text = "TH01" AndAlso txtInstructionTypeCode.Text <> "TH01") _
                OrElse (txtOrigDesc.Text = "Theory" AndAlso txtInstructionTypeDescrip.Text <> "Theory") Then
                    DisplayRADAlert(CallbackType.Postback, "Error1", "Code or Description cannot be changed for Theory", "Save Error")
                    Exit Sub
                End If
            End If

            With New InstructionTypeFacade
                '   update Instruction Type Info 
                result = .UpdateInstructionTypeInfo(BuildInstructionTypeInfo(txtInstructionTypeId.Text), AdvantageSession.UserState.UserName)
            End With

            '   bind the datalist
            BindDataList()

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstInstructionTypes, txtInstructionTypeId.Text, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstInstructionTypes, txtInstructionTypeId.Text)
            If Not result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error2", result, "Save Error")
                Exit Sub
            Else

                'if this record was chosen as the default then remove all other default setting from the db for this school
                If chkIsDefault.Checked = True Then
                    Dim ret As String = facInstructionTypes.RemoveDefaultFromInstructionTypes(txtInstructionTypeId.Text)
                    If ret <> "" Then
                        'DisplayErrorMessage(ret)
                        DisplayRADAlert(CallbackType.Postback, "Error3", result, "Save Error")
                    End If
                End If
                '   get the InstructionTypeId from the backend and display it
                GetInstructionTypeId(txtInstructionTypeId.Text)
            End If

            '   if there are no errors bind a new entity and init buttons
            If Page.IsValid Then

                '   set the property IsInDB to true in order to avoid an error if the user
                '   hits "save" twice after adding a record.
                chkIsInDB.Checked = True

                '   initialize buttons
                'InitButtonsForLoad()
                InitButtonsForEdit()


            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Function BuildInstructionTypeInfo(ByVal InstructionTypeId As String) As InstructionTypeInfo
        'instantiate class
        Dim InstructionTypeInfo As New InstructionTypeInfo

        With InstructionTypeInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get InstructionTypeId
            .InstructionTypeId = txtInstructionTypeId.Text.Trim

            'get Orig Id
            .OrigInstructionTypeId = txtOrigInstructionTypeId.Text.Trim

            'get code
            .InstructionTypeCode = txtInstructionTypeCode.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get Clock Id name
            .InstructionTypeDescrip = txtInstructionTypeDescrip.Text.Trim

            'get Campus Group
            .CampGrpId = txtCampGrpId.Text.Trim

            'get IsDefault
            .IsDefault = chkIsDefault.Checked

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return InstructionTypeInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click


        Try

            ddlStatusId.SelectedValue = " "
            '   bind an empty new InstructionTypeInfo Info
            BindInstructionTypeData(New InstructionTypeInfo)

            txtInstructionTypeId.Text = System.Guid.NewGuid.ToString
            txtOrigCode.Text = String.Empty
            txtOrigDesc.Text = String.Empty

            'Reset Style in the Datalist
            'CommonWebUtilities.SetStyleToSelectedItem(dlstInstructionTypes, Guid.Empty.ToString, ViewState, Header1)

            'initialize buttons
            InitButtonsForLoad()
            CommonWebUtilities.RestoreItemValues(dlstInstructionTypes, Guid.Empty.ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Try

            If Not (txtInstructionTypeId.Text = Guid.Empty.ToString) Then
                'update InstructionTypeInfo Info 
                If txtInstructionTypeDescrip.Text = "Theory" Then
                    'DisplayErrorMessage("You cannot delete the Theory Instruction Type")
                    DisplayRADAlert(CallbackType.Postback, "Error4", "You cannot delete the Theory Instruction Type", "Delete Error")
                    Exit Sub
                End If
                If chkIsDefault.Checked = True Then
                    'DisplayErrorMessage("You cannot delete the default Instruction Type unless you make a different one the default")
                    DisplayRADAlert(CallbackType.Postback, "Error5", "You cannot delete the default Instruction Type unless you make a different one the default", "Delete Error")
                    Exit Sub
                End If
                Dim result As String = (New InstructionTypeFacade).DeleteInstructionTypeInfo(txtInstructionTypeId.Text, Date.Parse(txtModDate.Text))
                If Not result = "" Then
                    '   Display Error Message
                    'DisplayErrorMessage(result)
                    DisplayRADAlert(CallbackType.Postback, "Error6", "Record cannot be deleted because it is being referenced by another page.", "Delete Error")
                Else
                    '   bind datalist
                    BindDataList()

                    '   bind an empty new InstructionTypeInfo
                    BindInstructionTypeData(New InstructionTypeInfo)

                    '   initialize buttons
                    InitButtonsForLoad()

                    txtOrigCode.Text = String.Empty
                    txtOrigDesc.Text = String.Empty

                End If
                CommonWebUtilities.RestoreItemValues(dlstInstructionTypes, Guid.Empty.ToString)
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstInstructionTypes_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstInstructionTypes.ItemCommand

        Try

            '   get the InstructionTypeId from the backend and display it
            GetInstructionTypeId(e.CommandArgument)

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstInstructionTypes, e.CommandArgument, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstInstructionTypes, e.CommandArgument)
            '   initialize buttons
            InitButtonsForEdit()

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstInstructionTypes_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstInstructionTypes_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub
    Private Sub GetInstructionTypeId(ByVal InstructionTypeId As String)

        '   bind Instruction Type Id properties
        BindInstructionTypeData((New InstructionTypeFacade).GetInstructionTypeInfo(InstructionTypeId))

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        InitButtonsForLoad()
        '   bind datalist
        BindDataList()
        'Header1.EnableHistoryButton(False)
        txtInstructionTypeCode.Text = String.Empty
        txtInstructionTypeDescrip.Text = String.Empty
        txtInstructionTypeId.Text = String.Empty
        txtOrigInstructionTypeId.Text = String.Empty
        txtCampGrpId.Text = String.Empty
        ddlStatusId.SelectedValue = " "
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

    Private Sub BuildDropDownLists()
        BuildStatusDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem(" ", " "))
            .SelectedIndex = 0
        End With


    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            If chkIsDefault.Checked = False And txtInstructionTypeDescrip.Text <> "Theory" Then
                'Set the Delete Button so it prompts the user for confirmation when clicked
                btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
            Else
                btnDelete.Attributes.Remove("onclick")
            End If
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub


End Class
