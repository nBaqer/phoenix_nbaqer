<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DisplayClassSections.aspx.vb" Inherits="AR_DisplayClassSections" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Advantage Error Message</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/localhost.css" type="text/css" rel="stylesheet">
		<LINK href="../css/systememail.css" type="text/css" rel="stylesheet">
		<LINK href="../css/BulletedList.css" type="text/css" rel="stylesheet">
		<style>
		    .headerImage {
		        background: #164D7F url(../images/advantage_popup_header.jpg) no-repeat;
		        width: 70%;
		        height: 48px;
		        vertical-align: bottom;
		        }
		    h1 {
		        font: bold 16px verdana;
		        padding: 0;
		        margin: 0 0 3px 170px;
		        color: #fff;
		        vertical-align: bottom;
		     }
		 </style>
	</HEAD>
	<body runat="server" ID="Body1" NAME="Body1">
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td class="headerImage" nowrap><h1>Classes Exist For <%= Trim(Request.QueryString("OldCampusGroup")) %></h1></td>
					<td class="topemail" style="width: 40%"><A class="close" onclick="top.close()" href=#>X Close</A></td>
				</tr>
			</table>
			<!--begin right column-->
			<table class="maincontenttable" id="Table5" cellSpacing="0" cellPadding="0" width="100%">
				<TBODY>
					<tr>
						<td class="detailsFrame"><div class="scrollwholedocs">
								<!-- begin content table-->
								<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<TR>
										<TD style="padding-top: 20px">
										          <asp:Label ID="lblStatus" runat="server" CssClass="Label" BackColor="#f0f4f8"></asp:Label>
										          <p></p>
										          <asp:DataGrid ID="dgrdTransactionSearch" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                                    BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true"
                                                    EditItemStyle-Wrap="false" GridLines="Horizontal" Width="100%" runat="server">
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                    <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
                                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Class Section">
                                                            <HeaderStyle CssClass="DataGridHeaderStyle" Width="20%"></HeaderStyle>
                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblClsSection" Text='<%# Container.DataItem("ClsSection") %>' CssClass="Label"
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Course">
                                                            <HeaderStyle CssClass="DataGridHeaderStyle" Width="20%"></HeaderStyle>
                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCourse" Text='<%# Container.DataItem("Course") %>' CssClass="Label"
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Campus">
                                                            <HeaderStyle CssClass="DataGridHeaderStyle" Width="20%"></HeaderStyle>
                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCampus" Text='<%# Container.DataItem("Campus") %>' CssClass="Label"
                                                                    runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
										
										</TD>
									</TR>
								</TABLE>
								<!-- end content table -->
							</div>
						</td>
					</tr>
				</TBODY>
			</table>
			<div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
		</form>
	</body>
</HTML>

