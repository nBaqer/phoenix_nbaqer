<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master"
    Inherits="TerminateStudent" CodeFile="TerminateStudent.aspx.vb" EnableEventValidation="false" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Terminate Student</title>

</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" Orientation="HorizontalTop">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop" style="height: 61px">
                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" Enabled="false"
                                        CausesValidation="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td class="detailsframe">
                                    <FAME:StudentSearch ID="StudentSearch" runat="server" OnTransferToParent="TransferToParent" />
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <asp:Panel ID="pnlRHS" runat="server">

                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="55%" align="center">
                                                <tr>
                                                    <td align="center" colspan="2" style="padding: 6px 0 6px 0">
                                                        <asp:DataGrid ID="dgdDropCourse" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                                            <EditItemStyle Wrap="False"></EditItemStyle>
                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                            <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="Code">
                                                                    <HeaderStyle CssClass="DataGridHeader" Width="20%"></HeaderStyle>
                                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Code" runat="server" CssClass="Label" Text='<%# Container.DataItem("Code")  %>' />
                                                                        <asp:TextBox ID="txtClsSectionId" runat="server" CssClass="ardatalistcontent" Visible="False" Text='<%# Container.DataItem("ClsSectionId") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Course Description">
                                                                    <HeaderStyle CssClass="DataGridHeader" Width="50%"></HeaderStyle>
                                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Descrip" runat="server" CssClass="Label" Text='<%# Container.DataItem("Descrip")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Unschedule">
                                                                    <HeaderStyle CssClass="DataGridHeader" Width="15%"></HeaderStyle>
                                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                    <HeaderTemplate>

                                                                        <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkDrop',
        document.forms[0].chkAllItems.checked)" /><asp:Label ID="Label1" Width="12%" runat="server" CssClass="Label" Font-Bold="True" Text='Drop' />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkDrop" runat="server" CssClass="CheckBox" />

                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Grade">
                                                                    <HeaderStyle CssClass="DataGridHeader" Width="15%"></HeaderStyle>
                                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlGrade" runat="server" CssClass="DropDownList"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--code addedby Priyanka on date 30th April 2009--%>
                                                                <asp:TemplateColumn HeaderText="Date Determined">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="Textboxdate" Width="75px"></asp:TextBox><asp:HyperLink
                                                                            ID="date" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="DataGridHeader" Width="25%"></HeaderStyle>
                                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid></td>
                                                </tr>
                                            </table>
                                            <asp:Panel runat="server" ID="MainPanel2">
                                                <%--------------------------New Markup-----------------------------------------------------%>

                                                <table class="contenttable" style="width: 100px; ">
                                                    <tr>
                                                        <td class="contentcell" style="text-align: left;">
                                                            <asp:Label ID="lblLDA1" runat="server" CssClass="label">Last Date Attended<span  
                                                                            style="font-size: xx-small; color: #b71c1c; font-family: v, Verdana">*</span></asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <telerik:RadDatePicker runat="server" ID="txtLDA" DatePopupButton-ToolTip="" Calendar-ShowRowHeaders="false" AutoPostBack="false" Width="325px">
                                                                <DateInput ID="DateInput1" runat="server" ForeColor="Black"></DateInput>
                                                                <Calendar ID="Calendar1" runat="server">
                                                                    <SpecialDays>
                                                                        <telerik:RadCalendarDay Repeatable="Today">
                                                                            <ItemStyle CssClass="rcToday" />
                                                                        </telerik:RadCalendarDay>
                                                                    </SpecialDays>
                                                                </Calendar>
                                                            </telerik:RadDatePicker>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtLDA"
                                                                runat="server"
                                                                ErrorMessage="Last Date Attended is required" ValidationGroup="Validation"
                                                                InitialValue="" Display="None">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblWithdrawalDate1" runat="server" CssClass="label">Withdrawal Date<span  
                                                                            style="font-size: xx-small; color: #b71c1c; font-family: v, Verdana">*</span></asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <telerik:RadDatePicker runat="server" ID="txtDateDetermined" DatePopupButton-ToolTip="" Calendar-ShowRowHeaders="false" AutoPostBack="false" MinDate="1/1/1945" Width="325px">
                                                                <DateInput ID="DateInput2" runat="server" ForeColor="Black"></DateInput>
                                                                <Calendar ID="Calendar2" runat="server">
                                                                    <SpecialDays>
                                                                        <telerik:RadCalendarDay Repeatable="Today">
                                                                            <ItemStyle CssClass="rcToday" />
                                                                        </telerik:RadCalendarDay>
                                                                    </SpecialDays>
                                                                </Calendar>
                                                            </telerik:RadDatePicker>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtDateDetermined"
                                                                runat="server"
                                                                ErrorMessage="Withdrawal Date is required" ValidationGroup="Validation"
                                                                InitialValue="" Display="None">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblStatus" runat="server" CssClass="label">Status<span  
                                                                            style="font-size: xx-small; color: #b71c1c; font-family: v, Verdana">*</span></asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <asp:DropDownList ID="ddlStatusCodeId" runat="server" AutoPostBack="False" CssClass="dropdownlist" Width="325px">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="reqCategory" ControlToValidate="ddlStatusCodeId"
                                                                runat="server"
                                                                ErrorMessage="Status is required" ValidationGroup="Validation"
                                                                InitialValue="Select" Display="None">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblDropReason" runat="server" CssClass="label">Drop Reason<span  
                                                                            style="font-size: xx-small; color: #b71c1c; font-family: v, Verdana">*</span></asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <asp:DropDownList ID="ddlDropReasonId" runat="server" AutoPostBack="False" CssClass="dropdownlist" Width="325px">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlDropReasonId"
                                                                runat="server"
                                                                ErrorMessage="Drop Reason is required" ValidationGroup="Validation"
                                                                InitialValue="Select" Display="None">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td class="contentcell4">
                                                            <asp:Button ID="btnTerminate1" runat="server" Enabled="true"
                                                                Text="Terminate Student" ValidationGroup="Validation"></asp:Button>
                                                        </td>
                                                    </tr>

                                                    <tr runat="server" id="trTerminate" visible="false">
                                                        <td colspan="2">

                                                            <table id="MainContainer" runat="server" class="MainContainer" cellpadding="5">
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <asp:LinkButton runat="server" ID="lnkDropCourses" Text="Click here to drop student courses" Visible="false" Enabled="false"></asp:LinkButton>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                            <table>
                                                                <tr>
                                                                    <td style="margin-left: 100px;">
                                                                        <telerik:RadGrid runat="server" ID="rgDropCourse" Width="500px" AutoGenerateColumns="false" Visible="false">
                                                                            <EditItemStyle Wrap="False"></EditItemStyle>
                                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                                            <MasterTableView>
                                                                                <Columns>
                                                                                    <telerik:GridTemplateColumn HeaderText="Code">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="20%"></HeaderStyle>
                                                                                        <ItemStyle CssClass="DataGridItemStyle" Width="20%"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Code" runat="server" CssClass="Label" Text='<%# Container.DataItem("Code")  %>' Width="110px" />
                                                                                            <telerik:RadTextBox runat="server" ID="rtbClsSectionId" CssClass="ardatalistcontent" Visible="False" Text='<%# Container.DataItem("ClsSectionId") %>' />
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>

                                                                                    <telerik:GridTemplateColumn HeaderText="Course Description">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="50%"></HeaderStyle>
                                                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Descrip" runat="server" CssClass="Label" Text='<%# Container.DataItem("Descrip")%>' Width="300px" />
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>

                                                                                    <telerik:GridTemplateColumn HeaderText="Unschedule">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="10%"></HeaderStyle>
                                                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                                        <HeaderTemplate>
                                                                                            <input id="chkAllItems1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkDrop', document.forms[0].chkAllItems.checked)" />
                                                                                            <asp:Label ID="Label1" runat="server" CssClass="Label" Font-Bold="True" Text='Drop' Width="7%" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkDrop" runat="server" CssClass="CheckBox" Width="10px" />
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>

                                                                                    <telerik:GridTemplateColumn HeaderText="Grade">
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="15%"></HeaderStyle>
                                                                                        <ItemStyle CssClass="DataGridItemStyle" Width="15%"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <telerik:RadComboBox runat="server" ID="rcbGrade" AutoPostBack="false" Width="100px" BackColor="White" ForeColor="Black" BorderWidth="0"></telerik:RadComboBox>
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>

                                                                                    <telerik:GridTemplateColumn HeaderText="Date Determined">
                                                                                        <ItemTemplate>
                                                                                            <telerik:RadDatePicker runat="server" ID="rdpDate" CssClass="Textboxdate" Width="100px" BorderWidth="0" Calendar-ShowRowHeaders="false">
                                                                                                <Calendar ID="Calendar2" runat="server">
                                                                                                    <SpecialDays>
                                                                                                        <telerik:RadCalendarDay Repeatable="Today">
                                                                                                            <ItemStyle CssClass="rcToday" />
                                                                                                        </telerik:RadCalendarDay>
                                                                                                    </SpecialDays>
                                                                                                </Calendar>
                                                                                            </telerik:RadDatePicker>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle CssClass="DataGridHeader" Width="15px"></HeaderStyle>
                                                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                                    </telerik:GridTemplateColumn>
                                                                                </Columns>
                                                                            </MasterTableView>
                                                                        </telerik:RadGrid>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%">


                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label runat="server" ID="lblMessages" Text="You must enter all required fields<br>" Visible="false" CssClass="Label"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                            </table>

                                                        </td>
                                                    </tr>

                                                </table>
                                            </asp:Panel>

                                            <asp:TextBox ID="Textbox1" runat="server" CssClass="TextBox" Width="0px" Visible="false">StuEnrollment</asp:TextBox>
                                            <asp:TextBox ID="Textbox2" runat="server" CssClass="TextBox" Width="0px" Visible="false">StuEnrollmentId</asp:TextBox>
                                            <asp:TextBox ID="Textbox3" runat="server" CssClass="TextBox" Width="0px" Visible="false">AcademicYear</asp:TextBox>
                                            <asp:TextBox ID="Textbox4" runat="server" CssClass="TextBox" Width="0px" Visible="false">AcademicYearId</asp:TextBox>
                                            <asp:TextBox ID="Textbox5" runat="server" CssClass="TextBox" Width="0px" Visible="false">Term</asp:TextBox>
                                            <asp:TextBox ID="Textbox6" runat="server" CssClass="TextBox" Width="0px" Visible="false">TermId</asp:TextBox>
                                        </asp:Panel>
                                        <!--end table content-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <asp:TextBox ID="txtStuEnrollment" TabIndex="2" runat="server"
                Visible="false" Width="0px">StuEnrollment</asp:TextBox><asp:TextBox ID="txtStuEnrollmentId"
                    runat="server" Visible="false" CssClass="TextBox" Width="0px">StuEnrollmentId</asp:TextBox>
            <asp:TextBox ID="txtAcademicYear" TabIndex="8" runat="server"
                Visible="false" Width="0px">AcademicYear</asp:TextBox><asp:TextBox ID="txtAcademicYearId"
                    runat="server" Visible="false" CssClass="TextBox" Width="0px">AcademicYearId</asp:TextBox>
            <asp:TextBox ID="txtTerm" TabIndex="9" runat="server" Width="0px"
                Visible="false">Term</asp:TextBox><asp:TextBox ID="txtTermId" runat="server" CssClass="TextBox"
                    Width="0px" Visible="false">TermId</asp:TextBox>
            <asp:Button ID="btnSubmit" runat="server" Enabled="False" Visible="false" CausesValidation="false"></asp:Button>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" ValidationGroup="Validation" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
