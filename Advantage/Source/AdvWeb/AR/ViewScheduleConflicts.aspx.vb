﻿
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Collections.Generic
Partial Class AR_ViewScheduleConflicts
    Inherits System.Web.UI.Page
    Dim student As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Comment added by Balaji on Oct 17 2011
        'This page will be called by the following pages in Advantage
        '''''''''''''' Register Students Page - Events(Add, Add All button click)
        '''''''''''''' register students manually - Events (btnSave click)
        '''''''''''''' Schedule Tab - Events (Add click)


        If Not Page.IsPostBack Then
            Dim dsScheduleConflicts As New DataSet
            Dim stuEnrollId As String = Request.QueryString("StuEnrollid")
            Dim Classes As String = Request.QueryString("Classes")
            Dim campusId As String = Request.QueryString("CampusId")
            Dim boolconflictinsideterm As String = "false"
            Dim ConflictTerms As String = Request.QueryString("TermId")
            Dim conflictsinsideclass As Boolean = False
            Dim boolConflictClassIgnoreRoom As String = "false"
            Dim boolRegStdManPage As String = "false"
            Try
                boolconflictinsideterm = Request.QueryString("boolconflictinsideterm")
                If boolconflictinsideterm Is Nothing Then
                    boolconflictinsideterm = "false"
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                boolconflictinsideterm = "false"
            End Try
            Try
                boolConflictClassIgnoreRoom = Request.QueryString("boolConflictClassIgnoreRoom")
                If boolConflictClassIgnoreRoom Is Nothing Then
                    boolConflictClassIgnoreRoom = "false"
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                boolConflictClassIgnoreRoom = "false"
            End Try
            Try
                conflictsinsideclass = Request.QueryString("boolconflictinsidesameclass")

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                conflictsinsideclass = False
            End Try
            Try
                boolRegStdManPage = Request.QueryString("boolRegStdManPage")
                If boolRegStdManPage Is Nothing Then
                    boolRegStdManPage = "false"
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                boolRegStdManPage = "false"
            End Try

            If boolconflictinsideterm Is Nothing Then
                boolconflictinsideterm = "false"
            End If

            student = Request.QueryString("Student")

            If Not stuEnrollId.Trim = "" Then
                If boolconflictinsideterm.ToLower = "true" Then
                    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsForClassesInsideTerm(Request.QueryString("ClsSectionId"), ConflictTerms, campusId)
                ElseIf boolConflictClassIgnoreRoom.ToLower = "true" Then
                    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsForClassesInsideTermIgnoreRooms(Request.QueryString("ClsSectionId"), ConflictTerms, campusId)
                Else
                    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(Request.QueryString("ClsSectionId"), stuEnrollId)
                End If

            Else
                'Dim strURL As String = "ViewScheduleConflicts.aspx?resid=909&mod=AR" + "&ClsSectionId=" + ClsSectId.ToString + "&StuEnrollId=" + stuEnrollId + "&Student=" + StudentID + "&buttonState=" + buttonState + "&Classes=" + Classes _
                '               + "&MeetingStartDate=" + MeetingStartDate + "&MeetingEndDate=" + MeetingEndDate + "&RoomId=" + RoomId + "&PeriodId=" + PeriodId _
                '               + "&TermId=" + ddltermId.selectedValue.toString + "&CourseId=" + ddlReqId.SelectedValue.ToString + "&InstructorId=" + ddlInstructorId.SelectedValue.ToString
                Dim TermId, CourseId, InstructorId, ClsSectionId As String
                Dim MeetingStartDate, MeetingEndDate, strRoomId, PeriodId As String
                Dim isSourceClassSectionPage As Boolean = False
                Dim isSourceCopyClassSectionPage As Boolean = False
                Dim DayId, StartTimeId, EndTimeId As String
                Dim TargetTermId As String = ""
                Dim sourceTermId As String = ""

                Try
                    isSourceClassSectionPage = Request.QueryString("IsSourceClassSectionPage")
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    isSourceClassSectionPage = False
                End Try

                Try
                    isSourceCopyClassSectionPage = Request.QueryString("isSourceCopyClassSectionPage")
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    isSourceCopyClassSectionPage = False
                End Try

                Try
                    TargetTermId = Request.QueryString("TargetTermId")
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    TargetTermId = ""
                End Try

                Try
                    sourceTermId = Request.QueryString("SourceTermId")
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    sourceTermId = ""
                End Try

                ClsSectionId = Request.QueryString("ClsSectionId")
                TermId = Request.QueryString("TermId")
                CourseId = Request.QueryString("CourseId")
                InstructorId = Request.QueryString("InstructorId")
                Classes = Request.QueryString("Classes")
                MeetingStartDate = Request.QueryString("MeetingStartDate")
                MeetingEndDate = Request.QueryString("MeetingEndDate")
                strRoomId = Request.QueryString("RoomId")
                PeriodId = Request.QueryString("PeriodId")

                If isSourceClassSectionPage = False AndAlso isSourceCopyClassSectionPage = False Then
                    If conflictsinsideclass = False Then
                        dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArray(ClsSectionId, MeetingStartDate, MeetingEndDate, _
                                                                                                     strRoomId, InstructorId, TermId, CourseId, PeriodId, campusId)
                    Else
                        dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArrayWithInSameClass(ClsSectionId, MeetingStartDate, MeetingEndDate, _
                                                                                                     strRoomId, InstructorId, TermId, CourseId, PeriodId, campusId)
                    End If
                Else
                    DayId = Request.QueryString("DayId")
                    StartTimeId = Request.QueryString("StartTimeId")
                    EndTimeId = Request.QueryString("EndTimeId")

                    If isSourceCopyClassSectionPage = True Then
                        dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileCopyingToAnotherTerm(ClsSectionId, sourceTermId, TargetTermId, campusId)

                    Else
                        dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesNoPeriods(ClsSectionId, MeetingStartDate, MeetingEndDate, _
                                                                                                      strRoomId, InstructorId, TermId, CourseId, DayId, StartTimeId, EndTimeId, campusId)
                    End If
                End If
            End If

            Dim lstData As ListItemCollection = New ListItemCollection()
            Dim classSchedules As New List(Of String)
            Dim table As DataTable
            Dim strPreviousCourse As String = ""
            Dim strPreviousClass As String = ""
            Dim strPreviousPeriod As String = ""
            table = CreateNewTable()
            If Not dsScheduleConflicts Is Nothing Then
                For Each dr As DataRow In dsScheduleConflicts.Tables(0).Rows
                    Dim row As DataRow
                    Dim expression As String = "CourseDescrip='" + dr("CourseDescrip").ToString + "'" + " and ClsSection='" + dr("ClsSection").ToString + "'" + " and ClassPeriod='" + dr("ClassPeriod").ToString + "'"

                    row = table.NewRow()
                    ' Then add the new row to the collection.
                    row("CourseDescrip") = dr("CourseDescrip").ToString
                    row("ClsSection") = dr("ClsSection").ToString
                    row("ClassPeriod") = dr("ClassPeriod").ToString

                    Dim matchRows() As DataRow
                    matchRows = dsScheduleConflicts.Tables(0).Select(expression)

                    Dim i As Integer
                    Dim strMeetingSchedule As String = ""
                    For i = 0 To matchRows.GetUpperBound(0)
                        strMeetingSchedule &= matchRows(i)("MeetingSchedule").ToString
                        strMeetingSchedule &= vbCrLf
                        'classSchedules.Add(matchRows(i)("MeetingSchedule").ToString)
                    Next
                    'row("MeetingSchedule") = classSchedules.ToList().ToString
                    row("MeetingSchedule") = strMeetingSchedule

                    'If row was already added to datatable ignore duplicates
                    If (Not dr("CourseId").Trim = strPreviousCourse And _
                         Not dr("ClsSectionId").ToString.Trim = strPreviousClass) Then
                        'If (Not dr("CourseId").Trim = strPreviousCourse And _
                        '       Not dr("ClsSectionId").ToString.Trim = strPreviousClass And _
                        '        Not row("ClassPeriod").Trim = strPreviousPeriod Then
                        'Dim MeetingInfo As New ClsSectMeetingInfo
                        'With MeetingInfo
                        '    .CourseDescrip = row("CourseDescrip").ToString
                        '    .ClsSection = row("ClsSection").ToString.Trim
                        '    .MeetingSchedule = classSchedules
                        'End With
                        'table.Rows.Add(row("CourseDescrip"), row("ClsSection"), row("ClassPeriod"), row("MeetingSchedule"))
                        table.Rows.Add(row)
                        'table.Rows.Add(MeetingInfo.CourseDescrip, MeetingInfo.ClsSection, "Period", MeetingInfo.MeetingSchedule.ToList.ToString)
                    End If

                    'For elimiating duplicate rows
                    strPreviousCourse = dr("CourseId").ToString
                    strPreviousClass = dr("ClsSectionId").ToString
                    strPreviousPeriod = dr("ClassPeriod").ToString

                    'Clear list collection
                    classSchedules.Clear()
                Next
            End If
            With RadGrid2
                .DataSource = table
                .DataBind()
            End With
            Dim strStudentName As String = ""
            Dim strStudentStartDate As String = ""
            Dim dsStudent As New DataSet
            dsStudent = (New ClassSectionFacade).GetStudentInfo(Request.QueryString("StuEnrollId"))
            For Each drStudent As DataRow In dsStudent.Tables(0).Rows
                strStudentName &= drStudent("StudentName") & ","
                strStudentStartDate = drStudent("StartDate")
            Next

            If Not strStudentName.Trim = "" Then
                strStudentName = Mid(strStudentName, 1, InStrRev(strStudentName, ",") - 1)
            End If
            If Not Classes.Trim = "" And Not stuEnrollId.Trim = "" Then
                If boolconflictinsideterm.ToLower = "true" Then
                    lblMessage.Text = "Student " & strStudentName & " (Start Date: " & strStudentStartDate & ") cannot be registered in classes offered for course(s) - " & Classes & " as more than one classes are offered at the same time."
                Else
                    lblMessage.Text = "Student " & strStudentName & " (Start Date: " & strStudentStartDate & ") cannot be registered in classes offered for course(s) - " & Classes & " as the student is already registered in the following classes offered at the same time."
                End If

            ElseIf Classes.Trim = "" And Not stuEnrollId.Trim = "" Then
                If boolconflictinsideterm.ToLower = "true" Then
                    lblMessage.Text = "Student " & strStudentName & " (Start Date: " & strStudentStartDate & ") cannot be registered in classes offered for course(s) - " & Classes & " as more than one classes are offered at the same time."
                Else
                    lblMessage.Text = "Student " & strStudentName & " (Start Date: " & strStudentStartDate & ") cannot be registered in the selected class as the student is already registered in the following classes offered at the same time."
                End If
            Else
                lblMessage.Text = "The class section for course " & Classes & " cannot be created as following classes are offered at the same time."
            End If

            'Register Students - Add All button
            If Request.QueryString("buttonState").ToString.Trim.ToLower = "all" Then
                If boolconflictinsideterm.ToLower = "true" Then
                    lblMessage.Text = "Student " & strStudentName & " (Start Date: " & strStudentStartDate & ") cannot be registered in classes offered for course(s) - " & Classes & " as more than one classes are offered at the same time."
                Else
                    lblMessage.Text = "Students - " & strStudentName & " cannot be registered in class offered for course " & Classes & " as the students are already registered in the following classes offered at the same time."
                End If
            End If
            'While rescheduleing classes
            If boolRegStdManPage.ToLower = "false" Then
                If conflictsinsideclass = True Or boolConflictClassIgnoreRoom = True Then
                    lblMessage.Text = "Class cannot be rescheduled due to a schedule conflict"
                End If
            End If
        End If

    End Sub
    Private Function CreateNewTable() As DataTable
        ' Create a new DataTable titled 'Names.'
        Dim namesTable As DataTable = New DataTable("Schedules")

        ' Add three column objects to the table.
        Dim strCourseDescrip As DataColumn = New DataColumn()
        strCourseDescrip.DataType = System.Type.GetType("System.String")
        strCourseDescrip.ColumnName = "CourseDescrip"
        namesTable.Columns.Add(strCourseDescrip)

        Dim strClsSection As DataColumn = New DataColumn()
        strClsSection.DataType = System.Type.GetType("System.String")
        strClsSection.ColumnName = "ClsSection"
        namesTable.Columns.Add(strClsSection)

        Dim strClsSectionPeriod As DataColumn = New DataColumn()
        strClsSectionPeriod.DataType = System.Type.GetType("System.String")
        strClsSectionPeriod.ColumnName = "ClassPeriod"
        namesTable.Columns.Add(strClsSectionPeriod)

        Dim strMeetingSchedule As DataColumn = New DataColumn()
        strMeetingSchedule.DataType = System.Type.GetType("System.String")
        strMeetingSchedule.ColumnName = "MeetingSchedule"
        namesTable.Columns.Add(strMeetingSchedule)

        ' Return the new DataTable.
        CreateNewTable = namesTable
    End Function
    Protected Sub radClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radClose.Click
        Label1.Text = "<script type='text/javascript'>Close()</" + "script>"
    End Sub
End Class
Public Class ClsSectMeetingInfo
    Private _CourseDescrip As String
    Private _ClsSection As String
    Private _meetingSchedule As List(Of String)
    Public Property CourseDescrip() As String
        Get
            CourseDescrip = _CourseDescrip
        End Get
        Set(ByVal Value As String)
            _CourseDescrip = Value
        End Set
    End Property
    Public Property ClsSection() As String
        Get
            ClsSection = _ClsSection
        End Get
        Set(ByVal Value As String)
            _ClsSection = Value
        End Set
    End Property
    Public Property MeetingSchedule() As List(Of String)
        Get
            MeetingSchedule = _meetingSchedule
        End Get
        Set(ByVal Value As List(Of String))
            _meetingSchedule = Value
        End Set
    End Property
End Class


