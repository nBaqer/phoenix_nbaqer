<%@ Page Language="vb" AutoEventWireup="false" Inherits="ProgVersions" CodeFile="ProgVersions.aspx.vb" %>

<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register Src="~/usercontrols/DialogTemplates.ascx" TagPrefix="uc1" TagName="DialogTemplates" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Program Versions</title>
    <%--    <link rel="stylesheet" type="text/css" href="../css/localhost_lowercase.css" />--%>
    <link href="../css/systememail.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">
        // Variables for MasterPageUserOptionsPanelBar
        var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL = '<%= Page.ResolveUrl("~")%>';
        var XMASTER_PAGE_USER_OPTIONS_USERNAME = "<%=Session("UserName") %>";
        var XMASTER_PAGE_USER_OPTIONS_USERID = '<%=Session("UserId")%>';
        var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID = '';

    </script>
    <%: Styles.Render("~/bundles/popupstyle") %>
    <%: Scripts.Render("~/bundles/popupscripts") %>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>

    <script type="text/javascript">
        function RowDblClick(sender, eventArgs) {
            sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
        }

        $(document).ready(function () {
            //set session storage key to detect changes on program version definition
            sessionStorage.setItem('programVersionDefinitionFormHasChanged', false);
            $('#txtAllowExcusAbsPerPayPrd').keypress(function (event) {
                var $this = $(this);
                if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                    ((event.which < 48 || event.which > 57) &&
                        (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                }

                var text = $(this).val();
                if ((event.which == 46) && (text.indexOf('.') == -1)) {
                    setTimeout(function () {
                        if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                            $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                        }
                    }, 1);
                }

                if ((text.indexOf('.') != -1) &&
                    (text.substring(text.indexOf('.')).length > 2) &&
                    (event.which != 0 && event.which != 8) &&
                    ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
                }
            });
        });
    </script>


</head>
<body runat="server" leftmargin="0" topmargin="0" id="Body1" name="Body1">
    <form id="Form1" method="post" runat="server">
        <uc1:DialogTemplates runat="server" ID="dialogTemplates" />

        <!-- beging header -->
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="toppopupheader">
                    <img src="../images/advantage_program_versions.jpg">
                </td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href="#">X Close</a>
                </td>
            </tr>
        </table>
        <!-- end header   11/12/13-->
        <div>
            <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
            </telerik:RadStyleSheetManager>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" />
            <asp:ScriptManager ID="scriptmanager1" runat="server">
            </asp:ScriptManager>
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframeprogramver">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table2">
                            <tr>
                                <td class="listframetop2">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="15%" nowrap align="left">
                                                <asp:Label ID="lblShow" runat="server"><b class="label">Show</b></asp:Label>
                                            </td>
                                            <td width="85%" nowrap>
                                                <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="server"
                                                    RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Active" Selected="True" />
                                                    <asp:ListItem Text="Inactive" />
                                                    <asp:ListItem Text="All" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 90%;">
                                    <div style="width: 90%">
                                        <asp:DataList ID="dlstProgVer" runat="server" DataKeyField="PrgVerId">
                                            <SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
                                            <ItemStyle CssClass="NonSelectedItem"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                                    Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'
                                                    CausesValidation="False"></asp:ImageButton>
                                                <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'
                                                    CausesValidation="False"></asp:ImageButton>
                                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                                <asp:LinkButton Text='<%# Container.DataItem("PrgVerDescrip")%>' runat="server" CssClass="NonSelectedItem"
                                                    CommandArgument='<%# Container.DataItem("PrgVerId")%>' ID="Linkbutton4" CausesValidation="False" />
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="leftside">
                        <table cellspacing="0" cellpadding="0" width="9" border="0" id="Table3">
                        </table>
                    </td>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"></asp:Button>
                                </td>
                            </tr>
                        </table>

                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table5">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollrightprogramver">
                                        <asp:Panel ID="pnlRHS" runat="server" Width="90%">
                                            <table cellspacing="0" cellpadding="0" width="40%" align="center">
                                                <tr>
                                                    <td style="padding-right: 16px; padding-left: 16px; padding-bottom: 16px; width: 100%; padding-top: 16px; text-align: center">
                                                        <asp:TextBox ID="txtProgDescrip" runat="server" ReadOnly="True" CssClass="textbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblPrgVerDescrip" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:TextBox ID="txtPrgVerDescrip" runat="server" CssClass="textbox"></asp:TextBox>
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblPrgVerCode" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:TextBox ID="txtPrgVerCode" runat="server" CssClass="textbox"></asp:TextBox>
                                                    </td>

                                                </tr>
                                                <tr>

                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="hdnCampGrpId" runat="server" />
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblPrgGrpId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlPrgGrpId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblDegreeId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlDegreeId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblDeptId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlDeptId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblSAPId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell" nowrap>

                                                        <asp:DropDownList ID="ddlSAPId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>

                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>

                                                        <asp:Label ID="lblFASAPId" runat="server" CssClass="label">FA SAP<span style="color: #b71c1c">*</span></asp:Label>
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:DropDownList ID="ddlFASAPId" runat="server" CssClass="dropdownlistff" CausesValidation="false">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="FASAPId" runat="server" ControlToValidate="ddlFASAPId"
                                                            Display="None" ErrorMessage="FA SAP Policy is required">FA SAP Policy is required</asp:RequiredFieldValidator>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblBillingMethodId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell" nowrap>
                                                        <asp:DropDownList ID="ddlBillingMethodId" runat="server" CssClass="dropdownlistff"
                                                            AutoPostBack="True" CausesValidation="false" Visible="False"
                                                            OnSelectedIndexChanged="ddlBillingMethodId_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:CustomValidator ID="cvBillingMethodID" runat="server" Visible="False"
                                                            ControlToValidate="ddlBillingMethodId" Display="None" SetFocusOnError="True"
                                                            ErrorMessage="Charging method could not be changed because is being used."
                                                            OnServerValidate="BillingMethodValidation" Text="Charging method could not be changed because is being used. ">
                                                        </asp:CustomValidator>
                                                        <asp:TextBox ID="txtBillingMethodDescrip" runat="server" CssClass="textbox"
                                                            BackColor="#dddddd" ReadOnly="true"
                                                            ToolTip="Please setup the Charging Method in the Maintenance Module under Student Accounts\Charging Methods">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>
                                                    </td>

                                                </tr>
                                                <tr>

                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblGrdSystemId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:DropDownList ID="ddlGrdSystemId" runat="server" CssClass="dropdownlistff" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblProgTypId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlProgTypId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblTestingModelId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:DropDownList ID="ddlTestingModelId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblWeeks" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:TextBox ID="txtWeeks" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator4" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtWeeks"></asp:CustomValidator>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblTerms" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:TextBox ID="txtTerms" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator2" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtTerms"></asp:CustomValidator>
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblCredits" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:TextBox ID="txtCredits" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator3" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtCredits"></asp:CustomValidator>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblHours" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:TextBox ID="txtHours" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator5" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtHours"></asp:CustomValidator>

                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblacademicyearlength" Text="Academic year length" runat="server"
                                                            CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:TextBox ID="txtAcademicYearlength" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator9" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtAcademicYearlength"></asp:CustomValidator>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblacademicyearweeks" Text="Weeks in academic year" runat="server"
                                                            CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:TextBox ID="txtacademicyearweeks" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator10" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtacademicyearweeks"></asp:CustomValidator>
                                                    </td>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblPayPeriodPerAcYear" Text="Pay Period per academic year" runat="server"
                                                            CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:TextBox ID="txtPayPeriodPerAcYear" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator11" runat="server" Display="None" ErrorMessage="Values should be greater Than 1"
                                                            OnServerValidate="TextValidateGreaterThanOne" ControlToValidate="txtPayPeriodPerAcYear"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectioncontentcell"></td>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblCalculationPeriodTypeId" Text="Period used for R2T4 calculation" runat="server"
                                                            CssClass="label"></asp:Label><span id="spnCalculationPeriodTypeId" runat="server" visible="False" style="color: red; font-size: 11px; font-style: normal;">*</span>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlCalculationPeriodTypeId" runat="server" CssClass="dropdownlistff" CausesValidation="False">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="R2T4CalculationPeriodValidator" runat="server" ControlToValidate="ddlCalculationPeriodTypeId"
                                                            Display="None" ErrorMessage="Period used for calculation is required">Period used for calculation is required</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectioncontentcell"></td>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblTermLength" Text="Terms substantially equal in length" runat="server"
                                                            CssClass="label"></asp:Label><span id="spnTermLength" runat="server" visible="False" style="color: red; font-size: 11px; font-style: normal;">*</span>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlTermLength" runat="server" CssClass="dropdownlistff" CausesValidation="false">
                                                            <asp:ListItem Value="" Text="Select" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="termLengthValidtor" runat="server" ControlToValidate="ddlTermLength" Enabled="false"
                                                            Display="None" ErrorMessage="Please select terms substantially equal in length value">Please select terms substantially equal in length value</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblExcusedAbsence" Text="Excused Absence Percentage Allowed Per Payment Period" Visible="false" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectionlabelcell">
                                                        <asp:TextBox ID="txtAllowExcusAbsPerPayPrd" runat="server" Enabled="False" Visible="False" CssClass="textbox"></asp:TextBox>
                                                        <asp:RegularExpressionValidator runat="server" ID="ExcusedRGX" ControlToValidate="txtAllowExcusAbsPerPayPrd" CssClass="regex"
                                                            ValidationExpression="^(?:10(?:\.0)?|[0-9](?:\.[0-9])?|0?\.[0-9])$" Display="None" ErrorMessage="Excused Absence Percentage Allowed Per Payment Period value should be between 0.0 to 10.0 with one decimal">Value should be between 0.0 to 10.0 with one decimal</asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblTuitionEarningId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="ClassSectionContentCella">
                                                        <asp:DropDownList ID="ddlTuitionEarningId" runat="server" CssClass="dropdownlistff">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:CheckBox ID="chkIsContinuingEd" runat="server" CssClass="label"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblUnitTypeId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlUnitTypeId" runat="server" CssClass="dropdownlistff" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:CheckBox ID="chkTrackTardies" runat="server" CssClass="label" AutoPostBack="true"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblCurrCodeId" runat="server" CssClass="label" Visible="false">Curriculum Code</asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:DropDownList ID="ddlCurrCodeId" runat="server" CssClass="dropdownlistff" Visible="false"
                                                            AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectioncontentcell"></td>
                                                </tr>
                                                <tr>
                                                    <asp:Panel ID="pnlScheduleMethod" runat="server" Visible="false">
                                                        <td class="classsectionlabelcell">
                                                            <asp:Label ID="lblSchedMethodId" runat="server" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td class="classsectioncontentcella">
                                                            <asp:DropDownList ID="ddlSchedMethodId" runat="server" CssClass="dropdownlistff">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="classsectionlabelcell"></td>
                                                        <td class="classsectioncontentcell"></td>
                                                    </asp:Panel>
                                                </tr>
                                                <!-- Added by Vijay Ramteke on Feb 05, 2010 For Mantis Id 17900 -->
                                                <tr>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblTotalCost" runat="server" CssClass="label" Text="Total Cost"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:TextBox ID="txtTotalCost" runat="server" CssClass="textbox"></asp:TextBox>
                                                    </td>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectioncontentcell"></td>
                                                </tr>
                                                <!-- Added by Vijay Ramteke on Feb 05, 2010 For Mantis Id 17900 -->
                                                <tr>
                                                    <asp:Panel ID="pnlTardies" runat="Server" Visible="false">
                                                        <td class="classsectionlabelcell"></td>
                                                        <td class="classsectioncontentcella"></td>
                                                        <td class="classsectionlabelcell" style="white-space: normal">
                                                            <asp:Label ID="lblTardiesMakingAbsence" runat="server" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td class="classsectioncontentcell">
                                                            <asp:TextBox ID="txtTardiesMakingAbsence" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RangeValidator ID="rvTardiesMakingAbsence" runat="server" ControlToValidate="txtTardiesMakingAbsence"
                                                                Display="none" MinimumValue="0" MaximumValue="365" ErrorMessage="Number of Tardies Making An Absence must be between 0 and 365."
                                                                Type="Integer"></asp:RangeValidator>
                                                        </td>
                                                    </asp:Panel>
                                                </tr>
                                                <tr>
                                                    <asp:Panel ID="pnlTitleIVFameApproved" runat="Server">
                                                        <td class="classsectionlabelcell"></td>
                                                        <td class="classsectioncontentcella"></td>
                                                        <td class="classsectionlabelcell" style="white-space: normal"></td>
                                                        <td class="classsectioncontentcell">
                                                            <asp:CheckBox ID="chkTitleIVFameApproved" runat="server" CssClass="label" AutoPostBack="True" Text="Fame Approved Title IV" OnCheckedChanged="chkTitleIVFameApproved_OnCheckedChanged"></asp:CheckBox>
                                                        </td>
                                                    </asp:Panel>
                                                </tr>
                                                <tr>
                                                    <asp:Panel ID="pnlTitleIV" runat="Server">
                                                        <td class="classsectionlabelcell"></td>
                                                        <td class="classsectioncontentcella"></td>
                                                        <td class="classsectionlabelcell" style="white-space: normal"></td>
                                                        <td class="classsectioncontentcell">
                                                            <asp:CheckBox ID="chkTitleIV" runat="server" CssClass="label" AutoPostBack="true" Text="Title IV" OnCheckedChanged="chkTitleIV_OnCheckedChanged"></asp:CheckBox>
                                                        </td>
                                                    </asp:Panel>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectioncontentcella"></td>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblThreeQuartTime" runat="server" CssClass="label" Visible="False"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:TextBox ID="txtThreeQuartTime" Visible="False" runat="server" CssClass="textbox"
                                                            Text="0">0</asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator8" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtThreeQuartTime"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell" nowrap>
                                                        <asp:Label ID="lblSelfPaced" runat="server" CssClass="label" Text="Self Paced"><span id="spnSelfPaced" runat="server" visible="False" style="color: #b71c1c">*</span></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:DropDownList ID="ddlSelfPaced" runat="server" CssClass="dropdownlistff" CausesValidation="True">
                                                            <asp:ListItem Value="" Text="Select" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="SelfPacedValidator" runat="server" ControlToValidate="ddlSelfPaced"
                                                            Display="None" ErrorMessage="Self Paced is required">Self Paced is required</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="classsectionlabelcell" nowrap></td>
                                                    <td class="classsectioncontentcella"></td>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblLTHalfTime" runat="server" CssClass="label" Visible="False"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <asp:TextBox ID="txtLTHalfTime" runat="server" CssClass="textbox" Text="0" Visible="False">0</asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator6" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtLTHalfTime"></asp:CustomValidator>
                                                    </td>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblHalfTime" runat="server" CssClass="label" Visible="False"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:TextBox ID="txtHalfTime" runat="server" CssClass="textbox" Text="0" Visible="False">0</asp:TextBox>
                                                        <asp:CustomValidator ID="Customvalidator7" runat="server" Display="None" ErrorMessage="Negative Values Not Allowed"
                                                            OnServerValidate="TextValidate" ControlToValidate="txtHalfTime"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="1" style="padding-bottom: 15px;">
                                                        <br />
                                                        <span class="label">
                                                            <label for="ddlRegistrationType">Registration Type: </label>
                                                        </span>
                                                    </td>
                                                    <td colspan="1">
                                                        <asp:DropDownList runat="server" ID="ddlRegistrationType" CssClass="DropDownListFF" AutoPostBack="true">
                                                            <asp:ListItem Text="By Class" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="By Program" Value="1"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Panel runat="server" ID="panelGradeScaleLabel">
                                                            <span class="label">
                                                                <label for="ddlGradeScales">&nbsp; Grade Scales <span style="color: #b71c1c">*</span>: </label>
                                                            </span>
                                                        </asp:Panel>
                                                    </td>
                                                    <td>
                                                        <asp:Panel runat="server" ID="panelGradeScalesDropdown">
                                                            <asp:DropDownList runat="server" ID="ddlGradeScales" CssClass="DropDownListFF">
                                                            </asp:DropDownList>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="classsectionlabelcell">
                                                        <asp:Label ID="lblInstructionTypeHours" runat="server" CssClass="label" Text="Instruction Type Hours"
                                                            Visible="false"></asp:Label>
                                                    </td>
                                                    <td class="classsectioncontentcella">
                                                        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
                                                        <telerik:RadGrid ID="grdPrgVerInstructionType" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                                                            HeaderStyle-HorizontalAlign="Center" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                                                            AllowPaging="True" AutoGenerateColumns="False" Width="100%" AllowMultiRowEdit="false"
                                                            >
                                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                                            <MasterTableView Width="100%" CommandItemDisplay="top" DataKeyNames="PrgVerInstructionTypeId"
                                                                AutoGenerateColumns="False" EditMode="InPlace" NoMasterRecordsText="No Instruction Type Hours to display." PageSize="10">
                                                                <CommandItemSettings AddNewRecordText="Add" />
                                                                <Columns>
                                                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                                                        <ItemStyle CssClass="MyImageButton" />
                                                                    </telerik:GridEditCommandColumn>

                                                                    <telerik:GridBoundColumn DataField="PrgVerInstructionTypeId" UniqueName="PrgVerInstructionTypeId" DataType="System.Guid" Visible="False"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="PrgVerId" UniqueName="PrgVerId" DataType="System.Guid" Visible="False"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="InstructionTypeId" UniqueName="InstructionTypeId" DataType="System.Guid" Visible="False"></telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="InstructionTypeDescrip" HeaderStyle-Width="40%" ItemStyle-Width="40%" HeaderText="Instruction Type">
                                                                        <ItemTemplate>
                                                                            <asp:Label CssClass="label" ID="lblInstructionTypeDesc" runat="server" Text='<%# Eval("InstructionTypeDescrip") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:DropDownList CssClass="dropdownlistff" ID="ddlInstructionType" runat="server" DataSource='<%# InstructionTypeCodes %>'
                                                                                DataTextField="InstructionTypeDescrip" DataValueField="InstructionTypeId">
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="InstructionRequiredFieldValidator" runat="server" ControlToValidate="ddlInstructionType"
                                                                                Display="None" ErrorMessage="Instruction Type can not be blank">Instruction Type can not be blank</asp:RequiredFieldValidator>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="Hours" HeaderStyle-Width="30%" ItemStyle-Width="30%" HeaderText="Hours">
                                                                        <ItemTemplate>
                                                                            <asp:Label CssClass="label" ID="lblHours" runat="server" Text='<%# Eval("Hours") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtEditHours" Text='<%# Bind("Hours") %>'
                                                                                CssClass="TextBox" runat="server" Width="40px" MaxLength="6"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="HoursRequiredFieldValidator" runat="server" ControlToValidate="txtEditHours"
                                                                                Display="None" ErrorMessage="Instruction Hours can not be blank">Instruction Hours can not be blank</asp:RequiredFieldValidator>
                                                                            <asp:CompareValidator ID="HoursCompareValidator" runat="server" ControlToValidate="txtEditHours" Display="None"
                                                                                ErrorMessage="Invalid Instruction Hours value" Operator="GreaterThan" ValueToCompare="0" Type="Double"></asp:CompareValidator>
                                                                            <asp:CustomValidator ID="CVHoursSum" runat="server" Display="None"
                                                                                OnServerValidate="HoursSumValidate" ControlToValidate="txtEditHours"></asp:CustomValidator>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="ContainerHidden" Visible="false">
                                                                        <ItemTemplate>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:Label runat="server" ID="lblCmdType" Text='<%# Bind("CmdType") %>' Visible="false">
                                                                            </asp:Label>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                                                                        UniqueName="DeleteColumn" ConfirmDialogType="RadWindow"
                                                                        ConfirmText="Are you sure you want to delete this Instruction Type?">
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                    </telerik:GridButtonColumn>
                                                                </Columns>
                                                                <EditFormSettings>
                                                                    <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                                                                    <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
                                                                    <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" BackColor="White"
                                                                        Width="100%" />
                                                                    <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px" BackColor="White" />
                                                                    <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                                                                    <EditColumn ButtonType="ImageButton" InsertText="Insert record" UpdateText="Update record"
                                                                        UniqueName="EditCommandColumn1" CancelText="Cancel edit">
                                                                    </EditColumn>
                                                                    <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
                                                                </EditFormSettings>
                                                            </MasterTableView>
                                                            <ClientSettings>
                                                                <ClientEvents OnRowDblClick="RowDblClick" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                    </td>
                                                    <td class="classsectionlabelcell"></td>
                                                    <td class="classsectioncontentcell">
                                                        <asp:CheckBox ID="chkUseTimeClock" runat="server" CssClass="label" Visible="True" />
                                                        <asp:CheckBox ID="chkFullTime" runat="server" CssClass="label" Visible="False"></asp:CheckBox>
                                                    </td>
                                                </tr>

                                            </table>
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                <tr>
                                                    <td class="contentcellprogramver" style="padding-top: 30px">
                                                        <asp:LinkButton ID="Linkbutton2" runat="server" Width="15%" CssClass="label">Program Version Definition</asp:LinkButton>
                                                        <asp:LinkButton ID="lbtSetUpFees" runat="server" Width="15%" CssClass="label" Enabled="False">Set Up Fees</asp:LinkButton>
                                                        <asp:LinkButton ID="lbtSetTermModules" runat="server" Width="15%" CssClass="label"
                                                            Enabled="False">Set Up Module Starts</asp:LinkButton>
                                                        <asp:LinkButton ID="lbSetSchedules" runat="server" Width="15%" CssClass="label" Enabled="true">Set Up Schedules</asp:LinkButton>
                                                        <asp:LinkButton ID="lbSetUpCourses" runat="server" Width="15%" CssClass="label" Enabled="False"
                                                            CausesValidation="false">Set Up Course Sequences</asp:LinkButton>
                                                        <asp:LinkButton ID="lbSetUpDefaultCharges" runat="server" Width="15%" CssClass="label"
                                                            Enabled="False" CausesValidation="false">Set Up Default Periods For Charges</asp:LinkButton>
                                                        <asp:LinkButton ID="lbSetUpExamSequence" runat="server" Width="15%" CssClass="label"
                                                                        Enabled="False" CausesValidation="false">Set Up Exam Sequences</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlRegent" runat="server" Visible="false">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center"
                                                style="border: 1px solid #ebebeb;">
                                                <tr>
                                                    <td class="contentcellheader" nowrap style="border-top: 0px; border-right: 0px; border-left: 0px">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="label">Map To Regent</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcellcitizenship" style="padding-top: 12px">
                                                        <asp:Panel ID="pnlRegentAwardChild" TabIndex="12" runat="server" Width="100%">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td width="40%">
                                                                        <asp:Label ID="lblregentterm" runat="server" CssClass="label">Curriculum Code<span style="color: red">*</span></asp:Label>
                                                                    </td>
                                                                    <td width="60%">
                                                                        <asp:DropDownList ID="ddlRegentCode" runat="server" CssClass="textbox" TabIndex="6">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                    <asp:TextBox ID="txtProgId" runat="server" Visible="false"></asp:TextBox>
                                    <%--AD-2013.--%>
                                    <asp:TextBox ID="txtProgType" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox>
                                    <asp:TextBox ID="txtUnitTypeId" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtHoursSum" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtOrigHours" runat="server" Visible="false"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <asp:TextBox ID="txtPrgVerId" runat="server" Style="visibility: hidden" Width="0" CssClass="donothing"></asp:TextBox>
        <asp:TextBox ID="txtCampusId" runat="server" Style="visibility: hidden" Width="0" CssClass="donothing" MaxLength="128"></asp:TextBox>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel
            ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False"></asp:ValidationSummary>
        <%--<div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
    </form>

</body>
</html>
