﻿Imports FAME.Common
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports Telerik.Web.UI
Imports FAME.Advantage.Common

Partial Class PlaceOnProbation
    Inherits BasePage
    Protected campusId, userid As String
    Private pObj As New UserPagePermissionInfo
    Private Sub Page_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim ds As New DataSet
        'Dim facade As New ClassSectionFacade
        Dim objCommon As New CommonUtilities
        'Dim dt As New DataTable

        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userid = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userid = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not Page.IsPostBack Then
            'Dim objcommon As New CommonUtilities
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")

            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            btnNew.Enabled = True
            ViewState("MODE") = "NEW"

            PopulateProbWarningDdl()
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")
        End If
    End Sub

    Private Sub PopulateProbWarningDdl()

        Dim facade As New ProbationFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlProbWarningTypeId
            .DataSource = facade.GetProbationStatuses()
            .DataTextField = "Descrip"
            .DataValueField = "ProbWarningTypeId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

   Protected Sub TransferToParent(enrollid As String, fullname As String, termid As String, _
                               termdescrip As String, academicyearid As String, academicyeardescrip As String, _
                               hourtype As String)


        txtStuEnrollmentId.Text = enrollid
       
        Session("StuEnrollment") = txtStuEnrollmentId.Text
        Session("StudentName") = fullname
        'Session("TermId") = txtTermId.Text
        'Session("Term") = txtTerm.Text
        'Session("AcademicYearID") = txtAcademicYearId.Text
        'Session("AcademicYear") = txtAcademicYear.Text
    End Sub

    Private Sub btnLOA_Click(sender As Object, e As System.EventArgs) Handles btnProbation.Click
        Dim facade As New ProbationFacade
        Dim message As String
        Dim student As String
        Dim errSDate As String

        If Session("StudentName") = "" Then
            CommonWebUtilities.DisplayWarningInMessageBox(Page,"Unable to find data. Student field cannot be empty.")
            Exit Sub
        Else
            student = Session("StudentName")
        End If

        If ddlProbWarningTypeId.SelectedItem.Text = "Select" Then
             CommonWebUtilities.DisplayWarningInMessageBox(Page,"The Probation/Warning Type field cannot be empty. Please select an option.")
            Exit Sub
        Else
            ViewState("ProbType") = ddlProbWarningTypeId.SelectedItem.Value.ToString
        End If

        ViewState("Reason") = txtReason.Text

        'Validate that start date on this student enrollment is greater 
        errSDate = ValidateStudentStartDate(CType(txtStartDate.SelectedDate, Date))
        If errSDate <> "" Then
            DisplayErrorMessage(errSDate)
            Exit Sub
        End If

        Dim change As StudentChangeHistoryObj = New StudentChangeHistoryObj()
        change.StuEnrollId = txtStuEnrollmentId.Text
        change.Reason = txtReason.Text
        change.StartDate = CType(txtStartDate.SelectedDate, Date)
        change.EndDate = txtEndDate.SelectedDate
        change.ModUser = AdvantageSession.UserState.UserName
        change.IsRevelsal = 0
        change.DateOfChange = CType(txtStartDate.SelectedDate, Date)
        change.ProbWarningTypeId = CType(ViewState("ProbType"), Integer)
        change.ModDate = DateTime.Now
        change.CampusId = campusId
        message = facade.PlaceStudentOnProbation(change)
 
        If message <> "" Then
            DisplayErrorMessage(message)
        Else
            If ViewState("ProbType") = 2 Then
               CommonWebUtilities.DisplayInfoInMessageBox(Page,"Student has been successfully placed on Probation")
                ClearRHS()
            ElseIf ViewState("ProbType") = 3 Then
                 CommonWebUtilities.DisplayInfoInMessageBox(Page,"Student has been successfully placed on Warning")
                ClearRHS()
            End If
        End If
    End Sub
    Public Function ValidateStudentStartDate(LOAStartDate As Date) As String

        Dim errMsg As String = ""
        Dim facade As New LOAFacade
        Dim StdstartDate As String

        StdstartDate = facade.GetStudentStartDate(txtStuEnrollmentId.Text)

        'If student has a start date, then validate against it.
        If StdstartDate <> "" Then
            If LOAStartDate < StdstartDate Then
                errMsg &= "Cannot place student with a future start date on Probation."
            End If
        End If
        Return errMsg
    End Function
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If TypeOf ctl Is TextBox Then
                    CType(ctl, TextBox).Text = ""
                End If
                If TypeOf ctl Is CheckBox Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If TypeOf ctl Is DropDownList Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
                If TypeOf ctl Is RadDatePicker Then
                    CType(ctl, RadDatePicker).Clear()
                End If
                If TypeOf ctl Is AdvControls.IAdvControl Then
                    Dim AdvCtrl As AdvControls.IAdvControl = CType(ctl, AdvControls.IAdvControl)
                    AdvCtrl.Clear()
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DisplayErrorMessage(errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(sender As Object, e As System.EventArgs) Handles btnNew.Click
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String

        Try
            ClearRHS()
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            txtStartDate.SelectedDate = Nothing
            txtEndDate.SelectedDate = Nothing
            ViewState("MODE") = "NEW"
            btnSave.Enabled = False
            'Header1.EnableHistoryButton(False)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnProbation)
        controlsToIgnore.Add(txtStartDate)
        controlsToIgnore.Add(txtEndDate)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        'BIndToolTip()
    End Sub


End Class
