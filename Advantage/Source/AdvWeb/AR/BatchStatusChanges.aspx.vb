﻿Imports System.Collections
Imports System.Data
Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports FAME.Common
Imports Telerik.Web.UI
Imports AdvWeb.VBCode
Imports FAME.Advantage.DataAccess.LINQ

Namespace AdvWeb.AR

    Partial Class BatchStatusChanges
        Inherits BasePage

        Private pObj As New UserPagePermissionInfo

        Protected WithEvents LnkDisbursements As LinkButton
        Protected WithEvents AllDataset As DataSet
        Protected WithEvents DataGridTable As DataTable
        Protected CampusId As String
        Protected UserName As String
        Protected UserId As String
        Protected MyAdvAppSettings As AdvAppSettings
        Private Const ACADEMIC_PROBATION_MESSAGE As String = "The highlighted students above will be moved to Academic Probation (SAP)."

        Private Property ShowAcademicProbationMessage As Boolean
            Get
                If (ViewState("ShowAcademicProbationMessage") Is Nothing) Then
                    Return False
                Else
                    Return DirectCast(ViewState("ShowAcademicProbationMessage"), Boolean)
                End If
            End Get
            Set(value As Boolean)
                ViewState.Add("ShowAcademicProbationMessage", value)
            End Set
        End Property

        Private Const JAVASCRIPT As String = "<script>var origwindow=window.self;window.open('../SA/PrintAnyReport.aspx');</script>"

        Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme
        End Sub

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

            MyAdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim objCommon As New CommonUtilities
            Dim resourceId As Integer

            Dim advantageUserState As User = AdvantageSession.UserState

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            CampusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
            'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
            UserId = AdvantageSession.UserState.UserId.ToString
            UserName = AdvantageSession.UserState.UserName

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), CampusId)
            'Check if this page still exists in the menu while switching campus
            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If

            'pObj = Header1.UserPagePermission

            If Not IsPostBack Then
                ViewState("MODE") = "NEW"

                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'objCommon.SetCaptionsAndColorRequiredFields(Form1)

                '   build drop-down lists
                BuildDropDownLists()

                '   initialize buttons
                InitButtonsForLoad()

                pnlNotes.Visible = True
                pnlStatusChanges.Visible = False

            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            End If

            'CreateDatasetAndTables()
            'Show List
            If ddlFromStatusCodeId.SelectedIndex > 0 And
               ddlStatusCodeId.SelectedIndex > 0 Then
                dgrdStuEnrollments.Visible = True
                lblStudentCount.Visible = True
                If ShowAcademicProbationMessage Then
                    lblAcademicProbation.Text = ACADEMIC_PROBATION_MESSAGE
                    lblAcademicProbation.Visible = True
                Else
                    lblAcademicProbation.Text = String.Empty
                    lblAcademicProbation.Visible = False
                End If
            Else
                dgrdStuEnrollments.Visible = False
                lblStudentCount.Visible = False
                lblAcademicProbation.Text = String.Empty
                lblAcademicProbation.Visible = False
            End If

        End Sub

        Private Sub BuildDropDownLists()
            BuildFromStatusCodesDDL()
            BuildPrgVersionsDDL()
            BuildLeadGroupsComboBox()
        End Sub
        Private Sub BuildLeadGroupsComboBox()
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            Dim data = New LeadGroupDA(connectionString).GetLeadGroupsByCampus(Guid.Parse(CampusId))
            With comboLeadGroups
                .DataTextField = "Description"
                .DataValueField = "Id"
                .DataSource = data
                .DataBind()
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub BuildFromStatusCodesDDL()
            '   bind DDL
            Dim facade As New BatchStatusChangeFacade
            With ddlFromStatusCodeId
                .DataTextField = "StatusCodeDescrip"
                .DataValueField = "StatusCodeId"
                Dim ds As DataSet = facade.GetActiveInactiveStatusesForBatchChanges(CampusId, UserName)
                .DataSource = ds
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub BuildPrgVersionsDDL()
            Dim facade As New StuEnrollFacade
            With ddlPrgVerId
                '.DataTextField = "PrgVerDescrip"
                .DataTextField = "PrgVerShiftDescrip"
                .DataValueField = "PrgVerId"
                .DataSource = facade.GetAllActiveInActivePrgVersions(CampusId)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
        End Sub
        Private Sub BuildToStatusCodesDDL(ByVal statusCodeId As String)
            '   bind DDL
            Dim facade As New BatchStatusChangeFacade
            With ddlStatusCodeId
                .DataTextField = "StatusCodeDescrip"
                .DataValueField = "StatusCodeId"
                .DataSource = facade.GetStatuses_New(statusCodeId, CampusId, UserName)
                'End If
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub InitButtonsForLoad()
            btnChangeStatus.Enabled = False
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
        End Sub

        Private Sub InitButtonsForWork()
            If pObj.HasFull Or pObj.HasEdit Then
                btnChangeStatus.Enabled = True
            Else
                btnChangeStatus.Enabled = False
            End If

            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
        End Sub

        Private Sub DisplayErrorMessage(ByVal errorMessage As String)
            '   Set error condition
            Customvalidator1.ErrorMessage = errorMessage
            Customvalidator1.IsValid = False

            If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
                '   Display error in message box in the client
                CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
            End If
        End Sub
        Private Sub DisplayInfoMessage(ByVal infoMessage As String)
            '   Set error condition
            Customvalidator1.ErrorMessage = infoMessage
            Customvalidator1.IsValid = False

            If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
                '   Display error in message box in the client
                CommonWebUtilities.DisplayInfoInMessageBox(Page, infoMessage)
            End If
        End Sub

        Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click

            If ddlFromStatusCodeId.SelectedValue <> Guid.Empty.ToString Then
                '   populate Status DDL depending on selected status on LHS
                BuildToStatusCodesDDL(ddlFromStatusCodeId.SelectedValue)

                '   bind data-grid using filters on LHS
                BindDataGrid()

                '   hide/show data-grid columns
                HideShowDataGridColumns()

                '   Initialize buttons
                InitButtonsForWork()

            Else
                '   Display Error Message
                DisplayErrorMessage("Please select a status.")

                '   Initialize buttons
                InitButtonsForLoad()
            End If
        End Sub

        Private Sub BindDataGrid()
            Dim prgVerId As String = ""
            Dim fromStatusCodeId As String = ""
            Dim startDate As String = ""
            Dim studentGroups As String = ""
            pnlNotes.Visible = False
            pnlDate.Visible = False
            pnlStatusChanges.Visible = True


            If ddlPrgVerId.SelectedValue <> Guid.Empty.ToString Then
                prgVerId = ddlPrgVerId.SelectedValue.ToString
            End If
            If txtStartDate.SelectedDate IsNot Nothing Then
                Try
                    Dim dtDate As DateTime = Convert.ToDateTime(txtStartDate.SelectedDate)
                    startDate = dtDate.ToString
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ' Invalid date
                    DisplayErrorMessage("Start Date is not a valid date.")
                    Exit Sub
                End Try
            End If
            ''CohortStartDate added
            Dim cohortStartDate As String = ""
            If txtCohortStartDate.SelectedDate IsNot Nothing Then
                Try
                    Dim dtDate As DateTime = Convert.ToDateTime(txtCohortStartDate.SelectedDate)
                    cohortStartDate = dtDate.ToString
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ' Invalid date
                    DisplayErrorMessage("Cohort Start Date is not a valid date.")
                    Exit Sub
                End Try
            End If

            If ddlFromStatusCodeId.SelectedValue <> Guid.Empty.ToString Then
                fromStatusCodeId = ddlFromStatusCodeId.SelectedValue.ToString
            End If

            ' ''Cohort StartDate added
            ''To add last name, first name and SSN to the filter list 
            ''Cohort StartDate added
            Dim lastname As String
            Dim firstname As String
            Dim ssn As String
            lastname = Replace(txtLastname.Text.Trim, "'", "''")
            firstname = Replace(txtFirstName.Text.Trim, "'", "''")
            ssn = txtSSN.Text.Trim

            Dim collection As IList(Of String) = comboLeadGroups.CheckedItems.Select(Function(x) "'" + x.Value + "'").ToList

            If (collection.Count <> 0) Then
                studentGroups = String.Join(",", collection)
            End If

            Dim dt As DataTable = (New BatchStatusChangeFacade).GetStudentsDSbasedonNameandSSN(fromStatusCodeId, prgVerId, startDate, HttpContext.Current.Request.Params("cmpid"), lastname, firstname, ssn, studentGroups, cohortStartDate, UserName)

            Dim dclone As DataTable = dt.Clone()

            ' Filter Table if the initial status is Future Start. The filter only work if SysStatusId = 7 
            ' The OR part eliminate filtering if the initial status is Future Start (7) 
            ' The first part -> (HasAttendance = 'NO' AND HasGrades = 'NO')
            ' Eliminate for Future Start all student with attendance or grades posted
            ' WARNING If in the future other changes are introduced to future start status revise the filter!!!
            Dim expression As String = "(HasAttendance = 'NO' AND ((HasGrades = 'NO' AND IsProgramRegistrationType = 0 ) OR (IsProgramRegistrationType =1)) ) OR SysStatusId <> 7 "
            If ddlStatusCodeId.SelectedValue <> Guid.Empty.ToString Then
                Dim facade As New BatchStatusChangeFacade
                Dim newSysStatus As Integer = facade.GetSysStatus(ddlStatusCodeId.SelectedValue.ToString)
                If (newSysStatus = StateEnum.NoStart) Then
                    expression = "(HasAttendance = 'NO' AND HasGrades = 'NO')"
                End If
            End If
            Dim rows As DataRow() = dt.Select(expression)

            For Each dataRow As DataRow In rows
                dclone.ImportRow(dataRow)
            Next

            Dim dcloneView As New DataView(dclone)
            dcloneView.Sort = "LastName ASC, FirstName ASC,  PrgVerDescrip ASC, StartDate ASC, ExpGradDate ASC"
            Dim dcloneSorted As DataTable = dcloneView.ToTable()

            With dgrdStuEnrollments
                .DataSource = dcloneSorted
                .DataBind()
            End With
            ViewState.Add("dgSource", dclone)
            If dt.Rows.Count = 1 And lblDate.Text = "Graduation Date" Then
                'Populate date textbox with Expected Graduation Date only if there is just one row
                Dim row As DataRow = dclone.Rows(0) ' dt.Rows(0)
                If Not row.IsNull("ExpGradDate") Then
                    txtDate.SelectedDate = CType(Convert.ToDateTime(row("ExpGradDate")).ToShortDateString, Date?)
                End If
            End If

            lblStudentCount.Text = "Total Student Enrollments: " & dcloneSorted.Rows.Count
        End Sub

        '   hide/show some grid columns
        Private Sub HideShowDataGridColumns()
            If ddlFromStatusCodeId.SelectedValue <> Guid.Empty.ToString Then
                Dim enumType As StateEnum = CType((New BatchStatusChangeFacade).GetSysStatus(ddlFromStatusCodeId.SelectedValue), StateEnum)
                Select Case enumType
                    Case StateEnum.LeaveOfAbsence
                        dgrdStuEnrollments.Columns(5).Visible = False       '   Last Date Attended
                        dgrdStuEnrollments.Columns(6).Visible = False       '   Dropped Date 
                        dgrdStuEnrollments.Columns(7).Visible = True        '   LOA Period
                        dgrdStuEnrollments.Columns(8).Visible = False       '   Suspension Period

                    Case StateEnum.Dropped
                        dgrdStuEnrollments.Columns(5).Visible = True        '   Last Date Attended 
                        dgrdStuEnrollments.Columns(6).Visible = True        '   Dropped Date 
                        dgrdStuEnrollments.Columns(7).Visible = False       '   LOA Period  
                        dgrdStuEnrollments.Columns(8).Visible = False       '   Suspension Period

                    Case StateEnum.Suspended
                        dgrdStuEnrollments.Columns(5).Visible = False       '   Last Date Attended  
                        dgrdStuEnrollments.Columns(6).Visible = False       '   Dropped Date
                        dgrdStuEnrollments.Columns(7).Visible = False       '   LOA Period
                        dgrdStuEnrollments.Columns(8).Visible = True        '   Suspension Period

                    Case Else
                        dgrdStuEnrollments.Columns(5).Visible = False       '   Last Date Attended 
                        dgrdStuEnrollments.Columns(6).Visible = False       '   Dropped Date
                        dgrdStuEnrollments.Columns(7).Visible = False       '   LOA Period      
                        dgrdStuEnrollments.Columns(8).Visible = False       '   Suspension Period
                End Select
            End If
        End Sub

        Private Function ValidateRhs(ByVal oldSysStatus As Integer, ByVal newSysStatus As Integer) As String
            Dim result As String = ""

            If ddlStatusCodeId.SelectedValue = Guid.Empty.ToString Then
                result = "Please select a New Status."
            Else

                If pnlDate.Visible = True And txtDate.SelectedDate Is Nothing Then
                    If oldSysStatus = StateEnum.LeaveOfAbsence Or
                       oldSysStatus = StateEnum.Suspended Or
                       oldSysStatus = StateEnum.Dropped Or
                       oldSysStatus = StateEnum.TransferOut Or
                       oldSysStatus = StateEnum.Graduated Then
                        result = (lblDate.Text & " is required.")
                    End If
                    If newSysStatus = StateEnum.Graduated Then
                        result = (lblDate.Text & " is required.")
                    End If
                    '' validate the LOA return date not to be a future date
                ElseIf pnlDate.Visible = True And txtDate.SelectedDate IsNot Nothing Then
                    If oldSysStatus = StateEnum.LeaveOfAbsence Or
                       oldSysStatus = StateEnum.Suspended Or
                       oldSysStatus = StateEnum.Dropped Or
                       oldSysStatus = StateEnum.TransferOut Or
                       oldSysStatus = StateEnum.Graduated Then
                        If Not IsDate(txtDate.SelectedDate) Then
                            result = "Please enter a valid date."
                        End If
                    End If
                    If newSysStatus = StateEnum.Graduated Then
                        If Not IsDate(txtDate.SelectedDate) Then
                            result = "Please enter a valid date."
                        End If
                    End If
                    If oldSysStatus = StateEnum.LeaveOfAbsence Or
                       oldSysStatus = StateEnum.Suspended Then
                        If IsDate(txtDate.SelectedDate) Then
                            If CDate(txtDate.SelectedDate) > Date.Today Then
                                result = (lblDate.Text & " cannot be a future date.")
                            End If
                        End If

                    End If
                End If
            End If

            Return result
        End Function

        Private Function ValidateLoaReturnDate(ByVal oldSysStatus As Integer) As String

            'Dim x As RadGrid
            Dim iitems As GridDataItemCollection = dgrdStuEnrollments.Items
            Dim iitem As GridDataItem
            Dim errmessage As String = ""

            '   loop through the collection to retrieve the StuEnrollId.
            For i As Integer = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkChangeStatus"), CheckBox).Checked = True) Then
                    If (oldSysStatus = StateEnum.LeaveOfAbsence) Then
                        Dim loaReturnDate As Date
                        Dim loaStartDate As Date = Date.MinValue
                        Dim loaPeriod As String
                        loaReturnDate = CType(txtDate.SelectedDate, Date)
                        loaPeriod = CType(iitem.FindControl("lblLOAPeriod"), Label).Text
                        If (loaPeriod = String.Empty) Then
                            Dim arr() As String
                            arr = Split(loaPeriod, "-")
                            If IsDate(arr(0)) Then
                                loaStartDate = CDate(arr(0))
                            End If
                            If IsDate(arr(1)) Then
                            End If
                        Else
                            loaStartDate = CType(CType(iitem.FindControl("lblLOAStartDate1"), Label).Text, Date).Date
                        End If
                        If loaReturnDate <= loaStartDate Then
                            errmessage = " LOA Return Date for " & CType(iitem.FindControl("lblStudentName"), Label).Text & "  should be greater than LOA Start Date " & loaStartDate
                        End If
                    End If
                End If
            Next
            Return errmessage
        End Function

        Private Sub btnChangeStatus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnChangeStatus.Click
            'Get necessary values
            Dim facade As New BatchStatusChangeFacade
            Dim oldSysStatus As Integer = facade.GetSysStatus(ddlFromStatusCodeId.SelectedValue.ToString)
            Dim newStatusCodeId As String = ddlStatusCodeId.SelectedValue.ToString
            Dim newSysStatus As Integer = facade.GetSysStatus(newStatusCodeId)
            ' Store status as Enum
            Dim newStudentStatusEnum As StateEnum
            newStudentStatusEnum = CType(newSysStatus, StateEnum)

            Dim strErrMsg As String = ValidateRhs(oldSysStatus, newSysStatus)
            If Not (strErrMsg = "") Then
                DisplayErrorMessage(strErrMsg)
                Exit Sub
            End If
            strErrMsg = ValidateLoaReturnDate(oldSysStatus)
            If Not (strErrMsg = "") Then
                DisplayErrorMessage(strErrMsg)
                Exit Sub
            End If

            Dim arrReason As New ArrayList
            Dim isSelected As Boolean
            Dim doNotUpdateCtr As Integer
            Dim reEnrollCount As Integer
            Dim arrStudents As New ArrayList
            Dim arrPrgVersions As New ArrayList
            Dim arrStudentsReEnroll As New ArrayList
            Dim arrPrgVersionsReEnroll As New ArrayList
            Dim gradeReps As String = CType(MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod"), String)
            Dim strIncludeHours As String = CType(MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade"), String)
            Dim includeHours As Boolean
            Dim gradesFormat As String
            Dim strreason As String
            Dim fac As New UserSecurityFacade

            Dim statusChange As StudentChangeHistoryObj
            Dim statusChangeList As List(Of StudentChangeHistoryObj) = New List(Of StudentChangeHistoryObj)
            Dim changeDate As DateTime?
            Dim previousSyStatusId As Integer
            'Dim previousStatusCodeId As String
            'Dim previousStudentStatusChangeId As String

            If strIncludeHours.ToLower = "true" Then
                includeHours = True
            End If

            Dim advantageUserState As User = AdvantageSession.UserState

            ' Hold the list of student selected to change status.
            Dim studentSelected As List(Of BatchStatusChangeStudent) = New List(Of BatchStatusChangeStudent)()

            '   save the data grid items in a collection.
            Dim iitems As GridDataItemCollection = dgrdStuEnrollments.Items
            Dim iitem As GridDataItem


            If oldSysStatus = StateEnum.CurrentlyAttending And (newSysStatus = StateEnum.FutureStart Or newSysStatus = StateEnum.NoStart) Then
                For i As Integer = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    If (CType(iitem.FindControl("chkChangeStatus"), CheckBox).Checked = True) Then
                        Dim hasAttendance As String = CType(iitem.FindControl("lblAtt"), Label).Text.ToUpper()
                        Dim hasGrades As String = CType(iitem.FindControl("lblGrades"), Label).Text.ToUpper()
                        If hasAttendance = "YES" Or hasGrades = "YES" Then
                            DisplayErrorMessage("One or more of the selected students cannot be changed to this status because attendance and/or grades exist. The change status action was not performed for these students. Please remove the attendance and/or grades before changing their status.")
                            Exit Sub
                        End If

                        If newSysStatus = StateEnum.FutureStart Then
                            Dim newStart As RadDatePicker = CType(iitem.FindControl("txtnewStartDate"), RadDatePicker)
                            If Not newStart Is Nothing Then
                                If newStart.SelectedDate Is Nothing Then
                                    DisplayErrorMessage("New Start Date is required for all selected students.")
                                    Exit Sub
                                End If
                            End If

                        End If



                    End If
                Next
            End If

            Try
                '   loop through the collection to retrieve the StuEnrollId.
                changeDate = CType(FindControlRecursive("txtDate"), RadDatePicker).SelectedDate

                For i As Integer = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    If (CType(iitem.FindControl("chkChangeStatus"), CheckBox).Checked = True) Then
                        isSelected = True
                        statusChange = New StudentChangeHistoryObj()
                        statusChange.StuEnrollId = CType(iitem.FindControl("lblStuEnrollId"), Label).Text

                        If (CType(iitem.FindControl("txtReenrollDate"), RadDatePicker).SelectedDate).ToString() = "" Then
                            DisplayErrorMessage("Re-enroll date is required ")
                            SetDisplay()
                            Exit Sub
                        End If
                        Try
                            statusChange.OldSysStatusId = CType(CType(iitem.FindControl("lblSysStatusId"), Label).Text, Integer)
                            statusChange.OrigStatusGuid = ddlFromStatusCodeId.SelectedValue  '  same as CType(iitem.FindControl("lblStatusCodeId"), Label).Text= lblStatusCodeId
                            statusChange.NewSysStatusId = newSysStatus
                            statusChange.NewStatusGuid = newStatusCodeId
                            statusChange.StudentStatus = ddlFromStatusCodeId.SelectedItem.Text


                            'Dim actualSchoolStatusCodeId As String = ddlFromStatusCodeId.SelectedValue  '  same as CType(iitem.FindControl("lblStatusCodeId"), Label).Text= lblStatusCodeId
                            Select Case statusChange.OldSysStatusId
                                Case StateEnum.LeaveOfAbsence
                                    statusChange.DateOfChange = changeDate
                                    statusChange.EndDate = CType(CType(iitem.FindControl("lblLOAEndDate1"), Label).Text, Date?)
                                    statusChange.StartDate = CType(CType(iitem.FindControl("lblLOAStartDate1"), Label).Text, Date)

                                    ' For those records which Previous Status was Academic Probation Or Suspension 
                                    ' should be back to previous status instead Currently Attending  

                                    previousSyStatusId = CType(CType(iitem.FindControl("lblLoaPreviousSysStatusId"), Label).Text, Integer)
                                    If (previousSyStatusId = StateEnum.AcademicProbation) Then
                                        statusChange.NewSysStatusId = CType(CType(iitem.FindControl("lblLoaPreviousSysStatusId"), Label).Text, Integer)
                                        statusChange.NewStatusGuid = CType(iitem.FindControl("lblPreviousStatusCodeIdToLOA"), Label).Text
                                        statusChange.PreviousStudentStatusChangeId = CType(iitem.FindControl("lblPreviousLOAStudentStatusChangeId"), Label).Text
                                    End If

                                Case StateEnum.Suspended
                                    statusChange.DateOfChange = changeDate
                                    statusChange.EndDate = CType(CType(iitem.FindControl("lblSusEndDate1"), Label).Text, Date?)
                                    statusChange.StartDate = CType(CType(iitem.FindControl("lblSusStartDate1"), Label).Text, Date)

                                    ' For those records which Previous Status was Academic Probation Or Suspension 
                                    ' should be back to previous status instead Currently Attending 
                                    ' This Process is not defined yet
                                    'previousSyStatusId = CType(CType(iitem.FindControl("lblLoaPreviousSysStatusId"), Label).Text, Integer)
                                    'If (previousSyStatusId = StateEnum.AcademicProbation) Then
                                    '    statusChange.NewSysStatusId = CType(CType(iitem.FindControl("lblSusPreviousSysStatusId"), Label).Text, Integer)
                                    '    statusChange.NewStatusGuid = CType(iitem.FindControl("lblPreviousStatusCodeIdToSus"), Label).Text
                                    '    statusChange.previousStudentStatusChangeId = CType(iitem.FindControl("lblPreviousSusStudentStatusChangeId"), Label).Text
                                    'End If

                                Case StateEnum.FutureStart
                                    statusChange.StartDate = CType(CType(iitem.FindControl("lblEnrollStartDate"), Label).Text, Date)
                                    statusChange.DateOfChange = DateTime.Now

                                Case StateEnum.CurrentlyAttending,
                                     StateEnum.Graduated,
                                     StateEnum.Suspended,
                                     StateEnum.Externship
                                    statusChange.DateOfChange = changeDate

                                Case Else

                                    statusChange.DateOfChange = CType(iitem.FindControl("txtReenrollDate"), RadDatePicker).SelectedDate

                            End Select
                            statusChange.CampusId = CampusId
                            statusChange.ModUser = UserName
                            statusChange.ModDate = DateTime.Now
                            statusChange.IsRevelsal = 0
                            statusChangeList.Add(statusChange)

                            Dim studentSel As BatchStatusChangeStudent = New BatchStatusChangeStudent()
                            'Fill the selected student object. This is a late addition for that is only used to clock hours validation
                            studentSel.StuEnrollmentId = statusChange.StuEnrollId
                            studentSel.ProgramVersionDescription = CType(iitem.FindControl("lblPrgVerDescrip"), Label).Text
                            studentSel.StudentName = CType(iitem.FindControl("lblStudentName"), Label).Text
                            studentSelected.Add(studentSel)


                            If oldSysStatus = StateEnum.CurrentlyAttending And newSysStatus = StateEnum.FutureStart Then
                                statusChange.StartDate = CType(iitem.FindControl("txtnewStartDate"), RadDatePicker).SelectedDate
                            End If


                            Exit Try
                        Catch ex As Exception
                            Dim exTracker = New AdvApplicationInsightsInitializer()
                            exTracker.TrackExceptionWrapper(ex)

                            Dim xerror As String = "Internal Error occurred: " + ex.Message
                            DisplayErrorMessage(xerror)
                            Exit Sub
                        End Try
                    End If
                Next
                'Code to validate if the student meet the number of hours in clock hours schools....................................
                'Check if we want graduated the students
                Dim notMeetCriteriaMessages As String = String.Empty
                If newStudentStatusEnum = StateEnum.Graduated AndAlso studentSelected.Count > 0 Then
                    'For Each Student do
                    For Each student As BatchStatusChangeStudent In studentSelected
                        ' Get if the program version is Clock Hours.
                        Dim programIsClockHour = BatchStatusChangeFacade.GetProgramIsClockHour(student.ProgramVersionDescription)
                        If (programIsClockHour) Then
                            ' Get the required hours for the program version
                            Dim requiredHours As Decimal = BatchStatusChangeFacade.GetHoursRequiredForProgramVersion(student.ProgramVersionDescription)
                            'Get the student Attendance
                            Dim studentAttendance As Integer = BatchStatusChangeFacade.GetStudentAttendanceForProgramVersion(student.StuEnrollmentId)
                            'Compare if meet the requirement
                            If requiredHours > studentAttendance Then
                                'Even if the student does not meet the number of hours BUT it is the system administrator or director of academics user
                                'running the page then we should still allow the student to graduate.
                                'If the support user is impersonating the system administrator or director of academics they should also be allow to override.
                                'Student does not meet the criteria, pull it in a message Response.
                                If AdvantageSession.IsImpersonating IsNot Nothing Then

                                End If
                                '1 is system role for system administrator and 13 is for director of academics.
                                If Not fac.DoesUserBelongToRoleForCampus(UserId, 1, CampusId) And Not fac.DoesUserBelongToRoleForCampus(UserId, 13, CampusId) _
                                    And Not (advantageUserState.UserName.ToLower = "support" AndAlso AdvantageSession.IsImpersonating IsNot Nothing AndAlso AdvantageSession.IsImpersonating.ToLower = "true" AndAlso fac.DoesUserBelongToRoleForCampus(AdvantageSession.ImpersonatedUserId, 1, CampusId)) _
                                    And Not (advantageUserState.UserName.ToLower = "support" AndAlso AdvantageSession.IsImpersonating IsNot Nothing AndAlso AdvantageSession.IsImpersonating.ToLower = "true" AndAlso fac.DoesUserBelongToRoleForCampus(AdvantageSession.ImpersonatedUserId, 13, CampusId)) _
                                    And Not (advantageUserState.UserName.ToLower = "sa" AndAlso AdvantageSession.IsImpersonating IsNot Nothing AndAlso AdvantageSession.IsImpersonating.ToLower = "true" AndAlso fac.DoesUserBelongToRoleForCampus(AdvantageSession.ImpersonatedUserId, 1, CampusId)) _
                                    And Not (advantageUserState.UserName.ToLower = "sa" AndAlso AdvantageSession.IsImpersonating IsNot Nothing AndAlso AdvantageSession.IsImpersonating.ToLower = "true" AndAlso fac.DoesUserBelongToRoleForCampus(AdvantageSession.ImpersonatedUserId, 13, CampusId)) Then
                                    notMeetCriteriaMessages += String.Format("- {0}, can not be graduated because the student(s) does/do not meet the number of hours required{1}", student.StudentName, Environment.NewLine)
                                End If
                            End If
                        End If

                        'This section is to check if the student has satisfied the course/credits/hours requirement for the program.
                        'Note that this is different from the check of attendance just done above.
                        gradesFormat = CType(MyAdvAppSettings.AppSettings("GradesFormat"), String)
                        strreason = ""

                        If Not (New GraduateAuditFacade).HasStudentMetAllRequirements(CampusId, student.StuEnrollmentId, gradeReps, includeHours, gradesFormat, strreason) Then
                            If Not fac.DoesUserBelongToRoleForCampus(UserId, 1, CampusId) And Not fac.DoesUserBelongToRoleForCampus(UserId, 13, CampusId) _
                               And Not (advantageUserState.UserName.ToLower = "support" AndAlso AdvantageSession.IsImpersonating IsNot Nothing AndAlso AdvantageSession.IsImpersonating.ToLower = "true" AndAlso fac.DoesUserBelongToRoleForCampus(AdvantageSession.ImpersonatedUserId, 1, CampusId)) _
                               And Not (advantageUserState.UserName.ToLower = "support" AndAlso AdvantageSession.IsImpersonating IsNot Nothing AndAlso AdvantageSession.IsImpersonating.ToLower = "true" AndAlso fac.DoesUserBelongToRoleForCampus(AdvantageSession.ImpersonatedUserId, 13, CampusId)) _
                               And Not (advantageUserState.UserName.ToLower = "sa" AndAlso AdvantageSession.IsImpersonating IsNot Nothing AndAlso AdvantageSession.IsImpersonating.ToLower = "true" AndAlso fac.DoesUserBelongToRoleForCampus(AdvantageSession.ImpersonatedUserId, 1, CampusId)) _
                               And Not (advantageUserState.UserName.ToLower = "sa" AndAlso AdvantageSession.IsImpersonating IsNot Nothing AndAlso AdvantageSession.IsImpersonating.ToLower = "true" AndAlso fac.DoesUserBelongToRoleForCampus(AdvantageSession.ImpersonatedUserId, 13, CampusId)) Then
                                notMeetCriteriaMessages += String.Format("- {0}, can not be graduated because {1}{2}", student.StudentName, strreason, Environment.NewLine)
                            End If
                        End If
                    Next
                    ' Check if any student does not meet the requirements
                    If (Not String.IsNullOrEmpty(notMeetCriteriaMessages)) Then
                        ' Student does not meet abort procedure and display error message
                        notMeetCriteriaMessages += String.Format("{0}The procedure did not modify any of the student(s)'s status(es), de-select the above student(s) when running this procedure and the rest of the student(s) will have their status changed.", Environment.NewLine)
                        DisplayErrorMessage(notMeetCriteriaMessages)
                        Exit Sub
                    End If
                End If
                ' End validation code................................................................................................

                If Not isSelected Then
                    DisplayErrorMessage("Please select one or more students to update.")
                    Exit Sub
                Else
                    If statusChangeList.Count > 0 Then
                        Dim result As String
                        Dim startDate As String
                        Dim endDate As String
                        For Each studentChange In statusChangeList
                            Select Case studentChange.NewSysStatusId
                                Case StateEnum.Suspended
                                    studentChange.Reason = txtSuspensionComments.Text
                                    startDate = CType(txtSuspensionStartDate.SelectedDate, String)
                                    endDate = CType(txtSuspensionEndDate.SelectedDate, String)
                                    Dim strErrorMessage As String = ""
                                    If startDate = "" Then
                                        strErrorMessage = "Suspension Start Date is empty" & vbCrLf
                                    End If
                                    If String.IsNullOrEmpty(endDate) Then
                                        strErrorMessage &= "Suspension End Date is empty"
                                    End If
                                    If Not strErrorMessage = "" Then
                                        DisplayErrorMessage(strErrorMessage)
                                        Exit Sub
                                    End If
                                    DateTime.TryParse(startDate, CType(studentChange.StartDate, Date))
                                    DateTime.TryParse(endDate, CType(studentChange.EndDate, Date))

                                Case StateEnum.LeaveOfAbsence
                                    startDate = CType(txtLOAStartDate.SelectedDate, String)
                                    endDate = CType(txtLOAEndDate.SelectedDate, String)
                                    Dim strErrorMessage As String = ""
                                    If startDate = "" Then
                                        strErrorMessage = "LOA Start Date is empty" & vbCrLf
                                    End If
                                    If endDate = "" Then
                                        strErrorMessage &= "LOA End Date is empty"
                                    End If
                                    If Not strErrorMessage = "" Then
                                        DisplayErrorMessage(strErrorMessage)
                                        Exit Sub
                                    End If
                                    DateTime.TryParse(startDate, CType(studentChange.DateOfChange, Date))
                                    DateTime.TryParse(startDate, CType(studentChange.StartDate, Date))
                                    DateTime.TryParse(endDate, CType(studentChange.EndDate, Date))
                                    studentChange.Reason = ddlLOAReasons.SelectedValue

                                Case StateEnum.CurrentlyAttending,
                                     StateEnum.AcademicProbation
                                    studentChange.DropReasonGuid = Nothing
                                    studentChange.DateOfReenrollment = CType(studentChange.DateOfChange, Date)
                                Case StateEnum.FutureStart

                                Case StateEnum.Dropped

                                    'Not allow from this page
                                    '            -------------------------
                                    'studentChange.DropReasonId =




                            End Select


                        Next

                        result = facade.UpdateStuEnrollmentStatusInBatch(statusChangeList)
                        If result <> "" Then
                            DisplayErrorMessage(result)
                            Exit Sub
                        End If
                    End If

                    '   notify user the students, whose status was not changed.
                    If doNotUpdateCtr > 0 Then
                        '   display list of students and their programs in a HTML page.
                        PrintReportHtml(BuildReportXML(arrStudents, arrPrgVersions, arrReason), "StuNotGraduatedReportHtml")
                    End If
                    If reEnrollCount > 0 Then
                        '   display list of students and their programs in a HTML page.
                        PrintReportHtml(BuildReportXML(arrStudentsReEnroll, arrPrgVersionsReEnroll, arrReason), "StuReEnrollReportHtml")

                        'TODO: Write to file
                    End If
                    If (doNotUpdateCtr = 0 And reEnrollCount = 0) Then
                        ''Modified by Saraswathi lakshmanan to fix issue De4038
                        ''QA: Student's registered class sections are being removed when the status is changed to No Start.
                        If newSysStatus = StateEnum.NoStart Then
                            DisplayInfoMessage("The student's status has been updated. If there are any registered class sections, they will be removed.")
                        Else
                            DisplayInfoMessage("The student's status has been updated.")
                        End If
                        '   notify user that everything went fine.
                        If newSysStatus = StateEnum.Suspended Then
                            pnlSuspendedDate.Visible = False
                            ddlStatusCodeId.SelectedIndex = 0
                        ElseIf newSysStatus = StateEnum.LeaveOfAbsence Then
                            pnlLOADate.Visible = False
                            ddlStatusCodeId.SelectedIndex = 0
                        ElseIf newSysStatus = StateEnum.CurrentlyAttending Then
                            ddlStatusCodeId.SelectedIndex = 0
                        End If
                    End If
                    '   get list of students after updating DB
                    'GetStudentsDS()

                    '   bind datagrid
                    BindDataGrid()
                    SetDisplay()
                End If

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                '   Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnChangeStatus_Click " & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnChangeStatus_Click " & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

        Private Function BuildReportXML(ByVal studentList As ArrayList, ByVal progVerList As ArrayList, ByVal reasonlist As ArrayList) As MemoryStream

            Dim myMemoryStream As New MemoryStream
            Dim printXmlTextWriter As New XmlTextWriter(myMemoryStream, New UTF8Encoding)
            '   write xml document header
            printXmlTextWriter.Formatting = Formatting.Indented
            printXmlTextWriter.Indentation = 0
            printXmlTextWriter.IndentChar = CType(Environment.NewLine, Char) '  ControlChars.Lf & ControlChars.Lf

            printXmlTextWriter.WriteStartDocument(False)

            '   output start of element "document"
            printXmlTextWriter.WriteStartElement("document")

            '   output start of element "studentsNoGraduated"
            printXmlTextWriter.WriteStartElement("studentsNoGraduated")

            Dim i As Integer

            For i = 0 To studentList.Count - 1
                '   output start of element "student"
                printXmlTextWriter.WriteStartElement("student")

                '   output start of element "counter"
                printXmlTextWriter.WriteStartElement("counter")
                printXmlTextWriter.WriteString(CType((i + 1), String))
                printXmlTextWriter.WriteEndElement()

                '   output start of element "studentName"
                printXmlTextWriter.WriteStartElement("studentName")
                printXmlTextWriter.WriteString(CType(studentList(i), String))
                printXmlTextWriter.WriteEndElement()

                '   output start of element "progVersion"
                printXmlTextWriter.WriteStartElement("progVersion")
                printXmlTextWriter.WriteString(CType(progVerList(i), String))
                printXmlTextWriter.WriteEndElement()

                printXmlTextWriter.WriteStartElement("Reason")
                printXmlTextWriter.WriteEndElement()
                'printXmlTextWriter.WriteString("The Student hasn't met the following requirements:")
                'printXmlTextWriter.WriteString("<br></br>")

                Dim docs() As String
                docs = Split(CType(reasonlist(i), String), ";")
                If docs.Length > 0 Then
                    For j As Integer = 0 To docs.Length - 1
                        printXmlTextWriter.WriteStartElement("documents")
                        printXmlTextWriter.WriteStartElement("docs")
                        printXmlTextWriter.WriteString(docs(j))
                        printXmlTextWriter.WriteEndElement()
                        printXmlTextWriter.WriteEndElement()
                    Next
                End If

                '   output end tag of element "student"
                printXmlTextWriter.WriteEndElement()
            Next

            '   output end tag of element "studentsNoGraduated"
            printXmlTextWriter.WriteEndElement()

            '   output end tag of element "document"
            printXmlTextWriter.WriteEndElement()

            'Write the XML to file and close the writer
            printXmlTextWriter.Flush()
            myMemoryStream.Position = 0

            Return myMemoryStream
        End Function


        Private Sub PrintReportHtml(ByVal xmlStream As MemoryStream, ByVal xslPageName As String)

            '   get XslStyleSheet 
            'Dim templateFilePath As String = Server.MapPath("..") + "/" + context.Request.Headers.Item("host") + "/Xsl/StuNotGraduatedReportHtml.xsl"
            Dim templateFilePath As String = Server.MapPath("..") + "/" + Context.Request.Headers.Item("host") + "/Xsl/" + xslPageName + ".xsl"

            '   generate HTML document
            Dim xslt As New XslCompiledTransform
            'Dim xslt As New XslTransform
            xslt.Load(templateFilePath)

            Dim xPathDoc As New XPathDocument(xmlStream)

            Dim documentMemoryStream As New MemoryStream

            Dim writer As New XmlTextWriter(documentMemoryStream, Nothing)

            writer.Formatting = Formatting.Indented

            xslt.Transform(xPathDoc, Nothing, writer, Nothing)

            '   save document and document type in sessions variables
            Session("DocumentMemoryStream") = documentMemoryStream
            Session("ContentType") = "text/html"

            '   Register a javascript to open the report in another window
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NewWindow", JAVASCRIPT, False)
        End Sub


        Protected Sub ddlStatusCodeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatusCodeId.SelectedIndexChanged
            '   set caption for date label/ hide date label and textbox
            Dim facade As New BatchStatusChangeFacade
            Dim oldSysStatus As Integer
            If ddlFromStatusCodeId.SelectedValue <> Guid.Empty.ToString And
               ddlStatusCodeId.SelectedValue <> Guid.Empty.ToString Then

                oldSysStatus = facade.GetSysStatus(ddlFromStatusCodeId.SelectedValue.ToString)
                Dim newSysStatus As Integer = facade.GetSysStatus(ddlStatusCodeId.SelectedValue.ToString)

                'start to change the grid to just show the students that has no attendance and no grades if the selected new status is no start
                If (newSysStatus = StateEnum.NoStart) Then

                    If Not (ViewState("dgSource") Is Nothing) Then
                        Dim dt = DirectCast(ViewState("dgSource"), DataTable)
                        Dim dclone As DataTable = dt.Clone()
                        Const expression As String = "(HasAttendance = 'NO' AND HasGrades = 'NO')"
                        Dim rows As DataRow() = dt.Select(expression)
                        For Each dataRow As DataRow In rows
                            dclone.ImportRow(dataRow)
                        Next
                        Dim dcloneView As New DataView(dclone)
                        dcloneView.Sort = "LastName ASC, FirstName ASC,  PrgVerDescrip ASC, StartDate ASC, ExpGradDate ASC"
                        Dim dcloneSorted As DataTable = dcloneView.ToTable()
                        With dgrdStuEnrollments
                            .DataSource = dcloneSorted
                            .DataBind()
                        End With
                        lblStudentCount.Text = "Total Student Enrollments: " & dcloneSorted.Rows.Count
                    End If
                Else
                    If Not (ViewState("dgSource") Is Nothing) Then

                        With dgrdStuEnrollments
                            .DataSource = DirectCast(ViewState("dgSource"), DataTable)
                            .DataBind()
                        End With
                        lblStudentCount.Text = "Total Student Enrollments: " & DirectCast(ViewState("dgSource"), DataTable).Rows.Count
                    End If
                End If
                'end to change the grid to just show the students that has no attendance and no grades

                If oldSysStatus = StateEnum.CurrentlyAttending And newSysStatus = StateEnum.FutureStart Then
                    dgrdStuEnrollments.Columns(12).Display = False      '   Re-enroll date
                    dgrdStuEnrollments.Columns(13).Display = True
                Else
                    dgrdStuEnrollments.Columns(12).Display = True      '   Re-enroll date
                    dgrdStuEnrollments.Columns(13).Display = False
                End If

                If (oldSysStatus = StateEnum.FutureStart And newSysStatus = StateEnum.FutureStart) Then
                    dgrdStuEnrollments.Columns(12).Display = False
                End If

                If oldSysStatus = StateEnum.CurrentlyAttending And (newSysStatus = StateEnum.FutureStart Or newSysStatus = StateEnum.NoStart) Then
                    dgrdStuEnrollments.Columns(9).Display = True 'hasattendance
                    dgrdStuEnrollments.Columns(10).Display = True 'hasgrades
                Else
                    dgrdStuEnrollments.Columns(9).Display = False 'hasattendance
                    dgrdStuEnrollments.Columns(10).Display = False 'hasgrades
                End If

                'If (oldSysStatus <> 12 And oldSysStatus <> 19) Then

                pnlDate.Visible = False
                txtDate.SelectedDate = CType(Date.Now.ToShortDateString, Date?)

                If newSysStatus = StateEnum.CurrentlyAttending Then

                    Select Case oldSysStatus
                        Case StateEnum.LeaveOfAbsence
                            lblDate.Text = "LOA Return Date"
                            pnlDate.Visible = True

                        Case StateEnum.Suspended
                            lblDate.Text = "Suspension End Date"
                            pnlDate.Visible = True

                        Case StateEnum.Graduated
                            lblDate.Text = "Revised Graduation Date"
                            pnlDate.Visible = True

                            'Case StateEnum.Dropped
                            '    lblDate.Text = "Reenroll Date"
                            '    pnlDate.Visible = True

                        Case StateEnum.TransferOut
                            lblDate.Text = "Re-Enroll Date"
                            pnlDate.Visible = True

                    End Select

                ElseIf newSysStatus = StateEnum.Graduated Then

                    Select Case oldSysStatus
                        Case StateEnum.CurrentlyAttending
                            lblDate.Text = "Graduation Date"
                            pnlDate.Visible = True
                        Case StateEnum.Externship
                            lblDate.Text = "Graduation Date"
                            pnlDate.Visible = True
                        Case StateEnum.Graduated
                            lblDate.Text = "Graduation Date"
                            pnlDate.Visible = True

                    End Select
                ElseIf newSysStatus = StateEnum.Suspended Then
                    pnlSuspendedDate.Visible = True
                    pnlLOADate.Visible = False
                    pnlDate.Visible = False
                ElseIf newSysStatus = StateEnum.LeaveOfAbsence Then
                    pnlSuspendedDate.Visible = False
                    pnlLOADate.Visible = True
                    pnlDate.Visible = False

                    '   bind DDL
                    With ddlLOAReasons
                        .DataTextField = "Descrip"
                        .DataValueField = "LOAReasonId"
                        .DataSource = facade.GetLOAReasons(CampusId)
                        'End If
                        .DataBind()
                        .Items.Insert(0, New ListItem("Select", ""))
                        .SelectedIndex = 0
                    End With

                End If

                'End If
            Else

                pnlDate.Visible = False
            End If

            If oldSysStatus = StateEnum.Dropped Or StateEnum.NoStart Then SetDisplay()


            'dgrdStuEnrollments.Visible = True
            'lblStudentCount.Visible = True

        End Sub
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnBuildList)
            controlsToIgnore.Add(btnChangeStatus)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        End Sub

        Private Sub SetDisplay()

            Dim facade As New BatchStatusChangeFacade
            Dim selectedStatus As Integer = facade.GetSysStatus(ddlStatusCodeId.SelectedValue.ToString)
            If selectedStatus = StateEnum.FutureStart Or selectedStatus = StateEnum.CurrentlyAttending Then
                Dim iitems As GridDataItemCollection = dgrdStuEnrollments.Items
                Dim iitem As GridDataItem
                Dim dateDetermined As String
                Dim rdp As RadDatePicker

                For i As Integer = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    dateDetermined = CType(iitem.FindControl("lblDroppedDate"), Label).Text
                    If Not dateDetermined.ToString = "" Then

                        rdp = CType(iitem.FindControl("txtreenrolldate"), RadDatePicker)
                        rdp.MinDate = CDate(dateDetermined)
                        rdp.MaxDate = CDate(dateDetermined).AddDays(180)

                    End If

                    Dim sysStatusId As String = CType(iitem.FindControl("lblSysStatusId"), Label).Text
                    If Not String.IsNullOrEmpty(sysStatusId) Then
                        If sysStatusId = "8" Then
                            Dim startDate As String = CType(iitem.FindControl("lblEnrollStartDate"), Label).Text
                            If Not String.IsNullOrEmpty(startDate) Then
                                rdp = CType(iitem.FindControl("txtreenrolldate"), RadDatePicker)
                                Dim enrollstartDate = rdp.SelectedDate

                                Dim MinDate = CDate(startDate)
                                Dim MaxDate = CDate(startDate).AddDays(180)
                                If enrollstartDate > MinDate And enrollstartDate < MaxDate Then
                                    rdp = CType(iitem.FindControl("txtreenrolldate"), RadDatePicker)
                                    rdp.MinDate = CDate(startDate)
                                    rdp.MaxDate = CDate(startDate).AddDays(180)
                                Else
                                    rdp.MinDate = CDate(enrollstartDate)
                                    rdp.MaxDate = CDate(enrollstartDate).AddDays(180)
                                End If

                            End If
                        End If
                    End If

                    Dim rdpNewStart As RadDatePicker
                    rdpNewStart = CType(iitem.FindControl("txtnewStartDate"), RadDatePicker)
                    rdpNewStart.MinDate = DateTime.Today

                Next

            End If

        End Sub

        Protected Sub dgrdStuEnrollments_PreRender(sender As Object, e As EventArgs) Handles dgrdStuEnrollments.PreRender
            Dim facade As New BatchStatusChangeFacade
            Dim newStatusCode = facade.GetSysStatus(ddlStatusCodeId.SelectedValue.ToString)
            Dim oldStatecode = facade.GetSysStatus(ddlFromStatusCodeId.SelectedValue.ToString)

            If newStatusCode = StateEnum.FutureStart And oldStatecode = StateEnum.FutureStart Then
                dgrdStuEnrollments.Columns(12).Visible = False
            Else
                dgrdStuEnrollments.Columns(12).Visible = True
            End If


            Dim attgradesEnabled = False
            If (newStatusCode = StateEnum.FutureStart Or newStatusCode = StateEnum.NoStart) And oldStatecode = StateEnum.CurrentlyAttending Then
                attgradesEnabled = True
            Else

            End If
            For Each col As GridColumn In dgrdStuEnrollments.MasterTableView.RenderColumns
                If col.UniqueName = "hasattendance" Then
                    col.Display = attgradesEnabled
                End If
                If col.UniqueName = "hasgrades" Then
                    col.Display = attgradesEnabled
                End If
            Next

            If newStatusCode = StateEnum.FutureStart And oldStatecode = StateEnum.CurrentlyAttending Then
                For Each col As GridColumn In dgrdStuEnrollments.MasterTableView.RenderColumns
                    If col.UniqueName = "reenrolldate" Then
                        'If (col.ColumnType & "GridTemplateColumn") AndAlso (col.UniqueName & "Edit") Then
                        col.Display = False
                    End If
                    If col.UniqueName = "newstartdate" Then
                        'If (col.ColumnType & "GridTemplateColumn") AndAlso (col.UniqueName & "Edit") Then
                        col.Display = True
                    End If
                Next
            Else
                If newStatusCode = StateEnum.FutureStart Or (newStatusCode = StateEnum.CurrentlyAttending And oldStatecode = StateEnum.Dropped) Then
                    For Each col As GridColumn In dgrdStuEnrollments.MasterTableView.RenderColumns
                        If col.UniqueName = "reenrolldate" Then
                            col.Display = True
                        End If
                    Next
                Else
                    For Each col As GridColumn In dgrdStuEnrollments.MasterTableView.RenderColumns
                        If col.UniqueName = "reenrolldate" Then
                            col.Display = False
                        End If
                    Next
                End If
            End If

        End Sub

        Protected Sub dgrdStuEnrollments_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles dgrdStuEnrollments.ItemDataBound
            Dim dataItem As GridDataItem = TryCast(e.Item, GridDataItem)
            If (dataItem IsNot Nothing) Then
                Dim previousSysStatusId As Integer
                Dim fromSysStatusId As StateEnum = CType((New BatchStatusChangeFacade).GetSysStatus(ddlFromStatusCodeId.SelectedValue), StateEnum)
                Select Case fromSysStatusId
                    Case StateEnum.LeaveOfAbsence
                        previousSysStatusId = CType(CType(dataItem.FindControl("lblLoaPreviousSysStatusID"), Label).Text, Integer)
                        If (previousSysStatusId = StateEnum.AcademicProbation) Then
                            dataItem.CssClass = "datagriditemstyleHihglighted"
                            ShowAcademicProbationMessage = True
                        Else
                            dataItem.CssClass = "datagriditemstyle"
                        End If

                    Case StateEnum.Suspended
                        ' As some records have a previous status as null (Not logical but is a fact) so label lblSusPreviousSysStatusID is empty
                        ' so this case we suppose for this case previous status was “Currently Attendding”
                        If (CType(dataItem.FindControl("lblSusPreviousSysStatusID"), Label).Text = String.Empty) Then
                            previousSysStatusId = 9 'Currently Attending 
                        Else
                            previousSysStatusId = CType(CType(dataItem.FindControl("lblSusPreviousSysStatusID"), Label).Text, Integer)
                        End If
                        If (previousSysStatusId = StateEnum.AcademicProbation) Then
                            dataItem.CssClass = "datagriditemstyleHihglighted"
                            ShowAcademicProbationMessage = True
                        Else
                            dataItem.CssClass = "datagriditemstyle"
                        End If
                End Select
            End If

        End Sub

        Protected Function GetReenRollDate(ByVal dat As DateTime) As String

            If (dat < DateTime.Parse("1/1/1950")) Then
                Return "1/1/2000"
            End If
            Return dat.ToString()
        End Function

        Protected Function GetMinDate() As DateTime

            Dim dt As DateTime = DateTime.Now.AddDays(-1)

            Return dt
        End Function
    End Class
End Namespace