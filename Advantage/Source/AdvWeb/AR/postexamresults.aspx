﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostExamResults.aspx.vb" Inherits="postexamresults" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <script src="../Scripts/ElementQueries.min.js"></script>
    <script src="../Scripts/ResizeSensor.min.js"></script>
    <script id="resizeScript" type="text/javascript">
        var $mainContainer;
        var $mainContent;
        var $containerScrollable;
        $(document).ready(function () {
            $mainContainer = $(".main-container");
            $mainContent = $(".main-content-responsive");
            $containerScrollable = $(".container-scrollable");

            resizeContainers();
            new ResizeSensor($mainContainer, function () {
                resizeContainers();
            });

            function resizeContainers() {
                $mainContent.innerHeight($mainContainer.innerHeight());
                $mainContent.innerWidth($mainContainer.innerWidth());
            }
        });

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <div class="container-scrollable" style="overflow: auto;">

        <div class="float-static">
            <div class="static">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="hidden"></asp:Button>
                            <input id="saveLoad" value="Save" type="button" class="save" onclick="javascript: save();" />
                            <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                Enabled="False"></asp:Button></td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- end top menu (save,new,reset,delete,history)-->
        <!--begin right column-->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
            align="center">
            <tr>
                <td class="detailsframe">
                    <div class="scrollsingleframe">
                        <div class="boxContainer full">
                            <h3><%=Header.Title  %></h3>
                            <table class="contenttable" cellspacing="10" cellpadding="0" width="50%" align="center">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblEnrollmentId" runat="server" CssClass="font-blue-darken-4">Enrollment</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEnrollmentId" runat="server"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="label1" runat="server" CssClass="font-blue-darken-4">Type</asp:Label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlTypes" runat="server" AutoPostBack="true">
                                            <asp:ListItem Value="501">Exams</asp:ListItem>
                                            <asp:ListItem Value="502">Final</asp:ListItem>
                                            <asp:ListItem Value="533">Practical Exams</asp:ListItem>
                                            <asp:ListItem Value="499">Homework</asp:ListItem>
                                            <asp:ListItem Value="544">Externship</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td>
                                        <asp:Label ID="label2"
                                            runat="server" CssClass="font-blue-darken-4">Term</asp:Label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlTerms" runat="server" AutoPostBack="true">
                                            <asp:ListItem Value="00000000-0000-0000-0000-000000000000">Select</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table>
                            <tr>
                                <td class="contentcell" align="center">
                                    <div class="boxContainer">
                                        <table role="grid">
                                            <asp:Repeater ID="rPostExamResults" runat="server" EnableViewState="False" >
                                                <HeaderTemplate>
                                                    <thead class="k-grid-header ">
                                                        <th class="k-grid-header " style="text-align: center; padding: .5em .6em .4em 0; border-left: 1px solid #E0E0E0; border-top: 1px solid #E0E0E0;border-bottom: 1px solid #E0E0E0">Term</th>
                                                        <th class="k-grid-header " style="text-align: center; padding: .5em .6em .4em 0; border-left: 1px solid #E0E0E0; border-top: 1px solid #E0E0E0;border-bottom: 1px solid #E0E0E0">Course</th>
                                                        <th class="k-grid-header " style="padding: .5em .6em .4em .5em; border-left: 1px solid #E0E0E0; border-top: 1px solid #E0E0E0;border-bottom: 1px solid #E0E0E0">Name</th>
                                                        <th class="k-grid-header" style="padding: .5em .6em .4em .5em; border: 1px solid #E0E0E0">
                                                            <asp:PlaceHolder ID="pllHeader" runat="server" />
                                                        </th>
                                                    </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 16%; border: 0px; padding-left: 8px" class="datagriditemstyle">
                                                            <%# DataBinder.Eval(Container.DataItem, "GrpDescrip")%>
                                                            <asp:TextBox ID="tbTermId" Text='<%#DataBinder.Eval(Container.DataItem, "TermId")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbDBId" Text='<%#DataBinder.Eval(Container.DataItem, "DBId")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbDBIdName" Text='<%#DataBinder.Eval(Container.DataItem, "DBIdName")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbClsSectionId" Text='<%#DataBinder.Eval(Container.DataItem, "ClsSectionId")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbOriginalScore" Text='<%#DataBinder.Eval(Container.DataItem, "Score")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbOriginalPostDate" Text='<%#DataBinder.Eval(Container.DataItem, "PostDate", "{0:d}")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbInstrGrdBkWgtDetailId" Text='<%#DataBinder.Eval(Container.DataItem, "InstrGrdBkWgtDetailId")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbType" Text='<%#DataBinder.Eval(Container.DataItem, "Type")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbStartDate" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbEndDate" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate", "{0:d}")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbMinScore" Text='<%#DataBinder.Eval(Container.DataItem, "MinSCore")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbMaxScore" Text='<%#DataBinder.Eval(Container.DataItem, "MaxSCore")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="txtNumber" Text='<%#DataBinder.Eval(Container.DataItem, "Number")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 15%; border: 0px; padding-left: 15px" class="datagriditemstyle">
                                                            <%#DataBinder.Eval(Container.DataItem, "Subject")%>
                                                        </td>
                                                        <td style="width: 38%; border: 0px; padding-left: 5px" class="datagriditemstyle">
                                                            <asp:Label ID="lblCompDescrip" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Name")%>'></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; border: 0px; text-align: center" class="datagriditemstyle">
                                                            <asp:PlaceHolder ID="pllDynamic" runat="server" />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <AlternatingItemTemplate>
                                                    <tr>
                                                        <td style="width: 16%; border: 0px; padding-left: 8px" class="datagridalternatingstyle">
                                                            <%# DataBinder.Eval(Container.DataItem, "GrpDescrip")%>
                                                            <asp:TextBox ID="tbTermId" Text='<%#DataBinder.Eval(Container.DataItem, "TermId")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbDBId" Text='<%#DataBinder.Eval(Container.DataItem, "DBId")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbClsSectionId" Text='<%#DataBinder.Eval(Container.DataItem, "ClsSectionId")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbInstrGrdBkWgtDetailId" Text='<%#DataBinder.Eval(Container.DataItem, "InstrGrdBkWgtDetailId")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbType" Text='<%#DataBinder.Eval(Container.DataItem, "Type")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbStartDate" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbEndDate" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate", "{0:d}")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbMinScore" Text='<%#DataBinder.Eval(Container.DataItem, "MinSCore")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="tbMaxScore" Text='<%#DataBinder.Eval(Container.DataItem, "MaxSCore")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                            <asp:TextBox ID="txtNumber" Text='<%#DataBinder.Eval(Container.DataItem, "Number")%>'
                                                                Visible="false" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 15%; border: 0px; padding-left: 15px" class="datagridalternatingstyle">
                                                            <%#DataBinder.Eval(Container.DataItem, "Subject")%>
                                                        </td>
                                                        <td style="width: 38%; border: 0px; padding-left: 5px" class="datagridalternatingstyle">
                                                            <asp:Label ID="lblCompDescrip" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Name")%>'></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; border: 0px; text-align: center" class="datagridalternatingstyle">
                                                            <asp:PlaceHolder ID="pllDynamic" runat="server" />
                                                        </td>
                                                    </tr>
                                                </AlternatingItemTemplate>


                                            </asp:Repeater>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!--end table content-->



                    </div>

                </td>
            </tr>
        </table>

        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>
    <script>

        function save() {
            show = true
            var pageLoading = $(document).find(".page-loading");
            if (pageLoading.length === 0)
                $(document).find("body").append('<div class="page-loading"></div>');

            kendo.ui.progress($(".page-loading"), show);
            $('#<%=btnSave.ClientID%>').click();
        }


    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
