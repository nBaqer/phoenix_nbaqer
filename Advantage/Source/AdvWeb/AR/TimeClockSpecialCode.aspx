<%@ Page Language="vb" AutoEventWireup="false" Inherits="TimeClockSpecialCode" CodeFile="TimeClockSpecialCode.aspx.vb"
    MasterPageFile="~/NewSite.master" Title="Punch Codes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstTimeClockSpecialCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstTimeClockSpecialCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstTimeClockSpecialCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstTimeClockSpecialCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
            Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="15%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radStatus" CssClass="Label" AutoPostBack="true" runat="Server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframetop2">
                            <div id="grdWithScroll" class="scrollleftfilters" onscroll="SetDivPosition()">
                                <asp:DataList ID="dlstTimeClockSpecialCode" runat="server">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" runat="server" CausesValidation="False" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
                                            CommandArgument='<%# Container.DataItem("TCSId")%>' ImageUrl="../images/Inactive.gif" />
                                        <asp:ImageButton ID="imgActive" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>'
                                            CommandArgument='<%# Container.DataItem("TCSId")%>' ImageUrl="../images/Active.gif" />
                                        <asp:LinkButton ID="Linkbutton1" CssClass="itemstyle" CausesValidation="False" runat="server"
                                            CommandArgument='<%# Container.DataItem("TCSId")%>' Text='<%# Container.DataItem("TCSpecialCode")%>' />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
                Orientation="HorizontalTop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False">
                            </asp:Button>
                            <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
                <!-- end top menu (save,new,reset,delete,history)-->
                <!--begin right column-->
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <div class="scrollright2">
                                <!-- begin table content-->
                                <asp:Panel ID="pnlRHS" runat="server">
                                    <table width="60%" align="center" class="contenttable">
                                        <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                        <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                        <asp:TextBox ID="txtTCSId" CssClass="Label" runat="server" MaxLength="128" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID="txtOrigTCSId" CssClass="Label" runat="server" MaxLength="128" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID="txtOrigTCSpecialCode" runat="server" Visible="False">TCSpecialCode</asp:TextBox>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblTCSPunchType" CssClass="label" runat="server">Punch Type<font color="red">*</font></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:RadioButtonList ID="rblPunchType" runat="server" CssClass="radiobuttonlist"
                                                    CellPadding="2" RepeatColumns="2" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="In" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Out" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                <asp:RequiredFieldValidator ID="revRblPunchType" runat="server" ControlToValidate="rblPunchType"
                                                    Display="None" ErrorMessage="Punch Type is Required">Punch Type is Required</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblTCSpecialCode" runat="server" CssClass="label">Code<font color="red">*</font></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtTCSpecialCode" runat="server" CssClass="textbox" MaxLength="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqTCSpecialCode" runat="server" ControlToValidate="txtTCSpecialCode"
                                                    Display="None" ErrorMessage="Code is Required">Code is Required</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revTCSpecialCode" runat="server" ControlToValidate="txtTCSpecialCode"
                                                    Display="None" ErrorMessage="too many characters" ValidationExpression=".{0,50}"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInstructionType" CssClass="label" runat="server">Instruction Type<font color="red">*</font></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlInstructionType" runat="server" CssClass="DropDownList">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="InstructionTypeCompareValidator" runat="server" ErrorMessage="Must Select an Instruction Type"
                                                    Display="None" ControlToValidate="ddlInstructionType" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select an Instruction Type</asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampId" CssClass="label" runat="server">Campus<font color="red">*</font></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlCampusId" runat="server" CssClass="DropDownList">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CampusCompareValidator" runat="server" ErrorMessage="Must Select a Campus"
                                                    Display="None" ControlToValidate="ddlCampusId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Campus</asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <!--end table content-->
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
    </div>
</asp:Content>
