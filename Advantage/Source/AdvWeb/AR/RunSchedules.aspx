﻿<%@ Page Title="Run Schedule Process" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="RunSchedules.aspx.vb" Inherits="AR_RunSchedules" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script language="javascript" src="../js/AuditHist.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
    <style type="text/css">
        div.RadWindow_Web20 a.rwCloseButton {
            margin-right: 20px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadWindowManager ID="ScheduleConflictWindow" runat="server" Behaviors="Default" InitialBehaviors="None" Visible="false">
        <Windows>
            <telerik:RadWindow ID="DialogWindow" Behaviors="Close" VisibleStatusbar="false"
                ReloadOnShow="true" Modal="true" runat="server" Height="400px" Width="700px"
                VisibleOnPageLoad="false" Top="20" Left="20">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">

        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="300" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <div class="titlefirstmargin" id="trTerm" runat="server">
                <asp:Label ID="lblPrgVerId" CssClass="label" runat="server">Program</asp:Label>
            </div>
            <div class="contentmargin" id="trTerm1" runat="server">
                <asp:DropDownList ID="ddlProgId" runat="server" AutoPostBack="true" Width="95%" CssClass="dropdownlist">
                </asp:DropDownList>
            </div>
            <div class="titlemargin" id="trCohortStartDate" runat="server">
                <asp:Label ID="lblCampusId" CssClass="label" runat="server">Term</asp:Label>
            </div>
            <div class="contentmargin" id="trCohortStartDate1" runat="server">
                <asp:DropDownList ID="ddlTermId" runat="server" AutoPostBack="false" Width="95%" CssClass="dropdownlist">
                </asp:DropDownList>

            </div>

            <div class="titlemargin" id="Div1" runat="server">
                <asp:Label ID="label1" CssClass="label" runat="server">Student Group</asp:Label>
            </div>
            <div class="contentmargin" id="Div2" runat="server">
                <asp:DropDownList ID="ddlLeadGrp" runat="server" AutoPostBack="False" Width="95%" CssClass="dropdownlist"
                    DataTextField="Descrip" DataValueField="LeadGrpId">
                </asp:DropDownList>

            </div>
            <div class="titlemargin" id="Div3" runat="server">
                <asp:Button ID="btnGetList" runat="server"
                    Text="Get Student List"></asp:Button>
            </div>




        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="False"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="False"></asp:Button>
                    </td>
                </tr>
            </table>

            <div class="boxContainer">
                <h3><%=Header.Title  %></h3>
                <div style="margin: 0px 5px 5px 5px;">
                    <br />
                    <asp:Button ID="btnBuildList" runat="server" Text="Run the Scheduling Process" Visible="false"></asp:Button>
                </div>

                <div style="margin: 0px 5px 0px 5px;">
                    <!-- begin content table-->
                    <asp:Panel ID="pnlrhs" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%" align="center">

                            <tr>
                                <td>
                                    <asp:Panel ID="pnlRegisteredStudents" runat="server" Visible="false">
                                        <asp:Label ID="Label2" runat="server" CssClass="labelbold" Visible="false">The following students were successfully scheduled:</asp:Label>
                                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" AllowSorting="true"
                                            CellPadding="0" BorderWidth="1px" BorderColor="#E0E0E0" Width="100%">
                                            <HeaderStyle CssClass="datagridheader" Wrap="True"></HeaderStyle>
                                            <RowStyle CssClass="datagriditemstyle" />
                                            <AlternatingRowStyle CssClass="datagridalternatingstyle" />
                                            <Columns>


                                                <asp:TemplateField HeaderText="Program" SortExpression="Program">
                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtStuEnrollId" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("StuEnrollId") %>' />
                                                        <asp:Label ID="lblProgram" Text='<%# Container.DataItem("Program")%>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Course Code" SortExpression="Code">
                                                    <HeaderStyle CssClass="datagridheader" Width="10%" Wrap="false"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" Text='<%# Container.DataItem("Code") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Course" SortExpression="Course">
                                                    <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCourse" Text='<%# Container.DataItem("Course") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section" SortExpression="Section">
                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtClsSectionId" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("ClsSectionId") %>' />
                                                        <asp:Label ID="lblSection" Text='<%# Container.DataItem("Section") %>' runat="server" />
                                                        <asp:Label ID="lblTermId" Text='<%# Container.DataItem("TermId") %>' runat="server" Visible="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start" SortExpression="StartDate">
                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStartDate" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>' CssClass="Label" runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End" SortExpression="EndDate">
                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEndDate" Text='<%# DataBinder.Eval(Container.DataItem,"EndDate", "{0:d}") %>' CssClass="Label" runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name" SortExpression="LastName" ItemStyle-Wrap="false">
                                                    <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLastName" Text='<%# Container.DataItem("LastName")%>' CssClass="Label" runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" SortExpression="FirstName" ItemStyle-Wrap="false">
                                                    <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFirstName" Text='<%# Container.DataItem("FirstName")%>' CssClass="Label" runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unschedule">
                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderTemplate>
                                                        <!--<asp:Label Id="Label1" cssclass = "Label" Runat="server" Text='Copy' /><br>-->
                                                        <input id="chkAllItems" checked type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkCopy',
        document.forms[0].chkAllItems.checked)" />Check All
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkCopy" runat="server" Text='' Checked />
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr height="10">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblRegisterStudents" runat="server" CssClass="labelbold" Visible="false">The following students are not scheduled</asp:Label>
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowSorting="true"
                                        CellPadding="0" BorderWidth="1px" BorderColor="#E0E0E0" Width="100%">
                                        <HeaderStyle CssClass="datagridheader" Wrap="True"></HeaderStyle>
                                        <RowStyle CssClass="datagriditemstyle" />
                                        <AlternatingRowStyle CssClass="DataGridAlternatingStyle" />
                                        <Columns>


                                            <asp:TemplateField HeaderText="Program" SortExpression="Program">
                                                <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtStuEnrollId" CssClass="Label" runat="server" Visible="False" Text='<%# Container.DataItem("StuEnrollId") %>' />
                                                    <asp:Label ID="lblProgram" Text='<%# Container.DataItem("Program")%>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Course Code" SortExpression="Code">
                                                <HeaderStyle CssClass="datagridheader" Width="10%" Wrap="false"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCode" Text='<%# Container.DataItem("Code") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Course" SortExpression="Course">
                                                <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCourse" Text='<%# Container.DataItem("Course") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Section" SortExpression="Section">
                                                <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtClsSectionId" CssClass="label" runat="server" Visible="False" Text='<%# Container.DataItem("ClsSectionId") %>' />
                                                    <asp:Label ID="lblSection" Text='<%# Container.DataItem("Section") %>' runat="server" />
                                                    <asp:Label ID="lblTermId" Text='<%# Container.DataItem("TermId") %>' runat="server" Visible="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start" SortExpression="StartDate">
                                                <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStartDate" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>' CssClass="label" runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End" SortExpression="EndDate">
                                                <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEndDate" Text='<%# DataBinder.Eval(Container.DataItem,"EndDate", "{0:d}") %>' CssClass="label" runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name" SortExpression="LastName" ItemStyle-Wrap="false">
                                                <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastName" Text='<%# Container.DataItem("LastName")%>' CssClass="label" runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="First Name" SortExpression="FirstName" ItemStyle-Wrap="false">
                                                <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFirstName" Text='<%# Container.DataItem("FirstName")%>' CssClass="label" runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unschedule">
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <HeaderTemplate>
                                                    <!--<asp:Label Id="Labelx1" cssclass = "Label" Runat="server" Text='Copy' /><br>-->
                                                    <input id="chkAllItems" checked type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkCopy',
        document.forms[0].chkAllItems.checked)" />Check All
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkCopy" runat="server" Text='' Checked />
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>


                        </table>
                    </asp:Panel>


                    <!-- end content table-->
                </div>
            </div>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

