<%@ Page Language="vb" AutoEventWireup="false" CodeFile="StartDateClasses.aspx.vb"
    Inherits="StartDateClasses" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Setup Start Date For Classes</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link rel="stylesheet" type="text/css" href="../css/localhost_lowercase.css" />
    <link href="../CSS/systememail.css" type="text/css" rel="stylesheet" />
    <link href="../css/resume.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <!-- beging header -->
    <!-- end header -->
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="toppopupheader">
                <img src="../images/advantage_module_starts.jpg">
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
    </table>
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="detailsframetop">
                <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="true">
                            </asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" Enabled="False"
                                CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" Text="Delete"
                                    CssClass="delete" Enabled="False" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                </table>
                <div class="scrollwholeadmreq" style="padding: 20px 6px 6px 6px">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 50%; padding: 6px; vertical-align: top">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="threecolumnheader">
                                            <asp:Label ID="label5" CssClass="labelbold" runat="server" Text="Available Classes With Overlapping Dates"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ListBox ID="lbxAvailable" runat="server" SelectionMode="Multiple" CssClass="listboxes"
                                                Height="75px"></asp:ListBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 5px; text-align: center">
                                            <asp:Button ID="btnAddAvailableClasses" runat="server" Text="Add Selected Classes"
                                                Width="200px" CausesValidation="False"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 50%; padding: 6px; vertical-align: top">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="Label3" runat="server" CssClass="label">Non-Term Start Date</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell" style="text-align: left">
                                            <asp:Label ID="lblTermDescrip" runat="server" CssClass="label">Label</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="Label4" runat="server" CssClass="label">Program</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell" style="text-align: left">
                                            <asp:Label ID="lblProgramDescrip" runat="server" CssClass="label">Label</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="Label1" runat="server" CssClass="label">Grade Scale<span style="COLOR: red">*</span></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlGradeScaleId" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="Label2" runat="server" CssClass="label">Shift<span style="COLOR: red">*</span></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlShiftId" runat="server" CssClass="dropdownlist"
                                                Enabled="False">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="padding-top: 16px">
                                <asp:Panel ID="pnlAttendance" Width="100%" runat="server" CssClass="label">
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    <!-- begin footer -->
    <!-- end footer -->
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
            runat="server">
        </asp:Panel>
    <asp:RequiredFieldValidator ID="rfvGradeScale" runat="server" ErrorMessage="Grade Scale is required"
        Display="None" ControlToValidate="ddlGradeScaleId"></asp:RequiredFieldValidator><asp:RequiredFieldValidator
            ID="rfvShift" runat="server" ErrorMessage="Shift is required" Display="None"
            ControlToValidate="ddlShiftId"></asp:RequiredFieldValidator><asp:ValidationSummary
                ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
    </form>
</body>
</html>
