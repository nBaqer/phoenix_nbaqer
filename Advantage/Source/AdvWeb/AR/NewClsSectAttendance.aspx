<%@ Page Title="Class Section Attendance" Language="vb" AutoEventWireup="false" Inherits="ClsSectAttendance" MasterPageFile="~/NewSite.master" CodeFile="NewClsSectAttendance.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

    <style type="text/css">
        .LegendLabel {
            font: normal 10px verdana;
            color: #000066;
            background-color: transparent;
        }

        div.RadWindow_Web20 a.rwCloseButton {
            margin-right: 20px;
        }
    </style>




    <script type="text/javascript">
        function SetHiddenText() {
            //document.forms[0].HiddenText.value = "YES";
            if (document.getElementById('<%= HiddenText.ClientID %>') != null) {
                document.getElementById('<%= HiddenText.ClientID %>').value = "YES";
            }


        }
        function confirmPostZero(button) {
            function aspButtonCallbackFn(arg) {
                if (arg) {
                    __doPostBack(button.name, "");
                    // eval(button.href);
                }
            }
            radconfirm("Are you sure you want to post zeros for all students listed in this page?", aspButtonCallbackFn, 330, 110, null, "Confirm");
        }

        function confirmPostException(button) {
            function aspButtonCallbackFn(arg) {
                if (arg) {
                    __doPostBack(button.name, "");
                    // eval(button.href);
                }
            }
            radconfirm("Are you sure you want to post the maximum scheduled attendance values for all students listed in this page?", aspButtonCallbackFn, 330, 110, null, "Confirm");
        }

        function closeRadWindow() {
            Sys.Application.remove_load(closeRadWindow);
            var oWnd = $find("<%= UserListDialog.ClientID%>");
            if (oWnd) {
                setTimeout(function () {
                    oWnd.close();
                }, 0);
            }
        }

    </script>

    <style type="text/css">
        .style1 {
            background-position: left top;
            background-image: url('../Images/left_gradient.gif');
            border-bottom: #000000 1px solid;
            border-top: #000000 1px solid;
            background-repeat: no-repeat;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <telerik:RadWindowManager runat="server" ID="RadWindowManager1">
    </telerik:RadWindowManager>

    <telerik:RadInputManager ID="RadInputManager1" EnableEmbeddedBaseStylesheet="false"
        runat="server">
        <telerik:TextBoxSetting BehaviorID="StringBehavior" InitializeOnClient="true" EmptyMessage="type here" />
        <telerik:NumericTextBoxSetting BehaviorID="CurrencyBehavior" EmptyMessage="type.."
            Type="Currency" Validation-IsRequired="true" MinValue="1" InitializeOnClient="true" MaxValue="100000" />
        <telerik:NumericTextBoxSetting InitializeOnClient="true" BehaviorID="NumberBehavior" EmptyMessage="type.."
            Type="Number" DecimalDigits="0" MinValue="0" MaxValue="100" />
    </telerik:RadInputManager>

   <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="AttendanceGridDiv">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AttendanceGridDiv" LoadingPanelID="RadAjaxLoadingPanel1"  />                
                    <telerik:AjaxUpdatedControl ControlID="UserListDialog" LoadingPanelID="RadAjaxLoadingPanel1"  />               
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="dgAttendance">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAttendance" LoadingPanelID="RadAjaxLoadingPanel1"  />
                    <telerik:AjaxUpdatedControl ControlID="lblOutput" />
                    <telerik:AjaxUpdatedControl ControlID="lblOutputBottom" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            
        <%--<telerik:AjaxSetting AjaxControlID="attendanceGridUpdate">
                <UpdatedControls>
                   <telerik:AjaxUpdatedControl ControlID="attendanceGridUpdate" />
                    <telerik:AjaxUpdatedControl ControlID="lblOutput" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            
              <%--<telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="attendanceGridUpdate" LoadingPanelID="RadAjaxLoadingPanel1"/>
                  <telerik:AjaxUpdatedControl ControlID="lblOutput" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <%--    
            <telerik:AjaxSetting AjaxControlID="GridView1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridView1" />
                    <telerik:AjaxUpdatedControl ControlID="lblOutput" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>

           <%-- <telerik:AjaxSetting AjaxControlID="btnPreviousWeek">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="attendanceGridUpdate" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblOutput" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <%--<telerik:AjaxSetting AjaxControlID="btnNextWeek">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="attendanceGridUpdate" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblOutput" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>

    <%--<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/loadingad.gif" />
    </telerik:RadAjaxLoadingPanel>--%>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Style="position: absolute; top: 17%; left: 24%; height: 80%; width: 80%;" IsSticky="true">
    </telerik:RadAjaxLoadingPanel>

    <script type="text/javascript">
        Telerik.Web.UI.RadWindowUtils.Localization =
{
    "Close": "Close",
    "Minimize": "Minimize",
    "Maximize": "Maximize",
    "Reload": "Reload",
    "PinOn": "Pin on",
    "PinOff": "Pin off",
    "Restore": "Restore",
    "OK": "Yes",
    "Cancel": "No"
};
    </script> 

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="380" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>

            <div class="titlefirstmargin" id="trTerm" runat="server">
                <asp:Label ID="lblTerm" CssClass="label" runat="server">Term</asp:Label>
            </div>
            <div class="contentmargin" id="trTerm1" runat="server">
                <telerik:RadComboBox ID="ddlTerms" runat="server" DataTextField="TermDescrip" DataValueField="TermId"
                    AutoPostBack="True" CausesValidation="False" AppendDataBoundItems="true" Filter="Contains" EmptyMessage="Select" CssClass="dropdownlist" Width="95%" TabIndex="1">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label1" Text='<%# Eval("TermDescrip")%>' ></asp:Label>
                        <telerik:RadToolTip ID="RadToolTipLabel1" runat="server" TargetControlID="Label1" Width="400px" Position="BottomRight" Text='<%# Eval("TermDescrip")%>' IsClientID="false"  >
                        </telerik:RadToolTip>
                    </ItemTemplate>
                </telerik:RadComboBox>
            </div>

            <div class="titlemargin" id="trCohortStartDate" runat="server">
                <asp:Label ID="Label5" runat="server" CssClass="label">Cohort Start Date</asp:Label>
            </div>
            <div class="contentmargin" id="trCohortStartDate1" runat="server">
                <telerik:RadComboBox ID="ddlcohortStDate" runat="server"
                    AutoPostBack="True" CausesValidation="False" Filter="Contains" CssClass="dropdownlist" AppendDataBoundItems="true" Width="95%">
                </telerik:RadComboBox>
            </div>

            <div class="titlefirstmargin" id="trShift" runat="server">
                <asp:Label ID="lblShift" CssClass="label" runat="server">Shift (Optional)</asp:Label>
            </div>
            <div class="contentmargin" id="divShift" runat="server">
                <telerik:RadComboBox ID="ddlShift" runat="server" DataTextField="ShiftDescrip" DataValueField="ShiftId"
                    AutoPostBack="True" CausesValidation="False" AppendDataBoundItems="true" Filter="Contains" EmptyMessage="Select" CssClass="dropdownlist" Width="95%" TabIndex="1">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label2" Text='<%# Eval("ShiftDescrip")%>'></asp:Label>
                        <telerik:RadToolTip ID="RadToolTipLabel2" runat="server" TargetControlID="Label2" Width="400px" Position="BottomRight" Text='<%# Eval("ShiftDescrip")%>' IsClientID="false"  >
                        </telerik:RadToolTip>
                    </ItemTemplate>
                </telerik:RadComboBox>
            </div>

            <div class="titlefirstmargin" id="trMinimumClassStartDate" runat="server">
                <asp:Label ID="lblMinimumClassStartDate" CssClass="label" runat="server">Class Starting From Date (Optional)</asp:Label>
            </div>
            <div class="contentmargin" id="divMinimumClassStartDate" runat="server">            
                <telerik:RadDatePicker ID="rdpMinimumClassStartDate" MinDate="1/1/1945" runat="server" AutoPostBack="true" >
                </telerik:RadDatePicker>
            </div>
            <div class="titlemargin">
                <asp:Label ID="Label3" CssClass="label" runat="server">Class</asp:Label>
            </div>
            <div class="contentmargin">
                <telerik:RadComboBox ID="ddlClasses" runat="server"
                    AutoPostBack="True" CausesValidation="False" Filter="Contains" EmptyMessage="Select" CssClass="dropdownlist" AppendDataBoundItems="true" Width="95%">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label4" Text='<%# Eval("Class")%>'></asp:Label>
                        <telerik:RadToolTip ID="RadToolTipLabel4" runat="server" TargetControlID="Label4" Width="400px" Position="BottomRight" Text='<%# Eval("Class")%>' IsClientID="false"  >
                        </telerik:RadToolTip>
                    </ItemTemplate>
                </telerik:RadComboBox>
            </div>
            <div class="titlemargin">
                <asp:Label ID="Label6" CssClass="label" runat="server">Meeting</asp:Label>
            </div>
            <div class="contentmargin">
                <telerik:RadComboBox ID="ddlClsSectmeeting" runat="server"
                    AutoPostBack="True" CausesValidation="False" Filter="Contains" EmptyMessage="Select" CssClass="dropdownlist" AppendDataBoundItems="true" Width="95%">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label7" Text='<%# Eval("Descrip")%>'></asp:Label>
                        <telerik:RadToolTip ID="RadToolTipLabel7" runat="server" TargetControlID="Label7" Width="400px" Position="BottomRight" Text='<%# Eval("Descrip")%>' IsClientID="false"  >
                        </telerik:RadToolTip>
                    </ItemTemplate>
                </telerik:RadComboBox>
            </div>
            <div class="titlemargin">
                <asp:Label ID="Label8" CssClass="label" runat="server">Dates</asp:Label>
            </div>
            <div class="contentmargin">
                <asp:TextBox ID="txtDates" CssClass="TextBox" runat="server" ReadOnly="True"></asp:TextBox>
            </div>

            <div class="titlemargin">
                <asp:Label ID="Label9" CssClass="label" runat="server">Start</asp:Label>
            </div>
            <div class="contentmargin">
                <telerik:RadDatePicker ID="txtStart" MinDate="1/1/1945" runat="server">
                </telerik:RadDatePicker>
            </div>

            <div class="titlemargin">
                <asp:Label ID="Label10" runat="server" Width="100%" CssClass="label">End</asp:Label>
            </div>
            <div class="contentmargin">
                <telerik:RadDatePicker ID="txtEnd" MinDate="1/1/1945" runat="server">
                </telerik:RadDatePicker>
            </div>

            <div class="titlemargin">
                <asp:Button ID="btnBuildList" runat="server" Text="Build List"></asp:Button>
            </div>

        </telerik:RadPane>

        <!-- begin rightcolumn -->
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">

            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                            Enabled="False"></asp:Button></td>
                </tr>
            </table>


            <!-- end top menu (save,new,reset,delete,history)-->
            <!--begin right column-->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
                <tr>
                    <td class="detailsframe">
                        <div>
                            <asp:Panel ID="pnlHeader" runat="server" Visible="False">
                                <asp:Panel ID="pnlRHS" runat="server" CssClass="label">
                                    <!-- begin table content-->

                                    <table class="contenttable" width="60%" border="0" align="center">
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInstr" CssClass="label" runat="server">Instructor</asp:Label></td>
                                            <td class="twocolumncontentcell">
                                                <asp:Label ID="lblInstrName" runat="server" CssClass="TextBox"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSection" CssClass="label" runat="server">Class Section</asp:Label></td>
                                            <td class="twocolumncontentcell">
                                                <asp:Label ID="lblSecName" runat="server" CssClass="TextBox"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblMeeting" CssClass="label" runat="server">Class Section Meeting</asp:Label></td>
                                            <td class="twocolumncontentcell">
                                                <asp:Label ID="lblClsSectMeeting" runat="server" CssClass="TextBox"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Label ID="lblMessage" runat="server" CssClass="LabelBoldReq" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="margin: 15px 0px 0px 0px;">

                                        <span>
                                            <asp:Button ID="btnPreviousWeek" runat="server" Text="<< Previous" Width="100px" TabIndex="-1" Visible="false"></asp:Button>
                                        </span>
                                        <span>
                                            <asp:Button ID="btnNextWeek" runat="server" Text="Next >>" Width="100px" TabIndex="-1" Visible="false"></asp:Button>
                                        </span>
                                        <span>
                                            <asp:Button ID="btnRefresh" runat="server" Visible="false" Text="Refresh Page" TabIndex="-1" Width="100px"></asp:Button>
                                        </span>

                                    </div>
                                    <%--<div runat="server" id="attendanceGridUpdate">--%>
                                        <table width="100%">

                                            <tr>
                                                <td width="20%"></td>
                                                <td align="center">
                                                    <asp:Label ID="lblsun" runat="server" CssClass="label" Visible="false"></asp:Label></td>
                                                <td align="center">
                                                    <asp:Label ID="lblmon" runat="server" CssClass="label" Visible="false"></asp:Label></td>
                                                <td align="center">
                                                    <asp:Label ID="lbltue" runat="server" CssClass="label" Visible="false"></asp:Label></td>
                                                <td align="center">
                                                    <asp:Label ID="lblwed" runat="server" CssClass="label" Visible="false"></asp:Label></td>
                                                <td align="center">
                                                    <asp:Label ID="lblthur" runat="server" CssClass="label" Visible="false"></asp:Label></td>
                                                <td align="center">
                                                    <asp:Label ID="lblfri" runat="server" CssClass="label" Visible="false"></asp:Label></td>
                                                <td align="center">
                                                    <asp:Label ID="lblsat" runat="server" CssClass="label" Visible="false"></asp:Label></td>

                                                <td width="3%"></td>

                                            </tr>

                                        </table>

                                        <div style="margin: 5px 0px 0px 5px;" runat="server" id="AttendanceGridDiv">
                                            <table width="60%" style="text-align: left;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblHeading" runat="server" Text="Enter Hours Present" CssClass="LabelBold" Style="color: black;"></asp:Label>
                                                    </td>
                                               <%-- </tr>
                                                <tr>--%>
                                                    <td align="center">
                                                        <asp:Label ID="lblOutput" runat="server" CssClass="LabelBold" Style="color: #b71c1c"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                          <asp:Button ID="btnExportToExcell" runat="server" Text="Export to Excel" Visible="false"></asp:Button>

                                                    </td>
                                                </tr>
                                            </table>
                                            <telerik:RadGrid ID="dgAttendance" Width="99%" ShowStatusBar="false"
                                                AllowSorting="True" PageSize="50" GridLines="None" AllowPaging="True" runat="server"
                                                AutoGenerateColumns="False" >
                                                <MasterTableView TableLayout="Fixed" DataKeyNames="StuEnrollID" EditMode="InPlace"
                                                    ClientDataKeyNames="StuEnrollID,StudentStartDate,SysStatusId,DateDetermined" CommandItemDisplay="TopAndBottom">
                                                    <PagerStyle AlwaysVisible="true" />
                                                    <CommandItemTemplate>
                                                        <table style="height: 30px;" width="100%">
                                                            <tr>
                                                                <td nowrap="nowrap" >
                                                                    <asp:Button ID="Button1" runat="server" Text="<< Previous"  OnClick="btnPreviousWeek_Click" Style="text-align: left;" />
                                                                    <asp:Button ID="Button2" runat="server" Text="Next >>"  OnClick="btnNextWeek_Click" />
                                                                </td>
                                                                <td nowrap="nowrap" >
                                                                    <div align="right"   style="width:100%; " >
                                                                        <asp:Button ID="btnPostZero" runat="server" Text="Post Zeros"  CommandName="PostZero"  Style="cursor: pointer; margin: 2px  5px 2px 5px; " Visible="<%# ShowHidePostZeroButton() %>" OnClientClick="confirmPostZero(this); return false;" />
                                                                        <asp:Button ID="btnPostException" runat="server" Text="Post By Exception"  CommandName="PostException" Style="cursor: pointer; margin: 2px 5px 2px 5px; " Visible="<%# ShowHidePostExceptionButton() %>" OnClientClick="confirmPostException(this); return false;" />
                                                                        <asp:ImageButton ID="imgCancelChanges" runat="server" ImageUrl="~/Images/CancelChangesGrid.gif" AlternateText="Cancel Changes" ToolTip="Cancel Changes" Height="24px" Style="cursor: pointer; text-align: right;" CommandName="CancelChanges" Visible="<%# ShowHideButtonsWhenCourseUsesTimeClock() %>" />
                                                                        <asp:ImageButton ID="imgProcessChanges" runat="server" ImageUrl="~/Images/SaveChangesforgrid.gif" AlternateText="Save Changes" ToolTip="Save Changes" Height="24px" Style="cursor: pointer; text-align: right;" CommandName="SaveChanges" Visible="<%# ShowHideButtonsWhenCourseUsesTimeClock() %>" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </CommandItemTemplate>
                                                    <Columns>
                                                        <telerik:GridBoundColumn UniqueName="StuEnrollID" DataField="StuEnrollId" HeaderText="Student EnrollID"
                                                            ReadOnly="True" Display="false" HeaderStyle-BackColor="#003399" />
                                                        <telerik:GridBoundColumn UniqueName="SysStatusId" DataField="SysStatusId" HeaderText="System Status"
                                                            ReadOnly="True" Display="false" />
                                                        <telerik:GridBoundColumn UniqueName="DateDetermined" DataField="DateDetermined" HeaderText="DateDetermined"
                                                            ReadOnly="True" Display="false" />
                                                        <telerik:GridBoundColumn UniqueName="StudentStartDate" DataField="StudentStartDate" HeaderText="StudentStartDate"
                                                            ReadOnly="True" Display="false" />
                                                        <telerik:GridTemplateColumn UniqueName="StudentName"
                                                            HeaderText="Student Name" HeaderStyle-Width="20%">
                                                            <ItemTemplate>

                                                                <asp:ImageButton ID="ibStudentInfo" runat="server" ImageUrl="../Images/FA/RptInfo.gif" />

                                                                <telerik:RadToolTip ID="RadToolTipStudentInfo" runat="server" TargetControlID="ibStudentInfo" RelativeTo="Element"
                                                                    Position="TopCenter" RenderInPageRoot="true" EnableShadow="true"  HideEvent="ManualClose" >
                                                                    <div id="ToolTipWrapper" style="padding: 15px;">
                                                                        <div>
                                                                            <b>Program Version:</b>
                                                                            <%# DataBinder.Eval(Container, "DataItem.PrgVerDescrip")%>
                                                                        </div>
                                                                        <div>
                                                                            <b>Student Status:</b>
                                                                            <%# DataBinder.Eval(Container, "DataItem.StatusDescrip")%>
                                                                        </div>
                                                                        <div>
                                                                            <b>Student Start Date:</b>
                                                                            <%# DataBinder.Eval(Container, "DataItem.StudentStartDate")%>
                                                                        </div>
                                                                        <div>
                                                                            <b>Time Clock:</b>
                                                                            <%# Session("ClsSectionUsesTimeClock")%>
                                                                        </div>
                                                                        <div>
                                                                            <b>SSN#:</b>
                                                                            <%# DataBinder.Eval(Container, "DataItem.SSN")%>
                                                                        </div>
                                                                    </div>
                                                                </telerik:RadToolTip>

                                                                <asp:Label ID="lblStudentName" runat="server" Text='<%# Eval("StudentName") %>' Visible="True" Style="vertical-align: top;" />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="SunAct"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblSunday" runat="server" CssClass="label" ForeColor="#757575"><%= Format(ViewState("FromDate"),"MM/dd/yy") & "<br> Sunday" %></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSunSch" runat="server" Text='<%# Eval("SunSch") %>' Visible="false" />
                                                                <asp:Label ID="lblSunDate" runat="server" Text='<%# Eval("SunDate") %>' Visible="false" />
                                                                <asp:TextBox ID="txtSunAct" runat="server" Text='<%# Bind("SunAct") %>' Width="50%" CssClass="TextBox" CausesValidation="true" />
                                                                <asp:ImageButton ID="imgbtnSunday" ImageUrl="../Images/Edit_bluePencil.gif" CommandName="OpenEditFormSunday" runat="server" />
                                                                <br />
                                                                <asp:CheckBox ID="chkSunTardy" CssClass="checkbox" Checked='<%# GetTardyinfo(Eval("SunTardy")) %>' Text="Tardy" runat="server" /><br/>
                                                                <asp:CheckBox ID="chkSunExcused" CssClass="checkbox" Checked='<%# GetExcusedInfo(Eval("SunExcused")) %>' Text="Excused" runat="server" Visible="False" ClientIDMode="Static" />
                                                                
                                                                <%--<asp:Label ID="lblSunTardy" runat="server" CssClass="checkbox" Text="Tardy"  Visible="true" Width="10px"/> --%>

                                                                <asp:RegularExpressionValidator ID="regSchDateSun" runat="server" Enabled='<%# CBool(Eval("SunSch")) = true %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtSunAct">Invalid!</asp:RegularExpressionValidator>

                                                                <asp:RegularExpressionValidator ID="regNotSchDateSun" runat="server" Enabled='<%# CBool(Eval("SunSch")) = False  %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtSunAct">Invalid!</asp:RegularExpressionValidator>


                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="MonAct"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblMonday" runat="server" CssClass="label" ForeColor="#757575"><%= Format(ViewState("FromDate").AddDays(1),"MM/dd/yy") & "<br> Monday" %></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMonSch" runat="server" Text='<%# Eval("MonSch") %>' Visible="false" />
                                                                <asp:Label ID="lblMonDate" runat="server" Text='<%# Eval("MonDate") %>' Visible="false" />
                                                                <asp:TextBox ID="txtMonAct" runat="server" Text='<%# Bind("MonAct") %>' Width="50%" CssClass="TextBox" CausesValidation="true" />
                                                                <asp:ImageButton ID="ImageButton2" ImageUrl="../Images/Edit_bluePencil.gif" CommandName="OpenEditFormMonday" runat="server" />
                                                                <%--<asp:Label ID="lblMonTardy" runat="server" CssClass="checkbox" Text="Tardy"  Visible="true" Text="Tardy" /> --%>
                                                                <br />
                                                                <asp:CheckBox ID="chkMonTardy" CssClass="checkbox" Checked='<%# GetTardyinfo(Eval("MonTardy")) %>' Text="Tardy" runat="server" /><br/>
                                                                <asp:CheckBox ID="chkMonExcused" CssClass="checkbox"  Checked='<%# GetExcusedInfo(Eval("MonExcused")) %>' Text="Excused" runat="server" Visible="False" ClientIDMode="Static" />

                                                                <asp:RegularExpressionValidator ID="regSchDateMon" runat="server" Enabled='<%# CBool(Eval("MonSch")) = true  %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtMonAct">Invalid!</asp:RegularExpressionValidator>

                                                                <asp:RegularExpressionValidator ID="regNotSchDateMon" runat="server" Enabled='<%# CBool(Eval("MonSch")) = False  %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtMonAct">Invalid!</asp:RegularExpressionValidator>


                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="TueAct"
                                                            HeaderText="Tue" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblTuesday" runat="server" CssClass="label" ForeColor="#757575"><%= Format(ViewState("FromDate").AddDays(2),"MM/dd/yy") & "<br> Tuesday"%></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTueSch" runat="server" Text='<%# Eval("TueSch") %>' Visible="false" />
                                                                <asp:Label ID="lblTueDate" runat="server" Text='<%# Eval("TueDate") %>' Visible="false" />
                                                                <asp:TextBox ID="txtTueAct" runat="server" Text='<%# Bind("TueAct") %>' Width="50%" CssClass="TextBox" CausesValidation="true" />
                                                                <asp:ImageButton ID="ImageButton3" ImageUrl="../Images/Edit_bluePencil.gif" CommandName="OpenEditFormTuesday" runat="server" />
                                                                <%--<asp:Label ID="lblTueTardy" runat="server" CssClass="checkbox" Text="Tardy"  Visible="true" Text="Tardy"/> --%>
                                                                <br />
                                                                <asp:CheckBox ID="chkTueTardy" CssClass="checkbox" Checked='<%# GetTardyinfo(Eval("TueTardy")) %>' runat="server" Text="Tardy" /><br/>
                                                                <asp:CheckBox ID="chkTueExcused" CssClass="checkbox"  Checked='<%# GetExcusedInfo(Eval("TueExcused")) %>' Text="Excused" runat="server" Visible="False" ClientIDMode="Static" />

                                                                <asp:RegularExpressionValidator ID="regSchDateTue" runat="server" Enabled='<%# CBool(Eval("TueSch")) = true %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtTueAct">Invalid!</asp:RegularExpressionValidator>

                                                                <asp:RegularExpressionValidator ID="regNotSchDateTue" runat="server" Enabled='<%# CBool(Eval("TueSch")) = False  %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtTueAct">Invalid!</asp:RegularExpressionValidator>

                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="WedAct"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblWednesday" runat="server" CssClass="label" ForeColor="#757575"><%= Format(ViewState("FromDate").AddDays(3),"MM/dd/yy") & "<br> Wednesday" %></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWedSch" runat="server" Text='<%# Eval("WedSch") %>' Visible="false" />
                                                                <asp:Label ID="lblWedDate" runat="server" Text='<%# Eval("WedDate") %>' Visible="false" />
                                                                <asp:TextBox ID="txtWedAct" runat="server" Text='<%# Bind("WedAct") %>' Width="50%" CssClass="TextBox" CausesValidation="true"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageButton4" ImageUrl="../Images/Edit_bluePencil.gif" CommandName="OpenEditFormWednesday" runat="server" />
                                                                <%--<asp:Label ID="lblWedTardy" runat="server" CssClass="checkbox" Text="Tardy"  Visible="true"/> --%>
                                                                <br />
                                                                <asp:CheckBox ID="chkWedTardy" CssClass="checkbox" Checked='<%# GetTardyinfo(Eval("WedTardy")) %>' Text="Tardy" runat="server" /><br/>
                                                                <asp:CheckBox ID="chkWedExcused" CssClass="checkbox" Checked='<%# GetExcusedInfo(Eval("WedExcused")) %>' Text="Excused" runat="server" Visible="False" ClientIDMode="Static" />

                                                                <asp:RegularExpressionValidator ID="regSchDateWed" runat="server" Enabled='<%# CBool(Eval("WedSch")) = true %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtWedAct">Invalid!</asp:RegularExpressionValidator>

                                                                <asp:RegularExpressionValidator ID="regNotSchDateWed" runat="server" Enabled='<%# CBool(Eval("WedSch")) = False  %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtWedAct">Invalid!</asp:RegularExpressionValidator>

                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="ThurAct"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblThursday" runat="server" CssClass="label" ForeColor="#757575"><%= Format(ViewState("FromDate").AddDays(4),"MM/dd/yy") & "<br> Thursday" %></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblThurSch" runat="server" Text='<%# Eval("ThurSch") %>' Visible="false" />
                                                                <asp:Label ID="lblThurDate" runat="server" Text='<%# Eval("ThurDate") %>' Visible="false" />
                                                                <asp:TextBox ID="txtThurAct" runat="server" Text='<%# Bind("ThurAct") %>' Width="50%" CssClass="TextBox" CausesValidation="true" />
                                                                <asp:ImageButton ID="ImageButton5" ImageUrl="../Images/Edit_bluePencil.gif" CommandName="OpenEditFormThursday" runat="server" />
                                                                <%--    <asp:Label ID="lblThurTardy" runat="server" CssClass="checkbox" Text="Tardy"  Visible="true"/> --%>
                                                                <br />
                                                                <asp:CheckBox ID="chkThurTardy" CssClass="checkbox" Checked='<%# GetTardyinfo(Eval("ThurTardy")) %>' runat="server" Text="Tardy" /><br/>
                                                                <asp:CheckBox ID="chkThurExcused" CssClass="checkbox" Checked='<%# GetExcusedInfo(Eval("ThurExcused")) %>' Text="Excused" runat="server" Visible="False" ClientIDMode="Static" />

                                                                <asp:RegularExpressionValidator ID="regSchDateThur" runat="server" Enabled='<%# CBool(Eval("ThurSch")) = true  %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtThurAct">Invalid!</asp:RegularExpressionValidator>

                                                                <asp:RegularExpressionValidator ID="regNotSchDateThur" runat="server" Enabled='<%# CBool(Eval("ThurSch")) = False  %>' Display="Dynamic" ForeColor="Red"
                                                                    ValidationGroup="Validationsummary2" ControlToValidate="txtThurAct">Invalid!</asp:RegularExpressionValidator>


                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="FriAct"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblFriday" runat="server" CssClass="label" ForeColor="#757575"><%= Format(ViewState("FromDate").AddDays(5),"MM/dd/yy") & "<br> Friday" %></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFriSch" runat="server" Text='<%# Eval("FriSch") %>' Visible="false" />
                                                                <asp:Label ID="lblFriDate" runat="server" Text='<%# Eval("FriDate") %>' Visible="false" />

                                                                <asp:TextBox ID="txtFriAct" runat="server" Text='<%# Bind("FriAct") %>' Width="50%" CssClass="TextBox" CausesValidation="true" />
                                                                <asp:ImageButton ID="ImageButton6" ImageUrl="../Images/Edit_bluePencil.gif" CommandName="OpenEditFormFriday" runat="server" />
                                                                <%--<asp:Label ID="lblFriTardy" runat="server" CssClass="checkbox" Text="Tardy"  Visible="true" /> --%>
                                                                <br />
                                                                <asp:CheckBox ID="chkFriTardy" CssClass="checkbox" Checked='<%# GetTardyinfo(Eval("FriTardy")) %>' runat="server" Text="Tardy" /><br/>
                                                                <asp:CheckBox ID="chkFriExcused" CssClass="checkbox" Checked='<%# GetExcusedInfo(Eval("FriExcused")) %>' Text="Excused" runat="server" Visible="False" ClientIDMode="Static" />

                                                                <asp:RegularExpressionValidator ID="regSchDateFri" runat="server" Enabled='<%# CBool(Eval("FriSch")) = true %>' Display="Dynamic" ForeColor="Red" ValidationGroup="Validationsummary2"
                                                                    ControlToValidate="txtFriAct">Invalid!</asp:RegularExpressionValidator>

                                                                <asp:RegularExpressionValidator ID="regNotSchDateFri" runat="server" Enabled='<%# CBool(Eval("FriSch")) = False  %>' Display="Dynamic" ForeColor="Red" ValidationGroup="Validationsummary2"
                                                                    ControlToValidate="txtFriAct">Invalid!</asp:RegularExpressionValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn UniqueName="SatAct"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="14%">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblSaturday" runat="server" CssClass="label" ForeColor="#757575"><%= Format(ViewState("FromDate").AddDays(6),"MM/dd/yy") & "<br> Saturday" %></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <div id="DateSquare">
                                                                    <asp:Label ID="lblSatSch" runat="server" Text='<%# Eval("SatSch") %>' Visible="false" />
                                                                    <asp:Label ID="lblSatDate" runat="server" Text='<%# Eval("SatDate") %>' Visible="false" />

                                                                    <asp:TextBox ID="txtSatAct" runat="server" Text='<%# Bind("SatAct") %>' Width="50%" CssClass="TextBox" CausesValidation="true" />
                                                                    <asp:ImageButton ID="ImageButton7" ImageUrl="../Images/Edit_bluePencil.gif" CommandName="OpenEditFormSaturday" runat="server" />
                                                                    <%--<asp:Label ID="lblSatTardy" runat="server" CssClass="checkbox" Text="Tardy"  Visible="true"/> --%>
                                                                    <br />
                                                                    <asp:CheckBox ID="chkSatTardy" CssClass="checkbox" Checked='<%# GetTardyinfo(Eval("SatTardy")) %>' runat="server" Text="Tardy" /><br/>
                                                                    <asp:CheckBox ID="chkSatExcused" CssClass="checkbox" Checked='<%# GetExcusedInfo(Eval("SatExcused")) %>' Text="Excused" runat="server" Visible="False" ClientIDMode="Static" />

                                                                    <asp:RegularExpressionValidator ID="regSchDateSat" runat="server" Enabled='<%# CBool(Eval("SatSch")) = true %>' Display="Dynamic" ForeColor="Red" 
                                                                        ValidationGroup="Validationsummary2" ControlToValidate="txtSatAct">Invalid!</asp:RegularExpressionValidator>

                                                                    <asp:RegularExpressionValidator ID="regNotSchDateSat" runat="server" Enabled='<%# CBool(Eval("SatSch")) = False  %>' Display="Dynamic" ForeColor="Red" 
                                                                        ValidationGroup="Validationsummary2" ControlToValidate="txtSatAct">Invalid!</asp:RegularExpressionValidator>
                                                                </div>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <table width="60%" style="text-align: left;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblHeadingBottom" runat="server" Text="Enter Hours Present" CssClass="LabelBold" Style="color: black;"></asp:Label>
                                                    </td>
                                                    <%-- </tr>
	                                                <tr>--%>
                                                    <td align="center">
                                                        <asp:Label ID="lblOutputBottom" runat="server" CssClass="LabelBold" Style="color: #b71c1c"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>                                        
                                        <telerik:RadWindow ID="UserListDialog" runat="server" Title="Editing record" Width="800"
                                            Height="600" Modal="true" VisibleOnPageLoad="false" VisibleStatusbar="false"
                                            Behaviors="Close,Move,Resize" >
                                            <ContentTemplate>


                                                <div style="margin: 5px 5px 5px; font: bold 12px arial,sans-serif;" align="left">
                                                    <asp:Label ID="lblStudentName1" runat="server" Text="Student Name"></asp:Label>
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:Label ID="lblClsSmeeting1" runat="server" Text="Class Section Meeting"></asp:Label>
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:Label ID="lblMeetDate1" runat="server" Text="Meeting Date"></asp:Label>
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:Label ID="lblScheduled1" Width="65px" runat="server" Text="Scheduled"></asp:Label>
                                                    <%-- <asp:TextBox ID="txtScheduled1" onblur="CalculateMakeUpandActual();"  Width="40px"    runat="server"></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtScheduled1" Width="40px" runat="server"></asp:TextBox>
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:Label ID="lblActual1" Width="65px" runat="server" Text="Actual"></asp:Label>
                                                    <%--  <asp:TextBox ID="txtActual1" onblur="CalculateMakeUpandActual();"  Width="40px"    runat="server"></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtActual1" Width="40px" runat="server"></asp:TextBox>
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:Label ID="lblAbsent" Width="65px" runat="server" Text="Absent"></asp:Label>
                                                    <asp:TextBox ID="txtAbsent1" Width="40px" runat="server" Enabled="false"></asp:TextBox>
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:Label ID="lblMakeUp" Width="65px" runat="server" Text="Make Up"></asp:Label>
                                                    <asp:TextBox ID="txtMakeUp1" Width="40px" runat="server" Enabled="false"></asp:TextBox>
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:CheckBox ID="chkTardy1" Text="Tardy" runat="server" />
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:CheckBox ID="chkExcused" Text="Excused" runat="server" Visible="False" />
                                                </div>
                                                <div style="margin: 5px 5px 5px;" align="left">
                                                    <span>
                                                        <asp:Button ID="btnsave1" runat="server"  Text="Save" Width="75px"></asp:Button>
                                                    </span>

                                                    <span>
                                                        <asp:Button ID="btnCancel1" runat="server"  Text="Cancel" Width="75px"></asp:Button>
                                                    </span>
                                                </div>
                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:Label runat="server" ID="lblerrmsg" Text=""></asp:Label>

                                                </div>

                                                <div style="margin: 5px 5px 5px; font: normal 12px arial,sans-serif;" align="left">
                                                    <asp:GridView ShowHeader="true"
                                                        EmptyDataText="No Records found." ID="GridView1" runat="server" CellPadding="4" AllowSorting="True" AutoGenerateColumns="False" ShowFooter="True" ForeColor="#333333" GridLines="None">
                                                        <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Font-Names="Verdana" Font-Size="10pt" />
                                                        <Columns>
                                                            <asp:CommandField ShowEditButton="True" />

                                                            <asp:TemplateField Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBadgeId" Text='<%# Eval("BadgeID") %>' runat="server" CssClass="label"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="ClockId" HeaderText="ClockId" Visible="False" />
                                                            <asp:BoundField DataField="Status" HeaderText="Status" Visible="False" />
                                                            <asp:BoundField DataField="StuEnrollId" HeaderText="StuEnrollId" Visible="False"  />

                                                            <asp:TemplateField HeaderText="Punch Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpunchtype" Text='<%# GetPunchType(DataBinder.Eval(Container.DataItem, "PunchType")) %>' runat="server" CssClass="label"></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="ddleditPunchType" SelectedValue='<%# DataBinder.Eval(Container.DataItem, "PunchType") %>' runat="server" CssClass="dropdownlist" Width="95px">
                                                                        <asp:ListItem Text="" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="Punch In" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Punch Out" Value="2"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="ddlPunchType" runat="server" CssClass="DropDownList" Width="95px">
                                                                        <asp:ListItem Text="" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="Punch In" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Punch Out" Value="2"></asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Punch Time">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpunchtime" Text='<%# Eval("PunchTime") %>' runat="server" CssClass="label"></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadTimePicker ID="txteditPunchTime" DbSelectedDate='<%# Bind("PunchTime") %>' ZIndex="200000" runat="server">
                                                                    </telerik:RadTimePicker>

                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <telerik:RadTimePicker ID="txtpunchTime" ToolTip="Time In" ZIndex="200000" runat="server">
                                                                    </telerik:RadTimePicker>

                                                                    <asp:Button ID="Button1" CommandName="ADD" runat="server"
                                                                        Text="Add" CssClass="Button" Width="50px" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField ShowDeleteButton="True" />
                                                        </Columns>
                                                        <EditRowStyle BackColor="#999999" />
                                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                    </asp:GridView>

                                                </div>

                                            </ContentTemplate>
                                        </telerik:RadWindow>
                                    <%--</div>--%>
                                    <asp:Panel ID="pnlLegend" runat="server" Visible="false">
                                        <div style="margin-top: 10px; text-align: left; font: normal 14px arial,sans-serif;">
                                            <table>
                                                <tr align="left">
                                                    <td style="text-align: right" class="LegendLabel">Legend :</td>
                                                    <td style="height: 5px; vertical-align: middle; background-color: White; border: solid 1px black; width: 90px">
                                                        <span class="LegendLabel">Entry</span></td>
                                                    <td style="height: 5px; vertical-align: middle; background-color: Aquamarine; border: solid 1px black; width: 90px">
                                                        <span class="LegendLabel">Scheduled</span></td>
                                                    <td style="height: 5px; vertical-align: middle; background-color: LightGrey; border: solid 1px black; width: 90px">
                                                        <span class="LegendLabel">Disabled</span></td>
                                                    <td style="height: 5px; vertical-align: middle; background-color: LightSalmon; border: solid 1px black; width: 90px">
                                                        <span class="LegendLabel">Makeup</span></td>
                                                    <td style="height: 5px; vertical-align: middle; background-color: LightBlue; border: solid 1px black; width: 90px">
                                                        <span class="LegendLabel">Absent</span></td>
                                                    <td style="height: 5px; vertical-align: middle; background-color: BlanchedAlmond; border: solid 1px black; width: 90px">
                                                        <span class="LegendLabel">Tardy</span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <asp:HiddenField ID="hdnBindCurrentWeek" runat="server" />
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td style="padding: 16px 16px 4px 16px; text-align: center"></td>
                                        </tr>
                                        <tr>
                                            <td style="PADDING-BOTTOM: 10px">
                                              </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="pnlAttendance" runat="server" TabIndex="-1" CssClass="label" Width="100%"></asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--<asp:TextBox id=HiddenText  runat="server" CssClass="noclass"  ForeColor="white" BorderColor="White" BorderStyle="None" Height="0" BorderWidth="0" tabindex="-1" ></asp:TextBox>--%>
                                            <asp:HiddenField ID="HiddenText" runat="server" ClientIDMode="Static" />

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStart" Display="None" ErrorMessage="You must enter a start meet date to search"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="txtStart" Display="None" ErrorMessage="You must enter a end meet date to search"></asp:RequiredFieldValidator>
                                                <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--end table content-->
                                </asp:Panel>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- end rightcolumn -->
        </telerik:RadPane>
    </telerik:RadSplitter>
    <!-- start validation panel-->
    <asp:CustomValidator ID="Customvalidator1" ValidationGroup="Validationsummary2" runat="server" CssClass="ValidationSummary" Display="None"
        ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:ValidationSummary ID="Validationsummary2" ValidationGroup="Validationsummary2" runat="server" CssClass="ValidationSummary" ShowSummary="true" ShowMessageBox="false" HeaderText="Validation summary"></asp:ValidationSummary>
    <!--end validation panel-->
</asp:Content>

