' ===============================================================================
'
' FAME AdvantageV1
'
' TranscriptPopUp.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data

Partial Class TranscriptPopUp
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblDocumentId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlDocumentId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnNew As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelete As System.Web.UI.WebControls.Button
    Protected WithEvents ddlScannedId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lnkViewDoc As System.Web.UI.WebControls.LinkButton
    Protected WithEvents ddlEnrollmentId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dlstLeadNames As System.Web.UI.WebControls.DataList
    Protected WithEvents lblFilter As System.Web.UI.WebControls.Label
    Private requestContext As HttpContext
    Protected WithEvents lblNoSchedCourses As System.Web.UI.WebControls.Label
    Protected WithEvents pnlNoSchedCourses As System.Web.UI.WebControls.Panel
    Protected StudentId As String
    Private bNumericGrade As Boolean
    Private bRoundNumGrade As Boolean
    Protected campusId As String
    Protected MyAdvAppSettings As AdvAppSettings
    ''Protected StudentId As String = "DCAF913F-F4B6-4ED3-B8E1-46C9CDFEAB69"
    'Protected StudentId As String = "14094BD8-18CA-4B95-9195-069D47774306"


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        campusId = AdvantageSession.UserState.CampusId.ToString
        If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
            bNumericGrade = True
        Else
            bNumericGrade = False
        End If
        If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
            bRoundNumGrade = True
        Else
            bRoundNumGrade = False
        End If

        DisplayContent(Request("ProgVerId"), Request("EnrollmentId"), Request("StudentId"))


        If Not Page.IsPostBack Then
            ChkIsInDB.Checked = True
            pnlSummary.Visible = False
        End If
    End Sub

    'Private Sub dlstLeadNames_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstLeadNames.ItemCommand
    Private Sub DisplayContent(ByVal ProgVerId As String, ByVal EnrollmentID As String, ByVal StudentId As String)
        Dim strPrgVerId As String = ProgVerId
        Dim Transcript As New TranscriptFacade
        Dim dt As DataTable
        Dim strFirstName, strLastName, FullName, strSSN, strStudentID As String
        Dim strID As String = String.Empty
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        '        Dim zipMask As String
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Dim StudentInfo() As String = Split(Transcript.GetStudentName(EnrollmentID), ";")
        strFirstName = StudentInfo(0)
        strLastName = StudentInfo(1)
        strSSN = StudentInfo(3)
        strStudentID = StudentInfo(4)

        FullName = strLastName & ", " & strFirstName

        If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
            'GetInputMaskValue(linkbtn)
            If strSSN.Length >= 1 Then
                Dim temp As String = strSSN
                strID = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
            End If
            'strID = strSSN
        ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier") = "StudentNumber" Then
            strID = strStudentID
        End If

        PopulateHeader(FullName, strID)

        'dgrdTransactionSearch.DataSource = Transcript.GetTranscriptGradeByEnrollment(e.CommandArgument, strPrgVerId, StudentId)
        dt = Transcript.GetStudentResults(EnrollmentID, True, bRoundNumGrade)

        dgrdTransactionSearch.DataSource = dt
        dgrdTransactionSearch.DataBind()

        Dim SummaryInfo As TranscriptInfo = Transcript.GetSummary(EnrollmentID, strPrgVerId)

        label12.Text = SummaryInfo.CompletedSAPRequirements 'SummaryInfo.IsMakingSAP
        lblTotalClasses.Text = Utilities.GetTotalClassesFromStudentResultsDT(dt)
        Label5.Text = Utilities.GetCreditsAttemptedFromStudentResultsDT(dt)
        Label7.Text = Utilities.GetCreditsEarnedFromStudentResultsDT(dt)
        Label9.Text = Utilities.GetAccumGPAFromStudentResultsDT(dt)
        pnlSummary.Visible = True

        Dim ds As DataSet = Transcript.GetScheduledCourses(EnrollmentID, HttpContext.Current.Request("cmpid").ToString)

        If ds.Tables(0).Rows.Count = 0 Then
            lblSchedCourses.Text = "There are no scheduled courses"
            dgrdScheduledCourses.Visible = False
        Else
            lblSchedCourses.Text = "Scheduled Courses"
            dgrdScheduledCourses.Visible = True
        End If
        dgrdScheduledCourses.DataSource = ds.Tables(0)
        dgrdScheduledCourses.DataBind()
        pnlScheduledCourses.Visible = True


        ds = Transcript.GetRemainingCourses(EnrollmentID, HttpContext.Current.Request("cmpid").ToString)

        If ds.Tables(0).Rows.Count = 0 Then
            lblRemainingCourses.Text = "There are no remaining courses"
            dgrdRemainingCourses.Visible = False
        Else
            lblRemainingCourses.Text = "Remaining Courses"
            dgrdRemainingCourses.Visible = True
        End If
        dgrdRemainingCourses.DataSource = ds.Tables(0)
        dgrdRemainingCourses.DataBind()
        pnlRemainingCourses.Visible = True

    End Sub

    Private Sub PopulateHeader(ByVal fullname As String, ByVal ID As String)
        '        Dim intpos As Integer


        'pnlHeader.Visible = True
        lblStdName.Text = fullname
        lblStdID.Text = ID
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Protected Sub dgrdTransactionSearch_PreRender(sender As Object, e As System.EventArgs) Handles dgrdTransactionSearch.PreRender
        If bNumericGrade Then
            dgrdTransactionSearch.Columns(3).Visible = False
            dgrdTransactionSearch.Columns(4).Visible = True
        Else
            dgrdTransactionSearch.Columns(3).Visible = True
            dgrdTransactionSearch.Columns(4).Visible = False
        End If
    End Sub
End Class
