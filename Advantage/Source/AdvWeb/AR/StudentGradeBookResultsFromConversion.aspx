<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="StudentGradeBookResultsFromConversion.aspx.vb" Inherits="AR_StudentGradeBookResultsFromConversion" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<%-- <link href="../css/style.css" type="text/css" rel="stylesheet"/>--%>
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<TR>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right">
                                    <asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button>
                                    <asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
			
					</td>
					<!-- end rightcolumn --></TR>
                    <TR>
					<td class="detailsframe">
						<div class="scrollsingleframe">
							<!-- begin table content-->
							<table class="contenttable" cellSpacing="0" cellPadding="0" width="45%" align="center">
								<tr>
									<TD class="contentcell"><asp:label id="lblEnrollmentId" cssClass="label" Runat="server">Enrollment</asp:label></TD>
									<TD class="contentcell4">
									    <asp:dropdownlist id="ddlStuEnrollId" runat="server" CssClass="dropdownlist" AutoPostBack="True"></asp:dropdownlist>
									    
									</TD>
								</tr>
								<tr>
									<TD class="contentcell"><asp:label id="Label1" cssClass="label" Runat="server">Course</asp:label></TD>
									<TD class="contentcell4">
									    <asp:dropdownlist id="ddlReqId" runat="server" CssClass="dropdownlist" ></asp:dropdownlist>
									    
									</TD>
								</tr>
								<TR>
									<TD class="contentcell">&nbsp;</TD>
									<TD class="contentcell4"><asp:button id="btnBuildList" runat="server" Text="Show Results" ></asp:button></TD>
								</TR>
								
							</table>
							
							<table class="contenttable" cellSpacing="0" cellPadding="0" width="100%" border="0">
							    <tr>
							        <td colspan=4 align="left">
                                        <asp:Label ID="lblHours" runat="server" cssClass="label"></asp:Label></td>
							    </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdStudentResults" runat="server" Width="100%" HorizontalAlign="Center"
                                        BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0">
                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="Descrip" HeaderText="Grading Component" >
                                                <HeaderStyle CssClass="datagridheaderstyle" Width="25%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PostUser" HeaderText="User" >
                                                <HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PostDate" HeaderText="Date Posted" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Comments" ReadOnly="True" HeaderText="Comments">
                                                <HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Score" ReadOnly="True" HeaderText="Score">
                                                <HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            </asp:BoundColumn>
                                            
                                        </Columns>
                                    </asp:DataGrid>
                                    </td>
                                </tr>
								<tr>
									<td><asp:panel id="pnlAttendance" cssClass="label" Runat="server" Width="100%"></asp:panel></td>
								</tr>
								
							</table>
							<!--end table content--></div>
					</td>
				</TR>
			</table>
            <asp:RequiredFieldValidator ID="rfvEnrollment" runat="server" ControlToValidate="ddlStuEnrollId" ErrorMessage="Enrollment is required" Display="None" InitialValue="Select"></asp:RequiredFieldValidator>
			<asp:RequiredFieldValidator ID="rfvCourse" runat="server" ControlToValidate="ddlReqId" ErrorMessage="Course is required" Display="None" InitialValue="Select"></asp:RequiredFieldValidator>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="ValidationSummary" ErrorMessage="CustomValidator"
				Display="None"></asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
				ShowSummary="False"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>

