<%@ Page Language="vb" AutoEventWireup="false" Inherits="StudentGradeBkPopUp" CodeFile="StudentGradeBkPopUp.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Student Gradebook</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../css/systememail.css" type="text/css" rel="stylesheet">
    <link href="../css/colors.css" rel="stylesheet" />
    <link href="../css/localhost_lowercase.css" rel="stylesheet" />
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
</head>
<body runat="server" id="Body1" name="Body1" style="overflow: auto;">
    <form id="Form1" method="post" runat="server">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="header blue-darken-4 padding5px">
            <tr>
                <td></td>
                <td style="text-align: right"></td>
            </tr>
        </table>
        <div id="header" style="height: 45px" class=" blue-darken-4 padding5px">
            <img src="../images/AdvantageLogo.png" style="float: left;">
            <a class="close " style="float: right; margin-right: 10px; margin-top: 10px;" onclick="top.close()" href="javascript:void(0)">X Close</a>
        </div>
        <div class="clear"></div>
        <!--begin right column-->
        <div class="boxContainer full">
            <h3>Student Gradebook</h3>
            <!-- begin content table-->
            <table class="contenttable" cellspacing="0" cellpadding="0" border="0" style="margin-top: 30px;">
                <tr>
                    <td class="arcontentcells" nowrap>
                        <asp:Label ID="lblStd" runat="server">Student</asp:Label></td>
                    <td class="arcontentcells2" nowrap style="padding-right: 20px">
                        <asp:Label ID="lblStdName" runat="server" CssClass="TextBox"></asp:Label></td>
                    <td class="arcontentcells" nowrap>
                        <asp:Label ID="lblSection" runat="server">Class Section</asp:Label></td>
                    <td class="arcontentcells2" nowrap style="padding-right: 20px">
                        <asp:Label ID="lblSecName" runat="server" CssClass="TextBox"></asp:Label></td>
                </tr>
            </table>
            <div style="margin-top: 10px;">
                <table class="contenttable" cellspacing="0" cellpadding="0" width="90%" border="0">
                    <tr>
                        <td align="center">
                            <asp:Panel ID="pnlInstrGBW" runat="server" Visible="false">
                                <asp:DataGrid ID="dgdStdGrdBk" runat="server" CssClass="datalistcontentar" AutoGenerateColumns="False"
                                    BorderStyle="Solid" BorderColor="#ebebeb" BorderWidth="1px">
                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                    <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                    <FooterStyle CssClass="Label"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Grading Criteria">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtStuEnrollId" CssClass="ardatalistcontent" runat="server" Visible="False" Text='<%# Container.DataItem("StuEnrollId") %>' />
                                                <asp:TextBox ID="txtGrdCritId" CssClass="ardatalistcontent" runat="server" Visible="False" Text='<%# Container.DataItem("InstrGrdBkWgtDetailId") %>' />
                                                <asp:Label ID="GrdCriteria" CssClass="ardatalistcontent" runat="server" Text='<%# Container.DataItem("Descrip") %>' />
                                                <asp:TextBox ID="txtIncomplete" CssClass="ardatalistcontent" runat="server" Visible="False" Text='<%# Container.DataItem("IsIncomplete") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Score">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtScore" runat="server" CssClass="ardatalistcontent2" Text='<%# Container.DataItem("Score") %>' />
                                                <asp:CompareValidator ID="MinValCompareValidator" runat="server" ErrorMessage="Invalid Score" Display="None"
                                                    Type="Integer" Operator="LessThan" ValueToCompare="32767" ControlToValidate="txtScore">Invalid Score</asp:CompareValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Comments">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtComments" CssClass="ardatalistcontent2" runat="server" Text='<%# Container.DataItem("Comments") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                            <asp:Panel ID="pnlCourseGBW" runat="server" Visible="true" ></asp:Panel>
                        </td>
                    </tr>
                </table>
                <table class="contenttable" cellspacing="0" cellpadding="0" width="30%" align="center">
                    <tr>
                        <td class="twocolumnlabelcell" style="padding-top: 20px">
                            <asp:Label ID="Label1" runat="server" CssClass="Label">Current Score</asp:Label></td>
                        <td class="twocolumncontentcell" style="padding-top: 20px">
                            <asp:TextBox ID="txtFinalScore" runat="server" CssClass="TextBox" ReadOnly="True"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="twocolumnlabelcell">
                            <asp:Label ID="Label2" runat="server" CssClass="Label">Current Grade</asp:Label></td>
                        <td class="twocolumncontentcell">
                            <asp:TextBox ID="txtFinalGrade" runat="server" CssClass="TextBox" ReadOnly="True"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="twocolumnlabelcell">&nbsp;</td>
                        <td class="twocolumncontentcell">
                            <asp:CheckBox ID="chkIsIncomplete" runat="server" CssClass="Label" AutoPostBack="True" Text="Incomplete Grade" Visible="false"></asp:CheckBox></td>
                    </tr>
                </table>
                <asp:TextBox ID="txtStudentId" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtStEmploymentId" runat="server" Visible="false"></asp:TextBox>
                <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                <asp:TextBox ID="txtStudentDocs" runat="server" Visible="False"></asp:TextBox>
            </div>
        </div>

    </form>
</body>
</html>
