Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade

Partial Class AR_DisplayClassSections
    Inherits System.Web.UI.Page
    Dim ds As New DataSet
    Dim TermId As String = ""
    Dim CampGrpId As String = ""
    Dim NewCampGrpId As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            TermId = Request.QueryString("TermId")
            CampGrpId = Request.QueryString("CampGrpId")
            NewCampGrpId = Request.QueryString("NewCampGrpId")
            lblStatus.Text = "<Strong> Term " & Request.QueryString("Term") & " cannot be assigned to " & Request.QueryString("NewCampus") & "</strong> as the following classes belong to the " & Request.QueryString("OldCampusGroup")
            BindGridView(TermId, CampGrpId)
        End If
    End Sub
    Private Sub BindGridView(ByVal TermId As String, ByVal CampGrpId As String)
        ds = (New TermProgressFacade).ClsSectionForTerm(TermId, CampGrpId, NewCampGrpId)
        dgrdTransactionSearch.DataSource = ds
        dgrdTransactionSearch.DataBind()
    End Sub
End Class
