﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Web.Script.Serialization
Imports System.IO

Partial Class AR_FASAPCheck
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim chkDate As DateTime
    Protected campusId As String
    Dim resourceId As Integer
    Dim userId As String
    Protected pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'disable history button
        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            BuildCampusDDL()
            BuildProgramVersionsDDL()
        End If

    End Sub

    Private Sub BuildCampusDDL()

        'The user should be restricted to the campus that he/she is logged in to
        Dim campusId As String
        Dim campDescrip As String
        Dim fac As New CampusGroupsFacade

        campusId = Master.CurrentCampusId
        campDescrip = fac.GetCampusDescrip(campusId)

        ddlCampusId.Items.Insert(0, New ListItem(campDescrip, campusId))


    End Sub

    Private Sub BuildProgramVersionsDDL()
        Dim fac As New ProgVerFacade
        Dim dt As New DataTable

        dt = fac.GetFASAPProgVersionsByUser(Master.CurrentCampusId)

        With ddlPrgVerId
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, New ListItem("All Programs", ""))
            .SelectedIndex = 0
        End With

    End Sub


    Private Sub ddlCampusId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampusId.SelectedIndexChanged
        'If Not ddlCampusId.SelectedValue = Guid.Empty.ToString Or Not ddlCampusId.SelectedValue = "" Then
        '    BuildTermDDL(ddlCampusId.SelectedValue)
        'End If
    End Sub

    Private Sub btnGetList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetList.Click
        Dim facade As New SAPCheckFacade
        Dim getEnrollments As New LeadFacade
        Dim StdArrayList As New ArrayList
        Dim Student As New SAPCheckInfo
        Dim ErrStr As String
        Dim getStudentEnrollments As New ArrayList
        'Dim strStudentId As String = "11AE7991-2198-4AAB-AAD3-73744F9E307A"
        'Student.StudentId = XmlConvert.ToGuid(strStudentId)
        chkDate = Date.Now
        ViewState("chkDate") = chkDate
        If Not facade.CheckFASAPDetailsExists(ddlPrgVerId.SelectedValue) Then
            DisplayErrorMessage("FASAP check cannot be run because there are no FASAP policy details setup.")
            Exit Sub
        End If
        getStudentEnrollments = getEnrollments.GetStudentsByCampusFASAP(ddlCampusId.SelectedValue, ddlPrgVerId.SelectedValue)


        ErrStr = facade.GetFASAPPolicyDetails(getStudentEnrollments, Session("UserName"), ddlCampusId.SelectedValue)

        BindDataGrid()
        btnBuildList.Visible = True
        btnBuildList.Enabled = True
        btnExportToExcell.Visible = True
        dgrdTransactionSearch.Columns(8).Visible = True
        If (ErrStr <> "") Then
            If (ErrStr.IndexOf("ErrStr2") <> -1) Then
                DisplayErrorMessage(ErrStr.Replace("ErrStr2", ""))
            End If
        End If

    End Sub

    Private Function CheckSAPDetailsExists(ByVal prgVerId As String) As Boolean
        Dim rtn As Boolean = True


        Return rtn
    End Function

    Private Sub BindDataGrid()
        Dim getSAPResults As New LeadFacade


        dgrdTransactionSearch.DataSource = getSAPResults.GetFASAPResults1(ddlCampusId.SelectedValue, ViewState("chkDate"), ddlPrgVerId.SelectedValue)
        dgrdTransactionSearch.DataBind()

        If dgrdTransactionSearch.Items.Count > 0 Then
            If CInt(CType(dgrdTransactionSearch.Items(0).FindControl("lblSAPTerminationCnt"), Label).Text) > 0 Then
                dgrdTransactionSearch.Columns(7).Visible = True
            Else
                dgrdTransactionSearch.Columns(7).Visible = False
            End If
        End If

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub


    Public Function GetTerminateEnabled(ByVal ProbationCount As Integer, ByVal TerminationProbationCount As Integer) As Boolean
        If (ProbationCount >= TerminationProbationCount And TerminationProbationCount <> 0) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function GetTerminateEnabledDesc(ByVal ProbationCount As Integer, ByVal TerminationProbationCount As Integer) As String
        If (ProbationCount >= TerminationProbationCount And TerminationProbationCount <> 0) Then
            Return "Student failed FASAP check " & TerminationProbationCount & " time(s)."
        Else
            Return ""
        End If

    End Function

    Public Function GetConsequence(ByVal ConsequenceTypId As Integer) As String
        If ConsequenceTypId = 1 Then
            Return "Academic Probation (SAP)"
        Else
            Return "Termination"
        End If
    End Function

    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        Dim fac As New SAPCheckFacade
        Dim dtStudent As New DataTable
        Dim dtResults As New DataTable
        dtStudent.Columns.Add("FirstName", Type.GetType("System.String"))
        dtStudent.Columns.Add("LastName", Type.GetType("System.String"))
        dtStudent.Columns.Add("PrgVerId", Type.GetType("System.String"))
        dtStudent.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dtStudent.Columns.Add("PrgVerDescrip", Type.GetType("System.String"))
        dtStudent.Columns.Add("Period", Type.GetType("System.String"))
        dtStudent.Columns.Add("ModDate", Type.GetType("System.String"))
        dtStudent.Columns.Add("MakingSAP", Type.GetType("System.String"))
        dtStudent.Columns.Add("Comments", Type.GetType("System.String"))
        dtStudent.Columns.Add("StudentStatus", Type.GetType("System.String"))
        dtStudent.Columns.Add("ConsequenceTypId", Type.GetType("System.String"))
        dtStudent.Columns.Add("ProbationCount", Type.GetType("System.String"))
        dtStudent.Columns.Add("TerminationProbationCount", Type.GetType("System.String"))
        dtStudent.Columns.Add("SAPTerminationCnt", Type.GetType("System.String"))
        dtStudent.Columns.Add("modUser", Type.GetType("System.String"))
        dtStudent.Columns.Add("campusId", Type.GetType("System.String"))
        Dim dgrdItem As DataGridItem
        For Each dgrdItem In dgrdTransactionSearch.Items

            Dim myCheckbox As CheckBox = CType(dgrdItem.Cells(8).Controls(1), CheckBox)
            If myCheckbox.Checked = True Then
                Dim drSC As DataRow = dtStudent.NewRow
                Dim strName As String() = CType(dgrdItem.Cells(0).Controls(1), LinkButton).Text.Split(" ")
                drSC("FirstName") = strName(0)
                drSC("LastName") = strName(1)
                drSC("PrgVerid") = CType(dgrdItem.Cells(0).Controls(1), LinkButton).CommandName
                drSC("StuEnrollId") = CType(dgrdItem.Cells(0).Controls(1), LinkButton).CommandArgument
                drSC("PrgVerDescrip") = CType(dgrdItem.Cells(1).Controls(1), Label).Text
                drSC("Period") = CType(dgrdItem.Cells(2).Controls(1), Label).Text
                drSC("ModDate") = CType(dgrdItem.Cells(3).Controls(1), Label).Text
                drSC("MakingSAP") = CType(dgrdItem.Cells(4).Controls(1), Label).Text
                drSC("StudentStatus") = CType(dgrdItem.Cells(5).Controls(1), Label).Text
                drSC("Comments") = CType(dgrdItem.Cells(6).Controls(1), Label).Text
                drSC("ConsequenceTypId") = CType(dgrdItem.Cells(9).Controls(1), Label).Text
                drSC("ProbationCount") = CType(dgrdItem.Cells(9).Controls(3), Label).Text
                drSC("TerminationProbationCount") = CType(dgrdItem.Cells(9).Controls(5), Label).Text
                drSC("SAPTerminationCnt") = CType(dgrdItem.Cells(9).Controls(5), Label).Text
                drSC("modUser") = Session("UserName")
                drSC("campusId") = ddlCampusId.SelectedValue
                dtStudent.Rows.Add(drSC)
            End If
        Next
        If dtStudent.Rows.Count = 0 Then
            BindDataGrid()
            DisplayErrorMessage("Check at least one student")
        Else
            dtResults = fac.PerformFASAPCheck(dtStudent)
            ViewState("dtResults") = dtResults
            dgrdTransactionSearch.Columns(8).Visible = False
            btnBuildList.Enabled = False
            With dgrdTransactionSearch
                .DataSource = dtResults
                .DataBind()
            End With
        End If
    End Sub
    Protected Sub btnExportToExcell_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcell.Click
        Try


            Dim oStringWriter As System.IO.StringWriter = New System.IO.StringWriter
            Dim oHtmlTextWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(oStringWriter)

            'delete from the panel all controls that are not literals
            HideAllTextBoxes(dgrdTransactionSearch)
            dgrdTransactionSearch.Columns(8).Visible = False
            'ClearControls(dgrdTransactionSearch)

            'render datagrid
            dgrdTransactionSearch.RenderControl(oHtmlTextWriter)

            'convert html to string
            Dim enc As System.Text.Encoding = System.Text.Encoding.UTF8
            Dim s As String = oStringWriter.ToString

            'move html content to a memorystream
            Dim documentMemoryStream As New System.IO.MemoryStream
            documentMemoryStream.Write(enc.GetBytes(s), 0, s.Length)

            '   save document and document type in sessions variables
            Session("DocumentMemoryStream") = documentMemoryStream
            Session("ContentType") = "application/vnd.ms-excel"
            'pnlAttendance.Visible = False
            'dgStudents.Visible = False
            If btnBuildList.Enabled = True Then
                BindDataGrid()
                dgrdTransactionSearch.Columns(8).Visible = True
            Else
                dgrdTransactionSearch.DataSource = ViewState("dtResults")
                dgrdTransactionSearch.DataBind()

            End If



            '   Setup the properties of the new window
            Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
            Dim name As String = "PrintAnyReport"
            Dim url As String = "../SA/" + name + ".aspx"
            CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)

            '   Register a javascript to open the report in another window
            ' Dim javascript As String = "<script>var origwindow=window.self;window.open('../SA/PrintAnyReport.aspx');</script>"
            ' Page.RegisterClientScriptBlock("NewWindow", javascript)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '   Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ExportToExcel " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ExportToExcel " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub


    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step i - 1
            ClearControls(control.Controls(i))
        Next
        If control.GetType.ToString = "System.Web.UI.HtmlControls.HtmlTableCell" Then
            'If control.GetType.ToString = "System.Web.UI.WebControls.TableCell" Then
            If Not control.GetType().GetProperty("SelectedItem") Is Nothing Then
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            Else
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            End If
        End If
        If control.GetType.ToString = "System.Web.UI.WebControls.CheckBox" Then
            'If control.GetType.ToString = "System.Web.UI.WebControls.TableCell" Then
            If Not control.GetType().GetProperty("SelectedItem") Is Nothing Then
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            Else
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            End If
        End If
        Return
    End Sub

    Private Sub HideAllTextBoxes(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step i - 1
            HideAllTextBoxes(control.Controls(i))
        Next


        If control.GetType.ToString = "System.Web.UI.WebControls.TextBox" Or control.GetType.ToString = "System.Web.UI.WebControls.CheckBox" Or control.GetType.ToString = "System.Web.UI.WebControls.LinkButton" Then

            'If control.GetType.ToString = "System.Web.UI.WebControls.TableCell" Then
            Dim literal As LiteralControl = New LiteralControl
            control.Parent.Controls.Add(literal)

            literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
            If literal.Text <> "" And control.GetType.ToString = "System.Web.UI.WebControls.CheckBox" Then literal.Text = "True <br>" & literal.Text

            'control.Parent.Controls.Remove(control)
            control.Visible = False


        End If
        Return
    End Sub

    Protected Sub dgrdTransactionSearch_ItemCommand1(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        Dim sb As New StringBuilder
        '   append PrgVersion to the querystring
        sb.Append("&ProgVerId=" + e.CommandName)
        ''   append ProgVerId Description to the querystring
        sb.Append("&EnrollmentId=" + e.CommandArgument)



        '   setup the properties of the new window
        'Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim winSettings As String = "status=yes,resizable=yes,scrollbars=yes,width=900px,height=720px"
        Dim name As String = "TranscriptPopUp"
        Dim url As String = "../AR/" + name + ".aspx?resid=252&mod=AR&cmpid=" + Request("cmpid") + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
End Class