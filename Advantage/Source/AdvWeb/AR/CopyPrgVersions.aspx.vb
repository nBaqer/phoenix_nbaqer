﻿Imports FAME.Common
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Xml
Imports Advantage.AFA.Integration
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports FAME.Integration.Adv.Afa.Messages.WebApi.Entities
Imports BO = Advantage.Business.Objects

Partial Class CopyPrgVersions
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Private MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim ds As New DataSet
        'Dim objCommon As New CommonUtilities
        Dim campusId As String
        'Dim userId As String
        '  Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        'Dim facCGrp As New CourseGrpFacade
        ' Dim ReqGrpId As String
        'Dim facade As New ReqGrpsDefFacade
        ' Dim AllDS As DataSet
        '  Dim LeadGrpDT As DataTable

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            If Not Page.IsPostBack Then
                resourceId = CInt(HttpContext.Current.Request.Params("resid"))
                campusId = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
                'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
                'userId = AdvantageSession.UserState.UserId.ToString


                Dim advantageUserState As New BO.User()
                advantageUserState = AdvantageSession.UserState

                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
                'Check if this page still exists in the menu while switching campus
                If Me.Master.IsSwitchedCampus = True Then
                    If pObj.HasNone = True Then
                        Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                        Exit Sub
                    Else
                        CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                    End If
                End If
                BuildDropDownLists(advantageUserState.UserId.ToString)
            End If
            'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

            'Header1.EnableHistoryButton(False)

            '   build dropdownlists


            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

        headerTitle.Text = Header.Title
    End Sub
    Private Sub BuildDropDownLists(ByVal UserId As String)
        BuildPrgVersionsDDL()
        BuildCampGrpDDL(UserId)
    End Sub
    'Private Sub BuildPrgVersionsDDL()
    '    '   create row filter and sort expression
    '    Dim facade As New CopyPrgVerFacade

    '    With ddlPrgVerId
    '        .DataTextField = "PrgVerDescrip"
    '        .DataValueField = "PrgVerId"
    '        .DataSource = facade.GetPrgVersions()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '    End With
    'End Sub
    Private Sub BuildPrgVersionsDDL()
        '   create row filter and sort expression
        Dim facade As New CopyPrgVerFacade

        With ddlPrgVerId
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = facade.GetPrgVersions(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        End With
    End Sub

    Private Sub BuildCampGrpDDL(ByVal UserId As String)
        '   create row filter and sort expression
        Dim facade As New CopyPrgVerFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = facade.GetCampGrpsForUser(UserId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        End With
    End Sub

    Private Sub ddlPrgVerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrgVerId.SelectedIndexChanged

    End Sub

    Private Sub btnCopyPrgVer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopyPrgVer.Click
        Dim facade As New CopyPrgVerFacade
        Dim message As String

        If ddlPrgVerId.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("The Program Version field cannot be empty. Please select an option.")
            Exit Sub
        Else
            ViewState("ProgVer") = ddlPrgVerId.SelectedItem.Value.ToString
        End If

        If ddlCampGrpId.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("The CampGrp field cannot be empty. Please select an option.")
            Exit Sub
        Else
            ViewState("CampGrp") = ddlCampGrpId.SelectedItem.Value.ToString
        End If

        If txtCode.Text = "" Or txtDescrip.Text = "" Then
            DisplayErrorMessage("The Code or Description fields cannot be empty. Please enter a value.")
            Exit Sub
        End If

        Dim createdId As Guid
        message = facade.CreateProgramVersion(txtCode.Text, txtDescrip.Text, ViewState("CampGrp"), ViewState("ProgVer"), AdvantageSession.UserState.UserName, createdId)

        If message <> "" Then
            DisplayErrorMessage(message)
        Else
            Try
                If (Not createdId = Guid.Empty) Then
                    'Copy of program version created, communicate it to AFA
                    SyncCopiedProgramVersionWithAFA(createdId, txtDescrip.Text, ViewState("CampGrp").ToString)
                End If
            Catch
            End Try
            DisplayErrorMessage("The new program version has been created.Please view the program versions page to view/modify this version")
            ClearRHS()
        End If
    End Sub

    Private Sub SyncCopiedProgramVersionWithAFA(programVersionId As Guid, programName As String, campGrpId As String)
        Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
        Dim time = DateTime.Now
        Dim locations = New LocationsHelper(connectionString).GetAFALocationsByCampusGroup(campGrpId)
        If (locations.Count > 0) Then
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)

            If (wapiSetting IsNot Nothing) Then
                Dim afaPrograms = locations _
                        .Select(Function(loc) New AdvStagingProgramV1 With {.LocationCMSID = loc, .ProgramVersionID = programVersionId, .DateCreated = time, .ProgramName = programName,
                                   .DateUpdated = time, .UserCreated = AdvantageSession.UserState.UserName, .UserUpdated = AdvantageSession.UserState.UserName}).ToList()

                Try
                    Dim pvReq = New FAME.AFA.Api.Library.AcademicRecords.ProgramVersionRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    Dim result = pvReq.SendNewProgramVersion(afaPrograms)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)


                End Try
            End If
        End If

    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet

        Try
            ClearRHS()

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

End Class
