<%@ Page Language="vb" AutoEventWireup="false" Inherits="StudentAttendancePrint" CodeFile="StudentAttendancePrint.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Student Attendance Print</title>
    <link href="../CSS/localhost.css" type="text/css" media="screen" rel="stylesheet">
    <link href="../CSS/print.css" type="text/css" media="print" rel="stylesheet">
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script language="JavaScript" type="text/JavaScript">
<!--
    function MM_openBrWindow(theURL, winName, features) { //v2.0
        window.open(theURL, winName, features);
    }
    //-->
    </script>
<%--<style type="text/css">
<!--    
body
{
    scrollbar-face-color: #000000;
    scrollbar-shadow-color: #2D2C4D;
    scrollbar-highlight-color:#7D7E94;
    scrollbar-3dlight-color: #7D7E94;
    scrollbar-darkshadow-color: #2D2C4D;
    scrollbar-track-color: #7D7E94;
    scrollbar-arrow-color: #C1C1D1;
}
 -->
</style>--%>
</head>
<body>

    <form id="Form1" method="post" runat="server">
            <table width="50%" cellspacing="0" cellpadding="0" align="center" border="0">
                <tr>
                    <td class="twocolumnlabelcell">
                        <asp:Label ID="lblStudent" runat="server" CssClass="label" Font-Bold="True">Student:</asp:Label></td>
                    <td class="twocolumncontentcell" style="text-align: left">
                        <asp:Label ID="lblStudentName" runat="server" CssClass="label"></asp:Label></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell">
                        <asp:Label ID="lblEnrollment" runat="server" CssClass="label" Font-Bold="True">Enrollment:</asp:Label></td>
                    <td class="twocolumncontentcell" style="text-align: left">
                        <asp:Label ID="lblEnrollmentDescrip" runat="server" CssClass="label"></asp:Label></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell">
                        <asp:Label ID="lblTerm" runat="server" CssClass="label" Font-Bold="True">Term:</asp:Label></td>
                    <td class="twocolumncontentcell" style="text-align: left">
                        <asp:Label ID="lblTermDescrip" runat="server" CssClass="label"></asp:Label></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell">
                        <asp:Label ID="lblClsSection" runat="server" CssClass="label" Font-Bold="True">Class Section:</asp:Label></td>
                    <td class="twocolumncontentcell" style="text-align: left">
                        <asp:Label ID="lblClassSection" runat="server" CssClass="label"></asp:Label></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell">
                        <asp:Label ID="lblClsDuration" runat="server" CssClass="label" Font-Bold="True">Class Duration:</asp:Label></td>
                    <td class="twocolumncontentcell" style="text-align: left">
                        <asp:Label ID="lblClassDuration" runat="server" CssClass="label"></asp:Label></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell">
                        <asp:Label ID="lblInstr" runat="server" CssClass="label" Font-Bold="True">Instructor:</asp:Label></td>
                    <td class="twocolumncontentcell" style="text-align: left">
                        <asp:Label ID="lblInstructor" runat="server" CssClass="label"></asp:Label></td>
                </tr>
            </table>
            <div class="scrollwholeprint" style="overflow:auto;">
                <table width="100%" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td class="spacertables2"></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:Panel ID="pnlAttendance" Width="100%" runat="server"></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="spacertables2"></td>
                    </tr>
                </table>
                <div class="spacer">&nbsp;</div>
                <table width="100%" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td style="text-align: center; padding: .1em 1.4em .1em 1.4em">

                            <asp:Panel ID="pnlTotalAttendance" runat="server" Visible="true">

                                <table width="100%" cellspacing="0" cellpadding="0" align="left" border="0">
                                    <tr>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblRow1" CssClass="label" runat="server" Font-Bold="True">Actual Totals</asp:Label></td>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblActPresent" CssClass="label" runat="server"></asp:Label></td>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblActAbsent" CssClass="label" runat="server"></asp:Label></td>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblActTardy" CssClass="label" runat="server"></asp:Label></td>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblActExcused" CssClass="label" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblRow2" CssClass="label" runat="server" Font-Bold="True">Adjusted Totals</asp:Label></td>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblTtlPresent" CssClass="label" runat="server"></asp:Label></td>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblTtlAbsent" CssClass="label" runat="server"></asp:Label></td>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblTtlTardy" CssClass="label" runat="server"></asp:Label></td>
                                        <td class="attendanceprintcolumns">
                                            <asp:Label ID="lblTtlExcused" CssClass="label" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="attendanceprintcolumns" style="width: 100%" colspan="5">
                                            <asp:Label ID="lblTardiesMsg" CssClass="label" runat="server" Font-Bold="True"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="attendanceprintcolumns" style="width: 100%" colspan="5">
                                            <asp:Label ID="lblPerc" CssClass="label" runat="server" Font-Bold="True"></asp:Label></td>
                                    </tr>
                                </table>

                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
    </form>

</body>
</html>
