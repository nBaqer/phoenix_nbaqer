Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class CourseGroups
    Inherits BasePage
    Protected WithEvents HyperLink1 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private validatetxt As Boolean = False
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = CInt(m_Context.Items("ResourceId"))

        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim facade As New CourseGrpFacade
        Dim SDFControls As New SDFComponent

        Dim coursegrpobj As New CourseGrpInfo

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then

                'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

                'Populate and bind ListBoxes
                PopulateStatusesListBox()
                PopulateCampGrpsListBox()

                'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                'Set the text box to a new guid
                txtReqId.Text = System.Guid.NewGuid.ToString

                'Set the default moduser and moddate
                txtModUser.Text = coursegrpobj.ModUser
                txtModDate.Text = coursegrpobj.ModDate.ToString

                'Color the required fields and set captions.
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                'Populate the datalist

                ds = facade.GetAllCourseGrps()
                PopulateDataList(ds)


            Else
                'Color the required fields and set captions.
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)

            End If

            'Check If any UDF exists for this resource
            Dim intSDFExists As Integer = SDFControls.GetSDFExists(ResourceId, ModuleId)
            If intSDFExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If

            If Trim(txtCode.Text) <> "" Then
                SDFControls.GenerateControlsEditSingleCell(pnlSDF, ResourceId, txtReqId.Text, ModuleId)
            Else
                SDFControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub
    Private Sub PopulateStatusesListBox()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New CourseGrpFacade

        ' Bind the Dataset to the CheckBoxList
        ddlStatusId.DataSource = facade.GetAllStatuses()
        ddlStatusId.DataTextField = "Status"
        ddlStatusId.DataValueField = "StatusId"
        ddlStatusId.DataBind()
    End Sub

    Private Sub PopulateCampGrpsListBox()

        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        ddlList.Add(New AdvantageDDLDefinition(ddlCampGrpId, AdvantageDropDownListName.CampGrps, Nothing, True, True, String.Empty))
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As DataSet
        'Dim objListGen As New DataListGenerator
        'Dim sw As New System.IO.StringWriter
        Dim facade As New CourseGrpFacade
        Dim savedguid As String
        Dim result As String
        '        Dim msg As String
        Try
            If validatetxt = True Then
                Exit Sub
            End If
            If ViewState("MODE") = "NEW" Then

                'Populate course object with the info.
                'Move the values in the controls over to the object
                Dim courseGrpObj As CourseGrpInfo

                courseGrpObj = PopulateCourseGrpObject()

                'Insert CampusGroup into syCampGrps Table
                result = facade.InsertCourseGrp(courseGrpObj, AdvantageSession.UserState.UserName)

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                    Exit Sub
                Else
                    GetCourseGrpDetails(txtReqId.Text)
                End If
                'Populate the datalist
                ds = facade.GetAllCourseGrps()
                PopulateDataList(ds)

                'Set Mode to Edit
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                savedguid = txtReqId.Text
            Else


                'Populate course object with the info.
                'Move the values in the controls over to the object
                Dim courseGrpObj As CourseGrpInfo

                courseGrpObj = PopulateCourseGrpObject()

                'Insert CampusGroup into syCampGrps Table
                result = facade.UpdateCourseGrp(courseGrpObj, AdvantageSession.UserState.UserName)

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                    Exit Sub
                Else
                    GetCourseGrpDetails(txtReqId.Text)
                End If
                'Populate the datalist
                ds = facade.GetAllCourseGrps()
                PopulateDataList(ds)


            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim sdfid As ArrayList
            Dim sdfidValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtReqId.Text)
                sdfid = SDFControl.GetAllLabels(pnlSDF)
                sdfidValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To sdfid.Count - 1
                    SDFControl.InsertValues(txtReqId.Text, Mid(sdfid(z).id, 5), sdfidValue(z))
                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstCourseGrp, txtReqId.Text)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Function PopulateCourseGrpObject() As CourseGrpInfo
        Dim courseGrpObj As New CourseGrpInfo


        courseGrpObj.CourseGrpId = XmlConvert.ToGuid(txtReqId.Text)
        courseGrpObj.CourseGrpCode = txtCode.Text
        courseGrpObj.CourseGrpDescrip = txtDescrip.Text

        If (txtCredits.Text) <> "" Then
            courseGrpObj.CourseGrpCredits = Decimal.Parse(txtCredits.Text)
        End If
        If (txtHours.Text) <> "" Then
            courseGrpObj.CourseGrpHours = Decimal.Parse(txtHours.Text)
        End If
        'CourseGrpObj.CourseGrpCode = 1
        courseGrpObj.CampGrpId = XmlConvert.ToGuid(ddlCampGrpId.SelectedItem.Value)

        courseGrpObj.StatusId = XmlConvert.ToGuid(ddlStatusId.SelectedItem.Value)

        courseGrpObj.ReqTypeId = 2
        Return courseGrpObj
    End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        'Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim facade As New CourseGrpFacade
        Dim coursegrpobj As New CourseGrpInfo

        Try
            ClearRHS()
            'Populate the datalist
            ds = facade.GetAllCourseGrps()
            PopulateDataList(ds)

            'Set the text box to a new guid
            txtReqId.Text = System.Guid.NewGuid.ToString

            'Set the default moduser and moddate
            txtModUser.Text = coursegrpobj.ModUser
            txtModDate.Text = coursegrpobj.ModDate.ToString

            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Reset Style in the Datalist
            'CommonWebUtilities.SetStyleToSelectedItem(dlstCourseGrp, Guid.Empty.ToString, ViewState, Header1)

            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
            CommonWebUtilities.RestoreItemValues(dlstCourseGrp, Guid.Empty.ToString)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim facade As New CourseGrpFacade
        Dim coursegrpobj As New CourseGrpInfo

        Try
            Dim result As String = facade.DeleteCourseGrp(txtReqId.Text, Date.Parse(txtModDate.Text))
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                ClearRHS()
                'Populate the datalist
                ds = facade.GetAllCourseGrps()
                PopulateDataList(ds)

                'Set the default moduser and moddate
                txtModUser.Text = coursegrpobj.ModUser
                txtModDate.Text = coursegrpobj.ModDate.ToString

                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                'Set the text box to a new guid
                txtReqId.Text = System.Guid.NewGuid.ToString

            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim SDFControl As New SDFComponent
            SDFControl.DeleteSDFValue(ResourceId)
            SDFControl.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
            CommonWebUtilities.RestoreItemValues(dlstCourseGrp, Guid.Empty.ToString)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstCourseGrp_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstCourseGrp.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim facade As New CourseGrpFacade
        Dim objListGen As New DataListGenerator

        Try
            strGUID = dlstCourseGrp.DataKeys(e.Item.ItemIndex).ToString()
            Master.PageObjectId = e.CommandArgument
            Master.PageResourceId = ResourceId
            Master.setHiddenControlForAudit()

            Master.PageObjectId = e.CommandArgument
            Master.PageResourceId = ResourceId
            Master.setHiddenControlForAudit()

            GetCourseGrpDetails(strGUID)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstCourseGrp.SelectedIndex = selIndex
            'Populate the datalist
            ds = facade.GetAllCourseGrps()
            PopulateDataList(ds)

            'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
            'SDF Code Starts Here
            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsEditSingleCell(pnlSDF, ResourceId, e.CommandArgument, ModuleId)

            CommonWebUtilities.RestoreItemValues(dlstCourseGrp, strGUID)

            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub  dlstCourses_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub  dlstCourses_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub GetCourseGrpDetails(ByVal CourseGrpID As String)
        Dim facade As New CourseGrpFacade
        BindCourseGrpInfoExist(facade.GetCourseGrpDetails(CourseGrpID))
    End Sub

    Private Sub BindCourseGrpInfoExist(ByVal CourseGrpInfo As CourseGrpInfo)
        'Bind The StudentInfo Data From The Database
        With CourseGrpInfo
            txtReqId.Text = .CourseGrpId.ToString
            'ddlCampGrpId.SelectedValue = .CampGrpId.ToString
            ddlStatusId.SelectedValue = .StatusId.ToString
            txtCode.Text = .CourseGrpCode
            txtDescrip.Text = .CourseGrpDescrip
            txtCredits.Text = .CourseGrpCredits
            txtHours.Text = .CourseGrpHours
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCampGrpId, .CampGrpId.ToString, .CampGrpDescrip)
        End With
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/19/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radstatus.SelectedItem.Text.ToLower = "active") Or (radstatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radstatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "Descrip", DataViewRowState.CurrentRows)

        With dlstCourseGrp
            .DataSource = dv
            .DataBind()
        End With
        dlstCourseGrp.SelectedIndex = -1

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities
        Dim facade As New CourseGrpFacade

        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtReqId.Text = Guid.NewGuid.ToString
            ds = facade.GetAllCourseGrps()
            PopulateDataList(ds)

            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Public Sub TextValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        Dim strMinus As String = Mid(args.Value, 1, 1)
        If strMinus = "-" Then
            DisplayErrorMessage("The Value Cannot be Negative")
            validatetxt = True
        End If
    End Sub

    Private Sub Linkbutton3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkbutton3.Click
        Dim sb As New StringBuilder

        '   append PrgVerId to the querystring
        sb.Append("&CourseGrpId=" + txtReqId.Text)
        '   append Course Description to the querystring
        sb.Append("&Hours=" + txtHours.Text)
        '   append Course Description to the querystring
        sb.Append("&Credits=" + txtCredits.Text)
        '   append Course Description to the querystring
        sb.Append("&CourseGrp=" + txtCode.Text)

        '   setup the properties of the new window
        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "CourseGrpDefs"
        Dim url As String = "../AR/" + name + ".aspx?resid=45&mod=SA&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(Linkbutton3)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

End Class
