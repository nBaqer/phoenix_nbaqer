﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentAddresses.aspx.vb" Inherits="StudentAddresses" EnableEventValidation="false" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script type="text/javascript">


       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }


</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>

<%--<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstStdAddress">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstStdAddress"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstStdAddress"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstStdAddress"  />
                </UpdatedControls>
            </telerik:AjaxSetting>                       
        </AjaxSettings> 
</telerik:RadAjaxManagerProxy>--%>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">


                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="listframetop">
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td nowrap align="left" width="15%">
                                            <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                        <td nowrap width="85%">
                                            <asp:RadioButtonList ID="radStatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                CssClass="label">
                                                <asp:ListItem Text="Active" Selected="True" />
                                                <asp:ListItem Text="Inactive" />
                                                <asp:ListItem Text="All" />
                                            </asp:RadioButtonList></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="listframebottom">
                                <div class="scrollleftfilters">
                                    <asp:DataList ID="dlstStdAddress" runat="server" Width="100%" DataKeyField="StdAddressId">
                                        <ItemStyle CssClass="itemstyle"></ItemStyle>
                                        <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                        <ItemTemplate>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td nowrap width="30%" valign="top">
                                                        <asp:ImageButton ID="imgInActive" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
                                                            CommandArgument='<%# Container.DataItem("StdAddressId")%>' CausesValidation="False"
                                                            ImageUrl="../images/Inactive.gif" />
                                                        <asp:ImageButton ID="imgActive" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>'
                                                            CommandArgument='<%# Container.DataItem("StdAddressId")%>' CausesValidation="False"
                                                            ImageUrl="../images/Active.gif" />
                                                        <asp:LinkButton Text='<%# Container.DataItem("AddressType") %>' runat="server" CssClass="itemstyle"
                                                            CommandArgument='<%# Container.DataItem("StdAddressId")%>' ID="Linkbutton2" CausesValidation="False" />
                                                    </td>
                                                    <td width="70%">
                                                        <asp:LinkButton Text='<%# Container.DataItem("Address1")%>' runat="server" CssClass="itemstyle"
                                                            CommandArgument='<%# Container.DataItem("StdAddressId")%>' ID="Linkbutton1" CausesValidation="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                    </div>
                            </td>
                        </tr>
                    </table>

 
    </telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
        <asp:Panel ID="pnlRHS" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2">
        <!-- begin content table-->
       
        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
            <tr>
                <td class="contentcell" nowrap>
                    &nbsp;</td>
                <td class="contentcell4" nowrap>
                    <asp:CheckBox ID="chkForeignZip" TabIndex="1" runat="server" AutoPostBack="true"
                        CssClass="checkboxinternational" Text="International"></asp:CheckBox></td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                    <asp:Label ID="lblAddress1" runat="server" CssClass="label">Address1</asp:Label></td>
                <td class="contentcell4">
                    <asp:TextBox ID="txtAddress1" TabIndex="2" runat="server" CssClass="textbox" MaxLength="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                    <asp:Label ID="lblAddress2" runat="server" CssClass="label">Address2</asp:Label></td>
                <td class="contentcell4">
                    <asp:TextBox ID="txtAddress2" TabIndex="3" runat="server" CssClass="textbox" MaxLength="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                    <asp:Label ID="lblCity" runat="server" CssClass="label">City</asp:Label></td>
                <td class="contentcell4">
                    <asp:TextBox ID="txtCity" TabIndex="4" runat="server" AutoPostBack="True" CssClass="textbox"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                    <asp:Label ID="lblStateId" runat="server" CssClass="label"></asp:Label></td>
                <td class="contentcell4">
                    <asp:DropDownList ID="ddlStateId" TabIndex="5" runat="server" CssClass="dropdownlist">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                    <asp:Label ID="lblOtherState" runat="server" CssClass="label">Other state not listed</asp:Label></td>
                <td class="contentcell4">
                    <asp:TextBox ID="txtOtherState" runat="server" AutoPostBack="True" CssClass="textbox"
                        Enabled="False" Visible="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                    <asp:Label ID="lblZip" runat="server" CssClass="label">Zip</asp:Label></td>
                <td class="contentcell4">
                   <telerik:RadMaskedTextBox ID="txtZip" TabIndex="6" runat="server" CssClass="textbox"
                        Mask="#####" DisplayMask="#####" DisplayPromptChar="" AutoPostBack="false"
                        Width="100px">
                    </telerik:RadMaskedTextBox>
                </td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                    <asp:Label ID="lblCountryId" runat="server" CssClass="label"></asp:Label></td>
                <td class="contentcell4">
                    <asp:DropDownList ID="ddlCountryId" TabIndex="7" runat="server" CssClass="dropdownlist">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                    <asp:Label ID="lblAddressTypeId" runat="server" CssClass="label"></asp:Label></td>
                <td class="contentcell4">
                    <asp:DropDownList ID="ddlAddressTypeId" TabIndex="8" runat="server" CssClass="dropdownlist">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="contentcell">
                    <asp:Label ID="lblStatusId" runat="server" CssClass="label">Status</asp:Label></td>
                <td class="contentcell4">
                    <asp:DropDownList ID="ddlStatusId" TabIndex="9" runat="server" CssClass="dropdownlist">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="contentcell" nowrap>
                </td>
                <td class="contentcell4date">
                    <asp:CheckBox ID="chkdefault1" TabIndex="10" runat="server" CssClass="label" Text="Default Address">
                    </asp:CheckBox></td>
            </tr>
        </table>
        
        <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="contentcellheader" nowrap>
                    <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label></td>
            </tr>
        </table>
        </asp:Panel>
        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="contentcell2" colspan="6">
                <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false">
                </asp:Panel>
            </td>
        </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="contentcell" nowrap>
                <asp:TextBox ID="txtStdAddressId" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtStudentId" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                <asp:CheckBox ID="chkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox></td>
            <td class="contentcell4">
                <asp:TextBox ID="txtRowIds" Visible="false"  Style="visibility: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox>
                <asp:TextBox ID="txtResourceId" Visible="false" Style="visibility: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox></td>
        </tr>
        </table>
        <!-- end content table-->
        </div>
        </td>
        </tr>
        </table>
        </asp:Panel>
    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

