Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.Common

Partial Class StudentExams
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbxAvailStuds As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbxSelectStuds As System.Web.UI.WebControls.ListBox
    Protected StudentId, campusid As String


#End Region
    Private writtenExamsCount As Integer
    Private practicalExamsCount As Integer
    Private writtenExamsAverage As Decimal
    Private practicalExamsAverage As Decimal
    Private writtenExamsSumOfWeights As Decimal
    Private practicalExamsSumOfWeights As Decimal
    Protected LeadId As String
    Protected state As AdvantageSessionState

    Dim resourceId As Integer

    Protected boolSwitchCampus As Boolean = False
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As Advantage.Business.Objects.StudentMRU

        Dim objStudentState As New Advantage.Business.Objects.StudentMRU

        Try
            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objCommon As New CommonUtilities
        campusid = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))

        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = getStudentFromStateObject(532) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        'Always disable the History button. It does not apply to this page.
        'Header1.EnableHistoryButton(False)
        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            BuildStudentEnrollmentsDDL(StudentId)
            BindRepeaters()
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            'End If
            MyBase.uSearchEntityControlId.Value = ""
        End If
        btnPrint.Attributes.Add("onClick", "getPrint('ContentMain2_print_area');")
    End Sub
    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade

        With ddlEnrollmentId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()
            'If Not .Items.Count = 1 Then
            '    .Items.Insert(0, New ListItem("All Enrollments", Guid.Empty.ToString))
            'End If
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindRepeaters()
        rWrittenExams.DataSource = (New ExamsFacade).GetExamsResultsByStudent(ddlEnrollmentId.SelectedValue, 501)
        rWrittenExams.DataBind()
        rPracticalExams.DataSource = (New ExamsFacade).GetExamsResultsByStudent(ddlEnrollmentId.SelectedValue, 533)
        rPracticalExams.DataBind()

        dgWrittenExams.DataSource = (New ExamsFacade).GetExamsResultsByStudent(ddlEnrollmentId.SelectedValue, 501)
        dgWrittenExams.DataBind()
        dgPracticalExam.DataSource = (New ExamsFacade).GetExamsResultsByStudent(ddlEnrollmentId.SelectedValue, 533)
        dgPracticalExam.DataBind()


        Try
            Dim StuEnrollInfo As New StudentEnrollmentInfo
            StuEnrollInfo = (New StuEnrollFacade).GetStudentDetails(ddlEnrollmentId.SelectedValue)
            lblStudentName.Text = (New StuEnrollFacade).StudentName(StudentId) 'CType(Header1.FindControl("txtStudentName"), TextBox).Text
            With StuEnrollInfo
                lblEnrollmentName.Text = .ProgramVersion
                lblEnrollmentStatus.Text = .StatusCode
                lblStartDate.Text = .StartDate
                lblGD.Text = .ExpGradDate
            End With
            pnlStudentInfo.Visible = True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            btnPrint.Visible = False
            pnlStudentInfo.Visible = False
            Exit Sub
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Function ConvertIsPassToString(ByVal isPass As Object) As String
        If isPass Is System.DBNull.Value Then Return String.Empty
        If CType(isPass, Boolean) Then Return "P" Else Return "F"
    End Function
    Protected Function ConvertToStringForT(ByVal score As Object) As String
        If score Is System.DBNull.Value Then Return String.Empty
        If score = "-1.00" Then Return "T" Else Return score
    End Function

    Protected Sub rWrittenExams_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rWrittenExams.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                writtenExamsCount += 1
                If Not (e.Item.DataItem("Score") Is System.DBNull.Value Or e.Item.DataItem("Weight") Is System.DBNull.Value) Then
                    writtenExamsAverage += CType(e.Item.DataItem("Score"), Decimal) * CType(e.Item.DataItem("Weight"), Decimal)
                    writtenExamsSumOfWeights += CType(e.Item.DataItem("Weight"), Decimal)
                End If
        End Select
    End Sub
    Protected Sub rPracticalExams_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rPracticalExams.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                practicalExamsCount += 1
                If Not (e.Item.DataItem("Score") Is System.DBNull.Value Or e.Item.DataItem("Weight") Is System.DBNull.Value) Then
                    practicalExamsAverage += CType(e.Item.DataItem("Score"), Decimal) * CType(e.Item.DataItem("Weight"), Decimal)
                    practicalExamsSumOfWeights += CType(e.Item.DataItem("Weight"), Decimal)
                End If
        End Select
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        lLeft.Text = "Written Exams (" + writtenExamsCount.ToString() + ") "

        'If writtenExamsSumOfWeights > 0.0 Then
        '    lLeft.Text += "Average: " + (writtenExamsAverage / writtenExamsSumOfWeights).ToString("##0.00")
        'Else
        '    lLeft.Text += "Average: 0.00"
        'End If

        lRight.Text = "Practical Exams (" + practicalExamsCount.ToString() + ") "
        'If practicalExamsSumOfWeights > 0 Then
        '    lRight.Text += "Average: " + (practicalExamsAverage / practicalExamsSumOfWeights).ToString("##0.00")
        'Else
        '    lRight.Text += "Average: 0.00"
        'End If

        'If writtenExamsSumOfWeights + practicalExamsSumOfWeights > 0.0 Then
        '    lOverallAverage.Text = CType((writtenExamsAverage + practicalExamsAverage) / (writtenExamsSumOfWeights + practicalExamsSumOfWeights), Decimal).ToString("##0.00")

        'End If
        GetOverallAverageForEnrollment(ddlEnrollmentId.SelectedValue)
        BIndToolTip()
    End Sub

    Protected Sub ddlEnrollmentId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
        BindRepeaters()
    End Sub

    Private Sub GetOverallAverageForEnrollment(ByVal StuEnrollId As String)
        Dim fac As New ExamsFacade
        lOverallAverage.Text = fac.GetOverallAverageForEnrollment(StuEnrollId)
    End Sub


    ' ''Added by Saraswathi lakshmanan on August 24 2009
    ' ''To find the list controls and add a tool tip to those items in the control
    ' ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

End Class
