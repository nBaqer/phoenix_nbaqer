﻿
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrgVersionDefaultCharge.aspx.vb"
    Inherits="AR_PrgVersionDefaultCharge" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Set Up Default Periods For Charges</title>
<%--    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <link href="../css/systememail.css" type="text/css" rel="stylesheet"/>--%>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
    <meta content="JavaScript" name="vs_defaultClientScript"/>
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
    <link href="../css/systememail.css" type="text/css" rel="stylesheet"/>
    <link href="../css/localhost.css" type="text/css" rel="stylesheet"/>
    <script language="javascript" src="ViewDocument.js"></script>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script language="javascript">
        function closewindow() {
            typeof (theChild);
            if (typeof (theChild) != "undefined") {
                if (theChild.open && !theChild.closed) {
                    theChild.close();
                }
            }
        }
			
    </script>
        <%: Styles.Render("~/bundles/popupstyle") %>
    <%: Scripts.Render("~/bundles/popupscripts") %>
    <script type="text/javascript">
        //              window.onload = function () {
        //                  var strCook = document.cookie;
        //                  if (strCook.indexOf("!~") != 0) {
        //                      var intS = strCook.indexOf("!~");
        //                      var intE = strCook.indexOf("~!");
        //                      var strPos = strCook.substring(intS + 2, intE);
        //                      document.getElementById("grdWithScroll").scrollTop = strPos;
        //                  }

        //              }
        //              function SetDivPosition() {
        //                  var intY = document.getElementById("grdWithScroll").scrollTop;
        //                  document.cookie = "yPos=!~" + intY + "~!";
        //              }
        function RowDblClick(sender, eventArgs) {
            sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
        }
         
    </script>
    <style type="text/css">
        .style1
        {
            width: 757px;
        }


    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td nowrap style="font-family: Verdana; font-size: large; color: #FFFFFF; background-color: #164D7F">
                Set Up Default Periods For Charges
            </td>
            <td class="topemail">
                <a class="close" href="#" onclick="top.close()">X Close</a>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
        <tr>
            <td class="DetailsFrameTopemail" style="width: 100%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="MenuFrame" align="right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                            <asp:Button
                                ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                Enabled="False"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                Enabled="false"></asp:Button>
                        </td>
                    </tr>
                </table>
                <!-- end top menu (save,new,reset,delete,history)-->
            </td>
        </tr>
    </table>
    <div>
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" />
        <asp:ScriptManager ID="scriptmanager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
        <div style="width:90%;padding:25px;">
                    <telerik:RadGrid ID="RadGrdDefaultCharges" GridLines="None" runat="server" AllowAutomaticDeletes="True"
            HeaderStyle-HorizontalAlign="Center" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
            AllowPaging="True" AutoGenerateColumns="False" Width="100%" AllowMultiRowEdit="false"
            >
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView Width="100%" CommandItemDisplay="top" DataKeyNames="PrgVerPmtId"
                AutoGenerateColumns="False" EditMode="InPlace" PageSize="10">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                        <ItemStyle CssClass="MyImageButton" />
                    </telerik:GridEditCommandColumn>
                    <telerik:GridTemplateColumn UniqueName="SystemTransCode" HeaderStyle-Width="20%"
                        ItemStyle-Width="20%">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" CssClass="title" Text="Charge Type" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label CssClass="label" ID="lblTransCodeDesc" runat="server" Text='<%# Eval("TransDescription") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList CssClass="DropDownList" ID="ddlSysTransCodes" runat="server" DataSource='<%# dtTransCodes %>'
                                DataTextField="Description" DataValueField="SysTransCodeId">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="20%" />
                        <ItemStyle Width="20%" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="DefaultPeriod" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" CssClass="title" Text="Default Period" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label CssClass="label" ID="lblfeedesc" runat="server" Text='<%# Eval("FeeDescription") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList CssClass="DropDownList" ID="ddlDefaultPeriod" runat="server" DataSource='<%# dtFeeLevels %>'
                                DataTextField="Description" DataValueField="FeeLevelId">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="20%" />
                        <ItemStyle Width="20%" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="PeriodCanChange" HeaderStyle-Width="20%"
                        ItemStyle-Width="20%">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" CssClass="title" Text="Period Can Change?" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkperiodCanChange" runat="server" Checked='<%# Eval("PeriodCanChange") %>'
                                Enabled="false" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkeditperiodCanChange" runat="server" />
                        </EditItemTemplate>
                        <HeaderStyle Width="20%" />
                        <ItemStyle Width="20%" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="ContainerHidden" Visible="false">
                        <ItemTemplate>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label runat="server" ID="lblCmdType" Text='<%# Eval("CmdType") %>' Visible="false">
                            </asp:Label>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                        UniqueName="DeleteColumn">
                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings>
                    <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                    <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
                    <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" BackColor="White"
                        Width="100%" />
                    <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px" BackColor="White" />
                    <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                    <EditColumn ButtonType="ImageButton" InsertText="Insert record" UpdateText="Update record"
                        UniqueName="EditCommandColumn1" CancelText="Cancel edit">
                    </EditColumn>
                    <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
                </EditFormSettings>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick="RowDblClick" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
        </telerik:RadGrid>
        </div>

       <%-- <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
    </div>
    </form>
</body>
</html>
