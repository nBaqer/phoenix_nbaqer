﻿Imports System.Collections
Imports System.Data
Imports System.Diagnostics
Imports System.Windows.Forms
Imports Advantage.Business.Objects
Imports Fame.Advantage.Api.Library.AcademicRecords
Imports Fame.Advantage.Api.Library.Models
Imports Telerik.Web.UI.Calendar
Imports Fame.Advantage.Common
Imports Fame.Advantage.DataAccess.LINQ
Imports Fame.AdvantageV1.BusinessFacade
Imports Fame.AdvantageV1.Common
Imports Telerik.Web.UI

Namespace AdvWeb.AR

    Partial Class SummaryGradePostings
        Inherits BasePage
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme

        End Sub
        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private pObj As New UserPagePermissionInfo
        Dim campusId As String
        Dim instructorId As String
        Dim username As String
        Dim isAcademicAdvisor As Boolean
        Dim ddlGrdCriteriaKey As String = "InstrGrdBkWgtDetailId"
        Protected MyAdvAppSettings As AdvAppSettings

        Public Class GlobalVariables
            Public Shared StudentEnrollmentsList As List(Of Guid) = New List(Of Guid)()
        End Class

#Region "Page events"
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add buttons 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnGetClsGrds)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
            'BindToolTip()
            BindToolTip("ddlTerm")
            BindToolTip("ddlClsSection")
            BindToolTip("ddlGrdCriteria")
        End Sub
        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            rdpPostdDateDefault.MaxDate = Today

            'Put user code to initialize the page here
            ' Dim ds As New DataSet
            ' Dim objCommon As New CommonUtilities
            'Dim campusId As String
            Dim userId As String
            'Dim m_Context As HttpContext
            'Dim fac As New UserSecurityFacade
            Dim resourceId As Integer

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            campusId = Master.CurrentCampusId
            'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
            userId = AdvantageSession.UserState.UserId.ToString

            Dim advantageUserState As User = AdvantageSession.UserState

            'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If


            If Not Page.IsPostBack Then
                'Dim objcommon As New CommonUtilities
                'Populate the Activities Data-grid with the person who is logged in Activities
                'Default the Selection Criteria for the activity records
                ViewState("Term") = ""
                ViewState("Class") = ""
                ViewState("GrdCriteria") = ""
                ViewState("MODE") = "NEW"
                ViewState("ClsSectIdGuid") = ""
                ViewState("SelectedIndex") = "-1"
                ViewState("Instructor") = userId
                PopulateTermDDL()
                PopulateShiftDDL()
                PopulateStudentGroupsDDL()
                rdpPostdDateDefault.SelectedDate = Today
                ''CohortStartDate DDL 
                PopulateCohortStartDateDDL()

                txtModUser.Text = AdvantageSession.UserState.UserName
                txtModDate.Text = Date.MinValue.ToString
                ViewState("MODE") = "NEW"
            End If

            If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If

            btnNew.Enabled = False
            btnDelete.Enabled = False
            ''If the UseCohortStartDateforfilter is set to Yes in WebConfigAppSettings 
            ''then the Term DropDownList is made visible false and CohortStartdateDropDown 
            ''is made visible true

            If MyAdvAppSettings.AppSettings("UseCohortStartDateForFilters").ToString.ToLower = "yes" Then
                trlblTerm.Visible = False
                trTerm.Visible = False
                trlblCohortStartDate.Visible = True
                trCohortStartDate.Visible = True
            Else
                trlblTerm.Visible = True
                trTerm.Visible = True
                trlblCohortStartDate.Visible = False
                trCohortStartDate.Visible = False
            End If


        End Sub

#End Region

#Region "DDL data binding"
        Private Sub PopulateTermDDL()
            '
            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New GrdPostingsFacade

            '' issue 14296: Instructor Supervisors and Education Directors(AcademicAdvisors)  
            ''need to be able to post and modify attendance. 
            '' To Find if the User is a Academic Advisors

            Dim facClsSectAtt As New ClsSectAttendanceFacade
            username = AdvantageSession.UserState.UserName
            instructorId = CType(ViewState("Instructor"), String)
            isAcademicAdvisor = facClsSectAtt.GetIsRoleAcademicAdvisorORInstructorSuperVisor(instructorId)
            'isAcademicAdvisor = facClsSectAtt.GetIsRoleAcademicAdvisor(instructorId)
            ViewState("isAcademicAdvisor") = isAcademicAdvisor
            campusId = Master.CurrentCampusId
            With ddlTerm
                .DataSource = facade.GetCurrentAndPastTermsForAnyUserbyRoles(instructorId, username, isAcademicAdvisor, campusId)
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With

        End Sub
        Private Sub PopulateShiftDDL()
            Dim dsShift As DataSet
            Dim facClsSectAtt As New ClsSectAttendanceFacade

            dsShift = facClsSectAtt.GetActiveShiftsByCampus(campusId)

            Dim drShift As DataRow = dsShift.Tables(0).NewRow
            drShift.Item("ShiftDescrip") = "Select"
            drShift.Item("ShiftID") = "00000000-0000-0000-0000-000000000000"
            dsShift.Tables(0).Rows.InsertAt(drShift, 0)

            With ddlShift
                .DataSource = dsShift
                .DataTextField = "ShiftDescrip"
                .DataValueField = "ShiftId"
                .DataBind()
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub PopulateStudentGroupsDDL()
            Dim DA As New StudentGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

            With ddStudentGroup
                .DataSource = DA.GetStudentGroupsByCampusId(campusId)
                .DataTextField = "Description"
                .DataValueField = "Id"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub
        Private Sub PopulateClsSectionDDL(Optional ByVal Shift As String = "00000000-0000-0000-0000-000000000000", Optional ByVal MinimumClassStartDate As Date = Nothing)
            ''
            ''   Get Degrees data to bind the CheckBoxList
            ''
            username = AdvantageSession.UserState.UserName
            instructorId = CType(ViewState("Instructor"), String)
            isAcademicAdvisor = CType(ViewState("isAcademicAdvisor"), Boolean)
            campusId = Master.CurrentCampusId
            Dim termId = CType(ViewState("Term"), String)
            Dim studentGroupId = CType(ViewState("StudentGroup"), String)

            If Shift = Nothing Then
                Shift = "00000000-0000-0000-0000-000000000000"
            Else
                Shift = ddlShift.SelectedValue.ToString
            End If


            Dim facade As New StdGrdBkFacade
            If (String.IsNullOrEmpty(termId) = True And String.IsNullOrEmpty(studentGroupId) = False) Then
                '   Get Degrees data to bind the CheckBoxList
                '
                Dim DA As New ClassSectionTermsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
                Dim classSectionResult As List(Of ClassSectionTerm) = DA.GetClsSectsbyUserandRoles(termId, studentGroupId, instructorId, campusId, Shift, rdpMinimumClassStartDate.DbSelectedDate, username, isAcademicAdvisor)
                Dim tempClassSectionResult As New List(Of ClassSectionTerm)
                tempClassSectionResult = classSectionResult.ToList()
                If (classSectionResult.Count > 0) Then
                    For Each row In classSectionResult
                        If (facade.IsCourseALab(row.RequirementId, row.InstructorGradeBookWeightId) = True) Then
                            tempClassSectionResult.Remove(row)
                        End If
                    Next
                    classSectionResult = tempClassSectionResult
                End If

                Dim selectValue = New ClassSectionTerm()
                selectValue.Id = ""
                selectValue.Description = "Select"

                classSectionResult.Insert(0, selectValue)

                ' Bind the Dataset to the CheckBoxList
                With ddlClsSection
                    .DataSource = classSectionResult
                    .DataTextField = "Description"
                    .DataValueField = "Id"
                    .DataBind()
                    .SelectedIndex = 0
                End With
            ElseIf (String.IsNullOrEmpty(termId) = False And String.IsNullOrEmpty(studentGroupId) = True) Then

                '' Bind the Dataset to the CheckBoxList
                With ddlClsSection
                    .DataSource = facade.GetClsSectionsbyUserandRoles(ViewState("Term"), instructorId, campusId, Shift, rdpMinimumClassStartDate.DbSelectedDate, username, isAcademicAdvisor)
                    .DataTextField = "CourseSectDescrip"
                    .DataValueField = "ClsSectionId"
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", ""))
                    .SelectedIndex = 0
                End With
            End If


        End Sub
        Private Sub PopulateGrdCriteriaDDL(ByVal ClassSect As String)
            Dim termId = CType(ViewState("Term"), String)
            Dim studentGroupId = CType(ViewState("StudentGroup"), String)
            Dim emptyGuid = Guid.Empty.ToString()
            If (termId <> emptyGuid And (studentGroupId = emptyGuid Or String.IsNullOrEmpty(studentGroupId))) Then
                '   Get Degrees data to bind the CheckBoxList
                Dim facade As New GrdPostingsFacade
                ' Bind the Dataset to the CheckBoxList
                isAcademicAdvisor = CType(ViewState("isAcademicAdvisor"), Boolean)

                With ddlGrdCriteria
                    .DataSource = facade.GetGrdCriteria(CType(ViewState("Class"), String), CType(ViewState("Instructor"), String), AdvantageSession.UserState.UserName, isAcademicAdvisor)
                    .DataTextField = "Descrip"
                    .DataValueField = "InstrGrdBkWgtDetailId"
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", ""))
                    .SelectedIndex = 0
                End With
            ElseIf ((termId = emptyGuid Or String.IsNullOrEmpty(termId)) And studentGroupId <> emptyGuid) Then

                ddlGrdCriteriaKey = "GrdComponentTypeId"
                Dim DA As New GradePostingDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
                Dim classId = CType(ViewState("Class"), String)
                Dim gradingCriteria = DA.GetGradingCriteria(classId, CType(ViewState("Instructor"), String), AdvantageSession.UserState.UserName, isAcademicAdvisor, False)

                Dim selectValue = New ClassSectionTerm()
                selectValue.Id = ""
                selectValue.Description = "Select"
                gradingCriteria.Insert(0, selectValue)

                With ddlGrdCriteria
                    .DataSource = gradingCriteria
                    .DataTextField = "Description"
                    .DataValueField = "Id"
                    .DataBind()
                    .SelectedIndex = 0
                End With

            End If



        End Sub
        Private Sub PopulateCourseLevelGrdCriteriaDDL(ByVal ClassSect As String)
            Dim termId = CType(ViewState("Term"), String)
            Dim studentGroupId = CType(ViewState("StudentGroup"), String)
            Dim emptyGuid = Guid.Empty.ToString()
            If (termId <> emptyGuid And (studentGroupId = emptyGuid Or String.IsNullOrEmpty(studentGroupId))) Then
                '
                '   Get Degrees data to bind the CheckBoxList
                '
                Dim facade As New GrdPostingsFacade
                Dim ds As DataSet

                ds = facade.GetCourseLevelGrdCriteria(ViewState("Class"))
                ' Bind the Dataset to the CheckBoxList
                With ddlGrdCriteria
                    .DataSource = ds
                    .DataTextField = "Descrip"
                    .DataValueField = "InstrGrdBkWgtDetailId"
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", ""))
                    .SelectedIndex = 0
                End With

                With ds.Tables(0)
                    .PrimaryKey = New DataColumn() { .Columns("InstrGrdBkWgtDetailId")}
                End With

                ViewState("CourseLvlGrdCriteria") = ds.Tables(0)

            ElseIf ((termId = emptyGuid Or String.IsNullOrEmpty(termId)) And studentGroupId <> emptyGuid) Then
                Dim DA As New GradePostingDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
                Dim classId = CType(ViewState("Class"), String)
                Dim gradingCriteria = DA.GetCourseLevelGrdCriteria(classId, False)

                Dim selectValue = New ClassSectionTerm()
                selectValue.Id = ""
                selectValue.Description = "Select"
                gradingCriteria.Insert(0, selectValue)

                With ddlGrdCriteria
                    .DataSource = gradingCriteria
                    .DataTextField = "Description"
                    .DataValueField = "Id"
                    .DataBind()
                    .SelectedIndex = 0
                End With

                'ViewState("CourseLvlGrdCriteria") = gradingCriteria
            End If

            If Not ddlGrdCriteria.SelectedItem Is Nothing Then
                ddlGrdCriteria.SelectedIndex = 0
            End If
            If Not ddlMaxNum.SelectedItem Is Nothing Then
                ddlMaxNum.SelectedIndex = 0
                lblMaxNum.Visible = False
                ddlMaxNum.Visible = False
            End If
        End Sub
        'Private Sub PopulateMaxNumberWeightingsDDL(ByVal ClassSect As String)
        '    '
        '    '   Get Degrees data to bind the CheckBoxList
        '    '
        '    Dim facade As New GrdPostingsFacade

        '    ' Bind the Dataset to the CheckBoxList
        '    With ddlGrdCriteria
        '        .DataSource = facade.GetGrdCriteria(ViewState("Class"), ViewState("Instructor"), AdvantageSession.UserState.UserName)
        '        .DataTextField = "Descrip"
        '        .DataValueField = "InstrGrdBkWgtDetailId"
        '        .DataBind()
        '        .Items.Insert(0, New ListItem("Select", ""))
        '        .SelectedIndex = 0
        '    End With
        'End Sub

        Private Sub PopulateCohortStartDateDDL()

            Dim facade As New GrdPostingsFacade
            username = AdvantageSession.UserState.UserName
            instructorId = ViewState("Instructor")
            isAcademicAdvisor = ViewState("isAcademicAdvisor")
            campusId = Master.CurrentCampusId

            With ddlcohortStDate
                .DataSource = facade.GetCohortStartDateforCurrentAndPastTermsForAnyUserbyRoles(instructorId, username, isAcademicAdvisor, campusId)
                .DataTextField = "CohortStartDate"
                .DataValueField = "CohortStartDate"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub
        ''Added by Saraswathi Lakshmanan to show the cohort Start Date on the LHS of the Screen to buuild the list based on cohortstdate and class sections 
        ''On Oct - 3-2008
        Private Sub PopulateClsSectionDDLbasedonCohortStDate(ByVal CohortStartDate As String)

            ''Added by Saraswathi Lakshmanan to fix Issue 14296
            username = AdvantageSession.UserState.UserName
            instructorId = ViewState("Instructor")
            isAcademicAdvisor = ViewState("isAcademicAdvisor")
            campusId = Master.CurrentCampusId

            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New StdGrdBkFacade

            ' Bind the Dataset to the CheckBoxList
            With ddlClsSection
                .DataSource = facade.GetClsSectionsbyUserandRolesandCohortStDt(ViewState("CohortStartDate"), instructorId, campusId, username, isAcademicAdvisor)
                .DataTextField = "CourseSectDescrip"
                .DataValueField = "ClsSectionId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub
        Private Sub DisplayErrorMessage(ByVal errorMessage As String)
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End Sub
#End Region

#Region "DDL SelectedIndexChanged"
        Private Sub ddlClsSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlClsSection.SelectedIndexChanged
            Dim facade As New StdGrdBkFacade
            ViewState("Class") = ddlClsSection.SelectedItem.Value.ToString()
            'Dim GrdBkLevel As String = SingletonAppSettings.AppSettings("GradeBookWeightingLevel")
            Dim grdBkLevel As String = facade.GetGradeBookWeightingLevel(CType(ViewState("Class"), String))
            ViewState("GrdBkLevel") = grdBkLevel


            'If (ViewState("Class") <> "") Then
            If grdBkLevel = "CourseLevel" Then
                ddlGrdCriteriaKey = "InstrGrdBkWgtDetailId"
                PopulateCourseLevelGrdCriteriaDDL(CType(ViewState("Class"), String)) 'Added by BN to take acct of Course Level Grade Bk Weights
            Else
                ddlGrdCriteriaKey = "InstrGrdBkWgtDetailId"
                PopulateGrdCriteriaDDL(CType(ViewState("Class"), String))
            End If
            'End If


        End Sub

        Private Sub StudentGroupDD_SelectionIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddStudentGroup.SelectedIndexChanged
            If Not ddlGrdCriteria.SelectedItem Is Nothing Then
                ddlGrdCriteria.SelectedIndex = 0
            End If
            If Not ddlMaxNum.SelectedItem Is Nothing Then
                ddlMaxNum.SelectedIndex = 0
                lblMaxNum.Visible = False
                ddlMaxNum.Visible = False
            End If
            ddlTerm.SelectedIndex = 0
            ToggleFilters()
            ViewState("StudentGroup") = ddStudentGroup.SelectedItem.Value.ToString()
            PopulateClsSectionDDL()
        End Sub

        Private Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTerm.SelectedIndexChanged
            ddlcohortStDate.SelectedIndex = 0

            If Not ddlGrdCriteria.SelectedItem Is Nothing Then
                ddlGrdCriteria.SelectedIndex = 0
            End If
            If Not ddlMaxNum.SelectedItem Is Nothing Then
                ddlMaxNum.SelectedIndex = 0
                lblMaxNum.Visible = False
                ddlMaxNum.Visible = False
            End If

            ddStudentGroup.SelectedIndex = 0
            ToggleFilters()
            ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
            'ViewState
            'If (ViewState("Term") <> "") Then
            PopulateClsSectionDDL()
            'End If

        End Sub
        Protected Sub ddlGrdCriteria_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGrdCriteria.SelectedIndexChanged
            'Dim facade As New StdGrdBkFacade
            Dim maxNumOfWeights As Integer = CType(MyAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings"), Integer)
            Dim gradeBkLevel As String = CType(ViewState("GrdBkLevel"), String)
            Dim minNumber As Integer
            Dim tbl As DataTable
            Dim row2 As DataRow
            Dim termId = CType(ViewState("Term"), String)
            Dim studentGroupId = CType(ViewState("StudentGroup"), String)
            Dim emptyGuid = Guid.Empty.ToString()
            Dim sGrdBkCriteria As String = ddlGrdCriteria.SelectedItem.Value.ToString

            If gradeBkLevel = "CourseLevel" Then

                If (termId <> emptyGuid And (studentGroupId = emptyGuid Or String.IsNullOrEmpty(studentGroupId))) Then
                    'Get the minimum number attached to the grade component type
                    tbl = CType(ViewState("CourseLvlGrdCriteria"), DataTable)
                    row2 = tbl.Rows.Find(sGrdBkCriteria)
                    If Not row2.IsNull("Number") Then
                        minNumber = CType(row2("Number"), Integer)
                    End If

                    ViewState("MaxNumOfWeights") = maxNumOfWeights

                    Dim x As Integer
                    'Clear the items in the Number dropdownlist..else it will append every time this ddl needs to be
                    'populated.
                    ddlMaxNum.Items.Clear()

                    'bind ddl 
                    'If there is a minimum number specified on this grade component type then use this
                    'as a counter to insert numbers into the drop down list else use the maximum number of weights
                    'specified in the web.config to insert items into this list.
                    If minNumber > 0 And gradeBkLevel = "CourseLevel" Then
                        ddlMaxNum.Visible = True
                        lblMaxNum.Visible = True
                        For x = 0 To minNumber - 1
                            ddlMaxNum.Items.Add(New ListItem(x + 1, x + 1))
                        Next
                        ddlMaxNum.Items.Insert(0, New ListItem("Select", 0))
                        ddlMaxNum.SelectedIndex = 0
                    ElseIf maxNumOfWeights > 0 And gradeBkLevel = "CourseLevel" Then
                        ddlMaxNum.Visible = True
                        lblMaxNum.Visible = True
                        For x = 0 To maxNumOfWeights - 1
                            ddlMaxNum.Items.Add(New ListItem(x + 1, x + 1))
                        Next
                        ddlMaxNum.Items.Insert(0, New ListItem("Select", 0))
                        ddlMaxNum.SelectedIndex = 0
                    End If
                End If
            End If



        End Sub

        Protected Sub ddlShift_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlShift.SelectedIndexChanged
            ddlcohortStDate.SelectedIndex = 0

            If Not ddlGrdCriteria.SelectedItem Is Nothing Then
                ddlGrdCriteria.SelectedIndex = 0
            End If
            If Not ddlMaxNum.SelectedItem Is Nothing Then
                ddlMaxNum.SelectedIndex = 0
                lblMaxNum.Visible = False
                ddlMaxNum.Visible = False
            End If

            ViewState("Shift") = ddlShift.SelectedItem.Value.ToString()
            If (ViewState("Shift") <> "") Then
                PopulateClsSectionDDL(ViewState("Shift"), ViewState("MinimumClassStartDate"))
            End If
        End Sub

        Protected Sub rdpMinimumClassStartDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpMinimumClassStartDate.SelectedDateChanged
            ddlcohortStDate.SelectedIndex = 0

            If Not ddlGrdCriteria.SelectedItem Is Nothing Then
                ddlGrdCriteria.SelectedIndex = 0
            End If
            If Not ddlMaxNum.SelectedItem Is Nothing Then
                ddlMaxNum.SelectedIndex = 0
                lblMaxNum.Visible = False
                ddlMaxNum.Visible = False
            End If

            ViewState("MinimumClassStartDate") = rdpMinimumClassStartDate.DbSelectedDate
            PopulateClsSectionDDL(ViewState("Shift"), ViewState("MinimumClassStartDate"))
        End Sub

        Protected Sub ddlcohortStDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlcohortStDate.SelectedIndexChanged
            ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
            ddlTerm.SelectedIndex = 0
            ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
            If (ViewState("CohortStartDate") <> "") Then
                PopulateClsSectionDDLbasedonCohortStDate(ViewState("CohortStartDate"))
            End If

        End Sub
#End Region

#Region "BUTTONS events"
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            Dim DA As New GradePostingDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            Dim studentGroupSelected = ddStudentGroup.SelectedIndex
            Dim termSelected = ddlTerm.SelectedIndex
            Dim itemToGradeTxt = ddlGrdCriteriaKey
            '   if GrdSystem is nothing do an insert. Else do an update
            'Dim iitems As DataGridItemCollection
            'Dim iitem As DataGridItem
            Dim i As Integer
            'Dim stuEnrollId As String
            'Dim iScore As String
            'Dim iIsCompGraded As Boolean
            'Dim sComments As String
            Dim dt1 As New DataTable
            Dim dt2 As New DataTable
            'Dim grdPostingObject As GrdPostingsInfo
            Dim count As Integer
            Dim facade As New GrdPostingsFacade
            'Dim errmsg As String
            'Dim result As String
            Try
                Dim result = New List(Of ClassSectionStudent)

                If ddlMaxNum.Visible = True Then
                    result = DA.GetClassSectionData(ViewState("Term"), ViewState("StudentGroup"), CType(ViewState("Class"), String), CType(ViewState("GrdCriteria"), String), itemToGradeTxt, CType(ddlMaxNum.SelectedValue, Integer))
                Else
                    result = DA.GetClassSectionDataInstructor(ViewState("Term"), ViewState("StudentGroup"), CType(ViewState("Class"), String), CType(ViewState("GrdCriteria"), String), itemToGradeTxt)
                End If

                If Not IsNothing(result) Then
                    count = result.Count
                End If

                ' Save User input data in one poco list
                'Get the actual list of scores show in the data-grid in page (possibility modified by the user...)
                Dim listOfCriteriasInScreen = New List(Of GrdRecordsPoco)
                Dim comp As DataGridItemCollection = dgdClsSectGrds.Items
                Dim classId As String = (ddlClsSection.SelectedItem.Value.ToString)

                For i = 0 To comp.Count - 1
                    Dim info As New GrdRecordsPoco()
                    Dim enrollment = DA.GetEnrollmentStartDate(New Guid(CType(comp(i).FindControl("txtStuEnrollId"), WebControls.TextBox).Text))
                    Dim studentName = New String(CType(comp(i).FindControl("Student"), WebControls.Label).Text)
                    Dim enrollStartDate As Date = Convert.ToDateTime(enrollment(0))
                    Dim sco = CType(comp(i).FindControl("txtScore"), WebControls.TextBox).Text.Trim
                    Dim noScore As Boolean = String.IsNullOrEmpty(sco)
                    Dim dateCompleted = CType(comp(i).FindControl("rdpDateCompleted"), RadDatePicker).SelectedDate
                    If Not noScore Then
                        If (String.IsNullOrEmpty(Convert.ToString(dateCompleted))) Then
                            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "Date completed is required.")
                            Exit Sub
                        ElseIf (dateCompleted > DateTime.Now.Date Or dateCompleted < enrollStartDate) Then
                            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "Date Completed is Invalid for " + studentName + " the date must me greater then enrollment Date : " + enrollStartDate + ", and not greater than today's date.")
                            Exit Sub
                        End If
                    End If
                    ' If you use the string.isnullOrEmpty in the If return 0 instead Nothing. I love VB :-(
                    If noScore Then
                        info.Score = Nothing
                        info.DateCompleted = Nothing
                    Else
                        If sco = "T" Or sco = "t" Then
                            sco = "-1.00"
                        End If
                        info.Score = CType(sco, Decimal)
                        info.DateCompleted = CType(dateCompleted, DateTime)
                    End If
                    If (info.Score > 100) Then
                        Throw New ArgumentOutOfRangeException(info.Score.ToString(), "Score cannot exceed 100")
                    End If

                    'info.ResNum = 0  'NOTE: ? Mining unknown always 0 in the table 
                    'info.GrdBkResultId = CType(comp(i).FindControl("hiddenGrdCritId"), HiddenField).Value


                    info.ClsSectionId = If(termSelected > 0 And studentGroupSelected = 0, classId, CType(comp(i).FindControl("HiddenClassSectionId"), WebControls.HiddenField).Value)
                    info.Comments = CType(comp(i).FindControl("txtComments"), WebControls.TextBox).Text
                    info.Descrip = ddlGrdCriteria.SelectedItem.Text

                    Dim InstrGrdBkWgtDetailId = If(termSelected > 0 And studentGroupSelected = 0, ddlGrdCriteria.SelectedItem.Value.ToString, CType(comp(i).FindControl("hiddenInstructorBookWeightDetailId"), WebControls.HiddenField).Value)

                    If ((InstrGrdBkWgtDetailId = "" Or InstrGrdBkWgtDetailId Is Nothing) And ddlGrdCriteria.SelectedItem.Value IsNot Nothing And ddlGrdCriteria.SelectedItem.Value <> "") Then
                        InstrGrdBkWgtDetailId = ddlGrdCriteria.SelectedItem.Value.ToString
                    End If

                    info.GrdBkResultId = CType(comp(i).FindControl("HiddenGradeBookId"), WebControls.HiddenField).Value

                    info.InstrGrdBkWgtDetailId = If(InstrGrdBkWgtDetailId = "", Nothing, InstrGrdBkWgtDetailId)
                    info.StuEnrollId = CType(comp(i).FindControl("txtStuEnrollId"), WebControls.TextBox).Text
                    info.UserName = AdvantageSession.UserState.UserName
                    If (IsNothing(info.Score) And String.IsNullOrWhiteSpace(info.Comments)) Then
                        info.IsIncomplete = True
                    Else
                        info.IsIncomplete = False
                    End If

                    'The result number is not used for some schools and so won't have a value.
                    'In that case we want to set it to db value.
                    Try
                        If (Not ddlMaxNum.Visible) Then
                            info.ResNum = (From grdBkRes In result Where grdBkRes.GradebookResultId = info.GrdBkResultId).First().ResNum
                            If (info.ResNum = 0) Then
                                info.ResNum = 1
                            End If
                        Else
                            info.ResNum = ddlMaxNum.SelectedValue
                        End If
                    Catch ex As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                        info.ResNum = 1
                    End Try


                    'info.IsIncomplete = Not (CType(CType(comp(i).FindControl("hdnIsCompGraded"), HiddenField).Value, Boolean))
                    listOfCriteriasInScreen.Add(info)

                    GlobalVariables.StudentEnrollmentsList.Add(New Guid(info.StuEnrollId))
                Next


                'Create the business object 
                'The visibility of the ddlMaxNum control tells us whether or not the class is course level or not.
                Dim business = New ArGradeStudentsByComponent(listOfCriteriasInScreen, classId)
                business.ProcessScoreInformation(ddlMaxNum.Visible)



                If GlobalVariables.StudentEnrollmentsList.Count > 0 Then
                    TitleIVSapUpdate(GlobalVariables.StudentEnrollmentsList)
                    'PostPaymentPeriodAttendanceAFA(GlobalVariables.StudentEnrollmentsList)
                End If


                CommonWebUtilities.DisplayInfoInMessageBox(Me, "Grades have successfully been posted")
                btnGetClsGrds_Click(sender, e)

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try

        End Sub

        Private Sub TitleIVSapUpdate(StudentEnrollmentsList)
            If (Not Session("AdvantageApiToken") Is Nothing) Then
                Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
                Dim TitleIVSAPRequest As New TitleIVSAPRequest(tokenResponse.ApiUrl, tokenResponse.Token)
                Dim pass As Boolean? = TitleIVSAPRequest.TitleIVSapCheck(StudentEnrollmentsList)
            End If
        End Sub

        Private Sub btnGetClsGrds_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetClsGrds.Click
            Dim facade As New GrdPostingsFacade
            Dim DA As New GradePostingDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

            ''To add the cohortStart date 
            ''If term or cohortStDate are not selected then, classSection cannot be fetched.

            If ddlTerm.SelectedItem.Text = "Select" And ddStudentGroup.SelectedItem.Text = "Select" And ddlcohortStDate.SelectedItem.Text = "Select" Then
                DisplayErrorMessage("Unable to find data. Please select an option.")
                ' DisplayRADAlert(CallbackType.Postback, "Error1", "Unable to find data. Please select an option.", 350, 100, "Term or Student Group NOT selected")
                dgdClsSectGrds.Visible = False
                pnlHeader.Visible = False
                Exit Sub
            ElseIf ddStudentGroup.SelectedItem.Text <> "Select" Then
                ViewState("StudentGroup") = ddStudentGroup.SelectedItem.Value.ToString
            ElseIf ddlTerm.SelectedItem.Text <> "Select" Then
                ViewState("Term") = ddlTerm.SelectedItem.Value.ToString
            ElseIf ddlcohortStDate.SelectedItem.Text <> "Select" Then
                ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
            End If

            If ddlClsSection.SelectedItem.Text = "Select" Then
                Dim classOrClassSection = ""
                If ddlTerm.SelectedItem.Text <> "Select" And ddStudentGroup.SelectedItem.Text = "Select" Then
                    classOrClassSection = "Class Section"
                ElseIf ddlTerm.SelectedItem.Text = "Select" And ddStudentGroup.SelectedItem.Text <> "Select" Then
                    classOrClassSection = "Class"
                End If
                DisplayErrorMessage("Unable to find data. Please select a Class Section")
                'DisplayRADAlert(CallbackType.Postback, "Error2", "Unable to find data. Please select a " + classOrClassSection, 500, 200, "Class Section Error")
                dgdClsSectGrds.Visible = False
                pnlHeader.Visible = False
                Exit Sub
            Else
                ViewState("ClsSection") = ddlClsSection.SelectedItem.Value.ToString
            End If

            If ddlGrdCriteria.SelectedItem.Text = "Select" Then
                DisplayErrorMessage("Unable to find data. Please select an Item To Grade")
                ' DisplayRADAlert(CallbackType.Postback, "Error3", "Unable to find data. Please select an Item To Grade", 350, 100, "Item to Grade Error")
                dgdClsSectGrds.Visible = False
                pnlHeader.Visible = False
                Exit Sub
            Else
                ViewState("GrdCriteria") = ddlGrdCriteria.SelectedItem.Value.ToString
            End If

            If ddlMaxNum.Visible = True Then
                If ddlMaxNum.SelectedItem.Text = "Select" Then
                    DisplayErrorMessage("Unable to find data. Please select a Number to grade")
                    ' DisplayRADAlert(CallbackType.Postback, "Error4", "Unable to find data. Please select a Number to grade", 350, 100, "Number to Grade Error")
                    dgdClsSectGrds.Visible = False
                    pnlHeader.Visible = False
                    Exit Sub
                End If
            End If
            Try
                Dim result = New List(Of ClassSectionStudent)
                Dim itemToGradeTxt = ddlGrdCriteriaKey
                'Populate Class Header
                PopulateClsHeader()
                If ddlMaxNum.Visible = True Then
                    result = DA.GetClassSectionData(ViewState("Term"), ViewState("StudentGroup"), CType(ViewState("Class"), String), CType(ViewState("GrdCriteria"), String), itemToGradeTxt, CType(ddlMaxNum.SelectedValue, Integer))
                Else
                    result = DA.GetClassSectionDataInstructor(ViewState("Term"), ViewState("StudentGroup"), CType(ViewState("Class"), String), CType(ViewState("GrdCriteria"), String), itemToGradeTxt)
                End If

                Dim bindingSource As New BindingSource
                bindingSource.DataSource = result
                With dgdClsSectGrds
                    .DataSource = result
                    .DataBind()
                    .Visible = True
                End With

                If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
                    btnSave.Enabled = True
                End If

                ToggleFilters(False)

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "btnGetClsGrds_Click" & ex.Message & " "
                Else
                    Session("Error") = "btnGetClsGrds_Click" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub
#End Region

#Region "GRID dgdClsSectGrds events"
        'Private Function PopulateGrdPostingObject(ByVal StuEnrollId As String, ByVal sScore As String, ByVal comments As String) As GrdPostingsInfo
        '    Dim GrdPostingObj As New GrdPostingsInfo
        '    Dim score As Double = 0

        '    GrdPostingObj.ClsSectId = (ddlClsSection.SelectedItem.Value.ToString)
        '    GrdPostingObj.StuEnrollId = StuEnrollId
        '    GrdPostingObj.GrdBkWgtDetailId = ddlGrdCriteria.SelectedItem.Value.ToString
        '    If ddlMaxNum.Visible = True Then
        '        GrdPostingObj.ResNum = ddlMaxNum.SelectedItem.Value
        '    End If
        '    Double.TryParse(sScore, score)
        '    GrdPostingObj.Score = score
        '    GrdPostingObj.Comments = comments
        '    GrdPostingObj.ModUser = txtModUser.Text
        '    GrdPostingObj.ModDate = Date.Parse(txtModDate.Text)
        '    Return GrdPostingObj
        'End Function

        Private Sub PopulateClsHeader()
            Dim intpos As Integer
            Dim facade As New GrdPostingsFacade
            Dim studentGroupSelected = ddStudentGroup.SelectedIndex
            Dim termSelected = ddlTerm.SelectedIndex
            pnlHeader.Visible = True
            If termSelected > 0 And studentGroupSelected = 0 Then
                lblInstrName.Text = facade.GetClsSectInstructor(CType(ViewState("ClsSection"), String))
                intpos = ddlClsSection.SelectedItem.Text.LastIndexOf("-", StringComparison.Ordinal)
                lblClsName.Text = ddlClsSection.SelectedItem.Text.Substring(0, intpos - 1)
                lblSecName.Text = ddlClsSection.SelectedItem.Text.Substring(intpos + 1)
                If ddlMaxNum.Visible = True Then
                    lblGrdCritName.Text = ddlGrdCriteria.SelectedItem.Text & " - " & ddlMaxNum.SelectedItem.Text
                Else
                    lblGrdCritName.Text = ddlGrdCriteria.SelectedItem.Text
                End If
                lblInstr.Visible = True
                lblInstrName.Visible = True
                lblSection.Visible = True
                lblSecName.Visible = True
            ElseIf studentGroupSelected > 0 And termSelected = 0 Then
                lblClsName.Text = ddlClsSection.SelectedItem.Text
                lblGrdCritName.Text = ddlGrdCriteria.SelectedItem.Text
                lblInstr.Visible = False
                lblInstrName.Visible = False
                lblSection.Visible = False
                lblSecName.Visible = False
            End If
        End Sub
        Public Function ConcatenateClassNameAndSectionNumber(ByVal ds As DataSet) As DataSet
            Dim tbl As DataTable
            Dim row As DataRow

            tbl = ds.Tables(0)
            With tbl
                .PrimaryKey = New DataColumn() { .Columns("a.ClsSectionId")}
            End With
            If ds.Tables(0).Rows.Count > 0 Then
                'Add a new column called CourseSectDescrip to the datatable 
                tbl.Columns.Add("CourseSectDescrip", GetType(String))
                For Each row In tbl.Rows
                    row("CourseSectDescrip") = row("Descrip") & " - " & row("ClsSection")
                Next
            End If
            Return ds
        End Function
        Protected Sub dgdClsSectGrds_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgdClsSectGrds.ItemDataBound
            'Dim linkbtn As LinkButton
            'Dim linkbtn2 As LinkButton
            'Dim ds As New DataSet

            Dim studentGroupSelected = ddStudentGroup.SelectedIndex
            Dim termSelected = ddlTerm.SelectedIndex

            Dim record = DirectCast(e.Item.DataItem, ClassSectionStudent)

            If record IsNot Nothing Then

                Dim studentEnrollmentIdLiteral = DirectCast(e.Item.FindControl("txtStuEnrollId"), WebControls.TextBox)
                Dim studentLiteral = DirectCast(e.Item.FindControl("Student"), WebControls.Label)
                Dim ssnLiteral = DirectCast(e.Item.FindControl("studentSSNBtn"), LinkButton)
                Dim studentNumberLiteral = DirectCast(e.Item.FindControl("studentNumberBtn"), LinkButton)
                Dim gradeLiteral = DirectCast(e.Item.FindControl("lblGrade"), WebControls.Label)
                Dim itemToGradeDescriptionLiteral = DirectCast(e.Item.FindControl("lblitemToGradeDescription"), WebControls.Label)
                Dim scoreLiteral = DirectCast(e.Item.FindControl("txtScore"), WebControls.TextBox)
                Dim commentLiteral = DirectCast(e.Item.FindControl("txtComments"), WebControls.TextBox)
                Dim isCompGradedLiteral = DirectCast(e.Item.FindControl("hdnIsCompGraded"), WebControls.HiddenField)
                Dim gradebookResultIdLiteral = DirectCast(e.Item.FindControl("HiddenGradeBookId"), WebControls.HiddenField)
                Dim gradingCriteriaIdLiteral = DirectCast(e.Item.FindControl("hiddenGrdCritId"), WebControls.HiddenField)
                Dim rdpDateCompletedLiteral = DirectCast(e.Item.FindControl("rdpDateCompleted"), RadDatePicker)
                Dim hiddenInstructorBookWeightDetailIdLiteral = DirectCast(e.Item.FindControl("hiddenInstructorBookWeightDetailId"), WebControls.HiddenField)
                Dim hiddenClassIdLiteral = DirectCast(e.Item.FindControl("HiddenClassSectionId"), WebControls.HiddenField)

                studentEnrollmentIdLiteral.Text = record.StudentEnrollmentId
                studentLiteral.Text = record.LastName + ", " + record.FirstName
                ssnLiteral.Text = record.Ssn
                studentNumberLiteral.Text = record.StudentNumber
                gradeLiteral.Text = record.Grade
                If Not record.Score = Nothing AndAlso record.Score.Length > 4 Then
                    scoreLiteral.Text = If(record.Score = "-1.0000", "T", record.Score.Trim().Substring(0, record.Score.Length - 2))
                Else
                    scoreLiteral.Text = record.Score
                End If
                commentLiteral.Text = record.Comments
                isCompGradedLiteral.Value = record.IsCompGraded.ToString()
                gradingCriteriaIdLiteral.Value = record.GradeSystemDetailId
                rdpDateCompletedLiteral.SelectedDate = If(record.DateCompleted.HasValue, record.DateCompleted, rdpPostdDateDefault.SelectedDate)
                hiddenInstructorBookWeightDetailIdLiteral.Value = record.InstructorGradeBookWeightDetailId
                hiddenClassIdLiteral.Value = record.ClassSectionId
                itemToGradeDescriptionLiteral.Text = record.ItemToGradeDescription
                gradebookResultIdLiteral.Value = record.GradebookResultId
            End If


            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem

                    If studentGroupSelected > 0 And termSelected = 0 Then
                        dgdClsSectGrds.Columns(3).Visible = True
                    Else
                        dgdClsSectGrds.Columns(3).Visible = False
                    End If
                    'linkbtn = CType(e.Item.FindControl("Linkbutton2"), LinkButton)


                    'linkbtn2 = CType(e.Item.FindControl("Linkbutton5"), LinkButton)
                    'StudentId = CType(iitem.FindControl("Linkbutton5"), LinkButton).Text

                    If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                        'GetInputMaskValue(linkbtn)
                        dgdClsSectGrds.Columns(2).Visible = False
                    ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier") = "StudentNumber" Then
                        dgdClsSectGrds.Columns(1).Visible = False
                    End If

                    'Select DDL if grade exists for student in "ClsSectStds" datatable.
            End Select
        End Sub
#End Region

        Private Sub ToggleFilters(ByVal Optional clearSelection = True)
            Dim studentGroupSelected = ddStudentGroup.SelectedIndex
            Dim termSelected = ddlTerm.SelectedIndex

            'student group selected
            If termSelected > 0 And studentGroupSelected = 0 Then
                trlblShift.Visible = True
                trlblShiftFilter.Visible = True
                rdpMinimumClassStart.Visible = True
                rdpMinimumClassStartFilter.Visible = True
                lblClsSection.Text = "Class"
            ElseIf studentGroupSelected > 0 And termSelected = 0 Then
                trlblShift.Visible = False
                trlblShiftFilter.Visible = False
                rdpMinimumClassStart.Visible = False
                rdpMinimumClassStartFilter.Visible = False
                lblClsSection.Text = "Class Section"
            End If

            If (clearSelection) Then
                If Not ddlGrdCriteria.SelectedItem Is Nothing Then
                    ddlGrdCriteria.SelectedIndex = 0
                End If
                If Not ddlClsSection.SelectedItem Is Nothing Then
                    ddlClsSection.SelectedIndex = 0
                    ViewState("Class") = ddlClsSection.SelectedItem.Value.ToString()
                End If

                If Not ddlTerm.SelectedItem Is Nothing Then
                    ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
                End If

                If Not ddStudentGroup.SelectedItem Is Nothing Then
                    ViewState("StudentGroup") = ddStudentGroup.SelectedItem.Value.ToString()
                End If
            End If

        End Sub

        Protected Sub rdpPostdDateDefault_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpPostdDateDefault.SelectedDateChanged
            If ddlTerm.SelectedItem.Text = "Select" And ddStudentGroup.SelectedItem.Text = "Select" And ddlcohortStDate.SelectedItem.Text = "Select" Then
                Exit Sub
            Else
                btnGetClsGrds_Click(sender, New System.EventArgs())
            End If
        End Sub

    End Class
End Namespace