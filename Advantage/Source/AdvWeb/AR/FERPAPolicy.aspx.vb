﻿Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class FERPAPolicy
    Inherits BasePage

    Protected WithEvents lbxAvailStuds As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbxSelectStuds As System.Web.UI.WebControls.ListBox
    Protected StudentId, campusid As String

    Private mruProvider As MRURoutines
    Protected state As AdvantageSessionState
    Protected LeadId As String
    Dim resourceId As Integer
    Protected boolSwitchCampus As Boolean = False
    Protected userId As String
    Private pObj As New UserPagePermissionInfo
    Protected ModuleId As String


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

#End Region
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub


    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try
            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim objCommon As New CommonUtilities

        'Get the StudentId from the state object associated with this page
        campusid = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim advantageUserState As User = AdvantageSession.UserState


        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusid)

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(625) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
        End With
        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        InitButtonsForLoad()
        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            BuildEntityDDL()
            BuildFERPACategories()
            'Display an error message if there are no enrollments

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""
        Else
            'InitButtonsForEdit()
        End If
    End Sub
    Private Sub BuildEntityDDL()
        With New StudentFERPA
            ddlEntity.DataTextField = "FERPAEntityDescrip"
            ddlEntity.DataValueField = "FERPAEntityID"
            ddlEntity.DataSource = .GetFERPAEntities("True", campusid)
            ddlEntity.DataBind()
            ddlEntity.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            ddlEntity.SelectedIndex = 0
        End With


    End Sub
    Private Sub BuildFERPACategories()
        Dim facade As New StudentFERPA
        With ChkFERPAPages
            .DataTextField = "FERPACategoryDescrip"
            .DataValueField = "FERPACategoryID"
            .DataSource = facade.GetFERPACategories("True", campusid)
            .DataBind()

        End With
    End Sub



    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim result As String
        'If ddlEntity.SelectedValue.ToString = "00000000-0000-0000-0000-000000000000" Then
        '    result = "Select a Entity Type"
        'Else
        With New StudentFERPA
            '   update bank Info 
            result = .UpdateFERPAPolicyInfo(BuildFERPAInfo(ddlEntity.SelectedValue.ToString))
        End With
        'End If



        '   bind datalist


        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        End If
    End Sub


    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'controlsToIgnore.Add(btnAdd)
        'controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(ddlEntity)

        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Protected Sub ddlEntity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEntity.SelectedIndexChanged
        BindFERPAPageList(ddlEntity.SelectedValue.ToString, StudentId)
    End Sub
    Private Function GetFERPACategories() As String()
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        '    Dim selectedDegrees() As String = selectedDegrees.CreateInstance(GetType(String), chkLeadGrpId.Items.Count)
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), ChkFERPAPages.Items.Count)
        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In ChkFERPAPages.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next


        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)
        If i = 0 Then Return Nothing
        Return selectedDegrees
        'update Selected Jobs
        'Dim LeadGrpFacade As New AdReqsFacade
        'If LeadGrpFacade.UpdateLeadGroupsAndStudentEnrollments(Guid.Empty.ToString, Session("UserName"), selectedDegrees, txtStuEnrollId.Text) < 0 Then
        '    DisplayErrorMessage("A related record exists,you can not perform this operation")
        '    Exit Function
        'End If
        'Return 0
    End Function
    Private Function BuildFERPAInfo(ByVal ferpaEntityID As String) As FERPAPolicyInfo



        '   instantiate class
        Dim ferpaInfo As New FERPAPolicyInfo

        With ferpaInfo
            '   get IsInDB
            .FERPAEntityId = ferpaEntityID
            .FERPACategoryId = GetFERPACategories()
            .StudentId = StudentId
            .ModUser = HttpContext.Current.Session("UserName").ToString
        End With

        '   return data
        Return ferpaInfo

    End Function
    Private Sub BindFERPAPageList(ByVal FERPAEntityID As String, ByVal StudentId As String)
        'Get Degrees data to bind the CheckBoxList
        Dim Facade As New StudentFERPA
        'Clear the current selection
        ChkFERPAPages.ClearSelection()
        Dim dt As DataTable = Facade.GetFERPAPolicy(FERPAEntityID, StudentId)



        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim row As DataRow
                row = dt.Rows(i)
                Dim item As ListItem
                For Each item In ChkFERPAPages.Items
                    If item.Value.ToString = CType(row("FERPACategoryId"), Guid).ToString Then
                        item.Selected = True
                        Exit For
                    End If
                Next
            Next
        End If

    End Sub
    Private Sub InitButtonsForLoad()
        'If pObj.HasFull Or pObj.HasAdd Then
        '    btnSave.Enabled = True
        '    'btnNew.Enabled = True
        'Else
        '    btnSave.Enabled = False
        '    'btnNew.Enabled = False
        'End If

        If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        'in popup windows new and delete buttons should be disabled
        If pObj.HasEdit Then
            'btnNew.Enabled = True
            'btnDelete.Enabled = True

            'Set the Delete Button so it prompts the user for confirmation when clicked
            'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            'btnNew.Enabled = False
            'btnDelete.Enabled = False
        End If

    End Sub
End Class
