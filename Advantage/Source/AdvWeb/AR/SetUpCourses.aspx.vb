

Imports System.Data
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade



Partial Class SetUpCourses
    Inherits System.Web.UI.Page
    Protected WithEvents Button9 As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Dim prgVerId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim facade As New ProgFacade
        prgVerId = Request.QueryString("PrgVerId")

        If Not Page.IsPostBack Then
            Dim campusId As String
            Dim userId As String
            Dim resourceId As Integer

            Dim advantageUserState As New BO.User()
            advantageUserState = AdvantageSession.UserState

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            If Request.QueryString("cmpid") <> "" Then
                ViewState("CampusId") = Request.QueryString("cmpid")
                campusId = ViewState("CampusId")
            End If
            If Request.QueryString("PrgVer") <> "" Then
                ViewState("ProgDesc") = Request.QueryString("PrgVer")
                txtProgDescrip.Text = ViewState("ProgDesc")
            End If

            campusId = AdvantageSession.UserState.CampusId.ToString
            userId = AdvantageSession.UserState.UserId.ToString

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
            InitButtonsForLoad()
            With dgdCourses
                .DataSource = facade.GetCoursesForProgVersion(prgVerId)
                .DataBind()
            End With
        End If
    End Sub

    
    Protected Sub dgdCourses_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdCourses.ItemDataBound
        'Dim iitems As DataGridItemCollection
        'Dim iitem As DataGridItem
        '        Dim i As Integer
        Dim ddl As DropDownList
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable
        ' Dim row As DataRow
        Dim count As Integer
        Dim termNo As String
        Dim lbl As Label

        
        count = CInt(ViewState("Count"))

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                count += 1
                ViewState("Count") = count.ToString
                ddl = CType(e.Item.FindControl("ddlTermNo"), DropDownList)
                termNo = CType(e.Item.FindControl("txtTermNo"), TextBox).Text
                BuildTermNos(ddl, termNo)
                lbl = CType(e.Item.FindControl("lblRowId"), Label)
                lbl.Text = count
        End Select
    End Sub
    Private Sub BuildTermNos(ByVal ddl As DropDownList, ByVal termNo As String)
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select", ""))
        ddl.Items.Add(New ListItem("1", "1"))
        ddl.Items.Add(New ListItem("2", "2"))
        ddl.Items.Add(New ListItem("3", "3"))
        ddl.Items.Add(New ListItem("4", "4"))
        ddl.Items.Add(New ListItem("5", "5"))
        ddl.Items.Add(New ListItem("6", "6"))
        ddl.Items.Add(New ListItem("7", "7"))
        ddl.Items.Add(New ListItem("8", "8"))
        ddl.Items.Add(New ListItem("9", "9"))
        ddl.Items.Add(New ListItem("10", "10"))
        ddl.Items.Add(New ListItem("11", "11"))
        ddl.Items.Add(New ListItem("12", "12"))
        ddl.Items.Add(New ListItem("13", "13"))
        ddl.Items.Add(New ListItem("14", "14"))
        ddl.Items.Add(New ListItem("15", "15"))
        ddl.Items.Add(New ListItem("16", "16"))
        ddl.Items.Add(New ListItem("17", "17"))
        ddl.Items.Add(New ListItem("18", "18"))
        ddl.Items.Add(New ListItem("19", "19"))
        ddl.Items.Add(New ListItem("20", "20"))
        ddl.SelectedValue = termNo

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim facade As New ProgFacade
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim reqid As String
        Dim termNo As Integer
        Dim i As Integer
        iitems = dgdCourses.Items
        Dim errStr As String = getErrorMessage(iitems)
        If (errStr <> "") Then
            DisplayErrorMessage("Please select the term for the following rows :" & vbCrLf & errStr)
        Else
            ViewState("Count") = 0
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                Try
                    reqid = CType(iitem.FindControl("txtReqId"), TextBox).Text
                    termNo = CInt(CType(iitem.FindControl("ddlTermNo"), DropDownList).SelectedItem.Value.ToString)
                    facade.UpdatePrgVersionTermNo(termNo, reqid, prgVerId)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Exit For
                    DisplayErrorMessage(ex.Message)
                End Try
            Next
            With dgdCourses
                .DataSource = facade.GetCoursesForProgVersion(prgVerId)
                .DataBind()
            End With

        End If


    End Sub

    Private Function getErrorMessage(ByVal iitems As DataGridItemCollection) As String
        Dim rtn As String = String.Empty
        Dim termNo As String
        Dim rowNo As String
        Dim i As Integer
        For i = 0 To iitems.Count - 1
            termNo = CType(iitems.Item(i).FindControl("ddlTermNo"), DropDownList).SelectedItem.Value.ToString
            rowNo = CType(iitems.Item(i).FindControl("lblRowId"), Label).Text
            If (termNo = "") Then
                rtn &= rowNo & vbCrLf
            End If
        Next
        Return rtn
    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then

            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If


    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
End Class
