Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections
'Imports Advantage.Business.Objects
Imports FAME.DataAccessLayer
Imports FAME.ExceptionLayer
Imports FAME.AdvantageV1.BusinessLayer.CampusGroup
Partial Class CampusGroups
    Inherits BasePage
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected ResourceId As String
    Protected moduleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim m_Context As HttpContext
        Dim SDFControls As New SDFComponent
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        moduleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        'Check if this page still exists in the menu while switching campus
      If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        'Put user code to initialize the page here
        Try
            If Not Page.IsPostBack Then

                Dim ds As DataSet
                Dim objCommon As New CommonUtilities
                Dim dt As New DataTable
                Dim objCampData As New CampusGroup

                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                'Generate a new guid to insert the record
                txtCampGrpId.Text = Guid.NewGuid.ToString()

                'Get Data to populate the Campuses List Box.
                ds = objCampData.GetCampuses()

                'Bind the Campuses List Box
                BindCampusListBox(ds)

                'Save the dataset to session
                SaveDataSet()

                'Bind the Campus Groups Datalist 
                BindDatalistCampGrps()

                txtModUser.Text = Session("UserName")

                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
            Else
                Dim objcommon As New CommonUtilities
                objcommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                'objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If

            'Check If any UDF exists for this resource
            Dim intSDFExists As Integer = SDFControls.GetSDFExists(ResourceId, moduleId)
            If intSDFExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If


            If Trim(txtCampGrpId.Text) <> "" Then
                SDFControls.GenerateControlsEdit(pnlSDF, ResourceId, txtCampGrpId.Text, moduleId)
            Else
                SDFControls.GenerateControlsNew(pnlSDF, ResourceId, moduleId)
            End If

            txtModUser.Text = Session("UserName")
            txtModDate.Text = Date.MinValue.ToString
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim i As Integer
        Dim db As New DataAccess
        Dim strGuid As String
        Dim ID As String
        Dim Description As String
        Dim Code As String
        Dim sb As New System.Text.StringBuilder
        Dim dt1, dt2 As DataTable
        Dim dr As DataRow
        Dim objCommon As New CommonUtilities
        Dim iCampusId As String
        Dim ds1 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim dv2 As New DataView
        Dim storedGuid As String
        Dim sStatusId As String
        Dim objCampData As New CampusGroup
        Dim result As String
        '        Dim result2 As String

        db.ConnectionString = "ConString"

        Code = txtCampGrpCode.Text
        Description = txtCampGrpDescrip.Text
        sStatusId = ddlStatusId.SelectedItem.Value.ToString

        Try
            ' CODE BELOW HANDLES A FRESH INSERT INTO THE TABLE(S)
            If ViewState("MODE") = "NEW" Then

                'Generate a new guid to insert the record
                strGuid = txtCampGrpId.Text

                'Insert CampusGroup into syCampGrps Table
                result = objCampData.InsertCampGroup(strGuid, Code, sStatusId, Description, Session("UserName"))

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    'Save the CampGrpId (GUID) for later use(during edit)
                    txtCampGrpId.Text = strGuid

                    For i = 0 To lbxSelectCamps.Items.Count - 1
                        'INSERT INTO syCmpGrpCmps using the campus id and the campgroup id(i.e pass in the GUID)

                        ID = lbxSelectCamps.Items(i).Value
                        objCampData.InsertCampusCampGrp(strGuid, ID, CType(Session("UserName"), String))

                    Next
                End If
                dt1 = Session("FldsSelectCamps")
                dt1.AcceptChanges()

                ds1 = objListGen.SummaryListGenerator(userId, campusId)
                dlstCampGrps.SelectedIndex = -1
                PopulateDataList(ds1)
                ds1.WriteXml(sw)
                ViewState("ds1") = sw.ToString()
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"

                ' CODE BELOW HANDLES AN UPDATE INTO THE TABLE(S)
            ElseIf ViewState("MODE") = "EDIT" Then

                'Retrieve the Guid to update particular record.
                storedGuid = txtCampGrpId.Text

                result = objCampData.UpdateCampGroup(Code, sStatusId, Description, storedGuid, Session("UserName"))
                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                End If
                'objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                ds1 = objListGen.SummaryListGenerator(userId, campusId)
                dlstCampGrps.SelectedIndex = -1
                PopulateDataList(ds1)
                ds1.WriteXml(sw)
                ViewState("ds1") = sw.ToString()

                'Run getchanges method on the dataset to see which rows have changed.
                'This section handles the changes to the Advantage Required fields
                dt1 = Session("FldsSelectCamps")
                dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted)
                If Not IsNothing(dt2) Then
                    For Each dr In dt2.Rows
                        If dr.RowState = DataRowState.Added Then
                            Try
                                iCampusId = dr("CampusId").ToString

                                objCampData.UpdateRowAdded(storedGuid, iCampusId, CType(Session("UserName"), String))
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException(ex.InnerException.Message)
                            End Try

                        ElseIf dr.RowState = DataRowState.Deleted Then
                            iCampusId = dr("CampusId", DataRowVersion.Original).ToString

                            objCampData.UpdateRowDeleted(storedGuid, iCampusId)
                            'Delete row from ResTblFlds table. It is important to remember that when a row
                            'is marked as deleted we cannot access the value from the current version of the
                            'row. We have to use the DataRowVersion enumeration to obtain the original value
                            'stored in the row.

                        End If
                    Next
                End If
                dt1.AcceptChanges()
                Session("FldsSelectCamps") = dt1
            End If

            'Clean Message for Save button
            ViewState("MessageOnSaveButton") = Nothing

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtCampGrpId.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtCampGrpId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstCampGrps, txtCampGrpId.Text, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstCampGrps, txtCampGrpId.Text)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub


    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If lbxAvailCamps.SelectedIndex >= 0 Then
            Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim objListGen As New DataListGenerator

            'Add selected item to the relevant assigned list. 

            lbxSelectCamps.Items.Add(New ListItem(lbxAvailCamps.SelectedItem.Text, lbxAvailCamps.SelectedItem.Value))
            dt = Session("FldsSelectCamps")
            dr = dt.NewRow()
            dr("CampusId") = lbxAvailCamps.SelectedItem.Value.ToString
            dt.Rows.Add(dr)
            Session("FldsSelectCamps") = dt
            'Remove the item from the lbxAvailCamps list
            SelIndex = lbxAvailCamps.SelectedIndex
            lbxAvailCamps.Items.RemoveAt(SelIndex)
            ds = objListGen.SummaryListGenerator(userId, campusId)
            dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
            PopulateDataList(ds)
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim SelIndex As Integer
        Dim iCampusId As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator

        If lbxSelectCamps.SelectedIndex >= 0 Then
            'Add selected item to the lbxAvailCamps list. In order to do this we
            'have to use the FldsSelectCamps to find out the 'FldId for the selected item.
            lbxAvailCamps.Items.Add(New ListItem(lbxSelectCamps.SelectedItem.Text, lbxSelectCamps.SelectedItem.Value))
            dt = Session("FldsSelectCamps")
            dr = dt.Rows.Find(lbxSelectCamps.SelectedItem.Value.ToString)
            iCampusId = dr("CampusId").ToString
            If dr.RowState <> DataRowState.Added Then
                'if the campusId is being referenced... prepare a message to be displayed when the user hits save button
                CheckIfCampusCanBeRemovedFromCampusGroup(iCampusId, lbxSelectCamps.SelectedItem.Text)
                'Mark the row as deleted
                dr.Delete()
            Else
                dt.Rows.Remove(dr)
            End If

            'Remove the item from the lbxSelectCamps list
            SelIndex = lbxSelectCamps.SelectedIndex
            lbxSelectCamps.Items.RemoveAt(SelIndex)
            Session("FldsSelectCamps") = dt

            ds = objListGen.SummaryListGenerator(userId, campusId)
            dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
            PopulateDataList(ds)

        End If
    End Sub

    Private Sub CheckIfCampusCanBeRemovedFromCampusGroup(ByVal campusId As String, ByVal campusName As String)
        'get all references for syCampuses table
        Dim tablesReferencingCampuses As String = (New DalExceptionsFacade).GetReferencedTablesByValue("syCampuses", campusId)

        If Not CanDeleteThisCampus(tablesReferencingCampuses) Then
            AddMessageToViewState(GetListOfTablesReferencingCampusesTable(tablesReferencingCampuses, campusName))
        End If
    End Sub

    Private Sub btnAddAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAll.Click
        Dim i As Integer
        '        Dim SelIndex As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator

        'Loop thru the available campus list and add all the entries to the selected campus list
        'And simultaneously remove all the campuses from avail camp list
        'The code in this sub should only execute if an item is selected
        'in the lbxAvailCamps listbox.

        For i = 0 To lbxAvailCamps.Items.Count - 1

            lbxSelectCamps.Items.Add(New ListItem(lbxAvailCamps.Items(i).Text, lbxAvailCamps.Items(i).Value))
            dt = Session("FldsSelectCamps")
            dr = dt.NewRow()
            dr("CampusId") = lbxAvailCamps.Items(i).Value
            dt.Rows.Add(dr)
            Session("FldsSelectCamps") = dt
        Next
        'Remove all the items from the lbxAvailCamps list
        lbxAvailCamps.Items.Clear()

        ds = objListGen.SummaryListGenerator(userId, campusId)
        dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
        PopulateDataList(ds)

    End Sub

    Private Sub btnRemoveAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAll.Click
        Dim i As Integer
        Dim iCampusId As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator

        For i = 0 To lbxSelectCamps.Items.Count - 1

            lbxAvailCamps.Items.Add(New ListItem(lbxSelectCamps.Items(i).Text, lbxSelectCamps.Items(i).Value))
            dt = Session("FldsSelectCamps")
            dr = dt.Rows.Find(lbxSelectCamps.Items(i).Value)
            iCampusId = dr("CampusId").ToString
            If dr.RowState <> DataRowState.Added Then
                'if the campusId is being referenced... prepare a message to be displayed when the user hits save button
                CheckIfCampusCanBeRemovedFromCampusGroup(iCampusId, lbxSelectCamps.Items(i).Text)

                'Mark the row as deleted
                dr.Delete()
            Else
                dt.Rows.Remove(dr)
            End If
        Next

        'Remove all the items from the lbxSelectCamps list
        lbxSelectCamps.Items.Clear()

        ds = objListGen.SummaryListGenerator(userId, campusId)
        dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
        PopulateDataList(ds)
    End Sub

    Private Sub AddMessageToViewState(ByVal message As String)
        If ViewState("MessageOnSaveButton") = Nothing Then
            ViewState("msg1") = message
            ViewState("MessageOnSaveButton") = "Removing" & message & " from the Campus Group might create data inconsistency."

        Else
            ViewState("msg1") = ViewState("msg1").ToString() & "," & message
            ViewState("MessageOnSaveButton") = "Removing" & ViewState("msg1").ToString() & " from the Campus Group might create data inconsistency."
        End If
    End Sub

    Private Sub CreateMessageToBeDisplayedOnSaveButton()
        If ViewState("MessageOnSaveButton") = Nothing Then
            'remove message from client onclick event of the save button
            CommonWebUtilities.RemoveClientMessageFromWebControl(btnsave, "onclick")
        Else
            'add message to client onclick event of the save button
            CommonWebUtilities.AddClientMessageToWebControl(btnsave, "onclick", ViewState("MessageOnSaveButton"))
        End If
    End Sub

    Private Function CanDeleteThisCampus(ByVal tablesReferencingCampuses As String) As Boolean

        'check if there are tables referencing syCampuses 
        Return Not AreThereRecordsReferencingCampusesTable(tablesReferencingCampuses)

    End Function

    Private Function AreThereRecordsReferencingCampusesTable(ByVal tablesReferencingCampuses As String) As Boolean
        Dim tables() As String = tablesReferencingCampuses.Split("/")
        For i As Integer = 0 To tables.Length - 2
            Dim table() As String = tables(i).Split(";")
            'table syCmpGrpCmps should be ignored
            If Not table(1) = "syCmpGrpCmps" Then
                If Integer.Parse(table(0)) > 0 Then Return True
            End If
        Next
        Return False
    End Function

    Private Function GetListOfTablesReferencingCampusesTable(ByVal tablesReferencingCampuses As String, ByVal campusName As String) As String
        Dim tables() As String = tablesReferencingCampuses.Split("/")
        Dim list As New StringBuilder()
        For i As Integer = 0 To tables.Length - 2
            Dim table() As String = tables(i).Split(";")
            'table syCmpGrpCmps should be ignored
            If Not (table(1) = "syCmpGrpCmps" Or table(1) = "syCampGrps") Then
                If Integer.Parse(table(0)) > 0 Then
                    AddTableToTheMessage(list, table(1), table(0), campusName)
                End If
            End If
        Next
        Return list.ToString()
    End Function

    Private Sub AddTableToTheMessage(ByVal list As StringBuilder, ByVal tableName As String, ByVal numberOfRecords As String, ByVal campusName As String)
        If list.Length = 0 Then
            list.Append(vbCrLf)
            list.Append(campusName)
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim dt As DataTable
        Dim ds1 As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds1")))

        Try
            'Clear the dataset to be synchronized with the database
            dt = Session("FldsSelectCamps")
            dt.Clear()
            Session("FldsSelectCamps") = dt

            ClearRHS()
            ds1.ReadXml(sr)
            PopulateDataList(ds1)
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"

            If ddlCampusId.SelectedItem.Value.ToString <> "" Or chkIsAllCampusGrp.Checked Then
                btnAdd.Enabled = False
                btnRemove.Enabled = False
                btnAddAll.Enabled = False
                btnRemoveAll.Enabled = False
                txtCampGrpCode.Enabled = False
                ddlStatusId.Enabled = False
                txtCampGrpDescrip.Enabled = False

            Else
                btnAdd.Enabled = True
                btnRemove.Enabled = True
                btnAddAll.Enabled = True
                btnRemoveAll.Enabled = True
                txtCampGrpCode.Enabled = True
                ddlStatusId.Enabled = True
                txtCampGrpDescrip.Enabled = True
            End If

            'Generate a new guid to insert the record
            txtCampGrpId.Text = Guid.NewGuid.ToString()

            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsNew(pnlSDF, ResourceId, moduleId)
            CommonWebUtilities.RestoreItemValues(dlstCampGrps, Guid.Empty.ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim sb As New System.Text.StringBuilder
        Dim objCommon As New CommonUtilities
        Dim db As New DataAccess
        '        Dim strGuid As String
        Dim dt As DataTable
        Dim objListGen As New DataListGenerator
        Dim ds1 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Dim sCampgrpId As String
        Dim objCampData As New CampusGroup
        Dim msg As String

        Try
            db.ConnectionString = "ConString"
            sCampgrpId = txtCampGrpId.Text

            'Delete from database and clear the screen as well. 
            'And set the mode to "NEW" since the record has been deleted from the db.
            msg = objCampData.DeleteCampGroup(sCampgrpId)
            If msg <> "" Then
                DisplayErrorMessage("The campus group you are trying to delete is Active/In Use and cannot be deleted")
                Exit Sub
            End If
            'Clear the dataset to be synchronized with the database
            dt = Session("FldsSelectCamps")
            dt.Clear()
            Session("FldsSelectCamps") = dt

            'Clear the Controls on the right hand side.
            ClearRHS()
            ds1 = objListGen.SummaryListGenerator(userId, campusId)
            dlstCampGrps.SelectedIndex = -1
            PopulateDataList(ds1)
            ds1.WriteXml(sw)
            ViewState("ds1") = sw.ToString()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            'Generate a new guid to insert the record
            txtCampGrpId.Text = Guid.NewGuid.ToString()

            If ddlCampusId.SelectedItem.Value.ToString <> "" Or chkIsAllCampusGrp.Checked Then
                btnAdd.Enabled = False
                btnRemove.Enabled = False
                btnAddAll.Enabled = False
                btnRemoveAll.Enabled = False
                txtCampGrpCode.Enabled = False
                ddlStatusId.Enabled = False
                txtCampGrpDescrip.Enabled = False
            Else
                btnAdd.Enabled = True
                btnRemove.Enabled = True
                btnAddAll.Enabled = True
                btnRemoveAll.Enabled = True
                txtCampGrpCode.Enabled = True
                ddlStatusId.Enabled = True
                txtCampGrpDescrip.Enabled = True
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim SDFControl As New SDFComponent
            SDFControl.DeleteSDFValue(txtCampGrpId.Text)
            SDFControl.GenerateControlsNew(pnlSDF, ResourceId, moduleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Header1.EnableHistoryButton(False)
            CommonWebUtilities.RestoreItemValues(dlstCampGrps, Guid.Empty.ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Dim i As Integer

        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next

            'ListBoxes need to be taken care of seperately
            For i = 0 To lbxSelectCamps.Items.Count - 1
                lbxAvailCamps.Items.Add(New ListItem(lbxSelectCamps.Items(i).Text, lbxSelectCamps.Items(i).Value))
            Next

            'Remove all the items from the lbxSelectCamps list
            lbxSelectCamps.Items.Clear()

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstCampGrps_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstCampGrps.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        Dim ds2 As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds1")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Dim db As New DataAccess
        '  Dim dr As OleDbDataReader
        Dim dt As New DataTable
        Dim tbl As DataTable ', tblN
        '  Dim dc As DataColumn
        Dim objCampData As New CampusGroup

        'Clean Message for Save button
        ViewState("MessageOnSaveButton") = Nothing

        Try
            strGUID = dlstCampGrps.DataKeys(e.Item.ItemIndex).ToString()

            Master.PageObjectId = e.CommandArgument
            Master.PageResourceId = ResourceId
            Master.setHiddenControlForAudit()

            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)

            ds = objCampData.GetCampusesOnItemCmd(strGUID)

            'Remember that for any given resource the TblFldsId must be unique.
            'This means that we can use the TblFldsId as the primary key
            'This section handles the populating of the lbxAssigned listbox
            tbl = ds.Tables("FldsSelectCamps")
            With ds.Tables("FldsSelectCamps")
                'Create primary key
                .PrimaryKey = New DataColumn() {tbl.Columns("CampusId")}
            End With

            'If the lbxAssigned control has entries clear it
            If lbxSelectCamps.Items.Count > 0 Then
                lbxSelectCamps.Items.Clear()
            End If

            'Bind only if the FldsAssignedR datatable is not empty
            If ds.Tables("FldsSelectCamps").Rows.Count > 0 Then
                With lbxSelectCamps
                    .DataSource = ds.Tables("FldsSelectCamps")
                    .DataTextField = "CampDescrip"
                    .DataValueField = "CampusId"
                    .DataBind()
                End With
            End If

            'Save the dataset to session
            Session("FldsSelectCamps") = ds.Tables("FldsSelectCamps")

            'This section deals with populating the lbxAvailable listbox
            tbl = ds.Tables("FldsAvailCamps")
            With ds.Tables("FldsAvailCamps")
                .PrimaryKey = New DataColumn() {tbl.Columns("CampusId")}
            End With

            'If the lbxAvailable control has items clear it
            If lbxAvailCamps.Items.Count > 0 Then
                lbxAvailCamps.Items.Clear()
            End If

            With lbxAvailCamps
                .DataSource = ds.Tables("FldsAvailCamps")
                .DataTextField = "CampDescrip"
                .DataValueField = "CampusId"
                .DataBind()
            End With

            'Save the dataset to session
            Session("FldsAvailCamps") = ds.Tables("FldsAvailCamps")

            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"

            selIndex = e.Item.ItemIndex
            'dlstCampGrps.SelectedIndex = selIndex

            'Save the selIndex to Viewstate, so it can be used by other subs.
            ViewState("SELINDEX") = CStr(selIndex)
            ds1.ReadXml(sr)

            PopulateDataList(ds1)

            If ddlCampusId.SelectedItem.Value.ToString <> "" Or chkIsAllCampusGrp.Checked Then
                btnAdd.Enabled = False
                btnRemove.Enabled = False
                btnAddAll.Enabled = False
                btnRemoveAll.Enabled = False
                'btndelete.Enabled = False
                txtCampGrpCode.Enabled = False
                ddlStatusId.Enabled = False
                txtCampGrpDescrip.Enabled = False
                'btnsave.Enabled = False
            Else
                btnAdd.Enabled = True
                btnRemove.Enabled = True
                btnAddAll.Enabled = True
                btnRemoveAll.Enabled = True
                'btndelete.Enabled = True
                txtCampGrpCode.Enabled = True
                ddlStatusId.Enabled = True
                txtCampGrpDescrip.Enabled = True
                'btnsave.Enabled = True
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
            'SDF Code Starts Here

            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsEdit(pnlSDF, ResourceId, txtCampGrpId.Text, moduleId)

            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            CommonWebUtilities.RestoreItemValues(dlstCampGrps, strGUID)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstCampGrps_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstCampGrps_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objListGen As New DataListGenerator
        Dim ds1 As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String

        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            ds1 = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds1)
            'Generate a new guid to insert the record
            txtCampGrpId.Text = Guid.NewGuid.ToString()

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/20/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radstatus.SelectedItem.Text.ToLower = "active") Or (radstatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radstatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "CampGrpDescrip", DataViewRowState.CurrentRows)

        With dlstCampGrps
            .DataSource = dv
            .DataBind()
        End With
        'dlstCampGrps.SelectedIndex = -1

    End Sub

    Private Sub BindCampusListBox(ByRef ds As DataSet)
        lbxAvailCamps.DataSource = ds
        lbxAvailCamps.DataTextField = "CampDescrip"
        lbxAvailCamps.DataValueField = "CampusId"
        lbxAvailCamps.DataBind()
    End Sub

    Private Sub BindDatalistCampGrps()
        Dim ds1 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        Dim sStatusId As String
        Dim objListGen As New DataListGenerator


        ds1 = objListGen.SummaryListGenerator(userId, campusId)

        'Generate the status id
        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
        sStatusId = row("StatusId").ToString()

        Dim dv As New DataView(ds1.Tables(0), "StatusId = '" & sStatusId & "'", "CampGrpDescrip", DataViewRowState.CurrentRows)

        With dlstCampGrps
            .DataSource = dv
            .DataBind()
        End With
        ds1.WriteXml(sw)
        ViewState("ds1") = sw.ToString()
    End Sub

    Private Sub SaveDataSet()
        Dim dt As New DataTable

        'Create a datable with a new column called CampusId, make this the primary key.
        Dim col2 As DataColumn = dt.Columns.Add("CampusId", GetType(String))

        'Make the CampusId column the primary key
        With dt
            .PrimaryKey = New DataColumn() {.Columns("CampusId")}
        End With

        'Save Dataset to Session
        Session("FldsSelectCamps") = dt
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtCampGrpId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Create message to be displayed on the onclick event of the save button
        CreateMessageToBeDisplayedOnSaveButton()

        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnAddAll)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(btnRemoveAll)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
End Class
