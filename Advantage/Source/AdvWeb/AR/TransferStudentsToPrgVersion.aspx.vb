﻿
Imports System.Data
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports System.Web
Imports System.Web.UI.WebControls
Imports Advantage.AFA.Integration
Imports Fame.Advantage.Common
Imports Fame.Advantage.DataAccess.LINQ
Imports Fame.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports Fame.Integration.Adv.Afa.Messages.WebApi.Entities
Imports Fame.Advantage.Api.Library.Models
Imports FAME.Advantage.Api.Library.Models.AFA.Enrollments
Imports Fame.AdvantageV1.BusinessFacade.AR

Partial Class AR_TransferStudentsToPrgVersion
    Inherits BasePage
    Protected campusId As String
    Private pObj As New UserPagePermissionInfo
    Private MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim advantageUserState As User = AdvantageSession.UserState
        'Dim ds As New DataSet
        'Dim objCommon As New CommonUtilities
        'Dim userId As String
        '        Dim m_Context As HttpContext
        Dim resourceId As Integer
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        'disable history button
        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        'userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not Page.IsPostBack Then
            '   build dropdownlists
            BuildDropDownLists()

            '   disable all buttons until From Program Version is selected
            InitButtonsForLoad()
        End If
    End Sub

    Private Sub BuildDropDownLists()
        BuildFromPrgVersionsDDL()
    End Sub

    Private Sub BuildFromPrgVersionsDDL()
        '   From Program Version DDL
        Dim facade As New StuEnrollFacade
        Dim ds As DataSet = facade.GetAllPrgVersions

        With ddlFromPrgVerId
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = ds.Tables(0)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

        Session("PrgVersions") = ds.Tables(0)
    End Sub

    Private Sub BuildToPrgVersionsDDL()
        Dim dtPrgVersions As DataTable
        dtPrgVersions = DirectCast(Session("PrgVersions"), DataTable)
        Dim progId As String = String.Empty
        Dim row() As DataRow = dtPrgVersions.Select("PrgVerId = '" & ddlFromPrgVerId.SelectedValue & "'")
        If row.GetLength(0) > 0 Then
            If Not row(0).IsNull("ProgId") Then
                progId = row(0)("ProgId").ToString
            End If
        End If

        Dim dv As DataView = New DataView(dtPrgVersions, "PrgVerId <> '" & ddlFromPrgVerId.SelectedValue & "' AND ProgId = '" & progId & "'", "", DataViewRowState.CurrentRows)

        '   To Program Version DDL
        With ddlToPrgVerId
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = dv
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False
        'btnNew.Enabled = True
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForWork()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub ddlFromPrgVerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFromPrgVerId.SelectedIndexChanged

        '   get all students enrolled in selected Program Version
        BindDataGrid()

        If ddlFromPrgVerId.SelectedValue <> Guid.Empty.ToString Then
            '   show Students grid
            dgrdStuEnrollments.Visible = True

            '   populate ddlToPrgVerId with all Program Versions but the one selected in ddlFromPrgVerId
            BuildToPrgVersionsDDL()
            ddlToPrgVerId.Enabled = True

            '   enable Save button
            InitButtonsForWork()

            'Else
            '    '   hide Students grid
            '    dgrdStuEnrollments.Visible = False

            '    '   disable To Program Version until user selects From Program Version
            '    ddlToPrgVerId.Enabled = False
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        If ddlToPrgVerId.SelectedValue = Guid.Empty.ToString Then
            DisplayErrorMessage("Please select a Program Version to transfer students to.")
            Exit Sub
        End If

        Dim toPrgVerId As String = ddlToPrgVerId.SelectedValue
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim isSelected As Boolean
        Dim arrStuEnrollId As New ArrayList
        Dim stuEnrollId As String
        Dim facade As New StuEnrollFacade

        '   Save the datagrid items in a collection.
        iitems = dgrdStuEnrollments.Items

        Try
            '   Loop thru the collection to retrieve the StuEnrollId.
            For i As Integer = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
                    '   set flag to true if option is selected
                    isSelected = True
                    stuEnrollId = CType(iitem.FindControl("lblStuEnrollId"), Label).Text
                    arrStuEnrollId.Add(stuEnrollId)
                End If
            Next

            If isSelected = False Then
                DisplayErrorMessage("Please select one or more students to transfer to " & ddlToPrgVerId.SelectedItem.Text)
                Exit Sub

            Else
                '   proceed to update
                Dim result As String = facade.UpdatePrgVersionForStuEnrollments(arrStuEnrollId, toPrgVerId, AdvantageSession.UserState.UserName)
                If result <> "" Then
                    DisplayErrorMessage(result)
                    Exit Sub
                Else

                    Dim token As TokenResponse

                    token = CType(Session("AdvantageApiToken"), TokenResponse)
                    If (token IsNot Nothing) Then
                        Dim enrollmentRequest As New Fame.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(token.ApiUrl, token.Token)

                        Dim enrollmentDetails = enrollmentRequest.GetAfaEnrollmentDetails(arrStuEnrollId.Cast(Of String).Select(Function(gStr) Guid.Parse(gStr)).ToList())

                        If (enrollmentDetails IsNot Nothing AndAlso enrollmentDetails.Any()) Then
                            'Sync enrollment with AFA
                            Dim enrollmentList As  New List(Of AfaEnrollment)
                            For Each enrollment As AfaEnrollmentDetails In enrollmentDetails
                                Dim schedHoursByWeek = (New AttendanceFacade).GetScheduledHoursByWeek(enrollment.StudentEnrollmentId.ToString())
                                Dim afaEnr = New AfaEnrollment With {.StudentEnrollmentId = enrollment.StudentEnrollmentId,
                                                                         .ProgramVersionId = enrollment.ProgramVersionId, .ProgramName = enrollment.ProgramVersionName,
                                                                         .StartDate = enrollment.StartDate, .ExpectedGradDate = enrollment.GradDate, .DroppedDate = enrollment.DroppedDate, .EnrollmentStatus = enrollment.EnrollmentStatus,
                                                                         .AdmissionsCriteria = enrollment.AdmissionsCriteria, .AFAStudentId = enrollment.AfaStudentId, .CmsId = enrollment.CMSId, .LastDateAttendance = enrollment.LastDateAttendance,
                                                                         .LeaveOfAbsenceDate = enrollment.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollment.ReturnFromLOADate, .StatusEffectiveDate = enrollment.StatusEffectiveDate,
                                                                         .SSN = enrollment.SSN, .CreatedBy = enrollment.ModUser, .CreatedDate = enrollment.ModDate, .UpdatedBy = enrollment.ModUser, .UpdatedDate = enrollment.ModDate, .AdvStudentNumber = enrollment.AdvStudentNumber, .HoursPerWeek = schedHoursByWeek, .InSchoolTransferHrs = enrollment.InSchoolTransferHrs, .PriorSchoolHrs = enrollment.PriorSchoolHrs}
                                enrollmentList.Add(afaEnr)
                            Next
                            SyncEnrollmentsWithAFA(enrollmentList)
                            
                        End If
                    End If
                    DisplayErrorMessage("Students have been successfully transferred.")
                End If

                '   get list of students after updating DB
                '   bind datagrid
                BindDataGrid()

            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            '   Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BindDataGrid()
        dgrdStuEnrollments.DataSource = (New StuEnrollFacade).GetStudentsInPrgVersion(ddlFromPrgVerId.SelectedValue, campusId)
        dgrdStuEnrollments.DataBind()
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        ddlFromPrgVerId.SelectedValue = Guid.Empty.ToString
        BuildToPrgVersionsDDL()
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
        If ddlFromPrgVerId.SelectedValue = Guid.Empty.ToString Then
            '   hide Students grid
            dgrdStuEnrollments.Visible = False

            '   disable To Program Version until user selects From Program Version
            ddlToPrgVerId.Enabled = False
        End If
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        'BIndToolTip()
    End Sub

    Private Sub SyncEnrollmentsWithAFA(stuEnrollIds As IEnumerable(Of String))
        Try
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            If (wapiSetting IsNot Nothing) Then
                Dim afaEnrollments = New LocationsHelper(connectionString, AdvantageSession.UserState.UserName).SyncEnrollmentsWithAFA(stuEnrollIds)
                If (afaEnrollments.Count > 0) Then
                    Dim enrReq = New Fame.AFA.Api.Library.AcademicRecords.EnrollmentRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    Dim result = enrReq.SendEnrollmentToAFA(afaEnrollments)
                End If
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub

    Private Sub SyncEnrollmentsWithAFA(enrollments As IEnumerable(Of AfaEnrollment))
        Try
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            If (wapiSetting IsNot Nothing) Then

                Dim afaEnrollments = New LocationsHelper(connectionString, AdvantageSession.UserState.UserName).SyncEnrollmentsWithAFA(enrollments)
                If (afaEnrollments.Count > 0) Then

                    Dim enrReq = New Fame.AFA.Api.Library.AcademicRecords.EnrollmentRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    Dim result = enrReq.SendEnrollmentToAFA(afaEnrollments)
                End If

            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub
End Class
