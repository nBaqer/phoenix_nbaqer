﻿Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
'Imports FAME.Advantage.DataDictionary
Imports FAME.Advantage.Common
Partial Class ClassSectionsWithPeriods_New
    Inherits BasePage
    Private validatetxt As Boolean = False
    Private pObj As New UserPagePermissionInfo
    Protected ModuleId As String
    Protected campusId As String
    Protected ResourceId As String
    Protected msg As String
    Protected MyAdvAppSettings As AdvAppSettings
    Dim ds As DataSet
    Public dtPeriods As DataTable
    Public dtAltPeriods As DataTable
    Public dtRooms As DataTable
    Public dtInstructionTypes As DataTable
    Public dsScheduleConflicts As New DataSet
    Public strClsSectionList As String = ""
    Public strCourseDescrip As String = ""
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'MyBase.Page_Load(sender, e)
        'Put user code to initialize the page here
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim objCommon As New CommonUtilities
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        'ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        '*** DD Add the ResourceID. Where should this be added?
        'HttpContext.Current.Items("ResourceId") = ResourceId
        'HttpContext.Current.Items("Language") = "En-US"


        'Header1.EnableHistoryButton(False)
        'ds = facade.InitialLoadPage(campusId)
        If Not IsPostBack Then

            HideShowTimeClockFeatures()

            'US3677
            btnCloneClass.Enabled = False


            'pObj = fac.GetUserResourcePermissions(userId, ResourceId, campusId)

            'populateTerms()
            'Dim objcommon As New CommonUtilities
            'Populate the Activities Datagrid with the person who is logged in Activities
            'Default the Selection Criteria for the activity records
            ViewState("SortFilter") = "TermDescrip, Descrip, ClsSection"
            ViewState("Term") = "00000000-0000-0000-0000-000000000000"
            ViewState("Course") = "00000000-0000-0000-0000-000000000000"
            ViewState("Instructor") = "00000000-0000-0000-0000-000000000000"
            ViewState("Shift") = "00000000-0000-0000-0000-000000000000"
            ViewState("Mode") = "NEW"
            txtClsSectionId.Text = System.Guid.NewGuid.ToString
            ViewState("ClsSectIdGuid") = ""
            ViewState("SelectedIndex") = "-1"
            ViewState("ItemSelected") = -1
            'Need to clear the following two viewstates for validation
            ViewState("ClsSectionId") = ""
            ViewState("CourseDescrip") = ""
            ViewState("ClassMeetingStartDate") = ""
            ViewState("ClassMeetingEndDate") = ""
            ViewState("RoomId") = ""
            ViewState("PeriodId") = ""

            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

            BuildDropDownLists()
            'get the Periods for the grid dropdown
            dtPeriods = (New ClassSectionPeriodsFacade).GetAllPeriods_SP(True, campusId)
            ViewState("dtPeriods") = dtPeriods
            'get the Rooms for the grid dropdown
            dtRooms = (New ClassSectionPeriodsFacade).GetAllRooms_SP(True, campusId)
            ViewState("dtRooms") = dtRooms

            Session("ClassMeetingStartDate_SavedValue") = ""
            Session("ClassMeetingEndDate_SavedValue") = ""
            Session("RoomId_SavedValue") = ""
            Session("Period_SavedValue") = ""

            ' 08/24/2011 Janet Robinson - Clock Hour Project 

            Dim dtDefaulInstType As DataTable
            dtDefaulInstType = ((New InstructionTypeFacade).GetDefaultInstructionTypeId).Tables(0)
            ViewState("DefaultInstTypeId") = dtDefaulInstType.Rows.Item(0).Item("InstructionTypeId")
            ViewState("DefaultInstTypeDescrip") = dtDefaulInstType.Rows.Item(0).Item("InstructionTypeDescrip")
            dtInstructionTypes = (New ClassSectionPeriodsFacade).GetInstructionTypes_SP(True, campusId)
            ViewState("dtInstructionTypes") = dtInstructionTypes
            Dim row As DataRow = dtInstructionTypes.NewRow()
            row("InstructionTypeDescrip") = ""
            row("InstructionTypeId") = "00000000-0000-0000-0000-000000000000"
            dtInstructionTypes.Rows.InsertAt(row, 0)
            dtInstructionTypes.AcceptChanges()

            If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower <> "yes" Then
                'hide arClsSectionTimeClockPolicy fields
                lblAllowEarlyIn.Visible = False
                chkAllowEarlyIn.Visible = False
                lblAllowLateOut.Visible = False
                chkAllowLateOut.Visible = False
                lblAllowExtraHours.Visible = False
                chkAllowExtraHours.Visible = False
                lblCheckTardyIn.Visible = False
                chkCheckTardyIn.Visible = False
                lblMaxInBeforeTardy.Visible = False
                radMaxInBeforeTardy.Visible = False
                lblAssignTardyInTime.Visible = False
                radAssignTardyInTime.Visible = False
            End If

            InitButtonsForLoad()
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            'ViewState("Mode") = "NEW"
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")

            dtPeriods = CType(ViewState("dtPeriods"), DataTable)
            Dim row As DataRow = dtPeriods.NewRow()
            row("PeriodId") = "00000000-0000-0000-0000-000000000000"
            row("PeriodDescrip") = ""
            dtPeriods.Rows.InsertAt(row, 0)
            dtPeriods.AcceptChanges()
            dtAltPeriods = dtPeriods.Copy()
            dtPeriods.Rows.RemoveAt(0)
            dtRooms = ViewState("dtRooms")
            dtRooms.AcceptChanges()

            ' 08/24/2011 Janet Robinson - Clock Hour Project 
            dtInstructionTypes = ViewState("dtInstructionTypes")
            dtInstructionTypes.AcceptChanges()
            '   InitButtonsForEdit()

        End If
        'Check If any UDF exists for this resource
        'Dim intSDFExists As Integer = SDFControls.GetSDFExists(ResourceId, ModuleId)
        'If intSDFExists >= 1 Then
        '    pnlUDFHeader.Visible = True
        'Else
        '    pnlUDFHeader.Visible = False
        'End If
        'If Trim(txtClsSectionId.Text) <> "" AndAlso ViewState("Mode") = "EDIT" Then
        '    SDFControls.GenerateControlsEdit(pnlSDF, ResourceId, txtClsSectionId.Text, ModuleId)
        'Else
        '    SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
        'End If
        If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString.ToLower = "regulartraditional" Then
            lblStudentStartDate.Visible = False
            txtStudentStartDate.Visible = False
            label3.Visible = True
            ddlLeadGrp.Visible = True
            'IMG23.Visible = False
        Else
            lblStudentStartDate.Visible = True
            txtStudentStartDate.Visible = True
            label3.Visible = True
            ddlLeadGrp.Visible = True
            'IMG23.Visible = True
        End If

        headerTitle.Text = Header.Title
    End Sub
    Private Sub btnBuildList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        Dim objCommon As New CommonUtilities
        dtlClsSectList.Visible = True
        ViewState("Term") = ddlTerm.SelectedItem.Value.ToString
        ViewState("Course") = ddlCourse.SelectedItem.Value.ToString
        ViewState("Instructor") = ddlInstructor.SelectedItem.Value.ToString
        ViewState("Shift") = ddlShiftId2.SelectedItem.Value.ToString
        'Empty and hide ConflictCourses listbox and label
        lbxConflictCourses.Items.Clear()
        lbxConflictCourses.Visible = False
        lblConflictCourses.Visible = False

        'ViewState("ItemSelected") = dtlClsSectList.SelectedIndex.ToString

        If Not radFilterStartDate.DbSelectedDate Is Nothing Then
            If radFilterEndDate.DbSelectedDate Is Nothing Then
                DisplayErrorMessage(" End Date is required to build list")
                Exit Sub
            End If
        End If

        If Not radFilterEndDate.DbSelectedDate Is Nothing Then
            If radFilterStartDate.DbSelectedDate Is Nothing Then
                DisplayErrorMessage(" Start Date is required to build list")
                Exit Sub
            End If
        End If

        PopulateDataList("Build")
        'ClearRHS()
        ' objCommon.SetBtnState(Form1, "NEW", pObj)
        ViewState("Mode") = "NEW"

        'Dim SDFControls As New SDFComponent
        'SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)

        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False

        'US3677: Enable the Copy Class Button 
        btnCloneClass.Enabled = False
    End Sub
    Private Sub ClearRHS()
        Dim ctl As Control
        Dim listItemColl As New ListItemCollection()
        Dim common As New CommonWebUtilities
        Dim ddl As DropDownList
        ' Dim iitem As ListItem
        Dim dt As New DataTable
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then

                    ddl = CType(ctl, DropDownList)
                    If ddl.Visible = True Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                        CommonWebUtilities.CleanAdvantageListItemCollection(ddl.Items)
                    End If

                End If
            Next
            ' I have to manually clear the label for the date completed field

            'Mannually empty conflict courses listbox and hide it.

            ddlTerm.SelectedIndex = 0
            ddlCourse.SelectedIndex = 0
            ddlTermId.SelectedIndex = 0
            ddlReqId.SelectedIndex = 0
            ddlInstructorId.SelectedIndex = 0
            ddlCampus.SelectedIndex = 0
            ddlGrdScaleId.SelectedIndex = 0
            txtStudentStartDate.Clear()
            txtStartDate.Clear()
            txtEndDate.Clear()
            txtModDate.Text = Date.Now.ToString
            'ViewState("dtClsmeetings") = dt

            'RadGrdClassMeetings.Rebind()
            lbxConflictCourses.Items.Clear()
            lblConflictCourses.Visible = False
            lbxConflictCourses.Visible = False
            txtClsSectionId.Text = System.Guid.NewGuid.ToString
            ViewState("ClsSectIdGuid") = txtClsSectionId.Text


            ' 09/12/2011 Janet Robinson - Clock Hours Project Added new fields
            chkAllowEarlyIn.Checked = False
            chkAllowExtraHours.Checked = False
            chkAllowLateOut.Checked = False
            chkCheckTardyIn.Checked = False
            radMaxInBeforeTardy.Clear()
            radAssignTardyInTime.Clear()

            'Troy: We need to rebuild the courses drop down list box just in case they had a class selected that was based on an inactive course.
            'In that case the drop down list box would have both active and inactive courses. However, when the new or delete button is clicked we only want it
            'to have active courses.
            populateCourses1(ddlReqId)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub PopulateDataList(ByVal CallingFunction As String, Optional ByVal GuidToFind As String = "", Optional ByVal ds As DataSet = Nothing)
        Dim dv As New DataView
        Dim dt2 As New DataTable


        If ds Is Nothing Then
            Dim facade As New ClassSectionPeriodsFacade
            Dim filterStartDate As Nullable(Of DateTime) = Nothing
            Dim filterEndDate As Nullable(Of DateTime) = Nothing

            If radFilterStartDate.SelectedDate IsNot Nothing Then filterStartDate = radFilterStartDate.SelectedDate Else filterStartDate = Nothing
            If radFilterEndDate.SelectedDate IsNot Nothing Then filterEndDate = radFilterEndDate.SelectedDate Else filterEndDate = Nothing

            dt2 = facade.PopulateClsSectDataList_SP(ViewState("Term"), ViewState("Course"), ViewState("Instructor"), ViewState("Shift"), campusId, filterStartDate, filterEndDate)

            With dv
                .Table = dt2
                .Sort = ViewState("SortFilter").ToString
                .RowStateFilter = DataViewRowState.CurrentRows
                .RowFilter = ""
            End With
        End If
        With dtlClsSectList
            .DataSource = dv
            .DataBind()
        End With
        'Cache.Insert("myClassSections", dv)
        With dtlClsSectList
            Select Case CallingFunction
                Case "Delete"
                    'dtlClsSectList.SelectedIndex = -1
                Case "New"
                    Dim myDRV As DataRowView
                    Dim i As Integer
                    Dim GuidInDV As String
                    For Each myDRV In dv
                        GuidInDV = myDRV(0).ToString
                        If GuidToFind.ToString = GuidInDV Then
                            Exit For
                        End If
                        i += 1
                    Next
                    'dtlClsSectList.SelectedIndex = i
                    If i < dt2.Rows.Count Then
                        dtlClsSectList.Items(i).Selected = True
                        ViewState("ItemSelected") = i
                    End If
                Case "Sort"
                    If CInt(ViewState("ItemSelected")) > -1 Then
                        Dim myDRV As DataRowView
                        Dim i As Integer
                        Dim GuidInDV As String
                        For Each myDRV In dv
                            GuidInDV = myDRV(0).ToString
                            If GuidToFind.ToString = GuidInDV Then
                                Exit For
                            End If
                            i += 1
                        Next
                        'dtlClsSectList.SelectedIndex = i
                        dtlClsSectList.Items(i).Selected = True
                        ViewState("ItemSelected") = i
                    Else
                        'dtlClsSectList.SelectedIndex = -1
                        ViewState("ItemSelected") = -1
                    End If
                Case "Edit"
                    Dim i As Integer = CInt(ViewState("ItemSelected"))
                    dtlClsSectList.Items(i).Selected = True
                Case "Item"
                    Dim i As Integer = CInt(ViewState("ItemSelected"))
                    dtlClsSectList.Items(i).Selected = True
                Case "Build"
                    'dtlClsSectList.SelectedIndex = -1
                Case "Add"
                    'dtlClsSectList.SelectedIndex = -1
                Case Else
                    'dtlClsSectList.SelectedIndex = -1
            End Select
            '.DataSource = dv
            '.DataBind()
        End With

    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim ClassSectionObject As ClassSectionInfo
        Dim ClsSectMtgArrList As ArrayList
        Dim GuidArrList As ArrayList
        Dim sw As New System.IO.StringWriter
        'Dim storedGuid As String
        Dim obj As New Object
        Dim ErrStr As String = ""
        'Dim str As String
        Dim errMessage As String
        Dim errDates As String
        Dim Facade As New ClassSectionPeriodsFacade
        Dim resInfo As ClsSectionResultInfo
        'Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim count As Integer = 0
        Dim errMaxCap As String = String.Empty

        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False

        If validatetxt = True Then
            Exit Sub
        End If
        Try
            'Validate that the class section start and end dates are within the term
            errDates = Facade.ValidateClassSectionDates(txtStartDate.SelectedDate, txtEndDate.SelectedDate, ddlTermId.SelectedItem.Value, ddlReqId.SelectedItem.Value)

            If errDates <> "" Then
                DisplayErrorMessage(errDates)
            Else
                If errMaxCap <> "" Then
                    DisplayErrorMessage(errMaxCap)
                    Exit Sub
                End If

                'Move the values in the controls over to the object
                ClassSectionObject = PopulateClassSectionObject()

                Dim arrClsSectList As New ArrayList
                Dim arrDeleteList As New ArrayList


                If ViewState("Mode") = "NEW" Then

                    'Add the Class Section Header
                    resInfo = Facade.AddClassSection(ClassSectionObject, Session("UserName"))
                    If resInfo.Exists = True Then
                        errMessage = "Section name already exists for the same course within this term." & vbCr
                        errMessage &= "Please select another section name."
                        DisplayErrorMessage(errMessage)
                        PopulateDataList("New", ClassSectionObject.ClsSectId.ToString)

                        'Set Mode to NEW since a record has NOT been inserted into the db.
                        'objCommon.SetBtnState(Form1, "NEW", pObj)
                        ViewState("Mode") = "NEW"
                        Exit Sub
                    End If

                    'If we get to this point it means that we have successfully added the class section header
                    'and we are now in edit mode. We do not have to wait for the outcome of adding the 
                    'class section meetings.
                    'objCommon.SetBtnState(Form1, "EDIT", pObj)
                    ViewState("Mode") = "EDIT"

                    hdnMaxStud.Value = txtMaxStud.Text

                    txtClsSectionId.Text = XmlConvert.ToString(ClassSectionObject.ClsSectId)

                    'Set ModDate for later use
                    txtModDate.Text = resInfo.ModDate.ToString

                    'Set OriginalTermId for later use
                    txtOriginalTermId.Text = ClassSectionObject.TermId.ToString

                    'Save overriden conflicts and alert user if an error occurs
                    msg = Facade.AddOverridenConflicts(txtClsSectionId.Text, arrClsSectList, arrDeleteList, Session("UserName"))
                    If msg <> "" Then
                        DisplayErrorMessage(msg)
                    End If

                    If ddlCampus.SelectedItem.Value <> "" Then
                        'Move the values in the controls over to the object
                        ClsSectMtgArrList = PopulateClsSectMeetingObject(txtClsSectionId.Text, ddlInstructorId.SelectedItem.Text)
                    Else
                        Exit Sub
                    End If
                    'Check conflicts b/w the different cls sect mtgs assgd to one clssection
                    'CheckClsSectMtgConflicts(ClsSectMtgArrList)

                    If msg = "" Then
                        'Add the Class Section Meeting Details
                        GuidArrList = Facade.AddClsSectMeeting(ClsSectMtgArrList, chkAllowConflicts.Checked, txtClsSectionId.Text, Session("UserName"))


                        PopulateDataList("New", ClassSectionObject.ClsSectId.ToString)
                        ViewState("Mode") = "EDIT"

                        Dim dtClsmeetings As DataTable = (New ClassSectionPeriodsFacade).GetClsSectMtgsWithPeriods_SP(New Guid(txtClsSectionId.Text))
                        ViewState("dtClsmeetings") = dtClsmeetings
                        RadGrdClassMeetings.DataSource = dtClsmeetings
                        RadGrdClassMeetings.DataBind()


                        If ErrStr <> "" Then
                            DisplayErrorMessage(ErrStr)
                        End If
                        btnCloneClass.Enabled = True
                        '*************************************************************************************
                    Else
                        DisplayErrorMessage(msg)
                    End If

                ElseIf ViewState("Mode") = "EDIT" Then
                    Dim resultsExist As Integer

                    resultsExist = 0

                    If (ViewState("OrgGrdScale") <> ddlGrdScaleId.SelectedValue.ToString) Then
                        'Exit Sub
                        If txtClsSectionId.Text <> "" Then
                            resultsExist = Facade.DoResultsExistFrClsSect(txtClsSectionId.Text)
                        End If
                    End If

                    If resultsExist > 0 Then
                        DisplayErrorMessage("Cannot change the Grade Scale since grades have been posted for this Class Section")
                    ElseIf Not boolMaxCntSatisfied() Then
                        DisplayErrorMessage("Cannot change the Max Student since already these number of students are already registered.")
                    Else

                        ClassSectionObject.ModDate = Date.Parse(txtModDate.Text)


                        ' 8/13/2013 DE9161 - Validate Before Update
                        ClsSectMtgArrList = PopulateClsSectMeetingObject(txtClsSectionId.Text, ddlInstructorId.SelectedItem.Text)

                        If msg <> "" Then
                            DisplayErrorMessage(msg)
                        Else
                            resInfo = Facade.UpdateClassSection(ClassSectionObject, Session("UserName"))
                            If resInfo.ErrorString <> "" Then
                                DisplayErrorMessage(resInfo.ErrorString)
                            Else
                                'Set ModDate for later use
                                txtModDate.Text = resInfo.ModDate.ToString

                                'Set OriginalTermId for later use
                                txtOriginalTermId.Text = ClassSectionObject.TermId.ToString

                                btnCloneClass.Enabled = True
                            End If
                        End If


                        'Save overriden conflicts and alert user if an error occurs
                        msg = Facade.AddOverridenConflicts(txtClsSectionId.Text, arrClsSectList, arrDeleteList, Session("UserName"))
                        If msg <> "" Then
                            DisplayErrorMessage(msg)
                        End If


                        ' 8/13/2013 DE9161 - Moved Up
                        'ClsSectMtgArrList = PopulateClsSectMeetingObject(txtClsSectionId.Text, ddlInstructorId.SelectedItem.Text)

                        'Check conflicts b/w the different cls sect mtgs assgd to one clssection
                        'CheckClsSectMtgConflicts(ClsSectMtgArrList)
                        If msg = "" Then
                            'Update the Class Section Meeting (If it doesnt exist..add it)
                            GuidArrList = Facade.UpdateClsSectMeeting_SP(ClsSectMtgArrList, chkAllowConflicts.Checked, txtClsSectionId.Text, Session("UserName"))
                            For Each obj In GuidArrList
                                'Loop thru the records in the arraylist, and bind each record to the 
                                'respective controls on the page
                                '   For count = 1 To ArrListcount
                                count = count + 1
                                If (obj.GetType Is GetType(ErrMsgInfo)) Then
                                    Dim errmsgobj As New ErrMsgInfo

                                    errmsgobj = DirectCast(obj, ErrMsgInfo)
                                    ErrStr &= errmsgobj.ErrMsg
                                Else


                                    Dim clssectmtgobj As New ClsSectMtgIdObjInfo
                                    clssectmtgobj = DirectCast(obj, ClsSectMtgIdObjInfo)

                                    If clssectmtgobj.ErrorString = "" Then

                                    Else
                                        DisplayErrorMessage(clssectmtgobj.ErrorString)

                                    End If

                                End If

                            Next
                            PopulateDataList("Edit")
                            txtClsSectionId.Text = XmlConvert.ToString(ClassSectionObject.ClsSectId)

                            Dim dtClsmeetings As DataTable = (New ClassSectionPeriodsFacade).GetClsSectMtgsWithPeriods_SP(New Guid(txtClsSectionId.Text))
                            ViewState("dtClsmeetings") = dtClsmeetings
                            RadGrdClassMeetings.DataSource = dtClsmeetings
                            RadGrdClassMeetings.DataBind()

                            'Session("StoredDs") = ds

                            If ErrStr <> "" Then
                                DisplayErrorMessage(ErrStr)
                            End If
                            '*****************************************************************************************
                        Else
                            DisplayErrorMessage(msg)
                        End If

                    End If
                End If

            End If


        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        InitButtonsForEdit()
    End Sub
    Private Function boolMaxCntSatisfied() As Boolean
        Dim studentCnt As Integer
        Dim Facade As New ClassSectionFacade
        If CInt(txtMaxStud.Text) < CInt(hdnMaxStud.Value) Then
            studentCnt = Facade.StudentsRegisteredCount(txtClsSectionId.Text)
            If CInt(txtMaxStud.Text) < studentCnt Then
                'DisplayErrorMessage("Cannot change the Max Student since already these number os students are registered.")
                txtMaxStud.Text = hdnMaxStud.Value
                Return False
            End If
        End If
        Return True
    End Function
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim dt As New DataTable
        'Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False

        'US3677
        btnCloneClass.Enabled = False

        Try
            InitButtonsForLoad()
            ClearRHS()

            'Create a new class section id
            txtClsSectionId.Text = Guid.NewGuid.ToString

            'Need to clear the following two viewstates for validation
            ViewState("ClsSectionId") = ""
            ViewState("CourseDescrip") = ""
            ViewState("ClassMeetingStartDate") = ""
            ViewState("ClassMeetingEndDate") = ""
            ViewState("RoomId") = ""
            ViewState("PeriodId") = ""

            ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False

            'ds = Session("StoredDs")
            ' ds.ReadXml(sr)
            'PopulateDataList("New")
            'objcommon.SetBtnState(Form1, "NEW", pObj)
            'set the state to new. 
            ViewState("Mode") = "NEW"

            'Empty and hide ConflictCourses listbox and label
            lbxConflictCourses.Items.Clear()
            lbxConflictCourses.Visible = False
            lblConflictCourses.Visible = False
            'RadGrdClassMeetings.DataSource = GetTable()
            ViewState("dtClsmeetings") = GetTable.Clone()
            RadGrdClassMeetings.DataSource = CType(ViewState("dtClsmeetings"), DataTable)
            RadGrdClassMeetings.DataBind()
            RadGrdClassMeetings.MasterTableView.IsItemInserted = False
            RadGrdClassMeetings.Rebind()
            dtlClsSectList.DataSource = dt
            dtlClsSectList.DataBind()
            ViewState("dtClsmeetings") = Nothing
            'Set the fetched from DB checkbox to false.
            'chkfetchedFromDB.Checked = False
            'chkfetchedFromDB1.Checked = False
            'chkfetchedFromDB2.Checked = False
            'chkfetchedFromDB3.Checked = False
            'chkfetchedFromDB4.Checked = False
            'chkfetchedFromDB5.Checked = False
            'chkfetchedFromDB6.Checked = False
            'chkfetchedFromDB7.Checked = False
            'chkfetchedFromDB8.Checked = False
            'chkfetchedFromDB9.Checked = False
            'chkfetchedFromDB10.Checked = False

            'Dim SDFControls As New SDFComponent
            'SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BtnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim Info As New ClassSectionPeriodsFacade
        Dim msg As String
        Dim dt As New DataTable
        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False

        'US3677
        btnCloneClass.Enabled = False

        Try
            'Delete entries from the arOverridenConflicts table
            Info.DeleteOverridenConflicts(txtClsSectionId.Text)

            Info.DeleteClsSectMeeting(XmlConvert.ToGuid(txtClsSectionId.Text))
            msg = Info.DeleteClassSection(XmlConvert.ToGuid(txtClsSectionId.Text), Date.Parse(txtModDate.Text), txtOriginalTermId.Text)
            If msg <> "" Then
                DisplayErrorMessage(msg)
                Exit Sub
            End If
            ViewState("Mode") = "NEW"
            ClearRHS()
            RadGrdClassMeetings.DataSource = GetTable()
            RadGrdClassMeetings.DataBind()
            RadGrdClassMeetings.MasterTableView.IsItemInserted = False
            RadGrdClassMeetings.Rebind()
            ViewState("dtClsmeetings") = Nothing
            PopulateDataList("Delete")
            'Session("StoredDs") = ds
            'objCommon.SetBtnState(Form1, "NEW", pObj)


            'Set the fetched from DB checkbox to false.
            'chkfetchedFromDB.Checked = False
            'chkfetchedFromDB1.Checked = False
            'chkfetchedFromDB2.Checked = False
            'chkfetchedFromDB3.Checked = False
            'chkfetchedFromDB4.Checked = False
            'chkfetchedFromDB5.Checked = False
            'chkfetchedFromDB6.Checked = False
            'chkfetchedFromDB7.Checked = False
            'chkfetchedFromDB8.Checked = False
            'chkfetchedFromDB9.Checked = False
            'chkfetchedFromDB10.Checked = False
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim SDFControl As New SDFComponent
            'SDFControl.DeleteSDFValue(txtClsSectionId.Text)
            'SDFControl.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Need to clear the following two viewstates for validation
            ViewState("ClsSectionId") = ""
            ViewState("CourseDescrip") = ""
            ViewState("ClassMeetingStartDate") = ""
            ViewState("ClassMeetingEndDate") = ""
            ViewState("RoomId") = ""
            ViewState("PeriodId") = ""
            ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        'Blank out the currently selected item since we just deleted it
        ViewState("ItemSelected") = ""
        InitButtonsForLoad()

    End Sub
    Private Function PopulateClassSectionObject() As ClassSectionInfo
        Dim ClassSectionObject As New ClassSectionInfo
        If ViewState("Mode") <> "NEW" Then
            ClassSectionObject.ClsSectId = XmlConvert.ToGuid(txtClsSectionId.Text)
        Else
            ClassSectionObject.ClsSectId = New Guid(txtClsSectionId.Text.ToString)  'System.Guid.NewGuid
        End If
        Dim a As String = ClassSectionObject.ClsSectId.ToString

        If ddlTermId.SelectedItem.Value <> "00000000-0000-0000-0000-000000000000" And ddlTermId.SelectedItem.Value <> "" Then
            ClassSectionObject.TermId = XmlConvert.ToGuid(ddlTermId.SelectedItem.Value)
        End If
        If ddlReqId.SelectedItem.Value <> "00000000-0000-0000-0000-000000000000" And ddlReqId.SelectedItem.Value <> "" Then
            ClassSectionObject.CourseId = XmlConvert.ToGuid(ddlReqId.SelectedItem.Value)
        End If
        If ddlInstructorId.SelectedItem.Value <> "00000000-0000-0000-0000-000000000000" And ddlInstructorId.SelectedItem.Value <> "" Then
            ClassSectionObject.InstructorId2 = (ddlInstructorId.SelectedItem.Value.ToString)
        End If
        If ddlShiftId.SelectedItem.Value <> "00000000-0000-0000-0000-000000000000" And ddlShiftId2.SelectedItem.Value <> "" Then
            ClassSectionObject.ShiftId2 = (ddlShiftId.SelectedItem.Value.ToString)
        End If
        If ddlCampus.SelectedItem.Value <> "00000000-0000-0000-0000-000000000000" And ddlCampus.SelectedItem.Value <> "" Then
            ClassSectionObject.CampusId = XmlConvert.ToGuid(ddlCampus.SelectedItem.Value)
        End If
        If ddlGrdScaleId.SelectedItem.Value <> "00000000-0000-0000-0000-000000000000" And ddlGrdScaleId.SelectedItem.Value <> "" Then
            ClassSectionObject.GrdScaleId = XmlConvert.ToGuid(ddlGrdScaleId.SelectedItem.Value)
        End If
        If txtClsSection.Text <> "" Then
            ClassSectionObject.Section = txtClsSection.Text
        End If
        If (txtStartDate.SelectedDate) IsNot Nothing Then
            ClassSectionObject.StartDate = CDate(txtStartDate.SelectedDate)
        End If
        If (txtEndDate.SelectedDate) IsNot Nothing Then
            ClassSectionObject.EndDate = CDate(txtEndDate.SelectedDate)
        End If
        ''Added by Saraswathi lakshmanan to set a minvalue when the textbox is empty.
        ''And finally save it as null.
        '19746: QA: Issue with the student group and scheduling on run schedules page. 
        ''Modified on Sept 17 2010
        'If (txtStudentStartDate.Text) <> "" Then
        If (txtStudentStartDate.SelectedDate) IsNot Nothing Then
            'ClassSectionObject.StudentStartDate = CDate(txtStudentStartDate.Text)
            ClassSectionObject.StudentStartDate = CDate(txtStudentStartDate.SelectedDate)
        Else
            ClassSectionObject.StudentStartDate = Date.MinValue
        End If

        If (txtMaxStud.Text) <> "" Then
            ClassSectionObject.MaxStud = (txtMaxStud.Text)
        End If

        'get ModUser
        ClassSectionObject.ModUser = txtModUser.Text

        If chkfetchedFromDB.Checked = True Then
            ClassSectionObject.fetchedFromDB = True
        Else
            ClassSectionObject.fetchedFromDB = False
        End If

        ClassSectionObject.OriginalTermId = txtOriginalTermId.Text
        If ddlLeadGrp.SelectedItem.Value <> "00000000-0000-0000-0000-000000000000" And ddlLeadGrp.SelectedItem.Value <> "" Then
            ClassSectionObject.LeadGrpId = XmlConvert.ToGuid(ddlLeadGrp.SelectedItem.Value)
        End If
        If ViewState("Mode") = "EDIT" Then
            If ddlInstructorId.SelectedValue <> hdnInstructor.Text And hdnGrdWeights.Text <> "00000000-0000-0000-0000-000000000000" Then
                hdnInstructor.Text = ddlInstructorId.SelectedValue
                hdnGrdWeights.Text = "00000000-0000-0000-0000-000000000000"
                lblErrMsg.Text = ""
            Else
                ClassSectionObject.InstrGrdBkWgtId = XmlConvert.ToGuid(hdnGrdWeights.Text)
            End If
        End If

        ' 09/08/2011 JRobinson Clock Hour Project - Add new fields for arClsSectionTimeClockPolicy table
        If chkAllowEarlyIn.Checked = True Then
            ClassSectionObject.AllowEarlyIn = True
        Else
            ClassSectionObject.AllowEarlyIn = False
        End If
        If chkAllowLateOut.Checked = True Then
            ClassSectionObject.AllowLateOut = True
        Else
            ClassSectionObject.AllowLateOut = False
        End If
        If chkAllowExtraHours.Checked = True Then
            ClassSectionObject.AllowExtraHours = True
        Else
            ClassSectionObject.AllowExtraHours = False
        End If
        If chkCheckTardyIn.Checked = True Then
            ClassSectionObject.CheckTardyIn = True
        Else
            ClassSectionObject.CheckTardyIn = False
        End If
        If Not (radMaxInBeforeTardy.SelectedDate) Is Nothing Then
            ClassSectionObject.MaxInBeforeTardy = CDate(radMaxInBeforeTardy.SelectedDate)
        Else
            ClassSectionObject.MaxInBeforeTardy = Date.MinValue
        End If
        If Not (radAssignTardyInTime.SelectedDate) Is Nothing Then
            ClassSectionObject.AssignTardyInTime = CDate(radAssignTardyInTime.SelectedDate)
        Else
            ClassSectionObject.AssignTardyInTime = Date.MinValue
        End If


        'get ModDate
        'ClassSectionObject.ModDate = Date.Parse(txtModDate.Text)
        Return ClassSectionObject
    End Function

    Private Function PopulateClsSectMeetingObject(ByVal ClsSectId As String, ByVal Instructor As String) As ArrayList
        Dim ClsSectArrayList As New ArrayList
        Dim dt As DataTable = CType(ViewState("dtClsmeetings"), DataTable)
        Dim facade As New ClassSectionPeriodsFacade
        Dim count As Integer = 0
        If Not dt Is Nothing Then

            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Select("", "CmdType desc")

                    If Not (dr("AltPeriodId") Is System.DBNull.Value) Then

                        If dt.Select("CmdType<>'3' and " & "PeriodId='" & dr("PeriodId").ToString & "' and Altperiodid= '" & dr("Altperiodid").ToString & "' and RoomId='" & dr("RoomId").ToString & "' and StartDate='" & dr("StartDate") & "' and enddate='" & dr("EndDate") & "'").Length > 1 Then
                            'DisplayErrorMessage("There are duplicate recs")
                            msg = "Please delete the duplicate class section meetings."
                            Exit For
                        End If
                    Else
                        If dt.Select("CmdType<>'3' and " & "PeriodId='" & dr("PeriodId").ToString & "' and RoomId='" & dr("RoomId").ToString & "' and StartDate='" & dr("StartDate") & "' and enddate='" & dr("EndDate") & "'").Length > 1 Then
                            'DisplayErrorMessage("There are duplicate recs")
                            msg = "Please delete the duplicate class section meetings."
                            Exit For
                        End If
                    End If

                    count = count + 1
                    If facade.GetMaxCapOnRoom(dr("RoomId").ToString) < txtMaxStud.Text Then
                        txtMaxStud.Text = hdnMaxStud.Value
                        msg = "Max Capacity for room in row " & count & " is less than max number of students allowed for this class section"
                        Exit For
                    End If
                    If dr("CmdType") > "0" Then
                        Dim ClsSectMtgObject As New FAME.AdvantageV1.Common.ClsSectMeetingInfo
                        ClsSectMtgObject.ClsSectId = XmlConvert.ToGuid(txtClsSectionId.Text)
                        ClsSectMtgObject.PeriodId = dr("PeriodId")
                        If Not (dr("AltPeriodId") Is System.DBNull.Value) Then ClsSectMtgObject.AltPeriodId = dr("AltPeriodId")

                        ClsSectMtgObject.Room = dr("RoomId")
                        'If CDate(dr("StartDate").ToString) < CDate(txtStartDate.Text) Then
                        '    msg &= "Meeting start date must be on or after Class Section start date " & txtStartDate.Text & vbCr
                        '    Exit For
                        'End If
                        ClsSectMtgObject.StartDate = dr("StartDate")

                        'If CDate(dr("EndDate").ToString) > CDate(txtEndDate.Text) Then
                        '    msg &= "Meeting end date must be on or before Class Section end date " & txtEndDate.Text & vbCr
                        '    Exit For
                        'End If

                        ClsSectMtgObject.EndDate = dr("EndDate")
                        ClsSectMtgObject.ModDate = DateTime.Today.ToShortDateString
                        ClsSectMtgObject.InstructorId = ddlInstructorId.SelectedItem.Value.ToString

                        ' 09/13/2011 Janet Robinson - V2.10 Clock Hour Project
                        ClsSectMtgObject.InstructionTypeId = dr("InstructionTypeId")

                        If dr("cmdType").ToString = "1" Then
                            ClsSectMtgObject.ClsSectMtgId = System.Guid.NewGuid
                        Else
                            ClsSectMtgObject.ClsSectMtgId = XmlConvert.ToGuid(dr("ClsSectMeetingId").ToString)
                        End If
                        ClsSectMtgObject.CmdType = CType(dr("CmdType"), Integer)

                        If Not dr("BreakDuration") Is System.DBNull.Value Then
                            ClsSectMtgObject.BreakDuration = CType(dr("BreakDuration"), Integer)
                        End If
                        ClsSectArrayList.Add(ClsSectMtgObject)
                    End If
                Next


            End If




        End If




        '' '' ''If msg <> "" Then
        '' '' ''    'Display Error Msg.
        '' '' ''    DisplayErrorMessage(msg)
        '' '' ''    'lblErrMsg.Text = ErrStr
        '' '' ''End If
        Return ClsSectArrayList
    End Function

    Public Sub SetClsSectMtgIds(ByVal GuidArrList As ArrayList)
        Dim arrlistcount As Integer
        Dim ClsSectMtgGuid As New ClsSectMtgIdObjInfo
        'Dim count As Integer
        Dim obj As New Object
        Dim ctl As Control
        Dim txt As TextBox
        Dim chk As CheckBox

        'Get the number of records in the array list
        arrlistcount = GuidArrList.Count

        If arrlistcount = 0 Then
            '   Meeting information has been removed.
            '   Empty content from ClsSectionMeetingId, ModDate and FetchedFromDB controls.
            For i As Integer = 1 To 10
                'ctl = pnlRHS.FindControl("txtClsSectMeetingId" & i.ToString)
                ctl = FindControlRecursive("txtClsSectMeetingId" & i.ToString)
                txt = CType(ctl, TextBox)
                If Not (txt Is Nothing) Then
                    txt.Text = System.Guid.Empty.ToString
                End If

                'ctl = pnlRHS.FindControl("txtModDate" & i.ToString)
                ctl = FindControlRecursive("txtModDate" & i.ToString)
                txt = CType(ctl, TextBox)
                If Not (txt Is Nothing) Then
                    txt.Text = ""
                End If

                'ctl = pnlRHS.FindControl("chkfetchedFromDB" & i.ToString)
                ctl = FindControlRecursive("chkfetchedFromDB" & i.ToString)
                chk = CType(ctl, CheckBox)
                If Not (chk Is Nothing) Then
                    chk.Checked = False
                End If
            Next

        Else

            For Each obj In GuidArrList
                If (obj.GetType Is GetType(ClsSectMtgIdObjInfo)) Then
                    ClsSectMtgGuid = DirectCast(obj, ClsSectMtgIdObjInfo)
                    'Loop thru the records in the arraylist, and bind each record to the 
                    'respective controls on the page
                    'Bind each record to the controls 

                    'ctl = pnlRHS.FindControl(ClsSectMtgGuid.txtboxname & ClsSectMtgGuid.count.ToString)
                    ctl = FindControlRecursive(ClsSectMtgGuid.txtboxname & ClsSectMtgGuid.count.ToString)
                    txt = CType(ctl, TextBox)
                    'The If condition was added by Troy on 6/13/2005 at 1:42 AM. This change is necessary
                    'based on the changes made in the Save click event
                    If Left(ClsSectMtgGuid.ClsSectMtgId.ToString, 6) <> "000000" Then
                        txt.Text = ClsSectMtgGuid.ClsSectMtgId.ToString
                    End If
                End If
            Next

        End If




    End Sub



    Private Sub ddlCampus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCampus.SelectedIndexChanged
        Dim ds As New DataSet
        'Dim sCampusId As String
        Dim Facade As New ClassSectionPeriodsFacade

        'Commented out by Troy on 4/4/2005. The populating of the rooms is now done in the page load event handler.
        'sCampusId = ddlCampus.SelectedValue.ToString
        'If sCampusId <> "" Then
        '    ds = Facade.GetCampRooms(sCampusId)
        '    BindAllRoomDDLs(ds)
        'End If

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub




    Private Sub chkAllowConflicts_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllowConflicts.CheckedChanged
        'If chkAllowConflicts.Checked Then
        '    If txtStartDate.Text <> "" And txtEndDate.Text <> "" And _
        '            ddlReqId.SelectedValue.ToString <> "" And _
        '            ddlCampus.SelectedValue.ToString <> "" Then
        '        Dim startDate As Date = txtStartDate.Text
        '        Dim endDate As Date = txtEndDate.Text
        '        With lbxConflictCourses
        '            .DataSource = (New TranscriptFacade).GetConflictingClassSectionsUsingRotationalSchedule(ddlReqId.SelectedValue.ToString, startDate, endDate, ddlCampus.SelectedValue.ToString, ddlShiftId.SelectedValue.ToString, txtClsSectionId.Text, False)
        '            .DataTextField = "ClassDescrip"
        '            .DataValueField = "ClsSectionId"
        '            .DataBind()
        '        End With
        '        lbxConflictCourses.Visible = True
        '        lblConflictCourses.Visible = True
        '    Else
        '        chkAllowConflicts.Checked = False
        '        DisplayErrorMessage("Start Date, End Date, Course and Campus are required to retrieve conflicting Class Sections.")
        '        Exit Sub
        '    End If
        'Else
        '    lbxConflictCourses.Visible = False
        '    lblConflictCourses.Visible = False
        'End If

    End Sub

    Public Sub TextValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        Dim strMinus As String = Mid(args.Value, 1, 1)
        If strMinus = "-" Then
            DisplayErrorMessage("The Value Cannot be Negative")
            validatetxt = True
        End If
    End Sub

    'Protected Sub ddlPeriodId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPeriodId.SelectedIndexChanged
    '    Dim ds As New DataSet
    '    Dim sPeriodId As String
    '    Dim Facade As New ClassSectionPeriodsFacade

    '    sPeriodId = ddlPeriodId.SelectedValue.ToString
    '    If sPeriodId <> "Select" Then
    '        ds = Facade.GetDaysInPeriod(sPeriodId)
    '        'BindAllRoomDDLs(ds)
    '        SelectDayDDLsFromPeriod(ds, "ddlDay", "ddlStart", "ddlEnd")
    '    End If
    'End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnBuildList)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        'BIndToolTip()
        BindToolTip("ddlTerm")
        BindToolTip("ddlCourse")
        BindToolTip("ddlInstructor")
        BindToolTip("ddlShiftId2")

    End Sub

    Private Sub populateTerms(ByVal ddl As Telerik.Web.UI.RadComboBox)
        Dim dt As New DataTable
        dt = (New TermsFacade).GetAllTerms_SP(True, campusId)
        ddl.Items.Clear()
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select"
        item1.Value = "00000000-0000-0000-0000-000000000000"
        ddl.Items.Add(item1)
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    '  DE7935 7/25/2012 Janet Robinson switch radcombobox to dropdown because validations not always firing
    Private Sub populateTerms1(ByVal ddl As DropDownList)
        Dim dt As New DataTable
        dt = (New TermsFacade).GetAllTerms_SP(True, campusId)
        Dim item1 As New ListItem
        ddl.DataSource = dt
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddl.SelectedIndex = 0

    End Sub
    Private Sub populateCourses(ByVal ddl As Telerik.Web.UI.RadComboBox)
        Dim dt As New DataTable
        dt = (New CoursesFacade).GetAllCourses_SP(True, campusId)
        ddl.Items.Clear()
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select"
        item1.Value = "00000000-0000-0000-0000-000000000000"
        ddl.Items.Add(item1)
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub

    Private Sub populateCourses1(ByVal ddl As DropDownList)
        Dim dt As New DataTable
        dt = (New CoursesFacade).GetAllCourses_SP(True, campusId)
        Dim item1 As New ListItem
        ddl.DataSource = dt
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddl.SelectedIndex = 0

    End Sub
    Private Sub populateInstructors_Rad(ByVal ddl As Telerik.Web.UI.RadComboBox)
        Dim dt As New DataTable
        dt = (New UserSecurityFacade).GetInstructorActive_SP()
        ddl.Items.Clear()
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select"
        item1.Value = "00000000-0000-0000-0000-000000000000"
        ddl.Items.Add(item1)
        ddl.DataSource = dt
        ddl.DataBind()
    End Sub
    Private Sub populateInstructors1(ByVal ddl As DropDownList)
        Dim dt As New DataTable
        dt = (New UserSecurityFacade).GetInstructorActive_SP()
        Dim item1 As New ListItem
        ddl.DataSource = dt
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddl.SelectedIndex = 0

    End Sub

    'Private Sub populateShifts()
    '    ddlShiftId.Items.Clear()
    '    ddlShiftId2.Items.Clear()
    '    Dim dt As New DataTable
    '    dt = (New ShiftsFacade).GetAllShifts_SP(True)
    '    ddlShiftId.DataSource = dt
    '    ddlShiftId.DataBind()
    '    ddlShiftId.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
    '    ddlShiftId.SelectedIndex = 0
    '    ddlShiftId2.DataSource = dt
    '    ddlShiftId2.DataBind()
    '    'ddlShiftId2.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
    '    ddlShiftId2.SelectedIndex = 0
    'End Sub

    Private Sub populateShifts_Rad(ByVal ddl As Telerik.Web.UI.RadComboBox)
        ddl.Items.Clear()
        Dim dt As New DataTable
        dt = (New ShiftsFacade).GetAllShifts_SP(True)
        ddl.Items.Clear()
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select"
        item1.Value = "00000000-0000-0000-0000-000000000000"
        ddl.Items.Add(item1)
        ddl.DataSource = dt
        ddl.DataBind()
    End Sub
    Private Sub populateShifts1(ByVal ddl As DropDownList)
        Dim dt As New DataTable
        dt = (New ShiftsFacade).GetAllShifts_SP(True)
        Dim item1 As New ListItem
        ddl.DataSource = dt
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddl.SelectedIndex = 0

    End Sub
    'Private Sub populateInstructors()
    '    ddlInstructor.Items.Clear()
    '    ddlInstructorId.Items.Clear()
    '    Dim dt As New DataTable
    '    dt = (New UserSecurityFacade).GetInstructor_SP()
    '    ddlInstructor.DataSource = dt
    '    ddlInstructor.DataBind()
    '    'ddlInstructor.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
    '    ddlInstructor.SelectedIndex = 0
    '    ddlInstructorId.DataSource = dt
    '    ddlInstructorId.DataBind()
    '    ddlInstructorId.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
    '    ddlInstructorId.SelectedIndex = 0



    'End Sub
    Private Sub populateCampus()
        'ddlCampus.Items.Clear()
        'Dim dt As New DataTable
        'dt = (New CampusGroupsFacade).GetAllCampus_SP(True, campusId)
        'ddlCampus.Items.Clear()
        'Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        'item1.Text = "Select"
        'item1.Value = "00000000-0000-0000-0000-000000000000"
        'ddlCampus.Items.Add(item1)
        'ddlCampus.DataSource = dt
        'ddlCampus.DataBind()

        Dim dt As New DataTable
        dt = (New CampusGroupsFacade).GetAllCampus_SP(True, campusId)
        Dim item1 As New ListItem
        ddlCampus.DataSource = dt
        ddlCampus.DataBind()
        ddlCampus.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddlCampus.SelectedIndex = 0
    End Sub
    Private Sub populateGradeScales()
        'ddlGrdScaleId.Items.Clear()
        'Dim dt As New DataTable
        'dt = (New GradesFacade).GetAllGradeScales_SP(True, campusId)
        'Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        'item1.Text = "Select"
        'item1.Value = "00000000-0000-0000-0000-000000000000"
        'ddlGrdScaleId.Items.Add(item1)
        'ddlGrdScaleId.DataSource = dt
        'ddlGrdScaleId.DataBind()

        Dim dt As New DataTable
        dt = (New GradesFacade).GetAllGradeScales_SP(True, campusId)
        Dim item1 As New ListItem
        ddlGrdScaleId.DataSource = dt
        ddlGrdScaleId.DataBind()
        ddlGrdScaleId.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddlGrdScaleId.SelectedIndex = 0
    End Sub

    Private Sub populateLeadGroups()
        'ddlLeadGrp.Items.Clear()
        'Dim dt As New DataTable
        'dt = (New LeadFacade).GetLeadGroups_SP(True, campusId)
        'Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        'item1.Text = "Select"
        'item1.Value = "00000000-0000-0000-0000-000000000000"
        'ddlLeadGrp.Items.Add(item1)
        'ddlLeadGrp.DataSource = dt
        'ddlLeadGrp.DataBind()

        Dim dt As New DataTable
        dt = (New LeadFacade).GetLeadGroups_SP(True, campusId)
        Dim item1 As New ListItem
        ddlLeadGrp.DataSource = dt
        ddlLeadGrp.DataBind()
        ddlLeadGrp.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddlLeadGrp.SelectedIndex = 0

    End Sub


    Private Sub populateCoursesForTerm(ByVal TermId As String, ByVal ctl As DropDownList, Optional ByVal ReqId As String = "")
        ctl.Items.Clear()

        Dim ds As DataSet
        Dim dt As New DataTable
        Dim showActiveOnly As Boolean

        If ReqId = "" Then
            showActiveOnly = True
        Else
            showActiveOnly = (New CoursesFacade).IsCourseActive(ReqId)
        End If



        If TermId = "00000000-0000-0000-0000-000000000000" Then
            dt = (New CoursesFacade).GetAllCourses_SP(True, campusId)
        Else
            ds = (New CoursesFacade).GetCoursesForTerm_SP(TermId, campusId, showActiveOnly)
            dt = ds.Tables(0)

            If (Not IsDBNull(ds.Tables(1).Rows(0)("StartDate")) AndAlso Not IsDBNull(ds.Tables(1).Rows(0)("EndDate"))) Then
                txtStartDate.SelectedDate = ds.Tables(1).Rows(0)("StartDate")
                txtEndDate.SelectedDate = ds.Tables(1).Rows(0)("EndDate")
            End If

        End If
        ctl.DataSource = dt
        ctl.DataBind()
        ctl.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ctl.SelectedIndex = 0
    End Sub

    Private Sub populateCoursesFromClassSectionsForTerm(ByVal TermId As String, ByVal ctl As Telerik.Web.UI.RadComboBox)
        ctl.Items.Clear()
        Dim dt As New DataTable
        If TermId = "00000000-0000-0000-0000-000000000000" Then
            dt = (New CoursesFacade).GetAllCourses_SP(True, campusId)
        Else
            dt = (New CoursesFacade).GetCoursesFromClassSectionsForTerm_SP(TermId)
        End If
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select"
        item1.Value = "00000000-0000-0000-0000-000000000000"
        ctl.Items.Add(item1)
        ctl.DataSource = dt
        ctl.DataBind()
    End Sub



    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        '  DE7935 7/25/2012 Janet Robinson switch radcombobox to dropdown because validations not always firing

        populateTerms(ddlTerm)
        'populateTerms(ddlTermId)
        populateTerms1(ddlTermId)

        populateCourses(ddlCourse)
        'populateCourses(ddlReqId)
        populateCourses1(ddlReqId)

        populateShifts_Rad(ddlShiftId2)
        'populateShifts_Rad(ddlShiftId)
        populateShifts1(ddlShiftId)

        'populateInstructors()
        populateInstructors_Rad(ddlInstructor)
        'populateInstructors_Rad(ddlInstructorId)
        populateInstructors1(ddlInstructorId)

        populateCampus()

        populateGradeScales()

        populateLeadGroups()



    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
    'Private Sub populateTerms()
    '    dtTable = New DataTable()
    '    'Open the SqlConnection
    '    SqlConnection.Open()
    '    Try
    '        'Select Query to populate the RadGrid with data from table Employees.
    '        'Dim selectQuery As String = "SELECT * FROM Employees,Departments where Employees.DeptId=Departments.DeptId "
    '        Dim command As SqlCommand = New SqlCommand("usp_GetTerms", SqlConnection)
    '        command.CommandType = CommandType.StoredProcedure
    '        'command.Parameters.Add("@CustomerID", "PAULK")
    '        'SqlDataAdapter.SelectCommand = New SqlCommand(selectQuery, SqlConnection)
    '        SqlDataAdapter.SelectCommand = command
    '        SqlDataAdapter.Fill(dtTable)
    '        RadCmbTerm.DataSource = dtTable
    '        RadCmbTerm.DataBind()
    '    Finally
    '        'Close the SqlConnection
    '        SqlConnection.Close()
    '    End Try
    'End Sub

    'Protected Sub ddlTerm_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlTerm.SelectedIndexChanged

    '    'Dim fac As New TranscriptFacade
    '    'Dim progID As String = fac.GetProgramForTerm(ddlTerm.SelectedItem.Value.ToString())
    '    'ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
    '    'If progID <> "" Then
    '    '    With ddlCourse
    '    '        .DataSource = fac.GetCoursesForProgram(progID)
    '    '        '.DataMember = "CourseDT"
    '    '        .DataValueField = "ReqId"
    '    '        .DataTextField = "FullDescrip"
    '    '        '.DataTextField = "Descrip"
    '    '        .DataBind()
    '    '        .Items.Insert(0, New ListItem("Select", ""))
    '    '        .SelectedIndex = 0
    '    '    End With
    '    'Else
    '    '    'bindCourseDDL(ddlCourse)
    '    '    BindAllCourseDDLs(ds, ddlCourse)
    '    'End If

    'End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlTerm.SelectedIndexChanged
        populateCoursesFromClassSectionsForTerm(ddlTerm.SelectedValue.ToString, ddlCourse)
        populateInstructors_Rad(ddlInstructor)
        populateShifts_Rad(ddlShiftId2)
    End Sub

    'Protected Sub ddlTermId_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlTermId.SelectedIndexChanged
    '    populateCoursesForTerm(ddlTermId.SelectedValue.ToString, ddlReqId)
    'End Sub
    Protected Sub ddlTermId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTermId.SelectedIndexChanged
        populateCoursesForTerm(ddlTermId.SelectedValue.ToString, ddlReqId)
    End Sub

    Protected Sub dtlClsSectList_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles dtlClsSectList.ItemCommand
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities
        Dim ClsSectObject As New ClassSectionInfo
        'Dim ClsSectMeeting As ClsSectMeetingInfo
        'Dim ClsSectMtgArrList As ArrayList
        'Dim ArrListcount As Integer
        Dim count As Integer = 0

        Dim facade As New ClassSectionPeriodsFacade
        Dim CommonUtilities As New CommonUtilities
        Dim ClsSectIdGuid As New Guid
        ' I have to manually clear the label for the date completed field

        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
        If e.Item.ItemIndex <> -1 Then
            ViewState("ItemSelected") = e.Item.ItemIndex.ToString
            'Get selected record from the database
            ClsSectIdGuid = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("ClsSectionId")
            ViewState("ClsSectIdGuid") = ClsSectIdGuid
            ds = facade.GetClassSection(ClsSectIdGuid)
            ClsSectObject = facade.ConvertSingleClsSectToObject(ds)
            Dim dtOverridenConflicts As DataTable = ds.Tables("OverridenConflicts")

            txtClsSectionId.Text = ClsSectObject.ClsSectId.ToString
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTermId, ClsSectObject.TermId.ToString, ClsSectObject.Term)
            'ddlTermId.SelectedValue = ClsSectObject.TermId.ToString

            'DE 6738 Build Course DDL before pointing to the right course
            populateCoursesForTerm(ddlTermId.SelectedValue.ToString, ddlReqId, ClsSectObject.CourseId.ToString)
            ddlReqId.SelectedValue = ClsSectObject.CourseId.ToString

            'Dim strSelection As String

            'strSelection = ClsSectObject.InstructorId2.ToString
            'If strSelection <> "" Then ddlInstructorId.SelectedValue = strSelection

            'strSelection = ClsSectObject.ShiftId2.ToString
            'If strSelection <> "" Then ddlShiftId.SelectedValue = strSelection

            'strSelection = ClsSectObject.CampusId.ToString
            'If strSelection <> "" Then ddlCampus.SelectedValue = strSelection

            'strSelection = ClsSectObject.GrdScaleId.ToString
            'If strSelection <> "" Then ddlGrdScaleId.SelectedValue = strSelection

            'strSelection = ClsSectObject.LeadGrpId.ToString
            'If strSelection <> "" Then ddlLeadGrp.SelectedValue = strSelection

            '**REM DD 6/20/11
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlInstructorId, ClsSectObject.InstructorId2.ToString, ClsSectObject.Instructor)
            ' set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlShiftId, ClsSectObject.ShiftId2.ToString, ClsSectObject.Shift)
            '  set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCampus, ClsSectObject.CampusId.ToString, ClsSectObject.Campus)
            ' set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGrdScaleId, ClsSectObject.GrdScaleId.ToString, ClsSectObject.GrdScale)

            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlLeadGrp, ClsSectObject.LeadGrpId.ToString, ClsSectObject.LeadGrp)
            '***

            ViewState("OrgGrdScale") = ddlGrdScaleId.SelectedValue.ToString

            txtClsSection.Text = ClsSectObject.Section
            txtStartDate.SelectedDate = ClsSectObject.StartDate.ToShortDateString
            txtEndDate.SelectedDate = ClsSectObject.EndDate.ToShortDateString
            If ClsSectObject.StudentStartDate = Date.MinValue Or ClsSectObject.StudentStartDate.ToShortDateString = "12/30/1899" Then
                txtStudentStartDate.SelectedDate = Nothing
                'txtStudentStartDate.Text = ""
            Else
                txtStudentStartDate.SelectedDate = ClsSectObject.StudentStartDate.ToShortDateString
            End If
            hdnGrdWeights.Text = ClsSectObject.InstrGrdBkWgtId.ToString
            hdnInstructor.Text = ClsSectObject.InstructorId.ToString

            txtMaxStud.Text = CType(ClsSectObject.MaxStud, String)
            hdnMaxStud.Value = CType(ClsSectObject.MaxStud, String)
            txtModUser.Text = ClsSectObject.ModUser
            txtModDate.Text = ClsSectObject.ModDate.ToString
            ' chkAllowConflicts.Checked = ClsSectObject.AllowOverrideConflicts
            txtOriginalTermId.Text = ClsSectObject.TermId.ToString


            ' 08/24/2011 Janet Robinson - Clock Hours Project Added new fields
            chkAllowEarlyIn.Checked = ClsSectObject.AllowEarlyIn
            chkAllowLateOut.Checked = ClsSectObject.AllowLateOut
            chkAllowExtraHours.Checked = ClsSectObject.AllowExtraHours
            chkCheckTardyIn.Checked = ClsSectObject.CheckTardyIn

            If ClsSectObject.MaxInBeforeTardy >= radMaxInBeforeTardy.MinDate Then
                radMaxInBeforeTardy.DbSelectedDate = ClsSectObject.MaxInBeforeTardy
            Else
                radMaxInBeforeTardy.Clear()
            End If

            If radAssignTardyInTime.DbSelectedDate < radAssignTardyInTime.MinDate Then
                radAssignTardyInTime.Clear()
            End If

            If radAssignTardyInTime.DbSelectedDate > radAssignTardyInTime.MaxDate Then
                radAssignTardyInTime.Clear()
            End If

            'Try
            '    radAssignTardyInTime.DbSelectedDate = ClsSectObject.AssignTardyInTime
            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    radAssignTardyInTime.Clear()
            'End Try

            'Set the fetched from DB check-box to true.
            chkfetchedFromDB.Checked = True

            'If ddlCampus.SelectedItem.Value.ToString <> "" Then
            '    ds = facade.GetCampRooms(ddlCampus.SelectedItem.Value.ToString)
            '    'BindAllRoomDDLs(ds)
            'End If
            If txtClsSectionId.Text <> "" Then
                Dim dtClsmeetings As DataTable = (New ClassSectionPeriodsFacade).GetClsSectMtgsWithPeriods_SP(New Guid(txtClsSectionId.Text))
                ViewState("dtClsmeetings") = dtClsmeetings
                RadGrdClassMeetings.DataSource = dtClsmeetings
                RadGrdClassMeetings.DataBind()

            End If
            RadGrdClassMeetings.MasterTableView.ClearEditItems()
            RadGrdClassMeetings.MasterTableView.IsItemInserted = False
            RadGrdClassMeetings.Rebind()

            'RadGrdClassMeetings.MasterTableView.IsItemInserted = False


            'Dim SDFControls As New SDFComponent
            'SDFControls.GenerateControlsEdit(pnlSDF, ResourceId, txtClsSectionId.Text, ModuleId)

            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            HideShowTimeClockFeatures()

            'Make sure to uncheck the Ignore Conflicts checkbox
            chkAllowScheduleConflicts.Checked = False

            Try
                ViewState("Mode") = "EDIT"
                InitButtonsForEdit()

                ' objCommon.SetBtnState(Form1, "EDIT", pObj)
                ViewState("ItemSelected") = e.Item.ItemIndex
                'PopulateDataList("Item")

                '   set Style to Selected Item
                ' '' '' '' '' ''CommonWebUtilities.SetStyleToSelectedItem(dtlClsSectList, ClsSectIdGuid.ToString, ViewState, Header1)

                'US3677: Enable the Copy Class Button 
                btnCloneClass.Enabled = True
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub dtlClsSectList_ItemCommand" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub dtlClsSectList_ItemCommand" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End If
    End Sub

    Protected Sub RadGrdClassMeetings_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdClassMeetings.DeleteCommand
        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        'Get the primary key value using the DataKeyValue.
        Dim ClsSectMeetingId As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("ClsSectMeetingId").ToString()
        Dim deletedRow() As Data.DataRow
        deletedRow = CType(ViewState("dtClsmeetings"), DataTable).Select("ClsSectMeetingId='" + ClsSectMeetingId + "'")
        'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
        deletedRow(0)("CmdType") = 3
        e.Canceled = True
        RadGrdClassMeetings.Rebind()

        'ViewState("dtClsmeetings") = Nothing

    End Sub

    Protected Sub RadGrdClassMeetings_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdClassMeetings.InsertCommand
        Dim insertedItem As GridDataInsertItem = DirectCast(e.Item, GridDataInsertItem)
        If ViewState("dtClsmeetings") Is Nothing Then
            ViewState("dtClsmeetings") = GetTable.Clone()
        End If


        'Troy: If the user has checked to ignore scheduling conflicts we should bypass conflicts checks
        If chkAllowScheduleConflicts.Checked = False Then
            '''''''''''''''''''''' Code to check schedule conflicts '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''' Logic to handle conflicts is in the stored procedure '''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''' This section also looks at old and new values of the class meeting parameters ''''''''''''''''''''''
            dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArray(txtClsSectionId.Text.ToString,
                                                                                              TryCast(insertedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate,
                                                                                              TryCast(insertedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate,
                                                                                              TryCast(insertedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue,
                                                                                              ddlInstructorId.SelectedValue.ToString,
                                                                                              ddlTermId.SelectedValue.ToString,
                                                                                              ddlReqId.SelectedValue.ToString,
                                                                                              TryCast(insertedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue,
                                                                                              ddlCampus.SelectedValue)

            Dim strStartDate As String = TryCast(insertedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate.ToShortDateString
            Dim strEndDate As String = TryCast(insertedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate.ToShortDateString
            Dim strRoomId As String = TryCast(insertedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue.ToString
            Dim strPeriodId As String = TryCast(insertedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue.ToString

            If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                ViewState("ClsSectionId") = txtClsSectionId.Text.ToString
                ViewState("CourseDescrip") = ddlReqId.SelectedItem.Text
                If InStr(ViewState("ClassMeetingStartDate").ToString.Trim, strStartDate.ToString.Trim) <= 0 Then
                    If Not ViewState("ClassMeetingStartDate").ToString.Trim = "" AndAlso Session("ClassMeetingStartDate_SavedValue") = "" Then
                        ViewState("ClassMeetingStartDate") &= "," & strStartDate & "," 'If there are existing Period Ids
                    ElseIf Not ViewState("ClassMeetingStartDate").ToString.Trim = "" AndAlso Not Session("ClassMeetingStartDate_SavedValue").ToString.Trim = "" Then
                        ViewState("ClassMeetingStartDate") = Replace(ViewState("ClassMeetingStartDate"), Session("ClassMeetingStartDate_SavedValue"), strStartDate)
                    Else
                        ViewState("ClassMeetingStartDate") &= strStartDate & ","
                    End If
                End If
                If InStr(ViewState("ClassMeetingEndDate").ToString.Trim, strEndDate.ToString.Trim) <= 0 Then
                    If Not ViewState("ClassMeetingEndDate").ToString.Trim = "" AndAlso Session("ClassMeetingEndDate_SavedValue") = "" Then
                        ViewState("ClassMeetingEndDate") &= "," & strEndDate & "," 'If there are existing Period Ids
                    ElseIf Not ViewState("ClassMeetingEndDate").ToString.Trim = "" AndAlso Not Session("ClassMeetingEndDate_SavedValue").ToString.Trim = "" Then
                        ViewState("ClassMeetingEndDate") = Replace(ViewState("ClassMeetingEndDate"), Session("ClassMeetingEndDate_SavedValue"), strEndDate)
                    Else
                        ViewState("ClassMeetingEndDate") &= strEndDate & ","
                    End If
                End If
                If InStr(ViewState("RoomId").ToString.Trim, strRoomId.ToString.Trim) <= 0 Then
                    'ViewState("RoomId") &= strRoomId & ","
                    If Not ViewState("RoomId").ToString.Trim = "" AndAlso Session("RoomId_SavedValue") = "" Then
                        ViewState("RoomId") &= "," & strRoomId & "," 'If there are existing Period Ids
                    ElseIf Not ViewState("RoomId").ToString.Trim = "" AndAlso Not Session("RoomId_SavedValue").ToString.Trim = "" Then
                        ViewState("RoomId") = Replace(ViewState("RoomId"), Session("RoomId_SavedValue"), strRoomId)
                    Else
                        ViewState("RoomId") &= strRoomId & ","
                    End If
                End If
                If InStr(ViewState("PeriodId").ToString.Trim, strPeriodId.ToString.Trim) <= 0 Then
                    If Not ViewState("PeriodId").ToString.Trim = "" AndAlso Session("Period_SavedValue") = "" Then
                        ViewState("PeriodId") &= "," & strPeriodId & "," 'If there are existing Period Ids
                    ElseIf Not ViewState("PeriodId").ToString.Trim = "" AndAlso Not Session("Period_SavedValue").ToString.Trim = "" Then
                        ViewState("PeriodId") = Replace(ViewState("PeriodId"), Session("Period_SavedValue"), strPeriodId)
                    Else
                        ViewState("PeriodId") &= strPeriodId & ","
                    End If
                End If
            End If
            If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                If InStrRev(ViewState("ClassMeetingStartDate"), ",") >= 1 Then
                    ViewState("ClassMeetingStartDate") = Mid(ViewState("ClassMeetingStartDate"), 1, InStrRev(ViewState("ClassMeetingStartDate"), ",") - 1)
                End If
                If InStrRev(ViewState("ClassMeetingEndDate"), ",") >= 1 Then
                    ViewState("ClassMeetingEndDate") = Mid(ViewState("ClassMeetingEndDate"), 1, InStrRev(ViewState("ClassMeetingEndDate"), ",") - 1)
                End If
                If InStrRev(ViewState("RoomId"), ",") >= 1 Then
                    ViewState("RoomId") = Mid(ViewState("RoomId"), 1, InStrRev(ViewState("RoomId"), ",") - 1)
                End If
                If InStrRev(ViewState("PeriodId"), ",") >= 1 Then
                    ViewState("PeriodId") = Mid(ViewState("PeriodId"), 1, InStrRev(ViewState("PeriodId"), ",") - 1)
                End If
                If Not ViewState("ClsSectionId").ToString.Trim = "" Then
                    ShowScheduleConflict(ViewState("ClsSectionId"), "", ViewState("CourseDescrip"),
                                         ViewState("ClassMeetingStartDate"),
                                         ViewState("ClassMeetingEndDate"),
                                         ViewState("RoomId"),
                                         ViewState("PeriodId")) 'Show the radWindow
                    'Clear Previous Values
                    Session("ClassMeetingStartDate_SavedValue") = ""
                    Session("ClassMeetingEndDate_SavedValue") = ""
                    Session("RoomId_SavedValue") = ""
                    Session("Period_SavedValue") = ""

                    'Need to clear the following two viewstates for validation
                    ViewState("ClsSectionId") = ""
                    ViewState("CourseDescrip") = ""
                    ViewState("ClassMeetingStartDate") = ""
                    ViewState("ClassMeetingEndDate") = ""
                    ViewState("RoomId") = ""
                    ViewState("PeriodId") = ""

                    'TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue = Session("PeriodId_SavedValue")
                    Exit Sub
                End If
            End If

            Dim ClsSectMeetingId As String = txtClsSectionId.ToString  'Guid.NewGuid.ToString

            'Check for Conflicts with in same course
            For Each dr As DataRow In CType(ViewState("dtClsmeetings"), DataTable).Select("ClsSectMeetingId<>'" + ClsSectMeetingId + "' and CmdType not in (3) ")
                Dim currentMeetingEndDate As Date = TryCast(insertedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate
                Dim currentMeetingStartDate As Date = TryCast(insertedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate
                Dim currentRoomId As String = TryCast(insertedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue.ToString
                Dim currentPeriodId As String = TryCast(insertedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue.ToString
                Dim currentAltPeriodId As String = TryCast(insertedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue.ToString

                Dim previouslyAddedEndDate As Date = dr("EndDate")
                Dim previouslyAddedStartDate As Date = dr("StartDate")
                Dim previouslyAddedRoomId As String = dr("RoomId").ToString
                Dim previouslyAddedPeriodId As String = dr("PeriodId").ToString
                Dim previouslyAddedAltPeriodId As String = dr("AltPeriodId").ToString

                'Check for date overlaps
                'If all the below mentioned conditions are satisfied, there is an date overlap for same room
                'If previouslyAddedStartDate <= currentMeetingEndDate AndAlso _
                '    previouslyAddedEndDate >= currentMeetingStartDate AndAlso _
                '    previouslyAddedRoomId = currentRoomId Then
                If previouslyAddedStartDate <= currentMeetingEndDate AndAlso
                    previouslyAddedEndDate >= currentMeetingStartDate Then
                    'Check for days overlap, in other words check if periods overlap
                    Dim DoesWorkDaysOverlap As Integer = 0

                    DoesWorkDaysOverlap = (New ClassSectionFacade).GetWorkDaysOverlap(currentPeriodId.ToString.Trim, previouslyAddedPeriodId.ToString.Trim)

                    'If currentPeriodId.ToString.Trim = previouslyAddedPeriodId.ToString.Trim Then
                    If DoesWorkDaysOverlap >= 1 Then
                        'now there is a day overlap
                        Dim dsTimePeriod As New DataSet
                        Dim currentMeetingStartTime As String = ""
                        Dim currentMeetingEndTime As String = ""
                        Dim previousMeetingStartTime As String = ""
                        Dim previousMeetingEndTime As String = ""
                        Dim currentAltMeetingStartTime As String = ""
                        Dim currentAltMeetingEndTime As String = ""
                        Dim previousAltMeetingStartTime As String = ""
                        Dim previousAltMeetingEndTime As String = ""

                        'no collision on period
                        dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(currentPeriodId)
                        For Each drCurrentTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                            currentMeetingStartTime = drCurrentTimePeriod("StartTime")
                            currentMeetingEndTime = drCurrentTimePeriod("EndTime")
                        Next

                        dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(previouslyAddedPeriodId)
                        For Each drPreviousTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                            previousMeetingStartTime = drPreviousTimePeriod("StartTime")
                            previousMeetingEndTime = drPreviousTimePeriod("EndTime")
                        Next

                        If currentMeetingStartTime.Trim < previousMeetingEndTime.Trim AndAlso
                           currentMeetingEndTime > previousMeetingStartTime Then
                            'There is a schedule conflict
                            dsTimePeriod = Nothing
                            Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                            radmgr1.ResponseScripts.Add("radalert('The current meeting schedule conflicts with another meeting schedule added for this class', 600, 160);")
                            Exit Sub
                        End If


                        'no collision on alt period
                        dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(currentAltPeriodId)
                        For Each drCurrentAltTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                            currentAltMeetingStartTime = drCurrentAltTimePeriod("StartTime")
                            currentAltMeetingEndTime = drCurrentAltTimePeriod("EndTime")
                        Next

                        'dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(previouslyAddedAltPeriodId)
                        'For Each drPreviousAltTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                        '    previousAltMeetingStartTime = drPreviousAltTimePeriod("StartTime")
                        '    previousAltMeetingEndTime = drPreviousAltTimePeriod("EndTime")
                        'Next

                        If Not dsTimePeriod Is Nothing Then
                            dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(previouslyAddedAltPeriodId)
                            If Not dsTimePeriod Is Nothing Then
                                For Each drPreviousAltTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                                    previousAltMeetingStartTime = drPreviousAltTimePeriod("StartTime")
                                    previousAltMeetingEndTime = drPreviousAltTimePeriod("EndTime")
                                Next
                            End If
                        End If

                        If currentAltMeetingStartTime.Trim < previousAltMeetingEndTime.Trim AndAlso
                           currentAltMeetingEndTime > previousAltMeetingStartTime Then
                            'There is a schedule conflict
                            dsTimePeriod = Nothing
                            Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                            radmgr1.ResponseScripts.Add("radalert('The current alternative meeting schedule conflicts with another alternative meeting schedule added for this class', 600, 160);")
                            Exit Sub
                        End If
                    End If
                End If
            Next
        End If


        Dim txtbrkDur As TextBox
        txtbrkDur = TryCast(insertedItem("ContainerBreakDuration").Controls(1), TextBox)
        Dim BreakDuration As Integer

        If Not txtbrkDur.Text Is String.Empty Then
            If IsNumeric(txtbrkDur.Text) Then
                If txtbrkDur.Text = Math.Round(CDec(txtbrkDur.Text)) Then
                    BreakDuration = txtbrkDur.Text
                    dtPeriods = ViewState("dtPeriods")
                    Dim dr As DataRow()
                    dr = dtPeriods.Select(" PeriodID='" & TryCast(insertedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue.ToString & "'")
                    If dr.Length >= 1 Then
                        For Each periodRow As DataRow In dr
                            Dim StartTime As DateTime
                            Dim EndTime As DateTime
                            Dim durationinmins As Integer
                            StartTime = periodRow("StartTime")
                            EndTime = periodRow("EndTime")
                            durationinmins = DateDiff(DateInterval.Minute, StartTime, EndTime)

                            If BreakDuration >= durationinmins Then
                                Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                                radmgr1.ResponseScripts.Add("radalert('Break duration should be less than the period duration', 600, 160,'Error Message');")
                                Exit Sub
                            End If
                        Next
                    End If

                    If Not TryCast(insertedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue.ToString.Trim = String.Empty Then

                        dr = dtPeriods.Select(" PeriodID='" & TryCast(insertedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue.ToString & "'")
                        If dr.Length >= 1 Then
                            For Each periodRow As DataRow In dr
                                Dim StartTime As DateTime
                                Dim EndTime As DateTime
                                Dim durationinmins As Integer
                                StartTime = periodRow("StartTime")
                                EndTime = periodRow("EndTime")
                                durationinmins = DateDiff(DateInterval.Minute, StartTime, EndTime)

                                If BreakDuration >= durationinmins Then
                                    Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                                    radmgr1.ResponseScripts.Add("radalert('Break duration should be less than the Alternate period duration', 600, 160,'Error Message');")
                                    Exit Sub
                                End If
                            Next
                        End If
                    End If
                Else
                    Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                    radmgr1.ResponseScripts.Add("radalert('Break duration should not be in decimals', 600, 160,'Error Message');")
                    Exit Sub
                End If
            Else
                Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                radmgr1.ResponseScripts.Add("radalert('Break duration should be numeric and in mins', 600, 160,'Error Message');")
                Exit Sub
            End If
        End If

        Dim insertedRow As Data.DataRow = CType(ViewState("dtClsmeetings"), DataTable).NewRow
        insertedRow("ClsSectMeetingId") = Guid.NewGuid.ToString()
        insertedRow("PeriodDescrip") = TryCast(insertedItem("ContainerPeriod").Controls(1), DropDownList).SelectedItem.Text
        insertedRow("PeriodId") = TryCast(insertedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue
        insertedRow("AltPeriodDescrip") = TryCast(insertedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedItem.Text
        insertedRow("AltPeriodId") = TryCast(insertedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue
        insertedRow("RoomDescrip") = TryCast(insertedItem("ContainerRooms").Controls(1), DropDownList).SelectedItem.Text
        insertedRow("RoomId") = TryCast(insertedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue
        insertedRow("StartDate") = TryCast(insertedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate
        insertedRow("EndDate") = TryCast(insertedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate
        If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
            insertedRow("InstructionTypeDescrip") = TryCast(insertedItem("ContainerInstruction").Controls(1), DropDownList).SelectedItem.Text
            insertedRow("InstructionTypeId") = TryCast(insertedItem("ContainerInstruction").Controls(1), DropDownList).SelectedValue
        Else
            insertedRow("InstructionTypeDescrip") = ViewState("DefaultInstTypeDescrip")
            insertedRow("InstructionTypeId") = ViewState("DefaultInstTypeId")
        End If
        insertedRow("AttUsedCnt") = 0
        If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
            insertedRow("BreakDuration") = 0
        Else
            If Not TryCast(insertedItem("ContainerBreakDuration").Controls(1), TextBox).Text.ToString = "" Then
                insertedRow("BreakDuration") = CDec(TryCast(insertedItem("ContainerBreakDuration").Controls(1), TextBox).Text)
            Else
                insertedRow("BreakDuration") = 0
            End If

        End If

        'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
        insertedRow("CmdType") = 1
        CType(ViewState("dtClsmeetings"), DataTable).Rows.InsertAt(insertedRow, 0)
        CType(ViewState("dtClsmeetings"), DataTable).AcceptChanges()


        e.Canceled = True
        RadGrdClassMeetings.MasterTableView.IsItemInserted = False
        RadGrdClassMeetings.Rebind()

    End Sub

    Protected Sub RadGrdClassMeetings_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdClassMeetings.ItemCommand

        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            e.Canceled = True

            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            newValues("StartDate") = txtStartDate.SelectedDate
            newValues("EndDate") = txtEndDate.SelectedDate
            newValues("CmdType") = 1
            newValues("AttUsedCnt") = 0
            newValues("BreakDuration") = ""
            'Insert the item and rebind
            e.Item.OwnerTableView.InsertItem(newValues)
        End If



    End Sub

    Protected Sub RadGrdClassMeetings_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrdClassMeetings.ItemDataBound

        Try
            If (TypeOf e.Item Is GridHeaderItem) Then
                RadGrdClassMeetings.MasterTableView.PageSize = 20
                Dim gridItem As GridHeaderItem = DirectCast(e.Item, GridHeaderItem)
                If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                    gridItem.OwnerTableView.Columns.FindByUniqueName("ContainerInstruction").Visible = True
                Else
                    gridItem.OwnerTableView.Columns.FindByUniqueName("ContainerInstruction").Visible = False
                End If
                If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
                    gridItem.OwnerTableView.Columns.FindByUniqueName("ContainerBreakDuration").Visible = False
                Else
                    gridItem.OwnerTableView.Columns.FindByUniqueName("ContainerBreakDuration").Visible = True
                End If


            End If
            If (TypeOf e.Item Is GridDataItem) Then
                Dim gridItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                    gridItem.OwnerTableView.Columns.FindByUniqueName("ContainerInstruction").Visible = True
                Else
                    gridItem.OwnerTableView.Columns.FindByUniqueName("ContainerInstruction").Visible = False
                End If
                If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
                    gridItem.OwnerTableView.Columns.FindByUniqueName("ContainerBreakDuration").Visible = False
                Else
                    gridItem.OwnerTableView.Columns.FindByUniqueName("ContainerBreakDuration").Visible = True
                End If

            End If
            Dim intAttUsedCnt As Integer = 0
            If (TypeOf e.Item Is GridDataItem) Then
                If e.Item.IsInEditMode Then
                    Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                    Dim dropDownList As DropDownList = DirectCast(gridEditFormItem("ContainerPeriod").FindControl("ddlContainerPeriod"), DropDownList)
                    Dim ddl2 As DropDownList = DirectCast(gridEditFormItem("ContainerAltPeriod").FindControl("ddlContainerAltPeriod"), DropDownList)
                    Dim ddl3 As DropDownList = DirectCast(gridEditFormItem("ContainerRooms").FindControl("ddlContainerRooms"), DropDownList)
                    For Each myitem As ListItem In dropDownList.Items
                        myitem.Attributes.Add("title", myitem.Text)
                    Next
                    For Each myitem2 As ListItem In ddl2.Items
                        myitem2.Attributes.Add("title", myitem2.Text)
                    Next
                    For Each myitem3 As ListItem In ddl3.Items
                        myitem3.Attributes.Add("title", myitem3.Text)
                    Next
                    If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                        dropDownList.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(2).ToString()
                        Session("Period_SavedValue") = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(2).ToString()

                    End If
                    'dropDownList.DataBind()
                    dropDownList = DirectCast(gridEditFormItem("ContainerAltPeriod").FindControl("ddlContainerAltPeriod"), DropDownList)
                    If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                        dropDownList.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(4).ToString()
                    End If
                    'dropDownList.DataBind()
                    dropDownList = DirectCast(gridEditFormItem("ContainerRooms").FindControl("ddlContainerRooms"), DropDownList)
                    If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                        dropDownList.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(6).ToString()
                        Session("RoomId_SavedValue") = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(6).ToString()
                    End If
                    'dropDownList.DataBind()
                    If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                        dropDownList = DirectCast(gridEditFormItem("ContainerInstruction").FindControl("ddlInstructionType"), DropDownList)
                        For Each myitem As ListItem In dropDownList.Items
                            myitem.Attributes.Add("title", myitem.Text)
                        Next
                        If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                            dropDownList.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(10).ToString()
                        End If
                    End If
                    If Not MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
                        Dim txtBrkDur As TextBox = DirectCast(gridEditFormItem("ContainerBreakDuration").FindControl("txtBreakDuration"), TextBox)
                        If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                            txtBrkDur.Text = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(19).ToString()
                        End If
                    End If

                    If ViewState("Mode") = "NEW" Then
                        Dim lblMeetingStartDate As RadDatePicker = DirectCast(gridEditFormItem("ContainerStartDate").FindControl("pickerStartDate"), RadDatePicker)
                        Dim lblMeetingEndDate As RadDatePicker = DirectCast(gridEditFormItem("ContainerStartDate").FindControl("pickerEndDate"), RadDatePicker)
                        lblMeetingStartDate.DbSelectedDate = Nothing
                        lblMeetingEndDate.DbSelectedDate = Nothing
                    End If

                    Session("ClassMeetingStartDate_SavedValue") = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(7).ToString()
                    Session("ClassMeetingEndDate_SavedValue") = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(8).ToString()
                Else
                    Dim gridItem As GridDataItem = DirectCast(e.Item, GridDataItem)

                    intAttUsedCnt = DirectCast((gridItem.DataItem), System.Data.DataRowView).Row.ItemArray(12).ToString()
                    If intAttUsedCnt > 0 Then
                        CType(gridItem("EditCommandColumn").Controls(0), ImageButton).ToolTip = "Attendance has been Posted - No Edit/Delete"
                        gridItem("EditCommandColumn").Enabled = False
                        gridItem("DeleteColumn").Controls(0).Visible = False

                    Else
                        CType(gridItem("EditCommandColumn").Controls(0), ImageButton).ToolTip = "Edit"
                        gridItem("EditCommandColumn").Enabled = True
                        gridItem("DeleteColumn").Controls(0).Visible = True
                    End If
                End If
            End If
            If (TypeOf e.Item Is GridCommandItem) Then
                Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
                cmditm.FindControl("RefreshButton").Visible = False
                cmditm.FindControl("RebindGridButton").Visible = False
            End If


        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        Finally

        End Try


    End Sub

    Protected Sub RadGrdClassMeetings_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrdClassMeetings.NeedDataSource
        Dim dt As New DataTable

        If ViewState("dtClsmeetings") Is Nothing Then
            RadGrdClassMeetings.DataSource = dt
        Else
            If CType(ViewState("dtClsmeetings"), DataTable).Rows.Count > 0 Then
                RadGrdClassMeetings.DataSource = CType(ViewState("dtClsmeetings"), DataTable).Select("CmdType not in (3)", "StartDate")
            Else
                RadGrdClassMeetings.DataSource = dt
            End If
        End If



    End Sub

    Protected Sub RadGrdClassMeetings_Unload(sender As Object, e As EventArgs) Handles RadGrdClassMeetings.Unload

    End Sub



    Protected Sub RadGrdClassMeetings_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdClassMeetings.UpdateCommand
        Dim editedItem As GridEditableItem = TryCast(e.Item, GridEditableItem)
        ''Get the primary key value using the DataKeyValue.
        Dim ClsSectMeetingId As String = ""
        If ViewState("Mode").ToString.ToLower = "new" Then
            ClsSectMeetingId = Guid.NewGuid.ToString
        Else
            ClsSectMeetingId = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("ClsSectMeetingId").ToString()
        End If

        'Troy: If the user has checked to ignore scheduling conflicts we should bypass conflicts checks
        If chkAllowScheduleConflicts.Checked = False Then
            dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArray(txtClsSectionId.Text.ToString,
                                                                                              TryCast(editedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate,
                                                                                              TryCast(editedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate,
                                                                                              TryCast(editedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue,
                                                                                              ddlInstructorId.SelectedValue.ToString,
                                                                                              ddlTermId.SelectedValue.ToString,
                                                                                              ddlReqId.SelectedValue.ToString,
                                                                                              TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue, ddlCampus.SelectedValue)

            Dim strStartDate As String = TryCast(editedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate.ToShortDateString
            Dim strEndDate As String = TryCast(editedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate.ToShortDateString
            Dim strRoomId As String = TryCast(editedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue.ToString
            Dim strPeriodId As String = TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue.ToString
            If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                ViewState("ClsSectionId") = txtClsSectionId.Text.ToString
                ViewState("CourseDescrip") = ddlReqId.SelectedItem.Text
                If InStr(ViewState("ClassMeetingStartDate").ToString.Trim, strStartDate.ToString.Trim) <= 0 Then
                    If Not ViewState("ClassMeetingStartDate").ToString.Trim = "" AndAlso Session("ClassMeetingStartDate_SavedValue") = "" Then
                        ViewState("ClassMeetingStartDate") &= "," & strStartDate & "," 'If there are existing Period Ids
                    ElseIf Not ViewState("ClassMeetingStartDate").ToString.Trim = "" AndAlso Not Session("ClassMeetingStartDate_SavedValue").ToString.Trim = "" Then
                        ViewState("ClassMeetingStartDate") = Replace(ViewState("ClassMeetingStartDate"), Session("ClassMeetingStartDate_SavedValue"), strStartDate)
                    Else
                        ViewState("ClassMeetingStartDate") &= strStartDate & ","
                    End If
                End If
                If InStr(ViewState("ClassMeetingEndDate").ToString.Trim, strEndDate.ToString.Trim) <= 0 Then
                    If Not ViewState("ClassMeetingEndDate").ToString.Trim = "" AndAlso Session("ClassMeetingEndDate_SavedValue") = "" Then
                        ViewState("ClassMeetingEndDate") &= "," & strEndDate & "," 'If there are existing Period Ids
                    ElseIf Not ViewState("ClassMeetingEndDate").ToString.Trim = "" AndAlso Not Session("ClassMeetingEndDate_SavedValue").ToString.Trim = "" Then
                        ViewState("ClassMeetingEndDate") = Replace(ViewState("ClassMeetingEndDate"), Session("ClassMeetingEndDate_SavedValue"), strEndDate)
                    Else
                        ViewState("ClassMeetingEndDate") &= strEndDate & ","
                    End If
                End If
                If InStr(ViewState("RoomId").ToString.Trim, strRoomId.ToString.Trim) <= 0 Then
                    'ViewState("RoomId") &= strRoomId & ","
                    If Not ViewState("RoomId").ToString.Trim = "" AndAlso Session("RoomId_SavedValue") = "" Then
                        ViewState("RoomId") &= "," & strRoomId & "," 'If there are existing Period Ids
                    ElseIf Not ViewState("RoomId").ToString.Trim = "" AndAlso Not Session("RoomId_SavedValue").ToString.Trim = "" Then
                        ViewState("RoomId") = Replace(ViewState("RoomId"), Session("RoomId_SavedValue"), strRoomId)
                    Else
                        ViewState("RoomId") &= strRoomId & ","
                    End If
                End If
                If InStr(ViewState("PeriodId").ToString.Trim, strPeriodId.ToString.Trim) <= 0 Then
                    If Not ViewState("PeriodId").ToString.Trim = "" AndAlso Session("Period_SavedValue") = "" Then
                        ViewState("PeriodId") &= "," & strPeriodId & "," 'If there are existing Period Ids
                    ElseIf Not ViewState("PeriodId").ToString.Trim = "" AndAlso Not Session("Period_SavedValue").ToString.Trim = "" Then
                        ViewState("PeriodId") = Replace(ViewState("PeriodId"), Session("Period_SavedValue"), strPeriodId)
                    Else
                        ViewState("PeriodId") &= strPeriodId & ","
                    End If
                End If
            End If
            If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                If InStrRev(ViewState("ClassMeetingStartDate"), ",") >= 1 Then
                    ViewState("ClassMeetingStartDate") = Mid(ViewState("ClassMeetingStartDate"), 1, InStrRev(ViewState("ClassMeetingStartDate"), ",") - 1)
                End If
                If InStrRev(ViewState("ClassMeetingEndDate"), ",") >= 1 Then
                    ViewState("ClassMeetingEndDate") = Mid(ViewState("ClassMeetingEndDate"), 1, InStrRev(ViewState("ClassMeetingEndDate"), ",") - 1)
                End If
                If InStrRev(ViewState("RoomId"), ",") >= 1 Then
                    ViewState("RoomId") = Mid(ViewState("RoomId"), 1, InStrRev(ViewState("RoomId"), ",") - 1)
                End If
                If InStrRev(ViewState("PeriodId"), ",") >= 1 Then
                    ViewState("PeriodId") = Mid(ViewState("PeriodId"), 1, InStrRev(ViewState("PeriodId"), ",") - 1)
                End If
                If Not ViewState("ClsSectionId").ToString.Trim = "" Then
                    ShowScheduleConflict(ViewState("ClsSectionId"), "", ViewState("CourseDescrip"),
                                         ViewState("ClassMeetingStartDate"),
                                         ViewState("ClassMeetingEndDate"),
                                         ViewState("RoomId"),
                                         ViewState("PeriodId")) 'Show the radWindow
                    'Clear Previous Values
                    Session("ClassMeetingStartDate_SavedValue") = ""
                    Session("ClassMeetingEndDate_SavedValue") = ""
                    Session("RoomId_SavedValue") = ""
                    Session("Period_SavedValue") = ""

                    'Need to clear the following two viewstates for validation
                    ViewState("ClsSectionId") = ""
                    ViewState("CourseDescrip") = ""
                    ViewState("ClassMeetingStartDate") = ""
                    ViewState("ClassMeetingEndDate") = ""
                    ViewState("RoomId") = ""
                    ViewState("PeriodId") = ""

                    'TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue = Session("PeriodId_SavedValue")
                    Exit Sub
                End If
            End If

            'Check for Conflicts with in same course
            For Each dr As DataRow In CType(ViewState("dtClsmeetings"), DataTable).Select("ClsSectMeetingId<>'" + ClsSectMeetingId + "' and CmdType not in (3)")
                Dim currentMeetingEndDate As Date = TryCast(editedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate
                Dim currentMeetingStartDate As Date = TryCast(editedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate
                Dim currentRoomId As String = TryCast(editedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue.ToString
                Dim currentPeriodId As String = TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue
                Dim currentAltPeriodId As String = TryCast(editedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue.ToString

                Dim previouslyAddedEndDate As Date = dr("EndDate")
                Dim previouslyAddedStartDate As Date = dr("StartDate")
                Dim previouslyAddedRoomId As String = dr("RoomId").ToString
                Dim previouslyAddedPeriodId As String = dr("PeriodId").ToString
                Dim previouslyAddedAltPeriodId As String = dr("AltPeriodId").ToString

                'Check for date overlaps
                'If all the below mentioned conditions are satisfied, there is an date overlap for same room
                If previouslyAddedStartDate <= currentMeetingEndDate AndAlso
                    previouslyAddedEndDate >= currentMeetingStartDate Then _
                    'previouslyAddedRoomId = currentRoomId Then
                    'Check for days overlap, in other words check if periods overlap
                    'Check for days overlap, in other words check if periods overlap
                    Dim DoesWorkDaysOverlap As Integer = 0
                    DoesWorkDaysOverlap = (New ClassSectionFacade).GetWorkDaysOverlap(currentPeriodId.ToString.Trim, previouslyAddedPeriodId.ToString.Trim)

                    'If currentPeriodId.ToString.Trim = previouslyAddedPeriodId.ToString.Trim Then
                    If DoesWorkDaysOverlap >= 1 Then
                        'If currentPeriodId.ToString.Trim = previouslyAddedPeriodId.ToString.Trim Then
                        'now there is a day overlap
                        Dim dsTimePeriod As New DataSet
                        Dim currentMeetingStartTime As String = ""
                        Dim currentMeetingEndTime As String = ""
                        Dim previousMeetingStartTime As String = ""
                        Dim previousMeetingEndTime As String = ""
                        Dim currentAltMeetingStartTime As String = ""
                        Dim currentAltMeetingEndTime As String = ""
                        Dim previousAltMeetingStartTime As String = ""
                        Dim previousAltMeetingEndTime As String = ""

                        'no collision on period
                        dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(currentPeriodId)
                        For Each drCurrentTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                            currentMeetingStartTime = drCurrentTimePeriod("StartTime")
                            currentMeetingEndTime = drCurrentTimePeriod("EndTime")
                        Next

                        dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(previouslyAddedPeriodId)
                        For Each drPreviousTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                            previousMeetingStartTime = drPreviousTimePeriod("StartTime")
                            previousMeetingEndTime = drPreviousTimePeriod("EndTime")
                        Next

                        If currentMeetingStartTime.Trim < previousMeetingEndTime.Trim AndAlso
                           currentMeetingEndTime > previousMeetingStartTime Then
                            'There is a schedule conflict
                            dsTimePeriod = Nothing
                            Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                            radmgr1.ResponseScripts.Add("radalert('The current meeting schedule conflicts with another meeting schedule added for this class', 600, 160);")
                            Exit Sub
                        End If

                        'no collision on alt period
                        dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(currentAltPeriodId)
                        For Each drCurrentAltTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                            currentAltMeetingStartTime = drCurrentAltTimePeriod("StartTime")
                            currentAltMeetingEndTime = drCurrentAltTimePeriod("EndTime")
                        Next

                        dsTimePeriod = (New ClassSectionFacade).GetTimeInterval(previouslyAddedAltPeriodId)
                        For Each drPreviousAltTimePeriod As DataRow In dsTimePeriod.Tables(0).Rows
                            previousAltMeetingStartTime = drPreviousAltTimePeriod("StartTime")
                            previousAltMeetingEndTime = drPreviousAltTimePeriod("EndTime")
                        Next

                        If currentAltMeetingStartTime.Trim < previousAltMeetingEndTime.Trim AndAlso
                           currentAltMeetingEndTime > previousAltMeetingStartTime Then
                            'There is a schedule conflict
                            dsTimePeriod = Nothing
                            Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                            radmgr1.ResponseScripts.Add("radalert('The current alternative meeting schedule conflicts with another alternative meeting schedule added for this class', 600, 160);")
                            Exit Sub
                        End If

                    End If
                End If
            Next
        End If

        Dim txtbrkDur As TextBox
        txtbrkDur = TryCast(editedItem("ContainerBreakDuration").Controls(1), TextBox)
        Dim BreakDuration As Integer

        If Not txtbrkDur.Text Is String.Empty Then
            If IsNumeric(txtbrkDur.Text) Then
                If txtbrkDur.Text = Math.Round(CDec(txtbrkDur.Text)) Then
                    BreakDuration = txtbrkDur.Text
                    dtPeriods = ViewState("dtPeriods")
                    Dim dr As DataRow()
                    dr = dtPeriods.Select(" PeriodID='" & TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue.ToString & "'")
                    If dr.Length >= 1 Then
                        For Each periodRow As DataRow In dr
                            Dim StartTime As DateTime
                            Dim EndTime As DateTime
                            Dim durationinmins As Integer
                            StartTime = periodRow("StartTime")
                            EndTime = periodRow("EndTime")
                            durationinmins = DateDiff(DateInterval.Minute, StartTime, EndTime)

                            If BreakDuration >= durationinmins Then
                                Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                                radmgr1.ResponseScripts.Add("radalert('Break duration should be less than the period duration', 600, 160,'Error Message');")
                                DisplayErrorMessage("")
                                Exit Sub
                            End If
                        Next
                    End If

                    If Not TryCast(editedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue.ToString.Trim = String.Empty Then

                        dr = dtPeriods.Select(" PeriodID='" & TryCast(editedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue.ToString & "'")
                        If dr.Length >= 1 Then
                            For Each periodRow As DataRow In dr
                                Dim StartTime As DateTime
                                Dim EndTime As DateTime
                                Dim durationinmins As Integer
                                StartTime = periodRow("StartTime")
                                EndTime = periodRow("EndTime")
                                durationinmins = DateDiff(DateInterval.Minute, StartTime, EndTime)

                                If BreakDuration >= durationinmins Then
                                    Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                                    radmgr1.ResponseScripts.Add("radalert('Break duration should be less than the Alternate period duration', 600, 160,'Error Message');")
                                    Exit Sub
                                End If
                            Next
                        End If
                    End If
                Else
                    Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                    radmgr1.ResponseScripts.Add("radalert('Break duration should not be in decimals', 600, 160,'Error Message');")
                    Exit Sub
                End If
            Else
                Dim radmgr1 As Telerik.Web.UI.RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                radmgr1.ResponseScripts.Add("radalert('Break duration should be numeric and in mins', 600, 160,'Error Message');")
                Exit Sub
            End If
        End If

        If ViewState("Mode") = "NEW" Then
            Dim insertedRow As Data.DataRow = CType(ViewState("dtClsmeetings"), DataTable).NewRow
            insertedRow("ClsSectMeetingId") = Guid.NewGuid.ToString()
            insertedRow("PeriodDescrip") = TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedItem.Text
            insertedRow("PeriodId") = TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue
            insertedRow("AltPeriodDescrip") = TryCast(editedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedItem.Text
            insertedRow("AltPeriodId") = TryCast(editedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue
            insertedRow("RoomDescrip") = TryCast(editedItem("ContainerRooms").Controls(1), DropDownList).SelectedItem.Text
            insertedRow("RoomId") = TryCast(editedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue
            insertedRow("StartDate") = TryCast(editedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate
            insertedRow("EndDate") = TryCast(editedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate
            If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                insertedRow("InstructionTypeDescrip") = TryCast(editedItem("ContainerInstruction").Controls(1), DropDownList).SelectedItem.Text
                insertedRow("InstructionTypeId") = TryCast(editedItem("ContainerInstruction").Controls(1), DropDownList).SelectedValue
            Else
                insertedRow("InstructionTypeDescrip") = ViewState("DefaultInstTypeDescrip")
                insertedRow("InstructionTypeId") = ViewState("DefaultInstTypeId")
            End If
            insertedRow("AttUsedCnt") = 0
            If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
                insertedRow("BreakDuration") = 0
            Else
                If Not TryCast(editedItem("ContainerBreakDuration").Controls(1), TextBox).Text.ToString = "" Then
                    insertedRow("BreakDuration") = CDec(TryCast(editedItem("ContainerBreakDuration").Controls(1), TextBox).Text)
                Else
                    insertedRow("BreakDuration") = 0
                End If

            End If

            'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
            insertedRow("CmdType") = 1
            CType(ViewState("dtClsmeetings"), DataTable).Rows.InsertAt(insertedRow, 0)
            CType(ViewState("dtClsmeetings"), DataTable).AcceptChanges()

            Dim ClsSectMeetingId1 As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("ClsSectMeetingId").ToString()
            Dim deletedRow() As Data.DataRow
            deletedRow = CType(ViewState("dtClsmeetings"), DataTable).Select("ClsSectMeetingId='" + ClsSectMeetingId1 + "'")
            'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
            deletedRow(0)("CmdType") = 3

            e.Canceled = True
            RadGrdClassMeetings.MasterTableView.IsItemInserted = False
            RadGrdClassMeetings.EditIndexes.Clear()
            RadGrdClassMeetings.Rebind()
            ViewState("Mode") = "NEW"
            Exit Sub
        End If

        Dim upDatedRow() As Data.DataRow
        upDatedRow = CType(ViewState("dtClsmeetings"), DataTable).Select("ClsSectMeetingId='" + ClsSectMeetingId + "'")
        upDatedRow(0)("PeriodDescrip") = TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedItem.Text
        upDatedRow(0)("PeriodId") = TryCast(editedItem("ContainerPeriod").Controls(1), DropDownList).SelectedValue
        Try
            upDatedRow(0)("AltPeriodDescrip") = TryCast(editedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedItem.Text
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            upDatedRow(0)("AltPeriodDescrip") = ""
        End Try
        Try
            upDatedRow(0)("AltPeriodId") = TryCast(editedItem("ContainerAltPeriod").Controls(1), DropDownList).SelectedValue
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            upDatedRow(0)("AltPeriodId") = Guid.Empty.ToString
        End Try
        upDatedRow(0)("RoomDescrip") = TryCast(editedItem("ContainerRooms").Controls(1), DropDownList).SelectedItem.Text
        upDatedRow(0)("RoomId") = TryCast(editedItem("ContainerRooms").Controls(1), DropDownList).SelectedValue
        upDatedRow(0)("StartDate") = TryCast(editedItem("ContainerStartDate").Controls(1), RadDatePicker).DbSelectedDate
        upDatedRow(0)("EndDate") = TryCast(editedItem("ContainerEndDate").Controls(1), RadDatePicker).DbSelectedDate

        If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
            upDatedRow(0)("BreakDuration") = 0
        Else

            If Not TryCast(editedItem("ContainerBreakDuration").Controls(1), TextBox).Text.ToString = "" Then
                upDatedRow(0)("BreakDuration") = CDec(TryCast(editedItem("ContainerBreakDuration").Controls(1), TextBox).Text)
            Else
                upDatedRow(0)("BreakDuration") = 0
            End If
        End If


        If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
            upDatedRow(0)("InstructionTypeDescrip") = TryCast(editedItem("ContainerInstruction").Controls(1), DropDownList).SelectedItem.Text
            upDatedRow(0)("InstructionTypeId") = TryCast(editedItem("ContainerInstruction").Controls(1), DropDownList).SelectedValue
        Else
            upDatedRow(0)("InstructionTypeDescrip") = ViewState("DefaultInstTypeDescrip")
            upDatedRow(0)("InstructionTypeId") = ViewState("DefaultInstTypeId")
        End If

        upDatedRow(0)("CmdType") = TryCast(editedItem("ContainerHidden").Controls(1), Label).Text
        'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
        If upDatedRow(0)("CmdType") <> "1" Then upDatedRow(0)("CmdType") = 2
        e.Canceled = True
        RadGrdClassMeetings.MasterTableView.ClearEditItems()
        RadGrdClassMeetings.Rebind()

    End Sub
    Protected Sub CustomValidator1_ServerValidate(ByVal [source] As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If CDate(args.Value.ToString) < CDate(txtStartDate.SelectedDate) Or CDate(args.Value.ToString) > CDate(txtEndDate.SelectedDate) Then
            args.IsValid = False
        End If
    End Sub
    Public Enum editModes
        normal
        edit
        insert
    End Enum
    Public Property EditMode() As editModes
        Get
            If Session("editMode") IsNot Nothing Then
                Return DirectCast(Session("editMode"), editModes)
            Else
                Return editModes.normal
            End If

        End Get
        Set(ByVal value As editModes)
            Session("editMode") = value
        End Set
    End Property
    Protected Sub dtlClsSectList_SortCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridSortCommandEventArgs) Handles dtlClsSectList.SortCommand
        PopulateDataList("Build")
        'dtlClsSectList.DataSource = CType(Cache("myClassSections"), DataView)
        'dtlClsSectList.DataBind()

    End Sub
    Private Function GetTable() As DataTable
        Dim tbl As New DataTable
        tbl.Columns.Add(New DataColumn("ClsSectMeetingId", GetType(String)))
        tbl.Columns.Add(New DataColumn("PeriodDescrip", GetType(String)))
        tbl.Columns.Add(New DataColumn("PeriodId", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("AltPeriodDescrip", GetType(String)))
        tbl.Columns.Add(New DataColumn("AltPeriodId", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("RoomDescrip", GetType(String)))
        tbl.Columns.Add(New DataColumn("RoomId", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("StartDate", GetType(Date)))
        tbl.Columns.Add(New DataColumn("EndDate", GetType(Date)))
        tbl.Columns.Add(New DataColumn("InstructionTypeDescrip", GetType(String)))
        tbl.Columns.Add(New DataColumn("InstructionTypeId", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("CmdType", GetType(Integer)))
        tbl.Columns.Add(New DataColumn("AttUsedCnt", GetType(Integer)))
        tbl.Columns.Add(New DataColumn("BreakDuration", GetType(Integer)))
        Return tbl
    End Function
    Protected Sub ddlInstructorId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInstructorId.SelectedIndexChanged
        If ViewState("Mode") = "EDIT" And ddlInstructorId.SelectedValue <> hdnInstructor.Text And hdnGrdWeights.Text <> "00000000-0000-0000-0000-000000000000" Then
            'DisplayErrorMessage("A grade book weights has been applied already,changing the instructor will remove the weights for this class section.")

            lblErrMsg.Text = "A grade book weights has been applied already,changing the instructor will remove the weights for this class section."



        End If
    End Sub
    Protected Sub ShowScheduleConflict(ByVal ClsSectId As String, ByVal StuEnrollId As String, ByVal Classes As String, _
                                       ByVal MeetingStartDate As String, _
                                       ByVal MeetingEndDate As String, _
                                       ByVal RoomId As String, _
                                       ByVal PeriodId As String, _
                                       Optional ByVal buttonState As String = "")
        Dim studentId As String = ""
        Dim strURL As String = "ViewScheduleConflicts.aspx?resid=909&mod=AR" + "&ClsSectionId=" + ClsSectId.ToString + "&StuEnrollId=" + StuEnrollId + "&Student=" + studentId + "&buttonState=" + buttonState + "&Classes=" + Server.UrlEncode(Classes) _
                               + "&MeetingStartDate=" + MeetingStartDate + "&MeetingEndDate=" + MeetingEndDate + "&RoomId=" + RoomId + "&PeriodId=" + PeriodId _
                               + "&TermId=" + ddlTermId.SelectedValue.ToString + "&CourseId=" + ddlReqId.SelectedValue.ToString + "&InstructorId=" + ddlInstructorId.SelectedValue.ToString + "&CampusId=" + ddlCampus.SelectedValue.ToString
        ScheduleConflictWindow.Visible = True
        ScheduleConflictWindow.Windows(0).NavigateUrl = strURL
        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = True
    End Sub

    Protected Sub chkAllowEarlyIn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllowEarlyIn.CheckedChanged
        HideShowTimeClockFeatures()
    End Sub

    Protected Sub chkAllowLateOut_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllowLateOut.CheckedChanged
        HideShowTimeClockFeatures()
    End Sub

    Protected Sub chkCheckTardyIn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCheckTardyIn.CheckedChanged
        HideShowTimeClockFeatures()
    End Sub

    Private Sub HideShowTimeClockFeatures()
        If MyAdvAppSettings.AppSettings("TimeClockClassSection") = "Yes" Then
            If chkAllowEarlyIn.Checked = True Or chkAllowLateOut.Checked = True Then
                chkAllowExtraHours.Enabled = True
            Else
                chkAllowExtraHours.Checked = False
                chkAllowExtraHours.Enabled = False
            End If
            If chkCheckTardyIn.Checked = True Then
                radMaxInBeforeTardy.Enabled = True
                radAssignTardyInTime.Enabled = True
                rqdMaxTardy.Enabled = True
                rqdAssignTardy.Enabled = True
            Else
                radMaxInBeforeTardy.SelectedDate = Nothing
                radAssignTardyInTime.SelectedDate = Nothing
                radMaxInBeforeTardy.DateInput.Text = Nothing
                radAssignTardyInTime.DateInput.Text = Nothing
                radMaxInBeforeTardy.Enabled = False
                radAssignTardyInTime.Enabled = False
                rqdMaxTardy.Enabled = False
                rqdAssignTardy.Enabled = False
            End If
        End If
    End Sub

    Protected Sub radMaxInBeforeTardy_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles radMaxInBeforeTardy.SelectedDateChanged
        HideShowTimeClockFeatures()
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    'Protected Sub btnClone_Click(sender As Object, e As EventArgs) Handles btnClone.Click
    '    ViewState("Mode") = "NEW"
    '    'Clear the following controls as user has to enter valid values
    '    txtStartDate.Clear()
    '    txtEndDate.Clear()
    '    txtClsSection.Text = ""
    '    txtClsSectionId.Text = System.Guid.NewGuid.ToString

    '    RadGrdClassMeetings.DataSource = ViewState("dtClsmeetings")
    '    RadGrdClassMeetings.DataBind()

    '    For Each masteritem1 As GridDataItem In RadGrdClassMeetings.MasterTableView.Items
    '        Try
    '            Dim lblMeetingStartDate As Label = CType(masteritem1.FindControl("Sdate"), Label)
    '            Dim lblMeetingEndDate As Label = CType(masteritem1.FindControl("Edate"), Label)

    '            lblMeetingStartDate.Text = ""
    '            lblMeetingEndDate.Text = ""
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '        End Try
    '    Next
    'End Sub

    Protected Sub btnCloneClass_Click(sender As Object, e As EventArgs) Handles btnCloneClass.Click
        ViewState("Mode") = "NEW"
        'Clear the following controls as user has to enter valid values
        txtStartDate.Clear()
        txtEndDate.Clear()
        txtClsSection.Text = ""
        txtClsSectionId.Text = System.Guid.NewGuid.ToString

        RadGrdClassMeetings.DataSource = ViewState("dtClsmeetings")
        RadGrdClassMeetings.DataBind()

        For Each masteritem1 As GridDataItem In RadGrdClassMeetings.MasterTableView.Items
            Try
                Dim lblMeetingStartDate As Label = CType(masteritem1.FindControl("Sdate"), Label)
                Dim lblMeetingEndDate As Label = CType(masteritem1.FindControl("Edate"), Label)

                lblMeetingStartDate.Text = ""
                lblMeetingEndDate.Text = ""
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        Next
    End Sub
End Class
