Imports Fame.Common
Imports FAME.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
' ===============================================================================
'
' FAME AdvantageV1
'
' BankCodes.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Partial Class GradeDescriptionDetails
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Panel4 As System.Web.UI.WebControls.Panel
    Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator3 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lbtBankAccts As System.Web.UI.WebControls.LinkButton
    Protected errorMessage As String


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        'Dim userId As String
        '        Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim m_Context As HttpContext

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        'userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = resourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()


            '   bind an empty new BankInfo
            BindBankData(New GrdPostingsInfo)

        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        End If
        btnDelete.Enabled = False
        headerTitle.Text = Header.Title
    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub BuildDataList()
        '   bind BankCodes datalist
        With New GradesFacade
            dlstBankCodes.DataSource = .GetAllGrades(ddlGrdSystemFilterId.SelectedValue)
            dlstBankCodes.DataBind()
        End With

    End Sub
    Private Sub BuildDropDownLists()
        BuildGradeSystemDDL()
    End Sub
    Private Sub BuildGradesDDL()
        '   bind the State DDL
        Dim grades As New GradesFacade
        With ddlGrade
            .DataTextField = "Grade"
            .DataValueField = "GrdSysDetailId"
            .DataSource = grades.GetAllGrades(ddlGrdSystemId.SelectedValue)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildGradeSystemDDL()
        '   bind the State DDL
        Dim grades As New GradesFacade
        Dim ds As New DataSet
        Try
            ds = grades.GetAllGradesDescripSystems()
            With ddlGrdSystemId
                .DataTextField = "Descrip"
                .DataValueField = "GrdSystemId"
                .DataSource = ds
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            With ddlGrdSystemFilterId
                .DataTextField = "Descrip"
                .DataValueField = "GrdSystemId"
                .DataSource = ds
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            If ds.Tables(0).Rows.Count = 1 Then
                ddlGrdSystemFilterId.SelectedIndex = 1
                ddlGrdSystemId.SelectedIndex = 1
                BuildDataList()
                BuildGradesDDL()
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            With ddlGrdSystemId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            With ddlGrdSystemFilterId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Try

    End Sub
    Private Sub dlstBankCodes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstBankCodes.ItemCommand
        '   get the BankCode from the backend and display it
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        Dim strId As String = dlstBankCodes.DataKeys(e.Item.ItemIndex).ToString()

        CommonWebUtilities.RestoreItemValues(dlstBankCodes, strId)

        GetBankCodeId(strId)

        '   set Style to Selected Item
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstBankCodes, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        'InitButtonsForEdit(e.CommandArgument)
        InitButtonsForEdit()



    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component

        If ddlGrdSystemId.SelectedValue = "" Then
            DisplayErrorMessage("Please select the grade system")
            Exit Sub
        End If
        If ddlGrade.SelectedValue = "" Then
            DisplayErrorMessage("Please select the grade")
            Exit Sub
        End If
        With New GradesFacade
            .UpdateGrades(ddlGrade.SelectedValue, txtQuality.Text, txtGradeDescrip.Text)
        End With
        BuildDataList()
        CommonWebUtilities.RestoreItemValues(dlstBankCodes, txtbankid.Text)
        '   set Style to Selected Item
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstBankCodes, ddlGrade.SelectedValue, ViewState)
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new BankInfo
        BindBankData(New GrdPostingsInfo)

        '   Reset Style in the Datalist
        CommonWebUtilities.SetStyleToSelectedItem(dlstBankCodes, Guid.Empty.ToString, ViewState)

        '   initialize buttons
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlstBankCodes, Guid.Empty.ToString)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    'Private Sub InitButtonsForEdit(ByVal bankId As String)
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
    End Sub
    Private Sub BindBankData(ByVal grd As GrdPostingsInfo)
        With grd
            chkisindb.Checked = .IsInDB
            txtgradedescrip.Text = .GradeDescription
            txtquality.Text = .GradeQuality
            ddlgrdsystemid.SelectedValue = ddlgrdsystemfilterid.SelectedValue
            If ddlgrdsystemid.SelectedValue <> "" Then
                BuildGradesDDL()
                ddlgrade.SelectedValue = .GrdSysDetailId
            End If
        End With
    End Sub
    Private Sub GetBankCodeId(ByVal bankCodeId As String)
        '   Create a StudentsFacade Instance
        Dim saf As New GradesFacade

        '   bind BankCode properties
        BindBankData(saf.GetGradesInfo(bankCodeId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        customvalidator1.ErrorMessage = errorMessage
        customvalidator1.IsValid = False

        If validationsummary1.ShowMessageBox = True And validationsummary1.ShowSummary = False And customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Protected Sub ddlGrdSystemId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlgrdsystemid.SelectedIndexChanged
        If Not ddlgrdsystemid.SelectedValue = "" Then
            BuildGradesDDL()
        End If
    End Sub
    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfilter.Click
        If Not ddlgrdsystemfilterid.SelectedValue = "" Then
            BuildDataList()
        End If
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BindToolTip()
    End Sub

    Protected Sub btndelete_Click(sender As Object, e As System.EventArgs) Handles btndelete.Click
        CommonWebUtilities.RestoreItemValues(dlstBankCodes, Guid.Empty.ToString)
    End Sub
End Class

