﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TransferGradesAlert.aspx.vb" Inherits="AR_TransferGradesAlert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Transfer Grades</title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <script language="javascript" type="text/javascript">
        function GetRadWindow()
        {
            var oWindow = null;
            if (window.radWindow)
                oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog       
            else if (window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow; //IE (and Moz as well)       
            return oWindow;
        }
        function Close()
        {
            GetRadWindow().Close();
        }

    </script>
</head>
<body runat="server" id="Body1" name="Body1" style="text-align: left;">
    <form id="form1" method="post" runat="server">
        <div>
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            </telerik:RadScriptManager>
            <telerik:RadAjaxManager ID="ramTransferGrades" runat="server" EnableAJAX="true">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="rgTransferGrades">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="pnlRHS" LoadingPanelID="ralpTransferGrades" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadAjaxLoadingPanel ID="ralpTransferGrades" runat="server" >
            </telerik:RadAjaxLoadingPanel>
            <table>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblMessage" runat="server" CssClass="LabelBold" Style="text-align: left; color: red"></asp:Label>
                    </td>
                    <td valign="top" align="right">
                        <telerik:RadButton ID="radClose" runat="server" Text="Close"  Visible="False" Style="font-size: 9px;">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
            <p></p>
            <div id="divScripString" runat="server" style="padding-left: 10px; width: 100%;">
                <!-- begin table content-->

                <!--end table content-->
            </div>
        </div>
        <asp:Label ID="Label1" runat="server" Width="410px"></asp:Label>
    </form>
</body>
</html>

