
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.Collections


Partial Class CourseBooks
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected campusId As String
    Private pObj As New UserPagePermissionInfo
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Set the Delete Button so it prompts the user for confirmation when clicked
        'Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        '        Dim startDate As DateTime
        ' Dim endDate As DateTime
        Dim facade As New CourseBkFacade
        Dim tbl1 As DataTable
        Dim tbl2 As DataTable

        Dim courseId As String
        Dim userId As String
        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        Try
            If Not Page.IsPostBack Then

                Dim course As String

                'StuEnrollId
                If Request.QueryString("CourseId") <> "" Then
                    courseId = Request.QueryString("CourseId")
                    ViewState("CourseId") = courseId
                    txtCourseId.Text = courseId
                End If

                'StuEnrollId
                If Request.QueryString("Course") <> "" Then
                    course = Request.QueryString("Course")
                    ViewState("Course") = course
                    txtCourseDescrip.Text = course
                End If


                'Fill dataset w/ coursebook information.
                ds = facade.LoadDataSet(txtCourseId.Text)
                tbl1 = ds.Tables("AvailBks")

                'Bind the datatable to the lbxBillingCodes Listbox.
                lbxAvailBooks.DataSource = tbl1
                lbxAvailBooks.DataTextField = "BkTitle"
                lbxAvailBooks.DataValueField = "BkId"
                lbxAvailBooks.DataBind()

                'startDate = startDate.Now
                'endDate = endDate.Now

                tbl2 = ds.Tables("SelectBks")

                'Bind the datatable to the "Selected Courses/Coursegrps" Listbox.
                lbxSelectBooks.DataSource = tbl2
                lbxSelectBooks.DataTextField = "BkTitle"
                lbxSelectBooks.DataValueField = "BkId"
                lbxSelectBooks.DataBind()

                Session("WorkingDS") = ds
            End If
            If pObj.HasFull Or pObj.HasEdit Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim i As Integer
        'Dim strGuid As String
        'Dim Description As String
        'Dim Code As String
        'Dim sb As New System.Text.StringBuilder
        Dim dt2 As DataTable = new DataTable()
        Dim dt1 As DataTable
        Dim dr As DataRow
        'Dim objCommon As New CommonUtilities
        Dim courseId As String
        'Dim ds1 As New DataSet
        'Dim objListGen As New DataListGenerator
        'Dim sw As New System.IO.StringWriter
        'Dim dv2 As New DataView
        'Dim strValue As String
        'Dim intPos As Integer
        Dim ds As DataSet
        'Dim prgVerId As String
        'Dim prgverhrs As Integer
        'Dim prgvercrds As Integer
        'Dim childhrs As Integer
        'Dim childcrds As Integer
        'Dim BR As ProgBL
        Dim facade As New CourseBkFacade
        Dim courseBkObj As New CourseBkInfo


        courseId = txtCourseId.Text

        Try

            'Pull the dataset from Session.
            ds = CType(Session("WorkingDS"), DataSet)

            ' CODE BELOW HANDLES AN INSERT/UPDATE INTO THE TABLE(S)

            'Run getchanges method on the dataset to see which rows have changed.
            'This section handles the changes to the Advantage Required fields
            dt1 = ds.Tables("SelectBks")
            If Not IsNothing(dt1) Then
                dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted Or DataRowState.Modified)
            End If

            If Not IsNothing(dt2) Then
                For Each dr In dt2.Rows
                    If dr.RowState = DataRowState.Added Then
                        Try

                            courseBkObj.BookId = dr("BkId").ToString
                            'CourseBkObj.StartDate = dr("StartDate")
                            'CourseBkObj.EndDate = dr("EndDate")
                            If (dr("IsReq") = True) Then
                                courseBkObj.IsReq = 1
                            Else
                                courseBkObj.IsReq = 0
                            End If
                            facade.InsertCourseBkInfo(courseBkObj, courseId)

                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            'Throw New BaseException(ex.InnerException.Message)
                            DisplayErrorMessage(ex.InnerException.Message)
                        End Try
                    ElseIf dr.RowState = DataRowState.Deleted Then
                        courseBkObj.BookId = dr("BkId", DataRowVersion.Original).ToString

                        facade.DeleteCourseBkInfo(courseBkObj, courseId)

                    ElseIf dr.RowState = DataRowState.Modified Then
                        courseBkObj.BookId = dr("BkId").ToString
                        'CourseBkObj.StartDate = dr("StartDate")
                        'CourseBkObj.EndDate = dr("EndDate")
                        If (dr("IsReq") = True) Then
                            courseBkObj.IsReq = 1
                        Else
                            courseBkObj.IsReq = 0
                        End If
                        facade.UpdateCourseBkInfo(courseBkObj, courseId)
                    End If
                Next
            End If
            If Not IsNothing(dt1) Then
                dt1.AcceptChanges()
            End If

            Session("FldsSelected") = dt1
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub


    Private Sub lbxAvailBooks_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles lbxAvailBooks.SelectedIndexChanged
        btnAdd.Enabled = True
        btnRemove.Enabled = False
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim item As ListItem
        '  Dim SelIndex As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim dtAdd As New DataTable
        Dim addAll As Boolean
        Dim index As Integer
        Dim dr2 As DataRow
        'Dim availcount As Integer = 0
        'Dim selectcount As Integer = 0
        'Dim maxCount As Integer = 0
        'Dim RemainCount As Integer = 0
        Dim availStdTable As New DataTable
        Dim availRow As DataRow
        Dim i As Integer = 0

        'Add necessary columns to the dtRemove datatable
        Dim col1 As DataColumn = dtAdd.Columns.Add("BkId")
        Dim col2 As DataColumn = dtAdd.Columns.Add("BookName")
        Dim col3 As DataColumn = dtAdd.Columns.Add("IsReq")

        For Each item In lbxAvailBooks.Items

            If item.Selected Then

                'Get the dataset from session
                ds = CType(Session("WorkingDS"), DataSet)
                'retrieve the datatable from the ds
                dt = ds.Tables("SelectBks")

                availStdTable = ds.Tables("AvailBks")

                dr = dt.NewRow()
                dr("BkId") = item.Value
                '         dr("CourseId") = txtCourseId.Text
                'dr("StartDate") = txtStartDate.Text
                'dr("EndDate") = txtEndDate.Text
                If (chkIsReq.Checked) Then
                    dr("IsReq") = True
                Else
                    dr("IsReq") = False
                End If
                dt.Rows.Add(dr)

                'Save the dataset to session
                Session("WorkingDS") = ds


                'Okay to remove the item so add it to the dtAdd DataTable.
                dr2 = dtAdd.NewRow()
                dr2("BkId") = item.Value
                dr2("BookName") = item.Text

                dtAdd.Rows.Add(dr2)

                'Increment the count of students allowed for this clssection by 1.
                'maxCount += 1

            End If
        Next

        'Remember that we cannot simply clear the the selected items in the available lbx. 
        'If the number of rows in the dtAdd datatable is the same as the count of
        'items in the selected list box then it means that all the items should be removed
        'and we can therefore use clear. If not we need to loop through the rows in
        'the dtAdd datatable. we will remove the corresponding row in the selected list box. 
        If dtAdd.Rows.Count = lbxAvailBooks.Items.Count Then
            lbxAvailBooks.Items.Clear()
            addAll = True
        End If

        For Each dr2 In dtAdd.Rows

            'Add the item to the selected list box

            lbxSelectBooks.Items.Add(New ListItem(CType(dr2("BookName"), String), CType(dr2("BkId"), String)))

            'If AddAll is set to true then we don't need to remove the items from the selected list box.
            'They were already removed above. If not we will need to find the item and then remove it.
            If addAll = False Then
                For i = 0 To lbxAvailBooks.Items.Count - 1
                    If dr2("BkId").ToString() = lbxAvailBooks.Items(i).Value.ToString Then
                        index = i
                    End If
                Next
                'Remove the item from the available list box.
                lbxAvailBooks.Items.RemoveAt(index)

                chkIsReq.Checked = False

                'Find the row that contains the ClsSectId.
                availRow = availStdTable.Rows.Find(dr2("BkId").ToString)

                If Not (availRow Is Nothing) Then
                    availStdTable.Rows.Remove(availRow)
                End If

            End If

        Next
        'Save the dataset to session
        Session("WorkingDS") = ds
        btnAdd.Enabled = False
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        'Dim SelIndex As Integer
        'Dim iChildId As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim item As ListItem
        Dim i As Integer
        '  Dim errorMessage As String
        Dim dtRemove As New DataTable
        Dim removeAll As Boolean
        Dim index As Integer
        Dim dr2 As DataRow
        Dim availStdTable As DataTable = new DataTable()
        Dim availRow As DataRow

        'Add necessary columns to the dtRemove datatable
        Dim col1 As DataColumn = dtRemove.Columns.Add("BkId")
        Dim col2 As DataColumn = dtRemove.Columns.Add("BookName")

        For Each item In lbxSelectBooks.Items

            If item.Selected Then
                'Add selected item to the lbxAvailCourses list. In order to do this we
                'have to use the FldsSelectCamps to find out the 'FldId for the selected item.
                'Remember that the item has a "-R" or "-E" attached to it so we have to find where the " - "
                'starts inorder to remove it.
                'lbxAvailBooks.Items.Add(New ListItem(lbxSelectBooks.SelectedItem.Text, lbxSelectBooks.SelectedItem.Value))
                'Get the dataset from session
                ds = CType(Session("WorkingDS"), DataSet)
                'retrieve the datatable from the ds
                dt = ds.Tables("SelectBks")

                availStdTable = ds.Tables("AvailBks")

                dr = dt.Rows.Find(item.Value)

                'Okay to remove the item so add it to the dtRemove DataTable.
                dr2 = dtRemove.NewRow()
                dr2("BkId") = item.Value
                dr2("BookName") = item.Text
                dtRemove.Rows.Add(dr2)

                If dr.RowState <> DataRowState.Added Then
                    'Mark the row as deleted
                    dr.Delete()
                Else
                    dt.Rows.Remove(dr)
                End If
                'Save the dataset to session
                Session("WorkingDS") = ds
            End If
          
        Next

        'Remember that we cannot simply clear the the selected items. If the number of rows in the dtRemove
        'datatable is the same as the count of items in the selected list box then it means that all the
        'items should be removed and we can therefore use clear. If not we need to loop through the rows in
        'the dtRemove datatable. we will remove the corresponding row in the selected list box. 
        If dtRemove.Rows.Count = lbxSelectBooks.Items.Count Then
            lbxSelectBooks.Items.Clear()
            removeAll = True
        End If

        For Each dr2 In dtRemove.Rows
            'Add the item to the available list box
            lbxAvailBooks.Items.Add(New ListItem(CType(dr2("BookName"), String), CType(dr2("BkId"), String)))

            'If removeAll is set to true then we don't need to remove the items from the selected list box.
            'They were already removed above. If not we will need to find the item and then remove it.
            If removeAll = False Then
                For i = 0 To lbxSelectBooks.Items.Count - 1
                    If dr2("BkId").ToString() = lbxSelectBooks.Items(i).Value.ToString Then
                        index = i
                    End If
                Next

                'Remove the item from the selected list box.
                lbxSelectBooks.Items.RemoveAt(index)


                chkIsReq.Checked = False
                'Add this student back to the available student datatable in order to keep it in sync.
                availRow = availStdTable.NewRow()
                availRow("BkId") = dr2("BkId")
                availRow("BkTitle") = dr2("BookName")
                availStdTable.Rows.Add(availRow)

            End If

        Next

        'Save the dataset to session
        Session("WorkingDS") = ds
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim ds As DataSet
        Dim dt As DataTable
        Dim sBookId As String
        Dim row As DataRow
        'Dim dr As DataRow
        'Dim sDate As DateTime
        'Dim eDate As DateTime
        Dim isReq As Boolean

        chkIsReq.Visible = True
        chkIsReq.Enabled = True

        'disable the add and remove buttons.
        btnAdd.Enabled = False
        btnRemove.Enabled = False

        'Populate these fields.
        'Get the dataset from session
        ds = CType(Session("WorkingDS"), DataSet)
        'retrieve the datatable from the ds
        dt = ds.Tables("SelectBks")
        '*******************************************************************

        sBookId = lbxSelectBooks.SelectedItem.Value.ToString

        row = dt.Rows.Find(sBookId)

        'isReq = ViewState("isReq")
        isReq = CType(row("IsReq"), Boolean)
        ViewState("isReq") = isReq
        If (isReq) Then
            chkIsReq.Checked = True
        Else
            chkIsReq.Checked = False
        End If

    End Sub


    Private Sub lbxSelectBooks_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles lbxSelectBooks.SelectedIndexChanged

        chkIsReq.Checked = False

        'Enable the remove button
        btnRemove.Enabled = True

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender 
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList() 
        'add save button 
        controlsToIgnore.Add(btnSave)
       'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(CType(ctrl, Panel))
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(CType(ctrl, DataGrid))
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub
  
End Class
