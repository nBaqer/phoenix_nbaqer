Imports Fame.common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class AR_StudentAttendanceFromConversion
    Inherits BasePage

    Protected studentId As String
    Private pObj As New UserPagePermissionInfo

    Protected LeadId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    Protected state As AdvantageSessionState
    Protected campusId As String
    Protected userId As String
    Protected boolSwitchCampus As Boolean = False


    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim objCommon As New CommonUtilities
        Dim fac As New UserSecurityFacade

        resourceId = HttpContext.Current.Items("ResourceId")
        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = AdvantageSession.UserState.UserId.ToString


        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(530) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            studentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With


        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''


        pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        'This is for testing purposes only.
        'studentId = CType("{E242A5C6-0C89-4AF5-94EC-A40B0A155680}", String)

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            '   build dropdownlists
            BuildDropDownLists()

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        End If

    End Sub

    Private Sub BuildDropDownLists()
        BuildStudentEnrollmentsDDL(studentId)

    End Sub

    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade

        With ddlStuEnrollId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()
            If Not .Items.Count = 1 Then
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            End If
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        'Get DataTable with attendance for the selected enrollment
        Dim fac As New ConversionAttendanceFacade

        dgrdStudentAttendance.DataSource = fac.GetEnrollmentAttendance(ddlStuEnrollId.SelectedItem.Value, txtStartDate.Text, txtEndDate.Text)
        dgrdStudentAttendance.DataBind()

        'Get the total hours for the date range selected
        lblHours.Text = "Total Hours for Date Range: " + CStr(fac.GetEnrollmentTotalAttendanceHours(ddlStuEnrollId.SelectedItem.Value, txtStartDate.Text, txtEndDate.Text))

    End Sub
End Class
