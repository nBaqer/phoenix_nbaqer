﻿
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports FAME.ExceptionLayer

Partial Class OverridePreReq
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo

    Protected WithEvents Label5 As Label
    Protected WithEvents pnlRHS As Panel
    Protected CampusId As String
    Protected UserId As String

    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim userId As String
        Dim resourceId As Integer
        '  Dim m_Context As HttpContext

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        CampusId = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As User = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not Page.IsPostBack Then
            ViewState("SelCourses") = Nothing
            ViewState("CoursesAlreadyTaken") = Nothing

            InitButtonsForLoad()
        End If

    End Sub

    Private Sub btnGetCourses_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetCourses.Click
        Dim facade As New OverridePreReqFacade
        Dim ds As DataSet

        If Session("StudentName") = "" Then
            DisplayErrorMessage("Unable to find data. Student field cannot be empty.")
            Exit Sub
        End If

        ds = facade.GetAvailSelCoursesForEnrollment(txtStuEnrollmentId.Text)

        With lbxAvailCourses
            .DataSource = ds.Tables("AvailCourses")
            .DataTextField = "CodeDescrip"
            .DataValueField = "ReqId"
            .DataBind()
        End With

        BindSelCoursesListbox(ds.Tables("SelCourses"))

        ViewState("SelCourses") = ds.Tables("SelCourses")
        ViewState("CoursesAlreadyTaken") = ds.Tables("CoursesAlreadyTaken")

        'Set buttons status
        InitButtonsForEdit()
    End Sub

    'Private Sub lbtSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtSearch.Click

    '    'Clear both AvailCourses and SelCourses listboxes.
    '    lbxAvailCourses.Items.Clear()
    '    lbxSelCourses.Items.Clear()

    '    'Disable all buttons on page
    '    InitButtonsForLoad()

    '    '   setup the properties of the new window
    '    Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px"
    '    Dim name As String = "StudentPopupSearch"
    '    Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx?resid=293&mod=AR&cmpid=" + campusId

    '    '   build list of parameters to be passed to the new window
    '    Dim parametersArray(6) As String

    '    '   set StudentName
    '    parametersArray(0) = txtStudentName.ClientID

    '    '   set StuEnrollment
    '    parametersArray(1) = txtStuEnrollmentId.ClientID
    '    parametersArray(2) = txtStuEnrollment.ClientID

    '    '   set Terms
    '    parametersArray(3) = txtTermId.ClientID
    '    parametersArray(4) = txtTerm.ClientID

    '    '   set AcademicYears
    '    parametersArray(5) = txtAcademicYearId.ClientID
    '    parametersArray(6) = txtAcademicYear.ClientID

    '    '   open new window and pass parameters
    '    CommonWebUtilities.OpenWindowAndPassParameters(Page, url, name, winSettings, parametersArray)
    'End Sub
    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, _
                          ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, _
                          ByVal hourtype As String)


        txtStuEnrollmentId.Text = enrollid
        'txtStudentName.Text = fullname
        'txtTermId.Text = termid
        'txtTerm.Text = termdescrip
        'txtAcademicYearId.Text = academicyearid
        'txtAcademicYear.Text = academicyeardescrip

        Session("StuEnrollment") = txtStuEnrollmentId.Text
        Session("StudentName") = fullname
        'Session("TermId") = txtTermId.Text
        'Session("Term") = txtTerm.Text
        'Session("AcademicYearID") = txtAcademicYearId.Text
        'Session("AcademicYear") = txtAcademicYear.Text
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim dt As DataTable = DirectCast(ViewState("SelCourses"), DataTable)
        Dim dtChanges As DataTable = Nothing
        Dim stuEnrollId As String = txtStuEnrollmentId.Text
        Dim facade As New OverridePreReqFacade
        Dim errMsg As String = ""

        Try
            'Run getChanges on SelCourses table to get rows that have changed.
            If Not IsNothing(dt) Then
                dtChanges = dt.GetChanges(DataRowState.Added Or DataRowState.Deleted)
            End If

            If Not IsNothing(dtChanges) Then
                For Each dr As DataRow In dtChanges.Rows
                    If dr.RowState = DataRowState.Added Then
                        Try
                            errMsg = facade.AddPreReqOverride(stuEnrollId, dr("ReqId").ToString, AdvantageSession.UserState.UserName)

                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Throw New BaseException(ex.InnerException.Message)
                        End Try

                    ElseIf dr.RowState = DataRowState.Deleted Then
                        Try
                            errMsg = facade.DeletePreReqOverride(stuEnrollId, dr("ReqId", DataRowVersion.Original).ToString)

                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Throw New BaseException(ex.InnerException.Message)
                        End Try
                    End If

                    If errMsg <> "" Then
                        DisplayErrorMessage(errMsg)
                        Exit Sub
                    End If
                Next
            End If

            If Not IsNothing(dt) Then
                dt.AcceptChanges()
                'Rebind listbox with values
                BindSelCoursesListbox(dt)
            End If

            ViewState("SelCourses") = dt

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub BindSelCoursesListbox(ByVal dt As DataTable)
        With lbxSelCourses
            .DataSource = dt
            .DataTextField = "CodeDescrip"
            .DataValueField = "ReqId"
            .DataBind()
        End With
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click

        If lbxAvailCourses.Items.Count = 0 Then
            DisplayErrorMessage("Unable to add course. There is no Available Courses on list.")

        ElseIf lbxAvailCourses.SelectedIndex = -1 Then
            DisplayErrorMessage("Please select a course from the Available Courses list.")

        ElseIf lbxAvailCourses.Items.Count > 0 And lbxAvailCourses.SelectedIndex <> -1 Then
            'Add selected item on AvailCourses listbox to SelCourses listbox. 
            Dim dt As DataTable = DirectCast(ViewState("SelCourses"), DataTable)
            Dim dr As DataRow = dt.NewRow()
            Dim reqId As String = lbxAvailCourses.SelectedItem.Value.ToString
            dr("ReqId") = reqId
            dr("CodeDescrip") = lbxAvailCourses.SelectedItem.Text
            dt.Rows.Add(dr)

            ViewState("SelCourses") = dt

            'Create a dataview sorted by course description
            Dim dv As New DataView(dt, "", "CodeDescrip ASC", DataViewRowState.CurrentRows)

            With lbxSelCourses
                .DataSource = dv
                .DataTextField = "CodeDescrip"
                .DataValueField = "ReqId"
                .DataBind()
            End With

            'Remove selected item from lbxAvailCourses listbox
            Dim selIndex As Integer = lbxAvailCourses.SelectedIndex
            lbxAvailCourses.Items.RemoveAt(selIndex)
        End If
    End Sub

    Private Sub btnAddAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddAll.Click
        Dim dt As DataTable = DirectCast(ViewState("SelCourses"), DataTable)
        Dim dr As DataRow

        If lbxAvailCourses.Items.Count = 0 Then
            DisplayErrorMessage("Unable to add courses. There is no Available Courses on list.")
            Exit Sub
        End If

        'Loop thru AvailCourses listbox and add all the entries to SelCourses listbox.s
        For i As Integer = 0 To lbxAvailCourses.Items.Count - 1
            dr = dt.NewRow()
            dr("ReqId") = lbxAvailCourses.Items(i).Value
            dr("CodeDescrip") = lbxAvailCourses.Items(i).Text
            dt.Rows.Add(dr)
            lbxSelCourses.Items.Add(New ListItem(lbxAvailCourses.Items(i).Text, lbxAvailCourses.Items(i).Value))
        Next

        ViewState("SelCourses") = dt

        'Remove all items from lbxAvailCourses listbox
        lbxAvailCourses.Items.Clear()
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click

        If lbxSelCourses.Items.Count = 0 Then
            DisplayErrorMessage("Unable to remove course. There is no Selected Courses on list.")

        ElseIf lbxSelCourses.SelectedIndex = -1 Then
            DisplayErrorMessage("Please select a course from the Selected Courses list.")

        ElseIf lbxSelCourses.Items.Count > 0 And lbxSelCourses.SelectedIndex <> -1 Then

            Dim dtTakenCourses As DataTable = DirectCast(ViewState("CoursesAlreadyTaken"), DataTable)
            Dim rows() As DataRow = dtTakenCourses.Select("ReqId = '" & lbxSelCourses.SelectedItem.Value & "'")

            If rows.GetLength(0) > 0 Then
                DisplayErrorMessage("Course could not be removed as student is either currently registered or has a grade posted already.")

            Else
                'Course has not been taken or has no grade posted, so it can be removed.

                'Add selected item on SelCourses listbox to AvailCourses listbox. 
                lbxAvailCourses.Items.Add(New ListItem(lbxSelCourses.SelectedItem.Text, lbxSelCourses.SelectedItem.Value))

                'Find selected item on SelCourses table
                Dim dt As DataTable = DirectCast(ViewState("SelCourses"), DataTable)
                Dim dr As DataRow = dt.Rows.Find(lbxSelCourses.SelectedItem.Value)

                If dr.RowState <> DataRowState.Added Then
                    'Mark the row as deleted
                    dr.Delete()
                Else
                    dt.Rows.Remove(dr)
                End If

                ViewState("SelCourses") = dt

                'Remove selected item from lbxSelCourses listbox
                Dim selIndex As Integer = lbxSelCourses.SelectedIndex
                lbxSelCourses.Items.RemoveAt(selIndex)
            End If
        End If
    End Sub

    Private Sub btnRemoveAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemoveAll.Click
        Dim dt As DataTable = DirectCast(ViewState("SelCourses"), DataTable)
        Dim dtTakenCourses As DataTable = DirectCast(ViewState("CoursesAlreadyTaken"), DataTable)
        Dim dtRemoved As New DataTable
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim i As Integer
        Dim idx As Integer
        Dim removeAll As Boolean

        If lbxSelCourses.Items.Count = 0 Then
            DisplayErrorMessage("Unable to remove courses. There is no Selected Courses on list.")
            Exit Sub
        End If

        'We cannot remove students who already have final grades posted. For each item in the selected list
        'we will check if a final grade has already been posted.If a final grade has NOT been posted it s okay
        'to remove the item and we will place it in the dtRemove datatable. We cannot remove the item while
        'we are moving through the collection so we have to use the dtRemove datatable after the loop to remove
        'any items that should be removed. 
        'If a final grade has been posted we will simply leave that item. When we are finished if there are any
        'items in the selected items, after the removal of the items that are in the dtRemove datatable we will
        'display a message that one or more students could not be removed
        'because they already have final grades posted.

        'Add necessary columns to the dtRemove datatable
        'Dim col1 As DataColumn = dtRemoved.Columns.Add("ReqId")
        'Dim col2 As DataColumn = dtRemoved.Columns.Add("CodeDescrip")

        For i = 0 To lbxSelCourses.Items.Count - 1
            Dim rows() As DataRow = dtTakenCourses.Select("ReqId = '" & lbxSelCourses.Items(i).Value & "'")

            If rows.GetLength(0) = 0 Then
                'Course has not been taken or has no grade posted, so it can be removed.
                'Add selected item into temporary table: dtRemoved
                dr2 = dtRemoved.NewRow
                dr2("ReqId") = lbxSelCourses.Items(i).Value
                dr2("CodeDescrip") = lbxSelCourses.Items(i).Text
                dtRemoved.Rows.Add(dr2)

                'Find selected item on SelCourses table
                dr = dt.Rows.Find(lbxSelCourses.Items(i).Value)

                If dr.RowState <> DataRowState.Added Then
                    'Mark the row as deleted
                    dr.Delete()
                Else
                    dt.Rows.Remove(dr)
                End If
            End If
        Next

        ViewState("SelCourses") = dt

        'Cannot simply clear SelCourses listbox. If the number of rows in the dtRemoved
        'datatable is the same as the count of items in SelCourses listbox then it means that all the
        'items must be removed;therefore, Clear() can be used. If not we need to loop through the rows in
        'the dtRemoved datatable and only remove the corresponding row in SelCourses listbox. 
        If dtRemoved.Rows.Count = lbxSelCourses.Items.Count Then
            lbxSelCourses.Items.Clear()
            removeAll = True
        End If

        For Each row As DataRow In dtRemoved.Rows
            'Add item to AvailCourses listbox
            lbxAvailCourses.Items.Add(New ListItem(CType(row("CodeDescrip"), String), CType(row("ReqId"), String)))

            'If removeAll is set to true then we don't need to remove the items from SelCourses listbox
            'because they were already removed above. If not we will need to find the item and then remove it.
            If Not removeAll Then
                For i = 0 To lbxSelCourses.Items.Count - 1
                    If row("ReqId").ToString() = lbxSelCourses.Items(i).Value.ToString Then
                        idx = i
                    End If
                Next
                'Remove item from SelCourses listbox.
                lbxSelCourses.Items.RemoveAt(idx)
            End If
        Next

        'If there are still items remaining in SelCourses listbox we need to display a message that those
        'items could not be removed because they have final grades posted.
        If lbxSelCourses.Items.Count > 0 Then
            DisplayErrorMessage("Some courses could not be removed as they have grades posted already.")
        End If
    End Sub

    Private Sub InitButtonsForLoad()
        'There is nothing to save unless user selects from Student.
        btnSave.Enabled = False

        'These buttons will always be disabled because they don't make sense in this page.
        btnNew.Enabled = False
        btnDelete.Enabled = False

        'Disable buttons specific to this page.
        btnAdd.Enabled = False
        btnAddAll.Enabled = False
        btnRemove.Enabled = False
        btnRemoveAll.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            btnSave.Enabled = True
            '
            btnAdd.Enabled = True
            btnAddAll.Enabled = True
            btnRemove.Enabled = True
            btnRemoveAll.Enabled = True
        End If

        'These buttons will always be disabled because they don't make sense in this page.
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnGetCourses)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnAddAll)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(btnRemoveAll)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
    End Sub

End Class
