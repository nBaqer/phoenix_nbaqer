Imports FAME.Common
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections


Partial Class Rooms
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Linkbutton2 As System.Web.UI.WebControls.LinkButton


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private validatetxt As Boolean = False
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Put user code to initialize the page here
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        'Dim sessionId As String
        'sessionId = Session.SessionID

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        Dim sStatusId As String
        Dim campusId As String
        Dim userId As String
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim facade As New RoomFacade
        Dim BldgRooms As Integer
        Dim maxRooms As Integer


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'If Request.QueryString("cmpid") <> "" Then
        '    ViewState("CampusId") = Request.QueryString("cmpid")
        '    campusId = ViewState("CampusId")
        'End If
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        campusId = Guid.Empty.ToString
        If Not IsNothing(AdvantageSession.UserState.CampusId) Then
            ViewState("CampusId") = AdvantageSession.UserState.CampusId.ToString
            campusId = ViewState("CampusId")
        End If
        userId = AdvantageSession.UserState.UserId.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        Try
            If Not Page.IsPostBack Then
                'Header1.EnableHistoryButton(False)
                m_Context = HttpContext.Current
                txtResourceId.Text = CInt(m_Context.Items("ResourceId"))
                objCommon.PageSetup(Form1, "NEW")

                'objCommon.PopulatePage(Form1)
                'Disable the new and delete buttons
                'objCommon.SetBtnState(Form1, "NEW", pObj)
                ViewState("MODE") = "NEW"
                InitButtonsForLoad()

                If Request.QueryString("BldgId") <> "" Then
                    txtBldgId.Text = Request.QueryString("BldgId")
                    ViewState("Bldg") = txtBldgId.Text
                End If
                If Request.QueryString("BldgName") <> "" Then
                    txtBldgDescrip.Text = Request.QueryString("BldgName")
                    ViewState("BldgName") = txtBldgDescrip.Text
                End If
                If Request.QueryString("BldgRooms") <> "" Then
                    maxRooms = Request.QueryString("BldgRooms")
                    ViewState("maxCap") = maxRooms
                End If

                'Get Maximum number of rooms that can be setup for this building
                BldgRooms = facade.GetBldgRooms(txtBldgId.Text)

                ViewState("BldgRooms") = BldgRooms

                If BldgRooms >= maxRooms Then
                    lblNote.Text = "Cannot enter anymore rooms for this building. Maximum limit has been reached."
                End If


                'Set the text box to a new guid
                txtRoomId.Text = System.Guid.NewGuid.ToString

                'ds = objListGen.SummaryListGenerator()
                ds = facade.GetRooms(ViewState("Bldg"))

                'Generate the status id
                ds2 = objListGen.StatusIdGenerator()

                'Set up the primary key on the datatable
                ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

                Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
                sStatusId = row("StatusId").ToString()

                Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "Descrip", DataViewRowState.CurrentRows)

                With dlstRooms
                    .DataSource = dv
                    .DataBind()
                End With
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                InitButtonsForLoad()
            Else
                objCommon.PageSetup(Form1, "EDIT")
                InitButtonsForEdit()
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim msg As String
        Dim facade As New RoomFacade
        Dim BldgRooms As Integer
        Try
            If validatetxt = True Then
                Exit Sub
            End If
            If ViewState("MODE") = "NEW" Then
                objCommon.PagePK = txtRoomId.Text
                txtBldgId.Text = ViewState("Bldg")
                'Get Maximum number of rooms that can be setup for this building
                BldgRooms = facade.GetBldgRooms(txtBldgId.Text)

                If BldgRooms >= ViewState("maxCap") Then
                    lblNote.Text = "Cannot enter anymore rooms for this building. Maximum limit has been reached."
                    Exit Sub
                End If
                msg = objCommon.DoInsert(Form1)
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                'objCommon.PagePK = txtRoomId.Text
                txtRowIds.Text = objCommon.PagePK
                'ds = objListGen.SummaryListGenerator()
                'dlstRooms.SelectedIndex = -1
                ds = facade.GetRooms(ViewState("Bldg"))
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                'objCommon.SetBtnState(Form1, "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                InitButtonsForEdit()

            ElseIf viewstate("MODE") = "EDIT" Then
                txtBldgId.Text = ViewState("Bldg")
                msg = objCommon.DoUpdate(Form1)
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                'ds = objListGen.SummaryListGenerator()
                'dlstRooms.SelectedIndex = -1
                ds = facade.GetRooms(ViewState("Bldg"))
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Response.Write(objCommon.strText)
            End If
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstRooms, txtRoomId.Text, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstRooms, txtRoomId.Text)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim facade As New RoomFacade
        ' Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Try
            ClearRHS()
            'ds.ReadXml(sr)
            ds = facade.GetRooms(ViewState("Bldg"))
            PopulateDataList(ds)
            'disable new and delete buttons.
            'objcommon.SetBtnState(Form1, "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtRoomId.Text = System.Guid.NewGuid.ToString

            txtBldgDescrip.Text = ViewState("BldgName")
            'Reset Style in the Datalist
            'CommonWebUtilities.SetStyleToSelectedItem(dlstRooms, Guid.Empty.ToString, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstRooms, Guid.Empty.ToString)
            InitButtonsForLoad()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim facade As New RoomFacade
        Dim msg As String

        Try
            msg = objCommon.DoDelete(Form1)
            If msg <> "" Then
                DisplayErrorMessage(msg)
            End If
            ClearRHS()
            'ds = objListGen.SummaryListGenerator()
            'dlstRooms.SelectedIndex = -1
            ds = facade.GetRooms(ViewState("Bldg"))
            PopulateDataList(ds)
            'ds.WriteXml(sw)
            'ViewState("ds") = sw.ToString()
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            Viewstate("MODE") = "NEW"
            'Set the text box to a new guid
            txtRoomId.Text = System.Guid.NewGuid.ToString
            txtBldgDescrip.Text = ViewState("BldgName")
            InitButtonsForLoad()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstRooms_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstRooms.ItemCommand
        '        Dim selIndex As Integer
        Dim ds As New DataSet
        'Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim facade As New RoomFacade
        txtRowIds.Text = e.CommandArgument.ToString
        Try
            strGUID = dlstRooms.DataKeys(e.Item.ItemIndex).ToString()
            objCommon.PopulatePage(Form1, strGUID)
            'objCommon.SetBtnState(Form1, "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            'selIndex = e.Item.ItemIndex
            'dlstRooms.SelectedIndex = selIndex
            ds = facade.GetRooms(ViewState("Bldg"))
            'ds.ReadXml(sr)
            PopulateDataList(ds)
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstRooms, e.CommandArgument, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstRooms, e.CommandArgument)
            InitButtonsForEdit()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstCalendar_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstCalendar_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim dv2 As New DataView
        Dim objListGen As New DataListGenerator
        Dim sStatusId As String
        Dim facade As New RoomFacade

        ds2 = objListGen.StatusIdGenerator()
        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        'If (chkStatus.Checked = True) Then 'Show Active Only

        '    Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
        '    sStatusId = row("StatusId").ToString()
        '    'Create dataview which displays active records only
        '    Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "Descrip", DataViewRowState.CurrentRows)
        '    dv2 = dv
        'ElseIf (chkStatus.Checked = False) Then

        '    Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
        '    sStatusId = row("StatusId").ToString()
        '    'Create dataview which displays inactive records only
        '    Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "Descrip", DataViewRowState.CurrentRows)
        '    dv2 = dv
        'End If
        If (radStatus.SelectedItem.Text = "Active") Then   'Show Active Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays active records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "Descrip", DataViewRowState.CurrentRows)
            dv2 = dv
        ElseIf (radStatus.SelectedItem.Text = "Inactive") Then   'Show InActive Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "Descrip", DataViewRowState.CurrentRows)
            dv2 = dv
        Else  'Show All
            'ds = objListGen.SummaryListGeneratorSort()
            ds = facade.GetRooms(ViewState("Bldg"))
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows)
            dv2 = dv
        End If
        With dlstRooms
            .DataSource = dv2
            .DataBind()
        End With
        dlstRooms.SelectedIndex = -1


    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Dim facade As New RoomFacade
        Try
            ClearRHS()
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            Viewstate("MODE") = "NEW"
            'Set the text box to a new guid
            txtRoomId.Text = System.Guid.NewGuid.ToString
            txtBldgDescrip.Text = ViewState("BldgName")   'BugFix 04503
            'ds = objListGen.SummaryListGenerator()
            ds = facade.GetRooms(ViewState("Bldg"))
            PopulateDataList(ds)
            InitButtonsForLoad()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Public Sub TextValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        Dim strMinus As String = Mid(args.Value, 1, 1)
        If strMinus = "-" Then
            DisplayErrorMessage("The Value Cannot be Negative")
            validatetxt = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender 
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList() 
        'add save button 
        controlsToIgnore.Add(btnSave)
       'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
End Class
