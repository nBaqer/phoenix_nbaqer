﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentGradeBook.aspx.vb" Inherits="AdvWeb.AR.StudentGradeBook" %>
<%@ MasterType  virtualPath="~/NewSite.master" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   
   
  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>   
     
        <asp:Panel ID="panFilter" runat="server">
            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                    <br />
                        <table id="tblSearch" runat="server" cellspacing="0" cellpadding="0" width="100%"
                            align="center" border="0">
                            <tr id="trTerm" runat="server">
                                <td class="contentcellblue">
                                    <asp:Label ID="lblTerm" runat="server" CssClass="label">Term</asp:Label>
                                </td>
                                <td class="contentcell4blue">
                                    <asp:DropDownList ID="ddlTerm" runat="server" CssClass="dropdownlist" AutoPostBack="True" Width="220px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trCohortStartDate" runat="server">
                                <td class="contentcellblue">
                                    <asp:Label ID="label3" runat="server" CssClass="label">Cohort Start Date</asp:Label>
                                </td>
                                <td class="contentcell4blue">
                                    <asp:DropDownList ID="ddlcohortStDate" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="220px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblCourse" runat="server" CssClass="label">Class Section</asp:Label>
                                </td>
                                <td class="contentcell4blue">
                                    <asp:DropDownList ID="ddlClsSection" runat="server" CssClass="dropdownlist" Width="220px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    &nbsp;
                                </td>
                                <td class="contentcell4blue">
                                    <%--<asp:Button ID="btnBuildList" runat="server" CssClass="buttontopfilter" Text="Build List"
                                        CausesValidation="False"></asp:Button>--%>
                                    <telerik:RadButton ID="btnBuildList" runat="server"  Text="Build List"
                                        CausesValidation="False" ></telerik:RadButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfltr3rows">
                            <asp:DataGrid ID="dtlClsSectStds" runat="server" CssClass="datalistcontentar" GridLines="None"
                                DataKeyField="StuEnrollId" AutoGenerateColumns="False" Width="325px">
                                <EditItemStyle Wrap="False"></EditItemStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                <FooterStyle CssClass="label"></FooterStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderStyle-CssClass="datalistheaderar" ItemStyle-CssClass="datalistar"
                                        HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Linkbutton1" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                Text='<%# Container.DataItem("FullName")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="datalistheaderar" ItemStyle-CssClass="datalist"
                                        HeaderText="SSN">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                Text='<%# Container.DataItem("SSN")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="datalistheaderar" ItemStyle-CssClass="datalist"
                                        HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Linkbutton5" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                Text='<%# Container.DataItem("StudentNumber")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="datalistheaderar" ItemStyle-CssClass="datalist"
                                        HeaderText="Final Grade">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblFinalGrade" runat="server" Text="Final Grade"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Linkbutton3" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                Text='<%# Container.DataItem("Grade")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="Linkbutton4" Visible="False" runat="server" CausesValidation="False"
                                                Text='<%# Container.DataItem("IsInComplete")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid></div>
                    </td>
                </tr>
            </table>
        </asp:Panel>


    </telerik:RadPane>


    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2">
        <!-- begin content table-->

            <asp:Panel ID="pnlHeader" runat="server" Visible="False">
                <asp:Panel ID="pnlRHS" runat="server">
                    <table class="contenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                        <tr>
                            <td class="arcontentcells" nowrap>
                                <asp:Label ID="lblStd" runat="server" CssClass="label">Student</asp:Label>
                            </td>
                            <td class="arcontentcells2" nowrap>
                                <asp:Label ID="lblStdName" runat="server" CssClass="textbox"></asp:Label>
                            </td>
                            <td class="arcontentcells" nowrap>
                                <asp:Label ID="lblSection" runat="server" CssClass="label">Class Section</asp:Label>
                            </td>
                            <td class="arcontentcells2" nowrap>
                                <asp:Label ID="lblSecName" runat="server" CssClass="textbox"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="contenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                        <tr>
                            <td style="padding: 10px">
                                <asp:DataGrid ID="dgdStdGrdBk" runat="server" CssClass="datalistcontentar" AutoGenerateColumns="False"
                                    BorderColor="#E0E0E0" BorderStyle="Solid" BorderWidth="1px">
                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                    <FooterStyle CssClass="label"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Grading Criteria">
                                            <HeaderStyle CssClass="datagridheader" Width="30%"></HeaderStyle>
                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtStuEnrollId" TabIndex="1" CssClass="textbox" runat="server" Visible="False"
                                                    Text='<%# Container.DataItem("StuEnrollId") %>' />
                                                <asp:TextBox ID="txtGrdCritId" TabIndex="2" CssClass="textbox" runat="server" Visible="False"
                                                    Text='<%# Container.DataItem("InstrGrdBkWgtDetailId") %>' />
                                                <asp:TextBox ID="txtGrdBkResultId" TabIndex="2" CssClass="textbox" runat="server" Visible="False"
                                                    Text='<%# Container.DataItem("GrdBkResultId") %>' />
                                                <asp:Label ID="GrdCriteria" CssClass="ardatalistcontent" runat="server" Text='<%# Container.DataItem("Descrip") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Weight">
                                            <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeight" CssClass="ardatalistcontent" runat="server" Text='<%# Container.DataItem("Weight") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Score">
                                            <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtScore" runat="server" TabIndex="3" CssClass="textbox" Text='<%# Container.DataItem("Score") %>' />
                                                <asp:CompareValidator ID="MinValCompareValidator" runat="server" ErrorMessage="Invalid Score"
                                                    Display="None" Type="Double" Operator="LessThan" ValueToCompare="32767" ControlToValidate="txtScore">Invalid Score</asp:CompareValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Date Completed">
                                            <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            <ItemTemplate>
                                                <telerik:RadDatePicker ID="rdpDateCompleted" MinDate="1/1/1945" runat="server" AutoPostBack="false" >
                                                </telerik:RadDatePicker>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Comments">
                                            <HeaderStyle CssClass="datagridheader" Width="60%"></HeaderStyle>
                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtComments" TabIndex="4" CssClass="textbox" runat="server" Text='<%# Container.DataItem("Comments") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <table class="contenttable" cellspacing="0" cellpadding="0" width="50%" align="center"
                        border="0">
                        <tr>
                            <td class="arbuttoncells">
                                <asp:Label ID="label1" runat="server" CssClass="label">Current Score</asp:Label>
                            </td>
                            <td class="arbuttoncells2">
                                <asp:TextBox ID="txtFinalScore" TabIndex="5" runat="server" CssClass="textbox" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="arbuttoncells">
                                <asp:Label ID="label2" runat="server" CssClass="label">Current Grade</asp:Label>
                            </td>
                            <td class="arbuttoncells2">
                                <asp:TextBox ID="txtFinalGrade" TabIndex="6" runat="server" CssClass="textbox" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="contentcell" nowrap>
                            </td>
                            <td class="contentcell4">
                                <asp:CheckBox ID="chkIsIncomplete" runat="server" CssClass="label" TabIndex="7" AutoPostBack="True"
                                    Text="Incomplete Grade" Visible="false"></asp:CheckBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>

        <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
        <!-- end content table-->
        </div>
        </td>
        </tr>
        </table>

    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

