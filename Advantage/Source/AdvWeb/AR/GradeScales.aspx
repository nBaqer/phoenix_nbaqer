<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="GradeScales.aspx.vb" Inherits="GradeScales" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstGradeScales">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstGradeScales" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstGradeScales" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstGradeScales" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstGradeScales" runat="server" DataKeyField="GrdScaleId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%#  Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("GrdScaleId")%>' Text='<%# container.dataitem("Descrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table id="Table2" cellspacing="0" cellpadding="0" width="60%" border="0">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDescrip" CssClass="label" runat="server">Description</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtDescrip" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescrip" Display="None"
                                                    ErrorMessage="Description can not be blank">Description can not be blank</asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                <asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ControlToValidate="ddlCampGrpId"
                                                    Display="None" ErrorMessage="Must Select a Campus Group" ValueToCompare="00000000-0000-0000-0000-000000000000"
                                                    Operator="NotEqual">Must Select a Campus Group</asp:CompareValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblGrdSystemId" CssClass="label" runat="server">Grade System</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlGrdSystemId" runat="server" CssClass="dropdownlist" AutoPostBack="True" Width="200px"></asp:DropDownList>
                                                <asp:CompareValidator ID="GradeSystemComparevalidator" runat="server" ControlToValidate="ddlGrdSystemId"
                                                    Display="None" ErrorMessage="Must Select a Grade System" ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual">Must Select a Grade System</asp:CompareValidator></td>
                                        </tr>
                                    </table>
                                    <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables" align="center">
                                                <asp:CheckBox ID="chkTestUI" runat="server" Text="3 Columns UI (Only for Testing)" CssClass="Checkbox"
                                                    Checked="True" Visible="False"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:DataGrid ID="dgrdGradeScaleDetails" runat="server" ShowFooter="True" BorderStyle="Solid"
                                                    DataKeyField="GrdScaleDetailId" AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true"
                                                    EditItemStyle-Wrap="false" Width="100%" BorderColor="#E0E0E0" BorderWidth="1px">
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Min Value">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="25%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMinVal" Text='<%# Container.DataItem("MinVal") %>' runat="server" CssClass="label">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtMinVal" CssClass="textbox" runat="server" Columns="3" MaxLength="6" Width="150px"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditMinVal" Text='<%# Container.DataItem("MinVal") %>' CssClass="textbox" runat="server" Columns="3" MaxLength="6" Width="230px">
                                                                </asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Max Value">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="25%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMaxVal" Text='<%# Container.DataItem("MaxVal") %>' runat="server" CssClass="label">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtMaxVal" CssClass="textbox" runat="server" MaxLength="6" Width="150px"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditMaxVal" Text='<%# Container.DataItem("MaxVal") %>' CssClass="textbox" runat="server" MaxLength="6">
                                                                </asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Grade Detail">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="25%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrdSysDetailDescrip" runat="server" Text='<%# Ctype(Container.DataItem, System.Data.DataRowView).Row.GetParentRow("GradeSystemDetailsGradeScaleDetails")("Grade") %>'>
                                                                </asp:Label>
                                                                <asp:Label ID="lblGrdSysDetailId" runat="server" Text='<%# Container.DataItem("GrdSysDetailId") %>' Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lblGrdScaleDetailId" runat="server" Text='<%# Container.DataItem("GrdScaleDetailId") %>' Visible="False">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:DropDownList ID="ddlGrdSysDetailId" runat="server" CssClass="dropdownlist" Width="150px"></asp:DropDownList>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlEditGrdSysDetailId" runat="server" CssClass="dropdownlist" Width="150px"></asp:DropDownList>
                                                                <asp:Label ID="lblEditGrdSysDetailId" runat="server" Text='<%# Container.DataItem("GrdSysDetailId") %>' Visible="False" CssClass="dropdownlist">
                                                                </asp:Label>
                                                                <asp:Label ID="lblEditGrdScaleDetailId" runat="server" Text='<%# Container.DataItem("GrdScaleDetailId") %>' Visible="False" CssClass="dropdownlist">lblEditGrdScaleDetailId</asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="25%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnlButEdit" runat="server" Text="<img border=0 src=../images/im_edit.gif alt= edit>"
                                                                    CausesValidation="False" CommandName="Edit">
																		<img border="0" src="../images//im_edit.gif" alt="edit"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkButUp" runat="server" Text="<img border=0 src=../images/up.gif alt=MoveUp>"
                                                                    CausesValidation="False" CommandName="Up"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkButDown" runat="server" Text="<img border=0 src=../images/down.gif alt=MoveDown>"
                                                                    CausesValidation="False" CommandName="Down"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                            <FooterTemplate>
                                                                <div style="padding: 5px;">
                                                                    <asp:Button ID="btnAddRow" runat="server" Text="Add" CausesValidation="False"
                                                                        CommandName="AddNewRow"></asp:Button>
                                                                </div>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="lnkbutUpdate" runat="server" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                                    CommandName="Update"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkbutDelete" runat="server" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                                    CausesValidation="False" CommandName="Delete"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkbutCancel" runat="server" Text="<img border=0 src=../images/im_delete.gif alt=Cancel>"
                                                                    CausesValidation="False" CommandName="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid></td>
                                        </tr>
                                    </table>
                                </div>

                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
            ShowSummary="False"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        <!--end validation panel-->
    </div>

</asp:Content>


