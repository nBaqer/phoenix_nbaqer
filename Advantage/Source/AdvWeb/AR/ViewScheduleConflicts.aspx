﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewScheduleConflicts.aspx.vb" Inherits="AR_ViewScheduleConflicts" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Schedule Conflicts</title>
    <script language ="javascript" type ="text/javascript" >
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow)
                oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog       
            else if (window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow; //IE (and Moz as well)       
            return oWindow;
        }       
        function Close() {
            GetRadWindow().Close();
        }   
      
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
       <table>
            <tr>
                <td align="left">
                    <asp:Label ID="lblMessage" runat="server" CssClass="LabelBold" style="text-align:left; color: #b71c1c"></asp:Label>
                </td>
                <td valign="top" align="right"><telerik:RadButton ID="radClose" runat="server" Text="Close"  Visible="false" Style="font-size:9px;">
                    </telerik:RadButton>
                </td>
            </tr>
       </table>
       <p></p>
       <div style=" padding-left:10px; overflow:auto;">
      <%-- <div class="scrollwhole2">--%>
       <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="False"
                                                        AllowSorting="True" AllowPaging="True" PageSize="8" GridLines="None" 
                                                        ShowGroupPanel="false"  Width="670px" AllowMultiRowSelection="true" Visible="true" CellPadding="0" AllowFilteringByColumn="True">
                                                        <PagerStyle Mode="NextPrevAndNumeric"></PagerStyle>
                                                         <GroupingSettings CaseSensitive="false" />
                                                        <MasterTableView AllowMultiColumnSorting="True" 
                                                        GroupLoadMode="Server" Name="ClassSection" SkinID="Web20" Width="670px" CommandItemSettings-AddNewRecordText="Post new score" CommandItemDisplay="Top" 
                                                        HeaderStyle-Font-Size="10px" HeaderStyle-Font-Bold="true" HeaderStyle-VerticalAlign="Top" AllowFilteringByColumn="True"
                                                        FilterItemStyle-Font-Size="9px" TableLayout="Auto" FilterItemStyle-HorizontalAlign="Left">
                                                        
                                                           <%-- <CommandItemSettings AddNewRecordText="Add New Record"  RefreshText="View All" ShowRefreshButton="false" />--%>
                                                           <CommandItemTemplate>
                                                                <div>
                                                                    <div style="float: left; text-align: left; vertical-align:top; width:400px;height:20px;">
                                                                        <asp:Label ID="Label2" runat="server" Font-Size="12px" Font-Bold="true">Classes with Scheduling Conflicts</asp:Label>
                                                                    </div>
                                                                </div>
                                                            </CommandItemTemplate>
                                                          	<Columns>
                                                                <telerik:GridTemplateColumn HeaderText="Course Description" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" AllowFiltering="false" DataField="CourseDescrip" AutoPostBackOnFilter="true">
                                                                    <ItemTemplate>
                                                                         <asp:Label ID="lblCodeDescrip" runat="server" Text='<%# Container.DataItem("CourseDescrip") %>' CssClass="LabelGrid" Width="150px"></asp:Label>
                                                                    </ItemTemplate> 
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Section" ItemStyle-VerticalAlign="Top" AllowFiltering="false"  ItemStyle-HorizontalAlign="Left"  DataField="ClsSection" AutoPostBackOnFilter="true">
                                                                    <ItemTemplate>
                                                                         <asp:Label ID="lblClsSection" runat="server" Text='<%# Container.DataItem("ClsSection") %>' CssClass="LabelGrid" Width="100px"></asp:Label>
                                                                    </ItemTemplate> 
                                                                </telerik:GridTemplateColumn>
                                                               <telerik:GridTemplateColumn HeaderText="Class Meetings Start-End Dates" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"  AllowFiltering="false" DataField="ClassPeriod">
                                                                    <ItemTemplate>
                                                                         <asp:Label ID="lblSchedule" runat="server" Text='<%# Container.DataItem("ClassPeriod") %>' CssClass="LabelGrid" Width="160px"></asp:Label>
                                                                    </ItemTemplate> 
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Meeting Schedules" ItemStyle-VerticalAlign="Top" AllowFiltering="false" 
                                                                DataField="MeetingSchedule" AutoPostBackOnFilter="true" FilterControlWidth="50px" ItemStyle-HorizontalAlign="Left" >
                                                                    <ItemTemplate>
                                                                         <asp:Label ID="lblMeetingSchedules" runat="server" Text='<%# Container.DataItem("MeetingSchedule") %>' CssClass="LabelGrid" Width="125px"></asp:Label>
                                                                    </ItemTemplate> 
                                                                </telerik:GridTemplateColumn>
															 </Columns>
                                                            </MasterTableView> 
                                                            <ClientSettings EnableRowHoverStyle="true" Scrolling-AllowScroll="true">
                                                                <Selecting AllowRowSelect="True" />
                                                             </ClientSettings>
                                                        </telerik:RadGrid>
       </div>
    </div>
    <asp:Label ID="Label1" Runat="server" Width="410px"></asp:Label>
    </form>
</body>
</html>
