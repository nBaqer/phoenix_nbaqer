﻿
Imports Telerik.Web.UI
Imports FAME.AdvantageV1.BusinessFacade
Partial Class AR_SetupTestRulesPanel
    Inherits System.Web.UI.Page
    Private Sub BindRadGrid()
        Dim radTestDetailsGrid As RadGrid
        radTestDetailsGrid = DirectCast(RadPanelBar1.FindItemByValue("TopSpeedTestingCategory").FindControl("RadTestDetailsGrid"), RadGrid)

        radTestDetailsGrid.DataSource = (New SetDictationTestRulesFacade).GetTestByCourse("abfda8f0-d24b-4285-8857-fd6b1c547ada")
        radTestDetailsGrid.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            BindRadGrid()
        End If
    End Sub
End Class
