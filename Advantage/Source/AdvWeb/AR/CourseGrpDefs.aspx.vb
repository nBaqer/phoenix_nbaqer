Imports FAME.DataAccessLayer
Imports System.Data
Imports FAME.Common
Imports FAME.ExceptionLayer
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class CourseGrpDefs
    Inherits System.Web.UI.Page
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected selectedModule As Integer
    Protected campusId As String
    Protected campus As String
    Protected courseid As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim userId As String
            ' Dim m_Context As HttpContext
            Dim resourceId As Integer
            Dim facCGrp As New CourseGrpFacade
            Dim advantageUserState As New BO.User()


            advantageUserState = AdvantageSession.UserState
            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            campus = AdvantageSession.UserState.CampusId.ToString
            userId = AdvantageSession.UserState.UserId.ToString
            campusId = AdvantageSession.UserState.CampusId.ToString
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campus)

            If Not Page.IsPostBack Then
                Dim db As New DataAccess
                '   Dim strSQL, strDescrip As String
                Dim ds As DataSet
                'Dim da As OleDbDataAdapter
                'Dim dr As OleDbDataReader
                Dim objCommon As New CommonUtilities
                Dim dt As New DataTable
                Dim dt1 As New DataTable
                Dim dt2 As New DataTable
                Dim objListGen As New DataListGenerator
                Dim ds1 As New DataSet
                Dim sw As New System.IO.StringWriter
                Dim ds2 As New DataSet
                Dim tbl1 As New DataTable
                Dim tbl2 As New DataTable
                Dim tbl3 As New DataTable
                'Dim sChildTyp As String
                'Dim sChildId As String
                'Dim row As DataRow
                Dim CourseGrpId As String
                Dim facade As New ProgFacade
                'Dim campusId As String

                'If Request.QueryString("cmpid") <> "" Then
                '    ViewState("CampusId") = Request.QueryString("cmpid")
                '    campusId = ViewState("CampusId")
                'End If
                'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString

                'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

                If Request.QueryString("CourseGrpId") <> "" Then
                    ViewState("CourseGrpId") = Request.QueryString("CourseGrpId")
                    txtCourseGrpId.Text = ViewState("CourseGrpId")
                End If

                If Request.QueryString("Hours") <> "" Then
                    ViewState("CourseGrpHours") = Request.QueryString("Hours")
                    txtHours.Text = ViewState("CourseGrpHours")
                End If
                If Request.QueryString("Credits") <> "" Then
                    ViewState("CourseGrpCredits") = Request.QueryString("Credits")
                    txtCredits.Text = ViewState("CourseGrpCredits")
                End If
                'Commented out by Troy 2/26/2005.If there is a special character such as #
                'in the course group name then whatever comes after it will be lost. For example,
                'if the name is "Course Group # 1 Test" only "Course Group" will be retrieved
                'from the query string.
                'If Request.QueryString("CourseGrp") <> "" Then
                '    ViewState("CourseGrp") = Request.QueryString("CourseGrp")
                '    txtCourseGrpDescrip.Text = ViewState("CourseGrp")
                'End If
                ViewState("CourseGrp") = facCGrp.GetCourseGrpName(txtCourseGrpId.Text)
                txtCourseGrpDescrip.Text = ViewState("CourseGrp")

                'txtCourseGrpId.Text = "656F4BB9-17A7-4E36-9F41-92BDBDE894BA"
                'txtCourseGrpDescrip.Text = "CourseGroup1"

                CourseGrpId = txtCourseGrpId.Text

                'Populate the Courses Not Assigned.
                ds = facade.ProcessCoursesNotAssgd("COURSEGRP", CourseGrpId, campusId)

                'Bind the datatable to the Available Courses Listbox.
                lbxAvailCourses.DataSource = ds
                lbxAvailCourses.DataTextField = "ChildDescripTyp"
                lbxAvailCourses.DataValueField = "ReqId"
                lbxAvailCourses.DataBind()


                'Populate the CourseGrps Not Assigned.
                ds = facade.ProcessCourseGrpsNotAssgd("COURSEGRP", CourseGrpId, campusId)


                'Bind the datatable to the Available CourseGrps Listbox.
                lbxAvailCourseGrps.DataSource = ds
                lbxAvailCourseGrps.DataTextField = "Descrip"
                lbxAvailCourseGrps.DataValueField = "ReqId"
                lbxAvailCourseGrps.DataBind()

                'Get all courses and coursegprs assigned to a particular prog version
                ds = facade.GetCourseCourseGrpsAssigned("COURSEGRP", CourseGrpId)

                'ds = facade.ProcessDescription(ds) #BN 04/28/05
                'Get Child Descriptions, populate hours and credits for selected
                'courses/coursegrps and determine whether a course/course grp is
                'required or elective.
                ds = facade.ProcessHrsCrdtsDescrips(ds)

                tbl1 = ds.Tables("FldsSelected")
                'Bind only if datatable is not empty
                If tbl1.Rows.Count > 0 Then
                    'Bind the datatable to the "Selected Courses/Coursegrps" Listbox.
                    lbxSelected.DataSource = tbl1
                    lbxSelected.DataTextField = "ChildDescripTyp"
                    lbxSelected.DataValueField = "ReqId"
                    lbxSelected.DataBind()

                End If

                'Get the hours and credits corr. to the sel.Courses/CourseGrps
                tbl2 = ds.Tables("ChildHrsCrds")
                If tbl2.Rows.Count > 0 Then
                    txtHours2.Text = tbl2.Rows(0).Item(0).ToString
                    txtCredits2.Text = tbl2.Rows(0).Item(1).ToString
                End If

                'Save the Hours & Credits to ViewState
                ViewState("selHours") = txtHours2.Text
                ViewState("selCredits") = txtCredits2.Text

                ''Get coursegroups not yet assigned to prog version.
                'strSQL = "SELECT t1.CourseGrpId,t1.CourseGrpDescrip " & _
                '     "FROM arCourseGrps t1 " & _
                '     "WHERE NOT EXISTS " & _
                '     "(SELECT t2.ChildId FROM arCourseGrpDefs t2 where t2.CourseGrpId = '" & txtCourseGrpId.Text & "' and t2.ChildId = t1.CourseGrpId)" & _
                '     "ORDER BY t1.CourseGrpDescrip"
                'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
                'ds = db.RunSQLDataSet(strSQL)

                'lbxAvailCourseGrps.DataSource = ds
                'lbxAvailCourseGrps.DataTextField = "CourseGrpDescrip"
                'lbxAvailCourseGrps.DataValueField = "CourseGrpId"
                'lbxAvailCourseGrps.DataBind()

                ''Close Connection
                'db.CloseConnection()

                ''Get courses not yet assigned to prog version.
                'strSQL = "SELECT t1.CourseId,t1.CourseDescrip " & _
                '     "FROM arCourses t1 " & _
                '     "WHERE NOT EXISTS " & _
                '     "(SELECT t2.ChildId FROM arCourseGrpDefs t2 where t2.CourseGrpId = '" & txtCourseGrpId.Text & "' and t2.ChildId = t1.CourseId)" & _
                '     "ORDER BY t1.CourseDescrip"
                'db.ConnectionString = "ConString"
                'ds = db.RunSQLDataSet(strSQL)

                'lbxAvailCourses.DataSource = ds
                'lbxAvailCourses.DataTextField = "CourseDescrip"
                'lbxAvailCourses.DataValueField = "CourseId"
                'lbxAvailCourses.DataBind()

                ''Close Connection
                'db.CloseConnection()


                ''Build query to obtain courseids and coursedescrips from the arCourses table.
                'strSQL = "SELECT t1.CourseId,CourseDescrip,CourseHours,CourseCredits " & _
                '         "FROM arCourses t1 "

                'db.ConnectionString = "ConString"
                'db.OpenConnection()
                'da = db.RunParamSQLDataAdapter(strSQL)
                'da.Fill(ds, "Courses")

                'tbl1 = ds.Tables("Courses")
                ''Make the ChildId column the primary key
                'With tbl1
                '    .PrimaryKey = New DataColumn() {.Columns("CourseId")}
                'End With


                ''Build query to obtain coursegrpids and coursegrpdescrips from the arCourseGrps table.
                'strSQL = "SELECT t1.CourseGrpId,CourseGrpDescrip,CourseGrpHours,CourseGrpCredits " & _
                '         "FROM arCourseGrps t1 "

                'db.ConnectionString = "ConString"
                'db.OpenConnection()
                'da = db.RunParamSQLDataAdapter(strSQL)
                'da.Fill(ds, "CourseGrps")


                'tbl2 = ds.Tables("CourseGrps")
                'With tbl2
                '    .PrimaryKey = New DataColumn() {.Columns("CourseGrpId")}
                'End With



                ''Build query to obtain course/coursegrpids and course/coursegrpdescrips from the arCourseGrpDef table.
                'strSQL = "SELECT t1.ChildId,ChildType,ChildSeq,Req " & _
                '         "FROM arCourseGrpDefs t1 " & _
                '         "WHERE t1.CourseGrpId = '" & txtCourseGrpId.Text & "'"

                'db.ConnectionString = "ConString"
                'db.OpenConnection()
                'da = db.RunParamSQLDataAdapter(strSQL)
                'da.Fill(ds, "FldsSelected")



                'lbxSelected.DataSource = tbl3
                'lbxSelected.DataTextField = "ChildDescripTyp"
                'lbxSelected.DataValueField = "ReqId"
                'lbxSelected.DataBind()



                ''Save the dataset to session
                'Session("FldsSelected") = tbl3

                'Save the dataset to session
                Session("WorkingDS") = ds

                'txtModUser.Text = Session("UserName")
                txtModUser.Text = AdvantageSession.UserState.UserName
                txtModDate.Text = Date.MinValue.ToString

                'Disable the new and delete buttons
                'objCommon.SetBtnState(Form1, "NEW", pObj)
            End If
            If pObj.HasFull Or pObj.HasEdit Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnRequired_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRequired.Click
        If lbxAvailCourses.SelectedIndex >= 0 Then
            Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim dt2 As DataTable
            Dim row As DataRow
            Dim selhours As Decimal
            Dim selcredits As Decimal
            Dim sCourseId As String
            Dim foundInCourseGroup As String
            Dim AddRowInDT As Boolean = True

            'Add selected item to the relevant assigned list. We want to ensure that the course does not 
            'already belong to the selected items. This could only happen if it was part of a course group
            'that is already in the selected items. For each item in the selected list we will check if it
            'is a course group and then check to see if the selected course is in it. If it is then we will
            'cancel the add operation and display an error message to the user.
            foundInCourseGroup = CourseExistsInSelectedList(lbxAvailCourses.SelectedItem.Value)

            If foundInCourseGroup = "" Then
                lbxSelected.Items.Add(New ListItem(lbxAvailCourses.SelectedItem.Text & " - R", lbxAvailCourses.SelectedItem.Value))

                'Get the dataset from session
                ds = Session("WorkingDS")
                'retrieve the datatable from the ds
                dt = ds.Tables("FldsSelected")

                Dim dvrs As DataViewRowState
                dvrs = DataViewRowState.Deleted
                Dim arows As DataRow() = dt.Select("ReqId = '" & lbxAvailCourses.SelectedItem.Value.ToString & "' ", "", dvrs)
                If arows.Length > 0 Then
                    dt.Rows.Remove(arows(0))
                    AddRowInDT = False
                End If

                If AddRowInDT = True Then

                    dr = dt.NewRow()
                    dr("ReqId") = lbxAvailCourses.SelectedItem.Value.ToString
                    dr("ReqSeq") = lbxSelected.Items.Count
                    dr("IsRequired") = 1
                    dr("ReqTypeId") = 1
                    'Get hours and credits of the selected course from datatable.
                    'retrieve the Courses datatable from the ds
                    dt2 = ds.Tables("Courses")
                    'Get the Requirement Id(i.e CourseId in this case)
                    sCourseId = lbxAvailCourses.SelectedItem.Value.ToString
                    'Find the row that contains the course info.
                    row = dt2.Rows.Find(sCourseId)
                    dr("Hours") = row("Hours")
                    dr("Credits") = row("Credits")
                    dt.Rows.Add(dr)

                    'Save the dataset to session
                    Session("WorkingDS") = ds
                End If
                'dt = Session("FldsSelected")
                'dr = dt.NewRow()
                'dr("ChildId") = lbxAvailCourses.SelectedItem.Value.ToString
                'dr("ChildType") = "C"
                'dr("ChildSeq") = lbxSelected.Items.Count
                'dr("Req") = 1
                'dt.Rows.Add(dr)
                'Session("FldsSelected") = dt
                '****************************************************************************
                'Everytime we select a course, we want to populate
                'the Hours and Credits associated with that 
                'particular course.
                '****************************************************************************
                'Get the dataset from session
                ds = Session("WorkingDS")


                'retrieve the Courses datatable from the ds
                dt2 = ds.Tables("Courses")

                'Get the Requirement Id(i.e CourseId in this case)
                sCourseId = lbxAvailCourses.SelectedItem.Value.ToString

                'Find the row in the courses datatable that contains this
                'course information.
                row = dt2.Rows.Find(sCourseId)

                'Retrieve the hours and credits attached to this course.
                If (ViewState("selHours") <> "") Then
                    selhours = Decimal.Parse(ViewState("selHours"))
                Else
                    selhours = 0
                End If
                If Not row.IsNull("Hours") Then
                    selhours += row("Hours")
                End If
                ViewState("selHours") = selhours.ToString
                txtHours2.Text = selhours.ToString

                If (ViewState("selCredits") <> "") Then
                    selcredits = Decimal.Parse(ViewState("selCredits"))
                Else
                    selcredits = 0
                End If
                If Not row.IsNull("Credits") Then
                    selcredits += row("Credits")
                End If
                ViewState("selCredits") = selcredits.ToString
                txtCredits2.Text = selcredits.ToString

                Session("WorkingDS") = ds


                'Remove the item from the lbxAvailCamps list
                SelIndex = lbxAvailCourses.SelectedIndex
                lbxAvailCourses.Items.RemoveAt(SelIndex)
            Else
                DisplayErrorMessage(lbxAvailCourses.SelectedItem.Text & " aready exists in " & foundInCourseGroup)
            End If


        End If
    End Sub

    Private Sub btnElective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnElective.Click
        If lbxAvailCourses.SelectedIndex >= 0 Then
            Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim dt2 As DataTable
            Dim row As DataRow
            Dim selhours As Decimal
            Dim selcredits As Decimal
            Dim sCourseGrpId As String
            '            Dim childID As String
            Dim foundInCourseGroup As String
            Dim AddRowInDT As Boolean = True

            'Add selected item to the relevant assigned list. We want to ensure that the course does not 
            'already belong to the selected items. This could only happen if it was part of a course group
            'that is already in the selected items. For each item in the selected list we will check if it
            'is a course group and then check to see if the selected course is in it. If it is then we will
            'cancel the add operation and display an error message to the user.
            foundInCourseGroup = CourseExistsInSelectedList(lbxAvailCourses.SelectedItem.Value)

            If foundInCourseGroup = "" Then
                lbxSelected.Items.Add(New ListItem(lbxAvailCourses.SelectedItem.Text & " - E", lbxAvailCourses.SelectedItem.Value))

                'Get the dataset from session
                ds = Session("WorkingDS")
                'retrieve the datatable from the ds
                dt = ds.Tables("FldsSelected")

                Dim dvrs As DataViewRowState
                dvrs = DataViewRowState.Deleted
                Dim arows As DataRow() = dt.Select("ReqId = '" & lbxAvailCourses.SelectedItem.Value.ToString & "' ", "", dvrs)
                If arows.Length > 0 Then
                    dt.Rows.Remove(arows(0))
                    AddRowInDT = False
                End If

                If AddRowInDT = True Then

                    dr = dt.NewRow()
                    dr("ReqId") = lbxAvailCourses.SelectedItem.Value.ToString
                    dr("ReqSeq") = lbxSelected.Items.Count
                    dr("IsRequired") = 0
                    dr("ReqTypeId") = 1
                    'Get hours and credits of the selected course from datatable.
                    'retrieve the Courses datatable from the ds
                    dt2 = ds.Tables("Courses")
                    'Get the Requirement Id(i.e CourseId in this case)
                    sCourseGrpId = lbxAvailCourses.SelectedItem.Value.ToString
                    'Find the row that contains the course info.
                    row = dt2.Rows.Find(sCourseGrpId)
                    dr("Hours") = row("Hours")
                    dr("Credits") = row("Credits")
                    dt.Rows.Add(dr)

                    '**************************************************************
                    'Everytime we select a course, we want to populate
                    'the Hours and Credits associated with that 
                    'particular course.

                    'Get the dataset from session
                    ds = Session("WorkingDS")
                End If

                'retrieve the datatable from the ds
                dt2 = ds.Tables("Courses")
                '**************************************************************

                sCourseGrpId = lbxAvailCourses.SelectedItem.Value.ToString

                row = dt2.Rows.Find(sCourseGrpId)


                'Retrieve the hours and credits attached to this course.
                If (ViewState("selHours") <> "") Then
                    selhours = Decimal.Parse(ViewState("selHours"))
                Else
                    selhours = 0
                End If
                If Not row.IsNull("Hours") Then
                    selhours += row("Hours")
                End If
                ViewState("selHours") = selhours.ToString
                txtHours2.Text = selhours.ToString

                If (ViewState("selCredits") <> "") Then
                    selcredits = Decimal.Parse(ViewState("selCredits"))
                Else
                    selcredits = 0
                End If
                If Not row.IsNull("Hours") Then
                    selcredits += row("Credits")
                End If
                ViewState("selCredits") = selcredits.ToString
                txtCredits2.Text = selcredits.ToString
                'dt = Session("FldsSelected")
                'dr = dt.NewRow()
                'dr("ChildId") = lbxAvailCourses.SelectedItem.Value.ToString
                'dr("ChildType") = "C"
                'dr("ChildSeq") = lbxSelected.Items.Count
                'dr("Req") = 0
                'dt.Rows.Add(dr)
                'Session("FldsSelected") = dt
                'Response.Write("Number of rows:" & DirectCast(Session("FldsSelected"), DataTable).Rows.Count.ToString)
                'Remove the item from the lbxAvailCamps list
                Session("WorkingDS") = ds
                SelIndex = lbxAvailCourses.SelectedIndex
                lbxAvailCourses.Items.RemoveAt(SelIndex)
            Else
                DisplayErrorMessage(lbxAvailCourses.SelectedItem.Text & " aready exists in " & foundInCourseGroup)
            End If


        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim SelIndex As Integer
        Dim iChildId As String
        Dim dt1 As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim intPos As Integer
        Dim dt2 As DataTable
        Dim row As DataRow
        Dim selhours As Decimal
        Dim selcredits As Decimal
        Dim sCourseId As String
        Dim sCourseGrpId As String
        '        Dim dt As DataTable

        If lbxSelected.SelectedIndex >= 0 Then
            'Add selected item to the lbxAvailCourses list. In order to do this we
            'have to use the FldsSelectCamps to find out the 'FldId for the selected item.
            'Remember that the item has a "-R" or "-E" attached to it so we have to find where the " - "
            'starts inorder to remove it.
            intPos = lbxSelected.SelectedItem.Text.IndexOf("-")
            If (lbxSelected.SelectedItem.Text.IndexOf("-") >= 0) Then 'Course was selected
                '**************************************************************
                'Everytime we de-select a course, we want to subract
                'the Hours and Credits associated with that 
                'particular course from the Hours & Credits text boxes.

                'Get the dataset from session
                ds = Session("WorkingDS")
                'retrieve the datatable to decrement the indices of the items below.
                dt1 = ds.Tables("FldsSelected")
                Dim aRows As DataRow()
                Dim row1 As DataRow
                aRows = dt1.Select("ReqSeq > " & lbxSelected.SelectedIndex)
                For Each row1 In aRows
                    row1("ReqSeq") = row1("ReqSeq") - 1
                Next
                'Session("WorkingDS") = ds

                'ds = Session("WorkingDS")
                'retrieve the datatable from the ds
                dt2 = ds.Tables("Courses")
                '**************************************************************

                sCourseId = lbxSelected.SelectedItem.Value.ToString

                row = dt2.Rows.Find(sCourseId)

                If Not IsNothing(row) Then

                    selhours = Decimal.Parse(ViewState("selHours"))
                    If Not row.IsNull("Hours") Then
                        selhours -= row("Hours")
                    End If
                    ViewState("selHours") = selhours.ToString
                    txtHours2.Text = selhours.ToString

                    selcredits = Decimal.Parse(ViewState("selCredits"))
                    If Not row.IsNull("Credits") Then
                        selcredits -= row("Credits")
                    End If
                    ViewState("selCredits") = selcredits.ToString
                    txtCredits2.Text = selcredits.ToString

                    lbxAvailCourses.Items.Add(New ListItem(lbxSelected.SelectedItem.Text.Substring(0, intPos - 1), lbxSelected.SelectedItem.Value))

                Else 'Course group with a hyphen was selected

                    lbxAvailCourseGrps.Items.Add(New ListItem(lbxSelected.SelectedItem.Text, lbxSelected.SelectedItem.Value))
                    '**************************************************************
                    'Everytime we de-select a course, we want to subract
                    'the Hours and Credits associated with that 
                    'particular coursegrp from the Hours & Credits text boxes.

                    'Get the dataset from session
                    ds = Session("WorkingDS")
                    dt1 = ds.Tables("FldsSelected")
                    Dim aRows2 As DataRow()
                    Dim row2 As DataRow

                    aRows2 = dt1.Select("ReqSeq > " & lbxSelected.SelectedIndex)
                    For Each row2 In aRows2
                        row2("ReqSeq") = row2("ReqSeq") - 1
                    Next
                    'Session("WorkingDS") = ds

                    'retrieve the datatable from the ds
                    dt2 = ds.Tables("CourseGrps")
                    '**************************************************************

                    sCourseGrpId = lbxSelected.SelectedItem.Value.ToString

                    row = dt2.Rows.Find(sCourseGrpId)

                    selhours = Decimal.Parse(ViewState("selHours"))
                    selhours -= row("Hours")
                    ViewState("selHours") = selhours.ToString
                    txtHours2.Text = selhours.ToString

                    selcredits = Decimal.Parse(ViewState("selCredits"))
                    selcredits -= row("Credits")
                    ViewState("selCredits") = selcredits.ToString
                    txtCredits2.Text = selcredits.ToString
                End If


            Else 'Coursegrp was selected
                lbxAvailCourseGrps.Items.Add(New ListItem(lbxSelected.SelectedItem.Text, lbxSelected.SelectedItem.Value))
                '**************************************************************
                'Everytime we de-select a course, we want to subract
                'the Hours and Credits associated with that 
                'particular coursegrp from the Hours & Credits text boxes.

                'Get the dataset from session
                ds = Session("WorkingDS")
                dt1 = ds.Tables("FldsSelected")
                Dim aRows As DataRow()
                Dim row1 As DataRow

                aRows = dt1.Select("ReqSeq > " & lbxSelected.SelectedIndex)
                For Each row1 In aRows
                    row1("ReqSeq") = row1("ReqSeq") - 1
                Next
                'Session("WorkingDS") = ds

                'retrieve the datatable from the ds
                dt2 = ds.Tables("CourseGrps")
                '**************************************************************

                sCourseGrpId = lbxSelected.SelectedItem.Value.ToString

                row = dt2.Rows.Find(sCourseGrpId)

                selhours = Decimal.Parse(ViewState("selHours"))
                selhours -= row("Hours")
                ViewState("selHours") = selhours.ToString
                txtHours2.Text = selhours.ToString

                selcredits = Decimal.Parse(ViewState("selCredits"))
                selcredits -= row("Credits")
                ViewState("selCredits") = selcredits.ToString
                txtCredits2.Text = selcredits.ToString
            End If


            dr = dt1.Rows.Find(lbxSelected.SelectedItem.Value.ToString)
            If Not IsNothing(dr) Then
                iChildId = dr("ReqId").ToString
                If dr.RowState <> DataRowState.Added Then
                    'Mark the row as deleted
                    dr.Delete()
                Else
                    dt1.Rows.Remove(dr)
                End If
            End If

            'Remove the item from the lbxSelectCamps list
            SelIndex = lbxSelected.SelectedIndex
            lbxSelected.Items.RemoveAt(SelIndex)

            'Save the dataset to session
            Session("WorkingDS") = ds


        End If

        'If lbxSelected.SelectedIndex >= 0 Then
        '    'Add selected item to the lbxAvailCourses list. In order to do this we
        '    'have to use the FldsSelectCamps to find out the 'FldId for the selected item.
        '    'Remember that the item has a "-R" or "-E" attached to it so we have to find where the " - "
        '    'starts inorder to remove it.
        '    intPos = lbxSelected.SelectedItem.Text.IndexOf("-")
        '    If (lbxSelected.SelectedItem.Text.IndexOf("-") >= 0) Then
        '        lbxAvailCourses.Items.Add(New ListItem(lbxSelected.SelectedItem.Text.Substring(0, intPos - 1)))
        '    Else
        '        lbxAvailCourseGrps.Items.Add(New ListItem(lbxSelected.SelectedItem.Text, lbxSelected.SelectedItem.Value))
        '    End If
        '    dt = Session("FldsSelected")
        '    dr = dt.Rows.Find(lbxSelected.SelectedItem.Value.ToString)
        '    iChildId = dr("ChildId").ToString
        '    If dr.RowState <> DataRowState.Added Then
        '        'Mark the row as deleted
        '        dr.Delete()
        '    Else
        '        dt.Rows.Remove(dr)
        '    End If
        '    'Remove the item from the lbxSelectCamps list
        '    SelIndex = lbxSelected.SelectedIndex
        '    lbxSelected.Items.RemoveAt(SelIndex)
        '    Session("FldsSelected") = dt


        'End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If lbxAvailCourseGrps.SelectedIndex >= 0 Then
            Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim dt2 As DataTable
            Dim row As DataRow
            Dim selhours As Decimal
            Dim selcredits As Decimal
            Dim sCourseGrpId As String
            '            Dim firstCommonReq As String
            Dim fac As New TranscriptFacade
            Dim foundCourse As String
            Dim errorMessage As String
            Dim AddRowInDT As Boolean = True


            'Add selected item to the relevant assigned list. We want to ensure that the course group does not 
            'have any reqs that belong to the selected items. We will build a collection of ReqInfo objects
            'based on the items in the selected list. We will then check if the course group has any common
            'course with the collection. If it does we will display an error message letting the user know
            'that there are common courses.
            foundCourse = fac.GetFirstCommonRequirement(lbxAvailCourseGrps.SelectedItem.Value.ToString, BuildReqInfoCollection)

            If foundCourse = "" Then
                'Add selected item to the relevant assigned list. 
                lbxSelected.Items.Add(New ListItem(lbxAvailCourseGrps.SelectedItem.Text, lbxAvailCourseGrps.SelectedItem.Value))

                'Get the dataset from session
                ds = Session("WorkingDS")
                'retrieve the datatable from the ds
                dt = ds.Tables("FldsSelected")

                Dim dvrs As DataViewRowState
                dvrs = DataViewRowState.Deleted
                Dim arows As DataRow() = dt.Select("ReqId = '" & lbxAvailCourseGrps.SelectedItem.Value.ToString & "' ", "", dvrs)
                If arows.Length > 0 Then
                    dt.Rows.Remove(arows(0))
                    AddRowInDT = False
                End If

                If AddRowInDT = True Then

                    dr = dt.NewRow()
                    dr("ReqId") = lbxAvailCourseGrps.SelectedItem.Value.ToString
                    dr("ReqSeq") = lbxSelected.Items.Count
                    dr("IsRequired") = 1
                    dr("ReqTypeId") = 2

                    'Get hours and credits of the selected coursegrp from datatable.
                    dt2 = ds.Tables("CourseGrps")
                    sCourseGrpId = lbxAvailCourseGrps.SelectedItem.Value.ToString

                    row = dt2.Rows.Find(sCourseGrpId)
                    dr("Hours") = row("Hours")
                    dr("Credits") = row("Credits")
                    dt.Rows.Add(dr)

                    'dt = Session("FldsSelected")
                    'dr = dt.NewRow()
                    'dr("ChildId") = lbxAvailCourseGrps.SelectedItem.Value.ToString
                    'dr("ChildType") = "G"
                    'dr("ChildSeq") = lbxSelected.Items.Count
                    'dr("Req") = 0
                    'dt.Rows.Add(dr)
                    'Session("FldsSelected") = dt

                    Session("WorkingDS") = ds
                End If
                '*******************************************************************
                'Everytime we select a coursegrp, we want to populate
                'the Hours and Credits associated with that 
                'particular coursegrp.

                'Get the dataset from session
                ds = Session("WorkingDS")
                'retrieve the datatable from the ds
                dt2 = ds.Tables("CourseGrps")
                '*******************************************************************

                sCourseGrpId = lbxAvailCourseGrps.SelectedItem.Value.ToString

                row = dt2.Rows.Find(sCourseGrpId)

                'Retrieve the hours and credits attached to this course.
                If (ViewState("selHours") <> "") Then
                    selhours = Decimal.Parse(ViewState("selHours"))
                Else
                    selhours = 0
                End If
                selhours += row("Hours")
                ViewState("selHours") = selhours.ToString
                txtHours2.Text = selhours.ToString

                If (ViewState("selCredits") <> "") Then
                    selcredits = Decimal.Parse(ViewState("selCredits"))
                Else
                    selcredits = 0
                End If
                selcredits += row("Credits")
                ViewState("selCredits") = selcredits.ToString
                txtCredits2.Text = selcredits.ToString

                Session("WorkingDS") = ds

                'Remove the item from the lbxAvailCamps list
                SelIndex = lbxAvailCourseGrps.SelectedIndex
                lbxAvailCourseGrps.Items.RemoveAt(SelIndex)
            Else
                'Display error message to the user with course that is already in the hierarchy
                errorMessage = "Cannot add " & lbxAvailCourseGrps.SelectedItem.Text & "." & vbCr
                errorMessage &= "It contains " & foundCourse & " which is already part of the selected items."
                DisplayErrorMessage(errorMessage)
            End If


        End If
    End Sub

    Private Sub btnDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetails.Click

        If lbxAvailCourseGrps.SelectedIndex >= 0 Then

            Dim sb As New StringBuilder
            '   append PrgVersion to the querystring
            sb.Append("&CourseGrp=" + lbxAvailCourseGrps.SelectedItem.Text)
            '   append ProgVerId Description to the querystring
            sb.Append("&CourseGrpId=" + lbxAvailCourseGrps.SelectedItem.Value.ToString)

            '   setup the properties of the new window
            Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
            Dim name As String = "CoursesInCourseGroup"
            Dim url As String = "../AR/" + name + ".aspx?resid=477&mod=AR&cmpid=" + ViewState("CampusId") + sb.ToString

            '   open new window and pass parameters
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
        End If
    End Sub

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Dim i As Integer
    '    Dim db As New DataAccess()
    '    Dim strGuid As String
    '    Dim ID As String
    '    Dim Description As String
    '    Dim Code As String
    '    Dim sb As New System.Text.StringBuilder()
    '    Dim dt1, dt2 As DataTable
    '    Dim dr As DataRow
    '    Dim objCommon As New CommonUtilities()
    '    Dim iCampusId As String
    '    Dim ds1 As New DataSet()
    '    Dim objListGen As New DataListGenerator()
    '    Dim sw As New System.IO.StringWriter()
    '    Dim dv2 As New DataView()
    '    Dim sChildTyp As String
    '    Dim iChildSeq As Integer
    '    Dim strValue As String
    '    Dim iReq As Integer
    '    Dim intPos As Integer

    '    db.ConnectionString = "ConString"

    '    Try


    '        ' CODE BELOW HANDLES AN INSERT/UPDATE INTO THE TABLE(S)

    '        'Run getchanges method on the dataset to see which rows have changed.
    '        'This section handles the changes to the Advantage Required fields
    '        dt1 = Session("FldsSelected")
    '        dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted)


    '        If Not IsNothing(dt2) Then
    '            For Each dr In dt2.Rows
    '                If dr.RowState = DataRowState.Added Then
    '                    Try
    '                        ID = dr("ChildId").ToString
    '                        sChildTyp = dr("ChildType")
    '                        If (dr("Req") = True) Then
    '                            iReq = 1
    '                        Else
    '                            iReq = 0
    '                        End If
    '                        iChildSeq = dr("ChildSeq")
    '                        'Insert row into arCourseGrpDefs table
    '                        With sb
    '                            .Append("INSERT INTO arCourseGrpDefs(CourseGrpId,ChildId,ChildType,ChildSeq,Req) ")
    '                            .Append("VALUES(?,?,?,?,?)")
    '                        End With

    '                        db.AddParameter("@coursegrpid", txtCourseGrpId.Text, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                        db.AddParameter("@childid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                        db.AddParameter("@childtype", sChildTyp, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                        db.AddParameter("@childseq", iChildSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                        db.AddParameter("@req", iReq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '                        db.ClearParameters()
    '                        sb.Remove(0, sb.Length)

    '                    Catch ex As System.Exception
     '                    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '                    	exTracker.TrackExceptionWrapper(ex)

    '                        Throw New BaseException(ex.InnerException.Message)
    '                    End Try
    '                ElseIf dr.RowState = DataRowState.Deleted Then
    '                    ID = dr("ChildId", DataRowVersion.Original).ToString
    '                    With sb
    '                        .Append("DELETE FROM arCourseGrpDefs ")
    '                        .Append("WHERE CourseGrpId = ? ")
    '                        .Append("AND ChildId = ? ")
    '                    End With

    '                    db.AddParameter("@campgrpid", txtCourseGrpId.Text, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                    db.AddParameter("@campusid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '                    db.ClearParameters()
    '                    sb.Remove(0, sb.Length)

    '                    'Delete row from ResTblFlds table. It is important to remember that when a row
    '                    'is marked as deleted we cannot access the value from the current version of the
    '                    'row. We have to use the DataRowVersion enumeration to obtain the original value
    '                    'stored in the row.

    '                End If
    '            Next
    '        End If
    '        dt1.AcceptChanges()
    '        Session("FldsSelected") = dt1

    '    Catch ex As System.Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try
    'End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim i As Integer
        'Dim strGuid As String
        'Dim Description As String
        'Dim Code As String
        Dim sb As New System.Text.StringBuilder
        Dim dt1, dt2 As DataTable ', dt3
        Dim dr As DataRow
        Dim objCommon As New CommonUtilities
        '    Dim iCampusId As String
        Dim ds1 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim dv2 As New DataView
        'Dim strValue As String
        'Dim intPos As Integer
        Dim facade As New ProgFacade
        Dim ChildObj As New ChildInfo
        Dim ds As New DataSet
        Dim CourseGrpId As String
        Dim coursegrphrs As Decimal
        Dim coursegrpcrds As Decimal
        '   Dim BR As ProgBL
        Dim childhrs As Decimal
        Dim childcrds As Decimal
        Dim result As String
        Dim sModDate As String
        Dim originalDR As DataRow


        CourseGrpId = txtCourseGrpId.Text
        'coursegrphrs = CInt(txtHours.Text)
        'coursegrpcrds = CInt(txtCredits.Text)
        'If (txtHours2.Text <> "") Then
        '    childhrs = CInt(txtHours2.Text)
        'End If
        'If (txtCredits2.Text <> "") Then
        '    childcrds = CInt(txtCredits2.Text)
        'End If
        coursegrphrs = Decimal.Parse(txtHours.Text)
        coursegrpcrds = Decimal.Parse(txtCredits.Text)
        If (txtHours2.Text <> "") Then
            childhrs = Decimal.Parse(txtHours2.Text)
        End If
        If (txtCredits2.Text <> "") Then
            childcrds = Decimal.Parse(txtCredits2.Text)


        End If
        Try

            'Pull the dataset from Session.
            ds = Session("WorkingDS")

            ' CODE BELOW HANDLES AN INSERT/UPDATE INTO THE TABLE(S)

            'Run getchanges method on the dataset to see which rows have changed.
            'This section handles the changes to the Advantage Required fields
            dt1 = ds.Tables("FldsSelected")
            dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted Or DataRowState.Modified)

            'ONLY IF WE THE HOURS AND CREDITS ARE VALIDATED CORRECTLY
            'USING THE BUSINESS LOGIC, GO AHEAD TO SAVE THIS RECORD TO 
            'THE DATABASE.
            If (facade.ValidateHrsCreds(coursegrphrs, coursegrpcrds, childhrs, childcrds, ds) = ProgBL.ValResult.Success) Then
                If Not IsNothing(dt2) Then
                    For Each dr In dt2.Rows
                        If dr.RowState = DataRowState.Added Then
                            Try
                                ChildObj.ChildId = dr("ReqId").ToString
                                If (dr("IsRequired") = True) Then
                                    ChildObj.Req = 1
                                Else
                                    ChildObj.Req = 0
                                End If
                                ChildObj.ChildSeq = dr("ReqSeq")

                                'sModDate = facade.InsertCourseCourseGrp("COURSEGRP", ChildObj, CourseGrpId, Session("UserName"))
                                sModDate = facade.InsertCourseCourseGrp("COURSEGRP", ChildObj, CourseGrpId, AdvantageSession.UserState.UserName)

                                If sModDate <> "" Then
                                    'Get the sequence of the selected item
                                    originalDR = dt1.Rows.Find(dr("ReqId"))
                                    originalDR("ModDate") = CDate(sModDate)
                                End If
                                'facade.InsertCourseCourseGrp("COURSEGRP", ChildObj, CourseGrpId, Session("UserName"))

                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException(ex.InnerException.Message)
                            End Try
                        ElseIf dr.RowState = DataRowState.Deleted Then
                            ChildObj.ChildId = dr("ReqId", DataRowVersion.Original).ToString

                            result = facade.DeleteCourseCourseGrp("COURSEGRP", ChildObj, CourseGrpId)
                            If Not result = "" Then
                                '   Display Error Message
                                DisplayErrorMessage(result)
                            End If
                        ElseIf dr.RowState = DataRowState.Modified Then
                            ChildObj.ChildId = dr("ReqId").ToString
                            ChildObj.ChildSeq = dr("ReqSeq")
                            ChildObj.ChildSeq = dr("ReqSeq")
                            ChildObj.ModDate = dr("ModDate")
                            'facade.UpdateCourseCourseGrp("COURSEGRP", ChildObj, CourseGrpId, Session("UserName"))
                            facade.UpdateCourseCourseGrp("COURSEGRP", ChildObj, CourseGrpId, AdvantageSession.UserState.UserName)
                        End If
                    Next
                End If
                dt1.AcceptChanges()
                'Check if datatable and selected list box are synchronised. If not, then refresh datatable
                'with data from the database.
                SynchronizeDTAndListBox(dt1)

                dt1 = Session("FldsSelected")

            ElseIf (facade.ValidateHrsCreds(coursegrphrs, coursegrpcrds, childhrs, childcrds, ds) = ProgBL.ValResult.FailHrsCrdtsNtMet) Then
                DisplayErrorMessage("Total hours/ credits for the Courses is less than the total for the Course Group")
                'Response.Write("hrs and credts for the courses/coursegrps is less than hrs & crdts of program version")
            ElseIf (facade.ValidateHrsCreds(coursegrphrs, coursegrpcrds, childhrs, childcrds, ds) = ProgBL.ValResult.FailReqHrsCrdsOver) Then
                DisplayErrorMessage("Total hours/ credits for the required courses must be less than the total for the Course Group")
                'Response.Write("hrs and credts for the required courses/coursegrps is less than hrs and crdts of program version")
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    'Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
    '    Dim count As Integer
    '    Dim selIndex As Integer
    '    Dim tmpTxt As String
    '    Dim tmpVal As String
    '    If lbxSelected.SelectedIndex > 0 Then
    '        For count = 0 To count = lbxSelected.Items.Count
    '            selIndex = lbxSelected.SelectedIndex
    '            tmpTxt = lbxSelected.Items(selIndex - 1).Text
    '            tmpVal = lbxSelected.Items(selIndex - 1).Value.ToString

    '            lbxSelected.Items(selIndex - 1).Text = lbxSelected.Items(selIndex).Text
    '            lbxSelected.Items(selIndex - 1).Value = lbxSelected.Items(selIndex).Value

    '            lbxSelected.Items(selIndex).Text = tmpTxt
    '            lbxSelected.Items(selIndex).Value = tmpVal
    '        Next
    '    End If
    'End Sub

    'Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
    '    Dim count As Integer
    '    Dim selIndex As Integer
    '    Dim tmpTxt As String
    '    Dim tmpVal As String

    '    If lbxSelected.SelectedIndex < (lbxSelected.Items.Count - 1) Then
    '        For count = 0 To count = lbxSelected.Items.Count
    '            selIndex = lbxSelected.SelectedIndex
    '            tmpTxt = lbxSelected.Items(selIndex + 1).Text
    '            tmpVal = lbxSelected.Items(selIndex + 1).Value.ToString

    '            lbxSelected.Items(selIndex + 1).Text = lbxSelected.Items(selIndex).Text
    '            lbxSelected.Items(selIndex + 1).Value = lbxSelected.Items(selIndex).Value

    '            lbxSelected.Items(selIndex).Text = tmpTxt
    '            lbxSelected.Items(selIndex).Value = tmpVal
    '        Next
    '    End If
    'End Sub
    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        Dim count As Integer
        Dim selIndex As Integer
        Dim tmpTxt As String
        Dim tmpVal As String
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim row1, row2 As DataRow
        Dim tmpSeq, curSeq As Integer

        If lbxSelected.SelectedIndex > 0 Then
            For count = 0 To count = lbxSelected.Items.Count
                selIndex = lbxSelected.SelectedIndex
                tmpTxt = lbxSelected.Items(selIndex - 1).Text
                tmpVal = lbxSelected.Items(selIndex - 1).Value.ToString

                'You need to swap the sequence numbers of these elements as well
                ds = Session("WorkingDS")
                dt = ds.Tables("FldsSelected")

                'Get the sequence of the item above the selected item.
                row1 = dt.Rows.Find(lbxSelected.Items(selIndex - 1).Value)
                tmpSeq = row1("ReqSeq")

                'Get the sequence of the selected item
                row2 = dt.Rows.Find(lbxSelected.Items(selIndex).Value)
                curSeq = row2("ReqSeq")

                'Swap the sequences of the selected item w/ the item above in the datatable
                row2("ReqSeq") = tmpSeq
                row1("ReqSeq") = curSeq

                'Save the dataset to session
                Session("WorkingDS") = ds

                'Swap the items in the selected index w/ the index above.
                lbxSelected.Items(selIndex - 1).Text = lbxSelected.Items(selIndex).Text
                lbxSelected.Items(selIndex - 1).Value = lbxSelected.Items(selIndex).Value

                lbxSelected.Items(selIndex).Text = tmpTxt
                lbxSelected.Items(selIndex).Value = tmpVal

                lbxSelected.SelectedIndex = selIndex - 1

            Next
        End If
    End Sub

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        Dim count As Integer
        Dim selIndex As Integer
        Dim tmpTxt As String
        Dim tmpVal As String
        Dim row1, row2 As DataRow
        Dim dt As DataTable
        Dim ds As DataSet
        Dim tmpSeq, curSeq As Integer

        'Only if there is an item selected and it is not the last item in the list
        If lbxSelected.SelectedIndex >= 0 And lbxSelected.SelectedIndex < (lbxSelected.Items.Count - 1) Then
            For count = 0 To count = lbxSelected.Items.Count
                selIndex = lbxSelected.SelectedIndex
                tmpTxt = lbxSelected.Items(selIndex + 1).Text
                tmpVal = lbxSelected.Items(selIndex + 1).Value.ToString

                'You need to swap the sequence numbers of these elements as well
                ds = Session("WorkingDS")
                dt = ds.Tables("FldsSelected")

                'Get the sequence of the item below the selected item.
                row1 = dt.Rows.Find(lbxSelected.Items(selIndex + 1).Value)
                tmpSeq = row1("ReqSeq")

                'Get the sequence of the selected item
                row2 = dt.Rows.Find(lbxSelected.Items(selIndex).Value)
                curSeq = row2("ReqSeq")

                'Swap the sequences of the selected item w/ the item above in the datatable
                row2("ReqSeq") = tmpSeq
                row1("ReqSeq") = curSeq

                'Save the dataset to session
                Session("WorkingDS") = ds
                lbxSelected.Items(selIndex + 1).Text = lbxSelected.Items(selIndex).Text
                lbxSelected.Items(selIndex + 1).Value = lbxSelected.Items(selIndex).Value

                lbxSelected.Items(selIndex).Text = tmpTxt
                lbxSelected.Items(selIndex).Value = tmpVal

                lbxSelected.SelectedIndex = selIndex + 1

            Next
        End If
    End Sub
    Private Sub SynchronizeDTAndListBox(ByVal DT As DataTable)
        'Check each item in the selected list if it is a course group. If it is see if the course passed
        'in exists in it.
        Dim strFound As String = ""
        Dim i As Integer
        'Dim dr As DataRow
        'Dim dtCsrGrpReqs As DataTable
        Dim objCommon As New CommonUtilities
        ' Dim item As ListItem
        Dim ds As DataSet
        Dim facade As New ProgFacade

        'For each item in the collection. If the item is a course then we will check the dt1 datatable
        'to see if it exists there. If the item is a course group then we will populate dt2 with the
        'reqs for it. We will then loop through and see if there are any common courses.
        For i = 0 To lbxSelected.Items.Count - 1
            Dim row2 As DataRow = DT.Rows.Find(lbxSelected.Items(i).Value.ToString)
            If IsNothing(row2) Then

                'Refresh the datatable.
                'Get all courses and coursegprs assigned to a particular prog version
                ds = facade.GetCourseCourseGrpsAssigned("COURSEGRP", txtCourseGrpId.Text)

                'Get Child Descriptions, populate hours and credits for selected
                'courses/coursegrps and determine whether a course/course grp is
                'required or elective.
                ds = facade.ProcessHrsCrdtsDescrips(ds)

                DT = ds.Tables("FldsSelected").Copy

                Session("WorkingDS") = ds
            End If
        Next
        Session("FldsSelected") = DT

    End Sub
    Private Function CourseExistsInSelectedList(ByVal reqId As String) As String
        'Check each item in the selected list if it is a course group. If it is see if the course passed
        'in exists in it.
        Dim strFound As String = ""
        Dim i As Integer
        Dim dt As DataTable
        Dim fac As New TranscriptFacade
        '    Dim dtCsrGrpReqs As DataTable
        Dim objCommon As New CommonUtilities

        If lbxSelected.Items.Count = 0 Then
            Return ""
        Else
            dt = Session("WorkingDS").Tables("FldsSelected")
            For i = 0 To lbxSelected.Items.Count - 1
                'Check if the current item is a course group
                Dim arows As DataRow() = dt.Select("ReqId = '" & lbxSelected.Items(i).Value.ToString & "' ")
                If arows.Length > 0 Then
                    If arows(0)("ReqTypeId") = 2 Then
                        'Check if the course exists in the course group
                        If fac.DoesReqExistsInCourseGroup(lbxAvailCourses.SelectedItem.Value.ToString, lbxSelected.Items(i).Value.ToString) Then
                            Return lbxSelected.Items(i).Text
                        End If
                    End If
                End If

            Next

            Return ""
        End If

    End Function

    Private Function BuildReqInfoCollection() As ArrayList
        Dim arrReqInfo As New ArrayList
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim rInfo As New ReqInfo

        'Get the dataset from session
        ds = Session("WorkingDS")
        'retrieve the datatable from the ds
        dt = ds.Tables("FldsSelected")

        If lbxSelected.Items.Count > 0 Then
            For i = 0 To lbxSelected.Items.Count - 1
                'Find each item in the dt datatable and get the relevant properties
                Dim arows As DataRow() = dt.Select("ReqId = '" & lbxSelected.Items(i).Value.ToString & "' ")
                rInfo.ReqTypeId = arows(0)("ReqTypeId")
                rInfo.ReqId = arows(0)("ReqId").ToString()
                rInfo.ReqDescription = lbxSelected.Items(i).Text
                'Add the object to the arraylist
                arrReqInfo.Add(rInfo)
            Next
        End If

        Return arrReqInfo
    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
