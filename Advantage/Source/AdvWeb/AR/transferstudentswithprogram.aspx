﻿<%@ Page Title="Transfer Student To Different Program" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="TransferStudentsWithProgram.aspx.vb" Inherits="AR_transferstudentswithprogram" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/JavaScript">

        function fnSelectAll() {
            var obj = document.getElementById("ContentMain1_dgrdTransactionSearch").all.tags("input"); //ID of the Datagrid
            var blnFlag = true; //To check or to uncheck
            var chk = document.getElementById("chkAllItems"); //ID of Checkbox at Datagrid header
            if (chk.checked == true) { blnFlag = true; }
            else { blnFlag = false; }
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].type == "checkbox") {
                    obj[i].checked = blnFlag;
                }
            }
        }
        function SelectAll() {
            var obj = document.getElementById("ContentMain1_dgrdTransactionSearch").all.tags("input"); //ID of the Datagrid
            var blnFlag = true; //To check or to uncheck
            var chk = document.getElementById("chkAllItems"); //ID of Checkbox at Datagrid header
            if (chk.checked == true) { blnFlag = true; }
            else { blnFlag = false; }
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].type == "checkbox") {
                    obj[i].checked = True;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            style="align-content: center;">
                            <tr>
                                <td class="detailsframe">
                                    <FAME:StudentSearch ID="StudSearch1" runat="server" ShowTerm="false" ShowAcaYr="false" OnTransferToParent="TransferToParent" />
                                    <div class="boxContainer">

                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <table cellpadding="0" cellspacing="0" width="60%" style="align-content: center; vertical-align: top;">

                                            <tr>
                                                <td class="fourcolumnlabelcell" style="text-align: left;">
                                                    <asp:Label ID="lblEnrollDate" Style="margin-left: 3px;" CssClass="label" runat="server" Text="Enroll Date"></asp:Label>
                                                    <telerik:RadDatePicker ID="txtEnrollDate" Style="margin-left: 41px;" MinDate="1/1/1945" runat="server">
                                                    </telerik:RadDatePicker>
                                                </td>

                                                <td class="fourcolumncontentcell" style="text-align: left;"></td>


                                                <td class="fourcolumnlabelcell" style="padding-left: 20px;">
                                                    <asp:Label ID="lblExpStartDate" CssClass="label" runat="server" Text="Expected Start Date"></asp:Label>
                                                </td>
                                                <td class="fourcolumncontentcell" style="text-align: left;">

                                                    <telerik:RadDatePicker Style="margin-left: 5px;" ID="txtExpStartDate" MinDate="1/1/1945" runat="server">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fourcolumnlabelcell" align="left">
                                                    <asp:Label ID="lblShiftId" Style="margin-left: 3px;" CssClass="label" runat="server" Text="Shift"></asp:Label>
                                                    <asp:DropDownList ID="ddlShiftId" Style="margin-left: 78px;" Width="200px" CssClass="dropdownlist" runat="Server" TabIndex="1"></asp:DropDownList>
                                                </td>
                                                <td class="fourcolumncontentcell" style="text-align: left"></td>
                                                <td class="fourcolumnlabelcell" style="padding-left: 20px">
                                                    <asp:Label ID="lblExpGradDate" CssClass="label" runat="server" Text="Expected Grad Date"></asp:Label>
                                                </td>
                                                <td class="fourcolumncontentcell" style="text-align: left">

                                                    <telerik:RadDatePicker Style="margin-left: 5px;" ID="txtExpGradDate" MinDate="1/1/1945" runat="server">
                                                    </telerik:RadDatePicker>
                                                </td>

                                                <td class="fourcolumnlabelcell" style="padding-left: 20px;"></td>
                                                <td class="fourcolumncontentcell" style="text-align: left;"></td>

                                            </tr>
                                        </table>
                                        <table style="align-content: center; vertical-align: top; width: 80%">
                                            <tr>
                                                <td style="width: 50%; padding: 16px; vertical-align: top;">

                                                    <fieldset style="margin: 0; padding: 0 12px 12px 12px; border: 1px solid #e0e0e0">
                                                        <legend class="font-blue">Transfer Courses From</legend>
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCampusFrom" runat="Server" CssClass="label">Campus:</asp:Label>
                                                                </td>
                                                                <td class="contentcell4" align="left">
                                                                    <asp:Label ID="lblSourceCampus" runat="server" CssClass="label" Style="text-align: left;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblProgramFrom" runat="Server" CssClass="label">Program:</asp:Label>
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:TextBox ID="lblSourceProgramName" Width="300px" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblProgramVerFrom" runat="Server" CssClass="label">Program Version:</asp:Label>
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:TextBox ID="lblSourcePrgVerName" Width="300px" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2"></td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>

                                                </td>
                                                <td style="width: 50%; padding: 16px; vertical-align: top;">
                                                    <fieldset style="margin: 0; padding: 0 12px 12px 12px; border: 1px solid #e0e0e0">
                                                        <legend  class="font-blue">Transfer Courses To</legend>
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblCampusTo" runat="Server" CssClass="label">Campus:<span style="color: #b71c1c">*</span>
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:DropDownList ID="ddlTargetCampusId" Width="270px" runat="server" CssClass="dropdownlist" Enabled="true" TabIndex="6" AutoPostBack="true">
                                                                        <asp:ListItem>Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="None" ErrorMessage="Select the Campus"
                                                                        ControlToValidate="ddlTargetCampusId"> Select</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblProgramTo" runat="Server" CssClass="label">Program:<span style="color: #b71c1c">*</span>
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:DropDownList ID="ddlTargetProgramId" Width="270px" runat="server" AutoPostBack="true" CssClass="dropdownlist" Enabled="true" TabIndex="7">
                                                                        <asp:ListItem>Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None" ErrorMessage="Select the Program"
                                                                        ControlToValidate="ddlTargetProgramId"> Select</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblProgramVerTo" runat="Server" CssClass="label">Program Version:<span style="color: #b71c1c">*</span>
                                                                    </asp:Label>
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:DropDownList ID="ddlTargetPrgVerId" Width="270px" runat="server" CssClass="dropdownlist" Enabled="true" TabIndex="8" AutoPostBack="true">
                                                                        <asp:ListItem>Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None" ErrorMessage="Select the Program version"
                                                                        ControlToValidate="ddlTargetPrgVerId"> Select</asp:RequiredFieldValidator>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblTransferHours" Visible="false" runat="Server" CssClass="label">Transfer Hours:</asp:Label>
                                                                </td>
                                                                <td class="contentcell4">
                                                                    <asp:TextBox CssClass="textbox" Visible="false" ID="txtTransferHours" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>

                                                    <br />

                                                    <asp:CheckBox ID="chkTransferAllPrograms" CssClass="checkbox" runat="server" Text="Transfer All Courses" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center; width: 100%; padding-bottom: 16px">
                                                    <asp:Button ID="btnGetCourses" runat="server" Text="Get Courses" TabIndex="9" CausesValidation="False" />
                                                    <asp:Button ID="btnTransferAllCourses" runat="Server" Text="Transfer Courses" Visible="false" TabIndex="9" CausesValidation="False" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlListOfCourses" runat="server" Visible="true">
                                            <table width="100%" class="contenttable" style="align-content: center;">
                                                <tr>
                                                    <td class="contentcellheader">
                                                        <div style="width: 80%; float: left">
                                                            <asp:Label ID="label3" runat="server" Font-Bold="true" CssClass="label">List Of Common Courses</asp:Label>
                                                        </div>
                                                        <div style="width: 20%;">
                                                            <asp:Button ID="btnTransferCourses" runat="Server" Text="Transfer Courses" Enabled="false" />
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="padding-top: 8px">
                                                        <asp:DataGrid ID="dgrdTransactionSearch" runat="server" Width="100%" EditItemStyle-Wrap="false"
                                                            HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False" BorderWidth="1px"
                                                            BorderStyle="Solid" BorderColor="#ebebeb" CellPadding="3">
                                                            <FooterStyle CssClass="label"></FooterStyle>
                                                            <EditItemStyle Wrap="False" CssClass="label"></EditItemStyle>
                                                            <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="Term Start Date">
                                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStartDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Code">
                                                                    <HeaderStyle CssClass="datagridheader" Width="8%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCode" Text='<%# Container.DataItem("Code") %>' CssClass="label"
                                                                            runat="server">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Course">
                                                                    <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="GrdBkLinkButton" Text='<%# Container.DataItem("Descrip")%>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Credits Attempted">
                                                                    <HeaderStyle CssClass="datagridheader" Width="5%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCredits" Text='<%# Container.DataItem("Credits") %>' CssClass="label"
                                                                            runat="server">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Earned Grade">
                                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOldGrade" Text='<%# Container.DataItem("Grade") %>' runat="server"
                                                                            CssClass="label">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="New Grade">
                                                                    <HeaderStyle CssClass="datagridheader" Width="28%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlEditGrdSysDetailId" runat="server" CssClass="label">
                                                                        </asp:DropDownList>
                                                                        <asp:Label ID="lblReqId" runat="server" Text='<%# Container.DataItem("ReqId") %>'
                                                                            Visible="False" />
                                                                        <asp:Label ID="lblTestId" runat="server" Text='<%# Container.DataItem("TestId") %>'
                                                                            Visible="False">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lblResultId" runat="server" Text='<%# Container.DataItem("ResultId") %>'
                                                                            Visible="False">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lblIsCourseCompleted" runat="server" Text='<%# Container.DataItem("IsCourseCompleted") %>'
                                                                            Visible="False">
                                                                        </asp:Label>
                                                                        <asp:TextBox ID="txtGrdSysDetailId" runat="server" Text='<%# Container.DataItem("GrdSysDetailId") %>'
                                                                            Visible="False" />
                                                                        <asp:TextBox ID="txtTermDescrip" runat="Server" Text='<%# Container.DataItem("TermDescrip") %>' Visible="false" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Transfer Term">
                                                                    <HeaderStyle CssClass="datagridheader" Width="18%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlEditTermId" runat="server" CssClass="dropdownlist">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn>
                                                                    <HeaderStyle CssClass="datagridheader" Width="5%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <HeaderTemplate>
                                                                        <input id="chkAllItems" type="checkbox" onclick="fnSelectAll()" checked="checked" />Select All
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkTransfer" runat="server" Text='' Width="100px" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:Label ID="lblCommonCourses" CssClass="labelbold" Visible="false" runat="Server">There are no common courses available</asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>

            <asp:TextBox ID="txtStudentName" Visible="false" TabIndex="1" runat="server" CssClass="textboxdate" Enabled="False"></asp:TextBox>
            <asp:TextBox ID="txtStuEnrollmentId" runat="server" CssClass="textbox" Width="0px" Visible="false">StuEnrollmentId</asp:TextBox>
            <asp:TextBox ID="txtStuEnrollment" TabIndex="2" runat="server" CssClass="textbox" Width="0px" Visible="false">StuEnrollment</asp:TextBox>
            <asp:TextBox ID="txtAcademicYear" TabIndex="8" runat="server" CssClass="textbox" Width="0px" Visible="false">AcademicYear</asp:TextBox>
            <asp:TextBox ID="txtStudentId" runat="server" CssClass="textbox" Width="0px" Visible="false">AcademicYearId</asp:TextBox>
            <asp:TextBox ID="txtTerm" TabIndex="9" runat="server" CssClass="textbox" Width="0px" Visible="false">Term</asp:TextBox>
            <asp:TextBox ID="txtTermId" runat="server" CssClass="textbox" Width="0px" Visible="false">TermId</asp:TextBox>
            <asp:TextBox ID="lblSourcePrgVerId" runat="server" CssClass="textbox" Width="0px" Visible="false"></asp:TextBox>
            <asp:TextBox ID="lblSourceProgramId" runat="server" CssClass="textbox" Width="0px" Visible="false"></asp:TextBox>

            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

