﻿

Imports System.Data
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade

Partial Class AR_PrgVersionDefaultCharge
    Inherits System.Web.UI.Page
    Dim prgVerId As String
    Private pObj As New UserPagePermissionInfo
    Public dtTransCodes As DataTable
    Public dtFeeLevels As DataTable
    Public DefaultChargesTable As DataTable
    Dim campusId As String
    Dim userId As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim facade As New ProgFacade
        prgVerId = Request.QueryString("PrgVerId")

        If Not Page.IsPostBack Then


            Dim advantageUserState As New BO.User()
            advantageUserState = AdvantageSession.UserState
            Dim resourceId As Integer
            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            If Request.QueryString("cmpid") <> "" Then
                ViewState("CampusId") = Request.QueryString("cmpid")
                campusId = ViewState("CampusId")
            End If
            If Request.QueryString("PrgVer") <> "" Then
                ViewState("ProgDesc") = Request.QueryString("PrgVer")
                'txtProgDescrip.Text = ViewState("ProgDesc")
            End If

            campusId = AdvantageSession.UserState.CampusId.ToString
            userId = AdvantageSession.UserState.UserId.ToString
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
            InitButtonsForLoad()

            DefaultChargesTable = facade.GetDefaultChargesForProgVersion(prgVerId)
            ViewState("DefaultChargesTable") = DefaultChargesTable
            With RadGrdDefaultCharges
                .DataSource = DefaultChargesTable
                .DataBind()
            End With

            dtTransCodes = (New ProgFacade).GetAllSysTransCodes("true")
            ViewState("dtTransCodes") = dtTransCodes
            ViewState("dtTransCodesOriginalTable") = dtTransCodes
            'get the Rooms for the grid dropdown
            dtFeeLevels = (New ProgFacade).GetAllFeeLevels()
            ViewState("dtFeeLevels") = dtFeeLevels

        Else
            dtTransCodes = ViewState("dtTransCodes")
            dtFeeLevels = ViewState("dtFeeLevels")
        End If

    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then

            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If


    End Sub

    Protected Sub RadGrdDefaultCharges_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdDefaultCharges.DeleteCommand

        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        'Get the primary key value using the DataKeyValue.
        Dim PrgVerPmtId As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("PrgVerPmtId").ToString()
        Dim deletedRow() As Data.DataRow
        deletedRow = CType(ViewState("DefaultChargesTable"), DataTable).Select("PrgVerPmtId='" + PrgVerPmtId + "'")

        'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
        deletedRow(0)("CmdType") = 3
        e.Canceled = True
        RadGrdDefaultCharges.Rebind()


        ''e.Canceled = True
        ''Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        ' ''Get the primary key value using the DataKeyValue.
        ''Dim PrgVerPmtId As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("PrgVerPmtId").ToString()
        ''Dim deletedRow() As Data.DataRow
        ''deletedRow = CType(ViewState("DefaultChargesTable"), DataTable).Select("PrgVerPmtId='" + PrgVerPmtId + "'")
        'CType(ViewState("DefaultChargesTable"), DataTable).Rows(e.Item.ItemIndex).Delete()
        ''cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
        ''e.Canceled = True
        ' ''deletedRow.
        'CType(ViewState("DefaultChargesTable"), DataTable).AcceptChanges()
        'e.Canceled = True
        'RadGrdDefaultCharges.Rebind()
    End Sub

    Protected Sub RadGrdDefaultCharges_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdDefaultCharges.InsertCommand
        Dim insertedItem As GridDataInsertItem = DirectCast(e.Item, GridDataInsertItem)
        dtTransCodes = ViewState("dtTransCodes")
        If dtTransCodes.Rows.Count = 0 Then
            DisplayErrorMessage("There are no more Charges to select and add ")
            Exit Sub
        End If


        If ViewState("DefaultChargesTable") Is Nothing Then
            ViewState("DefaultChargesTable") = GetTable.Clone()
        End If


        Dim insertedRow As Data.DataRow = CType(ViewState("DefaultChargesTable"), DataTable).NewRow

        insertedRow("PrgVerPmtID") = Guid.NewGuid.ToString()
        insertedRow("FeeLevelID") = TryCast(insertedItem("DefaultPeriod").Controls(1), DropDownList).SelectedItem.Value
        insertedRow("FeeDescription") = TryCast(insertedItem("DefaultPeriod").Controls(1), DropDownList).SelectedItem.Text
        insertedRow("PrgVerID") = prgVerId
        insertedRow("PeriodCanChange") = TryCast(insertedItem("PeriodCanChange").Controls(1), CheckBox).Checked
        insertedRow("SysTransCodeID") = TryCast(insertedItem("SystemTransCode").Controls(1), DropDownList).SelectedItem.Value
        insertedRow("TransDescription") = TryCast(insertedItem("SystemTransCode").Controls(1), DropDownList).SelectedItem.Text
        insertedRow("CmdType") = 1

        CType(ViewState("DefaultChargesTable"), DataTable).Rows.Add(insertedRow)
        CType(ViewState("DefaultChargesTable"), DataTable).AcceptChanges()


        e.Canceled = True
        RadGrdDefaultCharges.MasterTableView.IsItemInserted = False
        RadGrdDefaultCharges.Rebind()
    End Sub

    Protected Sub RadGrdDefaultCharges_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdDefaultCharges.ItemCommand

        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            e.Canceled = True

            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            'newValues("FeeLevelID") = 2
            'newValues("SysTransCodeID") = 2
            newValues("CmdType") = 1
            ''Insert the item and rebind
            e.Item.OwnerTableView.InsertItem(newValues)
        End If

    End Sub

    Protected Sub RadGrdDefaultCharges_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrdDefaultCharges.ItemDataBound
        Try
            Dim Transcode As Integer
            If (TypeOf e.Item Is GridDataItem) AndAlso e.Item.IsInEditMode Then
                dtTransCodes = ViewState("dtTransCodes")
                Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                Dim drrowtoAdd As DataRow
                If Not TypeOf e.Item Is GridDataInsertItem Then
                    If RadGrdDefaultCharges.Items.Count >= 0 Then

                        Transcode = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(0).ToString()
                        If dtTransCodes.Select("SysTransCodeId=" & Transcode).Length = 0 Then
                            For Each rowtoAdd As DataRow In CType(ViewState("dtTransCodesOriginalTable"), DataTable).Select("SysTransCodeId=" & Transcode)
                                drrowtoAdd = dtTransCodes.NewRow
                                drrowtoAdd(0) = rowtoAdd(0)
                                drrowtoAdd(1) = rowtoAdd(1)
                                dtTransCodes.Rows.Add(drrowtoAdd)
                            Next
                        End If
                        dtTransCodes.AcceptChanges()
                        ViewState("dtTransCodes") = dtTransCodes

                    End If
                End If
                'Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                Dim dropDownList As DropDownList = DirectCast(gridEditFormItem("DefaultPeriod").FindControl("ddlDefaultPeriod"), DropDownList)
                '  DropDownList = DirectCast(gridEditFormItem("DefaultPeriod").FindControl("ddlDefaultPeriod"), DropDownList)
                If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                    dropDownList.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(1).ToString()
                End If
                dropDownList.DataBind()
                dropDownList = DirectCast(gridEditFormItem("SystemTransCode").FindControl("ddlSysTransCodes"), DropDownList)
                dtTransCodes = ViewState("dtTransCodes")
                If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                    dropDownList.Items.Clear()
                    dropDownList.DataSource = dtTransCodes
                    dropDownList.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(0).ToString()
                End If
                dropDownList.DataBind()
            End If

            If (TypeOf e.Item Is GridCommandItem) Then
                Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
                cmditm.FindControl("RefreshButton").Visible = False
                '  cmditm.FindControl("RebindGridButton").Visible = False

                If btnSave.Enabled = False Then
                    'to hide AddNewRecord button
                    cmditm.FindControl("InitInsertButton").Visible = False
                    cmditm.FindControl("AddNewRecordButton").Visible = False
                End If
            End If


        Finally

        End Try
    End Sub

    Protected Sub RadGrdDefaultCharges_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrdDefaultCharges.NeedDataSource
        Dim dt As New DataTable


        If ViewState("DefaultChargesTable") Is Nothing Then
            RadGrdDefaultCharges.DataSource = dt
        Else
            If CType(ViewState("DefaultChargesTable"), DataTable).Rows.Count > 0 Then
                RadGrdDefaultCharges.DataSource = CType(ViewState("DefaultChargesTable"), DataTable).Select("CmdType not in (3)")
                Dim drrowtoAdd As DataRow
                dtTransCodes = ViewState("dtTransCodes")
                Dim TransCode As Integer
                Dim i As Integer
                For Each row As DataRow In CType(ViewState("DefaultChargesTable"), DataTable).Select("CmdType  in (3)")
                    TransCode = row("SysTransCodeID")
                    If dtTransCodes.Select("SysTransCodeId=" & TransCode).Length = 0 Then
                        For Each rowtoAdd As DataRow In CType(ViewState("dtTransCodesOriginalTable"), DataTable).Select("SysTransCodeId=" & TransCode)
                            drrowtoAdd = dtTransCodes.NewRow
                            drrowtoAdd(0) = rowtoAdd(0)
                            drrowtoAdd(1) = rowtoAdd(1)
                            dtTransCodes.Rows.Add(drrowtoAdd)
                        Next
                    End If
                    dtTransCodes.AcceptChanges()
                Next

                For Each row As DataRow In CType(ViewState("DefaultChargesTable"), DataTable).Select("CmdType not in (3)")
                    TransCode = row("SysTransCodeID")
                    For i = dtTransCodes.Rows.Count - 1 To 0 Step -1
                        If TransCode = dtTransCodes.Rows(i).Item("SysTransCodeID") Then
                            dtTransCodes.Rows(i).Delete()
                        End If
                    Next
                    dtTransCodes.AcceptChanges()
                Next

                ViewState("dtTransCodes") = dtTransCodes

            Else
                RadGrdDefaultCharges.DataSource = dt
            End If
        End If



    End Sub

    Protected Sub RadGrdDefaultCharges_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdDefaultCharges.UpdateCommand

        Dim editedItem As GridEditableItem = TryCast(e.Item, GridEditableItem)
        ''Get the primary key value using the DataKeyValue.
        Dim PrgVerPmtId As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("PrgVerPmtId").ToString()
        Dim upDatedRow() As Data.DataRow
        upDatedRow = CType(ViewState("DefaultChargesTable"), DataTable).Select("PrgVerPmtId='" + PrgVerPmtId + "'")
        upDatedRow(0)("FeeLevelID") = TryCast(editedItem("DefaultPeriod").Controls(1), DropDownList).SelectedItem.Value
        upDatedRow(0)("FeeDescription") = TryCast(editedItem("DefaultPeriod").Controls(1), DropDownList).SelectedItem.Text
        upDatedRow(0)("PeriodCanChange") = TryCast(editedItem("PeriodCanChange").Controls(1), CheckBox).Checked
        upDatedRow(0)("SysTransCodeID") = TryCast(editedItem("SystemTransCode").Controls(1), DropDownList).SelectedItem.Value
        upDatedRow(0)("TransDescription") = TryCast(editedItem("SystemTransCode").Controls(1), DropDownList).SelectedItem.Text

        upDatedRow(0)("CmdType") = TryCast(editedItem("ContainerHidden").Controls(1), Label).Text
        'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
        If upDatedRow(0)("CmdType") <> "1" Then upDatedRow(0)("CmdType") = 2
        e.Canceled = True
        RadGrdDefaultCharges.MasterTableView.ClearEditItems()
        RadGrdDefaultCharges.Rebind()

    End Sub

    Private Function GetTable() As DataTable
        Dim tbl As New DataTable
        tbl.Columns.Add(New DataColumn("PrgVerPmtId", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("FeeLevelID", GetType(Integer)))
        tbl.Columns.Add(New DataColumn("SysTransCodeID", GetType(Integer)))
        tbl.Columns.Add(New DataColumn("PrgVerID", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("PeriodCanChange", GetType(Boolean)))
        tbl.Columns.Add(New DataColumn("TransDescription", GetType(String)))
        tbl.Columns.Add(New DataColumn("FeeDescription", GetType(String)))
        tbl.Columns.Add(New DataColumn("CmdType", GetType(Integer)))
        Return tbl
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim facade As New ProgFacade
        If CType(ViewState("DefaultChargesTable"), DataTable).Rows.Count >= 1 Then
            Try
                facade.UpdateDefaultChargesforProgramVersion(CType(ViewState("DefaultChargesTable"), DataTable), Session("UserName"))
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                DisplayErrorMessage(ex.Message)
            End Try
        End If

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
End Class
