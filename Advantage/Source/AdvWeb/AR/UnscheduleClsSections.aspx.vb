﻿Imports Fame.common
Imports System.Data
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Xml
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports Telerik.Web.UI

Partial Class UnscheduleClsSections
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim ds As New DataSet
        Dim facade As New ClassSectionFacade
        Dim objCommon As New CommonUtilities
        Dim dt As New DataTable
        'Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        'disable history button
        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            'Dim objcommon As New CommonUtilities

            'Default the Selection Criteria 
            ViewState("Term") = ""
            ViewState("Course") = ""
            ViewState("Instructor") = ""
            ViewState("MODE") = "NEW"
            ViewState("UnschedClosureId") = ""
            ViewState("SelectedIndex") = "-1"

            'PopulateTermDDL()
            txtModUser.Text = AdvantageSession.UserState.UserName
            txtModDate.Text = Date.MinValue.ToString
            ViewState("ItemSelected") = -1

            BuildDropDownLists()

            dt.TableName = "UnschedDateInfo"

            'Add a column called TblName and another called TblPK to the dtTables datatable
            Dim col2 As DataColumn = dt.Columns.Add("ClsSectionId")
            With dt
                .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
            End With
            Session("UnschedDateInfo") = dt


        Else

        End If
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
    End Sub
    'Private Sub PopulateTermDDL()
    '    '
    '    '   Get Degrees data to bind the CheckBoxList
    '    '
    '    Dim facade As New UnscheduleClsFacade

    '    ' Bind the Dataset to the CheckBoxList
    '    With ddlTerm
    '        .DataSource = facade.GetAllTerms()
    '        .DataTextField = "TermDescrip"
    '        .DataValueField = "TermId"
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub

    'Private Sub ddlTerm_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged

    '    ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
    '    If (ViewState("Term") <> "") Then
    '        PopulateCourseDDL(Viewstate("Term"))
    '    End If
    'End Sub

    'Private Sub PopulateCourseDDL(ByVal Term As String)
    '    '
    '    '   Get Degrees data to bind the CheckBoxList
    '    '
    '    Dim facade As New UnscheduleClsFacade
    '    Dim campusId As String

    '    campusId = HttpContext.Current.Request.Params("cmpid")

    '    ' Bind the Dataset to the CheckBoxList
    '    With ddlCourse
    '        .DataSource = facade.GetCourses(ViewState("Term"), campusId)
    '        .DataTextField = "Descrip"
    '        .DataValueField = "ReqId"
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select All", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub

    'Private Sub ddlCourse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCourse.SelectedIndexChanged
    '    ViewState("Course") = ddlCourse.SelectedItem.Value.ToString()
    '    If (ViewState("Course") <> "") Then
    '        PopulateInstrDDL(Viewstate("Course"))
    '    End If

    'End Sub

    'Private Sub PopulateInstrDDL(ByVal Course As String)
    '    '
    '    '   Get Degrees data to bind the CheckBoxList
    '    '
    '    Dim facade As New UnscheduleClsFacade

    '    ' Bind the Dataset to the CheckBoxList
    '    With ddlInstructor
    '        .DataSource = facade.GetInstructors(ViewState("Course"))
    '        .DataTextField = "FullName"
    '        .DataValueField = "InstructorId"
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select All", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub

    'Private Sub btnBuildList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
    '    Dim facade As New UnscheduleClsFacade
    '    Dim ds As New DataSet

    '    If ddlTerm.SelectedItem.Text = "Select" Then
    '        DisplayErrorMessage("Unable to find data. Please select an option from the term filter")
    '        Exit Sub
    '    Else
    '        ViewState("Term") = ddlTerm.SelectedItem.Value.ToString
    '    End If
    '    If ddlCourse.SelectedItem.Value <> "" Then
    '        ViewState("Course") = ddlCourse.SelectedItem.Value.ToString
    '    End If
    '    Try
    '        If ddlInstructor.SelectedItem.Value <> "" Then
    '            ViewState("Instructor") = ddlInstructor.SelectedItem.Value.ToString
    '        End If
    '    Catch

    '    End Try
    '    pnlDate.Visible = True

    '    If pObj.HasFull Or pObj.HasAdd Then
    '        btnSave.Enabled = True
    '        btnNew.Enabled = True
    '    End If

    '    'Populate datalist with values
    '    PopulateDataList("Build")
    '    ClearRHS()
    '    'clear contents of the datagrid
    '    With dgdUnschedule
    '        .DataSource = Nothing
    '        .DataBind()
    '    End With
    '    ViewState("ClsSectStdGrds") = ds
    '    'PopulateDataList("Build")
    '    'ClearRHS()
    'End Sub
    Private Sub PopulateDataList(ByVal CallingFunction As String, Optional ByVal GuidToFind As String = "", Optional ByVal ds As DataSet = Nothing)
        'Dim dv As New DataView
        'Dim ds2 As New DataSet
        'Dim campusId As String

        'campusId = HttpContext.Current.Request.Params("cmpid")

        'If ds Is Nothing Then
        '    Dim facade As New UnscheduleClsFacade
        '    ds2 = facade.PopulateDataList(ViewState("Term"), ViewState("Course"), ViewState("Instructor"), campusId)
        '    If ds2.Tables(0).Rows.Count = 0 Then
        '        With dtlUnschedDates
        '            .DataSource = Nothing
        '            .DataBind()
        '        End With
        '        Exit Sub
        '    End If
        'Else

        'End If

        'With dtlUnschedDates
        '    Select Case CallingFunction
        '        Case "Delete"
        '            dtlUnschedDates.SelectedIndex = -1
        '        Case "New"
        '            Dim myDRV As DataRowView
        '            Dim i As Integer
        '            Dim GuidInDV As String
        '            For Each myDRV In dv
        '                GuidInDV = myDRV(0).ToString
        '                If GuidToFind.ToString = GuidInDV Then
        '                    Exit For
        '                End If
        '                i += 1
        '            Next
        '            dtlUnschedDates.SelectedIndex = i
        '            ViewState("ItemSelected") = i

        '        Case "Sort"
        '            If CInt(ViewState("ItemSelected")) > -1 Then
        '                Dim myDRV As DataRowView
        '                Dim i As Integer
        '                Dim GuidInDV As String
        '                For Each myDRV In dv
        '                    GuidInDV = myDRV(0).ToString
        '                    If GuidToFind.ToString = GuidInDV Then
        '                        Exit For
        '                    End If
        '                    i += 1
        '                Next
        '                dtlUnschedDates.SelectedIndex = i
        '                ViewState("ItemSelected") = i
        '            Else
        '                dtlUnschedDates.SelectedIndex = -1
        '                ViewState("ItemSelected") = -1
        '            End If
        '        Case "Edit"
        '            dtlUnschedDates.SelectedIndex = CInt(ViewState("ItemSelected"))
        '        Case "Item"
        '            dtlUnschedDates.SelectedIndex = CInt(ViewState("ItemSelected"))
        '        Case "Build"
        '            dtlUnschedDates.SelectedIndex = -1
        '        Case "Add"
        '            dtlUnschedDates.SelectedIndex = -1
        '        Case Else
        '            dtlUnschedDates.SelectedIndex = -1
        '    End Select
        '    .DataSource = ds2
        '    .DataBind()
        'End With
    End Sub
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
            For Each ctl In pnlDate.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
            ' I have to manually clear the label for the date completed field

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub GetCancelledClasses() 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document
        Dim dsRules As New DataSet
        Dim grdFacade As New UnscheduleClsFacade
        dsRules = GetListofCancelledClasses()
        If Not dsRules Is Nothing Then
            For Each lcol As DataColumn In dsRules.Tables(0).Columns
                lcol.ColumnMapping = System.Data.MappingType.Attribute
            Next
            Dim strXML As String = dsRules.GetXml
            grdFacade.SetupRules(strXML)
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   if GrdSystem is nothing do an insert. Else do an update
        'Dim iitems As DataGridItemCollection
        'Dim iitem As DataGridItem
        'Dim i As Integer
        ''        Dim sId As String
        ''  Dim unschedule As Boolean
        'Dim dt As DataTable
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim dt2 As New DataTable
        Dim UnschedObject As New UnscheduleClsInfo
        Dim sw As New System.IO.StringWriter
        '   Dim storedGuid As String
        Dim obj As New Object
        Dim ErrStr As String = ""
        '   Dim count As Integer
        Dim Facade As New UnscheduleClsFacade
        '  Dim row As DataRow
        'Dim dr1 As DataRow
        '' Dim ClsSectId As String
        'Dim result As String
        'Dim aRows() As DataRow

        'dt = Session("UnschedDateInfo")

        GetCancelledClasses()


        'For Each item As GridDataItem In RadGrid2.Items
        '    If item.OwnerTableView.Name = "ClassSection" Then
        '        Try
        '            If (TypeOf item Is GridDataItem) Then
        '                Dim headerItem As GridDataItem = DirectCast(item, GridDataItem)
        '                Dim lControl As New LiteralControl
        '                lControl.Text = "Cancel?"
        '                Try
        '                    'headerItem("Cancel").Controls
        '                Catch ex As Exception
         '                	Dim exTracker = new AdvApplicationInsightsInitializer()
        '                	exTracker.TrackExceptionWrapper(ex)

        '                End Try
        '            End If
        '            'Ctype(DirectCast(headerItem("Cancel"),Telerik.Web.UI.GridTableCell).Controls(0),CheckBox).Checked
        '            If CType(DirectCast(item("Cancel"), Telerik.Web.UI.GridTableCell).Controls(0), CheckBox).Checked = True Then
        '                CType(item.FindControl("txtCancelFrom"), RadDatePicker).SelectedDate = CDate(txtSDate.Text)
        '                CType(item.FindControl("txtCancelTo"), RadDatePicker).SelectedDate = CDate(txtEDate.Text)
        '                CType(item.FindControl("txtRescheduleTo"), RadDatePicker).SelectedDate = CDate(txtEDate.Text)
        '            End If
        '        Catch ex As Exception
         '        	Dim exTracker = new AdvApplicationInsightsInitializer()
        '        	exTracker.TrackExceptionWrapper(ex)

        '        End Try
        '    End If
        'Next


        ' Save the datagrid items in a collection.
        'iitems = dgdUnschedule.Items
        'Try

        '    'Loop thru the collection to retrieve the chkbox value
        '    For i = 0 To iitems.Count - 1
        '        iitem = iitems.Item(i)
        '        'retrieve clssection id from the datagrid
        '        'ClsSectId = CType(iitem.FindControl("txtClsSectionId"), TextBox).Text()

        '        If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True) Then

        '            'find if this clssectid exists in the datatable
        '            'Dim row1 As DataRow = dt.Rows.Find(ClsSectId)
        '            'If (row1 Is Nothing) Then 'Doesnt exist in the datatable. So,add it.

        '            'add to the datatable.
        '            Dim dr As DataRow = dt.NewRow
        '            dr("ClsSectionId") = CType(iitem.FindControl("txtClsSectionId"), TextBox).Text
        '            aRows = dt.Select("ClsSectionId = '" & dr("ClsSectionId").ToString() & "'")
        '            If (aRows.Length = 0) Then
        '                dt.Rows.Add(dr)
        '            Else

        '            End If


        '            'End If

        '            'Else 'if it's unchecked.

        '            'find if this clssectid exists in the datatable
        '            'Dim row1 As DataRow = dt.Rows.Find(ClsSectId)
        '            'If Not (row1 Is Nothing) Then 'Exists in datatable
        '            'Mark the row as deleted
        '            'row1.Delete()
        '            'End If
        '        End If

        '    Next
        '    'loop thru the datatable to see which rows have been added.
        '    dt2 = dt.GetChanges(DataRowState.Added Or DataRowState.Deleted)
        '    If Not IsNothing(dt2) Then
        '        For Each dr1 In dt2.Rows
        '            If dr1.RowState = DataRowState.Added Then
        '                'add row
        '                'Move the values in the controls over to the object
        '                UnschedObject.ClsSectId = dr1("ClsSectionId")
        '                UnschedObject.StartDate = txtSDate.Text
        '                UnschedObject.EndDate = txtEDate.Text
        '                'UnschedObject = PopulateUnschedObject(unschedule)
        '                'get ModUser
        '                UnschedObject.ModUser = txtModUser.Text

        '                'get ModDate
        '                UnschedObject.ModDate = Date.Parse(txtModDate.Text)
        '                'Add the unscheduled date to the db.
        '                Facade.InsertUnschedDate(UnschedObject, Session("UserName"))

        '            ElseIf dr1.RowState = DataRowState.Deleted Then

        '                UnschedObject.ClsSectId = dr1("ClsSectionId", DataRowVersion.Original).ToString
        '                UnschedObject.StartDate = txtSDate.Text
        '                UnschedObject.EndDate = txtEDate.Text
        '                'delete row
        '                result = Facade.DeleteUnschedDate(UnschedObject)
        '                If Not result = "" Then
        '                    '   Display Error Message
        '                    DisplayErrorMessage(result)
        '                End If
        '            End If
        '        Next
        '    End If
        '    dt.AcceptChanges()
        '    Session("UnschedDateInfo") = dt

        '    'enable the new button
        '    btnNew.Enabled = True
        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    'Redirect to error page.
        '    If ex.InnerException Is Nothing Then
        '        Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
        '    Else
        '        Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
        '    End If
        '    Response.Redirect("../ErrorPage.aspx")
        'End Try


    End Sub




    'Public Sub dtlUnschedDates_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dtlUnschedDates.ItemCommand
    '    '    Dim ds As New DataSet
    '    '    Dim facade As New UnscheduleClsFacade
    '    '    Dim CommonUtilities As New CommonUtilities
    '    '    Dim UnschedClosureId As New Guid
    '    '    Dim sDate As String
    '    '    Dim eDate As String
    '    '    Dim dt As New DataTable
    '    '    Dim dt2 As New DataTable
    '    '    Dim start As LinkButton
    '    '    Dim end1 As LinkButton
    '    '    Dim storeddt As DataTable
    '    '    Dim iitems As DataGridItemCollection
    '    '    Dim iitem As DataGridItem
    '    '    Dim i As Integer
    '    '    Dim ClsSectId As String

    '    '    ViewState("ItemSelected") = e.Item.ItemIndex.ToString
    '    '    'UnschedClosureId = dtlUnschedDates.DataKeys(e.Item.ItemIndex)
    '    '    'ViewState("UnschedClosureId") = dtlUnschedDates.DataKeys(e.Item.ItemIndex)

    '    '    start = CType(e.Item.FindControl("Linkbutton1"), LinkButton)
    '    '    end1 = CType(e.Item.FindControl("Linkbutton2"), LinkButton)

    '    '    txtSDate.Text = start.Text
    '    '    txtEDate.Text = end1.Text

    '    '    ds = facade.PopulateDataGridOnItemCmd(txtSDate.Text, txtEDate.Text)

    '    '    dt = ds.Tables("Unschedule")
    '    '    'Make the ChildId column the primary key
    '    '    With dt
    '    '        .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
    '    '    End With

    '    '    dt2 = ds.Tables("UnschedClsSects")
    '    '    'Make the ChildId column the primary key
    '    '    With dt2
    '    '        .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
    '    '    End With

    '    '    With dgdUnschedule
    '    '        .DataSource = dt
    '    '        .DataBind()
    '    '    End With

    '    '    ' Save the datagrid items in a collection.
    '    '    iitems = dgdUnschedule.Items

    '    '    'Loop thru the collection to retrieve the chkbox value
    '    '    For i = 0 To iitems.Count - 1
    '    '        iitem = iitems.Item(i)
    '    '        'retrieve clssection id from the datagrid
    '    '        ClsSectId = CType(iitem.FindControl("txtClsSectionId"), TextBox).Text()

    '    '        'find if this clssectid exists in the datatable
    '    '        Dim row1 As DataRow = dt2.Rows.Find(ClsSectId)
    '    '        If Not (row1 Is Nothing) Then 'exists in the datatable. So,add it.

    '    '            CType(iitem.FindControl("ChkUnschedule"), CheckBox).Checked = True

    '    '        Else 'if it's unchecked.

    '    '        End If
    '    '    Next
    '    '    storeddt = Session("UnschedDateInfo")
    '    '    storeddt = dt2.Copy

    '    '    Session("UnschedDateInfo") = storeddt

    '    '    'enable the new button
    '    '    btnNew.Enabled = True

    '    '    'make the get cls sections button invisible
    '    '    btnGetClsSects.Visible = False

    '    '    'selIndex = e.Item.ItemIndex
    '    '    'dtlUnschedDates.SelectedIndex = selIndex
    'End Sub

    Private Sub btnGetClsSects_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetClsSects.Click
        Dim facade As New CoursesFacade
        Dim sDate As String
        Dim eDate As String
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        Dim campusID As String

        Try
            sDate = txtSDate.SelectedDate
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            sDate = Nothing
        End Try

        Try
            eDate = txtEDate.SelectedDate
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            eDate = Nothing
        End Try
        'sDate = txtSDate.SelectedDate
        'eDate = txtEDate.SelectedDate
        If sDate = Nothing Or eDate = Nothing Then
            'DisplayErrorMessage("Class cancellation start and end dates are required")
            Dim strMessage As String = "Class cancellation start date and end date is required"
            radWindowManager1.RadAlert(strMessage, 400, 100, "Message", "")
            Exit Sub
        End If
        campusID = HttpContext.Current.Request.Params("cmpid")
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnNew.Enabled = True
        End If

        RadGrid2.Visible = True

        'ds = facade.PopulateDataGrid(sDate, eDate, campusID)

        'With dgdUnschedule
        '    .DataSource = ds
        '    .DataBind()
        'End With
        'ViewState("Unschedule") = ds.Tables("Unschedule")

        'ds = facade.GetClassSections(sDate, eDate, campusID, ddlInstructor.SelectedValue.ToString, ddlCourse.SelectedValue.ToString, ddlTerm.SelectedValue.ToString)
        ds1 = facade.GetClassSectionsAndMeetings(sDate, eDate, campusID, ddlInstructor.SelectedValue.ToString, ddlCourse.SelectedValue.ToString, ddlTerm.SelectedValue.ToString)
        'With dgdUnschedule
        '    .DataSource = ds
        '    .DataBind()
        'End With
        'With RadGrid1
        '    .DataSource = ds
        '    .DataBind()
        'End With
        ' dgdUnschedule.Visible = False
        With RadGrid2
            .DataSource = ds1
            .DataBind()
        End With

        RadGrid2.MasterTableView.VirtualItemCount = (RadGrid2.Items.Count / RadGrid2.PageSize)
        ViewState("Unschedule") = ds1.Tables(0)

    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        txtSDate.Clear()
        txtEDate.Clear()
        ddlTerm.SelectedIndex = 0
        ddlCourse.SelectedIndex = 0
        ddlInstructor.SelectedIndex = 0

        'make the get cls sections button visible
        btnGetClsSects.Visible = True
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip("ddlTerm")
        BindToolTip("ddlCourse")
        BindToolTip("ddlInstructor")


    End Sub
    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        populateTerms(ddlTerm)
        populateCourses(ddlCourse)
        populateInstructors()
    End Sub
    Private Sub populateTerms(ByVal ddl As Telerik.Web.UI.RadComboBox)
        Dim dt As New DataTable
        dt = (New TermsFacade).GetAllTerms_SP(True, campusId)
        ddl.Items.Clear()
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select Term"
        item1.Value = ""
        ddl.Items.Add(item1)
        ddl.DataSource = dt
        ddl.DataBind()

    End Sub
    Private Sub populateCourses(ByVal ddl As Telerik.Web.UI.RadComboBox)
        Dim dt As New DataTable
        dt = (New CoursesFacade).GetAllCourses_SP(True, campusId)
        ddl.Items.Clear()
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select Course"
        item1.Value = ""
        ddl.Items.Add(item1)
        ddl.DataSource = dt
        ddl.DataBind()
    End Sub
    Private Sub populateInstructors()
        Dim dt As New DataTable
        dt = (New UserSecurityFacade).GetInstructor_SP()

        With ddlInstructor
            .Items.Clear()
        End With
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select Instructor"
        item1.Value = ""
        ddlInstructor.Items.Add(item1)
        ddlInstructor.DataSource = dt
        ddlInstructor.DataBind()
    End Sub
    Protected Sub ddlTerm_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlTerm.SelectedIndexChanged
        If ddlTerm.SelectedIndex > 0 Then
            populateCoursesFromClassSectionsForTerm(ddlTerm.SelectedValue.ToString, ddlCourse)
        End If
    End Sub
    Private Sub populateCoursesFromClassSectionsForTerm(ByVal TermId As String, ByVal ctl As Telerik.Web.UI.RadComboBox)
        ctl.Items.Clear()
        Dim dt As New DataTable
        If TermId = "" Then
            dt = (New CoursesFacade).GetAllCourses_SP(True, campusId)
        Else
            dt = (New CoursesFacade).GetCoursesFromClassSectionsForTerm_SP(TermId)
        End If
        Dim item1 As New Telerik.Web.UI.RadComboBoxItem()
        item1.Text = "Select Course"
        item1.Value = ""
        ctl.Items.Add(item1)
        ctl.DataSource = dt
        ctl.DataBind()
    End Sub
    Protected Sub RadGrid2_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid2.ItemCreated
        If (TypeOf e.Item Is GridHeaderItem) Then
            Dim headerItem As GridHeaderItem = DirectCast(e.Item, GridHeaderItem)
            Dim lControl As New LiteralControl
            lControl.Text = "Cancel ?"
            Try
                headerItem("Cancel").Controls.Add(lControl)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            Dim lControl1 As New LiteralControl
            lControl1.Text = "Reschedule ?"
            Try
                headerItem("TemplateEditColumn").Controls.Add(lControl1)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End If
        If TypeOf e.Item Is GridDataItem Then
            Dim editCheck As CheckBox = DirectCast(e.Item.FindControl("chkReschedule"), CheckBox)
            editCheck.Attributes("href") = "#"

            'Response.Write(editCheck.Checked)
            'If editCheck.Checked = True Then
            editCheck.Attributes("onclick") = [String].Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("ClsSectionId"), e.Item.ItemIndex)
            'Else
            '   editCheck.Checked = False
            'End If

            'Dim editLink As HyperLink = DirectCast(e.Item.FindControl("EditLink"), HyperLink)
            'editLink.Attributes("href") = "#"
            'editLink.Attributes("onclick") = [String].Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("ClsSectionId"), e.Item.ItemIndex)

            'ScheduleConflictWindow.Visible = True
            'ScheduleConflictWindow.Windows(0).NavigateUrl = "EditFormVB.aspx?ClsSectionId=" + e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("ClsSectionId")
            'ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = True
        End If

    End Sub
    'Sub LoopHierarchyRecursive(ByVal gridTableView As GridTableView)
    '    For Each nestedViewItem As GridDataItem In gridTableView.GetItems(GridItemType.NestedView)
    '        'you should skip the items if not expanded, or tables not bound
    '        If nestedViewItem.DataItem.Length > 0 Then
    '            'now you can access: nestedViewItem.NestedTableViews[0].Items, which will be the DataItems of this nested table
    '            'then make recursive call
    '            Response.Write(nestedViewItem.DataItem(0).Items)
    '            LoopHierarchyRecursive(nestedViewItem.DataItem(0))
    '            ' above [0] stands for the first table in the hierarchy, since Telerik RadGrid supports multiple tables at a level
    '        End If
    '    Next
    'End Sub
    Protected Sub RadGrid2_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid2.ItemDataBound
        For Each item As GridDataItem In RadGrid2.Items
            If item.OwnerTableView.Name = "ClassSection" Then
                CType(item.FindControl("txtCancelFrom"), RadDatePicker).SelectedDate = CDate(txtSDate.SelectedDate)
                CType(item.FindControl("txtCancelTo"), RadDatePicker).SelectedDate = CDate(txtEDate.SelectedDate)
            End If
        Next
        'If (TypeOf e.Item Is GridHeaderItem) Then
        '    Dim headerItem As GridHeaderItem = DirectCast(e.Item, GridHeaderItem)
        '    Dim lControl As New LiteralControl
        '    lControl.Text = "Cancel?"
        '    Try
        '        headerItem("Cancel").Controls.Add(lControl)
        '    Catch ex As Exception
         '    	Dim exTracker = new AdvApplicationInsightsInitializer()
        '    	exTracker.TrackExceptionWrapper(ex)

        '    End Try
        'End If
    End Sub
    Private Function GetListofCancelledClasses() As DataSet
        Dim tbl As New DataTable("SetupTestRules")
        Dim strGrdComponentTypeId_ReqId As String = ""
        Dim strEffectiveDate As String = ""
        Dim intNumberofTests As Integer = 0
        Dim decMinimumScore As Decimal = 0.0
        Dim strModUser As String = ""
        Dim strModDate As DateTime = Date.Now
        Dim intSelectedCount As Integer = 0
        '   Dim clsRescheduledFrom, clsRescheduledTo As Date

        '        Dim item As GridDataItem
        With tbl
            'Define table schema
            .Columns.Add("UnschedClosureId", GetType(String))
            .Columns.Add("ClsSectionId", GetType(String))
            .Columns.Add("StartDate", GetType(Date))
            .Columns.Add("EndDate", GetType(Date))
            .Columns.Add("ClassReSchduledFrom", GetType(Date))
            .Columns.Add("ClassReSchduledTo", GetType(Date))
            .Columns.Add("ModUser", GetType(String))
            .Columns.Add("ClsMeetingId", GetType(String))
            .Columns.Add("RescheduledMeetingId", GetType(String))
            .Columns.Add("RoomId", GetType(String))
            .Columns.Add("PeriodId", GetType(String))
            .Columns.Add("reschedulestartdate", GetType(Date))
            .Columns.Add("rescheduleenddate", GetType(Date))
            .Columns.Add("instructiontypeid", GetType(String))
            .Columns.Add("ismeetingrescheduled", GetType(Boolean))

            'set field properties
            .Columns("StartDate").AllowDBNull = False
            .Columns("EndDate").AllowDBNull = False
            .Columns("RescheduledMeetingId").AllowDBNull = True
        End With

        Dim clsSectionId As String
        Dim clsMeetingId As String
        Dim dtStartDate As Date
        Dim dtEndDate As Date
        Dim unScheduledId As String
        Dim dtRescheduleFromDate As Date
        Dim dtRescheduleToDate As Date
        Dim rescheduleclsmeetingid As String

        Dim roomid As String
        Dim periodid As String
        Dim reschedulestartdate As Date
        Dim rescheduleenddate As Date
        Dim instructiontypeid As String
        Dim ismeetingrescheduled As Boolean

        'GetSelectedItems method brings in a collection of rows that were selected
        For Each dataItem As GridDataItem In RadGrid2.Items
            'BR: If the rules to the datatable using LoadDataRow Method
            'Benefits:LoadDataRow method helps get control RowState for the new DataRow
            'First Parameter: Array of values, the items in the array correspond to columns in the table. 
            'Second Parameter: AcceptChanges, enables to control the value of the RowState property of the new DataRow.
            '                  Passing a value of False for this parameter causes the new row to have a RowState of Added
            If CType(DirectCast(dataItem("Cancel"), Telerik.Web.UI.GridTableCell).Controls(0), CheckBox).Checked = True Then
                clsSectionId = CType(dataItem.FindControl("lblClsSectionId"), Label).Text.ToString
                clsMeetingId = CType(dataItem.FindControl("lblClsMeetingId"), Label).Text.ToString
                dtStartDate = CDate(CType(dataItem.FindControl("txtCancelFrom"), RadDatePicker).SelectedDate)
                If Not dtStartDate = Nothing And CDate(CType(dataItem.FindControl("txtCancelTo"), RadDatePicker).SelectedDate) = Nothing Then
                    dtEndDate = dtStartDate
                Else
                    dtEndDate = CDate(CType(dataItem.FindControl("txtCancelTo"), RadDatePicker).SelectedDate)
                End If
                Try
                    CType(dataItem.FindControl("txtDetails"), Label).Text = CType(dataItem.FindControl("Hidden1"), HiddenField).Value

                    Dim intDetails As Integer = InStr(CType(dataItem.FindControl("txtDetails"), Label).Text, "-")
                    If intDetails >= 1 Then

                        dtRescheduleFromDate = CDate(Mid(CType(dataItem.FindControl("txtDetails"), Label).Text, 1, intDetails - 1))
                        dtRescheduleToDate = CDate(Mid(CType(dataItem.FindControl("txtDetails"), Label).Text, intDetails + 1))
                    Else
                        dtRescheduleFromDate = "01/01/1900"
                        dtRescheduleToDate = "01/01/1900"
                    End If
                    'dtRescheduleDate = CDate(CType(dataItem.FindControl("txtRescheduleTo"), RadDatePicker).SelectedDate)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    dtRescheduleFromDate = "01/01/1900"
                    dtRescheduleToDate = "01/01/1900"
                End Try
                strModUser = Session("UserName")
                unScheduledId = Guid.NewGuid.ToString

                Try
                    rescheduleclsmeetingid = CType(dataItem.FindControl("HiddenRescheduledDate"), HiddenField).Value
                    If rescheduleclsmeetingid.Length = 0 Then
                        rescheduleclsmeetingid = Guid.Empty.ToString
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    rescheduleclsmeetingid = Guid.Empty.ToString
                End Try

                Try
                    roomid = CType(dataItem.FindControl("roomid"), HiddenField).Value
                    If roomid.Length = 0 Then
                        roomid = System.DBNull.Value.ToString
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    roomid = System.DBNull.Value.ToString
                End Try

                Try
                    periodid = CType(dataItem.FindControl("periodid"), HiddenField).Value
                    If periodid.Length = 0 Then
                        periodid = System.DBNull.Value.ToString
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    periodid = System.DBNull.Value.ToString
                End Try
                reschedulestartdate = dtRescheduleFromDate
                rescheduleenddate = dtRescheduleToDate

                Try
                    'instructiontypeid = CType(dataItem.FindControl("instructiontypeid"), HiddenField).Value
                    instructiontypeid = CType(dataItem.FindControl("lblInstructionTypeId"), HiddenField).Value
                    If instructiontypeid.Length = 0 Then
                        instructiontypeid = Guid.Empty.ToString
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    instructiontypeid = Guid.Empty.ToString
                End Try
                Try
                    ismeetingrescheduled = CType(dataItem.FindControl("ismeetingrescheduled"), HiddenField).Value
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ismeetingrescheduled = 0
                End Try

                Dim boolCheckForCancellations As Boolean = (New UnscheduleClsFacade).CheckIfAttendancePostedOnCancelledDate(clsMeetingId, dtStartDate, dtEndDate)
                If boolCheckForCancellations = False Then 'Cancel only when no attendance was posted for the student on that date
                    tbl.LoadDataRow(New Object() {unScheduledId, clsSectionId, dtStartDate, dtEndDate, dtRescheduleFromDate, _
                                                  dtRescheduleToDate, strModUser, clsMeetingId, rescheduleclsmeetingid, _
                                                  roomid, periodid, reschedulestartdate, rescheduleenddate, _
                                                  instructiontypeid, ismeetingrescheduled}, False)

                    CType(dataItem.FindControl("lblComments"), Label).Text = "Class cancelled on the selected dates"
                    If Not dtRescheduleFromDate = "01/01/1900" Then
                        CType(dataItem.FindControl("lblComments"), Label).Text = "Class cancelled and rescheduled on the selected dates"
                    End If
                Else
                    CType(dataItem.FindControl("lblComments"), Label).Text = "Class was not cancelled as attendance was already posted on these cancellation days for this meeting schedule"
                End If
            Else
                CType(dataItem.FindControl("lblComments"), Label).Text = ""
            End If
        Next
        If tbl.Rows.Count >= 1 Then
            Dim ds As DataSet = New DataSet()
            ds.Tables.Add(tbl)
            Return ds
        Else
            Return Nothing
        End If
    End Function
    Protected Sub RadGrid2_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid2.NeedDataSource
        RadGrid2.DataSource = ViewState("Unschedule")
    End Sub
    Private Sub RescheduleClasses()

    End Sub

End Class
