' ===============================================================================
' ViewCalendar_Month
' View a calendar in a monthly view
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports System.Drawing

Partial Class Attendance_Month
    Inherits System.Web.UI.Page
    Dim UnitType As String
    Dim decSchedHours As Decimal = 0.0
    Dim decActualHours As Decimal = 0.0
    Dim decTardy As Decimal = 0.0
    Dim decMakeup As Decimal = 0.0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim strStuEnrollId As String = Request.Params("stuenrollid")
            lblStudentName.Text = Request.Params("studentname")
            lblPrgVersion.Text = " " & Request.Params("prgversion")
            ViewState("UnitType") = Request.Params("unittype")
            Dim ds As DataSet = (New AttendanceFacade).GetAttendanceByEnrollment(strStuEnrollId)
            ViewState("attendance") = ds
            'Hide the title of the calendar control
            Calendar1.ShowTitle = False

            'Populate month and year dropdown list boxes which
            'replace the original calendar title
            If Not Page.IsPostBack Then
                Call Populate_MonthList()
                Call Populate_YearList()
            End If
            ' Calendar1.VisibleDate = Now()
        End If
        'Dim currentDay As Integer = Calendar1.VisibleDate.Day
        'Dim currentMonth As Integer = Calendar1.VisibleDate.Month
        'Dim currentYear As Integer = Calendar1.VisibleDate.Year
        'Calendar1.SelectedDates.Clear()

        ' Loop through current month and add all Wednesdays to the collection.
        'Dim i As Integer
        'For i = 1 To System.DateTime.DaysInMonth(currentYear, currentMonth)
        '    Dim targetDate As New Date(currentYear, currentMonth, i)
        '    If targetDate.DayOfWeek = DayOfWeek.Wednesday Then
        '        'Calendar1.SelectedDates.Add(targetDate)
        '        Response.Write(targetDate)
        '        Response.Write("<br>")
        '    End If
        'Next i
        'Response.Write(Session("SchedHours"))
        'Response.Write("<br>")
        'Response.Write(Session("ActualHours"))
        'Response.Write("<br>")
        'Response.Write(Session("MakeupHours"))
    End Sub
    Sub Populate_MonthList()
        drpCalMonth.Items.Add("January")
        drpCalMonth.Items.Add("February")
        drpCalMonth.Items.Add("March")
        drpCalMonth.Items.Add("April")
        drpCalMonth.Items.Add("May")
        drpCalMonth.Items.Add("June")
        drpCalMonth.Items.Add("July")
        drpCalMonth.Items.Add("August")
        drpCalMonth.Items.Add("September")
        drpCalMonth.Items.Add("October")
        drpCalMonth.Items.Add("November")
        drpCalMonth.Items.Add("December")
        drpCalMonth.Items.FindByValue(MonthName(DateTime.Now.Month)).Selected = True
    End Sub
    Sub Populate_YearList()
        'Year list can be extended
        Dim intYear As Integer
        For intYear = DateTime.Now.Year - 10 To DateTime.Now.Year
            drpCalYear.Items.Add(intYear)
        Next
        drpCalYear.Items.FindByValue(DateTime.Now.Year).Selected = True
    End Sub
    Protected Sub OnDayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        Dim ds As DataSet = ViewState("attendance")
        If ds Is Nothing Then Return

        Dim lbl As New Label()
        ' add a carriage return
        lbl.Text = "<div></div>"
        e.Cell.Controls.Add(lbl)

        Dim currentDay As Integer = Calendar1.VisibleDate.Day
        Dim currentMonth As Integer = Calendar1.VisibleDate.Month
        Dim currentYear As Integer = Calendar1.VisibleDate.Year
        Calendar1.SelectedDates.Clear()

        'Dim cellStr As String = "<div><a href='AddUserTask.aspx?UserTaskId={0}' />{1}</a></div>"
        'Dim sql As String = String.Format("{0}>='{1}' and {0}<'{2}'", GetFilter(), e.Day.Date.ToString(), e.Day.Date.AddDays(1).ToString())
        Dim tmpstr As String = ""
        Dim tmpstr1 As String = ""
        Dim strStatus As String = ""
        Dim tmpStr2 As String = ""




        For Each dr As DataRow In ds.Tables(0).Rows
            lbl = New Label()
            If ViewState("UnitType").ToLower = "pa" Then
                If dr("RecordDate") = e.Day.Date Then
                    If (dr("ActualHours") Is System.DBNull.Value Or dr("ActualHours").ToString = "999.00" Or dr("ActualHours").ToString = "9999.00") Then
                        tmpstr = ""
                    ElseIf (dr("ActualHours") >= "1.00" And ((dr("SchedHours") - dr("ActualHours")).ToString = "0.00") And (dr("IsTardy") = "0" Or dr("IsTardy") = False)) Then
                        tmpstr = "Present"
                    ElseIf dr("ActualHours") >= "1.00" And dr("SchedHours") = "0.00" And (dr("IsTardy") = "0" Or dr("IsTardy") = False) Then
                        tmpstr = "Present (Makeup)"
                    ElseIf dr("ActualHours") >= "1.00" And dr("SchedHours") = "0.00" And (dr("IsTardy") = "1" Or dr("IsTardy") = True) Then
                        tmpstr = "Present (Tardy) (Makeup)"
                    ElseIf (dr("ActualHours") >= "1.00" And ((dr("SchedHours") - dr("ActualHours")).ToString = "0.00") And (dr("IsTardy") = "1" Or dr("IsTardy") = True)) Then
                        tmpstr = "Present (Tardy)"
                    ElseIf (dr("ActualHours").ToString.Trim() = "0.00" And dr("SchedHours") >= "1.00") Then
                        tmpstr = "Absent"
                        'Condition added due to data from conversion
                    ElseIf (dr("ActualHours").ToString.Trim() >= "1.00" And ((dr("SchedHours") - dr("ActualHours")).ToString >= "1.00") And dr("SchedHours") >= "1.00") Then
                        tmpstr = "Absent"
                    Else
                        tmpstr = ""
                    End If

                    If tmpstr <> "" And tmpstr <> "NA" And tmpstr.Length >= 7 Then
                        If tmpstr.ToString.ToLower.Trim().Substring(0, 7) = "present" And (dr("SchedHours") Is System.DBNull.Value) Then
                            tmpstr = tmpstr & "(Makeup)"
                        End If
                    End If
                    If tmpstr <> "" Then
                        lbl.ForeColor = Color.Blue
                        lbl.BackColor = Color.Transparent
                    End If
                    lbl.Text = "<br>" & tmpstr
                    'lbl.Height = 10
                    'lbl.Width = e.Cell.Width
                    'lbl.CssClass = "daytext"
                End If
                e.Cell.Controls.Add(lbl)
                ''Added by saraswathi for timeclock schools
                ''Added the absent and makeuphours
                ''on Jan 27 2009
            ElseIf ViewState("UnitType").ToLower = "hrs" Or ViewState("UnitType").ToLower = "minutes" Then
                Dim tempstr3 As String = ""

                If dr("RecordDate") = e.Day.Date Then
                    If (dr("ActualHours") Is System.DBNull.Value Or dr("ActualHours").ToString = "999.00" Or dr("ActualHours").ToString = "9999.00") Then
                        tmpstr = ""
                    ElseIf ((CDec(dr("ActualHours")) >= 0 And dr("ActualHours").ToString <> "999.00" And dr("ActualHours").ToString <> "9999.00") And (dr("IsTardy") = "0" Or dr("IsTardy") = False)) Then
                        tmpstr = "Schedule=" & dr("SchedHours")
                        tmpstr1 = "Actual=" & dr("ActualHours")
                        If CDec(dr("SchedHours")) > CDec(dr("ActualHours")) Then
                            tempstr3 = "Absent=" & CDec(dr("SchedHours")) - CDec(dr("ActualHours"))
                        ElseIf CDec(dr("SchedHours")) < CDec(dr("ActualHours")) Then
                            tempstr3 = "MakeUp=" & CDec(dr("ActualHours")) - CDec(dr("SchedHours"))
                        End If
                        'decSchedHours += dr("SchedHours")
                        'decActualHours += dr("ActualHours")
                    ElseIf ((CDec(dr("ActualHours")) >= 0 And dr("ActualHours").ToString <> "999.00" And dr("ActualHours").ToString <> "9999.00") And (dr("IsTardy") = "1" Or dr("IsTardy") = True)) Then
                        tmpstr = "Schedule=" & dr("SchedHours")
                        tmpstr1 = "Actual=" & dr("ActualHours")
                        tmpStr2 = "(tardy)"
                        If CDec(dr("SchedHours")) > CDec(dr("ActualHours")) Then
                            tempstr3 = "Absent=" & CDec(dr("SchedHours")) - CDec(dr("ActualHours"))
                        ElseIf CDec(dr("SchedHours")) < CDec(dr("ActualHours")) Then
                            tempstr3 = "MakeUp=" & CDec(dr("ActualHours")) - CDec(dr("SchedHours"))
                        End If
                        'decSchedHours += dr("SchedHours")
                        'decActualHours += dr("ActualHours")
                        'ElseIf (dr("ActualHours").ToString.Trim() = "0.00" And dr("SchedHours") >= "1.00") Then
                        '    tmpstr = "Schedule=" & dr("SchedHours")
                        '    tmpstr1 = "Actual=" & dr("ActualHours")
                        '    'decSchedHours += dr("SchedHours")
                        '    'decActualHours += 0
                        '    'Condition added due to data from conversion
                        'ElseIf ((dr("ActualHours").ToString.Trim >= "1.00" And dr("ActualHours").ToString <> "999.00" And dr("ActualHours").ToString <> "9999.00") And ((dr("SchedHours") - dr("ActualHours")) >= "1.00") And dr("SchedHours") >= "1.00") Then
                        '    tmpstr = "Schedule=" & dr("SchedHours")
                        '    tmpstr1 = "Actual=" & dr("ActualHours")
                        '    'decSchedHours += dr("SchedHours")
                        '    'decActualHours += dr("ActualHours")
                        'ElseIf (dr("ActualHours").ToString.Trim() >= "1.00" And dr("SchedHours") = "0.00") Then
                        '    tmpstr = "Schedule=" & dr("SchedHours")
                        '    tmpstr1 = "Actual=0"
                        '    tmpStr2 = "Makeup=" & dr("ActualHours")
                        '    'decSchedHours += dr("SchedHours")
                        '    'decActualHours += 0
                        '    'decMakeup += dr("ActualHours")
                    Else
                        tmpstr = ""
                        tmpstr1 = ""
                        tmpStr2 = ""
                        tempstr3 = ""
                    End If
                    If tmpstr <> "" And tmpstr1 <> "" Then
                        lbl.ForeColor = Color.Blue
                        lbl.BackColor = Color.Transparent
                    End If
                    lbl.Text = tmpstr & "<br>" & tmpstr1 & "<br>" & tempstr3 & "<br>" & tmpStr2
                End If
                e.Cell.Controls.Add(lbl)
                ''commented by saraswathi on march 11 2009
                ''absent hours and makeup hours not shown for mintues type of school

                ' ''Else
                ' ''    If dr("RecordDate") = e.Day.Date Then
                ' ''        If (dr("ActualHours") Is System.DBNull.Value Or dr("ActualHours").ToString = "999.00" Or dr("ActualHours").ToString = "9999.00") Then
                ' ''            tmpstr = ""
                ' ''        ElseIf ((dr("ActualHours").ToString.Trim >= "1.00" And dr("ActualHours").ToString <> "999.00" And dr("ActualHours").ToString <> "9999.00") And (dr("IsTardy") = "0" Or dr("IsTardy") = False)) Then
                ' ''            tmpstr = "Schedule=" & dr("SchedHours")
                ' ''            tmpstr1 = "Actual=" & dr("ActualHours")
                ' ''            'decSchedHours += dr("SchedHours")
                ' ''            'decActualHours += dr("ActualHours")
                ' ''        ElseIf ((dr("ActualHours").ToString.Trim >= "1.00" And dr("ActualHours").ToString <> "999.00" And dr("ActualHours").ToString <> "9999.00") And (dr("IsTardy") = "1" Or dr("IsTardy") = True)) Then
                ' ''            tmpstr = "Schedule=" & dr("SchedHours")
                ' ''            tmpstr1 = "Actual=" & dr("ActualHours")
                ' ''            tmpStr2 = "(tardy)"
                ' ''            'decSchedHours += dr("SchedHours")
                ' ''            'decActualHours += dr("ActualHours")
                ' ''        ElseIf (dr("ActualHours").ToString.Trim() = "0.00" And dr("SchedHours") >= "1.00") Then
                ' ''            tmpstr = "Schedule=" & dr("SchedHours")
                ' ''            tmpstr1 = "Actual=" & dr("ActualHours")
                ' ''            'decSchedHours += dr("SchedHours")
                ' ''            'decActualHours += 0
                ' ''            'Condition added due to data from conversion
                ' ''        ElseIf ((dr("ActualHours").ToString.Trim >= "1.00" And dr("ActualHours").ToString <> "999.00" And dr("ActualHours").ToString <> "9999.00") And ((dr("SchedHours") - dr("ActualHours")) >= "1.00") And dr("SchedHours") >= "1.00") Then
                ' ''            tmpstr = "Schedule=" & dr("SchedHours")
                ' ''            tmpstr1 = "Actual=" & dr("ActualHours")
                ' ''            'decSchedHours += dr("SchedHours")
                ' ''            'decActualHours += dr("ActualHours")
                ' ''        ElseIf (dr("ActualHours").ToString.Trim() >= "1.00" And dr("SchedHours") = "0.00") Then
                ' ''            tmpstr = "Schedule=" & dr("SchedHours")
                ' ''            tmpstr1 = "Actual=0"
                ' ''            tmpStr2 = "Makeup=" & dr("ActualHours")
                ' ''            'decSchedHours += dr("SchedHours")
                ' ''            'decActualHours += 0
                ' ''            'decMakeup += dr("ActualHours")
                ' ''        Else
                ' ''            tmpstr = ""
                ' ''            tmpstr1 = ""
                ' ''        End If
                ' ''        If tmpstr <> "" And tmpstr1 <> "" Then
                ' ''            lbl.ForeColor = Color.Blue
                ' ''            lbl.BackColor = Color.Transparent
                ' ''        End If
                ' ''        lbl.Text = tmpstr & "<br>" & tmpstr1 & "<br>" & tmpStr2
                ' ''    End If
                ' ''    e.Cell.Controls.Add(lbl)
            End If
        Next
        'Session("SchedHours") = decSchedHours
        'Session("ActualHours") = decActualHours
        'Session("MakeupHours") = decMakeup
        'Next i
    End Sub
    Sub Set_Calendar(ByVal Sender As Object, ByVal E As EventArgs) Handles drpCalMonth.SelectedIndexChanged, drpCalYear.SelectedIndexChanged
        Dim intDay As Integer = Day(Date.Now)
        Dim dtDate As Date = drpCalMonth.SelectedItem.Value & intDay & ", " & drpCalYear.SelectedItem.Value
        Calendar1.TodaysDate = CDate(dtDate) 'CDate(drpCalMonth.SelectedItem.Value & " & intDay & ", " & drpCalYear.SelectedItem.Value)
    End Sub

    Protected Sub Calendar1_VisibleMonthChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles Calendar1.VisibleMonthChanged
    End Sub
End Class
