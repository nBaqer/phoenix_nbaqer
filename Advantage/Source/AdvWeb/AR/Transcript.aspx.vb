﻿
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Collections
Imports System.Diagnostics
Imports System.Drawing
Imports FAME.Advantage.Common
Imports Advantage.Business.Objects
Imports FAME.Advantage.Api.Library.AcademicRecords
Imports FAME.Advantage.Api.Library.Models
Imports FAME.Advantage.Api.Library.Models.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Domain.MultiTenant.Infrastructure.API

Partial Class Transcript
    Inherits BasePage

    Protected WithEvents lblDocumentId As Label
    Protected WithEvents ddlDocumentId As DropDownList
    Protected WithEvents ddlScannedId As DropDownList
    Protected WithEvents btnhistory As Button
    Protected WithEvents lnkViewDoc As LinkButton
    Protected WithEvents ddlEnrollmentId As DropDownList
    Protected WithEvents lblNoSchedCourses As Label
    Protected WithEvents pnlNoSchedCourses As Panel
    Protected StudentId As String
    Protected campusid As String
    Protected strGradesFormat As String = String.Empty



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

#End Region


    Protected state As AdvantageSessionState
    Protected LeadId As String
    'Dim resourceId As Integer
    Protected boolSwitchCampus As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Dim IsContinuingEd As Boolean
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function GetStudentFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try
            GlobalSearchHandler(0)
            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)




        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        MyAdvAppSettings = AdvAppSettings.GetAppSettings()

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU = GetStudentFromStateObject(230)
        'Pass resource id so that user can be redirected to same page while switching students
        If objStudentState Is Nothing Then
            RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'Always disable the History button. It does not apply to this page.
        'Header1.EnableHistoryButton(False)
        strGradesFormat = MyAdvAppSettings.AppSettings("GradesFormat").ToString().ToLower

        ''
        If (Not Page.IsPostBack Or Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            ChkIsInDB.Checked = True
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            BindDataList()

            'show an error message if there are no enrollments
            If dlstLeadNames.Items.Count = 0 Then
                DisplayErrorMessage("There are no enrollments")
                Exit Sub
            End If
            pnlSummary.Visible = False

            'When the page is first loaded we want to select the first enrollment in the DataList
            Dim progVerName As String
            Dim stuEnrollId As String

            progVerName = GetProgVerNameForFirstItem()
            stuEnrollId = GetStuEnrollIdForFirstItem()
            Dim dtCoursesinProgram As DataTable = (New TranscriptFacade).GetCoursesBelongingtotheProgramVersion(stuEnrollId, progVerName, True)
            ViewState("dtCoursesinProgram") = dtCoursesinProgram

            'We should only process the selected enrollment if it is NOT for a CE pv OR it is a CE pv and the enrollment is registered for classes
            Dim fac As New TranscriptFacade

            If fac.ShouldSelectedEnrollmentBeProcessed(stuEnrollId) Then
                ProcessSelectedEnrollment(stuEnrollId, progVerName)
                HideTermInfoIfRegistrationTypeByProgram(stuEnrollId)
            Else
                DisplayErrorMessage("This student is in a CE program and does not have any scheduled/graded classes")
            End If

            uSearchEntityControlId.Value = ""

        End If
    End Sub

    Private Sub BindDataList()
        Dim transcript As New TranscriptFacade
        Dim dsEnrollments As DataSet = transcript.GetEnrollment(StudentId, Master.CurrentCampusId)
        dlstLeadNames.DataSource = dsEnrollments
        dlstLeadNames.DataBind()
        'CheckPermissions()
    End Sub

    Private Sub dlstLeadNames_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstLeadNames.ItemCommand
        Dim progVerName As String = e.CommandName
        Dim stuEnrollId As String = e.CommandArgument
        Dim dtCoursesinProgram As DataTable
        Dim fac As New TranscriptFacade

        If fac.ShouldSelectedEnrollmentBeProcessed(stuEnrollId) Then
            dtCoursesinProgram = (New TranscriptFacade).GetCoursesBelongingtotheProgramVersion(stuEnrollId, progVerName, True)

            ViewState("dtCoursesinProgram") = dtCoursesinProgram
            ProcessSelectedEnrollment(stuEnrollId, progVerName)
            CommonWebUtilities.RestoreItemValues(dlstLeadNames, stuEnrollId)
            HideTermInfoIfRegistrationTypeByProgram(stuEnrollId)
        Else
            DisplayErrorMessage("This student is in a CE program and does not have any scheduled/graded classes")
        End If


    End Sub

    Private Sub HideTermInfoIfRegistrationTypeByProgram(ByVal studentEnrollmentId As String)
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim programVersionRequest As New ProgramVersionRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Dim registrationType As Enums.ProgramRegistrationType? = programVersionRequest.GetRegistrationType(Guid.Parse(studentEnrollmentId))
            If (Not registrationType Is Nothing) Then
                If (registrationType = Enums.ProgramRegistrationType.ByProgram) Then
                    If (dgrdScheduledCourses.Items.Count > 0) Then
                        dgrdScheduledCourses.Columns(0).Visible = False
                        dgrdScheduledCourses.Columns(1).Visible = False
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub ProcessSelectedEnrollment(stuEnrollId As String, progVerName As String)
        'Dim strPrgVerId As String = progVerName
        Dim transcript As New TranscriptFacade
        Dim dt As DataTable

        'Save the CommandArgument(i.e StuEnrollId) for later use - Added by 
        ' BN(9 / 6 / 5)
        ViewState("StuEnrollId") = stuEnrollId
        ViewState("ProgVerName") = progVerName

        Dim canChangeGrade = transcript.GetEnrollmentCanChangeGrade(stuEnrollId, AdvantageSession.UserState.UserId.ToString)
        ViewState("CanChangeGrade") = canChangeGrade

        Dim facade As New GraduateAuditFacade
        Dim gradeReps = CType(MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod"), String)
        Dim strIncludeHours = CType(MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade"), String)
        Dim includeHours As Boolean
        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If



        dt = facade.GetResultsByEnrollment(stuEnrollId, gradeReps, includeHours)
        Dim hideCreditsHours As Boolean

        ' IsContinuingEd = IsContinuingEdPrgVersion(stuEnrollId)
        IsContinuingEd = GraduateAuditFacade.IsContinuingEdPrgVersion(stuEnrollId)

        ''Modified by SAraswathi Lakshmanan on March 8 2010
        ''To fix mantis issue 18575: QA: Transcript tab needs to check for the transcript type when we use the "SupressCreditHoursForContinuedEDProgram" entry. 
        If (IsContinuingEd = True And MyAdvAppSettings.AppSettings("SupressCreditHoursForContinuedEDProgram").ToLower = "true" And MyAdvAppSettings.AppSettings("TranscriptType").ToLower = "traditional") Then
            hideCreditsHours = True
        Else
            hideCreditsHours = False
        End If

        'The datatable is not pulling externship courses that just has externship attendance
        Dim isStudentRegisteredInExternshipCourse As Integer
        Dim dtExternshipCourses As DataTable
        dtExternshipCourses = (New TranscriptFacade).getExternshipCourses(stuEnrollId)
        If Not dtExternshipCourses Is Nothing Then isStudentRegisteredInExternshipCourse = dtExternshipCourses.Rows.Count
        If isStudentRegisteredInExternshipCourse >= 1 Then
            Dim expression As String = "ResultId='" + dtExternshipCourses.Rows(0)("ResultId").ToString + "'"
            Dim foundRows() As DataRow
            foundRows = dt.Select(expression)
            If foundRows.Length = 0 Then
                Dim dr As DataRow
                dr = dt.NewRow()

                dr("TermId") = dtExternshipCourses.Rows(0)("TermId")
                dr("TermDescrip") = dtExternshipCourses.Rows(0)("TermDescrip")
                dr("ReqId") = dtExternshipCourses.Rows(0)("ReqId")
                dr("Code") = dtExternshipCourses.Rows(0)("Code")
                dr("Descrip") = dtExternshipCourses.Rows(0)("Descrip")
                dr("Credits") = dtExternshipCourses.Rows(0)("Credits")
                dr("Hours") = dtExternshipCourses.Rows(0)("Hours")
                dr("CourseCategoryId") = dtExternshipCourses.Rows(0)("CourseCategoryId")
                dr("StartDate") = dtExternshipCourses.Rows(0)("StartDate")
                dr("EndDate") = dtExternshipCourses.Rows(0)("EndDate")
                dr("ClassStartDate") = dtExternshipCourses.Rows(0)("ClassStartDate")
                dr("ClassEndDate") = dtExternshipCourses.Rows(0)("ClassEndDate")
                dr("GrdSysDetailId") = dtExternshipCourses.Rows(0)("GrdSysDetailId")
                dr("TestId") = dtExternshipCourses.Rows(0)("TestId")
                dr("ResultId") = dtExternshipCourses.Rows(0)("ResultId")
                dr("Grade") = dtExternshipCourses.Rows(0)("Grade")
                dr("IsPass") = dtExternshipCourses.Rows(0)("IsPass")
                dr("GPA") = dtExternshipCourses.Rows(0)("GPA")
                dr("IsCreditsAttempted") = dtExternshipCourses.Rows(0)("IsCreditsAttempted")

                dr("IsCreditsEarned") = dtExternshipCourses.Rows(0)("IsCreditsEarned")
                dr("IsInGPA") = dtExternshipCourses.Rows(0)("IsInGPA")
                dr("IsDrop") = dtExternshipCourses.Rows(0)("IsDrop")
                dr("CourseCategory") = dtExternshipCourses.Rows(0)("CourseCategory")
                dr("GPA") = dtExternshipCourses.Rows(0)("GPA")
                dr("Score") = dtExternshipCourses.Rows(0)("Score")

                dr("FinAidCredits") = dtExternshipCourses.Rows(0)("FinAidCredits")
                dr("DateIssue") = dtExternshipCourses.Rows(0)("DateIssue")
                dr("DropDate") = dtExternshipCourses.Rows(0)("DropDate")
                dr("ScheduledHours") = dtExternshipCourses.Rows(0)("ScheduledHours")
                dr("IsTransferGrade") = dtExternshipCourses.Rows(0)("IsTransferGrade")
                dr("AvgScoreNew") = dtExternshipCourses.Rows(0)("AvgScoreNew")
                dr("AvgGPANew") = dtExternshipCourses.Rows(0)("AvgGPANew")

                dt.Rows.Add(dr)
            End If
        End If
        'Added by Balaji on 7.19.2013 Ends here
        Dim todelete As List(Of Integer) = New List(Of Integer)()
        Try
            For Each dr As DataRow In dt.Rows
                If hideCreditsHours = True Then
                    dr("Credits") = DBNull.Value
                    dr("Hours") = DBNull.Value
                End If

                'Added by balaji on 7.18.2013 as Transcript page should incomplete courses in Completed section
                Dim boolIsCourseCompleted As Boolean
                boolIsCourseCompleted = (New TranscriptFacade).isCourseCompleted(dr("ResultId").ToString)
                If boolIsCourseCompleted = False Then
                    'dt.Rows.Remove(dr) 'if course is not completed remove the course from completed section of transcript
                    todelete.Add(dt.Rows.IndexOf(dr))
                    ' dr.Delete()
                End If
            Next
            For Each i As Integer In todelete
                dt.Rows(i).Delete()
            Next

            dt.AcceptChanges()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            dt.AcceptChanges()
        End Try

        If dt.Rows.Count > 0 Then
            dgrdTransactionSearch.DataSource = New DataView(dt, "", "StartDate,EndDate,Code,Descrip", DataViewRowState.CurrentRows)

        Else
            dgrdTransactionSearch.DataSource = Nothing
        End If
        dgrdTransactionSearch.DataBind()

        GetSummaryInfo(stuEnrollId.ToString())
        pnlSummary.Visible = True

        '
        '
        '   Scheduled Courses section
        Dim ds As DataSet = transcript.GetScheduledCourses(stuEnrollId, HttpContext.Current.Request("cmpid").ToString)

        If ds.Tables(0).Rows.Count = 0 Then
            lblSchedCourses.Text = "There are no scheduled courses"
            dgrdScheduledCourses.Visible = False
        Else
            'If externship course with 
            If isStudentRegisteredInExternshipCourse >= 1 Then
                For Each drScheduled As DataRow In ds.Tables(0).Rows
                    Dim dtScheduled As DataTable = ds.Tables(0)

                    Dim externshipClassId As String = dtExternshipCourses.Rows(0)("TestId").ToString
                    Dim strclassSectionId As String = drScheduled("testid").ToString
                    Dim isCourseCompleted As Boolean = CType(dtExternshipCourses.Rows(0)("IsCourseCompleted"), Boolean)

                    If (externshipClassId.Trim.ToUpper = strclassSectionId.Trim.ToUpper) AndAlso (isCourseCompleted = True) Then
                        drScheduled.Delete()
                        Exit For
                    End If
                Next
            End If
            ds.Tables(0).AcceptChanges()

            lblSchedCourses.Text = "Scheduled Courses"
            dgrdScheduledCourses.Visible = True
        End If
        dgrdScheduledCourses.DataSource = ds.Tables(0)
        dgrdScheduledCourses.DataBind()
        pnlScheduledCourses.Visible = True
        '
        '
        '
        '   Remaining Courses section
        ds = transcript.GetRemainingCourses(stuEnrollId, HttpContext.Current.Request("cmpid").ToString)

        If ds.Tables(0).Rows.Count = 0 Then
            lblRemainingCourses.Text = "There are no remaining courses"
            dgrdRemainingCourses.Visible = False
        Else
            lblRemainingCourses.Text = "Remaining Courses"
            dgrdRemainingCourses.Visible = True
        End If
       
        dgrdRemainingCourses.DataSource = ds.Tables(0)
        dgrdRemainingCourses.DataBind()
        pnlRemainingCourses.Visible = True
        CommonWebUtilities.RestoreItemValues(dlstLeadNames, stuEnrollId)
        'CommonWebUtilities.SetStyleToSelectedItem(dlstLeadNames, stuEnrollId, ViewState, Header1)
        'Header1.EnableHistoryButton(False)
        'CheckPermissions()
    End Sub


    Sub drgTransactionSearch_Click(ByVal sender As Object, ByVal e As DataGridCommandEventArgs)
    End Sub
    Private Function GetProgVerNameForFirstItem() As String
        If dlstLeadNames.Items.Count > 0 Then
            For j As Integer = 0 To dlstLeadNames.Items(0).Controls.Count - 1
                If dlstLeadNames.Items(0).Controls(j).GetType.ToString = "System.Web.UI.WebControls.LinkButton" Then
                    Return DirectCast(dlstLeadNames.Items(0).Controls(j), LinkButton).CommandName
                End If
            Next
        Else
            Return ""
        End If
        Return ""
    End Function
    Private Function GetStuEnrollIdForFirstItem() As String
        If dlstLeadNames.Items.Count > 0 Then
            For j As Integer = 0 To dlstLeadNames.Items(0).Controls.Count - 1
                If dlstLeadNames.Items(0).Controls(j).GetType.ToString = "System.Web.UI.WebControls.LinkButton" Then
                    Return DirectCast(dlstLeadNames.Items(0).Controls(j), LinkButton).CommandArgument.ToString
                End If
            Next
        Else
            Return ""
        End If
        Return ""
    End Function



    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        '   process post backs from the data-grid
        strGradesFormat = MyAdvAppSettings.AppSettings("GradesFormat").ToString().ToLower

        Dim resultId As String
        Dim termId As String
        Dim reqId As String
        Dim testId As String
        Dim editGrdSysDetailId As String
        Dim userName As String
        Dim reqDesc As String
        Dim stuEnrollId As String
        Dim progVerName As String

        If Not ViewState("StuEnrollId") = Nothing Then
            stuEnrollId = ViewState("StuEnrollId").ToString()
        Else
            stuEnrollId = String.Empty
        End If
        If Not ViewState("ProgVerName") = Nothing Then
            progVerName = ViewState("ProgVerName").ToString()
        Else
            progVerName = String.Empty
        End If

        Select Case e.CommandName
            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdTransactionSearch.EditItemIndex = e.Item.ItemIndex


                '------------------------------------------------------------------------
                ' Get the original term id so we can compare it to the old in the update
                ' to know if the user changed the term
                '------------------------------------------------------------------------
                Dim oldtermId = CType(e.Item.FindControl("lbloldTermId"), Label)
                If Not oldtermId Is Nothing Then
                    ViewState("TermId") = oldtermId.Text
                End If
                '------------------------------------------------------------------------

                'BindDatagrid Only
                BindDataGrid()
                '
                '   user hit "Update" inside the data grid
            Case "Update"

                Dim result As String


                '--------------------------------------------------------------------------------------------------
                '  Check if the term  has been changed.  This will determine if we need to check for attendance
                '  If term has been changed, set flag to pass into data layer.
                '--------------------------------------------------------------------------------------------------
                Dim btermChanged As Boolean

                Dim oldtermId As String = String.Empty
                If Not ViewState("TermId") Is Nothing Then
                    oldtermId = ViewState("TermId").ToString()
                End If

                Dim newTermId As String = CType(e.Item.FindControl("ddlEditClsSectionId"), DropDownList).SelectedValue


                If oldtermId <> newTermId Then
                    btermChanged = True
                End If
                '-------------------------------------------------------------------------------------------------

                termId = CType(e.Item.FindControl("ddlEditClsSectionId"), DropDownList).SelectedValue         'new TermId from TermStartDate Dropdown
                editGrdSysDetailId = CType(e.Item.FindControl("ddlEditGrdSysDetailId"), DropDownList).SelectedValue     'grade system de

                resultId = CType(e.Item.FindControl("lblResultId"), Label).Text 'primary key from arresults table

                reqId = CType(e.Item.FindControl("lblReqId"), Label).Text  'reqId from adReqs 
                testId = CType(e.Item.FindControl("lblTestId"), Label).Text 'clsSectionId
                userName = AdvantageSession.UserState.UserName 'user
                reqDesc = CType(e.Item.FindControl("GrdBkLinkButton"), LinkButton).Text 'description

                result = (New TranscriptFacade).UpdateSingleGradeTermAllEnrollments(resultId, termId, reqId, testId, editGrdSysDetailId, userName, reqDesc, btermChanged, stuEnrollId)

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                    Exit Sub
                End If

                'Prepare dataGrid
                PrepareDatagrid()



                'Process the whole thing again
                ProcessSelectedEnrollment(stuEnrollId, progVerName)

                '   user hit "Cancel"
            Case "Cancel"

                'Prepare dataGrid
                PrepareDatagrid()

                'BindDatagrid Only
                BindDataGrid()

                '   user hit "Delete" inside the datagrid
            Case "Delete"

                resultId = CType(e.Item.FindControl("lblResultId"), Label).Text
                testId = CType(e.Item.FindControl("lblTestId"), Label).Text
                userName = AdvantageSession.UserState.UserName

                Dim result As String = TranscriptFacade.DeleteSingleGradeAllEnrollments(resultId, testId, userName, stuEnrollId)
                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                    Exit Sub
                End If

                'prepare DataGrid
                PrepareDatagrid()

                'Process the whole thing again
                ProcessSelectedEnrollment(stuEnrollId, progVerName)

            Case Else

                Dim sb As New StringBuilder
                '   append StudentId to the querystring
                sb.Append("&StudentId=" + StudentId)
                ''   append TestId to the querystring
                sb.Append("&TestId=" + e.CommandArgument)
                ''   append StuEnrollId to the querystring
                sb.Append("&StuEnrollId=" + stuEnrollId)
                '   setup the properties of the new window
                Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge()
                Dim name = "StudentGradeBkPopUp"
                Dim url As String = "../AR/" + name + ".aspx?resid=364&mod=AR&cmpid=" + Request("cmpid") + sb.ToString
                '   open new window and pass parameters
                CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
        End Select
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub
    Private Sub PrepareDatagrid()
        '   set no record selected
        dgrdTransactionSearch.EditItemIndex = -1
    End Sub
    Protected Sub dgrdTransactionSearch_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemCreated
        Dim oControl As Control
        Dim resourceId As String
        'Dim UserId As String
        Dim pObj As UserPagePermissionInfo

        MyAdvAppSettings = AdvAppSettings.GetAppSettings()
        'Letter
        strGradesFormat = MyAdvAppSettings.AppSettings("GradesFormat").ToString().ToLower
        resourceId = HttpContext.Current.Request.Params("resid")
        campusid = HttpContext.Current.Request.Params("cmpid")
        'UserId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As User = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)

        For Each oControl In dgrdTransactionSearch.Controls(0).Controls
            If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
                If strGradesFormat = "letter" Then
                    CType(oControl, DataGridItem).Cells(5).Visible = False
                    CType(oControl, DataGridItem).Cells(6).Visible = True
                Else
                    CType(oControl, DataGridItem).Cells(5).Visible = True
                    CType(oControl, DataGridItem).Cells(6).Visible = False
                End If
                If Not pObj Is Nothing Then
                    If pObj.HasFull Or pObj.HasEdit Then
                        CType(oControl, DataGridItem).Cells(7).Visible = True
                    Else
                        CType(oControl, DataGridItem).Cells(7).Visible = False
                    End If
                End If
            End If
            If CType(oControl, DataGridItem).ItemType = ListItemType.Item Then
                If strGradesFormat = "letter" Then
                    CType(oControl, DataGridItem).Cells(5).Visible = False
                    CType(oControl, DataGridItem).Cells(6).Visible = True
                Else
                    CType(oControl, DataGridItem).Cells(5).Visible = True
                    CType(oControl, DataGridItem).Cells(6).Visible = False
                End If
                If Not pObj Is Nothing Then
                    If pObj.HasFull Or pObj.HasEdit Then
                        CType(oControl, DataGridItem).Cells(7).Visible = True
                    Else
                        CType(oControl, DataGridItem).Cells(7).Visible = False
                    End If
                End If


            End If
            If CType(oControl, DataGridItem).ItemType = ListItemType.AlternatingItem Then
                If strGradesFormat = "letter" Then
                    CType(oControl, DataGridItem).Cells(5).Visible = False
                    CType(oControl, DataGridItem).Cells(6).Visible = True
                Else
                    CType(oControl, DataGridItem).Cells(5).Visible = True
                    CType(oControl, DataGridItem).Cells(6).Visible = False
                End If
                If Not pObj Is Nothing Then
                    If pObj.HasFull Or pObj.HasEdit Then
                        CType(oControl, DataGridItem).Cells(7).Visible = True
                    Else
                        CType(oControl, DataGridItem).Cells(7).Visible = False
                    End If
                End If
            End If
        Next
    End Sub
    Private Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound
        '        Dim testId As String
        strGradesFormat = MyAdvAppSettings.AppSettings("GradesFormat").ToString().ToLower

        campusid = Master.CurrentCampusId
        Select Case e.Item.ItemType
            Case ListItemType.EditItem
                Dim ddl0 As DropDownList = CType(e.Item.FindControl("ddlEditClsSectionId"), DropDownList)
                ddl0.DataTextField = "TermDescrip"
                ddl0.DataValueField = "TermId"
                ddl0.DataSource = (New StudentsAccountsFacade).GetAllTermsForStudentEnrollment(CType(ViewState("StuEnrollId"), String), campusid)
                ddl0.DataBind()
                ddl0.SelectedValue = CType(e.Item.DataItem("TermId"), Guid).ToString

                'get a reference to the ddl in the edit 
                If strGradesFormat.Trim = "letter" Then
                    e.Item.Cells(5).Visible = False 'hide score
                    e.Item.Cells(6).Visible = True   'show Grades dropdown 
                    CType(e.Item.FindControl("ddlEditGrdSysDetailId"), DropDownList).Visible = True
                    CType(e.Item.FindControl("lblScore"), Label).Visible = False
                    If Not IsDBNull(e.Item.DataItem("GrdSysDetailId")) Then
                        Dim grdSysDetailId As String = CType(e.Item.DataItem("GrdSysDetailId"), Guid).ToString
                        Dim ddl As DropDownList = CType(e.Item.FindControl("ddlEditGrdSysDetailId"), DropDownList)
                        ddl.DataTextField = "Grade"
                        ddl.DataValueField = "GrdSysDetailId"
                        ddl.DataSource = (New GradesFacade).GetGradesByResultId(grdSysDetailId)
                        ddl.DataBind()
                        ddl.SelectedValue = grdSysDetailId
                    Else
                        Dim ddl As DropDownList = CType(e.Item.FindControl("ddlEditGrdSysDetailId"), DropDownList)
                        ddl.Visible = False
                    End If

                Else
                    'if numeric hide the grades dropdown
                    CType(e.Item.FindControl("ddlEditGrdSysDetailId"), DropDownList).Visible = False
                    ''New Code Added By Vijay Ramteke For Rally Id DE1393 On December 22, 2010 
                    ''CType(e.Item.FindControl("lblScore"), Label).Visible = True
                    If CType(e.Item.FindControl("lblIsTransferGrade"), Label).Text = "True" Then
                        CType(e.Item.FindControl("lblScore"), Label).Visible = False
                        CType(e.Item.FindControl("lblGrade"), Label).Visible = True
                    Else
                        CType(e.Item.FindControl("lblScore"), Label).Visible = True
                        CType(e.Item.FindControl("lblGrade"), Label).Visible = False
                    End If
                    ''New Code Added By Vijay Ramteke For Rally Id DE1393 On December 22, 2010 
                    e.Item.Cells(5).Visible = True  'show score column
                    e.Item.Cells(6).Visible = False 'hide grades column
                End If
            Case ListItemType.Item, ListItemType.AlternatingItem
                If strGradesFormat.Trim.ToLower = "numeric" Then
                    ''New Code Added By Vijay Ramteke For Rally Id DE1393 On December 22, 2010 
                    If CType(e.Item.FindControl("lblIsTransferGrade"), Label).Text = "True" Then
                        CType(e.Item.FindControl("lblScore"), Label).Visible = False
                        CType(e.Item.FindControl("lblGrade"), Label).Visible = True
                    Else
                        CType(e.Item.FindControl("lblScore"), Label).Visible = True
                        CType(e.Item.FindControl("lblGrade"), Label).Visible = False
                    End If
                    ''New Code Added By Vijay Ramteke For Rally Id DE1393 On December 22, 2010 
                    Dim strReqId As String = CType(e.Item.FindControl("lblscoreReqId"), Label).Text
                    Dim boolIsCourseALab As Boolean
                    boolIsCourseALab = (New GraduateAuditFacade).isCourseALabWorkOrLabHourCourse(strReqId)
                    If boolIsCourseALab = True Then
                        If (New GraduateAuditFacade).isCourseALabWorkOrLabHourCourse(strReqId) Then

                        Else
                            CType(e.Item.FindControl("lblScore"), Label).Text = ""
                        End If

                    End If
                End If
                If Not ViewState("CanChangeGrade") Is Nothing Then
                    Dim canChangeGrade As Boolean = CType(ViewState("CanChangeGrade"), Boolean)
                    If canChangeGrade = False Then
                        CType(e.Item.FindControl("lnlButEdit"), LinkButton).Enabled = False
                    End If

                End If
        End Select
        Dim l As LinkButton
        If e.Item.ItemType = ListItemType.EditItem Then
            l = CType(e.Item.Cells(5).FindControl("lnkbutDelete"), LinkButton)
            If l IsNot Nothing Then
                'l.Attributes.Add("onclick", "return confirm(‘Do you want to continue?’);")
                If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToLower = "byclass" Then
                    l.Attributes("onclick") = "javascript:return " &
                               "confirm('Removing the course will remove the attendance for this course.Do you want to continue? ')"
                End If
            End If
        End If
    End Sub
    Private Sub BindDataGrid()
        'dgrdTransactionSearch.DataSource = 
        'Transcript.GetTranscriptGradeByEnrollment(stuEnrollId, strPrgVerId, StudentId)
        'dt = Transcript.GetStudentResults(stuEnrollId)
        'Dim transcript As New TranscriptFacade
        Dim facade As New GraduateAuditFacade
        Dim gradeReps = CType(MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod"), String)
        Dim strIncludeHours = CType(MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade"), String)
        Dim includeHours As Boolean
        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If

        Dim studentEnrollId = CType(ViewState("StuEnrollId"), String)
        Dim dt As DataTable = facade.GetResultsByEnrollment(studentEnrollId, gradeReps, includeHours)

        Dim hideCreditsHours As Boolean
        Dim isContinuingEdu As Boolean = GraduateAuditFacade.IsContinuingEdPrgVersion(studentEnrollId)

        ''To fix mantis issue 18575: QA: Transcript tab needs to check for the transcript type when we use the "SupressCreditHoursForContinuedEDProgram" entry. 
        If (isContinuingEdu = True And MyAdvAppSettings.AppSettings("SupressCreditHoursForContinuedEDProgram").ToLower = "true" And MyAdvAppSettings.AppSettings("TranscriptType").ToLower = "traditional") Then
            hideCreditsHours = True
        Else
            hideCreditsHours = False
        End If

        'Added by Balaji on 7.19.2013 Starts here
        'The datatable is not pulling externship courses that just has externship attendance
        Dim isStudentRegisteredInExternshipCourse As Integer
        Dim dtExternshipCourses As DataTable
        dtExternshipCourses = (New TranscriptFacade).getExternshipCourses(studentEnrollId)
        If Not dtExternshipCourses Is Nothing Then isStudentRegisteredInExternshipCourse = dtExternshipCourses.Rows.Count
        If isStudentRegisteredInExternshipCourse >= 1 Then
            Dim expression As String = "ResultId='" + dtExternshipCourses.Rows(0)("ResultId").ToString + "'"
            Dim foundRows() As DataRow
            foundRows = dt.Select(expression)
            If foundRows.Length = 0 Then
                Dim dr As DataRow
                dr = dt.NewRow()

                dr("TermId") = dtExternshipCourses.Rows(0)("TermId")
                dr("TermDescrip") = dtExternshipCourses.Rows(0)("TermDescrip")
                dr("ReqId") = dtExternshipCourses.Rows(0)("ReqId")
                dr("Code") = dtExternshipCourses.Rows(0)("Code")
                dr("Descrip") = dtExternshipCourses.Rows(0)("Descrip")
                dr("Credits") = dtExternshipCourses.Rows(0)("Credits")
                dr("Hours") = dtExternshipCourses.Rows(0)("Hours")
                dr("CourseCategoryId") = dtExternshipCourses.Rows(0)("CourseCategoryId")
                dr("StartDate") = dtExternshipCourses.Rows(0)("StartDate")
                dr("EndDate") = dtExternshipCourses.Rows(0)("EndDate")
                dr("ClassStartDate") = dtExternshipCourses.Rows(0)("ClassStartDate")
                dr("ClassEndDate") = dtExternshipCourses.Rows(0)("ClassEndDate")
                dr("GrdSysDetailId") = dtExternshipCourses.Rows(0)("GrdSysDetailId")
                dr("TestId") = dtExternshipCourses.Rows(0)("TestId")
                dr("ResultId") = dtExternshipCourses.Rows(0)("ResultId")
                dr("Grade") = dtExternshipCourses.Rows(0)("Grade")
                dr("IsPass") = dtExternshipCourses.Rows(0)("IsPass")
                dr("GPA") = dtExternshipCourses.Rows(0)("GPA")
                dr("IsCreditsAttempted") = dtExternshipCourses.Rows(0)("IsCreditsAttempted")

                dr("IsCreditsEarned") = dtExternshipCourses.Rows(0)("IsCreditsEarned")
                dr("IsInGPA") = dtExternshipCourses.Rows(0)("IsInGPA")
                dr("IsDrop") = dtExternshipCourses.Rows(0)("IsDrop")
                dr("CourseCategory") = dtExternshipCourses.Rows(0)("CourseCategory")
                dr("GPA") = dtExternshipCourses.Rows(0)("GPA")
                dr("Score") = dtExternshipCourses.Rows(0)("Score")

                dr("FinAidCredits") = dtExternshipCourses.Rows(0)("FinAidCredits")
                dr("DateIssue") = dtExternshipCourses.Rows(0)("DateIssue")
                dr("DropDate") = dtExternshipCourses.Rows(0)("DropDate")
                dr("ScheduledHours") = dtExternshipCourses.Rows(0)("ScheduledHours")
                dr("IsTransferGrade") = dtExternshipCourses.Rows(0)("IsTransferGrade")
                dr("AvgScoreNew") = dtExternshipCourses.Rows(0)("AvgScoreNew")
                dr("AvgGPANew") = dtExternshipCourses.Rows(0)("AvgGPANew")

                dt.Rows.Add(dr)
            End If
        End If
        'Added by Balaji on 7.19.2013 Ends here

        ' Try
        Dim dt2 As New DataTable
        Dim index As Integer

        dt2 = dt.Copy
        index = 0

        For Each dr As DataRow In dt.Rows
            If hideCreditsHours = True Then
                dr("Credits") = DBNull.Value
                dr("Hours") = DBNull.Value
            End If

            'Added by balaji on 7.18.2013 as Transcript page should incomplete courses in Completed section
            Dim boolIsCourseCompleted As Boolean
            boolIsCourseCompleted = (New TranscriptFacade).isCourseCompleted(dr("ResultId").ToString)
            If boolIsCourseCompleted = False Then
                'dt.Rows.Remove(dr) 'if course is not completed remove the course from completed section of transcript
                'dr.Delete()
                dt2.Rows.RemoveAt(index)
            End If

            index += 1

        Next
        dt = dt2.Copy
        dt.AcceptChanges()
        'Catch ex As Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    dt.AcceptChanges()
        'End Try

        'Added By Vijay Ramteke on Feb 09, 2010

        If dt.Rows.Count > 0 Then
            dgrdTransactionSearch.DataSource = New DataView(dt, "", "StartDate,EndDate,Code,Descrip", DataViewRowState.CurrentRows)
        Else
            dgrdTransactionSearch.DataSource = Nothing
        End If

        dgrdTransactionSearch.DataBind()

        GetSummaryInfo(ViewState("StuEnrollId").ToString())
        pnlSummary.Visible = True
        '
        '
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Protected Sub dgrdScheduledCourses_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdScheduledCourses.ItemCommand
        Dim sb As New StringBuilder
        '   append StudentId to the query string
        sb.Append("&StudentId=" + StudentId)
        ''   append TestId to the query string
        sb.Append("&TestId=" + e.CommandArgument)
        ''   append StuEnrollId to the query string
        sb.Append("&StuEnrollId=" + ViewState("StuEnrollId"))

        '   setup the properties of the new window
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge()
        Dim name = "StudentGradeBkPopUp"
        Dim url As String = "../AR/" + name + ".aspx?resid=364&mod=AR&cmpid=" + Request("cmpid") + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

    End Sub
    'Private Function getAverage(ByVal dt As DataTable) As Decimal
    '    Dim rtn As Decimal = 0.0
    '    Dim sum As Decimal = 0.0
    '    Dim cnt As Integer = 0 'dt.Rows.Count
    '    '        If cnt = 0 Then Return 0.0
    '    If dt.Rows.Count >= 1 Then
    '        For i As Integer = 0 To dt.Rows.Count - 1
    '            If Not dt.Rows(i).IsNull("Score") Then
    '                'If course is a clinic service course then GrdSysDetailid will be NULL and this course will not be considered 
    '                'while calculating average
    '                If Not dt.Rows(i).IsNull("GrdSysDetailId") Then
    '                    sum = sum + CType(dt.Rows(i)("Score"), Decimal)
    '                    cnt += 1
    '                End If
    '            End If
    '        Next
    '        If sum > 0 Then
    '            rtn = Math.Round(sum / cnt, 2)
    '        End If
    '    End If

    '    Return rtn
    'End Function
    Public Function GetGradeRounding(score As Object) As Decimal
        Dim rtn As Decimal
        If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
            If Not IsDBNull(score) Then rtn = CType(Math.Round(score), Decimal) Else rtn = 0
        Else
            If Not IsDBNull(score) Then rtn = CType(score, Decimal) Else rtn = 0
        End If
        Return rtn
    End Function

    Protected Function IsCoursebelongtoPrgVersion(courseID As String) As Color
        If IsContinuingEd = False Then
            Dim dtCourseinProgram As DataTable
            dtCourseinProgram = CType(ViewState("dtCoursesinProgram"), DataTable)
            If dtCourseinProgram.Select("ReqId='" + courseID + "'").Length = 0 Then
                Return Color.Red
            End If
        End If
        Return Color.Black
    End Function

    Public Function IsCourseWeightForGPAEnabled() As Boolean
        Dim allow = "no"
        If Not IsNothing(MyAdvAppSettings.AppSettings("AllowCourseWeighting")) Then

            If Not String.IsNullOrEmpty(MyAdvAppSettings.AppSettings("AllowCourseWeighting").ToString.ToLower) Then
                allow = MyAdvAppSettings.AppSettings("AllowCourseWeighting").ToString.ToLower
            End If
        End If

        Return allow = "yes"
    End Function

    Public Sub GetSummaryInfo(stuEnrollIdList As String)

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim transcript As New TranscriptFacade
        Dim summaryInfo As TranscriptInfo


        summaryInfo = transcript.GetSummaryFromSP(stuEnrollIdList)
        lblCasessAttemptedValue.Text = summaryInfo.TotalClasses.ToString()
        lblCreditsAttemptedValue.Text = summaryInfo.TotalCreditsAttempted.ToString()
        lblHoursSchecluedValue.Text = summaryInfo.TotalHoursScheduled.ToString()
        lblGPAAverageTitle.Text = summaryInfo.GPAAverageLable

        Dim enrollmentDA As New StudentEnrollmentDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim programIsClockHour = enrollmentDA.IsStudentProgramClockHour(stuEnrollIdList)

        Try
            If programIsClockHour And MyAdvAppSettings.AppSettings("GradesFormat").ToLower = "numeric" Then
                Dim creditSummaryDA As New CreditSummaryDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
                lblGPAAverageValue.Text = creditSummaryDA.GetOverallGPA(stuEnrollIdList) 'GPA Calculator SP'
            Else
                lblGPAAverageValue.Text = summaryInfo.GPAAverageValue.ToString()
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
        End Try

        lblCreditsEarnedValue.Text = summaryInfo.TotalCreditsEarned.ToString()
        lblHoursEarnedValue.Text = summaryInfo.TotalHoursEarned.ToString()
        lblGradePointsValue.Text = summaryInfo.GradePoints.ToString()
        lblCompletedSAPRequirementsValue.Text = summaryInfo.CompletedSAPRequirements
        Dim academicType As String = summaryInfo.AcademicType
        If academicType = "Credits" Then
            lblCreditsAttemptedTitle.Visible = True
            lblCreditsAttemptedValue.Visible = True
            lblCreditsEarnedTitle.Visible = True
            lblCreditsEarnedValue.Visible = True
            lblHoursSchecluedTitle.Visible = False
            lblHoursSchecluedValue.Visible = False
            lblHoursEarnedTitle.Visible = False
            lblHoursEarnedValue.Visible = False

        ElseIf academicType = "ClockHours" Then
            lblCreditsAttemptedTitle.Visible = False
            lblCreditsAttemptedValue.Visible = False
            lblCreditsEarnedTitle.Visible = False
            lblCreditsEarnedValue.Visible = False
            lblHoursSchecluedTitle.Visible = True
            lblHoursSchecluedValue.Visible = True
            lblHoursEarnedTitle.Visible = True
            lblHoursEarnedValue.Visible = True

        ElseIf academicType = "Credits-ClockHours" Then
            lblCreditsAttemptedTitle.Visible = True
            lblCreditsAttemptedValue.Visible = True
            lblCreditsEarnedTitle.Visible = True
            lblCreditsEarnedValue.Visible = True
            lblHoursSchecluedTitle.Visible = True
            lblHoursSchecluedValue.Visible = True
            lblHoursEarnedTitle.Visible = True
            lblHoursEarnedValue.Visible = True
        End If
    End Sub



End Class

