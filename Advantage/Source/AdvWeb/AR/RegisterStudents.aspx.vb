﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Diagnostics
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common

Namespace AdvWeb.AR

    Partial Class RegisterStudents
        Inherits BasePage
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected WithEvents TxtRemStds As TextBox
        Protected WithEvents LblClsSection As Label
        Protected WithEvents DDLClsSection As DropDownList
        Protected WithEvents LblGrdCriteria As Label
        Protected WithEvents DDLGrdCriteria As DropDownList
        Protected WithEvents LblShifts As Label
        Protected WithEvents DDLShifts As DropDownList
        Protected WithEvents Btnhistory As Button
        Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme

        End Sub
        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Variables"
        Private pObj As New UserPagePermissionInfo
        Protected CampusId As String
        Protected MyAdvAppSettings As AdvAppSettings

#End Region

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

            MyAdvAppSettings = AdvAppSettings.GetAppSettings()

            'Put user code to initialize the page here
            'Dim userId As String
            Dim resourceId As Integer

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            CampusId = AdvantageSession.UserState.CampusId.ToString
            'userId = AdvantageSession.UserState.UserId.ToString

            Dim advantageUserState As User = AdvantageSession.UserState

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), CampusId)

        If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If


            If Not Page.IsPostBack Then
                'Dim objcommon As New CommonUtilities
                'Populate the Activities Data-grid with the person who is logged in Activities
                'Default the Selection Criteria for the activity records
                ViewState("Term") = ""
                ViewState("Course") = ""
                ViewState("MODE") = "NEW"
                ViewState("ClsSectIdGuid") = ""
                ViewState("SelectedIndex") = "-1"
                ViewState("Instructor") = "E62FE198-18D9-4244-821D-E89A740F4160"
                PopulateProgramDDL(CampusId)
                BuildAllTerms()
                BuildAllCourses()
                PopulateStatusDDL()
                'PopulateShiftsDDL()
                BuildDropDownLists()

                ViewState("MODE") = "NEW"

                If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToLower = "byclass" Then
                    btnRemove.OnClientClicking = "ConfirmAttendance"
                    btnRemoveAll.OnClientClicking = "ConfirmAttendance"
                End If

                'Disable the menu bar buttons. Enable SAVE when user selects a class and data is present.
                btnNew.Enabled = False
                btnDelete.Enabled = False
                btnSave.Enabled = False
            Else
                'If Session("JustClosed") = "Closed" Then
                '    MoveRecordToRHS(Session("ActivityAssignmentGuid"))
                '    Session("JustClosed") = ""
                'End If
            End If



        End Sub

#End Region

#Region "Button Click Events"

        Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
            Dim ds As New DataSet

            If (ddlProgId.SelectedIndex = 0 Or ddlProgId.SelectedIndex = -1) And ddlTerm.SelectedItem.Value = "" And ddlCourse.SelectedItem.Value = "" Then
                DisplayErrorMessage("Unable to find data. Please select an option.")
                Exit Sub

            End If
            If (ddlProgId.SelectedIndex <> 0 Or ddlProgId.SelectedIndex <> -1) Then
                ViewState("Progid") = ddlProgId.SelectedItem.Value.ToString
            End If

            If ddlTerm.Items.Count > 0 Then
                If ddlTerm.SelectedItem.Value <> "" Then
                    ViewState("Term") = ddlTerm.SelectedItem.Value.ToString
                Else
                    ViewState("Term") = ""
                End If
            End If

            If ddlStatus.SelectedItem.Value <> "" Then
                ViewState("Status") = ddlStatus.SelectedItem.Value.ToString
            Else
                ViewState("Status") = 0
            End If

            If ddlShift.SelectedItem.Value <> "" Then
                ViewState("Shift") = ddlShift.SelectedItem.Value.ToString
            Else
                ViewState("Shift") = ""
            End If
            If ddlCourse.Items.Count > 0 Then
                If ddlCourse.SelectedItem.Value <> "" Then
                    ViewState("Course") = ddlCourse.SelectedItem.Value.ToString
                Else
                    ViewState("Course") = String.Empty
                End If
            End If

            PopulateDataList("Build")
            ClearRhs()
            lblCourseName.Text = String.Empty
            lbxAvailStuds.Items.Clear()
            lbxSelectStuds.Items.Clear()
            lblRemStds.Text = String.Empty

            lblSecName.Text = String.Empty
            lblMaxStud.Text = String.Empty

            lblAvailStuds.Text = "Available Students"
            lblSelStuds.Text = "Selected Students"

            ViewState("StdGrdBk") = ds
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click

            If lbxAvailStuds.Items.Count < 1 Or lbxAvailStuds.GetSelectedIndices().Count < 1 Then
                Exit Sub
            End If

            Dim dt As DataTable = Nothing
            Dim dr As DataRow
            Dim clsSectId As String
            Dim dtClassSections As DataTable
            Dim drClassSection As DataRow
            Dim maxStud As Integer
            Dim availCount As Integer
            Dim selectCount As Integer
            Dim dtAvailStudents As DataTable
            Dim drAvailStudent As DataRow
            Dim expGradDate As DateTime
            Dim dtAdd As New DataTable
            Dim addAll As Boolean
            Dim index As Integer
            Dim dr2 As DataRow

            Dim maxCount As Integer = 0
            Dim remainCount As Integer

            dtAvailStudents = CType(Session("AvailStuds"), DataTable)
            dtClassSections = CType(Session("ClsSections"), DataTable)
            clsSectId = CType(ViewState("ClsSectId"), String)
            drClassSection = dtClassSections.Rows.Find(clsSectId)
            maxStud = CType(drClassSection("MaxStud"), Integer)
            remainCount = maxStud - lbxSelectStuds.Items.Count

            If (lbxSelectStuds.Items.Count < maxStud) Then

                dtAdd.Columns.Add("StuEnrollId")
                dtAdd.Columns.Add("Student")

                For Each item As ListItem In lbxAvailStuds.Items
                    If item.Selected Then
                        If (maxCount < remainCount) Then

                            dt = CType(Session("SelectedStuds"), DataTable)
                            dr = dt.NewRow()
                            dr("StuEnrollId") = item.Value
                            dr("FullName") = item.Text
                            dr("ExpGradDate") = expGradDate

                            dt.Rows.Add(dr)
                            Session("SelectedStuds") = dt

                            dr2 = dtAdd.NewRow()
                            dr2("StuEnrollId") = item.Value
                            dr2("Student") = item.Text
                            dtAdd.Rows.Add(dr2)

                            maxCount += 1

                            Session("SelectedStuds") = dt
                        End If
                    End If
                Next

                If dtAdd.Rows.Count = lbxAvailStuds.Items.Count Then
                    lbxAvailStuds.Items.Clear()
                    addAll = True
                End If

                For Each dr2 In dtAdd.Rows
                    lbxSelectStuds.Items.Add(New ListItem(CType(dr2("Student"), String), CType(dr2("StuEnrollId"), String)))

                    If addAll = False Then
                        For i As Integer = 0 To lbxAvailStuds.Items.Count - 1
                            If dr2("StuEnrollId").ToString() = lbxAvailStuds.Items(i).Value.ToString Then
                                index = i
                                Exit For
                            End If
                        Next

                        lbxAvailStuds.Items.RemoveAt(index)
                        drAvailStudent = dtAvailStudents.Rows.Find(dr2("StuEnrollId").ToString)
                        dtAvailStudents.Rows.Remove(drAvailStudent)

                    End If

                Next
                Dim dv As New DataView(dt, "", "ExpGradDate ASC", DataViewRowState.CurrentRows)

                With lbxSelectStuds
                    .DataSource = dv
                    .DataTextField = "FullName"
                    .DataValueField = "StuEnrollId"
                    .DataBind()
                End With

                selectCount = lbxSelectStuds.Items.Count
                availCount = lbxAvailStuds.Items.Count

                lblRemStds.Text = (maxStud - lbxSelectStuds.Items.Count).ToString()

                lblAvailStuds.Text = "Available Students" & " (" & availCount & ")"
                lblSelStuds.Text = "Selected Students" & " (" & selectCount & ")"

                Session("AvailStuds") = dtAvailStudents

            Else
                DisplayErrorMessage("Max Students limit reached for this course")
            End If

        End Sub

        Private Sub btnAddAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddAll.Click

            If lbxAvailStuds.Items.Count < 1 Then
                Exit Sub
            End If

            Dim dt As DataTable
            Dim dr As DataRow
            Dim clsSectId As String
            Dim dtClassSections As DataTable
            Dim drClassSection As DataRow
            Dim maxStud As Integer
            'Dim availCount As Integer
            Dim selectCount As Integer
            Dim dtAvailStudents As DataTable

            dtAvailStudents = CType(Session("AvailStuds"), DataTable)
            dtClassSections = CType(Session("ClsSections"), DataTable)
            clsSectId = CType(ViewState("ClsSectId"), String)
            drClassSection = dtClassSections.Rows.Find(clsSectId)
            maxStud = CType(drClassSection("MaxStud"), Integer)
            ViewState("MaxStud") = maxStud

            If (lbxSelectStuds.Items.Count < maxStud) Then
                If ((lbxSelectStuds.Items.Count + lbxAvailStuds.Items.Count) <= maxStud) Then
                    For i As Integer = 0 To lbxAvailStuds.Items.Count - 1
                        lbxSelectStuds.Items.Add(New ListItem(lbxAvailStuds.Items(i).Text, lbxAvailStuds.Items(i).Value))
                        dt = CType(Session("SelectedStuds"), DataTable)
                        dr = dt.NewRow()
                        'dr("StudentId") = lbxAvailStuds.Items(i).Value
                        dr("StuEnrollId") = lbxAvailStuds.Items(i).Value
                        dr("FullName") = lbxAvailStuds.Items(i).Text
                        dt.Rows.Add(dr)
                        Session("SelectedStuds") = dt
                    Next

                    selectCount = lbxSelectStuds.Items.Count
                    'availCount = lbxAvailStuds.Items.Count

                    lblRemStds.Text = (maxStud - lbxSelectStuds.Items.Count).ToString()

                    lblAvailStuds.Text = "Available Students" & " (0)"
                    lblSelStuds.Text = "Selected Students" & " (" & selectCount & ")"

                    lbxAvailStuds.Items.Clear()
                    dtAvailStudents.Clear()
                Else
                    DisplayErrorMessage("The number of students being registered is greater than the maximum number allowed for this class section.")
                End If
            Else
                DisplayErrorMessage("Max Students limit reached for this course")
            End If

            Session("AvailStuds") = dtAvailStudents

        End Sub

        Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click


            If lbxSelectStuds.Items.Count < 1 Or lbxSelectStuds.GetSelectedIndices().Count < 1 Then
                Exit Sub
            End If

            Dim iStuEnrollId As String
            Dim dt As DataTable
            Dim dr As DataRow
            'Dim ds As New DataSet
            'Dim objListGen As New DataListGenerator
            Dim maxStud As Integer
            Dim msg As String
            Dim facade As New RegFacade
            Dim item As ListItem
            'Dim iListCount As Integer = 0
            Dim i As Integer
            Dim errorMessage As String = String.Empty
            Dim dtRemove As New DataTable
            Dim removeAll As Boolean
            Dim index As Integer
            Dim dr2 As DataRow
            Dim availStdTable As DataTable
            Dim availRow As DataRow
            Dim availcount As Integer
            Dim selectcount As Integer

            availStdTable = CType(Session("AvailStuds"), DataTable)


            'We cannot remove students who already have final grades posted. For each item in the selected list
            'we will check if a final grade has already been posted.If a final grade has NOT been posted it s okay
            'to remove the item and we will place it in the dtRemove datatable. We cannot remove the item while
            'we are moving through the collection so we have to use the dtRemove datatable after the loop to remove
            'any items that should be removed. 
            'If a final grade has been posted we will simply leave that item. When we are finished if there are any
            'items in the selected items, after the removal of the items that are in the dtRemove datatable we will
            'display a message that one or more students could not be removed
            'because they already have final grades posted.

            'Add necessary columns to the dtRemove datatable
            dtRemove.Columns.Add("StuEnrollId")
            dtRemove.Columns.Add("Student")
            For Each item In lbxSelectStuds.Items
                If item.Selected Then
                    dt = CType(Session("SelectedStuds"), DataTable)
                    dr = dt.Rows.Find(item.Value)

                    iStuEnrollId = dr("StuEnrollId").ToString
                    msg = facade.DoesStdHaveFinalGrade(iStuEnrollId, CType(ViewState("ClsSectId"), String))
                    If msg <> "GradeExists" Then
                        'Okay to remove the item so add it to the dtRemove DataTable.
                        dr2 = dtRemove.NewRow()
                        dr2("StuEnrollId") = item.Value
                        dr2("Student") = item.Text
                        dtRemove.Rows.Add(dr2)

                        If dr.RowState <> DataRowState.Added Then
                            'Mark the row as deleted
                            dr.Delete()
                        Else
                            dt.Rows.Remove(dr)
                        End If
                    Else 'if grade already exists for this student, then set an error message for later display
                        errorMessage = "Student -" & item.Text & " has a final Grade posted for this Class Section. Unable to unregister the student." & vbCr
                    End If

                    Session("SelectedStuds") = dt
                End If
            Next
            'Remember that we cannot simply clear the the selected items. If the number of rows in the dtRemove
            'datatable is the same as the count of items in the selected list box then it means that all the
            'items should be removed and we can therefore use clear. If not we need to loop through the rows in
            'the dtRemove datatable. we will remove the corresponding row in the selected list box. 
            If dtRemove.Rows.Count = lbxSelectStuds.Items.Count Then
                lbxSelectStuds.Items.Clear()
                removeAll = True
            End If

            For Each dr2 In dtRemove.Rows
                'Add the item to the available list box
                lbxAvailStuds.Items.Add(New ListItem(CType(dr2("Student"), String), CType(dr2("StuEnrollId"), String)))

                'If removeAll is set to true then we don't need to remove the items from the selected list box.
                'They were already removed above. If not we will need to find the item and then remove it.
                If removeAll = False Then
                    For i = 0 To lbxSelectStuds.Items.Count - 1
                        If dr2("StuEnrollId").ToString() = lbxSelectStuds.Items(i).Value.ToString Then
                            index = i
                        End If
                    Next

                    'Remove the item from the selected list box.
                    lbxSelectStuds.Items.RemoveAt(index)

                    'Add this student back to the available student datatable in order to keep it in sync.
                    ''IF Condition Added by Sara
                    If availStdTable.Select(CType(("StuEnrollId='" + dr2("StuEnrollId") + "'"), String)).Length = 0 Then
                        availRow = availStdTable.NewRow()
                        availRow("StuEnrollId") = dr2("StuEnrollId")
                        availRow("FullName") = dr2("Student")
                        availStdTable.Rows.Add(availRow)
                    End If

                End If

            Next

            selectcount = lbxSelectStuds.Items.Count
            availcount = lbxAvailStuds.Items.Count

            lblAvailStuds.Text = "Available Students" & " (" & availcount & ")"
            lblSelStuds.Text = "Selected Students" & " (" & selectcount & ")"

            'Show remaining students that can be accomodated.
            maxStud = CType(ViewState("MaxStud"), Integer)
            lblRemStds.Text = CType((maxStud - lbxSelectStuds.Items.Count), String)

            'If there are still items remaining in the selected list box we need to display a message that those
            'items could not be removed because they have final grades posted.
            If errorMessage <> "" Then
                DisplayErrorMessage(errorMessage)
            End If

            'Save the Available datatable to Session 
            Session("AvailStuds") = availStdTable

        End Sub

        Private Sub btnRemoveAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemoveAll.Click
            Dim i As Integer
            Dim iStuEnrollId As String
            Dim dt As DataTable
            Dim dr As DataRow
            Dim dr2 As DataRow
            'Dim ds As New DataSet
            'Dim objListGen As New DataListGenerator
            Dim maxStud As Integer
            Dim msg As String
            Dim facade As New RegFacade
            '        Dim errmsg As String
            Dim dtRemove As New DataTable
            Dim removeAll As Boolean
            Dim index As Integer
            Dim availcount As Integer
            Dim selectcount As Integer
            Dim availStdTable As DataTable
            Dim availRow As DataRow

            If lbxSelectStuds.Items.Count < 1 Then
                Exit Sub
            End If

            availStdTable = CType(Session("AvailStuds"), DataTable)


            'We cannot remove students who already have final grades posted. For each item in the selected list
            'we will check if a final grade has already been posted.If a final grade has NOT been posted it s okay
            'to remove the item and we will place it in the dtRemove datatable. We cannot remove the item while
            'we are moving through the collection so we have to use the dtRemove datatable after the loop to remove
            'any items that should be removed. 
            'If a final grade has been posted we will simply leave that item. When we are finished if there are any
            'items in the selected items, after the removal of the items that are in the dtRemove datatable we will
            'display a message that one or more students could not be removed
            'because they already have final grades posted.

            'Add necessary columns to the dtRemove datatable
            dtRemove.Columns.Add("StuEnrollId")
            dtRemove.Columns.Add("Student")


            For i = 0 To lbxSelectStuds.Items.Count - 1
                dt = CType(Session("SelectedStuds"), DataTable)
                dr = dt.Rows.Find(lbxSelectStuds.Items(i).Value)

                iStuEnrollId = dr("StuEnrollId").ToString
                msg = facade.DoesStdHaveFinalGrade(iStuEnrollId, CType(ViewState("ClsSectId"), String))
                If msg <> "GradeExists" Then
                    'Okay to remove the item so add it to the dtRemove DataTable.
                    dr2 = dtRemove.NewRow()
                    dr2("StuEnrollId") = lbxSelectStuds.Items(i).Value
                    dr2("Student") = lbxSelectStuds.Items(i).Text
                    dtRemove.Rows.Add(dr2)

                    If dr.RowState <> DataRowState.Added Then
                        'Mark the row as deleted
                        dr.Delete()
                    Else
                        dt.Rows.Remove(dr)
                    End If
                End If

                Session("SelectedStuds") = dt

            Next
            'Remember that we cannot simply clear the the selected items. If the number of rows in the dtRemove
            'data table is the same as the count of items in the selected list box then it means that all the
            'items should be removed and we can therefore use clear. If not we need to loop through the rows in
            'the dtRemove data table. we will remove the corresponding row in the selected list box. 
            If dtRemove.Rows.Count = lbxSelectStuds.Items.Count Then
                lbxSelectStuds.Items.Clear()
                removeAll = True
            End If

            For Each dr2 In dtRemove.Rows
                'Add the item to the available list box
                lbxAvailStuds.Items.Add(New ListItem(CType(dr2("Student"), String), CType(dr2("StuEnrollId"), String)))

                'If removeAll is set to true then we don't need to remove the items from the selected list box.
                'They were already removed above. If not we will need to find the item and then remove it.
                If removeAll = False Then
                    For i = 0 To lbxSelectStuds.Items.Count - 1
                        If dr2("StuEnrollId").ToString() = lbxSelectStuds.Items(i).Value.ToString Then
                            index = i
                        End If
                    Next
                    'Remove the item from the selected list box.
                    lbxSelectStuds.Items.RemoveAt(index)
                End If

                'Add this student back to the available student datatable in order to keep it in sync.
                ''IF Condition Added by Sara
                If availStdTable.Select(CType(("StuEnrollId='" + dr2("StuEnrollId") + "'"), String)).Length = 0 Then
                    availRow = availStdTable.NewRow()
                    availRow("StuEnrollId") = dr2("StuEnrollId")
                    availRow("FullName") = dr2("Student")
                    availStdTable.Rows.Add(availRow)
                End If

            Next

            'Show remaining students that can be accomodated.
            maxStud = CType(ViewState("MaxStud"), Integer)
            lblRemStds.Text = CType((maxStud - lbxSelectStuds.Items.Count), String)

            selectcount = lbxSelectStuds.Items.Count
            availcount = lbxAvailStuds.Items.Count

            lblAvailStuds.Text = "Available Students" & " (" & availcount & ")"
            lblSelStuds.Text = "Selected Students" & " (" & selectcount & ")"

            'If there are still items remaining in the selected list box we need to display a message that those
            'items could not be removed because they have final grades posted.
            If lbxSelectStuds.Items.Count > 0 Then
                DisplayErrorMessage("Some students could not be removed as they have final grades posted already.")
            End If

            'Save the Available datatable to Session 
            Session("AvailStuds") = availStdTable

        End Sub

        ''' <summary>
        ''' Save Changes (Registered or unregistered)
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' 
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            Dim dt1, dt3 As DataTable
            Dim dt2 As DataTable = Nothing
            Dim dr As DataRow
            Dim row As DataRow
            Dim clsSectId As String
            'Dim facade As New RegFacade
            Dim iStuEnrollId As String

            clsSectId = CType(ViewState("ClsSectId"), String)

            Try
                'Run get changes method on the dataset to see which rows have changed.
                'This section handles the changes to the Advantage Required fields
                dt1 = CType(Session("SelectedStuds"), DataTable)

                '********************************************************************
                'This if statement was added to fix the issue where an error pg is 
                'received upon an "empty" save.
                '********************************************************************

                If Not IsNothing(dt1) Then
                    dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted)
                End If
                dt3 = CType(Session("StdCoreqs"), DataTable)
                If Not IsNothing(dt2) Then

                    Dim onlinefac = New OnLineFacade()
                    For Each dr In dt2.Rows

                        'Get the enrollment to get from here the student Id later
                        iStuEnrollId = If(dr.RowState = DataRowState.Deleted, dr("StuEnrollId", DataRowVersion.Original).ToString, dr("StuEnrollId").ToString)

                        'When create the ArRegisterClass the object get all OnSchool Enrollment of the Student (get student from enrollment)
                        Dim reg As ArRegisterClass = New ArRegisterClass(iStuEnrollId, clsSectId)
                        reg.UserName = AdvantageSession.UserName
                        reg.CampusId = CampusId

                        'Get Co-requirement if exists
                        For Each row In dt3.Rows
                            Dim a = New RegisterCoRequirement(CType(row("StudentID"), String), CType(row("TestId"), String), CType(row("StuEnrollId"), String))
                            reg.RegisterCoRequirementList.Add(a)
                        Next

                        'If the record is deleted then unregistered the student from the class and associated class.
                        If dr.RowState = DataRowState.Deleted Then
                            If (reg.ValidateRegistrationDelete()) Then

                                reg.UnRegisteredClassWidthEnrollments()
                            End If

                        ElseIf dr.RowState = DataRowState.Added Then

                            ' If all enrollments were registered skip the registration
                            If (reg.ValidateRegistrationAdd()) Then

                                ' No all enrollments were registered, then proceed to register. The object know what are not registered if any
                                reg.RegisterClassWithEnrollments()
                            End If
                        End If

                        'Get the on-line updates if necessary. This can not me put in transaction because access a external service
                        For Each info As RegistrationClassInfo In reg.ValidatedEnrollmentClassRegistrationList
                            If OnLineLib.IsOnLineClassSection(info.ClassId) Then
                                Dim onLineInfo As OnLineInfo = onlinefac.GetOnLineInfoByStuEnrollId(info.StuEnrollmentId)
                                OnLineLib.UpdateStudentOnLineEnrollment(onLineInfo, info.UserName)
                            End If
                        Next

                    Next
                End If

                '********************************************************************
                'The following if statement was added to fix the issue where an error pg is 
                'received upon an "empty" save.
                '********************************************************************
                If Not IsNothing(dt1) Then
                    dt1.AcceptChanges()
                    'Rebind data-list with values
                    PopulateDataList("Build")
                End If

                Session("SelectedStuds") = dt1
                CommonWebUtilities.RestoreItemValues(dtlRegStud, txtClsSectId.Text)

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

#End Region

#Region "Private Functions"

        Private Sub PopulateHeader()
            '        Dim intpos As Integer
            Dim dt1 As DataTable
            Dim clsSectId As String
            Dim section As String
            Dim maxStud As Integer
            Dim row As DataRow
            Dim course As String

            pnlHeader.Visible = True

            dt1 = CType(Session("ClsSections"), DataTable)
            clsSectId = CType(ViewState("ClsSectId"), String)
            'Find the row that contains the ClsSectId.
            row = dt1.Rows.Find(clsSectId)
            'Get the Cls Section Name
            section = CType(row("ClsSection"), String)
            'Get Course Name
            course = CType(row("ChildDescripTyp"), String) 'DE9082 row("Class")
            'Get the max students to check against while adding to selected lbx
            maxStud = CType(row("MaxStud"), Integer)
            ViewState("MaxStud") = maxStud

            'Get the Remaining students that can be accomodated.
            lblRemStds.Text = CType((maxStud - lbxSelectStuds.Items.Count), String)
            lblCourseName.Text = course
            lblSecName.Text = section
            lblMaxStud.Text = CType(maxStud, String)
        End Sub

        Private Sub DisplayErrorMessage(ByVal errorMessage As String)

            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End Sub

        Private Sub BuildDropDownLists()
            'this is the list of ddls
            Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
            '        Dim SchoolItem As String

            'Countries DDL 
            ddlList.Add(New AdvantageDDLDefinition(ddlShift, AdvantageDropDownListName.Shifts, CampusId, True, True, String.Empty))

            'Build DDLs
            CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        End Sub

        Private Sub BuildCourseForProgram(ByVal progId As String)
            Dim fac As New TranscriptFacade
            With ddlCourse
                .DataSource = fac.GetCoursesForProgram(progId, CampusId, True)
                '.DataMember = "CourseDT"
                .DataValueField = "ReqId"
                .DataTextField = "FullDescrip"
                '.DataTextField = "Descrip"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub PopulateDataList(ByVal callingFunction As String, Optional ByVal guidToFind As String = "", Optional ByVal ds As DataSet = Nothing)
            'Dim dv As New DataView
            Dim ds2 As DataSet
            Dim tbl1 As DataTable = Nothing
            Dim status As Integer
            Dim dv1 As DataView = New DataView()

            Dim campusId1 As String = HttpContext.Current.Request.Params("cmpid")

            If ds Is Nothing Then
                Dim facade As New RegFacade
                If callingFunction = "Build" Then
                    ds2 = facade.PopulateDataListNew(CType(ViewState("Progid"), String), CType(ViewState("Term"), String), CType(ViewState("Shift"), String), campusId1, CType(ViewState("Course"), String), "NotTermModule")
                Else
                    ds2 = facade.PopulateDataList(CType(ViewState("Term"), String), CType(ViewState("Shift"), String), campusId1, CType(ViewState("Course"), String), "NotTermModule")
                End If

                tbl1 = ds2.Tables("TermClsSects")
                'Create data-view based on Status.
                status = CInt(ViewState("Status"))
                If status = 1 Then '(i.e Open)
                    dv1 = New DataView(tbl1, "Assigned < MaxStud and Remaining < MaxStud ", "Assigned", DataViewRowState.CurrentRows)

                ElseIf status = 2 Then ' (i.e Full)
                    dv1 = New DataView(tbl1, "Assigned = MaxStud", "Assigned", DataViewRowState.CurrentRows)

                ElseIf status = 3 Then  '(i.e Empty)
                    dv1 = New DataView(tbl1, "Assigned = 0", "Assigned", DataViewRowState.CurrentRows)

                ElseIf status = 0 Then  '(i.e Empty)

                    dv1 = New DataView(tbl1, Nothing, Nothing, DataViewRowState.CurrentRows)

                End If

            Else

            End If
            If dv1.Count > 0 Then
                Dim row As DataRowView
                Dim index As Integer
                Dim foundrecord As Boolean = False
                For Each row In dv1
                    If guidToFind = row("ClsSectionId").ToString Then
                        foundrecord = True
                        Exit For
                    End If
                    index += 1
                Next
                If foundrecord = True Then
                    Session("SelectedIndex") = index
                Else
                    Session("SelectedIndex") = "-1"
                End If
            Else
                Session("SelectedIndex") = "-1"
            End If

            dtlRegStud.SelectedIndex = CInt(Session("SelectedIndex"))
            With dtlRegStud
                If status >= 1 Then
                    .DataSource = dv1
                    .DataBind()

                Else
                    .DataSource = tbl1
                    .DataBind()
                End If
            End With

            Session("ClsSections") = tbl1
        End Sub

        Private Sub ClearRhs()
            Dim ctl As Control
            Try
                '***************************************************************
                'This section clears the Field Properties section of the page
                '***************************************************************
                For Each ctl In pnlRHS.Controls
                    If ctl.GetType Is GetType(TextBox) Then
                        CType(ctl, TextBox).Text = ""
                    End If
                    If ctl.GetType Is GetType(CheckBox) Then
                        CType(ctl, CheckBox).Checked = False
                    End If
                    If ctl.GetType Is GetType(DropDownList) Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                    End If
                Next

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

        Private Sub PopulateTermDDL(ByVal progId As String)
            '
            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New RegFacade
            With ddlTerm
                .DataSource = facade.GetCurrentAndFutureTermsForProgram(progId, CampusId)
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub BuildAllTerms()
            Dim facade As New RegFacade
            With ddlTerm
                .DataSource = facade.GetCurrentAndFutureTerms(CampusId, True)
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub BuildAllCourses()
            With ddlCourse
                .DataSource = (New StudentsAccountsFacade).GetAllCoursesNotCourseGroups(True, CampusId)
                '.DataMember = "CourseDT"
                .DataValueField = "ReqId"
                .DataTextField = "FullDescrip"
                '.DataTextField = "Descrip"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With

        End Sub

        Private Sub PopulateStatusDDL()
            '
            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New RegFacade

            ' Bind the Dataset to the CheckBoxList
            With ddlStatus
                .DataSource = facade.GetAllStatuses()
                .DataTextField = "Descrip"
                .DataValueField = "ClsSectStatusId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub PopulateProgramDDL(ByVal campusId1 As String)
            '
            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New RegFacade

            ' Bind the Dataset to the CheckBoxList
            With ddlProgId
                .DataSource = facade.GetAllProgramsOfferedInCampus(campusId1)
                .DataTextField = "ShiftDescrip"
                .DataValueField = "ProgId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

#End Region

#Region "DataList Events"

        Public Sub dtlRegStud_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs)
            Dim ds As DataSet
            Dim facade As New RegFacade
            Dim clsSectId As String
            Dim tbl1, tbl2 As DataTable
            Dim availCount As Integer = 0
            Dim selectCount As Integer = 0

            ViewState("ItemSelected") = e.Item.ItemIndex.ToString
            clsSectId = dtlRegStud.DataKeys(e.Item.ItemIndex).ToString
            ViewState("ClsSectId") = clsSectId

            ds = facade.GetClsSectStudents(clsSectId, CType(ViewState("Term"), String), HttpContext.Current.Request.Params("cmpid"), chkPreReqs.Checked)
            Dim sb As StringBuilder = New StringBuilder()

            For Each dr In ds.Tables(0).Rows
                Dim stEnrId = dr("StuEnrollId").ToString()

                If facade.GetTransfersForStudent_SP(stEnrId, clsSectId) = True Then
                    sb.Append(dr("FirstName").ToString() & " " & dr("LastName").ToString() & ", ")
                End If
            Next

            If sb.Length > 0 Then

                Dim st As String = sb.ToString()
                If st.Substring(st.Length - 2, 2) = ", " Then
                    st = st.Substring(0, st.Length - 2)
                End If

                Dim sb2 As StringBuilder = New StringBuilder()
                sb2.Append("The following students have at least one enrollment with a transfer grade for this Course. ")
                sb2.Append("If you want to apply that transfer grade to the existing student enrollment, navigate to Academics/Common Tasks/Grade Posting/Record Transfer Grades for Student" & vbCrLf)
                DisplayErrorMessage(sb2.ToString() & vbCrLf & st)
            End If


            tbl1 = ds.Tables("SelectedStuds")
            With ds.Tables("SelectedStuds")
                .PrimaryKey = New DataColumn() {tbl1.Columns("StuEnrollId")}
            End With

            If lbxSelectStuds.Items.Count > 0 Then
                lbxSelectStuds.Items.Clear()
            End If

            If ds.Tables("SelectedStuds").Rows.Count > 0 Then
                With lbxSelectStuds
                    .DataSource = ds.Tables("SelectedStuds")
                    .DataTextField = "FullName"
                    .DataValueField = "StuEnrollId"
                    .DataBind()
                End With
                selectCount = ds.Tables("SelectedStuds").Rows.Count

            End If



            'Save the dataset to session
            Session("SelectedStuds") = ds.Tables("SelectedStuds")


            'This section deals with populating the lbxAvailable list-box
            tbl2 = ds.Tables("AvailStuds")
            If tbl2.Rows.Count > 0 Then
                availCount = tbl2.Rows.Count

            End If
            With ds.Tables("AvailStuds")
                .PrimaryKey = New DataColumn() {tbl2.Columns("StuEnrollId")}
            End With

            'If the lbxAvailable control has items clear it
            If lbxAvailStuds.Items.Count > 0 Then
                lbxAvailStuds.Items.Clear()
            End If

            With lbxAvailStuds
                .DataSource = ds.Tables("AvailStuds")
                .DataTextField = "FullName"
                .DataValueField = "StuEnrollId"
                .DataBind()
            End With

            lblAvailStuds.Text = "Available Students" & " (" & availCount & ")"
            lblSelStuds.Text = "Selected Students" & " (" & selectCount & ")"
            'Save the data-tables to session
            Session("AvailStuds") = ds.Tables("AvailStuds")
            Session("StdCoreqs") = ds.Tables("StdCoreqs")

            'objCommon.SetBtnState(Form1, "EDIT")
            ViewState("MODE") = "EDIT"

            PopulateDataList("Build", clsSectId)
            PopulateHeader()

            If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
                btnSave.Enabled = True
                btnAdd.Enabled = True
                btnAddAll.Enabled = True
                btnRemove.Enabled = True
                btnRemoveAll.Enabled = True
            End If

            CommonWebUtilities.RestoreItemValues(dtlRegStud, CType(ViewState("ClsSectId"), String))
            '   CommonWebUtilities.SetStyleToSelectedItem(dtlRegStud, ViewState("ClsSectId"), ViewState, Header1)
            chkSort.Checked = False
            ChkSort1.Checked = False


        End Sub


#End Region

#Region "DropDown Events"

        Protected Sub ddlProgId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlProgId.SelectedIndexChanged
            ddlCourse.Items.Clear()
            If (ddlProgId.SelectedValue <> "") Then
                PopulateTermDDL(ddlProgId.SelectedItem.Value.ToString)
                BuildCourseForProgram(ddlProgId.SelectedValue)
            Else
                'ddlTerm.Items.Clear()
                BuildAllTerms()
                BuildAllCourses()
            End If

        End Sub

        Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTerm.SelectedIndexChanged
            Dim fac As New TranscriptFacade
            With ddlCourse
                If Not Trim(ddlProgId.SelectedValue) = "" And Not Trim(ddlTerm.SelectedValue) = "" Then
                    .DataSource = fac.GetCoursesForTerm(ddlProgId.SelectedValue, ddlTerm.SelectedValue, CampusId, True)
                    '.DataMember = "CourseDT"
                    .DataValueField = "ReqId"
                    .DataTextField = "FullDescrip"
                    '.DataTextField = "Descrip"
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", ""))
                    .SelectedIndex = 0
                ElseIf Not Trim(ddlProgId.SelectedValue) = "" And Trim(ddlTerm.SelectedValue) = "" Then
                    BuildCourseForProgram(ddlProgId.SelectedValue)
                ElseIf Trim(ddlProgId.SelectedValue) = "" And Not Trim(ddlTerm.SelectedValue) = "" Then
                    Dim progID As String = fac.GetProgramForTerm(ddlTerm.SelectedValue)
                    If progID <> "" Then
                        .DataSource = fac.GetCoursesForProgram(progID, CampusId, True)
                        '.DataMember = "CourseDT"
                        .DataValueField = "ReqId"
                        .DataTextField = "FullDescrip"
                        '.DataTextField = "Descrip"
                        .DataBind()
                        .Items.Insert(0, New ListItem("Select", ""))
                        .SelectedIndex = 0
                    Else
                        BuildAllCourses()
                    End If
                Else
                    BuildAllCourses()
                End If
            End With
        End Sub


#End Region

#Region "Checkbox Events"

        Private Sub chkSort_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkSort.CheckedChanged
            'Create dataview of the datatable w/ the available list names
            Const strRowFilter As String = ""
            Dim availStdTable As DataTable
            ''Added by Saraswathi lakshmanan on Sept 11 2009
            ''To fix mantis issue 17514: QA: Sort by Name option on register students page should work only if there are a list of students in it. 
            If lbxAvailStuds.Items.Count > 0 Then
                availStdTable = CType(Session("AvailStuds"), DataTable)

                Const strSort As String = "FullName"

                Dim dv As DataView
                dv = New DataView(availStdTable, strRowFilter, strSort, DataViewRowState.CurrentRows)

                With lbxAvailStuds
                    .DataSource = dv
                    .DataTextField = "FullName"
                    .DataValueField = "StuEnrollId"
                    .DataBind()
                End With
            End If
        End Sub

        Private Sub ChkSort1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkSort1.CheckedChanged
            'Create dataview of the datatable w/ the available list names
            Const strRowFilter As String = ""
            ''Added by Saraswathi lakshmanan on Sept 11 2009
            ''To fix mantis issue 17514: QA: Sort by Name option on register students page should work only if there are a list of students in it. 
            If lbxSelectStuds.Items.Count > 0 Then
                Dim selectStdTable As DataTable = CType(Session("SelectedStuds"), DataTable)
                Const strSort As String = "FullName"
                Dim dv As DataView
                dv = New DataView(selectStdTable, strRowFilter, strSort, DataViewRowState.CurrentRows)

                With lbxSelectStuds
                    .DataSource = dv
                    .DataTextField = "FullName"
                    .DataValueField = "StuEnrollId"
                    .DataBind()
                End With
            End If
        End Sub

#End Region

    End Class
End Namespace