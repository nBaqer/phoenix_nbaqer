<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="StartDateSetup" CodeFile="StartDateSetup.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Start Date Setup</title>
    <link rel="stylesheet" type="text/css" href="../css/localhost_lowercase.css" />
    <link href="../CSS/systememail.css" type="text/css" rel="stylesheet" />
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <script type="text/javascript" src="../AuditHist.js"></script>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
<!--
        function FillOutDates() {
            var startDate = document.getElementById('txtSDate');
            var endDate = document.getElementById('txtEndDate');
            var ed = Date.parse(endDate.value);
            var sd = Date.parse(startDate.value);
            //alert(startDate.value.length);
            //var yrStr=startDate.value.substring(startDate.value.length-4,4);
            var startYr = new Date(startDate.value);
            var endYr = new Date(endDate.value);
            //alert(Yr.getFullYear());
            //alert(yrStr);
            if (isNaN(ed) | isNaN(sd)) { return true }

            if (startYr.getFullYear() < 1945) {
                alert('Enter Proper Start Date Format');
                document.getElementById('txtMidPtDate').value = '';
                document.getElementById('txtMaxGradDate').value = '';
            }
            else if (endYr.getFullYear() < 1945) {
                alert('Enter Proper End Date Format');
                document.getElementById('txtMidPtDate').value = '';
                document.getElementById('txtMaxGradDate').value = '';
            }
            else if (ed < sd) {
                alert('End Date must be greater than the Start Date');
            }
            else {


                var d = new Date();

                d.setTime(sd + (ed - sd) / 2);

                document.getElementById('txtMidPtDate').value = d.getMonth() + 1 + '/' + d.getDate() + '/' + d.getFullYear();
                d.setTime(ed + (ed - sd) / 2);
                document.getElementById('txtMaxGradDate').value = d.getMonth() + 1 + '/' + d.getDate() + '/' + d.getFullYear();
            }
            return true;
        }
// -->
    </script>
</head>
<body runat="server" leftmargin="0" topmargin="0">
    <form id="Form1" method="post" runat="server">
    <!-- beging header -->
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="toppopupheader">
                <img src="../images/advantage_start_dates.jpg">
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="javascript:">X Close</a>
            </td>
        </tr>
    </table>
    <!-- end header -->
    <div>
     <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" />
        <asp:ScriptManager ID="scriptmanager1" runat="server">
        </asp:ScriptManager>
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="listframeprogramver">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table2">
                    <tr>
                        <td class="listframetop2">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="15%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftpopup">
                                <asp:DataList ID="dlstSDateSetup" runat="server" DataKeyField="TermId">
                                    <SelectedItemStyle CssClass="SelectedItemStyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="ItemStyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                            Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'
                                            CausesValidation="False"></asp:ImageButton>
                                        <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'
                                            CausesValidation="False"></asp:ImageButton>
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                        <asp:LinkButton Text='<%# Container.DataItem("TermDescrip")%>' runat="server" CssClass="ItemStyle"
                                            CommandArgument='<%# Container.DataItem("TermId")%>' ID="Linkbutton4" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList></div>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="leftside">
                <table cellspacing="0" cellpadding="0" width="9" border="0" id="Table3">
                </table>
            </td>
            <td class="detailsframetop">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                    <tr>
                        <td class="menuframe" align="right">
                             <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                             <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False">
                             </asp:Button>
                             <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False">
                             </asp:Button>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="detailsframe">
                            <div class="scrollrightprogramdate">
                                <asp:Panel ID="pnlRHS" runat="server">
                                    <table class="contenttable" width="70%" align="center">
                                        <tr>
                                            <td></td><br />
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblProgDescrip" runat="server" CssClass="label">Program</asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtProgDescrip" runat="server" ReadOnly="True" CssClass="textbox"
                                                    MaxLength="128"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSDateSetupCode" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSDateSetupCode" runat="server" MaxLength="128" CssClass="textbox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblShiftId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlShiftId" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSDateSetupDescrip" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSDateSetupDescrip" runat="server" MaxLength="128" CssClass="textbox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" style="text-align: left">
                                             <%--   <asp:TextBox ID="txtSDate" onchange="FillOutDates" onfocusout="FillOutDates();" runat="server"
                                                    CssClass="TextBoxDate" MaxLength="128"></asp:TextBox>&nbsp;<a href="javascript:OpenCalendar('ClsSect','txtSDate', true, 1945)"><img
                                                        id="IMG1" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                                        align="middle"></a>--%>
                                                    <telerik:RadDatePicker ID="txtSDate" MinDate="1/1/1945" runat="server" BorderColor="#E0E0E0"
                                                        cellpadding="4" Culture="English (United States)" daynameformat="Short" FocusedDate='<%# Date.Now %>'
                                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Width="200px" >
                                                        </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblEndDate" runat="server" CssClass="label">End Date</asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" style="text-align: left">
                                               <%-- <asp:TextBox ID="txtEndDate" onchange="FillOutDates" onfocusout="FillOutDates();"
                                                    runat="server" CssClass="textboxDate" MaxLength="128"></asp:TextBox>&nbsp;<a href="javascript:OpenCalendar('ClsSect','txtEndDate', true, 1945)"><img
                                                        id="Img3" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                                        align="middle"></a>--%>
                                                    <telerik:RadDatePicker ID="txtEndDate" MinDate="1/1/1945" runat="server" BorderColor="#E0E0E0"
                                                        cellpadding="4" Culture="English (United States)" daynameformat="Short" FocusedDate='<%# Date.Now %>'
                                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Width="200px" >
                                                        </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblMidPtDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" style="text-align: left">
                                               <%-- <asp:TextBox ID="txtMidPtDate" runat="server" onfocusin="FillOutDates();" CssClass="TextBoxDate"
                                                    MaxLength="128"></asp:TextBox>&nbsp;<a href="javascript:OpenCalendar('ClsSect','txtMidPtDate', true, 1945)"><img
                                                        id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                                        align="middle"></a>--%>
                                                    <telerik:RadDatePicker ID="txtMidPtDate" MinDate="1/1/1945" runat="server" BorderColor="#E0E0E0"
                                                        cellpadding="4" Culture="English (United States)" daynameformat="Short" FocusedDate='<%# Date.Now %>'
                                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Width="200px" >
                                                        </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblMaxGradDate" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" style="text-align: left">
                                                <%--<asp:TextBox ID="txtMaxGradDate" runat="server" CssClass="TextBoxDate" MaxLength="128"></asp:TextBox>&nbsp;<a
                                                    href="javascript:OpenCalendar('ClsSect','txtMaxGradDate', true, 1945)"><img id="Img4"
                                                        src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server" align="middle"></a>--%>
                                                    <telerik:RadDatePicker ID="txtMaxGradDate" MinDate="1/1/1945" runat="server" BorderColor="#E0E0E0"
                                                        cellpadding="4" Culture="English (United States)" daynameformat="Short" FocusedDate='<%# Date.Now %>'
                                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Width="200px" >
                                                       
                                                        </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" style="text-align: left">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                            </td>
                                            <td class="twocolumncontentcell" colspan="2" align="center">
                                                <asp:LinkButton ID="lnkClsStartDate" runat="server" CssClass="label" Visible="true"
                                                    Enabled="true">Set Up Classes for Non-Term Starts</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtProgId" runat="server" MaxLength="128" Visible="true" Width="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSDateSetupId" runat="server" MaxLength="128"
                                                    Visible="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" Width="0"></asp:TextBox>
                                <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" Width="0"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
    <asp:TextBox ID="txtOldCampGrpId" runat="server" Width="0px"></asp:TextBox>
    <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
    </form>
</body>
</html>
