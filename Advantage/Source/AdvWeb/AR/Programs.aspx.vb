'Imports FAME.common
'Imports System.Xml
'Imports System.Text
'Imports FAME.Advantage.Common
Imports Fame.Common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Fame.Advantage.Common
Partial Class Programs
    Inherits BasePage
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents lblCampGrpId As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlCampGrpId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents HyperLink2 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents HyperLink3 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected MyAdvAppSettings As AdvAppSettings

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim mContext As HttpContext
        mContext = HttpContext.Current
        'Linkbutton2.Attributes.Add("onclick", "OpenPrgVersion();return false;")
        ResourceId = txtResourceId.Text
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New IO.StringWriter
        Dim ds2 As DataSet
        Dim sStatusId As String
        Dim sdfControls As New SDFComponent
        'Dim userId As String
        'Dim fac As New UserSecurityFacade
        '   Dim resourceId2 As Integer
        Dim schedulingMethod As String = MyAdvAppSettings.AppSettings("SchedulingMethod")

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        'userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        txtCampusId.Text = campusId
        Try
            If Not Page.IsPostBack Then
                'Header1.EnableHistoryButton(False)
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Set the text box to a new guid
                txtProgId.Text = System.Guid.NewGuid.ToString

                ViewState("IsUserSa") = advantageUserState.IsUserSA

                ds = objListGen.SummaryListGenerator(userId, campusId)

                'Generate the status id
                ds2 = objListGen.StatusIdGenerator()

                'Set up the primary key on the datatable
                ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

                Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
                sStatusId = row("StatusId").ToString()

                'If Not Trim(Session("UserName")).ToString.ToLower = "sa" Then
                '    BuildCampusGroups()
                'End If


                Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "ProgDescrip", DataViewRowState.CurrentRows)
                With dlstPrograms
                    .DataSource = dv
                    .DataBind()
                End With
                ds.WriteXml(sw)
                Session("ds") = sw.ToString()
                Linkbutton2.Enabled = False
                Linkbutton1.Enabled = False
                'SDFControls.GenerateControlsNew(pnlSDF, ResourceId)
                If schedulingMethod = "ModuleStart" Then
                    pnlPeriods.Visible = True
                    txtNumPmtPeriods.Visible = True
                    lblNumPmtPeriods.Visible = True
                Else
                    pnlPeriods.Visible = False
                    txtNumPmtPeriods.Visible = False
                    lblNumPmtPeriods.Visible = False
                End If
                BuildDropDownLists()

            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)

            End If


            'Check If any UDF exists for this resource
            Dim intSdfExists As Integer = sdfControls.GetSDFExists(ResourceId, ModuleId)
            If intSdfExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If

            If Trim(txtProgCode.Text) <> "" Then
                sdfControls.GenerateControlsEditSingleCell(pnlSDF, ResourceId, txtProgId.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        Finally
        End Try
        headerTitle.Text = Header.Title
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New IO.StringWriter
        Dim msg As String
        Dim strErrMsg As String = ""
        Dim grdFacade As New GradesFacade
        Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)
        Try
            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtProgId.Text
                txtRowIds.Text = objCommon.PagePK

                msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                'Update Program Code
                'US3155 - Create a new credential level field for IPEDS - Advantage - not updating here
                grdFacade.UpdateProgramCIPCode(txtCipCode.Text, "", txtRowIds.Text, chkGEProgram.Checked, chk1098T.Checked) 'ddlCredentialLevel.SelectedValue

                ds = objListGen.SummaryListGenerator(userId, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
                dlstPrograms.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                Session("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                Linkbutton2.Enabled = True
            ElseIf ViewState("MODE") = "EDIT" Then
                'If (Trim(txtOldCampusGroups.Text) <> Trim(ddlCampGrpId.SelectedValue)) And (txtOldCampusGroupsText.Text <> "All") And (ddlCampGrpId.SelectedItem.Text <> "All") Then
                'Validate and check if campus is part of new group
                'Dim intRecordsAffected As Integer = 0
                Dim boolAllowCampusGroupChange As Boolean = False
                boolAllowCampusGroupChange = (New ProgVerFacade).AllowCampusGroupChange(txtProgId.Text, ddlCampGrpId.SelectedValue)
                If boolAllowCampusGroupChange = False Then
                    strErrMsg = "Unable to change the campus group for " & txtProgDescrip.Text & " due to the following reason." & vbCrLf
                    strErrMsg &= vbCrLf
                    strErrMsg &= "   - The program versions assigned to this program belongs to campuses " & vbCrLf
                    strErrMsg &= "   - that is not part of selected campus group: " & ddlCampGrpId.SelectedItem.Text
                    DisplayErrorMessage(strErrMsg)
                    Exit Sub
                End If
                '08/20/2012 JTorres DE11070 -- 'Validate if Program change to Inactive  all PrgVersions should be Inactive
                If ddlStatusId.SelectedItem.ToString() = "Inactive" Then
                    Dim boolThisProgramHasPrgVersionsActive As Boolean = False
                    boolThisProgramHasPrgVersionsActive = (New ProgVerFacade).HasThisProgramPrgVersionsActive(txtProgId.Text)
                    If boolThisProgramHasPrgVersionsActive = True Then
                        strErrMsg = "Program '" & txtProgDescrip.Text & "' cannot be made Inactive as there are Active Program Versions in use. "
                        strErrMsg &= "If any program versions have active enrollments you should not make this change. "
                        strErrMsg &= "If no enrollments exist then select View Versions of This Program' and change the status of existing Program Versions to 'Inactive'. "
                        strErrMsg &= "Once done you will be able to inactivate the program."
                        DisplayErrorMessage(strErrMsg)
                        Exit Sub
                    End If
                End If

                'intRecordsAffected = (New ProgVerFacade).IsCampusPartOfGroup(campusId, ddlCampGrpId.SelectedValue)
                'If intRecordsAffected = 0 Then
                '    DisplayErrorMessage("The campus is not part of the selected campus group")
                '    Exit Sub
                'End If
                'Dim winSettings As String = "toolbar=no,status=no,width=700px,height=400px,left=250px,top=200px,modal=yes"
                'Dim name As String = "AdmReqSummary1"
                'Dim ds1 As New DataSet
                'Dim intPrgVersionExists As Integer
                'ds1 = (New ProgVerFacade).GetProgVersionsByCampusGroupandProgram(txtProgId.Text, txtOldCampusGroups.Text)
                'intPrgVersionExists = ds1.Tables(0).Rows.Count
                'If intPrgVersionExists >= 1 Then
                '    Dim URL As String = "DisplayProgramVersions.aspx?ProgId=" + txtProgId.Text + "&CampGrpId=" + txtOldCampusGroups.Text + "&Program=" + txtProgDescrip.Text + "&NewCampus=" + ddlCampGrpId.SelectedItem.Text + "&OldCampusGroup=" + Session("OldCampusGroup")
                '    CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                '    Exit Sub
                'End If
                'End If
                msg = objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                'Update Program Code
                'US3155 - Create a new credential level field for IPEDS - Advantage - not updating here
                grdFacade.UpdateProgramCIPCode(txtCipCode.Text, "", txtRowIds.Text, chkGEProgram.Checked, chk1098T.Checked) 'ddlCredentialLevel.SelectedValue

                ds = objListGen.SummaryListGenerator(userId, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
                dlstPrograms.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                Session("ds") = sw.ToString()
                Linkbutton2.Enabled = True
            End If
            Linkbutton1.Enabled = True
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim sdfid As ArrayList
            Dim sdfidValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim sdfControl As New SDFComponent
            Try
                sdfControl.DeleteSDFValue(txtProgId.Text)
                sdfid = sdfControl.GetAllLabels(pnlSDF)
                sdfidValue = sdfControl.GetAllValues(pnlSDF)
                For z = 0 To sdfid.Count - 1
                    sdfControl.InsertValues(txtProgId.Text, Mid(sdfid(z).id, 5), sdfidValue(z))
                Next
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPrograms, txtProgId.Text, ViewState, Header1)

            CommonWebUtilities.RestoreItemValues(dlstPrograms, txtProgId.Text)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                If ex.Message.IndexOf("with unique index") Then
                    Dim message As String = "This record already exists in the database"
                    DisplayErrorMessage(message)
                Else
                    Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
                    Response.Redirect("../ErrorPage.aspx")
                End If
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
                Response.Redirect("../ErrorPage.aspx")
            End If
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(Session("ds")))
        Try
            ClearRHS()
            ds.ReadXml(sr)
            PopulateDataList(ds)
            'Set the text box to a new guid
            txtProgId.Text = System.Guid.NewGuid.ToString
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Reset Style in the Datalist
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPrograms, Guid.Empty.ToString, ViewState, Header1)
            Linkbutton2.Enabled = False
            Linkbutton1.Enabled = False
            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
            CommonWebUtilities.RestoreItemValues(dlstPrograms, Guid.Empty.ToString)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim msg As String
        Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)
        Try
            msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If msg <> "" Then
                DisplayErrorMessage(msg)
            End If
            ClearRHS()
            ds = objListGen.SummaryListGenerator(userId, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
            dlstPrograms.SelectedIndex = -1
            PopulateDataList(ds)
            ds.WriteXml(sw)
            Session("ds") = sw.ToString()
            'disable new and delete buttons.
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtProgId.Text = System.Guid.NewGuid.ToString
            'Header1.EnableHistoryButton(False)
            Linkbutton2.Enabled = False
            Linkbutton1.Enabled = False
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim SDFControl As New SDFComponent
            SDFControl.DeleteSDFValue(txtProgId.Text)
            SDFControl.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CommonWebUtilities.RestoreItemValues(dlstPrograms, Guid.Empty.ToString)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
            chkGEProgram.Checked = True
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub dlstPrograms_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstPrograms.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        'Dim objListGenerator As New DataListGenerator
        Dim sr As New IO.StringReader(CStr(Session("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim dsCipCode As DataSet
        Try
            ddlCredentialLevel.SelectedIndex = -1
            strGUID = dlstPrograms.DataKeys(e.Item.ItemIndex).ToString()
            Master.PageObjectId = e.CommandArgument
            Master.PageResourceId = ResourceId
            Master.SetHiddenControlForAudit()

            txtRowIds.Text = e.CommandArgument.ToString
            txtProgId.Text = strGUID
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"

            selIndex = e.Item.ItemIndex
            dlstPrograms.SelectedIndex = selIndex
            ds.ReadXml(sr)
            PopulateDataList(ds)

            'Get CIPCode
            dsCipCode = (New GradesFacade).GetProgramCIPCode(strGUID)

            Dim drRow As DataRow = dsCipCode.Tables(0).Rows(0)

            Try
                txtCipCode.Text = CType(drRow("CIPCode"), String)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                txtCipCode.Text = ""
            End Try
            'Try
            '    ddlCredentialLevel.SelectedValue = drRow("CredentialLevel")
            'Catch ex As Exception
            '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    ddlCredentialLevel.SelectedIndex = 0
            'End Try

            Try
                chkGEProgram.Checked = CType(drRow("IsGEProgram"), Boolean)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                chkGEProgram.Checked = False
            End Try

            Try
                chk1098T.Checked = CType(drRow("Is1098T"), Boolean)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                chk1098T.Checked = False
            End Try

            txtOldCampusGroups.Text = ddlCampGrpId.SelectedValue
            txtOldCampusGroupsText.Text = ddlCampGrpId.SelectedItem.Text
            Session("OldCampusGroup") = ddlCampGrpId.SelectedItem.Text

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPrograms, e.CommandArgument, ViewState, Header1)

            Linkbutton2.Enabled = True
            'Linkbutton1.Enabled = True
            If CommonWebUtilities.GetSchedulingMethod(MyAdvAppSettings.AppSettings("SchedulingMethod", campusId)) = AdvantageCommonValues.SchedulingMethods.ModuleStart Then
                Linkbutton1.Visible = True
                Linkbutton1.Enabled = True
            Else
                Linkbutton1.Visible = False
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
            'SDF Code Starts Here

            Dim sdfControls As New SDFComponent
            sdfControls.GenerateControlsEditSingleCell(pnlSDF, ResourceId, txtProgId.Text, ModuleId)

            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            CommonWebUtilities.RestoreItemValues(dlstPrograms, txtProgId.Text)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstPrograms_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstPrograms_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        Finally
        End Try
    End Sub
    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        'Dim ds2 As New DataSet
        'Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        'Dim dv2 As New DataView
        '        Dim sStatusId As String
        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtProgId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
            Linkbutton2.Enabled = False
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As DataSet
        Dim dv2 As DataView
        Dim objListGen As New DataListGenerator
        Dim sStatusId As String

        ds2 = objListGen.StatusIdGenerator()
        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        'If (chkStatus.Checked = True) Then 'Show Active Only

        '    Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
        '    sStatusId = row("StatusId").ToString()
        '    'Create dataview which displays active records only
        '    Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "ProgDescrip", DataViewRowState.CurrentRows)
        '    dv2 = dv
        'ElseIf (chkStatus.Checked = False) Then

        '    Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
        '    sStatusId = row("StatusId").ToString()
        '    'Create dataview which displays inactive records only
        '    Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "ProgDescrip", DataViewRowState.CurrentRows)
        '    dv2 = dv
        'End If

        If (radstatus.SelectedItem.Text = "Active") Then   'Show Active Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays active records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "ProgDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        ElseIf (radstatus.SelectedItem.Text = "Inactive") Then   'Show Inactive Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "ProgDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        Else  'Show All
            ds = objListGen.SummaryListGeneratorSort()
            ' Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            'sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows)
            dv2 = dv
        End If


        With dlstPrograms
            .DataSource = dv2
            .DataBind()
        End With
        dlstPrograms.SelectedIndex = -1

    End Sub
    'Private Sub LinkButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Linkbutton2.Click
    '    Response.Redirect("ProgVersions.aspx?resid=61&Program=" & txtProgDescrip.Text)
    'End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        'Dim ds2 As New DataSet
        'Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)
        'Dim dv2 As New DataView
        '        Dim sStatusId As String

        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtProgId.Text = Guid.NewGuid.ToString
            ' ds = objListGen.SummaryListGenerator()
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
            'Header1.EnableHistoryButton(False)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkbutton2.Click

        Dim sb As New StringBuilder
        '   append Program Description to the querystring
        sb.Append("&Program=" + Server.UrlEncode(txtProgDescrip.Text))
        '   append ProgId to the querystring
        sb.Append("&ProgId=" + txtProgId.Text)
        ' append ProgType to the querystring
        sb.Append("&ProgType=" + ddlACId.SelectedItem.Text)
        '   setup the properties of the new window
        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "ProgVersions"
        Dim url As String = "../AR/" + name + ".aspx?resid=61&mod=AR&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        'theChild = window.open('../AR/ProgVersions.aspx?resid=61&Program=' + Program1 + '&ProgId=' + ProgId1 + '&CampusId=' + CampId,'ProgramVersions','Width=900,height=600');

    End Sub

    Protected Sub Linkbutton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkbutton1.Click
        Dim sb As New StringBuilder
        '   append PrgVersion to the querystring
        sb.Append("&Program=" + Server.UrlEncode(txtProgDescrip.Text))
        '   append ProgVerId Description to the querystring
        sb.Append("&ProgId=" + txtProgId.Text)

        '   setup the properties of the new window
        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "StartDateSetup"
        Dim url As String = "../AR/" + name + ".aspx?resid=68&mod=AR&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(Linkbutton1)
        controlsToIgnore.Add(Linkbutton2)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    'Private Sub BuildCampusGroups()
    '    With ddlCampGrpId
    '        .DataTextField = "CampGrpDescrip"
    '        .DataValueField = "CampGrpId"
    '        .DataSource = (New CampusGroupsFacade).GetAllCampGroupsByCampusId(campusId)
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub

    Public Function getDesc(ByVal progDesc As Object, ByVal shiftDesc As Object) As String
        Dim rtn As String = progDesc
        If Not IsDBNull(shiftDesc) Then
            If shiftDesc.ToString <> "" Then rtn = progDesc & " (" & shiftDesc & ")"
        End If
        Return rtn
    End Function

    Private Sub BuildDropDownLists()
        'BuildPrefixDDL()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Credential Status
        ddlList.Add(New AdvantageDDLDefinition(ddlCredentialLevel, AdvantageDropDownListName.CredentialLevel, campusId, True, True, String.Empty))
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
