Imports System.Web.UI.HtmlControls
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.Xml

Public Class StartDateClasses
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlAttendance2 As System.Web.UI.WebControls.Panel
    Protected MyAdvAppSettings As AdvAppSettings

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Private resourceId As Integer
    Private userId As String
    Private campusID As String
    Private termId As String
    Private term As String
    Private programID As String
    Private program As String
    Private setupErrMsg As String
    Private shift As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim fac As New StartDateFacade
        Dim ds As New DataSet
        Dim facSecurity As New UserSecurityFacade

        If Not Request.Item("TermId") Is Nothing Then
            termId = CType(Request.Item("TermId"), String)
            term = CType(Request.Item("Term"), String)
            programID = CType(Request.Item("ProgramId"), String)
            program = CType(Request.Item("Program"), String)
            campusID = XmlConvert.ToGuid(Request.Params("cmpid")).ToString
            shift = CType(Request.Item("shift"), String)
            lblTermDescrip.Text = term
            lblProgramDescrip.Text = program
        End If
        '-------------------------------------------------------------------------------------------------------------
        'This section of code must be commented out when we integrate the page with Advantage and the header control
        '-------------------------------------------------------------------------------------------------------------
        'termId = "{F247F9E8-54CB-4CCC-9A1F-2069CAE32183}"
        'termID = "{F47413D9-7CF2-4F18-AC78-25C2AB2FC302}"
        'termID = "{4E3D7F65-59FF-420B-8A6E-D3B226660C7C}"
        'programID = "{7FBAD920-9BC1-4E3F-9092-38B957D749F0}"
        'campusID = "{3F5E839A-589A-4B2A-B258-35A1A8B3B819}"
        'Session("UserName") = "sa"

        '---------------------------------------------------------------------------------------------------------
        'This section of code must be uncommented when we integrate the page with Advantage and the header control
        '---------------------------------------------------------------------------------------------------------
        'resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'campusID = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString

        'pObj = facSecurity.GetUserResourcePermissions(userId, resourceId, campusID)
        '------------------------------------------------------------------------------------------------------------
        If Not Page.IsPostBack Then
            BuildAttendanceGrid(True)
            BuildDropDownLists()
            BuildAvailableListBox()
            BuildGradeScaleDDL()
            BuildShiftDDL()
            Dim item As ListItem = ddlShiftId.Items.FindByValue(shift)
            If Not item Is System.DBNull.Value Then
                ddlShiftId.SelectedValue = item.Value
            End If
            'Get a DataSet with the relevant tables
            ds = fac.GetStartDateClassesAndMeetings(termId)
            Session("StartDateClassesAndMeetings") = ds

            If ds.Tables("ClassSections").Rows.Count > 0 Then
                BindExistingDataToTable(ds)
                BindGradeScaleAndShift(ds)
            End If

            'If there are students registered for the start date we want to disable the Save button.
            If fac.AreStuentsRegisteredForStart(termId) Then
                If Session("UserName") <> "sa" Then
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            End If
            If MyAdvAppSettings.AppSettings("SchedulingMethod", campusID) = "ModuleStart" Then
                Dim studentCount As Integer = (New StartDatesFacade).GetStudentCount(termId)
                'If (studentCount > 0) Then btnSave.Enabled = False
            End If

        Else
            'We are using a dynamically created table so we have to rebuild it on the post back
            'However, because we are relying on the GUIDS we want to to use the values from session
            'for the GUIDS
            BuildAttendanceGrid(False)

        End If



    End Sub

    Private Sub BuildAttendanceGrid(Optional ByVal newTable As Boolean = False)
        'First create the header
        Dim tbl As New Table
        '        Dim dRow As DataRow
        '   Dim dtm As DateTime
        Dim r As New TableRow
        Dim c1 As New TableCell
        Dim c2 As New TableCell
        Dim c3 As New TableCell
        Dim c4 As New TableCell
        Dim c5 As New TableCell
        Dim c6 As New TableCell
        Dim c7 As New TableCell
        Dim c8 As New TableCell
        Dim c9 As New TableCell
        Dim c10 As New TableCell
        Dim lblCourse As New Label
        Dim lblInstructor As New Label
        Dim lblPeriod As New Label
        Dim lblAltPeriod As New Label
        Dim lblStartDate As New Label
        Dim lblEndDate As New Label
        Dim lblRoom As New Label
        Dim lblMaxStud As New Label
        Dim lblRow As New Label
        Dim lblRelatedStarts As New Label
        Dim htValues As New Hashtable
        Dim fac As New StartDateFacade
        Dim dvRow1H As New HtmlGenericControl("div")
        Dim dvRow2H As New HtmlGenericControl("div")
        Dim dvCourseH As New HtmlGenericControl("div")
        Dim dvInstructorH As New HtmlGenericControl("div")
        Dim dvPeriodH As New HtmlGenericControl("div")
        Dim dvAltPeriodH As New HtmlGenericControl("div")
        Dim dvStartDateH As New HtmlGenericControl("div")
        Dim dvEndDateH As New HtmlGenericControl("div")
        Dim dvRoomH As New HtmlGenericControl("div")
        Dim dvMaxStudH As New HtmlGenericControl("div")
        Dim dvRelatedStarts1H As New HtmlGenericControl("div")
        Dim dvRelatedStarts2H As New HtmlGenericControl("div")


        dvRow1H.Attributes.Add("Class", "moduledivrow")
        dvRow2H.Attributes.Add("Class", "moduledivrow")
        dvCourseH.Attributes.Add("Class", "modulediv")
        dvInstructorH.Attributes.Add("Class", "moduledivbottom")
        dvPeriodH.Attributes.Add("Class", "modulediv")
        dvAltPeriodH.Attributes.Add("Class", "moduledivbottom")
        dvStartDateH.Attributes.Add("Class", "modulediv")
        dvEndDateH.Attributes.Add("Class", "moduledivbottom")
        dvRoomH.Attributes.Add("Class", "modulediv")
        dvMaxStudH.Attributes.Add("Class", "moduledivbottom")
        dvRelatedStarts1H.Attributes.Add("Class", "moduledivrow")
        dvRelatedStarts2H.Attributes.Add("Class", "moduledivrow")

        'The containing table.
        tbl.Width = Unit.Percentage(100)
        tbl.CellPadding = 0
        tbl.CellSpacing = 0
        tbl.CssClass = "modulestarttable"


        'The Header Row
        lblRow.CssClass = "labelbold"
        lblRow.Text = "Row"
        dvRow1H.Controls.Add(lblRow)
        c1.VerticalAlign = VerticalAlign.Bottom
        c1.Controls.Add(dvRow1H)
        c1.Controls.Add(dvRow2H)
        c1.CssClass = "datagridheadermodulestartrow"
        c1.Width = Unit.Percentage(3)
        r.Cells.Add(c1)

        lblCourse.CssClass = "labelbold"
        lblCourse.Text = "Course"
        dvCourseH.Controls.Add(lblCourse)
        c2.VerticalAlign = VerticalAlign.Bottom
        c2.Controls.Add(dvCourseH)
        lblInstructor.CssClass = "labelbold"
        lblInstructor.Text = "Instructor"
        dvInstructorH.Controls.Add(lblInstructor)
        c2.Controls.Add(dvInstructorH)
        c2.CssClass = "modulecellheader"
        c2.Width = Unit.Percentage(35)
        r.Cells.Add(c2)

        lblPeriod.CssClass = "labelbold"
        lblPeriod.Text = "Period"
        dvPeriodH.Controls.Add(lblPeriod)
        c3.VerticalAlign = VerticalAlign.Bottom
        c3.Controls.Add(dvPeriodH)
        lblAltPeriod.CssClass = "labelbold"
        lblAltPeriod.Text = "Alt Period"
        dvAltPeriodH.Controls.Add(lblAltPeriod)
        c3.Controls.Add(dvAltPeriodH)
        c3.CssClass = "modulecellheader"
        c3.Width = Unit.Percentage(15)
        r.Cells.Add(c3)

        lblStartDate.CssClass = "labelbold"
        lblStartDate.Text = "Start Date"
        dvStartDateH.Controls.Add(lblStartDate)
        c4.VerticalAlign = VerticalAlign.Bottom
        c4.Controls.Add(dvStartDateH)
        lblEndDate.CssClass = "labelbold"
        lblEndDate.Text = "End Date"
        dvEndDateH.Controls.Add(lblEndDate)
        c4.Controls.Add(dvEndDateH)
        c4.CssClass = "modulecellheader"
        c4.Width = Unit.Percentage(14)
        r.Cells.Add(c4)


        lblRoom.CssClass = "labelbold"
        lblRoom.Text = "Room"
        dvRoomH.Controls.Add(lblRoom)
        c4.VerticalAlign = VerticalAlign.Bottom
        c5.Controls.Add(dvRoomH)
        lblMaxStud.CssClass = "labelbold"
        lblMaxStud.Text = "Max Student"
        dvMaxStudH.Controls.Add(lblMaxStud)
        c5.Controls.Add(dvMaxStudH)
        c5.CssClass = "modulecellheader"
        c5.Width = Unit.Percentage(13)
        r.Cells.Add(c5)

        lblRelatedStarts.CssClass = "labelbold"
        lblRelatedStarts.Text = "Related Starts"
        dvRelatedStarts1H.Controls.Add(lblRelatedStarts)
        c6.VerticalAlign = VerticalAlign.Bottom
        c6.Controls.Add(dvRelatedStarts1H)
        c6.Controls.Add(dvRelatedStarts2H)
        c6.CssClass = "datagridheadermodulestartrow"
        c6.Width = Unit.Percentage(20)
        r.Cells.Add(c6)

        tbl.Rows.Add(r)

        'We want to create 100 rows for the table by default.
        'Code will be added later to allow the user to add more rows if necessary.
        For i As Integer = 1 To 100
            Dim lblCounter As New Label
            Dim lblClsSectionId As New Label
            Dim lblClsSectMeetingId As New Label
            Dim ddlCourse As New DropDownList
            Dim ddlPeriod As New DropDownList
            Dim ddlAltPeriod As New DropDownList
            Dim ddlRoom As New DropDownList
            Dim txtStartDate As New TextBox
            Dim txtEndDate As New TextBox
            Dim txtMaxStud As New TextBox
            Dim ddlInstructor As New DropDownList
            Dim lbxRelatedStarts As New ListBox
            Dim rnvMaxStud As New RangeValidator
            Dim rnvStartDate As New RangeValidator
            Dim rnvEndDate As New RangeValidator
            Dim cmvEndDate As New CompareValidator

            Dim rContent As New TableRow
            Dim cCounter As New TableCell
            Dim cCourse As New TableCell
            Dim cPeriod As New TableCell
            Dim cAltPeriod As New TableCell
            Dim cRoom As New TableCell
            Dim cStartDate As New TableCell
            Dim cEndDate As New TableCell
            Dim cMaxStud As New TableCell
            Dim cInstructor As New TableCell
            Dim cRelatedStarts As New TableCell

            Dim dvRow1C As New HtmlGenericControl("div")
            Dim dvRow2C As New HtmlGenericControl("div")
            Dim dvCourseC As New HtmlGenericControl("div")
            Dim dvInstructorC As New HtmlGenericControl("div")
            Dim dvPeriodC As New HtmlGenericControl("div")
            Dim dvAltPeriodC As New HtmlGenericControl("div")
            Dim dvStartDateC As New HtmlGenericControl("div")
            Dim dvEndDateC As New HtmlGenericControl("div")
            Dim dvRoomC As New HtmlGenericControl("div")
            Dim dvMaxStudC As New HtmlGenericControl("div")
            Dim dvRelatedStarts1C As New HtmlGenericControl("div")
            Dim dvRelatedStarts2C As New HtmlGenericControl("div")

            dvRow1C.Attributes.Add("Class", "moduledivrow")
            dvRow2C.Attributes.Add("Class", "moduledivrow")
            dvCourseC.Attributes.Add("Class", "modulediv")
            dvInstructorC.Attributes.Add("Class", "moduledivbottom")
            dvPeriodC.Attributes.Add("Class", "modulediv")
            dvAltPeriodC.Attributes.Add("Class", "moduledivbottom")
            dvStartDateC.Attributes.Add("Class", "modulediv")
            dvEndDateC.Attributes.Add("Class", "moduledivbottom")
            dvRoomC.Attributes.Add("Class", "modulediv")
            dvMaxStudC.Attributes.Add("Class", "moduledivbottom")
            dvRelatedStarts1C.Attributes.Add("Class", "moduledivrow")
            dvRelatedStarts2C.Attributes.Add("Class", "moduledivrow")

            Dim smi As New SectionAndMeetingIDS
            If newTable = True Then
                'We have to add the above two guids to the hashtable that we will store in session.
                'We will use the session object later on to retrieve these guids when there is a post back.
                lblClsSectionId.Text = Guid.NewGuid.ToString
                lblClsSectMeetingId.Text = Guid.NewGuid.ToString
                smi.ClsSectionId = XmlConvert.ToGuid(lblClsSectionId.Text)
                smi.ClsSectMeetingId = XmlConvert.ToGuid(lblClsSectMeetingId.Text)
                htValues(i) = smi
            Else
                'We need to pull the values from session
                lblClsSectionId.Text = DirectCast(Session("SectionAndMeetings"), Hashtable).Item(i).ClsSectionId.ToString
                lblClsSectMeetingId.Text = DirectCast(Session("SectionAndMeetings"), Hashtable).Item(i).ClsSectMeetingId.ToString
            End If

            lblCounter.ID = "lblCounter" & i.ToString
            rContent.CssClass = "datagriditemmodulestartrow"
            rContent.Width = Unit.Percentage(3)
            cCounter.CssClass = "datagriditemmodulestartrow"
            lblCounter.CssClass = "labelsmall"
            lblCounter.Font.Bold = True
            lblClsSectionId.ID = "lblClsSectionId" & i.ToString
            lblClsSectionId.Visible = False
            lblClsSectMeetingId.ID = "lblClsSectMeetingId" & i.ToString
            lblClsSectMeetingId.Visible = False
            lblCounter.Text = i.ToString
            dvRow1C.Controls.Add(lblCounter)
            dvRow1C.Controls.Add(lblClsSectionId)
            dvRow1C.Controls.Add(lblClsSectMeetingId)
            cCounter.Controls.Add(dvRow1C)
            cCounter.Controls.Add(dvRow2C)

            ddlCourse.ID = "ddlCourse" & i.ToString
            cCourse.CssClass = "datagriditemmodulestart"
            cCourse.Width = Unit.Percentage(35)
            ddlCourse.CssClass = "dropdownlistmodulestart"
            dvCourseC.Controls.Add(ddlCourse)
            cCourse.Controls.Add(dvCourseC)
            ddlInstructor.ID = "ddlInstructor" & i.ToString
            ddlInstructor.CssClass = "dropdownlistsmall"
            dvInstructorC.Controls.Add(ddlInstructor)
            cCourse.Controls.Add(dvInstructorC)

            ddlPeriod.ID = "ddlPeriod" & i.ToString
            cPeriod.CssClass = "datagriditemmodulestart"
            cPeriod.Width = Unit.Percentage(15)
            ddlPeriod.CssClass = "DropDownListsmall"
            dvPeriodC.Controls.Add(ddlPeriod)
            cPeriod.Controls.Add(dvPeriodC)
            ddlAltPeriod.ID = "ddlAltPeriod" & i.ToString
            ddlAltPeriod.CssClass = "dropdownlistsmall"
            dvAltPeriodC.Controls.Add(ddlAltPeriod)
            cPeriod.Controls.Add(dvAltPeriodC)

            txtStartDate.ID = "txtStartDate" & i.ToString
            txtStartDate.CssClass = "textboxsmall"
            cStartDate.CssClass = "datagriditemmodulestart"
            cStartDate.Width = Unit.Percentage(14)
            dvStartDateC.Controls.Add(txtStartDate)
            cStartDate.Controls.Add(dvStartDateC)
            txtEndDate.CssClass = "textboxsmall"
            txtEndDate.ID = "txtEndDate" & i.ToString
            dvEndDateC.Controls.Add(txtEndDate)
            cStartDate.Controls.Add(dvEndDateC)

            ddlRoom.ID = "ddlRoom" & i.ToString
            cRoom.CssClass = "datagriditemmodulestart"
            cRoom.Width = Unit.Percentage(13)
            ddlRoom.CssClass = "dropdownlistsmall"
            dvRoomC.Controls.Add(ddlRoom)
            cRoom.Controls.Add(dvRoomC)
            txtMaxStud.ID = "txtMaxStud" & i.ToString
            txtMaxStud.CssClass = "textboxsmall"
            dvMaxStudC.Controls.Add(txtMaxStud)
            cRoom.Controls.Add(dvMaxStudC)

            cRelatedStarts.CssClass = "datagriditemmodulestart"
            cRelatedStarts.Width = Unit.Percentage(20)
            lbxRelatedStarts.ID = "lbxRelatedStarts" & i.ToString
            lbxRelatedStarts.Rows = 3
            lbxRelatedStarts.CssClass = "listboxes"
            lbxRelatedStarts.SelectionMode = ListSelectionMode.Multiple
            cRelatedStarts.Controls.Add(lbxRelatedStarts)

            rContent.Controls.Add(cCounter)
            rContent.Controls.Add(cCourse)
            rContent.Controls.Add(cPeriod)
            rContent.Controls.Add(cStartDate)
            rContent.Controls.Add(cRoom)
            rContent.Controls.Add(cRelatedStarts)

            'Place a range validator to validate the maximum student field
            rnvMaxStud.ID = "rnv" + txtMaxStud.ID
            rnvMaxStud.ControlToValidate = txtMaxStud.ID
            rnvMaxStud.Type = ValidationDataType.Integer
            rnvMaxStud.MinimumValue = 0
            rnvMaxStud.MaximumValue = 200
            rnvMaxStud.Display = ValidatorDisplay.None
            rnvMaxStud.ErrorMessage = "Enter a value between 0 and 200 for max students for row " & i.ToString
            pnlRequiredFieldValidators.Controls.Add(rnvMaxStud)

            'Place a range validator to validate the start date. The start date of the class/meeting cannot be earlier than
            'the start of the start date.
            rnvStartDate.ID = "rnv" + txtStartDate.ID
            rnvStartDate.ControlToValidate = txtStartDate.ID
            rnvStartDate.Type = ValidationDataType.Date
            rnvStartDate.MinimumValue = Convert.ToDateTime(fac.GetTermStartDate(termId))
            rnvStartDate.MaximumValue = Convert.ToDateTime(fac.GetTermEndDate(termId))
            rnvStartDate.Display = ValidatorDisplay.None
            rnvStartDate.ErrorMessage = "Enter a date between " & rnvStartDate.MinimumValue.ToString & " and " & rnvStartDate.MaximumValue.ToString & " for start date for row " & i.ToString
            pnlRequiredFieldValidators.Controls.Add(rnvStartDate)

            'Place a range validator to validate the end date. The end date of the class/meeting cannot be later than
            'the end of the start date.
            rnvEndDate.ID = "rnv" + txtEndDate.ID
            rnvEndDate.ControlToValidate = txtEndDate.ID
            rnvEndDate.Type = ValidationDataType.Date
            rnvEndDate.MinimumValue = Convert.ToDateTime(fac.GetTermStartDate(termId))
            rnvEndDate.MaximumValue = Convert.ToDateTime(fac.GetTermEndDate(termId))
            rnvEndDate.Display = ValidatorDisplay.None
            rnvEndDate.ErrorMessage = "Enter a date between " & rnvEndDate.MinimumValue.ToString & " and " & rnvEndDate.MaximumValue.ToString & " for end date for row " & i.ToString
            pnlRequiredFieldValidators.Controls.Add(rnvEndDate)

            'The end date must be greater than the start date
            cmvEndDate.ID = "cmv" + txtEndDate.ID
            cmvEndDate.ControlToValidate = txtEndDate.ID
            cmvEndDate.ControlToCompare = txtStartDate.ID
            cmvEndDate.Type = ValidationDataType.Date
            cmvEndDate.Operator = ValidationCompareOperator.GreaterThanEqual
            cmvEndDate.Display = ValidatorDisplay.None
            cmvEndDate.ErrorMessage = "The end date for row " & i.ToString & " must be greater than or equal to the start date"
            pnlRequiredFieldValidators.Controls.Add(cmvEndDate)

            'tbl.EnableViewState = False
            tbl.Rows.Add(rContent)

            If (i > 1) And ((i Mod 2) = 0) Then
                rContent.CssClass = "datagridalternatingstyle"
            End If

        Next

        'Add the table to the attendance panel
        pnlAttendance.Controls.Add(tbl)
        'Place the hashtable in session if newTable is true
        If newTable = True Then
            Session("SectionAndMeetings") = htValues
        End If


    End Sub

    Private Sub BuildDropDownLists()
        BuildCourseDDDL()
        BuildPeriodDDL()
        BuildAltPeriodDDL()
        BuildRoomDDL()
        BuildInstructorDDL()
        BuildRelatedStarts()
    End Sub

    Private Sub BuildCourseDDDL()
        Dim fac As New TranscriptFacade
        Dim tbl As New Table
        Dim rowCounter As Integer
        '        Dim cellCounter As Integer
        Dim dt As New DataTable

        tbl = DirectCast(pnlAttendance.Controls(0), Table)
        dt = fac.GetCoursesForProgram(programID)

        'Start at the second row because the first row contains the header
        For rowCounter = 1 To tbl.Rows.Count - 1
            'The course ddl is in the 2nd. cell
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).DataSource = dt
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).DataTextField = "FullDescrip"
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).DataValueField = "ReqId"
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).DataBind()
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).Items.Insert(0, New ListItem("Select", ""))
        Next

    End Sub

    Private Sub BuildPeriodDDL()
        Dim fac As New PeriodFacade
        Dim tbl As New Table
        Dim rowCounter As Integer
        '        Dim cellCounter As Integer
        Dim dt As New DataTable

        dt = fac.GetPeriodsDescrip

        tbl = DirectCast(pnlAttendance.Controls(0), Table)

        'Start at the second row because the first row contains the header
        For rowCounter = 1 To tbl.Rows.Count - 1
            'The course ddl is in the 3rd. cell
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0).FindControl("ddlPeriod" + rowCounter.ToString), DropDownList).DataSource = dt
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0).FindControl("ddlPeriod" + rowCounter.ToString), DropDownList).DataTextField = "PeriodDescrip" '"PeriodCode"
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0).FindControl("ddlPeriod" + rowCounter.ToString), DropDownList).DataValueField = "PeriodId"
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0).FindControl("ddlPeriod" + rowCounter.ToString), DropDownList).DataBind()
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0).FindControl("ddlPeriod" + rowCounter.ToString), DropDownList).Items.Insert(0, New ListItem("Select", ""))
        Next
    End Sub

    Private Sub BuildAltPeriodDDL()
        Dim fac As New PeriodFacade
        Dim tbl As New Table
        Dim rowCounter As Integer
        '        Dim cellCounter As Integer
        Dim dt As New DataTable

        tbl = DirectCast(pnlAttendance.Controls(0), Table)
        dt = fac.GetPeriodsDescrip

        'Start at the second row because the first row contains the header
        For rowCounter = 1 To tbl.Rows.Count - 1
            'The course ddl is in the 4th. cell
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(1).FindControl("ddlAltPeriod" + rowCounter.ToString), DropDownList).DataSource = dt
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(1).FindControl("ddlAltPeriod" + rowCounter.ToString), DropDownList).DataTextField = "PeriodDescrip" '"PeriodCode"
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(1).FindControl("ddlAltPeriod" + rowCounter.ToString), DropDownList).DataValueField = "PeriodId"
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(1).FindControl("ddlAltPeriod" + rowCounter.ToString), DropDownList).DataBind()
            DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(1).FindControl("ddlAltPeriod" + rowCounter.ToString), DropDownList).Items.Insert(0, New ListItem("Select", ""))
        Next
    End Sub

    Private Sub BuildRoomDDL()
        Dim fac As New ClassSectionFacade
        Dim tbl As New Table
        Dim rowCounter As Integer
        '        Dim cellCounter As Integer
        Dim ds As New DataSet

        tbl = DirectCast(pnlAttendance.Controls(0), Table)
        ds = fac.GetCampRooms(campusID)


        'Start at the second row because the first row contains the header
        For rowCounter = 1 To tbl.Rows.Count - 1
            'The course ddl is in the 5th. cell
            DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0).FindControl("ddlRoom" + rowCounter.ToString), DropDownList).DataSource = ds
            DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0).FindControl("ddlRoom" + rowCounter.ToString), DropDownList).DataTextField = "Descrip"
            DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0).FindControl("ddlRoom" + rowCounter.ToString), DropDownList).DataValueField = "RoomId"
            DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0).FindControl("ddlRoom" + rowCounter.ToString), DropDownList).DataBind()
            DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0).FindControl("ddlRoom" + rowCounter.ToString), DropDownList).Items.Insert(0, New ListItem("Select", ""))
        Next
    End Sub

    Private Sub BuildInstructorDDL()
        Dim fac As New UserSecurityFacade
        Dim tbl As New Table
        Dim rowCounter As Integer
        '        Dim cellCounter As Integer
        Dim dt As New DataTable

        tbl = DirectCast(pnlAttendance.Controls(0), Table)
        dt = fac.GetInstructors(campusID)

        'Start at the second row because the first row contains the header
        For rowCounter = 1 To tbl.Rows.Count - 1
            'The course ddl is in the 9th. cell
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).DataSource = dt
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).DataTextField = "FullName"
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).DataValueField = "UserId"
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).DataBind()
            DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).Items.Insert(0, New ListItem("Select", ""))
        Next
    End Sub

    Private Sub BuildRelatedStarts()
        Dim fac As New StartDateFacade
        Dim tbl As New Table
        Dim rowCounter As Integer
        '        Dim cellCounter As Integer
        Dim dt As New DataTable

        tbl = DirectCast(pnlAttendance.Controls(0), Table)
        dt = fac.GetRelatedStarts(termId, shift)

        'Start at the second row because the first row contains the header
        For rowCounter = 1 To tbl.Rows.Count - 1
            'The course ddl is in the 10th. cell
            DirectCast(tbl.Rows(rowCounter).Cells(5).Controls(0), ListBox).DataSource = dt
            DirectCast(tbl.Rows(rowCounter).Cells(5).Controls(0), ListBox).DataTextField = "TermDescrip"
            DirectCast(tbl.Rows(rowCounter).Cells(5).Controls(0), ListBox).DataValueField = "TermId"
            DirectCast(tbl.Rows(rowCounter).Cells(5).Controls(0), ListBox).DataBind()
            DirectCast(tbl.Rows(rowCounter).Cells(5).Controls(0), ListBox).Items.Insert(0, New ListItem("Select", ""))
        Next
    End Sub

    Private Sub BuildAvailableListBox()
        Dim fac As New StartDateFacade

        With lbxAvailable
            .DataSource = fac.GetOverlappingClassesForStartDate(termId, shift)
            .DataTextField = "Class"
            .DataValueField = "ClsSectionId"
            .DataBind()
        End With
    End Sub

    Private Sub BuildGradeScaleDDL()
        Dim fac As New StartDateFacade

        With ddlGradeScaleId
            .DataSource = fac.GetGradeScales
            .DataTextField = "Descrip"
            .DataValueField = "GrdScaleId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildShiftDDL()
        Dim fac As New StartDateFacade

        With ddlShiftId
            .DataSource = fac.GetShifts
            .DataTextField = "ShiftDescrip"
            .DataValueField = "ShiftId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BindGradeScaleAndShift(ByVal ds As DataSet)
        'The GradeScaleAndShift dt in the DS contains the info we need to bind.
        If ds.Tables("GradeScaleAndShift").Rows.Count > 0 Then
            ddlGradeScaleId.SelectedValue = ds.Tables("GradeScaleAndShift").Rows(0)("GrdScaleId").ToString()
            'ddlShiftId.SelectedValue = ds.Tables("GradeScaleAndShift").Rows(0)("ShiftId").ToString()
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveUserEntries()
    End Sub

    Private Function ValidateTable(ByVal dtTableEntries As DataTable) As String
        Dim result As String
        Dim dr As DataRow
        Dim dtProcessed As New DataTable
        Dim drProcessed As DataRow
        Dim arrRows() As DataRow
        Dim filter As String

        dtProcessed.Columns.Add("ReqId", Type.GetType("System.String"))
        dtProcessed.Columns.Add("InstructorId", Type.GetType("System.String"))
        dtProcessed.Columns.Add("MaxStud", Type.GetType("System.String"))
        dtProcessed.Columns.Add("Row", Type.GetType("System.String"))

        result = ""

        For Each dr In dtTableEntries.Rows
            'Each row must have a course
            If dr("ReqId") = "" Then
                result &= "You must select a course for row " & dr("Row").ToString() + vbCr
            End If
            'If a row has an alt period it must also have a period
            If dr("AltPeriodId") <> "" Then
                If dr("PeriodId") = "" Then
                    result &= "You must specify for a period for row " & dr("Row").ToString() & " if you want to use an alternate period" + vbCr
                End If
            End If
            'Each row must have a start date and an end date
            If dr("StartDate") = Date.MinValue Then
                result &= "You must enter a start date for row " & dr("Row").ToString() + vbCr
            End If
            If dr("EndDate") = Date.MinValue Then
                result &= "You must enter an end date for row " & dr("Row").ToString() + vbCr
            End If
            'Each row must have a max student
            If dr("MaxStud") = "" Then
                result &= "You must enter the max number of students for row " & dr("Row").ToString() + vbCr
            End If
            'Each class for the same course must have the same instructor and max student
            'Check if the course associated with the row has been processed already.
            If Not HasCourseBeenProcessed(dr("ReqId"), dtProcessed) Then
                'Add the course to the processed table
                drProcessed = dtProcessed.NewRow
                drProcessed("ReqId") = dr("ReqId")
                drProcessed("InstructorId") = dr("InstructorId")
                drProcessed("MaxStud") = dr("MaxStud")
                drProcessed("Row") = dr("Row")
                dtProcessed.Rows.Add(drProcessed)
            Else
                'Check if the instructor and max student for the record in the processed table
                'match those of the current row being processed.
                filter = "ReqId = '" & dr("ReqId") & "'"
                arrRows = dtProcessed.Select(filter)
                If (arrRows(0)("InstructorId") <> dr("InstructorId")) Or (arrRows(0)("MaxStud") <> dr("MaxStud")) Then
                    result &= "The instructor and max student for the same course in row " & arrRows(0)("Row") + vbCr
                    result &= "and row " & dr("Row") & " must be the same or you can leave " + vbCr
                    result &= "leave them empty in row " & dr("Row") + vbCr
                End If

            End If
            'When a class is related to a start, we need to check if there is already another
            'class in the related start that is based on the same course. We will also need to
            'check if the class starts before the related start or if it ends after the 
            'related start.
            If dr("RelatedStarts").ToString <> "" Then
                Dim arrStarts() As String
                Dim dtClasses As New DataTable
                Dim dtTerm As New DataTable
                Dim fac As New ClassSectionFacade

                arrStarts = dr("RelatedStarts").ToString.Split(",")
                For Each start As String In arrStarts
                    If start <> "," And start <> "" Then
                        dtClasses = fac.GetClassesForTermBasedOnCourse(dr("ReqId").ToString, start)
                        If dtClasses.Rows.Count > 0 Then
                            For Each drCls As DataRow In dtClasses.Rows
                                If (dr("InstructorId").ToString <> drCls("InstructorId").ToString Or dr("MaxStud").ToString <> drCls("MaxStud").ToString) Then
                                    result &= "The instructor and max student for row " & dr("Row") + vbCr
                                    result &= "does not match a class based on the same course, " + drCls("Descrip") + ", " + vbCr
                                    result &= "in the related start of " + drCls("TermDescrip") + vbCr
                                End If
                            Next
                        End If

                        dtTerm = fac.GetTermDates(start)
                        If dr("StartDate") < dtTerm.Rows(0)("StartDate") Then
                            result &= "You cannot add the class in row " & dr("Row") & " to" & vbCr
                            result &= dtTerm.Rows(0)("TermDescrip") & " because the class starts" & vbCr
                            result &= "before it does"
                        End If
                        If dr("EndDate") > dtTerm.Rows(0)("EndDate") Then
                            result &= "You cannot add the class in row " & dr("Row") & " to" & vbCr
                            result &= dtTerm.Rows(0)("TermDescrip") & " because the class ends" & vbCr
                            result &= "after it does"
                        End If

                    End If
                Next
            End If
        Next

        Return result
    End Function

    Private Function HasCourseBeenProcessed(ByVal reqId As String, ByVal dtProcessed As DataTable) As Boolean
        Dim arrRows() As DataRow
        Dim convertedReqId As Guid
        Dim filter As String
        'If the dt is empty then simply returns false
        If dtProcessed.Rows.Count = 0 Then
            Return False
        Else
            'Search the DataTable

            convertedReqId = XmlConvert.ToGuid(reqId)
            filter = "ReqId = '" & convertedReqId.ToString & "'"
            arrRows = dtProcessed.Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Function BuildTableEntriesDT() As DataTable
        Dim dt As New DataTable
        Dim dtRRCS As New DataTable
        Dim dr As DataRow
        Dim tbl As New Table
        Dim rowCounter As Integer
        '        Dim cellCounter As Integer
        '  Dim strRelatedTerms As String
        Dim prevClsSectionId As String = String.Empty
        Dim prevReqId As String = String.Empty
        Dim prevMaxStud As String = String.Empty
        Dim prevInstructorId As String = String.Empty
        '   Dim prevRelatedStarts As String

        With dt
            .Columns.Add("Row", Type.GetType("System.String"))
            .Columns.Add("ClsSectionId", Type.GetType("System.String"))
            .Columns.Add("ClsSectMeetingId", Type.GetType("System.String"))
            .Columns.Add("ReqId", Type.GetType("System.String"))
            .Columns.Add("PeriodId", Type.GetType("System.String"))
            .Columns.Add("AltPeriodId", Type.GetType("System.String"))
            .Columns.Add("RoomId", Type.GetType("System.String"))
            .Columns.Add("StartDate", Type.GetType("System.DateTime"))
            .Columns.Add("EndDate", Type.GetType("System.DateTime"))
            .Columns.Add("MaxStud", Type.GetType("System.String"))
            .Columns.Add("InstructorId", Type.GetType("System.String"))
            'A class might be related to more than one starts so the relationship is one-to-many.
            'We are going to store the related starts as a comma  separated list.
            .Columns.Add("RelatedStarts", Type.GetType("System.String"))
        End With

        With dtRRCS
            .Columns.Add("Row", Type.GetType("System.String"))
            .Columns.Add("ClsSectionId", Type.GetType("System.String"))
            .Columns.Add("ReqId", Type.GetType("System.String"))
            .Columns.Add("InstructorId", Type.GetType("System.String"))
            .Columns.Add("MaxStud", Type.GetType("System.String"))
        End With



        tbl = DirectCast(pnlAttendance.Controls(0), Table)

        For rowCounter = 1 To tbl.Rows.Count - 1
            'We will only add the row if it is not empty
            If Not IsRowEmpty(rowCounter) Then
                dr = dt.NewRow
                dr("Row") = rowCounter.ToString
                dr("ClsSectionId") = DirectCast(tbl.Rows(rowCounter).Cells(0).Controls(0).FindControl("lblClsSectionId" + rowCounter.ToString), Label).Text
                dr("ClsSectMeetingId") = DirectCast(tbl.Rows(rowCounter).Cells(0).Controls(0).FindControl("lblClsSectMeetingId" + rowCounter.ToString), Label).Text

                If rowCounter > 1 Then
                    If DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).SelectedItem.Value = "" _
                        Or DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).Enabled = False Then
                        'We need to substitute the value for the ReqId from the previous row
                        'We need to substitute the value for the class section id from the previous row
                        dr("ReqId") = prevReqId
                        dr("ClsSectionId") = prevClsSectionId
                        'Use the value from the prev row if max student is empty
                        If DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(1).FindControl("txtMaxStud" + rowCounter.ToString), TextBox).Text = "" _
                            Or DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).Enabled = False Then
                            dr("MaxStud") = prevMaxStud
                        Else
                            dr("MaxStud") = DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(1).FindControl("txtMaxStud" + rowCounter.ToString), TextBox).Text
                        End If
                        'Use the value from the previous row if instructor is empty
                        If DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).SelectedItem.Value = "" _
                            Or DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).Enabled = False Then
                            dr("InstructorId") = prevInstructorId
                        Else
                            dr("InstructorId") = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).SelectedItem.Value
                        End If
                        'Use the value from the previous row if related starts is empty
                        dr("RelatedStarts") = GetStringWithSelectedRelatedTerms(rowCounter)
                    Else
                        dr("ReqId") = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).SelectedItem.Value
                        dr("MaxStud") = DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(1).FindControl("txtMaxStud" + rowCounter.ToString), TextBox).Text
                        dr("InstructorId") = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).SelectedItem.Value
                        dr("RelatedStarts") = GetStringWithSelectedRelatedTerms(rowCounter)
                    End If
                Else
                    dr("ReqId") = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).SelectedItem.Value
                    dr("MaxStud") = DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(1).FindControl("txtMaxStud" + rowCounter.ToString), TextBox).Text
                    dr("InstructorId") = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).SelectedItem.Value
                    dr("RelatedStarts") = GetStringWithSelectedRelatedTerms(rowCounter)
                End If

                dr("PeriodId") = DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0).FindControl("ddlPeriod" + rowCounter.ToString), DropDownList).SelectedItem.Value
                dr("AltPeriodId") = DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(1).FindControl("ddlAltPeriod" + rowCounter.ToString), DropDownList).SelectedItem.Value
                dr("RoomId") = DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0).FindControl("ddlRoom" + rowCounter.ToString), DropDownList).SelectedItem.Value
                If DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0).FindControl("txtStartDate" + rowCounter.ToString), TextBox).Text <> "" Then
                    dr("StartDate") = DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0).FindControl("txtStartDate" + rowCounter.ToString), TextBox).Text
                Else
                    dr("StartDate") = Date.MinValue
                End If

                If DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(1).FindControl("txtEndDate" + rowCounter.ToString), TextBox).Text <> "" Then
                    dr("EndDate") = DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(1).FindControl("txtEndDate" + rowCounter.ToString), TextBox).Text
                Else
                    dr("EndDate") = Date.MinValue
                End If


                dt.Rows.Add(dr)

                'Reset the previous values
                prevClsSectionId = dr("ClsSectionId")
                prevReqId = dr("ReqId")
                prevMaxStud = dr("MaxStud")
                prevInstructorId = dr("InstructorId")


            End If
        Next

        'After going through all the rows. If dt is not empty then we need to perform another task.
        'We handled the situation where the user enters several meetings for the same class and leaves out the course
        'max stud and instructor in the row. However, it is possible that further down in the grid the user wants to
        'add a meeting for a class that is added earlier in the grid. In this case the user will have to select the course
        'that the class is based on. However, the class section id for that row will be different from the class section id
        'of the class further up in the grid. Remember that when the user enters several meetings for the same class we go
        'through and change the class section id for each meeting to the first row of the class.
        'At this point we have to search the dt for each ReqId and check that all rows with the same reqid have the same class
        'section id. If a later row for the same reqid does not have the same class section id we will have to update it.
        If dt.Rows.Count > 0 Then
            For Each row As DataRow In dt.Rows
                'Check if the ReqId is already in the dtRRCS datatable
                If Not DoesReqIdExists(row, dtRRCS) Then
                    'add an entry for it
                    Dim r As DataRow = dtRRCS.NewRow
                    r("ReqId") = row("ReqId")
                    r("ClsSectionId") = row("ClsSectionId")
                    r("Row") = row("Row")
                    r("InstructorId") = row("InstructorId")
                    r("MaxStud") = row("MaxStud")
                    dtRRCS.Rows.Add(r)
                Else
                    'check if the class section id matches that of the current row being examined
                    Dim rRRCS() As DataRow
                    rRRCS = dtRRCS.Select("ReqId ='" & row("ReqId") & "'")
                    If row("ClsSectionId").ToString() <> rRRCS(0)("ClsSectionId").ToString() Then
                        'We need to update the class section id to the processed row
                        row("ClsSectionId") = rRRCS(0)("ClsSectionId").ToString()
                        DirectCast(tbl.Rows(CInt(row("Row"))).Cells(0).Controls(0).FindControl("lblClsSectionId" + row("Row").ToString), Label).Text = row("ClsSectionId")
                        'We also have to update the table itself and the hashtable as well.
                        Dim smi As New SectionAndMeetingIDS
                        Dim htValues As New Hashtable
                        htValues = DirectCast(Session("SectionAndMeetings"), Hashtable)
                        htValues.Item(CInt(row("Row"))).ClsSectionId = XmlConvert.ToGuid(row("ClsSectionId"))
                        Session("SectionAndMeetings") = htValues
                        'We also need to substitue the instructor and max student values if they
                        'are empty for the row being processed.
                        If row("InstructorId") = "" Then
                            row("InstructorId") = rRRCS(0)("InstructorId")
                        End If
                        If row("MaxStud") = "" Then
                            row("MaxStud") = rRRCS(0)("MaxStud")
                        End If
                    End If
                End If
            Next
        End If
        '



        Return dt
    End Function

    Private Function IsRowEmpty(ByVal rowCounter As Integer) As Boolean
        Dim tbl As New Table
        Dim course As String
        Dim cDDL As New DropDownList
        Dim period As String
        Dim altPeriod As String
        Dim room As String
        Dim startDate As String
        Dim endDate As String
        Dim maxStudents As String
        Dim txtMS As New TextBox
        Dim instructor As String
        Dim ddlIN As New DropDownList

        tbl = DirectCast(pnlAttendance.Controls(0), Table)
        course = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList).SelectedValue
        cDDL = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList)
        period = DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0).FindControl("ddlPeriod" + rowCounter.ToString), DropDownList).SelectedItem.Value
        altPeriod = DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(1).FindControl("ddlAltPeriod" + rowCounter.ToString), DropDownList).SelectedItem.Value
        room = DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0).FindControl("ddlRoom" + rowCounter.ToString), DropDownList).SelectedItem.Value
        startDate = DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0).FindControl("txtStartDate" + rowCounter.ToString), TextBox).Text
        endDate = DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(1).FindControl("txtEndDate" + rowCounter.ToString), TextBox).Text
        maxStudents = DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(1).FindControl("txtMaxStud" + rowCounter.ToString), TextBox).Text
        txtMS = DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(1).FindControl("txtMaxStud" + rowCounter.ToString), TextBox)
        instructor = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList).SelectedValue
        ddlIN = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList)

        If (course = "" Or cDDL.Enabled = False) And period = "" And altPeriod = "" And room = "" And startDate = "" _
            And endDate = "" And (maxStudents = "" Or txtMS.Enabled = False) And (instructor = "" Or ddlIN.Enabled = False) Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function DoesReqIdExists(ByVal dr As DataRow, ByVal dtToSearch As DataTable) As Boolean
        Dim arrRows() As DataRow
        Dim filter As String
        'If the datatable is empty then simply returns false
        If dtToSearch.Rows.Count = 0 Then
            Return False
        Else
            'Search the datatable
            filter = "ReqId = '" & dr("ReqId").ToString & "'"
            arrRows = dtToSearch.Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub BindExistingDataToTable(ByVal ds As DataSet)
        'This should only be called if the class section dt in the ds is not empty
        Dim drCS As DataRow
        'Dim drCSM As DataRow
        'Dim lastUsedRow As Integer
        Dim tbl As Table
        '  Dim rowCounter As Integer
        Dim meetingRow As Integer
        ' Dim cellCounter As Integer
        Dim row As Integer
        Dim htValues As  Hashtable
        Dim arrMeetings() As DataRow
        Dim drMeeting As DataRow
        Dim filter As String
        Dim arrStarts() As DataRow

        tbl = DirectCast(pnlAttendance.Controls(0), Table)

        'When binding we must first clear the table of any populated/selected values
        ClearTable()

        If ds.Tables("ClassSections").Rows.Count > 0 Then
            For Each drCS In ds.Tables("ClassSections").Rows
                'Get the first empty row in the table
                row = GetFirstEmptyRowInTable()
                'Bind the class section values to the first empty row in the table.
                'Class Section ID
                DirectCast(tbl.Rows(row).Cells(0).Controls(0).FindControl("lblClsSectionId" + row.ToString), Label).Text = drCS("ClsSectionId").ToString()
                'For the class section id we also have to update the hashtable stored in session
                htValues = DirectCast(Session("SectionAndMeetings"), Hashtable)
                DirectCast(htValues.Item(row), SectionAndMeetingIDS).ClsSectionId = drCS("ClsSectionId")
                Session("SectionAndMeetings") = htValues
                'Course
                DirectCast(tbl.Rows(row).Cells(1).Controls(0).FindControl("ddlCourse" + row.ToString), DropDownList).SelectedValue = drCS("ReqId").ToString()
                'Start Date
                DirectCast(tbl.Rows(row).Cells(3).Controls(0).FindControl("txtStartDate" + row.ToString), TextBox).Text = drCS("StartDate").ToShortDateString()
                'End Date
                DirectCast(tbl.Rows(row).Cells(3).Controls(1).FindControl("txtEndDate" + row.ToString), TextBox).Text = drCS("EndDate").ToShortDateString()
                'Max Students
                If drCS("MaxStud").ToString() <> "0" Then
                    DirectCast(tbl.Rows(row).Cells(4).Controls(1).FindControl("txtMaxStud" + row.ToString), TextBox).Text = drCS("MaxStud").ToString()
                Else
                    DirectCast(tbl.Rows(row).Cells(4).Controls(1).FindControl("txtMaxStud" + row.ToString), TextBox).Text = ""
                End If

                'Instructor
                DirectCast(tbl.Rows(row).Cells(1).Controls(1).FindControl("ddlInstructor" + row.ToString), DropDownList).SelectedValue = drCS("InstructorId").ToString()

                'We need to bind the meeting information
                'Get the meetings associated with the class being processed
                filter = "ClsSectionId ='" & drCS("ClsSectionId").ToString() & "'"
                arrMeetings = ds.Tables("ClassSectionMeetings").Select(filter)
                'There will always be at least one meeting. The information for the first meeting must be placed in
                'the row that the class section is placed. Other meetings will be added to the next available row in the table.
                'Remember that for the other meetings we want to select the course, max student and instructor fields that
                'are fixed at the class section level but also disable them.
                meetingRow = row
                For num As Integer = 0 To arrMeetings.Length - 1
                    drMeeting = arrMeetings(num)
                    If num > 0 Then
                        meetingRow += 1
                    End If
                    'Class section id
                    DirectCast(tbl.Rows(meetingRow).Cells(0).Controls(0).FindControl("lblClsSectionId" + meetingRow.ToString), Label).Text = drCS("ClsSectionId").ToString()
                    'Class Section Meeting Id
                    DirectCast(tbl.Rows(meetingRow).Cells(0).Controls(0).FindControl("lblClsSectMeetingId" + meetingRow.ToString), Label).Text = drMeeting("ClsSectMeetingId").ToString()
                    'For the class section meeting id we also have to update the hashtable stored in session
                    htValues = DirectCast(Session("SectionAndMeetings"), Hashtable)
                    DirectCast(htValues.Item(meetingRow), SectionAndMeetingIDS).ClsSectionId = drCS("ClsSectionId")
                    DirectCast(htValues.Item(meetingRow), SectionAndMeetingIDS).ClsSectMeetingId = drMeeting("ClsSectMeetingId")
                    Session("SectionAndMeetings") = htValues
                    'Course
                    DirectCast(tbl.Rows(meetingRow).Cells(1).Controls(0).FindControl("ddlCourse" + meetingRow.ToString), DropDownList).SelectedValue = drCS("ReqId").ToString()
                    If num > 0 Then
                        DirectCast(tbl.Rows(meetingRow).Cells(1).Controls(0).FindControl("ddlCourse" + meetingRow.ToString), DropDownList).Enabled = False
                    Else
                        DirectCast(tbl.Rows(meetingRow).Cells(1).Controls(0).FindControl("ddlCourse" + meetingRow.ToString), DropDownList).Enabled = True
                    End If
                    'Period
                    DirectCast(tbl.Rows(meetingRow).Cells(2).Controls(0).FindControl("ddlPeriod" + meetingRow.ToString), DropDownList).SelectedValue = drMeeting("PeriodId").ToString()
                    'Alt Period
                    DirectCast(tbl.Rows(meetingRow).Cells(2).Controls(1).FindControl("ddlAltPeriod" + meetingRow.ToString), DropDownList).SelectedValue = drMeeting("AltPeriodId").ToString()
                    'Room
                    If drMeeting("RoomId").ToString() <> "" Then
                        DirectCast(tbl.Rows(meetingRow).Cells(4).Controls(0).FindControl("ddlRoom" + meetingRow.ToString), DropDownList).SelectedValue = drMeeting("RoomId").ToString()
                    End If
                    Dim startBF As New StartDateFacade()
                    'Start Date
                    If (drMeeting("StartDate") Is System.DBNull.Value) Then

                        Dim rtn() As String = startBF.GetDatesIfNull(drCS("TermId").ToString, drMeeting("ClsSectionId").ToString)
                        DirectCast(tbl.Rows(meetingRow).Cells(3).Controls(0).FindControl("txtStartDate" + meetingRow.ToString), TextBox).Text = rtn(0)
                        'End Date
                        DirectCast(tbl.Rows(meetingRow).Cells(3).Controls(1).FindControl("txtEndDate" + meetingRow.ToString), TextBox).Text = rtn(1)


                    Else
                        DirectCast(tbl.Rows(meetingRow).Cells(3).Controls(0).FindControl("txtStartDate" + meetingRow.ToString), TextBox).Text = drMeeting("StartDate").ToShortDateString()
                        'End Date
                        DirectCast(tbl.Rows(meetingRow).Cells(3).Controls(1).FindControl("txtEndDate" + meetingRow.ToString), TextBox).Text = drMeeting("EndDate").ToShortDateString()
                    End If
                    'Max Students
                    DirectCast(tbl.Rows(meetingRow).Cells(4).Controls(1).FindControl("txtMaxStud" + meetingRow.ToString), TextBox).Text = drCS("MaxStud").ToString()
                    If num > 0 Then
                        DirectCast(tbl.Rows(meetingRow).Cells(4).Controls(1).FindControl("txtMaxStud" + meetingRow.ToString), TextBox).Enabled = False
                    Else
                        DirectCast(tbl.Rows(meetingRow).Cells(4).Controls(1).FindControl("txtMaxStud" + meetingRow.ToString), TextBox).Enabled = True
                    End If
                    'Instructor
                    DirectCast(tbl.Rows(meetingRow).Cells(1).Controls(1).FindControl("ddlInstructor" + meetingRow.ToString), DropDownList).SelectedValue = drCS("InstructorId").ToString()
                    If num > 0 Then
                        DirectCast(tbl.Rows(meetingRow).Cells(1).Controls(1).FindControl("ddlInstructor" + meetingRow.ToString), DropDownList).Enabled = False
                    Else
                        DirectCast(tbl.Rows(meetingRow).Cells(1).Controls(1).FindControl("ddlInstructor" + meetingRow.ToString), DropDownList).Enabled = True
                    End If
                Next

                'Bind the related starts information. We only need to bind to the first row even if the class has several
                'meet records.
                filter = "ClsSectionId ='" & drCS("ClsSectionId").ToString() & "'"
                arrStarts = ds.Tables("RelatedStarts").Select(filter)

                'DirectCast(tbl.Rows(row).Cells(9).Controls(0), ListBox)()
                If arrStarts.Length > 0 Then
                    For Each arrStart As DataRow In arrStarts
                        For Each item As ListItem In DirectCast(tbl.Rows(row).Cells(5).Controls(0), ListBox).Items
                            If item.Value = arrStart("TermId").ToString() Then
                                item.Selected = True
                            End If
                        Next
                    Next
                End If

            Next
        End If

    End Sub

    Private Sub ClearTable()
        Dim tbl As New Table


        tbl = DirectCast(pnlAttendance.Controls(0), Table)

        For rowCounter As Integer = 1 To tbl.Rows.Count - 1
            If Not IsRowEmpty(rowCounter) Then
                Dim ddlCRS As New DropDownList
                Dim txtMS As New TextBox
                Dim ddlINSTR As New DropDownList
                'Course
                ddlCRS = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0).FindControl("ddlCourse" + rowCounter.ToString), DropDownList)
                ddlCRS.SelectedIndex = 0
                If ddlCRS.Enabled = False Then
                    ddlCRS.Enabled = True
                End If
                'Period
                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0).FindControl("ddlPeriod" + rowCounter.ToString), DropDownList).SelectedIndex = 0
                'AltPeriod
                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(1).FindControl("ddlAltPeriod" + rowCounter.ToString), DropDownList).SelectedIndex = 0
                'Room
                DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0).FindControl("ddlRoom" + rowCounter.ToString), DropDownList).SelectedIndex = 0
                'Start Date
                DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0).FindControl("txtStartDate" + rowCounter.ToString), TextBox).Text = ""
                'End Date
                DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(1).FindControl("txtEndDate" + rowCounter.ToString), TextBox).Text = ""
                'Max Students
                txtMS = DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(1).FindControl("txtMaxStud" + rowCounter.ToString), TextBox)
                txtMS.Text = ""
                If txtMS.Enabled = False Then
                    txtMS.Enabled = True
                End If
                'Instructor
                ddlINSTR = DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(1).FindControl("ddlInstructor" + rowCounter.ToString), DropDownList)
                ddlINSTR.SelectedIndex = 0
                If ddlINSTR.Enabled = False Then
                    ddlINSTR.Enabled = True
                End If
            End If
        Next
    End Sub

    Private Function GetFirstEmptyRowInTable() As Integer
        Dim tbl As Table
        Dim rowCounter As Integer

        tbl = DirectCast(pnlAttendance.Controls(0), Table)

        For rowCounter = 1 To tbl.Rows.Count - 1
            If IsRowEmpty(rowCounter) Then
                Return rowCounter
            End If
        Next
        Return 0
    End Function

    Private Function GetStringWithSelectedRelatedTerms(ByVal rowCounter As Integer) As String
        Dim tbl As Table
        Dim relatedTerms As String = String.Empty

        tbl = DirectCast(pnlAttendance.Controls(0), Table)

        If DirectCast(tbl.Rows(rowCounter).Cells(5).Controls(0), ListBox).Items.Count > 0 Then
            Dim rCounter As Integer
            Dim itemsCount As Integer
            rCounter = 1
            itemsCount = DirectCast(tbl.Rows(rowCounter).Cells(5).Controls(0), ListBox).Items.Count

            For Each item As ListItem In DirectCast(tbl.Rows(rowCounter).Cells(5).Controls(0), ListBox).Items
                If item.Selected = True Then
                    If rCounter = itemsCount Then
                        relatedTerms &= item.Value.ToString
                    Else
                        relatedTerms &= item.Value.ToString & ","
                    End If

                End If
                rCounter += 1
            Next

        End If

        Return relatedTerms
    End Function

    Private Class SectionAndMeetingIDS
        Private _ClsSectionId As Guid
        Private _ClsSectMeetingId As Guid

        Public Property ClsSectionId() As Guid
            Get
                Return _ClsSectionId
            End Get
            Set(ByVal Value As Guid)
                _ClsSectionId = Value
            End Set
        End Property
        Public Property ClsSectMeetingId() As Guid
            Get
                Return _ClsSectMeetingId
            End Get
            Set(ByVal Value As Guid)
                _ClsSectMeetingId = Value
            End Set
        End Property
    End Class

    Private Sub btnAddAvailableClasses_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAvailableClasses.Click
        Dim arrCS As New ArrayList
        Dim fac As New StartDateFacade
        Dim result As String
        Dim validationResult As String
        'If there are no records in the lbx or there are no selected items alert the user
        If lbxAvailable.Items.Count = 0 Or lbxAvailable.SelectedIndex = -1 Then
            DisplayErrorMessage("There are no selected classes to add")
            'If there are no selections for grade scale and shift this will cause a problem on the post back so we need
            'to let the user select those.
        ElseIf ddlShiftId.SelectedItem.Text = "Select" Or ddlGradeScaleId.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("Please select a shift and a grade scale first")
        Else
            'First get the selected classes for this term
            For Each item As ListItem In lbxAvailable.Items
                If item.Selected = True Then
                    arrCS.Add(item.Value)
                End If
            Next
            'Validate that none of the selected classes are based on courses that classes in
            'the DataGrid are based on. For example, if the user is trying to add an overlapping
            'class that is based on Anatomy 1 and there is already a class based on Anatomy 1
            'in the DataGrid then we should not save anything and let the user know about the
            'conflict.
            validationResult = ValidateSelectedOverlappingClasses(arrCS)
            If validationResult = "" Then
                'Try adding the classes
                result = fac.AddClassesToStartDate(arrCS, termId, Session("UserName"))
                If result <> "" Then
                    DisplayErrorMessage(result)
                Else
                    BuildAvailableListBox()
                    SaveUserEntries(False)
                End If
            Else
                DisplayErrorMessage(validationResult)
            End If


        End If
    End Sub

    Private Sub SaveUserEntries(Optional ByVal blnCheckIfThereAreUserEntries As Boolean = True)
        Dim dtTableEntries As New DataTable
        Dim fac As New StartDateFacade
        Dim result As String
        Dim validationResult As String
        Dim ds As New DataSet

        dtTableEntries = BuildTableEntriesDT()
        'If there are no rows in the table let the user know.
        If blnCheckIfThereAreUserEntries = True And dtTableEntries.Rows.Count = 0 _
            And DirectCast(Session("StartDateClassesAndMeetings"), DataSet).Tables("ClassSections").Rows.Count = 0 Then
            DisplayErrorMessage("There are no entries selected.")
        Else
            validationResult = ValidateTable(dtTableEntries)
            If validationResult = "" Then
                result = fac.ProcessStartDateClassesAndMeetings(dtTableEntries, Session("StartDateClassesAndMeetings"), termId, Session("UserName"), campusID, ddlGradeScaleId.SelectedValue, ddlShiftId.SelectedValue)
                If result <> "" Then
                    DisplayErrorMessage(result)
                Else
                    'Get a DataSet with the relevant tables. We have to do this because we are not updating the entries
                    'in the DataSet while making changes to the grid.
                    ds = fac.GetStartDateClassesAndMeetings(termId)
                    Session("StartDateClassesAndMeetings") = ds
                    BindExistingDataToTable(ds)
                End If
            Else
                DisplayErrorMessage(validationResult)
            End If 'Validate table

        End If 'There are rows in the table
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

    Protected Sub btnSave_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Disposed

    End Sub

    Private Function ValidateSelectedOverlappingClasses(ByVal arrClasses As ArrayList) As String
        'For each class get the course that it is based on and check if there are any entries
        'in the DataGrid that is based on that course.
        Dim result As String = String.Empty
        Dim dtCourse As New DataTable
        Dim fac As New ClassSectionFacade
        Dim dt As New DataTable
        Dim filter As String
        Dim arrRows() As DataRow

        For Each cls As String In arrClasses
            dtCourse = fac.GetCourseForClassSection(cls.ToString)
            dt = BuildTableEntriesDT()
            filter = "ReqId = '" & dtCourse.Rows(0)("ReqId").ToString & "'"
            arrRows = dt.Select(filter)

            If arrRows.Length >= 1 Then
                result &= "You are trying to add a class that is based on the same" & vbCr
                result &= "course, " + dtCourse.Rows(0)("Descrip") + ", as row " & arrRows(0)("Row") + ". This is not permitted."
            End If
        Next
        Return result
    End Function
End Class
