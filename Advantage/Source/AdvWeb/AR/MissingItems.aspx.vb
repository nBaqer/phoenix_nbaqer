﻿Imports Fame.Common
Imports System.Data
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Xml
Imports BO = Advantage.Business.Objects

Partial Class MissingItems
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo

    Protected studentId As String
    Protected studentName As String

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Dim facade As New AdReqsFacade
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'btnBuildList.Attributes.Add("onclick", "SetHiddenText();return true;")

        Dim objCommon As New CommonUtilities
        Dim resourceId As Integer
        Dim campusId As String
        Dim userId As String

        'Dim strVID As String
        'Dim state As AdvantageSessionState
        'Dim m_Context As HttpContext

        '   disable History button the first time
        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not IsPostBack Then
            'objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")

            '   build dropdownlists
            BuildDropDownLists()

            '   initialize buttons
            InitButtonsForLoad()

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")

        End If
    End Sub
    Private Sub BuildDropDownLists()
        Dim ds As New DataSet
        ds = (New MissingItemsFacade).GetDropDownLists(HttpContext.Current.Request.Params("cmpid"))

        'BuildProgramVersionDDL(ds)
        BuildProgramDDL(ds)
        BuildShiftDDL(ds)
        BuildStudentGroupDDL(ds)
        BuildStatusCodeDDL(ds)
        BuildRequirementTypesDDL(ds)
    End Sub
    Private Sub BuildProgramDDL(ByVal ds As DataSet)
        With ddlPrgVerId
            .DataTextField = "ShiftDescrip"
            .DataValueField = "ProgId"
            .DataSource = ds.Tables("ProgramDT")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    ''Added by Saraswathi lakshmanan on Jan 27 2011
    ''To fix rally case De 4941 Add req type on the missing items page.

    Private Sub BuildRequirementTypesDDL(ByVal ds As DataSet)
        With ddlreqType
            .DataTextField = "TypeofReq"
            .DataValueField = "TypeofReq"
            .DataSource = ds.Tables("ReqTypesDT")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildProgramVersionDDL(ByVal ds As DataSet)
        With ddlPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = ds.Tables("ProgramVersionDT")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildShiftDDL(ByVal ds As DataSet)
        With ddlShiftId
            .DataTextField = "ShiftDescrip"
            .DataValueField = "ShiftId"
            .DataSource = ds.Tables("ShiftDT")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStatusCodeDDL(ByVal ds As DataSet)
        With ddlStatusCodeId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = ds.Tables("StatusCodeDT")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStudentGroupDDL(ByVal ds As DataSet)
        With ddlStudentGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = ds.Tables("StudentGroupDT")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub
    Private Sub btnBuildList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        Dim facade As New MissingItemsFacade
        Dim dtStudents As New DataTable
        Dim dtReqs As New DataTable
        Dim dtReqGrps As New DataTable
        Dim dtStuDocs As New DataTable
        Dim dtStuTests As New DataTable
        Dim dtResReqGrps As New DataTable
        '  Dim notFound As Boolean
        Dim dsStudentsWithMissingReqsandReqGroup As New DataTable
        Dim strStudentId As String = ""

        If ddlPrgVerId.SelectedValue = "" Or _
                ddlStudentGrpId.SelectedValue = "" Then
            DisplayErrorMessage("Program and Student Group are required fields. Please select a value from both filters.")
            Exit Sub
        End If

        'Get Students missing requirements and requirement groups and pass it as a filter
        'for the student list
        dsStudentsWithMissingReqsandReqGroup = (New AdReqsFacade).GetStudentsWithMissingRequirementsAndRequirementGroup(HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedValue, ddlShiftId.SelectedValue, ddlStatusCodeId.SelectedValue, ddlStudentGrpId.SelectedValue, ddlreqType.SelectedValue)

        If dsStudentsWithMissingReqsandReqGroup.Rows.Count >= 1 Then
            For Each row As DataRow In dsStudentsWithMissingReqsandReqGroup.Rows
                strStudentId &= row("StudentId").ToString & "','"
            Next
            strStudentId = Mid(strStudentId, 1, InStrRev(strStudentId, "'") - 2)
        End If
        ''Modifierd by Saraswathi lakshmanan on Jan 27 2011
        ''To fix rally case de4941 Add req type on the missing items page.
        If Not strStudentId = "" Then
            BindGridView(ddlPrgVerId.SelectedValue.ToString, ddlStudentGrpId.SelectedValue.ToString, ddlStatusCodeId.SelectedValue.ToString, ddlShiftId.SelectedValue.ToString, strStudentId, ddlreqType.SelectedValue)
        Else
            GridView1.DataSource = Nothing
            GridView1.DataBind()
        End If

        'Try
        '    'Session("StudentsDT") = Nothing
        '    'Session("ReqsDT") = Nothing
        '    'Session("ReqGroupsDT") = Nothing
        '    'Session("StuDocsDT") = Nothing
        '    'Session("StuTestsDT") = Nothing
        '    'Session("ResReqGrpsDT") = Nothing

        '    'Get DataTable with students in Campus Group, Program Version and Student Group
        '    dtStudents = facade.GetStudentListByAdReqs(HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedItem.Value, ddlStudentGrpId.SelectedItem.Value, ddlShiftId.SelectedItem.Value, ddlStatusCodeId.SelectedItem.Value)
        '    If dtStudents.Rows.Count = 0 Then
        '        '   Display Error message            
        '        DisplayErrorMessage("No students found matching selection.")
        '        Exit Sub
        '    End If
        '    Session("StudentsDT") = dtStudents

        '    'Get All Requirements 
        '    '   (A) Requirements for selected Program Version
        '    '   (B) Mandatory Requirements
        '    '   (C) Required Requirements for selected Student Group
        '    dtReqs = facade.GetRequirementsByProgramVersionAndMandatories(HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedItem.Value, ddlStudentGrpId.SelectedItem.Value)
        '    If dtReqs.Rows.Count = 0 Then
        '        '   Display Error message  
        '        notFound = True
        '    Else
        '        Session("ReqsDT") = dtReqs
        '    End If
        '    'Get Requirement Groups for selected Program Version
        '    dtReqGrps = facade.GetReqGroupsByProgramVersion(HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedItem.Value, ddlStudentGrpId.SelectedItem.Value)
        '    If dtReqGrps.Rows.Count = 0 Then
        '        If notFound Then
        '            '   Display Error message  
        '            DisplayErrorMessage("No requirements nor requirement groups have been defined.")
        '            Exit Sub
        '        End If
        '    End If
        '    Session("ReqGroupsDT") = dtReqGrps

        '    'Get fulfilled DOCUMENTS
        '    dtStuDocs = facade.GetStudentRequirements(HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedItem.Value, ddlStudentGrpId.SelectedItem.Value, ddlShiftId.SelectedItem.Value, ddlStatusCodeId.SelectedItem.Value)
        '    Session("StuDocsDT") = dtStuDocs

        '    'Get fulfilled TESTS
        '    dtStuTests = facade.GetStudentTestRequirements(HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedItem.Value, ddlStudentGrpId.SelectedItem.Value, ddlShiftId.SelectedItem.Value, ddlStatusCodeId.SelectedItem.Value)
        '    Session("StuTestsDT") = dtStuTests

        '    'Get fullfilled REQUIREMENT GROUPS
        '    Dim strReqGrpList As String = facade.GetReqGroupsList(dtReqGrps)
        '    dtResReqGrps = facade.GetAttemptedReqsFromReqGroupByStudentId(HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedItem.Value, ddlStudentGrpId.SelectedItem.Value, ddlShiftId.SelectedItem.Value, ddlStatusCodeId.SelectedItem.Value, dtReqGrps)
        '    Session("ResReqGrpsDT") = dtResReqGrps

        '    BuildDocReqsGrid()
        '    PopulateDocReqsGrid()

        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    '   Redirect to error page.
        '    If ex.InnerException Is Nothing Then
        '        Session("Error") = "Error in Sub btnBuildList_Click " & ex.Message & " "
        '    Else
        '        Session("Error") = "Error in Sub btnBuildList_Click " & ex.Message & " " & ex.InnerException.Message
        '    End If
        '    Response.Redirect("../ErrorPage.aspx")
        'End Try

    End Sub
    Private Sub BindGridView(ByVal ProgId As String, _
                                      ByVal StudentGroup As String, _
                                      Optional ByVal StatusCodeId As String = "", _
                                      Optional ByVal ShiftId As String = "", _
                                      Optional ByVal StudentId As String = "", Optional ByVal reqType As String = "")

        Dim dt As DataTable = facade.GetStudentList(ProgId, StudentGroup, StatusCodeId, ShiftId, HttpContext.Current.Request.Params("cmpid"), StudentId, reqType)
        GridView1.DataSource = dt
        GridView1.DataBind()
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim blReqs As BulletedList = CType(e.Row.FindControl("blRequirements"), BulletedList)
            Dim dlReqGroup As DataList = CType(e.Row.FindControl("dlReqGroup"), DataList)
            Dim blReqsWithinReqGroup As BulletedList = CType(e.Row.FindControl("BlReqsWithReqGroup"), BulletedList)
            Dim dt1, dt2, dt3, dt4, dt5 As New DataTable

            'Bind requirements not satisified for student in the campus
            dt1 = facade.GetRequirements(GridView1.DataKeys(e.Row.RowIndex).Value.ToString, HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedValue, ddlShiftId.SelectedValue, ddlStatusCodeId.SelectedValue.ToString, ddlreqType.SelectedValue.ToString)
            If dt1.Rows.Count >= 1 Then
                blReqs.DataSource = dt1
                blReqs.DataBind()
            End If

            'Get Requirement Groups not satified by student in the campus
            dt2 = facade.GetRequirementsGroupsByStudent(GridView1.DataKeys(e.Row.RowIndex).Value.ToString, HttpContext.Current.Request.Params("cmpid"), ddlPrgVerId.SelectedValue, ddlShiftId.SelectedValue, ddlStatusCodeId.SelectedValue.ToString, ddlreqType.SelectedValue.ToString)
            If dt2.Rows.Count >= 1 Then
                dlReqGroup.DataSource = dt2
                dlReqGroup.DataBind()
            End If

            Dim dlItems As DataListItemCollection
            Dim dlItem As DataListItem
            Dim dgItems As DataGridItemCollection
            Dim dgItem As DataGridItem
            Dim z As Integer '', q
            Dim lblReqGrpId As Label
            If dt2.Rows.Count >= 1 Then
                dlItems = dlReqGroup.Items
                For z = 0 To dlItems.Count - 1
                    dlItem = dlItems.Item(z)
                    lblReqGrpId = CType(dlItem.FindControl("lblReqGrpId"), Label)
                    dt5 = facade.GetRequirementsByReqGroupAndStudent(lblReqGrpId.Text.ToString, GridView1.DataKeys(e.Row.RowIndex).Value.ToString, HttpContext.Current.Request.Params("cmpid"), ddlreqType.SelectedValue.ToString)
                    If dt5.Rows.Count >= 1 Then
                        Dim dgGrid As DataGrid = CType(dlItem.FindControl("dgReq"), DataGrid)
                        dgGrid.DataSource = dt5
                        dgGrid.DataBind()
                        dgItems = dgGrid.Items
                        dgItem = dgItems.Item(0)
                        Try
                            Dim blReqWithinReqGroup As BulletedList = CType(dgItem.FindControl("blReqWithinReqGroup"), BulletedList)
                            blReqWithinReqGroup.DataSource = dt5
                            blReqWithinReqGroup.DataTextField = "Descrip"
                            blReqWithinReqGroup.DataBind()
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                        End Try
                    End If
                Next
            End If
        End If
    End Sub
End Class
