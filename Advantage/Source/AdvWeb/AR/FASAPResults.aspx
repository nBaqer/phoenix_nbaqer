﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="FASAPResults.aspx.vb" Inherits="FASAPResults" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            var studentId = $("#studentIdInfo").val();
            var titleIvResults = new Api.ViewModels.AcademicRecords.TitleIVResults.TitleIVResults(studentId);
            titleIvResults.initialize();


        });
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div class="boxContainer">
        <h3><%=Header.Title  %></h3>
        <div id="titleIVResultsHeader" style="padding: 20px">
            <span>Enrollment <span class="font-red">*</span></span>
        <input id="enrollmentsdd" style="margin-left: 15px; width: 20%" />

        </div>
        <div id="TitleIVResultsTable" style="padding: 20px; width: 98%">
            <div id="titleIvResultsGrid"></div>
        </div>
    </div>
</asp:Content>
