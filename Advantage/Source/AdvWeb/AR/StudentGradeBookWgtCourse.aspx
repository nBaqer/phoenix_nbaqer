<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" Inherits="StudentGradeBookWgtCourse" CodeFile="StudentGradeBookWgtCourse.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

    <style type="text/css">
        .scrollHorizontal {
            overflow-y: scroll;
            width: 99%;
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="390px" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>

            <asp:Panel ID="panFilter" runat="server">
                <table id="Table2" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                    <tr>
                        <td class="listframetop">
                            <br />
                            <table id="tblSearch" runat="server" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                <tr id="trTermlbl" runat="server">
                                    <td class="contentcellblue" id="tdterm" runat="server">
                                        <asp:Label ID="lblTerm" runat="server" CssClass="label">Term</asp:Label></td>
                                </tr>
                                <tr id="trTerm" runat="server">
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlTerm" runat="server" CssClass="dropdownlist" AutoPostBack="True" Width="306px"></asp:DropDownList></td>
                                </tr>

                                <tr id="trCohortStartDatelbl" runat="server">
                                    <td class="contentcellblue">
                                        <asp:Label ID="label3" runat="server" CssClass="label">Cohort Start Date</asp:Label></td>
                                </tr>
                                <tr id="trCohortStartDate" runat="server">
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlcohortStDate" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="306px">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr id="trlblShift" runat="server">
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblShift" runat="server" CssClass="label">Shift (Optional)</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="306px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblMinimumClassStartDate" runat="server" CssClass="label">Class Starting From Date (Optional)</asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="contentcell4blue">
                                        <telerik:RadDatePicker ID="rdpMinimumClassStartDate" MinDate="1/1/1945" runat="server" AutoPostBack="true" Width="306px">
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblCourse" runat="server" CssClass="label">Class Section</asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlClsSection" runat="server" CssClass="dropdownlist" Width="306px"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="contentcell4blue" style="text-align: right">
                                        <telerik:RadButton ID="btnBuildList" runat="server" Text="Build List" CausesValidation="False"></telerik:RadButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">

                            <asp:DataGrid ID="dtlClsSectStds" runat="server" CssClass="datalistcontentar" GridLines="None"
                                Width="95%"
                                DataKeyField="StuEnrollId" AutoGenerateColumns="False">
                                <EditItemStyle Wrap="False"></EditItemStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                <FooterStyle CssClass="label"></FooterStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderStyle-CssClass="datalistheaderar" ItemStyle-CssClass="datalistar" HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkStudentName" runat="server" CssClass="itemstyle" CausesValidation="False" Text='<%# Container.DataItem("FullName")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="datalistheaderar" ItemStyle-CssClass="datalist" HeaderText="SSN">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSSN" runat="server" CssClass="itemstyle" CausesValidation="False" Text='<%# Container.DataItem("SSN")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="datalistheaderar" ItemStyle-CssClass="datalist" HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkStudentNumber" runat="server" CssClass="itemstyle" CausesValidation="False" Text='<%# Container.DataItem("StudentNumber")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="datalistheaderar" ItemStyle-CssClass="datalist" HeaderText="Final Grade">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblFinalGrade" runat="server" Text="Final Grade"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkGrade" runat="server" CssClass="itemstyle" CausesValidation="False" Text='<%# Container.DataItem("Grade")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lnkIsIncomplete" Visible="False" runat="server" CausesValidation="False" Text='<%# Container.DataItem("IsInComplete")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>

                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="99%" border="0">
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="False"></asp:Button>
                        <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" Enabled="False" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" Enabled="False" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <!-- end top menu (save,new,reset,delete,history)-->
            <!--begin right column-->
            <div class="boxContainer full">
                <h3><%=Header.Title  %></h3>
                <!-- begin table content-->
                <asp:Panel ID="pnlHeader" runat="server">
                    <asp:Panel ID="pnlRHS" runat="server">
                        <table class="contenttable" cellspacing="0" cellpadding="0" style="width: 70%; min-width: 800px;" border="0">
                            <tr>
                                <td class="arcontentcells">
                                    <asp:Label ID="lblStd" runat="server" CssClass="label">Student</asp:Label></td>
                                <td style="width: 1000px">
                                    <asp:Label ID="lblStdName" runat="server" CssClass="textbox"></asp:Label></td>
                                <td class="arcontentcells">
                                    <asp:Label ID="lblSection" runat="server" CssClass="label">Class Section</asp:Label></td>
                                <td class="arcontentcells2">
                                    <asp:Label ID="lblSecName" runat="server" CssClass="textbox"></asp:Label></td>
                            </tr>
                        </table>
                        <div class="scrollHorizontal">
                            <asp:Panel ID="pnlGBWResults" runat="server"></asp:Panel>
                        </div>
                        <table class="contenttable" cellspacing="0" cellpadding="0"  style="width: 60%; min-width: 800px;" align="center" border="0">
                            <tr>
                                <td class="spacertables"></td>
                            </tr>
                            <tr>
                                <td class="arbuttoncells">
                                    <asp:Label ID="lblCurrentScore" runat="server" CssClass="label">Current Score</asp:Label></td>
                                <td class="arbuttoncells2">
                                    <asp:TextBox ID="txtFinalScore" runat="server" CssClass="textbox" ReadOnly="True"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="arbuttoncells">
                                    <asp:Label ID="lblCurrentGrade" runat="server" CssClass="label">Current Grade</asp:Label></td>
                                <td class="arbuttoncells2">
                                    <asp:TextBox ID="txtFinalGrade" runat="server" CssClass="textbox" ReadOnly="True"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="contentcell" nowrap></td>
                                <td class="contentcell4">
                                    <asp:CheckBox ID="chkIsIncomplete" runat="server" CssClass="label" Text="Incomplete Grade" Visible="false"></asp:CheckBox></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>

                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
            </div>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
</asp:Content>
