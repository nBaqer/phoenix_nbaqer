Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common

Partial Class Term
    Inherits BasePage
    Protected WithEvents BtnReset As Button
    Protected WithEvents Btnhistory As Button
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected UserId As String
    Protected CampusId As String
    Protected MyAdvAppSettings As AdvAppSettings
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Public StrCampusGroup As String
    ReadOnly cmpCallReportingAgencies As New ReportingAgencies
    Protected intRptFldId, intResultsAffected As Integer
    Protected boolRptAgencyAppToSchool As Boolean
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        'Header1.EnableHistoryButton(False)
        Dim mContext As HttpContext
        mContext = HttpContext.Current
        txtresourceId.Text = CInt(mContext.Items("ResourceId"))

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New IO.StringWriter
        Dim ds2 As DataSet
        Dim sStatusId As String
        Dim sdfControls As New SDFComponent
        Dim userId As String
        'Dim fac As New UserSecurityFacade
        '  Dim resourceId2 As Integer
        Dim rfv As RequiredFieldValidator
        Dim ctl As Control

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        ViewState("IsUserSa") = advantageUserState.IsUserSA

        mContext = HttpContext.Current
        txtresourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtresourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                BuildDropDownLists()

                PopulateTermTypes()

                'Set the text box to a new guid
                txtTermId.Text = System.Guid.NewGuid.ToString

                'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                'ds = objListGen.SummaryListGenerator(userId, campusId)

                ViewState("IsUserSa") = advantageUserState.IsUserSA

                ds = objListGen.SummaryListGenerator(userId, campusId)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()

                'Generate the status id
                ds2 = objListGen.StatusIdGenerator()

                'Set up the primary key on the datatable
                ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

                Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
                sStatusId = row("StatusId").ToString()

                'Get TermIds with PrgVerIds
                'Dim dst As DataSet = (New TermsFacade).GetAllTermIdsWithProgIds(True, campusId)
                Dim dst As DataSet = (New TermsFacade).GetAllTermIdsWithProgIds(False, campusId)
                ViewState("TermIdsProgIds") = dst.Tables(0)
                BuildProgsDDL(dst.Tables(0), ddlProgId1)
                BindDatalist()

                ''Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", " StartDate,TermDescrip", DataViewRowState.CurrentRows)
                'Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "", DataViewRowState.CurrentRows)

                'With dlstTerm
                '    .DataSource = dv
                '    .DataBind()
                'End With

                'change the 'Select' entry of the Program DDL to 'All Programs'
                ddlProgId.Items(0).Text = "All Programs" 'Insert(0, New ListItem("Select", ""))
                'SDFControls.GenerateControlsNew(pnlSDF, ResourceId)

                'fix for issue #08447 BN
                ctl = pnlRequiredFieldValidators.FindControl("rfvTermTypeId2962")
                rfv = CType(ctl, RequiredFieldValidator)
                rfv.Enabled = True
                rfv.InitialValue = 0

                'BindGroupsControl
                BindGroupsControl()

                'The commented section will be uncommented later
                'balaji 10/1/2008
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    ddlSession.Visible = True
                    ddlSessionStart.Visible = True
                    ddlSessionEnd.Visible = True
                    lblSessionStart.Visible = True
                    lblregentterm.Visible = True
                    lblSessionEnd.Visible = True
                    BuildSession()
                    pnlRegent.Visible = True
                End If
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ddlProgId.Items(0).Text = "All Programs"
                'fix for issue #08447 BN
                ctl = pnlRequiredFieldValidators.FindControl("rfvTermTypeId2962")
                rfv = CType(ctl, RequiredFieldValidator)
                rfv.Enabled = True
                rfv.InitialValue = 0
                'Call the procedure for edit
            End If

            'If Not Page.IsPostBack Then
            '    boolRptAgencyAppToSchool = AdvantageCommonValues.CheckIfReportingAgenciesExistForSchool
            '    If boolRptAgencyAppToSchool = True Then
            '        pnlRegent.Visible = True
            '        'Get the field id from the read only property 
            '        'stored in Advantage Common values
            '        intRptFldId = AdvantageCommonValues.Terms

            '        'Call the Generate Reporting Agencies function in Reporting Agencies component
            '        cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtTermId.Text, "Terms")
            '    Else
            '        pnlRegent.Visible = False
            '    End If
            'End If


            'Check If any UDF exists for this resource
            Dim intSdfExists As Integer = sdfControls.GetSDFExists(CType(ResourceId, Integer), ModuleId)
            If intSdfExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If

            If Trim(txtTermCode.Text) <> "" Then
                sdfControls.GenerateControlsEditSingleCell(pnlSDF, CType(ResourceId, Integer), txtTermId.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNewSingleCell(pnlSDF, CType(ResourceId, Integer), ModuleId)
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BuildDropDownLists()
        ddlProgId.Items.Clear()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        ddlList.Add(New AdvantageDDLDefinition(ddlProgId, AdvantageDropDownListName.Programs, campusId, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlCampGrpId, AdvantageDropDownListName.CampGrps, Nothing, True, True))
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub
    'Balaji on 10/1/2008 will be uncommented later
    Private Sub BuildSession()
        With ddlSession
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("Session", txtTermId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSessionStart
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("SessionStart", txtTermId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSessionEnd
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("SessionEnd", txtTermId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim msg As String
        Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)

        Try
            If (DateDiff(DateInterval.Day, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate)) <= 0) Then
                DisplayErrorMessage("The end date entered should always be greater than start date")
                Exit Sub
            End If
            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtTermId.Text
                txtRowIds.Text = objCommon.PagePK

                msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                ds = objListGen.SummaryListGenerator(userId, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
                dlstTerm.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                
            ElseIf ViewState("MODE") = "EDIT" Then
                Dim dsClassMinAndMaxDates As DataSet
                Dim minDAte As Date = DateTime.MinValue
                Dim maxDate As Date = DateTime.MinValue
                ''Date Validation Added by Saraswathi lakshmanan on May 13 2010
                ''Aded to fix issue mantis case 18921

                dsClassMinAndMaxDates = (New TermProgressFacade).getMinAndMaxDatesForClassSections(txtTermId.Text)
                If dsClassMinAndMaxDates.Tables.Count > 0 Then
                    If dsClassMinAndMaxDates.Tables(0).Rows.Count > 0 Then
                        If Not dsClassMinAndMaxDates.Tables(0).Rows(0)("MinDate") Is System.DBNull.Value Then
                            minDAte = CType(dsClassMinAndMaxDates.Tables(0).Rows(0)("MinDate"), Date)
                        End If
                        If Not dsClassMinAndMaxDates.Tables(0).Rows(0)("MaxDate") Is System.DBNull.Value Then
                            maxDate = CType(dsClassMinAndMaxDates.Tables(0).Rows(0)("MaxDate"), Date)
                        End If
                        If minDAte <> DateTime.MinValue And maxDate <> DateTime.MinValue Then
                            If CDate(txtStartDate.SelectedDate) > minDAte Then
                                DisplayErrorMessage("The Start date entered should always be Less than the existing ClassSection start date: " + Format(minDAte, "MM/dd/yyyy"))
                                Exit Sub
                            ElseIf CDate(txtEndDate.SelectedDate) < maxDate Then
                                DisplayErrorMessage("The End date entered should always be greater than the existing ClassSection End date: " + Format(maxDate, "MM/dd/yyyy"))
                                Exit Sub
                            End If
                        End If
                    End If
                End If

                If Trim(txtOldCampGrpId.Value) <> Trim(ddlCampGrpId.SelectedValue) Then
                    Dim winSettings As String = "toolbar=no,status=no,width=650px,height=400px,left=250px,top=200px,modal=yes"
                    Dim name As String = "AdmReqSummary1"
                    Dim intClsSectionExists As Integer = (New TermProgressFacade).IsThereAnyClsSectionForTerm(txtTermId.Text, txtOldCampGrpId.Value, ddlCampGrpId.SelectedValue)
                    If intClsSectionExists >= 1 Then
                        Dim URL As String = "DisplayClassSections.aspx?TermId=" + txtTermId.Text + "&CampGrpId=" + txtOldCampGrpId.Value + "&Term=" + txtTermDescrip.Text + "&NewCampus=" + ddlCampGrpId.SelectedItem.Text + "&OldCampusGroup=" + Session("OldCampusGroup") + "&NewCampGrpId=" + ddlCampGrpId.SelectedValue
                        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                        Exit Sub
                    End If
                End If
                msg = objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                ds = objListGen.SummaryListGenerator(userId, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
                dlstTerm.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Response.Write(objCommon.strText)
            End If



            'insert class sections of the selected groups
            If Not String.IsNullOrEmpty(txtTermId.Text) Then
                Dim CAmpGrpId As String
                CAmpGrpId = ddlCampGrpId.SelectedValue.ToString
                If CAmpGrpId.Trim <> String.Empty Then
                    ''Modified by Saarswathi lakshmanan on may 12 2010
                    ''to fix mantis case 18921
                    Dim result As String = (New ClassSectionFacade).InsertAllClassSectionsBelongingToACourseGroup_CAmpusID(txtTermId.Text, GetGroupsFromControl(ddlGroups.Items), CAmpGrpId, Session("UserName"))
                    If result <> "" Then
                        '   Display Error Message
                        DisplayErrorMessage(result)
                    Else
                        'bind groups control
                        BindGroupsControl()
                    End If
                End If

            End If

            '   set Fees Hyperlink
            setFeesHyperlink()

            'If regent is being used
            'comment by balaji on 10/1/2008
            'will be uncommented later 
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                ''Validate start and end term
                'If chkMarkTermasStartTerm.Checked And chkMarkTermasEndTerm.Checked Then
                '    DisplayErrorMessage("A term can be marked as either a start term or end term")
                '    Exit Sub
                'End If
                Dim strRegentUpdate As String = (New regentFacade).updateRegentTerm(txtTermId.Text, ddlSession.SelectedValue, CType(Session("User"), String), ddlSessionStart.SelectedValue, ddlSessionEnd.SelectedValue)
            End If

            'Dim arrReportingAgencyValue As ArrayList
            'arrReportingAgencyValue = cmpCallReportingAgencies.GetAllValues(pnlReportingAgencies)
            'intResultsAffected = cmpCallReportingAgencies.InsertValues(txtTermId.Text, arrReportingAgencyValue)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim sdfid As ArrayList
            Dim sdfidValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim sdfControl As New SDFComponent
            Try
                sdfControl.DeleteSDFValue(txtTermId.Text)
                sdfid = sdfControl.GetAllLabels(pnlSDF)
                sdfidValue = sdfControl.GetAllValues(pnlSDF)
                For z = 0 To sdfid.Count - 1
                    sdfControl.InsertValues(txtTermId.Text, Mid(sdfid(z).id, 5), sdfidValue(z))
                Next
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CommonWebUtilities.RestoreItemValues(dlstTerm, txtTermId.Text)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New IO.StringReader(CStr(ViewState("ds")))
        Try
            ClearRHS()
            ds.ReadXml(sr)
            PopulateDataList(ds)
            ''Added by saraswathi lakshmanan on may 22 2009
            ''Inactive program show up on the Terms maintenance page in a certain scenario. 
            ''To fix issue 16285
            BuildDropDownLists()
            ddlProgId.Items(0).Text = "All Programs"

            'Set the text box to a new guid
            txtTermId.Text = Guid.NewGuid.ToString
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"

            'Enable the start & end dates everytime user clicks New button
            txtStartDate.Enabled = True
            txtEndDate.Enabled = True

            'ddlProgId1.Items.Clear()
            'Dim dtterm1 As New DataTable
            'dtterm1 = ViewState("TermIdsProgIds")
            'BuildProgsDDL(dtterm1, ddlProgId1)

            'Reset Style in the Datalist
            CommonWebUtilities.RestoreItemValues(dlstTerm, Guid.Empty.ToString)
            lbtSetUpFees.Enabled = False

            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)

            'cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtTermId.Text)

            'If regent is being used
            'comment by balaji on 10/1/2008
            'will be uncommented later 
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                BuildSession()
            End If

            'BindGroupsControl
            BindGroupsControl()
            CommonWebUtilities.RestoreItemValues(dlstTerm, Guid.Empty.ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New IO.StringWriter
        Dim msg As String
        Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)
        Try
            intResultsAffected = cmpCallReportingAgencies.InsertValues(txtTermId.Text, Nothing)
            msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If msg <> "" Then
                DisplayErrorMessage("The term you are trying to delete is Active/In Use and cannot be deleted")
            End If
            ClearRHS()
            ds = objListGen.SummaryListGenerator(userId, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
            dlstTerm.SelectedIndex = -1
            PopulateDataList(ds)
            ds.WriteXml(sw)
            ViewState("ds") = sw.ToString()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtTermId.Text = System.Guid.NewGuid.ToString
            lbtSetUpFees.Enabled = False

            'Enable the start & end dates everytime user clicks New button
            txtStartDate.Enabled = True
            txtEndDate.Enabled = True


            ' Header1.EnableHistoryButton(False)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim SDFControl As New SDFComponent
            SDFControl.DeleteSDFValue(txtTermId.Text)
            SDFControl.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtTermId.Text)
            'If regent is being used
            'comment by balaji on 10/1/2008
            'will be uncommented later 
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                Dim strRegentUpdate As String = (New regentFacade).updateRegentTerm(txtTermId.Text, ddlSession.SelectedValue, Session("User"), ddlSessionStart.SelectedValue, ddlSessionEnd.SelectedValue)
                BuildSession()
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    If CType(ctl, DropDownList).Items.Count > 0 Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                    End If

                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub PopulateTermTypes()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New ProgVerFacade

        ' Bind the Dataset to the CheckBoxList
        ddlTermTypeId.DataSource = facade.GetTermTypes()
        ddlTermTypeId.DataTextField = "Descrip"
        ddlTermTypeId.DataValueField = "TermTypeId"
        ddlTermTypeId.DataBind()
        ddlTermTypeId.Items.Insert(0, New ListItem("Select", 0))
        ddlTermTypeId.SelectedIndex = 0
    End Sub
    Private Sub dlstTerm_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstTerm.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Try
            Master.PageObjectId = e.CommandArgument
            Master.PageResourceId = ResourceId
            Master.setHiddenControlForAudit()

            strGUID = dlstTerm.DataKeys(e.Item.ItemIndex).ToString()
            txtrowIds.Text = e.CommandArgument.ToString
            PopulateTermTypes()

            'CType(CType(sender, DataList).Controls(0), LinkButton).ForeColor = Color.Red

            ' DirectCast(DirectCast(dlstTerm.Controls(e.Item.ItemIndex), System.Web.UI.WebControls.DataListItem).Controls(7), System.Web.UI.WebControls.LinkButton).ForeColor = Color.Red

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(FindControlRecursive("dlstTerm"), strGUID, ViewState)

            'DirectCast(DirectCast(dlstTerm.Controls(e.Item.ItemIndex), System.Web.UI.WebControls.DataListItem).Controls(7), System.Web.UI.WebControls.LinkButton).ForeColor = Color.Red

            'ddlProgId1.Items.Clear()
            ''Get TermIds with PrgVerIds
            'Dim dtterm As DataTable

            'dtterm = ViewState("TermIdsProgIds")
            'BuildProgsDDL(dtterm, ddlProgId1)
            ''Added by saraswathi lakshmanan on may 22 2009
            ''Inactive program show up on the Terms maintenance page in a certain scenario. 
            ''To fix issue 16285
            BuildDropDownLists()
            ddlProgId.Items(0).Text = "All Programs"

            '**REM 8/8/11
            objCommon.PopulatePage1(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)

            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            Session("OldCampusGroup") = ddlCampGrpId.SelectedItem.Text

            selIndex = e.Item.ItemIndex
            dlstTerm.SelectedIndex = selIndex
            ds.ReadXml(sr)
            'PopulateDataList(ds)
            BindDatalist()

            'bind groups control
            BindGroupsControl()

            '   set Fees Hyperlink
            setFeesHyperlink()

            'get regent term value
            'comment by balaji on 10/1/2008
            'will be uncommented later 
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                Dim dt As New DataTable
                dt = (New regentFacade).getregentTerm(strGUID)
                If dt.Rows.Count >= 1 Then
                    For Each dr2 As DataRow In dt.Rows
                        ddlSession.SelectedValue = dr2("regentTerm").ToString
                        If Not dr2("regentStartTerm") Is System.DBNull.Value Then ddlSessionStart.SelectedValue = dr2("regentStartTerm").ToString Else ddlSessionStart.SelectedValue = ""
                        If Not dr2("regentEndTerm") Is System.DBNull.Value Then ddlSessionEnd.SelectedValue = dr2("regentEndTerm").ToString Else ddlSessionEnd.SelectedValue = ""
                    Next
                End If
            End If

            Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)

            'set permissions for the start and end dates textboxes
            Dim b As Boolean = (New StudentsAccountsFacade).IsThisTermBeingUsed(txtrowIds.Text)
            b = (b And isUserSa) Or Not b
            txtStartdate.Enabled = b
            txtEndDate.Enabled = b

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
            'SDF Code Starts Here

            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsEditSingleCell(pnlSDF, ResourceId, txtTermId.Text, ModuleId)

            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            txtOldCampGrpId.Value = ddlCampGrpId.SelectedValue

            'If Year(txtStartDate.SelectedDate) = 1945 Then
            '    txtStartDate.SelectedDate = Nothing
            'End If
            'If Year(txtEndDate.SelectedDate) = 1945 Then
            '    txtEndDate.SelectedDate = Nothing
            'End If

            CommonWebUtilities.RestoreItemValues(dlstTerm, strGUID)

            'If (e.Item.ItemType = WebControls.ListItemType.SelectedItem) Then
            '    dlstTerm.SelectedItem.BackColor = Color.Red
            'End If
            'CommonWebUtilities.RestoreItemValues(dlstTerm, strGUID)

            'If (e.Item.ItemType = WebControls.ListItemType.SelectedItem) Then
            '    Dim lnkRef As System.Web.UI.WebControls.LinkButton = DirectCast(e.Item.FindControl("linkbutton1"), System.Web.UI.WebControls.LinkButton)

            '    If (lnkRef IsNot Nothing) Then
            '        'Dim dataRow As DataRow = DirectCast(e.Item.DataItem, DataRowView).Row
            '        lnkRef.CommandArgument = strGUID 'String.Format("{0}", dataRow("TermId").ToString())
            '        Dim clickHandler As String = "return changeColor(this);"
            '        lnkRef.Attributes.Add("onclick", clickHandler)
            '    End If
            'End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstTerm_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstTerm_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    'Private Shared Sub RestoreItemValues(ByVal dataList As DataList, ByVal selectedItem As String)
    '    Dim linkButton As LinkButton
    '    Dim idxsOfLinkButtons(10) As Integer
    '    Dim k As Integer = 0
    '    '   loop through all datalist items 
    '    '   check only linkButtons
    '    For i As Integer = 0 To dataList.Items.Count - 1
    '        '   the first time get an array with the indexes of all linkbuttons
    '        If i = 0 Then
    '            For j As Integer = 0 To dataList.Items(i).Controls.Count - 1
    '                If dataList.Items(i).Controls(j).GetType.ToString = "System.Web.UI.WebControls.LinkButton" Then
    '                    idxsOfLinkButtons(k) = j
    '                    k += 1
    '                End If
    '            Next
    '            'ReDim idxsOfLinkButtons(k - 1)
    '        End If
    '        For j As Integer = 0 To k - 1
    '            linkButton = CType(dataList.Items(i).Controls(idxsOfLinkButtons(j)), LinkButton)
    '            If linkButton.CommandArgument = selectedItem Then
    '                linkButton.CssClass = dataList.ItemStyle.CssClass

    '                '   apply style to all controls in that item
    '                For Each ctl As Control In dataList.Items(i).Controls
    '                    '   do not change CssClass to Repeaters
    '                    Try
    '                        If ctl.GetType.ToString.IndexOf(".WebControls.") > 0 And ctl.GetType.ToString.IndexOf(".WebControls.Repeater") < 0 Then
    '                            CType(ctl, WebControl).CssClass = dataList.ItemStyle.CssClass
    '                        End If
    '                    Catch ex As Exception
     '                    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '                    	exTracker.TrackExceptionWrapper(ex)


    '                    End Try

    '                Next
    '                'linkButton.CssClass = "labelbold"
    '                linkButton.Font.Bold = True
    '                Exit Sub
    '            End If
    '        Next
    '    Next
    'End Sub
    'Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objListGen As New DataListGenerator
    '    Dim ds As New DataSet
    '    Dim ds2 As New DataSet
    '    Dim sw As New System.IO.StringWriter
    '    Dim objCommon As New CommonUtilities
    '    Dim dv2 As New DataView
    '    '        Dim sStatusId As String
    '    Try
    '        ClearRHS()
    '        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
    '        ViewState("MODE") = "NEW"
    '        'Set the text box to a new guid
    '        txtTermId.Text = System.Guid.NewGuid.ToString
    '        ds = objListGen.SummaryListGenerator()
    '        PopulateDataList(ds)
    '    Catch ex As System.Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try
    'End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/22/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radstatus.SelectedItem.Text.ToLower = "active") Or (radstatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radstatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        '    Dim dv As New DataView(ds.Tables(0), sDataFilter, "TermDescrip", DataViewRowState.CurrentRows)
        ''Modified to order the term by date instead of Description
        ''DE8332 QA: Terms on the Terms maintenance page is not sorted by date.
        Dim dv As New DataView(ds.Tables(0), sDataFilter, "", DataViewRowState.CurrentRows)


        With dlstTerm
            .DataSource = dv
            .DataBind()
        End With
        dlstTerm.SelectedIndex = -1
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        'Dim objListGen As New DataListGenerator
        'Dim ds As New DataSet
        'Dim ds2 As New DataSet
        'Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        'Dim dv2 As New DataView
        '        Dim sStatusId As String
        lbtSetUpFees.Enabled = False

        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtTermId.Text = Guid.NewGuid.ToString
            BindDatalist()
            'ds = objListGen.SummaryListGenerator()
            'PopulateDataList(ds)
            '  Header1.EnableHistoryButton(False)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub SetFeesHyperlink()

        '   enable Set Up Fees button
        lbtSetUpFees.Enabled = True

        '   set Style to Selected Item
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstTerm, termId, ViewState, Header1)

    End Sub
    Private Sub lbtSetUpFees_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtSetUpFees.Click

        Dim sb As New StringBuilder
        '   append CourseId to the querystring
        sb.Append("&TermId=" + txtTermId.Text)
        '   append Course Description to the querystring
        sb.Append("&Term=" + txtTermDescrip.Text)
        '   setup the properties of the new window
        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "PeriodicFees"
        Dim url As String = "../SA/" + name + ".aspx?resid=163&mod=SA&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub
    Private Sub BuildProgsDDL(ByVal dt As DataTable, ByVal ddl As DropDownList)
        Dim previousProgId As Guid = Guid.Empty
        Dim arr() As DataRow
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select", Guid.Empty.ToString()))
        arr = dt.Select("", "ProgId")
        For i As Integer = 0 To arr.Length - 1
            Dim row As DataRow = arr(i)
            If Not IsDBNull(row("ProgId")) Then
                If Not row("ProgId") = previousProgId Then
                    previousProgId = row("ProgId")
                    ddl.Items.Add(New ListItem(row("ShiftDescrip"), CType(row("ProgId"), Guid).ToString()))
                End If
            End If
        Next
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(lbtSetUpFees)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    'Protected Sub dlstTerm_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlstTerm.ItemDataBound
    '    Select Case e.Item.ItemType
    '        Case ListItemType.Item, ListItemType.AlternatingItem
    '            If Not IsTermInProgramVersion(e.Item.DataItem("TermId"), ddlProgId1.SelectedValue) Then
    '                e.Item.Visible = False
    '            End If
    '    End Select
    'End Sub
    Private Function IsTermInProgramVersion(ByVal termId As String, ByVal progId As String) As Boolean
        Dim progIdGuid As Guid = New Guid(progId)
        'Dim dt As DataTable = ViewState("TermIdsProgIds")
        Dim dt As DataTable = CType(ViewState("TermIdsProgIds"), DataTable)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim row As DataRow = dt.Rows(i)
            If Not IsDBNull(row("ProgId")) Then
                If row("ProgId") = progIdGuid Then
                    If CType(row("TermId"), Guid).ToString() = termId Then Return True
                End If
            End If
        Next
        Return False
    End Function

    Protected Sub ddlProgId1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProgId1.SelectedIndexChanged
        BindDatalist()
        ClearRHS()
    End Sub
    Private Sub BindDatalist()
        Dim ds As New DataSet
        Dim sr As New IO.StringReader(CStr(ViewState("ds")))
        ds.ReadXml(sr)
        If ds.Tables.Count >= 1 Then
            DeleteProgramRowsFromTable(ds.Tables(0))
            PopulateDataList(ds)
        End If

    End Sub
    Private Sub DeleteProgramRowsFromTable(ByVal dt As DataTable)
        If ddlProgId1.SelectedValue = Guid.Empty.ToString() Then Return
        Dim thereAreRowsToBeDeleted As Boolean
        Do While (True)
            thereAreRowsToBeDeleted = False
            For Each row As DataRow In dt.Rows
                If Not IsTermInProgramVersion(CType(row("TermId"), String), ddlProgId1.SelectedValue) Then
                    row.Delete()
                    thereAreRowsToBeDeleted = True
                    Exit For
                End If
            Next
            If Not thereAreRowsToBeDeleted Then Exit Do
        Loop
    End Sub
    Private Function GetGroupsFromControl(ByVal listItemCollection As ListItemCollection) As String()
        Dim list As New List(Of String)
        For Each item As ListItem In listItemCollection
            If item.Selected And item.Value <> Guid.Empty.ToString() Then list.Add(item.Value)
        Next
        Return list.ToArray()
    End Function
    Private Sub BindGroupsControl()
        'populate Future Terms panel
        Dim campGrpId As String
        campGrpId = ddlCampGrpId.SelectedValue.ToString
        ''Added by Saraswathi Tio fix issue US1660 Error when adding terms 
        ''Added on January 20 2010
        If txtTermId.Text = String.Empty Then
            'Set the text box to a new guid
            txtTermId.Text = System.Guid.NewGuid.ToString
        End If

        If campGrpId.Trim <> String.Empty Then
            ddlGroups.DataSource = (New ClassSectionFacade).GetGroupsWithMissingClassSections(txtTermId.Text, campGrpId)
            ddlGroups.DataValueField = "GrpId"
            ddlGroups.DataTextField = "Descrip"
            ddlGroups.DataBind()
            ddlGroups.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString()))
            ddlGroups.SelectedIndex = 0

            ' Group panel should be visible only if there are groups
            If ddlGroups.Items.Count > 1 Then
                pGroups.Visible = True
                lApplyToTheseGroups.Visible = True
            Else
                pGroups.Visible = False
                lApplyToTheseGroups.Visible = False
            End If
        Else
            pGroups.Visible = False
            lApplyToTheseGroups.Visible = False
        End If
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ' ''To find the list controls and add a tool tip to those items in the control
    ' ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub ddlCampGrpId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampGrpId.SelectedIndexChanged
        BindGroupsControl()
    End Sub
End Class
