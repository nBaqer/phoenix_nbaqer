﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SetupTestRulesPanel.aspx.vb" Inherits="AR_SetupTestRulesPanel" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    		<LINK href="../css/systememail.css" type="text/css" rel="stylesheet"/>
    </head>
<body>
    <form id="mainForm" method="post" runat="server">
        <telerik:RadScriptManager ID="ScriptManager" runat="server" />
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
            <telerik:RadPanelBar runat="server" ID="RadPanelBar1" ExpandMode="SingleExpandedItem" Width="740px">
                <Items>
                    <telerik:RadPanelItem Expanded="True" Text="Prerequisites" runat="server" Selected="true"> 
                        <Items>
                            <telerik:RadPanelItem Value="Prerequisites" runat="server">
                                <ItemTemplate>
                                    <div class="text" style="background-color: #edf9fe">
                                         <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblMinCategory" runat="server" CssClass="Label">Minimum Required per Category:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMinCategory" runat="server" Width="50px" CssClass="textbox"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMaxCategory" runat="server" CssClass="Label">Maximum Required per Category:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMaxCategory" runat="server" Text="2" Width="50px" CssClass="textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblMinCombination" runat="server" CssClass="Label">Minimum Combination:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMinCombination" runat="server" Width="50px" CssClass="textbox"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMaxCombination" runat="server" CssClass="Label">Maximum Combination:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMaxCombination" runat="server" Text="2" Width="50px" CssClass="textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblMentor" runat="server" CssClass="Label">Mentor/Proctored Test Requirement:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlMentor" runat="server" CssClass="DropDownList">
                                                        <asp:ListItem value="0">None</asp:ListItem>
                                                        <asp:ListItem value="1">1</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label">Prerequisite Course:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlPrereq" runat="server" CssClass="DropDownList">
                                                        <asp:ListItem value="0">SH120</asp:ListItem>
                                                        <asp:ListItem value="1">SH130</asp:ListItem>
                                                        <asp:ListItem value="1">SH200</asp:ListItem>
                                                        <asp:ListItem value="1">SH220</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr height="20px"><td></td></tr>
                                        </table>
                                        <p>
                                            <asp:Label ID="lblExample" runat="server" Enabled="false" Font-Bold="true" CssClass="LabelBold">
                                            Example: The following is the prerequisite for course SH220. <br />
                                            
                                            A minimum of 1 and maximum of 2 in each category for a combination of 5 of the 
                                            6 requirements listed under top-speed testing category + mentor\proctored test requirement 
                                            for course SH200
                                            </asp:Label>
                                        </p>
                                        <br />
                                    </div>
                                </ItemTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem Enabled="True" Text="TopSpeedTestingCategory" runat="server">
                        <Items>
                            <telerik:RadPanelItem Value="TopSpeedTestingCategory" runat="server">
                                    <ItemTemplate>
                                    <div class="text" style="background-color: #edf9fe">
                                         <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="80%" align="center">
										    <tr>
										        <td  class="twocolumnlabelcell"></td>
										        <td class="twocolumncontentcell">
										             <telerik:RadGrid ID="RadTestDetailsGrid" runat="server" 
                                                            AutoGenerateColumns="False" AllowSorting="True" 
                                                             AllowMultiRowSelection="True" 
                                                            GridLines="None" Width="600px">
                                                            <mastertableview>
                                                                <Columns>
                                                                    <telerik:GridTemplateColumn Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txtGrdComponentTypeid_ReqId" Text='<%# Bind("GrdComponentTypeid_ReqId") %>'></asp:TextBox>  
                                                                        </ItemTemplate> 
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Row #">
                                                                        <ItemStyle Width="10%" />
                                                                        <ItemTemplate>
                                                                            <asp:label runat="server" ID="lblRowNumber" Text='<%# Bind("RowNumber") %>'></asp:label>  
                                                                        </ItemTemplate> 
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Test">
                                                                        <ItemStyle Width="30%" />
                                                                        <ItemTemplate>
                                                                            <asp:label runat="server" ID="lblDescrip" Text='<%# Bind("Descrip") %>'></asp:label>  
                                                                        </ItemTemplate> 
                                                                    </telerik:GridTemplateColumn>
                                                                     <telerik:GridTemplateColumn Visible="true" HeaderText="Number of Tests" ItemStyle-Width="20%">
                                                                     <ItemStyle Width="20%" />
                                                                        <ItemTemplate>
                                                                            <asp:textbox ID="txtNumberofTest" runat="server" Text='<%# Bind("NumberofTests") %>'></asp:textbox>
                                                                        </ItemTemplate> 
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn Visible="true" HeaderText="Minimum number of words per minute" ItemStyle-Width="20%">
                                                                    <ItemStyle Width="20%" />
                                                                        <ItemTemplate>
                                                                            <asp:textbox ID="txtMinimumScore" runat="server" Text='<%# Bind("MinimumScore") %>'></asp:textbox>
                                                                        </ItemTemplate> 
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                            </mastertableview>
                                                            <clientsettings>
                                                                <scrolling allowscroll="True" />
                                                                <Selecting AllowRowSelect="True"></Selecting>
                                                            </clientsettings>
                                                        </telerik:RadGrid>
										        </td>
										    </tr>
										  </TABLE>
                                    </div>
                                </ItemTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem Enabled="True" Text="Mentor/Proctored Test Requirement" runat="server">
                        <Items>
                            <telerik:RadPanelItem Value="Mentor" runat="server">
                                <ItemTemplate>
                                    <div class="text" style="background-color: #edf9fe">
                                        
                                    </div>
                                </ItemTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
                <CollapseAnimation Duration="100" Type="None" />
                <ExpandAnimation Duration="100" Type="None" />
            </telerik:RadPanelBar>
            <br />
            <asp:Button runat="server" ID="backButton" Visible="false" CssClass="qsfButton" Text="Back"  style="margin: 10px 0 30px 25px" />
        </telerik:RadAjaxPanel>
    </form>
    <script type="text/javascript">
        function validateTerms(source, args)
        {
        }
    </script>
</body>
</html>
