﻿Imports System.Diagnostics
Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports System.Collections.Generic
Imports eWorld.UI
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Partial Class StudentAddresses
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnhistory As Button
    Protected WithEvents lblAddress As Label
    Protected sStatusId As String
    Protected boolStatus As String
    Protected WithEvents txtPhone As MaskedTextBox
    Protected WithEvents ZipFormat As Label
    Private requestContext As HttpContext
    Protected StudentId, LeadId As String
    Protected WithEvents txtCampusId As TextBox
    Protected resourceId As Integer
    Protected ModuleId As String
    Protected state As AdvantageSessionState




    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub


    Private Function GetStudentFromStateObject(ByVal paramResourceId As Integer) As StudentMRU


        Dim objStudentState As New StudentMRU

        MyBase.GlobalSearchHandler(0)

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
            StudentId = Guid.Empty.ToString()
        Else
            StudentId = AdvantageSession.MasterStudentId
        End If

        If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
            LeadId = Guid.Empty.ToString()
        Else
            LeadId = AdvantageSession.MasterLeadId
        End If


        With objStudentState
            .StudentId = New Guid(StudentId)
            .LeadId = New Guid(LeadId)
            .Name = AdvantageSession.MasterName
        End With

        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        Master.ShowHideStatusBarControl(True)





        Return objStudentState

    End Function
#End Region

#End Region
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected boolSwitchCampus As Boolean = False
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext

        Page.Title = "Student Addresses"

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString

        Trace.Warn("begin mru")
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = GetStudentFromStateObject(155) '

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString


        End With

        Trace.Warn("end mru")

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        m_Context = HttpContext.Current
        txtResourceId.Text = CInt(m_Context.Items("ResourceId"))



        Dim advantageUserState As New User()

        advantageUserState = AdvantageSession.UserState

        Trace.Warn("begin populate")
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        'strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString


        Try
            If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then


                'objCommon.PageSetup(Form1, "NEW")
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", chkForeignZip.Checked)
                'objCommon.SetBtnState(Form1, "NEW", pObj)
                ViewState("MODE") = "NEW"


                '   build dropdownlists
                BuildDropDownLists()

                '   bind datalist
                BindDataList(StudentId)

                '   bind an empty new AcademicYearInfo
                BindStudentAddressData(New StuAddressInfo)

                'selected index must be the default country if it is defined in configuration file
                ddlCountryId.SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)

                '   initialize buttons
                InitButtonsForLoad()

                'Try
                '    ddlCountryId.SelectedValue = strDefaultCountry
                'Catch ex As System.Exception
                 '	Dim exTracker = new AdvApplicationInsightsInitializer()
                '	exTracker.TrackExceptionWrapper(ex)

                '    ddlCountryId.SelectedIndex = 0
                'End Try

                'Disable Header DD
                'header1.EnableHistoryButton(False)
                '16521: ENH: Galen: FERPA: Compliance Issue 
                'added by Theresa G on May 7th 2010
                'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
                '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
                'End If

                If boolSwitchCampus = True Then
                    CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
                End If

                MyBase.uSearchEntityControlId.Value = ""

            Else

                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", chkForeignZip.Checked)
                'objCommon.PageSetup(Form1, "EDIT")
                'if international is checked then disable validator controls
                'Dim ctl As Control
                'If chkForeignZip.Checked Then
                '    ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
                '    rfv = CType(ctl, RequiredFieldValidator)
                '    rfv.Enabled = False
                'Else
                '    ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
                '    rfv = CType(ctl, RequiredFieldValidator)
                '    rfv.Enabled = True
                'End If
            End If

            GetInputMaskValue()

            'Check If any UDF exists for this resource
            Dim sdfControls As New SDFComponent
            Dim intSdfExists As Integer = sdfControls.GetSDFExists(resourceId, ModuleId)
            If intSdfExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If

            If Trim(txtAddress1.Text) <> "" Then
                sdfControls.GenerateControlsEditSingleCell(pnlSDF, resourceId, txtStdAddressId.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, ModuleId)
            End If

            Trace.Warn("end populate")
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub GetInputMaskValue()
        Dim strZipReq As String
        Dim objCommon As New CommonUtilities

        Try
            'Apply Mask Only If Its Local Zip

            If chkForeignZip.Checked = False Then
                txtZip.Mask = "#####"
                txtZip.DisplayMask = "#####"
                lblZip.ToolTip = "#####"
            Else
                txtZip.Mask = "aaaaaaaaaa"
                txtZip.DisplayMask = ""
                lblZip.ToolTip = ""
            End If

            strZipReq = objCommon.SetRequiredColorMask("Zip")

            'If InStr(strZipReq, "Y") >= 1 Then
            If strZipReq = "Yes" Then
                txtZip.BackColor = Color.FromName("#ffff99")
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in GetInputMaskValue" & ex.Message & " "
            Else
                Session("Error") = "Error in GetInputMaskValue" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BindDataList(ByVal studentId As String)
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind AcademicYears datalist
        dlstStdAddress.DataSource = New DataView((New StdAddressFacade).GetAllStudentAddresses(studentId).Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstStdAddress.DataBind()

    End Sub
    'Private Sub BuildDropDownLists()
    '    BuildStatusDDL()
    '    BuildAddressTypeDDL()
    '    BuildStatusDDL()
    '    BuildCountryDDL()
    'End Sub
    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCountryId, AdvantageDropDownListName.Countries, campusId, True, True, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStateId, AdvantageDropDownListName.States, Nothing, True, True, String.Empty))

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Address Types DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressTypeId, AdvantageDropDownListName.Address_Types, campusId, True, True, String.Empty))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        'selected index must be the default country if it is defined in configuration file
        ddlCountryId.SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)
    End Sub
    'Private Sub BuildCountryDDL()
    '    Dim Country As New PrefixesFacade
    '    With ddlCountryId
    '        .DataTextField = "CountryDescrip"
    '        .DataValueField = "CountryId"
    '        .DataSource = Country.GetAllCountries()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        'selected index must be the default country if it is defined in configuration file
    '        .SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)
    '    End With
    'End Sub
    'Private Sub BuildAddressTypeDDL()
    '    Dim AddressType As New PrefixesFacade
    '    With ddlAddressTypeId
    '        .DataTextField = "AddressDescrip"
    '        .DataValueField = "AddressTypeId"
    '        .DataSource = AddressType.GetAllAddressType()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildAddressStatesDDL()
        'Bind the Status DropDownList
        Dim states As New StatesFacade
        With ddlStateId
            .DataTextField = "StateDescrip"
            .DataValueField = "StateID"
            .DataSource = states.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BindStudentAddressData(ByVal StudentAddress As StuAddressInfo)
        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String
        'Get the mask for phone numbers and zip
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        With StudentAddress
            If .ForeignZip = True Then
                chkForeignZip.Checked = True
            Else
                chkForeignZip.Checked = False
            End If

            'get IsInDB
            chkIsInDB.Checked = .IsInDB
            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            txtStudentId.Text = StudentId
            txtStdAddressId.Text = .StdAddressId

            'Get State
            Try
                'ddlStateId.SelectedValue = .State
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateId, .State, .StateText)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                BuildAddressStatesDDL()
            End Try

            'Get Zip
            txtZip.Text = .Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            End If

            'Get Country 
            'ddlCountryId.SelectedValue = .Country
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountryId, .Country, .CountryText)

            'AddressType
            'ddlAddressTypeId.SelectedValue = .AddressType
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressTypeId, .AddressType, .AddTypeText)

            'AddressStatus
            'ddlStatusId.SelectedValue = .AddressStatus
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .AddressStatus, .AddStatusText)


            'OtherState
            txtOtherState.Text = .OtherState

            'Get Foreign Zip
            If .ForeignZip = True Then
                chkForeignZip.Checked = True
                txtOtherState.Visible = True
                txtOtherState.Enabled = True
                lblOtherState.Visible = True
                ddlStateId.Enabled = False
            Else
                chkForeignZip.Checked = False
                txtOtherState.Visible = False
                txtOtherState.Enabled = False
                lblOtherState.Visible = False
                ddlStateId.Enabled = True
            End If

            If .Default1 = True Then
                chkdefault1.Checked = True
            Else
                chkdefault1.Checked = False
            End If



            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString

        End With
    End Sub
    Private Sub BindStudentAddressExistingData(ByVal StudentAddress As StuAddressInfo)
        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String
        'Get the mask for phone numbers and zip
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        With StudentAddress
            If .ForeignZip = True Then
                chkForeignZip.Checked = True
            Else
                chkForeignZip.Checked = False
            End If

            'get IsInDB
            chkIsInDB.Checked = .IsInDB
            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            txtStudentId.Text = StudentId
            txtStdAddressId.Text = .StdAddressId

            'Get State
            Try
                'ddlStateId.SelectedValue = .State
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateId, .State, .StateText)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                BuildAddressStatesDDL()
            End Try

            'Get Zip
            txtZip.Text = .Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            End If

            'Get Country 
            'ddlCountryId.SelectedValue = .Country
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountryId, .Country, .CountryText)

            'AddressType
            'ddlAddressTypeId.SelectedValue = .AddressType
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressTypeId, .AddressType, .AddTypeText)

            'AddressStatus
            'ddlStatusId.SelectedValue = .AddressStatus
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .AddressStatus, .AddStatusText)


            'OtherState
            txtOtherState.Text = .OtherState

            'Get Foreign Zip
            If .ForeignZip = True Then
                chkForeignZip.Checked = True
                txtOtherState.Visible = True
                txtOtherState.Enabled = True
                lblOtherState.Visible = True
                ddlStateId.Enabled = False
            Else
                chkForeignZip.Checked = False
                txtOtherState.Visible = False
                txtOtherState.Enabled = False
                lblOtherState.Visible = False
                ddlStateId.Enabled = True
            End If

            If .Default1 = True Then
                chkdefault1.Checked = True
            Else
                chkdefault1.Checked = False
            End If

            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString

        End With
    End Sub
    Private Sub dlstStdAddress_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstStdAddress.ItemCommand

        '   get the AcademicYearId from the backend and display it
        GetStudentAddress(e.CommandArgument)
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = resourceId
        Master.setHiddenControlForAudit()
        If chkForeignZip.Checked = True Then
            txtOtherState.Visible = True
            lblOtherState.Visible = True
            ddlStateId.Enabled = False
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateId.Enabled = True
        End If

        Dim ctl As Control
        Dim rfv As New RequiredFieldValidator
        If chkForeignZip.Checked Then
            ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
            If Not (ctl Is Nothing) Then
                rfv = CType(ctl, RequiredFieldValidator)
                rfv.Enabled = False
            End If
            lblStateId.Text = "State"
        Else
            ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
            If Not (ctl Is Nothing) Then
                rfv = CType(ctl, RequiredFieldValidator)
                rfv.Enabled = True
            End If

        End If

        '   set Style to Selected Item
        'Disable Header DD
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStdAddress, e.CommandArgument, ViewState, header1)
        '   initialize buttons
        InitButtonsForEdit()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEditSingleCell(pnlSDF, resourceId, e.CommandArgument, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' US3037 5/4/2012 Janet Robinson
        Dim strId As String = dlstStdAddress.DataKeys(e.Item.ItemIndex).ToString()
        CommonWebUtilities.RestoreItemValues(dlstStdAddress, strId)

    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub GetStudentAddress(ByVal StdAddressId As String)
        '   bind AcademicYear properties
        BindStudentAddressExistingData((New StdAddressFacade).GetStudentAddressInfo(StdAddressId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList(txtStudentId.Text)
        BindStudentAddressData(New StuAddressInfo)
        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, ModuleId)
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlstStdAddress, Guid.Empty.ToString)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim result As String
        Dim errorMessage As String = String.Empty
        Dim facade As New StdAddressFacade
        Dim doesDefaultExist As Integer

        doesDefaultExist = facade.DoesDefaultAddExist(txtStudentId.Text, txtStdAddressId.Text)
        If doesDefaultExist >= 1 And chkdefault1.Checked = True Then
            DisplayErrorMessage("A default address already exists for this student")
            Exit Sub
        End If

        If errorMessage = "" Then
            With New StdAddressFacade
                '   update AcademicYear Info 
                result = .UpdateStudentAddress(BuildStdAddressInfo(txtStdAddressId.Text), AdvantageSession.UserState.UserName)
            End With

            '   bind the datalist
            BindDataList(txtStudentId.Text)

            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get the AcademicYearId from the backend and display it
                GetStudentAddress(txtStdAddressId.Text)
            End If
            '   if there are no errors bind a new entity and init buttons
            If Page.IsValid Then

                '   set the property IsInDB to true in order to avoid an error if the user
                '   hits "save" twice after adding a record.
                chkIsInDB.Checked = True

                'note: in order to display a new page after "save".. uncomment next lines
                '   bind an empty new AcademicYearInfo
                'BindAcademicYearData(New AcademicYearInfo)

                '   initialize buttons
                InitButtonsForEdit()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields When Save Button Is Clicked

                Dim SDFID As ArrayList
                Dim SDFIDValue As ArrayList
                '                Dim newArr As ArrayList
                Dim z As Integer
                Dim SDFControl As New SDFComponent
                Try
                    SDFControl.DeleteSDFValue(txtStdAddressId.Text)
                    SDFID = SDFControl.GetAllLabels(pnlSDF)
                    SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                    For z = 0 To SDFID.Count - 1
                        SDFControl.InsertValues(txtStdAddressId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                    Next
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try

                'SchoolDefined Fields Code Ends Here 
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                ' US3037 5/4/2012 Janet Robinson
                CommonWebUtilities.RestoreItemValues(dlstStdAddress, txtStdAddressId.Text)

            End If
        Else
            DisplayErrorMessageMask(errorMessage)
        End If



    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Function BuildStdAddressInfo(ByVal StdAddressId As String) As StuAddressInfo
        'instantiate class
        Dim StudentAddress As New StuAddressInfo
        Dim facInputMask As New InputMasksFacade
        '        Dim phoneMask As String
        Dim zipMask As String
        '  Dim ssnMask As String

        With StudentAddress
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get StudentMasterID
            .StudentID = txtStudentId.Text

            'get AcademicYearId
            .StdAddressId = StdAddressId

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get City
            .City = txtCity.Text

            'Get State
            .State = ddlStateId.SelectedValue


            'Get Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            Else
                .Zip = txtZip.Text
            End If

            If chkdefault1.Checked = True Then
                .Default1 = 1
            Else
                .Default1 = 0
            End If

            'Get Country 
            .Country = ddlCountryId.SelectedValue

            'Get AddressType
            .AddressType = ddlAddressTypeId.SelectedValue

            'Address Status
            .AddressStatus = ddlStatusId.SelectedValue

            'Foreign Zip
            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)
            .OtherState = txtOtherState.Text
        End With
        'return data
        Return StudentAddress
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        '   bind an empty new AcademicYearInfo
        BindStudentAddressData(New StuAddressInfo)

        'Reset Style in the Datalist
        'Disable Header DD
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStdAddress, Guid.Empty.ToString, ViewState, header1)

        'initialize buttons
        InitButtonsForLoad()

        ''Set Default Country
        'Try
        '    ddlCountryId.SelectedValue = strDefaultCountry
        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    ddlCountryId.SelectedIndex = 0
        'End Try

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, ModuleId)

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstStdAddress, Guid.Empty.ToString)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (txtStdAddressId.Text = Guid.Empty.ToString) Then
            'update AcademicYear Info 
            Dim result As String = (New StdAddressFacade).DeleteStudentAddress(txtStdAddressId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList(txtStudentId.Text)

                '   bind an empty new AcademicYearInfo
                BindStudentAddressData(New StuAddressInfo)

                '   initialize buttons
                InitButtonsForLoad()

                ''Set Default Country
                'Try
                '    ddlCountryId.SelectedValue = strDefaultCountry
                'Catch ex As System.Exception
                 '	Dim exTracker = new AdvApplicationInsightsInitializer()
                '	exTracker.TrackExceptionWrapper(ex)

                '    ddlCountryId.SelectedIndex = 0
                'End Try

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtStdAddressId.Text)
                SDFControl.GenerateControlsNewSingleCell(pnlSDF, resourceId, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If

        End If

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstStdAddress, Guid.Empty.ToString)

    End Sub
    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignZip.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        Dim ctl As Control
        Dim rfv As New RequiredFieldValidator
        CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        If chkForeignZip.Checked = True Then
            txtOtherState.Enabled = True

            txtOtherState.Visible = True
            txtOtherState.Enabled = True
            lblOtherState.Visible = True
            ddlStateId.Enabled = False
            ddlStateId.SelectedIndex = 0
            txtZip.Mask = "aaaaaaaaaa"
            txtZip.DisplayMask = ""
            lblZip.ToolTip = ""
        Else
            txtOtherState.Enabled = False

            txtOtherState.Text = ""
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateId.Enabled = True
            ddlStateId.SelectedIndex = 0
            ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
            If Not (ctl Is Nothing) Then
                rfv = CType(ctl, RequiredFieldValidator)
                rfv.Enabled = True
            End If
            txtZip.Mask = "#####"
            txtZip.DisplayMask = "#####"
            lblZip.ToolTip = "#####"
        End If


    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtOtherState.TextChanged
        Dim objCommon As New CommonWebUtilities
        ' CommonWebUtilities.SetFocus(Me.Page, txtZip)
        txtZip.Focus()
    End Sub
    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New InputMasksFacade
        Dim correctFormat As Boolean
        '        Dim strMask As String
        Dim zipMask As String
        Dim errorMessage As String = ""
        '     Dim ssnMask As String


        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)


        'Validate the zip field format. If the field is empty we should not apply the mask
        'against it.
        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for zip field."
            End If
        End If
        Return errorMessage
    End Function

    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtCity.TextChanged
        If chkForeignZip.Checked = True Then
            Dim objCommon As New CommonWebUtilities
            'CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
            txtOtherState.Focus()
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'controlsToIgnore.Add(chkForeignZip)
        'controlsToIgnore.Add(txtCity)
        'controlsToIgnore.Add(txtOtherState)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
