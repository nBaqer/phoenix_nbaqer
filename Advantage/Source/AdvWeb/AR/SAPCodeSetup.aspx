<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SAPCodeSetup.aspx.vb" Inherits="SAPCodeSetup" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script src="../js/checkall.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>

    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function replace(string, text, by) {
                // Replaces text with by in string
                var strLength = string.length, txtLength = text.length;
                if ((strLength == 0) || (txtLength == 0)) return string;
                var i = string.indexOf(text);
                if ((!i) && (text != string.substring(0, txtLength))) return string;
                if (i == -1) return string;
                var newstr = string.substring(0, i) + by;
                if (i + txtLength < strLength)
                    newstr += replace(string.substring(i + txtLength, strLength), text, by);
                return newstr;
            }
            function OpenSAPDetails() {
                           //alert("Inside func");
                var SAPId = document.forms[0].ContentMain2_txtSAPId.value;
                //alert(SAPId);
                var SAPDesc = replace(document.forms[0].ContentMain2_txtSAPDescrip.value, '#', '\#');
                var CampId = document.forms[0].ContentMain2_txtCampusId.value;
                var TrigUnitTyp = document.forms[0].ContentMain2_ddlTrigUnitTypId.value;
                var TrigOffsetTyp = document.forms[0].ContentMain2_ddlTrigOffsetTypId.value;
                var IsTitleIVSap = false;
                var chkFaSapPolicy = document.getElementById("<%=chkFaSapPolicy.ClientID%>");
                if (chkFaSapPolicy !== undefined && chkFaSapPolicy !== null) {
                    IsTitleIVSap = chkFaSapPolicy.checked;
                }

                //alert(replace(document.forms[0].txtSAPDescrip.value,'#','\#'));
                //alert(SAPDesc);
                //alert('../AR/SAPPolicyDetails.aspx?resid=109&SAPPolicy=' + unescape(escape(SAPDesc)) + '&SAPPolicyId=' + SAPId);
                theChild = window.open('../AR/SAPPolicyDetails.aspx?resid=109&SAPId=' + SAPId + '&SAPPolicy=' + SAPDesc + '&CampusId=' + CampId + '&TrigUnit=' + TrigUnitTyp + '&TrigOffset=' + TrigOffsetTyp + '&IsTitleIVSap=' + IsTitleIVSap, 'SAPPolicyDetails', 'status=yes,resizable=yes,scrollbars=yes,width=900px,height=570px');
            }
        </script>

    </telerik:RadScriptBlock>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap="nowrap" align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap="nowrap">
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstSAP" runat="server" DataKeyField="SAPId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("SAPId")%>' Text='<%# container.dataitem("SAPDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" id="Table2" style="width: 80%; text-align: left">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblSAPCode" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtSAPCode" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblSAPDescrip" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtSAPDescrip" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTrigUnitTypId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlTrigUnitTypId" runat="server" CssClass="dropdownlist" AutoPostBack="True" OnSelectedIndexChanged="ddlTrigUnitTypId_SelectedIndexChanged"></asp:DropDownList>

                                            </td>
                                            <asp:CompareValidator ID="Comparevalidator5" runat="server" ErrorMessage="Trigger Unit is required"
                                                Display="None" ControlToValidate="ddlTrigUnitTypId" Operator="NotEqual" ValueToCompare="0">Trigger Unit is required</asp:CompareValidator>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTrigOffsetTypId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlTrigOffsetTypId" runat="server" CssClass="dropdownlist" AutoPostBack="true"></asp:DropDownList></td>
                                            <asp:CompareValidator ID="Comparevalidator6" runat="server" ErrorMessage="Trigger Offset Type is required"
                                                Display="None" ControlToValidate="ddlTrigOffsetTypId" Operator="NotEqual" ValueToCompare="0">Trigger Offset Type is required</asp:CompareValidator>

                                        </tr>

                                        <tr>
                                            <td class="contentcell" style="white-space: normal">&nbsp;
                                            </td>
                                            <td class="contentcell4" style="white-space: normal; width: 68%; text-align: left">
                                                <asp:CheckBox ID="chkFaSapPolicy" runat="server" CssClass="checkbox"
                                                    Text="Title IV SAP Policy" Visible="False" AutoPostBack="True" />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="white-space: normal">&nbsp;</td>
                                            <td class="contentcell4" style="width: 68%; text-align: left">
                                                <asp:CheckBox ID="chkPayOnProbation" runat="server" CssClass="checkbox"
                                                    Text="Pay on Probation" Visible="False" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblMinTermGPA" runat="server" CssClass="label" Visible="False"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtMinTermGPA" runat="server" CssClass="textbox" MaxLength="128" Text="0" Visible="False"></asp:TextBox>

                                                <asp:CompareValidator ID="Comparevalidator1" runat="Server" ErrorMessage="Only Numeric Values Allowed For Minimum Term GPA!"
                                                    Display="None" Type="Double" Operator="DataTypeCheck" ControlToValidate="txtMinTermGPA"></asp:CompareValidator>
                                                <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Negative Values Not Allowed"
                                                    Display="None" ControlToValidate="txtMinTermGPA" OnServerValidate="TextValidate"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTermGPAOver" runat="server" CssClass="label" Visible="False"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTermGPAOver" runat="server" CssClass="textbox" MaxLength="128" Text="0" Visible="False"></asp:TextBox>

                                                <asp:CompareValidator ID="Comparevalidator3" runat="Server" ErrorMessage="Only Numeric Values Allowed For Term GPA Override!"
                                                    Display="None" Type="Double" Operator="DataTypeCheck" ControlToValidate="txtTermGPAOver" ShowMessageBox="true"></asp:CompareValidator>
                                                <asp:CustomValidator ID="Customvalidator15" runat="server" ErrorMessage="Negative Values Not Allowed"
                                                    Display="None" ControlToValidate="txtTermGPAOver" OnServerValidate="TextValidate"></asp:CustomValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTermAvOver" runat="server" CssClass="label" Visible="False"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTermAvOver" runat="server" CssClass="textbox" Text="0" MaxLength="128" Visible="False"></asp:TextBox>
                                                <asp:CompareValidator ID="Comparevalidator4" runat="Server" ErrorMessage="Only Numeric Values Allowed For Term Average Override!"
                                                    Display="None" Type="Double" Operator="DataTypeCheck" ControlToValidate="txtTermAvOver" ShowMessageBox="true"></asp:CompareValidator>
                                                <asp:CustomValidator ID="Customvalidator4" runat="server" ErrorMessage="Negative Values Not Allowed"
                                                    Display="None" ControlToValidate="txtTermAvOver" OnServerValidate="TextValidate"></asp:CustomValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblMinTermAv" runat="server" CssClass="label" Visible="False"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtMinTermAv" runat="server" CssClass="textbox" Text="0" MaxLength="128" Visible="False"></asp:TextBox>
                                                <asp:CompareValidator ID="Comparevalidator2" runat="Server" ErrorMessage="Only Numeric Values Allowed For Minimum Term Average!"
                                                    Display="None" Type="Double" Operator="DataTypeCheck" ControlToValidate="txtMinTermAv" ShowMessageBox="true"></asp:CompareValidator>
                                                <asp:CustomValidator ID="Customvalidator3" runat="server" ErrorMessage="Negative Values Not Allowed"
                                                    Display="None" ControlToValidate="txtMinTermAv" OnServerValidate="TextValidate"></asp:CustomValidator></td>
                                        </tr>

                                        <tr>
                                            <td class="contentcell" style="white-space: normal">
                                                <asp:Label ID="lblTerminate" runat="server" CssClass="label">Number of Probation(s) for Termination</asp:Label></td>
                                            <td class="contentcell4" style="width: 68%; text-align: left">
                                                <asp:TextBox ID="txtTerminateProbCnt" runat="server" CssClass="textbox" MaxLength="1"></asp:TextBox>
                                                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtTerminateProbCnt"
                                                    Display="None" ErrorMessage="Only Numeric Values Allowed For Termination Probation Count"
                                                    Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="contentcell" style="white-space: normal">&nbsp;
                                            </td>
                                            <td class="contentcell4" style="width: 68%; text-align: left">
                                                <asp:CheckBox ID="chkTrackExtern" runat="server" Visible="True" Text="Include Externship Attendance" CssClass="checkbox"></asp:CheckBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="contentcell" style="white-space: normal">&nbsp;</td>
                                            <td class="contentcell4" style="width: 68%; text-align: left">
                                                <asp:CheckBox ID="chkTransferHours" runat="server" CssClass="checkbox"
                                                    Text="Trigger Includes Transfer Hours" Visible="False" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4" nowrap="nowrap" style="width: 68%">
                                                <%--<input type="button" value="View SAP Policiy Details"/>--%>
                                                <%--<asp:LinkButton ID="Linkbutton2" runat="server" CssClass="label" >Details</asp:LinkButton>--%>

                                                <asp:Button runat="server" ID="btnViewSapDetails" Text="View SAP Policy Details" OnClientClick="OpenSAPDetails();return false;" />
                                            </td>
                                        </tr>
                                    </table>
                                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
                                    </telerik:RadAjaxLoadingPanel>
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                        <asp:Panel runat="server">
                                            <table class="contenttable" id="SapVerbiageContainer" style="width: 80%; text-align: left" runat="server" visible="False">
                                                <tr style="margin: 5px;">
                                                    <td colspan="10">
                                                        <h4>Title IV SAP Notice Status Messages</h4>
                                                        <p>These are the default messages that will appear for each corresponding status on the Title IV SAP Notice report.</p>
                                                        <p>The default value of these messages can be overwritten by replacing the text in the text boxes with the desired information.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="PassedLabel" runat="server" CssClass="label">Passed</asp:Label></td>
                                                    <td class="contentcell4">
                                                        <asp:TextBox ID="PassedTextBox" runat="server" TextMode="MultiLine" Wrap="True" Width="382px" Height="132px"></asp:TextBox>
                                                        <asp:ImageButton runat="server" ImageUrl="~/images/icon/undo.gif" CssClass="no-border" CausesValidation="false" ToolTip="Reset to Default" ID="ResetPassedButton" />


                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="WarningLabel" runat="server" CssClass="label">Warning</asp:Label></td>
                                                    <td class="contentcell4">
                                                        <asp:TextBox ID="WarningTextBox" runat="server" TextMode="MultiLine" Wrap="True" Width="382px" Height="132px"></asp:TextBox>

                                                        <asp:ImageButton runat="server" ImageUrl="~/images/icon/undo.gif" CssClass="no-border" CausesValidation="false" ToolTip="Reset to Default" ID="ResetWarningButton" />

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="IneligibleLabel" runat="server" CssClass="label">Ineligible</asp:Label></td>
                                                    <td class="contentcell4">
                                                        <asp:TextBox ID="IneligibleTextBox" runat="server" TextMode="MultiLine" Wrap="True" Width="382px" Height="132px"></asp:TextBox>
                                                        <asp:ImageButton runat="server" ImageUrl="~/images/icon/undo.gif" CssClass="no-border" CausesValidation="false" ToolTip="Reset to Default" ID="ResetIneligible" />

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="ProbationLabel" runat="server" CssClass="label">Probation</asp:Label></td>
                                                    <td class="contentcell4">
                                                        <asp:TextBox ID="ProbationTextBox" runat="server" TextMode="MultiLine" Wrap="True" Width="382px" Height="132px"></asp:TextBox>
                                                        <asp:ImageButton runat="server" ImageUrl="~/images/icon/undo.gif" CssClass="no-border" CausesValidation="false" ToolTip="Reset to Default" ID="ResetProbation" />
                                                    </td>

                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </telerik:RadAjaxPanel>
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                    <!--end table content-->
                                </div>

                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:TextBox ID="txtSAPId" CssClass="textbox" runat="server" MaxLength="128" Style="visibility: hidden"></asp:TextBox>
        <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
        <asp:TextBox ID="txtCampusId" CssClass="textbox" runat="server" MaxLength="128" Style="visibility: hidden"></asp:TextBox>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server" Width="50px"></asp:Panel>
        &nbsp;&nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    </div>

</asp:Content>
