﻿Imports System.Diagnostics
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports System.Collections
Imports Fame.Advantage.Common
Imports System.Text
Imports System.Threading.Tasks
Imports Fame.Advantage.Api.Library.AcademicRecords
Imports Fame.Advantage.Api.Library.Models
Imports Fame.Advantage.Api.Library.Models.Common
Imports Advantage.AFA.Integration

Partial Class StudentAttendance
    Inherits BasePage

#Region "Variables"

    Protected State As AdvantageSessionState
    Protected LeadId As String
    'Dim resourceId As Integer
    Protected WithEvents Btnhistory As Button
    Protected StudentId As String
    Protected StudentName As String
    Private pObj As New UserPagePermissionInfo
    Protected CampusId, Userid As String
    Protected BoolSwitchCampus As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings
    Private ppopulateAttendanceGrid As String = String.Empty

#End Region

#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

    Private Function GetStudentFromStateObject() As StudentMRU

        Dim objStudentState As New StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            BoolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function

#End Region

#Region " Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#End Region

#Region "Page Load"
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnBuildList)
        controlsToIgnore.Add(ddlStuEnrollId)
        controlsToIgnore.Add(ddlTermId)
        controlsToIgnore.Add(ddlClsSectionId)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip("ddlStuEnrollId")
        BindToolTip("ddlTermId")
        BindToolTip("ddlClsSectionId")

    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        '  btnBuildList.Attributes.Add("onclick", "SetHiddenText();return true;")

        MyAdvAppSettings = AdvAppSettings.GetAppSettings()

        btnBuildList.Attributes.Add("onclick", "return true;")

        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        CampusId = Master.CurrentCampusId
        Userid = AdvantageSession.UserState.UserId.ToString

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU
        objStudentState = GetStudentFromStateObject() 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        Dim sef As New StuEnrollFacade

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
            'StudentName = .Name
            StudentName = sef.StudentName(StudentId)
            'If BoolSwitchCampus = True Then
            '    CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            'End If
        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        Dim advantageUserState As User = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), CampusId)
        'pObj = Header1.UserPagePermission

        CampusId = Master.CurrentCampusId

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            ViewState("MODE") = "NEW"
            ViewState("isRegistrationTypeByProgram") = False

            If Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value) Then
                pnlAttendance.Controls.Clear()
                ddlStuEnrollId.Items.Clear()
                ddlTermId.Items.Clear()
                ddlClsSectionId.Items.Clear()
                ClearTotals()
                tblOverallTotals.Visible = False
            End If

            '   build drop down lists
            BuildDropDownLists()

            '   initialize buttons
            InitButtonsForLoad()

            'Set the Session("StudentOK") to nothing. This is very important to fix the following issue.
            'If the user leaves the page and then returns to it without closing the browser the
            'Session("StudentOK") variable will still be populated and so the first test in the else
            'clause below will pass and there will be an attempt to build the attendance table when
            'a term is selected. This will cause an error.
            ViewState("StudentOK") = Nothing
            'Session("StudentOK") = Nothing

            If Me.Master.IsSwitchedCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If



            'If BoolSwitchCampus = True Then
            '    CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            'End If

            MyBase.uSearchEntityControlId.Value = ""


        Else
            'objCommon.PageSetup(Form1, "EDIT")
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            If Not ViewState("StudentOK") Is Nothing Then
                'If Not Session("StudentOK") Is Nothing Then
                If HiddenText.Text <> "YES" Then
                    If Not DirectCast(ViewState("MeetDays"), DataTable) Is Nothing Then
                        BuildAttendanceGrid()
                    End If
                Else
                    HiddenText.Text = ""
                End If
            Else
                If HiddenText.Text = "YES" Then
                    HiddenText.Text = ""
                End If
            End If
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

#End Region

#Region "Binding DDL"
    Private Sub BuildDropDownLists()
        BuildStuEnrollDDL()
    End Sub
    Private Sub BuildStuEnrollDDL()
        '   bind the StuEnrollment DDL
        Dim transcript As New TranscriptFacade
        With ddlStuEnrollId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = transcript.GetEnrollment(StudentId.ToUpper, Master.CurrentCampusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildTermDDL(ByVal stuEnrollId As String)
        '   bind the Terms DDL
        Dim facClsSectAtt As New ClsSectAttendanceFacade
        Dim ds As DataSet

        ds = facClsSectAtt.GetTermsStudentIsRegisteredFor(stuEnrollId)

        '   Bind Terms DDL
        With ddlTermId
            .DataSource = ds.Tables("Terms")
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataBind()
            If .Items.Count > 0 Then
                If MyAdvAppSettings.AppSettings("UseImportedAttendance", CampusId).ToLower = "true" And SelectByPartOfTheValue(ddlTermId, "Historical") Then

                Else
                    .Items.Insert(0, New ListItem("All", Guid.Empty.ToString))
                    .SelectedIndex = 0
                End If
            End If
        End With


        If ds.Tables("FutureStart").Rows.Count > 0 Then
            chkFutureStart.Checked = CType(ds.Tables("FutureStart").Rows(0)("IsFutureStart"), Boolean)
        Else
            chkFutureStart.Checked = False
        End If


        Dim strTermList As String = facClsSectAtt.BuildTermList(ds)
        ViewState("AllTerms") = strTermList
    End Sub
    Private Sub BindClassSectionDDL(ByVal dtClsSections As DataTable)
        If dtClsSections.Rows.Count = 0 Then
            '   Display Error Message
            tblLegend.Visible = False
            CommonWebUtilities.DisplayInfoInMessageBox(Page, "Student is not registered for any class section.")
            ViewState("ClsSections") = Nothing
            ddlClsSectionId.Items.Clear()
            '
        Else
            '   bind class sections DDL
            With ddlClsSectionId
                .DataSource = dtClsSections
                .DataValueField = "ClsSectionId"
                .DataTextField = "Class"
                .DataBind()
                .Items.Insert(0, New ListItem("All", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
            '
            ViewState("ClsSections") = dtClsSections
        End If
    End Sub
#End Region

#Region "DDL selected index changed"

    Private Sub ClearTotals()
        LblActualPresent.Text = ""
        LblActualAbsent.Text = ""
        LblScheduled.Text = ""
        lblScheduledORTardy.Text = ""
        lblActualExcused.Text = ""
        LblAdjPresent.Text = ""
        LblAdjAbsent.Text = ""
        LblAdjScheduled.Text = ""
        lblAdjScheduledORTardy.Text = ""
        lblAdjExcused.Text = ""
        lblOverallAttendance.Text = ""
        lblTardy.Text = ""
    End Sub

    Private Sub ddlTermId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTermId.SelectedIndexChanged
        Dim strTermId As String
        Dim dtClsSections As DataTable

        ViewState("StudentOK") = Nothing

        If ddlStuEnrollId.SelectedValue <> Guid.Empty.ToString And
        ddlTermId.SelectedValue <> Guid.Empty.ToString Then

            strTermId = "'" & ddlTermId.SelectedItem.Value & "'"

        Else

            '   Retrieve all terms in order to get all class sections
            strTermId = CType(ViewState("AllTerms"), String)

        End If

        '   Bring class sections for the selected term
        dtClsSections = (New ClsSectAttendanceFacade).GetClsSectionsForStudent(ddlStuEnrollId.SelectedItem.Value, strTermId, txtPrgVerTrackAttendance.Text)

        '   Bind Class Sections DDL
        BindClassSectionDDL(dtClsSections)
        tblOverallTotals.Visible = False
        tblLegend.Visible = False
        '   Clear Attendance grid
        DeleteRowsFromAttendanceGrid()
        '   Clear the instructions
        'lblMessage.Text = ""
    End Sub
    Private Sub ddlStuEnrollId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStuEnrollId.SelectedIndexChanged
        ViewState("StudentOK") = Nothing
        ViewState("AllTerms") = Nothing
        ViewState("ClsSections") = Nothing

        Dim facClsSectAtt As New ClsSectAttendanceFacade
        '   Put in hidden textbox if Program Version tracks attendance to use it later
        txtPrgVerTrackAttendance.Text = CType(facClsSectAtt.DoesPrgVersionTrackAttendance(ddlStuEnrollId.SelectedValue, False), String)

        If ddlStuEnrollId.SelectedValue = Guid.Empty.ToString Then
            '   Clear all DDLs
            ddlTermId.Items.Clear()
            ddlClsSectionId.Items.Clear()
        Else
            '   Populate the Terms DDL
            BuildTermDDL(ddlStuEnrollId.SelectedValue)
            'ddlClsSectionId.Items.Clear()
            Dim dtClsSections As DataTable
            Dim strTermList As String = CType(ViewState("AllTerms"), String)
            '   Bring class sections for all terms
            dtClsSections = facClsSectAtt.GetClsSectionsForStudent(ddlStuEnrollId.SelectedItem.Value, strTermList, CType(txtPrgVerTrackAttendance.Text, Boolean))
            '  Bind Class Sections DDL
            BindClassSectionDDL(dtClsSections)
        End If
        tblOverallTotals.Visible = False
        tblLegend.Visible = False
        '   Clear out the attendance grid
        DeleteRowsFromAttendanceGrid()

        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim programVersionRequest As New ProgramVersionRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Dim registrationType As Enums.ProgramRegistrationType? = programVersionRequest.GetRegistrationType(Guid.Parse(ddlStuEnrollId.SelectedItem.Value))
            If (Not registrationType Is Nothing) Then
                If (registrationType = Enums.ProgramRegistrationType.ByProgram) Then
                    'isRegistrationTypeByProgram = True

                    lblTerms.Visible = False
                    ddlTermId.Visible = False
                    'lblClsSections.Visible = False
                    'ddlClsSectionId.Visible = False
                    btnBuildList.Visible = True
                    ViewState("isRegistrationTypeByProgram") = True
                    btnBuildList_Click(sender, e)

                End If
            End If
        End If
    End Sub
#End Region

#Region "Buttons Click"
    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
        ClearTotals()
        Dim endDate As DateTime
        'Dim tbl As New Table
        Dim coll As New Collection
        Dim arrDates As ArrayList
        Dim dtMeetDays As DataTable
        Dim dtClsSections As DataTable
        Dim dsDropAndLoaInfo As DataSet
        Dim dtActualAttendace As DataTable
        Dim facAttendance As New ClsSectAttendanceFacade


        tblLegend.Visible = True
        If ddlStuEnrollId.SelectedValue = Guid.Empty.ToString Then
            '   Display Error Message
            '       Enrollment is the only required filter because both the Term DDL 
            '       and the ClassSections DDL have the default entry of "All" in position 0.
            tblOverallTotals.Visible = False
            tblLegend.Visible = False

            CommonWebUtilities.DisplayWarningInMessageBox(Page, "Please select an Enrollment.")
            Exit Sub

        ElseIf ddlStuEnrollId.SelectedValue <> Guid.Empty.ToString And
        ddlTermId.Items.Count = 0 And ddlClsSectionId.Items.Count = 0 Then
            '   Display Error Message
            '       The Terms DDL and the ClassSections DDL are empty
            tblOverallTotals.Visible = False
            tblLegend.Visible = False
            CommonWebUtilities.DisplayInfoInMessageBox(Page, "Student is not registered for any class section.")
            Exit Sub
        ElseIf ddlStuEnrollId.SelectedValue <> Guid.Empty.ToString And
        ddlTermId.Items.Count > 0 And ddlClsSectionId.Items.Count = 0 Then
            '   Display Error Message
            '       The Terms DDL and the ClassSections DDL are empty
            tblOverallTotals.Visible = False
            tblLegend.Visible = False
            CommonWebUtilities.DisplayInfoInMessageBox(Page, "Student is not registered for any class section.")
            Exit Sub
        End If


        Try
            'Get datatable with LOA info for student
            If MyAdvAppSettings.AppSettings("UseImportedAttendance", CampusId).ToLower = "true" And (ddlTermId.SelectedItem.Text.Contains("Historical")) Then
                ' tHIS IS APPARENTLY FOR HISTORICAL VALUES....
                ViewState("AttendanceUnitType") = facAttendance.GetPrgVersionAttendanceType(ddlStuEnrollId.SelectedItem.Value, False)
            Else
                dsDropAndLoaInfo = facAttendance.GetDropAndLOAInfoForStudent(ddlStuEnrollId.SelectedItem.Value)
                ViewState("DropAndLOAInfoDS") = dsDropAndLoaInfo

                Dim strTermId As String
                If ddlTermId.SelectedValue = Guid.Empty.ToString Then
                    strTermId = CType(ViewState("AllTerms"), String)

                Else
                    strTermId = "'" & ddlTermId.SelectedValue & "'"
                End If

                'Get datatable with days of class section meetings
                If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                    If ddlClsSectionId.SelectedValue = Guid.Empty.ToString Then
                        dtMeetDays = facAttendance.GetClsSectionMeetingDays(ddlStuEnrollId.SelectedItem.Value, strTermId, False)
                    Else
                        dtMeetDays = facAttendance.GetClsSectionMeetingDays(ddlClsSectionId.SelectedItem.Value, False)
                    End If
                Else
                    If ddlClsSectionId.SelectedValue = Guid.Empty.ToString Then
                        dtMeetDays = facAttendance.GetClsSectionMeetingDays(ddlStuEnrollId.SelectedItem.Value, strTermId, True)
                    Else
                        dtMeetDays = facAttendance.GetClsSectionMeetingDays(ddlClsSectionId.SelectedItem.Value, True)
                    End If
                End If

                Dim isRegistrationTypeByProgram As Boolean = False
                If (Not ViewState("isRegistrationTypeByProgram") Is Nothing) Then
                    isRegistrationTypeByProgram = CType(ViewState("isRegistrationTypeByProgram"), Boolean)
                End If

                If dtMeetDays.Rows.Count = 0 And isRegistrationTypeByProgram = False Then
                    ' Error message: No meeting information for selected class section.
                    If ddlClsSectionId.SelectedItem.Value = Guid.Empty.ToString Then
                        CommonWebUtilities.DisplayInfoInMessageBox(Page, "Class sections have not been scheduled.")
                    Else
                        CommonWebUtilities.DisplayInfoInMessageBox(Page, "Class section has not been scheduled.")
                    End If
                    Exit Sub
                End If

                ViewState("MeetDays") = dtMeetDays
                Session("MeetDays") = dtMeetDays

                'Get student class section attendance postings
                dtActualAttendace = facAttendance.GetStudentAttendance(ddlStuEnrollId.SelectedItem.Value, ddlClsSectionId.SelectedItem.Value, strTermId)
                ViewState("ActualAttendance") = dtActualAttendace
                Session("ActualAttendance") = dtActualAttendace

                dtClsSections = DirectCast(ViewState("ClsSections"), DataTable)
                ViewState("AttendanceUnitType") = Nothing

                ' When the Program Version Registration type is set to By Program, the class section and terms are automatically filed with some dummy data to bypass some of the restration
                ' because of that, there are not proper start/end date for the class sections which trigger issues when trying to load meet days for the class section.

                If (Not isRegistrationTypeByProgram) Then
                    If ddlClsSectionId.SelectedItem.Value = Guid.Empty.ToString Then
                        'All class sections
                        'Get meet days for all class sections from start date to today's date
                        For Each dr As DataRow In dtClsSections.Rows
                            Dim rows() As DataRow = dtClsSections.Select("ClsSectionId = '" & dr("ClsSectionId").ToString & "'")

                            If rows.GetLength(0) = 1 Then
                                endDate = CType(rows(0)("EndDate"), Date)
                                If endDate > Date.Today Then endDate = Date.Today
                                'If facAttendance.IsTimeIntervalClsSection(dr("ClsSectionId").ToString) Then
                                If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                                    arrDates = facAttendance.GetMeetDatesArrayListFromRange(dtMeetDays, dr("ClsSectionId").ToString, rows(0)("StartDate").ToString, endDate, False, CampusId)        'Date.Today
                                Else
                                    'this change makes the program work with the new arClsSectMeetings table. Anatoly 7/12/2006
                                    arrDates = facAttendance.GetMeetDatesArrayListFromRange_New(dr("ClsSectionId").ToString, rows(0)("StartDate").ToString, endDate, False, CampusId)        'Date.Today
                                End If
                                If arrDates.Count > 0 Then
                                    coll.Add(arrDates, dr("ClsSectionId").ToString)
                                End If
                                'Place the attendance unit type in view state so it can be reused on post-backs
                                'Get attendance unit type just once
                                If txtPrgVerTrackAttendance.Text = "True" Then
                                    If ViewState("AttendanceUnitType") Is Nothing Then ViewState("AttendanceUnitType") = facAttendance.GetPrgVersionAttendanceType(ddlStuEnrollId.SelectedItem.Value, False)
                                Else
                                    If ViewState("AttendanceUnitType") Is Nothing Then ViewState("AttendanceUnitType") = facAttendance.GetClassSectionAttendanceType(dr("ClsSectionId").ToString)
                                End If
                            End If

                        Next
                        If coll.Count > 0 Then
                            ViewState("Dates") = coll
                            'Add the date collection to session so it can be used the print attendance page
                            Session("Dates") = coll
                        Else
                            ' Error message: Selected class section's start Date is greater than today's date.
                            CommonWebUtilities.DisplayInfoInMessageBox(Page, "Class sections for the selected term have not started.")
                            Exit Sub
                        End If

                    Else
                        'One class section
                        'Get meet days from class section's start date to today's date
                        Dim rows() As DataRow = dtClsSections.Select("ClsSectionId = '" & ddlClsSectionId.SelectedItem.Value & "'")

                        If rows.GetLength(0) = 1 Then
                            endDate = CType(rows(0)("EndDate"), Date)
                            If endDate > Date.Today Then endDate = Date.Today

                            'If facAttendance.IsTimeIntervalClsSection(ddlClsSectionId.SelectedItem.Value) Then
                            If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                                arrDates = facAttendance.GetMeetDatesArrayListFromRange(dtMeetDays, ddlClsSectionId.SelectedItem.Value, rows(0)("StartDate").ToString, endDate, False, CampusId)        'Date.Today
                                'arrDates = facAttendance.GetMeetDatesArrayListFromRange_New(ddlClsSectionId.SelectedItem.Value, rows(0)("StartDate").ToString, endDate, False, campusid)     'Date.Today
                            Else
                                'this change makes the program work with the new arClsSectMeetings table. Anatoly 7/12/2006
                                arrDates = facAttendance.GetMeetDatesArrayListFromRange_New(ddlClsSectionId.SelectedItem.Value, rows(0)("StartDate").ToString, endDate, False, CampusId)     'Date.Today
                            End If
                            If arrDates.Count > 0 Then
                                coll.Add(arrDates, ddlClsSectionId.SelectedItem.Value)
                                ViewState("Dates") = coll

                            Else
                                ' Error message: Selected class section's start Date is greater than today's date.
                                tblLegend.Visible = False
                                CommonWebUtilities.DisplayInfoInMessageBox(Page, ddlClsSectionId.SelectedItem.Text & " has not started.")
                                Exit Sub
                            End If
                        End If
                    End If
                End If

                'Place the attendance unit type in view state so it can be reused on post backs
                If txtPrgVerTrackAttendance.Text = "True" Then
                    ViewState("AttendanceUnitType") = facAttendance.GetPrgVersionAttendanceType(ddlStuEnrollId.SelectedItem.Value, False)
                Else
                    ViewState("AttendanceUnitType") = facAttendance.GetClassSectionAttendanceType(ddlClsSectionId.SelectedItem.Value)
                End If

                'Add the date collection to session so it can be used the print attendance page
                Session("Dates") = coll
            End If

            'end section ...............................................................

            lblMessage.Text = "<h3>" & "Overall Attendance Totals" & "</h3>"
            If ViewState("AttendanceUnitType") = "" Then
                Dim msg As String = ddlStuEnrollId.SelectedItem.Text & " does not tracks attendance"
                If ddlClsSectionId.SelectedItem.Value = Guid.Empty.ToString Then
                    msg &= " nor any of the courses the student has registered."
                Else
                    msg &= " nor the " & ddlClsSectionId.SelectedItem.Text & " class section."
                End If
                tblLegend.Visible = False
                DisplayErrorMessage(msg)
                Exit Sub
            End If
            Dim strAttType As String = " "

            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                strAttType = " Minutes"
            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                strAttType = " Hours"
            End If
            Dim overallPercent As AttendancePercentageInfo
            If txtPrgVerTrackAttendance.Text = "True" Then
                overallPercent = facAttendance.GetOverallPercentageAttendanceInfo(ddlStuEnrollId.SelectedItem.Value, CampusId)
                tblOverallTotals.Visible = True
                BuildTotalsTable(overallPercent, strAttType)
            Else
                tblOverallTotals.Visible = False
            End If
            If MyAdvAppSettings.AppSettings("UseImportedAttendance", CampusId).ToLower = "true" And (ddlTermId.SelectedItem.Text.Contains("Historical")) Then

            Else
                BuildAttendanceGrid()
                PopulateAttendanceGrid()
            End If


            'enable the save button if the user has permission
            If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
                btnSave.Enabled = True
            End If

            '   everything when fine
            ViewState("StudentOK") = True

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            '   Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnBuildList_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnBuildList_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        SaveAttendance()
    End Sub


    Private Sub SaveAttendance()

        'We want to loop through the table rows collection.
        'For each student we will examine each date. We will check the ActualAttendance dt in session
        'to see if the date exists there for the student.
        'If the date has an actual amount then if it exists in the dt with a different value we will
        'need to do an update. If it exists in the dt with the same value then we will not do anything.
        'If the date has an actual amount but does not exists in the dt then we will need to do an insert.
        'If the date does not have an actual amount but exists in the dt then we will need to delete that
        'entry from the database. If the date does not have an actual amount and does not exists in the dt
        'then we will not need to do any thing.
        Dim blnExcused As Boolean
        Dim blnTardy As Boolean
        Dim translatedActual As Decimal
        Dim rowCounter As Integer
        Dim cellCounter As Integer
        Dim actualAtt As String
        Dim clsMtgId As String
        Dim actTardy As Boolean = False
        Dim stuId As String = ddlStuEnrollId.SelectedItem.Value
        Dim attDate As DateTime
        Dim tbl As Table
        Dim ctrl As Control
        Dim arrRows() As DataRow
        Dim dtMeetDays As DataTable
        Dim dtClsSections As DataTable
        Dim dtActualAttendace As DataTable
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim scheduled As Decimal
        Dim drDays() As DataRow
        Dim dtClsSectionMeets As DataTable
        Dim sTime As String
        Dim invalidFieldsCount As Integer = 0
        Dim attendanceUpdated As Boolean = False

        dtMeetDays = DirectCast(ViewState("MeetDays"), DataTable)
        dtClsSections = DirectCast(ViewState("ClsSections"), DataTable)


        For Each dr As DataRow In dtClsSections.Rows


            drDays = dtMeetDays.Select("ClsSectionId = '" & dr("ClsSectionId").ToString & "'")
            If drDays.GetLength(0) > 0 Then


                dtClsSectionMeets = drDays.CopyToDataTable()
                dtClsSectionMeets = dtClsSectionMeets.DefaultView.ToTable(True, "PeriodDescrip", "InstructionTypeDescrip", "ClsSectMeetingId", "UseTimeClock", "StartDate", "EndDate", "TimeIntervalDescrip")



                For idx As Integer = 0 To dtClsSectionMeets.Rows.Count - 1

                    If idx = 0 Then
                        ctrl = pnlAttendance.FindControl("tbl" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                        sTime = CDate(dtClsSectionMeets.Rows(idx)("TimeIntervalDescrip")).TimeOfDay.ToString

                        tbl = DirectCast(ctrl, Table)

                        If Not (tbl Is Nothing) Then
                            'rowCounter = 1
                            'Dim arrDates As ArrayList = DateColl.Item(dr("ClsSectionId").ToString)
                            Dim arrDates As ArrayList
                            If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                                arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                            Else
                                arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                            End If
                            clsMtgId = dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                            'We start at the second row since the first row is the header
                            Dim rCnt As Integer = tbl.Rows.Count - 1

                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then rCnt = tbl.Rows.Count - 2
                            For rowCounter = 1 To rCnt
                                'Start at the second cell because the first stores the names of students
                                For cellCounter = 1 To arrDates.Count

                                    'clear borders from red
                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Remove("border")

                                    attDate = arrDates.Item(cellCounter - 1)
                                    If attDate.TimeOfDay.Hours = 0 Then attDate = CDate(attDate & " " & sTime)
                                    If DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text <> "" Then
                                        actualAtt = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text

                                        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                            actTardy = DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Checked
                                        End If
                                        'Check if the row exists in the ActualAttendance dt
                                        arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate & "# AND ClsSectionId = '" & dr("ClsSectionId").ToString & "'and ClsSectMeetingId = '" & clsMtgId & "'")

                                        If arrRows.Length = 0 Then
                                            arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate.Date & "# AND ClsSectionId = '" & dr("ClsSectionId").ToString & "'and ClsSectMeetingId = '" & clsMtgId & "'")
                                        End If

                                        If arrRows.Length > 0 Then
                                            'If the actual in the table does not match the actual in the dt then
                                            'we will need to do an update if the actual in the table is not an empty string.
                                            'If the actual in the table does not match the actual in the dt then
                                            'we will need to do a delete if the actual in the table is an empty string.
                                            Dim dtActual As String
                                            Dim isTardy As Boolean
                                            Dim isExcused As Boolean

                                            dtActual = arrRows(0)("Actual").ToString()
                                            isTardy = arrRows(0)("Tardy")
                                            isExcused = arrRows(0)("Excused")
                                            'blnTardy = False
                                            'blnExcused = False


                                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                                'If arrRows(0)("Actual").ToString() <> actualAtt Then
                                                'Do an update
                                                facAttendance.UpdateStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, actualAtt, actTardy, 0, clsMtgId)
                                                'Response.Write("Row:" & rowCounter & " Cell:" & cellCounter & "Old:" & arrRows(0)("Actual").ToString() & " New:" & actualAtt & "<br>")
                                                'End If
                                                attendanceUpdated = True
                                            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                                'If arrRows(0)("Actual").ToString() <> actualAtt Then
                                                'Do an update
                                                facAttendance.UpdateStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, actualAtt * 60, actTardy, 0, clsMtgId)
                                                attendanceUpdated = True

                                                'Response.Write("Row:" & rowCounter & " Cell:" & cellCounter & "Old:" & arrRows(0)("Actual").ToString() & " New:" & actualAtt & "<br>")
                                                'End If
                                            Else
                                                '-------------------DE8711: NZ 11/13/12 ---------------------------------------------------------------
                                                '11/19/12 NZ find scheduled classes 
                                                scheduled = DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(2), HiddenField).Value
                                                If scheduled > 1 Then scheduled = 1
                                                If IsValidValue(actualAtt.ToUpper, {"T", "P", "A", "E"}, False) = True Then
                                                    If scheduled > 0 OrElse actualAtt.ToUpper = "P" Then
                                                        translatedActual = facAttendance.TranslatePALetterToQuantity(actualAtt)

                                                        blnTardy = facAttendance.TranslatePALetterToTardyBoolean(actualAtt)
                                                        blnExcused = facAttendance.TranslatePALetterToExcusedBoolean(actualAtt)

                                                        facAttendance.UpdateStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, translatedActual, blnTardy, blnExcused, clsMtgId)
                                                        attendanceUpdated = True

                                                    Else
                                                        ' DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = ""
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Add("border", "1px solid red")
                                                        invalidFieldsCount = invalidFieldsCount + 1
                                                    End If
                                                Else
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Add("border", "1px solid red")
                                                    invalidFieldsCount = invalidFieldsCount + 1
                                                End If
                                                '---------------------------------------------------------------------------------------------------
                                            End If


                                        Else
                                            'Do an insert

                                            scheduled = DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(2), HiddenField).Value


                                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                                facAttendance.AddStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, actualAtt, actTardy, 0, scheduled * 60, clsMtgId)
                                                attendanceUpdated = True

                                            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                                facAttendance.AddStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, actualAtt * 60, actTardy, 0, scheduled * 60, clsMtgId)
                                                attendanceUpdated = True

                                            Else
                                                If scheduled > 1 Then scheduled = 1

                                                If IsValidValue(actualAtt.ToUpper, {"T", "P", "A", "E"}, False) = True Then
                                                    '-------------------DE8711: NZ 11/13/12 ---------------------------------------------------------------
                                                    '11/19/12 NZ find scheduled classes 
                                                    If scheduled > 0 OrElse actualAtt.ToUpper = "P" Then
                                                        translatedActual = facAttendance.TranslatePALetterToQuantity(actualAtt)
                                                        blnTardy = facAttendance.TranslatePALetterToTardyBoolean(actualAtt)
                                                        blnExcused = facAttendance.TranslatePALetterToExcusedBoolean(actualAtt)
                                                        facAttendance.AddStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, translatedActual, blnTardy, blnExcused, scheduled, clsMtgId)
                                                        attendanceUpdated = True

                                                    Else
                                                        ' DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = ""
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Add("border", "1px solid red")
                                                        invalidFieldsCount = invalidFieldsCount + 1
                                                    End If
                                                    '---------------------------------------------------------------------------------------------------
                                                Else
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Add("border", "1px solid red")
                                                    invalidFieldsCount = invalidFieldsCount + 1
                                                End If

                                            End If

                                            'Response.Write("INSERT Row:" & rowCounter & " Cell:" & cellCounter & " Actual:" & actualAtt & "<br>")
                                        End If
                                    Else
                                        'If the date exists in the dt then we will have to do a delete
                                        arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate & "# and ClsSectMeetingId = '" & clsMtgId & "'")

                                        If arrRows.Length = 0 Then
                                            arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate.Date & "# and ClsSectMeetingId = '" & clsMtgId & "'")

                                        End If
                                        If arrRows.Length > 0 Then
                                            'Do a delete
                                            facAttendance.DeleteStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, clsMtgId)
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.White
                                            attendanceUpdated = True

                                            'Response.Write("DELETE Row:" & rowCounter & " Cell:" & cellCounter & "<br>")
                                        End If
                                    End If
                                Next
                            Next
                            'End If
                        End If
                    ElseIf dtClsSectionMeets.Rows(idx - 1)("ClsSectMeetingId") <> dtClsSectionMeets.Rows(idx)("ClsSectMeetingId") Then
                        ctrl = pnlAttendance.FindControl("tbl" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                        sTime = CDate(dtClsSectionMeets.Rows(idx)("TimeIntervalDescrip")).TimeOfDay.ToString

                        tbl = DirectCast(ctrl, Table)

                        If Not (tbl Is Nothing) Then
                            rowCounter = 1

                            'Dim arrDates As ArrayList = DateColl.Item(dr("ClsSectionId").ToString)
                            Dim arrDates As ArrayList
                            If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                                arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                            Else
                                arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                            End If
                            clsMtgId = dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                            'We start at the second row since the first row is the header
                            Dim rCnt As Integer = tbl.Rows.Count - 1
                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then rCnt = tbl.Rows.Count - 2
                            For rowCounter = 1 To rCnt

                                'Start at the second cell because the first stores the names of students
                                For cellCounter = 1 To arrDates.Count

                                    'clear borders from red
                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Remove("border")

                                    attDate = arrDates.Item(cellCounter - 1)
                                    If attDate.TimeOfDay.Hours = 0 Then attDate = CDate(attDate & " " & sTime)
                                    If DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text <> "" Then
                                        actualAtt = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text

                                        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                            actTardy = DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Checked
                                        End If
                                        'Check if the row exists in the ActualAttendance dt
                                        arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate & "# AND ClsSectionId = '" & dr("ClsSectionId").ToString & "'and ClsSectMeetingId = '" & clsMtgId & "'")

                                        If arrRows.Length = 0 Then
                                            arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate.Date & "# AND ClsSectionId = '" & dr("ClsSectionId").ToString & "'and ClsSectMeetingId = '" & clsMtgId & "'")
                                        End If
                                        If arrRows.Length > 0 Then
                                            'If the actual in the table does not match the actual in the dt then
                                            'we will need to do an update if the actual in the table is not an empty string.
                                            'If the actual in the table does not match the actual in the dt then
                                            'we will need to do a delete if the actual in the table is an empty string.
                                            Dim dtActual As String
                                            Dim isTardy As Boolean
                                            Dim isExcused As Boolean
                                            blnTardy = False
                                            blnExcused = False

                                            dtActual = arrRows(0)("Actual").ToString()
                                            isTardy = arrRows(0)("Tardy")
                                            isExcused = arrRows(0)("Excused")

                                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                                'If arrRows(0)("Actual").ToString() <> actualAtt Then
                                                'Do an update
                                                facAttendance.UpdateStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, actualAtt, actTardy, 0, clsMtgId)
                                                attendanceUpdated = True

                                                'Response.Write("Row:" & rowCounter & " Cell:" & cellCounter & "Old:" & arrRows(0)("Actual").ToString() & " New:" & actualAtt & "<br>")
                                                'End If
                                            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                                'If arrRows(0)("Actual").ToString() <> actualAtt Then
                                                'Do an update
                                                facAttendance.UpdateStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, actualAtt * 60, actTardy, 0, clsMtgId)
                                                attendanceUpdated = True

                                                'Response.Write("Row:" & rowCounter & " Cell:" & cellCounter & "Old:" & arrRows(0)("Actual").ToString() & " New:" & actualAtt & "<br>")
                                                'End If
                                            Else
                                                '-------------------DE8711: NZ 11/13/12 ---------------------------------------------------------------
                                                '11/19/12 NZ find scheduled classes 
                                                scheduled = DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(2), HiddenField).Value
                                                If IsValidValue(actualAtt.ToUpper, {"T", "P", "A", "E"}, False) = True Then
                                                    If scheduled > 0 OrElse actualAtt.ToUpper = "P" Then
                                                        scheduled = 1
                                                        translatedActual = facAttendance.TranslatePALetterToQuantity(actualAtt)
                                                        'If (dtActual <> translatedActual) Or _
                                                        '(dtActual = "1" And translatedActual.ToString = "1" And actualAtt.ToUpper = "T" And isTardy = False) Or _
                                                        '(dtActual = "1" And translatedActual.ToString = "1" And actualAtt.ToUpper = "P" And isTardy = True) Or _
                                                        '(dtActual = "0" And translatedActual.ToString = "0" And actualAtt.ToUpper = "E" And isExcused = False) Or _
                                                        '(dtActual = "0" And translatedActual.ToString = "0" And actualAtt.ToUpper = "A" And isExcused = True) Then
                                                        blnTardy = facAttendance.TranslatePALetterToTardyBoolean(actualAtt)
                                                        blnExcused = facAttendance.TranslatePALetterToExcusedBoolean(actualAtt)
                                                        'End If
                                                        facAttendance.UpdateStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, translatedActual, blnTardy, blnExcused, clsMtgId)
                                                        attendanceUpdated = True

                                                    Else
                                                        'DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = ""
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Add("border", "1px solid red")
                                                        invalidFieldsCount = invalidFieldsCount + 1
                                                    End If
                                                Else
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Add("border", "1px solid red")
                                                    invalidFieldsCount = invalidFieldsCount + 1
                                                End If
                                                '---------------------------------------------------------------------------------------------------
                                            End If
                                        Else
                                            'Do an insert

                                            scheduled = DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(2), HiddenField).Value

                                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                                facAttendance.AddStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, actualAtt, actTardy, 0, scheduled * 60, clsMtgId)
                                                attendanceUpdated = True

                                            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                                facAttendance.AddStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, actualAtt * 60, actTardy, 0, scheduled * 60, clsMtgId)
                                                attendanceUpdated = True

                                            Else
                                                If IsValidValue(actualAtt.ToUpper, {"T", "P", "A", "E"}, False) = True Then
                                                    '-------------------DE8711: NZ 11/13/12 ---------------------------------------------------------------

                                                    'blnTardy = False
                                                    'blnExcused = False
                                                    If scheduled > 0 OrElse actualAtt.ToUpper = "P" Then
                                                        If scheduled > 1 Then scheduled = 1
                                                        translatedActual = facAttendance.TranslatePALetterToQuantity(actualAtt)
                                                        blnTardy = facAttendance.TranslatePALetterToTardyBoolean(actualAtt)
                                                        blnExcused = facAttendance.TranslatePALetterToExcusedBoolean(actualAtt)
                                                        facAttendance.AddStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, translatedActual, blnTardy, blnExcused, scheduled, clsMtgId)
                                                        attendanceUpdated = True

                                                    Else
                                                        'DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = ""
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Add("border", "1px solid red")
                                                        invalidFieldsCount = invalidFieldsCount + 1
                                                    End If
                                                Else
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Style.Add("border", "1px solid red")
                                                    invalidFieldsCount = invalidFieldsCount + 1
                                                End If
                                                '---------------------------------------------------------------------------------------------------
                                            End If

                                            'Response.Write("INSERT Row:" & rowCounter & " Cell:" & cellCounter & " Actual:" & actualAtt & "<br>")
                                        End If
                                    Else
                                        'If the date exists in the dt then we will have to do a delete
                                        arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate & "# and ClsSectMeetingId = '" & clsMtgId & "'")

                                        If arrRows.Length = 0 Then
                                            arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate.Date & "# and ClsSectMeetingId = '" & clsMtgId & "'")

                                        End If
                                        If arrRows.Length > 0 Then
                                            'Do a delete
                                            facAttendance.DeleteStudentClsSectionAttendanceRecordAll(stuId, dr("ClsSectionId").ToString, attDate, clsMtgId)
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.White
                                            attendanceUpdated = True

                                            'Response.Write("DELETE Row:" & rowCounter & " Cell:" & cellCounter & "<br>")
                                        End If
                                    End If
                                Next
                            Next
                            'End If
                        End If
                    End If 'HERE
                Next
            End If
        Next

        If (attendanceUpdated) Then
            Try
                PostPaymentPeriodAttendanceAFA(Guid.Parse(stuId))
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)


            End Try

        End If

        '
        Dim strTermId As String = ViewState("AllTerms")
        '
        'Get students class section attendance postings
        dtActualAttendace = facAttendance.GetStudentAttendance(ddlStuEnrollId.SelectedItem.Value, ddlClsSectionId.SelectedItem.Value, strTermId)
        ViewState("ActualAttendance") = dtActualAttendace
        Dim strAttType As String = " "

        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
            strAttType = " Minutes"
        ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
            strAttType = " Hours"
        End If

        If (txtPrgVerTrackAttendance.Text = "True") Then
            tblOverallTotals.Visible = True
            Dim overallPercent As AttendancePercentageInfo = facAttendance.GetOverallPercentageAttendanceInfo(ddlStuEnrollId.SelectedItem.Value, CampusId)
            BuildTotalsTable(overallPercent, strAttType)
        Else
            tblOverallTotals.Visible = False
        End If



        PopulateAttendanceGrid()
        Dim strEnrollmentSelectedItem As String = ddlStuEnrollId.SelectedValue
        BuildStuEnrollDDL()
        ddlStuEnrollId.SelectedValue = strEnrollmentSelectedItem

        If invalidFieldsCount <> 0 Then
            CommonWebUtilities.DisplayWarningInMessageBox(Page, "Please correct the " & invalidFieldsCount & " invalid highlighted boxes")
        Else
            If ppopulateAttendanceGrid.Trim <> String.Empty Then
                DisplayErrorMessage(ppopulateAttendanceGrid)
            Else
                CommonWebUtilities.DisplayInfoInMessageBox(Page, "All changes saved successfully!")
            End If
        End If
    End Sub
#End Region

#Region "COMMON SERVICE FUNCTIONS"
    Private Sub FindControlToValidateByControl(ByVal mCtrl As TextBox)

        Dim decimalVal As Decimal

        Dim RngValidator As New RangeValidator
        RngValidator.ID = "NumberInputRangeValidator"


        Dim strRange As String = mCtrl.Text

        Try
            decimalVal = Convert.ToDecimal(strRange)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            decimalVal = 0.0
        End Try

        If decimalVal > 24 And strRange <> String.Empty Then
            RngValidator.ErrorMessage = "The Hours range between 0 and 24"
            RngValidator.Display = ValidatorDisplay.Dynamic
            RngValidator.ControlToValidate = mCtrl.ID
            RngValidator.MaximumValue = "24"
            RngValidator.MinimumValue = "0"

            MainPanel.Controls.Add(RngValidator)

            ppopulateAttendanceGrid = "The Hours range between 0 and 24"

            Customvalidator1.IsValid = False
        Else
            ppopulateAttendanceGrid = String.Empty

        End If




    End Sub
    Private Function IsValidValue(ByVal actualAtt As String, ByVal requiredValues As String(), ByVal isNumber As Boolean) As Boolean
        Dim goodMatch As Boolean = False

        If isNumber = False Then

            For Each strValue As String In requiredValues
                If Trim(actualAtt) = strValue Then
                    goodMatch = True
                    Exit For
                End If
            Next
        End If

        Return goodMatch

    End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub
    Public Function GetScheduledMinutesForMeetDay(ByVal dtm As DateTime, ByVal clsSectionId As String, ClsSectMeetingId As String) As String
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim sDay As String
        Dim dtDays As DataTable
        Dim dr As DataRow
        Dim startTime As DateTime
        Dim endTime As DateTime
        Dim schedLength As TimeSpan

        'Get the short day value
        sDay = facAttendance.GetShortDayName(dtm)

        'Loop through the days that the class section meets and see which one this day matches one
        dtDays = DirectCast(ViewState("MeetDays"), DataTable)

        Dim arRow() As DataRow
        arRow = dtDays.Select("ClsSectionId='" & clsSectionId & "' AND WorkDaysDescrip='" & sDay & "' AND ClsSectMeetingId = '" & ClsSectMeetingId & "'")

        If arRow.GetLength(0) > 0 Then

            For Each dr In arRow
                If dr("StartDate") <= dtm.Date And dr("EndDate") >= dtm.Date Then
                    startTime = dr("TimeIntervalDescrip")
                    endTime = dr("EndTime")
                    Exit For
                End If
            Next

        End If

        schedLength = endTime.Subtract(startTime)

        Return CType(((schedLength.Hours * 60) + schedLength.Minutes), String)

    End Function
    Private Sub DeleteRowsFromAttendanceGrid()
        Dim tbl As Control
        For Each tbl In pnlAttendance.Controls
            If tbl.GetType() = GetType(Table) Then
                Dim tb As Table = CType(tbl, Table)
                If Not (tb Is Nothing OrElse tb.Rows Is Nothing) Then
                    Do While tb.Rows.Count > 0
                        tb.Rows.Remove(tb.Rows(0))
                    Loop
                End If
            End If
        Next
    End Sub
    Private Function GetDurationForMeetDate(ByVal MeetDays() As DataRow, ByVal sDay As String, ByVal dtm As Date, clsSectionMeetingId As String) As String
        'Dim sDay As String
        Dim dr As DataRow

        'Get the short day value
        'sDay = GetShortDayName(dtm)

        'Loop through the days that the class section meets and see which one this day matches one
        For Each dr In MeetDays
            If dr("WorkDaysDescrip").ToString() = sDay And dtm.Date >= dr("StartDate").toshortdatestring And dtm.Date <= dr("EndDate").toshortdatestring Then
                'Return "(" & Math.Round(dr("Duration"), 2) & " hrs)"
                If dtm.Date >= CType(dr("StartDate"), Date).Date And dtm.Date <= CType(dr("EndDate"), Date).Date And dr("ClsSectMeetingId").ToString = clsSectionMeetingId Then
                    If dtm.TimeOfDay = CType(dr("TimeIntervalDescrip"), Date).TimeOfDay Then
                        Return Math.Round(dr("Duration"), 2)
                    End If
                End If
            End If
        Next
        Return ""
    End Function

#End Region

#Region "Bind Grid"
    Private Sub BuildAttendanceGrid()
        'First create the header
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim dtm As DateTime
        Dim rowCounter As Integer
        Dim cellCounter As Integer
        Dim dtMeetDays As DataTable
        Dim dtClsSections As DataTable
        Dim dtDropInfo As DataTable
        Dim dtLoaInfo As DataTable
        Dim dtSuspInfo As DataTable
        Dim dsDropAndLoaInfo As DataSet
        Dim drDays() As DataRow
        Dim sDate As DateTime
        Dim eDate As DateTime
        dtMeetDays = DirectCast(ViewState("MeetDays"), DataTable)
        dtClsSections = DirectCast(ViewState("ClsSections"), DataTable)
        dsDropAndLoaInfo = DirectCast(ViewState("DropAndLOAInfoDS"), DataSet)
        dtDropInfo = dsDropAndLoaInfo.Tables("DropInfo")
        dtLoaInfo = dsDropAndLoaInfo.Tables("LOAInfo")
        dtSuspInfo = dsDropAndLoaInfo.Tables("SuspInfo")
        Dim dateColl = DirectCast(ViewState("Dates"), Collection)
        Dim dtClsSectionMeets As DataTable
        'delete all controls in the panels
        pnlAttendance.Controls.Clear()
        DeleteRowsFromAttendanceGrid()
        pnlRequiredFieldValidators.Controls.Clear()
        'DE8596
        Dim PostAttDuringHoliday As Boolean
        If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then PostAttDuringHoliday = False Else PostAttDuringHoliday = True
        For Each dr As DataRow In dtClsSections.Rows

            sDate = CDate(dr("StartDate"))
            eDate = CDate(dr("EndDate"))
            drDays = dtMeetDays.Select("ClsSectionId = '" & dr("ClsSectionId").ToString & "'")

            ' Class section has meetings
            If drDays.GetLength(0) > 0 Then
                'If 1 = 1 Then
                dtClsSectionMeets = drDays.CopyToDataTable()
                dtClsSectionMeets = dtClsSectionMeets.DefaultView.ToTable(True, "PeriodDescrip", "InstructionTypeDescrip", "ClsSectMeetingId", "UseTimeClock", "StartDate", "EndDate")
                Dim arrDates As New ArrayList
                'Dim arrDatesRange As New ArrayList

                Try
                    arrDates = dateColl.Item(dr("ClsSectionId").ToString)


                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)


                End Try

                If arrDates.Count <> 0 Then

                    ' ***********************************************
                    ' ************* Class Duration Table ************
                    ' ***********************************************
                    '
                    rowCounter = 0
                    Dim tblClsSecDuration As New Table

                    tblClsSecDuration.ID = "tblClsSecDuration" & dr("ClsSectionId").ToString
                    tblClsSecDuration.Width = Unit.Percentage(100)
                    tblClsSecDuration.CssClass = "textbox"
                    tblClsSecDuration.GridLines = GridLines.None
                    tblClsSecDuration.BorderStyle = BorderStyle.None
                    tblClsSecDuration.BackColor = Color.White
                    'First row, first cell
                    Dim c1 As New TableCell
                    Dim lbl As New Label
                    Dim r As New TableRow
                    lbl.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    c1.Controls.Add(lbl)
                    r.Cells.Add(c1)
                    tblClsSecDuration.Rows.Add(r)


                    r = New TableRow
                    c1 = New TableCell
                    lbl = New Label
                    r.BackColor = Color.LightGray
                    'First row, first cell contains "Class Duration" label
                    lbl.CssClass = "Label"
                    lbl.Text = "<strong>" & dr("Class").ToString & "</strong>" & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & "Duration: " & dr("ClassSectionDuration") & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & "Instructor: " & dr("Instructor")
                    c1.VerticalAlign = VerticalAlign.Bottom
                    c1.HorizontalAlign = HorizontalAlign.Left
                    c1.Controls.Add(lbl)
                    r.Cells.Add(c1)

                    tblClsSecDuration.Rows.Add(r)

                    pnlAttendance.Controls.Add(tblClsSecDuration)


                    Dim tblTotalsClass As Table
                    If txtPrgVerTrackAttendance.Text = "False" Then
                        tblTotalsClass = BuildTotalsTable(dr("ClsSectionId").ToString, "")
                    Else
                        tblTotalsClass = BuildTotalsTableProgram(dr("ClsSectionId").ToString, "")
                    End If
                    pnlAttendance.Controls.Add(tblTotalsClass)

                    For idx As Integer = 0 To dtClsSectionMeets.Rows.Count - 1

                        If idx = 0 Then

                            Dim tbl As New Table
                            tbl.ID = "tbl" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                            tbl.Width = Unit.Percentage(100)
                            tbl.CssClass = "textbox"
                            tbl.GridLines = GridLines.Horizontal
                            If dr("IsDrop") Then tbl.Enabled = False
                            'tblCounter += 1

                            'First row, first cell
                            c1 = New TableCell
                            lbl = New Label
                            r = New TableRow

                            'First row, first cell contains "Class Section" label
                            lbl.CssClass = "Label"
                            lbl.Text = "Class Section Meeting"
                            c1.VerticalAlign = VerticalAlign.Bottom
                            c1.HorizontalAlign = HorizontalAlign.Left
                            c1.Controls.Add(lbl)

                            r.Cells.Add(c1)

                            Dim ctr As Integer = 1
                            Dim sday As String
                            'Loop through the list of dates and add a cell with a label for each of them

                            'foreach (DateTime date in StartDate.To(EndDate).ExcludeEnd()                                    .Step(DayInterval.Days()) {     // Do something with the date } 

                            'While sDate <= eDate
                            '    arrDates.
                            'End While

                            'For Each dtm In arrDates              
                            If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                                arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                            Else
                                arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                            End If

                            'Due to conversion issues we need to add this condition
                            'if the difference between start date and end date is greater than 120 days 
                            'don't show the controls


                            If arrDates.Count >= 120 Then
                                ShowAttendance(False)
                                Exit Sub
                            Else
                                ShowAttendance(True)
                            End If

                            If arrDates.Count <= 120 Then
                                For indx As Integer = 0 To arrDates.Count - 1
                                    dtm = arrDates(indx)

                                    'If indx > 0 Then prevdtm = arrDates(indx - 1)
                                    sday = facAttendance.GetShortDayName(dtm)
                                    Dim dur As String = GetDurationForMeetDate(drDays, sday, dtm, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                    'remove the duplicates
                                    'If dur <> "" Then
                                    '    If prevdtm.Date = dtm.Date Then
                                    '        r.Cells(ctr - 2).Controls.Clear()
                                    '        ctr = ctr - 2
                                    '    End If
                                    'End If

                                    Dim c3 As New TableCell
                                    Dim lbl3 As New Label
                                    c3.HorizontalAlign = HorizontalAlign.Center
                                    c3.VerticalAlign = VerticalAlign.Top
                                    c3.CssClass = "Label"
                                    lbl3.CssClass = "Label"
                                    lbl3.Text = ctr
                                    ctr += 1

                                    'lbl3.ToolTip = facAttendance.GetShortDayName(dtm) & " " & facAttendance.GetShortMonthName(dtm) & " " & dtm.Day.ToString & " " & facAttendance.GetStartTimeForMeetDate(drDays, dtm)
                                    Dim hdn3 As New HiddenField






                                    lbl3.Text = sday & "<br>" & facAttendance.GetShortMonthName(dtm) & " " & dtm.Day.ToString & "<br>"
                                    'lbl3.Text &= " <br> " & facAttendance.GetStartTimeForMeetDate(drDays, dtm)
                                    c3.Controls.Add(lbl3)
                                    Dim lbl4 As New Label
                                    lbl4.ID = "lbl" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & ctr
                                    If dur <> "" Then lbl4.Text = "(" & dur & " hrs)"
                                    c3.Controls.Add(lbl4)


                                    hdn3.ID = "hdn" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & ctr
                                    If dur <> "" Then
                                        hdn3.Value = dur
                                    Else
                                        hdn3.Value = 0
                                    End If

                                    c3.Controls.Add(hdn3)



                                    r.Cells.Add(c3)
                                Next

                                tbl.Rows.Add(r)

                                'Second row, first cell
                                Dim rClsSect As New TableRow
                                rClsSect.BackColor = Color.FromName("f7fcff")
                                Dim cc As New TableCell
                                Dim llbl As New Label

                                rowCounter += 1

                                'Second row, first cell contains class section description
                                llbl.CssClass = "Label"
                                llbl.BackColor = Color.FromName("f7fcff")
                                llbl.Text = "<strong>" & dtClsSectionMeets.Rows(idx)("InstructionTypeDescrip") & " - " & dtClsSectionMeets.Rows(idx)("PeriodDescrip") & "</strong>"
                                llbl.ID = "lblClsSecDescrip" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                                cc.Wrap = False
                                cc.Controls.Add(llbl)
                                cc.CssClass = "Label"

                                rClsSect.Cells.Add(cc)

                                'Loop through the list of dates and add a cell with a textbox for each of them
                                cellCounter = 0
                                For Each dtm In arrDates

                                    Dim c5 As New TableCell
                                    Dim txtbox5 As New TextBox
                                    Dim cmv As New CompareValidator
                                    Dim rnv As New RangeValidator
                                    Dim rev As New RegularExpressionValidator

                                    cellCounter += 1

                                    c5.HorizontalAlign = HorizontalAlign.Center
                                    txtbox5.CssClass = "TextBox"
                                    txtbox5.ID = "txt" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & cellCounter.ToString
                                    txtbox5.Width = Unit.Pixel(30)
                                    txtbox5.BackColor = Color.White
                                    txtbox5.ReadOnly = False
                                    txtbox5.Text = ""
                                    'txtbox5.MaxLength = 4
                                    'Check if the student is on LOA for this date and for the enrollment that we are dealing with.
                                    If facAttendance.IsStudentOnLOA(ddlStuEnrollId.SelectedValue, dtm.ToShortDateString, dtLoaInfo) Then
                                        txtbox5.ReadOnly = True
                                        txtbox5.BackColor = Color.LightGray
                                        txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                        txtbox5.ToolTip = "Student is/was on LOA"
                                    ElseIf facAttendance.IsStudentOnLOA(ddlStuEnrollId.SelectedValue, dtm.ToShortDateString, dtSuspInfo) Then
                                        txtbox5.ReadOnly = True
                                        txtbox5.BackColor = Color.LightGray
                                        txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                        txtbox5.ToolTip = "Student is/was on LOA"
                                    Else
                                        'Check if the student has a drop status. If so and the LDA or Date Determined is earlier
                                        'than or equal to the test date then we need to make the text box read only.
                                        If dtDropInfo.Rows.Count > 0 Then
                                            If dtDropInfo.Rows(0)("SysStatusId") = StateEnum.Dropped Then   'StateEnum.Dropped=12
                                                If Not dtDropInfo.Rows(0).IsNull("DateDetermined") Then
                                                    If dtDropInfo.Rows(0)("DateDetermined") < dtm.ToShortDateString Then
                                                        txtbox5.ReadOnly = True
                                                        txtbox5.BackColor = Color.LightGray
                                                        txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                                        txtbox5.ToolTip = "Student has been dropped"
                                                    End If
                                                Else
                                                    'If no date determined was entered then we will not allow the user to input
                                                    'any attendance. This will force them to get the data corrected.
                                                    txtbox5.ReadOnly = True
                                                    txtbox5.BackColor = Color.LightGray
                                                    txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                                    txtbox5.ToolTip = "Student has been dropped but there is no date determined." & vbCr & "Please contact the Registrar to have this problem fixed."
                                                End If
                                            ElseIf dtDropInfo.Rows(0)("SysStatusId") = StateEnum.TransferOut Then   'StateEnum.Dropped=12
                                                If Not dtDropInfo.Rows(0).IsNull("LDA") Then
                                                    If dtDropInfo.Rows(0)("LDA") < dtm.ToShortDateString Then
                                                        txtbox5.ReadOnly = True
                                                        txtbox5.BackColor = Color.LightGray
                                                        txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                                        txtbox5.ToolTip = "Student has been Transferred"
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If


                                    'Check if the student has a start date specified. If not we will not allow the posting
                                    'of attendance but we also need to alert the instructor so that he/she can have the
                                    'registrar correct the problem.
                                    If dtDropInfo.Rows.Count > 0 Then
                                        If dtDropInfo.Rows(0).IsNull("StartDate") Then
                                            txtbox5.ReadOnly = True
                                            txtbox5.BackColor = Color.LightGray
                                            txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                            txtbox5.ToolTip = "Student does not have a start date." & vbCr & "Please contact the Registrar to have this problem fixed."
                                        Else
                                            'If the student has a start date that is after the meet date being examined then we
                                            'should not allow attendance to be posted for it.
                                            If dtm < dtDropInfo.Rows(0)("StartDate") Then
                                                txtbox5.ReadOnly = True
                                                txtbox5.BackColor = Color.LightGray
                                                txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                                txtbox5.ToolTip = "Student started after this day"
                                            End If
                                        End If
                                    End If
                                    If (facAttendance.GetStudentStatus(ddlStuEnrollId.SelectedValue) = "8") Then
                                        txtbox5.ReadOnly = True
                                        txtbox5.BackColor = Color.LightGray
                                        txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                        txtbox5.ToolTip = "Student has been cancelled"
                                    End If
                                    'DE8596
                                    If (facAttendance.IsDateHoliday(dtm) And Not PostAttDuringHoliday) Then
                                        txtbox5.ReadOnly = True
                                        txtbox5.BackColor = Color.LightGray
                                        txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                        txtbox5.ToolTip = "holiday"
                                    End If

                                    'If dtm > Date.Now Then txtbox5.ReadOnly = True

                                    Dim hdn5 As New HiddenField
                                    hdn5.ID = "hdn" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & cellCounter.ToString
                                    hdn5.Value = dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                                    'If DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text <> "" Then
                                    '    hdn5.Value = GetMeetIDForDate(drDays, dtm.DayOfWeek.ToString.Substring(0, 3), dtm)
                                    'Else
                                    '    hdn5.Value = GetMeetIDForUnscheduledDate(drDays, dtm.DayOfWeek.ToString.Substring(0, 3), dtm)
                                    'End If


                                    'If the attendance type is present/absent then we only want to allow the user to
                                    'enter P, A, E or T.
                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "PRESENT ABSENT" Then
                                        txtbox5.MaxLength = 1
                                        With rev
                                            '.ID = "rev" & rnd.Next(1, 1000000)
                                            Dim newGuid As Guid
                                            newGuid = Guid.NewGuid
                                            .ID = "rev" & newGuid.ToString
                                            .ControlToValidate = txtbox5.ID
                                            .Display = ValidatorDisplay.None
                                            .EnableClientScript = "False"
                                            .ErrorMessage = "Enter a value of P, A or T on " & dtm.ToLongDateString
                                            .ValidationExpression = "^[PpAaTt]{1}$"
                                            .Enabled = True
                                        End With
                                        pnlRequiredFieldValidators.Controls.Add(rev)
                                    End If

                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                        txtbox5.MaxLength = 6
                                        'Allow only numeric values between 0 and the scheduled minutes.
                                        'Dim classDuration As String = GetScheduledMinutesForMeetDay(dtm, dr("ClsSectionId").ToString, "")
                                        rnv.ControlToValidate = txtbox5.ID
                                        rnv.Type = ValidationDataType.Integer
                                        rnv.MinimumValue = 0
                                        rnv.MaximumValue = 10000
                                        rnv.Display = ValidatorDisplay.None
                                        'rnv.ErrorMessage = "Enter a value between 0 and " & classDuration & " on " & dtm.ToLongDateString & " for " & dr("Class").ToString
                                        rnv.ErrorMessage = "Enter a value greater than or equal to 0."
                                        pnlRequiredFieldValidators.Controls.Add(rnv)
                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                        txtbox5.MaxLength = 6
                                        'Allow only numeric values between 0 and the scheduled minutes.
                                        'Dim classDuration As String = GetScheduledMinutesForMeetDay(dtm, dr("ClsSectionId").ToString, "") / 60
                                        rnv.ControlToValidate = txtbox5.ID
                                        rnv.Type = ValidationDataType.Double
                                        rnv.MinimumValue = 0
                                        'rnv.MaximumValue = CInt(classDuration)
                                        rnv.Display = ValidatorDisplay.None
                                        'rnv.ErrorMessage = "Enter a value between 0 and " & classDuration & " on " & dtm.ToLongDateString & " for " & dr("Class").ToString
                                        rnv.ErrorMessage = "Enter a value greater than or equal to 0."
                                        pnlRequiredFieldValidators.Controls.Add(rnv)
                                    End If

                                    c5.Controls.Add(txtbox5)
                                    c5.Controls.Add(hdn5)
                                    rClsSect.Cells.Add(c5)

                                Next

                                tbl.Rows.Add(rClsSect)



                                'Third row, first cell
                                If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                    rClsSect = New TableRow
                                    rClsSect.BackColor = Color.FromName("f7fcff")
                                    cc = New TableCell
                                    llbl = New Label

                                    rowCounter += 1

                                    'Second row, first cell contains class section description
                                    'llbl.CssClass = "Label"
                                    'llbl.BackColor = Color.FromName("f7fcff")
                                    'llbl.Text = "<strong>" & dr("Class").ToString & "</strong>"
                                    'llbl.ID = "lblClsSecDescrip" & dr("ClsSectionId").ToString
                                    'cc.Wrap = False
                                    'cc.Controls.Add(llbl)
                                    'cc.CssClass = "Label"

                                    rClsSect.Cells.Add(cc)

                                    'Loop through the list of dates and add a cell with a textbox for each of them
                                    For Each dtm In arrDates

                                        Dim c5 As New TableCell
                                        Dim chkbox5 As New CheckBox
                                        'Dim cmv As New CompareValidator
                                        'Dim rnv As New RangeValidator
                                        'Dim rev As New RegularExpressionValidator

                                        cellCounter += 1

                                        c5.HorizontalAlign = HorizontalAlign.Center
                                        chkbox5.CssClass = "TextBox"
                                        chkbox5.ID = "chk" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & cellCounter.ToString
                                        chkbox5.Text = "T"
                                        chkbox5.Width = Unit.Pixel(30)
                                        c5.Controls.Add(chkbox5)
                                        rClsSect.Cells.Add(c5)




                                    Next

                                    tbl.Rows.Add(rClsSect)

                                End If




                                pnlAttendance.Controls.Add(tbl)

                                Dim tblTotals As Table
                                If txtPrgVerTrackAttendance.Text = "False" Then
                                    tblTotals = BuildTotalsTable(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                Else
                                    tblTotals = BuildTotalsTableProgram(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                End If
                                pnlAttendance.Controls.Add(tblTotals)
                            End If
                        ElseIf dtClsSectionMeets.Rows(idx - 1)("ClsSectMeetingId") <> dtClsSectionMeets.Rows(idx)("ClsSectMeetingId") Then
                            Dim tbl As New Table
                            tbl.ID = "tbl" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                            tbl.Width = Unit.Percentage(100)
                            tbl.CssClass = "textbox"
                            tbl.GridLines = GridLines.Horizontal

                            'tblCounter += 1

                            'First row, first cell
                            c1 = New TableCell
                            lbl = New Label
                            r = New TableRow

                            'First row, first cell contains "Class Section" label
                            lbl.CssClass = "Label"
                            lbl.Text = "Class Section Meeting"
                            c1.VerticalAlign = VerticalAlign.Bottom
                            c1.HorizontalAlign = HorizontalAlign.Left
                            c1.Controls.Add(lbl)

                            r.Cells.Add(c1)

                            Dim ctr As Integer = 1
                            Dim sday As String
                            'Loop through the list of dates and add a cell with a label for each of them

                            'foreach (DateTime date in StartDate.To(EndDate).ExcludeEnd()                                    .Step(DayInterval.Days()) {     // Do something with the date } 

                            'While sDate <= eDate
                            '    arrDates.
                            'End While

                            'For Each dtm In arrDates                 
                            If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                                arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                            Else
                                arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                            End If

                            If arrDates.Count >= 120 Then
                                ShowAttendance(False)
                                Exit Sub
                            Else
                                ShowAttendance(True)
                            End If

                            For indx As Integer = 0 To arrDates.Count - 1
                                dtm = arrDates(indx)

                                'If indx > 0 Then prevdtm = arrDates(indx - 1)
                                sday = facAttendance.GetShortDayName(dtm)
                                Dim dur As String = GetDurationForMeetDate(drDays, sday, dtm, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                'remove the duplicates
                                'If dur <> "" Then
                                '    If prevdtm.Date = dtm.Date Then
                                '        r.Cells(ctr - 2).Controls.Clear()
                                '        ctr = ctr - 2
                                '    End If
                                'End If

                                Dim c3 As New TableCell
                                Dim lbl3 As New Label
                                c3.HorizontalAlign = HorizontalAlign.Center
                                c3.VerticalAlign = VerticalAlign.Top
                                c3.CssClass = "Label"
                                lbl3.CssClass = "Label"
                                lbl3.Text = ctr
                                ctr += 1

                                'lbl3.ToolTip = facAttendance.GetShortDayName(dtm) & " " & facAttendance.GetShortMonthName(dtm) & " " & dtm.Day.ToString & " " & facAttendance.GetStartTimeForMeetDate(drDays, dtm)
                                Dim hdn3 As New HiddenField






                                lbl3.Text = sday & "<br>" & facAttendance.GetShortMonthName(dtm) & " " & dtm.Day.ToString & "<br>"
                                'lbl3.Text &= " <br> " & facAttendance.GetStartTimeForMeetDate(drDays, dtm)
                                c3.Controls.Add(lbl3)
                                Dim lbl4 As New Label
                                lbl4.ID = "lbl" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & ctr
                                If dur <> "" Then lbl4.Text = "(" & dur & " hrs)"
                                c3.Controls.Add(lbl4)


                                hdn3.ID = "hdn" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & ctr
                                If dur <> "" Then
                                    hdn3.Value = dur
                                Else
                                    hdn3.Value = 0
                                End If

                                c3.Controls.Add(hdn3)



                                r.Cells.Add(c3)
                            Next

                            tbl.Rows.Add(r)

                            'Second row, first cell
                            Dim rClsSect As New TableRow
                            rClsSect.BackColor = Color.FromName("f7fcff")
                            Dim cc As New TableCell
                            Dim llbl As New Label

                            rowCounter += 1

                            'Second row, first cell contains class section description
                            llbl.CssClass = "Label"
                            llbl.BackColor = Color.FromName("f7fcff")
                            llbl.Text = "<strong>" & dtClsSectionMeets.Rows(idx)("InstructionTypeDescrip") & " - " & dtClsSectionMeets.Rows(idx)("PeriodDescrip") & "</strong>"
                            llbl.ID = "lblClsSecDescrip" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                            cc.Wrap = False
                            cc.Controls.Add(llbl)
                            cc.CssClass = "Label"

                            rClsSect.Cells.Add(cc)

                            'Loop through the list of dates and add a cell with a textbox for each of them
                            cellCounter = 0
                            For Each dtm In arrDates

                                Dim c5 As New TableCell
                                Dim txtbox5 As New TextBox
                                Dim cmv As New CompareValidator
                                Dim rnv As New RangeValidator
                                Dim rev As New RegularExpressionValidator

                                cellCounter += 1

                                c5.HorizontalAlign = HorizontalAlign.Center
                                txtbox5.CssClass = "TextBox"
                                txtbox5.ID = "txt" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & cellCounter.ToString
                                txtbox5.Width = Unit.Pixel(30)
                                txtbox5.BackColor = Color.White
                                txtbox5.ReadOnly = False
                                txtbox5.Text = ""
                                'txtbox5.MaxLength = 4
                                'Check if the student is on LOA for this date and for the enrollment that we are dealing with.
                                If facAttendance.IsStudentOnLOA(ddlStuEnrollId.SelectedValue, dtm.ToShortDateString, dtLoaInfo) Then
                                    txtbox5.ReadOnly = True
                                    txtbox5.BackColor = Color.LightGray
                                    txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                    txtbox5.ToolTip = "Student is/was on LOA"
                                ElseIf facAttendance.IsStudentOnLOA(ddlStuEnrollId.SelectedValue, dtm.ToShortDateString, dtSuspInfo) Then
                                    txtbox5.ReadOnly = True
                                    txtbox5.BackColor = Color.LightGray
                                    txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                    txtbox5.ToolTip = "Student is/was on LOA"
                                Else
                                    'Check if the student has a drop status. If so and the LDA or Date Determined is earlier
                                    'than or equal to the test date then we need to make the text box read only.
                                    If dtDropInfo.Rows.Count > 0 Then
                                        If dtDropInfo.Rows(0)("SysStatusId") = StateEnum.Dropped Then   'StateEnum.Dropped=12
                                            If Not dtDropInfo.Rows(0).IsNull("DateDetermined") Then
                                                If dtDropInfo.Rows(0)("DateDetermined") < dtm.ToShortDateString Then
                                                    txtbox5.ReadOnly = True
                                                    txtbox5.BackColor = Color.LightGray
                                                    txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                                    txtbox5.ToolTip = "Student has been dropped"
                                                End If
                                            Else
                                                'If no date determined was entered then we will not allow the user to input
                                                'any attendance. This will force them to get the data corrected.
                                                txtbox5.ReadOnly = True
                                                txtbox5.BackColor = Color.LightGray
                                                txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                                txtbox5.ToolTip = "Student has been dropped but there is no date determined." & vbCr & "Please contact the Registrar to have this problem fixed."
                                            End If
                                        ElseIf dtDropInfo.Rows(0)("SysStatusId") = StateEnum.TransferOut Then   'StateEnum.Dropped=12
                                            If Not dtDropInfo.Rows(0).IsNull("LDA") Then
                                                If dtDropInfo.Rows(0)("LDA") < dtm.ToShortDateString Then
                                                    txtbox5.ReadOnly = True
                                                    txtbox5.BackColor = Color.LightGray
                                                    txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                                    txtbox5.ToolTip = "Student has been Transferred"
                                                End If
                                            End If
                                        End If
                                    End If
                                End If


                                'Check if the student has a start date specified. If not we will not allow the posting
                                'of attendance but we also need to alert the instructor so that he/she can have the
                                'registrar correct the problem.
                                If dtDropInfo.Rows.Count > 0 Then
                                    If dtDropInfo.Rows(0).IsNull("StartDate") Then
                                        txtbox5.ReadOnly = True
                                        txtbox5.BackColor = Color.LightGray
                                        txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                        txtbox5.ToolTip = "Student does not have a start date." & vbCr & "Please contact the Registrar to have this problem fixed."
                                    Else
                                        'If the student has a start date that is after the meet date being examined then we
                                        'should not allow attendance to be posted for it.
                                        If dtm < dtDropInfo.Rows(0)("StartDate") Then
                                            txtbox5.ReadOnly = True
                                            txtbox5.BackColor = Color.LightGray
                                            txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                            txtbox5.ToolTip = "Student started after this day"
                                        End If
                                    End If
                                End If
                                If (facAttendance.GetStudentStatus(ddlStuEnrollId.SelectedValue) = "8") Then
                                    txtbox5.ReadOnly = True
                                    txtbox5.BackColor = Color.LightGray
                                    txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                    txtbox5.ToolTip = "Student has been cancelled"
                                End If
                                'DE8596
                                If (facAttendance.IsDateHoliday(dtm) And Not PostAttDuringHoliday) Then
                                    txtbox5.ReadOnly = True
                                    txtbox5.BackColor = Color.LightGray
                                    txtbox5.Attributes.Add("onKeyDown", "preventBackspace();")
                                    txtbox5.ToolTip = "holiday"
                                End If
                                'If dtm > Date.Now Then txtbox5.ReadOnly = True


                                Dim hdn5 As New HiddenField
                                hdn5.ID = "hdn" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & cellCounter.ToString
                                hdn5.Value = dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                                'If DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text <> "" Then
                                '    hdn5.Value = GetMeetIDForDate(drDays, dtm.DayOfWeek.ToString.Substring(0, 3), dtm)
                                'Else
                                '    hdn5.Value = GetMeetIDForUnscheduledDate(drDays, dtm.DayOfWeek.ToString.Substring(0, 3), dtm)
                                'End If

                                'If the attendance type is present/absent then we only want to allow the user to
                                'enter P, A, E or T.
                                If ViewState("AttendanceUnitType").ToString.ToUpper = "PRESENT ABSENT" Then
                                    txtbox5.MaxLength = 1
                                    With rev
                                        '.ID = "rev" & rnd.Next(1, 1000000)
                                        Dim newGuid As Guid
                                        newGuid = Guid.NewGuid
                                        .ID = "rev" & newGuid.ToString
                                        .ControlToValidate = txtbox5.ID
                                        .Display = ValidatorDisplay.None
                                        .EnableClientScript = "False"
                                        .ErrorMessage = "Enter a value of P, A or T on " & dtm.ToLongDateString
                                        .ValidationExpression = "^[PpAaTt]{1}$"
                                    End With
                                    pnlRequiredFieldValidators.Controls.Add(rev)
                                End If

                                If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                    txtbox5.MaxLength = 6
                                    'Allow only numeric values between 0 and the scheduled minutes.
                                    'Dim classDuration As String = GetScheduledMinutesForMeetDay(dtm, dr("ClsSectionId").ToString, "")
                                    rnv.ControlToValidate = txtbox5.ID
                                    rnv.Type = ValidationDataType.Integer
                                    rnv.MinimumValue = 0
                                    rnv.MaximumValue = 10000
                                    rnv.Display = ValidatorDisplay.None
                                    'rnv.ErrorMessage = "Enter a value between 0 and " & classDuration & " on " & dtm.ToLongDateString & " for " & dr("Class").ToString
                                    rnv.ErrorMessage = "Enter a value greater than or equal to 0."
                                    pnlRequiredFieldValidators.Controls.Add(rnv)
                                ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                    txtbox5.MaxLength = 6
                                    'Allow only numeric values between 0 and the scheduled minutes.
                                    'Dim classDuration As String = GetScheduledMinutesForMeetDay(dtm, dr("ClsSectionId").ToString, "") / 60
                                    rnv.ControlToValidate = txtbox5.ID
                                    rnv.Type = ValidationDataType.Double
                                    rnv.MinimumValue = 0
                                    'rnv.MaximumValue = CInt(classDuration)
                                    rnv.Display = ValidatorDisplay.None
                                    'rnv.ErrorMessage = "Enter a value between 0 and " & classDuration & " on " & dtm.ToLongDateString & " for " & dr("Class").ToString
                                    rnv.ErrorMessage = "Enter a value greater than or equal to 0."
                                    pnlRequiredFieldValidators.Controls.Add(rnv)
                                End If

                                c5.Controls.Add(txtbox5)
                                c5.Controls.Add(hdn5)
                                rClsSect.Cells.Add(c5)

                            Next

                            tbl.Rows.Add(rClsSect)



                            'Third row, first cell
                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                rClsSect = New TableRow
                                rClsSect.BackColor = Color.FromName("f7fcff")
                                cc = New TableCell
                                'llbl = New Label

                                rowCounter += 1

                                'Second row, first cell contains class section description
                                'llbl.CssClass = "Label"
                                'llbl.BackColor = Color.FromName("f7fcff")
                                'llbl.Text = "<strong>" & dr("Class").ToString & "</strong>"
                                'llbl.ID = "lblClsSecDescrip" & dr("ClsSectionId").ToString
                                'cc.Wrap = False
                                'cc.Controls.Add(llbl)
                                'cc.CssClass = "Label"

                                rClsSect.Cells.Add(cc)

                                'Loop through the list of dates and add a cell with a textbox for each of them
                                For Each dtm In arrDates

                                    Dim c5 As New TableCell
                                    Dim chkbox5 As New CheckBox
                                    'Dim cmv As New CompareValidator
                                    'Dim rnv As New RangeValidator
                                    'Dim rev As New RegularExpressionValidator

                                    cellCounter += 1

                                    c5.HorizontalAlign = HorizontalAlign.Center
                                    chkbox5.CssClass = "TextBox"
                                    chkbox5.ID = "chk" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & rowCounter.ToString & cellCounter.ToString
                                    chkbox5.Text = "T"
                                    chkbox5.Width = Unit.Pixel(30)
                                    c5.Controls.Add(chkbox5)
                                    rClsSect.Cells.Add(c5)




                                Next

                                tbl.Rows.Add(rClsSect)

                            End If




                            pnlAttendance.Controls.Add(tbl)


                            Dim tblTotals As Table
                            If txtPrgVerTrackAttendance.Text = "False" Then
                                tblTotals = BuildTotalsTable(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                            Else
                                tblTotals = BuildTotalsTableProgram(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                            End If
                            pnlAttendance.Controls.Add(tblTotals)
                        End If
                        '
                        '
                        '
                        ' ***********************************************
                        ' *************** Attendance Table **************
                        ' ***********************************************
                        '

                    Next


                End If
            End If
        Next

    End Sub

    Private Sub PopulateAttendanceGrid()
        Dim rowCounter As Integer
        Dim cellCounter As Integer
        Dim arrRows() As DataRow
        Dim stuEnrollId As String = ddlStuEnrollId.SelectedValue
        Dim dtm As DateTime
        Dim ctrl As Control
        Dim ctrl2 As Control
        Dim tbl As New Table
        Dim ttlPresent As Decimal
        Dim ttlExcused As Decimal
        Dim ttlAbsent As Decimal
        Dim ttlTardy As Decimal
        Dim ttlScheduled As Decimal
        Dim ttlMakeUp As Decimal
        Dim ttlTotalPresent As Decimal
        Dim ttlTotalExcused As Decimal
        Dim ttlTotalAbsent As Decimal
        Dim ttlTotalTardy As Decimal
        Dim ttlTotalScheduled As Decimal
        Dim ttlTotalMakeUp As Decimal

        Dim absences As Decimal
        Dim minusPresent As Decimal
        Dim minusTardies As Decimal
        Dim Totalabsences As Decimal
        Dim TotalminusPresent As Decimal
        Dim TotalminusTardies As Decimal
        Dim factor As Integer
        Dim ccounter As Integer
        Dim dtMeetDays As DataTable
        Dim drDays() As DataRow
        Dim dtClsSections As DataTable
        Dim temp As String
        '  Dim classDuration As String
        Dim facade As New ClsSectAttendanceFacade
        Dim meetingID As String
        Dim dtClsSectionMeets As DataTable
        dtMeetDays = DirectCast(ViewState("MeetDays"), DataTable)
        dtClsSections = DirectCast(ViewState("ClsSections"), DataTable)
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim sTime As String
        Dim sDate As Date
        For Each dr As DataRow In dtClsSections.Rows


            drDays = dtMeetDays.Select("ClsSectionId = '" & dr("ClsSectionId").ToString & "'")
            If drDays.GetLength(0) > 0 Then

                dtClsSectionMeets = drDays.CopyToDataTable()
                dtClsSectionMeets = dtClsSectionMeets.DefaultView.ToTable(True, "PeriodDescrip", "InstructionTypeDescrip", "ClsSectMeetingId", "UseTimeClock", "StartDate", "EndDate", "TimeIntervalDescrip")
                rowCounter = 1
                ttlTotalPresent = 0
                ttlTotalExcused = 0
                ttlTotalAbsent = 0
                ttlTotalTardy = 0
                ttlTotalMakeUp = 0
                ttlTotalScheduled = 0
                absences = 0
                minusPresent = 0
                For idx As Integer = 0 To dtClsSectionMeets.Rows.Count - 1


                    If idx = 0 Then
                        sTime = CDate(dtClsSectionMeets.Rows(idx)("TimeIntervalDescrip")).TimeOfDay.ToString
                        ctrl = pnlAttendance.FindControl("tbl" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                        tbl = DirectCast(ctrl, Table)

                        If Not (tbl Is Nothing) Then

                            ttlPresent = 0
                            ttlExcused = 0
                            ttlAbsent = 0
                            ttlTardy = 0
                            ttlMakeUp = 0
                            ttlScheduled = 0
                            temp = ""
                            absences = 0

                            Dim arrDates As New ArrayList
                            Try
                                'arrDates = DateColl.Item(dr("ClsSectionId").ToString)
                                If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                                    arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                                Else
                                    arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                                End If
                            Catch ex As Exception
                                Dim exTracker = New AdvApplicationInsightsInitializer()
                                exTracker.TrackExceptionWrapper(ex)


                            End Try

                            'We start at the second row since the first row is the header
                            For cellCounter = 1 To arrDates.Count
                                dtm = arrDates.Item(cellCounter - 1)

                                'If we are dealing with post by exception then we are dealing with unposted days and so we
                                'we do not need to populate the table from the actual attendance records for the class section.
                                'In this case if we are dealing with a present/absent system we will need to populate the table
                                'with P. If we are dealing with a minutes system then we should populate the table with the 
                                'scheduled minutes of the class section.

                                'Check the ActAttendance dt in session to see if this student has attendance
                                'posted for this day. If so we want to fill the textbox with the amount of hours
                                'actually attended.
                                meetingID = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(1), HiddenField).Value
                                arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND (MeetDate = #" & dtm & "# or meetdate=#" & dtm.Date & "#)  AND ClsSectionId = '" & dr("ClsSectionId").ToString & "' and ClsSectMeetingId = '" & meetingID & "'")

                                If arrRows.Length = 0 Then
                                    sDate = dtm
                                    If dtm.TimeOfDay.Hours = 0 Then sDate = CDate(dtm & " " & sTime)
                                    arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND MeetDate = #" & sDate & "# AND ClsSectionId = '" & dr("ClsSectionId").ToString & "'and ClsSectMeetingId = '" & meetingID & "'")
                                End If


                                factor = facade.GetTardiesMakingAbsence(stuEnrollId, dr("ClsSectionId").ToString)

                                If DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).ReadOnly Then
                                    ' DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.LightGray

                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                        DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = False
                                    End If
                                Else

                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.White
                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                        DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = False
                                    End If
                                    If DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(2), HiddenField).Value <> 0 Then
                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.Aquamarine
                                        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                            DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = True
                                        End If
                                    End If

                                End If
                                If arrRows.Length > 0 Then





                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                        'Minutes system needs to count number of minutes present, tardies and absent.
                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = arrRows(0)("Actual").ToString()
                                        DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Checked = arrRows(0)("Tardy")


                                        If arrRows(0)("Scheduled") > 0 Then
                                            DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text = "(" & Double.Parse(arrRows(0)("Scheduled") / 60).ToString("#0.00") & " hrs)"
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.Aquamarine
                                            If Not DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).ReadOnly Then
                                                DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = True
                                            End If

                                        Else
                                            DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text = ""
                                            DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = False
                                        End If


                                        'classDuration = GetScheduledMinutesForMeetDay(dtm, dr("ClsSectionId").ToString, arrRows(0)("ClsSectMeetingId").ToString)

                                        ttlScheduled += arrRows(0)("Scheduled")

                                        If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) And Not arrRows(0)("Tardy") Then
                                            ttlAbsent += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                            ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightBlue


                                        Else
                                            ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())

                                            If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                                                If arrRows(0)("Tardy") Then
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                                End If
                                                ccounter += 1
                                                If ccounter = factor Then
                                                    absences += CInt(arrRows(0)("Scheduled"))
                                                    minusPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                                    minusTardies += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                                    ccounter = 0
                                                Else
                                                    absences += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                                End If
                                                ttlTardy += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                            Else
                                                ttlMakeUp += Integer.Parse(arrRows(0)("Actual").ToString()) - arrRows(0)("Scheduled")
                                                If arrRows(0)("Actual") > arrRows(0)("Scheduled") Then
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightSalmon
                                                End If

                                            End If
                                        End If
                                        If arrRows(0)("Tardy") Then
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                        End If
                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                        'Minutes system needs to count number of minutes present, tardies and absent.
                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = Math.Round((arrRows(0)("Actual") / 60), 2)
                                        DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Checked = arrRows(0)("Tardy")


                                        If arrRows(0)("Scheduled") > 0 Then
                                            DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text = "(" & Decimal.Parse(arrRows(0)("Scheduled") / 60).ToString("#0.00") & " hrs)"
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.Aquamarine
                                            If Not DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).ReadOnly Then
                                                DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = True
                                            End If
                                        Else
                                            DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text = ""
                                            DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = False
                                        End If




                                        'classDuration = GetScheduledMinutesForMeetDay(dtm, dr("ClsSectionId").ToString, arrRows(0)("ClsSectMeetingId").ToString)
                                        ttlScheduled += arrRows(0)("Scheduled")
                                        ' If Integer.Parse(arrRows(0)("Actual").ToString()) = 0 Then
                                        If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) And Not arrRows(0)("Tardy") Then
                                            ttlAbsent += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                            ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightBlue


                                        Else
                                            ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())

                                            If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                                                If arrRows(0)("Tardy") Then
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                                End If
                                                ccounter += 1
                                                If ccounter = factor Then
                                                    absences += CInt(arrRows(0)("Scheduled"))
                                                    minusPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                                    minusTardies += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                                    ccounter = 0
                                                Else
                                                    absences += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                                End If
                                                ttlTardy += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                            Else
                                                ttlMakeUp += Integer.Parse(arrRows(0)("Actual").ToString()) - arrRows(0)("Scheduled")
                                                If arrRows(0)("Actual") > arrRows(0)("Scheduled") Then
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightSalmon
                                                End If
                                            End If
                                        End If
                                        If arrRows(0)("Tardy") Then
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                        End If
                                    Else
                                        'Present/Absent system so we need to translate the 0 and 1 to "A" and "P" respectively.
                                        'If the record is marked as tardy we will display a "T" instead of a "P".
                                        'If the record is marked as excused we will display an "E" instead of "A"
                                        'DE8711: NZ 11/13/12--------------------------------------------------------------------------------------------------------
                                        If arrRows(0)("Scheduled") > 0 Then


                                            If arrRows(0)("Actual").ToString() = "0" Then
                                                If arrRows(0)("Excused") = True Then
                                                    'possibly excused
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "E"
                                                    ttlExcused += 1
                                                Else
                                                    'absent
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "A"
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightBlue
                                                    ttlAbsent += 1
                                                End If
                                            End If

                                            If arrRows(0)("Actual").ToString() = "1" Then

                                                If arrRows(0)("Tardy") = True Then
                                                    'tardy one
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "T"
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                                    ttlTardy += 1
                                                Else
                                                    'check if present on what day
                                                    If arrRows(0)("Scheduled") = 0 Then
                                                        'make up!
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightSalmon
                                                    Else
                                                        'scheduled class
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.Aquamarine
                                                    End If
                                                    ttlPresent += 1
                                                End If
                                            End If

                                            'If arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") = True Then
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "E"
                                            '    ttlExcused += 1
                                            'ElseIf arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") <> True Then
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "A"
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.LightBlue
                                            '    ttlAbsent += 1
                                            'ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") = True Then
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "T"
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.BlanchedAlmond
                                            '    ttlTardy += 1
                                            'ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") <> True Then
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
                                            '    If arrRows(0)("Scheduled") = 0 Then
                                            '        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.LightSalmon
                                            '    Else
                                            '        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.Aquamarine
                                            '    End If
                                            '    ttlPresent += 1
                                            'End If
                                        Else
                                            If arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") = False Then 'make up session
                                                DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
                                                DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightSalmon
                                                ttlPresent += 1
                                            Else
                                                DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = ""
                                            End If
                                        End If
                                        '--------------------------------------------------------------------------------------------------------
                                    End If

                                End If

                                If dtClsSectionMeets.Rows(idx)("UseTimeClock") And (ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS") Then
                                    Dim txtRead As TextBox = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox)
                                    txtRead.ReadOnly = True
                                    'txtRead.Enabled = False
                                    'txtRead.BackColor = System.Drawing.Color.White
                                    txtRead.Attributes.Add("onKeyDown", "preventBackspace();")
                                    Dim chkRead As CheckBox = DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox)
                                    If Not (chkRead Is Nothing) Then chkRead.Enabled = False
                                ElseIf dtClsSectionMeets.Rows(idx)("UseTimeClock") = False Then
                                    Dim txtRead As TextBox = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox)
                                    If txtRead.BackColor <> Color.LightGray Then
                                        txtRead.ReadOnly = False
                                        'txtRead.Enabled = False
                                        'txtRead.BackColor = System.Drawing.Color.White
                                        txtRead.Attributes.Remove("onKeyDown")
                                        If Not ViewState("AttendanceUnitType").ToString.ToUpper = "PRESENT ABSENT" Then
                                            If DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text <> "" Then
                                                Dim chkRead As CheckBox = DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox)
                                                If Not (chkRead Is Nothing) Then chkRead.Enabled = True
                                            Else
                                                Dim chkRead As CheckBox = DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox)
                                                If Not (chkRead Is Nothing) Then chkRead.Enabled = False
                                            End If
                                        End If



                                    End If

                                End If
                                Dim txtRead1 As TextBox = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox)
                                ''TT DE10077 Message for the HOURS not More then 24
                                If dtm.Date = CType(Date.Now, Date).Date And txtRead1.Text <> String.Empty Then
                                    ''DE10077 By Tatiana
                                    FindControlToValidateByControl(txtRead1)
                                End If
                                If dtm > Date.Now Then txtRead1.ReadOnly = True
                            Next

                            'totalPresentAdjusted = ttlPresent
                            'totalExcusedAdjusted = ttlExcused
                            'totalAbsenceAdjusted = ttlAbsent + ttlExcused
                            'totalTardiesAdjusted = ttlTardy

                            '***********************************************************
                            '************* Populate the AttendanceTotals table *********
                            '***********************************************************

                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                '******************************************
                                '****** Minutes Attendance Unit Type ******
                                '******************************************
                                'Show total minutes and total hours.
                                'Actual Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Present: " & Integer.Parse(ttlPresent).ToString("#0") & " Minutes<br>("


                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours)"
                                End If
                                'Excused
                                ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = ""
                                    DirectCast(ctrl2, Label).Visible = False
                                End If
                                'Tardy
                                ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Scheduled: " & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours)"
                                End If
                                'Absent
                                ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Absent: " & (Integer.Parse(ttlAbsent) + Integer.Parse(ttlTardy)).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    'DE8891 NZ
                                    DirectCast(ctrl2, Label).Text = temp & ((Integer.Parse(ttlAbsent) + Integer.Parse(ttlTardy)) / 60).ToString("#0.00") & " Hours)"
                                End If
                                '
                                'Adjusted Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblTtlPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If ttlPresent >= minusPresent Then
                                    ttlPresent -= minusPresent
                                End If
                                temp = "Present: " & Integer.Parse(ttlPresent).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours)" & IIf(minusPresent > 0, "*", "")
                                End If
                                'Excused

                                ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)

                                temp = "MakeUp: " & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Visible = True
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours)"
                                    'DirectCast(ctrl2, Label).Text = ""

                                End If

                                'MakeUp
                                ctrl2 = tbl.FindControl("lblTtlExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "MakeUp: " & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Visible = True
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours)"
                                    'DirectCast(ctrl2, Label).Text = ""

                                End If

                                'Scheduled
                                ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Scheduled: " & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours)"
                                End If
                                'Tardy
                                'ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString)
                                'If ttlTardy >= minusTardies Then
                                '    ttlTardy -= minusTardies
                                'End If
                                'temp = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0") & " Minutes<br>("
                                'If Not (ctrl2 Is Nothing) Then
                                '    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours)" & IIf(minusTardies > 0, "*", "")
                                'End If
                                'Absent
                                ctrl2 = tbl.FindControl("lblTtlAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If ttlPresent > 0 And ttlTardy > 0 Then
                                    ttlAbsent += absences
                                End If



                                temp = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours)" & IIf(absences > 0, "*", "")
                                End If

                            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                '******************************************
                                '****** Minutes Attendance Unit Type ******
                                '******************************************
                                'Show total minutes and total hours.
                                'Actual Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Present: " & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlPresent).ToString("#0") & " Minutes)"
                                End If
                                'Excused
                                'MakeUp
                                ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "MakeUp: " & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes)"
                                    DirectCast(ctrl2, Label).Visible = True
                                End If
                                'Tardy
                                'ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString)
                                'temp = "Tardy: " & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours<br>("
                                'If Not (ctrl2 Is Nothing) Then
                                '    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTardy).ToString("#0") & " Minutes)"
                                'End If


                                ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Scheduled: " & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes)"
                                End If

                                'Absent
                                ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Absent: " & ((Integer.Parse(ttlAbsent) + Integer.Parse(ttlTardy)) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlAbsent) + Integer.Parse(ttlTardy)).ToString("#0") & " Minutes)"
                                End If
                                '
                                'Adjusted Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblTtlPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If ttlPresent >= minusPresent Then
                                    ttlPresent -= minusPresent
                                End If
                                temp = "Present: " & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlPresent).ToString("#0") & " Minutes)" & IIf(minusPresent > 0, "*", "")
                                End If
                                'Excused
                                'MakeUp
                                ctrl2 = tbl.FindControl("lblTtlExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "MakeUp: " & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes)"
                                    DirectCast(ctrl2, Label).Visible = True
                                End If
                                'Tardy
                                'ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString)
                                'If ttlTardy >= minusTardies Then
                                '    ttlTardy -= minusTardies
                                'End If
                                'temp = "Tardy: " & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours<br>("
                                'If Not (ctrl2 Is Nothing) Then
                                '    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTardy).ToString("#0") & " Minutes)" & IIf(minusTardies > 0, "*", "")
                                'End If

                                ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Scheduled: " & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes)"
                                End If


                                'Absent
                                ctrl2 = tbl.FindControl("lblTtlAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If ttlPresent > 0 And ttlTardy > 0 Then
                                    ttlAbsent += absences
                                End If
                                temp = "Absent: " & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlAbsent).ToString("#0") & " Minutes)" & IIf(absences > 0, "*", "")
                                End If

                            Else

                                '******************************************
                                '*** PresentAbsent Attendance Unit Type ***
                                '******************************************
                                'Actual Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Present: " & Integer.Parse(ttlPresent).ToString("#0")
                                End If
                                'Excused
                                ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Excused: " & Integer.Parse(ttlExcused).ToString("#0")
                                End If
                                'Tardy
                                ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0")
                                End If
                                'Absent
                                ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0")
                                End If
                                '
                                'Adjusted Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblTtlPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Present: " & Integer.Parse(ttlPresent).ToString("#0")
                                End If
                                'Excused
                                ctrl2 = tbl.FindControl("lblTtlExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Excused: " & Integer.Parse(ttlExcused).ToString("#0")
                                End If
                                'Tardy
                                absences = 0
                                If ttlTardy > 0 Then
                                    absences = facade.GetAbsencesFromTardies(stuEnrollId, dr("ClsSectionId").ToString, ttlTardy)
                                End If
                                ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    If ttlTardy >= absences Then
                                        ttlTardy -= absences
                                    End If
                                    DirectCast(ctrl2, Label).Text = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0") & IIf(absences > 0, "*", "")
                                End If
                                'Absent
                                ctrl2 = tbl.FindControl("lblTtlAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    ttlAbsent += absences
                                    DirectCast(ctrl2, Label).Text = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0") & IIf(absences > 0, "*", "")
                                End If
                            End If

                            'Tardies(Message)
                            'ctrl2 = tbl.FindControl("lblTardiesMsg" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                            'If Not (ctrl2 Is Nothing) Then
                            '    If absences > 0 Then
                            '        If factor <> 0 Then
                            '            DirectCast(ctrl2, Label).Text = "* Indicates an adjustment: " & factor & " Tardies make an Absence"
                            '            DirectCast(ctrl2, Label).Visible = True
                            '        Else
                            '            DirectCast(ctrl2, Label).Visible = False
                            '        End If
                            '    Else
                            '        DirectCast(ctrl2, Label).Visible = False
                            '    End If
                            'End If
                            '
                            'Percentage
                            'ctrl2 = tbl.FindControl("lblPerc" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                            ''Dim percentage As Decimal = facade.ComputePercentageOfPresent(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                            'Dim percentage As Decimal
                            'If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                            '    'percentage = facade.ComputePercentageOfPresentforMinutes(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                            '    If ttlScheduled > 0 Then percentage = (ttlPresent / ttlScheduled) * 100
                            'ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                            '    If ttlScheduled > 0 Then percentage = (ttlPresent / ttlScheduled) * 100
                            '    'percentage = facade.ComputePercentageOfPresentforMinutes(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                            'Else
                            '    percentage = facade.ComputePercentageOfPresent(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                            'End If
                            'If Not (ctrl2 Is Nothing) Then
                            '    DirectCast(ctrl2, Label).Text = "Percentage of Present: " & percentage.ToString("#0.00") & "%"
                            'End If
                        End If
                        ttlTotalPresent += ttlPresent
                        ttlTotalExcused += ttlExcused
                        ttlTotalAbsent += ttlAbsent
                        ttlTotalTardy += ttlTardy
                        ttlTotalScheduled += ttlScheduled
                        ttlTotalMakeUp += ttlMakeUp
                        Totalabsences += absences
                        TotalminusPresent += minusPresent
                        TotalminusTardies += minusTardies
                    ElseIf dtClsSectionMeets.Rows(idx - 1)("ClsSectMeetingId") <> dtClsSectionMeets.Rows(idx)("ClsSectMeetingId") Then
                        sTime = CDate(dtClsSectionMeets.Rows(idx)("TimeIntervalDescrip")).TimeOfDay.ToString
                        ctrl = pnlAttendance.FindControl("tbl" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                        tbl = DirectCast(ctrl, Table)

                        If Not (tbl Is Nothing) Then

                            ttlPresent = 0
                            ttlExcused = 0
                            ttlAbsent = 0
                            ttlTardy = 0
                            ttlMakeUp = 0
                            ttlScheduled = 0
                            temp = ""
                            absences = 0

                            Dim arrDates As New ArrayList
                            Try
                                'arrDates = DateColl.Item(dr("ClsSectionId").ToString)
                                If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                                    arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                                Else
                                    arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(dr("ClsSectionId").ToString, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").toshortdatestring, dtClsSectionMeets.Rows(idx)("EndDate").toshortdatestring, CampusId)
                                End If
                            Catch ex As Exception
                                Dim exTracker = New AdvApplicationInsightsInitializer()
                                exTracker.TrackExceptionWrapper(ex)


                            End Try

                            'We start at the second row since the first row is the header
                            For cellCounter = 1 To arrDates.Count
                                dtm = arrDates.Item(cellCounter - 1)

                                'If we are dealing with post by exception then we are dealing with unposted days and so we
                                'we do not need to populate the table from the actual attendance records for the class section.
                                'In this case if we are dealing with a present/absent system we will need to populate the table
                                'with P. If we are dealing with a minutes system then we should populate the table with the 
                                'scheduled minutes of the class section.

                                'Check the ActAttendance dt in session to see if this student has attendance
                                'posted for this day. If so we want to fill the textbox with the amount of hours
                                'actually attended.
                                meetingID = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(1), HiddenField).Value
                                arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND (MeetDate = #" & dtm & "# or meetdate=#" & dtm.Date & "#)  AND ClsSectionId = '" & dr("ClsSectionId").ToString & "' and ClsSectMeetingId = '" & meetingID & "'")

                                If arrRows.Length = 0 Then
                                    sDate = dtm
                                    If dtm.TimeOfDay.Hours = 0 Then sDate = CDate(dtm & " " & sTime)
                                    arrRows = DirectCast(ViewState("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND MeetDate = #" & sDate & "# AND ClsSectionId = '" & dr("ClsSectionId").ToString & "'and ClsSectMeetingId = '" & meetingID & "'")
                                End If


                                factor = facade.GetTardiesMakingAbsence(stuEnrollId, dr("ClsSectionId").ToString)

                                If DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).ReadOnly Then
                                    ' DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.LightGray

                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                        DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = False
                                    End If
                                Else

                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.White
                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                        DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = False
                                    End If
                                    If DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(2), HiddenField).Value <> 0 Then
                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.Aquamarine
                                        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                            DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = True
                                        End If
                                    End If

                                End If
                                If arrRows.Length > 0 Then





                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                        'Minutes system needs to count number of minutes present, tardies and absent.
                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = arrRows(0)("Actual").ToString()
                                        DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Checked = arrRows(0)("Tardy")


                                        If arrRows(0)("Scheduled") > 0 Then
                                            DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text = "(" & Double.Parse(arrRows(0)("Scheduled") / 60).ToString("#0.00") & " hrs)"
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.Aquamarine
                                            If Not DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).ReadOnly Then
                                                DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = True
                                            End If

                                        Else
                                            DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text = ""
                                            DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = False
                                        End If


                                        'classDuration = GetScheduledMinutesForMeetDay(dtm, dr("ClsSectionId").ToString, arrRows(0)("ClsSectMeetingId").ToString)

                                        ttlScheduled += arrRows(0)("Scheduled")

                                        If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) And Not arrRows(0)("Tardy") Then
                                            ttlAbsent += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                            ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightBlue


                                        Else
                                            ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())

                                            If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                                                If arrRows(0)("Tardy") Then
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                                End If
                                                ccounter += 1
                                                If ccounter = factor Then
                                                    absences += CInt(arrRows(0)("Scheduled"))
                                                    minusPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                                    minusTardies += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                                    ccounter = 0
                                                Else
                                                    absences += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                                End If
                                                ttlTardy += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                            Else
                                                ttlMakeUp += Integer.Parse(arrRows(0)("Actual").ToString()) - arrRows(0)("Scheduled")
                                                If arrRows(0)("Actual") > arrRows(0)("Scheduled") Then
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightSalmon
                                                End If

                                            End If
                                        End If
                                        If arrRows(0)("Tardy") Then
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                        End If
                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                        'Minutes system needs to count number of minutes present, tardies and absent.
                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = Math.Round((arrRows(0)("Actual") / 60), 2)
                                        DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Checked = arrRows(0)("Tardy")


                                        If arrRows(0)("Scheduled") > 0 Then
                                            DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text = "(" & Decimal.Parse(arrRows(0)("Scheduled") / 60).ToString("#0.00") & " hrs)"
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.Aquamarine
                                            If Not DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).ReadOnly Then
                                                DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = True
                                            End If
                                        Else
                                            DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text = ""
                                            DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox).Enabled = False
                                        End If




                                        'classDuration = GetScheduledMinutesForMeetDay(dtm, dr("ClsSectionId").ToString, arrRows(0)("ClsSectMeetingId").ToString)
                                        ttlScheduled += arrRows(0)("Scheduled")
                                        ' If Integer.Parse(arrRows(0)("Actual").ToString()) = 0 Then
                                        If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) And Not arrRows(0)("Tardy") Then
                                            ttlAbsent += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                            ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightBlue


                                        Else
                                            ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())

                                            If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                                                If arrRows(0)("Tardy") Then
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                                End If
                                                ccounter += 1
                                                If ccounter = factor Then
                                                    absences += CInt(arrRows(0)("Scheduled"))
                                                    minusPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                                    minusTardies += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                                    ccounter = 0
                                                Else
                                                    absences += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                                End If
                                                ttlTardy += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                            Else
                                                ttlMakeUp += Integer.Parse(arrRows(0)("Actual").ToString()) - arrRows(0)("Scheduled")
                                                If arrRows(0)("Actual") > arrRows(0)("Scheduled") Then
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightSalmon
                                                End If
                                            End If
                                        End If
                                        If arrRows(0)("Tardy") Then
                                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                        End If
                                    Else

                                        'DE8711: NZ 11/13/12--------------------------------------------------------------------------------------------------------
                                        If arrRows(0)("Scheduled") > 0 Then
                                            'Present/Absent system so we need to translate the 0 and 1 to "A" and "P" respectively.
                                            'If the record is marked as tardy we will display a "T" instead of a "P".
                                            'If the record is marked as excused we will display an "E" instead of "A"
                                            If arrRows(0)("Actual").ToString() = "0" Then
                                                If arrRows(0)("Excused") = True Then
                                                    'possibly excused
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "E"
                                                    ttlExcused += 1
                                                Else
                                                    'absent
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "A"
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightBlue
                                                    ttlAbsent += 1
                                                End If
                                            End If

                                            If arrRows(0)("Actual").ToString() = "1" Then

                                                If arrRows(0)("Tardy") = True Then
                                                    'tardy one
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "T"
                                                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.BlanchedAlmond
                                                    ttlTardy += 1
                                                Else
                                                    'check if present on what day
                                                    If arrRows(0)("Scheduled") = 0 Then
                                                        'make up!
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightSalmon
                                                    Else
                                                        'scheduled class
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
                                                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.Aquamarine
                                                    End If
                                                    ttlPresent += 1
                                                End If
                                            End If



                                            'If arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") = True Then
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "E"
                                            '    ttlExcused += 1
                                            'ElseIf arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") <> True Then
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "A"
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.LightBlue
                                            '    ttlAbsent += 1
                                            'ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") = True Then
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "T"
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.BlanchedAlmond
                                            '    ttlTardy += 1
                                            'ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") <> True Then
                                            '    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
                                            '    If arrRows(0)("Scheduled") = 0 Then
                                            '        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.LightSalmon
                                            '    Else
                                            '        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = System.Drawing.Color.Aquamarine
                                            '    End If
                                            '    ttlPresent += 1
                                            'End If

                                        Else
                                            If arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") = False Then 'make up session
                                                DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
                                                DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).BackColor = Color.LightSalmon
                                                ttlPresent += 1
                                            Else
                                                DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = ""
                                            End If
                                        End If
                                    End If
                                    '--------------------------------------------------------------------------------------------------------
                                End If

                                If dtClsSectionMeets.Rows(idx)("UseTimeClock") And (ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS") Then
                                    Dim txtRead As TextBox = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox)
                                    txtRead.ReadOnly = True
                                    'txtRead.Enabled = False
                                    'txtRead.BackColor = System.Drawing.Color.White
                                    txtRead.Attributes.Add("onKeyDown", "preventBackspace();")
                                    Dim chkRead As CheckBox = DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox)
                                    If Not (chkRead Is Nothing) Then chkRead.Enabled = False
                                ElseIf dtClsSectionMeets.Rows(idx)("UseTimeClock") = False Then
                                    Dim txtRead As TextBox = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox)
                                    If txtRead.BackColor <> Color.LightGray Then
                                        txtRead.ReadOnly = False
                                        'txtRead.Enabled = False
                                        'txtRead.BackColor = System.Drawing.Color.White
                                        txtRead.Attributes.Remove("onKeyDown")
                                        If Not ViewState("AttendanceUnitType").ToString.ToUpper = "PRESENT ABSENT" Then
                                            If DirectCast(tbl.Rows(rowCounter - 1).Cells(cellCounter).Controls(1), Label).Text <> "" Then
                                                Dim chkRead As CheckBox = DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox)
                                                If Not (chkRead Is Nothing) Then chkRead.Enabled = True
                                            Else
                                                Dim chkRead As CheckBox = DirectCast(tbl.Rows(rowCounter + 1).Cells(cellCounter).Controls(0), CheckBox)
                                                If Not (chkRead Is Nothing) Then chkRead.Enabled = False
                                            End If
                                        End If



                                    End If

                                End If
                                Dim txtRead1 As TextBox = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox)

                                If dtm > Date.Now Then txtRead1.ReadOnly = True
                            Next

                            'totalPresentAdjusted = ttlPresent
                            'totalExcusedAdjusted = ttlExcused
                            'totalAbsenceAdjusted = ttlAbsent + ttlExcused
                            'totalTardiesAdjusted = ttlTardy

                            '***********************************************************
                            '************* Populate the AttendanceTotals table *********
                            '***********************************************************

                            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                '******************************************
                                '****** Minutes Attendance Unit Type ******
                                '******************************************
                                'Show total minutes and total hours.
                                'Actual Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Present: " & Integer.Parse(ttlPresent).ToString("#0") & " Minutes<br>("


                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours)"
                                End If
                                'Excused
                                ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = ""
                                    DirectCast(ctrl2, Label).Visible = False
                                End If
                                'Tardy
                                ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Scheduled: " & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours)"
                                End If
                                'Absent
                                ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Absent: " & (Integer.Parse(ttlAbsent) + Integer.Parse(ttlTardy)).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours)"
                                End If
                                '
                                'Adjusted Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblTtlPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If ttlPresent >= minusPresent Then
                                    ttlPresent -= minusPresent
                                End If
                                temp = "Present: " & Integer.Parse(ttlPresent).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours)" & IIf(minusPresent > 0, "*", "")
                                End If
                                'Excused

                                ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)

                                temp = "MakeUp: " & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Visible = True
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours)"
                                    'DirectCast(ctrl2, Label).Text = ""

                                End If

                                'MakeUp
                                ctrl2 = tbl.FindControl("lblTtlExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "MakeUp: " & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Visible = True
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours)"
                                    'DirectCast(ctrl2, Label).Text = ""

                                End If

                                'Scheduled
                                ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Scheduled: " & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours)"
                                End If
                                'Tardy
                                'ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString)
                                'If ttlTardy >= minusTardies Then
                                '    ttlTardy -= minusTardies
                                'End If
                                'temp = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0") & " Minutes<br>("
                                'If Not (ctrl2 Is Nothing) Then
                                '    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours)" & IIf(minusTardies > 0, "*", "")
                                'End If
                                'Absent
                                ctrl2 = tbl.FindControl("lblTtlAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If ttlPresent > 0 And ttlTardy > 0 Then
                                    ttlAbsent += absences
                                End If



                                temp = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0") & " Minutes<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours)" & IIf(absences > 0, "*", "")
                                End If

                            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                '******************************************
                                '****** Minutes Attendance Unit Type ******
                                '******************************************
                                'Show total minutes and total hours.
                                'Actual Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Present: " & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlPresent).ToString("#0") & " Minutes)"
                                End If
                                'Excused
                                'MakeUp
                                ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "MakeUp: " & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes)"
                                    DirectCast(ctrl2, Label).Visible = True
                                End If
                                'Tardy
                                'ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString)
                                'temp = "Tardy: " & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours<br>("
                                'If Not (ctrl2 Is Nothing) Then
                                '    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTardy).ToString("#0") & " Minutes)"
                                'End If


                                ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Scheduled: " & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes)"
                                End If

                                'Absent
                                ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Absent: " & ((Integer.Parse(ttlAbsent) + Integer.Parse(ttlTardy)) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlAbsent) + Integer.Parse(ttlTardy)).ToString("#0") & " Minutes)"
                                End If
                                '
                                'Adjusted Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblTtlPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If ttlPresent >= minusPresent Then
                                    ttlPresent -= minusPresent
                                End If
                                temp = "Present: " & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlPresent).ToString("#0") & " Minutes)" & IIf(minusPresent > 0, "*", "")
                                End If
                                'Excused
                                'MakeUp
                                ctrl2 = tbl.FindControl("lblTtlExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "MakeUp: " & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes)"
                                    DirectCast(ctrl2, Label).Visible = True
                                End If
                                'Tardy
                                'ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString)
                                'If ttlTardy >= minusTardies Then
                                '    ttlTardy -= minusTardies
                                'End If
                                'temp = "Tardy: " & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours<br>("
                                'If Not (ctrl2 Is Nothing) Then
                                '    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTardy).ToString("#0") & " Minutes)" & IIf(minusTardies > 0, "*", "")
                                'End If

                                ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                temp = "Scheduled: " & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes)"
                                End If


                                'Absent
                                ctrl2 = tbl.FindControl("lblTtlAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If ttlPresent > 0 And ttlTardy > 0 Then
                                    ttlAbsent += absences
                                End If
                                temp = "Absent: " & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours<br>("
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlAbsent).ToString("#0") & " Minutes)" & IIf(absences > 0, "*", "")
                                End If

                            Else

                                '******************************************
                                '*** PresentAbsent Attendance Unit Type ***
                                '******************************************
                                'Actual Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Present: " & Integer.Parse(ttlPresent).ToString("#0")
                                End If
                                'Excused
                                ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Excused: " & Integer.Parse(ttlExcused).ToString("#0")
                                End If
                                'Tardy
                                ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0")
                                End If
                                'Absent
                                ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0")
                                End If
                                '
                                'Adjusted Totals
                                'Present
                                ctrl2 = tbl.FindControl("lblTtlPresent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Present: " & Integer.Parse(ttlPresent).ToString("#0")
                                End If
                                'Excused
                                ctrl2 = tbl.FindControl("lblTtlExcused" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    DirectCast(ctrl2, Label).Text = "Excused: " & Integer.Parse(ttlExcused).ToString("#0")
                                End If
                                'Tardy
                                absences = 0
                                If ttlTardy > 0 Then
                                    absences = facade.GetAbsencesFromTardies(stuEnrollId, dr("ClsSectionId").ToString, ttlTardy)
                                End If
                                ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    If ttlTardy >= absences Then
                                        ttlTardy -= absences
                                    End If
                                    DirectCast(ctrl2, Label).Text = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0") & IIf(absences > 0, "*", "")
                                End If
                                'Absent
                                ctrl2 = tbl.FindControl("lblTtlAbsent" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                                If Not (ctrl2 Is Nothing) Then
                                    ttlAbsent += absences
                                    DirectCast(ctrl2, Label).Text = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0") & IIf(absences > 0, "*", "")
                                End If
                            End If

                            'Tardies(Message)
                            'ctrl2 = tbl.FindControl("lblTardiesMsg" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                            'If Not (ctrl2 Is Nothing) Then
                            '    If absences > 0 Then
                            '        If factor <> 0 Then
                            '            DirectCast(ctrl2, Label).Text = "* Indicates an adjustment: " & factor & " Tardies make an Absence"
                            '            DirectCast(ctrl2, Label).Visible = True
                            '        Else
                            '            DirectCast(ctrl2, Label).Visible = False
                            '        End If
                            '    Else
                            '        DirectCast(ctrl2, Label).Visible = False
                            '    End If
                            'End If
                            '
                            'Percentage
                            'ctrl2 = tbl.FindControl("lblPerc" & dr("ClsSectionId").ToString & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                            ''Dim percentage As Decimal = facade.ComputePercentageOfPresent(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                            'Dim percentage As Decimal
                            'If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                            '    'percentage = facade.ComputePercentageOfPresentforMinutes(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                            '    If ttlScheduled > 0 Then percentage = (ttlPresent / ttlScheduled) * 100
                            'ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                            '    If ttlScheduled > 0 Then percentage = (ttlPresent / ttlScheduled) * 100
                            '    'percentage = facade.ComputePercentageOfPresentforMinutes(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                            'Else
                            '    percentage = facade.ComputePercentageOfPresent(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                            'End If
                            'If Not (ctrl2 Is Nothing) Then
                            '    DirectCast(ctrl2, Label).Text = "Percentage of Present: " & percentage.ToString("#0.00") & "%"
                            'End If
                        End If
                        ttlTotalPresent += ttlPresent
                        ttlTotalExcused += ttlExcused
                        ttlTotalAbsent += ttlAbsent
                        ttlTotalTardy += ttlTardy
                        ttlTotalScheduled += ttlScheduled
                        ttlTotalMakeUp += ttlMakeUp
                        Totalabsences += absences
                        TotalminusPresent += minusPresent
                        TotalminusTardies += minusTardies
                    End If


                Next

                ''Added on Aug 15 2012
                ''the build list was failing when one of the classes didn't start yet and when we try to build the list for all terms and clsSections.
                ''So, this if statement is added to check if the table exists

                If Not (tbl Is Nothing) Then



                    'overall course attendance
                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                        '******************************************
                        '****** Minutes Attendance Unit Type ******
                        '******************************************
                        'Show total minutes and total hours.
                        'Actual Totals
                        'Present
                        ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString)
                        temp = "Present: " & Integer.Parse(ttlTotalPresent).ToString("#0") & " Minutes<br>("


                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalPresent) / 60).ToString("#0.00") & " Hours)"
                        End If
                        'Excused
                        ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = ""
                            DirectCast(ctrl2, Label).Visible = False
                        End If
                        'Tardy
                        ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString)
                        temp = "Scheduled: " & Integer.Parse(ttlTotalScheduled).ToString("#0") & " Minutes<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalScheduled) / 60).ToString("#0.00") & " Hours)"
                        End If
                        'Absent
                        'ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString)
                        'temp = "Absent: " & (Integer.Parse(ttlTotalAbsent) + Integer.Parse(ttlTotalTardy)).ToString("#0") & " Minutes<br>("
                        'If Not (ctrl2 Is Nothing) Then
                        '    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalAbsent) / 60).ToString("#0.00") & " Hours)"
                        'End If
                        ''
                        ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString)
                        temp = "Absent: " & Integer.Parse(ttlTotalAbsent) & " Minutes<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalAbsent) / 60).ToString("#0.00") & " Hours)"
                        End If
                        '
                        'Adjusted Totals
                        'Present
                        ctrl2 = tbl.FindControl("lblttlPresent" & dr("ClsSectionId").ToString)
                        If ttlTotalPresent >= TotalminusPresent Then
                            ttlTotalPresent -= TotalminusPresent
                        End If
                        temp = "Present: " & Integer.Parse(ttlTotalPresent).ToString("#0") & " Minutes<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalPresent) / 60).ToString("#0.00") & " Hours)" & IIf(TotalminusPresent > 0, "*", "")
                        End If
                        'Excused

                        ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString)

                        temp = "MakeUp: " & Integer.Parse(ttlTotalMakeUp).ToString("#0") & " Minutes<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Visible = True
                            DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalMakeUp) / 60).ToString("#0.00") & " Hours)"
                            'DirectCast(ctrl2, Label).Text = ""

                        End If

                        'MakeUp
                        ctrl2 = tbl.FindControl("lblttlExcused" & dr("ClsSectionId").ToString)
                        temp = "MakeUp: " & Integer.Parse(ttlTotalMakeUp).ToString("#0") & " Minutes<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Visible = True
                            DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalMakeUp) / 60).ToString("#0.00") & " Hours)"
                            'DirectCast(ctrl2, Label).Text = ""

                        End If

                        'Scheduled
                        ctrl2 = tbl.FindControl("lblttlTardy" & dr("ClsSectionId").ToString)
                        temp = "Scheduled: " & Integer.Parse(ttlTotalScheduled).ToString("#0") & " Minutes<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalScheduled) / 60).ToString("#0.00") & " Hours)"
                        End If
                        'Tardy
                        'ctrl2 = tbl.FindControl("lblttlTotalTardy" & dr("ClsSectionId").ToString)
                        'If ttlTotalTardy >= TotalminusTardies Then
                        '    ttlTotalTardy -= TotalminusTardies
                        'End If
                        'temp = "Tardy: " & Integer.Parse(ttlTotalTardy).ToString("#0") & " Minutes<br>("
                        'If Not (ctrl2 Is Nothing) Then
                        '    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalTardy) / 60).ToString("#0.00") & " Hours)" & IIf(TotalminusTardies > 0, "*", "")
                        'End If
                        'Absent
                        ctrl2 = tbl.FindControl("lblttlAbsent" & dr("ClsSectionId").ToString)
                        If ttlTotalPresent > 0 And ttlTotalTardy > 0 Then
                            ttlTotalAbsent += Totalabsences
                        End If



                        temp = "Absent: " & Integer.Parse(ttlTotalAbsent).ToString("#0") & " Minutes<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalAbsent) / 60).ToString("#0.00") & " Hours)" & IIf(Totalabsences > 0, "*", "")
                        End If

                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                        '******************************************
                        '****** Minutes Attendance Unit Type ******
                        '******************************************
                        'Show total minutes and total hours.
                        'Actual Totals
                        'Present
                        ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString)
                        temp = "Present: " & (Integer.Parse(ttlTotalPresent) / 60).ToString("#0.00") & " Hours<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalPresent).ToString("#0") & " Minutes)"
                        End If
                        'Excused
                        'MakeUp
                        ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString)
                        temp = "MakeUp: " & (Integer.Parse(ttlTotalMakeUp) / 60).ToString("#0.00") & " Hours<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalMakeUp).ToString("#0") & " Minutes)"
                            DirectCast(ctrl2, Label).Visible = True
                        End If
                        'Tardy
                        'ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString)
                        'temp = "Tardy: " & (Integer.Parse(ttlTotalTardy) / 60).ToString("#0.00") & " Hours<br>("
                        'If Not (ctrl2 Is Nothing) Then
                        '    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalTardy).ToString("#0") & " Minutes)"
                        'End If


                        ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString)
                        temp = "Scheduled: " & (Integer.Parse(ttlTotalScheduled) / 60).ToString("#0.00") & " Hours<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalScheduled).ToString("#0") & " Minutes)"
                        End If

                        'Absent
                        'ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString)
                        'temp = "Absent: " & ((Integer.Parse(ttlTotalAbsent) + Integer.Parse(ttlTotalTardy)) / 60).ToString("#0.00") & " Hours<br>("
                        'If Not (ctrl2 Is Nothing) Then
                        '    DirectCast(ctrl2, Label).Text = temp & (Integer.Parse(ttlTotalAbsent) + Integer.Parse(ttlTotalTardy)).ToString("#0") & " Minutes)"
                        'End If
                        ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString)
                        temp = "Absent: " & (Integer.Parse(ttlTotalAbsent) / 60).ToString("#0.00") & " Hours<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalAbsent) & " Minutes)"
                        End If
                        'Adjusted Totals
                        'Present
                        ctrl2 = tbl.FindControl("lblttlPresent" & dr("ClsSectionId").ToString)
                        If ttlTotalPresent >= TotalminusPresent Then
                            ttlTotalPresent -= TotalminusPresent
                        End If
                        temp = "Present: " & (Integer.Parse(ttlTotalPresent) / 60).ToString("#0.00") & " Hours<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalPresent).ToString("#0") & " Minutes)" & IIf(TotalminusPresent > 0, "*", "")
                        End If
                        'Excused
                        'MakeUp
                        ctrl2 = tbl.FindControl("lblttlExcused" & dr("ClsSectionId").ToString)
                        temp = "MakeUp: " & (Integer.Parse(ttlTotalMakeUp) / 60).ToString("#0.00") & " Hours<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalMakeUp).ToString("#0") & " Minutes)"
                            DirectCast(ctrl2, Label).Visible = True
                        End If
                        'Tardy
                        'ctrl2 = tbl.FindControl("lblttlTotalTardy" & dr("ClsSectionId").ToString)
                        'If ttlTotalTardy >= TotalminusTardies Then
                        '    ttlTotalTardy -= TotalminusTardies
                        'End If
                        'temp = "Tardy: " & (Integer.Parse(ttlTotalTardy) / 60).ToString("#0.00") & " Hours<br>("
                        'If Not (ctrl2 Is Nothing) Then
                        '    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalTardy).ToString("#0") & " Minutes)" & IIf(TotalminusTardies > 0, "*", "")
                        'End If

                        ctrl2 = tbl.FindControl("lblttlTardy" & dr("ClsSectionId").ToString)
                        temp = "Scheduled: " & (Integer.Parse(ttlTotalScheduled) / 60).ToString("#0.00") & " Hours<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalScheduled).ToString("#0") & " Minutes)"
                        End If


                        'Absent
                        ctrl2 = tbl.FindControl("lblttlAbsent" & dr("ClsSectionId").ToString)
                        If ttlTotalPresent > 0 And ttlTotalTardy > 0 And factor <> 0 Then
                            ttlTotalAbsent += Totalabsences
                        End If
                        temp = "Absent: " & (Integer.Parse(ttlTotalAbsent) / 60).ToString("#0.00") & " Hours<br>("
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTotalAbsent).ToString("#0") & " Minutes)" & IIf(Totalabsences > 0, "*", "")
                        End If

                    Else

                        '******************************************
                        '*** PresentAbsent Attendance Unit Type ***
                        '******************************************
                        'Actual Totals
                        'Present
                        ctrl2 = tbl.FindControl("lblActPresent" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = "Present: " & Integer.Parse(ttlTotalPresent).ToString("#0")
                        End If
                        'Excused
                        ctrl2 = tbl.FindControl("lblActExcused" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = "Excused: " & Integer.Parse(ttlTotalExcused).ToString("#0")
                        End If
                        'Tardy
                        ctrl2 = tbl.FindControl("lblActTardy" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = "Tardy: " & Integer.Parse(ttlTotalTardy).ToString("#0")
                        End If
                        'Absent
                        ctrl2 = tbl.FindControl("lblActAbsent" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = "Absent: " & Integer.Parse(ttlTotalAbsent).ToString("#0")
                        End If
                        '
                        'Adjusted Totals
                        'Present
                        ctrl2 = tbl.FindControl("lblttlTotalPresent" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = "Present: " & Integer.Parse(ttlTotalPresent).ToString("#0")
                        End If
                        'Excused
                        ctrl2 = tbl.FindControl("lblttlTotalExcused" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            DirectCast(ctrl2, Label).Text = "Excused: " & Integer.Parse(ttlTotalExcused).ToString("#0")
                        End If
                        'Tardy
                        Totalabsences = 0
                        If ttlTotalTardy > 0 Then
                            Totalabsences = facade.GetAbsencesFromTardies(stuEnrollId, dr("ClsSectionId").ToString, ttlTotalTardy)
                        End If
                        ctrl2 = tbl.FindControl("lblttlTotalTardy" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            If ttlTotalTardy >= Totalabsences Then
                                ttlTotalTardy -= Totalabsences
                            End If
                            DirectCast(ctrl2, Label).Text = "Tardy: " & Integer.Parse(ttlTotalTardy).ToString("#0") & IIf(Totalabsences > 0, "*", "")
                        End If
                        'Absent
                        ctrl2 = tbl.FindControl("lblttlTotalAbsent" & dr("ClsSectionId").ToString)
                        If Not (ctrl2 Is Nothing) Then
                            ttlTotalAbsent += Totalabsences
                            DirectCast(ctrl2, Label).Text = "Absent: " & Integer.Parse(ttlTotalAbsent).ToString("#0") & IIf(Totalabsences > 0, "*", "")
                        End If
                    End If

                    'Tardies(Message)
                    ctrl2 = tbl.FindControl("lblTardiesMsg" & dr("ClsSectionId").ToString)
                    If Not (ctrl2 Is Nothing) Then
                        If Totalabsences > 0 Then
                            If factor <> 0 Then
                                DirectCast(ctrl2, Label).Text = "* Indicates an adjustment: " & factor & " Tardies make an Absence"
                                DirectCast(ctrl2, Label).Visible = True
                            Else
                                DirectCast(ctrl2, Label).Visible = False
                            End If
                        Else
                            DirectCast(ctrl2, Label).Visible = False
                        End If
                    End If

                    ctrl2 = tbl.FindControl("lblPerc" & dr("ClsSectionId").ToString)
                    'Dim percentage As Decimal = facade.ComputePercentageOfPresent(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                    Dim percentage As Decimal
                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                        'percentage = facade.ComputePercentageOfPresentforMinutes(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                        If ttlTotalScheduled > 0 Then percentage = (ttlTotalPresent / ttlTotalScheduled) * 100
                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                        If ttlTotalScheduled > 0 Then percentage = (ttlTotalPresent / ttlTotalScheduled) * 100
                        'percentage = facade.ComputePercentageOfPresentforMinutes(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                    Else
                        percentage = facade.ComputePercentageOfPresent(ttlTotalPresent, ttlTotalAbsent, ttlTotalTardy, ttlTotalExcused)
                    End If
                    If Not (ctrl2 Is Nothing) Then
                        DirectCast(ctrl2, Label).Text = "Percentage of Present: " & percentage.ToString("#0.00") & "%"
                    End If

                End If
            End If
        Next
    End Sub
#End Region

#Region "PRINTING"
    Private Sub OpenAttendanceHTMLPrint(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btn As Button = CType(sender, Button)

            '   print to HTML
            If Not IsNothing(btn) Then

                Dim clsSectId As String = btn.ID.Substring(12)
                Dim clsSectDescrip = ""
                Dim termDescrip = ""
                Dim instructor = ""
                Dim clsDuration = ""
                Dim dtClsSections = DirectCast(ViewState("ClsSections"), DataTable)
                Dim rows() As DataRow

                rows = dtClsSections.Select("ClsSectionId='" & clsSectId & "'")
                If rows.GetLength(0) > 0 Then
                    clsSectDescrip = rows(0)("Class").ToString
                    termDescrip = rows(0)("TermDescrip").ToString
                    instructor = rows(0)("Instructor").ToString
                    clsDuration = rows(0)("ClassSectionDuration").ToString
                End If


                Dim dt As New DataTable("AttendancePrintLabels")
                dt.Columns.Add("StudentName")
                dt.Columns.Add("EnrollDescrip")
                dt.Columns.Add("TermDescrip")
                dt.Columns.Add("ClsSection")
                dt.Columns.Add("AttendanceUnitType")
                dt.Columns.Add("ClsDuration")
                dt.Columns.Add("Instructor")

                Dim dr As DataRow
                dr = dt.NewRow
                dr("StudentName") = StudentName
                dr("EnrollDescrip") = ddlStuEnrollId.SelectedItem.Text
                dr("TermDescrip") = termDescrip
                dr("ClsSection") = clsSectDescrip
                dr("AttendanceUnitType") = ViewState("AttendanceUnitType").ToString.ToUpper
                dr("ClsDuration") = clsDuration
                dr("Instructor") = instructor
                dt.Rows.Add(dr)
                ViewState("AttendancePrintLabels") = dt
                Session("AttendancePrintLabels") = dt

                Dim sb As New StringBuilder
                With sb
                    '   append StuEnrollId to the querystring
                    .Append("&StuEnrollId=" + ddlStuEnrollId.SelectedValue)
                    '   append ddlClsSectionId to the querystring
                    .Append("&ClsSectionId=" + clsSectId)
                    .Append("&PrgVerTrack=" + txtPrgVerTrackAttendance.Text)
                    .Append("&CampusId=" + CampusId)
                End With

                '   setup the properties of the new window
                Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge + ",menubar=yes, scrollbars=yes"
                Dim name As String = "StudentAttendancePrint"
                Dim url As String = "../AR/" + name + ".aspx?resid=361&mod=AR" + sb.ToString

                '   open new window and pass parameters
                CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
            End If


        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            '   Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub OpenAttendanceHTMLPrint " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub OpenAttendanceHTMLPrint " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

#End Region

#Region "Functions to get Grid  Totals"
    Private Function BuildTotalsTable(ByVal clsSectionId As String, clsSectionMeetingId As String) As Table
        Dim tblTotals As New Table
        Dim row As TableRow
        Dim ccell As TableCell
        Dim lbl As Label
        Dim prefix As String = ""
        Dim text As String
        Dim col As String = ""
        Dim width As Unit

        tblTotals.ID = "tblTotals" & clsSectionId & clsSectionMeetingId
        tblTotals.Width = Unit.Pixel(800)   'Unit.Percentage(80)
        tblTotals.CssClass = "contenttable"
        tblTotals.GridLines = GridLines.None
        tblTotals.CellSpacing = 5


        For i = 0 To 1

            row = New TableRow
            text = ""
            width = Unit.Percentage(12)

            Select Case i
                Case 0
                    prefix = "Act"      'Row contains actual totals
                Case 1
                    prefix = "Ttl"      'Row contains adjusted totals
            End Select

            For j As Integer = 0 To 5
                Select Case j
                    Case 0
                        col = "Row"     'Column contains label
                        If i = 0 Then
                            text = "Actual Totals"
                        Else
                            text = "Adjusted Totals"
                        End If
                        width = Unit.Percentage(30)
                    Case 1
                        col = "Present" 'Column contains present label
                    Case 2
                        col = "Absent"  'Column contains absent label
                    Case 3
                        col = "Tardy"   'Column contains tardy label                       
                    Case 4
                        col = "Excused" 'Column contains excused label
                    Case 5
                        col = "Btn"     'Column contains HTML print button and PDF print button
                        width = Unit.Percentage(30)
                End Select

                ccell = New TableCell
                ccell.VerticalAlign = VerticalAlign.Middle
                ccell.HorizontalAlign = HorizontalAlign.Left
                ccell.CssClass = "Label"
                ccell.Width = width

                If Not (j = 5) Then
                    'Label cell
                    lbl = New Label
                    lbl.CssClass = "Label"
                    lbl.Text = text
                    lbl.ID = "lbl" & prefix & col & clsSectionId & clsSectionMeetingId
                    ccell.Controls.Add(lbl)

                Else
                    If i = 1 Then
                        'Only on the second row
                        'PrintButtons cell
                        Dim btnHtmlPrint = New Button
                        'btnHTMLPrint.CssClass = "Button"
                        If clsSectionMeetingId <> "" Then

                        End If
                        btnHtmlPrint.Text = "Print"
                        'to hide the print button for individual meetings
                        If clsSectionMeetingId <> "" Then
                            btnHtmlPrint.Visible = False
                        End If
                        btnHtmlPrint.ID = "btnHTMLPrint" & clsSectionId & clsSectionMeetingId
                        btnHtmlPrint.Width = Unit.Pixel(50)
                        AddHandler btnHtmlPrint.Click, AddressOf OpenAttendanceHTMLPrint
                        ccell.Controls.Add(btnHtmlPrint)
                    End If
                End If

                'Add cell to row
                row.Cells.Add(ccell)
            Next
            'Add row to table
            tblTotals.Rows.Add(row)
        Next

        'row contains a adjustment message
        row = New TableRow
        ccell = New TableCell
        lbl = New Label
        lbl.ID = "lblTardiesMsg" & clsSectionId & clsSectionMeetingId
        lbl.Text = ""
        lbl.CssClass = "Label"
        ccell.VerticalAlign = VerticalAlign.Middle
        ccell.HorizontalAlign = HorizontalAlign.Left
        ccell.Controls.Add(lbl)
        ccell.CssClass = "Label"
        ccell.Width = Unit.Percentage(100)
        ccell.ColumnSpan = 6
        row.Cells.Add(ccell)
        tblTotals.Rows.Add(row)

        'row contains a percentage of presents
        row = New TableRow
        ccell = New TableCell
        lbl = New Label
        lbl.ID = "lblPerc" & clsSectionId & clsSectionMeetingId
        lbl.Text = ""
        lbl.CssClass = "Label"
        ccell.VerticalAlign = VerticalAlign.Middle
        ccell.HorizontalAlign = HorizontalAlign.Left
        ccell.Controls.Add(lbl)
        ccell.CssClass = "Label"
        ccell.Width = Unit.Percentage(100)
        ccell.ColumnSpan = 6
        row.Cells.Add(ccell)
        tblTotals.Rows.Add(row)

        'spacer row
        row = New TableRow
        ccell = New TableCell
        lbl = New Label
        lbl.Text = ""
        lbl.CssClass = "Label"
        ccell.CssClass = "spacertables"
        ccell.Controls.Add(lbl)
        row.Cells.Add(ccell)
        tblTotals.Rows.Add(row)

        Return tblTotals
    End Function
    Private Function BuildTotalsTableProgram(ByVal clsSectionId As String, clsSectionMeetingId As String) As Table
        Dim tblTotals As New Table
        Dim row As TableRow
        Dim ccell As TableCell
        Dim lbl As Label
        Dim prefix As String = ""
        Dim text As String = ""
        Dim col As String = ""
        Dim width As Unit

        tblTotals.ID = "tblTotals" & clsSectionId & clsSectionMeetingId
        tblTotals.Width = Unit.Pixel(800)   'Unit.Percentage(80)
        tblTotals.CssClass = "contenttable"
        tblTotals.GridLines = GridLines.None
        tblTotals.CellSpacing = 5

        Dim i As Integer = 0

        row = New TableRow
        text = ""
        width = Unit.Percentage(12)

        Select Case i
            Case 0
                prefix = "Act"      'Row contains actual totals

        End Select


        For j As Integer = 0 To 5
            Select Case j
                Case 0
                    col = "Row"     'Column contains label
                    If i = 0 Then
                        If clsSectionMeetingId = "" Then
                            text = "Class Section Totals"
                        Else
                            text = "Class Section Meeting Totals"
                        End If

                    Else
                        If clsSectionMeetingId = "" Then
                            text = "Class Section Adjusted Totals"
                        Else
                            text = "Class Section Meeting Adjusted Totals"
                        End If

                    End If
                    width = Unit.Percentage(20)
                Case 1
                    col = "Present" 'Column contains present label
                Case 2
                    col = "Absent"  'Column contains absent label
                Case 3
                    col = "Tardy"   'Column contains tardy label                       
                Case 4
                    col = "Excused" 'Column contains excused label
                Case 5
                    col = "Btn"     'Column contains HTML print button and PDF print button
                    width = Unit.Percentage(30)
            End Select

            ccell = New TableCell
            ccell.VerticalAlign = VerticalAlign.Middle
            ccell.HorizontalAlign = HorizontalAlign.Left
            ccell.CssClass = "Label"
            ccell.Width = width

            If Not (j = 5) Then
                'Label cell
                lbl = New Label
                lbl.CssClass = "Label"
                lbl.Text = text
                lbl.ID = "lbl" & prefix & col & clsSectionId & clsSectionMeetingId
                ccell.Controls.Add(lbl)

            Else
                If i = 0 Then
                    'Only on the second row
                    'PrintButtons cell
                    Dim btnHtmlPrint As Button = New Button
                    'btnHTMLPrint.CssClass = "Button"
                    btnHtmlPrint.Text = "Print"
                    If clsSectionMeetingId <> "" Then
                        btnHtmlPrint.Visible = False
                    End If
                    btnHtmlPrint.ID = "btnHTMLPrint" & clsSectionId & clsSectionMeetingId
                    btnHtmlPrint.Width = Unit.Pixel(50)
                    AddHandler btnHtmlPrint.Click, AddressOf OpenAttendanceHTMLPrint
                    ccell.Controls.Add(btnHtmlPrint)
                End If
            End If

            'Add cell to row
            row.Cells.Add(ccell)
        Next

        'Add row to table
        tblTotals.Rows.Add(row)
        row = New TableRow
        ccell = New TableCell
        row.Cells.Add(ccell)
        tblTotals.Rows.Add(row)
        row = New TableRow
        ccell = New TableCell
        row.Cells.Add(ccell)
        tblTotals.Rows.Add(row)
        row = New TableRow
        ccell = New TableCell
        row.Cells.Add(ccell)
        tblTotals.Rows.Add(row)
        Return tblTotals
    End Function
    Private Sub BuildTotalsTable(ByVal overallPercent As AttendancePercentageInfo, ByVal attType As String)
        Dim dtImportedAtt As DataTable
        If attType = " Minutes" Then

            LblActualPresent.Text = overallPercent.TotalPresent & attType & "<br>(" & (Integer.Parse(overallPercent.TotalPresent) / 60).ToString("#0.00") & " Hours)"
            LblActualAbsent.Text = (overallPercent.TotalAbsent + +overallPercent.TotalTardies) & attType & "<br>(" & (Integer.Parse(overallPercent.TotalAbsent + +overallPercent.TotalTardies) / 60).ToString("#0.00") & " Hours)"
            'LblActualTardy.Text = overallPercent.TotalTardies & attType & "<br>(" & (Integer.Parse(overallPercent.TotalTardies) / 60).ToString("#0.00") & " Hours)"
            LblScheduled.Text = overallPercent.TotalScheduled & attType & "<br>(" & (Integer.Parse(overallPercent.TotalScheduled) / 60).ToString("#0.00") & " Hours)"
            lblActualExcused.Visible = False
            LblAdjPresent.Text = overallPercent.TotalPresentAdjusted & attType & "<br>(" & (Integer.Parse(overallPercent.TotalPresentAdjusted) / 60).ToString("#0.00") & " Hours)"
            LblAdjAbsent.Text = overallPercent.TotalAbsentAdjusted & attType & "<br>(" & (Integer.Parse(overallPercent.TotalAbsentAdjusted) / 60).ToString("#0.00") & " Hours)" & IIf((overallPercent.TotalAbsentAdjusted > 0 And overallPercent.TardiesMakingAbsence > 0), "*", "")
            'LblAdjTardy.Text = overallPercent.TotalTardiesAdjusted & attType & "<br>(" & (Integer.Parse(overallPercent.TotalTardiesAdjusted) / 60).ToString("#0.00") & " Hours)" & IIf((overallPercent.TotalTardiesAdjusted > 0 And overallPercent.TardiesMakingAbsence > 0), "*", "")
            LblAdjScheduled.Text = overallPercent.TotalScheduled & attType & "<br>(" & (Integer.Parse(overallPercent.TotalScheduled) / 60).ToString("#0.00") & " Hours)"
            lblMakeUp.Text = overallPercent.TotalMakeUp & attType & "<br>(" & (Integer.Parse(overallPercent.TotalMakeUp) / 60).ToString("#0.00") & " Hours)"
            lblAdjMakeUp.Text = overallPercent.TotalMakeUp & attType & "<br>(" & (Integer.Parse(overallPercent.TotalMakeUp) / 60).ToString("#0.00") & " Hours)"
            lblAdjExcused.Visible = False
            lblScheduledORTardy.Text = "Scheduled:"
            lblAdjScheduledORTardy.Text = "Scheduled:"
            lblMakeUpORTardy.Text = "MakeUp:"
            lblAdjMakeUpORTardy.Text = "MakeUp:"

        ElseIf attType = " Hours" Then

            LblActualPresent.Text = (Integer.Parse(overallPercent.TotalPresent) / 60).ToString("#0.00") & attType & "<br>(" & overallPercent.TotalPresent & " Minutes" & ")"
            LblActualAbsent.Text = (Integer.Parse(overallPercent.TotalAbsent + overallPercent.TotalTardies) / 60).ToString("#0.00") & attType & "<br>(" & (overallPercent.TotalAbsent + +overallPercent.TotalTardies) & " Minutes" & ")"
            'LblActualTardy.Text = (Integer.Parse(overallPercent.TotalTardies) / 60).ToString("#0.00") & attType & "<br>(" & overallPercent.TotalTardies & " Minutes" & ")"
            LblScheduled.Text = (Integer.Parse(overallPercent.TotalScheduled) / 60).ToString("#0.00") & attType & "<br>(" & overallPercent.TotalScheduled & " Minutes" & ")"
            lblActualExcused.Visible = False
            LblAdjPresent.Text = (Integer.Parse(overallPercent.TotalPresentAdjusted) / 60).ToString("#0.00") & attType & "<br>(" & overallPercent.TotalPresentAdjusted & " Minutes" & ")"
            LblAdjAbsent.Text = (Integer.Parse(overallPercent.TotalAbsentAdjusted) / 60).ToString("#0.00") & attType & "<br>(" & overallPercent.TotalAbsentAdjusted & " Minutes" & ")" & IIf((overallPercent.TotalAbsentAdjusted > 0 And overallPercent.TardiesMakingAbsence > 0), "*", "")
            LblAdjScheduled.Text = (Integer.Parse(overallPercent.TotalScheduled) / 60).ToString("#0.00") & attType & "<br>(" & overallPercent.TotalScheduled & " Minutes" & ")"
            lblMakeUp.Text = (Integer.Parse(overallPercent.TotalMakeUp) / 60).ToString("#0.00") & attType & "<br>(" & overallPercent.TotalMakeUp & " Minutes" & ")"
            lblAdjMakeUp.Text = (Integer.Parse(overallPercent.TotalMakeUp) / 60).ToString("#0.00") & attType & "<br>(" & overallPercent.TotalMakeUp & " Minutes" & ")"
            lblAdjExcused.Visible = False
            lblScheduledORTardy.Text = "Scheduled:"
            lblAdjScheduledORTardy.Text = "Scheduled:"
            lblMakeUpORTardy.Text = "MakeUp:"
            lblAdjMakeUpORTardy.Text = "MakeUp:"

        Else
            LblActualPresent.Text = overallPercent.TotalPresent & attType
            LblActualAbsent.Text = overallPercent.TotalAbsent & attType
            LblScheduled.Text = overallPercent.TotalTardies & attType
            lblScheduledORTardy.Text = "Tardy:"
            lblActualExcused.Visible = True
            lblActualExcused.Text = "Excused: " & overallPercent.TotalExcused
            LblAdjPresent.Text = overallPercent.TotalPresentAdjusted & attType
            LblAdjAbsent.Text = overallPercent.TotalAbsentAdjusted & attType & IIf((overallPercent.TotalAbsentAdjusted > 0 And overallPercent.TardiesMakingAbsence > 0), "*", "")

            LblAdjScheduled.Text = overallPercent.TotalTardiesAdjusted & attType & IIf((overallPercent.TotalTardiesAdjusted > 0 And overallPercent.TardiesMakingAbsence > 0), "*", "")
            lblAdjScheduledORTardy.Text = "Tardy:"
            lblAdjExcused.Visible = True
            lblAdjExcused.Text = "Excused: " & overallPercent.TotalExcusedAdjusted
            lblMakeUpORTardy.Visible = False
            lblMakeUp.Visible = False
            lblAdjMakeUpORTardy.Visible = False
            lblAdjMakeUp.Visible = False
        End If
        If MyAdvAppSettings.AppSettings("UseImportedAttendance", CampusId).ToLower = "true" And overallPercent.AttType.ToUpper = "CLOCK HOURS" And overallPercent.TardiesMakingAbsence = 0 Then
            rowConverted.Visible = True
            dtImportedAtt = (New ClsSectAttendanceFacade).GetImportedAttendance_SP(ddlStuEnrollId.SelectedValue)
            If dtImportedAtt.Rows.Count > 0 Then
                LblConvScheduled.Text = dtImportedAtt.Rows(0)("Scheduled") / 60 & attType & "(" & CInt(dtImportedAtt.Rows(0)("Scheduled")) & " Minutes)"
                LblConvAbsent.Text = dtImportedAtt.Rows(0)("Absent") / 60 & attType & "(" & CInt(dtImportedAtt.Rows(0)("Absent")) & " Minutes)"
                LblConvPresent.Text = dtImportedAtt.Rows(0)("Actual") / 60 & attType & "(" & CInt(dtImportedAtt.Rows(0)("Actual")) & " Minutes)"
                If (overallPercent.TotalScheduled + dtImportedAtt.Rows(0)("Scheduled")) > 0 Then
                    lblOverallAttendance.Text = Math.Round(
                                                      (
                                                     (overallPercent.TotalPresent + dtImportedAtt.Rows(0)("Actual")) * 100
                                                     ) / (overallPercent.TotalScheduled + dtImportedAtt.Rows(0)("Scheduled")), 2)
                Else
                    lblOverallAttendance.Text = 0.0
                End If

            Else
                LblConvScheduled.Text = "0" & attType
                LblConvAbsent.Text = "0" & attType
                LblConvPresent.Text = "0" & attType
                lblOverallAttendance.Text = Math.Round(overallPercent.PercentageAttendance, 2)
            End If

        Else
            lblOverallAttendance.Text = Math.Round(overallPercent.PercentageAttendance, 2)
            lblTardy.Text = overallPercent.TardiesMakingAbsence
        End If


    End Sub
    Public Function SelectByPartOfTheValue(droplist As DropDownList, part As String) As Boolean
        'Dim found As Boolean = False
        Const retVal As Boolean = False
        Dim i As Integer
        For i = 0 To droplist.Items.Count - 1
            If droplist.Items(i).Text.Contains(part) Then
                Return True
            End If
        Next
        Return retVal
    End Function

#End Region

    Private Sub ShowAttendance(ByVal boolState As Boolean)
        If boolState = False Then
            lblAttendance.Visible = False
            pnlAttendance.Visible = False
            tblLegend.Visible = False
        Else
            lblAttendance.Visible = True
            pnlAttendance.Visible = True
            tblLegend.Visible = True
        End If
    End Sub

    Private Async Function PostPaymentPeriodAttendanceAFA(StudentEnrollmentId As Guid) As Task(Of Boolean)
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
            'Send updated to AFA if integration is enabled(handled internally)
            Return Await New PaymentPeriodHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, AdvantageSession.UserState.UserName) _
                    .PostPaymentPeriodAttendance(CampusId, StudentEnrollmentId)
        End If
        Return False
    End Function
    Protected Sub ddlClsSectionId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlClsSectionId.SelectedIndexChanged

    End Sub
End Class
