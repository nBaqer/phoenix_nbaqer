Imports Fame.Common
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class StdLOAs
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected campusId As String

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Protected resourceId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim ds As New DataSet
        Dim facade As New ProbationFacade
        Dim objCommon As New CommonUtilities
        Dim dt As New DataTable
        '        Dim m_Context As HttpContext
        Dim StdEnrollId As String = String.Empty

        'disable history button
        'Header1.EnableHistoryButton(False)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = HttpContext.Current.Request.Params("resid")
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Not Page.IsPostBack Then
            If Request.QueryString("StuEnrollId") <> "" Then
                StdEnrollId = Request.QueryString("StuEnrollId")
                ViewState("StuEnrollId") = StdEnrollId
            End If

            'Dim objcommon As New CommonUtilities
            ds = facade.GetStudentLOAs(StdEnrollId)

            With dgdDropCourse
                .DataSource = ds
                .DataBind()
            End With
            ViewState("Drop") = ds.Tables("Drop")

            InitButtonsForLoad()

        Else

        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   if GrdSystem is nothing do an insert. Else do an update
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim sEndDate As String
        Dim sStartDate As String
        Dim sId As String
        Dim sLOAReturnDate As String
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dt2 As New DataTable
        Dim sw As New System.IO.StringWriter
        Dim obj As New Object
        Dim ErrStr As String = ""
        Dim Facade As New ProbationFacade
        Dim errDates As String
        Dim Lfacade As New LOAFacade
        dt = ViewState("StudentGrades")

        'count = ds.Tables("ClsSectGrds").Rows.Count

        ' Save the datagrid items in a collection.
        iitems = dgdDropCourse.Items
        Try

            'If (count = 0) Then 'i.e if there's no student grades for this assigt/test

            'Loop thru the collection to retrieve the StudentId,score & Comments.
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                sEndDate = CType(iitem.FindControl("txtEndDate"), TextBox).Text
                sLOAReturnDate = CType(iitem.FindControl("lblreturnDate"), Label).Text
                sStartDate = CType(iitem.FindControl("txtStartDate"), TextBox).Text
                sId = CType(iitem.FindControl("txtStudentLOAId"), TextBox).Text

                'Validate that the class section start and end dates are within the term
                errDates = ValidateStartEndDates(sStartDate, sEndDate)

                If errDates <> "" Then
                    DisplayErrorMessage(errDates)
                    Exit Sub
                End If

                If Lfacade.CheckAttendanceInLDADates(ViewState("StuEnrollId"), sStartDate, DateAdd("d", 1, sEndDate)) Then
                    DisplayErrorMessage("Student Attendance has been posted between the selected LOA dates.")
                    Exit Sub
                End If

                'First check if the student has a start date
                'Validate that start date on this student enrollment is greater
                errDates = ValidateStudentStartDate(sStartDate)
                If errDates <> "" Then
                    DisplayErrorMessage(errDates)
                    Exit Sub
                End If

                If sLOAReturnDate Is DBNull.Value Or sLOAReturnDate = "" Then
                    errDates = ValidateLOAOverlappingDates(sStartDate, sEndDate, sId)
                    If errDates <> "" Then
                        DisplayErrorMessage(errDates)
                        Exit Sub
                    End If

                End If
               
                'update the student grade
                ' ErrStr &= Facade.UpdateLOAEndDate(sEndDate, sId, Session("UserName"))
                ErrStr &= Facade.UpdateLOADate(sStartDate, sEndDate, sId, Session("UserName"))


            Next

            If ErrStr = "" Then
                'DisplayErrorMessage("End Date has been changed for this student LOA.")
                DisplayErrorMessage("Record  successfully changed for this student LOA.")
            Else
                DisplayErrorMessage(ErrStr)
            End If
            'Else ' If there are already some grades posted for this assgt/test.
            'For i = 0 To iitems.Count - 1
            '    iitem = iitems.Item(i)
            '    sId = CType(iitem.FindControl("txtStudentId"), TextBox).Text
            '    iScore = CType(iitem.FindControl("txtScore"), TextBox).Text
            '    sComments = CType(iitem.FindControl("txtComments"), TextBox).Text



            '    If iScore <> "" Or sComments <> "" Then

            '        'check to see if this student exists in the posted list.
            '        Dim row2 As DataRow = dt2.Rows.Find(sId)
            '        If Not (row2 Is Nothing) Then
            '            'Move the values in the controls over to the object
            '            GrdPostingObject = PopulateGrdPostingObject(sId, iScore, sComments)

            '            'update the student grade
            '            Facade.UpdateGrdPosting(GrdPostingObject)
            '        Else
            '            'Move the values in the controls over to the object
            '            GrdPostingObject = PopulateGrdPostingObject(sId, iScore, sComments)

            '            'Add the student grade
            '            Facade.InsertGrdPosting(GrdPostingObject)
            '        End If

            '    Else
            '        'check to see if this student exists in the posted list.
            '        Dim row2 As DataRow = dt2.Rows.Find(sId)
            '        If Not (row2 Is Nothing) Then ' if student grade exists
            '            Dim GrdPostingObj As New GrdPostingsInfo

            '            'Move the values in the controls over to the object
            '            GrdPostingObj.ClsSectId = (ddlClsSection.SelectedItem.Value.ToString)
            '            GrdPostingObj.StudentId = sId
            '            GrdPostingObj.GrdBkWgtDetailId = ddlGrdCriteria.SelectedItem.Value.ToString                            'Delete the student grade
            '            Facade.DeleteGrdPosting(GrdPostingObj)
            '        End If
            '    End If
            'Next

            'End If



        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub
    Public Function ValidateStartEndDates(ByVal startDate As String, ByVal endDate As String) As String

        Dim errMsg As String = ""

        If startDate = "" Then
            errMsg &= "The Start date field is required. Please enter a valid date."
            Return errMsg
        End If
        If endDate = "" Then
            errMsg &= "The End date field is required. Please enter a valid date."
            Return errMsg
        End If
        ''Fixed by Saraswathi lakshmanan on march 21 2011
        '' Defect DE5428: QA: Mantis # 06587: Error page appears when invalid date is entered in student LOAs.
        If Not IsDate(startDate) Then
            errMsg &= "Please enter a valid Start date."
            Return errMsg
        End If

        If Not IsDate(endDate) Then
            errMsg &= "Please enter a valid End date."
            Return errMsg
        End If
        If CDate(endDate) < CDate(startDate) Then
            errMsg &= "End date on LOA must be greater than the Start date "
            Return errMsg
        End If
        Return errMsg
    End Function
    Private Function ValidateStudentStartDate(ByVal LOAStartDate As Date) As String

        Dim errMsg As String = ""
        Dim facade As New LOAFacade
        Dim StdstartDate As String

        StdstartDate = facade.GetStudentStartDate(ViewState("StuEnrollId"))

        'If the student has a start date then validate it against LOA startdate.
        If StdstartDate <> "" Then
            If LOAStartDate < CDate(StdstartDate) Then
                errMsg &= "Cannot place student with a future start date on LOA."
            End If
        End If




        Return errMsg
    End Function


    Private Function ValidateLOAOverlappingDates(ByVal LOAStartDate As String, ByVal LOAEndDate As String, ByVal LOAID As String) As String
        Dim errMsg As String = ""
        Dim facade As New LOAFacade

        errMsg = facade.AreStudentLOADatesOverlapping_GivenLOAID(ViewState("StuEnrollId"), LOAStartDate, LOAEndDate, LOAID)
        Return errMsg
    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

    Protected Function IsReadOnly(ByVal LoaReturnDate As System.Nullable(Of Date)) As Boolean
        If LoaReturnDate.HasValue Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

End Class
