﻿
Imports Fame.AdvantageV1.Common
Imports Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer

Namespace AdvWeb.AR
    Partial Class ArTranscript
        Inherits BasePage



#Region "Form Events"

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
          
        End Sub

#End Region

        Public Function GetStudentId() As String
            Dim strVID As String = Request.QueryString("VID")
            Dim state As AdvantageSessionState = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            Dim studentId As String = DirectCast(state(strVID), AdvantageStateInfo).StudentId
            Return studentId
        End Function

        Public Function GetCampusId() As String
            Dim campusId As String = Request.QueryString("cmpid").ToString.Trim
            Return campusId
        End Function


    End Class
End Namespace