﻿Imports System.Collections
Imports System.Data
Imports System.Diagnostics
Imports System.Windows.Forms
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects

Namespace AdvWeb.AR

    Partial Class StudentGradeBook
        Inherits BasePage
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme
        End Sub
        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Protected WithEvents GrdBkGradeScaleDetailsTable As DataTable
        Protected WithEvents GrdBkGradeSystemDetailsTable As DataTable
        Protected WithEvents Btnhistory As WebControls.Button

        Private pObj As New UserPagePermissionInfo

        Dim campusId As String
        Dim instructorId As String
        Dim username As String
        Dim isAcademicAdvisor As Boolean
        Protected BoolSwitchCampus As Boolean = False
        Protected MyAdvAppSettings As AdvAppSettings

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

            MyAdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim userId As String
            Dim resourceId As Integer

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            campusId = Master.CurrentCampusId
            userId = AdvantageSession.UserState.UserId.ToString


            Dim advantageUserState As New BO.User()
            advantageUserState = AdvantageSession.UserState

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If


            If MyAdvAppSettings.AppSettings("UseCohortStartDateForFilters").ToString.ToLower = "yes" Then
                trTerm.Visible = False
                trCohortStartDate.Visible = True
            Else
                trTerm.Visible = True
                trCohortStartDate.Visible = False
            End If


            If Not Page.IsPostBack Then
                'Dim objcommon As New CommonUtilities
                'Populate the Activities Datagrid with the person who is logged in Activities
                'Default the Selection Criteria for the activity records
                ViewState("Term") = ""
                ViewState("Class") = ""
                ViewState("GrdCriteria") = ""
                ViewState("MODE") = "NEW"
                ViewState("ClsSectIdGuid") = ""
                ViewState("SelectedIndex") = "-1"
                ViewState("Instructor") = userId
                PopulateTermDDL()

                ''CohortStartDate DDL 
                PopulateCohortStartDateDDL()


                ViewState("MODE") = "NEW"
                txtModUser.Text = AdvantageSession.UserState.UserName
                txtModDate.Text = Date.MinValue.ToString
                InitButtonsForLoad()
            End If
        End Sub
        Protected Sub dtlClsSectStds_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dtlClsSectStds.ItemCreated
            Dim oControl As UI.Control
            MyAdvAppSettings = AdvAppSettings.GetAppSettings()
            For Each oControl In dtlClsSectStds.Controls(0).Controls
                If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
                    If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                        CType(CType(oControl, DataGridItem).FindControl("lblFinalGrade"), WebControls.Label).Text = "Final Score"
                    Else
                        CType(CType(oControl, DataGridItem).FindControl("lblFinalGrade"), WebControls.Label).Text = "Final Grade"
                    End If
                End If
            Next
        End Sub

        Private Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTerm.SelectedIndexChanged
            'Put user code to initialize the page here
            ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
            ddlcohortStDate.SelectedIndex = 0

            ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
            If (ViewState("Term") <> "") Then
                PopulateClsSectionDDL()

            End If

        End Sub


        Private Sub PopulateTermDDL()

            Dim facade As New GrdPostingsFacade
            'We want to first populate the ddlTerms control
            '' issue 14296: Instructor Supervisors and Education Directors(AcademicAdvisors)  
            ''need to be able to post and modify attendance. 
            '' To Find if the User is a Academic Advisors

            Dim facClsSectAtt As New ClsSectAttendanceFacade

            username = AdvantageSession.UserState.UserName
            instructorId = CType(ViewState("Instructor"), String)
            'isAcademicAdvisor = facClsSectAtt.GetIsRoleAcademicAdvisor(instructorId)
            isAcademicAdvisor = facClsSectAtt.GetIsRoleAcademicAdvisorORInstructorSuperVisor(instructorId)
            ViewState("isAcademicAdvisor") = isAcademicAdvisor
            campusId = Master.CurrentCampusId
            With ddlTerm
                .DataSource = facade.GetCurrentAndPastTermsForAnyUserbyRoles(instructorId, username, isAcademicAdvisor, campusId)
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub PopulateClsSectionDDL()

            username = AdvantageSession.UserState.UserName
            instructorId = CType(ViewState("Instructor"), String)
            isAcademicAdvisor = CType(ViewState("isAcademicAdvisor"), Boolean)
            campusId = Master.CurrentCampusId

            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New StdGrdBkFacade

            ' Bind the Dataset to the CheckBoxList
            With ddlClsSection
                .DataSource = facade.GetClsSectionsbyUserandRoles(CType(ViewState("Term"), String), instructorId, campusId, username, isAcademicAdvisor)
                .DataTextField = "CourseSectDescrip"
                .DataValueField = "ClsSectionId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            dtlClsSectStds.DataSource = Nothing
            dtlClsSectStds.DataBind()
            ClearRhSandHide()

        End Sub

        Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
            Dim ds As New DataSet

            ''To add the cohortStart date 
            ''If term or cohortStDate are not selected then, classSection cannot be fetched.

            If ddlTerm.SelectedItem.Text = "Select" And ddlcohortStDate.SelectedItem.Text = "Select" Then
                'DisplayErrorMessage("Unable to find data. Please select an option.")
                DisplayRADAlert(CallbackType.Postback, "Error1", "Unable to find data. Please select an option.", "Term Not Entered Error")
                Exit Sub
            ElseIf ddlTerm.SelectedItem.Text <> "Select" Then
                ViewState("Term") = ddlTerm.SelectedItem.Value.ToString
            ElseIf ddlcohortStDate.SelectedItem.Text <> "Select" Then
                ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
            End If

            If ddlClsSection.SelectedItem.Text = "Select" Then
                'DisplayErrorMessage("Unable to find data. Please select an option from Class Section")
                DisplayRADAlert(CallbackType.Postback, "Error2", "Unable to find data. Please select an option from Class Section", "Class Section Error")
                Exit Sub
            Else
                ViewState("ClsSection") = ddlClsSection.SelectedItem.Value.ToString
            End If

            'Populate data-list with values
            PopulateDataList("Build")
            ClearRhs()
            pnlHeader.Visible = False
            label1.Visible = False
            label2.Visible = False
            txtFinalScore.Visible = False
            txtFinalGrade.Visible = False
            lblStdName.Text = ""
            lblSecName.Text = ""
            txtFinalScore.Text = ""
            txtFinalGrade.Text = ""
            'clear contents of the data-grid
            With dgdStdGrdBk
                .DataSource = Nothing
                .DataBind()
            End With
            ViewState("StdGrdBk") = ds
            InitButtonsForLoad()
        End Sub

        Public Sub ClearRhSandHide()
            ClearRhs()
            pnlHeader.Visible = False
            label1.Visible = False
            label2.Visible = False
            txtFinalScore.Visible = False
            txtFinalGrade.Visible = False
            lblStdName.Text = ""
            lblSecName.Text = ""
            txtFinalScore.Text = ""
            txtFinalGrade.Text = ""
        End Sub


        Private Sub PopulateDataList(ByVal callingFunction As String, Optional ByVal guidToFind As String = "", Optional ByVal ds As DataSet = Nothing)
            Dim dv As New DataView
            Dim ds2 As New DataSet

            If ds Is Nothing Then
                Dim facade As New StdGrdBkFacade
                ds2 = facade.PopulateDataList(CType(ViewState("ClsSection"), String))

                'Filter to not repeat the same student
                If ds2.Tables(0).Rows.Count > 0 Then
                    Dim clean = ds2.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Guid)("StudentId")).[Select](Function(g) g.First())
                    Dim cleanDt As DataTable = clean.CopyToDataTable()
                    ds2.Tables(0).Clear()
                    ds2.Tables(0).Merge(cleanDt)
                End If
            Else
            End If

            With dtlClsSectStds
                Select Case callingFunction
                    Case "Delete"
                        dtlClsSectStds.SelectedIndex = -1
                    Case "New"
                        Dim myDrv As DataRowView
                        Dim i As Integer
                        Dim guidInDv As String
                        For Each myDrv In dv
                            guidInDv = myDrv(0).ToString
                            If guidToFind.ToString = guidInDv Then
                                Exit For
                            End If
                            i += 1
                        Next
                        dtlClsSectStds.SelectedIndex = i
                        ViewState("ItemSelected") = i

                    Case "Sort"
                        If CInt(ViewState("ItemSelected")) > -1 Then
                            Dim myDrv As DataRowView
                            Dim i As Integer
                            Dim guidInDv As String
                            For Each myDrv In dv
                                guidInDv = myDrv(0).ToString
                                If guidToFind.ToString = guidInDv Then
                                    Exit For
                                End If
                                i += 1
                            Next
                            dtlClsSectStds.SelectedIndex = i
                            ViewState("ItemSelected") = i
                        Else
                            dtlClsSectStds.SelectedIndex = -1
                            ViewState("ItemSelected") = -1
                        End If
                    Case "Edit"
                        dtlClsSectStds.SelectedIndex = CInt(ViewState("ItemSelected"))
                    Case "Item"
                        dtlClsSectStds.SelectedIndex = CInt(ViewState("ItemSelected"))
                    Case "Build"
                        dtlClsSectStds.SelectedIndex = -1
                    Case "Add"
                        dtlClsSectStds.SelectedIndex = -1
                    Case Else
                        dtlClsSectStds.SelectedIndex = -1
                End Select
                .DataSource = ds2
                .DataBind()
            End With
        End Sub

        Private Sub ClearRhs()
            Dim ctl As UI.Control
            Try
                '***************************************************************
                'This section clears the Field Properties section of the page
                '***************************************************************
                For Each ctl In pnlRHS.Controls
                    If ctl.GetType Is GetType(WebControls.TextBox) Then
                        CType(ctl, WebControls.TextBox).Text = ""
                    End If
                    If ctl.GetType Is GetType(WebControls.CheckBox) Then
                        CType(ctl, WebControls.CheckBox).Checked = False
                    End If
                    If ctl.GetType Is GetType(DropDownList) Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                    End If
                Next

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub



        Private Sub dtlClsSectStds_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtlClsSectStds.SelectedIndexChanged

        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            '   if GrdSystem is nothing do an insert. Else do an update
            'Dim iitems As DataGridItemCollection
            'Dim iitem As DataGridItem
            Dim i As Integer
            Dim stuEnrollId As String
            'Dim iScore As String
            'Dim sComments As String
            Dim ds As DataSet
            'Dim dt1 As DataTable
            'Dim stdGrdBkObj As StdGrdBkInfo
            '  Dim count As Integer
            'Dim facade As New StdGrdBkFacade
            'Dim grdbkcriteria As String
            'Dim errmsg As String = ""
            'Dim result As String

            ds = CType(Session("GrdBkCriteria"), DataSet)
            Dim DA As New GradePostingDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            '********************************************************************
            'This if statement was added to fix the issue where an error pg is 
            'received upon an "empty" save.
            '********************************************************************
            'If Not IsNothing(ds) Then
            '    dt1 = ds.Tables("GrdBkCriteria")
            '    '  count = ds.Tables("GrdBkCriteria").Rows.Count
            'End If

            'Get the parameter to create the object to process the information 
            stuEnrollId = CType(ViewState("StuEnrollId"), String)
            Dim classid As String = CType(ViewState("ClsSection"), String)

            'Get the actual list of scores show in the data-grid in page (possibility modified by the user...)
            Dim listOfCriteriasInScreen = New List(Of GrdRecordsPoco)
            Dim comp As DataGridItemCollection = dgdStdGrdBk.Items
            For i = 0 To comp.Count - 1
                Dim info As New GrdRecordsPoco()
                Dim sco = CType(comp(i).FindControl("txtScore"), WebControls.TextBox).Text.Trim

                Dim scobo As Boolean = String.IsNullOrEmpty(sco)    ' If you use the string.isnullOrEmpty in the If return 0 instead Nothing. I love VB :-(
                If scobo Then
                    info.Score = Nothing
                    info.DateCompleted = Nothing
                Else
                    info.Score = CType(sco, Decimal)
                    Dim enrollment = DA.GetEnrollmentStartDate(New Guid(stuEnrollId))
                    Dim enrollStartDate As Date = Convert.ToDateTime(enrollment(0))
                    Dim dateCompleted = CType(comp(i).FindControl("rdpDateCompleted"), RadDatePicker).SelectedDate
                    If (String.IsNullOrEmpty(Convert.ToString(dateCompleted))) Then
                        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "Date completed is required.")
                        Exit Sub
                    ElseIf (dateCompleted > DateTime.Now.Date Or dateCompleted < enrollStartDate) Then
                        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "Date Completed is Invalid.")
                        Exit Sub
                    Else
                        info.DateCompleted = CType(dateCompleted, DateTime)
                    End If
                End If
                If (info.Score > 100) Then
                    Throw New ArgumentOutOfRangeException(info.Score.ToString(), "Score cannot exceed 100")
                End If
                info.ResNum = 0  'NOTE: ? Mining unknown always 0 in the table 
                'info.GrdBkResultId = CType(comp(i).FindControl("txtGrdCritId"), TextBox).Text
                info.Comments = CType(comp(i).FindControl("txtComments"), WebControls.TextBox).Text
                info.Descrip = CType(comp(i).FindControl("GrdCriteria"), WebControls.Label).Text
                info.InstrGrdBkWgtDetailId = CType(comp(i).FindControl("txtGrdCritId"), WebControls.TextBox).Text
                info.GrdBkResultId = CType(comp(i).FindControl("txtGrdBkResultId"), WebControls.TextBox).Text
                listOfCriteriasInScreen.Add(info)
            Next

            'Store values in registerScoreObj
            Dim registerScoreObj As ArRegisterScoreInfo = New ArRegisterScoreInfo(stuEnrollId, classid)
            'registerScoreObj.CriteriasInDbList = listOfCriteriasInDb
            registerScoreObj.RecordsInScreenList = listOfCriteriasInScreen
            registerScoreObj.CampusId = campusId
            registerScoreObj.UserName = AdvantageSession.UserState.UserName
            registerScoreObj.IsInCompletedGraded = chkIsIncomplete.Checked

            Try
                'Do the Score ...
                registerScoreObj.ExecuteScoreUpdate()

            Catch exec As ArgumentOutOfRangeException
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(exec)

                DisplayRADAlert(CallbackType.Postback, "Error 5", exec.Message, "Save Error")
                Return
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Session("Error") = "Error When Save" & ex.Message
                Response.Redirect("../ErrorPage.aspx")
            End Try

            ' Save the data-grid items in a collection.

            'iitems = dgdStdGrdBk.Items
            'Try


            '    For i = 0 To iitems.Count - 1
            '        iitem = iitems.Item(i)

            '        iScore = CType(iitem.FindControl("txtScore"), TextBox).Text.Trim

            '        If iScore <> "" Then
            '            If (iScore) > 100 Then
            '                errmsg = "Score cannot exceed 100"
            '            End If
            '        End If


            '        If errmsg <> "" Then
            '            'DisplayErrorMessage(errmsg)
            '            DisplayRADAlert(CallbackType.Postback, "Error5", errmsg, 350, 100, "Save Error")
            '            Exit Sub
            '        Else

            'sComments = CType(iitem.FindControl("txtComments"), TextBox).Text
            'grdbkcriteria = CType(iitem.FindControl("txtGrdCritId"), TextBox).Text


            'If iScore <> "" Or sComments <> "" Then

            '    'check to see if this grade exists in the posted list.
            '    Dim row2 As DataRow = dt1.Rows.Find(grdbkcriteria)
            '    If Not row2.IsNull("Score") Or Not row2.IsNull("Comments") Then

            '        'Update row in the datatable.
            '        row2("Score") = iScore
            '        row2("Comments") = sComments

            '        stdGrdBkObj = PopulateStdGrdBkObject(stuEnrollId, iScore, sComments, grdbkcriteria)
            '        stdGrdBkObj.IsCompGraded = True

            '        facade.UpdateIncompleteGrade(stdGrdBkObj, AdvantageSession.UserState.UserName)

            'result = facade.UpdateStudentGrade(stdGrdBkObj, AdvantageSession.UserState.UserName)
            '        If Not result = "" Then
            '            DisplayRADAlert(CallbackType.Postback, "Error6", result, 350, 100, "Save Error")
            '        End If
            '    Else

            '        'Update row in the datatable.
            '        row2("Score") = iScore
            '        row2("Comments") = sComments

            '        stdGrdBkObj = PopulateStdGrdBkObject(stuEnrollId, iScore, sComments, grdbkcriteria)
            '        stdGrdBkObj.GrdBkResultId = Guid.NewGuid.ToString()
            '        stdGrdBkObj.IsCompGraded = True

            'facade.UpdateIncompleteGrade(stdGrdBkObj, AdvantageSession.UserState.UserName)

            'result = facade.InsertStudentGrade(stdGrdBkObj, AdvantageSession.UserState.UserName)
            'If Not result = "" Then
            '    DisplayRADAlert(CallbackType.Postback, "Error7", result, 350, 100, "Save Error")
            'End If
            'row2("GrdBkResultId") = stdGrdBkObj.GrdBkResultId
            '    End If

            'Else
            ''check to see if this result exists in the posted list.
            'Dim row2 As DataRow = dt1.Rows.Find(grdbkcriteria)
            'If Not row2.IsNull("Score") Or Not row2.IsNull("Comments") Then ' if student grade exists in the table but not the datagrid

            '    'Update row in datatable
            '    row2("Score") = Nothing
            '    row2("Comments") = Nothing

            '    Dim stdGrdBkObj1 As New StdGrdBkInfo

            '    stdGrdBkObj1.GrdBkResultId = row2("GrdBkResultId").ToString

            '    result = facade.DeleteStudentGrade(stdGrdBkObj1)
            '    If Not result = "" Then
            '        DisplayRADAlert(CallbackType.Postback, "Error8", result, 350, 100, "Save Error")
            '    End If
            'End If
            'End If
            '        End If
            '    Next

            'Dim stdGrdBkObj2 As New StdGrdBkInfo

            'Move the values in the controls over to the object
            'stdGrdBkObj2.ClassId = (ddlClsSection.SelectedItem.Value.ToString)
            'stdGrdBkObj2.StuEnrollId = CType(ViewState("StuEnrollId"), String)
            'If (chkIsIncomplete.Checked = True) Then
            '    stdGrdBkObj2.IsIncomplete = 1
            'Else
            '    stdGrdBkObj2.IsIncomplete = 0
            'End If
            ''If (chkIsIncomplete.Checked = True) Then
            'facade.UpdateIncompleteGrade(stdGrdBkObj2, AdvantageSession.UserState.UserName)
            ''End If

            Session("GrdBkCriteria") = ds

            '********************************************************************
            'The following if statement was added to fix the issue where an error pg is 
            'received upon an "empty" save.
            '********************************************************************
            'If Not IsNothing(ds) Then
            DisplayScoreGrade()
            'End If

            'Catch ex As Exception
            '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    'Redirect to error page.
            '    If ex.InnerException Is Nothing Then
            '        Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            '    Else
            '        Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            '    End If
            '    Response.Redirect("../ErrorPage.aspx")
            'End Try
        End Sub

        'Private Function PopulateStdGrdBkObject(ByVal stuEnrollId As String, ByVal score As String, ByVal comments As String, ByVal grdBkCritId As String) As StdGrdBkInfo
        '    Dim stdGrdBkObj As New StdGrdBkInfo

        '    stdGrdBkObj.ClassId = (ddlClsSection.SelectedItem.Value.ToString)
        '    stdGrdBkObj.StuEnrollId = stuEnrollId
        '    stdGrdBkObj.GrdBkWgtDetailId = grdBkCritId
        '    If score <> "" Then
        '        stdGrdBkObj.Score = CType(score, Double)

        '    End If
        '    stdGrdBkObj.Comments = comments
        '    'get ModUser
        '    stdGrdBkObj.ModUser = txtModUser.Text

        '    'get ModDate
        '    stdGrdBkObj.ModDate = Date.Parse(txtModDate.Text)
        '    If (chkIsIncomplete.Checked = True) Then
        '        stdGrdBkObj.IsIncomplete = 1
        '    Else
        '        stdGrdBkObj.IsIncomplete = 0
        '    End If

        '    Return stdGrdBkObj
        'End Function

        Private Sub PopulateHeader(ByVal fullname As String)
            '        Dim intpos As Integer


            pnlHeader.Visible = True
            lblStdName.Text = fullname
            lblSecName.Text = ddlClsSection.SelectedItem.Text
        End Sub

        Private Sub DisplayScoreGrade()

            Dim ds As DataSet
            Dim totalscore As Decimal
            Dim grdsclId As String
            Dim weightsum As Decimal 'This was originally integer. Changed for GrdBkWeights allowing decimals.
            Dim scoreToGrade As Decimal
            Dim gradeRounding As String = MyAdvAppSettings.AppSettings("GradeRounding").ToString.ToLower()
            label1.Visible = True
            label2.Visible = True
            txtFinalScore.Visible = True
            txtFinalGrade.Visible = True

            'Get Grade Scale and Grade System details that are attached to this class section
            ds = (New StdGrdBkFacade).GetGrdScaleSystemDetails(CType(ViewState("ClsSection"), String), CType(ViewState("Instructor"), String), CType(ViewState("Term"), String))

            GrdBkGradeScaleDetailsTable = ds.Tables("GradeScaleDetails")
            GrdBkGradeSystemDetailsTable = ds.Tables("GradeSystemDetails")

            Dim comp As DataGridItemCollection = dgdStdGrdBk.Items
            For i = 0 To comp.Count - 1
                'Check for score
                Dim sco = CType(comp(i).FindControl("txtScore"), WebControls.TextBox).Text.Trim
                Dim scobo As Boolean = String.IsNullOrEmpty(sco)
                If scobo Then
                    'Null or Nothing so no action needed
                Else
                    Dim weight = CType(comp(i).FindControl("lblWeight"), WebControls.Label).Text.Trim
                    weightsum += CType(weight, Decimal)

                    totalscore += (CType(sco, Decimal) * CType(weight, Decimal) / 100)
                End If
            Next

            '   add score
            If weightsum <> 0 Then
                If gradeRounding = "yes" Then
                    txtFinalScore.Text = CType(Math.Round((totalscore / weightsum) * 100), String)
                Else
                    txtFinalScore.Text = CType(Math.Round((totalscore / weightsum) * 100, 2), String)
                End If
                'Math.Round((totalscore / weightsum) * 100, 2)
            Else
                txtFinalScore.Text = ""
            End If

            If weightsum <> 0 Then
                'Round off score before converting to grade. (for example: round off 89.75 to 90)
                'scoreToGrade = Math.Round((totalscore / weightsum) * 100, 0)

                If gradeRounding = "yes" Then
                    scoreToGrade = Math.Round((totalscore / weightsum) * 100)
                Else
                    scoreToGrade = Math.Round((totalscore * 100) / weightsum, 2)
                End If

            End If

            '     get grade-scale id
            If txtFinalScore.Text <> "" Then
                grdsclId = (New StdGrdBkFacade).GetGrdScaleId(CType(ViewState("ClsSection"), String))
                txtFinalGrade.Text = ConvertScoreToGrade(scoreToGrade, grdsclId)
            Else
                txtFinalGrade.Text = ""
            End If

        End Sub

        Private Function ConvertScoreToGrade(ByVal score As Decimal, ByVal grdScaleId As String) As String
            '   filter GradeScaleDetails table by GrdScaleId
            Dim rows() As DataRow = GrdBkGradeScaleDetailsTable.Select("GrdScaleId=" + "'" + grdScaleId + "'")

            '   loop only if you have rows
            Dim i As Integer
            Dim boolScoreFallsInGradeRange As Boolean = False
            If rows.Length > 0 Then
                '   loop throughout all the GradeScale
                For i = 0 To rows.Length - 1
                    '   check if the score is between MinVal and MaxVal values
                    If score >= CType(rows(i)("MinVal"), Decimal) And score <= CType(rows(i)("MaxVal"), Decimal) Then
                        '   return corresponding Grade
                        Return CType(rows(i).GetParentRow("GradeSystemDetailsGradeScaleDetails")("Grade"), String)
                    End If
                Next
                'code modified by balaji on 04/04/2009 to fix issue 15792
                'Modification starts here
                If boolScoreFallsInGradeRange = False Then
                    If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                        score = Math.Round(score, 0)
                    Else
                        score = Math.Floor(score)
                    End If

                    For i = 0 To rows.Length - 1
                        '   check if the score is between MinVal and MaxVal values
                        If score >= CType(rows(i)("MinVal"), Decimal) And score <= CType(rows(i)("MaxVal"), Decimal) Then
                            '   return corresponding Grade
                            Return CType(rows(i).GetParentRow("GradeSystemDetailsGradeScaleDetails")("Grade"), String)
                        End If
                    Next
                End If
                'code modified by balaji on 04/04/2009 to fix issue 15792
                'Modification Ends here
            End If
            'the score does not have a corresponding Grade
            Return ""
        End Function



        Private Sub dtlClsSectStds_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dtlClsSectStds.ItemCommand
            Dim ds As DataSet
            Dim facade As New StdGrdBkFacade
            Dim dt As DataTable

            Dim stuEnrollment As String
            Dim fullname As LinkButton
            '  Dim selIndex As Integer
            Dim grade As LinkButton
            Dim incomplete As LinkButton

            chkIsIncomplete.Checked = False

            ViewState("ItemSelected") = e.Item.ItemIndex.ToString
            stuEnrollment = dtlClsSectStds.DataKeys(e.Item.ItemIndex).ToString
            ViewState("StuEnrollId") = stuEnrollment

            InitButtonsForEdit()

            'Populate data-list with values
            PopulateDataList("Item")

            fullname = CType(e.Item.FindControl("Linkbutton1"), LinkButton)
            grade = CType(e.Item.FindControl("Linkbutton3"), LinkButton)
            incomplete = CType(e.Item.FindControl("Linkbutton4"), LinkButton)

            If incomplete.Text = "True" Then
                chkIsIncomplete.Checked = True
            End If

            ''show the Grade details to the academic Advisor and to allow them to modify the grades
            isAcademicAdvisor = CType(ViewState("isAcademicAdvisor"), Boolean)
            ds = facade.PopulateDataGridOnItemCmd(CType(ViewState("StuEnrollId"), String), CType(ViewState("ClsSection"), String), fullname.Text, CType(ViewState("Instructor"), String), AdvantageSession.UserState.UserName, isAcademicAdvisor)

            dt = ds.Tables("GrdBkCriteria")

            PopulateHeader(fullname.Text)
            Dim bindingSource As New BindingSource
            bindingSource.DataSource = dt
            With dgdStdGrdBk
                .DataSource = dt
                .DataBind()
            End With

            If (dt.Rows.Count > 0) Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim datePicker As RadDatePicker = dgdStdGrdBk.Items(i).FindControl("rdpDateCompleted")
                    If (dt.Rows(i)("DateCompleted").ToString() <> String.Empty) Then
                        datePicker.SelectedDate = Convert.ToDateTime(dt.Rows(i)("DateCompleted").ToString())
                    End If
                Next
            End If

            If Trim(grade.Text) = "I" Then
                'DisplayErrorMessage("Student has an Incomplete Grade for the course")
                DisplayRADAlert(CallbackType.Postback, "Error8", "Student has an Incomplete Grade for the course", "Save Error")
            ElseIf grade.Text = "W" Then
                'DisplayErrorMessage("Student has Withdrawn from the course")
                DisplayRADAlert(CallbackType.Postback, "Error9", "Student has Withdrawn from the course", "Save Error")
            End If

            Session("GrdBkCriteria") = ds

            DisplayScoreGrade()
        End Sub

        Private Sub dtlClsSectStds_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dtlClsSectStds.ItemDataBound

            'Dim linkbtn As LinkButton
            'Dim linkbtn2 As LinkButton

            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem

                    'linkbtn = CType(e.Item.FindControl("Linkbutton2"), LinkButton)


                    'linkbtn2 = CType(e.Item.FindControl("Linkbutton5"), LinkButton)
                    'StudentId = CType(iitem.FindControl("Linkbutton5"), LinkButton).Text
                    If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                        'GetInputMaskValue(linkbtn)
                        dtlClsSectStds.Columns(2).Visible = False
                    ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier") = "StudentNumber" Then
                        dtlClsSectStds.Columns(1).Visible = False
                    End If

                    'Select DDL if grade exists for student in "ClsSectStds" datatable.
            End Select
        End Sub
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add buttons 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnBuildList)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
            BindToolTip("ddlTerm")
            BindToolTip("ddlClsSection")

        End Sub

        Private Sub PopulateClsSectionDdLbasedonCohortStDate()
            username = AdvantageSession.UserState.UserName
            instructorId = CType(ViewState("Instructor"), String)
            isAcademicAdvisor = CType(ViewState("isAcademicAdvisor"), Boolean)
            campusId = Master.CurrentCampusId

            '   Get Degrees data to bind the CheckBoxList
            '
            Dim facade As New StdGrdBkFacade

            ' Bind the Dataset to the CheckBoxList
            With ddlClsSection
                .DataSource = facade.GetClsSectionsbyUserandRolesandCohortStDt(CType(ViewState("CohortStartDate"), String), instructorId, campusId, username, isAcademicAdvisor)
                .DataTextField = "CourseSectDescrip"
                .DataValueField = "ClsSectionId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            dtlClsSectStds.DataSource = Nothing
            dtlClsSectStds.DataBind()
            ClearRhSandHide()

        End Sub

        Private Sub PopulateCohortStartDateDDL()
            Dim facade As New GrdPostingsFacade
            username = AdvantageSession.UserState.UserName
            instructorId = CType(ViewState("Instructor"), String)
            isAcademicAdvisor = CType(ViewState("isAcademicAdvisor"), Boolean)
            campusId = Master.CurrentCampusId

            With ddlcohortStDate
                .DataSource = facade.GetCohortStartDateforCurrentAndPastTermsForAnyUserbyRoles(instructorId, username, isAcademicAdvisor, campusId)
                .DataTextField = "CohortStartDate"
                .DataValueField = "CohortStartDate"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Protected Sub ddlcohortStDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlcohortStDate.SelectedIndexChanged
            ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
            ddlTerm.SelectedIndex = 0
            ViewState("Term") = ddlTerm.SelectedItem.Value.ToString()
            If (ViewState("CohortStartDate") <> "") Then
                PopulateClsSectionDdLbasedonCohortStDate()
            End If
        End Sub


        Private Sub InitButtonsForLoad()
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
        End Sub
        Private Sub InitButtonsForEdit()

            If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If
            btnNew.Enabled = False
            btnDelete.Enabled = False

        End Sub
    End Class
End Namespace