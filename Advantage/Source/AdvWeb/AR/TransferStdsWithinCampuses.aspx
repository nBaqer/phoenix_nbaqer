﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="TransferStdsWithinCampuses.aspx.vb" Inherits="AR_TransferStdsWithinCampuses" %>

<%@ Register TagPrefix="fame" TagName="StudentBar" Src="~/UserControls/StudentBar.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <table width="50%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="label1" runat="server" CssClass="label">Program Version to transfer to</asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:DropDownList ID="ddlFromPrgVerId" Width="370px" runat="server" CssClass="dropdownlist" AutoPostBack="True"></asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="label3" runat="server" CssClass="label">Campus to Transfer to</asp:Label></td>
                                                <td class="contentcell4" align="left">
                                                    <asp:DropDownList ID="ddlCampusId" Width="370px" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                            </tr>


                                            <tr>
                                                <td class="contentcell"></td>
                                                <td class="contentcell4">
                                                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tr>
                                                <td style="padding: 10px 0 10px 0">
                                                    <asp:DataGrid ID="dgrdStuEnrollments" runat="server" Width="95%" AutoGenerateColumns="False" BorderWidth="1px" BorderStyle="Solid" BorderColor="#ebebeb">
                                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                        <FooterStyle CssClass="label"></FooterStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Student">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStudentName" CssClass="label" runat="server" Text='<%# Container.DataItem("StudentName") %>' Width="180px" />
                                                                    <asp:Label ID="lblStuEnrollId" CssClass="label" runat="server" Visible="False" Text='<%# Container.DataItem("StuEnrollId") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Current Program Version">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPrgVer" CssClass="label" runat="server" Text='<%# Container.DataItem("PrgVerDescrip")%>' Width="80px" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="SSN">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSSN" CssClass="label" runat="server" Text='<%# Container.DataItem("SSN")%>' Width="80px" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Status">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStatusCode" CssClass="label" runat="server" Text='<%# Container.DataItem("StatusCodeDescrip") %>' Width="120px" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Start Date">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStartDate" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate", "{0:d}") %>' Width="80px" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Expected Graduation Date">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExpGradDate" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ExpGradDate", "{0:d}") %>' Width="80px" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn>
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <HeaderTemplate>
                                                                    <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkTransfer', document.forms[0].chkAllItems.checked)" />Select 
																	All
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkTransfer" runat="server" Text='' Width="100px" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--end table content-->
                                    </div>
                                    <asp:TextBox ID="txtClsSectionId1" runat="server" CssClass="label" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>

