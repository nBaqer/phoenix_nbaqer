﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SummaryGradePostings.aspx.vb" Inherits="AdvWeb.AR.SummaryGradePostings" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="250px" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <asp:Panel ID="panFilter" runat="server" Style="padding-left: 10px;">
                <table id="Table2" class="maincontenttable">
                    <tr>
                        <td class="listframetop2">
                            <br />
                            <table class="maincontenttable" align="center">

                                <%--<tr id="trTerm" runat="server">--%>
                                <tr id="trlblTerm" runat="server">
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblTerm" runat="server" CssClass="label">Term</asp:Label>

                                    </td>
                                </tr>
                                <tr id="trTerm" runat="server">
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>


                                <tr>
                                    <td>OR
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblStudentGroup" runat="server" CssClass="label">Student Group</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddStudentGroup" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <%--<tr id="trCohortStartDate" runat="server">--%>
                                <tr id="trlblCohortStartDate" runat="server">
                                    <td class="contentcellblue">
                                        <asp:Label ID="label3" runat="server" CssClass="label">Cohort Start Date</asp:Label>
                                    </td>
                                </tr>
                                <tr id="trCohortStartDate" runat="server">
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlcohortStDate" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr id="trlblShift" runat="server">
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblShift" runat="server" CssClass="label">Shift (Optional)</asp:Label>
                                    </td>
                                </tr>
                                <tr id="trlblShiftFilter" runat="server">
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>


                                <tr id="rdpMinimumClassStart" runat="server">
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblMinimumClassStartDate" runat="server" CssClass="label">Class Starting From Date (Optional)</asp:Label>
                                    </td>
                                </tr>
                                <tr id="rdpMinimumClassStartFilter" runat="server">
                                    <td class="contentcell4blue">
                                        <telerik:RadDatePicker ID="rdpMinimumClassStartDate" MinDate="1/1/1945" runat="server" AutoPostBack="true" Width="200px">
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblClsSection" runat="server" CssClass="label">Class</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlClsSection" runat="server" AutoPostBack="True" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblGrdCriteria" runat="server" CssClass="label">Item To Grade</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlGrdCriteria" runat="server" CssClass="dropdownlist" AutoPostBack="true" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblMaxNum" runat="server" CssClass="label" Visible="false">Number</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell4blue">
                                        <asp:DropDownList ID="ddlMaxNum" runat="server" CssClass="dropdownlist" Visible="false" AutoPostBack="true" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="contentcell4blue" colspan="2" style="text-align: center;">
                                        <%-- <asp:Button ID="btnGetClsGrds" runat="server" CssClass="buttontopfilter" Text="Get Students List"
                                        CausesValidation="False"></asp:Button>--%>
                                        <telerik:RadButton ID="btnGetClsGrds" runat="server" Text="Get Students List"
                                            CausesValidation="False">
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>


        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table class="maincontenttable">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->
                            <asp:Panel ID="pnlHeader" runat="server" Visible="False">
                                <asp:Panel ID="pnlRHS" runat="server">
                                    <table class="contenttable">
                                        <tr>
                                            <td class="arcontentcells" nowrap>
                                                <asp:Label ID="lblInstr" runat="server" CssClass="label">Instructor</asp:Label>
                                            </td>
                                            <td class="arcontentcells2" nowrap>
                                                <asp:Label ID="lblInstrName" runat="server" CssClass="textbox"></asp:Label>
                                            </td>
                                            <td class="arcontentcells" nowrap>
                                                <asp:Label ID="lblClass" runat="server" CssClass="label">Class</asp:Label>
                                            </td>
                                            <td class="arcontentcells2" nowrap>
                                                <asp:Label ID="lblClsName" runat="server" CssClass="textbox"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="arcontentcells" nowrap>
                                                <asp:Label ID="lblSection" runat="server" CssClass="label">Section</asp:Label>
                                            </td>
                                            <td class="arcontentcells2" nowrap>
                                                <asp:Label ID="lblSecName" runat="server" CssClass="textbox"></asp:Label>
                                            </td>
                                            <td class="arcontentcells" nowrap>
                                                <asp:Label ID="lblGrdCrit" runat="server" CssClass="label">Grades for</asp:Label>
                                            </td>
                                            <td class="arcontentcells2" nowrap>
                                                <asp:Label ID="lblGrdCritName" runat="server" CssClass="textbox"></asp:Label>
                                            </td>

                                            <tr runat="server">
                                                <td class="contentcellblue">
                                                    <asp:Label ID="lblPostdDateDefaul" runat="server" CssClass="label">Default Date Completed</asp:Label>
                                                </td>
                                                <td class="contentcell4blue">
                                                    <telerik:RadDatePicker ID="rdpPostdDateDefault" MinDate="1/1/1945" runat="server" AutoPostBack="true" Width="200px">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>

                                        </tr>
                                    </table>
                                    <table class="contenttable" style="width: 100%;">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DataGrid ID="dgdClsSectGrds" runat="server" CssClass="datalistcontentar" BorderWidth="1px"
                                                    Width="99%"
                                                    BorderStyle="Solid" BorderColor="#E0E0E0" AutoGenerateColumns="False">
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Student">
                                                            <HeaderStyle CssClass="datagridheader" Width="25%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtStuEnrollId" runat="server" CssClass="textbox" Visible="False" />
                                                                <asp:Label ID="Student" runat="server" CssClass="label" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="SSN">
                                                            <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="studentSSNBtn" runat="server" CssClass="itemstyle" CausesValidation="False">
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Student ID">
                                                            <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="studentNumberBtn" runat="server" CssClass="itemstyle" CausesValidation="False">
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Description">
                                                            <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblitemToGradeDescription" runat="server" CssClass="itemstyle" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Final Grade">
                                                            <HeaderStyle CssClass="datagridheader" Width="5%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrade" runat="server" CssClass="label" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Current Score">
                                                            <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtScore" runat="server" TabIndex="1" Width="70%" CssClass="textbox">
                                                                </asp:TextBox>
                                                               <%-- <asp:CompareValidator ID="MinValCompareValidator" runat="server" ErrorMessage="Invalid Score"
                                                                    Display="None" Type="Double" Operator="LessThan" ValueToCompare="32767" ControlToValidate="txtScore">Invalid Score</asp:CompareValidator>--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Date Completed">
                                                            <HeaderStyle CssClass="datagridheader" Width="5%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <telerik:RadDatePicker ID="rdpDateCompleted" MinDate="1/1/1945" runat="server" AutoPostBack="false">
                                                                </telerik:RadDatePicker>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Comments">
                                                            <HeaderStyle CssClass="datagridheader" Width="35%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtComments" runat="server" TabIndex="2" TextMode="MultiLine" Width="98%"
                                                                    Rows="3" CssClass="textbox" />
                                                                <asp:HiddenField ID="hdnIsCompGraded" runat="server" />
                                                                <asp:HiddenField ID="hiddenGrdCritId" runat="server" />
                                                                <asp:HiddenField ID="hiddenInstructorBookWeightDetailId" runat="server" />
                                                                <asp:HiddenField ID="HiddenClassSectionId" runat="server" />
                                                                <asp:HiddenField ID="HiddenGradeBookId" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                            <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                            <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

