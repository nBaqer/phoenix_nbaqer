<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Term.aspx.vb" Inherits="Term" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <%-- <asp:TextBox ID="txtOldCampGrpId" runat="server" Width="0px" TabIndex="-1" Style="visibility:hidden;" ClientIDMode="Static"></asp:TextBox>--%>
                <asp:HiddenField ID="txtOldCampGrpId" runat="server" ClientIDMode="Static" EnableViewState="true" />
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <asp:Label ID="label1" runat="server" Style="font: normal 11px verdana; color: #000066; background-color: transparent; padding: 2px; vertical-align: middle;">Program</asp:Label>&nbsp;&nbsp;
        <asp:DropDownList ID="ddlProgId1" CssClass="dropdownlist" Style="width: 200px" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <asp:DataList ID="dlstTerm" runat="server" DataKeyField="TermId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle" Font-Bold="true" />
                                    <ItemStyle CssClass="itemstyle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("TermId")%>' Text='<%# container.dataitem("TermDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>

                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTermCode" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTermCode" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblProgId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlProgId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTermTypeId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlTermTypeId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTermDescrip" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTermDescrip" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStartdate" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <telerik:RadDatePicker ID="txtStartdate" MinDate="1/1/1945" runat="server">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblEndDate" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <telerik:RadDatePicker ID="txtEndDate" MinDate="1/1/1945" runat="server">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" AutoPostBack="true" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">&nbsp;
                                            </td>
                                            <td class="contentcell">
                                                <asp:LinkButton ID="lbtSetUpFees" runat="server" CssClass="label" Enabled="False">Set up Fees for this term</asp:LinkButton></td>

                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lApplyToTheseGroups" CssClass="label" runat="server" Visible="false">Create Class Sections<br /> for these Groups:</asp:Label></td>
                                            <td class="contentcell4date">
                                                <asp:Panel ID="pGroups" runat="server" CssClass="panel" BorderWidth="1" Visible="true">
                                                    <table cellspacing="1" cellpadding="6" width="100%" align="center">
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <asp:DropDownList ID="ddlGroups" runat="server" CssClass="dropdownlist" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTermId" runat="server" MaxLength="128" Visible="false"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlRegent" runat="server" Visible="false">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center" style="border: 1px solid #ebebeb;">
                                            <tr>
                                                <td class="contentcellheader" nowrap
                                                    style="border-top: 0px; border-right: 0px; border-left: 0px">
                                                    <asp:Label ID="label2" runat="server" Font-Bold="true" CssClass="label">Map To Regent</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellcitizenship" style="padding-top: 12px">
                                                    <asp:Panel ID="pnlRegentAwardChild" TabIndex="12" runat="server" Width="80%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="35%" nowrap class="contentcell">
                                                                    <asp:Label ID="lblregentterm" runat="server" CssClass="label">Session<span style="color: red">*</span></asp:Label></td>
                                                                <td width="65%" nowrap class="contentcell4">
                                                                    <asp:DropDownList ID="ddlSession" runat="server" CssClass="textbox" TabIndex="8"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="35%" nowrap class="contentcell">
                                                                    <asp:Label ID="lblSessionStart" runat="server" CssClass="label">Session Start<span style="color: red">*</span></asp:Label></td>
                                                                <td width="65%" nowrap class="contentcell4">
                                                                    <asp:DropDownList ID="ddlSessionStart" runat="server" CssClass="textbox" TabIndex="6"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="35%" nowrap class="contentcell">
                                                                    <asp:Label ID="lblSessionEnd" runat="server" CssClass="label">Session End<span style="color: red">*</span></asp:Label></td>
                                                                <td width="65%" nowrap class="contentcell4">
                                                                    <asp:DropDownList ID="ddlSessionEnd" runat="server" CssClass="textbox" TabIndex="6"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:TextBox ID="txtrowIds" Style="visibility: hidden;" runat="server" CssClass="donothing"></asp:TextBox>
                                    <asp:TextBox ID="txtresourceId" Style="visibility: hidden" runat="server" CssClass="donothing"></asp:TextBox>
                                    <!--end table content-->
                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap>
                                                    <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcell2">
                                                    <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false"></asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </asp:Panel>
                            <!--end table content-->

                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>

        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="true"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>

</asp:Content>


