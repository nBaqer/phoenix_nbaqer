﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="FERPAPolicy.aspx.vb" Inherits="FERPAPolicy" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
 
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<TR>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right">
                                    <asp:button id="btnSave" runat="server" Text="Save" CssClass="save"></asp:button>
                                    <asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
							align="center">
							<tr>
								<td class="detailsframe">
                                      <div class="scrollsingleframe">


                                          <table class="contenttable" cellspacing="0" cellpadding="0" width="35%" align="center">
                                              <tr>
                                                  <td class="contentcell">
                                                      <asp:Label ID="lblEnrollmentId" CssClass="label" runat="server">Entity Types</asp:Label>
                                                  </td>
                                                  <td class="twocolumncontentcelldate" style="padding-bottom: 20px">
                                                      <asp:DropDownList ID="ddlEntity" Width="350px" runat="server" CssClass="dropdownlist" AutoPostBack="True" />
                                                  </td>
                                              </tr>
                                          </table>
                                          <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                              <tr>
                                                  <td>
                                                      &nbsp;
                                                  </td>
                                                  <td class="threecolumnheaderreg" nowrap>
                                                      <asp:Label ID="lblAvailStuds" CssClass="labelbold" runat="server">Select Data Categories</asp:Label>
                                                  </td>
                                                  <td>
                                                      &nbsp;
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;
                                                  </td>
                                                  <td class="leadcell2" align="center">
                                                      <asp:CheckBoxList ID="ChkFERPAPages" TabIndex="5" runat="Server" RepeatColumns="2"
                                                          CssClass="checkboxstyle">
                                                      </asp:CheckBoxList>
                                                  </td>
                                                  <td>
                                                      &nbsp;
                                                  </td>
                                              </tr>
                                          </table>
                                          <asp:TextBox ID="txtStudentId" runat="server" Visible="false" />
                                          <asp:TextBox ID="txtStEmploymentId" runat="server" Visible="false" />
                                          <asp:CheckBox ID="ChkIsInDB" runat="server" Checked="False" Visible="false" />
                                          <asp:TextBox ID="txtStudentDocs" runat="server" Visible="False" />


										
                                     </div>	
								</td>
							</tr>
						</table>
					</td>
					<!-- end rightcolumn --></TR>
			</table>
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel>
            <asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

