
<%@ Page Language="vb" AutoEventWireup="false" Inherits="StdSuspensions" CodeFile="StdSuspensions.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Student Suspensions</title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <link href="../css/systememail.css" type="text/css" rel="stylesheet">
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<body id="Body1" runat="server" name="Body1">
    <form id="Form1" method="post" runat="server">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="../images/advantage_suspensions.jpg"></td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href=#>X Close</a></td>
            </tr>
        </table>
        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="DetailsFrameTop">
                    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="MenuFrame" align="right">
                                <asp:Button ID="btnSave" runat="server" Enabled="False" CssClass="save" Text="Save">
                                </asp:Button><asp:Button ID="btnNew" runat="server" Enabled="False" CssClass="new"
                                    Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server"
                                        Enabled="False" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button></td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->
                    <table id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <div class="scrollsingleframe">
                                    <!-- begin table content-->
                                    <asp:Panel ID="pnlRHS" runat="server">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dgdDropCourse" runat="server" AutoGenerateColumns="False" BorderWidth="1px"
                                                        BorderColor="#E0E0E0" Width="90%">
                                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Start Date">
                                                            <HeaderStyle CssClass="DataGridHeader" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="StartDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="End Date">
                                                            <HeaderStyle CssClass="DataGridHeader" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="EndDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"EndDate", "{0:d}") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Reason">
                                                            <HeaderStyle CssClass="DataGridHeader" Width="10%"></HeaderStyle>
                                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Reason" runat="server" CssClass="label" Text='<%# Container.DataItem("Reason")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <!--end table content-->
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
                <!-- end rightcolumn -->
            </tr>
        </table>
        <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                runat="server">
            </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
    </form>
</body>
</HTML>
