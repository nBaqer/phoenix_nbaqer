
Partial Class AR_Tardy
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Request.Params("unittype").ToLower = "pa" Then
                lblnumberofdaysabsent.Text = CInt(Request.Params("actualdaysabsent"))
                Label4.Text = CInt(Request.Params("numbermakestardy"))
                lblnumberoftardy.Text = CInt(Request.Params("studenttardy"))
                lblNumTotal.Text = CInt(Request.Params("numberofabsentdays"))
                'Tardy penalty will be number of total days absent minus the number of days student was actually absent
                lblTardyPenalty.Text = (CInt(Request.Params("numberofabsentdays")) - CInt(Request.Params("actualdaysabsent"))).ToString
            Else
                lblnumberofdaysabsent.Text = Request.Params("actualdaysabsent")
                Label4.Text = Request.Params("numbermakestardy")
                lblnumberoftardy.Text = Request.Params("studenttardy")
                lblNumTotal.Text = Request.Params("numberofabsentdays")
                'Tardy penalty will be number of total days absent minus the number of days student was actually absent
                lblTardyPenalty.Text = (CInt(Request.Params("numberofabsentdays")) - CInt(Request.Params("actualdaysabsent"))).ToString
            End If

        End If
    End Sub
End Class
