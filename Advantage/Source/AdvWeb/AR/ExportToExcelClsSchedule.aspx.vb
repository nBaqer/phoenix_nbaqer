Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data

Partial Class AR_ExportToExcelClsSchedule
    Inherits System.Web.UI.Page
    Protected Sub AR_ExportToExcelClsSchedule_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Request.Params("source").ToString.Trim = "classschedule" Then
                pnlInstructorhours.Visible = False
                ClassSchedule()

                lblStartDateValue.Text = Request.Params("Start")
                lblEndDateValue.Text = Request.Params("End")

                Response.ClearContent()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment; filename=classschedule.xls")
                Response.Charset = ""
                Me.EnableViewState = False

                Dim tw As New System.IO.StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                pnlClassSchedule.RenderControl(hw)
                Response.Write(tw.ToString())
            Else
                Dim fac As New ClassSchedulesFacade
                dgrdInstructorHours.DataSource = fac.GetInstructorHours(Request.Params("Start"), Request.Params("End"), Request.Params("Campus"), "")
                dgrdInstructorHours.DataBind()

                pnlInstructorhours.Visible = True
                pnlCSM.Visible = False
                lblStartDateValue.Text = Request.Params("Start")
                lblEndDateValue.Text = Request.Params("End")

                Response.ClearContent()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment; filename=classschedule.xls")
                Response.Charset = ""
                Me.EnableViewState = False

                Dim tw As New System.IO.StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                pnlClassSchedule.RenderControl(hw)
                Response.Write(tw.ToString())
            End If

            '  Response.End()

        End If
    End Sub
    Protected Sub ClassSchedule()
        Dim arrDates As New ArrayList
        Dim dt As New DataTable
        Dim fac As New ClassSchedulesFacade

        ''Get the ArrayList with the list of dates based on the start and end dates specified
        'arrDates = fac.GetDatesInPeriod(Request.Params("Start"), Request.Params("End"))
        'Session("Dates") = arrDates

        ''Code Modified By Saraswathi lakshmanan on August 24 2009
        ''To fix issue 15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 

        arrDates = fac.GetDatesInPeriod(Request.Params("Start"), Request.Params("End"))
        Session("Dates") = arrDates
        Dim dtPartialHols As DataTable
        dtPartialHols = fac.GetPartialHolidays(Request.Params("Start"), Request.Params("End"), Request.Params("Campus"))


        'Get the class meetings based on the start and end dates specified
        dt = fac.GetMeetings(Request.Params("Start"), Request.Params("End"), Request.Params("Campus"), Request.Params("Instructor"))
        Session("Meetings") = dt

        BuildAttendanceGrid(arrDates, dt, dtPartialHols)

    End Sub

    Private Sub BuildAttendanceGrid(ByVal arrDates As ArrayList, ByVal dtMeetings As DataTable, ByVal DtPartialhols As DataTable)
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim tbl As New Table
        Dim r As New TableRow
        Dim c1, c2, c3, c4, c5 As New TableCell
        Dim lbl, lbl2, lbl3, lbl4, lbl5 As New Label
        Dim rnd As New Random
        Dim dtDays As New DataTable
        Dim meetingRows() As DataRow

        pnlCSM.Controls.Clear()
        'pnlRequiredFieldValidators.Controls.Clear()

        tbl.Width = Unit.Percentage(100)
        tbl.CellPadding = 0
        tbl.CellSpacing = 0
        tbl.CssClass = "contenttable"

        'First create the header
        lbl.CssClass = "LabelBold"
        lbl.Text = "Start"
        c1.HorizontalAlign = HorizontalAlign.Left
        c1.Controls.Add(lbl)
        c1.CssClass = "Datagridheader"
        r.Cells.Add(c1)

        lbl2.CssClass = "LabelBold"
        lbl2.Text = "End"
        c2.HorizontalAlign = HorizontalAlign.Left
        c2.Controls.Add(lbl2)
        c2.CssClass = "Datagridheader"
        r.Cells.Add(c2)

        lbl3.CssClass = "LabelBold"
        lbl3.Text = "Shift"
        c3.HorizontalAlign = HorizontalAlign.Left
        c3.Controls.Add(lbl3)
        c3.CssClass = "Datagridheader"
        r.Cells.Add(c3)

        lbl4.CssClass = "LabelBold"
        lbl4.Text = "Code-Term"
        c4.HorizontalAlign = HorizontalAlign.Left
        c4.Controls.Add(lbl4)
        c4.CssClass = "Datagridheader"
        r.Cells.Add(c4)

        lbl5.CssClass = "LabelBold"
        lbl5.Text = "Instructor"
        c5.HorizontalAlign = HorizontalAlign.Left
        c5.Controls.Add(lbl5)
        c5.CssClass = "Datagridheader"
        r.Cells.Add(c5)

        tbl.Rows.Add(r)

        'For each date in the arraylist we want to get the corresponding meetings from the datatable.
        'We will only add the date if it has meetings.
        For Each dtmPeriod As Date In arrDates
            meetingRows = dtMeetings.Select("WorkDaysDescrip = '" & facAttendance.GetShortDayName(dtmPeriod) & "' AND StartDate <= '" & dtmPeriod & "' AND EndDate >= '" & dtmPeriod & "'", "StartTime,EndTime,Instructor")

            If meetingRows.Length > 0 Then


                ''If day is a partial Holiday
                ''Remove the meetings for those timings alone

                ''Code Modified By Saraswathi lakshmanan on August 24 2009
                ''To fix issue 15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 
                ''If the Class Falls between the holiday time, then that class is not shown. or else if it is overlapping with the Holiday time , then it is shown.
                ''Eg: 1-4 PM is HOliday and Class is between 2-4PM then this class is not shown.
                ''If the holiday is between 1-4 and Class is between 12-3 then it is Shown. Both the start and End time of the Class should fall between the Start and end time of the Holiday.
                Dim PartialHolsDateRow() As DataRow
                Dim PartialHolsAvail() As DataRow
                Dim icountpartialhols As Integer = 0
                PartialHolsDateRow = DtPartialhols.Select("HolidayDate='" & dtmPeriod & "'")
                If PartialHolsDateRow.Length > 0 Then
                    Dim i As Integer
                    For i = 0 To meetingRows.Length - 1
                        PartialHolsAvail = DtPartialhols.Select("HolidayDate='" & dtmPeriod & "' AND StartTime<='" & meetingRows(i)("StartTime") & "' AND EndTime>='" & meetingRows(i)("EndTime") & "'")
                        If PartialHolsAvail.Length > 0 Then
                            icountpartialhols = icountpartialhols + 1
                        End If
                    Next

                End If


                If icountpartialhols < meetingRows.Length Then






                    Dim rDate As New TableRow
                    Dim rEmpty As New TableRow
                    Dim rEmpty2 As New TableRow
                    Dim cDateCaption As New TableCell
                    Dim cEmpty As New TableCell
                    Dim cEmpty2 As New TableCell
                    Dim lblDate As New Label
                    Dim lblEmpty As New Label
                    Dim lblEmpty2 As New Label

                    'Add an empty row before adding the date caption
                    lblEmpty.CssClass = "Label"
                    lblEmpty.Text = "&nbsp;"
                    cEmpty.HorizontalAlign = HorizontalAlign.Left
                    cEmpty.Controls.Add(lblEmpty)
                    rEmpty.Controls.Add(cEmpty)
                    tbl.Rows.Add(rEmpty)

                    'First we need to write out a cell with the date in the format like Tuesday, September 02, 2008.
                    lblDate.CssClass = "LabelBold"
                    lblDate.Text = dtmPeriod.ToLongDateString
                    cDateCaption.HorizontalAlign = HorizontalAlign.Left
                    cDateCaption.Controls.Add(lblDate)
                    rDate.Controls.Add(cDateCaption)
                    tbl.Rows.Add(rDate)

                    'Add an empty row after adding the date caption
                    lblEmpty2.CssClass = "Label"
                    lblEmpty2.Text = "&nbsp;"
                    cEmpty2.HorizontalAlign = HorizontalAlign.Left
                    cEmpty2.Controls.Add(lblEmpty2)
                    rEmpty2.Controls.Add(cEmpty2)
                    tbl.Rows.Add(rEmpty2)

                    'Get the correpsonding meetings from the dtMeetings datatable and write out their details
                    For Each dr As DataRow In meetingRows
                        'Add a row to the table for it
                        Dim rMeet As New TableRow
                        Dim cStart As New TableCell
                        Dim cEnd As New TableCell
                        Dim cShift As New TableCell
                        Dim cCodeTerm As New TableCell
                        Dim cInstructor As New TableCell
                        Dim lStart As New Label
                        Dim lEnd As New Label
                        Dim lShift As New Label
                        Dim lcodeterm As New Label
                        Dim lInstructor As New Label

                        lStart.CssClass = "Label"
                        lStart.Text = dr("StartTime").ToShortTimeString
                        cStart.HorizontalAlign = HorizontalAlign.Left
                        cStart.Controls.Add(lStart)
                        rMeet.Controls.Add(cStart)

                        lEnd.CssClass = "Label"
                        lEnd.Text = dr("EndTime").ToShortTimeString
                        cEnd.HorizontalAlign = HorizontalAlign.Left
                        cEnd.Controls.Add(lEnd)
                        rMeet.Controls.Add(cEnd)

                        lShift.CssClass = "Label"
                        lShift.Text = dr("ShiftDescrip")
                        cShift.HorizontalAlign = HorizontalAlign.Left
                        cShift.Controls.Add(lShift)
                        rMeet.Controls.Add(cShift)

                        lcodeterm.CssClass = "Label"
                        lcodeterm.Text = dr("CodeTerm")
                        cCodeTerm.HorizontalAlign = HorizontalAlign.Left
                        cCodeTerm.Controls.Add(lcodeterm)
                        rMeet.Controls.Add(cCodeTerm)

                        lInstructor.CssClass = "Label"
                        lInstructor.Text = dr("Instructor").ToString
                        cInstructor.HorizontalAlign = HorizontalAlign.Left
                        cInstructor.Controls.Add(lInstructor)
                        rMeet.Controls.Add(cInstructor)


                        '                tbl.Rows.Add(rMeet)
                        '            Next
                        '        End If
                        'Next

                        ''Code Modified By Saraswathi lakshmanan on August 24 2009
                        ''To fix issue 15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 
                        ''If the Class Falls between the holiday time, then that class is not shown. or else if it is overlapping with the Holiday time , then it is shown.
                        ''Eg: 1-4 PM is HOliday and Class is between 2-4PM then this class is not shown.
                        ''If the holiday is between 1-4 and Class is between 12-3 then it is Shown. Both the start and End time of the Class should fall between the Start and end time of the Holiday.

                        If DtPartialhols.Select("HolidayDate='" & dtmPeriod & "' AND StartTime<='" & dr("StartTime") & "' AND EndTime>='" & dr("EndTime") & "'").Length = 0 Then
                            tbl.Rows.Add(rMeet)
                        End If

                    Next
                End If
            End If
        Next


        'Add the table to the panel control
        pnlCSM.Controls.Add(tbl)

    End Sub
End Class
