<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Tardy.aspx.vb" Inherits="AR_Tardy" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Absence Summary</title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <div>
        <asp:Panel ID="pnlReportingAgenciesHeader" runat="server" Visible="True">
            <table class="contenttable" cellspacing="0" cellpadding="0" width="90%" align="center"
                style="border: 1px solid #ebebeb;">
                <tr>
                    <td class="contentcellheader" nowrap colspan="6" style="border-top: 0px; border-right: 0px;
                        border-left: 0px">
                        <asp:Label ID="lblReportingAgencies" runat="server" Font-Bold="true" CssClass="Label">Absence Summary</asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="close"
                            href="#" onclick="top.close()">Close Window</a>
                    </td>
                </tr>
                <tr>
                    <td class="contentcellcitizenship" colspan="6" style="padding-top: 12px">
                        <asp:Panel ID="pnlReportingAgencies" TabIndex="12" runat="server" EnableViewState="false">
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblnumber" runat="Server" CssClass="labelbold">Number of days student was actually absent:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblnumberofdaysabsent" runat="Server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label3" runat="Server" CssClass="labelbold">Number of tardies that makes 1 absence:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label4" runat="Server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lbltardy" runat="Server" CssClass="labelbold">Number of days student was tardy:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblnumberoftardy" runat="Server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label5" runat="Server" CssClass="labelbold">Tardy penalty:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTardyPenalty" runat="Server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label1" runat="Server" CssClass="label">==========</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblTotal" runat="Server" CssClass="labelbold">Total days absent(including tardy) :</asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblNumTotal" runat="Server" CssClass="labelbold"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" runat="Server" CssClass="label">==========</asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
