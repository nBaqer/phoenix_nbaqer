﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TransferPendingDialog.aspx.vb" Inherits="AdvWeb.AR.ArTransferPendingDialog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transfer Grades</title>
    <style type="text/css">
        #btnOk {
            text-align: right;
        }
        #ButtonType2 {
            text-align: right;
        }
    </style>
</head>
<body style="font-family: Arial, sans-serif">
    
    <form id="form1" runat="server">
        <div id="TransferredSection" runat="server">
            <div>
                <p>Some courses were successfully transferred. but not the followings... </p>
            </div>
        </div>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
        <div id="TransferByOne" runat="server" >
            <div>
                <p>The student has final grades transferred in Advantage for the below courses</p>
            </div>
            <telerik:RadGrid ID="GridTransfer" runat="server"  CellSpacing="0" GridLines="None" AutoGenerateColumns="False">
                <MasterTableView ClientDataKeyNames="TransferId" CommandItemDisplay="None">

                    <Columns>

                        <telerik:GridBoundColumn DataField="TransferId" HeaderText="" ReadOnly="true" Visible="False">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="CourseCode" HeaderText="Course Code" ReadOnly="true">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="Grade" HeaderText="Grade" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="Score" HeaderText="Grade" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="TermDescription" HeaderText="Term" ReadOnly="true">
                        </telerik:GridBoundColumn>

                    </Columns>

                </MasterTableView>
            </telerik:RadGrid>
             <input id="checkIFYouWantToTransfer" value="2" type="checkbox">
            <label for="checkIFYouWantToTransfer">Check if you want to transfer the result to this enrollment</label>
        </div>
        <br />
        <div id="GradePostedByOne" runat="server">
            <div>
                <p>The student has final grades posted in Advantage for the below courses</p>
            </div>
            <telerik:RadGrid ID="RadGridPosted" runat="server"  CellSpacing="0" GridLines="None" AutoGenerateColumns="False">
                <MasterTableView ClientDataKeyNames="TestId" CommandItemDisplay="None">

                    <Columns>

                        <telerik:GridBoundColumn DataField="TestId" HeaderText="" ReadOnly="true" Visible="False">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="CourseCode" HeaderText="Course Code" ReadOnly="true">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="Grade" HeaderText="Grade" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="Score" HeaderText="Grade" ReadOnly="true">
                        </telerik:GridBoundColumn>
  
                        <telerik:GridBoundColumn DataField="TermDescription" HeaderText="Term" ReadOnly="true">
                        </telerik:GridBoundColumn>

                    </Columns>

                </MasterTableView>
            </telerik:RadGrid>
            <input id="checkIfYouWantToGrade" value="1" type="checkbox">
            <label for="checkIfYouWantToGrade">Check if you want to transfer the result to this enrollment</label>
        </div>
        <div id="WeirdSection" runat="server">
            <hr/>
            <div>
                <p>These courses can not be transferred. Reason in Message Field: </p>
            </div>
            <telerik:RadGrid ID="RadGridWeird" runat="server"  CellSpacing="0" GridLines="None" AutoGenerateColumns="False">
                <MasterTableView ClientDataKeyNames="ReqId" CommandItemDisplay="None">

                    <Columns>

                        <telerik:GridBoundColumn DataField="ReqId" HeaderText="" ReadOnly="true" Visible="False">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="ReqCode" HeaderText="Course Code" ReadOnly="true">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="CourseDescription" HeaderText="Course Description" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="MessageToWorld" HeaderText="Message" ReadOnly="true">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>

        </div>
        <p></p>
        <div id="ButtonType1" runat="server" style="text-align: right">
        <button id="btnTransfer" title="TRANSFER" onclick="returnResult('transfer')">TRANSFER</button>
        <button id="btnCancel" title="CANCEL" onclick="returnResult('cancel'); return false;">CANCEL</button>
        </div>
         <div id="ButtonType2" runat="server"  >
        <button id="btnOk" title="Close" onclick="returnResult('transfer')">CLOSE</button>
        </div>
      <script type="text/javascript">

          function GetRadWindow() {
              var oWindow = null;
              if (window.radWindow) oWindow = window.radWindow;
              else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
              return oWindow;
          }

          function returnResult(e) {
              //create the argument that will be returned to the parent page
              var oArg = new Object();
              oArg.result = e;
              oArg.checkTransfer = "false";
              oArg.checkGrade = "false";
              var checht = document.getElementById("checkIFYouWantToTransfer");
              if (checht != null) {
                  oArg.checkTransfer = checht.checked.toString(); 
              }
              var checkg = document.getElementById("checkIfYouWantToGrade");
              if (checkg != null) {
                  oArg.checkGrade = checkg.checked.toString();
              }
            
              //get a reference to the current RadWindow
              var oWnd = GetRadWindow();

              //Close the RadWindow and send the argument to the parent page
              oWnd.close(oArg);
             

          }
    </script>
      

    </form>

</body>
</html>
