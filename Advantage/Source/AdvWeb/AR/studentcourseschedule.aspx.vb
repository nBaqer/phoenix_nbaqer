﻿
Imports System.Collections
Imports System.Data
Imports System.Diagnostics
Imports System.Text
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.Common.AR

Namespace AdvWeb.AR

    Partial Class Studentcourseschedule
        Inherits BasePage 
        'This call is required by the Web Form Designer.
        <DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected BoolStatus As String
        'Private requestContext As HttpContext
        Public StudentId As String
        Property Campusid As String
        Protected ResourceID As Integer

        Protected state As AdvantageSessionState
        Protected LeadId As String
        Protected BoolSwitchCampus As Boolean
        Private pObj As New UserPagePermissionInfo
        Protected MyAdvAppSettings As AdvAppSettings
        Protected DtAvail As DataTable
        Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme
        End Sub
#Region "MRU Routines"
        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

        End Sub
        Private Function GetStudentFromStateObject() As StudentMRU

            Dim objStudentState As New StudentMRU

            Try

                GlobalSearchHandler(0)

                BoolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

                If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                    StudentId = Guid.Empty.ToString()
                Else
                    StudentId = AdvantageSession.MasterStudentId
                End If

                If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                    LeadId = Guid.Empty.ToString()
                Else
                    LeadId = AdvantageSession.MasterLeadId
                End If


                With objStudentState
                    .StudentId = New Guid(StudentId)
                    .LeadId = New Guid(LeadId)
                    .Name = AdvantageSession.MasterName
                End With

                HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
                HttpContext.Current.Items("Language") = "En-US"

                Master.ShowHideStatusBarControl(True)





            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Dim strSearchUrl As String
                strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + Master.CurrentCampusId + "&desc=View Existing Students"
                Response.Redirect(strSearchUrl)
            End Try

            Return objStudentState

        End Function
#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            MyAdvAppSettings = AdvAppSettings.GetAppSettings()
            ResourceID = CInt(HttpContext.Current.Request.Params("resid"))
            Campusid = Master.CurrentCampusId
            '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
            'Get StudentId and LeadId
            Dim objStudentState As StudentMRU = GetStudentFromStateObject() 'Pass resource-id so that user can be redirected to same page while switching students
            If objStudentState Is Nothing Then
                RedirectToStudentSearchPage(Request.QueryString("mod"), Master.CurrentCampusId)
                Exit Sub
            End If

            With objStudentState
                StudentId = .StudentId.ToString
                LeadId = .LeadId.ToString

            End With
            '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

            Dim advantageUserState As User = AdvantageSession.UserState

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(ResourceID, String), Campusid)

            If BoolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            'Comment added by Balaji on 11/17/2011
            'The Save button is disabled as save functionality has been moved to Add/Remove buttons
            'to make the schedule conflict flawless
            btnSave.Enabled = False

        
            If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(uSearchEntityControlId.Value)) Then

                BuildEnrollmentDDL()
                'Display an error message if there are no enrollments
                If ddlEnrollmentId.Items.Count = 0 Then
                    DisplayErrorMessage("There are no enrollments")
                    Exit Sub
                End If

                studentSchedule.Initialize(Me.ddlEnrollmentId.SelectedValue, Me.campusId,Me.resourceId,Me.LeadId,Me.StudentId, Me.MyAdvAppSettings, pObj)

                uSearchEntityControlId.Value = ""
            End If

        End Sub

        Private Sub BuildEnrollmentDDL()
            Dim fac As New TranscriptFacade
            Dim ds As DataSet
            ds = fac.GetEnrollment(StudentId, Campusid)

            If ds.Tables(0).Rows.Count > 0 Then
                ddlEnrollmentId.DataSource = ds
                ddlEnrollmentId.DataTextField = "PrgVerDescrip"
                ddlEnrollmentId.DataValueField = "StuEnrollId"
                ddlEnrollmentId.DataBind()
                ddlEnrollmentId.SelectedIndex = 0
            End If

            studentSchedule.ToggleButtons()
       

        End Sub
        ''' <summary>
        ''' 2/1/07 ThinkTron - need to rebind the time clock schedule panel
        ''' when the user changes the enrollment
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub ddlEnrollmentId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
            studentSchedule.Reset(ddlEnrollmentId.SelectedValue, Me.Campusid)
            
        End Sub
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            ' 2/1/07 ThinkTron - save Time Clock schedule
            If studentSchedule.panelSchedule.Visible Then
                ' Require that both badge-number and schedule are entered
                'If Me.txtBadgeNumber.Text = "" Or ddlSchedule.SelectedValue = "" Then
                ' For now, just require the schedule
                If studentSchedule.ddlSched.SelectedValue = "" Then
                    DisplayErrorMessage("Select the schedule")
                    Exit Sub
                End If
                ' check if the badge-number entered is in use
                ' and if so, display an error message and stop the enrollment from succeeding
                Dim stuEnrollId As String = ddlEnrollmentId.SelectedValue
                If studentSchedule.textBadgeNumber.Text.Length > 0 AndAlso
                   SchedulesFacade.IsBadgeNumberInUse(studentSchedule.textBadgeNumber.Text, stuEnrollId,Campusid) Then
                    DisplayErrorMessage("The entered badge number is currently used by another student.  Please choose another badge number.")
                    Exit Sub
                End If

                If stuEnrollId <> "" AndAlso studentSchedule.ddlSched.SelectedValue <> "" Then

                    Dim schedInfo As New StudentScheduleInfo()

                    schedInfo.ScheduleId = studentSchedule.ddlSched.SelectedValue
                    If String.IsNullOrEmpty(studentSchedule.ddlSched.SelectedValue) Then
                        schedInfo.ScheduleId = Nothing
                    End If

                    schedInfo.StuEnrollId = stuEnrollId
                    schedInfo.BadgeNumber = studentSchedule.textBadgeNumber.Text

                    If String.IsNullOrEmpty(studentSchedule.textBadgeNumber.Text) Then
                        schedInfo.BadgeNumber = Nothing
                    End If

                    schedInfo.Active = True
                    schedInfo.ModUser = AdvantageSession.UserName

                    If SchedulesFacade.AssignStudentScheduleFromSchedulePage(schedInfo, CType(Session("UserName"), String)) <> 0 Then
                        DisplayErrorMessage("An error occurred while attempting to save the schedule.")
                    End If

                End If
            End If

        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender


            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(ddlEnrollmentId)

            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
            'BindToolTip()


        End Sub
        Private Sub DisplayErrorMessage(ByVal errorMessage As String)
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End Sub
    End Class
End Namespace