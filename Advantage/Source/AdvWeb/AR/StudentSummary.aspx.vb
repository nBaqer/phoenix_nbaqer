Imports FAME.Common
Imports System.Xml
Imports System.Data
Imports System.Text
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.Common.AR
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Partial Class StudentSummary
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

#End Region

#Region "Variables"

    Protected WithEvents Btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected ShowStatusLevel As AdvantageCommonValues.IncludeStatus
    Protected StudentId As String
    Protected WithEvents Img9 As System.Web.UI.HtmlControls.HtmlImage

    Private StuMasterInfo As New StudentEnrollmentInfo

    'To get the firstname and lastname of student to pass into the enrollmentid component.
    Dim strFirstName, strLastName As String
    Dim EnrollmentId As New EnrollmentComponent
    Dim facade As New StuEnrollFacade
    Protected campusId As String
    Dim SDFControls As New SDFComponent
    Dim moduleId As String
    Dim resourceId As Integer
    Protected LeadId As String
    Protected state As AdvantageSessionState

    Protected boolSwitchCampus As Boolean = False




    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Dim userId As String
    Private pObj As New UserPagePermissionInfo
#End Region

#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)






        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

#Region "Page Events"

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim facade As New StuEnrollFacade

        Dim fac As New UserSecurityFacade

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        ViewState("CampusId") = campusId
        moduleId = HttpContext.Current.Request.Params("Mod").ToString
        Session("mod") = HttpContext.Current.Request.Params("Mod").ToString

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = getStudentFromStateObject(539) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        userId = AdvantageSession.UserState.UserId.ToString

        If radStatus.SelectedItem.Text = "Active" Then
            ShowStatusLevel = AdvantageCommonValues.IncludeStatus.ActiveOnly
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            ShowStatusLevel = AdvantageCommonValues.IncludeStatus.InactiveOnly
        Else
            ShowStatusLevel = AdvantageCommonValues.IncludeStatus.ActiveAndInactive
        End If
        Try
            If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
                If boolSwitchCampus = True Then
                    CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
                End If
                txtStudentId.Text = StudentId

                Dim FullName() As String = Split(facade.StudentName(StudentId))
                strFirstName = FullName(0)
                strLastName = FullName(1)

                Session("StudentName") = strFirstName & " " & strLastName

                btnNew.Enabled = False
                btnSave.Enabled = False
                btnDelete.Enabled = False

                BindEnrollmentDataList()

                ds = facade.PopulateDataList(StudentId, ShowStatusLevel, campusId)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()

                SelectFirstEnrollmentWhenPageFirstLoads()
                MyBase.uSearchEntityControlId.Value = ""
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

#End Region

#Region "Button Click Events"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    End Sub

#End Region

#Region "DataList Events"

    Private Sub dlstCategories_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstStdEnroll.ItemCommand
        Dim strGUID As String = ""
        Try
            strGUID = dlstStdEnroll.DataKeys(e.Item.ItemIndex).ToString()
            txtStuEnrollId.Text = strGUID
            GetStudentInfo(strGUID)

            'MERGEFIX
            'CommonWebUtilities.SetStyleToSelectedItem(dlstStdEnroll, strGUID, ViewState, Header1)
            CommonWebUtilities.SetStyleToSelectedItem(dlstStdEnroll, strGUID, ViewState)

            Dim isClockHourProgram As Boolean = False
            isClockHourProgram = facade.IsStudentProgramClockHourType(txtStuEnrollId.Text)
            If isClockHourProgram = True Then
                lbltransferHours.Visible = True
                txtTransferHours.Visible = True
            End If

            ' US3037 5/4/2012 Janet Robinson
            CommonWebUtilities.RestoreItemValues(dlstStdEnroll, e.CommandArgument)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

#End Region

#Region "Radiobutton Events"

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        Dim objCommon As New CommonUtilities
        Try
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtStuEnrollId.Text = System.Guid.NewGuid.ToString
            BindEnrollmentDataList()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

#End Region

#Region "Private Functions"
    Private Sub GetStudentInfo(ByVal stuEnrollID As String)
        Dim studentinfo As New StuEnrollFacade
        Dim summaryinfo As New AttendanceFacade
        BindStudentInfoExist(studentinfo.GetStudentDetails(StuEnrollID))
        BuildLeadGroupsDDL()
        BindLeadGroupList(StuEnrollID)
        Dim attType As String = FAME.AdvantageV1.BusinessFacade.AR.SchedulesFacade.PrgVerAttendanceType(txtPrgVerId.Text)
        Dim attUnitType As String = ""
        If attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
            attUnitType = "PA"
        ElseIf attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.MinutesGuid Then
            attUnitType = "minutes"
        Else
            attUnitType = "hrs"
        End If
        Session("UnitType") = attUnitType
        ChangeLabels(attUnitType)
        BindAttendanceSummary(StuEnrollID)
        ShowScheduleMaintenancePage(stuEnrollID)
    End Sub

    Private Sub ShowScheduleMaintenancePage(ByVal stuEnrollId As String)
        Dim info As FAME.AdvantageV1.Common.AR.StudentScheduleInfo
        info = FAME.AdvantageV1.BusinessFacade.AR.SchedulesFacade.GetActiveStudentScheduleInfo(stuEnrollId)
        txtBadgeId.Text = info.BadgeNumber
        Try
            Dim objId As String = info.ScheduleId
            Session("ObjId") = objId
            txtClockSchedule.Text = info.Descrip
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    Public Shared Function GetJavsScriptPopup(ByVal popupUrl As String, _
                                                ByVal width As Integer, _
                                                ByVal height As Integer, _
                                                ByVal retControlId As String) As String
        Dim js As New StringBuilder
        js.Append("var strReturn; " & vbCrLf)
        js.Append("strReturn=window.showModalDialog('")
        js.Append(popupUrl)
        js.Append("',null,'resizable:yes;status:no;scrollbars=yes;minimize:yes; maximize:yes; dialogWidth:")
        js.Append(width.ToString())
        js.Append("px;dialogHeight:")
        js.Append(height.ToString())
        js.Append("px;dialogHide:true;help:no;scroll:yes'); " & vbCrLf)
        If Not retControlId Is Nothing Then
            If retControlId.Length <> 0 Then
                js.Append("if (strReturn != null) " & vbCrLf)
                js.Append("     document.getElementById('")
                js.Append(retControlId)
                js.Append("').value=strReturn;" & vbCrLf)
            End If
        End If
        Return js.ToString()
    End Function

    Private Sub ChangeLabels(ByVal attUnitType As String)
        If attUnitType.ToLower = "pa" Then
            lblTotalHours.Text = "Total days to date"
            lblSchedHours.Text = "Schedule days to date"
            lblTotalHoursAbsent.Text = "Total days absent"
            lblMakeUpHours.Text = "Total makeup days"
            lblSchedHoursByWeek.Text = "Schedule hours (week)"
            ''Modified by Saraswathi lakshmanan
            ''march 31 2009
            'lblLDA.Text = " Last day attended"
            lblLDA.Text = " Last date attended"
            lblAttPercentage.Text = "Attendance percentage"
            Button1.Visible = True
            btntardyminutes.Visible = False
            ''Added by saraswathi to fix issue 15538
            lblPresent.Text = "Present( in days)"
            lblAbsent.Text = "Absent( in days)"
            lblTardy.Text = "Tardy( in days)"
        ElseIf attUnitType = "minutes" Then
            lblTotalHours.Text = "Total hours to date"
            lblSchedHours.Text = "Schedule hours to date"
            lblTotalHoursAbsent.Text = "Total hours absent"
            lblMakeUpHours.Text = "Total makeup hours"
            lblSchedHoursByWeek.Text = "Schedule hours (week)"
            ''Modified by Saraswathi lakshmanan
            ''march 31 2009
            'lblLDA.Text = " Last day attended"
            lblLDA.Text = " Last date attended"
            lblAttPercentage.Text = "Attendance percentage"
            Button1.Visible = False
            btntardyminutes.Visible = True
            ''Added by saraswathi to fix issue 15538
            lblPresent.Text = "Present( in hrs)"
            lblAbsent.Text = "Absent( in hrs)"
            lblTardy.Text = "Tardy( in hrs)"
        Else
            lblTotalHours.Text = "Total hours to date"
            lblSchedHours.Text = "Schedule hours to date"
            lblTotalHoursAbsent.Text = "Total hours absent"
            lblMakeUpHours.Text = "Total makeup hours"
            lblSchedHoursByWeek.Text = "Schedule hours (week)"
            ''Modified by Saraswathi lakshmanan
            ''march 31 2009
            'lblLDA.Text = " Last day attended"
            lblLDA.Text = " Last date attended"
            lblAttPercentage.Text = "Attendance percentage"
            Button1.Visible = False
            ''modified by saraswathi on march 17 2009
            ''to show the view details of the tardy
            btntardyminutes.Visible = True
            ''Added by saraswathi to fix issue 15538
            lblPresent.Text = "Present( in hrs)"
            lblAbsent.Text = "Absent( in hrs)"
            lblTardy.Text = "Tardy( in hrs)"

        End If
        lblPresent.Font.Underline = True
        lblAbsent.Font.Underline = True
        lblTardy.Font.Underline = True
    End Sub

    Private Sub BindAttendanceSummary(ByVal StuEnrollId As String)
        Dim attSummaryInfo As New ClockHourAttendanceInfo

        'This procedure calls the centralized component that returns all attendance summary details
        attSummaryInfo = (New AttendanceFacade).GetAttendanceSummary(StuEnrollId, txtPrgVerId.Text)
        With attSummaryInfo
            txtTotalHours.Text = .TotalHoursPresent
            txtSchedHours.Text = .TotalHoursSched
            txtTotalHoursAbsent.Text = .TotalHoursAbsent
            txtMakeUpHours.Text = .TotalMakeUpHours
            ''TransferHours added by Saraswathi Lakhsmanan on June 3 2010
            txtTransferHours.Text = .TransferHours

            txtSchedHoursByWeek.Text = (New AttendanceFacade).GetScheduledHoursByWeek(StuEnrollId) '.TotalSchedHoursByWeek
            If .LDA = Date.MinValue Then
                txtLDA.Text = ""
            Else
                txtLDA.Text = .LDA.ToShortDateString
            End If
            Try
                txtAttPercentage.Text = .AttendancePercentage
                If txtAttPercentage.Text <> "" Then
                    txtAttPercentage.Text &= "%"
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtAttPercentage.Text = "0%"
            End Try
            'These Session Variables are required for the Tardy Pop up Page
            Session("actualdaysabsent") = .actualdaysabsent
            Session("studenttardy") = .actualtardyhours '.Tardy
            Session("numberofabsentdays") = .TotalHoursAbsent
            Session("tardiesmakingabsence") = .numberoftardiesmaking1absence
            Session("actualtardyhours") = .actualtardyhours
            Session("numberoftardy") = .actualtardyhours '.Tardy
            Session("adjactual") = .TotalHoursPresent
            Session("adjabsent") = .adjustedabsent
            Session("adjtardy") = .adjustedtardy

            'required for tardy calculation - minutes
            Session("actualhourspresent") = .ActualDaysPresent
            Session("actualhoursabsent") = .actualdaysabsent
            Session("actualtardyhours") = .actualtardyhours

            ''Update the attendance summary
            lblReportingAgencies.Text = "<b>Absence Summary </b>" & " (Number of tardies that equals 1 absence: " & Session("tardiesmakingabsence") & ")"


            ''Actual Hrs Data
            lblActPresent.Text = Session("actualhourspresent")
            lblActAbsent.Text = Session("actualhoursabsent")
            lblActTardy.Text = Math.Round(CType(Session("actualtardyhours"), Decimal), 2)

            ''Adjusted Data
            lblAdjPresent.Text = Session("adjactual")
            lblAdjAbsent.Text = Math.Round(CType(Session("adjabsent"), Decimal), 2) + Math.Round(CType(Session("actualhoursabsent"), Decimal), 2)
            lblAdjTardy.Text = Math.Round(CType(Session("adjtardy"), Decimal), 2)
            'If Session("adjtardy")("adjabsent") > 0 Then
            '    lblTardyPenalty.Text = Math.Abs(Session("adjabsent") * 60 - Math.Round(CType(Session("actabsent"), Decimal), 2) * 60).ToString & " minutes"
            'Else
            '    lblTardyPenalty.Text = ""
            'End If
        End With
    End Sub

    Private Sub BindStudentInfoExist(ByVal studentInfo As StudentEnrollmentInfo)
        Try
            With StudentInfo
                txtEnrollDate.Text = .EnrollDate
                txtExpStartDate.Text = .ExpStartDate
                txtExpGradDate.Text = .ExpGradDate
                If .StartDate <> "" Then
                    txtStartDate.Text = .StartDate
                Else
                    txtStartDate.Text = ""
                End If
                If .ReenrollDate <> "" Then
                    txtReEnrollDate.Text = .ReenrollDate
                Else
                    txtReEnrollDate.Text = ""
                End If
                txtPrgVerId.Text = .ProgramVersionID
                Session("PrgVersion") = .ProgramVersion
            End With
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub


    Private Sub SelectFirstEnrollmentWhenPageFirstLoads()
        '        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim facade As New StuEnrollFacade

        Try
            If dlstStdEnroll.DataKeys.Count = 0 Then Return
            strGUID = dlstStdEnroll.DataKeys(0).ToString()
            txtStuEnrollId.Text = strGUID
            GetStudentInfo(strGUID)

            Dim isClockHourProgram As Boolean = False
            isClockHourProgram = facade.IsStudentProgramClockHourType(txtStuEnrollId.Text)
            If IsClockHourProgram = True Then
                lbltransferHours.Visible = True
                txtTransferHours.Visible = True
            End If
            CommonWebUtilities.RestoreItemValues(dlstStdEnroll, strGUID)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            If ex.InnerException Is Nothing Then
                Session("Error") = "Error selecting the first enrollment when page is first loaded: " & ex.Message & " "
            Else
                Session("Error") = "Error selecting the first enrollment when page is first loaded: " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BuildLeadGroupsDDL()
        Dim facade As New AdReqsFacade
        With chkLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = facade.GetAllLeadGroups(campusId, "All")
            .DataBind()
        End With
    End Sub

    Private Sub BindLeadGroupList(ByVal StuEnrollId As String)

        'Get Degrees data to bind the CheckBoxList
        Dim Facade As New AdReqsFacade

        'Clear the current selection
        chkLeadGrpId.ClearSelection()
        lstStudentGroup.Items.Clear()

        Dim ds As DataSet = Facade.GetLeadgroupsSelectedByStudentEnrollment(txtStuEnrollId.Text)
        Dim JobCount As Integer = Facade.GetLeadGroupCountByStudentEnrollment(txtStuEnrollId.Text)
        If JobCount >= 1 Then
            'Select the items on the CheckBoxList
            Dim i As Integer
            If Not (ds.Tables(0).Rows.Count < 0) Then
                lstStudentGroup.Items.Clear()
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim row As DataRow
                    row = ds.Tables(0).Rows(i)
                    Dim item As ListItem
                    For Each item In chkLeadGrpId.Items
                        If item.Value.ToString = DirectCast(row("LeadGrpId"), Guid).ToString Then
                            item.Selected = True
                            lstStudentGroup.Items.Add(item.Text)
                            Exit For
                        End If
                    Next
                Next
            End If
        Else
            lstStudentGroup.Items.Clear()
            chkLeadGrpId.ClearSelection()
        End If
    End Sub

    Private Sub BindEnrollmentDataList()
        Dim ds As DataSet
        ds = (New StuEnrollFacade).PopulateDataList(StudentId, ShowStatusLevel, ViewState("CampusId").ToString)
        With dlstStdEnroll
            .DataSource = ds
            .DataBind()
        End With
    End Sub

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
