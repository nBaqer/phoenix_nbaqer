﻿
Imports System.Activities.Expressions
Imports Fame.DataAccessLayer
Imports System.Data
Imports Fame.Common
Imports Fame.ExceptionLayer
Imports Fame.Advantage.Common
Imports System.Collections
Imports Fame.Advantage.Api.Library.AcademicRecords
Imports Fame.Advantage.Api.Library.Models
Imports Fame.Advantage.Api.Library.Models.AcademicRecords
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports Fame.Advantage.Api.Library.Models.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.PostExams
Imports Telerik.Web.UI


Partial Class ProgramVersionExamSequence
    Inherits System.Web.UI.Page
    Dim programVersionId = Guid.Empty.ToString()
  
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim resourceId As Integer
        Dim campusId As String
        Dim userId As String
      
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        If Request.QueryString("PrgVerId") <> "" Then
            ViewState("PrgVerId") = Request.QueryString("PrgVerId")
            programVersionId = ViewState("PrgVerId").ToString()
        End If


        Dim requirements = (New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetRequirementsForProgramVersion(programVersionId)
        Dim exams = New List(Of ProgramVersionExam)
        
        'Dim exams = (New PostExamsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetProgramVersionExams(programVersionId)
        If not Page.IsPostBack Then
            ddlRequirements.DataSource = requirements
            ddlRequirements.DataTextField = "Descrip"
            ddlRequirements.DataValueField = "ReqId"
            ddlRequirements.DataBind()
            ddlRequirements.Items.Insert(0, new ListItem("Select", string.Empty))
            
           

            If ( ViewState("examsSequenceDataSource") IsNot nothing)
                exams = ViewState("examsSequenceDataSource")
                programVersionExamSequenceTable.DataSource = exams
                programVersionExamSequenceTable.Rebind()
            End If

            ViewState("pageLoadingFirstTime") = true
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "sessionStorage.setItem('programVersionDefinitionFormHasChanged', false)", True)
        End If



        If (ViewState("pageLoadingFirstTime") = true ) Then
            programVersionExamSequenceTable.DataSource = exams
            programVersionExamSequenceTable.Rebind()
            ViewState("pageLoadingFirstTime") = false
        End If

        'If (ddlRequirements.SelectedItem.Value <> String.Empty)
        '    exams = (New PostExamsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetProgramVersionExams(programVersionId, ddlRequirements.SelectedItem.Value)
        '    programVersionExamSequenceTable.DataSource = exams
        '    ViewState("examsSequenceDataSource") = exams
        '    '    programVersionExamSequenceTable.Rebind()
        'End If





    End Sub
    


    Protected Sub examPositionTextBox_OnTextChanged(sender As Object, e As EventArgs) 
        Dim textBox As TextBox = sender
        Dim exams As List(Of ProgramVersionExam) = ViewState("examsSequenceDataSource")
        Dim newPosition = convert.ToInt32(textBox.Text)
        Dim detailId = textBox.Attributes.Item("data-examid")
        
        If newPosition <= 0 Then
            newPosition = 1
        End If

        If ( newPosition > exams.Count)
            newPosition = exams.Count
        End If
        dim toRemove = exams.FirstOrDefault(Function (ex) ex.GradeBookDetailId.ToString() = detailId)
        exams.Remove(toRemove)
        exams.Insert(newPosition - 1, toRemove)


        Dim position = 1
        For Each ex As ProgramVersionExam In exams
            ex.Position = position
            position = position + 1
        Next

        ViewState("examsSequenceDataSource") = exams
        programVersionExamSequenceTable.DataSource = exams
        programVersionExamSequenceTable.Rebind()

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "sessionStorage.setItem('programVersionDefinitionFormHasChanged', true)", True)
    End Sub

    Protected Sub btnSave_OnClick(sender As Object, e As EventArgs)
        Dim exams as List(Of ProgramVersionExam) = ViewState("examsSequenceDataSource") 
        
        dim result = (New PostExamsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).UpdateSequence(exams)
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "sessionStorage.setItem('programVersionDefinitionFormHasChanged', false)", True)
    End Sub

    Protected Sub ddlRequirements_OnSelectedIndexChanged(sender As Object, e As EventArgs)
        dim exams = (New PostExamsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetProgramVersionExams(programVersionId, ddlRequirements.SelectedItem.Value)
        programVersionExamSequenceTable.DataSource = exams
        ViewState("examsSequenceDataSource") = exams
        programVersionExamSequenceTable.Rebind()
    End Sub

    Protected Sub btnUpdateGrid_OnClick(sender As Object, e As EventArgs)
        Throw New NotImplementedException
    End Sub
End Class
