﻿<%@ Page Title="Transfer Components By Student" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="TransferComponentsByStudent.aspx.vb" Inherits="TransferComponentsByStudent" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Transfer Course Components</title>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable" align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0"
                                            align="center">
                                            <tr runat="server" id="trStuSearchControl">
                                                <td class="contentcell">
                                                    <FAME:StudentSearch ID="StudSearch1" runat="server" OnTransferToParent="TransferToParent" ShowAcaYr="false" ShowTerm="false" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="boxContainer">
                                            <h3><%=Header.Title  %></h3>
                                            <asp:Panel ID="pnlDisplayInfo" runat="server" Visible="false">
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="500px" border="0"
                                                    align="center">
                                                    <tr>
                                                        <td class="contentcell" nowrap="nowrap">
                                                            <asp:Label ID="lblStudentDisp" CssClass="label" runat="server">Student</asp:Label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td class="contentcell">
                                                            <asp:TextBox ID="txtStudentDisp" CssClass="textbox" Enabled="false" Width="300px"
                                                                runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell" nowrap="nowrap">
                                                            <asp:Label ID="lblEnrollDisp" CssClass="label" runat="server">Enrollment</asp:Label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td class="contentcell">
                                                            <asp:TextBox ID="txtEnrollDisp" CssClass="textbox" Enabled="false" Width="300px"
                                                                runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0"
                                                align="center">
                                                <tr>
                                                    <td>
                                                        <table width="100%" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblGridTitle" Text="Incomplete Courses" CssClass="headerLabel"></asp:Label>
                                                                </td>
                                                                <td align="right">
                                                                    <telerik:RadButton ID="btnTransferComponents" runat="server" Text="Transfer Components"></telerik:RadButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <telerik:RadGrid ID="rgIncompleteCourses" runat="server" EnableLinqExpressions="false" AutoGenerateColumns="false">
                                                                        <ClientSettings>
                                                                            <Selecting AllowRowSelect="False" />
                                                                        </ClientSettings>
                                                                        <MasterTableView DataKeyNames="ResultId" Name="IncompleteCourses" HierarchyLoadMode="Client" AllowFilteringByColumn="true">
                                                                            <DetailTables>
                                                                                <telerik:GridTableView Name="CompletedComponents" DataKeyNames="InstrGrdBkWgtId" Width="100%">
                                                                                    <ParentTableRelation>
                                                                                        <telerik:GridRelationFields DetailKeyField="ResultId" MasterKeyField="ResultId"></telerik:GridRelationFields>
                                                                                    </ParentTableRelation>
                                                                                    <Columns>
                                                                                        <telerik:GridBoundColumn DataField="Description" HeaderText="Completed Component"></telerik:GridBoundColumn>
                                                                                        <telerik:GridBoundColumn DataField="ComponentType" HeaderText="Component Type"></telerik:GridBoundColumn>
                                                                                        <telerik:GridBoundColumn DataField="Score" HeaderText="Score">
                                                                                        </telerik:GridBoundColumn>
                                                                                    </Columns>
                                                                                </telerik:GridTableView>
                                                                            </DetailTables>
                                                                            <Columns>
                                                                                <telerik:GridBoundColumn UniqueName="Code" DataField="Code" HeaderText="Course Code" ReadOnly="true" AllowFiltering="false">
                                                                                    <HeaderStyle Wrap="false" Width="80px" />
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn UniqueName="Descrip" DataField="Descrip" HeaderText="Course Description" ReadOnly="true" AllowFiltering="false">
                                                                                    <HeaderStyle Width="300px" />
                                                                                    <ItemStyle Wrap="false" />
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn UniqueName="TermId" DataField="TermId" Visible="false"></telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn UniqueName="ClsSectionId" DataField="ClsSectionId" Display="false"></telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn UniqueName="TermDescrip" DataField="TermDescrip" HeaderText="Term" ReadOnly="true" AllowFiltering="true">
                                                                                    <HeaderStyle Width="235px" />
                                                                                    <FilterTemplate>
                                                                                        <telerik:RadComboBox ID="rcbTerm" UniqueName="rcbTerm" runat="server" SkinID="FilterComboBox"
                                                                                            DataTextField="TermDescrip" DataValueField="TermId" AutoPostBack="true" Width="230px"
                                                                                            OnSelectedIndexChanged="rcbTerm_SelectedIndexChanged" OnPreRender="rcbTerm_PreRender">
                                                                                        </telerik:RadComboBox>
                                                                                    </FilterTemplate>
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Available Classes for Registration">
                                                                                    <ItemTemplate>
                                                                                        <telerik:RadComboBox ID="rcbAvailableClasses" runat="server" SkinID="EditComboBox" Width="100%"
                                                                                            DataTextField="TermDescrip" DataValueField="ClsSectionId"
                                                                                            OnClientSelectedIndexChanged="rcb_SelectedIndexChanged">
                                                                                            <HeaderTemplate>
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td>Term</td>
                                                                                                        <td style="width: 100px;">Class Section</td>
                                                                                                        <td style="width: 75px;">Max Students</td>
                                                                                                        <td style="width: 50px;">Assigned</td>
                                                                                                        <td style="width: 50px;">Remaining</td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td><%# Eval("TermDescrip")%></td>
                                                                                                        <td style="width: 100px;"><%# Eval("ClsSection")%></td>
                                                                                                        <td style="width: 75px;"><%# Eval("MaxStudents")%></td>
                                                                                                        <td style="width: 50px;"><%# Eval("Assigned")%></td>
                                                                                                        <td style="width: 50px;"><%# Eval("Remaining")%></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                        </telerik:RadComboBox>
                                                                                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                                                                            <script type="text/javascript">
                                                                                                function rcb_SelectedIndexChanged(sender, args) {
                                                                                                    var inputElement = sender.get_inputDomElement();
                                                                                                    var rcbIndex = sender.get_selectedIndex();
                                                                                                    if (rcbIndex > 0) {
                                                                                                        inputElement.style.fontWeight = "bold";
                                                                                                    }
                                                                                                    else {
                                                                                                        inputElement.style.fontWeight = "normal";
                                                                                                    }
                                                                                                }
                                                                                            </script>
                                                                                        </telerik:RadScriptBlock>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                    </telerik:RadGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
