
Imports System.Collections
Imports FAME.Advantage.Common
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Linq
Imports System.Data
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports System.Threading.Tasks
Imports Advantage.AFA.Integration
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.BusinessFacade.TimeClock
Imports Telerik.Web.UI.Calendar
Imports FAME.Advantage.Domain.MultiTenant.Infrastructure.API
Imports FAME.Advantage.Api.Library.AcademicRecords
Imports FAME.Advantage.Api.Library.Models
Imports NHibernate.Util


Partial Class ClsSectAttendance
    Inherits BasePage

    Public Class GlobalVariables
        Public Shared StudentEnrollmentsList As List(Of Guid) = New List(Of Guid)()
    End Class

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor. 
        InitializeComponent()
    End Sub

#End Region

#Region "Variables"

    Private pObj As New UserPagePermissionInfo
    Private strError As String
    ''Added by Saraswathi Lakshmanan- on 25-sept-2008 to fix issue 14296
    Private isAcademicAdvisor As Boolean
    Private instructorId As String
    Private username As String
    Private campusId As String
    Private userId As String
    Public UseTimeCLock As Boolean
    Private FromDate As Date
    Private ToDate As Date
    Protected WithEvents btnhistory As Button
    Protected WithEvents dgStudents As DataGrid
    Protected WithEvents dgActuals As DataGrid
    Protected WithEvents dgMeetDays As DataGrid
    Protected WithEvents dgDates As DataGrid
    Protected WithEvents tblAttendance As Table
    'Private revCount As Integer
    'Private tblCounter As String
    Dim dsStudent As New DataSet
    Dim dsPunch As New DataSet
#End Region

#Region "PAGE EVENTS"
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnBuildList)
        'controlsToIgnore.Add(CalButton1)  'Calendar control for the Start Date on the LHS
        ' controlsToIgnore.Add(CalButton2)  'Calendar control for the End Date on the LHS
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        btnBuildList.Attributes.Add("onclick", "SetHiddenText();return true;")

        '        Dim m_Context As HttpContext
        '  Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = AdvantageSession.UserState.UserId.ToString


        Dim advantageUserState As User = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        ''Added by Saraswathi Lakshmanan
        ''If the UseCohortStartDateforfilter is set to Yes in WebConfigAppSettings 
        ''then the Term DropDownList is made visible false and CohortStartdateDropDown 
        ''is made visible true

        If MyAdvAppSettings.AppSettings("UseCohortStartDateForFilters").ToString.ToLower = "yes" Then
            trTerm.Visible = False
            trCohortStartDate.Visible = True
            trTerm1.Visible = False
            trCohortStartDate1.Visible = True
        Else
            trTerm.Visible = True
            trCohortStartDate.Visible = False
            trTerm1.Visible = True
            trCohortStartDate1.Visible = False
        End If

        ' ''Added -not to show post by exception when it is cosmo school

        'If Not (SingletonAppSettings.AppSettings("TimeClockClassSection") Is Nothing) Then
        '    If SingletonAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
        '        chkPostByException.Visible = False
        '    Else
        '        chkPostByException.Visible = True
        '    End If
        'Else
        '    chkPostByException.Visible = True
        'End If

        If Not Page.IsPostBack Then
            'We want to first populate the ddlTerms control
            Dim facClsSectAtt As New ClsSectAttendanceFacade
            Dim dtTerms As New DataSet
            Dim dsShift As New DataSet

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''Added BY Saraswathi Lakshmanan to fix
            '' issue 14296: Instructor Supervisors and Education Directors(AcademicAdvisors)  
            ''need to be able to post and modify attendance. 
            '' To Find if the User is a Academic Advisors


            instructorId = HttpContext.Current.Session("UserId")
            username = AdvantageSession.UserState.UserName

            ''Added by Saraswathi
            ViewState("Instructor") = instructorId
            'isAcademicAdvisor = facClsSectAtt.GetIsRoleAcademicAdvisor(instructorId)
            isAcademicAdvisor = facClsSectAtt.GetIsRoleAcademicAdvisorORInstructorSuperVisor(instructorId)
            ViewState("isAcademicAdvisor") = isAcademicAdvisor

            dtTerms = facClsSectAtt.GetTermsbyUserandRoles(userId, username, isAcademicAdvisor, campusId)

            With ddlTerms
                .DataSource = dtTerms
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataBind()
                '.Items.Insert(0, New RadComboBoxItem("Select", ""))
                '.SelectedIndex = 0
            End With

            dsShift = facClsSectAtt.GetActiveShiftsByCampus(campusId)

            Dim drShift As DataRow = dsShift.Tables(0).NewRow
            drShift.Item("ShiftDescrip") = "Select"
            drShift.Item("ShiftID") = "00000000-0000-0000-0000-000000000000"
            dsShift.Tables(0).Rows.InsertAt(drShift, 0)

            With ddlShift
                .DataSource = dsShift
                .DataTextField = "ShiftDescrip"
                .DataValueField = "ShiftId"
                .DataBind()
                '.SelectedIndex = 0
            End With

            ' If SingletonAppSettings.AppSettings("UseCohortStartDateForFilters").ToString.ToLower = "yes" Then
            ''CohortStartDate DDL 
            PopulateCohortStartDateDDL()
            ' End If


            'Set the Session("Students") to nothing. This is very important to fix the following issue.
            'If the user leaves the page and then returns to it without closing the browser the
            'Session("Students") variable will still be populated and so the first test in the else
            'clause below will pass and there will be an attempt to build the attendance table when
            'a term is selected. This will cause an error.
            Session("Students") = Nothing

        Else
            If Not Session("Students") Is Nothing Then
                If HiddenText.Value <> "YES" Then
                    '   BuildAttendanceGrid()
                Else
                    HiddenText.Value = ""
                End If
            Else
                If HiddenText.Value = "YES" Then
                    HiddenText.Value = ""
                End If

            End If
            lblHeading.Text = String.Empty
            lblHeadingBottom.Text = String.Empty
            lblMessage.Text = String.Empty

        End If

        'If pObj.HasFull Or pObj.HasAdd Then
        '    btnSave.Enabled = True
        'Else
        '    btnSave.Enabled = False
        'End If
        btnNew.Enabled = False
        btnDelete.Enabled = False
        'Hide all labels 
        lblsun.Visible = False
        lblmon.Visible = False
        lbltue.Visible = False
        lblwed.Visible = False
        lblthur.Visible = False
        lblfri.Visible = False
        lblsat.Visible = False
        btnPreviousWeek.Visible = False
        btnNextWeek.Visible = False

        'If hdnBindCurrentWeek.Value = "true" Then
        '    hdnBindCurrentWeek.Value = "false"
        '    'BindCurrentWeek()
        '    dgAttendance.Rebind()
        'End If

    End Sub
    Private Sub HideRadWindow()
        'Hide radWindow
        UserListDialog.VisibleOnPageLoad = False
        HideGrid()
    End Sub
    Private Sub HideGrid()
        dgAttendance.Visible = False
        btnPreviousWeek.Visible = False
        btnNextWeek.Visible = False
        pnlLegend.Visible = False
        btnExportToExcell.Visible = False

        lblsun.Visible = False
        lblmon.Visible = False
        lbltue.Visible = False
        lblwed.Visible = False
        lblthur.Visible = False
        lblfri.Visible = False
        lblsat.Visible = False
    End Sub
    Private Sub ShowGrid()
        dgAttendance.Visible = True
        'btnPreviousWeek.Visible = True
        'btnNextWeek.Visible = True
        pnlLegend.Visible = True
        btnExportToExcell.Visible = True

        'lblsun.Visible = True
        'lblmon.Visible = True
        'lbltue.Visible = True
        'lblwed.Visible = True
        'lblthur.Visible = True
        'lblfri.Visible = True
        'lblsat.Visible = True
    End Sub

#End Region

#Region "Searches - drop downs - binding, selected index changes"
    Protected Sub ddlcohortStDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlcohortStDate.SelectedIndexChanged
        ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
        ddlTerms.SelectedIndex = 0
        ViewState("Term") = ddlTerms.SelectedItem.Value.ToString()
        If (ViewState("CohortStartDate") <> "") Then
            PopulateClsSectionDDLbasedonCohortStDate(ViewState("CohortStartDate"))
        End If

    End Sub
    Private Sub ddlClasses_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlClasses.SelectedIndexChanged
        Dim facClsSectAtt As New ClsSectAttendanceFacade
        Dim dt As New DataTable
        Dim dtClsMeetings As New DataTable

        'Hide radWindow
        HideRadWindow()

        'The select option is not a valid option
        'If ddlClasses.SelectedIndex = 0 Then
        '    DisplayErrorMessage("The selected option is not valid for Classes")
        'Else

        If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId).ToString = "Traditional" Then
            dtClsMeetings = facClsSectAtt.GetClsSectionMeetingsForClsSection(ddlClasses.SelectedItem.Value, False)
        Else
            dtClsMeetings = facClsSectAtt.GetClsSectionMeetingsForClsSection(ddlClasses.SelectedItem.Value, True)
        End If

        dt = facClsSectAtt.GetClsSectionDateRange(ddlClasses.SelectedItem.Value)
        'place it in session so we can reuse it later on when we need to validate that the start and end
        'dates are within the class section date range.
        Session("ClassSectionDateRange") = dt
        Session("ClassSectionMeetings") = dtClsMeetings

        'txtDates.Text = Convert.ToDateTime(dt.Rows(0)("StartDate")).ToShortDateString & " - " & Convert.ToDateTime(dt.Rows(0)("EndDate")).ToShortDateString
        'txtStart.Text = dt.Rows(0)("StartDate").ToShortDateString
        'txtEnd.Text = dt.Rows(0)("EndDate").ToShortDateString

        ddlClsSectmeeting.Items.Clear()
        ddlClsSectmeeting.Text = ""
        ddlClsSectmeeting.ClearSelection()
        With ddlClsSectmeeting
            .DataSource = dtClsMeetings
            .DataTextField = "Descrip"
            .DataValueField = "ClsSectMeetingId"
            .DataBind()
            '.Items.Insert(0, New RadComboBoxItem("Select", ""))
            ' .SelectedIndex = 0
        End With

        If dtClsMeetings.Rows.Count > 0 Then
            txtDates.Text = Convert.ToDateTime(dtClsMeetings.Rows(0)("StartDate")).ToShortDateString & " - " & Convert.ToDateTime(dtClsMeetings.Rows(0)("EndDate")).ToShortDateString
            txtStart.SelectedDate = dtClsMeetings.Rows(0)("StartDate")
            txtEnd.SelectedDate = dtClsMeetings.Rows(0)("EndDate")
        End If
        '   End If

        'We should clear the attendance grid on the rhs if it exists
        DeleteRowsFromAttendanceGrid()
        'Clear the instructions
        lblMessage.Text = ""
        lblInstrName.Text = ""
        lblSecName.Text = ""
        lblClsSectMeeting.Text = ""
        txtDates.Text = ""
        txtStart.Clear()
        txtEnd.Clear()


    End Sub
    Private Sub ddlTerms_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTerms.SelectedIndexChanged
        RefreshClassFilters()

    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlShift.SelectedIndexChanged
        RefreshClassFilters()

    End Sub

    Protected Sub rdpMinimumClassStartDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpMinimumClassStartDate.SelectedDateChanged
        RefreshClassFilters()


    End Sub

    Protected Sub RefreshClassFilters()
        'Hide radWindow
        HideRadWindow()
        'The select option is not a valid option
        'If ddlTerms.SelectedIndex = 0 Then
        '    DisplayErrorMessage("The selected option is not valid for Terms")

        'Else


        'Bring the class sections for the instructor for the selected term
        Dim facClsSectAtt As New ClsSectAttendanceFacade
        Dim dtClsSections As DataTable
        Dim campusId As String
        Dim instructorId As String
        Dim username As String

        instructorId = HttpContext.Current.Session("UserId")
        campusId = HttpContext.Current.Request.Params("cmpid")
        username = AdvantageSession.UserState.UserName

        isAcademicAdvisor = ViewState("isAcademicAdvisor")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ''Optional Parameter Added by Saraswathi Lakshmanan to fix Issue 14296
        'dtClsSections = facClsSectAtt.GetClsSectionsForTermForAttendance(ddlTerms.SelectedItem.Value, campusId, instructorId, username, isAcademicAdvisor)

        If ddlShift.SelectedItem Is Nothing Then
            dtClsSections = facClsSectAtt.GetClsSectionsForTermForAttendance(ddlTerms.SelectedItem.Value, "00000000-0000-0000-0000-000000000000", rdpMinimumClassStartDate.DbSelectedDate, campusId, instructorId, username, isAcademicAdvisor)
        Else
            dtClsSections = facClsSectAtt.GetClsSectionsForTermForAttendance(ddlTerms.SelectedItem.Value, ddlShift.SelectedItem.Value, rdpMinimumClassStartDate.DbSelectedDate, campusId, instructorId, username, isAcademicAdvisor)
        End If

        ddlClasses.Items.Clear()
        ddlClasses.Text = ""
        ddlClasses.ClearSelection()

        With ddlClasses
            .DataSource = dtClsSections
            .DataValueField = "ClsSectionId"
            .DataTextField = "Class"
            .DataBind()
            '.Items.Insert(0, New RadComboBoxItem("Select", ""))
            '.SelectedIndex = 0
        End With
        ddlClasses.SelectedIndex = -1

        'We should clear the attendance grid on the rhs if it exists
        DeleteRowsFromAttendanceGrid()
        'Clear the instructions
        lblMessage.Text = ""
        ddlClsSectmeeting.Items.Clear()
        ' ddlClsSectmeeting.SelectedValue = ""
        ddlClsSectmeeting.Text = ""
        ddlClsSectmeeting.ClearSelection()
        txtDates.Text = ""
        'txtStart.SelectedDate = ""
        'txtEnd.SelectedDate = ""
        lblInstrName.Text = ""
        lblSecName.Text = ""
        lblClsSectMeeting.Text = ""
        txtStart.Clear()
        txtEnd.Clear()
        ' End If
        '
    End Sub

    Protected Sub ddlClsSectmeeting_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlClsSectmeeting.SelectedIndexChanged
        Dim dtClsMeetings As New DataTable

        'Hide radWindow
        HideRadWindow()
        dtClsMeetings = Session("ClassSectionMeetings")
        'txtDates.Text = Convert.ToDateTime(dtClsMeetings.Rows(0)("StartDate")).ToShortDateString & " - " & Convert.ToDateTime(dtClsMeetings.Rows(0)("EndDate")).ToShortDateString
        'txtStart.Text = dtClsMeetings.Rows(0)("StartDate").ToShortDateString
        'txtEnd.Text = dtClsMeetings.Rows(0)("EndDate").ToShortDateString

        'DE6750 Janet Robinson 11/17/2011 
        Dim RowId As Integer = 0
        If ddlClsSectmeeting.SelectedIndex > 0 Then
            'DE6750
            'If ddlClsSectmeeting.SelectedIndex - 1 > 0 Then
            'RowId = ddlClsSectmeeting.SelectedIndex - 1
            RowId = ddlClsSectmeeting.SelectedIndex
        End If
        txtDates.Text = Convert.ToDateTime(dtClsMeetings.Rows(RowId)("StartDate")).ToShortDateString & " - " & Convert.ToDateTime(dtClsMeetings.Rows(RowId)("EndDate")).ToShortDateString
        txtStart.SelectedDate = dtClsMeetings.Rows(RowId)("StartDate")
        txtEnd.SelectedDate = dtClsMeetings.Rows(RowId)("EndDate")
        'DE6750

    End Sub
    Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
        buildListGrid()
        lblOutput.Text = ""
        lblOutputBottom.Text = ""
    End Sub
    Private Sub PopulateCohortStartDateDDL()

        Dim facade As New TermsFacade

        username = AdvantageSession.UserState.UserName
        instructorId = ViewState("Instructor")
        isAcademicAdvisor = ViewState("isAcademicAdvisor")
        campusId = Master.CurrentCampusId

        With ddlcohortStDate
            .DataSource = facade.GetCohortStartDatebyUserandRoles(instructorId, username, isAcademicAdvisor, campusId)
            .DataTextField = "CohortStartDate"
            .DataValueField = "CohortStartDate"
            .DataBind()
            '.Items.Insert(0, New RadComboBoxItem("Select", ""))
            '.SelectedIndex = 0
        End With
    End Sub
    Private Sub PopulateClsSectionDDLbasedonCohortStDate(ByVal CohortStartDate As String)

        ''Added by Saraswathi Lakshmanan to fix Issue 14296
        username = AdvantageSession.UserState.UserName
        instructorId = ViewState("Instructor")
        isAcademicAdvisor = ViewState("isAcademicAdvisor")
        campusId = Master.CurrentCampusId

        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New ClsSectAttendanceFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlClasses
            .DataSource = facade.GetClsSectionsForCohortStartDateForAttendance(ViewState("CohortStartDate"), campusId, instructorId, username, isAcademicAdvisor)
            .DataValueField = "ClsSectionId"
            .DataTextField = "Class"
            .DataBind()
            '.Items.Insert(0, New RadComboBoxItem("Select", ""))
            '.SelectedIndex = 0
        End With

        'We should clear the attendance grid on the rhs if it exists
        DeleteRowsFromAttendanceGrid()
        'Clear the instructions
        lblMessage.Text = ""

    End Sub
    Private Function ValidateLHS() As String
        Dim dtm1 As DateTime
        Dim dtm2 As DateTime
        Dim blnSkip As Boolean
        Dim dt As DataTable
        ''Modified by Saraswathi lakshmanan
        ''To add the cohortStart date 
        ''If term or cohortStDate are not selected then, classSection cannot be fetched.
        If Not ddlcohortStDate.SelectedItem Is Nothing And ddlcohortStDate.Visible Then
            If ddlTerms.SelectedItem.Text = "Select" And ddlcohortStDate.SelectedItem.Text = "Select" Then
                DisplayErrorMessage("Unable to find data. Please select an option.")
                Return "Error"
            ElseIf ddlTerms.SelectedItem.Text <> "Select" Then
                ViewState("Term") = ddlTerms.SelectedItem.Value.ToString
            ElseIf ddlcohortStDate.SelectedItem.Text <> "Select" Then
                ViewState("CohortStartDate") = ddlcohortStDate.SelectedItem.Value.ToString
            End If
        Else
            If ddlTerms.SelectedItem Is Nothing Then
                DisplayErrorMessage("Unable to find data. Please select an option.")
                Return "Error"
            ElseIf ddlTerms.SelectedItem.Text = "Select" Then
                DisplayErrorMessage("Unable to find data. Please select an option.")
                Return "Error"
            ElseIf ddlTerms.SelectedItem.Text <> "Select" Then
                ViewState("Term") = ddlTerms.SelectedItem.Value.ToString
            End If
        End If



        If ddlClasses.SelectedIndex = -1 Then
            strError &= "Unable to find data. Please select an option from Class Section. " & vbCr
        End If

        If ddlClsSectmeeting.SelectedIndex = -1 Then
            strError &= "Unable to find data. Please select an option from Class Section Meeting. " & vbCr
        End If


        If txtStart.SelectedDate Is Nothing Then
            strError &= "Start date is required. " & vbCr
        Else
            Try
                'Validate that the value entered is a valid datetime entry
                dtm1 = Convert.ToDateTime(txtStart.SelectedDate)
                ''Validate that attendance is not being posted beyond the current system date
                'If Convert.ToDateTime(txtStart.Text) > Date.Now Then
                '    strError &= "Start date cannot be greater than the current date " & vbCr
                'End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                strError &= "The start date you entered is not a valid date." & vbCr
                blnSkip = True
            End Try

        End If

        If txtEnd.SelectedDate Is Nothing Then
            strError &= "End date is required. " & vbCr
        Else
            Try
                'Validate that the value entered is a valid datetime entry
                dtm2 = Convert.ToDateTime(txtEnd.SelectedDate)
                'If Convert.ToDateTime(txtEnd.Text) > Date.Now Then
                '    strError &= "End date cannot be greater than the current date " & vbCr
                'End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                strError &= "The end date you entered is not a valid date."
                'We need to exit the sub at this point as the following validations only work with
                'valid date entries
                Return strError
            End Try

        End If

        'Validate that the date range selected is within the date range of the class section.
        'This can only be performed if we have valid date entries.
        If blnSkip = False Then
            If Not (txtStart.SelectedDate Is Nothing And txtEnd.SelectedDate Is Nothing) Then
                'If there are no items in ddlClasses or if no item is selected in it then we will not have the
                'DataTable in session that stores the start and end date for the class section.
                If ddlClasses.SelectedIndex = 0 Or ddlClasses.SelectedIndex = -1 Or ddlClasses.Items.Count = 0 Then
                    'Do nothing. We dont want to execute the test in the else clause.
                Else
                    '    If Convert.ToDateTime(txtStart.Text) >= DirectCast(Session("ClassSectionDateRange"), DataTable).Rows(0)("StartDate") _
                    '                                And Convert.ToDateTime(txtEnd.Text) <= DirectCast(Session("ClassSectionDateRange"), DataTable).Rows(0)("EndDate") Then
                    '        'do nothing as this is what we want
                    '    Else
                    '        strError &= "The date range you have entered is not within the date range for the selected class section."

                    '    End If


                    'DE6750
                    dt = CType(Session("ClassSectionDateRange"), DataTable)
                    If Convert.ToDateTime(txtStart.SelectedDate) >= dt.Rows(0)("StartDate") _
                                              And Convert.ToDateTime(txtEnd.SelectedDate) <= dt.Rows(0)("EndDate") Then
                        'do nothing as this is what we want
                    Else
                        strError &= "The date range you have entered is not within the date range for the selected class section."

                    End If
                    'DE6750
                End If

            End If

            'Validate that the start date is not greater than the end date
            'This can only be performed if we have valid date entries.
            If blnSkip = False Then
                If Not (txtStart.SelectedDate Is Nothing And txtEnd.SelectedDate Is Nothing) Then
                    If Convert.ToDateTime(txtStart.SelectedDate) > Convert.ToDateTime(txtEnd.SelectedDate) Then
                        strError &= "The start date cannot be greater than the end date."
                    End If
                End If
            End If
        End If

        Return strError
    End Function
    Private Sub DeleteRowsFromAttendanceGrid()
        '        Dim rowCounter As Integer
        Dim tbl As Table

        Try
            If pnlAttendance.Controls.Count() = 0 Then
                Return
            End If

            tbl = DirectCast(pnlAttendance.Controls(0), Table)
            Do While tbl.Rows.Count > 0
                tbl.Rows.Remove(tbl.Rows(0))
            Loop
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'No need to do anything. It means that the attendance grid has not been created as yet.
        End Try

    End Sub
    Private Sub buildListGrid()
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim dtStudents As DataTable
        Dim dtMeetDays As DataTable
        Dim dtActualAttendace As DataTable
        Dim dtStudentsLOA As DataTable
        Dim arrDates As ArrayList
        '        Dim dtm As DateTime
        'Dim tbl As New Table
        Dim dictionaryofDates As Dictionary(Of Date, Date) = New Dictionary(Of Date, Date)()
        strError = ValidateLHS()
        'ShowGrid()
        If strError <> "" Then
            lblMessage.Text = ""
            DisplayErrorMessage(strError)
        Else
            ShowGrid()
            If Not txtStart.SelectedDate Is Nothing Then
                FromDate = GetStartDateoftheWeek(CDate(txtStart.SelectedDate))
            End If
            If Not txtEnd.SelectedDate Is Nothing Then
                ToDate = GetEndDateoftheWeek(CDate(txtEnd.SelectedDate))
            End If

            Dim fac As New TimeClockFacade
            Session("ClsSectionUsesTimeClock") = fac.IsClsSectionUsesTimeClock(ddlClasses.SelectedItem.Value)
            'Get DataTable with students who are registered for this class section
            dtStudents = facAttendance.GetStudentsInClsSection(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value)
            Session("Students") = dtStudents

            'Get DataTable with LOA info for students who are in the class section
            dtStudentsLOA = facAttendance.GetLOAInfoForStudentsInClassSection(ddlClasses.SelectedItem.Value)
            Session("StudentsLOAInfo") = dtStudentsLOA
            'Get DataTable with days of class section meetings
            If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId).ToString = "Traditional" Then
                dtMeetDays = facAttendance.GetClsSectionMeetingDaysgivenClsSectionandMeeting(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value, False)
            Else
                dtMeetDays = facAttendance.GetClsSectionMeetingDaysgivenClsSectionandMeeting(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value, True)
            End If
            Session("MeetDays") = dtMeetDays

            'Get students class section attendance postings
            dtActualAttendace = facAttendance.GetStudentsAttendanceForClsSectionGivenClsSectionandMeeting(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value, FromDate, DateAdd("d", 1, ToDate), campusId.ToString)
            Session("ActualAttendance") = dtActualAttendace

            If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId).ToString = "Traditional" Then
                arrDates = facAttendance.NewGetMeetDatesArrayListFromRange(dtMeetDays, ddlClasses.SelectedItem.Value, FromDate, ToDate, False, campusId)
                dictionaryofDates = facAttendance.NewGetMeetDatesArrayListFromRange_Dict(dtMeetDays, ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value, FromDate, ToDate, False, campusId)
            Else
                'this change makes the program work with the new arClsSectMeetings table. Anatoly 7/12/2006
                arrDates = facAttendance.NewGetMeetDatesArrayListFromRange(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value, FromDate, ToDate, False, campusId)
                dictionaryofDates = facAttendance.NewGetMeetDatesArrayListFromRange_Dict(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value, FromDate, ToDate, False, campusId)
            End If
            Session("DictionaryofDates") = dictionaryofDates
            Session("Dates") = arrDates


            'Place the attendance unit type in viewstate so it can be reused on postbacks
            ViewState("AttendanceUnitType") = facAttendance.GetClassSectionAttendanceType(ddlClasses.SelectedItem.Value)
            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                lblMessage.Text = "Enter Minutes Present"
                lblHeading.Text = "Enter Minutes Present"
                lblHeadingBottom.Text = "Enter Minutes Present"
            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                lblMessage.Text = "Enter Hours Present"
                lblHeading.Text = "Enter Hours Present"
                lblHeadingBottom.Text = "Enter Hours Present"
            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "PRESENT ABSENT" Then
                lblMessage.Text = "Enter P, A, E or T"
                lblMessage.Text &= "<br>" & "P=Present A=Absent E=Excused T=Tardy "
                lblHeading.Text = "Enter P, A, E or T"
                lblHeading.Text &= "<br>" & "P=Present A=Absent E=Excused T=Tardy "
                lblHeadingBottom.Text = "Enter P, A, E or T"
                lblHeadingBottom.Text &= "<br>" & "P=Present A=Absent E=Excused T=Tardy "
            End If

            GetNonEditableDaysforStudent(ddlClasses.SelectedItem.Value, campusId)
            pnlHeader.Visible = True
            lblInstrName.Text = facAttendance.GetInstructorName(ddlClasses.SelectedItem.Value)
            lblSecName.Text = ddlClasses.SelectedItem.Text
            lblClsSectMeeting.Text = ddlClsSectmeeting.SelectedItem.Text
            ''Create a table and Populate the table with the details of the student attendance
            FromDate = GetStartDateoftheWeek(txtStart.SelectedDate)
            ToDate = GetEndDateoftheWeek(txtEnd.SelectedDate)
            ViewState("FromDate") = FromDate
            ViewState("ToDate") = ToDate
            RefreshGrid()
            UserListDialog.VisibleOnPageLoad = False
        End If
    End Sub
#End Region

#Region "dgAttendance - BINDING"
    Protected Sub dgAttendance_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles dgAttendance.NeedDataSource
        RefreshGrid(False)
    End Sub
    Private Sub BindCurrentWeekLabels()
        'Dim dtStudentDetails As DataTable
        'dtStudentDetails = ViewState("dtStudentDetails")
        Dim FromDate As Date
        FromDate = ViewState("FromDate")
        'dgAttendance.DataSource = dtStudentDetails.Select("SunDate LIke '" + Format(CDate(FromDate), "MM/dd/yyyy") + "%'")
        lblsun.Text = Format(FromDate, "MM/dd")
        lblmon.Text = Format(FromDate.AddDays(1), "MM/dd")
        lbltue.Text = Format(FromDate.AddDays(2), "MM/dd")
        lblwed.Text = Format(FromDate.AddDays(3), "MM/dd")
        lblthur.Text = Format(FromDate.AddDays(4), "MM/dd")
        lblfri.Text = Format(FromDate.AddDays(5), "MM/dd")
        lblsat.Text = Format(FromDate.AddDays(6), "MM/dd")

    End Sub
    Protected Sub dgAttendance_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles dgAttendance.ItemDataBound

        'DE8711: NZ reworked ----------------------------------------------------
        If (TypeOf (e.Item) Is GridDataItem) Then


            Dim item As GridDataItem = e.Item

            ' JG - 09/8/2014 THIS CODE CANNOT WORK BECAUSE IT ALWAYS RETURNS THE FIRST ROW OF DATA IN THE GRID, NOT THE CURRENT ROW
            'Dim SysStatusValue As String = item.OwnerTableView.DataKeyValues(0)("SysStatusId").ToString()
            'Dim StuEnrollId As String = item.OwnerTableView.DataKeyValues(0)("StuEnrollID").ToString()
            'Dim DateDetermined As String = item.OwnerTableView.DataKeyValues(0)("DateDetermined").ToString()
            'Dim StudentStartDate As String = item.OwnerTableView.DataKeyValues(0)("StudentStartDate").ToString()


            Dim SysStatusValue As String = item("SysStatusId").Text
            Dim StuEnrollId As String = item("StuEnrollID").Text
            Dim DateDetermined As String = item("DateDetermined").Text
            Dim StudentStartDate As String = item("StudentStartDate").Text


            Dim PostAttDuringHoliday As Boolean = False
            If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then PostAttDuringHoliday = False Else PostAttDuringHoliday = True
            Dim weekDays As String() = {"Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"}
            For Each strWeekDay As String In weekDays
                Dim chk As CheckBox = TryCast(item.FindControl("chk" & strWeekDay & "Tardy"), CheckBox)
                Dim chk2 As CheckBox = TryCast(item.FindControl("chk" & strWeekDay & "Excused"), CheckBox)
                Dim lblDate As Label = TryCast(item.FindControl("lbl" & strWeekDay & "Date"), Label)
                Dim lblSch As Label = TryCast(item.FindControl("lbl" & strWeekDay & "Sch"), Label)
                Dim txtBx As TextBox = TryCast(item.FindControl("txt" & strWeekDay & "Act"), TextBox)
                Dim Cell As TableCell = item(strWeekDay & "Act")
                Dim regSch As RegularExpressionValidator = TryCast(item.FindControl("regSchDate" & strWeekDay), RegularExpressionValidator)
                Dim regNoSch As RegularExpressionValidator = TryCast(item.FindControl("regNotSchDate" & strWeekDay), RegularExpressionValidator)

                If Not IsNothing(lblSch) AndAlso lblSch.Text <> "0" Then item(strWeekDay & "Act").BackColor = Color.Aquamarine

                If Not IsNothing(lblDate) Then
                    Dim lblValue As String = lblDate.Text
                    If IsDate(lblValue) AndAlso CDate(lblValue) > DateTime.Now() Then
                        Cell.BackColor = Color.LightGray
                        Cell.Enabled = False
                    End If

                    Dim dayEditableCheck As String
                    Dim stStartDate As Date
                    If Date.TryParse(StudentStartDate, stStartDate) Then
                        dayEditableCheck = IsDayEditable(CDate(lblDate.Text), StuEnrollId, SysStatusValue, DateDetermined, stStartDate)
                    Else
                        dayEditableCheck = ""
                    End If

                    If Not String.IsNullOrEmpty(dayEditableCheck) Then
                        If dayEditableCheck = "Holiday" And PostAttDuringHoliday = True Then
                        Else
                            Cell.BackColor = Color.LightGray
                            Cell.Enabled = False
                        End If
                    End If

                    'DE13057
                    'If it is not a scheduled day and post attendance on holiday is false then disable
                    Dim drCurrentWorkDaySchedule As DataRow()

                    drCurrentWorkDaySchedule = Session("MeetDays").Select("WorkdaysDescrip='" + CDate(lblDate.Text).DayOfWeek.ToString.Substring(0, 3) + "'")

                    If drCurrentWorkDaySchedule.Length = 0 And PostAttDuringHoliday = False Then
                        Cell.BackColor = Color.LightGray
                        Cell.Enabled = False
                    End If

                End If
                If Not IsNothing(txtBx) Then
                    Select Case ViewState("AttendanceUnitType").ToString.ToUpper
                        Case "MINUTES", "CLOCK HOURS"
                            txtBx.MaxLength = 5
                            regSch.ErrorMessage = "Invalid!"
                            regSch.ValidationExpression = "^[0-9.]{1,5}$"
                            regNoSch.ErrorMessage = "Invalid!"
                            regNoSch.ValidationExpression = "^[0-9.]{1,5}$"

                            If txtBx.Text <> "" Then
                                If lblSch.Text <> "" Then
                                    If CDec(txtBx.Text) > CDec(lblSch.Text) Then
                                        Cell.BackColor = Color.LightSalmon
                                    ElseIf CDec(txtBx.Text) < CDec(lblSch.Text) Then
                                        Cell.BackColor = Color.LightBlue
                                    End If
                                Else
                                    Cell.BackColor = Color.LightBlue
                                End If
                            End If
                        Case Else
                            txtBx.MaxLength = 1
                            regSch.ErrorMessage = "Enter P, A, E or T"
                            regSch.ValidationExpression = "^(P|p|A|a|E|e|T|t|M|m)$"
                            regNoSch.ErrorMessage = "Enter P, A, E or T"
                            regNoSch.ValidationExpression = "^(P|p|M|m)$"
                            If txtBx.Text <> "" Then
                                If txtBx.Text.ToUpper = "P" And CDec(lblSch.Text) = 0 Then
                                    Cell.BackColor = Color.LightSalmon
                                End If
                                If txtBx.Text.ToUpper = "A" Then
                                    Cell.BackColor = Color.LightBlue
                                End If
                            End If
                    End Select
                End If
                If Not IsNothing(chk) AndAlso chk.Checked = True Then
                    Cell.BackColor = Color.BlanchedAlmond
                End If
            Next


            If Not Session("ClsSectionUsesTimeClock") Is Nothing Then
                If Session("ClsSectionUsesTimeClock") = True Then
                    ''make the cells readonly true
                    ''Change color for Tardy Dates
                    DirectCast(item.FindControl("chkSunTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkMonTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkTueTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkWedTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkThurTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkFriTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkSatTardy"), CheckBox).Attributes.Add("onclick", "return false;")

                    DirectCast(item.FindControl("chkSunExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkMonExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkTueExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkWedExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkThurExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkFriExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    DirectCast(item.FindControl("chkSatExcused"), CheckBox).Attributes.Add("onclick", "return false;")

                    DirectCast(item.FindControl("txtSunAct"), TextBox).ReadOnly = True
                    DirectCast(item.FindControl("txtMonAct"), TextBox).ReadOnly = True
                    DirectCast(item.FindControl("txtTueAct"), TextBox).ReadOnly = True
                    DirectCast(item.FindControl("txtWedAct"), TextBox).ReadOnly = True
                    DirectCast(item.FindControl("txtThurAct"), TextBox).ReadOnly = True
                    DirectCast(item.FindControl("txtSatAct"), TextBox).ReadOnly = True
                    DirectCast(item.FindControl("txtFriAct"), TextBox).ReadOnly = True

                Else
                    ''Make the cells readonly false
                    ''make the cells readonly true
                    ''Change color for Tardy Dates
                    DirectCast(item.FindControl("txtSunAct"), TextBox).ReadOnly = False
                    DirectCast(item.FindControl("txtMonAct"), TextBox).ReadOnly = False
                    DirectCast(item.FindControl("txtTueAct"), TextBox).ReadOnly = False
                    DirectCast(item.FindControl("txtWedAct"), TextBox).ReadOnly = False
                    DirectCast(item.FindControl("txtThurAct"), TextBox).ReadOnly = False
                    DirectCast(item.FindControl("txtSatAct"), TextBox).ReadOnly = False
                    DirectCast(item.FindControl("txtFriAct"), TextBox).ReadOnly = False

                    If DirectCast(item.FindControl("lblSunSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkSunTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblMonSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkMonTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblTueSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkTueTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblWedSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkWedTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblThurSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkThurTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblFriSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkFriTardy"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblSatSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkSatTardy"), CheckBox).Attributes.Add("onclick", "return false;")

                    If DirectCast(item.FindControl("lblSunSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkSunExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblMonSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkMonExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblTueSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkTueExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblWedSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkWedExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblThurSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkThurExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblFriSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkFriExcused"), CheckBox).Attributes.Add("onclick", "return false;")
                    If DirectCast(item.FindControl("lblSatSch"), Label).Text = "0" Then DirectCast(item.FindControl("chkSatExcused"), CheckBox).Attributes.Add("onclick", "return false;")


                End If

                Dim attUnitType As String = ViewState("AttendanceUnitType").ToString.ToUpper()

                If (attUnitType = "CLOCK HOURS") Or (attUnitType = "MINUTES") Then
                    DirectCast(item.FindControl("chkSunExcused"), CheckBox).Visible = True
                    DirectCast(item.FindControl("chkMonExcused"), CheckBox).Visible = True
                    DirectCast(item.FindControl("chkTueExcused"), CheckBox).Visible = True
                    DirectCast(item.FindControl("chkWedExcused"), CheckBox).Visible = True
                    DirectCast(item.FindControl("chkThurExcused"), CheckBox).Visible = True
                    DirectCast(item.FindControl("chkFriExcused"), CheckBox).Visible = True
                    DirectCast(item.FindControl("chkSatExcused"), CheckBox).Visible = True

                    chkExcused.Visible = True
                End If

            ElseIf TypeOf (e.Item) Is GridPagerItem Then
                'set custom pager size
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"50", "100", "150", "200"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    'add the following line
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", dgAttendance.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        End If

    End Sub
    Private Function GetDurationForMeetDate(ByVal MeetDays As DataTable, ByVal sDay As String, ByVal dtm As DateTime) As String
        'Dim sDay As String
        Dim dr As DataRow

        'Get the short day value
        'sDay = GetShortDayName(dtm)

        ''Added by Saraswathi on Jan 12 2009
        ''Match the day, then,meetdate between Start and enddate and then meettime is matched
        ''this gives the duration of the class
        'Loop through the days that the class section meets and see which one this day matches one
        ''Previously it was comparing only against the day.
        '' the hours was returned for the first record it encounteres with the given day,
        '' hence, it is fixed to show the correct hours based on the date and time

        For Each dr In MeetDays.Rows
            If dr("WorkDaysDescrip").ToString() = sDay Then
                If dtm.Date >= CType(dr("StartDate"), Date).Date And dtm.Date <= CType(dr("EndDate"), Date).Date Then
                    If dtm.TimeOfDay = CType(dr("TimeIntervalDescrip"), Date).TimeOfDay Then
                        Return "(" & Math.Round(dr("Duration"), 2) & " hrs)"
                    End If
                End If
            End If
        Next

        Return ""
    End Function
    Private Function CreateStudentAttendanceDetailsTable() As DataTable

        Dim dt As New DataTable("StudentAttendance")

        dt.Columns.Add(New DataColumn("StuEnrollId", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentName", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StatusDescrip", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SysStatusID", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PrgVerDescrip", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("UseTimeClock", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SSN", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentStartDate", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("DateDetermined", Type.GetType("System.String")))


        dt.Columns.Add(New DataColumn("LDADate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunTardy", System.Type.GetType("System.String")))


        dt.Columns.Add(New DataColumn("MonExcused", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueExcused", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedExcused", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurExcused", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriExcused", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatExcused", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunExcused", System.Type.GetType("System.String")))
        Return dt

    End Function
    Private Function BuildMeetingDatesDataTable(ByVal AttUnitType As String) As DataTable
        Dim dtmeetDays As New DataTable
        dtmeetDays = DirectCast(Session("MeetDays"), DataTable)
        If AttUnitType = "hrs" Then

            Dim MeetDaysQuery = From Scheduledt In dtmeetDays.AsEnumerable()
                                Select Scheduledt
            If MeetDaysQuery.Count > 0 Then
                dtmeetDays = MeetDaysQuery.CopyToDataTable()
            Else
                dtmeetDays = New DataTable()
            End If

        ElseIf AttUnitType = "P/A" Then
            Dim MeetDaysQuery = From Scheduledt In dtmeetDays.AsEnumerable()
                                Select Scheduledt
            If MeetDaysQuery.Count > 0 Then
                dtmeetDays = MeetDaysQuery.CopyToDataTable()
            Else
                dtmeetDays = New DataTable()
            End If
        ElseIf AttUnitType = "min" Then

            Dim meetDaysQuery = (From Scheduledt In dtmeetDays.AsEnumerable()
                                 Select WorkDaysID = Scheduledt("WorkDayId"), WorkDaysDescrip = Scheduledt("WorkDaysDescrip"), TimeIntervalDescrip = Scheduledt("TimeIntervalDescrip"), EndTime = Scheduledt("EndTime"),
                                 duration = Scheduledt("duration") * 60, ClsSectionId = Scheduledt("ClsSectionId"), StartDate = Scheduledt("StartDate"), EndDate = Scheduledt("EndDate"), ClsSectMeetingID = Scheduledt("ClsSectMeetingID"),
                                 InstructionTypeID = Scheduledt("InstructionTypeID"), InstructionTypeDescrip = Scheduledt("InstructionTypeDescrip")).ToList

            Dim dtMeetNew As New DataTable
            dtMeetNew = dtmeetDays.Clone

            For Each item As Object In meetDaysQuery
                Dim dr As DataRow
                dr = dtMeetNew.NewRow
                dr("WorkDayId") = item.WorkDaysID
                dr("WorkDaysDescrip") = item.WorkDaysDescrip
                dr("TimeIntervalDescrip") = item.TimeIntervalDescrip
                dr("EndTime") = item.EndTime
                dr("duration") = Math.Round(item.duration, 2)
                dr("ClsSectionId") = item.ClsSectionId
                dr("StartDate") = item.StartDate
                dr("EndDate") = item.EndDate
                dr("ClsSectMeetingID") = item.ClsSectMeetingID
                dtMeetNew.Rows.Add(dr)
            Next

            For Each dr As DataRow In dtmeetDays.Rows
                dr.Delete()
            Next


            For Each item As DataRow In dtMeetNew.Rows
                dtmeetDays.ImportRow(item)
            Next
            dtmeetDays.AcceptChanges()
        End If
        Return dtmeetDays

    End Function
    Private Function BuildStudentActualAttendanceDatatable(ByVal AttUnitType As String) As DataTable
        Dim dtActualAttendace As New DataTable
        dtActualAttendace = DirectCast(Session("ActualAttendance"), DataTable)
        ' dtActualAttendace.Columns.Add("StringActuals", GetType(System.String))
        dtActualAttendace.Columns.Add(New DataColumn("StringActuals", Type.GetType("System.String")))
        For Each item As DataRow In dtActualAttendace.Rows
            item("StringActuals") = item("Actual").ToString
        Next
        dtActualAttendace.Columns.Remove("Actual")
        dtActualAttendace.Columns("StringActuals").ColumnName = "Actual"

        If AttUnitType = "hrs" Then
            Dim dtActualAttNew As DataTable
            dtActualAttNew = dtActualAttendace.Clone
            Dim ActualAttQuery = (From Actualdt In dtActualAttendace.AsEnumerable()
                                  Select New With {.StuEnrollId = Actualdt("StuEnrollId"), .MeetDate = Actualdt("MeetDate"), .Actual = Math.Round(Actualdt("Actual") / 60, 2), .Scheduled = Math.Round(Actualdt("Scheduled") / 60, 2), .Tardy = Actualdt("Tardy"), .Excused = Actualdt("Excused")}).ToList

            For Each item As Object In ActualAttQuery
                Dim dr As DataRow
                dr = dtActualAttNew.NewRow
                dr("StuEnrollId") = item.StuEnrollId
                dr("MeetDate") = item.MeetDate
                dr("Actual") = item.Actual
                dr("Scheduled") = item.Scheduled
                dr("Tardy") = item.Tardy
                dr("Excused") = item.Excused
                dtActualAttNew.Rows.Add(dr)
            Next
            For Each item As DataRow In dtActualAttendace.Rows
                item.Delete()

                ''  dtActualAttendace.Rows(item).Delete()
            Next
            For Each item As DataRow In dtActualAttNew.Rows
                dtActualAttendace.ImportRow(item)
            Next

        ElseIf AttUnitType = "P/A" Then

            Dim ActualAttQuery = From Actualdt In dtActualAttendace.AsEnumerable()
                                 Select Actualdt
            For Each item As DataRow In ActualAttQuery
                ''modified for US2678 August 02 2012
                If item("Actual") = "1" And item("Tardy") = True Then
                    item("Actual") = "T"
                ElseIf item("Actual") = "1" And item("Tardy") = False Then
                    item("Actual") = "P"
                ElseIf item("Actual") = "0" And item("Excused") = True Then
                    item("Actual") = "E"
                ElseIf item("Actual") = "0" Then
                    item("Actual") = "A"
                ElseIf item("Actual") = "9999" Then
                    item("Actual") = ""
                End If
            Next

            If ActualAttQuery.Count > 0 Then
                dtActualAttendace = ActualAttQuery.CopyToDataTable()
                'Else
                '    dtActualAttendace = New DataTable()
            End If

        ElseIf AttUnitType = "min" Then

            Dim ActualAttQuery = From Actualdt In dtActualAttendace.AsEnumerable()
                                 Select Actualdt
            If ActualAttQuery.Count > 0 Then
                dtActualAttendace = ActualAttQuery.CopyToDataTable()
                'Else
                '    dtActualAttendace = New DataTable()
            End If

        End If
        Return dtActualAttendace
    End Function
    Private Sub GetNonEditableDaysforStudent(ByVal ClsSectionID As String, ByVal CampId As String)
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim dsNonEditableDaysforStudent As DataSet
        dsNonEditableDaysforStudent = facAttendance.GetLOASuspensionandHolidaysforStudentsinClsSection(ClsSectionID, CampId)
        ViewState("dsNonEditableDaysforStudent") = dsNonEditableDaysforStudent
    End Sub
    Public Function GetScheduledMinutesForMeetDay(ByVal dtm As DateTime) As String
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim sDay As String
        Dim dtDays As DataTable
        Dim dr As DataRow
        Dim startTime As DateTime
        Dim endTime As DateTime
        Dim schedLength As TimeSpan

        'Get the short day value
        sDay = facAttendance.GetShortDayName(dtm)

        'Loop through the days that the class section meets and see which one this day matches one
        dtDays = DirectCast(Session("MeetDays"), DataTable)
        For Each dr In dtDays.Rows
            'Modified by Saraswathi lakshmanan On June 8 2009
            'StartDate and EndDate are also compared against the given date.
            If dr("WorkDaysDescrip").ToString() = sDay And dr("StartDate") <= dtm.Date And dr("EndDate") >= dtm.Date Then
                startTime = dr("TimeIntervalDescrip")
                endTime = dr("EndTime")
            End If
        Next

        schedLength = endTime.Subtract(startTime)

        Return (schedLength.Hours * 60) + schedLength.Minutes

    End Function
    Private Function PopulateStudentAttendanceTableforGrid() As DataTable

        Dim AttUnitType As String
        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
            AttUnitType = "min"
        ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
            AttUnitType = "hrs"
        Else
            AttUnitType = "P/A"
        End If

        Dim drActuals As DataRow()
        ' Dim drMeetings As DataRow()
        Dim dtStudentDetails As DataTable
        Dim dtMeetDays As New DataTable
        Dim dtActualAttendace As New DataTable
        Dim DictDates As New Dictionary(Of Date, Date)

        dtStudentDetails = CreateStudentAttendanceDetailsTable()
        Dim FromDate As Date = Date.Now
        Dim ToDate As Date = Date.Now
        FromDate = CDate(txtStart.SelectedDate)
        ToDate = IIf(CDate(txtEnd.SelectedDate) > Date.Now, Date.Now, CDate(txtEnd.SelectedDate))
        Dim ProcessDate As Date
        Dim strprocessDate As String
        Dim dtStudents As DataTable
        dtStudents = DirectCast(Session("Students"), DataTable)
        'dtMeetDays = DirectCast(Session("MeetDays"), DataTable)
        'dtActualAttendace = DirectCast(Session("ActualAttendance"), DataTable)
        DictDates = DirectCast(Session("DictionaryofDates"), Dictionary(Of Date, Date))
        Dim row As DataRow = Nothing
        Dim facInputMasks As New InputMasksFacade
        '  Dim Query As System.Data.EnumerableRowCollection(Of <anonymous type> )



        dtMeetDays = BuildMeetingDatesDataTable(AttUnitType)
        dtActualAttendace = BuildStudentActualAttendanceDatatable(AttUnitType)

        For i As Integer = 0 To dtStudents.Rows.Count - 1
            ProcessDate = FromDate

            Select Case ProcessDate.DayOfWeek
                Case DayOfWeek.Sunday
                    '  ProcessDate = ProcessDate - NZ 11/19 assignment has no effect
                Case DayOfWeek.Monday
                    ProcessDate = ProcessDate.AddDays(-1)
                Case DayOfWeek.Tuesday
                    ProcessDate = ProcessDate.AddDays(-2)
                Case DayOfWeek.Wednesday
                    ProcessDate = ProcessDate.AddDays(-3)
                Case DayOfWeek.Thursday
                    ProcessDate = ProcessDate.AddDays(-4)
                Case DayOfWeek.Friday
                    ProcessDate = ProcessDate.AddDays(-5)
                Case DayOfWeek.Saturday
                    ProcessDate = ProcessDate.AddDays(-6)
            End Select
            FromDate = ProcessDate

            Select Case ToDate.DayOfWeek
                Case DayOfWeek.Sunday
                    ToDate = ToDate.AddDays(6)
                Case DayOfWeek.Monday
                    ToDate = ToDate.AddDays(5)
                Case DayOfWeek.Tuesday
                    ToDate = ToDate.AddDays(4)
                Case DayOfWeek.Wednesday
                    ToDate = ToDate.AddDays(3)
                Case DayOfWeek.Thursday
                    ToDate = ToDate.AddDays(2)
                Case DayOfWeek.Friday
                    ToDate = ToDate.AddDays(1)
                Case DayOfWeek.Saturday
                    'ToDate = ToDate - NZ 11/19 assignment has no effect
            End Select


            strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

            While (ProcessDate <= ToDate)

                If ProcessDate.DayOfWeek = DayOfWeek.Sunday Then
                    row = dtStudentDetails.NewRow()
                End If

                row("StuEnrollid") = dtStudents.Rows(i)("StuEnrollID").ToString
                row("StudentName") = dtStudents.Rows(i)("LastName").ToString + ", " + dtStudents.Rows(i)("FirstName").ToString
                row("SysStatusId") = dtStudents.Rows(i)("SysStatusId").ToString
                row("StatusDescrip") = dtStudents.Rows(i)("StatusCodeDescrip").ToString
                row("DateDetermined") = dtStudents.Rows(i)("DateDetermined").ToString
                row("PrgVerDescrip") = dtStudents.Rows(i)("PrgVerDescrip").ToString

                If dtStudents.Rows(i)("UseTimeClock") Is DBNull.Value Then
                    row("UseTimeClock") = "No"
                Else

                    If dtStudents.Rows(i)("UseTimeClock") = False Then
                        row("UseTimeClock") = "No"
                    Else
                        row("UseTimeClock") = "Yes"
                    End If
                End If

                If Not dtStudents.Rows(i)("SSN") Is DBNull.Value Then

                    Dim sn As String = dtStudents.Rows(i)("SSN").ToString()
                    If sn.Length > 0 Then
                        row("SSN") = "***-**-" + dtStudents.Rows(i)("SSN").ToString.Substring(dtStudents.Rows(i)("SSN").ToString.Length - 4, 4)
                    Else
                        row("SSN") = ""
                    End If

                End If

                row("StudentStartDate") = Format(dtStudents.Rows(i)("StartDate"), "MM-dd-yyyy")

                Dim drCurrentWorkDaySchedule As DataRow()

                drCurrentWorkDaySchedule = dtMeetDays.Select("WorkdaysDescrip='" + ProcessDate.DayOfWeek.ToString.Substring(0, 3) + "'")
                Dim TimeofMeeting As TimeSpan
                'DE8628               
                Dim SchDuration As Decimal = 0

                If drCurrentWorkDaySchedule.Length >= 1 Then
                    'DE8628
                    'TimeofMeeting = CDate(drCurrentWorkDaySchedule(0)("TimeIntervalDescrip")).TimeOfDay
                    If drCurrentWorkDaySchedule.Length = 1 Then
                        TimeofMeeting = CDate(drCurrentWorkDaySchedule(0)("TimeIntervalDescrip")).TimeOfDay
                        SchDuration = drCurrentWorkDaySchedule(0)("Duration")
                    Else
                        Dim selectWk As Integer = 0
                        Dim aday As Date = txtStart.SelectedDate
                        Dim firstDate As Date = aday.AddDays(-CInt(aday.DayOfWeek))

                        selectWk = Math.Floor(DateDiff("d", firstDate, ProcessDate) / 7) Mod 2
                        If selectWk = 0 Then
                            TimeofMeeting = CDate(drCurrentWorkDaySchedule(0)("TimeIntervalDescrip")).TimeOfDay
                            SchDuration = drCurrentWorkDaySchedule(0)("Duration")
                        Else
                            TimeofMeeting = CDate(drCurrentWorkDaySchedule(1)("TimeIntervalDescrip")).TimeOfDay
                            SchDuration = drCurrentWorkDaySchedule(1)("Duration")
                        End If
                    End If
                Else
                    TimeofMeeting = TimeSpan.Zero
                End If

                Dim isHoliday As Boolean = False
                If IsDayEditable(ProcessDate, dtStudents.Rows(i)("StuEnrollID").ToString, dtStudents.Rows(i)("SysStatusId").ToString, dtStudents.Rows(i)("DateDetermined").ToString, Format(dtStudents.Rows(i)("StartDate"), "MM-dd-yyyy")) = "Holiday" Then
                    isHoliday = True

                End If


                If DictDates.ContainsKey(ProcessDate + TimeofMeeting) Then
                    drActuals = dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And MeetDate='" + ProcessDate.ToShortDateString.ToString + " " + TimeofMeeting.ToString + "'")

                    If drActuals.Length = 0 Then
                        drActuals = dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And MeetDate='" + ProcessDate.ToShortDateString.ToString + "'")
                    End If


                    If ProcessDate.DayOfWeek = DayOfWeek.Monday Then
                        row("MonDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            row("MonSch") = drActuals(0)("Scheduled")
                            row("MonAct") = drActuals(0)("Actual")
                            row("MonTardy") = drActuals(0)("Tardy")
                            row("MonExcused") = drActuals(0)("Excused")
                        Else
                            If isHoliday = True Then
                                row("MonSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("MonSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("MonSch") = SchDuration
                                Else
                                    row("MonSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("MonAct") = " "
                            row("MonTardy") = " "
                            row("MonExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Tuesday Then
                        row("TueDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            row("TueSch") = drActuals(0)("Scheduled")
                            row("TueAct") = drActuals(0)("Actual")
                            row("TueTardy") = drActuals(0)("Tardy")
                            row("TueExcused") = drActuals(0)("Excused")
                        Else
                            If isHoliday = True Then
                                row("TueSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("TueSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("TueSch") = SchDuration
                                Else
                                    row("TueSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("TueAct") = " "
                            row("TueTardy") = " "
                            row("TueExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Wednesday Then
                        row("WedDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            row("WedSch") = drActuals(0)("Scheduled")
                            row("WedAct") = drActuals(0)("Actual")
                            row("WedTardy") = drActuals(0)("Tardy")
                            row("WedExcused") = drActuals(0)("Excused")
                        Else
                            If isHoliday = True Then
                                row("WedSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("WedSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("WedSch") = SchDuration
                                Else
                                    row("WedSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("WedAct") = " "
                            row("WedTardy") = " "
                            row("WedExcused") = " "

                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Thursday Then
                        row("ThurDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            row("ThurSch") = drActuals(0)("Scheduled")
                            row("ThurAct") = drActuals(0)("Actual")
                            row("ThurTardy") = drActuals(0)("Tardy")
                            row("ThurExcused") = drActuals(0)("Excused")
                        Else
                            If isHoliday = True Then
                                row("ThurSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("ThurSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("ThurSch") = SchDuration
                                Else
                                    row("ThurSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If
                            row("ThurAct") = " "
                            row("ThurTardy") = " "
                            row("ThurExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Friday Then
                        row("FriDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            row("FriSch") = drActuals(0)("Scheduled")
                            row("FriAct") = drActuals(0)("Actual")
                            row("FriTardy") = drActuals(0)("Tardy")
                            row("FriExcused") = drActuals(0)("Excused")
                        Else
                            If isHoliday = True Then
                                row("FriSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("FriSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("FriSch") = SchDuration
                                Else
                                    row("FriSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("FriAct") = " "
                            row("FriTardy") = " "
                            row("FriExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Saturday Then
                        row("SatDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            row("SatSch") = drActuals(0)("Scheduled")
                            row("SatAct") = drActuals(0)("Actual")
                            row("SatTardy") = drActuals(0)("Tardy")
                            row("SatExcused") = drActuals(0)("Excused")
                        Else
                            If isHoliday = True Then
                                row("SatSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("SatSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("SatSch") = SchDuration
                                Else
                                    row("SatSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("SatAct") = " "
                            row("SatTardy") = " "
                            row("SatExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Sunday Then
                        row("SunDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            row("SunSch") = drActuals(0)("Scheduled")
                            row("SunAct") = drActuals(0)("Actual")
                            row("SunTardy") = drActuals(0)("Tardy")
                            row("SunExcused") = drActuals(0)("Excused")
                        Else
                            If isHoliday = True Then
                                row("SunSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("SunSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("SunSch") = SchDuration
                                Else
                                    row("SunSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("SunAct") = " "
                            row("SunTardy") = " "
                            row("SunExcused") = " "
                        End If
                    End If

                    'ElseIf dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And MeetDate='" + ProcessDate.ToShortDateString.ToString + "'").Length >= 1 Then
                ElseIf dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And MeetDate>='" + ProcessDate.ToString + "' And MeetDate<='" + ProcessDate.AddDays(1).ToString + "'").Length >= 1 Then
                    drActuals = dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And  MeetDate>='" + ProcessDate.ToString + "' And MeetDate<='" + ProcessDate.AddDays(1).ToString + "'")

                    If ProcessDate.DayOfWeek = DayOfWeek.Monday Then
                        row("MonDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            'DE6805 Make sure process date is the actual meet date
                            If ProcessDate = CDate(drActuals(0)("MeetDate")).ToShortDateString Then
                                row("MonSch") = drActuals(0)("Scheduled")
                                row("MonAct") = drActuals(0)("Actual")
                                row("MonTardy") = drActuals(0)("Tardy")
                                row("MonExcused") = drActuals(0)("Excused")
                            Else
                                row("MonAct") = " "
                                row("MonTardy") = " "
                                row("MonExcused") = " "
                            End If
                            'DE6805
                        Else
                            If isHoliday = True Then
                                row("MonSch") = 0
                            Else

                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("MonSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("MonSch") = SchDuration
                                Else
                                    row("MonSch") = dtMeetDays.Rows(0)("Duration")
                                End If

                            End If

                            row("MonAct") = " "
                            row("MonTardy") = " "
                            row("MonExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Tuesday Then
                        row("TueDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            'DE6805 Make sure process date is the actual meet date
                            If ProcessDate = CDate(drActuals(0)("MeetDate")).ToShortDateString Then
                                row("TueSch") = drActuals(0)("Scheduled")
                                row("TueAct") = drActuals(0)("Actual")
                                row("TueTardy") = drActuals(0)("Tardy")
                                row("TueExcused") = drActuals(0)("Excused")
                            Else
                                row("TueAct") = " "
                                row("TueTardy") = " "
                                row("TueExcused") = " "
                            End If
                            'DE6805
                        Else
                            If isHoliday = True Then
                                row("TueSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("TueSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("TueSch") = SchDuration
                                Else
                                    row("TueSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("TueAct") = " "
                            row("TueTardy") = " "
                            row("TueExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Wednesday Then
                        row("WedDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            'DE6805 Make sure process date is the actual meet date
                            If ProcessDate = CDate(drActuals(0)("MeetDate")).ToShortDateString Then
                                row("WedSch") = drActuals(0)("Scheduled")
                                row("WedAct") = drActuals(0)("Actual")
                                row("WedTardy") = drActuals(0)("Tardy")
                                row("WedExcused") = drActuals(0)("Excused")
                            Else
                                row("WedAct") = " "
                                row("WedTardy") = " "
                                row("WedExcused") = " "
                            End If
                            'DE6805
                        Else
                            If isHoliday = True Then
                                row("WedSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("WedSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("WedSch") = SchDuration
                                Else
                                    row("WedSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("WedAct") = " "
                            row("WedTardy") = " "
                            row("WedExcused") = " "

                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Thursday Then
                        row("ThurDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            'DE6805 Make sure process date is the actual meet date
                            If ProcessDate = CDate(drActuals(0)("MeetDate")).ToShortDateString Then
                                row("ThurSch") = drActuals(0)("Scheduled")
                                row("ThurAct") = drActuals(0)("Actual")
                                row("ThurTardy") = drActuals(0)("Tardy")
                                row("ThurExcused") = drActuals(0)("Excused")
                            Else
                                row("ThurAct") = " "
                                row("ThurTardy") = " "
                                row("ThurExcused") = " "
                            End If
                            'DE6805
                        Else
                            If isHoliday = True Then
                                row("ThurSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("ThurSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("ThurSch") = SchDuration
                                Else
                                    row("ThurSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If
                            row("ThurAct") = " "
                            row("ThurTardy") = " "
                            row("ThurExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Friday Then
                        row("FriDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            'DE6805 Make sure process date is the actual meet date
                            If ProcessDate = CDate(drActuals(0)("MeetDate")).ToShortDateString Then
                                row("FriSch") = drActuals(0)("Scheduled")
                                row("FriAct") = drActuals(0)("Actual")
                                row("FriTardy") = drActuals(0)("Tardy")
                                row("FriExcused") = drActuals(0)("Excused")
                            Else
                                row("FriAct") = " "
                                row("FriTardy") = " "
                                row("FriExcused") = " "
                            End If
                            'DE6805
                        Else
                            If isHoliday = True Then
                                row("FriSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("FriSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("FriSch") = SchDuration
                                Else
                                    row("FriSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("FriAct") = " "
                            row("FriTardy") = " "
                            row("FriExcused") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Saturday Then
                        row("SatDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            'DE6805 Make sure process date is the actual meet date
                            If ProcessDate = CDate(drActuals(0)("MeetDate")).ToShortDateString Then
                                row("SatSch") = drActuals(0)("Scheduled")
                                row("SatAct") = drActuals(0)("Actual")
                                row("SatTardy") = drActuals(0)("Tardy")
                                row("SatExcused") = drActuals(0)("Excused")
                            Else
                                row("SatAct") = " "
                                row("SatTardy") = " "
                                row("SatExcused") = " "
                            End If
                            'DE6805
                        Else
                            If isHoliday = True Then
                                row("SatSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("SatSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("SatSch") = SchDuration
                                Else
                                    row("SatSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("SatAct") = " "
                            row("SatTardy") = " "
                        End If
                    ElseIf ProcessDate.DayOfWeek = DayOfWeek.Sunday Then
                        row("SunDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                        If drActuals.Length = 1 Then
                            'DE6805 Make sure process date is the actual meet date
                            If ProcessDate = CDate(drActuals(0)("MeetDate")).ToShortDateString Then
                                row("SunSch") = drActuals(0)("Scheduled")
                                row("SunAct") = drActuals(0)("Actual")
                                row("SunTardy") = drActuals(0)("Tardy")
                                row("SunExcused") = drActuals(0)("Excused")
                            Else
                                row("SunAct") = " "
                                row("SunTardy") = " "
                                row("SunExcused") = " "
                            End If
                            'DE6805
                        Else
                            If isHoliday = True Then
                                row("SunSch") = 0
                            Else
                                If drCurrentWorkDaySchedule.Length > 0 Then
                                    'row("SunSch") = drCurrentWorkDaySchedule(0)("Duration")
                                    row("SunSch") = SchDuration
                                Else
                                    row("SunSch") = dtMeetDays.Rows(0)("Duration")
                                End If
                            End If

                            row("SunAct") = " "
                            row("SunTardy") = " "
                            row("SunExcused") = " "
                        End If
                    End If

                Else

                    Select Case ProcessDate.DayOfWeek
                        Case DayOfWeek.Sunday
                            row("SunDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                            If row("SunTardy") Is DBNull.Value Then
                                row("SunTardy") = ""
                            End If
                            If row("SunExcused") Is DBNull.Value Then
                                row("SunExcused") = ""
                            End If

                        Case DayOfWeek.Monday
                            row("MonDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                            If row("MonTardy") Is DBNull.Value Then
                                row("MonTardy") = ""
                            End If
                            If row("MonExcused") Is DBNull.Value Then
                                row("MonExcused") = ""
                            End If
                        Case DayOfWeek.Tuesday
                            row("TueDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                            If row("TueTardy") Is DBNull.Value Then
                                row("TueTardy") = ""
                            End If
                            If row("TueExcused") Is DBNull.Value Then
                                row("TueExcused") = ""
                            End If
                        Case DayOfWeek.Wednesday
                            row("WedDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                            If row("WedTardy") Is DBNull.Value Then
                                row("WedTardy") = ""
                            End If
                            If row("WedExcused") Is DBNull.Value Then
                                row("WedExcused") = ""
                            End If
                        Case DayOfWeek.Thursday
                            row("ThurDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                            If row("ThurTardy") Is DBNull.Value Then
                                row("ThurTardy") = ""
                            End If
                            If row("ThurExcused") Is DBNull.Value Then
                                row("ThurExcused") = ""
                            End If
                        Case DayOfWeek.Friday
                            row("FriDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                            If row("FriTardy") Is DBNull.Value Then
                                row("FriTardy") = ""
                            End If
                            If row("FriExcused") Is DBNull.Value Then
                                row("FriExcused") = ""
                            End If
                        Case DayOfWeek.Saturday
                            row("SatDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
                            If row("SatTardy") Is DBNull.Value Then
                                row("SatTardy") = ""
                            End If
                            If row("SatExcused") Is DBNull.Value Then
                                row("SatExcused") = ""
                            End If
                    End Select


                End If



                If ProcessDate.DayOfWeek = DayOfWeek.Saturday Then
                    If row("MonSch") Is DBNull.Value Then
                        row("MonSch") = 0
                    End If
                    If row("TueSch") Is DBNull.Value Then
                        row("TueSch") = 0
                    End If

                    If row("WedSch") Is DBNull.Value Then
                        row("WedSch") = 0
                    End If
                    If row("ThurSch") Is DBNull.Value Then
                        row("ThurSch") = 0
                    End If

                    If row("FriSch") Is DBNull.Value Then
                        row("FriSch") = 0
                    End If
                    If row("SatSch") Is DBNull.Value Then
                        row("SatSch") = 0
                    End If
                    If row("SunSch") Is DBNull.Value Then
                        row("SunSch") = 0
                    End If
                    dtStudentDetails.Rows.Add(row)
                End If
                dtStudentDetails.AcceptChanges()
                ProcessDate = ProcessDate.AddDays(1)
                strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

            End While

        Next


        ViewState("dtStudentDetails") = dtStudentDetails
        ' ViewState("FromDate") = FromDate

        Return dtStudentDetails

        'dgAttendance.DataSource = dtStudentDetails.Select("SunDate=#" + FromDate + "#")
        'dgAttendance.DataBind()

        'If dtStudentDetails.Rows.Count > 0 Then
        '    lblsun.Text = Format(CDate(dtStudentDetails.Rows(0)("SunDate")), "MM/dd/yyyy")
        '    lblmon.Text = Format(CDate(dtStudentDetails.Rows(0)("MonDate")), "MM/dd/yyyy")
        '    lbltue.Text = Format(CDate(dtStudentDetails.Rows(0)("TueDate")), "MM/dd/yyyy")
        '    lblwed.Text = Format(CDate(dtStudentDetails.Rows(0)("WedDate")), "MM/dd/yyyy")
        '    lblthur.Text = Format(CDate(dtStudentDetails.Rows(0)("ThurDate")), "MM/dd/yyyy")
        '    lblfri.Text = Format(CDate(dtStudentDetails.Rows(0)("FriDate")), "MM/dd/yyyy")
        '    lblsat.Text = Format(CDate(dtStudentDetails.Rows(0)("SatDate")), "MM/dd/yyyy")
        'End If

    End Function
    Private Function ConvertActualAttendanceBasedonUnitType(ByVal dtAttendance As DataTable) As DataTable

        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then


            For Each item As DataRow In dtAttendance.Rows
                If IsNumeric(item("Actual")) AndAlso item("Actual") <> 9999 Then
                    item("Actual") = (CDec(item("Actual"))).ToString
                End If
                If item("Scheduled") <> "9999" Then
                    item("Scheduled") = (CDec(item("Scheduled"))).ToString
                End If

            Next
        ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then

            For Each item As DataRow In dtAttendance.Rows
                If IsNumeric(item("Actual")) AndAlso item("Actual") <> 9999 Then
                    item("Actual") = (CDec(item("Actual")) * 60).ToString
                End If
                If item("Scheduled") <> "9999" Then
                    item("Scheduled") = (CDec(item("Scheduled")) * 60).ToString
                End If

            Next

        Else ''If ViewState("AttendanceUnitType") = "Present Absent" Then
            For Each item As DataRow In dtAttendance.Rows
                If item("Actual").ToString.Trim.ToUpper = "P" Then
                    item("Actual") = "1"
                ElseIf item("Actual").ToString.Trim.ToUpper = "A" Then
                    item("Actual") = "0"
                ElseIf item("Actual").ToString.Trim = "" Then
                    item("Actual") = "9999"
                ElseIf item("Actual").ToString = "9999" Then
                    item("Actual") = "9999"
                ElseIf item("Actual").ToString.Trim.ToUpper = "T" Then
                    If item("Scheduled") = "0" Then
                        item("Actual") = "1"
                        item("Tardy") = False
                    Else
                        item("Actual") = "1"
                        item("Tardy") = True
                    End If

                ElseIf item("Actual").ToString.Trim.ToUpper = "E" Then
                    item("Actual") = "0"
                    item("Excused") = True
                    item("Tardy") = False
                End If

                If item("Scheduled") = "" Then
                    item("Scheduled") = "9999"
                ElseIf item("Scheduled") = 0 Then
                    item("Scheduled") = "0"
                ElseIf CDec(item("Scheduled")) > 0 Then
                    item("Scheduled") = "1"
                End If
            Next

        End If
        Return dtAttendance

    End Function
    Private Function CreateclsSectAttendanceTableForXML() As DataTable

        Dim dt As New DataTable("atClsSectAttendance")
        dt.Columns.Add(New DataColumn("StuEnrollId", Type.GetType("System.Guid")))
        dt.Columns.Add(New DataColumn("ClsSectionId", Type.GetType("System.Guid")))
        dt.Columns.Add(New DataColumn("ClsSectMeetingId", Type.GetType("System.Guid")))
        dt.Columns.Add(New DataColumn("MeetDate", Type.GetType("System.DateTime")))
        dt.Columns.Add(New DataColumn("Actual", Type.GetType("System.Decimal")))
        dt.Columns.Add(New DataColumn("Tardy", Type.GetType("System.Boolean")))
        dt.Columns.Add(New DataColumn("Excused", Type.GetType("System.Boolean")))
        dt.Columns.Add(New DataColumn("Scheduled", Type.GetType("System.Decimal")))
        Return dt

    End Function
    Private Function IsDayEditable(ByVal processDate As Date, ByVal StuEnrollId As String, ByVal SysStatusId As String, ByVal Datedetermined As String, ByVal StudentStartDate As Date) As String
        Dim dsNonEditableDaysforStudent As New DataSet
        dsNonEditableDaysforStudent = ViewState("dsNonEditableDaysforStudent")

        ''For LOA

        ''Dim LOAAttQuery = From Actualdt In dsNonEditableDaysforStudent.Tables(0).AsEnumerable() Where Actualdt("StuEnrollId") = StuEnrollId And Actualdt("StartDate") >= processDate And Actualdt("EndDate") <= processDate Select Actualdt


        'If LOAAttQuery.Count > 0 Then
        '    Return "LOA"
        'End If


        If StudentStartDate > processDate Then
            Return "Student has not started yet"
        End If

        If processDate < txtStart.SelectedDate Or processDate.ToShortDateString > txtEnd.SelectedDate Then
            Return "Not class meeting day"
        End If


        If SysStatusId = StateEnum.TransferOut Then


            Dim dtTransAttQuery As DataRow() = dsNonEditableDaysforStudent.Tables(3).Select("StuEnrollid='" & StuEnrollId & "' and LDA < '" & processDate.ToShortDateString & "'")

            If dtTransAttQuery.Length > 0 Then
                Return "Transferred"
            End If
        End If

        If SysStatusId = "12" Then
            If Not Datedetermined Is DBNull.Value Then
                If Datedetermined < processDate Then
                    Return "Dropped"
                End If
            Else
                Return "No datedetermined available for dropped student"
            End If
        End If

        Dim dtLOAAttQuery As DataRow() = dsNonEditableDaysforStudent.Tables(0).Select("StuEnrollid='" & StuEnrollId & "' and startdate<= '" & processDate.ToShortDateString & "' and enddate >='" & processDate.ToShortDateString & "'")

        If dtLOAAttQuery.Length > 0 Then
            Return "LOA"
        End If

        'Dim SuspAttQuery = From Actualdt In dsNonEditableDaysforStudent.Tables(1).AsEnumerable() Where Actualdt("StuEnrollId") = StuEnrollId And Actualdt("StartDate") >= processDate And Actualdt("EndDate") <= processDate Select Actualdt

        'If SuspAttQuery.Count > 0 Then
        '    Return "Suspended"
        'End If


        Dim dtSuspAttQuery As DataRow() = dsNonEditableDaysforStudent.Tables(1).Select("StuEnrollid='" & StuEnrollId & "' and startdate<= '" & processDate.ToShortDateString & "' and enddate >='" & processDate.ToShortDateString & "'")

        If dtSuspAttQuery.Length > 0 Then
            Return "Suspended"
        End If

        Dim dtDropCourseAttQuery As DataRow() = dsNonEditableDaysforStudent.Tables(4).Select("StuEnrollid='" & StuEnrollId & "' and startdate<= '" & processDate.ToShortDateString & "' and enddate >='" & processDate.ToShortDateString & "'")

        If dtDropCourseAttQuery.Length > 0 Then
            Return "CourseDropped"
        End If

        ' If SingletonAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
        'Dim HolidayAttQuery = From Actualdt In dsNonEditableDaysforStudent.Tables(2).AsEnumerable() Where Actualdt("HolidayStartDate") >= processDate And Actualdt("HolidayEndDate") <= processDate And Actualdt("StartDescrip") >= processDate.TimeOfDay And Actualdt("EndDescrip") <= processDate.TimeOfDay Select Actualdt
        ' Dim HolidayAttQuery = From Actualdt In dsNonEditableDaysforStudent.Tables(2).AsEnumerable() Where CDate(Actualdt("HolidayStartDate")).Date >= processDate.Date And CDate(Actualdt("HolidayEndDate")).Date <= processDate.Date Select Actualdt

        'If HolidayAttQuery.Count > 0 Then
        '    Return "Holiday"
        'End If
        ''  End If

        ''DE7436
        ''QA: If the holidays is setup for more than one day and PostAttendanceOnHoliday is set to "No" then the cells are not disabled on the attendance pages.
        Dim HolidayAttQuery = From Actualdt In dsNonEditableDaysforStudent.Tables(2).AsEnumerable() Where CDate(Actualdt("HolidayStartDate")).Date <= processDate.Date And CDate(Actualdt("HolidayEndDate")).Date >= processDate.Date Select Actualdt


        Dim dtstudentsgetmeetingTime As DataTable
        dtstudentsgetmeetingTime = Session("Students")
        Dim StartTime As String
        Dim EndTime As String
        StartTime = dtstudentsgetmeetingTime.Rows(0)("StartTime")
        EndTime = dtstudentsgetmeetingTime.Rows(0)("EndTime")

        'Dim ClsSectMeetingQuery = (From PunchesTable In dt.AsEnumerable() Where PunchesTable.Field(Of Guid)("StuEnrollId") = New Guid(StuEnrollId) Select ClsSectMeetId = PunchesTable("ClsSectMeetingID"), StartDate = PunchesTable("StartDate"), GradDate = PunchesTable("ExpGradDate")).Distinct

        For Each result As DataRow In HolidayAttQuery
            If result("AllDay") = False And Not result("StartTime") Is DBNull.Value And Not result("EndTime") Is DBNull.Value Then
                If (StartTime >= Format(result("StartTime"), "HH:mm:ss") And StartTime <= Format(result("EndTime"), "HH:mm:ss")) Or (EndTime >= Format(result("StartTime"), "HH:mm:ss") And EndTime <= Format(result("EndTime"), "HH:mm:ss")) Then
                    Return "Holiday"
                ElseIf (StartTime <= Format(result("StartTime"), "HH:mm:ss") And EndTime >= Format(result("EndTime"), "HH:mm:ss")) Then
                    Return "Holiday"
                ElseIf (StartTime >= Format(result("StartTime"), "HH:mm:ss") And EndTime <= Format(result("EndTime"), "HH:mm:ss")) Then
                    Return "Holiday"
                End If
            Else
                Return "Holiday"
            End If
        Next

        Return ""
    End Function
#End Region

#Region "dgAttendance - item command"

    Protected Sub dgAttendance_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles dgAttendance.ItemCommand


        ' DE8711: NZ ---------------- reworked

        If Not IsNothing(e.Item) AndAlso e.CommandName.ToString.ToLower <> "" Then

            If e.Item.ItemType = GridItemType.CommandItem Then
                HideRadWindow()
                ShowGrid()
                Select Case e.CommandName.ToString.ToLower
                    Case "savechanges"
                        If Session("ClsSectionUsesTimeClock") = True Then Exit Sub
                        SaveAttendanceInfo(False, False)
                    Case "postzero"
                        SaveAttendanceInfo(True, False)
                    Case "postexception"
                        SaveAttendanceInfo(False, True)
                End Select
                RefreshGrid()
            End If

            If e.Item.ItemType = GridItemType.Item OrElse e.Item.ItemType = GridItemType.AlternatingItem Then

                If e.CommandName.StartsWith("OpenEditForm") = True Then
                    Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
                    Dim itemStuEnrollIDValue As String = itemStuEnrollID.GetDataKeyValue("StuEnrollID").ToString()
                    Dim stuName As String = DirectCast(e.Item.FindControl("lblStudentName"), Label).Text
                    Dim meetDate As String = ""
                    Dim actValue As String = ""
                    Dim sch As String = ""
                    Dim clsSectMeeting As String = ddlClsSectmeeting.SelectedItem.Text
                    Dim tardy As Boolean = False
                    Dim excused As Boolean = False
                    Select Case e.CommandName
                        Case "OpenEditFormSunday"
                            meetDate = DirectCast(e.Item.FindControl("lblSunDate"), Label).Text
                            actValue = DirectCast(e.Item.FindControl("txtSunAct"), TextBox).Text
                            sch = DirectCast(e.Item.FindControl("lblSunSch"), Label).Text
                            tardy = DirectCast(e.Item.FindControl("chkSunTardy"), CheckBox).Checked
                            excused = DirectCast(e.Item.FindControl("chkSunExcused"), CheckBox).Checked
                        Case "OpenEditFormMonday"
                            meetDate = DirectCast(e.Item.FindControl("lblMonDate"), Label).Text
                            actValue = DirectCast(e.Item.FindControl("txtMonAct"), TextBox).Text
                            sch = DirectCast(e.Item.FindControl("lblMonSch"), Label).Text
                            tardy = DirectCast(e.Item.FindControl("chkMonTardy"), CheckBox).Checked
                            excused = DirectCast(e.Item.FindControl("chkMonExcused"), CheckBox).Checked
                        Case "OpenEditFormTuesday"
                            meetDate = DirectCast(e.Item.FindControl("lblTueDate"), Label).Text
                            actValue = DirectCast(e.Item.FindControl("txtTueAct"), TextBox).Text
                            sch = DirectCast(e.Item.FindControl("lblTueSch"), Label).Text
                            tardy = DirectCast(e.Item.FindControl("chkTueTardy"), CheckBox).Checked
                            excused = DirectCast(e.Item.FindControl("chkTueExcused"), CheckBox).Checked
                        Case "OpenEditFormWednesday"
                            meetDate = DirectCast(e.Item.FindControl("lblWedDate"), Label).Text
                            actValue = DirectCast(e.Item.FindControl("txtWedAct"), TextBox).Text
                            sch = DirectCast(e.Item.FindControl("lblWedSch"), Label).Text
                            tardy = DirectCast(e.Item.FindControl("chkWedTardy"), CheckBox).Checked
                            excused = DirectCast(e.Item.FindControl("chkWedExcused"), CheckBox).Checked
                        Case "OpenEditFormThursday"
                            meetDate = DirectCast(e.Item.FindControl("lblThurDate"), Label).Text
                            actValue = DirectCast(e.Item.FindControl("txtThurAct"), TextBox).Text
                            sch = DirectCast(e.Item.FindControl("lblThurSch"), Label).Text
                            tardy = DirectCast(e.Item.FindControl("chkThurTardy"), CheckBox).Checked
                            excused = DirectCast(e.Item.FindControl("chkThurExcused"), CheckBox).Checked
                        Case "OpenEditFormFriday"
                            meetDate = DirectCast(e.Item.FindControl("lblFriDate"), Label).Text
                            actValue = DirectCast(e.Item.FindControl("txtFriAct"), TextBox).Text
                            sch = DirectCast(e.Item.FindControl("lblFriSch"), Label).Text
                            tardy = DirectCast(e.Item.FindControl("chkFriTardy"), CheckBox).Checked
                            excused = DirectCast(e.Item.FindControl("chkFriExcused"), CheckBox).Checked
                        Case "OpenEditFormSaturday"
                            meetDate = DirectCast(e.Item.FindControl("lblSatDate"), Label).Text
                            actValue = DirectCast(e.Item.FindControl("txtSatAct"), TextBox).Text
                            sch = DirectCast(e.Item.FindControl("lblSatSch"), Label).Text
                            tardy = DirectCast(e.Item.FindControl("chkSatTardy"), CheckBox).Checked
                            excused = DirectCast(e.Item.FindControl("chkSatExcused"), CheckBox).Checked
                    End Select
                    lblClsSmeeting1.Text = clsSectMeeting
                    txtScheduled1.Text = sch
                    txtActual1.Text = actValue
                    lblMeetDate1.Text = meetDate
                    lblStudentName1.Text = stuName
                    chkTardy1.Checked = tardy
                    chkExcused.Checked = excused
                    AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, clsSectMeeting, sch, actValue, meetDate, stuName, tardy, excused)
                    UserListDialog.VisibleOnPageLoad = True
                ElseIf e.CommandName = "CancelChanges" Then
                    HideRadWindow()
                    ShowGrid()
                    BindCurrentWeekLabels()
                    dgAttendance.Rebind()
                End If
            End If


        End If
    End Sub
    Private Sub GetStudentCompleteDetails(ByVal StuEnrollid As String)
        Dim facade As New TimeClockFacade
        dsStudent = facade.GetStudentCompleteDetails_byClass(StuEnrollid)
        Session("StudentDetails") = dsStudent

    End Sub
    Private Sub GetdatasetfromDB(ByVal StuEnrollid As String, ByVal schdate As Date, ByVal ClsSectMeetingID As String)

        Dim facade As New TimeClockFacade
        dsPunch = facade.GetPunchesforClsSectMeetingForaStudentonaGivenDate_ByClass(StuEnrollid, schdate, ClsSectMeetingID)
        Dim dtpunch As New DataTable
        Session("dsPunch") = dsPunch
        dtpunch = dsPunch.Tables("StudentTimeClockPunches")
        Session("PunchTable") = dtpunch


    End Sub
    Public Function SaveAttendanceInfo(ByVal PostZero As Boolean, ByVal PostException As Boolean) As String
        Dim fac As New TimeClockFacade
        Dim aday As Date = txtStart.SelectedDate
        Dim firstDate As Date = aday.AddDays(-CInt(aday.DayOfWeek))
        Dim fromDate As Date = ViewState("FromDate")
        Dim selectWk As Integer = Math.Floor(DateDiff("d", firstDate, fromDate) / 7) Mod 2
        Dim startTime As String = fac.GetStartTimeForClassSectionMeeting(ddlClsSectmeeting.SelectedItem.Value, campusId, selectWk)
        Dim dtStudentDetails As DataTable = ViewState("dtStudentDetails")
        Dim dtUpdatedAttendanceRows As DataTable = CreateclsSectAttendanceTable()
        Dim updatedRows() As DataRow
        Dim invalidFieldsCount As Integer = 0
        Dim studentEnrollmentsList As List(Of Guid) = New List(Of Guid)()
        For Each item As GridItem In dgAttendance.MasterTableView.Items
            If TypeOf item Is GridDataItem Then
                Dim lblStudentName As Label = DirectCast(item.FindControl("lblStudentName"), Label)
                Dim str As String = lblStudentName.Text
                Dim itemStuEnrollID As GridDataItem = CType(item, GridDataItem)

                Dim itemStuEnrollIDValue As String = itemStuEnrollID.GetDataKeyValue("StuEnrollID").ToString()

                If (itemStuEnrollIDValue IsNot Nothing AndAlso itemStuEnrollIDValue IsNot String.Empty) Then
                    Dim gitemStuEnrollIDValue = New Guid(itemStuEnrollIDValue)

                    If (Not studentEnrollmentsList.Contains(gitemStuEnrollIDValue)) Then
                        studentEnrollmentsList.Add(gitemStuEnrollIDValue)
                    End If
                End If

                updatedRows = dtStudentDetails.Select("StuEnrollId='" + itemStuEnrollIDValue + "' And SunDate Like'" + Format(CDate(fromDate), "MM/dd/yyyy") + "%'")
                For Each updateddr As DataRow In updatedRows

                    Dim clId As String = ddlClasses.SelectedItem.Value

                    Dim enrollments As List(Of String) = RegFacade.GetActiveEnrollmentWithClassAssociated(itemStuEnrollIDValue, clId)

                    For Each stuEnroll As String In enrollments
                        itemStuEnrollIDValue = stuEnroll

                        Dim weekDays As String() = {"Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"}
                        For Each strWeekDay As String In weekDays
                            Dim row As DataRow
                            row = dtUpdatedAttendanceRows.NewRow()
                            row("StuEnrollId") = itemStuEnrollIDValue
                            row("ClsSectionID") = ddlClasses.SelectedItem.Value
                            row("ClsSectMeetingID") = ddlClsSectmeeting.SelectedItem.Value

                            Dim sysStatusValue As String = updateddr("SysStatusId")
                            Dim stuEnrollId As String = itemStuEnrollIDValue
                            Dim dateDetermined As String = updateddr("DateDetermined")
                            Dim studentStartDate As String = updateddr("StudentStartDate")
                            Dim dayStatus As String = ""

                            Dim chk As CheckBox = TryCast(item.FindControl("chk" & strWeekDay & "Tardy"), CheckBox)
                            Dim chk2 As CheckBox = TryCast(item.FindControl("chk" & strWeekDay & "Excused"), CheckBox)
                            ' Dim lblDate As Label = TryCast(item.FindControl("lbl" & strWeekDay & "Date"), Label)
                            Dim lblSch As String = TryCast(item.FindControl("lbl" & strWeekDay & "Sch"), Label).Text
                            Dim txtBx As TextBox = TryCast(item.FindControl("txt" & strWeekDay & "Act"), TextBox)



                            dayStatus = IsDayEditable(CDate(updateddr(strWeekDay & "Date")), stuEnrollId, sysStatusValue, dateDetermined, studentStartDate)
                            row("MeetDate") = DateTime.Parse(Format(CDate(updateddr(strWeekDay & "Date")), "MM/dd/yyyy") + " " + startTime)
                            row("Scheduled") = updateddr(strWeekDay & "Sch").ToString


                            If IsDBNull(row("Scheduled")) Then
                                row("Scheduled") = False
                            End If
                            txtBx.Style.Remove("border")

                            Select Case CBool(row("Scheduled"))
                                Case True 'scheduled date
                                    row("Excused") = False
                                    row("Tardy") = False
                                    'if value entered
                                    If txtBx.Text = "" Then
                                        'set actual values
                                        If PostZero And (updateddr(strWeekDay & "Sch") <> "0" And updateddr(strWeekDay & "Sch") <> "9999") And dayStatus = "" And CDate(row("MeetDate")) <= Date.Now Then
                                            row("Actual") = "0"
                                        ElseIf PostException And (updateddr(strWeekDay & "Sch") <> "0" And updateddr(strWeekDay & "Sch") <> "9999") And dayStatus = "" And CDate(row("MeetDate")) <= Date.Now Then
                                            If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                                If IsNumeric(updateddr(strWeekDay & "Sch")) Then
                                                    row("Actual") = updateddr(strWeekDay & "Sch")
                                                Else
                                                    invalidFieldsCount = invalidFieldsCount + 1
                                                    txtBx.Style.Add("border", "1px solid red")
                                                End If

                                            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                                If IsNumeric(updateddr(strWeekDay & "Sch")) Then
                                                    row("Actual") = CDec(updateddr(strWeekDay & "Sch")).ToString
                                                Else
                                                    invalidFieldsCount = invalidFieldsCount + 1
                                                    txtBx.Style.Add("border", "1px solid red")
                                                End If
                                            Else
                                                row("Actual") = "P"
                                            End If
                                        Else
                                            row("Actual") = "9999"
                                        End If
                                    Else
                                        If ViewState("AttendanceUnitType").ToString.ToUpper = "PRESENT ABSENT" Then
                                            If IsValidValue(txtBx.Text.ToUpper, {"T", "P", "A", "E"}, False) = True Then
                                                If txtBx.Text.ToUpper = "E" Then
                                                    row("Excused") = True
                                                End If
                                                row("Actual") = txtBx.Text.ToUpper
                                                row("Tardy") = chk.Checked
                                            Else
                                                txtBx.Style.Add("border", "1px solid red")
                                                invalidFieldsCount = invalidFieldsCount + 1
                                            End If
                                        Else
                                            'de8912 
                                            If IsNumeric(txtBx.Text) Then

                                                Dim actHours As Double = Double.Parse(txtBx.Text)
                                                Dim schHours As Double = Double.Parse(lblSch)

                                                row("Tardy") = False
                                                row("Excused") = False

                                                If actHours > 0 Then
                                                    row("Tardy") = chk.Checked
                                                Else
                                                    row("Tardy") = False
                                                End If

                                                If actHours >= 0 And actHours < schHours Then
                                                    row("Excused") = chk2.Checked
                                                Else
                                                    row("Excused") = False
                                                End If

                                                row("Actual") = txtBx.Text.ToUpper

                                            Else
                                                invalidFieldsCount = invalidFieldsCount + 1
                                                txtBx.Style.Add("border", "1px solid red")
                                            End If
                                        End If

                                    End If

                                    updateddr(strWeekDay & "Tardy") = chk.Checked
                                    updateddr(strWeekDay & "Act") = row("Actual")

                                Case False 'make up / unscheduled date - only allow presence!

                                    row("Actual") = "9999"
                                    row("Excused") = False
                                    row("Tardy") = False

                                    If txtBx.Text <> "" Then

                                        If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                                            If IsNumeric(txtBx.Text) Then
                                                row("Actual") = txtBx.Text.ToString
                                            Else
                                                invalidFieldsCount = invalidFieldsCount + 1
                                                txtBx.Style.Add("border", "1px solid red")
                                            End If
                                        ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                                            If IsNumeric(txtBx.Text) Then
                                                row("Actual") = CDec(txtBx.Text).ToString
                                            Else
                                                invalidFieldsCount = invalidFieldsCount + 1
                                                txtBx.Style.Add("border", "1px solid red")
                                            End If
                                        Else
                                            If IsValidValue(txtBx.Text.ToUpper, {"T", "P", "A", "E"}, False) = True Then
                                                If txtBx.Text.ToUpper = "P" Then
                                                    row("Actual") = txtBx.Text.ToUpper
                                                Else
                                                    invalidFieldsCount = invalidFieldsCount + 1
                                                    txtBx.Style.Add("border", "1px solid red")
                                                End If
                                            Else
                                                invalidFieldsCount = invalidFieldsCount + 1
                                                txtBx.Style.Add("border", "1px solid red")
                                            End If
                                        End If

                                    End If

                                    updateddr(strWeekDay & "Tardy") = False
                                    updateddr(strWeekDay & "Act") = row("Actual")

                            End Select

                            dtUpdatedAttendanceRows.Rows.Add(row)
                            dtUpdatedAttendanceRows.AcceptChanges()
                        Next
                    Next

                Next
            End If
        Next


        Dim res As String = ""
        If dtUpdatedAttendanceRows.Rows.Count > 0 AndAlso invalidFieldsCount = 0 Then
            dtUpdatedAttendanceRows = ConvertActualAttendanceBasedonUnitType(dtUpdatedAttendanceRows)
            Dim dtnewDatatableforSaving As New DataTable
            dtnewDatatableforSaving = dtUpdatedAttendanceRows.Clone
            For Each item As DataRow In dtUpdatedAttendanceRows.Rows
                dtnewDatatableforSaving.ImportRow(item)
            Next
            Dim dsFinalDataset As New DataSet
            dsFinalDataset.Tables.Add(dtnewDatatableforSaving)
            res = AddClsSectAttendance(dsFinalDataset)
        End If
        If invalidFieldsCount = 0 Then
            lblOutput.Text = "Attendance was saved successfully"
            lblOutputBottom.Text = "Attendance was saved successfully"

            If studentEnrollmentsList.Count > 0 Then
                TitleIVSapUpdateAsync(studentEnrollmentsList)
                PostPaymentPeriodAttendanceAFA(studentEnrollmentsList)
            End If
        Else
            lblOutput.Text = "Changes not saved for the " & invalidFieldsCount & " invalid boxes"
            lblOutputBottom.Text = "Changes not saved for the " & invalidFieldsCount & " invalid boxes"
        End If

        Return res
    End Function
    Private Sub TitleIVSapUpdate(StudentEnrollmentsList As List(Of Guid))
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim TitleIVSAPRequest As New TitleIVSAPRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Dim pass As Boolean? = TitleIVSAPRequest.TitleIVSapCheck(StudentEnrollmentsList)
        End If
    End Sub
    Private Async Function TitleIVSapUpdateAsync(StudentEnrollmentsList As List(Of Guid)) As Task(Of Boolean)
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim TitleIVSAPRequest As New TitleIVSAPRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Return Await TitleIVSAPRequest.TitleIVSapCheckAsync(StudentEnrollmentsList)
        End If
        Return False
    End Function
    Private Async Function PostPaymentPeriodAttendanceAFA(StudentEnrollments As List(Of Guid)) As Task(Of Boolean)
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString

            'Send updated to AFA if integration is enabled(handled internally)
            Return Await New PaymentPeriodHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, AdvantageSession.UserState.UserName) _
                    .PostPaymentPeriodAttendance(campusId, StudentEnrollments)
        End If
        Return False
    End Function
    Private Function AddClsSectAttendance(ByVal dsFinalDataset As DataSet) As String 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document

        Dim rtn As String = String.Empty

        If Not dsFinalDataset Is Nothing Then
            For Each lcol As DataColumn In dsFinalDataset.Tables(0).Columns
                lcol.ColumnMapping = MappingType.Attribute
            Next
            Dim strXML As String = dsFinalDataset.GetXml
            rtn = (New ClsSectAttendanceFacade).PostClsSectAttendance(strXML, AdvantageSession.UserName)

        End If
        Return ""
    End Function
    Private Function CreateclsSectAttendanceTable() As DataTable

        Dim dt As New DataTable("atClsSectAttendance")
        dt.Columns.Add(New DataColumn("StuEnrollId", Type.GetType("System.Guid")))
        dt.Columns.Add(New DataColumn("ClsSectionId", Type.GetType("System.Guid")))
        dt.Columns.Add(New DataColumn("ClsSectMeetingId", Type.GetType("System.Guid")))
        dt.Columns.Add(New DataColumn("MeetDate", Type.GetType("System.DateTime")))
        dt.Columns.Add(New DataColumn("Actual", Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("Tardy", Type.GetType("System.Boolean")))
        dt.Columns.Add(New DataColumn("Excused", Type.GetType("System.Boolean")))
        dt.Columns.Add(New DataColumn("Scheduled", Type.GetType("System.String")))
        Return dt

    End Function
    Public Sub AssignvaluestoRadUselistWindow(ByVal StuEnrollid As String, ByVal ClsSectMeeting As String, ByVal Scheduled As String, ByVal Actuals As String, ByVal MeetDate As Date, ByVal StudentName As String, ByVal tardy As Boolean, ByVal excused As Boolean)
        ViewState("StuEnrollid") = StuEnrollid
        ViewState("MeetDate") = MeetDate
        lblClsSmeeting1.Text = "<b>Class Section Meeting: </b>" & ClsSectMeeting
        If InStr(Scheduled, ".") Then
            txtScheduled1.Text = Mid(Scheduled, 1, InStr(Scheduled, ".") + 2)
        Else
            txtScheduled1.Text = Scheduled
        End If

        txtActual1.Text = Actuals
        lblMeetDate1.Text = "<b>Meeting Day: </b>" & Format(CDate(MeetDate), "MM-dd-yyyy") + ", " + CDate(MeetDate).DayOfWeek.ToString
        lblStudentName1.Text = StudentName
        chkTardy1.Checked = tardy

        chkExcused.Checked = excused
        chkExcused.Attributes.Add("OnClick", "return true;")

        lblerrmsg.Text = ""

        If Session("ClsSectionUsesTimeClock") = False Then
            txtActual1.Enabled = True
            txtAbsent1.Enabled = False
            If txtActual1.Text.Trim = "" Then
                chkTardy1.Checked = tardy
            End If
            chkTardy1.Attributes.Add("OnClick", "return true;")
        Else
            If txtActual1.Text.Trim = "" Then
                chkTardy1.Checked = tardy
            End If
            chkTardy1.Attributes.Add("OnClick", "return true;")
            chkTardy1.Enabled = True
        End If

        Dim Absent As String = 0
        Dim MakeUP As String = 0

        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then

            If Actuals <> "" And Scheduled <> "" Then
                If CDec(Scheduled) > CDec(Actuals) Then
                    Absent = CDec(Scheduled) - CDec(Actuals)
                ElseIf CDec(Actuals) > CDec(Scheduled) Then
                    MakeUP = CDec(Actuals) - CDec(Scheduled)
                End If
            End If

        ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "PRESENT ABSENT" Then

            If Actuals.ToString.Trim = "P" Or Actuals.ToString.Trim = "p" Then
                txtActual1.Text = "Y"
                txtAbsent1.Text = "N"
            ElseIf Actuals.ToString.Trim = "A" Or Actuals.ToString.Trim = "a" Then
                txtActual1.Text = "N"
                txtAbsent1.Text = "Y"
            ElseIf Actuals.ToString.Trim = " " Then
                txtActual1.Text = ""
                txtAbsent1.Text = ""
            ElseIf Actuals.ToString.Trim = "T" Or Actuals.ToString.Trim = "t" Then
                txtActual1.Text = "Y"
                txtAbsent1.Text = "N"
                chkTardy1.Checked = True
            ElseIf Actuals.ToString.Trim = "E" Or Actuals.ToString.Trim = "e" Then
                txtActual1.Text = "N"
                txtAbsent1.Text = "Y"
            End If

            If (Scheduled > 0) And (Scheduled <> 999 Or Scheduled <> 9999) Then
                txtScheduled1.Text = "Y"
            Else
                txtScheduled1.Text = "N"
            End If

            txtAbsent1.Visible = False
            txtMakeUp1.Visible = False
            lblAbsent.Visible = False
            lblMakeUp.Visible = False

        End If
        txtAbsent1.Text = Absent
        txtMakeUp1.Text = MakeUP
        GetStudentCompleteDetails(StuEnrollid)
        GetdatasetfromDB(StuEnrollid, Format(CDate(MeetDate), "MM-dd-yyyy"), ddlClsSectmeeting.SelectedItem.Value)
        BindDataGrid()
        txtScheduled1.Enabled = True

        If Session("ClsSectionUsesTimeClock") = True Then
            EnableorDisableControlsforTimeClock(True)
        Else
            EnableorDisableControlsforTimeClock(False)
            txtScheduled1.Enabled = False
            txtActual1.Enabled = False
            chkTardy1.Enabled = False
            'If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
            '    txtScheduled1.Enabled = False
            '    txtActual1.Enabled = False
            '    chkTardy1.Enabled = False

            'End If
        End If

    End Sub
#End Region

#Region "dgAttendance - SHOW / HIDE front end binding functions"
    Public Function ShowHidePostZeroButton() As Boolean
        If Session("ClsSectionUsesTimeClock") = True Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ShowHidePostExceptionButton() As Boolean
        If Session("ClsSectionUsesTimeClock") = True Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function ShowHideButtonsWhenCourseUsesTimeClock() As Boolean
        If Session("ClsSectionUsesTimeClock") = True Then
            Return False
        Else
            Return True
        End If
    End Function
    Protected Function GetTardyinfo(ByVal Tardy As String) As Boolean
        If Tardy = "" Then
            Return False
        ElseIf Tardy = "True" Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Function GetExcusedInfo(ByVal Tardy As String) As Boolean
        If Tardy = "" Then
            Return False
        ElseIf Tardy = "True" Then
            Return True
        Else
            Return False
        End If
    End Function




#End Region

#Region "dgAttendance - BUTTONS clicks"
    Protected Sub btnPreviousWeek_Click(sender As Object, e As EventArgs) Handles btnPreviousWeek.Click
        Dim dtStudentDetails As DataTable
        dtStudentDetails = ViewState("dtStudentDetails")
        Dim FromDate As Date
        FromDate = ViewState("FromDate")
        FromDate = FromDate.AddDays(-7)
        ViewState("FromDate") = FromDate
        'dgAttendance.DataSource = dtStudentDetails.Select("SunDate LIke '" + Format(CDate(FromDate), "MM/dd/yyyy") + "%'")
        'dgAttendance.DataBind()
        'BindCurrentWeekLabels()
        RefreshGrid(True)
        UserListDialog.VisibleOnPageLoad = False
        lblOutput.Text = ""
        lblOutputBottom.Text = ""
    End Sub
    Protected Sub btnNextWeek_Click(sender As Object, e As EventArgs) Handles btnNextWeek.Click
        Dim dtStudentDetails As DataTable
        dtStudentDetails = ViewState("dtStudentDetails")
        Dim FromDate As Date
        FromDate = ViewState("FromDate")
        FromDate = FromDate.AddDays(7)
        ViewState("FromDate") = FromDate
        'dgAttendance.DataSource = dtStudentDetails.Select("SunDate LIke '" + Format(CDate(FromDate), "MM/dd/yyyy") + "%'")
        'dgAttendance.DataBind()
        'BindCurrentWeekLabels()
        RefreshGrid(True)
        UserListDialog.VisibleOnPageLoad = False
        lblOutput.Text = ""
        lblOutputBottom.Text = ""
    End Sub
    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        RefreshGrid()
    End Sub
    Private Sub RefreshGrid(Optional needBind As Boolean = True)

        Dim facAttendance As New ClsSectAttendanceFacade
        Dim dtActualAttendace As DataTable
        Dim dtMeetDays As DataTable
        ToDate = GetEndDateoftheWeek(CType(txtEnd.SelectedDate, Date))
        campusId = Master.CurrentCampusId

        If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId).ToString = "Traditional" Then
            dtMeetDays = facAttendance.GetClsSectionMeetingDaysgivenClsSectionandMeeting(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value, False)
        Else
            dtMeetDays = facAttendance.GetClsSectionMeetingDaysgivenClsSectionandMeeting(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value, True)
        End If
        Session("MeetDays") = dtMeetDays
        dtActualAttendace = facAttendance.GetStudentsAttendanceForClsSectionGivenClsSectionandMeeting(ddlClasses.SelectedItem.Value,
                                                                                                     ddlClsSectmeeting.SelectedItem.Value,
                                                                                                      ViewState("FromDate"), DateAdd("d", 1, ToDate), campusId.ToString)
        Session("ActualAttendance") = dtActualAttendace
        Dim dtStudentDetails As DataTable = PopulateStudentAttendanceTableforGrid()
        ViewState("dtStudentDetails") = dtStudentDetails
        dgAttendance.DataSource = dtStudentDetails.Select("SunDate LIke '" + Format(CDate(ViewState("FromDate")), "MM/dd/yyyy") + "%'")
        If needBind Then dgAttendance.DataBind()
        BindCurrentWeekLabels()

    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        lblOutput.Text = ""
        lblOutputBottom.Text = ""
        'We want to loop through the table rows collection.
        'For each student we will examine each date. We will check the ActualAttendance dt in session
        'to see if the date exists there for the student.
        'If the date has an actual amount then if it exists in the dt with a different value we will
        'need to do an update. If it exists in the dt with the same value then we will not do anything.
        'If the date has an actual amount but does not exists in the dt then we will need to do an insert.
        'If the date does not have an actual amount but exists in the dt then we will need to delete that
        'entry from the database. If the date does not have an actual amount and does not exists in the dt
        'then we will not need to do any thing.
        Dim rowCounter As Integer
        Dim cellCounter As Integer
        Dim arrRows() As DataRow
        Dim stuId As String
        Dim attDate As DateTime
        Dim actualAtt As String
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim translatedActual As Decimal
        Dim blnExcused As Boolean
        Dim blnTardy As Boolean
        Dim blnFutureStart As Boolean
        Dim tbl As Table
        Dim errMsg As String
        'initialize list for keeping track of attendance updates to notify AFA
        Dim enrollmentIds As List(Of Guid) = New List(Of Guid)

        Dim regFacade = New RegFacade()

        'We start at the second row since the first row is the header
        tbl = DirectCast(pnlAttendance.Controls(0), Table)
        For rowCounter = 1 To tbl.Rows.Count - 1
            stuId = DirectCast(Session("Students"), DataTable).Rows(rowCounter - 1)("StuEnrollId").ToString()

            Dim clId As String = ddlClasses.SelectedItem.Value

            Dim enrollments As List(Of String) = RegFacade.GetActiveEnrollmentWithClassAssociated(stuId, clId)

            For Each stuEnroll As String In enrollments
                stuId = stuEnroll
            Next

            'Check If the student is future start
            blnFutureStart = DirectCast(Session("Students"), DataTable).Rows(rowCounter - 1)("IsFutureStart")

            'Start at the third cell because the first stores the names of students and the second stores the status
            For cellCounter = 3 To DirectCast(Session("Dates"), ArrayList).Count + 2
                attDate = DirectCast(Session("Dates"), ArrayList).Item(cellCounter - 3)

                If DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text <> "" Then
                    'If student is future start, then status needs to be changed to Currently Attending
                    If blnFutureStart = True Then
                        errMsg = facAttendance.ChangeStatusToCurrAttending(stuId)
                    End If
                    Dim isTardy As Boolean
                    actualAtt = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text.ToUpper

                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                        isTardy = DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(1), CheckBox).Checked
                    End If
                    'Check if the row exists in the ActualAttendance dt
                    arrRows = DirectCast(Session("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate & "#")
                    If arrRows.Length > 0 Then
                        'If the actual in the table does not match the actual in the dt then
                        'we will need to do an update if the actual in the table is not an empty string.
                        'If the actual in the table does not match the actual in the dt then
                        'we will need to do a delete if the actual in the table is an empty string.
                        Dim dtActual As String

                        Dim isExcused As Boolean

                        dtActual = arrRows(0)("Actual").ToString()

                        'isTardy = arrRows(0)("Tardy")
                        isExcused = arrRows(0)("Excused")

                        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                            If arrRows(0)("Actual").ToString() <> actualAtt Then
                                'Do an update
                                facAttendance.UpdateStudentClsSectionAttendanceRecord(stuId, ddlClasses.SelectedItem.Value, attDate, actualAtt, isTardy)
                                enrollmentIds.Add(Guid.Parse(stuId))

                                'Response.Write("Row:" & rowCounter & " Cell:" & cellCounter & "Old:" & arrRows(0)("Actual").ToString() & " New:" & actualAtt & "<br>")
                            End If
                        ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                            If arrRows(0)("Actual").ToString() <> actualAtt Then
                                'Do an update
                                facAttendance.UpdateStudentClsSectionAttendanceRecord(stuId, ddlClasses.SelectedItem.Value, attDate, actualAtt * 60, isTardy)
                                enrollmentIds.Add(Guid.Parse(stuId))

                                'Response.Write("Row:" & rowCounter & " Cell:" & cellCounter & "Old:" & arrRows(0)("Actual").ToString() & " New:" & actualAtt & "<br>")
                            End If
                        Else
                            isTardy = arrRows(0)("Tardy")
                            translatedActual = facAttendance.TranslatePALetterToQuantity(actualAtt)
                            If (dtActual <> translatedActual) Or
                            (dtActual = "1" And translatedActual.ToString = "1" And actualAtt = "T" And isTardy = False) Or
                            (dtActual = "1" And translatedActual.ToString = "1" And actualAtt = "P" And isTardy = True) Or
                            (dtActual = "0" And translatedActual.ToString = "0" And actualAtt = "E" And isExcused = False) Or
                            (dtActual = "0" And translatedActual.ToString = "0" And actualAtt = "A" And isExcused = True) Then
                                blnTardy = facAttendance.TranslatePALetterToTardyBoolean(actualAtt)
                                blnExcused = facAttendance.TranslatePALetterToExcusedBoolean(actualAtt)
                                facAttendance.UpdateStudentClsSectionAttendanceRecord(stuId, ddlClasses.SelectedItem.Value, attDate, translatedActual, blnTardy, blnExcused)
                                enrollmentIds.Add(Guid.Parse(stuId))

                            End If

                        End If
                    Else
                        'Do an insert
                        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                            facAttendance.AddStudentClsSectionAttendanceRecord(stuId, ddlClasses.SelectedItem.Value, attDate, actualAtt, isTardy)
                            enrollmentIds.Add(Guid.Parse(stuId))

                        ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                            facAttendance.AddStudentClsSectionAttendanceRecord(stuId, ddlClasses.SelectedItem.Value, attDate, actualAtt * 60, isTardy)
                            enrollmentIds.Add(Guid.Parse(stuId))

                        Else
                            translatedActual = facAttendance.TranslatePALetterToQuantity(actualAtt)
                            blnTardy = facAttendance.TranslatePALetterToTardyBoolean(actualAtt)
                            blnExcused = facAttendance.TranslatePALetterToExcusedBoolean(actualAtt)
                            facAttendance.AddStudentClsSectionAttendanceRecord(stuId, ddlClasses.SelectedItem.Value, attDate, translatedActual, blnTardy, blnExcused)
                            enrollmentIds.Add(Guid.Parse(stuId))

                        End If

                        'Response.Write("INSERT Row:" & rowCounter & " Cell:" & cellCounter & " Actual:" & actualAtt & "<br>")
                    End If
                Else
                    'If the date exists in the dt then we will have to do a delete
                    arrRows = DirectCast(Session("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & attDate & "#")
                    If arrRows.Length > 0 Then
                        'Do a delete
                        facAttendance.DeleteStudentClsSectionAttendanceRecord(stuId, ddlClasses.SelectedItem.Value, attDate)
                        enrollmentIds.Add(Guid.Parse(stuId))
                        'Response.Write("DELETE Row:" & rowCounter & " Cell:" & cellCounter & "<br>")
                    End If
                End If
            Next

        Next

        PostPaymentPeriodAttendanceAFA(enrollmentIds)

        FromDate = GetStartDateoftheWeek(txtStart.SelectedDate)
        ToDate = GetEndDateoftheWeek(txtEnd.SelectedDate)
        'Get students class section attendance postings
        Session("ActualAttendance") = facAttendance.GetStudentsAttendanceForClsSection(ddlClasses.SelectedItem.Value, FromDate, ToDate)
        Session("Students") = facAttendance.GetStudentsInClsSection(ddlClasses.SelectedItem.Value, ddlClsSectmeeting.SelectedItem.Value)
        RefreshGrid()

    End Sub
#End Region

#Region "gridView1 - PunchTable"
    Protected Function GetPunchType(ByVal number As Integer) As String
        If (number = 1) Then
            Return "Punch In"
        Else
            Return "Punch Out"
        End If
    End Function
    Private Sub EnableorDisableControlsforTimeClock(ByVal status As Boolean)
        txtActual1.ReadOnly = status
        GridView1.Visible = status

    End Sub
    Private Sub BindDataGrid()
        Dim dr As DataRow
        Dim dtPunch As DataTable
        dtPunch = CType(Session("PunchTable"), DataTable)
        If dtPunch.Rows.Count = 0 Then
            dr = dtPunch.NewRow
            dr("BadgeId") = "None"
            dr("PunchType") = "0"
            dtPunch.Rows.Add(dr)
        End If
        GridView1.DataSource = dtPunch
        ' GridView1.FooterRow.Visible = True
        GridView1.DataBind()
        Session("PunchTable") = dtPunch
    End Sub
    Function ValidateNonTimeCLockdata() As String
        Dim result As String = ""

        If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
            If Not IsNumeric(txtActual1.Text) Then
                result = "Enter in minutes"
            End If
            If Not IsNumeric(txtScheduled1.Text) Then
                result = "Enter in minutes"
            End If
        ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
            If Not IsNumeric(txtActual1.Text) Then
                result = "Enter in hours"
            End If
            If Not IsNumeric(txtScheduled1.Text) Then
                result = "Enter in hours"
            End If
        Else
            If IsNumeric(txtActual1.Text) Then
                result = "Enter P/A/T/E"
            End If
        End If
        Return result

    End Function
    Private Function ValidateData() As String
        Dim i As Integer

        Dim msg As String = String.Empty
        dsStudent = Session("StudentDetails")

        'ds.Tables(0).TableName = "StudentSchedule"
        'ds.Tables(1).TableName = "STudentLOAs"
        'ds.Tables(2).TableName = "StdSuspensions"
        'ds.Tables(3).TableName = "syHolidays"

        If dsStudent.Tables("StudentSchedule").Rows.Count > 0 Then
            If Not dsStudent.Tables("StudentSchedule").Rows(i)("BadgeNumber") Is Nothing Then
                If dsStudent.Tables("StudentSchedule").Rows(i)("BadgeNumber").ToString.Trim = "" Then
                    msg = msg + "Badge Id not found for student. "
                End If
            Else
                msg = msg + "Badge Id not found for student. "
            End If
        Else
            msg = msg + "Badge Id not found for student. "
        End If

        If ViewState("MeetDate") > DateTime.Now Then
            msg = msg + "Cannot post for a Future date. "
        End If

        If dsStudent.Tables("StudentSchedule").Rows.Count > 0 Then
            For i = 0 To dsStudent.Tables("StudentSchedule").Rows.Count - 1
                If ViewState("MeetDate") < CDate(dsStudent.Tables("StudentSchedule").Rows(i)("StartDate")) Then
                    msg = msg + "Cannot post prior to Start date(" + dsStudent.Tables("StudentSchedule").Rows(i)("StartDate") + ")."
                End If
                If ViewState("MeetDate") > CDate(dsStudent.Tables("StudentSchedule").Rows(i)("ExpGradDate")) Then
                    msg = msg + "Cannot post after Graduation date(" + dsStudent.Tables("StudentSchedule").Rows(i)("ExpGradDate") + ")."
                End If
            Next
        End If

        If dsStudent.Tables("STudentLOAs").Rows.Count > 0 Then
            For i = 0 To dsStudent.Tables("STudentLOAs").Rows.Count - 1
                If ViewState("MeetDate") > CDate(dsStudent.Tables("STudentLOAs").Rows(i)("StartDate")) And ViewState("MeetDate") < CDate(dsStudent.Tables("STudentLOAs").Rows(i)("EndDate")) Then
                    msg = msg + "Cannot post for LOA Period(" + dsStudent.Tables("STudentLOAs").Rows(i)("StartDate") + "-" + dsStudent.Tables("STudentLOAs").Rows(i)("EndDate") + ")."
                End If
            Next
        End If

        If dsStudent.Tables("StdSuspensions").Rows.Count > 0 Then
            For i = 0 To dsStudent.Tables("StdSuspensions").Rows.Count - 1
                If ViewState("MeetDate") > CDate(dsStudent.Tables("StdSuspensions").Rows(i)("StartDate")) And ViewState("MeetDate") < CDate(dsStudent.Tables("StdSuspensions").Rows(i)("EndDate")) Then
                    msg = msg + "Cannot post for Suspension Period(" + dsStudent.Tables("StdSuspensions").Rows(i)("StartDate") + "-" + dsStudent.Tables("StdSuspensions").Rows(i)("EndDate") + ")."
                End If
            Next
        End If



        If dsStudent.Tables("syHolidays").Rows.Count > 0 Then
            For i = 0 To dsStudent.Tables("syHolidays").Rows.Count - 1
                If ViewState("MeetDate") >= CDate(dsStudent.Tables("syHolidays").Rows(i)("HolidayStartDate")) And ViewState("MeetDate") <= CDate(dsStudent.Tables("syHolidays").Rows(i)("HolidayEndDate")) Then
                    If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                        If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                            msg = msg + "Cannot post on a Holiday(" + dsStudent.Tables("syHolidays").Rows(i)("HolidayStartDate") + "-" + dsStudent.Tables("syHolidays").Rows(i)("HolidayEndDate") + ")."
                        End If
                    Else
                        msg = msg + "Cannot post on a Holiday(" + dsStudent.Tables("syHolidays").Rows(i)("HolidayStartDate") + "-" + dsStudent.Tables("syHolidays").Rows(i)("HolidayEndDate") + ")."
                    End If
                End If
            Next
        End If
        Return msg

    End Function
    Private Function PopulateClsSectAttendanceinfo() As ClsSectAttendanceInfo
        Dim StuEnrollId As String
        Dim ClsSectionID As String
        Dim ClsSectMeetingID As String
        Dim ClsSectattobj As New ClsSectAttendanceInfo
        Dim MeetDate As DateTime
        StuEnrollId = ViewState("StuEnrollid")
        ClsSectionID = ddlClasses.SelectedItem.Value
        ClsSectMeetingID = ddlClsSectmeeting.SelectedItem.Value

        ClsSectattobj.ClsSectionId = New Guid(ClsSectionID)
        ClsSectattobj.ClsSectMeetingId = New Guid(ClsSectMeetingID)
        ClsSectattobj.StuEnrollId = New Guid(StuEnrollId)
        Dim fac As New TimeClockFacade
        Dim StartTime As String

        Dim selectWk As Integer

        Dim aday As Date = txtStart.SelectedDate
        Dim firstDate As Date = aday.AddDays(-CInt(aday.DayOfWeek))

        selectWk = Math.Floor(DateDiff("d", firstDate, ViewState("FromDate")) / 7) Mod 2




        'StartTime = fac.GetStartTimeForClassSectionMeeting_byClass(ddlClsSectmeeting.SelectedItem.Value, campusId)
        StartTime = fac.GetStartTimeForClassSectionMeeting(ddlClsSectmeeting.SelectedItem.Value, campusId, selectWk)


        'StartTime = fac.GetStartTimeForClassSectionMeeting_byClass(ClsSectMeetingID, campusId)

        MeetDate = DateTime.Parse(Format(ViewState("MeetDate"), "MM/dd/yyyy") + " " + StartTime)

        ClsSectattobj.MeetDate = MeetDate
        ' ClsSectattobj.Scheduled = txtScheduled.Text
        ClsSectattobj.Tardy = chkTardy1.Checked
        ClsSectattobj.Excused = chkExcused.Checked

        If txtActual1.Text = "" Then
            ClsSectattobj.Actual = "9999"
        Else

            Dim actHours As Double
            Double.TryParse(txtActual1.Text, actHours)

            Dim schHours As Double
            Double.TryParse(txtScheduled1.Text, schHours)


            Dim attUnityType As String = ViewState("AttendanceUnitType").ToString.ToUpper()

            If attUnityType = "MINUTES" Or attUnityType = "CLOCK HOURS" Then

                If actHours > 0 Then
                    ClsSectattobj.Tardy = chkTardy1.Checked
                Else
                    ClsSectattobj.Tardy = False
                End If

                If actHours >= 0 And actHours < schHours Then
                    ClsSectattobj.Excused = chkExcused.Checked
                Else
                    ClsSectattobj.Excused = False
                End If

            End If





            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                ClsSectattobj.Actual = txtActual1.Text
                ClsSectattobj.Scheduled = txtScheduled1.Text

            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                ClsSectattobj.Actual = (txtActual1.Text * 60)
                ClsSectattobj.Scheduled = (txtScheduled1.Text * 60)
            Else
                If txtActual1.Text.Trim = "P" Or txtActual1.Text.Trim = "p" Or txtActual1.Text.Trim.ToUpper = "Y" Then
                    ClsSectattobj.Actual = "1"
                ElseIf txtActual1.Text.Trim = "A" Or txtActual1.Text.Trim = "a" Or txtAbsent1.Text.Trim.ToUpper = "Y" Then
                    ClsSectattobj.Actual = "0"
                ElseIf txtActual1.Text.Trim = "T" Or txtActual1.Text.Trim = "t" Or chkTardy1.Checked = True Then
                    ClsSectattobj.Actual = "1"
                    ClsSectattobj.Tardy = True
                ElseIf txtActual1.Text.Trim = "E" Or txtActual1.Text.Trim = "e" Or txtAbsent1.Text.Trim.ToUpper = "Y" Then
                    ClsSectattobj.Actual = "0"
                End If
                If txtScheduled1.Text.ToUpper = "N" Then
                    ClsSectattobj.Scheduled = "0"
                End If
                If txtScheduled1.Text.ToUpper = "Y" Then
                    ClsSectattobj.Scheduled = "1"
                End If
            End If

        End If

        Return ClsSectattobj

    End Function
    Protected Sub btnsave1_Click(sender As Object, e As EventArgs) Handles btnsave1.Click
        Dim result As String
        lblerrmsg.Text = ""
        Dim dtpunch As DataTable
        If Session("ClsSectionUsesTimeClock") = False Then
            Dim validdata As String
            validdata = ValidateNonTimeCLockdata()

            If Not validdata = "" Then
                DisplayErrorMessage(validdata)
                Exit Sub
            End If
            Dim clsSectattinfo As ClsSectAttendanceInfo
            clsSectattinfo = PopulateClsSectAttendanceinfo()
            Dim facade As New ClsSectAttendanceFacade

            result = facade.InsertClsSectAttendanceInfo(clsSectattinfo)

            ''  reloadCurrentWeek()
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub
            Else
                RefreshGrid()
                ' RadAjaxManager1.ResponseScripts.Add("  CloseAndRebind();")
                '
                'Page.ClientScript.RegisterStartupScript([GetType](), "key", "<script type='text/javascript'>CloseWindow();</script>", False)
                ScriptManager.RegisterStartupScript(Me, [GetType](), "closeWin", "Sys.Application.add_load(closeRadWindow);", True)

            End If

            ''-------------------------------------------------------------------------

            ''This Function is used to save the punches entered by the user
            ''The Punch In and Punch out are entered
            ''and the Actual and Makeup ghours or Absent hours
            '' are calculated based on the Schedule hrs
            ''The tardy can be posted irrelevent of whether the student attended late or not
        Else


            'result = ""
            Dim res As String
            Dim i As Integer
            Dim fac As New TimeClockFacade
            ''Remove the empty row added
            dtpunch = Session("PunchTable")
            If dtpunch.Rows.Count = 0 Then

                result = fac.DeletePunchandUpdateClockAttendance_ByClass(ViewState("StuEnrollid"), ViewState("MeetDate"), txtScheduled1.Text, ddlClsSectmeeting.SelectedItem.Value)
                If Not result = "" Then
                    '   Display Error Message
                    ' DisplayErrorMessage(result)
                    lblerrmsg.Text = result
                    Exit Sub
                Else
                    '  DisplayErrorMessage("Record Deleted. Click Refresh on the main screen to view the changes made")

                    lblerrmsg.Text = "Record Deleted."
                    ''''''''''''''''''''''''''
                    GetdatasetfromDB(ViewState("StuEnrollid"), ViewState("MeetDate"), ddlClsSectmeeting.SelectedItem.Value)
                    '    BindHours()
                    'hdnsave.Value = "true"
                    'DisplayhrsinClockAttendancePage()
                    Exit Sub
                End If
            ElseIf dtpunch.Rows.Count <= 1 Then
                lblerrmsg.Text = "Invalid Data to Save."
                Exit Sub

            End If
            ' End If
            ''Remove the artificially added row
            For i = 0 To dtpunch.Rows.Count - 1
                If dtpunch.Rows(i)("BadgeId") = "None" Then
                    dtpunch.Rows(i).Delete()
                    dtpunch.AcceptChanges()
                    Exit For
                End If
            Next
            Session("PunchTable") = dtpunch

            ''If input not valid, return error Message
            If Not IsInputValid() Then
                '  DisplayErrorMessage("PunchIn and Punch Out are not valid.")
                '   lblerrmsg.Text = "PunchIn and Punch Out are not valid."
                Exit Sub
            End If

            ''Save the punches to the database

            Dim tardymarked As Boolean
            tardymarked = chkTardy1.Checked

            dsStudent = Session("StudentDetails")

            campusId = dsStudent.Tables("StudentSchedule").Rows(0)("CampusID").ToString
            ''Scheduled Hours added by Saraswathi lakshmanan on August 31 2009
            ''17344: Need ability to edit scheduled hours for time clock schools 
            Dim Scheduled As Decimal

            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
                Scheduled = txtScheduled1.Text
            ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
                Scheduled = (txtScheduled1.Text * 60)
            Else
                ''Present or Absent Type
                If Not txtScheduled1.Text = "" And txtScheduled1.Text = "Y" Or txtScheduled1.Text = "y" Then
                    Scheduled = 1
                Else
                    Scheduled = 0
                End If

            End If

            result = fac.SavePunchesEntered_byClass(dtpunch, tardymarked, Scheduled, campusId, ViewState("AttendanceUnitType"))
            ''If tardy is checked then mark tardy as true
            res = ""
            'If chktardy.Checked = True Then
            '    res = facade.SaveTardyMarked(stuEnrollID, dtPunch.Rows(0)("ScheduleId").ToString, CDate(ddldate.Text))
            'End If

            If Not result = "" And Not res = "" Then
                '   Display Error Message
                '  DisplayErrorMessage(result)
                lblerrmsg.Text = result
                Exit Sub
            Else

                ' DisplayErrorMessage(fac.Log() + "Click Refresh on the main screen to view the posted punches")
                lblerrmsg.Text = fac.Log() '' + "Click Refresh on the main screen to view the posted punches"
            End If


            GetdatasetfromDB(ViewState("StuEnrollid"), ViewState("MeetDate"), ddlClsSectmeeting.SelectedItem.Value)
            RefreshGrid()
            '            Page.ClientScript.RegisterStartupScript([GetType](), "key", "<script type='text/javascript'>CloseWindow();</script>", False)

            ScriptManager.RegisterStartupScript(Me, [GetType](), "closeWin", "Sys.Application.add_load(closeRadWindow);", True)


            ' RadAjaxManager1.ResponseScripts.Add("  CloseAndRebind();")
            'BindHours()
            'hdnsave.Value = "true"
            'DisplayhrsinClockAttendancePage()
        End If

        ''--------------------------------------------------------------------------
    End Sub
    Private Function IsInputValid() As Boolean
        Dim noofRows As Integer
        Dim noofInpunches As Integer
        Dim noofOutpunches As Integer
        Dim i As Integer
        Dim dtPunch As DataTable


        dtPunch = Session("PunchTable")
        noofRows = dtPunch.Rows.Count
        If noofRows Mod 2 = 1 Then
            '  DisplayErrorMessage("Every Punch In must have a Punch Out")
            lblerrmsg.Text = "Every Punch In must have a Punch Out"
            Return False
        End If
        For i = 0 To noofRows - 1
            If dtPunch.Rows(i)("PunchType") = 1 Then
                noofInpunches = noofInpunches + 1
            End If
            If dtPunch.Rows(i)("PunchType") = 2 Then
                noofOutpunches = noofOutpunches + 1
            End If
        Next
        If noofInpunches > 0 And noofOutpunches = 0 Then
            '   DisplayErrorMessage("No Punch Out found")
            lblerrmsg.Text = "No Punch Out found"
            Return False
        End If
        If noofInpunches = 0 And noofOutpunches > 0 Then
            '  DisplayErrorMessage("No Punch In found")
            lblerrmsg.Text = "No Punch In found"
            Return False
        End If
        If noofInpunches <> noofOutpunches Then
            '  DisplayErrorMessage("Number of Punch In and Punch Out does not match ")
            lblerrmsg.Text = "Number of Punch In and Punch Out does not match "
            Return False
        End If

        Return True
    End Function
    Private Sub BindData()
        GridView1.DataSource = Session("PunchTable")
        GridView1.DataBind()
    End Sub
    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        'Reset the edit index.
        GridView1.EditIndex = -1
        'Bind data to the GridView control.
        BindData()
        HideRadWindow()
        ShowGrid()
    End Sub
    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        Dim dtPunch As DataTable
        e.Cancel = True
        dtPunch = Session("PunchTable")
        'Dim PunTime As Date
        'Dim PunchType As String
        'Dim badgeId As String
        Dim row = GridView1.Rows(e.RowIndex)

        'PunTime = CType(GridView1.Rows(e.RowIndex).FindControl("lblpunchtime"), Label).Text
        'PunchType = CType(GridView1.Rows(e.RowIndex).FindControl("lblpunchtype"), Label).Text
        'badgeId = CType(GridView1.Rows(e.RowIndex).FindControl("lblbadgeId"), Label).Text

        dtPunch.Rows(e.RowIndex).Delete()
        dtPunch.AcceptChanges()

        Session("PunchTable") = dtPunch
        'Bind data to the GridView control.
        BindData()
    End Sub
    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs) Handles GridView1.RowEditing
        'Set the edit index.
        GridView1.EditIndex = e.NewEditIndex
        BindData()
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            If CType(e.Row.Cells(0).FindControl("lblbadgeId"), Label).Text = "None" Then
                e.Row.Visible = False
            End If
        End If
    End Sub
    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        'Retrieve the table from the session object.
        Dim DtPUnch As DataTable
        DtPUnch = Session("PunchTable")
        ' Dim PunchTime As Date
        Dim SchDate As Date
        Dim StuPunDate As String
        'Update the values.
        Dim row = GridView1.Rows(e.RowIndex)
        Dim PunchTime As DateTime


        SchDate = Format(CDate(ViewState("MeetDate")), "MM/dd/yyyy")
        'PunchTime = (CType((row.Cells(0).FindControl("txteditPunchTime")), eWorld.UI.TimePicker)).PostedTime
        Dim startInput As RadTimePicker = DirectCast(row.Cells(0).FindControl("txteditPunchTime"), RadTimePicker)
        PunchTime = Format(startInput.SelectedDate, "h:mm:ss tt")
        '  StuPunDate = startInput.SelectedDate
        StuPunDate = SchDate + " " + PunchTime
        Dim concatenatedDate As DateTime = DateTime.Parse(StuPunDate)

        DtPUnch.Rows(row.DataItemIndex)("PunchTime") = concatenatedDate
        DtPUnch.Rows(row.DataItemIndex)("PunchType") = CType(row.Cells(0).FindControl("ddleditPunchType"), DropDownList).SelectedItem.Value

        'Reset the edit index.
        GridView1.EditIndex = -1
        Session("PunchTable") = DtPUnch
        'Bind data to the GridView control.
        BindData()

    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView1.RowCommand


        ''Add new record
        Dim dtPUnch As DataTable
        Dim PunchType As Integer
        Dim PunchTime As DateTime
        Dim SchDate As Date
        Dim StuPunDate As String
        Dim dr As DataRow
        Dim res As String
        Dim badgeno As String
        lblerrmsg.Text = ""
        'If exitSub = True Then
        '    Exit Sub
        'End If
        dsStudent = Session("StudentDetails")

        If e.CommandName = "ADD" Then
            dtPUnch = Session("PunchTable")
            SchDate = Format(CDate(ViewState("MeetDate")), "MM/dd/yyyy")
            '    SchDate = CDate(lblMeetDate.Text.Split(",")(0).ToString)

            PunchType = CType(GridView1.FooterRow.FindControl("ddlPunchType"), DropDownList).SelectedItem.Value
            If Format(CType(GridView1.FooterRow.FindControl("txtPunchTime"), RadTimePicker).SelectedDate, "h:mm:ss tt") = "" Then
                ' DisplayErrorMessage("Enter Punch Time")
                lblerrmsg.Text = "Enter Punch Time"
                Exit Sub
            End If
            PunchTime = Format(CType(GridView1.FooterRow.FindControl("txtPunchTime"), RadTimePicker).SelectedDate, "h:mm:ss tt")

            res = ValidateData()
            Dim StuEnrollId As String

            If res = "" Then
                dr = dtPUnch.NewRow
                badgeno = dsStudent.Tables("StudentSchedule").Rows(0)("BadgeNumber")
                If badgeno <> "" Then
                    If IsNumeric(badgeno) Then
                        badgeno = Math.Abs(CDec(badgeno))
                    End If
                End If


                StuEnrollId = ViewState("StuEnrollid")
                dr("BadgeId") = badgeno
                dr("ClockId") = "PN00"
                StuPunDate = SchDate + " " + PunchTime
                Dim concatenatedDate As DateTime = DateTime.Parse(StuPunDate)
                dr("PunchTime") = concatenatedDate
                dr("PunchType") = PunchType
                dr("Status") = "0"
                dr("StuEnrollId") = StuEnrollId
                dr("FromSystem") = False
                dr("ClsSectMeetingID") = ddlClsSectmeeting.SelectedItem.Value

                dtPUnch.Rows.Add(dr)
                Session("PunchTable") = dtPUnch
                GridView1.DataSource = dtPUnch
                GridView1.DataBind()
            Else
                DisplayErrorMessage(res)
                Exit Sub
            End If


        End If

    End Sub
    Protected Sub btnCancel1_Click(sender As Object, e As EventArgs) Handles btnCancel1.Click
        ' RadAjaxManager1.ResponseScripts.Add("CancelEdit();")

        ScriptManager.RegisterStartupScript(Me, [GetType](), "closeWin", "Sys.Application.add_load(closeRadWindow);", True)
    End Sub
#End Region

#Region "COMMON SERVICE FUNCTIONS"
    Private Function IsValidValue(ByVal actualAtt As String, ByVal requiredValues As String(), ByVal isNumber As Boolean) As Boolean
        Dim goodMatch As Boolean = False

        If isNumber = False Then

            For Each strValue As String In requiredValues
                If Trim(actualAtt) = strValue Then
                    goodMatch = True
                    Exit For
                End If
            Next
        End If

        Return goodMatch

    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Function GetStartDateoftheWeek(ByVal GivenDate As Date) As Date

        Dim FromDate As Date

        Select Case GivenDate.DayOfWeek
            Case DayOfWeek.Sunday
                FromDate = GivenDate
            Case DayOfWeek.Monday
                FromDate = GivenDate.AddDays(-1)
            Case DayOfWeek.Tuesday
                FromDate = GivenDate.AddDays(-2)
            Case DayOfWeek.Wednesday
                FromDate = GivenDate.AddDays(-3)
            Case DayOfWeek.Thursday
                FromDate = GivenDate.AddDays(-4)
            Case DayOfWeek.Friday
                FromDate = GivenDate.AddDays(-5)
            Case DayOfWeek.Saturday
                FromDate = GivenDate.AddDays(-6)
        End Select
        Return FromDate
    End Function
    Private Function GetEndDateoftheWeek(ByVal GivenDate As Date) As Date

        Dim ToDate As Date

        Select Case GivenDate.DayOfWeek
            Case DayOfWeek.Sunday
                ToDate = GivenDate.AddDays(6)
            Case DayOfWeek.Monday
                ToDate = GivenDate.AddDays(5)
            Case DayOfWeek.Tuesday
                ToDate = GivenDate.AddDays(4)
            Case DayOfWeek.Wednesday
                ToDate = GivenDate.AddDays(3)
            Case DayOfWeek.Thursday
                ToDate = GivenDate.AddDays(2)
            Case DayOfWeek.Friday
                ToDate = GivenDate.AddDays(1)
            Case DayOfWeek.Saturday
                ToDate = GivenDate
        End Select
        Return ToDate
    End Function
    'Private Sub ClearControls(ByVal control As Control)
    '    Dim i As Integer
    '    For i = control.Controls.Count - 1 To 0 Step i - 1
    '        ClearControls(control.Controls(i))
    '    Next
    '    If control.GetType.ToString = "System.Web.UI.HtmlControls.HtmlTableCell" Then
    '        'If control.GetType.ToString = "System.Web.UI.WebControls.TableCell" Then
    '        If Not control.GetType().GetProperty("SelectedItem") Is Nothing Then
    '            Dim literal As LiteralControl = New LiteralControl
    '            control.Parent.Controls.Add(literal)
    '            literal.Text = CType(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing), String)
    '            control.Parent.Controls.Remove(control)
    '        Else
    '            Dim literal As LiteralControl = New LiteralControl
    '            control.Parent.Controls.Add(literal)
    '            literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
    '            control.Parent.Controls.Remove(control)
    '        End If
    '    End If
    '    Return
    'End Sub
    'Private Sub HideAllTextBoxes(ByVal control As Control)
    '    Dim i As Integer
    '    For i = control.Controls.Count - 1 To 0 Step i - 1
    '        HideAllTextBoxes(control.Controls(i))
    '    Next
    '    If control.GetType.ToString = "System.Web.UI.WebControls.TextBox" Then
    '        'If control.GetType.ToString = "System.Web.UI.WebControls.TableCell" Then
    '        Dim literal As LiteralControl = New LiteralControl
    '        control.Parent.Controls.Add(literal)
    '        literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
    '        'control.Parent.Controls.Remove(control)
    '        control.Visible = False
    '    End If
    '    Return
    'End Sub
#End Region

    Protected Sub btnExportToExcell_Click(sender As Object, e As EventArgs) Handles btnExportToExcell.Click
        ExportDatasetToCsv1()
    End Sub

    Private Sub ExportDatasetToCsv1()
        Dim attachment As String = "attachment; filename=attendance " + Date.Now.ToString + ".xls"
        Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/vnd.ms-excel"
        Dim tab As String = ""

        Dim dRows As DataRow() = CType(ViewState("dtStudentDetails"), DataTable).Select("SunDate LIke '" + Format(CDate(ViewState("FromDate")), "MM/dd/yyyy") + "%'")

        Dim dRow As DataRow = dRows(0)


        Response.Write(tab + "Student Name")
        tab = vbTab

        Response.Write(tab + CDate(dRow.ItemArray(30)).ToShortDateString + " ")
        tab = vbTab

        Response.Write(tab + CDate(dRow.ItemArray(24)).ToShortDateString())
        tab = vbTab

        Response.Write(tab + CDate(dRow.ItemArray(25)).ToShortDateString())
        tab = vbTab

        Response.Write(tab + CDate(dRow.ItemArray(26)).ToShortDateString())
        tab = vbTab

        Response.Write(tab + CDate(dRow.ItemArray(27)).ToShortDateString())
        tab = vbTab

        Response.Write(tab + CDate(dRow.ItemArray(28)).ToShortDateString())
        tab = vbTab

        Response.Write(tab + CDate(dRow.ItemArray(29)).ToShortDateString())
        tab = vbTab


        Response.Write(vbLf)

        Response.Write(tab + "Sunday")
        tab = vbTab

        Response.Write(tab + "Monday")
        tab = vbTab

        Response.Write(tab + "Tuesday")
        tab = vbTab

        Response.Write(tab + "Wednesday")
        tab = vbTab

        Response.Write(tab + "Thursday")
        tab = vbTab

        Response.Write(tab + "Friday")
        tab = vbTab

        Response.Write(tab + "Saturday")
        tab = vbTab



        Response.Write(vbLf)

        For Each dr As DataRow In dRows
            tab = ""

            Response.Write(tab + dr.ItemArray(1).ToString())
            tab = vbTab
            Response.Write(tab + dr.ItemArray(23).ToString())
            tab = vbTab
            Response.Write(tab + dr.ItemArray(11).ToString())
            tab = vbTab
            Response.Write(tab + dr.ItemArray(13).ToString())
            tab = vbTab
            Response.Write(tab + dr.ItemArray(15).ToString())
            tab = vbTab
            Response.Write(tab + dr.ItemArray(17).ToString())
            tab = vbTab
            Response.Write(tab + dr.ItemArray(19).ToString())
            tab = vbTab
            Response.Write(tab + dr.ItemArray(21).ToString())
            tab = vbTab



            Response.Write(vbLf)
        Next
        Response.End()

    End Sub

    'Private Sub ExportDatasetToCsv()

    '    'Declaration of Variables

    '    Dim IsOutputStreamed As Boolean = False
    '    Dim dRows As DataRow() = CType(ViewState("dtStudentDetails"), DataTable).Select("SunDate LIke '" + Format(CDate(ViewState("FromDate")), "MM/dd/yyyy") + "%'")



    '    Dim myString As New StringBuilder
    '    Dim bFirstRecord As Boolean = True

    '    Dim myWriter As New StreamWriter("C:\MyTestCSV.csv")


    '    ' myString = ""

    '    Try



    '        For Each dr As DataRow In dRows

    '            bFirstRecord = True

    '            For Each field As Object In dr.ItemArray

    '                If Not bFirstRecord Then

    '                    myString.Append(",")


    '                End If

    '                myString.Append(field.ToString)

    '                bFirstRecord = False

    '            Next

    '            'New Line to differentiate next row 
    '            myString.Append(Environment.NewLine)


    '        Next

    '        If String.IsNullOrEmpty(myString.ToString()) Then

    '            Response.Clear()

    '            Response.ContentType = "Text/vnd.ms-excel"

    '            Response.AddHeader("Content-Disposition", "attachment;filename=report.csv")

    '            Response.Write(myString.ToString())


    '            IsOutputStreamed = True
    '        End If
    '    Catch ex As Exception
    '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)


    '        MsgBox(ex.Message)

    '    Finally
    '        If IsOutputStreamed Then

    '            Response.End()
    '        End If
    '    End Try




    '    ''Write the String to the Csv File 

    '    'myWriter.WriteLine(myString)

    '    ''Clean up 
    '    'myWriter.Close()





    'End Sub

End Class


#Region "OLD CODE - arhived"
'Private Sub BuildAttendanceGrid(Optional ByVal newTable As Boolean = False)
'    'First create the header
'    Dim facAttendance As New ClsSectAttendanceFacade
'    Dim tbl As New Table
'    Dim dRow As DataRow
'    Dim dtm As DateTime
'    Dim dtm1 As DateTime
'    Dim r As New TableRow
'    Dim c1 As New TableCell
'    Dim c2 As New TableCell
'    Dim c4 As New TableCell
'    Dim lbl As New Label
'    Dim lbl2 As New Label
'    Dim lbl4 As New Label
'    Dim rnd As New Random
'    Dim rowCounter As Integer
'    Dim cellCounter As Integer
'    Dim dtDays As New DataTable
'    Dim userMessage As String = ""

'    pnlAttendance.Controls.Clear()
'    pnlRequiredFieldValidators.Controls.Clear()

'    If newTable = True Then
'        'tblCounter = rnd.Next(1, 1000000)
'        tblCounter += 1
'        tbl.ID = "tbl" & tblCounter.ToString
'    End If

'    tbl.Width = Unit.Percentage(100)
'    tbl.CellPadding = 0
'    tbl.CellSpacing = 0
'    tbl.CssClass = "contenttable"

'    lbl.CssClass = "LabelBold"
'    lbl.Text = "Student"
'    c1.VerticalAlign = VerticalAlign.Bottom
'    c1.Controls.Add(lbl)
'    c1.CssClass = "Datagridheader"
'    r.Cells.Add(c1)

'    lbl2.CssClass = "LabelBold"
'    lbl2.Text = "Status"
'    c2.VerticalAlign = VerticalAlign.Bottom
'    c2.Controls.Add(lbl2)
'    c2.CssClass = "Datagridheader"
'    r.Cells.Add(c2)

'    lbl4.CssClass = "LabelBold"
'    lbl4.Text = "IsFutureStart"
'    c4.VerticalAlign = VerticalAlign.Bottom
'    c4.Controls.Add(lbl4)
'    c1.CssClass = "Datagridheader"
'    c4.Visible = False
'    r.Cells.Add(c4)

'    dtDays = DirectCast(Session("MeetDays"), DataTable)
'    Dim sday As String
'    For Each dtm In DirectCast(Session("Dates"), ArrayList)
'        Dim c3 As New TableCell
'        Dim lbl3 As New Label
'        sday = facAttendance.GetShortDayName(dtm)
'        c3.HorizontalAlign = HorizontalAlign.Center
'        c3.VerticalAlign = VerticalAlign.Top
'        c3.CssClass = "Label"
'        lbl3.CssClass = "Label"
'        lbl3.Text = sday & "<br>" & facAttendance.GetShortMonthName(dtm) & " " & dtm.Day.ToString
'        ''parameter Dtm is added to the function GetDurationForMeetDate
'        ''Added on jan 12 2009
'        lbl3.Text &= " <br> " & facAttendance.GetStartTimeForMeetDate(dtDays, dtm) & "<br>" & GetDurationForMeetDate(dtDays, sday, dtm)
'        c3.Controls.Add(lbl3)
'        c3.CssClass = "Datagridheader"
'        r.Cells.Add(c3)
'    Next

'    tbl.Rows.Add(r)

'    'Now loop through the list of students and create a row for each of them
'    For Each dRow In DirectCast(Session("Students"), DataTable).Rows
'        Dim rStudent As New TableRow
'        Dim cLast As New TableCell
'        Dim cStatus As New TableCell
'        Dim cFirst As New TableCell
'        Dim cFStart As New TableCell
'        Dim lLast As New Label
'        Dim lFirst As New Label
'        Dim lStatus As New Label
'        Dim lFStart As New CheckBox

'        rowCounter += 1

'        lLast.CssClass = "Label"
'        lLast.Text = dRow("LastName") & ", " & dRow("FirstName")
'        cLast.Controls.Add(lLast)
'        cLast.CssClass = "datagriditemstyle"
'        rStudent.Cells.Add(cLast)

'        lStatus.CssClass = "Label"
'        lStatus.Text = dRow("StatusCodeDescrip")
'        cStatus.Controls.Add(lStatus)
'        cStatus.CssClass = "datagriditemstyle"
'        rStudent.Cells.Add(cStatus)

'        lFStart.CssClass = "CheckBox"
'        lFStart.Checked = dRow("IsFutureStart")
'        cFStart.Controls.Add(lFStart)
'        cFStart.CssClass = "Label"
'        cFStart.Visible = False
'        rStudent.Cells.Add(cFStart)

'        'Loop through the list of dates and add a cell with a textbox for each of them
'        For Each dtm1 In DirectCast(Session("Dates"), ArrayList)
'            Dim c5 As New TableCell
'            Dim txtbox5 As New TextBox
'            Dim chkbox5 As New CheckBox
'            '                Dim arrRows() As DataRow
'            Dim cmv As New CompareValidator
'            Dim rnv As New RangeValidator
'            Dim rev As New RegularExpressionValidator

'            cellCounter += 1

'            c5.HorizontalAlign = HorizontalAlign.Center
'            c5.CssClass = "Datagriditemstyle"
'            txtbox5.CssClass = "TextBox"
'            txtbox5.ID = "txt" & rowCounter.ToString & cellCounter.ToString
'            chkbox5.CssClass = "CheckBox"
'            chkbox5.ID = "chk" & rowCounter.ToString & cellCounter.ToString
'            chkbox5.Text = "T"
'            'Check if the student is on LOA for this date and for the enrollment that we
'            'are dealing with.
'            If facAttendance.IsStudentOnLOA(dRow("StuEnrollId").ToString(), dtm1, DirectCast(Session("StudentsLOAInfo"), DataTable)) Then
'                txtbox5.ReadOnly = True
'                txtbox5.ToolTip = "Student is/was on LOA"
'            Else
'                'Check if the student has a drop status. If so and the LDA or Date Determined is earlier
'                'than or equal to the test date then we need to make the text box read only.
'                If dRow("SysStatusId") = 12 Then
'                    If Not dRow.IsNull("DateDetermined") Then
'                        If dRow("DateDetermined") <= dtm1 Then
'                            txtbox5.ReadOnly = True
'                            txtbox5.ToolTip = "Student has been dropped"
'                        End If
'                    Else
'                        'If no date determined was entered then we will not allow the user to input
'                        'any attendance. This will force them to get the data corrected.
'                        txtbox5.ReadOnly = True
'                        txtbox5.ToolTip = "Student has been dropped but there is no date determined." & vbCr & "Please contact the Registrar to have this problem fixed."
'                    End If

'                End If
'                If dRow("SysStatusId") = 8 Then
'                    txtbox5.ReadOnly = True
'                    txtbox5.ToolTip = "Student has been Cancelled."

'                End If


'                'Check if the student has a start date specified. If not we will not allow the posting
'                'of attendance but we also need to alert the instructor so that he/she can have the
'                'registrar correct the problem.
'                If dRow.IsNull("StartDate") Then
'                    txtbox5.ReadOnly = True
'                    txtbox5.ToolTip = "Student does not have a start date." & vbCr & "Please contact the Registrar to have this problem fixed."
'                Else
'                    'If the student has a start date that is after the meet date being examined then we
'                    'should not allow attendance to be posted for it.
'                    If dtm1 < dRow("StartDate") Then
'                        txtbox5.ReadOnly = True
'                        txtbox5.ToolTip = "Student started after this day"
'                    End If
'                End If

'                'If the date being examined is greater than today's date then we should not allow
'                'attendance to be posted for it.
'                If dtm1 > Date.Now Then
'                    txtbox5.ReadOnly = True
'                    txtbox5.ToolTip = "Attendance cannot be posted for a date that is greater than the current date."
'                End If
'            End If
'            c5.Controls.Add(txtbox5)
'            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Or ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                c5.Controls.Add(chkbox5)
'            End If
'            rStudent.Cells.Add(c5)

'            'If the attendance type is present/absent then we only want to allow the user to
'            'enter P, A, E or T.
'            If ViewState("AttendanceUnitType").ToString.ToUpper = "PRESENT ABSENT" Then

'                With rev
'                    revCount += 1
'                    '.ID = "rev" & rnd.Next(1, 1000000)
'                    .ID = "rev" & revCount.ToString()
'                    .ControlToValidate = txtbox5.ID
'                    .Display = ValidatorDisplay.None
'                    .ErrorMessage = "Enter a value of P, A, E or T  " & " for " & dRow("LastName") & ", " & dRow("FirstName") & " on " & dtm1.ToLongDateString
'                    .ValidationExpression = "[PpAaEeTt]"
'                End With
'                pnlRequiredFieldValidators.Controls.Add(rev)
'            End If

'            If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                'Allow only numeric values between 0 and the scheduled minutes.
'                rnv.ControlToValidate = txtbox5.ID
'                rnv.MinimumValue = 0
'                rnv.Type = ValidationDataType.Integer
'                'rnv.MaximumValue = CInt(GetScheduledMinutesForMeetDay(dtm1))
'                rnv.MaximumValue = 10000
'                rnv.Display = ValidatorDisplay.None
'                rnv.ErrorMessage = "Enter a value between 0 and " & GetScheduledMinutesForMeetDay(dtm1) & " for " & dRow("LastName") & ", " & dRow("FirstName") & " on " & dtm1.ToLongDateString
'                pnlRequiredFieldValidators.Controls.Add(rnv)
'            End If
'            If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                'Allow only numeric values between 0 and the scheduled minutes.
'                rnv.ControlToValidate = txtbox5.ID
'                rnv.MinimumValue = 0
'                rnv.Type = ValidationDataType.Double
'                'rnv.MaximumValue = CInt(GetScheduledMinutesForMeetDay(dtm1))
'                rnv.Display = ValidatorDisplay.None
'                rnv.ErrorMessage = "Enter a value between 0 and " & GetScheduledMinutesForMeetDay(dtm1) & " for " & dRow("LastName") & ", " & dRow("FirstName") & " on " & dtm1.ToLongDateString
'                pnlRequiredFieldValidators.Controls.Add(rnv)
'            End If
'        Next

'        tbl.Rows.Add(rStudent)

'        'If the enrollment does not have a start date we want to add the student to the message
'        'to be displayed to the user.
'        If dRow.IsNull("StartDate") Then
'            userMessage &= "   --- " & dRow("FirstName") & " " & dRow("LastName") & vbCr
'        End If


'    Next

'    pnlAttendance.Controls.Add(tbl)

'    If userMessage <> "" Then
'        Dim headerMessg As String
'        headerMessg = "You will not be able to post attendance for the following students" & vbCr
'        headerMessg &= "because they do not have a start date specified." & vbCr
'        headerMessg &= "Please contact the Registrar to have this problem fixed." & vbCr
'        headerMessg &= userMessage
'        DisplayErrorMessage(headerMessg)
'    End If
'End Sub
'Public Function SaveAttendanceInfo(ByVal PostZero As Boolean, ByVal PostException As Boolean) As String

'    ''    Dim dtStart As System.DateTime = DateTime.Parse(Format(curDate, "MM/dd/yyyy") + " " + StartTime)
'    Dim fac As New FAME.AdvantageV1.BusinessFacade.TimeClock.TimeClockFacade
'    Dim StartTime As String
'    Dim selectWk As Integer = 0

'    Dim aday As Date = txtStart.SelectedDate
'    Dim firstDate As Date = aday.AddDays(-CInt(aday.DayOfWeek))

'    selectWk = Math.Floor(DateDiff("d", firstDate, ViewState("FromDate")) / 7) Mod 2




'    'StartTime = fac.GetStartTimeForClassSectionMeeting_byClass(ddlClsSectmeeting.SelectedItem.Value, campusId)
'    StartTime = fac.GetStartTimeForClassSectionMeeting(ddlClsSectmeeting.SelectedItem.Value, campusId, selectWk)


'    Dim dtStudentDetails As DataTable
'    dtStudentDetails = ViewState("dtStudentDetails")
'    Dim FromDate As Date
'    FromDate = ViewState("FromDate")
'    Dim dtUpdatedAttendanceRows As DataTable
'    dtUpdatedAttendanceRows = CreateclsSectAttendanceTable()

'    Dim updatedRows() As DataRow

'    For Each item As GridItem In dgAttendance.MasterTableView.Items
'        If TypeOf item Is GridDataItem Then
'            Dim row As DataRow


'            Dim lblStudentName As Label = DirectCast(item.FindControl("lblStudentName"), Label)
'            Dim str As String
'            str = lblStudentName.Text
'            Dim itemStuEnrollID As GridDataItem = CType(item, GridDataItem)
'            Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text

'            updatedRows = dtStudentDetails.Select("StuEnrollId='" + itemStuEnrollIDValue + "' And SunDate Like'" + Format(CDate(FromDate), "MM/dd/yyyy") + "%'")

'            For Each updateddr In updatedRows

'                For i As Integer = 0 To 6
'                    row = dtUpdatedAttendanceRows.NewRow()
'                    row("StuEnrollId") = itemStuEnrollIDValue
'                    row("ClsSectionID") = ddlClasses.SelectedItem.Value
'                    row("ClsSectMeetingID") = ddlClsSectmeeting.SelectedItem.Value

'                    Dim SysStatusValue As String = updateddr("SysStatusId")
'                    Dim StuEnrollId As String = itemStuEnrollIDValue
'                    Dim DateDetermined As String = updateddr("DateDetermined")
'                    Dim StudentStartDate As String = updateddr("StudentStartDate")
'                    Dim DayStatus As String = ""

'                    Select Case i
'                        Case 0
'                            DayStatus = IsDayEditable(CDate(updateddr("SunDate")), StuEnrollId, SysStatusValue, DateDetermined, StudentStartDate)
'                            row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("SunDate")), "MM/dd/yyyy") + " " + StartTime)
'                            row("Scheduled") = updateddr("SunSch").ToString
'                            If DirectCast(item.FindControl("txtSunAct"), TextBox).Text = "" Then
'                                If PostZero And (updateddr("SunSch") <> "0" And updateddr("SunSch") <> "9999") And DayStatus = "" Then
'                                    row("Actual") = "0"
'                                ElseIf PostException And (updateddr("SunSch") <> "0" And updateddr("SunSch") <> "9999") And DayStatus = "" Then

'                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                                        row("Actual") = updateddr("SunSch")
'                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                                        'row("Actual") = (CDec(updateddr("SunSch")) * 60).ToString
'                                        row("Actual") = CDec(updateddr("SunSch")).ToString
'                                    Else
'                                        row("Actual") = "P"
'                                    End If

'                                Else
'                                    row("Actual") = "9999"
'                                End If

'                            Else
'                                row("Actual") = DirectCast(item.FindControl("txtSunAct"), TextBox).Text
'                                updateddr("SunAct") = DirectCast(item.FindControl("txtSunAct"), TextBox).Text
'                            End If
'                            row("Tardy") = DirectCast(item.FindControl("chkSunTardy"), CheckBox).Checked
'                            updateddr("SunTardy") = DirectCast(item.FindControl("chkSunTardy"), CheckBox).Checked
'                            If DirectCast(item.FindControl("txtSunAct"), TextBox).Text = "E" Then
'                                row("Excused") = True
'                            Else
'                                row("Excused") = False
'                            End If

'                        Case 1
'                            DayStatus = IsDayEditable(CDate(updateddr("MonDate")), StuEnrollId, SysStatusValue, DateDetermined, StudentStartDate)
'                            row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("MonDate")), "MM/dd/yyyy") + " " + StartTime)
'                            row("Scheduled") = updateddr("MonSch").ToString
'                            If DirectCast(item.FindControl("txtMonAct"), TextBox).Text = "" Then
'                                If PostZero And (updateddr("MonSch") <> "0" And updateddr("MonSch") <> "9999") And DayStatus = "" Then
'                                    row("Actual") = "0"
'                                ElseIf PostException And (updateddr("MonSch") <> "0" And updateddr("MonSch") <> "9999") And DayStatus = "" Then
'                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                                        row("Actual") = updateddr("MonSch")
'                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                                        'row("Actual") = (CDec(updateddr("MonSch")) * 60).ToString
'                                        row("Actual") = CDec(updateddr("MonSch")).ToString
'                                    Else
'                                        row("Actual") = "P"
'                                    End If
'                                Else
'                                    row("Actual") = "9999"
'                                End If
'                            Else
'                                row("Actual") = DirectCast(item.FindControl("txtMonAct"), TextBox).Text
'                                updateddr("MonAct") = DirectCast(item.FindControl("txtMonAct"), TextBox).Text
'                            End If
'                            row("Tardy") = DirectCast(item.FindControl("chkMonTardy"), CheckBox).Checked
'                            updateddr("MonTardy") = DirectCast(item.FindControl("chkMonTardy"), CheckBox).Checked
'                            If DirectCast(item.FindControl("txtMonAct"), TextBox).Text = "E" Then
'                                row("Excused") = True
'                            Else
'                                row("Excused") = False
'                            End If

'                        Case 2
'                            DayStatus = IsDayEditable(CDate(updateddr("TueDate")), StuEnrollId, SysStatusValue, DateDetermined, StudentStartDate)
'                            row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("TueDate")), "MM/dd/yyyy") + " " + StartTime)
'                            row("Scheduled") = updateddr("TueSch").ToString
'                            If DirectCast(item.FindControl("txtTueAct"), TextBox).Text = "" Then
'                                If PostZero And (updateddr("TueSch") <> "0" And updateddr("TueSch") <> "9999") And DayStatus = "" Then
'                                    row("Actual") = "0"
'                                ElseIf PostException And (updateddr("TueSch") <> "0" And updateddr("TueSch") <> "9999") And DayStatus = "" Then
'                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                                        row("Actual") = updateddr("TueSch")
'                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                                        ''   row("Actual") = (CDec(updateddr("TueSch")) * 60).ToString
'                                        row("Actual") = CDec(updateddr("TueSch")).ToString
'                                    Else
'                                        row("Actual") = "P"
'                                    End If
'                                Else
'                                    row("Actual") = "9999"
'                                End If
'                            Else
'                                row("Actual") = DirectCast(item.FindControl("txtTueAct"), TextBox).Text
'                                updateddr("TueAct") = DirectCast(item.FindControl("txtTueAct"), TextBox).Text
'                            End If
'                            row("Tardy") = DirectCast(item.FindControl("chkTueTardy"), CheckBox).Checked
'                            updateddr("TueTardy") = DirectCast(item.FindControl("chkTueTardy"), CheckBox).Checked
'                            If DirectCast(item.FindControl("txtTueAct"), TextBox).Text = "E" Then
'                                row("Excused") = True
'                            Else
'                                row("Excused") = False
'                            End If
'                        Case 3
'                            DayStatus = IsDayEditable(CDate(updateddr("WedDate")), StuEnrollId, SysStatusValue, DateDetermined, StudentStartDate)
'                            row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("WedDate")), "MM/dd/yyyy") + " " + StartTime)
'                            row("Scheduled") = updateddr("WedSch").ToString
'                            If DirectCast(item.FindControl("txtWedAct"), TextBox).Text = "" Then
'                                If PostZero And (updateddr("WedSch") <> "0" And updateddr("WedSch") <> "9999") And DayStatus = "" Then
'                                    row("Actual") = "0"
'                                ElseIf PostException And (updateddr("WedSch") <> "0" And updateddr("WedSch") <> "9999") And DayStatus = "" Then
'                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                                        row("Actual") = updateddr("WedSch")
'                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                                        ' row("Actual") = (CDec(updateddr("WedSch")) * 60).ToString
'                                        row("Actual") = CDec(updateddr("WedSch")).ToString
'                                    Else
'                                        row("Actual") = "P"
'                                    End If
'                                Else
'                                    row("Actual") = "9999"
'                                End If
'                            Else
'                                row("Actual") = DirectCast(item.FindControl("txtWedAct"), TextBox).Text
'                                updateddr("WedAct") = DirectCast(item.FindControl("txtWedAct"), TextBox).Text
'                            End If
'                            row("Tardy") = DirectCast(item.FindControl("chkWedTardy"), CheckBox).Checked
'                            updateddr("WedTardy") = DirectCast(item.FindControl("chkWedTardy"), CheckBox).Checked
'                            If DirectCast(item.FindControl("txtWedAct"), TextBox).Text = "E" Then
'                                row("Excused") = True
'                            Else
'                                row("Excused") = False
'                            End If
'                        Case 4
'                            DayStatus = IsDayEditable(CDate(updateddr("ThurDate")), StuEnrollId, SysStatusValue, DateDetermined, StudentStartDate)
'                            row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("ThurDate")), "MM/dd/yyyy") + " " + StartTime)
'                            row("Scheduled") = updateddr("ThurSch").ToString
'                            If DirectCast(item.FindControl("txtThurAct"), TextBox).Text = "" Then
'                                If PostZero And (updateddr("ThurSch") <> "0" And updateddr("ThurSch") <> "9999") And DayStatus = "" Then
'                                    row("Actual") = "0"
'                                ElseIf PostException And (updateddr("ThurSch") <> "0" And updateddr("ThurSch") <> "9999") And DayStatus = "" Then
'                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                                        row("Actual") = updateddr("ThurSch")
'                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                                        '  row("Actual") = (CDec(updateddr("ThurSch")) * 60).ToString
'                                        row("Actual") = CDec(updateddr("ThurSch")).ToString
'                                    Else
'                                        row("Actual") = "P"
'                                    End If
'                                Else
'                                    row("Actual") = "9999"
'                                End If
'                            Else
'                                row("Actual") = DirectCast(item.FindControl("txtThurAct"), TextBox).Text
'                                updateddr("ThurAct") = DirectCast(item.FindControl("txtThurAct"), TextBox).Text
'                            End If
'                            row("Tardy") = DirectCast(item.FindControl("chkThurTardy"), CheckBox).Checked
'                            updateddr("ThurTardy") = DirectCast(item.FindControl("chkThurTardy"), CheckBox).Checked
'                            If DirectCast(item.FindControl("txtThurAct"), TextBox).Text = "E" Then
'                                row("Excused") = True
'                            Else
'                                row("Excused") = False
'                            End If
'                        Case 5
'                            DayStatus = IsDayEditable(CDate(updateddr("FriDate")), StuEnrollId, SysStatusValue, DateDetermined, StudentStartDate)
'                            row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("FriDate")), "MM/dd/yyyy") + " " + StartTime)
'                            row("Scheduled") = updateddr("FriSch").ToString
'                            If DirectCast(item.FindControl("txtFriAct"), TextBox).Text = "" Then
'                                If PostZero And (updateddr("FriSch") <> "0" And updateddr("FriSch") <> "9999") And DayStatus = "" Then
'                                    row("Actual") = "0"
'                                ElseIf PostException And (updateddr("FriSch") <> "0" And updateddr("FriSch") <> "9999") And DayStatus = "" Then
'                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                                        row("Actual") = updateddr("FriSch")
'                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                                        row("Actual") = CDec(updateddr("FriSch")).ToString
'                                        '  row("Actual") = (CDec(updateddr("FriSch")) * 60).ToString
'                                    Else
'                                        row("Actual") = "P"
'                                    End If
'                                Else
'                                    row("Actual") = "9999"
'                                End If
'                            Else
'                                row("Actual") = DirectCast(item.FindControl("txtFriAct"), TextBox).Text
'                                updateddr("FriAct") = DirectCast(item.FindControl("txtFriAct"), TextBox).Text
'                            End If
'                            row("Tardy") = DirectCast(item.FindControl("chkFriTardy"), CheckBox).Checked
'                            updateddr("FriTardy") = DirectCast(item.FindControl("chkFriTardy"), CheckBox).Checked
'                            If DirectCast(item.FindControl("txtFriAct"), TextBox).Text = "E" Then
'                                row("Excused") = True
'                            Else
'                                row("Excused") = False
'                            End If
'                        Case 6
'                            DayStatus = IsDayEditable(CDate(updateddr("SatDate")), StuEnrollId, SysStatusValue, DateDetermined, StudentStartDate)
'                            row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("SatDate")), "MM/dd/yyyy") + " " + StartTime)
'                            row("Scheduled") = updateddr("SatSch").ToString
'                            If DirectCast(item.FindControl("txtSatAct"), TextBox).Text = "" Then
'                                If PostZero And (updateddr("SatSch") <> "0" And updateddr("SatSch") <> "9999") And DayStatus = "" Then
'                                    row("Actual") = "0"
'                                ElseIf PostException And (updateddr("SatSch") <> "0" And updateddr("SatSch") <> "9999") And DayStatus = "" Then
'                                    If ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                                        row("Actual") = updateddr("SatSch")
'                                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                                        row("Actual") = CDec(updateddr("SatSch")).ToString
'                                        '    row("Actual") = (CDec(updateddr("SatSch")) * 60).ToString
'                                    Else
'                                        row("Actual") = "P"
'                                    End If
'                                Else
'                                    row("Actual") = "9999"
'                                End If
'                            Else
'                                row("Actual") = DirectCast(item.FindControl("txtSatAct"), TextBox).Text
'                                updateddr("SatAct") = DirectCast(item.FindControl("txtSatAct"), TextBox).Text
'                            End If
'                            row("Tardy") = DirectCast(item.FindControl("chkSatTardy"), CheckBox).Checked
'                            updateddr("SatTardy") = DirectCast(item.FindControl("chkSatTardy"), CheckBox).Checked
'                            If DirectCast(item.FindControl("txtSatAct"), TextBox).Text = "E" Then
'                                row("Excused") = True
'                            Else
'                                row("Excused") = False
'                            End If

'                    End Select
'                    dtUpdatedAttendanceRows.Rows.Add(row)
'                    dtUpdatedAttendanceRows.AcceptChanges()
'                Next

'            Next

'        End If
'    Next

'    Dim res As String = ""
'    If dtUpdatedAttendanceRows.Rows.Count > 0 Then

'        dtUpdatedAttendanceRows = ConvertActualAttendanceBasedonUnitType(dtUpdatedAttendanceRows)
'        Dim dtnewDatatableforSaving As New DataTable
'        dtnewDatatableforSaving = dtUpdatedAttendanceRows.Clone
'        For Each item In dtUpdatedAttendanceRows.Rows
'            dtnewDatatableforSaving.ImportRow(item)
'        Next
'        Dim dsFinalDataset As New DataSet
'        dsFinalDataset.Tables.Add(dtnewDatatableforSaving)
'        res = AddClsSectAttendance(dsFinalDataset)

'    End If
'    Return res

'End Function
'Protected Sub dgAttendance_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles dgAttendance.ItemCommand

'    Dim clsSectionId As String
'    Dim ClsSectMeetingId As String
'    clsSectionId = ddlClasses.SelectedItem.Value
'    ClsSectMeetingId = ddlClsSectmeeting.SelectedItem.Value
'    Dim PostZero As Boolean = False


'    If e.CommandName.ToString.ToLower = "savechanges" Then
'        If Session("ClsSectionUsesTimeClock") = True Then
'            Exit Sub
'        End If
'        HideRadWindow()
'        ShowGrid()

'        Dim res As String
'        res = SaveAttendanceInfo(False, False)
'        RefreshGrid()
'    ElseIf e.CommandName = "PostZero" Then
'        HideRadWindow()
'        ShowGrid()
'        Dim res As String
'        res = SaveAttendanceInfo(True, False)
'        RefreshGrid()
'    ElseIf e.CommandName = "PostException" Then
'        HideRadWindow()
'        ShowGrid()
'        Dim res As String
'        res = SaveAttendanceInfo(False, True)
'        RefreshGrid()

'    ElseIf e.CommandName = "OpenEditFormSunday" Then

'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text

'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblSunDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text

'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtSunAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblSunSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkSunTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked

'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)

'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")

'        UserListDialog.VisibleOnPageLoad = True


'    ElseIf e.CommandName = "OpenEditFormMonday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblMonDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtMonAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblMonSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkMonTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked

'        lblClsSmeeting1.Text = ClsSectMeeting
'        txtScheduled1.Text = Sch
'        txtActual1.Text = ActValue
'        lblMeetDate1.Text = MeetDate
'        lblStudentName1.Text = StuName
'        chkTardy1.Checked = Tardy

'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)
'        UserListDialog.VisibleOnPageLoad = True


'    ElseIf e.CommandName = "OpenEditFormTuesday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblTueDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtTueAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblTueSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkTueTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)

'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName = "OpenEditFormWednesday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblWedDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtWedAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblWedSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkWedTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)

'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName = "OpenEditFormThursday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblThurDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtThurAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblThurSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkThurTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName = "OpenEditFormFriday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblFriDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtFriAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblFriSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkFriTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName = "OpenEditFormSaturday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblSatDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtSatAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblSatSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkSatTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True

'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)
'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName.ToLower = "cancelchanges" Then
'        HideRadWindow()
'        ShowGrid()
'        BindCurrentWeek()
'        dgAttendance.Rebind()
'    End If
'End Sub
'Private Sub ConvertDatasetToAttendanceType()
'    Dim dtStudentDetails As DataTable
'    dtStudentDetails = ViewState("dtStudentDetails")

'    For Each row As DataRow In dtStudentDetails.Rows
'        If ViewState("AttendanceUnitType") = "CLOCK HOURS" Then
'            row("SunSch") = row("SunSch") / 60
'            row("MonSch") = row("MonSch") / 60
'            row("TueSch") = row("TueSch") / 60
'            row("WedSch") = row("WedSch") / 60
'            row("ThurSch") = row("ThurSch") / 60
'            row("FriSch") = row("FriSch") / 60
'            row("SatSch") = row("SatSch") / 60

'            If row("SunAct") <> "9999" Then
'                row("SunAct") = row("SunAct") / 60
'            End If
'            If row("MonAct") <> "9999" Then
'                row("MonAct") = row("MonAct") / 60
'            End If
'            If row("TueAct") <> "9999" Then
'                row("TueAct") = row("TueAct") / 60
'            End If
'            If row("WedAct") <> "9999" Then
'                row("WedAct") = row("WedAct") / 60
'            End If
'            If row("ThurAct") <> "9999" Then
'                row("ThurAct") = row("ThurAct") / 60
'            End If
'            If row("FriAct") <> "9999" Then
'                row("FriAct") = row("FriAct") / 60
'            End If
'            If row("SatAct") <> "9999" Then
'                row("SatAct") = row("SatAct") / 60
'            End If

'        Else

'            If row("SunSch") > 0 Then
'                row("SunSch") = 1
'            End If
'            If row("MonSch") > 0 Then
'                row("MonSch") = 1
'            End If
'            If row("TueSch") > 0 Then
'                row("TueSch") = 1
'            End If
'            If row("WedSch") > 0 Then
'                row("WedSch") = 1
'            End If
'            If row("ThurSch") > 0 Then
'                row("ThurSch") = 1
'            End If
'            If row("FriSch") > 0 Then
'                row("FriSch") = 1
'            End If
'            If row("SatSch") > 0 Then
'                row("SatSch") = 1
'            End If

'            If row("SunAct") = "P" Then
'                row("SunAct") = "1"
'            ElseIf row("SunAct") = "T" Then
'                row("SunAct") = "1"
'            ElseIf row("SunAct") = "A" Then
'                row("SunAct") = "0"
'            ElseIf row("SunAct") = "E" Then
'                row("SunAct") = "0"
'            End If

'            If row("MonAct") = "P" Then
'                row("MonAct") = "1"
'            ElseIf row("MonAct") = "T" Then
'                row("MonAct") = "1"
'            ElseIf row("MonAct") = "A" Then
'                row("MonAct") = "0"
'            ElseIf row("MonAct") = "E" Then
'                row("MonAct") = "0"
'            End If

'            If row("TueAct") = "P" Then
'                row("TueAct") = "1"
'            ElseIf row("TueAct") = "T" Then
'                row("TueAct") = "1"
'            ElseIf row("TueAct") = "A" Then
'                row("TueAct") = "0"
'            ElseIf row("TueAct") = "E" Then
'                row("TueAct") = "0"
'            End If

'            If row("WedAct") = "P" Then
'                row("WedAct") = "1"
'            ElseIf row("WedAct") = "T" Then
'                row("WedAct") = "1"
'            ElseIf row("WedAct") = "A" Then
'                row("WedAct") = "0"
'            ElseIf row("WedAct") = "E" Then
'                row("WedAct") = "0"
'            End If
'            If row("ThurAct") = "P" Then
'                row("ThurAct") = "1"
'            ElseIf row("ThurAct") = "T" Then
'                row("ThurAct") = "1"
'            ElseIf row("ThurAct") = "A" Then
'                row("ThurAct") = "0"
'            ElseIf row("ThurAct") = "E" Then
'                row("ThurAct") = "0"
'            End If
'            If row("FriAct") = "P" Then
'                row("FriAct") = "1"
'            ElseIf row("FriAct") = "T" Then
'                row("FriAct") = "1"
'            ElseIf row("FriAct") = "A" Then
'                row("FriAct") = "0"
'            ElseIf row("FriAct") = "E" Then
'                row("FriAct") = "0"
'            End If
'            If row("SatAct") = "P" Then
'                row("SatAct") = "1"
'            ElseIf row("SatAct") = "T" Then
'                row("SatAct") = "1"
'            ElseIf row("SatAct") = "A" Then
'                row("SatAct") = "0"
'            ElseIf row("SatAct") = "E" Then
'                row("SatAct") = "0"
'            End If

'        End If


'    Next




'End Sub
'Public Sub BindToolTipToThisControl(ByVal ctrl As ListControl)
'    For Each myitem As ListItem In ctrl.Items
'        myitem.Attributes.Add("title", myitem.Text)
'    Next
'End Sub
'Protected Sub ddlClasses_DataBound(sender As Object, e As System.EventArgs) Handles ddlClasses.DataBound
'    '  BindToolTipToThisControl(ddlClasses)
'End Sub

''To get the Complete details for the student
''Like BadgeId,ScheduleId,StartDate,GradDate,LoaDates,SuspDates and Holidays

'Private Sub btnExportToExcell_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcell.Click

'    Dim oStringWriter As System.IO.StringWriter = New System.IO.StringWriter
'    Dim oHtmlTextWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(oStringWriter)

'    'delete from the panel all controls that are not literals
'    HideAllTextBoxes(pnlAttendance)
'    ClearControls(pnlAttendance)

'    'render datagrid
'    pnlAttendance.RenderControl(oHtmlTextWriter)

'    'convert html to string
'    Dim enc As System.Text.Encoding = System.Text.Encoding.UTF8
'    Dim s As String = oStringWriter.ToString

'    'move html content to a memorystream
'    Dim documentMemoryStream As New System.IO.MemoryStream
'    documentMemoryStream.Write(enc.GetBytes(s), 0, s.Length)

'    '   save document and document type in sessions variables
'    Session("DocumentMemoryStream") = documentMemoryStream
'    Session("ContentType") = "application/vnd.ms-excel"
'    'pnlAttendance.Visible = False
'    'dgStudents.Visible = False
'    '  BuildAttendanceGrid()
'    '  PopulateAttendanceGrid()
'    '   Register a javascript to open the report in another window
'    Dim javascript As String = "<script>var origwindow=window.self;window.open('../sa/PrintAnyReport.aspx');</script>"
'    '  Page.RegisterClientScriptBlock("NewWindow", javascript)
'    ClientScript.RegisterClientScriptBlock(Me.[GetType](), "NewWindow", javascript)

'End Sub
''Parameter Dtm is added to this code to find the duration of the hours for a given meetdate
''Modified by Saraswathi lakshmanan on Jan 12 2009
''Added by Saraswathi Lakshmanan to show the cohort Start Date on the LHS of the Screen to buuild the list based on cohortstdate and class sections 
''On Oct - 3-2008
''Added by Saraswathi lakshmanan on August 24 2009
''To find the list controls and add a tool tip to those items in the control
''list controls include drop down list, list box, group checkbox, etc.
'Public Sub BIndToolTip()
'    Dim i As Integer
'    Dim ctl As Control
'    For Each ctl In Page.Form.Controls
'        If TypeOf ctl Is ListControl Then
'            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
'                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
'            Next
'        End If
'        If TypeOf ctl Is Panel Then
'            BindToolTipForControlsInsideaPanel(ctl)
'        End If
'        If TypeOf ctl Is DataGrid Then
'            BindToolTipForControlsInsideaGrid(ctl)
'        End If
'    Next
'End Sub
'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
'    Dim ctrl As Control
'    Dim j As Integer
'    For Each ctrl In Ctrlpanel.Controls
'        If TypeOf ctrl Is ListControl Then
'            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
'                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
'            Next
'        ElseIf TypeOf ctrl Is Panel Then
'            BindToolTipForControlsInsideaPanel(ctrl)
'        ElseIf TypeOf ctrl Is DataGrid Then
'            BindToolTipForControlsInsideaGrid(ctrl)
'        End If
'    Next

'End Sub

'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
'    Dim j As Integer
'    Dim itm As DataGridItem
'    Dim ctrl As Control
'    Dim ctrl1 As Control

'    For Each itm In CtrlGrid.Items
'        For Each ctrl In itm.Controls
'            For Each ctrl1 In ctrl.Controls
'                If TypeOf ctrl1 Is ListControl Then
'                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
'                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
'                    Next
'                End If
'            Next
'        Next
'    Next
'End Sub

'Protected Sub ddlClsSectmeeting_DataBound(sender As Object, e As System.EventArgs) Handles ddlClsSectmeeting.DataBound
'    BindToolTipToThisControl(ddlClsSectmeeting)
'End Sub
'Protected Sub ddlcohortStDate_DataBound(sender As Object, e As System.EventArgs) Handles ddlcohortStDate.DataBound
'    BindToolTipToThisControl(ddlcohortStDate)
'End Sub
'Private Function TranslatePALetterToQuantity(ByVal letterValue As String) As Decimal
'    Select Case letterValue.ToUpper
'        Case "A", "E"
'            Return 0
'        Case "P", "T"
'            Return 1
'    End Select

'End Function

'Private Function TranslatePALetterToTardyBoolean(ByVal letterValue As String) As Boolean
'    If letterValue.ToUpper = "T" Then
'        Return True
'    Else
'        Return False
'    End If
'End Function

'Private Function TranslatePALetterToExcusedBoolean(ByVal letterValue As String) As Boolean
'    If letterValue.ToUpper = "E" Then
'        Return True
'    Else
'        Return False
'    End If
'End Function


'Private Sub PopulateAttendanceGrid()
'    Dim rowCounter As Integer
'    Dim cellCounter As Integer
'    Dim arrRows() As DataRow
'    Dim stuId As String
'    Dim dtm As DateTime
'    Dim tbl As New Table
'    Dim facAttendance As New ClsSectAttendanceFacade

'    'We start at the second row since the first row is the header
'    tbl = DirectCast(pnlAttendance.Controls(0), Table)
'    For rowCounter = 1 To tbl.Rows.Count - 1
'        stuId = DirectCast(Session("Students"), DataTable).Rows(rowCounter - 1)("StuEnrollId").ToString()
'        'Start at the third cell because the first stores the names of students and the second
'        'stores the status
'        For cellCounter = 3 To DirectCast(Session("Dates"), ArrayList).Count + 3 - 1
'            dtm = DirectCast(Session("Dates"), ArrayList).Item(cellCounter - 3)
'            'If we are dealing with post by exception then we are dealing with unposted days and so we
'            'we do not need to populate the table from the actual attendance records for the class section.
'            'In this case if we are dealing with a present/absent system we will need to populate the table
'            'with P. If we are dealing with a minutes system then we should populate the table with the 
'            'scheduled minutes of the class section.

'            'If chkPostByException.Checked = False Then
'            'Check the ActualAttendance dt in session to see if this student has attendance
'            'posted for this day. If so we want to fill the textbox with the amount of hours
'            'actually attended.
'            arrRows = DirectCast(Session("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuId & "'" & " AND MeetDate = #" & dtm & "#")

'            If arrRows.Length > 0 Then
'                If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = arrRows(0)("Actual").ToString()
'                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(1), CheckBox).Checked = arrRows(0)("Tardy")
'                ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = (arrRows(0)("Actual") / 60).ToString()
'                    DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(1), CheckBox).Checked = arrRows(0)("Tardy")
'                Else
'                    'Present/Absent system so we need to translate the 0 and 1 to "A" and "P" respectively.
'                    'If the record is marked as tardy we will display a "T" instead of a "P".
'                    'If the record is marked as excused we will display an "E" instead of "A"
'                    If arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") = True Then
'                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "E"
'                    ElseIf arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") <> True Then
'                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "A"
'                    ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") = True Then
'                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "T"
'                    ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") <> True Then
'                        DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
'                    End If

'                End If

'                'End If
'            Else
'                If chkPostByException.Checked = True Then
'                    'We are dealing with post by exception.
'                    Dim popAtt As Boolean
'                    popAtt = True

'                    'We should only populate the attendance with the default if the following conditions are met:
'                    '(1)The student is not on LOA
'                    '(2)The student is not a drop
'                    '(3)The start date is not null
'                    '(4)The start date is on or before the meeting date being examined

'                    If facAttendance.IsStudentOnLOA(stuId, dtm, DirectCast(Session("StudentsLOAInfo"), DataTable)) Then
'                        popAtt = False
'                    Else
'                        Dim dRows As DataRow()
'                        dRows = DirectCast(Session("Students"), DataTable).Select("StuEnrollId = '" & stuId & "'")
'                        If dRows(0)("SysStatusId") = 12 Then
'                            If Not dRows(0).IsNull("DateDetermined") Then
'                                If dRows(0)("DateDetermined") <= dtm Then
'                                    popAtt = False
'                                Else
'                                    popAtt = False
'                                End If
'                            End If
'                        End If

'                        If dRows(0).IsNull("StartDate") Then
'                            popAtt = False
'                        Else
'                            'If the student has a start date that is after the meet date being examined then we
'                            'should not allow attendance to be posted for it.
'                            If dtm < dRows(0)("StartDate") Then
'                                popAtt = False
'                            End If
'                        End If

'                    End If

'                    'If the date being examined is greater than today's date then we should not allow
'                    'attendance to be posted for it.
'                    If dtm > Date.Now Then
'                        popAtt = False
'                    End If


'                    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'                        'We need to get the scheduled minutes for this meet day.

'                        If popAtt Then
'                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = GetScheduledMinutesForMeetDay(dtm)
'                        End If
'                    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'                        If popAtt Then
'                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = GetScheduledMinutesForMeetDay(dtm) / 60
'                        End If

'                    Else
'                        'Present/Absent system so we can simply fill each textbox with a P.
'                        If popAtt Then
'                            DirectCast(tbl.Rows(rowCounter).Cells(cellCounter).Controls(0), TextBox).Text = "P"
'                        End If

'                    End If
'                End If
'            End If
'        Next
'    Next

'    'If the dates array is empty and post by exception is checked then we want to display a message
'    'to let the user know that there are no empty days for the date range selected.
'    If chkPostByException.Checked = True Then
'        If DirectCast(Session("Dates"), ArrayList).Count = 0 Then
'            DisplayErrorMessage("There are no empty days for the date range you specified. Please uncheck post by exception before editing.")
'        End If
'    End If

'End Sub

'Private Function IsMeetDay(ByVal dtm As DateTime) As Boolean
'    Dim facAttendance As New ClsSectAttendanceFacade
'    Dim sDay As String
'    Dim dtDays As New DataTable
'    Dim dr As DataRow

'    'Get the short day value
'    sDay = facAttendance.GetShortDayName(dtm)

'    'Loop through the days that the class section meets and see if this day matches one
'    dtDays = DirectCast(Session("MeetDays"), DataTable)

'    If dtDays.Rows.Count = 0 Then
'        Return False
'    Else
'        For Each dr In dtDays.Rows
'            If dr("WorkDaysDescrip").ToString() = sDay Then
'                Return True
'            End If
'        Next
'        Return False
'    End If
'End Function

'Private Function GetMeetDatesArrayListFromRange(ByVal startDate As String, ByVal endDate As String) As ArrayList
'    Dim arrDates As New ArrayList
'    Dim dtm As DateTime
'    Dim counter As Integer
'    Dim facAttendance As New ClsSectAttendanceFacade


'    'Add the start date to the arraylist if it is a meet day. If post by exception is selected
'    'then we will only add the date if it is unposted.
'    dtm = Convert.ToDateTime(startDate)

'    If IsMeetDay(dtm) Then
'        If chkPostByException.Checked = True Then
'            If facAttendance.IsMeetDateUnposted(ddlClasses.SelectedItem.Value, dtm) Then
'                arrDates.Add(dtm)
'            End If
'        Else
'            arrDates.Add(dtm)
'        End If
'    End If

'    If Convert.ToDateTime(startDate) <> Convert.ToDateTime(endDate) Then
'        'Use a loop to determine the other dates to be added.
'        'We will only add a date if it is a meet date. In addition, if post by exception is
'        'selected we will only add unposted days - days for which there are no attendance records.
'        For counter = 1 To 365
'            dtm = dtm.AddDays(1)
'            If IsMeetDay(dtm) Then
'                If chkPostByException.Checked = True Then
'                    If facAttendance.IsMeetDateUnposted(ddlClasses.SelectedItem.Value, dtm) Then
'                        arrDates.Add(dtm)
'                    End If
'                Else
'                    arrDates.Add(dtm)
'                End If

'            End If

'            If dtm = Convert.ToDateTime(endDate) Then
'                Exit For
'            End If
'        Next
'    End If

'    Return arrDates
'End Function

'Private Function GetStartTimeForMeetDate(ByVal dtm) As String
'    Dim facAttendance As New ClsSectAttendanceFacade
'    Dim sDay As String
'    Dim dtDays As New DataTable
'    Dim dr As DataRow

'    'Get the short day value
'    sDay = facAttendance.GetShortDayName(dtm)

'    'Loop through the days that the class section meets and see which one this day matches one
'    dtDays = DirectCast(Session("MeetDays"), DataTable)
'    For Each dr In dtDays.Rows
'        If dr("WorkDaysDescrip").ToString() = sDay Then
'            Return dr("TimeIntervalDescrip")
'        End If
'    Next
'End Function

'Protected Sub ddlTerms_DataBound(sender As Object, e As System.EventArgs) Handles ddlTerms.DataBound
'    BindToolTipToThisControl(ddlTerms)
'End Sub

'Protected Sub dgAttendance_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles dgAttendance.ItemCreated
'    'If TypeOf e.Item Is GridDataItem Then
'    '    Dim editLink As ImageButton = DirectCast(e.Item.FindControl("imgbtnSunday"), ImageButton)
'    '    '    editLink.Attributes("href") = "#"
'    '    'editLink.Attributes("onclick") = [String].Format("return EditSunday('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("StuEnrollId"), e.Item.ItemIndex)

'    '    'Dim Cell As TableCell
'    '    'Cell = e.Item.Cells("StudentName")



'    '    'Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'    '    Dim str As String = "test"
'    '    '    str = Cell.Controls(1).ToString

'    '    Dim SunAct As String = DirectCast(e.Item.FindControl("txtSunAct"), TextBox).Text

'    '    editLink.Attributes("onclick") = [String].Format("return EditSunday('{0}','{1}');", str, SunAct)


'    'End If

'    If e.Item.ItemType = GridItemType.AlternatingItem Or e.Item.ItemType = GridItemType.Item Then
'        Dim item As GridDataItem
'        item = e.Item
'        Dim labelStuName As Label
'        labelStuName = item("StudentName").FindControl("lblStudentName")

'    End If


'End Sub   

'Protected Sub dgAttendance_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles dgAttendance.ItemCommand

'    Dim clsSectionId As String
'    Dim ClsSectMeetingId As String
'    clsSectionId = ddlClasses.SelectedItem.Value
'    ClsSectMeetingId = ddlClsSectmeeting.SelectedItem.Value


'    If e.CommandName.ToString.ToLower = "savechanges" Then


'        If Session("ClsSectionUsesTimeClock") = True Then
'            Exit Sub
'        End If

'        HideRadWindow()

'        ShowGrid()

'        ''    Dim dtStart As System.DateTime = DateTime.Parse(Format(curDate, "MM/dd/yyyy") + " " + StartTime)
'        Dim fac As New FAME.AdvantageV1.BusinessFacade.TimeClock.TimeClockFacade
'        Dim StartTime As String
'        StartTime = fac.GetStartTimeForClassSectionMeeting_byClass(ddlClsSectmeeting.SelectedItem.Value, campusId)


'        Dim dtStudentDetails As DataTable
'        dtStudentDetails = ViewState("dtStudentDetails")
'        Dim FromDate As Date
'        FromDate = ViewState("FromDate")
'        Dim dtUpdatedAttendanceRows As DataTable
'        Dim res As String

'        dtUpdatedAttendanceRows = CreateclsSectAttendanceTable()

'        Dim updatedRows() As DataRow

'        For Each item As GridItem In dgAttendance.MasterTableView.Items
'            If TypeOf item Is GridDataItem Then
'                Dim row As DataRow


'                Dim lblStudentName As Label = DirectCast(item.FindControl("lblStudentName"), Label)
'                Dim str As String
'                str = lblStudentName.Text
'                Dim itemStuEnrollID As GridDataItem = CType(item, GridDataItem)
'                Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text

'                updatedRows = dtStudentDetails.Select("StuEnrollId='" + itemStuEnrollIDValue + "' And SunDate=#" + FromDate + "#")

'                For Each updateddr In updatedRows

'                    For i As Integer = 0 To 6
'                        row = dtUpdatedAttendanceRows.NewRow()
'                        row("StuEnrollId") = itemStuEnrollIDValue
'                        row("ClsSectionID") = ddlClasses.SelectedItem.Value
'                        row("ClsSectMeetingID") = ddlClsSectmeeting.SelectedItem.Value

'                        Select Case i
'                            Case 0

'                                row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("SunDate")), "MM/dd/yyyy") + " " + StartTime)
'                                row("Scheduled") = updateddr("SunSch").ToString
'                                If DirectCast(item.FindControl("txtSunAct"), TextBox).Text = "" Then
'                                    row("Actual") = "9999"
'                                Else
'                                    row("Actual") = DirectCast(item.FindControl("txtSunAct"), TextBox).Text
'                                    updateddr("SunAct") = DirectCast(item.FindControl("txtSunAct"), TextBox).Text
'                                End If
'                                row("Tardy") = DirectCast(item.FindControl("chkSunTardy"), CheckBox).Checked
'                                updateddr("SunTardy") = DirectCast(item.FindControl("chkSunTardy"), CheckBox).Checked
'                                row("Excused") = False

'                            Case 1
'                                row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("MonDate")), "MM/dd/yyyy") + " " + StartTime)
'                                row("Scheduled") = updateddr("MonSch").ToString
'                                If DirectCast(item.FindControl("txtMonAct"), TextBox).Text = "" Then
'                                    row("Actual") = "9999"
'                                Else
'                                    row("Actual") = DirectCast(item.FindControl("txtMonAct"), TextBox).Text
'                                    updateddr("MonAct") = DirectCast(item.FindControl("txtMonAct"), TextBox).Text
'                                End If
'                                row("Tardy") = DirectCast(item.FindControl("chkMonTardy"), CheckBox).Checked
'                                updateddr("MonTardy") = DirectCast(item.FindControl("chkMonTardy"), CheckBox).Checked
'                                row("Excused") = False
'                            Case 2
'                                row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("TueDate")), "MM/dd/yyyy") + " " + StartTime)
'                                row("Scheduled") = updateddr("TueSch").ToString
'                                If DirectCast(item.FindControl("txtTueAct"), TextBox).Text = "" Then
'                                    row("Actual") = "9999"
'                                Else
'                                    row("Actual") = DirectCast(item.FindControl("txtTueAct"), TextBox).Text
'                                    updateddr("TueAct") = DirectCast(item.FindControl("txtTueAct"), TextBox).Text
'                                End If
'                                row("Tardy") = DirectCast(item.FindControl("chkTueTardy"), CheckBox).Checked
'                                updateddr("TueTardy") = DirectCast(item.FindControl("chkTueTardy"), CheckBox).Checked
'                                row("Excused") = False
'                            Case 3
'                                row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("WedDate")), "MM/dd/yyyy") + " " + StartTime)
'                                row("Scheduled") = updateddr("WedSch").ToString
'                                If DirectCast(item.FindControl("txtWedAct"), TextBox).Text = "" Then
'                                    row("Actual") = "9999"
'                                Else
'                                    row("Actual") = DirectCast(item.FindControl("txtWedAct"), TextBox).Text
'                                    updateddr("WedAct") = DirectCast(item.FindControl("txtWedAct"), TextBox).Text
'                                End If
'                                row("Tardy") = DirectCast(item.FindControl("chkWedTardy"), CheckBox).Checked
'                                updateddr("WedTardy") = DirectCast(item.FindControl("chkWedTardy"), CheckBox).Checked
'                                row("Excused") = False
'                            Case 4
'                                row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("ThurDate")), "MM/dd/yyyy") + " " + StartTime)
'                                row("Scheduled") = updateddr("ThurSch").ToString
'                                If DirectCast(item.FindControl("txtThurAct"), TextBox).Text = "" Then
'                                    row("Actual") = "9999"
'                                Else
'                                    row("Actual") = DirectCast(item.FindControl("txtThurAct"), TextBox).Text
'                                    updateddr("ThurAct") = DirectCast(item.FindControl("txtThurAct"), TextBox).Text
'                                End If
'                                row("Tardy") = DirectCast(item.FindControl("chkThurTardy"), CheckBox).Checked
'                                updateddr("ThurTardy") = DirectCast(item.FindControl("chkThurTardy"), CheckBox).Checked
'                                row("Excused") = False
'                            Case 5
'                                row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("FriDate")), "MM/dd/yyyy") + " " + StartTime)
'                                row("Scheduled") = updateddr("FriSch").ToString
'                                If DirectCast(item.FindControl("txtFriAct"), TextBox).Text = "" Then
'                                    row("Actual") = "9999"
'                                Else
'                                    row("Actual") = DirectCast(item.FindControl("txtFriAct"), TextBox).Text
'                                    updateddr("FriAct") = DirectCast(item.FindControl("txtFriAct"), TextBox).Text
'                                End If
'                                row("Tardy") = DirectCast(item.FindControl("chkFriTardy"), CheckBox).Checked
'                                updateddr("FriTardy") = DirectCast(item.FindControl("chkFriTardy"), CheckBox).Checked
'                                row("Excused") = False
'                            Case 6
'                                row("MeetDate") = DateTime.Parse(Format(CDate(updateddr("SatDate")), "MM/dd/yyyy") + " " + StartTime)
'                                row("Scheduled") = updateddr("SatSch").ToString
'                                If DirectCast(item.FindControl("txtSatAct"), TextBox).Text = "" Then
'                                    row("Actual") = "9999"
'                                Else
'                                    row("Actual") = DirectCast(item.FindControl("txtSatAct"), TextBox).Text
'                                    updateddr("SatAct") = DirectCast(item.FindControl("txtSatAct"), TextBox).Text
'                                End If
'                                row("Tardy") = DirectCast(item.FindControl("chkSatTardy"), CheckBox).Checked
'                                updateddr("SatTardy") = DirectCast(item.FindControl("chkSatTardy"), CheckBox).Checked
'                                row("Excused") = False

'                        End Select
'                        dtUpdatedAttendanceRows.Rows.Add(row)
'                        dtUpdatedAttendanceRows.AcceptChanges()
'                    Next

'                Next

'            End If
'        Next

'        If dtUpdatedAttendanceRows.Rows.Count > 0 Then

'            dtUpdatedAttendanceRows = ConvertActualAttendanceBasedonUnitType(dtUpdatedAttendanceRows)
'            Dim dtnewDatatableforSaving As New DataTable
'            dtnewDatatableforSaving = dtUpdatedAttendanceRows.Clone
'            For Each item In dtUpdatedAttendanceRows.Rows
'                dtnewDatatableforSaving.ImportRow(item)
'            Next
'            Dim dsFinalDataset As New DataSet
'            dsFinalDataset.Tables.Add(dtnewDatatableforSaving)
'            res = AddClsSectAttendance(dsFinalDataset)

'        End If
'        RefreshGrid()
'        ' BindCurrentWeek()
'        'dgAttendance.DataBind()

'    ElseIf e.CommandName = "OpenEditFormSunday" Then

'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text

'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblSunDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text

'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtSunAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblSunSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkSunTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked

'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)

'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")

'        UserListDialog.VisibleOnPageLoad = True


'    ElseIf e.CommandName = "OpenEditFormMonday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblMonDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtMonAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblMonSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkMonTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked

'        lblClsSmeeting1.Text = ClsSectMeeting
'        txtScheduled1.Text = Sch
'        txtActual1.Text = ActValue
'        lblMeetDate1.Text = MeetDate
'        lblStudentName1.Text = StuName
'        chkTardy1.Checked = Tardy

'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)
'        UserListDialog.VisibleOnPageLoad = True


'    ElseIf e.CommandName = "OpenEditFormTuesday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblTueDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtTueAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblTueSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkTueTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)

'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName = "OpenEditFormWednesday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblWedDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtWedAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblWedSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkWedTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)

'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName = "OpenEditFormThursday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblThurDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtThurAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblThurSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkThurTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName = "OpenEditFormFriday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblFriDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtFriAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblFriSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkFriTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True
'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName = "OpenEditFormSaturday" Then
'        Dim itemStuEnrollID As GridDataItem = CType(e.Item, GridDataItem)
'        Dim itemStuEnrollIDValue As String = itemStuEnrollID("StuEnrollID").Text
'        Dim lblStudentName As Label = DirectCast(e.Item.FindControl("lblStudentName"), Label)
'        Dim StuName As String
'        StuName = lblStudentName.Text
'        Dim lblMeetDate As Label = DirectCast(e.Item.FindControl("lblSatDate"), Label)
'        Dim MeetDate As String
'        MeetDate = lblMeetDate.Text
'        Dim TxtAct As TextBox = DirectCast(e.Item.FindControl("txtSatAct"), TextBox)
'        Dim ActValue As String
'        ActValue = TxtAct.Text
'        Dim lblSch As Label = DirectCast(e.Item.FindControl("lblSatSch"), Label)
'        Dim Sch As String
'        Sch = lblSch.Text
'        Dim ClsSectMeeting As String
'        ClsSectMeeting = ddlClsSectmeeting.SelectedItem.Text
'        Dim chkTardy As CheckBox = DirectCast(e.Item.FindControl("chkSatTardy"), CheckBox)
'        Dim Tardy As Boolean
'        Tardy = chkTardy.Checked
'        'RadWindowManager1.Windows(0).NavigateUrl = "EditClsSectAttendanceRadWindow.aspx?&StudentName=" + StuName + "&StuEnrollId=" + itemStuEnrollIDValue + "&ClsSectMeeting=" + ClsSectMeeting + "&Scheduled=" + Sch + "&Actual=" + ActValue + "&Tardy=" + Tardy.ToString + "&MeetDate=" + MeetDate + "&clsSectionId=" + clsSectionId + "&clsSectMeetingId=" + ClsSectMeetingId + "&AttendanceUnitType=" + ViewState("AttendanceUnitType")
'        'RadWindowManager1.Windows(0).VisibleOnPageLoad = True

'        AssignvaluestoRadUselistWindow(itemStuEnrollIDValue, ClsSectMeeting, Sch, ActValue, MeetDate, StuName, Tardy)
'        UserListDialog.VisibleOnPageLoad = True
'    ElseIf e.CommandName.ToLower = "cancelchanges" Then
'        HideRadWindow()
'        ShowGrid()
'        BindCurrentWeek()
'        dgAttendance.Rebind()
'    End If
'End Sub

'Private Sub PopulateStudentAttendanceTableforGrid()

'    Dim AttUnitType As String
'    If ViewState("AttendanceUnitType").ToString.ToUpper = "MINUTES" Then
'        AttUnitType = "min"
'    ElseIf ViewState("AttendanceUnitType").ToString.ToUpper = "CLOCK HOURS" Then
'        AttUnitType = "hrs"
'    Else
'        AttUnitType = "P/A"
'    End If



'    Dim drActuals As DataRow()
'    ' Dim drMeetings As DataRow()
'    Dim dtStudentDetails As DataTable
'    Dim dtMeetDays As New DataTable
'    Dim dtActualAttendace As New DataTable
'    Dim DictDates As New Dictionary(Of Date, Date)

'    dtStudentDetails = CreateStudentAttendanceDetailsTable()
'    Dim FromDate As Date = Date.Now
'    Dim ToDate As Date = Date.Now
'    FromDate = CDate(txtStart.Text)
'    ToDate = IIf(CDate(txtEnd.Text) > Date.Now, Date.Now, CDate(txtEnd.Text))
'    Dim ProcessDate As Date
'    Dim strprocessDate As String
'    Dim dtStudents As DataTable
'    dtStudents = DirectCast(Session("Students"), DataTable)
'    'dtMeetDays = DirectCast(Session("MeetDays"), DataTable)
'    'dtActualAttendace = DirectCast(Session("ActualAttendance"), DataTable)
'    DictDates = DirectCast(Session("DictionaryofDates"), Dictionary(Of Date, Date))
'    Dim row As DataRow
'    Dim facInputMasks As New InputMasksFacade
'    '  Dim Query As System.Data.EnumerableRowCollection(Of <anonymous type> )


'    dtMeetDays = BuildMeetingDatesDataTable(AttUnitType)
'    dtActualAttendace = BuildStudentActualAttendanceDatatable(AttUnitType)

'    For i = 0 To dtStudents.Rows.Count - 1
'        ProcessDate = FromDate

'        Select Case ProcessDate.DayOfWeek
'            Case DayOfWeek.Sunday
'                ProcessDate = ProcessDate
'            Case DayOfWeek.Monday
'                ProcessDate = ProcessDate.AddDays(-1)
'            Case DayOfWeek.Tuesday
'                ProcessDate = ProcessDate.AddDays(-2)
'            Case DayOfWeek.Wednesday
'                ProcessDate = ProcessDate.AddDays(-3)
'            Case DayOfWeek.Thursday
'                ProcessDate = ProcessDate.AddDays(-4)
'            Case DayOfWeek.Friday
'                ProcessDate = ProcessDate.AddDays(-5)
'            Case DayOfWeek.Saturday
'                ProcessDate = ProcessDate.AddDays(-6)
'        End Select
'        FromDate = ProcessDate

'        Select Case ToDate.DayOfWeek
'            Case DayOfWeek.Sunday
'                ToDate = ToDate.AddDays(6)
'            Case DayOfWeek.Monday
'                ToDate = ToDate.AddDays(5)
'            Case DayOfWeek.Tuesday
'                ToDate = ToDate.AddDays(4)
'            Case DayOfWeek.Wednesday
'                ToDate = ToDate.AddDays(3)
'            Case DayOfWeek.Thursday
'                ToDate = ToDate.AddDays(2)
'            Case DayOfWeek.Friday
'                ToDate = ToDate.AddDays(1)
'            Case DayOfWeek.Saturday
'                ToDate = ToDate
'        End Select


'        strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

'        While (ProcessDate <= ToDate)

'            If ProcessDate.DayOfWeek = DayOfWeek.Sunday Then
'                row = dtStudentDetails.NewRow()
'            End If

'            row("StuEnrollid") = dtStudents.Rows(i)("StuEnrollID").ToString
'            row("StudentName") = dtStudents.Rows(i)("LastName").ToString + ", " + dtStudents.Rows(i)("FirstName").ToString
'            row("SysStatusId") = dtStudents.Rows(i)("SysStatusId").ToString
'            row("StatusDescrip") = dtStudents.Rows(i)("StatusCodeDescrip").ToString
'            row("DateDetermined") = dtStudents.Rows(i)("DateDetermined").ToString
'            row("PrgVerDescrip") = dtStudents.Rows(i)("PrgVerDescrip").ToString
'            If dtStudents.Rows(i)("UseTimeClock") = False Then
'                row("UseTimeClock") = "No"
'            Else
'                row("UseTimeClock") = "Yes"
'            End If
'            If Not dtStudents.Rows(i)("SSN") Is DBNull.Value Then
'                row("SSN") = "***-**-" + dtStudents.Rows(i)("SSN").ToString.Substring(dtStudents.Rows(i)("SSN").ToString.Length - 4, 4)
'            End If

'            row("StudentStartDate") = Format(dtStudents.Rows(i)("StartDate"), "MM-dd-yyyy")

'            Dim drCurrentWorkDaySchedule As DataRow()
'            drCurrentWorkDaySchedule = dtMeetDays.Select("WorkdaysDescrip='" + ProcessDate.DayOfWeek.ToString.Substring(0, 3) + "'")
'            Dim TimeofMeeting As TimeSpan
'            If drCurrentWorkDaySchedule.Length >= 1 Then
'                TimeofMeeting = CDate(drCurrentWorkDaySchedule(0)("TimeIntervalDescrip")).TimeOfDay
'            Else
'                TimeofMeeting = TimeSpan.Zero
'            End If

'            Dim isHoliday As Boolean = False
'            If IsDayEditable(ProcessDate, dtStudents.Rows(i)("StuEnrollID").ToString, dtStudents.Rows(i)("SysStatusId").ToString, dtStudents.Rows(i)("DateDetermined").ToString, Format(dtStudents.Rows(i)("StartDate"), "MM-dd-yyyy")) = "Holiday" Then
'                isHoliday = True

'            End If


'            If DictDates.ContainsKey(ProcessDate + TimeofMeeting) Then
'                drActuals = dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And MeetDate='" + ProcessDate.ToShortDateString.ToString + " " + TimeofMeeting.ToString + "'")

'                If drActuals.Length = 0 Then
'                    drActuals = dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And MeetDate='" + ProcessDate.ToShortDateString.ToString + "'")
'                End If


'                If ProcessDate.DayOfWeek = DayOfWeek.Monday Then
'                    row("MonDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("MonSch") = drActuals(i)("Scheduled")
'                        row("MonAct") = drActuals(i)("Actual")
'                        row("MonTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("MonSch") = 0
'                        Else
'                            row("MonSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("MonAct") = " "
'                        row("MonTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Tuesday Then
'                    row("TueDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("TueSch") = drActuals(i)("Scheduled")
'                        row("TueAct") = drActuals(i)("Actual")
'                        row("TueTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("TueSch") = 0
'                        Else
'                            row("TueSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("TueAct") = " "
'                        row("TueTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Wednesday Then
'                    row("WedDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("WedSch") = drActuals(i)("Scheduled")
'                        row("WedAct") = drActuals(i)("Actual")
'                        row("WedTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("WedSch") = 0
'                        Else
'                            row("WedSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("WedAct") = " "
'                        row("WedTardy") = " "

'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Thursday Then
'                    row("ThurDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("ThurSch") = drActuals(i)("Scheduled")
'                        row("ThurAct") = drActuals(i)("Actual")
'                        row("ThurTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("ThurSch") = 0
'                        Else
'                            row("ThurSch") = dtMeetDays.Rows(0)("Duration")
'                        End If
'                        row("ThurAct") = " "
'                        row("ThurTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Friday Then
'                    row("FriDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("FriSch") = drActuals(i)("Scheduled")
'                        row("FriAct") = drActuals(i)("Actual")
'                        row("FriTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("FriSch") = 0
'                        Else
'                            row("FriSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("FriAct") = " "
'                        row("FriTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Saturday Then
'                    row("SatDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("SatSch") = drActuals(i)("Scheduled")
'                        row("SatAct") = drActuals(i)("Actual")
'                        row("SatTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("SatSch") = 0
'                        Else
'                            row("SatSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("SatAct") = " "
'                        row("SatTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Sunday Then
'                    row("SunDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("SunSch") = drActuals(i)("Scheduled")
'                        row("SunAct") = drActuals(i)("Actual")
'                        row("SunTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("SunSch") = 0
'                        Else
'                            row("SunSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("SunAct") = " "
'                        row("SunTardy") = " "
'                    End If
'                End If

'                'ElseIf dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And MeetDate='" + ProcessDate.ToShortDateString.ToString + "'").Length >= 1 Then
'            ElseIf dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And MeetDate>='" + ProcessDate.ToString + "' And MeetDate<='" + ProcessDate.AddDays(1).ToString + "'").Length >= 1 Then
'                drActuals = dtActualAttendace.Select("StuEnrollId='" + dtStudents.Rows(i)("StuEnrollId").ToString + "' And  MeetDate>='" + ProcessDate.ToString + "' And MeetDate<='" + ProcessDate.AddDays(1).ToString + "'")

'                If ProcessDate.DayOfWeek = DayOfWeek.Monday Then
'                    row("MonDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("MonSch") = drActuals(i)("Scheduled")
'                        row("MonAct") = drActuals(i)("Actual")
'                        row("MonTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("MonSch") = 0
'                        Else
'                            row("MonSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("MonAct") = " "
'                        row("MonTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Tuesday Then
'                    row("TueDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("TueSch") = drActuals(i)("Scheduled")
'                        row("TueAct") = drActuals(i)("Actual")
'                        row("TueTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("TueSch") = 0
'                        Else
'                            row("TueSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("TueAct") = " "
'                        row("TueTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Wednesday Then
'                    row("WedDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("WedSch") = drActuals(i)("Scheduled")
'                        row("WedAct") = drActuals(i)("Actual")
'                        row("WedTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("WedSch") = 0
'                        Else
'                            row("WedSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("WedAct") = " "
'                        row("WedTardy") = " "

'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Thursday Then
'                    row("ThurDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("ThurSch") = drActuals(i)("Scheduled")
'                        row("ThurAct") = drActuals(i)("Actual")
'                        row("ThurTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("ThurSch") = 0
'                        Else
'                            row("ThurSch") = dtMeetDays.Rows(0)("Duration")
'                        End If
'                        row("ThurAct") = " "
'                        row("ThurTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Friday Then
'                    row("FriDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("FriSch") = drActuals(i)("Scheduled")
'                        row("FriAct") = drActuals(i)("Actual")
'                        row("FriTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("FriSch") = 0
'                        Else
'                            row("FriSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("FriAct") = " "
'                        row("FriTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Saturday Then
'                    row("SatDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("SatSch") = drActuals(i)("Scheduled")
'                        row("SatAct") = drActuals(i)("Actual")
'                        row("SatTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("SatSch") = 0
'                        Else
'                            row("SatSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("SatAct") = " "
'                        row("SatTardy") = " "
'                    End If
'                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Sunday Then
'                    row("SunDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                    If drActuals.Length = 1 Then
'                        row("SunSch") = drActuals(i)("Scheduled")
'                        row("SunAct") = drActuals(i)("Actual")
'                        row("SunTardy") = drActuals(i)("Tardy")
'                    Else
'                        If isHoliday = True Then
'                            row("SunSch") = 0
'                        Else
'                            row("SunSch") = dtMeetDays.Rows(0)("Duration")
'                        End If

'                        row("SunAct") = " "
'                        row("SunTardy") = " "
'                    End If
'                End If

'            Else

'                Select Case ProcessDate.DayOfWeek
'                    Case DayOfWeek.Sunday
'                        row("SunDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                        If row("SunTardy") Is DBNull.Value Then
'                            row("SunTardy") = ""
'                        End If

'                    Case DayOfWeek.Monday
'                        row("MonDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                        If row("MonTardy") Is DBNull.Value Then
'                            row("MonTardy") = ""
'                        End If
'                    Case DayOfWeek.Tuesday
'                        row("TueDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                        If row("TueTardy") Is DBNull.Value Then
'                            row("TueTardy") = ""
'                        End If
'                    Case DayOfWeek.Wednesday
'                        row("WedDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                        If row("WedTardy") Is DBNull.Value Then
'                            row("WedTardy") = ""
'                        End If
'                    Case DayOfWeek.Thursday
'                        row("ThurDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                        If row("ThurTardy") Is DBNull.Value Then
'                            row("ThurTardy") = ""
'                        End If
'                    Case DayOfWeek.Friday
'                        row("FriDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                        If row("FriTardy") Is DBNull.Value Then
'                            row("FriTardy") = ""
'                        End If
'                    Case DayOfWeek.Saturday
'                        row("SatDate") = Format(ProcessDate, "MM/dd/yyyy") + " " + TimeofMeeting.ToString
'                        If row("SatTardy") Is DBNull.Value Then
'                            row("SatTardy") = ""
'                        End If

'                End Select


'            End If



'            If ProcessDate.DayOfWeek = DayOfWeek.Saturday Then
'                If row("MonSch") Is System.DBNull.Value Then
'                    row("MonSch") = 0
'                End If
'                If row("TueSch") Is System.DBNull.Value Then
'                    row("TueSch") = 0
'                End If

'                If row("WedSch") Is System.DBNull.Value Then
'                    row("WedSch") = 0
'                End If
'                If row("ThurSch") Is System.DBNull.Value Then
'                    row("ThurSch") = 0
'                End If

'                If row("FriSch") Is System.DBNull.Value Then
'                    row("FriSch") = 0
'                End If
'                If row("SatSch") Is System.DBNull.Value Then
'                    row("SatSch") = 0
'                End If
'                If row("SunSch") Is System.DBNull.Value Then
'                    row("SunSch") = 0
'                End If
'                dtStudentDetails.Rows.Add(row)
'            End If
'            dtStudentDetails.AcceptChanges()
'            ProcessDate = ProcessDate.AddDays(1)
'            strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

'        End While

'    Next


'    ViewState("dtStudentDetails") = dtStudentDetails


'    ViewState("FromDate") = FromDate

'    ''   ConvertDatasetToAttendanceType()

'    BindCurrentWeek()
'    dgAttendance.DataBind()


'    'dgAttendance.DataSource = dtStudentDetails.Select("SunDate=#" + FromDate + "#")
'    'dgAttendance.DataBind()

'    'If dtStudentDetails.Rows.Count > 0 Then
'    '    lblsun.Text = Format(CDate(dtStudentDetails.Rows(0)("SunDate")), "MM/dd/yyyy")
'    '    lblmon.Text = Format(CDate(dtStudentDetails.Rows(0)("MonDate")), "MM/dd/yyyy")
'    '    lbltue.Text = Format(CDate(dtStudentDetails.Rows(0)("TueDate")), "MM/dd/yyyy")
'    '    lblwed.Text = Format(CDate(dtStudentDetails.Rows(0)("WedDate")), "MM/dd/yyyy")
'    '    lblthur.Text = Format(CDate(dtStudentDetails.Rows(0)("ThurDate")), "MM/dd/yyyy")
'    '    lblfri.Text = Format(CDate(dtStudentDetails.Rows(0)("FriDate")), "MM/dd/yyyy")
'    '    lblsat.Text = Format(CDate(dtStudentDetails.Rows(0)("SatDate")), "MM/dd/yyyy")
'    'End If

'End Sub
#End Region