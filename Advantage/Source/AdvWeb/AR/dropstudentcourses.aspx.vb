﻿Imports FAME.Common
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports System.Collections

Partial Class AR_dropstudentcourses
    Inherits BasePage
    Protected CampusId, UserId As String
    Private pObj As New UserPagePermissionInfo
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim advantageUserState As User = AdvantageSession.UserState
        Dim objCommon As New CommonUtilities
        Dim resourceId As Integer
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        CampusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        UserId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), CampusId)



        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW", pObj)
        End If

        txtAcademicYearId.Attributes.Add("onClick", "__doPostBack('btnGetStdCourses','')")

    End Sub

    Private Sub btnGetStdCourses_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetStdCourses.Click
        Dim facade As New DropStudentFacade
        Dim term As String
        Dim ds As DataSet

        With dgdDropCourse
            .DataSource = Nothing
            .DataBind()
        End With
        CampusId = HttpContext.Current.Request.Params("cmpid")

        If Session("StudentName") Is Nothing Then
            DisplayErrorMessage("Unable to find data. Student and Term fields cannot be empty.")
            Exit Sub
        Else
        End If

        If Session("TermId") Is Nothing Then
            DisplayErrorMessage("Unable to find data.  Student and Term fields cannot be empty.")
            Exit Sub
        Else
            term = CType(Session("TermId"), String)
            ViewState("Term") = term
        End If


        ds = facade.PopulateDataGrid(txtStuEnrollmentId.Text, term, CampusId)

        With dgdDropCourse
            .DataSource = ds
            .DataBind()
        End With
        ViewState("Drop") = ds.Tables("Drop")
    End Sub

    Private Sub BuildGradesDDL(ByVal ddl As DropDownList, ByVal clsSectId As String)

        Dim ddlDS As DataSet

        '   save TuitionCategories in the View state for subsequent use
        If ViewState("GradesDS") Is Nothing Then
            ddlDS = (New DropStudentFacade).GetDropGrades(clsSectId)
            ViewState("GradesDS") = ddlDS
        Else
            ddlDS = CType(ViewState("GradesDS"), DataSet)
        End If

        '   bind the TuitionCategories DDL
        With ddl
            .DataTextField = "Grade"
            .DataValueField = "GrdSysDetailId"
            .DataSource = ddlDS
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
        End With

    End Sub

    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, _
                               ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, _
                               ByVal hourtype As String)


        txtStuEnrollmentId.Text = enrollid
        txtTermId.Text = termid

        Session("StuEnrollment") = txtStuEnrollmentId.Text
        Session("StudentName") = fullname
        Session("TermId") = txtTermId.Text
    End Sub

    Private Sub dgdDropCourse_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgdDropCourse.ItemDataBound
        Dim ddl As DropDownList
        Dim count As Integer
        Dim clsSectId As String

        count = CInt(ViewState("Count"))

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                count += 1
                ViewState("Count") = count.ToString
                ddl = CType(e.Item.FindControl("ddlGrade"), DropDownList)
                clsSectId = CType(e.Item.FindControl("txtClsSectionId"), TextBox).Text
                BuildGradesDDL(ddl, clsSectId)

                'Select DDL if grade exists for student in "ClsSectStds" datatable.
        End Select
    End Sub
    Protected Sub ToggleRowSelection(ByVal sender As Object, ByVal e As EventArgs)
        Dim isAllItemsChecked As Boolean = True
        Dim ctrHeader As System.Web.UI.Control = dgdDropCourse.Controls(0).Controls(0)
        'Dim ctrHeader As System.Web.UI.Control = dgdDropCourse.FindControl(0)
        'Dim chkAllItems As CheckBox = TryCast(ctrHeader.FindControl("chkAllItems"), CheckBox)
        Dim chkAllItems As CheckBox
        chkAllItems = TryCast(ctrHeader.FindControl("chkAllItems"), CheckBox)
        'chkAllItems = TryCast(dgdDropCourse.Controls(0).Controls(0).FindControl("chkAllItems"), CheckBox)
        'chkAllItems = TryCast(dgdDropCourse.FindControl("chkAllItems"), CheckBox)
        'if some item is not checked the checkbox header should be false
        For Each dataItem As DataGridItem In dgdDropCourse.Items
            If Not TryCast(dataItem.FindControl("chkDrop"), CheckBox).Checked Then
                isAllItemsChecked = False
                Exit For
            End If
            isAllItemsChecked = True
        Next
        chkAllItems.Checked = isAllItemsChecked
    End Sub
    Protected Sub ToggleSelectedState(ByVal sender As Object, ByVal e As EventArgs)
        Dim chkAllItems As CheckBox = TryCast(sender, CheckBox)
        For Each dataItem As DataGridItem In dgdDropCourse.Items
            TryCast(dataItem.FindControl("chkDrop"), CheckBox).Checked = chkAllItems.Checked
        Next
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        '   if GrdSystem is nothing do an insert. Else do an update
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim sId As String
        Dim sGrade As String
        Dim objCommon As New CommonUtilities
        'Dim finalGradeObject As FinalGradeInfo
        'Dim errStr As String = ""
        Dim facade As New DropStudentFacade
        Dim clsSectId As String
        Dim selected As Boolean = False
        Dim dateDetermined As String

        ' Save the data-grid items in a collection.
        iitems = dgdDropCourse.Items
        Try
            sId = txtStuEnrollmentId.Text

            'Create the list of task
            Dim droppingList As IList(Of PocoDropCourse) = New List(Of PocoDropCourse)

            'Loop through the collection to retrieve the StudentId,score & Comments.
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkDrop"), CheckBox).Checked = True) Then
                    'set flag to true if option is selected
                    selected = True
                    sGrade = CType(iitem.FindControl("ddlGrade"), DropDownList).SelectedItem.Value
                    clsSectId = CType(iitem.FindControl("txtClsSectionId"), TextBox).Text
                    If sGrade <> "" Then

                        'Move the values in the controls over to the object
                        dateDetermined = CType(IIf(CType(iitem.FindControl("txtDateDetermined"), RadDatePicker).SelectedDate & "" = "", Nothing, CType(iitem.FindControl("txtDateDetermined"), RadDatePicker).SelectedDate), String)
                        If dateDetermined <> Nothing Then
                            If dateDetermined > Date.Today Then
                                DisplayErrorMessage("Date Determined should not be greater than current date.")
                                CType(iitem.FindControl("txtDateDetermined"), RadDatePicker).SelectedDate = Nothing
                                Exit Sub
                            End If
                        End If

                        ' If we are here populate the list of task....
                        Dim poco As PocoDropCourse = New PocoDropCourse()
                        poco.ClassId = clsSectId
                        poco.DateDetermined = CType(dateDetermined, Date)
                        poco.Grade = sGrade
                        poco.StuEnrollId = sId
                        poco.User = UserId
                        droppingList.Add(poco)

                        ' finalGradeObject = PopulateFinalGrdObject(sId, sGrade, clsSectId, dateDetermined)

                        'update the student grade
                        'errStr &= CType(facade.UpdateFinalGrade(finalGradeObject, AdvantageSession.UserState.UserName), String)
                    Else
                        If (CType(iitem.FindControl("txtDateDetermined"), RadDatePicker).SelectedDate IsNot Nothing) Then
                            CType(iitem.FindControl("txtDateDetermined"), RadDatePicker).SelectedDate = Nothing
                            Exit Sub
                        End If
                        DisplayErrorMessage("Please select Grade")
                    End If
                End If
            Next
            If selected = False Then
                DisplayErrorMessage("Please select one or more courses to drop")
                Exit Sub
            End If

            'Dropping the Students
            If (droppingList.Count > 0) Then
                Dim droppedObject = New DropCourseStudentClass(droppingList)
                droppedObject.DroppingStudents()
            End If


            'If errStr = "" Then 'if no error in updating grades
            'clear the data-grid every time grades are dropped
            With dgdDropCourse
                .DataSource = facade.PopulateDataGrid(txtStuEnrollmentId.Text, CType(ViewState("Term"), String), CampusId)
                .DataBind()
            End With
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT", pObj)
            btnDelete.Enabled = False
            'End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    'Private Function PopulateFinalGrdObject(ByVal stuEnrollId As String, ByVal grade As String, ByVal clsSectId As String, ByVal dateDetermined As String) As FinalGradeInfo
    '    Dim grdPostingObj As New FinalGradeInfo

    '    grdPostingObj.ClsSectId = clsSectId
    '    grdPostingObj.StuEnrollId = stuEnrollId
    '    grdPostingObj.Grade = grade
    '    grdPostingObj.DateDetermined = dateDetermined
    '    Return grdPostingObj
    'End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnGetStdCourses)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        'BIndToolTip()
    End Sub


End Class
