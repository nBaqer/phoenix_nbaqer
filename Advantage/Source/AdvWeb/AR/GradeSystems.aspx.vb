'===============================================================================
'
' FAME AdvantageV1
'
' GradeSystems.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class GradeSystems
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents AllDataset As System.Data.DataSet
    Protected WithEvents gradeSystemsTable As System.Data.DataTable
    Protected WithEvents gradeSystemDetailsTable As System.Data.DataTable
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.

    'Do not delete or move it.



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init

        'CODEGEN: This method call is required by the Web Form Designer

        'Do not modify it using the code editor.

        InitializeComponent()

    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private ValidateGradeSystemMessage As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objCommon As New CommonUtilities

        Dim campusId As String
        Dim userId As String
        Dim m_Context As HttpContext
        Dim resourceId As Integer

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = resourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Try
            If Not IsPostBack Or Session("AllDataset") Is Nothing Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                '   build dropdownlists
                BuildDropDownLists()

                '   get AllDataset from the DB
                With New GradesFacade
                    AllDataset = .GetGradeSystemsDS()
                End With

                '   save it on the session
                Session("AllDataset") = AllDataset


            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)

            End If

            '   create Dataset and Tables
            CreateDatasetAndTables()

            '   the first time we have to bind an empty datagrid
            If Not IsPostBack Then PrepareForNewData()

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub

    Private Sub SwapViewOrders(ByVal id1 As String, ByVal id2 As String)

        '   get the rows with each id
        Dim row1(), row2() As DataRow
        row1 = gradeSystemDetailsTable.Select("GrdSysDetailId=" + "'" + id1 + "'")
        row2 = gradeSystemDetailsTable.Select("GrdSysDetailId=" + "'" + id2 + "'")

        '   swap value of viewOrder
        Dim viewOrder As Integer
        viewOrder = row1(0)("ViewOrder")
        row1(0)("ViewOrder") = row2(0)("ViewOrder")
        row2(0)("ViewOrder") = viewOrder
    End Sub

    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
    End Sub

    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub

    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radstatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind GradeSystems datalist
        dlstGradeSystems.DataSource = New DataView(gradeSystemsTable, rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstGradeSystems.DataBind()

    End Sub

    Private Sub dgrdGradeSystemDetails_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdGradeSystemDetails.ItemCommand

        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdGradeSystemDetails.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdGradeSystemDetails.ShowFooter = False
                btnsave.Enabled = False

                '   user hit "Update" inside the datagrid
            Case "Update"
                '   process only if the edit textboxes are nonblank and valid
                If IsDataInEditTextboxesValid(e) And Not AreEditFieldsBlank(e) Then
                    '   get the row to be updated
                    Dim row() As DataRow = gradeSystemDetailsTable.Select("GrdSysDetailId=" + "'" + New Guid(CType(e.Item.FindControl("txtEditGrdSysDetailId"), TextBox).Text).ToString + "'")

                    '   fill new values in the row
                    row(0)("Grade") = CType(e.Item.FindControl("txtEditGrade"), TextBox).Text
                    row(0)("IsPass") = CType(e.Item.FindControl("chkEditIsPass"), CheckBox).Checked
                    '   GPA can be null and also can be 0.0
                    If (CType(e.Item.FindControl("txtEditGPA"), TextBox).Text) = "" Then
                        row(0)("GPA") = System.DBNull.Value
                    Else
                        row(0)("GPA") = Decimal.Parse(CType(e.Item.FindControl("txtEditGPA"), TextBox).Text)
                    End If
                    If Not CType(e.Item.FindControl("txtEditGPA"), TextBox).Text = "" Then row(0)("GPA") = CType(e.Item.FindControl("txtEditGPA"), TextBox).Text Else row(0)("GPA") = System.DBNull.Value
                    row(0)("IsCreditsEarned") = CType(e.Item.FindControl("chkEditIsCreditsEarned"), CheckBox).Checked
                    row(0)("IsCreditsAttempted") = CType(e.Item.FindControl("chkEditIsCreditsAttempted"), CheckBox).Checked
                    ''Added by saraswathi on may 04-2009
                    row(0)("IsCreditsAwarded") = CType(e.Item.FindControl("chkEditIsCreditsAwarded"), CheckBox).Checked

                    row(0)("IsInGPA") = CType(e.Item.FindControl("chkEditIsInGPA"), CheckBox).Checked
                    row(0)("IsInSap") = CType(e.Item.FindControl("chkEditIsInSAP"), CheckBox).Checked
                    row(0)("IsTransferGrade") = CType(e.Item.FindControl("chkEditIsTransferGrade"), CheckBox).Checked
                    row(0)("IsIncomplete") = CType(e.Item.FindControl("chkEditIsIncomplete"), CheckBox).Checked
                    row(0)("IsDefault") = CType(e.Item.FindControl("chkEditIsDefault"), CheckBox).Checked
                    row(0)("IsDrop") = CType(e.Item.FindControl("chkEditIsDrop"), CheckBox).Checked
                    row(0)("ModUser") = Session("UserName")
                    row(0)("ModDate") = Date.Now

                    '   prepare Datagrid
                    PrepareDataGrid()

                Else
                    DisplayErrorMessage("Invalid data")
                    Exit Sub
                End If

                '   user hit "Cancel"
            Case "Cancel"

                '   prepare DataGrid
                PrepareDataGrid()

                '   user hit "Delete" inside the datagrid
            Case "Delete"

                '   get the row to be deleted
                Dim row() As DataRow = gradeSystemDetailsTable.Select("GrdSysDetailId=" + "'" + CType(e.Item.FindControl("txtEditGrdSysDetailId"), TextBox).Text + "'")

                '   delete row 
                row(0).Delete()

                '   prepare DataGrid
                PrepareDataGrid()

            Case "AddNewRow"
                '   process only if the footer textboxes are nonblank and valid
                If IsDataInFooterTextboxesValid(e) And Not AreFooterFieldsBlank() Then
                    '   If the GradeSystem does not have an Id assigned.. create one and assign it.
                    If Session("GrdSystemId") Is Nothing Then
                        '   build a new row in GradeSystems table
                        BuildNewRowInGradeSystems()
                    End If

                    '   get a new row in GradeSystemDetails
                    Dim newRow As DataRow = gradeSystemDetailsTable.NewRow

                    '   fill the new row with values
                    newRow("GrdSysDetailId") = Guid.NewGuid
                    newRow("GrdSystemId") = New Guid(CType(Session("GrdSystemId"), String))
                    newRow("Grade") = CType(e.Item.FindControl("txtGrade"), TextBox).Text
                    newRow("IsPass") = CType(e.Item.FindControl("chkIsPass"), CheckBox).Checked
                    '   GPA can be null and also can be 0.0
                    If (CType(e.Item.FindControl("txtGPA"), TextBox).Text) = "" Then
                        newRow("GPA") = System.DBNull.Value
                    Else
                        newRow("GPA") = Decimal.Parse(CType(e.Item.FindControl("txtGPA"), TextBox).Text)
                    End If
                    newRow("IsCreditsEarned") = CType(e.Item.FindControl("chkIsCreditsEarned"), CheckBox).Checked
                    newRow("IsCreditsAttempted") = CType(e.Item.FindControl("chkIsCreditsAttempted"), CheckBox).Checked
                    ''Added by saraswathi on may 04-2009
                    newRow("IsCreditsAwarded") = CType(e.Item.FindControl("chkIsCreditsAwarded"), CheckBox).Checked

                    newRow("IsInGPA") = CType(e.Item.FindControl("chkIsInGPA"), CheckBox).Checked
                    newRow("IsInSap") = CType(e.Item.FindControl("chkIsInSAP"), CheckBox).Checked
                    newRow("IsTransferGrade") = CType(e.Item.FindControl("chkIsTransferGrade"), CheckBox).Checked
                    newRow("IsIncomplete") = CType(e.Item.FindControl("chkIsIncomplete"), CheckBox).Checked
                    newRow("IsDefault") = CType(e.Item.FindControl("chkIsDefault"), CheckBox).Checked
                    newRow("IsDrop") = CType(e.Item.FindControl("chkIsDrop"), CheckBox).Checked
                    newRow("ViewOrder") = GetMaxViewOrder(gradeSystemDetailsTable) + 1
                    newRow("ModUser") = Session("UserName")
                    newRow("ModDate") = Date.Now

                    '   add row to the table
                    newRow.Table.Rows.Add(newRow)
                Else
                    DisplayErrorMessage("Invalid data")
                    Exit Sub
                End If

                '   user hit "Up"
            Case "Up"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(gradeSystemDetailsTable, "GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim grdSysDetailId As String = CType(e.Item.FindControl("txtGrdSysDetailid"), TextBox).Text

                '   get the position of this item and swap viewOrder values with prevous item
                Dim i As Integer
                For i = 1 To dv.Count - 1
                    If CType(dv(i).Row("GrdSysDetailId"), Guid).ToString = grdSysDetailId Then
                        SwapViewOrders(CType(dv(i).Row("GrdSysDetailId"), Guid).ToString, CType(dv(i - 1).Row("GrdSysDetailId"), Guid).ToString)
                        Exit For
                    End If
                Next

                '   user hit "Down" 
            Case "Down"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(gradeSystemDetailsTable, "GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim grdSysDetailId As String = CType(e.Item.FindControl("txtGrdSysDetailid"), TextBox).Text

                '   get the position of this item and swap viewOrder values with next item
                Dim i As Integer
                For i = 0 To dv.Count - 2
                    If CType(dv(i).Row("GrdSysDetailId"), Guid).ToString = grdSysDetailId Then
                        SwapViewOrders(CType(dv(i).Row("GrdSysDetailId"), Guid).ToString, CType(dv(i + 1).Row("GrdSysDetailId"), Guid).ToString)
                        Exit For
                    End If
                Next

        End Select

        '   bind datagrid to GradeSystemDetailsTable only if there were no validation errors
        If e.Item.FindControl("txtGrade") Is Nothing Then
            dgrdGradeSystemDetails.DataSource = New DataView(gradeSystemDetailsTable, "GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
            dgrdGradeSystemDetails.DataBind()
        Else
            If Not (CType(e.Item.FindControl("txtGrade"), TextBox).Text = "") Then
                dgrdGradeSystemDetails.DataSource = New DataView(gradeSystemDetailsTable, "GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
                dgrdGradeSystemDetails.DataBind()
            End If
        End If

    End Sub

    Private Sub dlstGradeSystems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstGradeSystems.ItemCommand

        '   this portion of code added to refresh data from the database
        '**********************************************************************************
        '   get allDataset from the DB
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        With New GradesFacade
            AllDataset = .GetGradeSystemsDS()
        End With

        '   save it on the session
        Session("AllDataset") = AllDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()
        '**********************************************************************************

        '   save grdSystemId in session
        Session("GrdSystemId") = e.CommandArgument

        '   get the row with the data to be displayed
        Dim row() As DataRow = AllDataset.Tables("arGradeSystems").Select("GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'")

        '   fill new values in the row
        txtDescrip.Text = row(0)("Descrip")
        ddlStatusId.SelectedValue = CType(row(0)("StatusId"), Guid).ToString
        ddlCampGrpId.SelectedValue = CType(row(0)("CampGrpId"), Guid).ToString

        '   if there are Grade Scales attached to this GradeSystem disable the datagrid
        'dgrdGradeSystemDetails.Enabled = row(0)("InUseByGradeScales") = 0

        '   bind GradeSystemDetails datagrid
        dgrdGradeSystemDetails.EditItemIndex = -1
        dgrdGradeSystemDetails.DataSource = New DataView(gradeSystemDetailsTable, "GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
        dgrdGradeSystemDetails.DataBind()

        '   enable or disable dataGrid controls only if it has already being posted
        btnsave.Enabled = dgrdGradeSystemDetails.Enabled
        btnnew.Enabled = dgrdGradeSystemDetails.Enabled
        btndelete.Enabled = dgrdGradeSystemDetails.Enabled

        '   initialize buttons
        InitButtonsForEdit()

        '   show footer
        dgrdGradeSystemDetails.ShowFooter = True

        CommonWebUtilities.RestoreItemValues(dlstGradeSystems, CType(Session("GrdSystemId"), String))
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        '   if GrdSystemId does not exist do an insert else do an update
        If Session("GrdSystemId") Is Nothing Then
            '   do an insert
            BuildNewRowInGradeSystems()
        Else
            '   do an update

            '   get the row with the data to be displayed
            Dim row() As DataRow = gradeSystemsTable.Select("GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'")

            '   update row data
            row(0)("Descrip") = txtDescrip.Text
            row(0)("StatusId") = New Guid(ddlStatusId.SelectedValue)
            row(0)("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
            row(0)("ModUser") = Session("UserName")
            row(0)("ModDate") = Date.Now
        End If

        'validate grade system
        Dim result As String = ValidateGradeSystem(gradeSystemDetailsTable, CType(Session("GrdSystemId"), String))
        If Not result = "" Then
            DisplayErrorMessage(result)
            Exit Sub
        End If

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors then bind and set selected style to the datalist and
        '   initialize buttons 
        If Customvalidator1.IsValid Then

            '   bind GradeSystems datalist
            BindDataList()

            '   initialize buttons
            InitButtonsForEdit()

        End If
        CommonWebUtilities.RestoreItemValues(dlstGradeSystems, Session("GrdSystemId").ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click

        '   get all rows to be deleted
        Dim rows() As DataRow = gradeSystemDetailsTable.Select("GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'")

        '   delete all rows from GradeSystemDetails table
        Dim i As Integer
        If rows.Length > 0 Then
            For i = 0 To rows.Length - 1
                rows(i).Delete()
            Next
        End If

        '   get the row to be deleted in GradeSystems table
        Dim row() As DataRow = gradeSystemsTable.Select("GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'")

        '   delete row from the table
        row(0).Delete()

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   Prepare screen for new data
            PrepareForNewData()

        End If
        CommonWebUtilities.RestoreItemValues(dlstGradeSystems, Guid.Empty.ToString)
    End Sub

    Private Function GetMaxViewOrder(ByVal table As DataTable) As Integer
        Dim i As Integer
        GetMaxViewOrder = 1

        '   if there are no rows just return 1 
        Dim rows() As DataRow = table.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If rows.Length = 0 Then Return 1

        For i = 0 To rows.Length - 1
            If CType(rows(i)("ViewOrder"), Integer) > GetMaxViewOrder Then
                GetMaxViewOrder = CType(rows(i)("ViewOrder"), Integer)
            End If
        Next
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click

        '*********************** original code *******************************************
        ''   update DB
        'UpdateDB()

        ''   if there were no errors prepare screen for new data
        'If Customvalidator1.IsValid Then
        '    PrepareForNewData()
        'End If
        '************************* end of original code **********************************


        '   reset dataset to previous state. delete all changes
        AllDataset.RejectChanges()
        PrepareForNewData()

        '   New Grade Systems are enabled
        dgrdGradeSystemDetails.Enabled = True

        CommonWebUtilities.RestoreItemValues(dlstGradeSystems, Guid.Empty.ToString)
    End Sub

    Private Sub PrepareForNewData()

        '   bind GradeSystems datalist
        BindDataList()

        '   save GrdSystemId in session
        Session("GrdSystemId") = Nothing

        '   bind an empty GradeSystemDetails datagrid
        dgrdGradeSystemDetails.DataSource = New DataView(gradeSystemDetailsTable, "GrdSystemId=" + "'" + CType(Session("GrdSystemId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
        dgrdGradeSystemDetails.DataBind()

        '   initialize fields 
        txtDescrip.Text = ""
        ddlCampGrpId.SelectedValue = Guid.Empty.ToString

        '   initialize buttons
        InitButtonsForLoad()

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
            If AllDataset.HasChanges() Then
                'Set the Delete Button so it prompts the user for confirmation when clicked
                btnnew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
            Else
                btnnew.Attributes.Remove("onclick")
            End If

            '   save grade systems in the session
            Session("AllDataset") = AllDataset
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnsave)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            BindToolTip()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub

    Private Sub BuildNewRowInGradeSystems()
        '   get a new row
        Dim newRow As DataRow = gradeSystemsTable.NewRow

        '   create a Guid for GradeSystems if it has not been created 
        Session("GrdSystemId") = Guid.NewGuid.ToString

        '   fill the new row with values
        newRow("GrdSystemId") = New Guid(CType(Session("GrdSystemId"), String))
        newRow("Descrip") = txtDescrip.Text
        newRow("StatusId") = New Guid(ddlStatusId.SelectedValue)
        newRow("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
        newRow("ModUser") = Session("UserName")
        newRow("ModDate") = Date.Now

        '   add row to the table
        newRow.Table.Rows.Add(newRow)

    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
            dgrdGradeSystemDetails.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False

    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = dgrdGradeSystemDetails.Enabled
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub

    Private Sub CreateDatasetAndTables()

        '   Get GradeSystems Dataset from the session and create tables
        AllDataset = Session("AllDataset")
        gradeSystemsTable = AllDataset.Tables("arGradeSystems")
        gradeSystemDetailsTable = AllDataset.Tables("arGradeSystemDetails")

    End Sub

    Private Sub UpdateDB()
        '   update DB
        With New GradesFacade

            Dim result As String = .UpdateGradeSystemsDS(AllDataset)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get a new version of AllDataset because some other users may have added,
                '   updated or deleted records from the DB
                AllDataset = .GetGradeSystemsDS()
                Session("AllDataset") = AllDataset
                CreateDatasetAndTables()
            End If
        End With
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Function IsDataInEditTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtEditGPA")) Then Return False
        Return True
    End Function

    Private Function IsTextBoxValidInteger(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        Try
            Dim i As Integer = Integer.Parse(textbox.Text)
            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    Private Function IsTextBoxValidDecimal(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        Try
            Dim d As Decimal = Decimal.Parse(textbox.Text)
            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdGradeSystemDetails.Controls(0).Controls(dgrdGradeSystemDetails.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtGrade"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtGPA"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function AreEditFieldsBlank(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not CType(e.Item.FindControl("txtEditGrade"), TextBox).Text = "" Then Return False
        If Not CType(e.Item.FindControl("txtEditGPA"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function IsDataInFooterTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtGPA")) Then Return False
        Return True
    End Function

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then

            BindDataList()

            '   bind an empty datagrid
            PrepareForNewData()
        End If

    End Sub

    Private Sub dgrdGradeSystemDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdGradeSystemDetails.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                '   disable or enable edit buttons inside the datagrid
                CType(e.Item.FindControl("lnlButEdit"), LinkButton).Visible = dgrdGradeSystemDetails.Enabled
                CType(e.Item.FindControl("lnkButUp"), LinkButton).Visible = dgrdGradeSystemDetails.Enabled
                CType(e.Item.FindControl("lnkButDown"), LinkButton).Visible = dgrdGradeSystemDetails.Enabled
        End Select

    End Sub
    Private Sub PrepareDataGrid()
        '   set no record selected
        dgrdGradeSystemDetails.EditItemIndex = -1

        '   show footer
        dgrdGradeSystemDetails.ShowFooter = True

        '   save button must be disabled
        'btnSave.Enabled = True
        InitButtonsForEdit()

    End Sub

    Private Function ValidateGradeSystem(ByVal gradeSystemsDetailTable As DataTable, ByVal gradeSystemId As String) As String
        ValidateGradeSystemMessage = ""
        Dim rows() As DataRow = gradeSystemsDetailTable.Select("GrdSystemId=" + "'" + gradeSystemId + "'")
        Dim n As Integer = rows.Length

        'return message if the grade system is empty
        If n = 0 Then Return "Grade System is empty"

        'There should be only one item specified as being incomplete grade. In
        'addition this item cannot have any other property set as True such as Pass or Credits Earned etc.

        Dim incompleteCount As Integer = 0
        Dim defaultCount As Integer = 0

        For i As Integer = 0 To n - 1
            Dim row As DataRow = rows(i)
            'count number of "incomplete" items and check for not true properties
            If row("IsIncomplete") Then
                incompleteCount += 1
                ''Added by saraswathi on may 04 2009 isCreditsAwarded
                If row("IsCreditsEarned") Or row("IsCreditsAttempted") Or row("IsInGPA") Or row("IsInSap") Or row("IsTransferGrade") Or row("IsDefault") Or row("IsDrop") Then
                    addMessageErrorToTheList(row, "Incomplete item should not have any other property")
                End If
            End If

            'count number of "default" items and check for not true properties
            If row("IsDefault") Then
                defaultCount += 1
                ''Added by saraswathi on may 04 2009 isCreditsAwarded
                If row("IsCreditsEarned") Or row("IsCreditsAttempted") Or row("IsInGPA") Or row("IsInSap") Or row("IsTransferGrade") Or row("IsIncomplete") Or row("IsDrop") Then
                    addMessageErrorToTheList(row, "Default item should not have any other property")
                End If
            End If

            'check for not true properties for "Drop"
            'If row("IsDrop") Then
            '    ''Added by saraswathi on may 04 2009 isCreditsAwarded
            '    If row("IsCreditsEarned") Or row("IsInGPA") Or row("IsInSap") Or row("IsTransferGrade") Or row("IsIncomplete") Or row("IsDefault") Then
            '        addMessageErrorToTheList(row, "Drop item should not have any other property")
            '    End If
            'End If

            'check for values marked as "InGPA"
            If row("IsInGPA") And row("GPA") Is System.DBNull.Value Then
                addMessageErrorToTheList(row, "Any item that 'Is In GPA' must have a GPA value")
            End If

        Next
        'check number of incomplete items
        If incompleteCount > 1 Then ValidateGradeSystemMessage &= "There is more than one 'Incomplete' grade." + vbCr + vbLf

        'check number of default items
        If defaultCount > 1 Then ValidateGradeSystemMessage &= "There is more than one 'Default' grade." + vbCr + vbLf

        'check for duplicated grades
        CheckDuplicateGrades(rows)

        'return result
        Return ValidateGradeSystemMessage

    End Function

    Private Sub addMessageErrorToTheList(ByVal row As DataRow, ByVal message As String)
        ValidateGradeSystemMessage &= "Grade: " + row("Grade") + " " + message + "." + vbCr + vbLf
    End Sub

    Private Sub CheckDuplicateGrades(ByVal rows() As DataRow)
        'define array with proper dimensions
        Dim g(rows.Length - 1) As String

        'create array
        For i As Integer = 0 To rows.Length - 1
            g(i) = rows(i)("Grade")
        Next

        'sort array
        Array.Sort(g)

        'check for duplicates
        For i As Integer = 1 To rows.Length - 1
            If g(i).Trim = g(i - 1).Trim Then
                ValidateGradeSystemMessage &= "Grade: " + g(i) + " has been entered more than once." + vbCr + vbLf
            End If
        Next

    End Sub

End Class
