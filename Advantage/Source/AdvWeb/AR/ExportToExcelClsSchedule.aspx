<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportToExcelClsSchedule.aspx.vb" Inherits="AR_ExportToExcelClsSchedule" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Export to Excel</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="pnlClassSchedule" runat="server">
             <table align=center>
                <tr>
                    <td><asp:Label ID="lblStartDate" runat=server CssClass="label">Start Date</asp:Label></td>
                    <td><asp:Label ID="lblStartDateValue" runat=server CssClass="textbox"></asp:Label></td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblEndDate" runat=server CssClass="label">End Date</asp:Label></td>
                    <td><asp:Label ID="lblEndDateValue" runat=server CssClass="textbox"></asp:Label></td>
                </tr>
             </table>
             <br /><br /><br />
             <asp:Panel id=pnlCSM Runat="server" CssClass="label" Width="100%"></asp:Panel>
            <asp:panel ID="pnlInstructorhours" runat ="server" Visible=false>
                   <asp:DataGrid ID="dgrdInstructorHours" runat="server" Width="100%" HorizontalAlign="Center"
                                        BorderStyle="Solid" AutoGenerateColumns="True" AllowSorting="False" BorderColor="#E0E0E0">
                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle></asp:DataGrid>
           </asp:panel>
                   </asp:Panel>
    </div>
    </form>
</body>
</html>
