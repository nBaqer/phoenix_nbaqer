

Imports Fame.DataAccessLayer
Imports System.Data
Imports Fame.Common
Imports Fame.ExceptionLayer
Imports Fame.Advantage.Common
Imports System.Collections
Imports Fame.Advantage.Api.Library.AcademicRecords
Imports Fame.Advantage.Api.Library.Models
Imports Fame.Advantage.Api.Library.Models.AcademicRecords
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports Fame.Advantage.Api.Library.Models.Common
Imports Telerik.Web.UI
' Imports FAME.Advantage.Domain.Student.Enrollments

Partial Class ProgVerDef
    Inherits System.Web.UI.Page
    Protected WithEvents Button9 As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Public CourseWeigthDatatable As DataTable
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            Dim m_Context As HttpContext
            m_Context = HttpContext.Current
            txtResourceId.Text = CInt(m_Context.Items("ResourceId"))
            If Not Page.IsPostBack Then
                Dim db As New DataAccess
                '    Dim strSQL, strDescrip As String
                Dim ds As DataSet
                'Dim da As OleDbDataAdapter
                'Dim dr As OleDbDataReader
                Dim objCommon As New CommonUtilities
                Dim dt As New DataTable
                Dim dt1 As New DataTable
                Dim dt2 As New DataTable
                Dim objListGen As New DataListGenerator
                Dim ds1 As New DataSet
                Dim sw As New System.IO.StringWriter
                Dim ds2 As New DataSet
                Dim tbl1 As New DataTable
                Dim tbl2 As New DataTable
                'Dim sChildTyp As String
                'Dim sChildId As String
                'Dim row As DataRow
                'Dim selHours As Integer
                'Dim selCredits As Integer
                Dim facade As New ProgFacade
                Dim PrgVerID As String
                'Dim displhrs As Integer
                'Dim displcrds As Integer
                'Dim i As Integer
                Dim campusId As String
                Dim userId As String

                Dim resourceId As Integer
                Dim StudentsRegistered As Integer

                Dim advantageUserState As New BO.User()
                advantageUserState = AdvantageSession.UserState

                resourceId = CInt(HttpContext.Current.Request.Params("resid"))
                campusId = AdvantageSession.UserState.CampusId.ToString
                userId = AdvantageSession.UserState.UserId.ToString


                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

                If Request.QueryString("ProgVerId") <> "" Then
                    ViewState("PrgVerId") = Request.QueryString("ProgVerId")
                    txtProgVerId.Text = ViewState("PrgVerId")
                End If
                If Request.QueryString("PrgVersion") <> "" Then
                    ViewState("{PrgVersion") = Server.UrlDecode(Request.QueryString("PrgVersion"))
                    txtProgVerDescrip.Text = ViewState("{PrgVersion")
                End If
                If Request.QueryString("Hours") <> "" Then
                    ViewState("PrgVerHours") = Request.QueryString("Hours")
                    txtHours.Text = ViewState("PrgVerHours")
                End If
                If Request.QueryString("Credits") <> "" Then
                    ViewState("PrgVerCredits") = Request.QueryString("Credits")
                    txtCredits.Text = ViewState("PrgVerCredits")
                End If
                If Request.QueryString("TrackAttendance") <> "" Then
                    ViewState("TrackAttendance") = Request.QueryString("TrackAttendance")
                End If

                PrgVerID = txtProgVerId.Text

                'Populate the minimum grade required text box
                Dim dtMinGrdReqd As New DataTable
                dtMinGrdReqd = facade.GetMinGradeReqdDT(PrgVerID)
                For Each drMinGrd As DataRow In dtMinGrdReqd.Rows
                    txtMinGrdReq.Text = drMinGrd("Grade")
                    Try
                        txtMinGradeId.Text = CType(drMinGrd("GrdSysDetailId"), Guid).ToString
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        txtMinGradeId.Text = ""
                    End Try
                Next


                'Populate the Courses Not Assigned.
                ds = facade.ProcessCoursesNotAssgd("PROGVER", PrgVerID, ViewState("TrackAttendance"), campusId)

                'Bind the datatable to the Available Courses Listbox.
                lbxAvailCourses.DataSource = ds
                lbxAvailCourses.DataTextField = "ChildDescripTyp"
                lbxAvailCourses.DataValueField = "ReqId"
                lbxAvailCourses.DataBind()


                'Populate the CourseGrps Not Assigned.
                ds = facade.ProcessCourseGrpsNotAssgd("PROGVER", PrgVerID, campusId)


                'Bind the datatable to the Available CourseGrps Listbox.
                lbxAvailCourseGrps.DataSource = ds
                lbxAvailCourseGrps.DataTextField = "Descrip"
                lbxAvailCourseGrps.DataValueField = "ReqId"
                lbxAvailCourseGrps.DataBind()

                'Get all courses and coursegprs assigned to a particular prog version
                ds = facade.GetCourseCourseGrpsAssigned("PROGVER", PrgVerID)


                'Get Child Descriptions, populate hours and credits for selected
                'courses/coursegrps and determine whether a course/course grp is
                'required or elective.
                ds = facade.ProcessHrsCrdtsDescrips(ds)

                CourseWeigthDatatable = GetTable.Clone()
                ViewState("CourseWeightTable") = CourseWeigthDatatable
                CourseWeigthDatatable = ViewState("CourseWeightTable")
                courseWeightTable.MasterTableView.ClearEditItems()

                tbl1 = ds.Tables("FldsSelected")
                'Bind only if datatable is not empty
                If tbl1.Rows.Count > 0 Then
                    'Bind the datatable to the "Selected Courses/Coursegrps" Listbox.
                    lbxSelected.DataSource = tbl1
                    lbxSelected.DataTextField = "ChildDescripTyp"
                    lbxSelected.DataValueField = "ReqId"
                    lbxSelected.DataBind()

                    CourseWeigthDatatable = GetTable(tbl1)
                    'courseWeightTable.DataSource = CourseWeithDatatable
                    ViewState("CourseWeightTable") = CourseWeigthDatatable

                End If
                courseWeightTable.DataSource = CourseWeigthDatatable

                courseWeightTable.Rebind()

                'Get the hours and credits corr. to the sel.Courses/CourseGrps
                tbl2 = ds.Tables("ChildHrsCrds")
                If tbl2.Rows.Count > 0 Then
                    txtHours2.Text = tbl2.Rows(0).Item(0).ToString
                    txtCredits2.Text = tbl2.Rows(0).Item(1).ToString
                End If

                'Save the Hours & Credits to ViewState
                ViewState("selHours") = txtHours2.Text
                ViewState("selCredits") = txtCredits2.Text

                'Save the dataset to session
                Session("WorkingDS") = ds

                txtModUser.Text = Session("UserName")
                txtModDate.Text = Date.MinValue.ToString

                'Disable the new and delete buttons
                objCommon.SetBtnState(Form1, "NEW", pObj)

                'If there are any students registered for this program version,
                'then the user should not be allowed to modify this program version
                'definition

                StudentsRegistered = facade.DoesPrgVerHaveStdsReg(txtProgVerId.Text)
                Dim b As Boolean = StudentsRegistered = 0 Or (StudentsRegistered > 0 And GetWebConfigValueForAllowAddingCoursesToProgramVersionsAfterStudentsHaveBeenEnrolled())
                btnSave.Enabled = b
                btnDelete.Enabled = b
                btnAdd.Enabled = b
                btnRequired.Enabled = b
                btnElective.Enabled = b
                btnRemove.Enabled = b
                btnUp.Enabled = b
                btnDown.Enabled = b
                btnDetails.Enabled = b
                PopulateGrdOverddl()
                InitButtonsForLoad(advantageUserState.IsUserSupport, advantageUserState.IsUserSA)

                'Enable Course Weigth Overall into GPA
                Dim EnableCourseWeigthForGPA = IsCourseWeightForGPAEnabled()

                CourseWeightForGPA.Visible = EnableCourseWeigthForGPA
                CourseWeightForGPALabel.Visible = EnableCourseWeigthForGPA

                Dim progVerFacade As New ProgVerFacade

                If EnableCourseWeigthForGPA Then
                    Dim weightIntoOverallGPAChecked As Boolean = progVerFacade.GetDoCourseWeightOverallGPAValue(ViewState("PrgVerId"))
                    CourseWeightForGPA.Checked = weightIntoOverallGPAChecked
                    ToogleCourseWeightForGPA()
                End If

                Dim programVersionDetails As DataSet = progVerFacade.GetProgVersionsByProgramVerId(ViewState("PrgVerId").ToString(), campusId)
                If (Not programVersionDetails.Tables(0).Rows(0)("ProgramRegistrationType") Is Nothing) Then
                    If (Not programVersionDetails.Tables(0).Rows(0)("ProgramRegistrationType") Is DBNull.Value) Then
                        ViewState("ProgramRegistrationType") = programVersionDetails.Tables(0).Rows(0)("ProgramRegistrationType").ToString()
                        If (CType(Enums.ProgramRegistrationType.ByProgram, Integer).ToString() = programVersionDetails.Tables(0).Rows(0)("ProgramRegistrationType").ToString()) Then
                            tdButtonsAvailableCourse.Visible = False
                            tdLabelAvailableCourse.Visible = False
                            tdListboxAvailableCourse.Visible = False
                            btnElective.Visible = False
                        Else
                            tdButtonsAvailableCourse.Visible = False
                            tdLabelAvailableCourse.Visible = False
                            tdListboxAvailableCourse.Visible = False
                        End If
                    End If
                End If


                If (Not programVersionDetails.Tables(0).Rows(0)("GradeScaleId") Is Nothing) Then
                    If (Not programVersionDetails.Tables(0).Rows(0)("GradeScaleId") Is DBNull.Value) Then
                        ViewState("GradeScaleId") = programVersionDetails.Tables(0).Rows(0)("GradeScaleId").ToString()
                    End If
                End If

            End If
            btnDetails.Enabled = False
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Public Function IsCourseWeightForGPAEnabled() As Boolean
        Dim allow = "no"
        If Not IsNothing(MyAdvAppSettings.AppSettings("AllowCourseWeighting")) Then

            If Not String.IsNullOrEmpty(MyAdvAppSettings.AppSettings("AllowCourseWeighting").ToString.ToLower) Then
                allow = MyAdvAppSettings.AppSettings("AllowCourseWeighting").ToString.ToLower
            End If
        End If

        If (allow = "yes") Then
            Return GetWebConfigValueForGradesFormat().ToString() = "Numeric" And GetWebConfigValueForGradeBookWeightingLevel().ToString() = "CourseLevel"
        Else
            Return False
        End If

    End Function

    Private Sub btnRequired_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRequired.Click
        Dim strRemove As String = String.Empty
        If lbxAvailCourses.SelectedIndex >= 0 Then
            '   Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim dt2 As DataTable
            '   Dim dt3 As DataTable
            Dim row As DataRow
            Dim selhours As Decimal
            Dim selcredits As Decimal
            Dim sCourseId As String
            '   Dim childID As String
            Dim foundInCourseGroup As String
            Dim AddRowInDT As Boolean = True

            ddlGrdSysDetailId.Enabled = True

            'Add selected item to the relevant assigned list. We want to ensure that the course does not 
            'already belong to the selected items. This could only happen if it was part of a course group
            'that is already in the selected items. For each item in the selected list we will check if it
            'is a course group and then check to see if the selected course is in it. If it is then we will
            'cancel the add operation and display an error message to the user.
            Dim thereAreSelectedItems As Boolean = True
            Do While (thereAreSelectedItems)

                For Each item As ListItem In lbxAvailCourses.Items
                    If (item.Selected) Then
                        foundInCourseGroup = CourseExistsInSelectedList(item.Value)

                        If foundInCourseGroup = "" Then
                            lbxSelected.Items.Add(New ListItem(item.Text & " - R", item.Value))

                            'Get the dataset from session
                            ds = Session("WorkingDS")
                            'retrieve the datatable from the ds
                            dt = ds.Tables("FldsSelected")

                            Dim dvrs As DataViewRowState
                            dvrs = DataViewRowState.Deleted
                            Dim arows As DataRow() = dt.Select("ReqId = '" & item.Value.ToString & "' ", "", dvrs)
                            If arows.Length > 0 Then
                                dt.Rows.Remove(arows(0))
                                AddRowInDT = False
                            End If

                            If AddRowInDT = True Then
                                dr = dt.NewRow()
                                dr("ReqId") = item.Value.ToString
                                dr("ReqSeq") = lbxSelected.Items.Count
                                dr("IsRequired") = 1
                                dr("ReqTypeId") = 1
                                dr("Descrip") = item.Text
                                If (chkTrkForCompletion.Checked) Then
                                    dr("TrkForCompletion") = True
                                Else
                                    dr("TrkForCompletion") = False
                                End If
                                If ddlGrdSysDetailId.SelectedItem.Value <> "" Then
                                    dr("GrdSysDetailId") = ddlGrdSysDetailId.SelectedItem.Value.ToString
                                End If

                                'Get hours and credits of the selected course from datatable.
                                'retrieve the Courses datatable from the ds
                                dt2 = ds.Tables("Courses")
                                'Get the Requirement Id(i.e CourseId in this case)
                                sCourseId = item.Value.ToString
                                'Find the row that contains the course info.
                                row = dt2.Rows.Find(sCourseId)
                                dr("Hours") = row("Hours")
                                dr("Credits") = row("Credits")
                                dt.Rows.Add(dr)

                                'Save the dataset to session
                                Session("WorkingDS") = ds

                                If IsCourseWeightForGPAEnabled() Then
                                    'add row to table for weights
                                    CourseWeigthDatatable = GetTable(dt)
                                    ViewState("CourseWeightTable") = CourseWeigthDatatable
                                    courseWeightTable.DataSource = CourseWeigthDatatable
                                    courseWeightTable.Rebind()

                                    CalculatePercentageTotal()
                                End If


                            End If


                            '****************************************************************************
                            'Everytime we select a course, we want to populate
                            'the Hours and Credits associated with that 
                            'particular course.
                            '****************************************************************************
                            'Get the dataset from session
                            ds = Session("WorkingDS")

                            'retrieve the Courses datatable from the ds
                            dt2 = ds.Tables("Courses")

                            'Get the Requirement Id(i.e CourseId in this case)
                            sCourseId = item.Value.ToString

                            'Find the row in the courses datatable that contains this
                            'course information.
                            row = dt2.Rows.Find(sCourseId)

                            'Retrieve the hours and credits attached to this course.
                            If (ViewState("selHours") <> "") Then
                                selhours = Decimal.Parse(ViewState("selHours"))
                            Else
                                selhours = 0
                            End If
                            If Not row.IsNull("Hours") Then
                                selhours += row("Hours")
                            End If
                            ViewState("selHours") = selhours.ToString
                            txtHours2.Text = selhours.ToString

                            If (ViewState("selCredits") <> "") Then
                                selcredits = Decimal.Parse(ViewState("selCredits"))
                            Else
                                selcredits = 0
                            End If
                            If Not row.IsNull("Credits") Then
                                selcredits += row("Credits")
                            End If
                            ViewState("selCredits") = selcredits.ToString
                            txtCredits2.Text = selcredits.ToString

                            Session("WorkingDS") = ds
                            'Remove the item from the lbxAvailCamps list
                            lbxAvailCourses.Items.Remove(item)

                        Else
                            DisplayErrorMessage(item.Text & " aready exists in " & foundInCourseGroup)
                        End If
                        'thereAreSelectedItems = True
                        Exit For
                    End If
                    thereAreSelectedItems = False
                Next
                If (lbxAvailCourses.Items.Count = 0) Then thereAreSelectedItems = False
            Loop
            ddlGrdSysDetailId.SelectedIndex = 0
            'lbxAvailCourses.Items.Remove
            'RemoveSelItemsFromListBox(lbxAvailCourses)
        End If


    End Sub
    ''Private Sub RemoveItemsFromListBox(ByVal ctrl As ListBox, ByVal selIndex As String)
    ''    If (selIndex <> "") Then
    ''        Dim arr() As String = selIndex.Split(",")
    ''        Dim i As Integer = 0
    ''        For i = 0 To arr.Length - 1
    ''            ctrl.Items.RemoveAt(Convert.ToInt32(arr(i)))
    ''        Next
    ''    End If
    ''End Sub

    ''Private Sub RemoveSelItemsFromListBox(ByVal ctrl As ListBox)
    ''    Dim Index As Integer = 0
    ''    For Index = 0 To ctrl.Items.Count
    ''        If (ctrl.Items(Index).Selected) Then
    ''            ctrl.Items.RemoveAt(Index)
    ''        End If
    ''    Next
    ''End Sub


    Private Function GetTable() As DataTable
        Dim tbl As New DataTable
        tbl.Columns.Add(New DataColumn("CourseName", GetType(String)))
        tbl.Columns.Add(New DataColumn("CourseWeight", GetType(String)))
        tbl.Columns.Add(New DataColumn("ProgramVersionDefinitionId", GetType(String)))
        tbl.Columns.Add(New DataColumn("RequirementId", GetType(String)))
        Return tbl
    End Function

    Private Function GetTable(table As DataTable) As DataTable
        Dim tbl = GetTable()
        Dim rows = table.Select("IsRequired=1").ToList()
        Dim recordCount = rows.Count()
        Dim initialPercentageCounter As Decimal = 0
        Dim currentValueToAssign As Decimal = 0
        Dim index As Integer = 1
        Dim totalPercentage As Decimal = 0
        For Each item In rows
            If IsDBNull(item("CourseWeight")) = False Then
                totalPercentage += CType(item("CourseWeight"), Decimal)

            End If
        Next

        For Each item As DataRow In rows
            Dim newRow = tbl.NewRow()
            Dim isRequired = CType(item("IsRequired"), Boolean)

            'Only if course is required
            If isRequired Then

                'Check if current value is null
                If item("CourseWeight") Is Nothing Then
                    'assign equally weighted for each row
                    If recordCount > 0 Then


                        currentValueToAssign = If(totalPercentage = 0, Convert.ToDecimal(Math.Round(100 / recordCount, 2, MidpointRounding.AwayFromZero)), 0)

                        initialPercentageCounter += currentValueToAssign
                        If index = recordCount And initialPercentageCounter > 0 Then
                            'substract value added to initial percentage counter 
                            initialPercentageCounter -= currentValueToAssign
                            currentValueToAssign = 100 - initialPercentageCounter
                        End If

                        newRow("CourseWeight") = currentValueToAssign
                    Else
                        newRow("CourseWeight") = 0
                    End If

                Else 'else Check current value

                    Dim weight = If(IsDBNull(item("CourseWeight")), 0, CType(item("CourseWeight"), Decimal))

                    'If current weight is greater that 0, then assign it
                    If (weight > 0) Then
                        newRow("CourseWeight") = item("CourseWeight")
                    Else 'else assign equally weighted for each row

                        If recordCount > 0 Then


                            currentValueToAssign = If(totalPercentage = 0, Convert.ToDecimal(Math.Round(100 / recordCount, 2, MidpointRounding.AwayFromZero)), 0)

                            initialPercentageCounter += currentValueToAssign
                            If index = recordCount And initialPercentageCounter > 0 Then
                                'substract value added to initial percentage counter 
                                initialPercentageCounter -= currentValueToAssign
                                currentValueToAssign = 100 - initialPercentageCounter
                            End If

                            newRow("CourseWeight") = currentValueToAssign
                        Else
                            newRow("CourseWeight") = 0
                        End If
                    End If

                End If


                newRow("ProgramVersionDefinitionId") = item("ProgVerDefId")
                newRow("CourseName") = item("Descrip") & If(IsDBNull(item("Code")), "", " (" & item("Code") & " )")
                newRow("RequirementId") = item("ReqId")

                tbl.Rows.Add(newRow)
                index += 1
            End If

        Next
        Return tbl
    End Function

    Private Sub btnElective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnElective.Click
        Try

            If lbxAvailCourses.SelectedIndex >= 0 Then
                '       Dim SelIndex As Integer
                Dim dt As DataTable
                Dim dr As DataRow
                Dim ds As New DataSet
                Dim dt2 As DataTable
                Dim row As DataRow
                Dim selhours As Decimal
                Dim selcredits As Decimal
                Dim sCourseId As String
                '       Dim childID As String
                Dim foundInCourseGroup As String
                Dim AddRowInDT As Boolean = True

                'Add selected item to the relevant assigned list. We want to ensure that the course does not 
                'already belong to the selected items. This could only happen if it was part of a course group
                'that is already in the selected items. For each item in the selected list we will check if it
                'is a course group and then check to see if the selected course is in it. If it is then we will
                'cancel the add operation and display an error message to the user.
                Dim thereAreSelectedItems As Boolean = True
                ddlGrdSysDetailId.Enabled = True
                Do While (thereAreSelectedItems)

                    For Each item As ListItem In lbxAvailCourses.Items
                        If (item.Selected) Then


                            foundInCourseGroup = CourseExistsInSelectedList(item.Value)

                            If foundInCourseGroup = "" Then
                                'Add selected item to the relevant assigned list. 

                                lbxSelected.Items.Add(New ListItem(item.Text & " - E", item.Value))

                                'Get the dataset from session
                                ds = Session("WorkingDS")
                                'retrieve the datatable from the ds
                                dt = ds.Tables("FldsSelected")

                                Dim dvrs As DataViewRowState
                                dvrs = DataViewRowState.Deleted
                                Dim arows As DataRow() = dt.Select("ReqId = '" & item.Value.ToString & "' ", "", dvrs)
                                If arows.Length > 0 Then
                                    dt.Rows.Remove(arows(0))
                                    AddRowInDT = False
                                End If

                                If AddRowInDT = True Then
                                    dr = dt.NewRow()
                                    dr("ReqId") = item.Value.ToString
                                    dr("ReqSeq") = lbxSelected.Items.Count
                                    dr("ReqTypeId") = 1
                                    dr("IsRequired") = 0
                                    If (chkTrkForCompletion.Checked) Then
                                        dr("TrkForCompletion") = True
                                    Else
                                        dr("TrkForCompletion") = False
                                    End If
                                    If ddlGrdSysDetailId.SelectedItem.Value <> "" Then
                                        dr("GrdSysDetailId") = ddlGrdSysDetailId.SelectedItem.Value.ToString
                                    End If
                                    'Get hours and credits of the selected course from datatable.
                                    'retrieve the Courses datatable from the ds
                                    dt2 = ds.Tables("Courses")
                                    'Get the Requirement Id(i.e CourseId in this case)
                                    sCourseId = item.Value.ToString
                                    'Find the row that contains the course info.
                                    row = dt2.Rows.Find(sCourseId)
                                    dr("Hours") = row("Hours")
                                    dr("Credits") = row("Credits")
                                    dt.Rows.Add(dr)

                                    'Save the dataset to session
                                    Session("WorkingDS") = ds
                                End If



                                '**************************************************************
                                'Everytime we select a course, we want to populate
                                'the Hours and Credits associated with that 
                                'particular course.

                                'Get the dataset from session
                                ds = Session("WorkingDS")
                                'retrieve the datatable from the ds
                                dt2 = ds.Tables("Courses")
                                '**************************************************************

                                sCourseId = item.Value.ToString

                                row = dt2.Rows.Find(sCourseId)


                                'Retrieve the hours and credits attached to this course.
                                If (ViewState("selHours") <> "") Then
                                    selhours = Decimal.Parse(ViewState("selHours"))
                                Else
                                    selhours = 0
                                End If
                                If Not row.IsNull("Hours") Then
                                    selhours += row("Hours")
                                End If
                                ViewState("selHours") = selhours.ToString
                                txtHours2.Text = selhours.ToString

                                If (ViewState("selCredits") <> "") Then
                                    selcredits = Decimal.Parse(ViewState("selCredits"))
                                Else
                                    selcredits = 0
                                End If
                                If Not row.IsNull("Hours") Then
                                    selcredits += row("Credits")
                                End If
                                ViewState("selCredits") = selcredits.ToString
                                txtCredits2.Text = selcredits.ToString

                                Session("WorkingDS") = ds
                                'Remove the item from the lbxAvailCamps list
                                lbxAvailCourses.Items.Remove(item)

                            Else
                                DisplayErrorMessage(lbxAvailCourses.SelectedItem.Text & " aready exists in " & foundInCourseGroup)
                                Exit Sub
                            End If
                            thereAreSelectedItems = True
                            Exit For
                        End If
                        thereAreSelectedItems = False
                    Next
                    If (lbxAvailCourses.Items.Count = 0) Then thereAreSelectedItems = False
                Loop
                ddlGrdSysDetailId.SelectedIndex = 0
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim exStr As String = ex.ToString
        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        '   Dim SelIndex As Integer
        Dim iChildId As String
        Dim dt1 As DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim intPos As Integer
        Dim dt2 As DataTable
        Dim row As DataRow
        Dim selhours As Decimal
        Dim selcredits As Decimal
        Dim sCourseId As String
        Dim sCourseGrpId As String
        '    Dim dt As DataTable
        Dim intElect As Integer
        Dim intHyphen As Integer
        Dim facade As New ProgFacade
        Dim errString As String = String.Empty
        Dim errVerbiage As String = " cannot be removed because there are registered students"

        If lbxSelected.SelectedIndex >= 0 Then
            'Add selected item to the lbxAvailCourses list. In order to do this we
            'have to use the FldsSelectCamps to find out the 'FldId for the selected item.
            'Remember that the item has a "-R" or "-E" attached to it so we have to find where the " - "
            'starts inorder to remove it.

            Dim thereAreSelectedItems As Boolean = True
            Do While (thereAreSelectedItems)



                For Each item As ListItem In lbxSelected.Items
                    If (item.Selected) Then
                        If (Not facade.IsCourseHaveStudents(item.Value, txtProgVerId.Text)) Then
                            intHyphen = item.Text.LastIndexOf("-")
                            intPos = item.Text.LastIndexOf("- R")
                            intElect = item.Text.LastIndexOf("- E")
                            If (intPos >= 0 Or intElect >= 0) Then  'Course was selected
                                lbxAvailCourses.Items.Add(New ListItem(item.Text.Substring(0, intHyphen - 1), item.Value))
                                '**************************************************************
                                'Everytime we de-select a course, we want to subract
                                'the Hours and Credits associated with that 
                                'particular course from the Hours & Credits text boxes.

                                'Get the dataset from session
                                ds = Session("WorkingDS")
                                'retrieve the datatable to decrement the indices of the items below.
                                dt1 = ds.Tables("FldsSelected")
                                Dim aRows As DataRow()
                                Dim row1 As DataRow
                                aRows = dt1.Select("ReqSeq > " & lbxSelected.SelectedIndex)
                                For Each row1 In aRows
                                    row1("ReqSeq") = row1("ReqSeq") - 1
                                Next
                                'Session("WorkingDS") = ds

                                'ds = Session("WorkingDS")

                                'retrieve the datatable from the ds
                                dt2 = ds.Tables("Courses")
                                '**************************************************************

                                sCourseId = item.Value.ToString

                                row = dt2.Rows.Find(sCourseId)

                                selhours = Decimal.Parse(ViewState("selHours"))
                                If Not row.IsNull("Hours") Then
                                    selhours -= row("Hours")
                                End If
                                ViewState("selHours") = selhours.ToString
                                txtHours2.Text = selhours.ToString

                                selcredits = Decimal.Parse(ViewState("selCredits"))
                                If Not row.IsNull("Credits") Then
                                    selcredits -= row("Credits")
                                End If
                                ViewState("selCredits") = selcredits.ToString
                                txtCredits2.Text = selcredits.ToString

                                If IsCourseWeightForGPAEnabled() And (intPos > 0) Then
                                    Dim courseName = item.Text.Substring(0, intHyphen - 1)
                                    CourseWeigthDatatable = ViewState("CourseWeightTable")
                                    Dim drCW As DataRow = CourseWeigthDatatable.Select("CourseName = '" & courseName & "' ", "").FirstOrDefault()
                                    If drCW IsNot Nothing Then
                                        CourseWeigthDatatable.Rows.Remove(drCW)
                                    End If
                                End If

                            Else 'Coursegrp was selected
                                lbxAvailCourseGrps.Items.Add(New ListItem(item.Text, item.Value))
                                '**************************************************************
                                'Everytime we de-select a course, we want to subract
                                'the Hours and Credits associated with that 
                                'particular coursegrp from the Hours & Credits text boxes.

                                'Get the dataset from session
                                ds = Session("WorkingDS")
                                dt1 = ds.Tables("FldsSelected")
                                Dim aRows As DataRow()
                                Dim row1 As DataRow

                                aRows = dt1.Select("ReqSeq > " & lbxSelected.SelectedIndex)
                                For Each row1 In aRows
                                    row1("ReqSeq") = row1("ReqSeq") - 1
                                Next
                                'Session("WorkingDS") = ds

                                'ds = Session("WorkingDS")
                                'retrieve the datatable from the ds
                                dt2 = ds.Tables("CourseGrps")
                                '**************************************************************

                                sCourseGrpId = item.Value.ToString

                                row = dt2.Rows.Find(sCourseGrpId)

                                selhours = Decimal.Parse(ViewState("selHours"))
                                selhours -= row("Hours")
                                ViewState("selHours") = selhours.ToString
                                txtHours2.Text = selhours.ToString

                                selcredits = Decimal.Parse(ViewState("selCredits"))
                                selcredits -= row("Credits")
                                ViewState("selCredits") = selcredits.ToString
                                txtCredits2.Text = selcredits.ToString

                                If IsCourseWeightForGPAEnabled() Then
                                    Dim courseName = item.Text
                                    CourseWeigthDatatable = ViewState("CourseWeightTable")
                                    Dim drCW As DataRow = CourseWeigthDatatable.Select("CourseName = '" & courseName & "' ", "").FirstOrDefault()

                                    If drCW IsNot Nothing Then
                                        CourseWeigthDatatable.Rows.Remove(drCW)
                                    End If

                                End If
                            End If

                            If IsCourseWeightForGPAEnabled() And CourseWeightForGPA.Checked Then

                                courseWeightTable.DataSource = CourseWeigthDatatable
                                ViewState("CourseWeightTable") = CourseWeigthDatatable
                                courseWeightTable.Rebind()
                                CalculatePercentageTotal()


                                DisplayWarningMessage()
                            End If

                            dr = dt1.Rows.Find(item.Value.ToString)
                            If Not dr Is Nothing Then
                                iChildId = dr("ReqId").ToString
                                If dr.RowState <> DataRowState.Added Then
                                    '    '    'Mark the row as deleted
                                    dr.Delete()
                                Else
                                    dt1.Rows.Remove(dr)
                                End If
                            End If
                            'Remove the item from the lbxSelectCamps list
                            lbxSelected.Items.Remove(item)
                            'Save the dataset to session
                            Session("WorkingDS") = ds
                            thereAreSelectedItems = True
                            Exit For
                        Else
                            item.Selected = False
                            If (errString = String.Empty) Then
                                errString = item.Text
                            Else
                                errString = errString + "," + item.Text
                            End If

                        End If

                    End If
                    thereAreSelectedItems = False

                Next
                If (lbxSelected.Items.Count = 0) Then thereAreSelectedItems = False
            Loop

            If (errString <> String.Empty) Then
                DisplayErrorMessage(errString + errVerbiage)
            Else
                ddlGrdSysDetailId.SelectedIndex = 0
            End If

        End If
    End Sub

    Private Function CalculatePercentageTotal(ByVal Optional setUI As Boolean = True) As Decimal
        CourseWeigthDatatable = ViewState("CourseWeightTable")
        Dim totalPercentage As Decimal = 0
        For Each item In CourseWeigthDatatable.Rows
            totalPercentage += CType(item("CourseWeight"), Decimal)
        Next

        If setUI Then
            CourseWeightFooterValue.Text = totalPercentage.ToString("N") & " %"
        End If
        Return totalPercentage

    End Function

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If lbxAvailCourseGrps.SelectedIndex >= 0 Then
            '   Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim dt2 As DataTable
            Dim row As DataRow
            Dim selhours As Decimal
            Dim selcredits As Decimal
            Dim sCourseGrpId As String
            'Dim childID As String
            'Dim firstCommonReq As String
            Dim fac As New TranscriptFacade
            Dim foundCourse As String
            Dim errorMessage As String

            'Add selected item to the relevant assigned list. We want to ensure that the course group does not 
            'have any reqs that belong to the selected items. We will build a collection of ReqInfo objects
            'based on the items in the selected list. We will then check if the course group has any common
            'course with the collection. If it does we will display an error message letting the user know
            'that there are common courses.

            ddlGrdSysDetailId.Enabled = False

            Dim thereAreSelectedItems As Boolean = True
            Do While (thereAreSelectedItems)



                For Each item As ListItem In lbxAvailCourseGrps.Items
                    If (item.Selected) Then

                        foundCourse = fac.GetFirstCommonRequirement(item.Value.ToString, BuildReqInfoCollection)
                        If foundCourse = "" Then
                            'Add selected item to the relevant assigned list. 
                            lbxSelected.Items.Add(New ListItem(item.Text, item.Value))

                            'Get the dataset from session
                            ds = Session("WorkingDS")
                            'retrieve the datatable from the ds
                            dt = ds.Tables("FldsSelected")

                            Dim dvrs As DataViewRowState
                            dvrs = DataViewRowState.Deleted
                            Dim arows As DataRow() = dt.Select("ReqId = '" & item.Value.ToString & "' ", "", dvrs)
                            If arows.Length > 0 Then
                                dt.Rows.Remove(arows(0))
                            End If

                            dr = dt.NewRow()
                            dr("ReqId") = item.Value.ToString
                            dr("ReqSeq") = lbxSelected.Items.Count
                            dr("IsRequired") = 1
                            dr("ReqTypeId") = 2
                            'Always set the Track for Completion to True for a CourseGrp (as per Troy 04/07/04)
                            dr("TrkForCompletion") = True

                            'Get hours and credits of the selected coursegrp from datatable.
                            dt2 = ds.Tables("CourseGrps")
                            sCourseGrpId = item.Value.ToString

                            row = dt2.Rows.Find(sCourseGrpId)
                            dr("Hours") = row("Hours")
                            dr("Credits") = row("Credits")
                            dr("Descrip") = item.Text
                            'Get hours and credits of the selected course from datatable.
                            dt.Rows.Add(dr)

                            If IsCourseWeightForGPAEnabled() Then
                                'add row to table for weights
                                CourseWeigthDatatable = GetTable(dt)
                                ViewState("CourseWeightTable") = CourseWeigthDatatable
                                courseWeightTable.DataSource = CourseWeigthDatatable
                                courseWeightTable.Rebind()

                                CalculatePercentageTotal()
                            End If

                            'Save the dataset to session?
                            Session("WorkingDS") = ds

                            '*******************************************************************
                            'Everytime we select a coursegrp, we want to populate
                            'the Hours and Credits associated with that 
                            'particular coursegrp.

                            'Get the dataset from session
                            ds = Session("WorkingDS")
                            'retrieve the datatable from the ds
                            dt2 = ds.Tables("CourseGrps")
                            '*******************************************************************

                            sCourseGrpId = item.Value.ToString

                            row = dt2.Rows.Find(sCourseGrpId)

                            'Retrieve the hours and credits attached to this course.
                            If (ViewState("selHours") <> "") Then
                                selhours = Decimal.Parse(ViewState("selHours"))
                            Else
                                selhours = 0
                            End If
                            selhours += row("Hours")
                            ViewState("selHours") = selhours.ToString
                            txtHours2.Text = selhours.ToString

                            If (ViewState("selCredits") <> "") Then
                                selcredits = Decimal.Parse(ViewState("selCredits"))
                            Else
                                selcredits = 0
                            End If
                            selcredits += row("Credits")
                            ViewState("selCredits") = selcredits.ToString
                            txtCredits2.Text = selcredits.ToString

                            Session("WorkingDS") = ds
                            'Remove the item from the lbxAvailCamps list
                            lbxAvailCourseGrps.Items.Remove(item)


                        Else
                            'Display error message to the user with course that is already in the hierarchy
                            errorMessage = "Cannot add " & lbxAvailCourseGrps.SelectedItem.Text & "." & vbCr
                            errorMessage &= "It contains " & foundCourse & " which is already part of the selected items."
                            DisplayErrorMessage(errorMessage)
                            Exit Sub
                        End If

                        thereAreSelectedItems = True
                        Exit For
                    End If
                    thereAreSelectedItems = False
                Next
                If (lbxAvailCourseGrps.Items.Count = 0) Then thereAreSelectedItems = False
            Loop

        End If
    End Sub

    Private Sub btnDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetails.Click

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim i As Integer
        'Dim strGuid As String
        'Dim Description As String
        'Dim Code As String
        Dim sb As New System.Text.StringBuilder
        Dim dt1, dt2 As DataTable ', dt3 
        Dim dr As DataRow
        Dim originalDR As DataRow
        Dim objCommon As New CommonUtilities
        ' Dim iCampusId As String
        Dim ds1 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim dv2 As New DataView
        'Dim strValue As String
        'Dim intPos As Integer
        Dim facade As New ProgFacade
        Dim ChildObj As ChildInfo
        Dim ds As New DataSet
        Dim prgVerId As String
        Dim prgverhrs As Decimal
        Dim prgvercrds As Decimal
        Dim childhrs As Decimal
        Dim childcrds As Decimal
        '     Dim BR As ProgBL
        Dim grade As String
        Dim sModDate As String
        Dim NoStudentEnrolledInPrgVer As Boolean
        Dim ValEnum As New ProgBL.ValResult

        dim success as Boolean = true 

        If facade.DoesPrgVerHaveStdsEnrolled(txtProgVerId.Text) = 0 Then
            NoStudentEnrolledInPrgVer = True
        Else
            NoStudentEnrolledInPrgVer = False
        End If

        prgVerId = txtProgVerId.Text
        'prgverhrs = CInt(txtHours.Text)
        'prgvercrds = CInt(txtCredits.Text)
        'If (txtHours2.Text <> "") Then
        '    childhrs = CInt(txtHours2.Text)
        'End If
        'If (txtCredits2.Text <> "") Then
        '    childcrds = CInt(txtCredits2.Text)
        'End If
        prgverhrs = Decimal.Parse(txtHours.Text)
        prgvercrds = Decimal.Parse(txtCredits.Text)
        If (txtHours2.Text <> "") Then
            childhrs = Decimal.Parse(txtHours2.Text)
        End If
        If (txtCredits2.Text <> "") Then
            childcrds = Decimal.Parse(txtCredits2.Text)
        End If

        Try

            'Pull the dataset from Session.
            ds = Session("WorkingDS")

            'Run getchanges method on the dataset to see which rows have changed.
            'This section handles the changes to the Advantage Required fields
            dt1 = ds.Tables("FldsSelected")
            dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted Or DataRowState.Modified)
            If IsNothing(dt2) Then
                UpdateGradeOverRide()
                Exit Sub
            End If
            ' CODE BELOW HANDLES AN INSERT/UPDATE INTO THE TABLE(S)

            ValEnum = facade.ValidateHrsCreds(prgverhrs, prgvercrds, childhrs, childcrds, ds)

            'ONLY IF WE THE HOURS AND CREDITS ARE VALIDATED CORRECTLY
            'USING THE BUSINESS LOGIC, GO AHEAD TO SAVE THIS RECORD TO 
            'THE DATABASE.
            If ((ValEnum = ProgBL.ValResult.Success) Or (ValEnum = ProgBL.ValResult.FailHrsCrdtsNtMet And NoStudentEnrolledInPrgVer) Or (ValEnum = ProgBL.ValResult.FailReqHrsCrdsOver And NoStudentEnrolledInPrgVer)) Then

                If Not IsNothing(dt2) Then
                    For Each dr In dt2.Rows
                        ChildObj = New ChildInfo
                        If dr.RowState = DataRowState.Added Then
                            Try
                                ChildObj.ChildId = dr("ReqId").ToString
                                If (dr("IsRequired") = True) Then
                                    ChildObj.Req = 1
                                Else
                                    ChildObj.Req = 0
                                End If
                                If (dr("TrkForCompletion") = True) Then
                                    ChildObj.TrkForCompletion = 1
                                Else
                                    ChildObj.TrkForCompletion = 0
                                End If
                                grade = dr("GrdSysDetailId").ToString

                                'If Left(dr("GrdSysDetailId").ToString, 6) <> "000000" Then
                                If dr("GrdSysDetailId").ToString <> "" Then
                                    ChildObj.GradeOverride = dr("GrdSysDetailId").ToString
                                End If
                                ChildObj.ChildSeq = dr("ReqSeq")
                                ChildObj.Hours = dr("Hours")
                                ChildObj.Credits = dr("Credits")

                                sModDate = facade.InsertCourseCourseGrp("PROGVER", ChildObj, prgVerId, Session("UserName"))

                                If sModDate <> "" Then
                                    'Get the sequence of the selected item
                                    originalDR = dt1.Rows.Find(dr("ReqId"))
                                    originalDR("ModDate") = CDate(sModDate)
                                End If

                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException(ex.InnerException.Message)
                            End Try
                        ElseIf dr.RowState = DataRowState.Deleted Then
                            ChildObj.ChildId = dr("ReqId", DataRowVersion.Original).ToString

                            facade.DeleteCourseCourseGrp("PROGVER", ChildObj, prgVerId)

                        ElseIf dr.RowState = DataRowState.Modified Then
                            ChildObj.ChildId = dr("ReqId").ToString
                            ChildObj.ChildSeq = dr("ReqSeq")
                            ChildObj.ModDate = dr("ModDate")
                            facade.UpdateCourseCourseGrp("PROGVER", ChildObj, prgVerId, Session("UserName"))
                        End If
                    Next
                    UpdateGradeOverRide()
                End If

                If IsCourseWeightForGPAEnabled() Then
                    Dim progVerFacade As New ProgVerFacade
                    progVerFacade.UpdateDoCourseWeightIntoOverallGPA(ViewState("PrgVerId"), CourseWeightForGPA.Checked)
                    CourseWeigthDatatable = ViewState("CourseWeightTable")
                    For Each item In CourseWeigthDatatable.Rows
                        Dim weight = If(CourseWeightForGPA.Checked, CType(item("CourseWeight"), Decimal), 0)
                        If IsDBNull(item("ProgramVersionDefinitionId")) Then
                            Dim programVersionDefinitionId As String = progVerFacade.GetProgramVersionDefinitionId(prgVerId, CType(item("RequirementId"), String))


                            progVerFacade.UpdateProgramVersionDefinitionWeightValue(programVersionDefinitionId, weight)
                        Else
                            progVerFacade.UpdateProgramVersionDefinitionWeightValue(CType(item("ProgramVersionDefinitionId"), String), weight)
                        End If
                    Next
                End If

                dt1.AcceptChanges()
                'Check if datatable and selected list box are synchronised. If not, then refresh datatable
                'with data from the database.
                SynchronizeDTAndListBox(dt1)

                Session("FldsSelected") = dt1
                If (facade.ValidateHrsCreds(prgverhrs, prgvercrds, childhrs, childcrds, ds) = ProgBL.ValResult.FailHrsCrdtsNtMet) Then
                    DisplayErrorMessage("Please note that this save was possible since there are " & vbCr &
                                        "no students enrolled in this Program, although " & vbCr &
                                        "total hours/ credits for the Courses" & vbCr &
                                        "is less than the total for the Program Version")
                    success = false
                ElseIf (facade.ValidateHrsCreds(prgverhrs, prgvercrds, childhrs, childcrds, ds) = ProgBL.ValResult.FailReqHrsCrdsOver) Then
                    DisplayErrorMessage("Please note that this save was possible since there are " & vbCr &
                                        "no students enrolled in this Program, although " & vbCr &
                                        "total hours/ credits for the required courses" & vbCr &
                                        "must be less than the total for the Program Version")
                    success = false
                End If

            ElseIf (facade.ValidateHrsCreds(prgverhrs, prgvercrds, childhrs, childcrds, ds) = ProgBL.ValResult.FailHrsCrdtsNtMet) Then
                DisplayErrorMessage("Total hours/ credits for the Courses is less than the total for the Program Version")
                success = false
                'Response.Write("hrs and credts for the courses/coursegrps is less than hrs & crdts of program version")
            ElseIf (facade.ValidateHrsCreds(prgverhrs, prgvercrds, childhrs, childcrds, ds) = ProgBL.ValResult.FailReqHrsCrdsOver) Then
                DisplayErrorMessage("Total hours/ credits for the required courses must be less than the total for the Program Version")
                success = false
                'Response.Write("hrs and credts for the required courses/coursegrps is less than hrs and crdts of program version")
            End If


            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)

            If (Not ViewState("ProgramRegistrationType") Is Nothing) Then

                Dim gradeScaleId As Guid?
                If (Not ViewState("GradeScaleId") Is Nothing) Then
                    gradeScaleId = Guid.Parse(ViewState("GradeScaleId").ToString())
                End If
                Dim programVersionRequest As New ProgramVersionRequest(tokenResponse.ApiUrl, tokenResponse.Token)
                Dim programVersionResponse As ActionResult(Of ProgramVersion) = programVersionRequest.SetProgramRegistrationType(CType(ViewState("ProgramRegistrationType"), Enums.ProgramRegistrationType),
                                                                                                                                 Guid.Parse(prgVerId),
                                                                                                                                 gradeScaleId)
                If (Not programVersionResponse Is Nothing) Then
                    If (programVersionResponse.ResultStatus = Enums.ResultStatus.Error Or programVersionResponse.ResultStatus = Enums.ResultStatus.NotFound) Then
                        Me.DisplayErrorMessage(programVersionResponse.ResultStatusMessage)
                        success = false
                    ElseIf (programVersionResponse.ResultStatus = Enums.ResultStatus.Warning) Then
                        Me.DisplayErrorMessage(programVersionResponse.ResultStatusMessage)
                        success = false
                    End If
                End If
            End If
            if (success)
                DisplayInfoMessage("The Program Version Definition has been saved!")
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        RadAjaxManager1.ResponseScripts.Add("$(document).ready(function(){MasterPage.SHOW_ERROR_WINDOW('" + errorMessage + "');});")
    End Sub
    
    Private Sub DisplayInfoMessage(ByVal message As String)

        '   Display error in message box in the client
        RadAjaxManager1.ResponseScripts.Add("$(document).ready(function(){MasterPage.SHOW_INFO_WINDOW('" + message + "');});")
    End Sub


    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        Dim count As Integer
        Dim selIndex As Integer
        Dim tmpTxt As String
        Dim tmpVal As String
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim row1, row2 As DataRow
        Dim tmpSeq, curSeq As Integer

        If lbxSelected.SelectedIndex > 0 Then
            For count = 0 To count = lbxSelected.Items.Count
                selIndex = lbxSelected.SelectedIndex
                tmpTxt = lbxSelected.Items(selIndex - 1).Text
                tmpVal = lbxSelected.Items(selIndex - 1).Value.ToString

                'You need to swap the sequence numbers of these elements as well
                ds = Session("WorkingDS")
                dt = ds.Tables("FldsSelected")

                'Get the sequence of the item above the selected item.
                row1 = dt.Rows.Find(lbxSelected.Items(selIndex - 1).Value)
                tmpSeq = row1("ReqSeq")

                'Get the sequence of the selected item
                row2 = dt.Rows.Find(lbxSelected.Items(selIndex).Value)
                curSeq = row2("ReqSeq")

                'Swap the sequences of the selected item w/ the item above in the datatable
                row2("ReqSeq") = tmpSeq
                row1("ReqSeq") = curSeq

                'Save the dataset to session
                Session("WorkingDS") = ds

                'Swap the items in the selected index w/ the index above.
                lbxSelected.Items(selIndex - 1).Text = lbxSelected.Items(selIndex).Text
                lbxSelected.Items(selIndex - 1).Value = lbxSelected.Items(selIndex).Value

                lbxSelected.Items(selIndex).Text = tmpTxt
                lbxSelected.Items(selIndex).Value = tmpVal

                lbxSelected.SelectedIndex = selIndex - 1

            Next
        End If
    End Sub

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        Dim count As Integer
        Dim selIndex As Integer
        Dim tmpTxt As String
        Dim tmpVal As String
        Dim row1, row2 As DataRow
        Dim dt As DataTable
        Dim ds As DataSet
        Dim tmpSeq, curSeq As Integer

        'Only if there is an item selected and it is not the last item in the list
        If lbxSelected.SelectedIndex >= 0 And lbxSelected.SelectedIndex < (lbxSelected.Items.Count - 1) Then
            For count = 0 To count = lbxSelected.Items.Count
                selIndex = lbxSelected.SelectedIndex
                tmpTxt = lbxSelected.Items(selIndex + 1).Text
                tmpVal = lbxSelected.Items(selIndex + 1).Value.ToString

                'You need to swap the sequence numbers of these elements as well
                ds = Session("WorkingDS")
                dt = ds.Tables("FldsSelected")

                'Get the sequence of the item below the selected item.
                row1 = dt.Rows.Find(lbxSelected.Items(selIndex + 1).Value)
                tmpSeq = row1("ReqSeq")

                'Get the sequence of the selected item
                row2 = dt.Rows.Find(lbxSelected.Items(selIndex).Value)
                curSeq = row2("ReqSeq")

                'Swap the sequences of the selected item w/ the item above in the datatable
                row2("ReqSeq") = tmpSeq
                row1("ReqSeq") = curSeq

                'Save the dataset to session
                Session("WorkingDS") = ds
                lbxSelected.Items(selIndex + 1).Text = lbxSelected.Items(selIndex).Text
                lbxSelected.Items(selIndex + 1).Value = lbxSelected.Items(selIndex).Value

                lbxSelected.Items(selIndex).Text = tmpTxt
                lbxSelected.Items(selIndex).Value = tmpVal

                lbxSelected.SelectedIndex = selIndex + 1

            Next
        End If
    End Sub

    Private Sub lbxAvailCourses_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxAvailCourses.SelectedIndexChanged
        'Enable the edit button
        'chkTrkForCompletion.Visible = True
        chkTrkForCompletion.Enabled = True
        'lblGrdOverride.Visible = True
        'ddlGrdSysDetailId.Visible = True
        'PopulateGrdOverddl()
    End Sub

    Private Sub PopulateGrdOverddl()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New ProgFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlGrdSysDetailId
            .DataSource = facade.GetGrdSysDetails(txtProgVerId.Text)
            .DataTextField = "Grade"
            '.DataValueField = "GrdSystemId"
            .DataValueField = "GrdSysDetailId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select Grade", ""))
            .Items.Insert(1, New ListItem(txtMinGrdReq.Text, txtMinGradeId.Text))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub SynchronizeDTAndListBox(ByVal DT As DataTable)
        'Check each item in the selected list if it is a course group. If it is see if the course passed
        'in exists in it.
        Dim strFound As String = ""
        Dim i As Integer
        'Dim dr As DataRow
        'Dim dtCsrGrpReqs As DataTable
        Dim objCommon As New CommonUtilities
        '   Dim item As ListItem
        Dim ds As DataSet
        Dim facade As New ProgFacade

        'For each item in the collection. If the item is a course then we will check the dt1 datatable
        'to see if it exists there. If the item is a course group then we will populate dt2 with the
        'reqs for it. We will then loop through and see if there are any common courses.
        For i = 0 To lbxSelected.Items.Count - 1
            Dim row2 As DataRow = DT.Rows.Find(lbxSelected.Items(i).Value.ToString)
            If IsNothing(row2) Then

                'Refresh the datatable.
                'Get all courses and coursegprs assigned to a particular prog version
                ds = facade.GetCourseCourseGrpsAssigned("PROGVER", txtProgVerId.Text)

                'Get Child Descriptions, populate hours and credits for selected
                'courses/coursegrps and determine whether a course/course grp is
                'required or elective.
                ds = facade.ProcessHrsCrdtsDescrips(ds)

                DT = ds.Tables("FldsSelected").Copy
            End If
        Next
        Session("FldsSelected") = DT

    End Sub
    Private Function CourseExistsInSelectedList(ByVal reqId As String) As String
        'Check each item in the selected list if it is a course group. If it is see if the course passed
        'in exists in it.
        Dim strFound As String = ""
        Dim i As Integer
        Dim dt As DataTable
        Dim fac As New TranscriptFacade
        '  Dim dtCsrGrpReqs As DataTable
        Dim objCommon As New CommonUtilities

        If lbxSelected.Items.Count = 0 Then
            Return ""
        Else
            dt = Session("WorkingDS").Tables("FldsSelected")
            For i = 0 To lbxSelected.Items.Count - 1
                'Check if the current item is a course group
                Dim arows As DataRow() = dt.Select("ReqId = '" & lbxSelected.Items(i).Value.ToString & "' ")
                If arows(0)("ReqTypeId") = 2 Then
                    'Check if the course exists in the course group
                    If fac.DoesReqExistsInCourseGroup(lbxAvailCourses.SelectedItem.Value.ToString, lbxSelected.Items(i).Value.ToString) Then
                        Return lbxSelected.Items(i).Text
                    End If
                End If
            Next

            Return ""
        End If

    End Function
    Private Function BuildReqInfoCollection() As ArrayList
        Dim arrReqInfo As New ArrayList
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim rInfo As New ReqInfo

        'Get the dataset from session
        ds = Session("WorkingDS")
        'retrieve the datatable from the ds
        dt = ds.Tables("FldsSelected")

        If lbxSelected.Items.Count > 0 Then
            For i = 0 To lbxSelected.Items.Count - 1
                'Find each item in the dt datatable and get the relevant properties
                Dim arows As DataRow() = dt.Select("ReqId = '" & lbxSelected.Items(i).Value.ToString & "' ")
                rInfo.ReqTypeId = arows(0)("ReqTypeId").ToString()
                rInfo.ReqId = arows(0)("ReqId").ToString()
                rInfo.ReqDescription = lbxSelected.Items(i).Text
                'Add the object to the arraylist
                arrReqInfo.Add(rInfo)
            Next
        End If

        Return arrReqInfo
    End Function
    Private Function GetWebConfigValueForAllowAddingCoursesToProgramVersionsAfterStudentsHaveBeenEnrolled() As Boolean

        'return boolean value
        Return MyAdvAppSettings.AppSettings("AllowAddingCoursesToProgramVersionsAfterStudentsHaveBeenEnrolled")

    End Function

    Private Function GetWebConfigValueForGradeBookWeightingLevel() As String

        'return boolean value
        Return MyAdvAppSettings.AppSettings("GradeBookWeightingLevel")

    End Function

    Private Function GetWebConfigValueForGradesFormat() As String

        'return boolean value
        Return MyAdvAppSettings.AppSettings("GradesFormat")

    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    Protected Sub lbxSelected_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbxSelected.SelectedIndexChanged
        Dim fac As New TranscriptFacade
        Dim grade As String
        If (lbxSelected.SelectedIndex <> -1) Then
            If (checkMultiSelect(lbxSelected) = False) Then
                Dim intReturnTotalCount As Integer = 0
                intReturnTotalCount = fac.GetCourseGroup(lbxSelected.SelectedValue)
                If intReturnTotalCount >= 1 Then
                    ddlGrdSysDetailId.SelectedIndex = 0
                    ddlGrdSysDetailId.Enabled = False
                Else
                    ddlGrdSysDetailId.Enabled = True
                    grade = fac.OverrideGrd(txtProgVerId.Text, lbxSelected.SelectedValue)
                    If (grade <> "00000000-0000-0000-0000-000000000000") Then
                        ddlGrdSysDetailId.SelectedValue = grade
                    Else
                        ddlGrdSysDetailId.SelectedIndex = 0
                    End If
                End If
            Else
                ddlGrdSysDetailId.SelectedIndex = 0
                ddlGrdSysDetailId.Enabled = False
            End If
        Else
            ddlGrdSysDetailId.SelectedIndex = 0
        End If

    End Sub

    Private Function checkMultiSelect(ByVal lbx As WebControls.ListBox) As Boolean
        Dim rtn As Boolean = False
        Dim i As Integer
        Dim selectCount As Integer = 0
        For i = 0 To lbx.Items.Count - 1
            If (lbx.Items(i).Selected = True) Then
                selectCount = selectCount + 1
            End If
            If (selectCount > 1) Then
                rtn = True
                Exit For
            End If
        Next
        Return rtn
    End Function
    Private Sub UpdateGradeOverRide()
        Dim fac As New TranscriptFacade
        If (lbxSelected.SelectedIndex <> -1) Then
            If (checkMultiSelect(lbxSelected) = False) Then

                fac.UpdateOverRideGrade(ddlGrdSysDetailId.SelectedValue, txtProgVerId.Text, lbxSelected.SelectedValue)
            End If
        End If

    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub
    Private Sub InitButtonsForLoad(ByVal isSupport As Boolean, ByVal isSa As Boolean)
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        If Not isSupport And Not isSa Then
            If pObj.HasDisplay Then
                btnRequired.Enabled = False
                btnElective.Enabled = False
                btnAdd.Enabled = False
                btnDelete.Enabled = False
                btnRemove.Enabled = False
                btnUp.Enabled = False
                btnDown.Enabled = False
            End If
        Else
            btnRequired.Enabled = True
            btnElective.Enabled = True
            btnAdd.Enabled = True
            btnDelete.Enabled = True
            btnRemove.Enabled = True
            btnUp.Enabled = True
            btnDown.Enabled = True
        End If

    End Sub
    Protected Sub CourseWeightForGPA_OnCheckedChanged(sender As Object, e As EventArgs) Handles CourseWeightForGPA.CheckedChanged
        Dim ds = Session("WorkingDS")
        'retrieve the datatable from the ds
        Dim dt = ds.Tables("FldsSelected")

        CourseWeigthDatatable = GetTable(dt)
        ViewState("CourseWeightTable") = CourseWeigthDatatable
        courseWeightTable.DataSource = CourseWeigthDatatable
        courseWeightTable.Rebind()

        CalculatePercentageTotal()
        ToogleCourseWeightForGPA()
        DisplayWarningMessage()
    End Sub

    Private Sub ToogleCourseWeightForGPA()

        'Enable Course Weigth Overall into GPA
        Dim EnableCourseWeigthForGPA = IsCourseWeightForGPAEnabled()
        If EnableCourseWeigthForGPA Then
            Dim isCourseWeightOverallChecked = CourseWeightForGPA.Checked
            courseWeightTable.Visible = isCourseWeightOverallChecked
            CourseWeightFooterLabel.Visible = isCourseWeightOverallChecked
            CourseWeightFooterValue.Visible = isCourseWeightOverallChecked

            CalculatePercentageTotal()
        End If

    End Sub

    Protected Sub CourseWeightTextBox_OnTextChanged(sender As Object, e As EventArgs)
        Dim programVersionDefinitionId = sender.Attributes("data-definitionId")
        Dim requirementId = sender.Attributes("data-requirementId")
        CourseWeigthDatatable = ViewState("CourseWeightTable")
        Dim drCW As DataRow = CourseWeigthDatatable.Select("ProgramVersionDefinitionId = '" & programVersionDefinitionId & "' or RequirementId = '" & requirementId & "'", "").FirstOrDefault()

        If drCW IsNot Nothing Then


            Dim newValue As Decimal = CType(sender.Text, Decimal)
            drCW("CourseWeight") = newValue
            Dim newTotal = CalculatePercentageTotal(True)

            If newTotal > 100 Then
                Dim adjustedValue As Decimal = 100 - (newTotal - newValue)
                drCW("CourseWeight") = adjustedValue
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Total weight percentage cannot exceed 100%. Your recent input was set to the maximum value possible.')", True)
            End If

            DisplayWarningMessage()

        End If
    End Sub

    Private Sub DisplayWarningMessage()
        Dim updatedTotal = CalculatePercentageTotal(True)
        If updatedTotal < 100 And CourseWeightForGPA.Checked Then
            btnSave.Enabled = False
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Total weight percentage is not equal to 100%. In order to save, total weight percentage must be 100%.')", True)
        ElseIf updatedTotal = 100 And CourseWeightForGPA.Checked Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = True
        End If

        ViewState("CourseWeightTable") = CourseWeigthDatatable
        courseWeightTable.DataSource = CourseWeigthDatatable
        courseWeightTable.Rebind()
        'change value on data table as well
        CalculatePercentageTotal()
    End Sub
End Class