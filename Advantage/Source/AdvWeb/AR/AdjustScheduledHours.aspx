﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="AdjustScheduledHours.aspx.vb" Inherits="AR_AdjustScheduledHours" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

    <link href="../css/AdvantageValidator.css" rel="stylesheet" />
    <link href="../css/KendoSplitterOverrides.css" rel="stylesheet" />
    <script src="../Scripts/ElementQueries.min.js"></script>
    <script src="../Scripts/ResizeSensor.min.js"></script>
    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>
    <script type="text/javascript" src="../Scripts/Fame.Advantage.Common.js"></script>
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>

    <style>
        #horizontal {
            height: calc(100% - 26px);
        }

        #vertical {
            border-width: 0;
        }

        #panels {
            margin-left: 20px;
            text-align: center;
        }

            #panels li {
                margin-right: 10px;
                margin-bottom: 10px;
                list-style: none;
                float: left;
            }

        .formRow {
            padding: 10px;
            margin-left: 20px;
        }

        .left-pane-title {
            vertical-align: -webkit-baseline-middle
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var adjustScheduledHours = new Api.ViewModels.AcademicRecords.Attendance.AdjustScheduledHours();
            adjustScheduledHours.initialize();
        });
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <div id="horizontal">
        <div id="left-pane">
            <div class="pane-content">

                <div class="k-header k-shadow" style="text-align: center; height: 25px;">
                    <span class="left-pane-title" style="font-size: 14px;">Adjust Scheduled Hours - Options</span>
                </div>

                <div style="text-align: center; margin: 25px;">
                    <div style="margin-top: 10px; margin-bottom: 10px;">
                        Campus 
                    </div>
                    <div>
                        <input id="ddlCampus" class="fieldlabel1 inputCustom" style="width: 80%; display: inline-block;" required />
                    </div>
                </div>

                <div style="text-align: center; margin: 25px;">
                    <div style="margin-top: 10px; margin-bottom: 10px;">
                        Date of Adjustment
                    </div>
                    <div>
                        <input id="dpDateOfAdjustment" class="inputCustom" style="width: 80%; text-align: center;" required />
                    </div>
                </div>

                <div style="text-align: center; margin: 25px;">
                    <div style="margin-top: 10px; margin-bottom: 10px;">
                        Select Adjustment Method 
                    </div>
                    <div>
                        <select class="fieldlabel1 inputCustom" id="ddlSelectAdjustmentMethod" style="width: 80%; display: inline-block;" required>
                            <option value="1">Remove scheduled hours</option>
                            <option value="2">Adjust scheduled hours by: "0.00"</option>
                            <option value="3">Set scheduled hours to equal actual hours</option>
                            <option value="4">Revert scheduled hours prior to any adjustment</option>
                        </select>
                    </div>
                </div>

                <div id="hoursAmountContainer" style="text-align: center; margin: 25px; display: none;">
                    <div style="margin-top: 10px; margin-bottom: 10px;">
                        Amount:
                    </div>
                    <div>
                        <input id="txtHoursAdjustment" class="inputCustom form-control k-textbox" style="width: 80px;" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                    </div>
                </div>

                <div style="text-align: center; margin: 25px;">
                    <div style="margin-top: 10px; margin-bottom: 10px;">
                        Define a Schedule to Adjust
                    </div>
                    <div>
                        <select class="fieldlabel1 inputCustom" id="ddlDefineScheduleAdjust" style="width: 80%; display: inline-block;" required>
                            <option value="1">All attending students</option>
                            <option value="2">Students Group</option>
                            <option value="3">In a Program of Study</option>
                        </select>
                    </div>
                </div>

                <div id="studentGroupsContainer" style="text-align: center; margin: 25px; display: none;">
                    <div style="margin-top: 10px; margin-bottom: 10px;">
                        Student Groups
                    </div>
                    <div>
                        <input id="ddlStudentGroups" class="fieldlabel1 inputCustom" style="width: 80%; display: inline-block;" required />
                    </div>
                </div>

                <div id="programVersionContainer" style="text-align: center; margin: 25px; display: none;">
                    <div style="margin-top: 10px; margin-bottom: 10px;">
                        Program Version 
                    </div>
                    <div>
                        <input id="ddlProgramVersion" class="fieldlabel1 inputCustom" style="width: 80%; display: inline-block;" required />
                    </div>
                </div>

                <div style="text-align: center;">
                    <button id="searchBtn" type="button" class="k-button">Show Students</button>
                </div>

            </div>
        </div>

        <div id="center-pane" class="flexGrid-container">
            <div class="pane-content flexGrid">
                <div class="formRow flexGrid-filter">
                    <div class="inlineBlock" style="float: right;">
                        <button class="save" id="btnSave">Save</button>
                        <button class="new" id="btnNew">New</button>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
                <div id="studentsGrid">
                </div>
            </div>
        </div>
    </div>

</asp:Content>
