

' ===============================================================================
'
' FAME AdvantageV1
'
' TimeClockSpecialCode.aspx.vb
'
' ===============================================================================
' Copyright (C) 2005 - 2012 FAME Inc.
' All rights reserved.
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common.TimeClock
Imports FAME.AdvantageV1.BusinessFacade.TimeClock
Imports FAME.Advantage.Common
Imports System.Collections
Imports System.Data
Imports BO = Advantage.Business.Objects

Partial Class TimeClockSpecialCode
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected resourceId As String
    Protected userId As String
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Not IsPostBack Then
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList()

            '   bind an empty new TimeClockSpecialCodeInfo
            BindTimeClockSpecialCodeData(New TimeClockSpecialCodeInfo)

            'Header1.EnableHistoryButton(False)

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")

        End If

        ' DE7503 6/1/2012 Janet Robinson added super 
        If AdvantageSession.UserState.UserName.ToString.ToLower = "support" OrElse AdvantageSession.UserState.UserName.ToString.ToLower = "super" Then
            ' 11/10/2011 Janet Robinson Clock Hr Project - only support can perform maintenance 
            '   initialize buttons
            InitButtonsForLoad()
            rblPunchType.Enabled = True
            txtTCSpecialCode.Enabled = True
            ddlInstructionType.Enabled = True
            ddlCampusId.Enabled = True
            ddlStatusId.Enabled = True
            'txtTCSpecialCode.BackColor = Color.FromName("#ffff99")
            'ddlCampusId.BackColor = Color.FromName("#ffff99")
            'ddlInstructionType.BackColor = Color.FromName("#ffff99")
        Else
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            rblPunchType.Enabled = False
            txtTCSpecialCode.Enabled = False
            ddlInstructionType.Enabled = False
            ddlCampusId.Enabled = False
            ddlStatusId.Enabled = False
        End If


    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind Time Clock Special Code datalist
        dlstTimeClockSpecialCode.DataSource = New DataView((New TimeClockFacade).GetAllClockSpecialCodes().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstTimeClockSpecialCode.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusDDL()
        BuildInstructionTypesDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildCampusDDL()
        '   bind the Campus DDL
        'Dim campuses As New SingletonAppSettings

        With ddlCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataSource = MyAdvAppSettings.GetAllCampuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildInstructionTypesDDL()
        '   bind the InstructionTypes DDL

        With ddlInstructionType
            .DataTextField = "InstructionTypeDescrip"
            .DataValueField = "InstructionTypeId"
            .DataSource = (New InstructionTypeFacade).GetInstructionTypesByStatus("true")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dlstTimeClockSpecialCode_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstTimeClockSpecialCode.ItemCommand

        '   get the TCSpecialCode from the backend and display it
        GetClockSpecialCodeInfo(e.CommandArgument)

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstTimeClockSpecialCode, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        ' DE7503 6/1/2012 Janet Robinson added super 
        If AdvantageSession.UserState.UserName.ToString.ToLower = "support" OrElse AdvantageSession.UserState.UserName.ToString.ToLower = "super" Then
            ' 11/10/2011 Janet Robinson Clock Hr Project - only support can perform maintenance 
            InitButtonsForEdit()
        End If

        ' US3037 5/8/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstTimeClockSpecialCode, e.CommandArgument)

    End Sub
    Private Sub BindTimeClockSpecialCodeData(ByVal ClockSpecialCodeData As TimeClockSpecialCodeInfo)
        With ClockSpecialCodeData
            chkIsInDB.Checked = .IsInDB
            txtTCSpecialCode.Text = .TCSpecialCode
            txtOrigTCSpecialCode.Text = .TCSpecialCode
            txtTCSId.Text = .TCSId
            txtOrigTCSId.Text = .OrigTCSId
            If Not (ClockSpecialCodeData.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = ClockSpecialCodeData.StatusId
            rblPunchType.SelectedValue = .PunchType
            If Not (ClockSpecialCodeData.CampusId = Guid.Empty.ToString) Then ddlCampusId.SelectedValue = ClockSpecialCodeData.CampusId Else ddlCampusId.SelectedIndex = 0
            If Not (ClockSpecialCodeData.InstructionTypeId = Guid.Empty.ToString) Then ddlInstructionType.SelectedValue = ClockSpecialCodeData.InstructionTypeId Else ddlInstructionType.SelectedIndex = 0
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        If Not Page.IsValid Then Exit Sub
        Dim result As String
        If txtOrigTCSpecialCode.Text.Trim = String.Empty OrElse (txtOrigTCSpecialCode.Text.Trim <> String.Empty And txtOrigTCSpecialCode.Text.Trim <> txtTCSpecialCode.Text.Trim) Then
            ' check for duplicate Time Clock Special Code and Campus combination
            With New TimeClockFacade
                result = .CheckTimeClockSpecialCodeDup(txtTCSpecialCode.Text, ddlCampusId.SelectedValue)
            End With
            If result <> "" Then
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error1", result, "Save Error")
                Exit Sub
            End If
            ' check for same Time Clock Special Code being used for both punch in and punch out
            With New TimeClockFacade
                result = .CheckTimeClockSpecialCodePunchDup(txtTCSpecialCode.Text, rblPunchType.SelectedValue)
            End With
            If result <> "" Then
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error2", result, "Save Error")
                Exit Sub
            End If
        End If
        With New TimeClockFacade
            '   update Time Clock Special Code Info 
            result = .UpdateClockSpecialCodeInfo(BuildTimeClockSpecialCodeInfo(txtTCSId.Text), AdvantageSession.UserState.UserName)
        End With

        '   bind the datalist
        BindDataList()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstTimeClockSpecialCode, txtTCSId.Text, ViewState, Header1)

        If Not result = "" Then
            '   Display Error Message
            'DisplayErrorMessage(result)
            DisplayRADAlert(CallbackType.Postback, "Error3", result, "Save Error")
            Exit Sub
        Else
            '   get the TCSId from the backend and display it
            GetClockSpecialCodeInfo(txtTCSId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            '   initialize buttons
            'InitButtonsForLoad()
            ' DE7503 6/1/2012 Janet Robinson added super 
            If AdvantageSession.UserState.UserName.ToString.ToLower = "support" OrElse AdvantageSession.UserState.UserName.ToString.ToLower = "super" Then
                ' 11/10/2011 Janet Robinson Clock Hr Project - only support can perform maintenance 
                InitButtonsForEdit()
            End If

            ' US3037 5/8/2012 Janet Robinson
            CommonWebUtilities.RestoreItemValues(dlstTimeClockSpecialCode, txtTCSId.Text)

        End If
    End Sub
    Private Function BuildTimeClockSpecialCodeInfo(ByVal TCSId As String) As TimeClockSpecialCodeInfo
        'instantiate class
        Dim TimeClockSpecialCodeInfo As New TimeClockSpecialCodeInfo

        With TimeClockSpecialCodeInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get TCSpecialCode
            .TCSpecialCode = txtTCSpecialCode.Text.Trim

            'get TCSId
            .TCSId = txtTCSId.Text.Trim

            'get Orig TCSId
            .OrigTCSId = txtOrigTCSId.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get TCS Punch Type
            .PunchType = rblPunchType.SelectedValue

            'get Campus Group
            .CampusId = ddlCampusId.SelectedValue

            'get Instruction Type
            .InstructionTypeId = ddlInstructionType.SelectedValue

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return TimeClockSpecialCodeInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        '   bind an empty new TimeClockSpecialCodeInfo Info
        BindTimeClockSpecialCodeData(New TimeClockSpecialCodeInfo)

        rblPunchType.SelectedIndex = -1

        txtTCSId.Text = System.Guid.NewGuid.ToString
        txtOrigTCSpecialCode.Text = String.Empty

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstTimeClockSpecialCode, Guid.Empty.ToString, ViewState, Header1)

        'initialize buttons
        ' DE7503 6/1/2012 Janet Robinson added super 
        If AdvantageSession.UserState.UserName.ToString.ToLower = "support" OrElse AdvantageSession.UserState.UserName.ToString.ToLower = "super" Then
            ' 11/10/2011 Janet Robinson Clock Hr Project - only support can perform maintenance 
            InitButtonsForLoad()
        End If

        ' US3037 5/8/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstTimeClockSpecialCode, Guid.Empty.ToString)

    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        If Not (txtTCSId.Text = Guid.Empty.ToString) Then
            'update Time Clock Special Code Info 
            Dim result As String = (New TimeClockFacade).DeleteClockSpecialCodeInfo(txtTCSId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error4", result, "Delete Error")
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new TimeClockSpecialCodeInfo
                BindTimeClockSpecialCodeData(New TimeClockSpecialCodeInfo)

                '   initialize buttons
                ' DE7503 6/1/2012 Janet Robinson added super 
                If AdvantageSession.UserState.UserName.ToString.ToLower = "support" OrElse AdvantageSession.UserState.UserName.ToString.ToLower = "super" Then
                    ' 11/10/2011 Janet Robinson Clock Hr Project - only support can perform maintenance 
                    InitButtonsForLoad()
                End If

                ' US3037 5/8/2012 Janet Robinson
                CommonWebUtilities.RestoreItemValues(dlstTimeClockSpecialCode, Guid.Empty.ToString)

            End If

        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If
    End Sub
    Private Sub GetClockSpecialCodeInfo(ByVal TCSId As String)

        '   bind TCSpecialCode properties
        BindTimeClockSpecialCodeData((New TimeClockFacade).GetClockSpecialCodeInfo(TCSId))

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList()
        'Header1.EnableHistoryButton(False)
        txtTCSId.Text = String.Empty
        txtOrigTCSId.Text = String.Empty
        txtTCSpecialCode.Text = String.Empty
        rblPunchType.SelectedIndex = -1
        ddlCampusId.SelectedIndex = 0
        ddlInstructionType.SelectedIndex = 0
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button
        controlsToIgnore.Add(btnSave)

        'Add javascript code to warn the user about non saved changes
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
