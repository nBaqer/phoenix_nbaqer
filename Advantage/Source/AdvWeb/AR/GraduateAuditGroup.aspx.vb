﻿Imports System.Collections
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports Telerik.Web.UI

Namespace AdvWeb.AR

    Partial Class GraduateAuditGroup
        Inherits BasePage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()

        End Sub
        Protected StudentId As String
        Protected ResourceId As Integer
        Protected ModuleId As String
        Protected CampusId As String
        Protected UserId As String
        Protected State As AdvantageSessionState
        Protected LeadId As String
        Protected BoolSwitchCampus As Boolean = False
        Protected MyAdvAppSettings As AdvAppSettings



        Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
            AdvantageSession.PageTheme = PageTheme.Blue_Theme

        End Sub
#Region "MRU Routines"
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
      
            ScriptManager.GetCurrent(Page).RegisterPostBackControl(btnExportPdf)
        End Sub
        Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As Advantage.Business.Objects.StudentMRU

            Dim objStudentState As New Advantage.Business.Objects.StudentMRU

            Try

                GlobalSearchHandler(0)

                BoolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

                If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                    StudentId = Guid.Empty.ToString()
                Else
                    StudentId = AdvantageSession.MasterStudentId
                End If

                If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                    LeadId = Guid.Empty.ToString()
                Else
                    LeadId = AdvantageSession.MasterLeadId
                End If


                With objStudentState
                    .StudentId = New Guid(StudentId)
                    .LeadId = New Guid(LeadId)
                    .Name = AdvantageSession.MasterName
                End With

                HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
                HttpContext.Current.Items("Language") = "En-US"

                Master.ShowHideStatusBarControl(True)





            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Dim strSearchUrl As String
                strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
                Response.Redirect(strSearchUrl)
            End Try

            Return objStudentState

        End Function
#End Region

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load

            MyAdvAppSettings = AdvAppSettings.GetAppSettings()
            CampusId = Master.CurrentCampusId

            '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
            'Get StudentId and LeadId
            Dim objStudentState As Advantage.Business.Objects.StudentMRU
            objStudentState = getStudentFromStateObject(286) 'Pass resource-id so that user can be redirected to same page while switching students
            If objStudentState Is Nothing Then
                RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If

            With objStudentState
                StudentId = .StudentId.ToString
                LeadId = .LeadId.ToString

            End With

            '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''
            'Always disable the History button. It does not apply to this page.
            'Header1.EnableHistoryButton(False)

            If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(uSearchEntityControlId.Value)) Then
                ChkIsInDB.Checked = True
                btnSave.Enabled = False
                btnNew.Enabled = False
                btnDelete.Enabled = False
                'BindDataList()
                BindDropDownList(StudentId)

                'We want to select the first item since most students will only have one enrollment. 
                'The enrollments are ordered starting with the latest enroll date. We have to code for the
                'possibility that there might be a student without any enrollment.
                If ddlEnrollmentId.Items.Count > 0 Then
                    If ddlEnrollmentId.Items.Count > 1 Then

                        Dim datasource As List(Of EnrollmentItemsDto) = CType(ddlEnrollmentId.DataSource, List(Of EnrollmentItemsDto))
                        Dim enrollmentAll As EnrollmentItemsDto = New EnrollmentItemsDto()
                        enrollmentAll.PrgVerDescrip = "Show All"
                        enrollmentAll.StuEnrollId = Guid.Empty.ToString()
                        enrollmentAll.PrgVerId = Guid.Empty.ToString()
                        datasource.Insert(0, enrollmentAll)
                        ddlEnrollmentId.DataSource = datasource
                        ddlEnrollmentId.DataBind()
                    End If

                    ddlEnrollmentId.SelectedIndex = 0
                    lblEnrollmentId.Text = If(ddlEnrollmentId.SelectedItem.Value = Guid.Empty.ToString(), "All Enrollments", "Enrollments")

                    'Generate the table for the selected enrollment
                    GenerateGraduateAuditTable()

                    If BoolSwitchCampus = True Then
                        CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
                    End If

                    uSearchEntityControlId.Value = ""
                Else
                    Dim gridDataSource As List(Of GraduateAuditGroupRow) = New List(Of GraduateAuditGroupRow)()
                    masterGrid.DataSource = gridDataSource
                    masterGrid.DataBind()
                End If
            End If
        End Sub

        Private Sub BindDropDownList(ByVal stdId As String)
            'Dim transcript As New TranscriptFacade

            ddlEnrollmentId.DataSource = TranscriptFacade.GetEnrollmentList(stdId, Master.CurrentCampusId)
            ddlEnrollmentId.DataTextField = "PrgVerDescrip"
            ddlEnrollmentId.DataValueField = "StuEnrollId"
            ddlEnrollmentId.DataBind()
        End Sub

        Private Sub GenerateGraduateAuditTable()
            'Get info for report....
            Dim headerTitles As IList(Of String) = New List(Of String)()
            Dim headerData As GraduateAuditGroupDto = New GraduateAuditGroupDto()
            Dim detailDataList As IList(Of GraduateAuditGroupDto) = New List(Of GraduateAuditGroupDto)()
            Dim graduateAuditFac As GraduateAuditFacade = New GraduateAuditFacade()

            Dim stuEnrollId = ddlEnrollmentId.SelectedValue.ToString()
            Dim isClockHour = graduateAuditFac.IsStudentRegisteredForClockHour(stuEnrollId)
            If isClockHour Then
                CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "This page is not for students in Clock Hour Programs.")
                Dim emptylist As List(Of GraduateAuditGroupRow) = New List(Of GraduateAuditGroupRow)()
                masterGrid.DataSource = emptylist
                masterGrid.DataBind()
                Return

            End If

            Dim auditInfo As GraduateAuditInfo = New GraduateAuditInfo()
            auditInfo.StuEnrollmentId = ddlEnrollmentId.SelectedValue.ToString()
            auditInfo.ItemCollection = ddlEnrollmentId.Items
            auditInfo.GradeReps = CType(MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod"), String)
            auditInfo.IncludeHours = CType(MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade"), Boolean)
            auditInfo.StrGradesFormat = CType(MyAdvAppSettings.AppSettings("GradesFormat"), String)
            auditInfo.TranscripType = MyAdvAppSettings.AppSettings("TranscriptType").ToString
            auditInfo.Dsgradesysdetailids = (New TranscriptFacade).GetGradesSysDetailId()
            auditInfo.Addcreditsbyservice = MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString

            graduateAuditFac.CreateReportList(auditInfo, headerTitles, headerData, detailDataList)
            Dim gridDataSource As List(Of GraduateAuditGroupRow) = New List(Of GraduateAuditGroupRow)()

            'Add first row with data resume
            gridDataSource.Add(GraduateAuditGroupRow.AddGraduateAuditGroupDto(headerData))

            'Add Empty row as separator
            gridDataSource.Add(GraduateAuditGroupRow.Factory())

            'Add the rest of information
            gridDataSource.AddRange(From dto In detailDataList Select GraduateAuditGroupRow.AddGraduateAuditGroupDto(dto))

            'Configure Grid
            masterGrid.MasterTableView.RowIndicatorColumn.Visible = False
            masterGrid.MasterTableView.ExpandCollapseColumn.Visible = False
            masterGrid.MasterTableView.ShowHeader = True

            'Configure the titles of the columns (headers)
            Dim index As Integer = 0
            For Each s As String In headerTitles
                masterGrid.Columns(index).HeaderText = s
                index = index + 1
            Next

            'Bind the grid
            masterGrid.DataSource = gridDataSource
            masterGrid.DataBind()
        End Sub

        Private Sub DdlEnrollmentIdSelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
            lblEnrollmentId.Text = If(ddlEnrollmentId.SelectedItem.Value = Guid.Empty.ToString(), "All Enrollments", "Enrollments")

            'Generate the table for the selected enrollment
            GenerateGraduateAuditTable()
        End Sub

        Protected Sub BtnExportToPdfClick(ByVal sender As Object, ByVal e As EventArgs)
            Dim facade As StudentMasterFacade = New StudentMasterFacade()
            Dim student As StudentMasterInfo = facade.GetStudentInfo(StudentId)

            masterGrid.ExportSettings.Pdf.PageHeight = Unit.Parse("215mm")
            masterGrid.ExportSettings.Pdf.PageWidth = Unit.Parse("280mm")
            masterGrid.ExportSettings.Pdf.DefaultFontFamily = "Arial Unicode MS"
            masterGrid.ExportSettings.Pdf.PaperSize = GridPaperSize.Letter
            masterGrid.ExportSettings.Pdf.PageTitle = String.Format("{0} {1} {2} - Graduate Audit Report", student.FirstName, student.MiddleName, student.LastName)
            masterGrid.ExportSettings.ExportOnlyData = False
            masterGrid.ExportSettings.OpenInNewWindow = True
            masterGrid.MasterTableView.BorderWidth = 1
            masterGrid.MasterTableView.ShowHeader = True
            'masterGrid.ExportSettings.Pdf.ForceTextWrap = True
            masterGrid.MasterTableView.ExportToPdf()
        End Sub

        Protected Sub MasterGridItemCreated(ByVal sender As Object, ByVal e As GridItemEventArgs)

            If TypeOf e.Item Is GridHeaderItem Then
              
            End If
            If TypeOf e.Item Is GridDataItem Then
                Dim item = DirectCast(e.Item, GridDataItem)
              
                item.Cells(4).Style("text-align") = "right"
                item.Cells(5).Style("text-align") = "right"
                item.Cells(6).Style("text-align") = "right"
                item.Cells(7).Style("text-align") = "right"
                item.Cells(8).Style("text-align") = "right"
                item.Cells(9).Style("text-align") = "right"
                item.Cells(10).Style("text-align") = "right"
            End If
        End Sub

    End Class

End Namespace