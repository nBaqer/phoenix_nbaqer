﻿Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Namespace AdvWeb.AR
    Partial Class AR_UndoStudentTermination
        Inherits BasePage
        Private pObj As New UserPagePermissionInfo
        Public ObjStudentState As StudentMRU
        Protected CampusId As String
        ' Protected boolSwitchCampus As Boolean = False

        Private Sub Page_Load(sender As System.Object, e As EventArgs) Handles MyBase.Load

            'Get necessary objects.....................
            ObjStudentState = GetObjStudentState(0)
            If ObjStudentState Is Nothing Then
                RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If
            Dim advantageUserState As User = AdvantageSession.UserState
            Dim resourceId = HttpContext.Current.Request.Params("resid")
            CampusId = Master.CurrentCampusId
            Master.PageObjectId = ObjStudentState.StudentId.ToString()
            Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.SetHiddenControlForAudit()
            Master.ShowHideStatusBarControl(True) ' Show student bar
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, CampusId)
            If Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switch-campus", False)
                    Return
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(1, ObjStudentState.Name)
                End If
            End If
            hdnCampusId.value = CampusId
        End Sub


    End Class
End Namespace
