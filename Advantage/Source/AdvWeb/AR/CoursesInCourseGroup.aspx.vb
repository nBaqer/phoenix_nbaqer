
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade

Partial Class AR_CoursesInCourseGroup
    Inherits Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim courseGroupId As String = String.Empty
        Dim  courseGroup As String = String.Empty

        If Request.QueryString("CourseGrpId") <> "" Then
            courseGroupId = Request.QueryString("CourseGrpId")
        Else
            DisplayErrorMessage("This was no course group selected. Hence, no courses can be displayed.")
        End If

        If Request.QueryString("CourseGrp") <> "" Then
            courseGroup = Request.QueryString("CourseGrp")
            ViewState("CourseGrp") = courseGroup
        End If


        DisplayContent(courseGroupId, courseGroup)


        If Not Page.IsPostBack Then
            ChkIsInDB.Checked = True

        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    'Private Sub dlstLeadNames_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstLeadNames.ItemCommand
    Private Sub DisplayContent(ByVal courseGroupId As String, ByVal courseGrp As String)
        'Dim facade As New StdGrdBkFacade
        'Dim CommonUtilities As New CommonUtilities
        'Dim ds As New DataSet
        'Dim dt As New DataTable
        '        Dim strFirstName, strLastName, strInstructor, FullName, CourseName As String
        Dim dtReqsForCourseGrp As DataTable
        Dim fac As New TranscriptFacade

        dtReqsForCourseGrp = fac.GetReqsForCourseGroup(courseGroupId)
        lblStdName.Text = courseGrp

        With dgrdCourses
            .DataSource = dtReqsForCourseGrp
            .DataBind()
        End With

    End Sub
End Class
