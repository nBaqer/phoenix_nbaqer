' ===============================================================================
'
' FAME AdvantageV1
'
' GradeScales.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.

Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class GradeScales
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Private lastMax As Decimal = -1
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents AllDataset As DataSet
    Protected WithEvents GradeScalesTable As DataTable
    Protected WithEvents GradeScaleDetailsTable As DataTable
    Protected WithEvents GradeSystemsTable As DataTable
    Protected WithEvents GradeSystemDetailsTable As DataTable
    Protected WithEvents lblInstructorId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlInstructorId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Private numberOfRecords As Integer = 0
    Private previousMaxValue As Integer = -1
    'NOTE: The following placeholder declaration is required by the Web Form Designer.

    'Do not delete or move it.



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init

        'CODEGEN: This method call is required by the Web Form Designer

        'Do not modify it using the code editor.

        InitializeComponent()

    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private ValidateGradeScalesMessage As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim mContext As HttpContext
        'Dim fac As New UserSecurityFacade


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ViewState("IsUserSa") = advantageUserState.IsUserSA

        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        If Not IsPostBack Or Session("AllDataset") Is Nothing Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   get AllDataset from the DB
            With New GradesFacade
                AllDataset = .GetGradeScalesDS()
            End With

            '   save it on the session
            Session("AllDataset") = AllDataset

        Else
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        End If

        '   create dataset and tables
        CreateDatasetAndTables()

        '   the first time we have to bind an empty datagrid
        If Not IsPostBack Then
            PrepareForNewData()
            'Header1.EnableHistoryButton(False)
        End If
        headerTitle.Text = Header.Title
    End Sub

    Private Sub SwapViewOrders(ByVal id1 As String, ByVal id2 As String)

        '   get the rows with each id
        Dim row1(), row2() As DataRow
        row1 = GradeScaleDetailsTable.Select("GrdScaleDetailId=" + "'" + id1 + "'")
        row2 = GradeScaleDetailsTable.Select("GrdScaleDetailId=" + "'" + id2 + "'")

        '   swap value of viewOrder
        Dim viewOrder As Integer
        viewOrder = row1(0)("ViewOrder")
        row1(0)("ViewOrder") = row2(0)("ViewOrder")
        row2(0)("ViewOrder") = viewOrder
    End Sub

    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
        BuildGradeSystemsDDL()
    End Sub

    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub

    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub BuildGradeSystemsDDL()
        '   bind the GradeSystems DDL
        Dim gradeSystems As New GradesFacade

        With ddlGrdSystemId
            .DataTextField = "Descrip"
            .DataValueField = "GrdSystemId"
            .DataSource = gradeSystems.GetAllGradeSystems(False)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind GradeSystems datalist
        dlstGradeScales.DataSource = New DataView(GradeScalesTable, rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstGradeScales.DataBind()

    End Sub

    Private Sub dgrdGradeScaleDetails_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdGradeScaleDetails.ItemCommand

        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdGradeScaleDetails.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdGradeScaleDetails.ShowFooter = False

                '   disable save button
                btnSave.Enabled = False

                '   user hit "Update" inside the datagrid
            Case "Update"

                If Not ValidateEditFields(e) Then
                    '   Display Error Message
                    DisplayErrorMessage("Invalid data")
                    Exit Sub
                End If

                '   get the row to be updated
                Dim row() As DataRow = GradeScaleDetailsTable.Select("GrdScaleDetailId=" + "'" + CType(e.Item.FindControl("lblEditGrdScaleDetailId"), Label).Text + "'")

                '   fill new values in the row
                row(0)("MinVal") = CType(e.Item.FindControl("txtEditMinVal"), TextBox).Text
                row(0)("MaxVal") = CType(e.Item.FindControl("txtEditMaxVal"), TextBox).Text
                row(0)("GrdSysDetailId") = New Guid(CType(e.Item.FindControl("ddlEditGrdSysDetailId"), DropDownList).SelectedValue)
                row(0)("ModUser") = Session("UserName")
                row(0)("ModDate") = Date.Now

                'Prepare dataGrid
                PrepareDatagrid()

                '   user hit "Cancel"
            Case "Cancel"

                'Prepare dataGrid
                PrepareDatagrid()

                '   user hit "Delete" inside the datagrid
            Case "Delete"

                '   get the row to be deleted
                Dim row() As DataRow = GradeScaleDetailsTable.Select("GrdScaleDetailId=" + "'" + CType(e.Item.FindControl("lblEditGrdScaleDetailId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                'prepare DataGrid
                PrepareDataGrid()

            Case "AddNewRow"
                If Not ValidateFooter() Then
                    '   Display Error Message
                    DisplayErrorMessage("Invalid data")
                    Exit Sub
                End If

                '   If the GradeScale does not have an Id assigned.. create one and assign it.
                If Session("GrdScaleId") Is Nothing Then
                    '   build a new row in GradeScales table
                    BuildNewRowInGradeScales()
                End If

                '   get a new row in GradeScaleDetails
                Dim newRow As DataRow = GradeScaleDetailsTable.NewRow

                '   fill the new row with values
                newRow("GrdScaleDetailId") = Guid.NewGuid
                newRow("GrdScaleId") = New Guid(CType(Session("GrdScaleId"), String))

                If chkTestUI.Checked Then
                    newRow("MinVal") = CType(e.Item.FindControl("txtMinVal"), TextBox).Text
                Else
                    newRow("MinVal") = 0.0
                End If

                newRow("MaxVal") = CType(e.Item.FindControl("txtMaxVal"), TextBox).Text
                newRow("GrdSysDetailId") = New Guid(CType(e.Item.FindControl("ddlGrdSysDetailId"), DropDownList).SelectedValue)
                newRow("ViewOrder") = GetMaxViewOrder(GradeScaleDetailsTable) + 1
                newRow("ModUser") = Session("UserName")
                newRow("ModDate") = Date.Now

                '   add row to the table
                newRow.Table.Rows.Add(newRow)

                '   user hit "Up"
            Case "Up"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim GrdScaleDetailId As String = CType(e.Item.FindControl("lblGrdScaleDetailId"), Label).Text

                '   get the position of this item and swap viewOrder values with prevous item
                Dim i As Integer
                For i = 1 To dv.Count - 1
                    If CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString = GrdScaleDetailId Then
                        SwapViewOrders(CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString, CType(dv(i - 1).Row("GrdScaleDetailId"), Guid).ToString)
                        Exit For
                    End If
                Next

                '   user hit "Down" 
            Case "Down"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim grdScaleDetailId As String = CType(e.Item.FindControl("lblGrdScaleDetailId"), Label).Text

                '   get the position of this item and swap viewOrder values with next item
                Dim i As Integer
                For i = 0 To dv.Count - 2
                    If CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString = grdScaleDetailId Then
                        SwapViewOrders(CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString, CType(dv(i + 1).Row("GrdScaleDetailId"), Guid).ToString)
                        Exit For
                    End If
                Next

        End Select

        '   bind datagrid to GradeScaleDetailsTable 
        BindDataGrid(New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows))

    End Sub

    Private Sub dlstGradeScales_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstGradeScales.ItemCommand

        '   this portion of code added to refresh data from the database
        '**********************************************************************************
        '   get allDataset from the DB
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        With New GradesFacade
            AllDataset = .GetGradeScalesDS()
        End With

        '   save it on the session
        Session("AllDataset") = AllDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()
        '**********************************************************************************

        '   save GrdScaleId in session
        Session("GrdScaleId") = e.CommandArgument

        '   no record in the datagrid is selected
        dgrdGradeScaleDetails.EditItemIndex = -1

        '   get the row with the data to be displayed
        Dim row() As DataRow = AllDataset.Tables("arGradeScales").Select("GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'")

        '   fill new values in the row
        txtDescrip.Text = CType(row(0)("Descrip"), String)
        ddlStatusId.SelectedValue = CType(row(0)("StatusId"), Guid).ToString
        ddlCampGrpId.SelectedValue = CType(row(0)("CampGrpId"), Guid).ToString
        ddlGrdSystemId.SelectedValue = CType(row(0)("GrdSystemId"), Guid).ToString

        '   if there are Grade Scales attached to this GradeSystem disable the datagrid
        Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)
        If Not isUserSa Then
            dgrdGradeScaleDetails.Enabled = row(0)("InUseByClassSections") = 0
            dgrdGradeScaleDetails.Columns(3).Visible = dgrdGradeScaleDetails.Enabled
        End If

        '   bind GradeScaleDetails datagrid
        BindDataGrid(New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows))

        '   enable or disable buttons only if it has classSections attached
        btnSave.Enabled = dgrdGradeScaleDetails.Enabled
        btnNew.Enabled = dgrdGradeScaleDetails.Enabled
        btnDelete.Enabled = dgrdGradeScaleDetails.Enabled

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstGradeScales, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        '   show footer
        dgrdGradeScaleDetails.ShowFooter = True

        CommonWebUtilities.RestoreItemValues(dlstGradeScales, CType(Session("GrdScaleId"), String))
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   if GrdScaleId does not exist do an insert else do an update
        If Session("GrdScaleId") Is Nothing Then
            '   do an insert
            'BuildNewRowInGradeScales()

            '   Display Error Message
            DisplayErrorMessage("At least one Grade Scale is required")
            Exit Sub
        Else
            '   do an update

            '   get the row with the data to be displayed
            Dim row() As DataRow = GradeScalesTable.Select("GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'")

            '   update row data
            row(0)("Descrip") = txtDescrip.Text
            row(0)("StatusId") = New Guid(ddlStatusId.SelectedValue)
            row(0)("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
            row(0)("GrdSystemId") = New Guid(ddlGrdSystemId.SelectedValue)
            '   InstructorId can be null
            If Session("InstructorId") Is Nothing Then
                row(0)("InstructorId") = System.DBNull.Value
            Else
                row(0)("InstructorId") = New Guid(CType(Session("InstructorId"), Guid).ToString)
            End If
            row(0)("ModUser") = Session("UserName")
            row(0)("ModDate") = Date.Now
        End If

        'validate grade scale
        Dim result As String = ValidateGradeScale(GradeScaleDetailsTable, CType(Session("GrdScaleId"), String))
        If Not result = "" Then
            DisplayErrorMessage(result)
            Exit Sub
        End If

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors bind and set selected style to the datalist and
        '   initialize buttons 
        If Customvalidator1.IsValid Then

            '   bind GradeScales datalist
            BindDataList()

            '   initialize buttons
            InitButtonsForEdit()

        End If
        CommonWebUtilities.RestoreItemValues(dlstGradeScales, CType(Session("GrdScaleId"), String))
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        '   get all rows to be deleted
        Dim rows() As DataRow = GradeScaleDetailsTable.Select("GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'")

        '   delete all rows from GradeScaleDetails table
        Dim i As Integer
        If rows.Length > 0 Then
            For i = 0 To rows.Length - 1
                rows(i).Delete()
            Next
        End If

        '   get the row to be deleted in GradeScales table
        Dim row() As DataRow = GradeScalesTable.Select("GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'")

        '   delete row from the table only if there is a GrdScaleId selected
        If row.Length > 0 Then row(0).Delete()

        '   update DB
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   prepare screen for new data
            PrepareForNewData()
        End If
        'Header1.EnableHistoryButton(False)

        CommonWebUtilities.RestoreItemValues(dlstGradeScales, Guid.Empty.ToString)
    End Sub

    Private Function GetMaxViewOrder(ByVal table As DataTable) As Integer
        Dim i As Integer
        GetMaxViewOrder = 1

        '   if there are no rows just return 1 
        Dim rows() As DataRow = table.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

        If rows.Length = 0 Then Return 1

        For i = 0 To rows.Length - 1
            If CType(rows(i)("ViewOrder"), Integer) > GetMaxViewOrder Then
                GetMaxViewOrder = CType(rows(i)("ViewOrder"), Integer)
            End If
        Next
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '*********************** original code *******************************************
        ''   update DB
        'UpdateDB()

        ''   if there were no errors prepare screen for new data
        'If Customvalidator1.IsValid Then
        '    PrepareForNewData()
        'End If
        '************************* end of original code **********************************

        '   reset dataset to previous state. delete all changes
        AllDataset.RejectChanges()

        '   New Grade Scales are enabled
        dgrdGradeScaleDetails.Enabled = True
        dgrdGradeScaleDetails.Columns(3).Visible = True

        'prepare for new data
        PrepareForNewData()

        CommonWebUtilities.RestoreItemValues(dlstGradeScales, Guid.Empty.ToString)
    End Sub

    Private Sub PrepareForNewData()

        '   bind GradeScales datalist
        BindDataList()

        '   save GrdScaleId in session
        Session("GrdScaleId") = Nothing

        '   initialize fields 
        txtDescrip.Text = ""
        ddlCampGrpId.SelectedValue = Guid.Empty.ToString
        ddlGrdSystemId.SelectedValue = Guid.Empty.ToString

        '   bind an empty GradeScaleDetails datagrid
        BindDataGrid(New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows))

        '   initialize buttons
        InitButtonsForLoad()

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

        Try
            '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
            If AllDataset.HasChanges() Then
                'Set the Delete Button so it prompts the user for confirmation when clicked
                btnnew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
            Else
                btnnew.Attributes.Remove("onclick")
            End If

            '   save grade systems in the session
            Session("AllDataset") = AllDataset

            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnsave)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            BindToolTip()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try


    End Sub

    Private Sub BuildNewRowInGradeScales()
        '   get a new row
        Dim newRow As DataRow = GradeScalesTable.NewRow

        '   create a Guid for GradeScales if it has not been created 
        Session("GrdScaleId") = Guid.NewGuid.ToString

        '   fill the new row with values
        newRow("GrdScaleId") = New Guid(CType(Session("GrdScaleId"), String))
        newRow("Descrip") = txtDescrip.Text
        newRow("StatusId") = New Guid(ddlStatusId.SelectedValue)
        newRow("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
        newRow("GrdSystemId") = New Guid(ddlGrdSystemId.SelectedValue)
        '   InstructorId can be null
        If Session("InstructorId") Is Nothing Then
            newRow("InstructorId") = System.DBNull.Value
        Else
            newRow("InstructorId") = New Guid(CType(Session("InstructorId"), Guid).ToString)
        End If
        newRow("ModUser") = Session("UserName")
        newRow("ModDate") = Date.Now

        '   add row to the table
        newRow.Table.Rows.Add(newRow)

    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            dgrdGradeScaleDetails.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = dgrdGradeScaleDetails.Enabled
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged

        '   update DB
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            PrepareForNewData()
        End If

    End Sub

    Private Sub dgrdGradeScaleDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdGradeScaleDetails.ItemDataBound

        '   process only rows in the items and footer sections
        Select Case e.Item.ItemType
            Case ListItemType.EditItem

                '   Get selected value of ddlGrdSystemId
                Dim grdSystemId As String = ddlGrdSystemId.SelectedValue

                '   bind GrdSysDetailId dropdownlist
                Dim ddlGrdSysDetailId As DropDownList = CType(e.Item.FindControl("ddlEditGrdSysDetailId"), DropDownList)
                With ddlGrdSysDetailId
                    .DataTextField = "Grade"
                    .DataValueField = "GrdSysDetailId"
                    .DataSource = New DataView(GradeSystemDetailsTable, "GrdSystemId=" + "'" + grdSystemId + "'", "ViewOrder asc ", DataViewRowState.CurrentRows)
                    .DataBind()
                    .SelectedValue = CType(e.Item.FindControl("lblEditGrdSysDetailId"), Label).Text
                End With

                '   align the text inside the textbox 
                Dim txtBox As TextBox = CType(e.Item.FindControl("txtEditMinVal"), TextBox)
                txtBox.Attributes.Add("style", "text-align: center;")

                'enable or disable Min Units
                txtBox.Visible = chkTestUI.Checked

                '   align the text inside the textbox 
                Dim txtBox1 As TextBox = CType(e.Item.FindControl("txtEditMaxVal"), TextBox)
                txtBox1.Attributes.Add("style", "text-align: center;")

            Case ListItemType.Item, ListItemType.AlternatingItem

                '   increment the number of records
                numberOfRecords += 1

                '   disable or enable edit buttons inside the datagrid
                CType(e.Item.FindControl("lnlButEdit"), LinkButton).Visible = dgrdGradeScaleDetails.Enabled
                CType(e.Item.FindControl("lnkButUp"), LinkButton).Visible = dgrdGradeScaleDetails.Enabled
                CType(e.Item.FindControl("lnkButDown"), LinkButton).Visible = dgrdGradeScaleDetails.Enabled

                'enable or disable Min Units
                If Not chkTestUI.Checked Then
                    CType(e.Item.FindControl("lblMinVal"), Label).Text = previousMaxValue + 1
                    previousMaxValue = Integer.Parse(CType(e.Item.FindControl("lblMaxVal"), Label).Text)
                End If

                'save MaxVal
                If Not e.Item.FindControl("lblMaxVal") Is Nothing Then
                    lastMax = Decimal.Parse(CType(e.Item.FindControl("lblMaxVal"), Label).Text)
                End If

            Case ListItemType.Footer
                '   bind GradesSystemDetails dropdownlist
                Dim ddl As DropDownList = CType(e.Item.FindControl("ddlGrdSysDetailId"), DropDownList)
                ddl.DataValueField = "GrdSysDetailId"
                ddl.DataTextField = "Grade"
                ddl.DataSource = New DataView(GradeSystemDetailsTable, "GrdSystemId=" + "'" + ddlGrdSystemId.SelectedValue + "'", "ViewOrder desc ", DataViewRowState.CurrentRows)
                ddl.DataBind()

                '   if there are no records in the datagrid the footer should be marked as required
                If numberOfRecords = 0 Then
                    CType(e.Item.FindControl("txtMinVal"), TextBox).CssClass = "textbox TextBoxReqField"
                    CType(e.Item.FindControl("txtMaxVal"), TextBox).CssClass = "textbox TextBoxReqField"
                    CType(e.Item.FindControl("ddlGrdSysDetailId"), DropDownList).CssClass = "DropDownListReqField"
                    ddlGrdSystemId.CssClass = "DropDownListReqField"
                Else
                    ddlGrdSystemId.CssClass = "DropDownList"
                End If

                '   enable or disable dropdownlist status 
                CType(e.Item.FindControl("ddlGrdSysDetailId"), DropDownList).Enabled = dgrdGradeScaleDetails.Enabled
                dgrdGradeScaleDetails.ShowFooter = dgrdGradeScaleDetails.Enabled
                Dim footer As Control = dgrdGradeScaleDetails.Controls(0).Controls(dgrdGradeScaleDetails.Controls(0).Controls.Count - 1)
                footer.Visible = dgrdGradeScaleDetails.Enabled

                'enable or disable Min Units
                CType(e.Item.FindControl("txtMinVal"), TextBox).Visible = chkTestUI.Checked

                'put MaxVal + 1 in the textbox
                CType(e.Item.FindControl("txtMinVal"), TextBox).Text = (lastMax + 1).ToString

        End Select

    End Sub

    Private Sub ddlGrdSystemId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlGrdSystemId.SelectedIndexChanged

        '   bind GradeScaleDetails datagrid
        BindDataGrid(New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows))

    End Sub

    Private Sub BindDataGrid(ByVal dv As DataView)

        '   bind GradeScaleDetails datagrid
        dgrdGradeScaleDetails.DataSource = New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
        dgrdGradeScaleDetails.DataBind()

        '   if there are no rows in GradeScaleDetailsTable for this GrdScaleId delete all GradeScale data
        If dv.Count = 0 Then
            '   enable the selection of GradeSystems
            ddlGrdSystemId.Enabled = True

            '   get the row to be deleted in GradeScales table
            Dim row() As DataRow = GradeScalesTable.Select("GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'")

            '   delete row from the table only if there is a GrdScaleId selected
            If row.Length > 0 Then row(0).Delete()

            '   no GrdScaleId selected
            Session("GrdScaleId") = Nothing
        Else
            ddlGrdSystemId.Enabled = False
        End If
    End Sub

    Private Sub CreateDatasetAndTables()

        '   Get GradeScales Dataset from the session and create tables
        AllDataset = Session("AllDataset")
        GradeScalesTable = AllDataset.Tables("arGradeScales")
        GradeScaleDetailsTable = AllDataset.Tables("arGradeScaleDetails")
        GradeSystemsTable = AllDataset.Tables("arGradeSystems")
        GradeSystemDetailsTable = AllDataset.Tables("arGradeSystemDetails")

    End Sub

    Private Sub UpdateDB()
        '   update DB
        With New GradesFacade
            Dim result As String = .UpdateGradeScalesDS(AllDataset)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else
                '   get a new version of AllDataset because some other users may have added,
                '   updated or deleted records from the DB
                AllDataset = .GetGradeScalesDS()
                Session("AllDataset") = AllDataset
                CreateDatasetAndTables()
            End If
        End With
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Function ValidateFooter() As Boolean
        If AreFooterFieldsBlank() Then Return False
        Dim footer As Control = dgrdGradeScaleDetails.Controls(0).Controls(dgrdGradeScaleDetails.Controls(0).Controls.Count - 1)

        If chkTestUI.Checked Then
            If Not IsTextBoxValidInteger(CType(footer.FindControl("txtMinVal"), TextBox)) Then Return False
            If Not IsTextBoxValidInteger(CType(footer.FindControl("txtMaxVal"), TextBox)) Then Return False
            If Integer.Parse(CType(footer.FindControl("txtMaxVal"), TextBox).Text) < Integer.Parse(CType(footer.FindControl("txtMinVal"), TextBox).Text) Then Return False
        End If

        If Not IsTextBoxValidInteger(CType(footer.FindControl("txtMaxVal"), TextBox)) Then Return False
        '   gradeSystemDetaildId must be selected and be valid
        If Not CommonWebUtilities.IsValidGuid(CType(footer.FindControl("ddlGrdSysDetailId"), DropDownList).SelectedValue) Then Return False
        Return True
    End Function

    Private Function IsTextBoxValidInteger(ByVal textbox As TextBox) As Boolean
        Try
            Dim i As Integer = Integer.Parse(textbox.Text)
            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdGradeScaleDetails.Controls(0).Controls(dgrdGradeScaleDetails.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtMinVal"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtMaxVal"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function ValidateEditFields(ByVal e As DataGridCommandEventArgs) As Boolean
        If AreEditFieldsBlank(e) Then Return False
        If chkTestUI.Checked Then
            If Not IsTextBoxValidInteger(CType(e.Item.FindControl("txtEditMinVal"), TextBox)) Then Return False
            If Not IsTextBoxValidInteger(CType(e.Item.FindControl("txtEditMaxVal"), TextBox)) Then Return False
            If Integer.Parse(CType(e.Item.FindControl("txtEditMaxVal"), TextBox).Text) < Integer.Parse(CType(e.Item.FindControl("txtEditMinVal"), TextBox).Text) Then Return False
        End If
        Return True
    End Function

    Private Function AreEditFieldsBlank(ByVal e As DataGridCommandEventArgs) As Boolean
        If chkTestUI.Checked Then If Not CType(e.Item.FindControl("txtEditMinVal"), TextBox).Text = "" Then Return False
        If Not CType(e.Item.FindControl("txtEditMaxVal"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        'prepare for new data
        PrepareForNewData()
        'bind datalist
        BindDataList()
    End Sub

    Private Function ValidateGradeScale(ByVal gradeScaleDetailsTable As DataTable, ByVal gradeScaleId As String) As String
        ValidateGradeScalesMessage = ""
        Dim rows() As DataRow = gradeScaleDetailsTable.Select("GrdScaleId=" + "'" + gradeScaleId + "'", "ViewOrder asc")
        Dim n As Integer = rows.Length

        'return message if the grade system is empty
        If n = 0 Then Return "Grade System is empty"

        'update Min Val in rows 
        If Not chkTestUI.Checked Then
            rows(0)("MinVal") = 0.0
            For i As Integer = 1 To n - 1
                rows(i)("MinVal") = rows(i - 1)("MaxVal") + 1
            Next
        Else
            'validate manual values
            For i As Integer = 1 To n - 1
                If Not (rows(i)("MinVal") = rows(i - 1)("MaxVal") + 1) Then
                    ValidateGradeScalesMessage &= "Min Value: " + CType(rows(i)("MinVal"), Decimal).ToString + " is out of sequence." + vbCr + vbLf
                End If
            Next
            'verify that all Min Values are less than Max Values
            For i As Integer = 0 To n - 1
                If Not (rows(i)("MinVal") <= rows(i)("MaxVal")) Then
                    ValidateGradeScalesMessage &= "Min Value: " + CType(rows(i)("MinVal"), Decimal).ToString + " is greater than " + CType(rows(i)("MaxVal"), Decimal).ToString + vbCr + vbLf
                End If
            Next
        End If

        'check for duplicated grades
        CheckDuplicateGrades(rows)

        'return result
        Return ValidateGradeScalesMessage

    End Function

    Private Sub CheckDuplicateGrades(ByVal rows() As DataRow)
        'define array with proper dimensions
        Dim g(rows.Length - 1) As String

        'create array
        For i As Integer = 0 To rows.Length - 1
            g(i) = CType(rows(i)("GrdSysDetailId"), Guid).ToString + ";" + CType(rows(i).GetParentRow("GradeSystemDetailsGradeScaleDetails")("Grade"), String)
        Next

        'sort array
        Array.Sort(g)

        'check for duplicates
        For i As Integer = 1 To rows.Length - 1
            If g(i) = g(i - 1) Then
                ValidateGradeScalesMessage &= "Grade: " + g(i).Substring(g(i).IndexOf(";") + 1) + " has been entered more than once." + vbCr + vbLf
            End If
        Next
    End Sub

    Private Sub PrepareDatagrid()
        '   set no record selected
        dgrdGradeScaleDetails.EditItemIndex = -1

        '   show footer
        dgrdGradeScaleDetails.ShowFooter = True

        '   save button must be enabled
        btnSave.Enabled = True
    End Sub

End Class
