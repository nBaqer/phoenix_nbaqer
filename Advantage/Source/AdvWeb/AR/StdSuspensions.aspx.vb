Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects


Partial Class StdSuspensions
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected campusId As String


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Protected resourceId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim ds As New DataSet
        Dim facade As New ProbationFacade
        Dim objCommon As New CommonUtilities
        Dim dt As New DataTable
        '        Dim m_Context As HttpContext
        Dim StdEnrollId As String = String.Empty

        'disable history button
        'Header1.EnableHistoryButton(False)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = HttpContext.Current.Request.Params("resid")
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Not Page.IsPostBack Then
            If Request.QueryString("StuEnrollId") <> "" Then
                StdEnrollId = Request.QueryString("StuEnrollId")
                ViewState("StuEnrollId") = StdEnrollId
            End If

            'Dim objcommon As New CommonUtilities
            ds = facade.GetStudentSuspensions(StdEnrollId)

            With dgdDropCourse
                .DataSource = ds
                .DataBind()
            End With
            ViewState("Drop") = ds.Tables("Drop")
        Else

        End If
    End Sub

End Class
