﻿<%@ Page Title="Transcript" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Transcript2.aspx.vb" Inherits="AdvWeb.AR.ArTranscript" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <link href="../App_Themes/Blue_Theme/Blue_Theme.css" type="text/css" rel="stylesheet" />
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <script src="../Scripts/AR/Transcript.js"></script>
    <script type="text/javascript">
        var XGET_ENROLLMENT_URL = "<%= Page.ResolveUrl("~")%>" + "proxy/api/Students/Enrollments/{StudentId}/Field1=ComboBoxEnrollment";
        var studentID = "<%= GetStudentId()%>";
        var campusID = "<%= GetCampusId()%>";
        XGET_ENROLLMENT_URL = XGET_ENROLLMENT_URL.replace("{StudentId}", studentID);
        // Page Ready Event
        $(document).ready(function () {

            TranscriptDocumentReadyk(XGET_ENROLLMENT_URL, campusID);

        });
    </script>
    
    <div style="align-content: center">
        <span>Select Enrollment: </span><input id="EnrollmentComboBox" />
    </div>
    <hr/>
    <span>Current Curses</span>


</asp:Content>

