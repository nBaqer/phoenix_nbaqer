Imports System.Diagnostics
Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common

Partial Class Courses
    Inherits BasePage
    ' ReSharper disable InconsistentNaming
    Protected WithEvents btnLessons As Button
    Protected WithEvents btnhistory As Button
    Protected WithEvents Comparevalidator1 As CompareValidator
    Protected WithEvents lblReqTypeId As Label
    Protected WithEvents ddlReqTypeId As DropDownList
    Protected WithEvents cmpvalidator1 As CompareValidator
    Protected WithEvents txtHoursValidate As TextBox
    Protected WithEvents revInvNoChars As RegularExpressionValidator
    Protected ResourceId As String
    Protected ModuleId As String
    ' ReSharper restore InconsistentNaming
#Region " Web Form Designer Generated Code "
    'Dim RadComboBox1 As Object

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private validatetxt As Boolean = False
    Private pObj As New UserPagePermissionInfo
    Protected CampusId As String
    Protected UserName As String
    Protected StrDefaultCountry As String
    Protected MyAdvAppSettings As AdvAppSettings
    Private GradeBookWeightingLevel As String

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load


        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        GradeBookWeightingLevel = MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString().ToLower
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim mContext As HttpContext
        mContext = HttpContext.Current
        txtResourceId.Text = CType(mContext.Items("ResourceId"), String)

        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        'Dim sw As New System.IO.StringWriter
        Dim facade As New CoursesFacade
        Dim sdfControls As New SDFComponent
        'Dim userId As String
        'Dim fac As New UserSecurityFacade
        Dim courseobj As New CourseInfo

        If Not (MyAdvAppSettings.AppSettings("TimeClockClassSection") Is Nothing) Then
            If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                chkUseTimeClock.Visible = True
            Else
                chkUseTimeClock.Visible = False
            End If
        Else
            chkUseTimeClock.Visible = False
        End If

        Dim advantageUserState As User = AdvantageSession.UserState
        ResourceId = HttpContext.Current.Request.Params("resid")
        campusId = AdvantageSession.UserState.CampusId.ToString
        'userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ViewState("IsUserSa") = advantageUserState.IsUserSA

        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            'Put user code to initialize the page here
            If Not Page.IsPostBack Then
                'Color the required fields and set captions.
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                'Set the text box to a new guid
                txtReqId.Text = Guid.NewGuid.ToString

                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Populate the datalist

                ds = facade.GetAllCourses(campusId)

                'Set the default moduser and moddate and tardiesmakingabsence fields
                txtModUser.Text = courseobj.ModUser
                txtModDate.Text = courseobj.ModDate.ToString
                txtTardiesMakingAbsence.Text = "0"

                PopulateDataList(ds)

                BuildDepartmentsDDL()
                lbtSetUpFees.Enabled = False

            Else
                'Color the required fields and set captions.
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)

            End If

            'Check If any UDF exists for this resource
            Dim intSdfExists As Integer = sdfControls.GetSDFExists(CType(ResourceId, Integer), ModuleId)
            If intSdfExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If

            If Trim(txtCode.Text) <> "" Then
                sdfControls.GenerateControlsEditSingleCell(pnlSDF, CType(ResourceId, Integer), txtReqId.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNewSingleCell(pnlSDF, CType(ResourceId, Integer), ModuleId)
            End If

            'Special case: Do not allow more than 3 digits on Tardies Making An Absence
            txtTardiesMakingAbsence.MaxLength = 3
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As DataSet
        'Dim objListGen As New DataListGenerator
        Dim facade As New CoursesFacade
        'Dim savedguid As String = String.Empty
        Dim result As String

        Try
            If validatetxt = True Then
                Exit Sub
            End If

            'commented by balaji on 07/21/2008
            'this part has been commented as all attendance unit types track tardies and 
            'this validation is not neccessary
            'If Not (ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.PresentAbsentGuid Or _
            '        ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.MinutesGuid) And _
            '        chkTrackTardies.Checked Then
            '    DisplayErrorMessage("Please select an Attendace Unit Type or deselect Track Tardies")
            '    Exit Sub
            'End If

            'If the course is checked as IsAttendanceOnly, the user should not be able to save without specifying hours
            If chkAttendanceOnly.Checked = True Then
                If txtHours.Text = "" Then
                    DisplayErrorMessage("You must enter hours greater than 0 for a course that is marked as Attendance Only")
                    Exit Sub
                End If
                If txtHours.Text = "0.00" Or txtHours.Text = "0.0" Or txtHours.Text = "0" Then
                    DisplayErrorMessage("Hours cannot be 0 for a course that is marked as Attendance Only")
                    Exit Sub
                End If
            End If

            If ViewState("MODE") = "NEW" Then

                'Populate course object with the info.
                'Move the values in the controls over to the object
                Dim courseObj As CourseInfo

                courseObj = PopulateCourseObject()

                'Insert CampusGroup into syCampGrps Table
                result = facade.InsertCourse(courseObj, CType(Session("UserName"), String))

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                    Exit Sub
                Else
                    GetCourseDetails(txtReqId.Text)
                End If

                'Populate the datalist
                ds = facade.GetAllCourses(CampusId)
                PopulateDataList(ds)

                'Set Mode to Edit
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                'savedguid = txtReqId.Text
            Else

                If Not facade.CheckCourseWithProgramAttendanceType(txtReqId.Text, ddlUnitTypeId.SelectedValue) Then
                    DisplayErrorMessage("You cannot change the attendance unit type ")
                    Exit Sub
                End If

                If txtAttUnitType.Text <> ddlUnitTypeId.SelectedValue And facade.DoesReqHasAttendanceRecords(txtReqId.Text) Then
                    DisplayErrorMessage("You cannot change the attendance unit type because there are attendance records related to this course")
                Else
                    'Populate course object with the info.
                    'Move the values in the controls over to the object
                    Dim courseObj As CourseInfo

                    courseObj = PopulateCourseObject()

                    'Insert CampusGroup into syCampGrps Table
                    result = facade.UpdateCourse(courseObj, CType(Session("UserName"), String))

                    If Not result = "" Then
                        '   Display Error Message
                        DisplayErrorMessage(result)
                        Exit Sub
                    Else
                        GetCourseDetails(txtReqId.Text)
                    End If
                    'Populate the datalist
                    ds = facade.GetAllCourses(CampusId)
                    PopulateDataList(ds)
                End If
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim sdfid As ArrayList
            Dim sdfidValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim sdfControl As New SDFComponent
            Try
                sdfControl.DeleteSDFValue(txtReqId.Text)
                sdfid = sdfControl.GetAllLabels(pnlSDF)
                sdfidValue = sdfControl.GetAllValues(pnlSDF)
                For z = 0 To sdfid.Count - 1
                    sdfControl.InsertValues(txtReqId.Text, Mid(CType(sdfid(z).id, String), 5), CType(sdfidValue(z), String))
                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '   set fees Hyperlink
            SetFeesHyperlink()

            SetBooksHyperlink(chkAttendanceOnly.Checked)

            CommonWebUtilities.RestoreItemValues(dlstCourses, txtReqId.Text)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Function PopulateCourseObject() As CourseInfo
        Dim courseObj As New CourseInfo

        courseObj.CourseId = XmlConvert.ToGuid(txtReqId.Text)
        courseObj.CourseCode = txtCode.Text
        courseObj.CourseDescrip = txtDescrip.Text
        If (txtHours.Text) <> "" Then
            courseObj.CourseHours = Decimal.Parse(txtHours.Text)
        End If
        If (txtCredits.Text) <> "" Then
            courseObj.CourseCredits = Decimal.Parse(txtCredits.Text)
        End If
        If (txtFinAidCredits.Text) <> "" Then
            courseObj.FinAidCredits = Decimal.Parse(txtFinAidCredits.Text)
        End If
        If (radProgress.SelectedItem.Text = "Satisfactory/UnSatisfactory") Then
            courseObj.SU = 1
            courseObj.PF = 0
        ElseIf (radProgress.SelectedItem.Text = "Pass/Fail") Then
            courseObj.SU = 0
            courseObj.PF = 1
        Else
            courseObj.SU = 0
            courseObj.PF = 0
        End If
        courseObj.ReqTypeId = 1
        courseObj.CampGrpId = XmlConvert.ToGuid(ddlCampGrpId.SelectedItem.Value)
        courseObj.GrdLvlId = (ddlGrdLvlId.SelectedValue.ToString)
        courseObj.StatusId = XmlConvert.ToGuid(ddlStatusId.SelectedItem.Value)
        courseObj.UnitTypeId = XmlConvert.ToGuid(ddlUnitTypeId.SelectedItem.Value)
        courseObj.CourseCatalog = txtCourseCatalog.Text
        courseObj.CourseComments = txtCourseComments.Text
        courseObj.CourseCatId = (ddlCourseCategoryId.SelectedValue.ToString)
        courseObj.TrackTardies = chkTrackTardies.Checked
        courseObj.IsOnLine = cbIsOnLine.Checked
        courseObj.Externship = chkExternship.Checked
        courseObj.IsUseTimeClock = chkUseTimeClock.Checked
        If txtTardiesMakingAbsence.Text <> "" Then
            courseObj.TardiesMakingAbsence = Integer.Parse(txtTardiesMakingAbsence.Text)
        End If
        If (ddlDeptId.SelectedItem.Value <> "") Then
            courseObj.DeptId = XmlConvert.ToGuid(ddlDeptId.SelectedItem.Value)
        End If
        courseObj.IsAttendanceOnly = chkAttendanceOnly.Checked
        courseObj.AllowCompletedCourseRetake = chkAllowRetakes.Checked

        Return courseObj
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        Dim objCommon As New CommonUtilities
        Dim ds As DataSet
        Dim facade As New CoursesFacade
        'Dim objListGen As New DataListGenerator
        Dim courseobj As New CourseInfo

        Try
            ClearRHS()

            'Populate the datalist
            ds = facade.GetAllCourses(campusId)
            PopulateDataList(ds)

            'Set the text box to a new guid
            txtReqId.Text = Guid.NewGuid.ToString
            'ds = objListGen.SummaryListGenerator()

            'Set the default moduser and moddate
            txtModUser.Text = courseobj.ModUser
            txtModDate.Text = courseobj.ModDate.ToString
            txtTardiesMakingAbsence.Text = "0"

            'disable new and delete buttons.
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Reset Style in the Datalist
            'CommonWebUtilities.SetStyleToSelectedItem(dlstCourses, Guid.Empty.ToString, ViewState, Header1)
            lbtSetUpFees.Enabled = False
            lbtSetupBooks.Enabled = True
            txtCredits.Enabled = True
            txtFinAidCredits.Enabled = True
            txtHours.Enabled = True
            txtCredits.ToolTip = ""
            txtFinAidCredits.ToolTip = ""
            txtHours.ToolTip = ""
            chkExternship.Checked = False

            Dim sdfControls As New SDFComponent
            sdfControls.GenerateControlsNewSingleCell(pnlSDF, CType(ResourceId, Integer), ModuleId)
            CommonWebUtilities.RestoreItemValues(dlstCourses, Guid.Empty.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        Dim objcommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        'Dim sw As New System.IO.StringWriter
        Dim facade As New CoursesFacade
        Dim courseobj As New CourseInfo
        Try
            Dim result As String = facade.DeleteCourse(txtReqId.Text, Date.Parse(txtModDate.Text))
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else

                ClearRHS()
                dlstCourses.SelectedIndex = -1
                ds = facade.GetAllCourses(campusId)
                PopulateDataList(ds)

                'Set the default moduser and moddate
                txtModUser.Text = courseobj.ModUser
                txtModDate.Text = courseobj.ModDate.ToString
                txtTardiesMakingAbsence.Text = "0"

                'disable new and delete buttons.
                objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                'set the state to new. 
                ViewState("MODE") = "NEW"
                'Set the text box to a new guid
                txtReqId.Text = Guid.NewGuid.ToString
                'Header1.EnableHistoryButton(False)
                lbtSetUpFees.Enabled = False
                lbtSetupBooks.Enabled = True
                txtCredits.Enabled = True
                txtFinAidCredits.Enabled = True
                txtHours.Enabled = True
                txtCredits.ToolTip = ""
                txtFinAidCredits.ToolTip = ""
                txtHours.ToolTip = ""
                chkExternship.Checked = False
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim sdfControl As New SDFComponent
            sdfControl.DeleteSDFValue(ResourceId)
            sdfControl.GenerateControlsNewSingleCell(pnlSDF, CType(ResourceId, Integer), ModuleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CommonWebUtilities.RestoreItemValues(dlstCourses, Guid.Empty.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next

            'Need to explicitly set to 0 because this control is hidden,
            'and it would not get cleared on above for loop.
            txtTardiesMakingAbsence.Text = "0"

            'Hide panel that contains the TardiesMakingAbsence textbox.
            pnlTardies.Visible = chkTrackTardies.Checked

            chkExternship.Checked = False

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstCourses_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlstCourses.ItemCommand
        Dim selIndex As Integer
        Dim ds As DataSet
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim facade As New CoursesFacade
        'Dim objListGen As New DataListGenerator
        '        Dim CourseBeingUsedInPrgVerDef As Boolean

        Try
            strGUID = dlstCourses.DataKeys(e.Item.ItemIndex).ToString()
            'txtRowIds.Text = e.CommandArgument.ToString
            'objCommon.PopulatePage(form1, strGUID)
            Master.PageObjectId = CType(e.CommandArgument, String)
            Master.PageResourceId = CType(ResourceId, Integer)
            Master.setHiddenControlForAudit()

            GetCourseDetails(strGUID)

            'StudentsRegistered = facade.DoesPrgVerHaveStdsReg(txtProgVerId.Text)

            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstCourses.SelectedIndex = selIndex
            'Populate the datalist
            ds = facade.GetAllCourses(campusId)
            PopulateDataList(ds)

            '   set fees Hyperlink
            setFeesHyperlink()

            ShowHideRulesLink()

            SetBooksHyperlink(chkAttendanceOnly.Checked)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
            'SDF Code Starts Here

            Dim sdfControls As New SDFComponent
            sdfControls.GenerateControlsEditSingleCell(pnlSDF, CType(ResourceId, Integer), txtReqId.Text, ModuleId)

            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If Not MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages") Is Nothing Then
                If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    lbtSetupRules.Enabled = True
                Else
                    lbtSetupRules.Enabled = False
                End If
            End If
            CommonWebUtilities.RestoreItemValues(dlstCourses, strGUID)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub  dlstCourses_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub  dlstCourses_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub GetCourseDetails(ByVal courseID As String)
        Dim facade As New CoursesFacade
        BindCourseInfoExist(facade.GetCourseDetails(courseID))
    End Sub

    Private Sub BindCourseInfoExist(ByVal courseInfo As CourseInfo)
        'Bind The StudentInfo Data From The Database
        With courseInfo
            txtReqId.Text = .CourseId.ToString
            ddlCampGrpId.SelectedValue = .CampGrpId.ToString
            ddlGrdLvlId.SelectedValue = .GrdLvlId.ToString
            ddlCourseCategoryId.SelectedValue = .CourseCatId.ToString
            ddlStatusId.SelectedValue = .StatusId.ToString
            ddlUnitTypeId.SelectedValue = .UnitTypeId.ToString
            txtCode.Text = .CourseCode
            txtDescrip.Text = .CourseDescrip
            txtHours.Text = CType(.CourseHours, String)
            txtCredits.Text = CType(.CourseCredits, String)
            txtFinAidCredits.Text = CType(.FinAidCredits, String)
            txtCourseCatalog.Text = .CourseCatalog
            txtCourseComments.Text = .CourseComments
            txtAttUnitType.Text = .UnitTypeId.ToString
            If .SU = True Then
                radProgress.SelectedIndex = 0
            ElseIf .PF = True Then
                radProgress.SelectedIndex = 1
            Else
                radProgress.SelectedIndex = 2
            End If
            chkExternship.Checked = .Externship


            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            'Enable the credits text box only if there are no student registered for this course.
            txtCredits.Enabled = Not .IsCourseInPrgVerDef
            txtFinAidCredits.Enabled = Not .IsCourseInPrgVerDef
            txtHours.Enabled = Not .IsCourseInPrgVerDef
            If Not txtCredits.Enabled Then
                txtCredits.ToolTip = "The credits for this course cannot be changed as this course is part of a program version definition"
            End If
            If Not txtHours.Enabled Then
                txtHours.ToolTip = "The hours for this course cannot be changed as this course is part of a program version definition"
            End If
            If .DeptId.ToString <> "00000000-0000-0000-0000-000000000000" Then
                ddlDeptId.SelectedValue = .DeptId.ToString
            Else
                ddlDeptId.SelectedValue = ""
            End If
            'Modified on 02/03/2006 on request from Troy and Brad
            Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)
            If isUserSa Then
                txtCredits.Enabled = True
                txtFinAidCredits.Enabled = True
                txtHours.Enabled = True
            End If
            chkTrackTardies.Checked = .TrackTardies
            pnlTardies.Visible = chkTrackTardies.Checked
            'cbIsOnLine.Checked = .IsOnLine
            chkUseTimeClock.Checked = .IsUseTimeClock
            txtTardiesMakingAbsence.Text = CType(.TardiesMakingAbsence, String)
            chkAttendanceOnly.Checked = .IsAttendanceOnly
            SetBooksHyperlink(chkAttendanceOnly.Checked)


            chkAllowRetakes.Checked = .AllowCompletedCourseRetake

        End With
    End Sub

    'Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objListGen As New DataListGenerator
    '    Dim ds As New DataSet
    '    Dim ds2 As New DataSet
    '    Dim sw As New System.IO.StringWriter
    '    Dim objCommon As New CommonUtilities
    '    Dim dv2 As New DataView
    '    '        Dim sStatusId As String
    '    Dim facade As New CoursesFacade

    '    Try
    '        ClearRHS()
    '        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
    '        ViewState("MODE") = "NEW"
    '        'Set the text box to a new guid
    '        txtReqId.Text = System.Guid.NewGuid.ToString
    '        ds = facade.GetAllCourses(campusId)
    '        PopulateDataList(ds)
    '    Catch ex As System.Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try
    'End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/22/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radStatus.SelectedItem.Text.ToLower = "active") Or (radStatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radStatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "Descrip", DataViewRowState.CurrentRows)

        With dlstCourses
            .DataSource = dv
            .DataBind()
        End With
        dlstCourses.SelectedIndex = -1

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        Dim ds As DataSet
        Dim objCommon As New CommonUtilities
        Dim facade As New CoursesFacade

        lbtSetUpFees.Enabled = False
        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtReqId.Text = Guid.NewGuid.ToString
            ds = facade.GetAllCourses(campusId)
            PopulateDataList(ds)
            Dim sdfControls As New SDFComponent
            sdfControls.GenerateControlsNew(pnlSDF, CType(ResourceId, Integer), ModuleId)


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Public Sub TextValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs)
        Dim strMinus As String = Mid(args.Value, 1, 1)
        If strMinus = "-" Then
            DisplayErrorMessage("The Value Cannot be Negative")
            validatetxt = True
        End If
    End Sub
    Private Sub SetFeesHyperlink()
        '   enable Set Up Fees button
        lbtSetUpFees.Enabled = True

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstCourses, courseId, ViewState, Header1)

    End Sub
    Private Sub lbtSetUpFees_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtSetUpFees.Click

        Dim sb As New StringBuilder
        '   append CourseId to the querystring
        sb.Append("&CourseId=" + txtReqId.Text)
        '   append Course Description to the querystring
        sb.Append("&Course=" + Server.UrlEncode(txtDescrip.Text))
        '   setup the properties of the new window
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Const name As String = "CourseFees"
        Dim url As String = "../SA/" + name + ".aspx?resid=64&mod=SA&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub

    Protected Sub chkTrackTardies_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkTrackTardies.CheckedChanged
        If chkTrackTardies.Checked Then
            pnlTardies.Visible = True
            SetFocus(txtTardiesMakingAbsence)
        Else
            pnlTardies.Visible = False
            txtTardiesMakingAbsence.Text = "0"
        End If
    End Sub

    Protected Sub chkAttendanceOnly_CheckChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkAttendanceOnly.CheckedChanged
        SetBooksHyperlink(chkAttendanceOnly.Checked)
    End Sub
    Protected Sub chkExternship_CheckChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkExternship.CheckedChanged
        lbtSetUpGrdBkWgt_EnableLogic()
    End Sub

    Private Sub SetBooksHyperlink(ByVal value As Boolean)
        value = (Not value)
        txtCredits.Enabled = value
        txtFinAidCredits.Enabled = value
        lbtSetUpFees.Enabled = value
        lbtSetupBooks.Enabled = value
        chkAllowRetakes.Enabled = value

        lbtSetUpGrdBkWgt_EnableLogic()
    End Sub
    Private Sub lbtSetUpGrdBkWgt_EnableLogic()

        If GradeBookWeightingLevel = "instructorlevel" Then
            If chkExternship.Checked = True Then
                If chkAttendanceOnly.Checked = True Then
                    lbtSetUpGrdBkWgt.Enabled = False
                Else
                    lbtSetUpGrdBkWgt.Enabled = True
                End If
            Else
                lbtSetUpGrdBkWgt.Enabled = False
            End If
        End If
        If GradeBookWeightingLevel = "courselevel" Then
            If chkAttendanceOnly.Checked = True Then
                lbtSetUpGrdBkWgt.Enabled = False
            Else
                lbtSetUpGrdBkWgt.Enabled = True
            End If
        End If
    End Sub
    Protected Sub lbtSetUpGrdBkWgt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtSetUpGrdBkWgt.Click
        Dim sb As New StringBuilder
        '   append CourseId to the querystring
        sb.Append("&ReqId=" + txtReqId.Text)
        '   append Course Description to the querystring
        sb.Append("&Descrip=" + Server.UrlEncode(txtDescrip.Text))
        '   setup the properties of the new window
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium
        Const name As String = "GrdBkWeightingsCourse"
        Dim reqId As String = txtReqId.Text
        Dim url As String = String.Format("../MaintPopup.aspx?mod={0}&resid=475&cmpid={1}&pid={2}&ascx={3}", _
                    "AR", CampusId, reqId, "~/AR/IMaint_GrdBookWeightingsCourse.ascx")

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub

    Protected Sub lbtSetupBooks_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtSetupBooks.Click
        Dim sb As New StringBuilder
        '   append CourseId to the querystring
        sb.Append("&CourseId=" + txtReqId.Text)
        '   append Course Description to the querystring
        sb.Append("&Course=" + Server.UrlEncode(txtDescrip.Text))
        '   setup the properties of the new window
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Const name As String = "CourseBooks"
        Dim url As String = "../AR/" + name + ".aspx?resid=65&mod=AR&cmpid=" + CampusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub

    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(lbtSetupBooks)
        controlsToIgnore.Add(lbtSetUpGrdBkWgt)
        controlsToIgnore.Add(lbtSetUpFees)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub BuildDepartmentsDDL()
        '   bind the Department DDL
        Dim departments As New DepartmentsFacade

        With ddlDeptId
            .DataTextField = "DeptDescrip"
            .DataValueField = "DeptId"
            .DataSource = departments.GetAllARDepartments()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub lbtSetupRules_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtSetupRules.Click
        Dim sb As New StringBuilder
        sb.Append("&CourseId=" + txtReqId.Text)
        '   append Course Description to the querystring
        sb.Append("&Course=" + Server.UrlEncode(txtDescrip.Text))
        '   setup the properties of the new window
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Const name As String = "SetupTestRules"
        Dim url As String = "../AR/" + name + ".aspx?resid=613&mod=AR&cmpid=" + CampusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub

    Private Sub ShowHideRulesLink()
        Dim intRowCount As Integer = (New GradesFacade).GetCountbySysCompTypeIdandCourseId(txtReqId.Text)
        If intRowCount > 0 Then
            lbtSetupRules.Enabled = True
        Else
            lbtSetupRules.Enabled = False
            lbtSetupRules.ToolTip = "Link disabled because the dictation/speed test has not been applied to the selected course"
        End If
    End Sub
End Class
