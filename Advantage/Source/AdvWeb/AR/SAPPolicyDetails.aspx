<%@ Page Language="vb" AutoEventWireup="false" Inherits="SAPPolicyDetails" CodeFile="SAPPolicyDetails.aspx.vb" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>SAP Policy Details</title>
	<%--	<LINK href="../CSS/localhost.css" type="text/css" rel="stylesheet">--%>
		<LINK href="../CSS/systememail.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
	</HEAD>
	<body runat="server" leftMargin="0" topMargin="0" ID="Body1" NAME="Body1">
		<form id="Form1" method="post" runat="server">
		 <asp:ScriptManager ID="scriptmanager1" runat="server"></asp:ScriptManager>
			<!-- beging header -->
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><IMG src="../images/advantage_sap_details.jpg"></td>
					<td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
				</tr>
			</table>
			<!-- end header -->
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" height="100%" border="0">
				<tr>
					<td class="ListFrame">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td class="listframeNoFilter">No Filter Applicable</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom"><div class="scrollleftsap"><asp:datalist id="dlstIncrements" runat="server" DataKeyField="SAPDetailId">
											<SelectedItemTemplate>
												<asp:Label ID="lblTrigDescrip1" cssclass="SelectedItem" Runat="server" Text='<%# Container.DataItem("TrigDescrip")%>'>
												</asp:Label>
											</SelectedItemTemplate>
											<ItemTemplate>
												<asp:LinkButton text='<%# Container.DataItem("TrigDescrip")%>' Runat="server" CssClass="NonSelectedItem" CommandArgument='<%# Container.DataItem("SAPDetailId")%>' ID="Linkbutton1" CausesValidation="False" />
											</ItemTemplate>
										</asp:datalist></div>
								</td>
							</tr>
						</table>
					</td>
					<td class="leftside">
						<table cellSpacing="0" cellPadding="0" width="9" border="0" ID="Table3">
						</table>
					</td>
					<td class="DetailsFrameTop">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<tr>
								<td class="MenuFrame" align="right">
									<asp:button id="btnSave" runat="server" Text="Save" CssClass="save"></asp:button>
									<asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"></asp:button>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table5">
							<tr>
								<td class="detailsframe">
									<DIV class="scrollrightSAP">
										<!-- begin table content-->
										<asp:panel id="pnlRHS" Runat="server">
											<TABLE cellSpacing="0" cellPadding="0" width="90%" align="center">
												<tr>
													<td class="twocolumnlabelcell" style="WIDTH: 45%">
														<asp:label id="Label1" runat="server" CssClass="Label">SAP Policy Description</asp:label></td>
													<td class="twocolumncontentcell" style="WIDTH: 55%">
														<asp:textbox id="txtSAPPolicy" runat="server" CssClass="TextBox" ReadOnly="True"></asp:textbox></td>
												</tr>
												<tr>
													<td class="twocolumnlabelcell" style="WIDTH: 45%">
														<asp:label id="Label2" runat="server" CssClass="Label">Increment number</asp:label></td>
													<td class="twocolumncontentcell" style="WIDTH: 55%">
														<asp:textbox id="txtSeq" runat="server" CssClass="TextBox" Enabled="False"></asp:textbox></td>
												</tr>
												<tr>
													<td class="twocolumnlabelcell" style="WIDTH: 45%">
														<asp:label id="Label3" runat="server" CssClass="Label">SAP Check at </asp:label></td>
													<td class="twocolumncontentcell" style="WIDTH: 55%">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center">
															<tr>
																<td style="PADDING-RIGHT: 4px; WIDTH: 50%">
																	<asp:textbox id="txtTrigValue" runat="server" CssClass="TextBox"></asp:textbox></td>
																<td style="WIDTH: 50%">
																	<asp:dropdownlist id="ddlTrigUnit" runat="server" cssclass="DropDownListFF" Enabled="false"></asp:dropdownlist></td>
															</tr>
														</TABLE>
													</td>
												<tr>
													<td class="twocolumnlabelcell" style="WIDTH: 45%">
														<asp:label id="Label4" runat="server" CssClass="Label">after the </asp:label></td>
													<td class="twocolumncontentcell" style="WIDTH: 55%">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center">
															<tr>
																<td style="PADDING-RIGHT: 4px; WIDTH: 50%">
																	<asp:dropdownlist id="ddlTrigOffsetTyp" runat="server" cssclass="DropDownListFF" AutoPostBack="True" Enabled="false"></asp:dropdownlist></td>
																<td style="WIDTH: 50%">
																	<asp:textbox id="txtTrigOffsetSeq" runat="server" CssClass="TextBox"></asp:textbox>
																	<asp:CustomValidator id="Customvalidator2" Runat="server" OnServerValidate="TextValidate" Display="None"
																		ErrorMessage="Negative Values Not Allowed" ControlToValidate="txtTrigOffsetSeq"></asp:CustomValidator></td>
															</tr>
														</TABLE>
													</td>
												</tr>
												<tr>
													<td class="twocolumnlabelcell" style="WIDTH: 45%">
														<asp:Label id="Label5" runat="server" CssClass="Label">At this increment the student must have</asp:Label><BR>
													</td>
													<td class="twocolumncontentcell" style="WIDTH: 55%">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center">
															<tr>
																<td style="PADDING-RIGHT: 4px; WIDTH: 50%">
																	<asp:dropdownlist id="ddlQualType" runat="server" cssclass="DropDownListFF"></asp:dropdownlist></td>
																<td style="WIDTH: 50%">
																	<asp:textbox id="txtQualMinValue" runat="server" CssClass="TextBox" MaxLength=6></asp:textbox></td>
															</tr>
														</TABLE>
													</td>
												</tr>
                                             <tr>
                                             <asp:Panel ID="pnlAttendance" runat="server" Visible="false">
                                             <td  class="twocolumnlabelcell" style="WIDTH: 45%">
                                                <asp:Label ID="lblAttendance" runat="server"  CssClass="Label">Measure SAP by</asp:Label>
                                             </td>
                                             <td width="55%" nowrap>
												<asp:radiobuttonlist id="radAttendance" CssClass="Label" AutoPostBack="true" Runat="Server" RepeatDirection="Horizontal">
													<asp:ListItem Text="Summary" Value="0" Selected="True"/>
													<asp:ListItem Text="Instruction Detail" Value="1" />
												</asp:radiobuttonlist>
											</td>
                                               </asp:Panel>
                                             </tr>

                                             
												<tr>
                                              <asp:Panel runat="server" ID="pnlOverallAttendance">
													<td class="twocolumnlabelcell" style="WIDTH: 45%">
														<asp:label id="Label6" runat="server" CssClass="Label">And must have completed at least</asp:label></td>
													<td class="twocolumncontentcell" style="WIDTH: 55%">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center">
															<tr>
																<td style="PADDING-RIGHT: 4px; WIDTH: 30%">
																	<asp:textbox id="txtQuantMinValue" runat="server"  MaxLength="5" CssClass="TextBox"></asp:textbox></td>
																<td style="WIDTH: 70%">
																	<asp:dropdownlist id="ddlQuantMinUnitType" runat="server" cssclass="DropDownListFF"></asp:dropdownlist></td>
															</tr>
														</TABLE>
													</td>
                                                     </asp:Panel>
												</tr>
                                             	<tr>
                                                
                                              <asp:Panel runat="server" ID="pnlInstructionTypeAttendance" Visible="false">
                                          				
													
																
													
                                                     </asp:Panel>
                                                    
												</tr>
													<tr>
                                                        <td class="twocolumnlabelcell" style="WIDTH: 45%">
                                                            <asp:Label ID="lblMinAttendance" runat="server" CssClass="Label">And must have a 
                                                            minimum attendance of </asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell" style="WIDTH: 55%; TEXT-ALIGN: left">
                                                            <asp:TextBox ID="txtMinAttendanceValue" runat="server" CssClass="TextBox" 
                                                                Text="0" width="29%"></asp:TextBox>
                                                            <span class="label" style="WIDTH: 71%" runat="server" ID="percentSpan">percent</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="twocolumnlabelcell" style="WIDTH: 45%">
                                                            <asp:Label ID="lblMinCredsCompltd" runat="server" CssClass="Label">And must have 
                                                            a minimum of</asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell" style="WIDTH: 55%; TEXT-ALIGN: left">
                                                            <asp:TextBox ID="txtMinCredsCompltd" runat="server" CssClass="TextBox" 
                                                                width="29%"></asp:TextBox>
                                                            <span class="label" style="WIDTH: 71%" runat="server" ID="creditSpan">Credits</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="twocolumnlabelcell" style="WIDTH: 45%; WHITE-SPACE: normal">
                                                            <asp:Label ID="consequenceLabel" runat="server" CssClass="Label">Student who fails to meet 
                                                            the above standards will be</asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell" style="WIDTH: 55%">
                                                            <asp:DropDownList ID="ddlConsequence" runat="server" cssclass="DropDownListFF">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="twocolumnlabelcell" style="WIDTH: 45%">
                                                            <asp:Label ID="lblAccuracy" runat="server" CssClass="Label">Accuracy (in 
                                                            percentage)</asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell" nowrap style="WIDTH: 55%; TEXT-ALIGN: left">
                                                            <ew:NumericBox ID="txtAccuracy" runat="server" CssClass="TextBox" 
                                                                DecimalPlaces="2" TabIndex="36" Width="19%"></ew:NumericBox>
                                                            <asp:Panel ID="pnlpercent" runat="server" Visible="false">
                                                                <span id="spanpercent" class="label"></span>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
													</TABLE>
											<p></p>
												<table>
										              <tr>
										                         <td width="20px">&nbsp;</td>
										                        <td>
										        	<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
						 <telerik:RadPanelBar runat="server" ID="RadPanelBar1" ExpandMode="SingleExpandedItem" Width="600px" >
                                    <Items>
                                        <telerik:RadPanelItem Enabled="True" Text="Shorthand Skill level" runat="server" Selected="true" Font-Bold="true" Expanded="True">
                                            <Items>
                                                <telerik:RadPanelItem Value="Skill" runat="server">
                                                    <ItemTemplate>
                                                        <div class="text">
                                                            <asp:Panel ID="pnlSkill" runat="server" Height="300px"  ScrollBars="Vertical">
                                                            </asp:Panel>
                                                       </div>
                                                    </ItemTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                    </Items>
                                    <CollapseAnimation Duration="100" Type="None" />
                                    <ExpandAnimation Duration="100" Type="None" />
                                </telerik:RadPanelBar></telerik:RadAjaxPanel>
                                </td>
                                                    </tr>
						                    </table>
											<asp:textbox id="txtSAPId" Runat="server" Visible="false"></asp:textbox>
											<asp:textbox id="txtSAPDetailId" Runat="server" Visible="false"></asp:textbox>
										</asp:panel>
										<!--end table content-->
										<asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
										<asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
										<asp:TextBox ID="txtValidationSequence" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox>
									</DIV>
								</td>
							</tr>
						</table>
					
					</td>
				</tr>
			</TABLE>
			<div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
			<asp:panel id="Panel1" runat="server" CssClass="ValidationSummary"></asp:panel><asp:customvalidator id="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
				ShowSummary="False"></asp:validationsummary>
		</form>
	</body>
</HTML>
