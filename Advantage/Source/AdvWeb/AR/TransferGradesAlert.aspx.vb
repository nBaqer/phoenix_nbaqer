﻿
Imports System.Data
Imports System.Collections.Generic
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI

Imports BO = Advantage.Business.Objects

Partial Class AR_TransferGradesAlert
    Inherits System.Web.UI.Page

    'Private pObj As New UserPagePermissionInfo
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim scripString As String = CType(Session.Item("scriptstring"), String)
        Session.Remove("scriptstring")
        If Not Page.IsPostBack Then
            divScripString.InnerHtml = scripString
        End If
    End Sub
    Protected Sub radClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radClose.Click
        Label1.Text = "<script type='text/javascript'>Close()</" + "script>"
    End Sub
End Class
