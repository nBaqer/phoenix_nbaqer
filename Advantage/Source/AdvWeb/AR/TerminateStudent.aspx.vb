Imports System.Diagnostics
Imports FAME.Advantage.Common.AdvControls
Imports FAME.AdvantageV1.DataAccess
Imports FAME.Common
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.BusinessFacade.StudentEnrollState
Imports Telerik.Web.UI

Partial Class TerminateStudent
    Inherits BasePage

	
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents PnlDate As Panel
    Protected CampusId As String
    Protected Userid As String
    Protected Statetabindicator As Boolean
    Protected HoldothersStep67 As String
    Protected HoldothersStep68 As String
    Protected DSResults As DataSet
    Protected BoolHasFullEditPermission As Boolean = False

    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        Session("SearchType") = "Terminate"
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Private strError As String
    Protected MyAdvAppSettings As AdvAppSettings
    Protected ResourceId As Integer

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        MyAdvAppSettings = AdvAppSettings.GetAppSettings()

        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim advantageUserState As User = AdvantageSession.UserState

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        CampusId = AdvantageSession.UserState.CampusId.ToString
        Userid = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(ResourceId, String), CampusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        txtAcademicYearId.Attributes.Add("onClick", "__doPostBack('btnSubmit','')")
        If Not Page.IsPostBack Then
            'checking whether to navigate to the old Terminate Student page or new Student Termination page based on the configuration vaule.
            'If config value is 'disabled' then redirecting to old/existing page
            If MyAdvAppSettings.AppSettings("NewStudentTerminationworkflow").ToLower = "disabled" Then

                objCommon.PageSetup(CType(Master.FindControl("contentmain2"), ContentPlaceHolder), "NEW")

                ''Disable the new and delete buttons
                'turn off unneeded fields on the student search user control
                Dim trTerms As HtmlTableRow = CType(StudentSearch.FindControl("trTerms"), HtmlTableRow)
                trTerms.Visible = False
                Dim trAcadYears As HtmlTableRow = CType(StudentSearch.FindControl("trAcadYears"), HtmlTableRow)
                trAcadYears.Visible = False
                PopulateStatusRCB()
                PopulateReasonRCB()
                'else if config value is 'enabled' then redirecting to new page
            Else
                Dim strSearchUrl = "~/AR/StudentTermination.aspx?resid=843&mod=AR&cmpid=" + campusId + "&desc=Student Termination"
                Response.Redirect(strSearchUrl, False)
            End If

        End If

    End Sub
    Protected Sub btnTerminate1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTerminate1.Click

        ''''the following code is for terminate logic'''''
        If CheckFields() = True Then Exit Sub
        Dim facade As New StatusChangeHistoryDB
        Dim message As String
        'Dim sId As String
        Dim dateDeter As DateTime
        Dim endMsg As String
        lblMessages.Text = ""
        If txtDateDetermined.SelectedDate Is Nothing Then ''txtLDA.SelectedDate
            'This should not be happen then raise a exception
            Throw New ArgumentException("You must enter a Date of Termination")
        Else
            dateDeter = txtDateDetermined.SelectedDate.GetValueOrDefault()
        End If

        'Validate to make sure that LDA and DateDetermined fields are not in the future.
        ValidateDates()

        If strError <> String.Empty Then
            DisplayErrorMessage(strError)
            Exit Sub
        End If

        Dim stuEnrollment = Session("enrollid").ToString
        Dim studentStatus = TerminateFacade.GetStudentStatus(stuEnrollment)
        Dim stateObj As IStudentEnrollmentState = StateFactory.CreateStateObj(studentStatus)
        Dim change = New StudentChangeHistoryObj()
        change.CampusId = CampusId
        change.StuEnrollId = stuEnrollment
        change.DateOfChange = dateDeter
        change.DateDetermined = dateDeter
        change.DateOfReenrollment = Nothing
        Dim ldaDate As DateTime? = facade.GetLdaDate(stuEnrollment)

        'TODO Clarify if I take this from the interface or from Real Value
        change.Lda = If(ldaDate Is Nothing, txtLDA.SelectedDate, ldaDate)
        change.ModDate = DateTime.Now
        change.OrigStatusGuid = TerminateFacade.GetStudentStatusId(stuEnrollment)
        change.ModUser = AdvantageSession.UserState.UserName
        change.DropReasonGuid = ddlDropReasonId.SelectedValue
        change.IsRevelsal = 0
        change.NewStatusGuid = ddlStatusCodeId.SelectedValue
        message = CType(stateObj.DropStudent(change), String)
        If message <> String.Empty Then

            DisplayErrorMessage(message)
            rgDropCourse.Visible = False
        Else
            endMsg = AddNewTask()
            endMsg &= "The student has been dropped." & vbLf
            endMsg &= "Please note that the revenue recognition process for this student will be terminated."
            ClearRHS()
            DisplayInfoMessage(endMsg)
            'Clear all controls
            StudentSearch.Clear()
            ddlStatusCodeId.SelectedIndex = 0
            ddlDropReasonId.SelectedIndex = 0
            txtLDA.DbSelectedDate = Nothing
            txtDateDetermined.DbSelectedDate = Nothing
        End If

    End Sub
    Private Sub PopulateStatusRCB()
        Dim facade As New TerminateFacade
        ' Bind the Dataset to the CheckBoxList
        With ddlStatusCodeId
            .DataSource = facade.GetDropStatuses()
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub PopulateReasonRCB()
        Dim facade As New TerminateFacade
        ' Bind the Dataset to the CheckBoxList
        With ddlDropReasonId
            .DataSource = facade.GetDropReasons(CampusId)
            .DataTextField = "Descrip"
            .DataValueField = "DropReasonId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Function AddNewTask() As String
        Dim utInfo As New UserTaskInfo
        Dim rtn As String = String.Empty
        ' determine if this is an update or a new task by looking
        ' at the ViewState("usertaskid") which is set by passing a parameter to
        ' the page.

        utInfo.IsInDB = False
        utInfo.AssignedById = TMCommon.GetCurrentUserId()
        utInfo.EndDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.StartDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.Message = "The student has been dropped." & vbLf
        utInfo.Priority = 1
        'utInfo.OwnerId = TMCommon.GetCurrentUserId()
        'utInfo.ReId = txtStuEnrollmentId.Text
        utInfo.ModUser = TMCommon.GetCurrentUserId()
        utInfo.Status = FAME.AdvantageV1.Common.TM.TaskStatus.Pending
        utInfo.ResultId = txtStuEnrollmentId.Text


        ' Update the task and check the result
        'If Not UserTasksFacade.AddTaskFromStatusCode(ddlStatusCodeId.SelectedValue, utInfo) Then
        ' Asked to remove the message.
        If Not UserTasksFacade.AddTaskFromStatusCode(ddlStatusCodeId.SelectedValue, utInfo) Then
            '    rtn = "Unable to add a reminder to the Task Manager. " & vbLf
            rtn = String.Empty
        End If
        Return rtn
    End Function


    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, ByVal hourtype As String)

        If Not enrollid = String.Empty Then
            Session("enrollid") = enrollid
        End If
        'Session("enrollid") = enrollid
        Session("fullname") = fullname
        Session("termid") = termid
        Session("termdescrip") = termdescrip
        Session("academicyearid") = academicyearid
        Session("academicyeardescrip") = academicyeardescrip
        Session("hourtype") = hourtype

        If Not academicyeardescrip = String.Empty Then
            txtLDA.SelectedDate = Nothing
            txtDateDetermined.SelectedDate = Nothing
            ddlStatusCodeId.SelectedIndex = 0
            ddlDropReasonId.SelectedIndex = 0
            rgDropCourse.Visible = False
        End If

        'default lda to system date 
        txtDateDetermined.SelectedDate = DateTime.Now.ToShortDateString

        'if the lda exists in the enrollment record use it, else get it from attendance
        Dim studentLDA As Date = (New ClsSectAttendanceFacade).GetStudentLDA(Session("enrollid"))
        Dim lda As Date = (New ClsSectAttendanceFacade).GetLDA(Session("enrollid"))

        If Not (studentLDA = Date.MinValue) Then
            txtLDA.SelectedDate = studentLDA
            txtDateDetermined.SelectedDate = studentLDA
        Else
            If Not (lda = Date.MinValue) Then
                txtLDA.SelectedDate = lda
                txtDateDetermined.SelectedDate = lda
            End If
        End If

        If txtLDA.SelectedDate Is Nothing Then
            If Not (lda = Date.MinValue) Then
                If txtLDA.SelectedDate Is Nothing Then

                    txtLDA.SelectedDate = lda.ToShortDateString()
                    txtDateDetermined.SelectedDate = lda.ToShortDateString()
                Else
                    If Date.Parse(txtLDA.SelectedDate) < lda Then
                        txtLDA.SelectedDate = lda.ToShortDateString()
                        txtDateDetermined.SelectedDate = lda.ToShortDateString()
                    End If


                End If
                txtLDA.DatePopupButton.Visible = False
                txtLDA.Width = "115"
                txtLDA.Enabled = False

            Else
                txtLDA.SelectedDate = Nothing
                txtLDA.Enabled = True

            End If
            Exit Sub
        Else
            If Not (lda = Date.MinValue) Then
                If CStr(txtLDA.SelectedDate) = "" Then
                    txtLDA.SelectedDate = lda.ToShortDateString()
                    txtDateDetermined.SelectedDate = lda.ToShortDateString()
                Else
                    If Date.Parse(txtLDA.SelectedDate) < lda Then
                        txtLDA.SelectedDate = lda.ToShortDateString()
                        txtDateDetermined.SelectedDate = lda.ToShortDateString()
                    End If


                End If
                txtLDA.Enabled = False

            Else
                txtLDA.SelectedDate = Nothing
                txtLDA.Enabled = True

            End If
        End If

        trTerminate.Visible = True

    End Sub

    Public Function GetIsRoleSystemAdministarator(ByVal UserId As String) As Boolean
        Dim stusearchDB As New StudentSearchDB
        Return stusearchDB.GetIsRoleSystemAdministarator(UserId)
    End Function


    Private Function CheckFields() As Boolean

        Dim bReturnValue As Boolean = False
        If lblMessages.Text.Contains("You must enter all required fields") Then
            lblMessages.Text = "You must enter all required fields<br>"
        Else
            lblMessages.Text = ""
        End If

        If txtLDA.IsEmpty Then
            lblMessages.Text = "You must enter a Last Date of Attendance<br>"
            bReturnValue = True
        End If

        If txtDateDetermined.IsEmpty Then
            lblMessages.Text = lblMessages.Text & "You must enter a Withdrawal Date<br>"
            bReturnValue = True
        End If

        If ddlStatusCodeId.SelectedIndex = 0 Then
            lblMessages.Text = lblMessages.Text & "You must select a Status<br>"
            bReturnValue = True
        End If

        If ddlDropReasonId.SelectedIndex = 0 Then
            lblMessages.Text = lblMessages.Text & "You must select a Drop Reason<br>"
            bReturnValue = True
        End If

        If bReturnValue = True Then
            lblMessages.Font.Size = 10
            'lblMessages.Visible = True
            DisplayErrorMessage(lblMessages.Text)
        End If
        Return bReturnValue

    End Function

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
                If ctl.GetType Is GetType(RadComboBox) Then
                    CType(ctl, RadComboBox).SelectedIndex = 0
                End If
                If TypeOf ctl Is RadDatePicker Then
                    CType(ctl, RadDatePicker).Clear()
                End If
                If TypeOf ctl Is IAdvControl Then
                    Dim AdvCtrl As IAdvControl = CType(ctl, IAdvControl)
                    AdvCtrl.Clear()
                End If
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub


    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click

        Try
            ClearRHS()

            'US3134 hard-coded because above method wasn't clearing the controls
            lblMessages.Text = String.Empty
            lblMessages.Visible = False
            ddlDropReasonId.SelectedIndex = 0
            ddlStatusCodeId.SelectedIndex = 0
            txtLDA.Clear()
            txtDateDetermined.Clear()
            StudentSearch.Clear()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub rgDropCourse_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles rgDropCourse.ItemDataBound
        'Dim iitems As DataGridItemCollection
        'Dim iitem As DataGridItem
        ' Dim i As Integer
        Dim rcb As RadComboBox
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable
        'Dim row As DataRow
        Dim count As Integer
        Dim ClsSectId As String

        'variable added by priyanka on date 30th april
        Dim rdp As RadDatePicker
        '  Dim link As HyperLink

        tbl1 = Session("ClsSectStds")
        tbl2 = Session("ClsSectGrds")
        count = CInt(ViewState("Count"))

        Select Case e.Item.ItemType
            Case GridItemType.Item, GridItemType.AlternatingItem
                Dim Item As GridItem = DirectCast(e.Item, GridItem)
                count += 1
                ViewState("Count") = count.ToString
                rcb = DirectCast(Item.FindControl("rcbGrade"), RadComboBox)
                ClsSectId = CType(Item.FindControl("rtbClsSectionId"), RadTextBox).Text

                'code added by priyanka on date 30th april 2009
                rdp = CType(Item.FindControl("rdpDate"), RadDatePicker)
                If (rdp.SelectedDate & "" = "") Then
                    rdp.SelectedDate = txtDateDetermined.SelectedDate
                End If
                'link = CType(e.Item.FindControl("date"), HyperLink)
                'link.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + rdp.ClientID + "',true,1945)"

                BuildGradesRCB(rcb, ClsSectId)
                'Select DDL if grade exists for student in ClsSectStds datatable.
        End Select

    End Sub

    Private Sub BuildGradesRCB(ByVal rcb As RadComboBox, ByVal ClsSectId As String)
        Dim rcbDS As DataSet
        '   save TuitionCategories in the Viewstate for subsequent use
        If ViewState("GradesDS") Is Nothing Then
            rcbDS = (New DropStudentFacade).GetDropAndFailGrades(ClsSectId)
            ViewState("GradesDS") = rcbDS
        Else
            rcbDS = ViewState("GradesDS")
        End If

        '   bind the TuitionCategories DDL
        With rcb
            .DataTextField = "Grade"
            .DataValueField = "GrdSysDetailId"
            .DataSource = rcbDS
            .DataBind()
            .Items.Insert(0, New RadComboBoxItem("Select", ""))
            '.SelectedIndex = 0
        End With
    End Sub

    Private Sub ValidateDates()

        'Validate that LDA date is not greater than current system date
        If Not txtLDA.SelectedDate Is Nothing Then
            If Convert.ToDateTime(txtLDA.SelectedDate) > Date.Now Then
                strError &= "Last Date Attended cannot be greater than the current date " & vbCr
            End If
        End If

        'Validate that DateDetermined date is not greater than current system date
        If Not txtDateDetermined.SelectedDate Is Nothing Then
            If Convert.ToDateTime(txtDateDetermined.SelectedDate) > Date.Now Then
                strError &= "Withdrawal Date cannot be greater than the current date " & vbCr
            End If
            If Convert.ToDateTime(txtDateDetermined.SelectedDate) < Convert.ToDateTime(txtLDA.SelectedDate) Then
                strError &= "Withdrawal Date cannot be earlier than the Last Date Attended " & vbCr
            End If
        End If
    End Sub

    Protected Sub txtStuEnrollmentId_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtStuEnrollmentId.TextChanged
        ' Dim str As String = String.Empty
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        BindToolTip()
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End If

    End Sub
    Private Sub DisplayInfoMessage(ByVal infoMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = infoMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayInfoInMessageBox(Page, infoMessage)
        End If

    End Sub
End Class

