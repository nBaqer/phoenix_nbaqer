﻿Imports FAME.Common
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Imports System.Drawing
Imports Advantage.AFA.Integration
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports FAME.Integration.Adv.Afa.Messages.WebApi.Entities
Imports FAME.Advantage.Api.Library.Models
Imports FAME.AdvantageV1.BusinessFacade.AR

Partial Class AR_transferstudentswithprogram
    Inherits BasePage
    Public campusId, userId As String
    Private StuMasterInfo As New StudentEnrollmentInfo
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load


        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim advantageUserState As User = AdvantageSession.UserState
        Dim resourceId As Integer

        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))

        Dim pObj As UserPagePermissionInfo
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            BuildDropDownLists()
            lblSourceCampus.Text = (New TransferStudentsToAnotherProgramFacade).GetSourceCampusId(campusId)

            Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
            'Shifts DDL
            ddlList.Add(New AdvantageDDLDefinition(ddlShiftId, AdvantageDropDownListName.Shifts, campusId, True, True, String.Empty))

            'Build DDLs
            CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
            If MyAdvAppSettings.AppSettings("TransferAllCoursesToDifferentProgram").ToString.ToLower = "yes" Then
                chkTransferAllPrograms.Checked = True
                btnTransferAllCourses.Visible = True
                pnlListOfCourses.Visible = False
                btnGetCourses.Enabled = False
            Else
                chkTransferAllPrograms.Checked = False
                btnTransferAllCourses.Visible = False
                btnGetCourses.Enabled = True
            End If
        End If
        txtStudentName.BackColor = Color.FromName("#ffff99")

        If Not lblSourcePrgVerId.Text = "" Then
            lblProgramTo.ToolTip = "The Transfer Courses To Program List will bring in all programs except " & lblSourceProgramName.Text
        End If
        ddlTargetProgramId.ToolTip = " Program Drop-down is filtered by campus selected and user logged in"
    End Sub

    Protected Sub dgrdTransactionSearch_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemCreated
        Dim oControl As Control
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        For Each oControl In dgrdTransactionSearch.Controls(0).Controls
            If CType(oControl, DataGridItem).ItemType = ListItemType.Header Then
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    CType(oControl, DataGridItem).Cells(5).Visible = False
                    CType(oControl, DataGridItem).Cells(4).Visible = True
                    CType(oControl, DataGridItem).Cells(4).Text = "Score Earned "
                    CType(oControl, DataGridItem).Cells(4).ToolTip = "Displays score earned for " & lblSourcePrgVerName.Text
                Else
                    CType(oControl, DataGridItem).Cells(5).Text = "Transfer Grade to " & ddlTargetPrgVerId.SelectedItem.Text
                    CType(oControl, DataGridItem).Cells(5).ToolTip = "Displays grades related to " & ddlTargetPrgVerId.SelectedItem.Text
                    CType(oControl, DataGridItem).Cells(4).Text = "Grade Earned "
                    CType(oControl, DataGridItem).Cells(4).ToolTip = "Displays grade earned for " & lblSourcePrgVerName.Text
                End If
                CType(oControl, DataGridItem).Cells(6).ToolTip = "Displays active terms for " & ddlTargetPrgVerId.SelectedItem.Text
                CType(oControl, DataGridItem).Cells(2).ToolTip = "Displays courses taken by " & txtStudentName.Text & " for " & lblSourcePrgVerName.Text & " and also common to " & ddlTargetPrgVerId.SelectedItem.Text
            End If
            If CType(oControl, DataGridItem).ItemType = ListItemType.Item Then
                'Dim isCommonCourse As Boolean = False
                'isCommonCourse = (New TransferStudentsToAnotherProgramFacade).IsCommonCourse(ddlTargetPrgVerId.SelectedValue, CType(dgrdTransactionSearch.FindControl("lblReqId"), Label).Text)
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    CType(oControl, DataGridItem).Cells(5).Visible = False
                    CType(oControl, DataGridItem).Cells(4).Visible = True
                    'If isCommonCourse = False Then
                    '    CType(dgrdTransactionSearch.FindControl("ddlEditTermId"), DropDownList).Enabled = False
                    'End If
                Else
                    CType(oControl, DataGridItem).Cells(5).Visible = True
                    CType(oControl, DataGridItem).Cells(4).Visible = True
                    'If isCommonCourse = False Then
                    '    CType(dgrdTransactionSearch.FindControl("ddlEditTermId"), DropDownList).Enabled = False
                    '    CType(dgrdTransactionSearch.FindControl("ddlEditGrdSysDetailId"), DropDownList).Enabled = False
                    'End If
                End If
            End If
            If CType(oControl, DataGridItem).ItemType = ListItemType.AlternatingItem Then
                Dim isCommonCourse As Boolean = False
                'If Course is not common course then hide Terms dropdown
                'isCommonCourse = (New TransferStudentsToAnotherProgramFacade).IsCommonCourse(ddlTargetPrgVerId.SelectedValue, CType(dgrdTransactionSearch.FindControl("lblReqId"), Label).Text)
                If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    CType(oControl, DataGridItem).Cells(5).Visible = False
                    CType(oControl, DataGridItem).Cells(4).Visible = True
                    'If isCommonCourse = False Then
                    '    CType(dgrdTransactionSearch.FindControl("ddlEditTermId"), DropDownList).Enabled = False
                    'End If
                Else
                    CType(oControl, DataGridItem).Cells(5).Visible = True
                    CType(oControl, DataGridItem).Cells(4).Visible = True
                    'If isCommonCourse = False Then
                    '    CType(dgrdTransactionSearch.FindControl("ddlEditTermId"), DropDownList).Enabled = False
                    '    CType(dgrdTransactionSearch.FindControl("ddlEditGrdSysDetailId"), DropDownList).Enabled = False
                    'End If
                End If
            End If
        Next
    End Sub
    Protected Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound
        'get a reference to the ddl in the edit 
        Dim z1 As Integer
        Dim dgItems1 As DataGridItemCollection
        Dim dgItem1 As DataGridItem
        Dim dgGrid As DataGrid
        Dim strPrgVerId As String = ddlTargetPrgVerId.SelectedValue.ToString
        dgGrid = CType(MyBase.FindControlRecursive("dgrdTransactionSearch"), DataGrid)
        dgItems1 = dgGrid.Items
        For z1 = 0 To dgItems1.Count - 1
            dgItem1 = dgItems1.Item(z1)
            Dim grdSysDetailId As String = CType(dgItem1.FindControl("txtGrdSysDetailId"), TextBox).Text.ToString
            Dim ddl As DropDownList = CType(dgItem1.FindControl("ddlEditGrdSysDetailId"), DropDownList)
            Dim strEarnedGrade As String = CType(dgItem1.FindControl("lblOldGrade"), Label).Text
            Dim strTermDescrip As String = CType(dgItem1.FindControl("txtTermDescrip"), TextBox).Text
            If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                ddl.Visible = False
            Else
                ddl.Visible = True
                ddl.DataSource = (New TransferStudentsToAnotherProgramFacade).GetGradesByResultId(strPrgVerId)
                ddl.DataTextField = "Grade"
                ddl.DataValueField = "GrdSysDetailId"
                ddl.DataBind()
                ddl.Items.Insert(0, New ListItem("Select", ""))
                Try
                    ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(strEarnedGrade))
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddl.SelectedIndex = 0
                End Try
            End If
            Dim ddl1 As DropDownList = CType(dgItem1.FindControl("ddlEditTermId"), DropDownList)
            ddl1.DataSource = (New TransferStudentsToAnotherProgramFacade).GetTermsByProgram(ddlTargetProgramId.SelectedValue, campusId)
            ddl1.DataTextField = "TermDescrip"
            ddl1.DataValueField = "TermId"
            ddl1.DataBind()
            ddl1.Items.Insert(0, New ListItem("Select", ""))
            Try
                ddl1.SelectedIndex = ddl1.Items.IndexOf(ddl1.Items.FindByText(strTermDescrip))
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                ddl1.SelectedIndex = 0
            End Try
            Dim isCommonCourse As Boolean = False
            isCommonCourse = (New TransferStudentsToAnotherProgramFacade).IsCommonCourse(ddlTargetPrgVerId.SelectedValue, CType(ddl.FindControl("lblReqId"), Label).Text)
            If MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                If isCommonCourse = False Then
                    CType(dgItem1.FindControl("ddlEditTermId"), DropDownList).Enabled = False
                End If
            Else
                If isCommonCourse = False Then
                    CType(dgItem1.FindControl("ddlEditTermId"), DropDownList).Enabled = False
                    CType(dgItem1.FindControl("ddlEditGrdSysDetailId"), DropDownList).Enabled = False
                End If
            End If
        Next
    End Sub
    Protected Sub btnGetCourses_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetCourses.Click
        Dim strMessage As String = ""
        If txtStudentName.Text = "" Then
            strMessage = "Please select a student"
        End If
        'If ddlSourcePrgVerId.SelectedValue = "" Then
        '    strMessage &= "Please select the program version to trasfer the course from"
        'End If
        If ddlTargetPrgVerId.SelectedValue = "" Then
            strMessage &= "Please select the Program Version to transfer the course to "
        End If
        If Not strMessage = "" Then
            DisplayErrorMessage(strMessage)
            Exit Sub
        End If
        pnlListOfCourses.Visible = True
        BindDataGrid()
    End Sub
    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Response.Write(txtStudentId.Text)
    '    If Not txtFirstName.Text = "" Or Not txtLastName.Text = "" Or Not txtSSN.Text = "" Or Not txtDOB.Text = "" Or Not ddlPrgVerId.SelectedIndex = 0 Then
    '        With ddlStudentId
    '            .Items.Clear()
    '            .DataSource = (New TransferStudentsToAnotherProgramFacade).GetStudentResults(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtDOB.Text, campusId, ddlPrgVerId.SelectedValue)
    '            .DataValueField = "StudentId"
    '            .DataTextField = "FullName"
    '            .DataBind()
    '        End With
    '    End If
    'End Sub

    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String,
                               ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String,
                               ByVal hourtype As String)

        If enrollid = "" Then enrollid = Session("StuEnrollment")
        If fullname = "" Then fullname = Session("StudentName")
        If termid = "" Then termid = Session("TermId")
        If termdescrip = "" Then termdescrip = Session("Term")
        If academicyeardescrip = "" Then academicyeardescrip = Session("AcademicYear")

        txtStuEnrollmentId.Text = enrollid
        txtStudentName.Text = fullname
        txtTermId.Text = termid
        txtTerm.Text = termdescrip
        'txtAcademicYearId.Text = academicyearid
        txtAcademicYear.Text = academicyeardescrip

        Session("StuEnrollment") = txtStuEnrollmentId.Text
        Session("StudentName") = txtStudentName.Text
        Session("TermId") = txtTermId.Text
        Session("Term") = txtTerm.Text
        'Session("AcademicYearID") = txtAcademicYearId.Text
        Session("AcademicYear") = txtAcademicYear.Text
        GetEnrollmentInfo()
        LoadControlsWithEnrollmentInfo()

    End Sub
    Protected Sub GetEnrollmentInfo()
        'Here, we will get the info to populate on the page once we get the student.
        Dim studentinfo As New StuEnrollFacade
        StuMasterInfo = studentinfo.GetStudentDetails_UI(Session("StuEnrollment"))
    End Sub
    Protected Sub LoadControlsWithEnrollmentInfo()
        'Populate the information from the student search on the page
        Try

            With StuMasterInfo
                txtEnrollDate.SelectedDate = .EnrollDate
                txtExpStartDate.SelectedDate = .ExpStartDate
                txtExpGradDate.SelectedDate = .ExpGradDate

                ddlShiftId.SelectedItem.Text = Trim(.Shift)

                lblSourceCampus.Text = .Campus
                lblSourceProgramName.Text = .Program
                lblSourcePrgVerName.Text = .ProgramVersion

                lblSourcePrgVerId.Text = .ProgramVersionID
                txtStudentId.Text = .StudentID


            End With

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex

        End Try
    End Sub
    Private Sub BuildDropDownLists()
        BuildTargetCampus()
        BuildSourceProgram()
        BuildTargetProgram("")
    End Sub
    Protected Sub BuildTargetCampus()
        If AdvantageSession.UserState.IsUserSA Then
            With ddlTargetCampusId
                .DataSource = (New TransferStudentsToAnotherProgramFacade).GetAllCampusId()
                .DataValueField = "CampusId"
                .DataTextField = "CampDescrip"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            With ddlTargetCampusId
                .DataSource = (New TransferStudentsToAnotherProgramFacade).GetTargetCampusId(userId)
                .DataValueField = "CampusId"
                .DataTextField = "CampDescrip"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If

    End Sub
    Protected Sub BuildSourceProgram()
        'With ddlSourceProgramId
        '    .DataSource = (New TransferStudentsToAnotherProgramFacade).GetProgram(campusId, userId)
        '    .DataValueField = "ProgId"
        '    .DataTextField = "ProgDescrip"
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", ""))
        '    .SelectedIndex = 0
        'End With
    End Sub
    Protected Sub BuildTargetProgram(ByVal SourceProgramId As String)
        If Not ddlTargetCampusId.SelectedValue = "" Then
            With ddlTargetProgramId
                .DataSource = (New TransferStudentsToAnotherProgramFacade).GetTargetProgram(ddlTargetCampusId.SelectedValue, userId, SourceProgramId)
                .DataValueField = "ProgId"
                .DataTextField = "ShiftDescrip"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Protected Sub BuildSchedule(ByVal TargetProgramId As String, ByVal SourceProgramId As String)
        If Not ddlTargetCampusId.SelectedValue = "" Then
            '' DE 12167  -- The column ScheduleId was removed from arStuEnrollments table so we don't need this information
            ''With ddlScheduleId
            ''    .DataSource = (New TransferStudentsToAnotherProgramFacade).GetSchedules(ddlTargetCampusId.SelectedValue, userId, TargetProgramId, SourceProgramId)
            ''    .DataValueField = "ScheduleId"
            ''    .DataTextField = "Descrip"
            ''    .DataBind()
            ''    .Items.Insert(0, New ListItem("Select", ""))
            ''    .SelectedIndex = 0
            ''End With
        End If
    End Sub
    Protected Sub BuildSourceProgramVersion()
        If Trim(txtStudentName.Text) = "" Then
            DisplayErrorMessage("Please Select a student ")
            Exit Sub
        End If
        'ddlSourcePrgVerId.Items.Clear()
        BuildTargetProgram(lblSourceProgramId.Text)


        'Enable Targets
        ddlTargetCampusId.Enabled = True
        ddlTargetProgramId.Enabled = True
        ddlTargetPrgVerId.Enabled = True

        'If Not lblSourcePrgVerId.Text = "" And Not Trim(txtStudentName.Text) = "" Then
        '    With ddlSourcePrgVerId
        '        .DataSource = (New TransferStudentsToAnotherProgramFacade).GetSourceProgramVersion(ddlSourceProgramId.SelectedValue, txtStudentId.Text)
        '        .DataValueField = "PrgVerId"
        '        .DataTextField = "PrgVerDescrip"
        '        .DataBind()
        '        .Items.Insert(0, New ListItem("Select ProgramVersion", ""))
        '        .SelectedIndex = 0
        '    End With
        'Else
        '    With ddlSourcePrgVerId
        '        .Items.Insert(0, New ListItem("Select ProgramVersion", ""))
        '        .SelectedIndex = 0
        '    End With
        'End If
    End Sub
    Protected Sub BuildTargetProgramVersion()
        ddlTargetPrgVerId.Items.Clear()
        If Not ddlTargetProgramId.SelectedIndex = 0 And Not ddlTargetCampusId.SelectedIndex = 0 Then
            With ddlTargetPrgVerId
                .DataSource = (New TransferStudentsToAnotherProgramFacade).GetTargetProgramVersion(ddlTargetProgramId.SelectedValue, ddlTargetCampusId.SelectedValue, lblSourcePrgVerId.Text)
                .DataValueField = "PrgVerId"
                '.DataTextField = "PrgVerDescrip"
                .DataTextField = "PrgVerShiftDescrip"
                .DataBind()
                .Items.Insert(0, New ListItem("Select ProgramVersion", ""))
                .SelectedIndex = 0
            End With
        Else
            With ddlTargetPrgVerId
                .Items.Insert(0, New ListItem("Select ProgramVersion", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    'Protected Sub BuildProgramVersion()
    '    With ddlPrgVerId
    '        .DataSource = (New TransferStudentsToAnotherProgramFacade).GetProgramVersion(campusId)
    '        .DataValueField = "PrgVerId"
    '        .DataTextField = "PrgVerDescrip"
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Protected Sub ddlSourceProgramId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSourceProgramId.SelectedIndexChanged
    '    If Not lblSourcePrgVerId.Text = "" Then
    '        BuildSourceProgramVersion()
    '    End If
    'End Sub
    Protected Sub ddlTargetProgramId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTargetProgramId.SelectedIndexChanged
        If Not ddlTargetProgramId.SelectedIndex = 0 Then
            BuildTargetProgramVersion()
            lblCommonCourses.Visible = True
            dgrdTransactionSearch.Visible = False
            btnTransferCourses.Enabled = False

            Dim IsClockHourProgram As Boolean = False
            IsClockHourProgram = (New TransferStudentsToAnotherProgramFacade).IsProgramClockHourType(ddlTargetProgramId.SelectedValue)
            If IsClockHourProgram = True Then
                lblTransferHours.Visible = True
                txtTransferHours.Visible = True
            End If

        End If
    End Sub
    Private Function ValidateEntries() As String
        Dim strmess As String = ""
        If txtStudentName.Text = "" Then
            strmess = "Select a student;"
        ElseIf ddlTargetCampusId.Text = "Select" Or ddlTargetCampusId.Text = "" Then
            strmess = strmess + "Select a Campus to transfer to;"
        ElseIf ddlTargetProgramId.Text = "Select" Or ddlTargetProgramId.Text = "" Then
            strmess = strmess + "Select a Program to transfer to;"
        ElseIf ddlTargetPrgVerId.Text = "Select" Or ddlTargetPrgVerId.Text = "" Then
            strmess = strmess + "Select a Program Version to transfer to;"
            ''18453: QA: If there is a badge id to be transferred between programs then we need to force the user to select a schedule. 
            ''18461: QA: User is forced to select a schedule when there is no schedule to select from.

            '' DE 12167  -- The column ScheduleId was removed from arStuEnrollments table so we don't need this information
            ''ElseIf ddlScheduleId.Items.Count > 1 And (ddlScheduleId.Text = "" Or ddlScheduleId.Text = "Select") Then
            ''strmess = strmess + "Select a Schedule to transfer to;"
        Else
            strmess = ""
        End If
        Return strmess
    End Function


    Protected Sub btnTransferCourses_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTransferCourses.Click

        Dim strmess As String
        strmess = ValidateEntries()
        If strmess <> "" Then
            DisplayErrorMessage(strmess)
            Exit Sub
        End If


        Dim transferFacade As New TransferStudentsToAnotherProgramFacade
        Dim stuEnrollId As String = transferFacade.GetStudentEnrollmentByProgramVersion(txtStudentId.Text, ddlTargetPrgVerId.SelectedValue)
        If stuEnrollId = "" Then
            stuEnrollId = Guid.NewGuid.ToString
        End If
        Dim strMessage As String
        Dim enrollmentId As New EnrollmentComponent
        Dim enrollFacade As New StuEnrollFacade
        Dim strEnrollmentMessage As String
        Dim strTransferGradeMessage As String
        Dim strAlertMessage As String = ""
        Dim prgVersion As String = ddlTargetPrgVerId.SelectedItem.Text
        Dim fullName() As String = Split(enrollFacade.StudentName(txtStudentId.Text))
        Dim strFirstName As String = fullName(0)
        Dim strLastName As String = fullName(1)
        Dim decTransferHrs As Decimal = 0
        If txtTransferHours.Text <> "" Then decTransferHrs = txtTransferHours.Text
        'To Generate EnrollmentID 
        Dim strEnrollmentId As String = enrollmentId.GenerateEnrollmentID(strLastName, strFirstName)
        Dim strGradeMessage As String = "Student Enrollment and Ledger information has been successfully transferred."

        If dgrdTransactionSearch.Visible = False Then
            Try
                strEnrollmentMessage = transferFacade.InsertStudentEnrollment(stuEnrollId, txtStudentId.Text, ddlTargetCampusId.SelectedValue, ddlTargetPrgVerId.SelectedValue, AdvantageSession.UserState.UserName, lblSourcePrgVerId.Text, txtEnrollDate.SelectedDate, txtExpStartDate.SelectedDate, txtExpGradDate.SelectedDate, strEnrollmentId, campusId, ddlShiftId.SelectedValue, decTransferHrs)
                strTransferGradeMessage = transferFacade.TransferGrades(stuEnrollId, Nothing, Nothing, Nothing, AdvantageSession.UserState.UserName, Nothing, Nothing, txtStudentId.Text, lblSourcePrgVerId.Text, ddlTargetCampusId.SelectedValue)

                Dim token As TokenResponse

                token = CType(Session("AdvantageApiToken"), TokenResponse)
                Dim gStuEnrollId = Guid.Parse(stuEnrollId)
                If (token IsNot Nothing) Then
                    Dim enrollmentRequest As New FAME.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(token.ApiUrl, token.Token)
                    Dim enrollmentDetails = enrollmentRequest.GetAfaEnrollmentDetails(gStuEnrollId)

                    If (enrollmentDetails IsNot Nothing) Then
                        'Sync enrollment with AFA
                        Dim schedHoursByWeek = (New AttendanceFacade).GetScheduledHoursByWeek(stuEnrollId)
                        Dim afaEnr = New AfaEnrollment With {.StudentEnrollmentId = Guid.Parse(stuEnrollId), .StudentId = Guid.Parse(txtStudentId.Text),
                                .ProgramVersionId = Guid.Parse(ddlTargetPrgVerId.SelectedValue), .ProgramName = ddlTargetPrgVerId.SelectedItem.Text, .CampusId = Guid.Parse(ddlTargetCampusId.SelectedValue),
                                .StartDate = txtExpStartDate.SelectedDate, .ExpectedGradDate = txtExpGradDate.SelectedDate, .DroppedDate = enrollmentDetails.DroppedDate, .EnrollmentStatus = enrollmentDetails.EnrollmentStatus,
                             .AdmissionsCriteria = enrollmentDetails.AdmissionsCriteria, .AFAStudentId = enrollmentDetails.AfaStudentId, .CmsId = enrollmentDetails.CMSId, .LastDateAttendance = enrollmentDetails.LastDateAttendance,
                             .LeaveOfAbsenceDate = enrollmentDetails.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollmentDetails.ReturnFromLOADate, .StatusEffectiveDate = enrollmentDetails.StatusEffectiveDate,
                             .SSN = enrollmentDetails.SSN, .CreatedBy = enrollmentDetails.ModUser, .CreatedDate = enrollmentDetails.ModDate, .UpdatedBy = enrollmentDetails.ModUser, .UpdatedDate = enrollmentDetails.ModDate, .AdvStudentNumber = enrollmentDetails.AdvStudentNumber, .HoursPerWeek = schedHoursByWeek, .InSchoolTransferHrs = enrollmentDetails.InSchoolTransferHrs, .PriorSchoolHrs = enrollmentDetails.PriorSchoolHrs}
                        SyncEnrollmentsWithAFA(afaEnr)
                    End If
                End If

                ResetControls()

                DisplayInfoMessage(strGradeMessage)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Exit Sub
            End Try
            btnTransferCourses.Enabled = False
            Exit Sub
        End If
        strMessage = ValidateGradeAndTerm()
        If Not strMessage = "" Then
            DisplayErrorMessage(strMessage)
            Exit Sub
        End If


        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer = 0
        Dim intArrCount As Integer = 0


        'Check if any of the item is selected in the datagrid
        iitems = dgrdTransactionSearch.Items
        'Loop thru the collection to get the Array Size
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
                intArrCount += 1
            End If
        Next
        If intArrCount >= 1 Then
            Try
                transferFacade.InsertStudentEnrollment(stuEnrollId, txtStudentId.Text, ddlTargetCampusId.SelectedValue, ddlTargetPrgVerId.SelectedValue, AdvantageSession.UserState.UserName, lblSourcePrgVerId.Text, txtEnrollDate.SelectedDate, txtExpStartDate.SelectedDate, txtExpGradDate.SelectedDate, strEnrollmentId, campusId, ddlShiftId.SelectedValue, decTransferHrs)
                TransferGrades(stuEnrollId)
                'Reset the controls on the page. If this is not done the user will be able to hit the Transfer Courses button again and again
                'and have a new enrollment created each time.

                Dim token As TokenResponse

                token = CType(Session("AdvantageApiToken"), TokenResponse)
                Dim gStuEnrollId = Guid.Parse(stuEnrollId)
                If (token IsNot Nothing) Then
                    Dim enrollmentRequest As New FAME.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(token.ApiUrl, token.Token)
                    Dim enrollmentDetails = enrollmentRequest.GetAfaEnrollmentDetails(gStuEnrollId)

                    If (enrollmentDetails IsNot Nothing) Then
                        'Sync enrollment with AFA
                        Dim schedHoursByWeek = (New AttendanceFacade).GetScheduledHoursByWeek(stuEnrollId)
                        Dim afaEnr = New AfaEnrollment With {.StudentEnrollmentId = Guid.Parse(stuEnrollId), .StudentId = Guid.Parse(txtStudentId.Text),
                                .ProgramVersionId = Guid.Parse(ddlTargetPrgVerId.SelectedValue), .ProgramName = ddlTargetPrgVerId.SelectedItem.Text, .CampusId = Guid.Parse(ddlTargetCampusId.SelectedValue),
                                .StartDate = txtExpStartDate.SelectedDate, .ExpectedGradDate = txtExpGradDate.SelectedDate, .DroppedDate = enrollmentDetails.DroppedDate, .EnrollmentStatus = enrollmentDetails.EnrollmentStatus,
                             .AdmissionsCriteria = enrollmentDetails.AdmissionsCriteria, .AFAStudentId = enrollmentDetails.AfaStudentId, .CmsId = enrollmentDetails.CMSId, .LastDateAttendance = enrollmentDetails.LastDateAttendance,
                             .LeaveOfAbsenceDate = enrollmentDetails.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollmentDetails.ReturnFromLOADate, .StatusEffectiveDate = enrollmentDetails.StatusEffectiveDate,
                             .SSN = enrollmentDetails.SSN, .CreatedBy = enrollmentDetails.ModUser, .CreatedDate = enrollmentDetails.ModDate, .UpdatedBy = enrollmentDetails.ModUser, .UpdatedDate = enrollmentDetails.ModDate, .AdvStudentNumber = enrollmentDetails.AdvStudentNumber, .HoursPerWeek = schedHoursByWeek, .InSchoolTransferHrs = enrollmentDetails.InSchoolTransferHrs, .PriorSchoolHrs = enrollmentDetails.PriorSchoolHrs}
                        SyncEnrollmentsWithAFA(afaEnr)
                    End If
                End If

                ResetControls()

                DisplayInfoMessage(strGradeMessage)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Exit Sub
            End Try
        Else
            DisplayErrorMessage("Please select the course to be transferred ")
            Exit Sub
        End If
        BindDataGrid()
    End Sub
    Protected Sub TransferGrades(ByVal StuEnrollId As String)
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim strCourseId, strNewGradeId, strTermId, strResultId As String
        Dim intArrCount As Integer
        Dim z As Integer
        Dim TransferFacade As New TransferStudentsToAnotherProgramFacade
        Dim intIsCourseCompleted As Integer

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        'Loop thru the collection to get the Array Size
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
                intArrCount += 1
            End If
        Next

        'Declare Arrays
        Dim selectedCourse() As String = Array.CreateInstance(GetType(String), intArrCount)
        Dim selectedGrade() As String = Array.CreateInstance(GetType(String), intArrCount)
        Dim selectedTerm() As String = Array.CreateInstance(GetType(String), intArrCount)
        Dim selectedResult() As String = Array.CreateInstance(GetType(String), intArrCount)
        Dim selectedIsCourseCompleted() As Integer = Array.CreateInstance(GetType(Integer), intArrCount)

        'Store Values in Array
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            strCourseId = CType(iitem.FindControl("lblReqId"), Label).Text.ToString
            If Not MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                strNewGradeId = CType(iitem.FindControl("ddlEditGrdSysDetailId"), DropDownList).SelectedValue.ToString
            Else
                strNewGradeId = CType(iitem.FindControl("lblOldGrade"), Label).Text.ToString
            End If
            strTermId = CType(iitem.FindControl("ddlEditTermId"), DropDownList).SelectedValue.ToString
            strResultId = CType(iitem.FindControl("lblResultId"), Label).Text
            intIsCourseCompleted = CInt(CType(iitem.FindControl("lblIsCourseCompleted"), Label).Text)

            If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
                selectedCourse.SetValue(strCourseId, z)
                selectedGrade.SetValue(strNewGradeId, z)
                selectedTerm.SetValue(strTermId, z)
                selectedResult.SetValue(strResultId, z)
                selectedIsCourseCompleted.SetValue(intIsCourseCompleted, z)
                z += 1
            End If
        Next
        'resize the array
        If z > 0 Then ReDim Preserve selectedCourse(z - 1)
        If z > 0 Then ReDim Preserve selectedGrade(z - 1)
        If z > 0 Then ReDim Preserve selectedTerm(z - 1)
        If z > 0 Then ReDim Preserve selectedResult(z - 1)
        Try
            TransferFacade.TransferGrades(StuEnrollId, selectedCourse, selectedGrade, selectedTerm, AdvantageSession.UserState.UserName, selectedResult, selectedIsCourseCompleted, txtStudentId.Text, lblSourcePrgVerId.Text, ddlTargetCampusId.SelectedValue)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Exit Sub
        End Try
        'For i = 0 To iitems.Count - 1
        '    iitem = iitems.Item(i)
        '    If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
        '        CType(iitem.FindControl("chkTransfer"), CheckBox).Enabled = False
        '    End If
        'Next
    End Sub
    Protected Sub TransferGradesForAllPrograms(ByVal StuEnrollId As String, ByVal ds As DataSet)
        'Dim iitems As DataGridItemCollection
        'Dim iitem As DataGridItem
        'Dim i As Integer
        Dim strCourseId, strNewGradeId, strTermId, strResultId As String
        Dim intArrCount As Integer
        Dim intIsCourseCompleted As Integer
        Dim z As Integer
        Dim TransferFacade As New TransferStudentsToAnotherProgramFacade

        '' Save the datagrid items in a collection.
        'iitems = dgrdTransactionSearch.Items
        ''Loop thru the collection to get the Array Size
        'For i = 0 To iitems.Count - 1
        '    iitem = iitems.Item(i)
        '    'If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
        '    intArrCount += 1
        '    'End If
        'Next

        If ds.Tables(0).Rows.Count >= 1 Then
            intArrCount = ds.Tables(0).Rows.Count
            Dim selectedCourse() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedGrade() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedTerm() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedResult() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedIsCourseCompleted() As Integer = Array.CreateInstance(GetType(Integer), intArrCount)

            For Each dr As DataRow In ds.Tables(0).Rows
                strCourseId = dr("ReqId").ToString 'CType(iitem.FindControl("lblReqId"), Label).Text.ToString
                If Not MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    strNewGradeId = dr("GrdSysDetailId").ToString 'CType(iitem.FindControl("ddlEditGrdSysDetailId"), DropDownList).SelectedValue.ToString
                Else
                    strNewGradeId = dr("Grade").ToString
                End If
                strTermId = dr("termId").ToString 'CType(iitem.FindControl("ddlEditTermId"), DropDownList).SelectedValue.ToString
                strResultId = dr("ResultId").ToString 'CType(iitem.FindControl("lblResultId"), Label).Text
                intIsCourseCompleted = dr("IsCourseCompleted")
                'If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
                selectedCourse.SetValue(strCourseId, z)
                selectedGrade.SetValue(strNewGradeId, z)
                selectedTerm.SetValue(strTermId, z)
                selectedResult.SetValue(strResultId, z)
                selectedIsCourseCompleted.SetValue(intIsCourseCompleted, z)
                z += 1
                ' End If
            Next

            'resize the array
            If z > 0 Then ReDim Preserve selectedCourse(z - 1)
            If z > 0 Then ReDim Preserve selectedGrade(z - 1)
            If z > 0 Then ReDim Preserve selectedTerm(z - 1)
            If z > 0 Then ReDim Preserve selectedResult(z - 1)
            Try
                TransferFacade.TransferGrades(StuEnrollId, selectedCourse, selectedGrade, selectedTerm, AdvantageSession.UserState.UserName, selectedResult, selectedIsCourseCompleted, txtStudentId.Text, lblSourcePrgVerId.Text, ddlTargetCampusId.SelectedValue, True)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Exit Sub
            End Try
        End If
        'For i = 0 To iitems.Count - 1
        '    iitem = iitems.Item(i)
        '    If (CType(iitem.FindControl("chkTransfer"), CheckBox).Checked = True) Then
        '        CType(iitem.FindControl("chkTransfer"), CheckBox).Enabled = False
        '    End If
        'Next
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    Private Sub DisplayInfoMessage(ByVal infoMessage As String)
        CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, infoMessage)
    End Sub

    Private Sub CheckAllInDataGrid()
        Dim z1 As Integer
        Dim dgItems1 As DataGridItemCollection
        Dim dgItem1 As DataGridItem
        Dim dgGrid As DataGrid
        Dim strPrgVerId As String = ddlTargetPrgVerId.SelectedValue.ToString
        ' dgGrid = CType(FindControl("dgrdTransactionSearch"), DataGrid)
        dgGrid = CType(MyBase.FindControlRecursive("dgrdTransactionSearch"), DataGrid)
        dgItems1 = dgGrid.Items
        For z1 = 0 To dgItems1.Count - 1
            dgItem1 = dgItems1.Item(z1)
            Dim chkAll As CheckBox = CType(dgItem1.FindControl("chkTransfer"), CheckBox)
            chkAll.Checked = True
        Next
    End Sub
    Protected Sub txtStudentName_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtStudentName.TextChanged
    End Sub
    Private Function ValidateGradeAndTerm() As String
        Dim z1 As Integer
        Dim dgItems1 As DataGridItemCollection
        Dim dgItem1 As DataGridItem
        Dim dgGrid As DataGrid
        Dim strMessage As String = ""
        Dim strPrgVerId As String = ddlTargetPrgVerId.SelectedValue.ToString
        '   dgGrid = CType(FindControl("dgrdTransactionSearch"), DataGrid)
        dgGrid = CType(MyBase.FindControlRecursive("dgrdTransactionSearch"), DataGrid)
        dgItems1 = dgGrid.Items
        For z1 = 0 To dgItems1.Count - 1
            dgItem1 = dgItems1.Item(z1)
            Dim chk As CheckBox = CType(dgItem1.FindControl("chkTransfer"), CheckBox)
            Dim Course As String = CType(dgItem1.FindControl("GrdBkLinkButton"), Label).Text.ToString
            If chk.Checked = True Then
                Dim ddl1 As DropDownList = CType(dgItem1.FindControl("ddlEditTermId"), DropDownList)
                If Not MyAdvAppSettings.AppSettings("GradesFormat").ToString.ToLower = "numeric" Then
                    Dim ddl As DropDownList = CType(dgItem1.FindControl("ddlEditGrdSysDetailId"), DropDownList)
                    If ddl.SelectedValue = "" Then
                        strMessage &= "Select the transfer grade for " & Course & vbLf
                    End If
                End If
                If ddl1.Enabled = True And ddl1.SelectedValue = "" Then
                    strMessage &= "Select the term for " & Course & vbLf
                End If
            End If
        Next
        Return strMessage
    End Function
    Private Sub BindDataGrid()
        Dim ds As New DataSet
        If Not txtStudentId.Text = "" And Not lblSourcePrgVerId.Text = "" And Not ddlTargetPrgVerId.SelectedIndex = 0 Then
            ds = (New TransferStudentsToAnotherProgramFacade).GetCommonCoursesAndGrades(txtStudentId.Text, lblSourcePrgVerId.Text, ddlTargetPrgVerId.SelectedValue)
            If ds.Tables(0).Rows.Count >= 1 Then
                dgrdTransactionSearch.Visible = True
                lblCommonCourses.Visible = False
                dgrdTransactionSearch.DataSource = ds
                dgrdTransactionSearch.DataBind()
                CheckAllInDataGrid()
                label3.Text = "List of (graded) courses taken by " & txtStudentName.Text & " for " & lblSourcePrgVerName.Text '& " and common to " & ddlTargetPrgVerId.SelectedItem.Text
                btnTransferCourses.Enabled = True
            Else
                lblCommonCourses.Visible = True
                dgrdTransactionSearch.Visible = False
                Dim intEnrollmentExists As Integer = 0
                intEnrollmentExists = (New TransferStudentsToAnotherProgramFacade).IsStudentAlreadyEnrolled(txtStudentId.Text, ddlTargetPrgVerId.SelectedValue)
                If intEnrollmentExists >= 1 Then
                    btnTransferCourses.Enabled = False
                Else
                    btnTransferCourses.Enabled = True
                End If
            End If
        End If
    End Sub
    Protected Sub ddlTargetCampusId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTargetCampusId.SelectedIndexChanged
        If Not ddlTargetCampusId.SelectedValue = "" Then
            BuildTargetProgram(lblSourceProgramId.Text)
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
    End Sub
    Protected Sub ddlTargetPrgVerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTargetPrgVerId.SelectedIndexChanged
        If Not ddlTargetPrgVerId.SelectedValue = "" Then
            BuildSchedule(ddlTargetPrgVerId.SelectedValue, lblSourcePrgVerId.Text)
            chkTransferAllPrograms.Text = "Transfer all courses to " & ddlTargetPrgVerId.SelectedItem.Text
        End If
    End Sub
    Protected Sub chkTransferAllPrograms_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkTransferAllPrograms.CheckedChanged
        If chkTransferAllPrograms.Checked = True Then
            btnGetCourses.Enabled = False
            btnTransferAllCourses.Enabled = True
        Else
            btnGetCourses.Enabled = True
            btnTransferAllCourses.Enabled = False
        End If
    End Sub
    Protected Sub btnTransferAllCourses_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTransferAllCourses.Click

        Dim strmess As String
        strmess = ValidateEntries()
        If strmess <> "" Then
            DisplayErrorMessage(strmess)
            Exit Sub
        End If

        Dim ds As DataSet
        Dim transferFacade As New TransferStudentsToAnotherProgramFacade
        Dim stuEnrollId As String = transferFacade.GetStudentEnrollmentByProgramVersion(txtStudentId.Text, ddlTargetPrgVerId.SelectedValue)
        If stuEnrollId = "" Then
            stuEnrollId = Guid.NewGuid.ToString
        End If
        'Dim strMessage As String = ""
        Dim enrollmentId As New EnrollmentComponent
        Dim enrollFacade As New StuEnrollFacade
        Dim strEnrollmentMessage As String
        Dim strTransferGradeMessage As String
        Dim strAlertMessage As String = ""
        Dim prgVersion As String = ddlTargetPrgVerId.SelectedItem.Text
        Dim fullName() As String = Split(enrollFacade.StudentName(txtStudentId.Text))
        Dim strFirstName As String = fullName(0)
        Dim strLastName As String = fullName(1)
        Dim decTransferHrs As Decimal = 0
        If txtTransferHours.Text <> "" Then decTransferHrs = txtTransferHours.Text
        'To Generate EnrollmentID 
        Dim strEnrollmentId As String = enrollmentId.GenerateEnrollmentID(strLastName, strFirstName)
        Dim strGradeMessage As String = "Student Enrollment and Ledger information has been successfully transferred."

        If Not txtStudentId.Text = "" And Not lblSourcePrgVerId.Text = "" And Not ddlTargetPrgVerId.SelectedIndex = 0 Then
            ds = (New TransferStudentsToAnotherProgramFacade).GetCommonCoursesAndGrades(txtStudentId.Text, lblSourcePrgVerId.Text, ddlTargetPrgVerId.SelectedValue, "", True)
            If ds.Tables(0).Rows.Count >= 1 Then
                Try
                    transferFacade.InsertStudentEnrollment(stuEnrollId, txtStudentId.Text, ddlTargetCampusId.SelectedValue, ddlTargetPrgVerId.SelectedValue, AdvantageSession.UserState.UserName, lblSourcePrgVerId.Text, txtEnrollDate.SelectedDate, txtExpStartDate.SelectedDate, txtExpGradDate.SelectedDate, strEnrollmentId, campusId, ddlShiftId.SelectedValue, decTransferHrs)
                    TransferGradesForAllPrograms(stuEnrollId, ds)

                    Dim studentEnrollmentDa = (New StudentEnrollmentDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString))
                    Dim programVersionDa = (New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString))
                    Dim newEnrollmentId = studentEnrollmentDa.GetNewlyTransferEnrollmentId(txtStudentId.Text, ddlTargetCampusId.SelectedValue, ddlTargetPrgVerId.SelectedValue)
                    Dim sourceProgId = programVersionDa.GetProgramVersionProgramId(lblSourcePrgVerId.Text, txtStudentId.Text)
                    studentEnrollmentDa.UpdateTransferHours(newEnrollmentId.ToString(), decTransferHrs, If(ddlTargetCampusId.SelectedValue = campusId, decTransferHrs, 0), If(ddlTargetCampusId.SelectedValue = campusId, sourceProgId.ToString(), Guid.Empty.ToString()))

                    Dim token As TokenResponse

                    token = CType(Session("AdvantageApiToken"), TokenResponse)
                    Dim gStuEnrollId = Guid.Parse(stuEnrollId)
                    If (token IsNot Nothing) Then
                        Dim enrollmentRequest As New FAME.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(token.ApiUrl, token.Token)
                        Dim enrollmentDetails = enrollmentRequest.GetAfaEnrollmentDetails(gStuEnrollId)

                        If (enrollmentDetails IsNot Nothing) Then
                            'Sync enrollment with AFA
                            Dim schedHoursByWeek = (New AttendanceFacade).GetScheduledHoursByWeek(stuEnrollId)
                            Dim afaEnr = New AfaEnrollment With {.StudentEnrollmentId = Guid.Parse(stuEnrollId), .StudentId = Guid.Parse(txtStudentId.Text),
                                    .ProgramVersionId = Guid.Parse(ddlTargetPrgVerId.SelectedValue), .ProgramName = ddlTargetPrgVerId.SelectedItem.Text, .CampusId = Guid.Parse(ddlTargetCampusId.SelectedValue),
                                    .StartDate = txtExpStartDate.SelectedDate, .ExpectedGradDate = txtExpGradDate.SelectedDate, .DroppedDate = enrollmentDetails.DroppedDate, .EnrollmentStatus = enrollmentDetails.EnrollmentStatus,
                                 .AdmissionsCriteria = enrollmentDetails.AdmissionsCriteria, .AFAStudentId = enrollmentDetails.AfaStudentId, .CmsId = enrollmentDetails.CMSId, .LastDateAttendance = enrollmentDetails.LastDateAttendance,
                                 .LeaveOfAbsenceDate = enrollmentDetails.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollmentDetails.ReturnFromLOADate, .StatusEffectiveDate = enrollmentDetails.StatusEffectiveDate,
                                 .SSN = enrollmentDetails.SSN, .CreatedBy = enrollmentDetails.ModUser, .CreatedDate = enrollmentDetails.ModDate, .UpdatedBy = enrollmentDetails.ModUser, .UpdatedDate = enrollmentDetails.ModDate, .AdvStudentNumber = enrollmentDetails.AdvStudentNumber, .HoursPerWeek = schedHoursByWeek, .InSchoolTransferHrs = enrollmentDetails.InSchoolTransferHrs, .PriorSchoolHrs = enrollmentDetails.PriorSchoolHrs}
                            SyncEnrollmentsWithAFA(afaEnr)
                        End If
                    End If



                    ResetControls()


                    DisplayInfoMessage(strGradeMessage)
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    DisplayErrorMessage(ex.Message)
                    Exit Sub
                End Try
            Else
                Try
                    strEnrollmentMessage = transferFacade.InsertStudentEnrollment(stuEnrollId, txtStudentId.Text, ddlTargetCampusId.SelectedValue, ddlTargetPrgVerId.SelectedValue, AdvantageSession.UserState.UserName, lblSourcePrgVerId.Text, txtEnrollDate.SelectedDate, txtExpStartDate.SelectedDate, txtExpGradDate.SelectedDate, strEnrollmentId, campusId, ddlShiftId.SelectedValue, decTransferHrs)
                    strTransferGradeMessage = transferFacade.TransferGrades(stuEnrollId, Nothing, Nothing, Nothing, AdvantageSession.UserState.UserName, Nothing, Nothing, txtStudentId.Text, lblSourcePrgVerId.Text, ddlTargetCampusId.SelectedValue, True)

                    Dim token As TokenResponse

                    token = CType(Session("AdvantageApiToken"), TokenResponse)
                    Dim gStuEnrollId = Guid.Parse(stuEnrollId)
                    If (token IsNot Nothing) Then
                        Dim enrollmentRequest As New FAME.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(token.ApiUrl, token.Token)
                        Dim enrollmentDetails = enrollmentRequest.GetAfaEnrollmentDetails(gStuEnrollId)

                        If (enrollmentDetails IsNot Nothing) Then
                            'Sync enrollment with AFA
                            Dim schedHoursByWeek = (New AttendanceFacade).GetScheduledHoursByWeek(stuEnrollId)
                            Dim afaEnr = New AfaEnrollment With {.StudentEnrollmentId = Guid.Parse(stuEnrollId), .StudentId = Guid.Parse(txtStudentId.Text),
                                    .ProgramVersionId = Guid.Parse(ddlTargetPrgVerId.SelectedValue), .ProgramName = ddlTargetPrgVerId.SelectedItem.Text, .CampusId = Guid.Parse(ddlTargetCampusId.SelectedValue),
                                    .StartDate = txtExpStartDate.SelectedDate, .ExpectedGradDate = txtExpGradDate.SelectedDate, .DroppedDate = enrollmentDetails.DroppedDate, .EnrollmentStatus = enrollmentDetails.EnrollmentStatus,
                                 .AdmissionsCriteria = enrollmentDetails.AdmissionsCriteria, .AFAStudentId = enrollmentDetails.AfaStudentId, .CmsId = enrollmentDetails.CMSId, .LastDateAttendance = enrollmentDetails.LastDateAttendance,
                                 .LeaveOfAbsenceDate = enrollmentDetails.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollmentDetails.ReturnFromLOADate, .StatusEffectiveDate = enrollmentDetails.StatusEffectiveDate,
                                 .SSN = enrollmentDetails.SSN, .CreatedBy = enrollmentDetails.ModUser, .CreatedDate = enrollmentDetails.ModDate, .UpdatedBy = enrollmentDetails.ModUser, .UpdatedDate = enrollmentDetails.ModDate, .AdvStudentNumber = enrollmentDetails.AdvStudentNumber, .HoursPerWeek = schedHoursByWeek, .InSchoolTransferHrs = enrollmentDetails.InSchoolTransferHrs, .PriorSchoolHrs = enrollmentDetails.PriorSchoolHrs}
                            SyncEnrollmentsWithAFA(afaEnr)
                        End If
                    End If

                    ResetControls()

                    DisplayInfoMessage(strGradeMessage)
                    Exit Sub
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    DisplayErrorMessage(ex.Message)
                    Exit Sub
                End Try
            End If
        End If
    End Sub

    Protected Sub ResetControls()
        txtStuEnrollmentId.Text = ""
        txtStudentName.Text = ""
        txtTermId.Text = ""
        txtTerm.Text = ""
        txtAcademicYear.Text = ""
        txtEnrollDate.SelectedDate = Nothing
        txtExpStartDate.SelectedDate = Nothing
        txtExpGradDate.SelectedDate = Nothing
        ddlShiftId.SelectedItem.Text = ""
        lblSourceCampus.Text = ""
        lblSourceProgramName.Text = ""
        lblSourcePrgVerName.Text = ""
        lblSourcePrgVerId.Text = ""
        txtStudentId.Text = ""
        ddlTargetCampusId.SelectedIndex = 0
        ddlTargetProgramId.SelectedIndex = 0
        ddlTargetPrgVerId.SelectedIndex = 0
        '' DE 12167  -- The column ScheduleId was removed from arStuEnrollments table so we don't need this information
        ''ddlScheduleId.SelectedIndex = 0
        txtTransferHours.Text = ""
    End Sub


    Private Sub SyncEnrollmentsWithAFA(enrollment As AfaEnrollment)
        Try
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            If (wapiSetting IsNot Nothing) Then

                Dim afaEnrollments = New LocationsHelper(connectionString, AdvantageSession.UserState.UserName).SyncEnrollmentsWithAFA(enrollment)
                If (afaEnrollments.Count > 0) Then

                    Dim enrReq = New FAME.AFA.Api.Library.AcademicRecords.EnrollmentRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    Dim result = enrReq.SendEnrollmentToAFA(afaEnrollments)
                End If

            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub
End Class
