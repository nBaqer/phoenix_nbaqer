' ===============================================================================
' IMaint_GrdBookWeightingsCourse.ascx
' RHS for setting up grade book weights
' Uses IMaintFormBase interface
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' IMaintFormBase properties
'       ParentId = ReqId
'       ObjId = GrdBookWgtId

Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade.AR
Imports Fame.AdvantageV1.Common.AR
Imports Fame.Advantage.Common
Imports BO = Advantage.Business.Objects

Partial Class AR_IMaint_GrdBookWeightingsCourse
    Inherits System.Web.UI.UserControl
    Implements IMaintFormBase
    Protected MyAdvAppSettings As AdvAppSettings


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            If Not Page.IsPostBack Then
                If ParentId IsNot Nothing AndAlso ParentId <> "" Then
                    Dim info As CourseInfo = CoursesFacade.GetCoureseInfo(ParentId)
                    lblCourse.Text = info.Descrip
                    hfCourseCredits.Value = info.Credits
                    If info.IsExternship And MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "instructorlevel" Then
                        ViewState("InstructorLevelExternship") = True
                    Else
                        ViewState("InstructorLevelExternship") = False
                    End If
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            lblCourse.Text = "N/A"
        End Try
    End Sub

    Public Function BindForm(ByVal ID As String, Optional ByVal campusId As String = "", Optional ByVal Permission As String = "", Optional ByVal InSchoolEnrollmentStatus As String = "", Optional ByVal OutSchoolEnrollmentStatus As String = "") As String Implements IMaintFormBase.BindForm
        Try
            Dim info As GrdBkWgtsInfo = GradeBookFacade.GetGrdBookWgtsInfo(ID)

            ' set IMaintFormBase params
            ParentId = info.ReqId
            ObjId = info.GrdBkWgtId
            ModDate = info.ModDate

            Me.txtEffectiveDate.Text = info.EffectiveDate.ToShortDateString()
            rptDetails.DataSource = info.DetailsDT
            rptDetails.DataBind()

            Dim c As Integer = rptDetails.Items.Count - 1
            Dim details(c) As GrdBkWgtDetailsInfo
            Dim boolRequired As Boolean = False
            Dim boolMustPass As Boolean = False
            For i As Integer = 0 To c
                Dim rpi As RepeaterItem = Me.rptDetails.Items(i)
                boolRequired = info.DetailsDT.Rows(i)(7)  'Column 7 is Required
                boolMustPass = info.DetailsDT.Rows(i)(8)  'Column 8 is Mustpass
                CType(rpi.FindControl("chkRequired"), CheckBox).Checked = boolRequired
                CType(rpi.FindControl("chkMustPass"), CheckBox).Checked = boolMustPass
            Next


            ' return a message if this grade book is already in use
            If GradeBookFacade.IsGrdBkWgtInUse(info.GrdBkWgtId) And Session("UserName") <> "sa" Then
                Return "Changes cannot be made to this grade book weighting because it is in use."
            ElseIf GradeBookFacade.IsGrdBkWgtInUse(info.GrdBkWgtId) And Session("UserName").ToString.Trim = "sa" Then
                Return "Changes made to this grade book weighting may impact existing usage"
            End If
            Return "" ' Success
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Public Function GetLHSDataSet(ByVal ID As String, ByVal bActive As Boolean, ByVal bInactive As Boolean) As System.Data.DataSet Implements IMaintFormBase.GetLHSDataSet
        ' ID here is the CourseId
        If ID Is Nothing Or ID = "" Then Return Nothing
        Return GradeBookFacade.GetGrdBkWgts(ID, "", bActive, bInactive)
    End Function
    Public Function Handle_Delete() As String Implements IMaintFormBase.Handle_Delete
        Try
            If ObjId Is Nothing Or ObjId = "" Then Return "No record to delete"
            Dim res As String = GradeBookFacade.DeleteGrdBkWgt(ObjId, ModDate)
            If res <> "" Then
                Return res
            End If

            ObjId = ""
            ModDate = Nothing
            Return "" ' Success
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function
    Public Function Handle_New() As String Implements IMaintFormBase.Handle_New
        Try
            ObjId = Nothing
            ModDate = Nothing
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim CampusId As String = ARCommon.GetCampusID()
            Me.rptDetails.DataSource = GradeBookFacade.GetEmptyGrdBkWgtDetails(CampusId)
            Me.rptDetails.DataBind()

            Me.txtEffectiveDate.Text = Date.Now.ToShortDateString()
            Return "" ' Success
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Public Function Handle_Save() As String Implements IMaintFormBase.Handle_Save
        Try
            Dim res As String = ValidateForm()
            If res <> "" Then Return res

            Dim gid As String = ObjId
            Dim info As New GrdBkWgtsInfo

            ' determine if this is an update or a new record by looking
            If gid Is Nothing Or gid = "" Then
                info.IsInDb = False
                ObjId = info.GrdBkWgtId
            Else
                info.IsInDb = True
                info.GrdBkWgtId = ObjId
            End If

            info.Descrip = "Automatic"
            info.ReqId = ParentId
            info.Active = True ' Can this get set to inactive?
            info.EffectiveDate = Date.Parse(txtEffectiveDate.Text)
            info.ModDate = Date.Now
            info.ModUser = ARCommon.GetUserNameFromUserId(ARCommon.GetCurrentUserId())

            ' build up the array of grade book weight details
            Dim c As Integer = rptDetails.Items.Count - 1
            Dim details(c) As GrdBkWgtDetailsInfo
            For i As Integer = 0 To c

                Try


                    Dim rpi As RepeaterItem = Me.rptDetails.Items(i)
                    details(i) = New GrdBkWgtDetailsInfo()
                    details(i).GrdBkWgtId = info.GrdBkWgtId
                    details(i).GrdBkWgtDetailId = CType(rpi.FindControl("hfGrdBkWgtId"), HiddenField).Value
                    details(i).GrdCompTypeId = CType(rpi.FindControl("hfGrdCompTypeId"), HiddenField).Value
                    details(i).GrdSysCompTypeId = CType(rpi.FindControl("hfSysGrdCompTypeId"), HiddenField).Value
                    details(i).ModUser = CType(rpi.FindControl("hfModUser"), HiddenField).Value
                    details(i).Seq = i

                    ' get the weight
                    Dim txt As TextBox = CType(rptDetails.Items(i).FindControl("txtWeight"), eWorld.UI.NumericBox)
                    If Not txt.Text.Trim = "" Then
                        details(i).weight = Decimal.Parse(txt.Text.Trim)
                    Else
                        details(i).weight = Nothing
                    End If

                    ' get the number
                    txt = CType(rptDetails.Items(i).FindControl("txtNumber"), eWorld.UI.NumericBox)
                    If Not txt.Text.Trim = "" Then
                        details(i).Number = Decimal.Parse(txt.Text.Trim)
                    End If

                    ' get the parameter
                    txt = CType(rptDetails.Items(i).FindControl("txtParameter"), eWorld.UI.NumericBox)
                    If Not txt.Text.Trim = "" Then
                        details(i).Parameter = Integer.Parse(txt.Text.Trim)
                    End If



                    'update GrdPolicyId
                    Dim ddl As DropDownList = CType(rptDetails.Items(i).FindControl("ddlGrdPolicy"), DropDownList)
                    If ddl.SelectedIndex > 0 Then
                        Dim grdPolicyId As Integer = ddl.SelectedValue
                        details(i).GrdPolicyTypeId = grdPolicyId
                    End If

                    'required 
                    details(i).Required = CType(rptDetails.Items(i).FindControl("chkRequired"), CheckBox).Checked

                    'must pass
                    details(i).mustPass = CType(rptDetails.Items(i).FindControl("chkMustPass"), CheckBox).Checked



                    'get credits per service
                    If MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                        txt = CType(rptDetails.Items(i).FindControl("txtCreditsPerService"), eWorld.UI.NumericBox)
                        If Not txt.Text.Trim = "" Then
                            details(i).CreditsPerService = Decimal.Parse(txt.Text.Trim)
                        End If
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Dim idx As Integer = i
                End Try

            Next
            info.Details = details

            ' do the add or update 
            'Dim user As String = ARCommon.GetUserNameFromUserId(ARCommon.GetCurrentUserId())
            Dim advantageUserState As New BO.User()
            advantageUserState = AdvantageSession.UserState
            res = GradeBookFacade.UpdateGrdBkWgts(info, advantageUserState.UserName)

            ' if the update was successful, rebind the form so that everything display correctly.
            ' Also, this will update the ModDate so that clicking Save followed by Delete works properly
            If res = "" Then
                BindForm(ObjId)
            End If
            Return res ' return the result of the update
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function
    Public ReadOnly Property LHSImageUrl() As String Implements IMaintFormBase.LHSImageUrl
        Get
            Return "~/images/AR/lhs_grdcomptypes.gif"
        End Get
    End Property
    Public Property ModDate() As Date Implements IMaintFormBase.ModDate
        Get
            Return ViewState("moddate_GrdBkWgts")
        End Get
        Set(ByVal value As Date)
            ViewState("moddate_GrdBkWgts") = value
        End Set
    End Property
    Public Property ObjId() As String Implements IMaintFormBase.ObjId
        Get
            Return ViewState("objId_GrdBkWgts")
        End Get
        Set(ByVal value As String)
            ViewState("objId_GrdBkWgts") = value
        End Set
    End Property
    Public Property ParentId() As String Implements IMaintFormBase.ParentId
        Get
            Return ViewState("parentId_GrdBkWgts")
        End Get
        Set(ByVal value As String)
            ViewState("parentId_GrdBkWgts") = value
        End Set
    End Property
    Public Property Title() As String Implements IMaintFormBase.Title
        Get
            Return "Setup Grade Book Weights"
        End Get
        Set(ByVal value As String)

        End Set
    End Property

#Region "Repeater Helpers"
    ''' <summary>
    ''' Called for each item in the repeater control.
    ''' Gives us a chance to fix up the ui
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptDetails_OnItemBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptDetails.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            CType(e.Item.FindControl("hfGrdBkWgtId"), HiddenField).Value = e.Item.DataItem("ID").ToString()
            CType(e.Item.FindControl("hfGrdCompTypeId"), HiddenField).Value = e.Item.DataItem("GrdComponentTypeId").ToString()
            CType(e.Item.FindControl("hfSysGrdCompTypeId"), HiddenField).Value = e.Item.DataItem("SysComponentTypeId").ToString()
            CType(e.Item.FindControl("hfModUser"), HiddenField).Value = e.Item.DataItem("ModUser").ToString()
            CType(e.Item.FindControl("lblDescrip"), Label).Text = e.Item.DataItem("Descrip").ToString()
            CType(e.Item.FindControl("lblType"), Label).Text = e.Item.DataItem("ComponentType").ToString()
            CType(e.Item.FindControl("txtNumber"), eWorld.UI.NumericBox).Text = e.Item.DataItem("Number").ToString()
            CType(e.Item.FindControl("txtParameter"), eWorld.UI.NumericBox).Text = e.Item.DataItem("Parameter").ToString()
            If MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                CType(e.Item.FindControl("txtCreditsPerService"), eWorld.UI.NumericBox).Text = e.Item.DataItem("CreditsPerService").ToString()
            End If

            ' bind the grading book policies
            Dim ddl As DropDownList = CType(e.Item.FindControl("ddlGrdPolicy"), DropDownList)
            'instantiate a metadata object for Grade Policies ddl
            Dim gradingPoliciesDDLMetadata As New Fame.AdvantageV1.Common.GradePoliciesDDLMetadata()
            'add all metadataddl's to a list
            Dim ddlList As New System.Collections.Generic.List(Of Fame.AdvantageV1.Common.AdvantageDropDownListMetadata)
            ddlList.Add(gradingPoliciesDDLMetadata)
            'get ds with dropdownlists tables
            Dim ds As DataSet = (New Fame.AdvantageV1.BusinessFacade.DropDownListsFacade).GetDropDownLists(ddlList)
            gradingPoliciesDDLMetadata.Bind(ddl, ds, True) 'bind ddl's

            ddl.SelectedValue = e.Item.DataItem("GrdPolicyId").ToString()

            ' disable Weight text box if the grade component is Lab Work or Lab Hours
            Dim CompType As SysGradeBookComponentTypes = SysGradeBookComponentTypes.Homework
            If Not DBNull.Value.Equals(e.Item.DataItem("SysComponentTypeId")) Then
                CompType = CType(e.Item.DataItem("SysComponentTypeId"), SysGradeBookComponentTypes)
            End If

            If CompType = SysGradeBookComponentTypes.LabHours Then
                CType(e.Item.FindControl("txtWeight"), eWorld.UI.NumericBox).Enabled = False
                CType(e.Item.FindControl("txtNumber"), eWorld.UI.NumericBox).DecimalPlaces = 2
                CType(e.Item.FindControl("txtCreditsPerService"), eWorld.UI.NumericBox).DecimalPlaces = 2
            ElseIf CompType = SysGradeBookComponentTypes.LabWork Then
                CType(e.Item.FindControl("txtWeight"), eWorld.UI.NumericBox).Enabled = False
                CType(e.Item.FindControl("txtNumber"), eWorld.UI.NumericBox).DecimalPlaces = 0
                CType(e.Item.FindControl("txtCreditsPerService"), eWorld.UI.NumericBox).DecimalPlaces = 2
            ElseIf CompType = SysGradeBookComponentTypes.Externship Then
                CType(e.Item.FindControl("txtWeight"), eWorld.UI.NumericBox).Enabled = False
                CType(e.Item.FindControl("txtNumber"), eWorld.UI.NumericBox).DecimalPlaces = 0
                CType(e.Item.FindControl("txtCreditsPerService"), eWorld.UI.NumericBox).Enabled = False
            Else
                If ViewState("InstructorLevelExternship") Then
                    CType(e.Item.FindControl("txtWeight"), eWorld.UI.NumericBox).Text = e.Item.DataItem("Weight").ToString()
                    CType(e.Item.FindControl("txtNumber"), eWorld.UI.NumericBox).DecimalPlaces = 0
                    CType(e.Item.FindControl("txtWeight"), eWorld.UI.NumericBox).Enabled = False
                    CType(e.Item.FindControl("txtNumber"), eWorld.UI.NumericBox).Enabled = False
                    CType(e.Item.FindControl("txtCreditsPerService"), eWorld.UI.NumericBox).Enabled = False
                Else
                    CType(e.Item.FindControl("txtWeight"), eWorld.UI.NumericBox).Text = e.Item.DataItem("Weight").ToString()
                    CType(e.Item.FindControl("txtNumber"), eWorld.UI.NumericBox).DecimalPlaces = 0
                    CType(e.Item.FindControl("txtCreditsPerService"), eWorld.UI.NumericBox).Enabled = False
                End If
              
            End If

            ' add javascript to let the user click on the "To Maintenance Screen" button
            Dim ReqId As String = ParentId
            Dim GrdComponentTypeId As String = e.Item.DataItem("GrdComponentTypeId").ToString()
            Dim url As String = String.Format("MaintPopup.aspx?mod={0}&resid=1&cmpid={1}&pid={2}&objid={3}&ascx={4}", _
                    Session("mod"), ARCommon.GetCampusID(), ReqId, GrdComponentTypeId, "~/AR/IMaint_GrdCompTypes.ascx")
            Dim js As String = ARCommon.GetJavsScriptPopup(url, 800, 450, Nothing)
            Dim ib As ImageButton = CType(e.Item.FindControl("ibToMaintPage_GrdComp"), ImageButton)
            ib.Attributes.Add("onclick", js)
        End If
    End Sub

    ''' <summary>
    ''' Handle when the user click the "To Maintenance Page" button which is shown on
    ''' every grade book component.  We need to refresh the page in case they have
    ''' added a new grade book componenet.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptDetails_OnItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs) Handles rptDetails.ItemCommand
        If e.CommandName = "ToMaintGrdComps" Then
            If ObjId Is Nothing Or ObjId = "" Then
                Handle_New()
            Else
                BindForm(ObjId)
            End If
        End If
    End Sub
#End Region

#Region "Validate Form Methods"
    Private Function IsParameterRequired(ByVal grdPolicyId As String) As Boolean
        Return True ' just return true for now as all the GrdPolicy have AllowParam = 1
    End Function

    Private Function IsParameterLessOrEqualThanNumberOfTests(ByVal par As String, ByVal noftests As String) As Boolean
        Try
            If Integer.Parse(par) <= Integer.Parse(noftests) Then Return True Else Return False
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    ''' <summary>
    ''' Determines if lab work is the only field entered
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsOnlyLabEntered() As Boolean
        For Each rpi As RepeaterItem In rptDetails.Items
            Dim sSysGrdCompType As String = CType(rpi.FindControl("hfSysGrdCompTypeId"), HiddenField).Value()
            Dim CompType As SysGradeBookComponentTypes = SysGradeBookComponentTypes.Exam
            If sSysGrdCompType <> "" Then
                CompType = CType(sSysGrdCompType, SysGradeBookComponentTypes)
            End If
            Dim num As String = CType(rpi.FindControl("txtNumber"), eWorld.UI.NumericBox).Text()

            ' if the Number field has something in it and the Sys Component Type is not a lab,
            ' then we know that the lab is not the only field
            If (num <> "" AndAlso CompType <> SysGradeBookComponentTypes.LabHours AndAlso CompType <> SysGradeBookComponentTypes.LabWork) Then
                Return False
            End If
        Next
        Return True
    End Function

    ''' <summary>
    ''' Validates the form and returns any error message
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateForm() As String
        Dim errorMessage As String = ""
        Dim weightTotal As Decimal = 0
        Dim totalCreditsPerService As Decimal = 0.0
        Dim grdBkCount As Integer = Int32.Parse(MyAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings").ToString)
        'iterate over all GradeBookWeightings 
        For i As Integer = 0 To rptDetails.Items.Count - 1
            'validate weight
            Dim s As String = CType(rptDetails.Items(i).FindControl("txtWeight"), eWorld.UI.NumericBox).Text
            If s.Trim() <> "" Then
                weightTotal += Decimal.Parse(s)
            End If
            Dim compType As String = CType(rptDetails.Items(i).FindControl("hfSysGrdCompTypeId"), HiddenField).Value()

            'validate presence of required parameters
            Dim ddl As DropDownList = CType(rptDetails.Items(i).FindControl("ddlGrdPolicy"), DropDownList)

            If ddl.SelectedIndex > 0 Then
                Dim param As String = CType(rptDetails.Items(i).FindControl("txtParameter"), eWorld.UI.NumericBox).Text
                Dim num As String = CType(rptDetails.Items(i).FindControl("txtNumber"), eWorld.UI.NumericBox).Text
                If IsParameterRequired(ddl.SelectedValue) And param.Trim() = "" Then
                    errorMessage += "A parameter value for the Grading Policy " + ddl.SelectedItem.Text + " is required." + vbCrLf
                ElseIf Not IsParameterRequired(ddl.SelectedValue) And Not param.Trim() = "" Then
                    errorMessage += "The parameter value " + param + " for the Grading Policy " + ddl.SelectedItem.Text + " is not required." + vbCrLf
                End If

                'validate parameter
                If Not IsParameterLessOrEqualThanNumberOfTests(param.Trim(), num.Trim()) Then
                    errorMessage += "Parameter value " + param.Trim() + " is invalid. Must be less or equal to Number of Tests." + vbCrLf
                End If
            End If
            If compType = "499" Or compType = "501" Or compType = "502" Then
                Dim numCnt As String = CType(rptDetails.Items(i).FindControl("txtNumber"), eWorld.UI.NumericBox).Text
                If numCnt <> "" Then
                    If grdBkCount < Decimal.Parse(numCnt) Then
                        errorMessage += "Please provide a value for Number of " + CType(rptDetails.Items(i).FindControl("lblDescrip"), Label).Text + " less than the configuration variable 'MaxNoOfGradeBookWeightings'." + vbCrLf
                    End If
                End If
            End If
            'credits per service
            If MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                Dim txt As TextBox = CType(rptDetails.Items(i).FindControl("txtCreditsPerService"), eWorld.UI.NumericBox)
                Dim number As String = CType(rptDetails.Items(i).FindControl("txtNumber"), eWorld.UI.NumericBox).Text
                If txt.Text.Trim <> "" And number <> "" Then
                    totalCreditsPerService = totalCreditsPerService + (Decimal.Parse(number) * Decimal.Parse(txt.Text.Trim))
                End If
            End If

        Next

        'validate the total credits per service with course credits
        If MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
            If totalCreditsPerService > 0 Then
                If totalCreditsPerService = Decimal.Parse(hfCourseCredits.Value) Then

                Else
                    errorMessage += "Sum of credits per service must be equal to course credits value of " & Decimal.Parse(hfCourseCredits.Value) & "." + vbCrLf
                End If
            End If

        End If

        'validate sum of weights
        Dim bOnlyLab As Boolean = IsOnlyLabEntered()
        If Not bOnlyLab AndAlso Not weightTotal = 100 AndAlso Not weightTotal = 0 Then
            errorMessage += "Sum of weights must be '100' or '0'." + vbCrLf
        End If

        Return errorMessage
    End Function

#End Region

End Class
