<%@ Page Title="Post Services By Student" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostServicesByStudent.aspx.vb" Inherits="PostServicesByStudent" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="pnlMain">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <script src="../Scripts/ElementQueries.min.js"></script>
    <script src="../Scripts/ResizeSensor.min.js"></script>
    <script id="resizeScript" type="text/javascript">
        var $mainContainer;
        var $mainContent;
        var $containerScrollable;
        $(document).ready(function () {
            $mainContainer = $(".main-container");
            $mainContent = $(".main-content-responsive");
            $containerScrollable = $(".container-scrollable");

            resizeContainers();
            new ResizeSensor($mainContainer, function () {
                resizeContainers();
            });

            function resizeContainers() {
                $mainContent.innerHeight($mainContainer.innerHeight());
                $mainContent.innerWidth($mainContainer.innerWidth());
                $containerScrollable.innerHeight($mainContainer.innerHeight() * .80);
            }
        });

    </script>

    <% If Not clinicServicesScoresEnabled Then %>
    <style>
        .servicesWithScores {
            display: none;
        }
    </style>
    <% end if %>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <div class="container-scrollable" style="overflow: auto;">
        <table id="Table1" cellspacing="0" cellpadding="0" style="width: 100%;" border="0">
            <tr>
                <td class="detailsframetop">
                    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" />
                                <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" Enabled="False"
                                    CausesValidation="False" />
                                <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" Enabled="False"
                                    CausesValidation="False" /></td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->

                    <asp:Panel ID="pnlMain" runat="server">

                        <!-- content begin -->
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <table>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblEnrollment" runat="server" CssClass="label" Style="vertical-align: middle;">Enrollment</asp:Label>
                                        <telerik:RadComboBox ID="rcbEnrollment" runat="server" DataTextField="PrgVerDescrip"
                                            DataValueField="StuEnrollId" AutoPostBack="True" Filter="Contains" CausesValidation="False" AppendDataBoundItems="true"
                                            Width="250px" DropDownWidth="250px">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td align="right">
                                        <telerik:RadButton ID="btnExportToPdf" runat="server" Text="Export to PDF"></telerik:RadButton>
                                    </td>
                                </tr>
                            </table>

                        </div>
                        <div class="boxContainer noBorder">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <telerik:RadGrid ID="rgPosts" runat="server" Width="100%" ShowStatusBar="true"
                                            AutoGenerateColumns="False" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False"
                                            AllowPaging="True" CommandItemStyle-HorizontalAlign="Right">
                                            <PagerStyle Mode="NumericPages"></PagerStyle>

                                            <MasterTableView Width="100%" DataKeyNames="TermID" AllowMultiColumnSorting="True">
                                                <DetailTables>
                                                    <telerik:GridTableView DataKeyNames="ClsSectionID" Width="100%" Name="Class" HierarchyDefaultExpanded="true">

                                                        <DetailTables>
                                                            <telerik:GridTableView DataKeyNames="InstrGrdBkWgtDetailId,ClsSectionID,EnrollDate" Name="LabWork"
                                                                Width="100%" NoDetailRecordsText="No lab work is assigned to this course." AllowPaging="False">

                                                                <DetailTables>
                                                                    <telerik:GridTableView DataKeyNames="InstrGrdBkWgtDetailId,GrdBkResultId,EnrollDate" Name="LabWorkDetail" Width="100%" NoDetailRecordsText="No lab work has been posted.">
                                                                        <Columns>
                                                                            <telerik:GridBoundColumn HeaderText="Post Date" DataField="PostDate" DataType="System.DateTime" DataFormatString="{0:MM/dd/yy}">
                                                                                <HeaderStyle CssClass="servicesWithScores"></HeaderStyle>
                                                                                <ItemStyle CssClass="servicesWithScores"></ItemStyle>
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn HeaderText="Score" DataField="Score" DataFormatString="{0:0.##}">
                                                                                <HeaderStyle CssClass="servicesWithScores"></HeaderStyle>
                                                                                <ItemStyle CssClass="servicesWithScores"></ItemStyle>
                                                                            </telerik:GridBoundColumn>
                                                                            <%--<telerik:GridBoundColumn HeaderText="Adjustment" DataField="Adjustment" DataFormatString="{0:0.##}"></telerik:GridBoundColumn>--%>
                                                                            <telerik:GridBoundColumn HeaderText="Posted By" DataField="ModUser"></telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn HeaderText="Comments" DataField="Comments" ItemStyle-Width="50%">
                                                                                <HeaderStyle CssClass="servicesWithScores"></HeaderStyle>
                                                                                <ItemStyle CssClass="servicesWithScores"></ItemStyle>
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridButtonColumn ButtonType="PushButton" HeaderText="Delete" CommandName="removeService" ConfirmDialogType="Classic"
                                                                                ConfirmText="Delete this lab work?" ConfirmTitle="Delete" Text="Delete" UniqueName="DeleteColumn"
                                                                                HeaderStyle-Width="2%">
                                                                            </telerik:GridButtonColumn>
                                                                        </Columns>
                                                                    </telerik:GridTableView>
                                                                </DetailTables>

                                                                <Columns>
                                                                    <telerik:GridBoundColumn HeaderText="EnrollDate" DataField="EnrollDate" DataFormatString="{0:0.##}" Display="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Lab Work" DataField="Descrip">
                                                                        <ItemStyle Width="30%"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Required" DataField="Required" DataFormatString="{0:0.##}">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Completed" DataField="Completed" DataFormatString="{0:#.##}">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Remaining" DataField="Remaining" DataFormatString="{0:0.##}">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Average" DataField="Average" DataFormatString="{0:0.##}">
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="servicesWithScores"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" CssClass="servicesWithScores"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Score
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="servicesWithScores"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="servicesWithScores"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <telerik:RadNumericTextBox ID="txtScore" MinValue="-999" MaxValue="999" runat="server" Width="50" CssClass="txtScore servicesWithScores" ToolTip="Zeros will affect student average.">
                                                                                <NumberFormat DecimalDigits="2"></NumberFormat>

                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Adjustment
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <telerik:RadNumericTextBox ID="txtAdj" MinValue="-999" MaxValue="999" runat="server" Width="60">
                                                                                <NumberFormat DecimalDigits="0"></NumberFormat>
                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Date
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="servicesWithScores"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="servicesWithScores"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <telerik:RadDatePicker ID="txtDate" CssClass="servicesWithScores" MinDate="1/1/1945" runat="server" Width="130">
                                                                            </telerik:RadDatePicker>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Comments
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="servicesWithScores"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="servicesWithScores"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <telerik:RadTextBox ID="txtComment" runat="server" Width="150px" CssClass="TextBox servicesWithScores"></telerik:RadTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                            </telerik:GridTableView>
                                                        </DetailTables>
                                                        <DetailTables>
                                                            <telerik:GridTableView DataKeyNames="InstrGrdBkWgtDetailId,ClsSectionID,EnrollDate" Name="LabHours" Width="100%" NoDetailRecordsText="No lab hours are assigned to this course.">

                                                                <DetailTables>
                                                                    <telerik:GridTableView DataKeyNames="InstrGrdBkWgtDetailId,GrdBkResultId,EnrollDate" Name="LabHourDetail" Width="100%" NoDetailRecordsText="No lab hours have been posted.">
                                                                        <Columns>
                                                                            <telerik:GridBoundColumn HeaderText="Post Date" DataField="PostDate" DataType="System.DateTime" DataFormatString="{0:MM/dd/yy}">
                                                                                <HeaderStyle CssClass="servicesWithScores"></HeaderStyle>
                                                                                <ItemStyle CssClass="servicesWithScores"></ItemStyle>
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn HeaderText="Adjustment" DataField="Score" DataFormatString="{0:0.##}"></telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn HeaderText="Score" DataField="Score" DataFormatString="{0:0.##}" Display="false">
                                                                                <HeaderStyle CssClass="servicesWithScores"></HeaderStyle>
                                                                                <ItemStyle CssClass="servicesWithScores"></ItemStyle>
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn HeaderText="Posted By" DataField="ModUser"></telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn HeaderText="Comments" DataField="Comments" ItemStyle-Width="50%">
                                                                                <HeaderStyle CssClass="servicesWithScores"></HeaderStyle>
                                                                                <ItemStyle CssClass="servicesWithScores"></ItemStyle>
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridButtonColumn ButtonType="PushButton" HeaderText="Delete" CommandName="removeService" ConfirmDialogType="Classic"
                                                                                ConfirmText="Delete these lab hours?" ConfirmTitle="Delete" Text="Delete" UniqueName="DeleteColumn"
                                                                                HeaderStyle-Width="2%">
                                                                            </telerik:GridButtonColumn>
                                                                        </Columns>
                                                                    </telerik:GridTableView>
                                                                </DetailTables>

                                                                <Columns>
                                                                    <telerik:GridBoundColumn HeaderText="EnrollDate" DataField="EnrollDate" DataFormatString="{0:0.##}" Display="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Lab Hours" DataField="Descrip">
                                                                        <ItemStyle Width="30%"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Required" DataField="Required" DataFormatString="{0:0.##}">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Completed" DataField="Completed" DataFormatString="{0:#.##}">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Remaining" DataField="Remaining" DataFormatString="{0:0.##}">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn HeaderText="Average" DataField="Average" DataFormatString="{0:0.##}" Display="false">
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="servicesWithScores"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" CssClass="servicesWithScores"></ItemStyle>
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn Display="false">
                                                                        <HeaderTemplate>
                                                                            Score
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="servicesWithScores"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="servicesWithScores"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <telerik:RadNumericTextBox ID="txtScore" MinValue="-999" MaxValue="999" runat="server" Width="50" CssClass="txtScore servicesWithScores" ToolTip="Zeros will affect student average." Display="false">
                                                                                <NumberFormat DecimalDigits="2"></NumberFormat>
                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Adjustment
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <telerik:RadNumericTextBox ID="txtAdj" MinValue="-999" MaxValue="999" runat="server" Width="60">
                                                                                <NumberFormat DecimalDigits="2"></NumberFormat>
                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Date
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="servicesWithScores"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="servicesWithScores"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <telerik:RadDatePicker ID="txtDate" CssClass="servicesWithScores" MinDate="1/1/1945" runat="server" Width="130">
                                                                            </telerik:RadDatePicker>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn>
                                                                        <HeaderTemplate>
                                                                            Comments
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="servicesWithScores"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="servicesWithScores"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <telerik:RadTextBox ID="txtComment" runat="server" Width="150px" CssClass="TextBox servicesWithScores"></telerik:RadTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                            </telerik:GridTableView>
                                                        </DetailTables>

                                                        <Columns>
                                                            <telerik:GridBoundColumn SortExpression="Descrip" HeaderButtonType="TextButton" DataField="Descrip" UniqueName="Descrip" HeaderText="Course"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn SortExpression="ClsSection" HeaderButtonType="TextButton" DataField="ClsSection" UniqueName="ClsSection" HeaderText="Class Section"></telerik:GridBoundColumn>
                                                        </Columns>
                                                    </telerik:GridTableView>
                                                </DetailTables>
                                                <Columns>
                                                    <telerik:GridBoundColumn SortExpression="StartDate" HeaderButtonType="TextButton" DataField="Termdescrip" HeaderText="Term"></telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- content end -->

                    </asp:Panel>
                    <!--end table content-->
                </td>
            </tr>
        </table>
        <!-- end rightcolumn -->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

