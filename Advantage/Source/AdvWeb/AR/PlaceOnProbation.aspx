﻿<%@ Page Title="Place Student On Probation" Language="VB" MasterPageFile="~/NewSite.master"
    AutoEventWireup="false" CodeFile="PlaceOnProbation.aspx.vb" Inherits="PlaceOnProbation" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Place Student On Probation</title>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script language="javascript" src="../js/AuditHist.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- end top menu (save,new,reset,delete,history)-->
            <!--begin right column-->
            <table class="maincontenttable">
                <tr>
                    <td class="detailsframe">
                        <FAME:StudentSearch ID="StudSearch1" runat="server" ShowTerm="false" ShowAcaYr="false"
                            OnTransferToParent="TransferToParent" />
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin table content-->
                            <asp:Panel ID="pnlRHS" runat="server">
                                <table class="contenttable">
                                    <tr>
                                        <td>
                                            <table class="contenttable" width="100%">
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblStartDate" CssClass="label" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <telerik:RadDatePicker ID="txtStartDate" runat="server"
                                                            MinDate="1/1/1945" Width="325px">
                                                        </telerik:RadDatePicker>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblEndDate" CssClass="label" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <telerik:RadDatePicker ID="txtEndDate" runat="server"
                                                            MinDate="1/1/1945" Width="325px">
                                                        </telerik:RadDatePicker>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblProbWarningTypeId" CssClass="label" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlProbWarningTypeId" 
                                                            runat="server" CssClass="dropdownlist" Width="325px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblReason" runat="server" CssClass="label">Comments</asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:TextBox ID="txtReason" Width="325px" runat="server"
                                                            CssClass="tocommentsnowrap" MaxLength="300" TextMode="MultiLine" Rows="5" Columns="60"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell"></td>
                                                    <td class="contentcell4">
                                                        <asp:Button ID="btnProbation" runat="server" Text="Place on Probation" CausesValidation="True"></asp:Button>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>
                            <!--end table content-->
                        </div>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtStuEnrollment" TabIndex="2" runat="server" CssClass="textbox"
                Width="0px" Visible="false">StuEnrollment</asp:TextBox>
            <asp:TextBox ID="txtStuEnrollmentId" runat="server" CssClass="textbox" Width="0px"
                Visible="false">StuEnrollmentId</asp:TextBox>
            <asp:TextBox ID="txtAcademicYear" TabIndex="8" runat="server" CssClass="textbox"
                Width="0px" Visible="false">AcademicYear</asp:TextBox>
            <asp:TextBox ID="txtAcademicYearId" runat="server" CssClass="textbox" Width="0px"
                Visible="false">AcademicYearId</asp:TextBox>
            <asp:TextBox ID="txtTerm" TabIndex="9" runat="server" CssClass="textbox" Width="0px"
                Visible="false">Term</asp:TextBox>
            <asp:TextBox ID="txtTermId" runat="server" CssClass="textbox" Width="0px" Visible="false">TermId</asp:TextBox>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
