﻿<%@ Page Title="Manage Student Schedule" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentCourseSchedule.aspx.vb" Inherits="AdvWeb.AR.Studentcourseschedule" %>

<%@ Register Src="~/usercontrols/AR/StudentSchedule.ascx" TagPrefix="fame" TagName="StudentSchedule" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <style type="text/css">
        div.RadWindow_Web20 a.rwCloseButton {
            margin-right: 20px;
        }
        .rfdSelectBox {
            width: 100% !important;
        }
    </style>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ConfirmAttendance(sender, args) {
            args.set_cancel(!window.confirm("Removing classes will remove the attendance for this student. Do you want to continue?"));
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%">
            <asp:ScriptManagerProxy ID="ScriptManagerProxy2" runat="server">
            </asp:ScriptManagerProxy>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="hidden" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->

                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <div class="boxContainerNoHeight full">
                <h3><%=Header.Title  %></h3>
                <table class="contenttable" align="center" style="width: 35%;">
                    <tr>
                        <td class="contentcell" style="padding: 16px;">
                            <asp:Label ID="lblEnrollmentId" CssClass="label" runat="server">Enrollment</asp:Label>
                        </td>
                        <td class="contentcell4Date">
                            <asp:DropDownList ID="ddlEnrollmentId" runat="server" CssClass="dropdownlist" AutoPostBack="True" Width="200px"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="boxContainer full">
                <fame:StudentSchedule ID="studentSchedule" runat="server" />
            </div>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
