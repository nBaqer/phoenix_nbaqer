﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="RegisterStudents.aspx.vb" Inherits="AdvWeb.AR.RegisterStudents" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script type="text/javascript">

        function OldPageResized(sender, args)
        {
            $telerik.repaintChildren(sender);
        }

        function ConfirmAttendance(sender, args)
        {
            args.set_cancel(!window.confirm("Unregistering students will remove attendance for this class. Do you want to continue?"));
        }

    </script>
    <style type="text/css">
        div.RadWindow_Web20 a.rwCloseButton {
            margin-right: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    </telerik:RadAjaxManagerProxy>
    <telerik:RadWindowManager ID="ScheduleConflictWindow" runat="server" Behaviors="Default"
        InitialBehaviors="None" Visible="false">
        <Windows>
            <telerik:RadWindow ID="DialogWindow" Behaviors="Close" VisibleStatusbar="false" ReloadOnShow="true"
                Modal="true" runat="server" Height="400px" Width="700px" VisibleOnPageLoad="false"
                Top="20" Left="20">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="400"
            Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table class="maincontenttable" id="Table2">
                <tr>
                    <td class="listframetop2">
                        <asp:Panel ID="panFilter" runat="server">
                            <table class="maincontenttable">
                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblProgId" runat="server" CssClass="label">Program</asp:Label>
                                    </td>
                                    <td class="contentcell4blue" align="left">
                                        <asp:DropDownList ID="ddlProgId" Width="300px" runat="server" AutoPostBack="True"
                                            CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblTerm" runat="server" CssClass="label">Term</asp:Label>
                                    </td>
                                    <td class="contentcell4blue" align="left">
                                        <asp:DropDownList ID="ddlTerm" Width="300px" runat="server" AutoPostBack="True" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="label1" runat="server" CssClass="label">Course</asp:Label>
                                    </td>
                                    <td class="contentcell4blue" align="left">
                                        <asp:DropDownList ID="ddlCourse" Width="300px" runat="server" AutoPostBack="False"
                                            CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblStatus" runat="server" CssClass="label" Width="100%">Status</asp:Label>
                                    </td>
                                    <td class="contentcell4blue" align="left">
                                        <asp:DropDownList ID="ddlStatus" Width="300px" runat="server" AutoPostBack="false"
                                            CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellblue">
                                        <asp:Label ID="lblShift" runat="server" CssClass="label">Shift</asp:Label>
                                    </td>
                                    <td class="contentcell4blue" align="left">
                                        <asp:DropDownList ID="ddlShift" Width="300px" runat="server" AutoPostBack="False"
                                            CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellblue"></td>
                                    <td class="contentcell4blue" align="center">
                                        <asp:CheckBox ID="chkPreReqs" runat="server" CssClass="checkboxinternational"
                                            Text="Override Course Prerequisites" />
                                        <telerik:RadToolTip runat="server" ID="RadToolTip1" Width="350px" Height="70px" OffsetX="30"
                                            OffsetY="0" IsClientID="false" EnableViewState="true" ShowCallout="false" RenderInPageRoot="true"
                                            RelativeTo="Mouse" Position="BottomRight" Animation="None" ShowEvent="OnMouseOver"
                                            TargetControlID="chkPreReqs"
                                            ShowDelay="0"
                                            AutoCloseDelay="0"
                                            Text="Override any Course Prerequisite setup and allows for student registration into a class where the Course Prerequisite has not yet been satisfied." HideEvent="LeaveTargetAndToolTip">
                                        </telerik:RadToolTip>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellblue"></td>
                                    <td class="contentcell4blue" align="left">
                                        <asp:Button ID="btnBuildList" runat="server" Text="Build List"
                                            Width="130"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftFltrRegStds">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="registerstudentcontentcell" style="border-right: #e0e0e0 1px solid; border-top: #e0e0e0 1px solid; border-left: #e0e0e0 1px solid; width: 60%; border-bottom: #e0e0e0 1px solid">
                                        <asp:Label ID="lblClass" runat="server" CssClass="labelbold">Class</asp:Label>
                                    </td>
                                    <td class="registerstudentcontentcell" style="border-right: #e0e0e0 1px solid; border-top: #e0e0e0 1px solid; width: 15%; border-bottom: #e0e0e0 1px solid">
                                        <asp:Label ID="lblSection1" runat="server" CssClass="labelbold">Section</asp:Label>
                                    </td>
                                    <td class="registerstudentcontentcell" style="border-right: #e0e0e0 1px solid; border-top: #e0e0e0 1px solid; width: 5%; border-bottom: #e0e0e0 1px solid">
                                        <asp:Label ID="lblMax" runat="server" CssClass="labelbold">Max</asp:Label>
                                    </td>
                                    <td class="registerstudentcontentcell" style="border-right: #e0e0e0 1px solid; border-top: #e0e0e0 1px solid; width: 10%; border-bottom: #e0e0e0 1px solid">
                                        <asp:Label ID="lblAssigned" runat="server" CssClass="labelbold">Assigned</asp:Label>
                                    </td>
                                    <td class="registerstudentcontentcell" style="border-right: #e0e0e0 1px solid; border-top: #e0e0e0 1px solid; width: 10%; border-bottom: #e0e0e0 1px solid">
                                        <asp:Label ID="lblRemaining" runat="server" CssClass="labelbold">Remaining</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:DataList ID="dtlRegStud" runat="server" Width="100%" DataKeyField="ClsSectionId"
                                            OnItemCommand="dtlRegStud_ItemCommand" RepeatDirection="Vertical" RepeatLayout="Table"
                                            GridLines="horizontal">
                                            <ItemStyle CssClass="itemstyle"></ItemStyle>
                                            <SelectedItemStyle CssClass="SelectedItemStyle"></SelectedItemStyle>
                                            <ItemStyle CssClass="itemstyle"></ItemStyle>
                                            <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                            <ItemTemplate>
                                                <table class="maincontenttable">
                                                    <tr>
                                                        <td class="registerstudentcontentcell" style="width: 60%">
                                                            <asp:LinkButton ID="Linkbutton4" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                                CommandArgument='<%# Container.DataItem("ClsSectionId")%>' Text='<%# Container.DataItem("ChildDescripTyp")%>'>
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td class="registerstudentcontentcell" style="width: 15%">
                                                            <asp:LinkButton ID="Linkbutton1" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                                CommandArgument='<%# Container.DataItem("ClsSectionId")%>' Text='<%# Container.DataItem("ClsSection")%>'>
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td class="registerstudentcontentcell" style="width: 5%; padding-left: 5px">
                                                            <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                                CommandArgument='<%# Container.DataItem("ClsSectionId")%>' Text='<%# Container.DataItem("MaxStud")%>'>
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td class="registerstudentcontentcell" style="width: 10%; padding-left: 10px">
                                                            <asp:LinkButton ID="Linkbutton3" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                                CommandArgument='<%# Container.DataItem("ClsSectionId")%>' Text='<%# Container.DataItem("Assigned")%>'>
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td class="registerstudentcontentcell" style="width: 10%; padding-left: 15px">
                                                            <asp:LinkButton ID="Linkbutton5" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                                CommandArgument='<%# Container.DataItem("ClsSectionId")%>' Text='<%# Container.DataItem("Remaining")%>'>
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">
            <table class="maincontenttable">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                                CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                        <h3><%=Header.Title  %></h3>
                        <asp:Panel ID="pnlRHS" runat="server">
                            <%--        <div class="scrollrightregisterstudents">--%>
                            <div class="scrollright2">
                                <!-- begin content table-->
                                <asp:Panel ID="pnlHeader" runat="server" Visible="False" HorizontalAlign="Center">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCourse" runat="server" CssClass="label">Course</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:Label ID="lblCourseName" runat="server" Width="350px" CssClass="textbox"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblsection" runat="server" CssClass="label">Class Section</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:Label ID="lblSecName" runat="server" Width="350px" CssClass="textbox"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblMaxStud1" runat="server" CssClass="label">Max Students</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:Label ID="lblMaxStud" runat="server" Width="50px" CssClass="textbox"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblRemStds1" runat="server" CssClass="label">Remaining Students</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:Label ID="lblRemStds" runat="server" Width="50px" CssClass="textbox"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <table class="contenttable" width="100%" align="center">
                                    <tr>
                                        <td class="threecolumnheaderreg" nowrap="nowrap">
                                            <asp:Label ID="lblAvailStuds" runat="server" CssClass="label" Font-Bold="True">Available Students</asp:Label>
                                        </td>
                                        <td class="threecolumnspacerreg" nowrap="nowrap"></td>
                                        <td class="threecolumnheaderreg" nowrap="nowrap">
                                            <asp:Label ID="lblSelStuds" runat="server" CssClass="label" Font-Bold="True">Selected Students</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="threecolumncontentreg" style="text-align: left" nowrap="nowrap">
                                            <asp:ListBox ID="lbxAvailStuds" runat="server" Height="400px" SelectionMode="Multiple"
                                                CssClass="listboxes"></asp:ListBox>
                                            <br />
                                            <asp:CheckBox ID="chkSort" runat="server" AutoPostBack="True" CssClass="checkbox"
                                                Text="Sort by Name"></asp:CheckBox>
                                        </td>
                                        <td class="threecolumnbuttonsreg" nowrap="nowrap">
                                            <telerik:RadButton ID="btnAdd" runat="server" CssClass="buttons1" Text="Add >" Enabled="False" >
                                            </telerik:RadButton>
                                            <br />
                                            <telerik:RadButton ID="btnAddAll" runat="server" CssClass="buttons1" Text="Add All >" Enabled="False" >
                                            </telerik:RadButton>
                                            <br />
                                            <br />
                                            <telerik:RadButton ID="btnRemove" runat="server" CssClass="buttons1" Text="< Remove" Enabled="False">
                                            </telerik:RadButton>
                                            <br />
                                            <telerik:RadButton ID="btnRemoveAll" runat="server" CssClass="buttons1" Text="< Remove All"
                                                Enabled="False">
                                            </telerik:RadButton>
                                        </td>
                                        <td class="threecolumncontentreg" style="text-align: left" nowrap="nowrap">
                                            <asp:ListBox ID="lbxSelectStuds" runat="server" Height="400px" SelectionMode="Multiple"
                                                CssClass="listboxes"></asp:ListBox>
                                            <br />
                                            <asp:CheckBox ID="ChkSort1" runat="server" AutoPostBack="True" CssClass="checkbox"
                                                Text="Sort by Name"></asp:CheckBox>
                                        </td>
                                    </tr>
                                    <asp:Label ID="lblClsSectId" runat="server" CssClass="label" Visible="False"></asp:Label>
                                    <asp:TextBox ID="txtClsSectId" runat="server" CssClass="label" Visible="False"></asp:TextBox>
                                </table>
                                <!-- end content table-->
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>
