<%@ Page Language="vb" AutoEventWireup="false" Inherits="SetUpCourses" CodeFile="SetUpCourses.aspx.vb" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Set Up Course Sequence</title>
    <link href="../css/systememail.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/popup.css" />
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<body runat="server">
    <form id="Form1" method="post" runat="server">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="../images/advantage_course_sequence.jpg"></td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href="#">X Close</a></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
            <tr>
                <td class="DetailsFrameTopemail" style="width: 100%">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="MenuFrame" align="right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                    Enabled="false"></asp:Button></td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                </td>
            </tr>
        </table>
        <div style="width:90%;padding:25px;">
            <table cellspacing="0" cellpadding="0" width="100%" align="center">
                <tr>
                    <td align="center">
                    <asp:Label ID="lblPrgVer" runat="server" CssClass="Label" Text="Program Version"></asp:Label> </td><td>
                        <asp:TextBox ID="txtProgDescrip" runat="server" ReadOnly="True" Width="40%" CssClass="TextBox"></asp:TextBox></td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td colspan="2">
                        <div>
                            <asp:DataGrid ID="dgdCourses" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                <EditItemStyle CssClass="Label"></EditItemStyle>
                                <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                <FooterStyle CssClass="Label"></FooterStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Row">
                                        <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                        <ItemStyle Width="10%" CssClass="DataGridItemStyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRowId" runat="server" CssClass="Label" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Course">
                                        <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                        <ItemStyle Width="50%" CssClass="DataGridItemStyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtReqId" runat="server" CssClass="Textbox" Visible="False" Text='<%# container.DataItem("ReqId") %>' />
                                            <asp:Label ID="Course" runat="server" CssClass="Label" Text='<%# Container.DataItem("Descrip") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Code">
                                        <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                        <ItemStyle Width="20%" CssClass="DataGridItemStyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Code" runat="server" CssClass="Label" Text='<%# Container.DataItem("Code") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Term">
                                        <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                        <ItemStyle Width="20%" CssClass="DataGridItemStyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtTermNo" runat="server" CssClass="Textbox" Visible="False" Text='<%# container.DataItem("TermNo") %>' />
                                            <asp:DropDownList ID="ddlTermNo" runat="server" CssClass="DropDownList">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        <asp:TextBox ID="txtClsSectionId1" runat="server" CssClass="Label" Visible="False"></asp:TextBox><asp:TextBox
                            ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox
                                ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox></div>
                                </td> </tr> </table>
        </div>
        
        <!--end table content-->
      <%-- <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
    </form>
</body>
</html>
