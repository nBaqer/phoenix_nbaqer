﻿<%@ Page Title="Record Transfer Grades for Student" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="TransferGrades.aspx.vb" Inherits="AdvWeb.AR.TransferGrades" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <FAME:StudentSearch ID="StudSearch1" runat="server" ShowTerm="false" ShowAcaYr="false" OnTransferToParent="TransferToParent" />
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->


                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <asp:Panel ID="pnlDate" runat="server" Visible="True"  >
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="40%" align="center">

                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Button ID="btnGetStdCourses" runat="server" Text="Get Student Courses"
                                                                CausesValidation="False"></asp:Button>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <asp:Button ID="btnCheckPreviousGrade" runat="server" Text="Check for Previous Grades"
                                                                CausesValidation="False"></asp:Button>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </asp:Panel>
                                            <table class="maincontenttable" align="center">
                                                <tr>
                                                    <td style="padding-top: 10px">
                                                        <asp:DataGrid ID="dgdTransferGrade" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                                            <EditItemStyle Wrap="False"></EditItemStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                            <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="Code">
                                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Code" runat="server" Text='<%# Container.DataItem("Code")  %>' />
                                                                        <asp:TextBox ID="txtReqId" runat="server" CssClass="ardatalistcontent" Visible="False"
                                                                            Text='<%# Container.DataItem("ReqId") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Course Description">
                                                                    <HeaderStyle CssClass="datagridheader" Width="30%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Descrip" runat="server" Text='<%# Container.DataItem("Descrip")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Credits">
                                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCredits" CssClass="label" Text='<%# Container.DataItem("Credits") %>'
                                                                            runat="server"> </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Hours">
                                                                    <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblHours" CssClass="label" Text='<%# Container.DataItem("Hours") %>'
                                                                            runat="server"> </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Score">
                                                                    <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                                    <ItemStyle CssClass="datalistar" Width="5%"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtScore" runat="server" CssClass="textbox" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Grade">
                                                                    <HeaderStyle CssClass="datalistheaderar"></HeaderStyle>
                                                                    <ItemStyle CssClass="datalistar" Width="10%"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlGrade" runat="server" CssClass="dropdownlist" Width="80px">
                                                                        </asp:DropDownList>
                                                                        <asp:CheckBox ID="chkGrade" runat="server" CssClass="checkbox" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Transfer Term">
                                                                    <HeaderStyle CssClass="datagridheader" Width="30%"></HeaderStyle>
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlTerm" runat="server" CssClass="dropdownlist">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtStuEnrollment" runat="server" Width="0px" TabIndex="2" Visible="false">StuEnrollment</asp:TextBox>
                                                        <asp:TextBox ID="txtStuEnrollmentId" runat="server" Width="0px" Visible="false">StuEnrollmentId</asp:TextBox>
                                                        <asp:TextBox ID="txtAcademicYear" runat="server" Width="0px" TabIndex="8" Visible="false">AcademicYear</asp:TextBox>
                                                        <asp:TextBox ID="txtAcademicYearId" runat="server" Width="0px" Visible="false">AcademicYearId</asp:TextBox>
                                                        <asp:TextBox ID="txtTerm" runat="server" Width="0px" TabIndex="9" Visible="false">Term</asp:TextBox>
                                                        <asp:TextBox ID="txtTermId" runat="server" Width="0px" Visible="false">TermId</asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>


                                        <!--end table content-->
                                    </div>
                                </td>
                            </tr>
                        </table>
            </table>


            <!--end table content-->


            <asp:Panel ID="Panel1" runat="server">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        </telerik:RadPane>
    </telerik:RadSplitter>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true" Behaviors="Close, Resize, Move">

        <Windows>
            <telerik:RadWindow ID="RadTransferPending" runat="server" Modal="True" MaxWidth="1100px" MinWidth="800px" MinHeight="600px"
                OnClientClose="OnClientClose">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>


    <script type="text/javascript">
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                var result = arg.result + ',' + arg.checkTransfer.toString() + ',' + arg.checkGrade.toString();
                __doPostBack('RadTransferPending', result);

            } else {
                //Manage close by X
                __doPostBack('RadTransferPending', "cancel,false,false");

            }
        }


    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

