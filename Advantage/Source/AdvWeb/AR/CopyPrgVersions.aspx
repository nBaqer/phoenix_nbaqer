﻿<%@ Page Title="Record Transfer Grades for Student" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="CopyPrgVersions.aspx.vb" Inherits="CopyPrgVersions" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3>
                                            <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                        </h3>
                                        <!-- begin table content-->


                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <div style="padding: 0 0 20px 0">
                                                <table cellspacing="0" cellpadding="4" width="80%" align="center">
                                                    <tr>
                                                        <td class="contentcellheader" style="text-align: center">
                                                            <asp:Label ID="lblPrgVerIdTitle" CssClass="labelbold" runat="server">Copy From Program Version</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-right: #ebebeb 1px solid; padding: 16px; vertical-align: top; border-left: #ebebeb 1px solid; width: 100%; border-bottom: #ebebeb 1px solid; white-space: nowrap; text-align: center">
                                                            <asp:DropDownList ID="ddlPrgVerId" runat="server" CssClass="dropdownlist" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblPrgVerDescrip" runat="server" CssClass="labelbold" Visible="False">PrgVerDescrip</asp:Label>
                                                            <asp:Label ID="lblPrgVerId1" runat="server" Visible="False">PrgVerId</asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="padding: 0 0 20px 0">
                                                <table class="tablecopyprog" cellspacing="0" cellpadding="4" width="80%" align="center">
                                                    <tr>
                                                        <td class="contentcellheader" style="border-right: 0px; border-top: 0px; border-left: 0px; text-align: center"
                                                            colspan="2">
                                                            <asp:Label ID="lblHeading" CssClass="labelbold" runat="server">Copy to New Program Version</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblCode" CssClass="label" runat="server">Code<font color="red">*</font></asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <asp:TextBox ID="txtCode" CssClass="textbox" runat="server" MaxLength="128"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblDescrip" CssClass="label" runat="server">Description<font color="red">*</font></asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <asp:TextBox ID="txtDescrip" CssClass="textbox" runat="server" MaxLength="128"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblCampGrId" CssClass="label" runat="server">Campus Group<font color="red">*</font></asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <table cellspacing="0" cellpadding="0" width="80%" align="center">
                                                <tr>
                                                    <td style="text-align: center">
                                                        <asp:Button ID="btnCopyPrgVer" runat="server" Text="Create New Program Version"
                                                            CausesValidation="False"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>


                                        <!--end table content-->
                                    </div>
                                </td>
                            </tr>
                        </table>
            </table>
            <!--end table content-->

            <asp:TextBox ID="txtStuEnrollment" TabIndex="2" runat="server" CssClass="textbox" Width="0px" Visible="false">StuEnrollment</asp:TextBox>
            <asp:TextBox ID="txtStuEnrollmentId" runat="server" CssClass="textbox" Width="0px" Visible="false">StuEnrollmentId</asp:TextBox>
            <asp:TextBox ID="txtAcademicYear" TabIndex="8" runat="server" CssClass="textbox" Width="0px" Visible="false">AcademicYear</asp:TextBox>
            <asp:TextBox ID="txtAcademicYearId" runat="server" CssClass="textbox" Width="0px" Visible="false">AcademicYearId</asp:TextBox>
            <asp:TextBox ID="txtTerm" TabIndex="9" runat="server" CssClass="textbox" Width="0px" Visible="false">Term</asp:TextBox>
            <asp:TextBox ID="txtTermId" runat="server" CssClass="textbox" Width="0px" Visible="false">TermId</asp:TextBox>

            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

