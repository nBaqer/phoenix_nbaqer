﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="AttachGBWToClsSects.aspx.vb" Inherits="AttachGBWToClsSects" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table class="Table100" id="Table1" >
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table class="Table100" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                                
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable, Table100" align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="40%" align="center">
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblTermId" Text="Term" CssClass="label" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlTerm" runat="server" CssClass="dropdownlist">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblStatusId" Text="Status" CssClass="label" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="dropdownlist" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblinstrgrdbkwgtid" Text="Grade Book" CssClass="label" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlDescrip" runat="server" CssClass="dropdownlist">
                                                        </asp:DropDownList>
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td class="contentcell"></td>
                                                    <td class="contentcell4" style="padding-bottom: 10px">
                                                        <%--<asp:Button ID="btnGetClsSects" runat="server"  Text="Get All ClassSections"
                                                                CausesValidation="true"></asp:Button>--%>
                                                        <telerik:RadButton ID="btnGetClsSects" runat="server" Text="Get All ClassSections"
                                                            CausesValidation="true">
                                                        </telerik:RadButton>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellspacing="0" cellpadding="0" width="900px" align="center">
                                                <tr>
                                                    <td class="threecolumnheader1">
                                                        <asp:Label ID="lblAvailClsSects" runat="server" CssClass="labelbold">Available Class Sections</asp:Label>
                                                    </td>
                                                    <td class="threecolumnspacer1"></td>
                                                    <td class="threecolumnheader1">
                                                        <asp:Label ID="lblSelCamps" runat="server" CssClass="labelbold">Selected Class Sections</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="threecolumncontent">
                                                        <asp:ListBox ID="lbxAvailClsSects" runat="server" Rows="10" CssClass="listboxes"></asp:ListBox>
                                                    </td>
                                                    <td class="threecolumnbuttons" align="center">
                                                        <telerik:RadButton ID="btnAdd" runat="server" CssClass="buttons1" Text="Add >" CausesValidation="false" Width="100px"></telerik:RadButton>
                                                        <br />
                                                        <telerik:RadButton ID="btnAddAll" runat="server" CssClass="buttons1" Text="Add All >>" CausesValidation="false" Width="100px"></telerik:RadButton>
                                                        <br />
                                                        <telerik:RadButton ID="btnRemove" runat="server" CssClass="buttons1" Text="Remove <" CausesValidation="false" Width="100px"></telerik:RadButton>
                                                        <br />
                                                        <telerik:RadButton ID="btnRemoveAll" runat="server" CssClass="buttons1" Text="Remove all <<" CausesValidation="false" Width="100px"></telerik:RadButton>
                                                    </td>
                                                    <td class="threecolumncontent">
                                                        <asp:ListBox ID="lbxSelectedClsSects" runat="server" Rows="10" CssClass="listboxes"></asp:ListBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

