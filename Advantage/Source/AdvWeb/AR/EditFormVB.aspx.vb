﻿Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade

Partial Class AR_EditFormVB
    Inherits System.Web.UI.Page
    Public dtPeriods As DataTable
    Public dtAltPeriods As DataTable
    Public dtRooms As DataTable
    Public dtInstructionTypes As DataTable
    Dim TermDescrip As String
    Dim CourseDescrip As String
    Dim InstructorDescrip As String
    Public ClsSectionId As String
    Public clsMeetingId As String
    Public instructiontypeid1 As String
    Public campusId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Write(Request.QueryString("id"))
        If Not Page.IsPostBack Then
            campusId = Request.QueryString("campusId")
            ClsSectionId = Request.QueryString("ClsSectionId")
            BuildDropDownLists(campusId)
            lblTermDescrip.Text = Request.QueryString("term")
            lblCourseDescrip.Text = "(" + Request.QueryString("coursecode") + ") " + Request.QueryString("coursedescrip")
            lblInstructorDescrip.Text = Request.QueryString("instructor")
            lblCancelFromDescrip.Text = Request.QueryString("cancelfrom") & " to " + Request.QueryString("cancelto")
            lblinstructiontype.Text = Request.QueryString("instructiontype")
            lblMessage.Value = Request.QueryString("rowindex")
            'ddlInstructionType.SelectedItem.Text = lblinstructiontype.Text
            clsMeetingId = Guid.NewGuid.ToString  'The ClsSectMeeting Id needs to be set here for radWindow to pass it back to parent window
            txtrescheduledmeetingid.Text = clsMeetingId
            txtTermId.Text = Request.QueryString("termid")
            txtreqid.Text = Request.QueryString("reqid")
            instructiontypeid1 = Request.QueryString("instructiontypeid").ToString
            RadGrid2.Visible = False
        End If
    End Sub
    Private Sub BuildDropDownLists(ByVal campusId As String)
        With ddlContainerPeriod
            .DataSource = (New ClassSectionPeriodsFacade).GetAllPeriods_SP(True, campusId)
            .DataTextField = "PeriodDescrip"
            .DataValueField = "PeriodId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlContainerAltPeriod
            .DataSource = (New ClassSectionPeriodsFacade).GetAllPeriods_SP(True, campusId)
            .DataTextField = "PeriodDescrip"
            .DataValueField = "PeriodId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
            .SelectedIndex = 0
        End With

        With ddlInstructionType
            .DataSource = (New ClassSectionPeriodsFacade).GetInstructionTypes_SP(True, campusId)
            .DataTextField = "InstructionTypeDescrip"
            .DataValueField = "InstructionTypeId"
            .DataBind()
            .SelectedValue = Request.QueryString("instructiontypeid")
        End With

        With ddlContainerRooms
            .DataSource = (New ClassSectionPeriodsFacade).GetAllRooms_SP(True, campusId)
            .DataTextField = "Descrip"
            .DataValueField = "RoomId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Protected Sub ShowScheduleConflict(ByVal ClsSectId As String, ByVal StuEnrollId As String, ByVal Classes As String, _
                                      ByVal MeetingStartDate As String, _
                                      ByVal MeetingEndDate As String, _
                                      ByVal RoomId As String, _
                                      ByVal PeriodId As String, _
                                      Optional ByVal buttonState As String = "", _
                                      Optional ByVal conflictinsidesameclass As Boolean = False)
        Dim studentId As String = ""
        Dim strURL As String = "ViewScheduleConflicts.aspx?resid=909&mod=AR" + "&ClsSectionId=" + ClsSectId.ToString + "&StuEnrollId=" + StuEnrollId + "&Student=" + studentId + "&buttonState=" + buttonState + "&Classes=" + Classes _
                               + "&MeetingStartDate=" + MeetingStartDate + "&MeetingEndDate=" + MeetingEndDate + "&RoomId=" + ddlContainerRooms.SelectedValue.ToString + "&PeriodId=" + ddlContainerPeriod.SelectedValue.ToString _
                               + "&TermId=" + txtTermId.Text.ToString + "&CourseId=" + txtreqid.Text.ToString + "&InstructorId=" + Request.QueryString("instructiontypeid") + "&CampusId=" + Request.QueryString("CampusId").ToString _
                               + "&boolconflictinsidesameclass=" + conflictinsidesameclass.ToString
        ScheduleConflictWindow.Visible = True
        ScheduleConflictWindow.Windows(0).NavigateUrl = strURL
        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = True
    End Sub
    'Protected Sub ShowScheduleConflict(ByVal ClsSectId As String, ByVal StuEnrollId As String, ByVal StudentId As String, _
    '                                   ByVal MeetingStartDate As String, ByVal MeetingEndDate As String, _
    '                                   ByVal RoomId As String, ByVal InstructorId As String,

    '                                   Optional ByVal buttonState As String = "Add", Optional ByVal Classes As String = "", _
    '                                   Optional ByVal TermId As String = "", Optional ByVal flag As Boolean = False, Optional ByVal conflictinsidesameclass As Boolean = False)
    '    Dim strURL As String = "ViewScheduleConflicts.aspx?resid=909&mod=AR" + "&ClsSectionId=" + ClsSectId.ToString + "&StuEnrollId=" + StuEnrollId.ToString + "&Student=" + StudentId.ToString + "&buttonState=" + buttonState + "&Classes=" + Classes.ToString + "&TermId=" + TermId.ToString + "&boolconflictinsideterm=" + flag.ToString + "&CampusId=" + Request.QueryString("CampusId").tostring + "&boolconflictinsidesameclass=" + conflictinsidesameclass.ToString

    '    (ClsSectionId, MeetingStartDate, MeetingEndDate, _
    '                                                                                                  strRoomId, InstructorId, TermId, CourseId, DayId, StartTimeId, EndTimeId, campusId)
    '    ScheduleConflictWindow.Visible = True
    '    ScheduleConflictWindow.Windows(0).NavigateUrl = strURL
    '    ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = True
    'End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Dim tbl As New DataTable("SetupMeetings")
        'With tbl
        '    'Define table schema
        '    .Columns.Add("ClsSectMeetingId", GetType(String))
        '    .Columns.Add("WorkDaysId", GetType(String))
        '    .Columns.Add("RoomId", GetType(String))
        '    .Columns.Add("TimeIntervalId", GetType(String))
        '    .Columns.Add("ClsSectionId", GetType(String))
        '    .Columns.Add("EndIntervalId", GetType(String))
        '    .Columns.Add("ModUser", GetType(String))
        '    .Columns.Add("ModDate", GetType(Date))
        '    .Columns.Add("PeriodId", GetType(String))
        '    .Columns.Add("AltPeriodId", GetType(String))
        '    .Columns.Add("StartDate", GetType(Date))
        '    .Columns.Add("EndDate", GetType(Date))
        '    .Columns.Add("InstructionTypeId", GetType(String))
        '    .Columns.Add("IsMeetingRescheduled", GetType(Boolean))
        '    'set field properties
        '    .Columns("StartDate").AllowDBNull = False
        '    .Columns("EndDate").AllowDBNull = False
        'End With


        'Dim strDayId As String = Guid.Empty.ToString
        'Dim strRoomId As String = ddlContainerRooms.SelectedValue
        'Dim STimeDescrip As String = Guid.Empty.ToString
        'Dim ETimeDescrip As String = Guid.Empty.ToString
        'Dim sDate As Date = txtSDate.DbSelectedDate
        'Dim EDate As Date = txtEDate.DbSelectedDate

        'ClsSectionId = Request.QueryString("ClsSectionId")

        'clsMeetingId = txtrescheduledmeetingid.Text

        ''Try
        ''    ddlInstructionType.SelectedItem.Text = ddlInstructionType.Items.FindByText(lblinstructiontype.Text).ToString
        ''    'ddlInstructionType.SelectedIndex = ddlInstructionType.Items.IndexOf(ddlInstructionType.Items.FindByText(lblinstructiontype.Text))
        ''Catch ex As System.Exception
         ''	Dim exTracker = new AdvApplicationInsightsInitializer()
        ''	exTracker.TrackExceptionWrapper(ex)

        ''    ddlInstructionType.SelectedIndex = 0
        ''End Try

        'Dim dsScheduleConflicts As New DataSet
        'dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArray(ClsSectionId.ToString, _
        '                                                                                sDate, _
        '                                                                                EDate, _
        '                                                                                strRoomId, _
        '                                                                                ddlInstructionType.SelectedValue.ToString, _
        '                                                                                txtTermId.Text, _
        '                                                                                txtreqid.Text, _
        '                                                                                ddlContainerPeriod.SelectedValue.ToString, _
        '                                                                                Request.QueryString("campusId"))


        'If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
        '    ShowScheduleConflict(ClsSectionId.ToString, "", lblCourseDescrip.Text, sDate, EDate, strRoomId, ddlContainerPeriod.SelectedValue.ToString, "All", False)
        '    'DisplayErrorMessage("Class cannot be rescheduled due to a schedule conflict with another class")
        '    'RadAjaxManager1.ResponseScripts.Add("radalert('Class cannot be rescheduled as it conflicts with a meeting schedule added for another class', 600, 160);")
        '    Exit Sub
        'Else 'No Conflicts
        '    'Check for conflicts in same class
        '    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArrayWithInSameClass(ClsSectionId.ToString, _
        '                                                                              sDate, _
        '                                                                              EDate, _
        '                                                                              strRoomId, _
        '                                                                              ddlInstructionType.SelectedValue.ToString, _
        '                                                                              txtTermId.Text, _
        '                                                                              txtreqid.Text, _
        '                                                                              ddlContainerPeriod.SelectedValue.ToString, _
        '                                                                              Request.QueryString("campusId"))
        '    If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
        '        ShowScheduleConflict(ClsSectionId.ToString, "", lblCourseDescrip.Text, sDate, EDate, strRoomId, ddlContainerPeriod.SelectedValue.ToString, "All", True)
        '        'RadAjaxManager1.ResponseScripts.Add("radalert('Class cannot be rescheduled as it conflicts with a meeting schedule added for this class', 600, 160);")
        '        Exit Sub
        '    End If
        '    tbl.LoadDataRow(New Object() {clsMeetingId, strDayId, strRoomId, STimeDescrip, _
        '                                                                     ClsSectionId.ToString, ETimeDescrip, "sa", _
        '                                                                     Date.Now, ddlContainerPeriod.SelectedValue.ToString, ddlContainerAltPeriod.SelectedValue.ToString, _
        '                                                                    sDate, EDate, ddlInstructionType.SelectedValue.ToString, True}, False)
        'End If

        'If tbl.Rows.Count >= 1 Then
        '    If Not tbl Is Nothing Then
        '        Dim ds1 As New DataSet
        '        For Each lcol As DataColumn In tbl.Columns
        '            lcol.ColumnMapping = System.Data.MappingType.Attribute
        '        Next
        '        ds1.Tables.Add(tbl)
        '        Dim strXML As String = ds1.GetXml
        '        Dim clsFacade As New UnscheduleClsFacade
        '        Dim intReturn As Integer = 0
        '        intReturn = clsFacade.RescheduleClassMeetings(strXML)
        '        If intReturn = 1 Then

        '            'lblMessage.visible = True
        '            'lblMessage.text = "Class was rescheduled successfully"
        '        End If
        '    End If
        'End If
        CheckForScheduleConflicts()
    End Sub
    Private Sub CheckForScheduleConflicts()
        Dim tbl As New DataTable("SetupMeetings")
        With tbl
            'Define table schema
            .Columns.Add("ClsSectMeetingId", GetType(String))
            .Columns.Add("WorkDaysId", GetType(String))
            .Columns.Add("RoomId", GetType(String))
            .Columns.Add("TimeIntervalId", GetType(String))
            .Columns.Add("ClsSectionId", GetType(String))
            .Columns.Add("EndIntervalId", GetType(String))
            .Columns.Add("ModUser", GetType(String))
            .Columns.Add("ModDate", GetType(Date))
            .Columns.Add("PeriodId", GetType(String))
            .Columns.Add("AltPeriodId", GetType(String))
            .Columns.Add("StartDate", GetType(Date))
            .Columns.Add("EndDate", GetType(Date))
            .Columns.Add("InstructionTypeId", GetType(String))
            .Columns.Add("IsMeetingRescheduled", GetType(Boolean))
            'set field properties
            .Columns("StartDate").AllowDBNull = False
            .Columns("EndDate").AllowDBNull = False
        End With

        Dim strDayId As String = Guid.Empty.ToString
        Dim strRoomId As String = ddlContainerRooms.SelectedValue
        Dim STimeDescrip As String = Guid.Empty.ToString
        Dim ETimeDescrip As String = Guid.Empty.ToString
        Dim sDate As Date = txtSDate.DbSelectedDate
        Dim EDate As Date = txtEDate.DbSelectedDate

        ClsSectionId = Request.QueryString("ClsSectionId")
        campusId = Request.QueryString("campusId")
        clsMeetingId = txtrescheduledmeetingid.Text


        Dim dsScheduleConflicts As New DataSet
        dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArray(ClsSectionId.ToString, _
                                                                                        sDate, _
                                                                                        EDate, _
                                                                                        strRoomId, _
                                                                                        ddlInstructionType.SelectedValue.ToString, _
                                                                                        txtTermId.Text, _
                                                                                        txtreqid.Text, _
                                                                                        ddlContainerPeriod.SelectedValue.ToString, _
                                                                                        campusId)

        Dim table As DataTable
        Dim strPreviousCourse As String = ""
        Dim strPreviousClass As String = ""
        Dim strPreviousPeriod As String = ""
        Dim classSchedules As New List(Of String)
        table = CreateNewTable()
        If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
            lblScheduleConflicts.Text = "Class cannot be rescheduled as there is a schedule conflict"
            hiddenflag.Value = 1
            For Each dr As DataRow In dsScheduleConflicts.Tables(0).Rows
                Dim row As DataRow
                Dim expression As String = "CourseDescrip='" + dr("CourseDescrip").ToString + "'" + " and ClsSection='" + dr("ClsSection").ToString + "'" + " and ClassPeriod='" + dr("ClassPeriod").ToString + "'"

                row = table.NewRow()
                ' Then add the new row to the collection.
                row("CourseDescrip") = dr("CourseDescrip").ToString
                row("ClsSection") = dr("ClsSection").ToString
                row("ClassPeriod") = dr("ClassPeriod").ToString

                Dim matchRows() As DataRow
                matchRows = dsScheduleConflicts.Tables(0).Select(expression)

                Dim i As Integer
                Dim strMeetingSchedule As String = ""
                For i = 0 To matchRows.GetUpperBound(0)
                    strMeetingSchedule &= matchRows(i)("MeetingSchedule").ToString
                    strMeetingSchedule &= vbCrLf
                    'classSchedules.Add(matchRows(i)("MeetingSchedule").ToString)
                Next
                'row("MeetingSchedule") = classSchedules.ToList().ToString
                row("MeetingSchedule") = strMeetingSchedule

                'If row was already added to datatable ignore duplicates
                If (Not dr("CourseId").Trim = strPreviousCourse And _
                     Not dr("ClsSectionId").ToString.Trim = strPreviousClass) Then
                    'If (Not dr("CourseId").Trim = strPreviousCourse And _
                    '       Not dr("ClsSectionId").ToString.Trim = strPreviousClass And _
                    '        Not row("ClassPeriod").Trim = strPreviousPeriod Then
                    'Dim MeetingInfo As New ClsSectMeetingInfo
                    'With MeetingInfo
                    '    .CourseDescrip = row("CourseDescrip").ToString
                    '    .ClsSection = row("ClsSection").ToString.Trim
                    '    .MeetingSchedule = classSchedules
                    'End With
                    'table.Rows.Add(row("CourseDescrip"), row("ClsSection"), row("ClassPeriod"), row("MeetingSchedule"))
                    table.Rows.Add(row)
                    'table.Rows.Add(MeetingInfo.CourseDescrip, MeetingInfo.ClsSection, "Period", MeetingInfo.MeetingSchedule.ToList.ToString)
                End If

                'For elimiating duplicate rows
                strPreviousCourse = dr("CourseId").ToString
                strPreviousClass = dr("ClsSectionId").ToString
                strPreviousPeriod = dr("ClassPeriod").ToString

                'Clear list collection
                classSchedules.Clear()
            Next
            RadGrid2.Visible = True
            With RadGrid2
                .DataSource = table
                .DataBind()
            End With
            Exit Sub
        Else

            'Check for conflicts in same class
            dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArrayWithInSameClass(ClsSectionId.ToString, _
                                                                                      sDate, _
                                                                                      EDate, _
                                                                                      strRoomId, _
                                                                                      ddlInstructionType.SelectedValue.ToString, _
                                                                                      txtTermId.Text, _
                                                                                      txtreqid.Text, _
                                                                                      ddlContainerPeriod.SelectedValue.ToString, _
                                                                                      Request.QueryString("campusId"))
            If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                lblScheduleConflicts.Text = "Class cannot be rescheduled as there is a schedule conflict"
                hiddenflag.Value = 1
                For Each dr As DataRow In dsScheduleConflicts.Tables(0).Rows
                    Dim row As DataRow
                    Dim expression As String = "CourseDescrip='" + dr("CourseDescrip").ToString + "'" + " and ClsSection='" + dr("ClsSection").ToString + "'" + " and ClassPeriod='" + dr("ClassPeriod").ToString + "'"

                    row = table.NewRow()
                    ' Then add the new row to the collection.
                    row("CourseDescrip") = dr("CourseDescrip").ToString
                    row("ClsSection") = dr("ClsSection").ToString
                    row("ClassPeriod") = dr("ClassPeriod").ToString

                    Dim matchRows() As DataRow
                    matchRows = dsScheduleConflicts.Tables(0).Select(expression)

                    Dim i As Integer
                    Dim strMeetingSchedule As String = ""
                    For i = 0 To matchRows.GetUpperBound(0)
                        strMeetingSchedule &= matchRows(i)("MeetingSchedule").ToString
                        strMeetingSchedule &= vbCrLf
                        'classSchedules.Add(matchRows(i)("MeetingSchedule").ToString)
                    Next
                    'row("MeetingSchedule") = classSchedules.ToList().ToString
                    row("MeetingSchedule") = strMeetingSchedule

                    'If row was already added to datatable ignore duplicates
                    If (Not dr("CourseId").Trim = strPreviousCourse And _
                         Not dr("ClsSectionId").ToString.Trim = strPreviousClass) Then
                        table.Rows.Add(row)
                    End If

                    'For elimiating duplicate rows
                    strPreviousCourse = dr("CourseId").ToString
                    strPreviousClass = dr("ClsSectionId").ToString
                    strPreviousPeriod = dr("ClassPeriod").ToString

                    'Clear list collection
                    classSchedules.Clear()
                Next
                RadGrid2.Visible = True
                With RadGrid2
                    .DataSource = table
                    .DataBind()
                End With
                Exit Sub
            Else
                hiddenflag.Value = 0
                lblScheduleConflicts.Text = ""
                RadGrid2.Visible = False
                tbl.LoadDataRow(New Object() {clsMeetingId, strDayId, strRoomId, STimeDescrip, _
                                                                             ClsSectionId.ToString, ETimeDescrip, "sa", _
                                                                             Date.Now, ddlContainerPeriod.SelectedValue.ToString, ddlContainerAltPeriod.SelectedValue.ToString, _
                                                                           sDate, EDate, ddlInstructionType.SelectedValue.ToString, True}, False)
            End If
        End If
        'Commented by Balaji on 12.7.2011 
        'reschedules class before cancelling - not allowed

        'If tbl.Rows.Count >= 1 Then
        '    If Not tbl Is Nothing Then
        '        Dim ds1 As New DataSet
        '        For Each lcol As DataColumn In tbl.Columns
        '            lcol.ColumnMapping = System.Data.MappingType.Attribute
        '        Next
        '        ds1.Tables.Add(tbl)
        '        Dim strXML As String = ds1.GetXml
        '        Dim clsFacade As New UnscheduleClsFacade
        '        Dim intReturn As Integer = 0
        '        intReturn = clsFacade.RescheduleClassMeetings(strXML)
        '        If intReturn = 1 Then
        '            'lblMessage.visible = True
        '            'lblMessage.text = "Class was rescheduled successfully"
        '        End If
        '    End If
        'End If
        'btnSubmit.Attributes.Add("OnClick", "returnToParent();")
        RadAjaxManager1.ResponseScripts.Add("returnToParent();")
    End Sub
    Protected Sub ddlContainerRooms_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContainerRooms.SelectedIndexChanged
        'Dim strDayId As String = Guid.Empty.ToString
        'Dim strRoomId As String = ddlContainerRooms.SelectedValue
        'Dim STimeDescrip As String = Guid.Empty.ToString
        'Dim ETimeDescrip As String = Guid.Empty.ToString
        'Dim sDate As Date = txtSDate.DbSelectedDate
        'Dim EDate As Date = txtEDate.DbSelectedDate

        'ClsSectionId = Request.QueryString("ClsSectionId")
        'campusId = Request.QueryString("campusId")
        'clsMeetingId = txtrescheduledmeetingid.Text


        'Dim dsScheduleConflicts As New DataSet
        'dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileAddingClassesArray(ClsSectionId.ToString, _
        '                                                                                sDate, _
        '                                                                                EDate, _
        '                                                                                strRoomId, _
        '                                                                                ddlInstructionType.SelectedValue.ToString, _
        '                                                                                txtTermId.Text, _
        '                                                                                txtreqid.Text, _
        '                                                                                ddlContainerPeriod.SelectedValue.ToString, _
        '                                                                                campusId)

        'Dim table As DataTable
        'Dim strPreviousCourse As String = ""
        'Dim strPreviousClass As String = ""
        'Dim strPreviousPeriod As String = ""
        'Dim classSchedules As New List(Of String)
        'table = CreateNewTable()
        'If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
        '    lblScheduleConflicts.Text = "Class cannot be rescheduled as there is a schedule conflict"
        '    hiddenflag.Value = 1
        '    For Each dr As DataRow In dsScheduleConflicts.Tables(0).Rows
        '        Dim row As DataRow
        '        Dim expression As String = "CourseDescrip='" + dr("CourseDescrip").ToString + "'" + " and ClsSection='" + dr("ClsSection").ToString + "'" + " and ClassPeriod='" + dr("ClassPeriod").ToString + "'"

        '        row = table.NewRow()
        '        ' Then add the new row to the collection.
        '        row("CourseDescrip") = dr("CourseDescrip").ToString
        '        row("ClsSection") = dr("ClsSection").ToString
        '        row("ClassPeriod") = dr("ClassPeriod").ToString

        '        Dim matchRows() As DataRow
        '        matchRows = dsScheduleConflicts.Tables(0).Select(expression)

        '        Dim i As Integer
        '        Dim strMeetingSchedule As String = ""
        '        For i = 0 To matchRows.GetUpperBound(0)
        '            strMeetingSchedule &= matchRows(i)("MeetingSchedule").ToString
        '            strMeetingSchedule &= vbCrLf
        '            'classSchedules.Add(matchRows(i)("MeetingSchedule").ToString)
        '        Next
        '        'row("MeetingSchedule") = classSchedules.ToList().ToString
        '        row("MeetingSchedule") = strMeetingSchedule

        '        'If row was already added to datatable ignore duplicates
        '        If (Not dr("CourseId").Trim = strPreviousCourse And _
        '             Not dr("ClsSectionId").ToString.Trim = strPreviousClass) Then
        '            'If (Not dr("CourseId").Trim = strPreviousCourse And _
        '            '       Not dr("ClsSectionId").ToString.Trim = strPreviousClass And _
        '            '        Not row("ClassPeriod").Trim = strPreviousPeriod Then
        '            'Dim MeetingInfo As New ClsSectMeetingInfo
        '            'With MeetingInfo
        '            '    .CourseDescrip = row("CourseDescrip").ToString
        '            '    .ClsSection = row("ClsSection").ToString.Trim
        '            '    .MeetingSchedule = classSchedules
        '            'End With
        '            'table.Rows.Add(row("CourseDescrip"), row("ClsSection"), row("ClassPeriod"), row("MeetingSchedule"))
        '            table.Rows.Add(row)
        '            'table.Rows.Add(MeetingInfo.CourseDescrip, MeetingInfo.ClsSection, "Period", MeetingInfo.MeetingSchedule.ToList.ToString)
        '        End If

        '        'For elimiating duplicate rows
        '        strPreviousCourse = dr("CourseId").ToString
        '        strPreviousClass = dr("ClsSectionId").ToString
        '        strPreviousPeriod = dr("ClassPeriod").ToString

        '        'Clear list collection
        '        classSchedules.Clear()
        '    Next
        '    RadGrid2.Visible = True
        '    With RadGrid2
        '        .DataSource = table
        '        .DataBind()
        '    End With
        'Else
        '    lblScheduleConflicts.Text = ""
        '    RadGrid2.Visible = False
        'End If
    End Sub
    Private Function CreateNewTable() As DataTable
        ' Create a new DataTable titled 'Names.'
        Dim namesTable As DataTable = New DataTable("Schedules")

        ' Add three column objects to the table.
        Dim strCourseDescrip As DataColumn = New DataColumn()
        strCourseDescrip.DataType = System.Type.GetType("System.String")
        strCourseDescrip.ColumnName = "CourseDescrip"
        namesTable.Columns.Add(strCourseDescrip)

        Dim strClsSection As DataColumn = New DataColumn()
        strClsSection.DataType = System.Type.GetType("System.String")
        strClsSection.ColumnName = "ClsSection"
        namesTable.Columns.Add(strClsSection)

        Dim strClsSectionPeriod As DataColumn = New DataColumn()
        strClsSectionPeriod.DataType = System.Type.GetType("System.String")
        strClsSectionPeriod.ColumnName = "ClassPeriod"
        namesTable.Columns.Add(strClsSectionPeriod)

        Dim strMeetingSchedule As DataColumn = New DataColumn()
        strMeetingSchedule.DataType = System.Type.GetType("System.String")
        strMeetingSchedule.ColumnName = "MeetingSchedule"
        namesTable.Columns.Add(strMeetingSchedule)

        ' Return the new DataTable.
        CreateNewTable = namesTable
    End Function

End Class
