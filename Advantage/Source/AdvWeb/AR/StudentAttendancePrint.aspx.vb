
Imports System.IO
Imports System.Xml
Imports System.Data
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade

Partial Class StudentAttendancePrint
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected StudentId As String
    Protected resourceId As Integer
    Dim strAttUnitType As String = ""
    Dim campusid As String
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'commented by Theresa G on Sep 08 2010 because the page is erroring out since we dont pass the VID
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        'Dim strVID As String
        'Dim state As AdvantageSessionState
        'Dim rfv As New RequiredFieldValidator


        ''Get the StudentId from the state object associated with this page
        'strVID = HttpContext.Current.Request.Params("VID")
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        ''   get the studentId fgrom the session
        'If CommonWebUtilities.IsValidGuid(strVID) Then
        '    If (state.Contains(strVID)) Then
        '        StudentId = DirectCast(state(strVID), AdvantageStateInfo).StudentId
        '    Else
        '        'Send the user to error page requesting a login again..
        '        Session("Error") = "This is not a valid action. Please login to Advantage again."
        '        Response.Redirect("../ErrorPage.aspx")
        '    End If
        'Else
        '    'Send the user to error page requesting a login again..
        '    Session("Error") = "This is not a valid action. Please login to Advantage again."
        '    Response.Redirect("../ErrorPage.aspx")
        'End If
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        campusid = HttpContext.Current.Request.Params("CampusId") 'cmpid
        PopulateAllPanels()
        ' resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'If Not Page.IsPostBack Then
        '    '16521: ENH: Galen: FERPA: Compliance Issue 
        '    'added by Theresa G on May 7th 2010
        '    'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
        '    '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
        '    'End If
        'End If

        'Try
        '    '   build Xml document with deposit information
        '    Dim xmlStream As MemoryStream = CreateXMLStreamToPrint()

        '    '   get XslStyleSheet 
        '    Dim templateFilePath As String = Server.MapPath("..") + "/" + context.Request.Headers.Item("host") + "/Xsl/AttendanceReportPdf.xsl"

        '    ''   generate document in PDF using Antenna formatter
        '    Dim xslt As New XslTransform
        '    xslt.Load(templateFilePath)

        '    Dim xpathdocument As New xpathdocument(xmlStream)

        '    Dim documentMemoryStream As New MemoryStream
        '    Dim writer As New XmlTextWriter(documentMemoryStream, Nothing)

        '    writer.Formatting = Formatting.Indented

        '    xslt.Transform(xpathdocument, Nothing, writer, Nothing)

        '    XfoDotNetCtl.XfoObj.Initialize()
        '    Dim doc As New XfoDotNetCtl.XfoObj
        '    Dim pdfStream As New MemoryStream

        '    Try
        '        doc.Render(documentMemoryStream, pdfStream)

        '    Catch ex As XfoDotNetCtl.XfoException
         '    	Dim exTracker = new AdvApplicationInsightsInitializer()
        '    	exTracker.TrackExceptionWrapper(ex)

        '        doc.Dispose()
        '        XfoDotNetCtl.XfoObj.Terminate()
        '    End Try
        '    ''end of code for Antenna Formatter

        '    Response.Clear()
        '    Response.ContentType = "application/pdf"
        '    Response.AppendHeader("content-length", Convert.ToString(pdfStream.Length))
        '    Response.BinaryWrite(pdfStream.ToArray())
        '    Response.End()

        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    '   Redirect to error page.
        '    If ex.InnerException Is Nothing Then
        '        Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
        '    Else
        '        Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
        '    End If
        '    Response.Redirect("../ErrorPage.aspx")
        'End Try

    End Sub

    Private Sub BuildAttendanceGridForPresentAbsent(ByVal NumberOfDates As Integer)





        Dim facAttendance As New ClsSectAttendanceFacade
        Dim arrDates As ArrayList
        Dim holiday As Integer = 0
        Dim tbl As Table


        Dim dtClsSectionMeets As New DataTable
        Dim drDays() As DataRow
        Dim dtMeetDays As New DataTable

        '   First row contains Date, Time, Present and Absent labels



        If NumberOfDates > 0 Then


            dtMeetDays = DirectCast(Session("MeetDays"), DataTable)
            drDays = dtMeetDays.Select("ClsSectionId = '" & Trim(Request.QueryString("ClsSectionId")) & "'")
            dtClsSectionMeets = drDays.CopyToDataTable()
            dtClsSectionMeets = dtClsSectionMeets.DefaultView.ToTable(True, "PeriodDescrip", "InstructionTypeDescrip", "ClsSectMeetingId", "UseTimeClock", "StartDate", "EndDate", "TimeIntervalDescrip")
            '   First row contains Day Of Week, Dates, Time, Attendance labels
            For idx As Integer = 0 To dtClsSectionMeets.Rows.Count - 1
                '   Attendance grid

                If idx = 0 Then
                    If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                        arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, Trim(Request.QueryString("ClsSectionId")), dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                    Else
                        arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(Trim(Request.QueryString("ClsSectionId")), dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                    End If


                    holiday = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - arrDates.Count


                    tbl = New Table
                    tbl.Width = Unit.Percentage(95)
                    tbl.CssClass = "contenttable"
                    tbl.ID = "tblAttendance" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    Dim row As TableRow
                    '   Day of Week column header
                    Dim cc As New TableCell
                    Dim lbl As New Label

                    row = New TableRow
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "<strong>" & dtClsSectionMeets.Rows(idx)("InstructionTypeDescrip") & " - " & dtClsSectionMeets.Rows(idx)("PeriodDescrip") & "</strong>"
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    row.CssClass = "printheaderrow"
                    row.BackColor = System.Drawing.Color.LightGray
                    cc.ColumnSpan = 5
                    cc.Width = Unit.Percentage(100)
                    row.Cells.Add(cc)
                    tbl.Rows.Add(row)


                    row = New TableRow
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Date"
                    lbl.ID = "lblDate" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(40)
                    row.Cells.Add(cc)

                    '   Time column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Time"
                    lbl.ID = "lblTime" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(35)
                    row.Cells.Add(cc)

                    '   Attendance column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Attendance"
                    lbl.ID = "lblAtt" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(15)
                    row.Cells.Add(cc)

                    '   Add first row to Attendance grid
                    tbl.Rows.Add(row)
                    NumberOfDates = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - holiday
                    '   Loop to create one row per Date
                    For i As Integer = 1 To NumberOfDates
                        row = New TableRow

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblDate" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Left
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(50)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblTime" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Left
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(35)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblAtt" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Center
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(15)
                        row.Cells.Add(cc)

                        tbl.Rows.Add(row)
                    Next

                    pnlAttendance.Controls.Add(tbl)
                ElseIf dtClsSectionMeets.Rows(idx - 1)("ClsSectMeetingId") <> dtClsSectionMeets.Rows(idx)("ClsSectMeetingId") Then
                    If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                        arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, Trim(Request.QueryString("ClsSectionId")), dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                    Else
                        arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(Trim(Request.QueryString("ClsSectionId")), dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                    End If


                    holiday = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - arrDates.Count


                    tbl = New Table
                    tbl.Width = Unit.Percentage(95)
                    tbl.CssClass = "contenttable"
                    tbl.ID = "tblAttendance" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    Dim row As TableRow
                    '   Day of Week column header
                    Dim cc As New TableCell
                    Dim lbl As New Label

                    row = New TableRow
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "<strong>" & dtClsSectionMeets.Rows(idx)("InstructionTypeDescrip") & " - " & dtClsSectionMeets.Rows(idx)("PeriodDescrip") & "</strong>"
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    row.CssClass = "printheaderrow"
                    row.BackColor = System.Drawing.Color.LightGray
                    cc.ColumnSpan = 5
                    cc.Width = Unit.Percentage(100)
                    row.Cells.Add(cc)
                    tbl.Rows.Add(row)


                    row = New TableRow
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Date"
                    lbl.ID = "lblDate" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(40)
                    row.Cells.Add(cc)

                    '   Time column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Time"
                    lbl.ID = "lblTime" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(35)
                    row.Cells.Add(cc)

                    '   Attendance column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Attendance"
                    lbl.ID = "lblAtt" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(15)
                    row.Cells.Add(cc)

                    '   Add first row to Attendance grid
                    tbl.Rows.Add(row)
                    NumberOfDates = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - holiday
                    '   Loop to create one row per Date
                    For i As Integer = 1 To NumberOfDates
                        row = New TableRow

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblDate" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Left
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(50)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblTime" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Left
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(35)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblAtt" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Center
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(15)
                        row.Cells.Add(cc)

                        tbl.Rows.Add(row)
                    Next

                    pnlAttendance.Controls.Add(tbl)

                End If



            Next
        End If
    End Sub

    Private Sub BuildAttendanceGridForMinutes(ByVal NumberOfDates As Integer)
        '   Attendance grid
        Dim tbl As Table

        Dim facAttendance As New ClsSectAttendanceFacade
        Dim arrDates As ArrayList
        Dim holiday As Integer = 0
        Dim dtClsSectionMeets As New DataTable
        Dim drDays() As DataRow
        Dim dtMeetDays As New DataTable
        If NumberOfDates > 0 Then
            '   First row contains Date, Time, Present and Absent labels
            dtMeetDays = DirectCast(Session("MeetDays"), DataTable)
            drDays = dtMeetDays.Select("ClsSectionId = '" & Trim(Request.QueryString("ClsSectionId")) & "'")
            dtClsSectionMeets = drDays.CopyToDataTable()
            dtClsSectionMeets = dtClsSectionMeets.DefaultView.ToTable(True, "PeriodDescrip", "InstructionTypeDescrip", "ClsSectMeetingId", "UseTimeClock", "StartDate", "EndDate", "TimeIntervalDescrip")
            For idx As Integer = 0 To dtClsSectionMeets.Rows.Count - 1

                If idx = 0 Then
                    If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                        arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, Trim(Request.QueryString("ClsSectionId")), dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                    Else
                        arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(Trim(Request.QueryString("ClsSectionId")), dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                    End If


                    holiday = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - arrDates.Count
                    tbl = New Table
                    tbl.Width = Unit.Percentage(95)
                    tbl.CssClass = "contenttable"
                    tbl.ID = "tblAttendance" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString

                    Dim row As TableRow
                    '   Day of Week column header
                    Dim cc As New TableCell
                    Dim lbl As New Label

                    'First row, first cell contains "Class Duration" label
                    NumberOfDates = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - holiday
                    row = New TableRow
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "<strong>" & dtClsSectionMeets.Rows(idx)("InstructionTypeDescrip") & " - " & dtClsSectionMeets.Rows(idx)("PeriodDescrip") & "</strong>"
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    row.CssClass = "printheaderrow"
                    row.BackColor = System.Drawing.Color.LightGray
                    cc.ColumnSpan = 5
                    cc.Width = Unit.Percentage(100)
                    row.Cells.Add(cc)
                    tbl.Rows.Add(row)

                    row = New TableRow
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Date"
                    lbl.ID = "lblDate" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(40)
                    row.Cells.Add(cc)

                    '   Time column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Time"
                    lbl.ID = "lblTime" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(30)
                    row.Cells.Add(cc)

                    '   Present column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Present(Minutes)"
                    If strAttUnitType = "CLOCK HOURS" Then
                        lbl.Text = "Present(Hours)"
                    End If

                    lbl.ID = "lblAtt" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Center
                    cc.Wrap = True
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(15)
                    row.Cells.Add(cc)

                    '   Absent column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Absent(Minutes)"
                    If strAttUnitType = "CLOCK HOURS" Then
                        lbl.Text = "Absent(Hours)"
                    End If
                    lbl.ID = "lblAbs" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Center
                    cc.Wrap = True
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(15)
                    row.Cells.Add(cc)

                    '   Tardy column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Tardy(Minutes)"
                    If strAttUnitType = "CLOCK HOURS" Then
                        lbl.Text = "Tardy(Hours)"
                    End If
                    lbl.ID = "lblTardy" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Center
                    cc.Wrap = True
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(15)
                    row.Cells.Add(cc)

                    '   Add first row to Attendance grid
                    tbl.Rows.Add(row)

                    '   Loop to create one row per Date
                    For i As Integer = 1 To NumberOfDates
                        row = New TableRow

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblDate" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Left
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(40)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblTime" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Left
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(30)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblAtt" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Center
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(15)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblAbs" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Center
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(15)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblTardy" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Center
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(15)
                        row.Cells.Add(cc)

                        tbl.Rows.Add(row)
                    Next

                    pnlAttendance.Controls.Add(tbl)
                ElseIf dtClsSectionMeets.Rows(idx - 1)("ClsSectMeetingId") <> dtClsSectionMeets.Rows(idx)("ClsSectMeetingId") Then
                    If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                        arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, Trim(Request.QueryString("ClsSectionId")), dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                    Else
                        arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(Trim(Request.QueryString("ClsSectionId")), dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                    End If


                    holiday = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - arrDates.Count
                    tbl = New Table
                    tbl.Width = Unit.Percentage(95)
                    tbl.CssClass = "contenttable"
                    tbl.ID = "tblAttendance" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString

                    Dim row As TableRow
                    '   Day of Week column header
                    Dim cc As New TableCell
                    Dim lbl As New Label

                    'First row, first cell contains "Class Duration" label
                    NumberOfDates = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - holiday
                    row = New TableRow
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "<strong>" & dtClsSectionMeets.Rows(idx)("InstructionTypeDescrip") & " - " & dtClsSectionMeets.Rows(idx)("PeriodDescrip") & "</strong>"
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    row.CssClass = "printheaderrow"
                    row.BackColor = System.Drawing.Color.LightGray
                    cc.ColumnSpan = 5
                    cc.Width = Unit.Percentage(100)
                    row.Cells.Add(cc)
                    tbl.Rows.Add(row)

                    row = New TableRow
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Date"
                    lbl.ID = "lblDate" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(40)
                    row.Cells.Add(cc)

                    '   Time column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Time"
                    lbl.ID = "lblTime" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Left
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(30)
                    row.Cells.Add(cc)

                    '   Present column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Present(Minutes)"
                    If strAttUnitType = "CLOCK HOURS" Then
                        lbl.Text = "Present(Hours)"
                    End If

                    lbl.ID = "lblAtt" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Center
                    cc.Wrap = True
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(15)
                    row.Cells.Add(cc)

                    '   Absent column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Absent(Minutes)"
                    If strAttUnitType = "CLOCK HOURS" Then
                        lbl.Text = "Absent(Hours)"
                    End If
                    lbl.ID = "lblAbs" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Center
                    cc.Wrap = True
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(15)
                    row.Cells.Add(cc)

                    '   Tardy column header
                    cc = New TableCell
                    lbl = New Label
                    lbl.CssClass = "printheader"
                    lbl.Text = "Tardy(Minutes)"
                    If strAttUnitType = "CLOCK HOURS" Then
                        lbl.Text = "Tardy(Hours)"
                    End If
                    lbl.ID = "lblTardy" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString
                    cc.VerticalAlign = VerticalAlign.Middle
                    cc.HorizontalAlign = HorizontalAlign.Center
                    cc.Wrap = True
                    cc.Controls.Add(lbl)
                    cc.CssClass = "printcell"
                    cc.Width = Unit.Percentage(15)
                    row.Cells.Add(cc)

                    '   Add first row to Attendance grid
                    tbl.Rows.Add(row)

                    '   Loop to create one row per Date
                    For i As Integer = 1 To NumberOfDates
                        row = New TableRow

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblDate" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Left
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(40)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblTime" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Left
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(30)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblAtt" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Center
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(15)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblAbs" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Center
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(15)
                        row.Cells.Add(cc)

                        cc = New TableCell
                        lbl = New Label
                        lbl.CssClass = "printcontent"
                        lbl.ID = "lblTardy" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & i
                        cc.VerticalAlign = VerticalAlign.Middle
                        cc.HorizontalAlign = HorizontalAlign.Center
                        cc.Controls.Add(lbl)
                        cc.CssClass = "printcell"
                        cc.Width = Unit.Percentage(15)
                        row.Cells.Add(cc)

                        tbl.Rows.Add(row)
                    Next

                    pnlAttendance.Controls.Add(tbl)
                End If



            Next
        End If
    End Sub

    Private Sub PopulateAllPanels()
        Dim rowCounter As Integer
        Dim ttlPresent As Decimal
        Dim ttlExcused As Decimal
        Dim ttlAbsent As Decimal
        Dim ttlTardy As Decimal
        Dim ttlScheduled As Decimal
        Dim ttlMakeUp As Decimal
        Dim dtm As DateTime
        Dim temp As String

        Dim ctrl As New Control
        Dim tbl As New Table
        Dim arrRows() As DataRow
        Dim drDays() As DataRow
        Dim dtLabels As New DataTable
        Dim dtMeetDays As New DataTable
        Dim dtClsSections As New DataTable
        Dim stuEnrollId As String = Trim(Request.QueryString("StuEnrollId"))
        Dim clsSectionId As String = Trim(Request.QueryString("ClsSectionId"))
        Dim PrgVerTrack As String = Trim(Request.QueryString("PrgVerTrack"))
        Dim facAttendance As New ClsSectAttendanceFacade
        'Dim classDuration As String
        Dim factor As Integer
        Dim ccounter As Integer
        Dim absences As Decimal
        Dim minusPresent As Decimal
        Dim minusTardies As Decimal
        Dim dtClsSectionMeets As New DataTable
        Dim sday As String
        Dim sTime As String
        Dim holiday As Integer = 0
        '   get tables from Session
        dtLabels = DirectCast(Session("AttendancePrintLabels"), DataTable)
        dtMeetDays = DirectCast(Session("MeetDays"), DataTable)
        dtClsSections = DirectCast(Session("ClsSections"), DataTable)

        '   populate labels on page from AttendancePrintLabels Session table
        If Not IsNothing(Session("AttendancePrintLabels")) AndAlso dtLabels.Rows.Count > 0 Then
            '   this table has only one row
            Dim dr As DataRow = dtLabels.Rows(0)
            lblStudentName.Text = dr("StudentName")
            lblEnrollmentDescrip.Text = dr("EnrollDescrip")
            lblTermDescrip.Text = dr("TermDescrip")
            lblClassSection.Text = dr("ClsSection")
            lblClassDuration.Text = dr("ClsDuration")
            lblInstructor.Text = dr("Instructor")
            '   attendance unit type is used later on this method
            strAttUnitType = dr("AttendanceUnitType")
        End If

        ttlPresent = 0
        ttlExcused = 0
        ttlAbsent = 0
        ttlTardy = 0
        temp = ""

        Dim DateColl As Collection = DirectCast(Session("Dates"), Collection)
        Dim arrDates As ArrayList = DateColl.Item(clsSectionId)

        drDays = dtMeetDays.Select("ClsSectionId = '" & clsSectionId & "'")
        If strAttUnitType = "MINUTES" Or strAttUnitType = "CLOCK HOURS" Then
            BuildAttendanceGridForMinutes(CDate(arrDates(arrDates.Count - 1)).Date.Subtract(CDate(arrDates(0)).Date).Days + 1)
        Else
            BuildAttendanceGridForPresentAbsent(arrDates.Count)
        End If



        'We start at the second row since the first row is the header



        dtClsSectionMeets = drDays.CopyToDataTable()
        dtClsSectionMeets = dtClsSectionMeets.DefaultView.ToTable(True, "PeriodDescrip", "InstructionTypeDescrip", "ClsSectMeetingId", "UseTimeClock", "StartDate", "EndDate", "TimeIntervalDescrip")
        For idx As Integer = 0 To dtClsSectionMeets.Rows.Count - 1

            If idx = 0 Then
                sTime = CDate(dtClsSectionMeets.Rows(idx)("TimeIntervalDescrip")).TimeOfDay.ToString
                drDays = dtMeetDays.Select("ClsSectionId = '" & clsSectionId & "'  and ClsSectMeetingId= '" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & "'")
                ctrl = pnlAttendance.FindControl("tblAttendance" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                tbl = DirectCast(ctrl, Table)

                If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                    arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, clsSectionId, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                Else
                    arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(clsSectionId, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                End If


                'holiday = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - arrDates.Count


                For rowCounter = 2 To tbl.Rows.Count - 1
                    'For rowCounter = 2 To arrDates.Count
                    dtm = arrDates.Item(rowCounter - 2)
                    sday = facAttendance.GetShortDayName(dtm)
                    DirectCast(tbl.Rows(rowCounter).Cells(0).Controls(0), Label).Text = sday & ", " & facAttendance.GetShortMonthName(dtm) & " " & dtm.Day.ToString
                    ' DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0), Label).Text = facAttendance.GetMeetingTimeForMeetDate(drDays, dtm)
                    If GetDurationForMeetDate(drDays, sday, dtm, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString) <> "" Then
                        DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0), Label).Text = facAttendance.GetMeetingTimeForMeetDate(drDays, dtm)
                    End If

                    'Check the ActualAttendance dt in session to see if this student has attendance
                    'posted for this day. If so we want to fill the textbox with the amount of hours
                    'actually attended.
                    arrRows = DirectCast(Session("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND (MeetDate = #" & dtm & "# or meetdate=#" & dtm.Date & "#)  AND ClsSectionId = '" & clsSectionId & "' and ClsSectMeetingId = '" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & "'")

                    If arrRows.Length = 0 Then
                        If dtm.TimeOfDay.Hours = 0 Then dtm = CDate(dtm & " " & sTime)
                        arrRows = DirectCast(Session("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND MeetDate = #" & dtm & "# AND ClsSectionId = '" & clsSectionId & "'and ClsSectMeetingId = '" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & "'")
                    End If



                    If arrRows.Length > 0 Then

                        factor = facAttendance.GetTardiesMakingAbsence(stuEnrollId, clsSectionId)

                        If strAttUnitType.ToUpper = "MINUTES" Or strAttUnitType.ToUpper = "CLOCK HOURS" Then
                            'Minutes system needs to count number of minutes present, tardies and absent.
                            'classDuration = GetScheduledMinutesForMeetDay(dtm, clsSectionId, arrRows(0)("ClsSectMeetingId").ToString)
                            'If Integer.Parse(arrRows(0)("Actual").ToString()) = 0 Then
                            '    DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0), Label).Text = CInt(classDuration)
                            '    ttlAbsent += CInt(classDuration)
                            'Else
                            '    DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = arrRows(0)("Actual").ToString()
                            '    ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                            '    ttlTardy += CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                            '    If CInt(classDuration) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                            '        ccounter += 1
                            '        DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0), Label).Text = CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                            '        If ccounter = factor Then
                            '            absences += CInt(classDuration)
                            '            minusPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                            '            minusTardies += CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                            '            ccounter = 0
                            '        End If
                            '    End If
                            'End If
                            'classDuration = GetScheduledMinutesForMeetDay(dtm, clsSectionId, arrRows(0)("ClsSectMeetingId").ToString)

                            ttlScheduled += arrRows(0)("Scheduled")
                            If strAttUnitType.ToUpper = "CLOCK HOURS" Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = Math.Round((arrRows(0)("Actual") / 60), 2)
                            Else
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = arrRows(0)("Actual").ToString()
                            End If

                            If arrRows(0)("Tardy") Then DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0), Label).Text = "Yes"
                            If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                                If strAttUnitType.ToUpper = "CLOCK HOURS" Then
                                    DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0), Label).Text = (Math.Round((CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual"))) / 60, 2)).ToString()
                                Else
                                    DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0), Label).Text = CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                End If

                            End If

                            If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) And Not arrRows(0)("Tardy") Then
                                ttlAbsent += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                'DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0), Label).Text = CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                                ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                            Else
                                ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())

                                If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                                    ttlAbsent += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                    ccounter += 1
                                    If ccounter = factor Then
                                        absences += CInt(arrRows(0)("Scheduled"))
                                        minusPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                        minusTardies += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                        ccounter = 0
                                    Else
                                        absences += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                    End If
                                    ttlTardy += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                Else
                                    ttlMakeUp += Integer.Parse(arrRows(0)("Actual").ToString()) - arrRows(0)("Scheduled")
                                End If
                            End If




                        Else
                            'Present/Absent system so we need to translate the 0 and 1 to "A" and "P" respectively.
                            'If the record is marked as tardy we will display a "T" instead of a "P".
                            'If the record is marked as excused we will display an "E" instead of "A"
                            If arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") = True Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = "E"
                                ttlExcused += 1

                            ElseIf arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") <> True Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = "A"
                                ttlAbsent += 1

                            ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") = True Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = "T"
                                ttlTardy += 1

                            ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") <> True Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = "P"
                                ttlPresent += 1
                            End If
                        End If
                    End If
                Next
            ElseIf dtClsSectionMeets.Rows(idx - 1)("ClsSectMeetingId") <> dtClsSectionMeets.Rows(idx)("ClsSectMeetingId") Then
                sTime = CDate(dtClsSectionMeets.Rows(idx)("TimeIntervalDescrip")).TimeOfDay.ToString
                drDays = dtMeetDays.Select("ClsSectionId = '" & clsSectionId & "'  and ClsSectMeetingId= '" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & "'")
                ctrl = pnlAttendance.FindControl("tblAttendance" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString)
                tbl = DirectCast(ctrl, Table)

                If MyAdvAppSettings.AppSettings("SchedulingMethod").ToString = "Traditional" Then
                    arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(drDays, clsSectionId, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                Else
                    arrDates = facAttendance.GetMeetDatesForMeetingsArrayListFromRange(clsSectionId, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString, dtClsSectionMeets.Rows(idx)("StartDate").ToString, dtClsSectionMeets.Rows(idx)("EndDate").ToString, campusid)
                End If


                'holiday = DateDiff(DateInterval.Day, dtClsSectionMeets.Rows(idx)("StartDate"), dtClsSectionMeets.Rows(idx)("EndDate")) + 1 - arrDates.Count


                For rowCounter = 2 To tbl.Rows.Count - 1
                    'For rowCounter = 2 To arrDates.Count
                    dtm = arrDates.Item(rowCounter - 2)
                    sday = facAttendance.GetShortDayName(dtm)
                    DirectCast(tbl.Rows(rowCounter).Cells(0).Controls(0), Label).Text = sday & ", " & facAttendance.GetShortMonthName(dtm) & " " & dtm.Day.ToString
                    ' DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0), Label).Text = facAttendance.GetMeetingTimeForMeetDate(drDays, dtm)
                    If GetDurationForMeetDate(drDays, sday, dtm, dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString) <> "" Then
                        DirectCast(tbl.Rows(rowCounter).Cells(1).Controls(0), Label).Text = facAttendance.GetMeetingTimeForMeetDate(drDays, dtm)
                    End If

                    'Check the ActualAttendance dt in session to see if this student has attendance
                    'posted for this day. If so we want to fill the textbox with the amount of hours
                    'actually attended.
                    arrRows = DirectCast(Session("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND (MeetDate = #" & dtm & "# or meetdate=#" & dtm.Date & "#)  AND ClsSectionId = '" & clsSectionId & "' and ClsSectMeetingId = '" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & "'")

                    If arrRows.Length = 0 Then
                        If dtm.TimeOfDay.Hours = 0 Then dtm = CDate(dtm & " " & sTime)
                        arrRows = DirectCast(Session("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND MeetDate = #" & dtm & "# AND ClsSectionId = '" & clsSectionId & "'and ClsSectMeetingId = '" & dtClsSectionMeets.Rows(idx)("ClsSectMeetingId").ToString & "'")
                    End If



                    If arrRows.Length > 0 Then

                        factor = facAttendance.GetTardiesMakingAbsence(stuEnrollId, clsSectionId)

                        If strAttUnitType.ToUpper = "MINUTES" Or strAttUnitType.ToUpper = "CLOCK HOURS" Then
                            'Minutes system needs to count number of minutes present, tardies and absent.
                            'classDuration = GetScheduledMinutesForMeetDay(dtm, clsSectionId, arrRows(0)("ClsSectMeetingId").ToString)
                            'If Integer.Parse(arrRows(0)("Actual").ToString()) = 0 Then
                            '    DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0), Label).Text = CInt(classDuration)
                            '    ttlAbsent += CInt(classDuration)
                            'Else
                            '    DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = arrRows(0)("Actual").ToString()
                            '    ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                            '    ttlTardy += CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                            '    If CInt(classDuration) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                            '        ccounter += 1
                            '        DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0), Label).Text = CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                            '        If ccounter = factor Then
                            '            absences += CInt(classDuration)
                            '            minusPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                            '            minusTardies += CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                            '            ccounter = 0
                            '        End If
                            '    End If
                            'End If
                            'classDuration = GetScheduledMinutesForMeetDay(dtm, clsSectionId, arrRows(0)("ClsSectMeetingId").ToString)

                            ttlScheduled += arrRows(0)("Scheduled")
                            If strAttUnitType.ToUpper = "CLOCK HOURS" Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = Math.Round((arrRows(0)("Actual") / 60), 2)
                            Else
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = arrRows(0)("Actual").ToString()
                            End If

                            If arrRows(0)("Tardy") Then DirectCast(tbl.Rows(rowCounter).Cells(4).Controls(0), Label).Text = "Yes"
                            If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                                If strAttUnitType.ToUpper = "CLOCK HOURS" Then
                                    DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0), Label).Text = (Math.Round((CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual"))) / 60, 2)).ToString()
                                Else
                                    DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0), Label).Text = CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                End If

                            End If

                            If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) And Not arrRows(0)("Tardy") Then
                                ttlAbsent += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                'DirectCast(tbl.Rows(rowCounter).Cells(3).Controls(0), Label).Text = CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                                ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                            Else
                                ttlPresent += Integer.Parse(arrRows(0)("Actual").ToString())

                                If CInt(arrRows(0)("Scheduled")) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                                    ttlAbsent += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                    ccounter += 1
                                    If ccounter = factor Then
                                        absences += CInt(arrRows(0)("Scheduled"))
                                        minusPresent += Integer.Parse(arrRows(0)("Actual").ToString())
                                        minusTardies += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                        ccounter = 0
                                    Else
                                        absences += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                    End If
                                    ttlTardy += CInt(arrRows(0)("Scheduled")) - Integer.Parse(arrRows(0)("Actual").ToString())
                                Else
                                    ttlMakeUp += Integer.Parse(arrRows(0)("Actual").ToString()) - arrRows(0)("Scheduled")
                                End If
                            End If




                        Else
                            'Present/Absent system so we need to translate the 0 and 1 to "A" and "P" respectively.
                            'If the record is marked as tardy we will display a "T" instead of a "P".
                            'If the record is marked as excused we will display an "E" instead of "A"
                            If arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") = True Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = "E"
                                ttlExcused += 1

                            ElseIf arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") <> True Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = "A"
                                ttlAbsent += 1

                            ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") = True Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = "T"
                                ttlTardy += 1

                            ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") <> True Then
                                DirectCast(tbl.Rows(rowCounter).Cells(2).Controls(0), Label).Text = "P"
                                ttlPresent += 1
                            End If
                        End If
                    End If
                Next
            End If


        Next

        If strAttUnitType.ToUpper = "MINUTES" Then
            'Actual Totals
            lblActPresent.Text = "Present: " & Integer.Parse(ttlPresent).ToString("#0") & " Minutes <br>(" & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours)"
            lblActAbsent.Text = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0") & " Minutes <br>(" & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours)"
            'lblActTardy.Text = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0") & " Minutes <br>(" & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours)"
            'lblActExcused.Text = "&nbsp;"
            lblActTardy.Text = "Scheduled: " & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes <br>(" & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours)"
            lblActExcused.Text = "MakeUp: " & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes <br>(" & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours)"
            lblRow2.Visible = False

            'Adjusted Totals
            If PrgVerTrack <> "True" Then
                lblRow2.Visible = True




                If ttlPresent >= minusPresent Then
                    ttlPresent -= minusPresent
                End If
                temp = "Present: " & Integer.Parse(ttlPresent).ToString("#0") & " Minutes<br>("

                lblTtlPresent.Text = temp & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours)" & IIf(minusPresent > 0, "*", "")

                'Excused



                temp = "MakeUp: " & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes<br>("

                lblTtlExcused.Text = temp & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes)"




                'Scheduled

                temp = "Scheduled: " & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes<br>("

                lblTtlTardy.Text = temp & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours)"

                If ttlPresent > 0 And ttlTardy > 0 Then
                    ttlAbsent = absences
                End If



                temp = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0") & " Minutes<br>("

                lblTtlAbsent.Text = temp & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours)" & IIf(absences > 0, "*", "")







            End If

        ElseIf strAttUnitType.ToUpper = "CLOCK HOURS" Then
            lblActPresent.Text = "Present: " & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours <br>(" &
            Integer.Parse(ttlPresent).ToString("#0") & " Minutes)"

            lblActAbsent.Text = "Absent: " & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours <br>(" & Integer.Parse(ttlAbsent).ToString("#0") & " Minutes)"
            'lblActTardy.Text = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0") & " Minutes <br>(" & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours)"
            'lblActExcused.Text = "&nbsp;"
            lblActTardy.Text = "Scheduled: " & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours <br>(" & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes)"
            lblActExcused.Text = "MakeUp: " & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours <br>(" & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes)"
            lblRow2.Visible = False

            'Adjusted Totals
            If PrgVerTrack <> "True" Then

                lblRow2.Visible = True
                If ttlPresent >= minusPresent Then
                    ttlPresent -= minusPresent
                End If
                temp = "Present: " & (Integer.Parse(ttlPresent) / 60).ToString("#0.00") & " Hours<br>("

                lblTtlPresent.Text = temp & Integer.Parse(ttlPresent).ToString("#0") & " Minutes)" & IIf(minusPresent > 0, "*", "")

                'Excused
                'MakeUp

                temp = "MakeUp: " & (Integer.Parse(ttlMakeUp) / 60).ToString("#0.00") & " Hours<br>("
                lblTtlExcused.Text = temp & Integer.Parse(ttlMakeUp).ToString("#0") & " Minutes)"


                'Tardy
                'ctrl2 = tbl.FindControl("lblTtlTardy" & dr("ClsSectionId").ToString)
                'If ttlTardy >= minusTardies Then
                '    ttlTardy -= minusTardies
                'End If
                'temp = "Tardy: " & (Integer.Parse(ttlTardy) / 60).ToString("#0.00") & " Hours<br>("
                'If Not (ctrl2 Is Nothing) Then
                '    DirectCast(ctrl2, Label).Text = temp & Integer.Parse(ttlTardy).ToString("#0") & " Minutes)" & IIf(minusTardies > 0, "*", "")
                'End If


                temp = "Scheduled: " & (Integer.Parse(ttlScheduled) / 60).ToString("#0.00") & " Hours<br>("
                lblTtlTardy.Text = temp & Integer.Parse(ttlScheduled).ToString("#0") & " Minutes)"



                'Absent

                If ttlPresent > 0 And ttlTardy > 0 Then
                    ttlAbsent = absences
                End If
                temp = "Absent: " & (Integer.Parse(ttlAbsent) / 60).ToString("#0.00") & " Hours<br>("

                lblTtlAbsent.Text = temp & Integer.Parse(ttlAbsent).ToString("#0") & " Minutes)" & IIf(absences > 0, "*", "")




            End If


        Else
            'Actual Totals
            lblActPresent.Text = "Present: " & Integer.Parse(ttlPresent).ToString("#0")
            lblActExcused.Text = "Excused: " & Integer.Parse(ttlExcused).ToString("#0")
            lblActTardy.Text = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0")
            lblActAbsent.Text = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0")
            lblRow2.Visible = False
            'Adjusted Totals
            If PrgVerTrack <> "True" Then
                lblRow2.Visible = True
                absences = facAttendance.GetAbsencesFromTardies(stuEnrollId, clsSectionId, ttlTardy)
                lblTtlPresent.Text = "Present: " & Integer.Parse(ttlPresent).ToString("#0")
                lblTtlExcused.Text = "Excused: " & Integer.Parse(ttlExcused).ToString("#0")
                ttlTardy -= absences
                lblTtlTardy.Text = "Tardy: " & Integer.Parse(ttlTardy).ToString("#0") & IIf(absences > 0, "*", "")
                ttlAbsent += absences
                lblTtlAbsent.Text = "Absent: " & Integer.Parse(ttlAbsent).ToString("#0") & IIf(absences > 0, "*", "")
            End If
        End If
        '
        'Tardies Message
        If PrgVerTrack <> "True" Then
            If absences > 0 Then
                lblTardiesMsg.Text = "* Indicates an adjustment: " & factor & " Tardies make an Absence"
                lblTardiesMsg.Visible = True
            Else
                lblTardiesMsg.Visible = False
            End If

            '
            'Percentage
            'Dim percentage As Decimal = facAttendance.ComputePercentageOfPresent(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
            Dim percentage As Decimal = 0
            If strAttUnitType.ToUpper = "MINUTES" Or strAttUnitType.ToUpper = "CLOCK HOURS" Then
                'percentage = facAttendance.ComputePercentageOfPresentforMinutes(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
                If ttlScheduled > 0 Then percentage = (ttlPresent / ttlScheduled) * 100
            Else
                percentage = facAttendance.ComputePercentageOfPresent(ttlPresent, ttlAbsent, ttlTardy, ttlExcused)
            End If
            lblPerc.Text = "Percentage of Present: " & percentage.ToString("#0.00") & "%"
        End If
    End Sub

    Public Function GetScheduledMinutesForMeetDay(ByVal dtm As DateTime, ByVal clsSectionId As String, ClsSectMeetingId As String) As String
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim sDay As String
        Dim dtDays As New DataTable
        Dim dr As DataRow
        Dim startTime As DateTime
        Dim endTime As DateTime
        Dim schedLength As System.TimeSpan

        'Get the short day value
        sDay = facAttendance.GetShortDayName(dtm)

        'Loop through the days that the class section meets and see which one this day matches one
        dtDays = DirectCast(Session("MeetDays"), DataTable)

        Dim arRow() As DataRow
        arRow = dtDays.Select("ClsSectionId='" & clsSectionId & "' AND WorkDaysDescrip='" & sDay & "' AND ClsSectMeetingId = '" & ClsSectMeetingId & "'")

        If arRow.GetLength(0) > 0 Then

            'dr = arRow(0)
            ''For Each dr In arRow
            ''If dr("WorkDaysDescrip").ToString() = sDay Then
            'startTime = dr("TimeIntervalDescrip")
            'endTime = dr("EndTime")
            ''End If
            ''Next
            ''modified by Saraswathi lakshmanan on June 29 2010
            ''To correct the validation when saving the hours for clock attendance

            ''dr = arRow(0)
            For Each dr In arRow
                If dr("StartDate") <= dtm.Date And dr("EndDate") >= dtm.Date Then
                    startTime = dr("TimeIntervalDescrip")
                    endTime = dr("EndTime")
                    Exit For
                End If
            Next



        End If

        schedLength = endTime.Subtract(startTime)

        Return (schedLength.Hours * 60) + schedLength.Minutes

    End Function

    Private Function BuildAttendanceDS(ByVal attUnitType As String) As DataSet
        Dim rowCounter As Integer
        Dim ttlPresent As Decimal
        Dim ttlExcused As Decimal
        Dim ttlAbsent As Decimal
        Dim ttlTardy As Decimal
        Dim dtm As DateTime
        Dim classDuration As String
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim ds As New DataSet
        Dim drDays() As DataRow
        Dim dr As DataRow
        Dim arrRows() As DataRow
        Dim dtMeetDays As New DataTable
        Dim dtClsSections As New DataTable
        Dim stuEnrollId As String = Trim(Request.QueryString("StuEnrollId"))
        Dim clsSectionId As String = Trim(Request.QueryString("ClsSectionId"))

        Dim dtAttendance As New DataTable("StuAttendance")
        dtAttendance.Columns.Add("Date")
        dtAttendance.Columns.Add("Time")
        If attUnitType.ToUpper = "MINUTES" Then
            dtAttendance.Columns.Add("Present")
            dtAttendance.Columns.Add("Absent")
        Else
            dtAttendance.Columns.Add("Attendance")
        End If

        '   get tables from Session
        dtMeetDays = DirectCast(Session("MeetDays"), DataTable)
        dtClsSections = DirectCast(Session("ClsSections"), DataTable)
        Dim DateColl As Collection = DirectCast(Session("Dates"), Collection)
        Dim arrDates As ArrayList = DateColl.Item(clsSectionId)
        drDays = dtMeetDays.Select("ClsSectionId = '" & clsSectionId & "'")

        For rowCounter = 0 To arrDates.Count - 1
            dtm = arrDates.Item(rowCounter)

            dr = dtAttendance.NewRow
            dr("Date") = dtm.ToShortDateString  'facAttendance.GetShortDayName(dtm) & ", " & facAttendance.GetShortMonthName(dtm) & " " & dtm.Day.ToString
            dr("Time") = facAttendance.GetMeetingTimeForMeetDate(drDays, dtm)

            'Check the ActualAttendance dt in session to see if this student has attendance
            'posted for this day. If so we want to fill the textbox with the amount of hours
            'actually attended.
            arrRows = DirectCast(Session("ActualAttendance"), DataTable).Select("StuEnrollId = '" & stuEnrollId & "'" & " AND MeetDate = #" & dtm & "# AND ClsSectionId = '" & clsSectionId & "'")

            If arrRows.Length > 0 Then

                If attUnitType.ToUpper = "MINUTES" Then
                    dr("Present") = arrRows(0)("Actual").ToString()
                    classDuration = GetScheduledMinutesForMeetDay(dtm, clsSectionId, arrRows(0)("ClsSectMeetingId").ToString)
                    If CInt(classDuration) > Integer.Parse(arrRows(0)("Actual").ToString()) Then
                        dr("Absent") = CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())
                    End If
                    ttlPresent += Decimal.Parse(arrRows(0)("Actual").ToString())
                    ttlAbsent += CInt(classDuration) - Integer.Parse(arrRows(0)("Actual").ToString())

                Else
                    'Present/Absent system so we need to translate the 0 and 1 to "A" and "P" respectively.
                    'If the record is marked as tardy we will display a "T" instead of a "P".
                    'If the record is marked as excused we will display an "E" instead of "A"
                    If arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") = True Then
                        dr("Attendance") = "E"
                        ttlExcused += 1

                    ElseIf arrRows(0)("Actual").ToString() = "0" And arrRows(0)("Excused") <> True Then
                        dr("Attendance") = "A"
                        ttlAbsent += 1

                    ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") = True Then
                        dr("Attendance") = "T"
                        ttlTardy += 1

                    ElseIf arrRows(0)("Actual").ToString() = "1" And arrRows(0)("Tardy") <> True Then
                        dr("Attendance") = "P"
                        ttlPresent += 1
                    End If
                End If

            Else
                '   When Minutes system, need to count the number of missed minutes.
                If attUnitType.ToUpper = "MINUTES" Then
                    classDuration = GetScheduledMinutesForMeetDay(dtm, clsSectionId, arrRows(0)("ClsSectMeetingId").ToString)
                    ttlAbsent += CInt(classDuration)
                End If
            End If

            dtAttendance.Rows.Add(dr)
        Next
        ds.Tables.Add(dtAttendance)


        Dim dtTotals As New DataTable("Totals")
        dtTotals.Columns.Add("Present")
        dtTotals.Columns.Add("Absent")
        dtTotals.Columns.Add("Tardy")
        dtTotals.Columns.Add("Excused")

        dr = dtTotals.NewRow
        dr("Present") = ttlPresent
        dr("Absent") = ttlAbsent
        dr("Tardy") = ttlTardy
        dr("Excused") = ttlExcused
        dtTotals.Rows.Add(dr)
        ds.Tables.Add(dtTotals)


        Return ds
    End Function

    Private Function CreateXMLStreamToPrint() As MemoryStream
        Dim ctrl As New Control
        Dim tbl As New Table
        Dim dtLabels As New DataTable
        Dim myMemoryStream As New MemoryStream
        Dim rowCounter As Integer = 0
        Dim ttlPresent As Decimal = 0
        Dim ttlExcused As Decimal = 0
        Dim ttlAbsent As Decimal = 0
        Dim ttlTardy As Decimal = 0
        Dim classDuration As String = ""
        Dim stuName As String = ""
        Dim stuEnrollment As String = ""
        Dim clsSection As String = ""
        Dim term As String = ""
        Dim temp As String = ""
        Dim attUnitType As String = ""

        '   get table from Session
        dtLabels = DirectCast(Session("AttendancePrintLabels"), DataTable)
        If dtLabels.Rows.Count > 0 Then
            '   this table has only one row
            Dim dr As DataRow = dtLabels.Rows(0)
            stuName = dr("StudentName")
            stuEnrollment = dr("EnrollDescrip")
            term = dr("TermDescrip")
            clsSection = dr("ClsSection")
            attUnitType = dr("AttendanceUnitType")
        End If

        Dim ds As DataSet = BuildAttendanceDS(attUnitType)
        Dim dtAtt As DataTable = ds.Tables("StuAttendance")
        Dim dtTtl As DataTable = ds.Tables("Totals")


        Dim printXmlTextWriter As New XmlTextWriter(myMemoryStream, Nothing)

        '   write xml document header
        printXmlTextWriter.Formatting = System.Xml.Formatting.Indented
        printXmlTextWriter.WriteStartDocument(False)
        printXmlTextWriter.WriteComment("FAME - This document contains the attendance of a student for a particular course")

        '   output start of element "document"
        printXmlTextWriter.WriteStartElement("document")

        '   output start of element "StudentName"
        printXmlTextWriter.WriteStartElement("studentName")
        printXmlTextWriter.WriteString(stuName)
        printXmlTextWriter.WriteEndElement()

        '   output start of element "Enrollment"
        printXmlTextWriter.WriteStartElement("enrollment")
        printXmlTextWriter.WriteString(stuEnrollment)
        printXmlTextWriter.WriteEndElement()

        '   output start of element "TermDescrip"
        printXmlTextWriter.WriteStartElement("term")
        printXmlTextWriter.WriteString(term)
        printXmlTextWriter.WriteEndElement()

        '   output start of element "ClsSection"
        printXmlTextWriter.WriteStartElement("clsSection")
        printXmlTextWriter.WriteString(clsSection)
        printXmlTextWriter.WriteEndElement()

        For Each dr As DataRow In dtAtt.Rows
            '   output start of element "attendance"
            printXmlTextWriter.WriteStartElement("attendance")

            printXmlTextWriter.WriteAttributeString("Date", CType(dr("Date"), String))
            printXmlTextWriter.WriteAttributeString("Time", CType(dr("Time"), String))
            If dr.IsNull("Attendance") Then
                printXmlTextWriter.WriteAttributeString("Attendance", "")
            Else
                printXmlTextWriter.WriteAttributeString("Attendance", CType(dr("Attendance"), String))
            End If

            '   output end tag of element "attendance"
            printXmlTextWriter.WriteEndElement()
        Next

        '   output start of element "totalPresent"
        printXmlTextWriter.WriteStartElement("totalPresent")
        printXmlTextWriter.WriteString(CType(dtTtl.Rows(0)("Present"), Decimal))
        printXmlTextWriter.WriteEndElement()

        '   output start of element "totalAbsent"
        printXmlTextWriter.WriteStartElement("totalAbsent")
        printXmlTextWriter.WriteString(CType(dtTtl.Rows(0)("Absent"), Decimal))
        printXmlTextWriter.WriteEndElement()

        '   output start of element "totalTardy"
        printXmlTextWriter.WriteStartElement("totalTardy")
        printXmlTextWriter.WriteString(CType(dtTtl.Rows(0)("Tardy"), Decimal))
        printXmlTextWriter.WriteEndElement()

        '   output start of element "totalExcused"
        printXmlTextWriter.WriteStartElement("totalExcused")
        printXmlTextWriter.WriteString(CType(dtTtl.Rows(0)("Excused"), Decimal))
        printXmlTextWriter.WriteEndElement()

        '   output end tag of element "document"
        printXmlTextWriter.WriteEndElement()

        'Write the XML to file and close the writer
        printXmlTextWriter.Flush()
        myMemoryStream.Position = 0

        Return myMemoryStream
        ''Dim foo As New UTF8Encoding
        ''Dim buffer(myMemoryStream.Length - 1) As Byte
        ''myMemoryStream.Read(buffer, 0, myMemoryStream.Length)
        ''Dim str As String = foo.GetString(buffer)
    End Function
    Private Function GetDurationForMeetDate(ByVal MeetDays() As DataRow, ByVal sDay As String, ByVal dtm As Date, clsSectionMeetingId As String) As String
        'Dim sDay As String
        Dim dr As DataRow

        'Get the short day value
        'sDay = GetShortDayName(dtm)

        'Loop through the days that the class section meets and see which one this day matches one
        For Each dr In MeetDays
            If dr("WorkDaysDescrip").ToString() = sDay And dtm.Date >= dr("StartDate") And dtm.Date <= dr("EndDate") Then
                'Return "(" & Math.Round(dr("Duration"), 2) & " hrs)"
                If dtm.Date >= CType(dr("StartDate"), Date).Date And dtm.Date <= CType(dr("EndDate"), Date).Date And dr("ClsSectMeetingId").ToString = clsSectionMeetingId Then
                    If dtm.TimeOfDay = CType(dr("TimeIntervalDescrip"), Date).TimeOfDay Then
                        Return Math.Round(dr("Duration"), 2)
                    End If
                End If
            End If
        Next
        Return ""
    End Function
End Class
