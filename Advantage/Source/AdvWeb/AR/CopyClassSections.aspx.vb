﻿Imports FAME.common
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Xml
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common

Partial Class CopyClsSections
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Public campusId, userId As String
    Private tblCounter As String
    Public dsScheduleConflicts As New DataSet
    Protected WithEvents lblDate As System.Web.UI.WebControls.Label
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected MyAdvAppSettings As AdvAppSettings


    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ds As New DataSet
        Dim facade As New CopyClsSectFacade
        Dim objCommon As New CommonUtilities

        Dim userId As String
        '  Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        'disable history button
        'Header1.EnableHistoryButton(False)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            If CommonWebUtilities.GetSchedulingMethod(MyAdvAppSettings.AppSettings("SchedulingMethod")) = AdvantageCommonValues.SchedulingMethods.TraditionalClassesRestrictedByStartDate Then
                lblNote.Visible = True
            Else
                lblNote.Visible = False
            End If
            'Dim objcommon As New CommonUtilities
            'Populate the Activities Datagrid with the person who is logged in Activities
            'Default the Selection Criteria for the activity records
            ViewState("SortFilter") = "TermDescrip, CourseDescrip, ClsSection"
            ViewState("Term") = ""
            ViewState("Course") = ""
            ViewState("Instructor") = ""
            ViewState("MODE") = "NEW"
            ViewState("ClsSectIdGuid") = ""
            ViewState("SelectedIndex") = "-1"
            ViewState("ClassMeetingStartDate") = ""
            ViewState("ClassMeetingEndDate") = ""
            ViewState("RoomId") = ""
            ViewState("DayId") = ""
            ViewState("STimeDescrip") = ""
            ViewState("ETimeDescrip") = ""
            ViewState("InstructionTypeId") = ""
            ViewState("ClsSectionId") = ""
            ViewState("CourseDescrip") = ""
            ds = facade.InitialLoadPage()
            BindDropDownLists(ds)

        Else
            'If Session("JustClosed") = "Closed" Then
            '    MoveRecordToRHS(Session("ActivityAssignmentGuid"))
            '    Session("JustClosed") = ""
            'End If
        End If
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
    End Sub

    Private Sub BindDropDownLists(ByVal ds As DataSet)

        'Bind the 2 ActivityStatus dropdown lists
        BindAllTermDDLs(ds)
        'BindCampusDDL(ds)
    End Sub

    Private Sub BindAllTermDDLs(ByVal ds As DataSet)

        BuildTermDDL(ds, "ddlTerm")

    End Sub
    Private Sub BuildTermDDL(ByVal ds As DataSet, ByVal DDLName As String)
        Dim facade As New AttchGBWToClsSectsFacade
        Dim campusId As String

        campusid = Master.CurrentCampusId


        With ddlTerm1
            .DataSource = facade.GetAllTerms(campusId)
            .DataValueField = "TermId"
            .DataTextField = "TermDescrip"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        'With ddlTerm2
        '    .DataSource = facade.GetAllTerms
        '    .DataValueField = "TermId"
        '    .DataTextField = "TermDescrip"
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", ""))
        '    .SelectedIndex = 0
        'End With
    End Sub

    'Private Sub BindCampusDDL(ByVal ds As DataSet)

    '    With ddlCampus
    '        .DataSource = ds
    '        .DataMember = "CampusDT"
    '        .DataValueField = "CampusId"
    '        .DataTextField = "CampDescrip"
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub

    Private Sub ddlTerm1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTerm1.SelectedIndexChanged
        Dim facade As New CopyClsSectFacade

        Dim ds As New DataSet
        'Dim term As String
        Dim campusId As String
        dgdCopyClsSect.Visible = False
        campusId = HttpContext.Current.Request.Params("cmpid")
        ViewState("CampusId") = campusId
        If ddlTerm1.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("Unable to find data. Please select a Term")
            btnSave.Enabled = False
            ClearRHS()
            Exit Sub
        Else
            PopulateToTerm(campusId)
        End If



        ddlTerm2.Visible = True
        ddlTerm2.Enabled = True
        'dgdCopyClsSect.Visible = True
        Label2.Visible = True

        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        End If



    End Sub
    Private Sub PopulateToTerm(ByVal campusId As String)
        Dim termfacade As New AttchGBWToClsSectsFacade
        Dim Term As String = ddlTerm1.SelectedItem.Value.ToString
        ddlTerm2.Items.Clear()
        With ddlTerm2
            .DataSource = termfacade.GetTerms(Term, campusId)
            .DataValueField = "TermId"
            .DataTextField = "TermDescrip"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub ClearRHS()
        'ddlTerm2.Visible = False
        ddlTerm2.Enabled = False
        dgdCopyClsSect.Visible = False
        'Label2.Visible = False
    End Sub


    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        'Dim sId As String
        'Dim unschedule As Boolean
        'Dim dt As DataTable
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim dt2 As New DataTable
        Dim sw As New System.IO.StringWriter
        '  Dim storedGuid As String
        Dim obj As New Object
        Dim ErrStr As String = ""
        '   Dim count As Integer
        '     Dim facade As New CopyClsSectFacade
        '   Dim row As DataRow
        '  Dim dr1 As DataRow

        Dim ClassSectionObject As New ClassSectionInfo
        Dim ClsSectMtgArrList As ArrayList
        '  Dim GuidArrList As ArrayList
        Dim ClsSectId As String
        Dim Facade As New CopyClsSectFacade
        Dim CSFacade As New ClassSectionFacade
        ' Dim TermId As String
        Dim CourseId As String
        Dim EmpId As String
        '  Dim sdate As String
        '   Dim edate As String
        'Dim stime As String
        'Dim etime As String
        'Dim instr As String
        Dim GrdScaleId As String
        Dim ShiftId As String
        Dim selected As Boolean = False
        Dim NewClsSect As Guid
        Dim resInfo As ClsSectionResultInfo

        Dim tbl As New DataTable("SetupMeetings")
        Dim strGrdComponentTypeId_ReqId As String = ""
        Dim strEffectiveDate As String = ""
        Dim intNumberofTests As Integer = 0
        Dim decMinimumScore As Decimal = 0.0
        Dim strModUser As String = ""
        Dim strModDate As DateTime = Date.Now
        Dim intSelectedCount As Integer = 0
        '        Dim item As GridDataItem

        With tbl
            'Define table schema
            .Columns.Add("ClsSectMeetingId", GetType(String))
            .Columns.Add("WorkDaysId", GetType(String))
            .Columns.Add("RoomId", GetType(String))
            .Columns.Add("TimeIntervalId", GetType(String))
            .Columns.Add("ClsSectionId", GetType(String))
            .Columns.Add("EndIntervalId", GetType(String))
            .Columns.Add("ModUser", GetType(String))
            .Columns.Add("ModDate", GetType(Date))
            .Columns.Add("PeriodId", GetType(String))
            .Columns.Add("AltPeriodId", GetType(String))
            .Columns.Add("StartDate", GetType(Date))
            .Columns.Add("EndDate", GetType(Date))
            .Columns.Add("InstructionTypeID", GetType(String))
            'set field properties
            .Columns("StartDate").AllowDBNull = True
            .Columns("EndDate").AllowDBNull = True
            .Columns("TimeIntervalId").AllowDBNull = True
            .Columns("EndIntervalId").AllowDBNull = True
            .Columns("WorkDaysId").AllowDBNull = True
            .Columns("PeriodId").AllowDBNull = True
            .Columns("AltPeriodId").AllowDBNull = True
        End With

        ' dt = Session("UnschedDateInfo")

        ' Save the datagrid items in a collection.
        iitems = dgdCopyClsSect.Items
        Try
            If ddlTerm2.SelectedItem.Text = "Select" Then
                DisplayErrorMessage("The 'To Term' field cannot be empty. Please select a Term to copy to.")
                Exit Sub
            End If
            'Loop thru the collection to retrieve the chkbox value
            For i = 0 To iitems.Count - 1
                Try
                    iitem = iitems.Item(i)

                    If (CType(iitem.FindControl("chkCopy"), CheckBox).Checked = True) Then


                        'set flag to true if option is selected
                        selected = True

                        'retrieve clssection id from the datagrid
                        ClsSectId = CType(iitem.FindControl("txtClsSectionId"), TextBox).Text()

                        'Move the values in the controls over to the object
                        'ClassSectionObject = PopulateClassSectionObject()
                        'ClassSectionObject.ClsSectId = XmlConvert.ToGuid(ClsSectId)
                        ClassSectionObject.ClsSectId = System.Guid.NewGuid

                        'Save the newly created class section id for later use
                        NewClsSect = ClassSectionObject.ClsSectId

                        'ClassSectionObject.CampusId = New Guid(HttpContext.Current.Request.Params("cmpid"))
                        If ddlTerm2.SelectedItem.Text = "Select" Then
                            DisplayErrorMessage("The 'To Term' field cannot be empty. Please select a Term to copy to.")
                            Exit Sub
                        Else
                            ClassSectionObject.TermId = XmlConvert.ToGuid(ddlTerm2.SelectedItem.Value)
                        End If

                        EmpId = CType(iitem.FindControl("txtEmpId"), TextBox).Text()
                        GrdScaleId = CType(iitem.FindControl("txtGrdScaleId"), TextBox).Text()
                        ShiftId = CType(iitem.FindControl("txtShiftId"), TextBox).Text()
                        If ShiftId <> "" Then
                            ClassSectionObject.ShiftId2 = XmlConvert.ToGuid(ShiftId).ToString
                        End If
                        If EmpId <> "" Then
                            ClassSectionObject.InstructorId2 = XmlConvert.ToGuid(EmpId).ToString
                        End If
                        ClassSectionObject.GrdScaleId = XmlConvert.ToGuid(GrdScaleId)
                        CourseId = CType(iitem.FindControl("txtCourseId"), TextBox).Text()
                        ClassSectionObject.CourseId = XmlConvert.ToGuid(CourseId)
                        ClassSectionObject.StartDate = ViewState("StartDate")
                        ClassSectionObject.EndDate = ViewState("EndDate")
                        ClassSectionObject.Section = CType(iitem.FindControl("lblClsSection"), Label).Text()
                        ClassSectionObject.MaxStud = CType(iitem.FindControl("txtMaxStud"), TextBox).Text()
                        ClassSectionObject.CampusId = XmlConvert.ToGuid(ViewState("CampusId"))

                        ' 10/25/2011 JRobinson Clock Hour Project - Add new fields for arClsSectionTimeClockPolicy table
                        If CType(iitem.FindControl("chkAllowEarlyIn"), CheckBox).Checked = True Then
                            ClassSectionObject.AllowEarlyIn = True
                        Else
                            ClassSectionObject.AllowEarlyIn = False
                        End If
                        If CType(iitem.FindControl("chkAllowLateOut"), CheckBox).Checked = True Then
                            ClassSectionObject.AllowLateOut = True
                        Else
                            ClassSectionObject.AllowLateOut = False
                        End If
                        If CType(iitem.FindControl("chkAllowExtraHours"), CheckBox).Checked = True Then
                            ClassSectionObject.AllowExtraHours = True
                        Else
                            ClassSectionObject.AllowExtraHours = False
                        End If
                        If CType(iitem.FindControl("chkCheckTardyIn"), CheckBox).Checked = True Then
                            ClassSectionObject.CheckTardyIn = True
                        Else
                            ClassSectionObject.CheckTardyIn = False
                        End If
                        ClassSectionObject.MaxInBeforeTardy = CType(iitem.FindControl("txtMaxInBeforeTardy"), TextBox).Text()
                        ClassSectionObject.AssignTardyInTime = CType(iitem.FindControl("txtAssignTardyInTime"), TextBox).Text()

                        'Add the Class Section Header
                        resInfo = Facade.AddClassSection(ClassSectionObject, Session("UserName"))


                        'get the cls section  meetings for this class section.
                        ds = Facade.GetClsSectMtgs(XmlConvert.ToGuid(ClsSectId))

                        'Add these class sections to an array list.
                        ClsSectMtgArrList = Facade.ConvertClsSectMtgsToArrList(ds, EmpId, NewClsSect, ViewState("StartDate"), ViewState("EndDate"))

                        Dim ClsSectMeeting As FAME.AdvantageV1.Common.ClsSectMeetingInfo

                        For Each ClsSectMeeting In ClsSectMtgArrList
                            'if object is not empty
                            'then add each single record
                            If (Not ClsSectMeeting Is Nothing) Then
                                'Check for conflicts
                                If (chkIgnoreConflicts.Checked = False) Then
                                    'If ClsSectMeeting.Day.ToString <> "00000000-0000-0000-0000-000000000000" Then
                                    'dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringCopy(ClsSectId.ToString, ddlTerm2.SelectedValue.ToString)
                                    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsWhileCopyingToAnotherTerm(ClsSectId.ToString, ddlTerm1.SelectedValue.ToString, ddlTerm2.SelectedValue.ToString, campusId)
                                    If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then 'There is a time collision
                                        Dim strStartDate As String = ClsSectMeeting.StartDate
                                        Dim strEndDate As String = ClsSectMeeting.EndDate
                                        Dim strRoomId As String = ClsSectMeeting.Room.ToString
                                        Dim strDayId As String = ClsSectMeeting.Day.ToString
                                        Dim STimeDescrip As String = "" 'ClsSectMeeting.STimeDescrip
                                        Dim ETimeDescrip As String = "" 'ClsSectMeeting.ETimeDescrip
                                        Dim ClsMeetingId As String = ClsSectMeeting.ClsSectMtgId.ToString
                                        Dim strInstructionTypeId As String = ClsSectMeeting.InstructionTypeId.ToString

                                        STimeDescrip = ClsSectMeeting.STimeDescrip
                                        ETimeDescrip = ClsSectMeeting.ETimeDescrip

                                        ViewState("ClsSectionId") = ClsSectId
                                        ViewState("CourseDescrip") = CType(iitem.FindControl("Label3"), Label).Text()

                                        If InStr(ViewState("ClassMeetingStartDate").ToString.Trim, strStartDate.ToString.Trim) <= 0 Then
                                            If Not ViewState("ClassMeetingStartDate").ToString.Trim = "" AndAlso Session("ClassMeetingStartDate_SavedValue") = "" Then
                                                ViewState("ClassMeetingStartDate") &= "," & strStartDate & "," 'If there are existing Period Ids
                                            ElseIf Not ViewState("ClassMeetingStartDate").ToString.Trim = "" AndAlso Not Session("ClassMeetingStartDate_SavedValue").ToString.Trim = "" Then
                                                ViewState("ClassMeetingStartDate") = Replace(ViewState("ClassMeetingStartDate"), Session("ClassMeetingStartDate_SavedValue"), strStartDate)
                                            Else
                                                ViewState("ClassMeetingStartDate") &= strStartDate & ","
                                            End If
                                        End If
                                        If InStr(ViewState("ClassMeetingEndDate").ToString.Trim, strEndDate.ToString.Trim) <= 0 Then
                                            If Not ViewState("ClassMeetingEndDate").ToString.Trim = "" AndAlso Session("ClassMeetingEndDate_SavedValue") = "" Then
                                                ViewState("ClassMeetingEndDate") &= "," & strEndDate & "," 'If there are existing Period Ids
                                            ElseIf Not ViewState("ClassMeetingEndDate").ToString.Trim = "" AndAlso Not Session("ClassMeetingEndDate_SavedValue").ToString.Trim = "" Then
                                                ViewState("ClassMeetingEndDate") = Replace(ViewState("ClassMeetingEndDate"), Session("ClassMeetingEndDate_SavedValue"), strEndDate)
                                            Else
                                                ViewState("ClassMeetingEndDate") &= strEndDate & ","
                                            End If
                                        End If
                                        If InStr(ViewState("RoomId").ToString.Trim, strRoomId.ToString.Trim) <= 0 Then
                                            If Not ViewState("RoomId").ToString.Trim = "" AndAlso Session("RoomId_SavedValue") = "" Then
                                                ViewState("RoomId") &= "," & strRoomId & "," 'If there are existing Period Ids
                                            ElseIf Not ViewState("RoomId").ToString.Trim = "" AndAlso Not Session("RoomId_SavedValue").ToString.Trim = "" Then
                                                ViewState("RoomId") = Replace(ViewState("RoomId"), Session("RoomId_SavedValue"), strRoomId)
                                            Else
                                                ViewState("RoomId") &= strRoomId & ","
                                            End If
                                        End If
                                        If InStr(ViewState("DayId").ToString.Trim, strDayId.ToString.Trim) <= 0 Then
                                            If Not ViewState("DayId").ToString.Trim = "" AndAlso Session("DayId_SavedValue") = "" Then
                                                ViewState("DayId") &= "," & strDayId & "," 'If there are existing Period Ids
                                            ElseIf Not ViewState("DayId").ToString.Trim = "" AndAlso Not Session("DayId_SavedValue").ToString.Trim = "" Then
                                                ViewState("DayId") = Replace(ViewState("DayId"), Session("DayId_SavedValue"), strDayId)
                                            Else
                                                ViewState("DayId") &= strDayId & ","
                                            End If
                                        End If
                                        If STimeDescrip Is Nothing Then
                                            STimeDescrip = ""
                                        End If
                                        If ETimeDescrip = Nothing Then
                                            ETimeDescrip = ""
                                        End If
                                        If InStr(ViewState("STimeDescrip").ToString.Trim, STimeDescrip.ToString.Trim) <= 0 Then
                                            If Not ViewState("STimeDescrip").ToString.Trim = "" AndAlso Session("STimeDescrip_SavedValue") = "" Then
                                                ViewState("STimeDescrip") &= "," & STimeDescrip & "," 'If there are existing Period Ids
                                            ElseIf Not ViewState("STimeDescrip").ToString.Trim = "" AndAlso Not Session("STimeDescrip_SavedValue").ToString.Trim = "" Then
                                                ViewState("STimeDescrip") = Replace(ViewState("STimeDescrip"), Session("STimeDescrip_SavedValue"), STimeDescrip)
                                            Else
                                                ViewState("STimeDescrip") &= STimeDescrip & ","
                                            End If
                                        End If
                                        If InStr(ViewState("ETimeDescrip").ToString.Trim, ETimeDescrip.ToString.Trim) <= 0 Then
                                            If Not ViewState("ETimeDescrip").ToString.Trim = "" AndAlso Session("ETimeDescrip_SavedValue") = "" Then
                                                ViewState("ETimeDescrip") &= "," & ETimeDescrip & "," 'If there are existing Period Ids
                                            ElseIf Not ViewState("ETimeDescrip").ToString.Trim = "" AndAlso Not Session("ETimeDescrip_SavedValue").ToString.Trim = "" Then
                                                ViewState("ETimeDescrip") = Replace(ViewState("ETimeDescrip"), Session("ETimeDescrip_SavedValue"), ETimeDescrip)
                                            Else
                                                ViewState("ETimeDescrip") &= ETimeDescrip & ","
                                            End If
                                        End If
                                        If InStr(ViewState("InstructionTypeId").ToString.Trim, strInstructionTypeId.ToString.Trim) <= 0 Then
                                            If Not ViewState("InstructionTypeId").ToString.Trim = "" AndAlso Session("InstructionTypeId_SavedValue") = "" Then
                                                ViewState("InstructionTypeId") &= "," & strInstructionTypeId & "," 'If there are existing Instruction Type Ids
                                            ElseIf Not ViewState("InstructionTypeId").ToString.Trim = "" AndAlso Not Session("InstructionTypeId_SavedValue").ToString.Trim = "" Then
                                                ViewState("InstructionTypeId") = Replace(ViewState("InstructionTypeId"), Session("InstructionTypeId_SavedValue"), strInstructionTypeId)
                                            Else
                                                ViewState("InstructionTypeId") &= strInstructionTypeId & ","
                                            End If
                                        End If
                                    Else
                                        Dim strRoomId As String = ClsSectMeeting.Room.ToString
                                        If ClsSectMeeting.Room.ToString = Guid.Empty.ToString Or ClsSectMeeting.Room.ToString = "" Then
                                            strRoomId = Guid.Empty.ToString
                                        End If
                                        Dim strDayId As String = ClsSectMeeting.Day.ToString
                                        If ClsSectMeeting.Day.ToString = Guid.Empty.ToString Or ClsSectMeeting.Day.ToString = "" Then
                                            strDayId = Guid.Empty.ToString
                                        End If
                                        Dim STimeDescrip As String = ClsSectMeeting.STimeDescrip
                                        If ClsSectMeeting.STimeDescrip = Guid.Empty.ToString Or ClsSectMeeting.STimeDescrip = "" Then
                                            STimeDescrip = Guid.Empty.ToString
                                        End If
                                        Dim ETimeDescrip As String = ClsSectMeeting.ETimeDescrip
                                        If ClsSectMeeting.ETimeDescrip = Guid.Empty.ToString Or ClsSectMeeting.ETimeDescrip = "" Then
                                            ETimeDescrip = Guid.Empty.ToString
                                        End If
                                        Dim ClsMeetingId As String = ClsSectMeeting.ClsSectMtgId.ToString
                                        Dim strInstructionTypeId As String = ClsSectMeeting.InstructionTypeId.ToString
                                        Dim strPeriodId As String = ClsSectMeeting.PeriodId.ToString
                                        If ClsSectMeeting.PeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.PeriodId.ToString = "" Then
                                            strPeriodId = Guid.Empty.ToString
                                        End If
                                        Dim strAltPeriodId As String = ClsSectMeeting.AltPeriodId.ToString
                                        If ClsSectMeeting.AltPeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.AltPeriodId.ToString = "" Then
                                            strAltPeriodId = Guid.Empty.ToString
                                        End If
                                        'No collisions load the data in the table
                                        tbl.LoadDataRow(New Object() {ClsMeetingId, strDayId, strRoomId, STimeDescrip, _
                                                                      ClsSectMeeting.ClsSectId, ETimeDescrip, "sa", _
                                                                      Date.Now, strPeriodId, strAltPeriodId, _
                                                                      ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, strInstructionTypeId}, False)
                                    End If
                                    'msg = validation.TimeCollisions(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, _
                                    '                                ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, _
                                    '                                ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)


                                    'End If
                                Else
                                    Dim strRoomId As String = ClsSectMeeting.Room.ToString
                                    If ClsSectMeeting.Room.ToString = Guid.Empty.ToString Or ClsSectMeeting.Room.ToString = "" Then
                                        strRoomId = Guid.Empty.ToString
                                    End If
                                    Dim strDayId As String = ClsSectMeeting.Day.ToString
                                    If ClsSectMeeting.Day.ToString = Guid.Empty.ToString Or ClsSectMeeting.Day.ToString = "" Then
                                        strDayId = Guid.Empty.ToString
                                    End If
                                    Dim STimeDescrip As String = ClsSectMeeting.STimeDescrip
                                    If ClsSectMeeting.STimeDescrip = Guid.Empty.ToString Or ClsSectMeeting.STimeDescrip = "" Then
                                        STimeDescrip = Guid.Empty.ToString
                                    End If
                                    Dim ETimeDescrip As String = ClsSectMeeting.ETimeDescrip
                                    If ClsSectMeeting.ETimeDescrip = Guid.Empty.ToString Or ClsSectMeeting.ETimeDescrip = "" Then
                                        ETimeDescrip = Guid.Empty.ToString
                                    End If
                                    Dim ClsMeetingId As String = ClsSectMeeting.ClsSectMtgId.ToString
                                    Dim strInstructionTypeId As String = ClsSectMeeting.InstructionTypeId.ToString
                                    Dim strPeriodId As String = ClsSectMeeting.PeriodId.ToString
                                    If ClsSectMeeting.PeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.PeriodId.ToString = "" Then
                                        strPeriodId = Guid.Empty.ToString
                                    End If
                                    Dim strAltPeriodId As String = ClsSectMeeting.AltPeriodId.ToString
                                    If ClsSectMeeting.AltPeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.AltPeriodId.ToString = "" Then
                                        strAltPeriodId = Guid.Empty.ToString
                                    End If
                                    'No collisions load the data in the table
                                    tbl.LoadDataRow(New Object() {ClsMeetingId, strDayId, strRoomId, STimeDescrip, _
                                                                  ClsSectMeeting.ClsSectId, ETimeDescrip, "sa", _
                                                                  Date.Now, strPeriodId, strAltPeriodId, _
                                                                  ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, strInstructionTypeId}, False)

                                End If
                            End If
                        Next


                        If tbl.Rows.Count >= 1 Then
                            If Not tbl Is Nothing Then
                                Dim ds1 As New DataSet
                                For Each lcol As DataColumn In tbl.Columns
                                    lcol.ColumnMapping = System.Data.MappingType.Attribute
                                Next
                                ds1.Tables.Add(tbl.Copy())
                                Dim strXML As String = ds1.GetXml
                                Dim clsFacade As New UnscheduleClsFacade
                                clsFacade.SetupMeetings(strXML)
                                ds1 = Nothing
                                tbl.Clear()
                            End If
                        End If

                        If Not ViewState("ClsSectionId").ToString.Trim = "" Then
                            If InStrRev(ViewState("ClassMeetingStartDate"), ",") >= 1 Then
                                ViewState("ClassMeetingStartDate") = Mid(ViewState("ClassMeetingStartDate"), 1, InStrRev(ViewState("ClassMeetingStartDate"), ",") - 1)
                            End If
                            If InStrRev(ViewState("ClassMeetingEndDate"), ",") >= 1 Then
                                ViewState("ClassMeetingEndDate") = Mid(ViewState("ClassMeetingEndDate"), 1, InStrRev(ViewState("ClassMeetingEndDate"), ",") - 1)
                            End If
                            If InStrRev(ViewState("RoomId"), ",") >= 1 Then
                                ViewState("RoomId") = Mid(ViewState("RoomId"), 1, InStrRev(ViewState("RoomId"), ",") - 1)
                            End If
                            If InStrRev(ViewState("DayId"), ",") >= 1 Then
                                ViewState("DayId") = Mid(ViewState("DayId"), 1, InStrRev(ViewState("DayId"), ",") - 1)
                            End If
                            If InStrRev(ViewState("STimeDescrip"), ",") >= 1 Then
                                ViewState("STimeDescrip") = Mid(ViewState("STimeDescrip"), 1, InStrRev(ViewState("STimeDescrip"), ",") - 1)
                            End If
                            If InStrRev(ViewState("ETimeDescrip"), ",") >= 1 Then
                                ViewState("ETimeDescrip") = Mid(ViewState("ETimeDescrip"), 1, InStrRev(ViewState("ETimeDescrip"), ",") - 1)
                            End If
                            If InStrRev(ViewState("InstructionTypeId"), ",") >= 1 Then
                                ViewState("InstructionTypeId") = Mid(ViewState("InstructionTypeId"), 1, InStrRev(ViewState("InstructionTypeId"), ",") - 1)
                            End If
                        End If

                        If Not ViewState("ClsSectionId").ToString.Trim = "" Then
                            ShowScheduleConflict(ViewState("ClsSectionId"), "", ViewState("CourseDescrip"), _
                                         ViewState("ClassMeetingStartDate").ToString, _
                                         ViewState("ClassMeetingEndDate"), _
                                         ViewState("RoomId"), _
                                         ViewState("DayId"), _
                                         ViewState("STimeDescrip"), _
                                         ViewState("ETimeDescrip"), _
                                         True,
                                         ddlTerm2.SelectedValue.ToString) 'Show the radWindow
                            'ViewState("ClsSectionId") = ""
                            Exit Sub
                        End If
                        'Add the Class Section Meeting Details
                        'GuidArrList = Facade.AddClsSectMeeting(ClsSectMtgArrList, Session("UserName"), chkIgnoreConflicts.Checked, ClassSectionObject.ClsSectId, resInfo.ModDate)

                        'Check for the kind of object returned. If it's of 
                        '"ErrMsgInfo" obj type, then display this error msg else 
                        'call SetClsSectMtgIds to do the needful

                        'For Each obj In GuidArrList
                        '    If (obj.GetType Is GetType(ErrMsgInfo)) Then
                        '        Dim errmsgobj As New ErrMsgInfo

                        '        errmsgobj = DirectCast(obj, ErrMsgInfo)

                        '        ErrStr &= errmsgobj.ErrMsg

                        '    End If

                        'Next
                        'If ErrStr <> "" Then
                        '    'Display Error Msg.

                        '    DisplayErrorMessage(ErrStr)
                        'End If

                    End If
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Dim ss As String = ex.ToString
                End Try
            Next
            PopulateDataset(ddlTerm1.SelectedItem.Value.ToString, ddlTerm2.SelectedItem.Value.ToString, ViewState("CampusId"))
            If selected = False Then
                DisplayErrorMessage("Please select one or more class sections in order to copy to term")
                Exit Sub
            End If
            'PopulateToTerm(ViewState("CampusId"))

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub
    Protected Sub ShowScheduleConflict(ByVal ClsSectId As String, ByVal StuEnrollId As String, ByVal Classes As String, _
                                   ByVal MeetingStartDate As String, _
                                   ByVal MeetingEndDate As String, _
                                   ByVal RoomId As String, _
                                   ByVal DayId As String, _
                                   ByVal StartTimeId As String, _
                                   ByVal EndTimeId As String, _
                                   ByVal isSourceCopyClassSectionPage As Boolean, _
                                   ByVal TargetTermId As String, _
                                   Optional ByVal PeriodId As String = "", _
                                   Optional ByVal buttonState As String = "")
        Dim studentId As String = ""
        Dim strURL As String = "ViewScheduleConflicts.aspx?resid=909&mod=AR" + "&ClsSectionId=" + ClsSectId.ToString + "&StuEnrollId=" + StuEnrollId + "&Student=" + studentId + "&buttonState=" + buttonState + "&Classes=" + Classes _
                               + "&MeetingStartDate=" + MeetingStartDate + "&MeetingEndDate=" + MeetingEndDate + "&RoomId=" + RoomId + "&PeriodId=" + PeriodId _
                               + "&TermId=" + "" + "&CourseId=" + "" + "&InstructorId=" + "" + "&CampusId=" + campusId.ToString _
                               + "&DayId=" + DayId.ToString + "&StartTimeId=" + StartTimeId.ToString + "&EndTimeId=" + EndTimeId.ToString + "&isSourceCopyClassSectionPage=true" + "&TargetTermId=" + TargetTermId + "&SourceTermId=" + ddlTerm1.SelectedValue.ToString
        ScheduleConflictWindow.Visible = True
        ScheduleConflictWindow.Windows(0).NavigateUrl = strURL
        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = True
    End Sub
    'Private Function PopulateClassSectionObject() As ClassSectionInfo
    '    'Dim ClassSectionObject As New ClassSectionInfo
    '    'If ViewState("MODE") <> "NEW" Then
    '    '    ClassSectionObject.ClsSectId = XmlConvert.ToGuid(txtClsSectionId.Text)
    '    'Else
    '    '    ClassSectionObject.ClsSectId = System.Guid.NewGuid
    '    'End If
    '    'Dim a As String = ClassSectionObject.ClsSectId.ToString
    '    'ClassSectionObject.TermId = XmlConvert.ToGuid(ddlTermId.SelectedItem.Value)
    '    'ClassSectionObject.CourseId = XmlConvert.ToGuid(ddlCourseId.SelectedItem.Value)
    '    'ClassSectionObject.EmpId = XmlConvert.ToGuid(ddlInstructor1.SelectedItem.Value)
    '    'ClassSectionObject.Section = txtSection.Text
    '    'ClassSectionObject.StartDate = CDate(txtStartDate.Text)
    '    'ClassSectionObject.EndDate = CDate(txtEndDate.Text)
    '    'ClassSectionObject.MaxStud = (txtMaxStud.Text)

    '    'Return ClassSectionObject
    'End Function


    Private Sub ddlTerm2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTerm2.SelectedIndexChanged
        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
        Dim facade As New CopyClsSectFacade
        Dim ds As New DataSet
        Dim term As String
        'Dim SDate As String
        'Dim eDate As String
        Dim row As DataRow
        If ddlTerm2.SelectedItem.Text = "Select" Then
            DisplayErrorMessage("Unable to find data. Please select a Term")
            dgdCopyClsSect.Visible = False
            Exit Sub
        End If
        term = ddlTerm2.SelectedItem.Value.ToString
        PopulateDataset(ddlTerm1.SelectedItem.Value.ToString, ddlTerm2.SelectedItem.Value.ToString, ViewState("CampusId"))

        If (term <> "") Then
            ds = facade.GetSDateEDate(term)

            For Each row In ds.Tables(0).Rows
                ViewState("StartDate") = row("StartDate")
                ViewState("EndDate") = row("EndDate")
            Next
        End If
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    Private Sub PopulateDataset(ByVal term1 As String, ByVal term2 As String, ByVal campusid As String)
        Dim facade As New CopyClsSectFacade
        Dim ds As New DataSet
        ds = facade.PopulateDataGrid(term1, term2, campusid)
        dgdCopyClsSect.Visible = True
        With dgdCopyClsSect
            .DataSource = ds
            .DataBind()
        End With

    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

End Class
