<%@ Page Language="vb" AutoEventWireup="false" Inherits="ProgVerDef" CodeFile="ProgVerDef.aspx.vb" %>

<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register TagPrefix="uc1" TagName="DialogTemplates" Src="~/usercontrols/DialogTemplates.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>



<head>
    <title>Program Version Definitions</title>
    <link href="../css/systememail.css" type="text/css" rel="stylesheet">

    <script type="text/javascript">
        // Variables for MasterPageUserOptionsPanelBar
        var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL = '<%= Page.ResolveUrl("~")%>';
        var XMASTER_PAGE_USER_OPTIONS_USERNAME = "<%=Session("UserName") %>";
        var XMASTER_PAGE_USER_OPTIONS_USERID = '<%=Session("UserId")%>';
        var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID = '';

    </script>
    <%: Styles.Render("~/bundles/popupstyle") %>
    <%: Scripts.Render("~/bundles/popupscripts") %>
    <script src="../Kendo/js/jszip.min.js"></script>
    <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
    <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script type="text/javascript">
        var dataHasChange = false;
        $(document).ready(function () {
            $("#btnSave").on('click',
                function() {
                    sessionStorage.setItem('programVersionDefinitionFormHasChanged', false);
                });
            //set session storage key to indicate changes on inputs
            $("input").on("change", function () {
                sessionStorage.setItem('programVersionDefinitionFormHasChanged', true);
            });


            $("#percentageTable").on('change',
                ".courseWeightTextBox",
                function() {
                    calculateTotal();
                });


            //set session storage key to indicate changes on rows added, or selected, and unselected course/groups
            $("#btnRequired, #btnElective, #btnAdd, #btnRemove, #btnAdd, #btnDown").on("click", function () {
                sessionStorage.setItem('programVersionDefinitionFormHasChanged', true);
            });
        });


        //calculate the total of the input percentages
        function calculateTotal() {
            var total = 0;

            $(".courseWeightTextBox").each(function () {
                if (!isNaN(this.value) && this.value.length != 0) {
                    total += parseFloat(this.value);
                }
            });

            if (total > 100) {
                total = 100.00;
            }

            $("#CourseWeightFooterValue").text(total.toFixed(2) + " %");
        }

        //close event function
        function CloseEvent(closeWindow, btnName) {
            showWindow('#confirmationTemplate', "Do you want to save your changes?", closeWindow, btnName);
        }

        //show window message
        function showWindow(template, message, closeWindow, btnName) {

            if (sessionStorage.getItem('programVersionDefinitionFormHasChanged') === "true") {
                var dfd = $.Deferred();
                var result = false;
                var win = $('<div id="popupWindow"></div>')
                    .appendTo("body")
                    .kendoWindow({
                        width: 400,
                        resizable: false,
                        title: false,
                        modal: true,
                        visible: false,
                        scrollable: false,
                        close: function (e) {
                            this.destroy();
                            dfd.resolve(result);
                        }
                    })
                    .data("kendoWindow");

                win.content($(template).html()).open().center();

                $('.popupMessage').html(message);

                $("#popupWindow .confirm_yes").val('Yes');
                $("#popupWindow .confirm_no").val('No');

                $("#popupWindow .confirm_no").click(() => {
                    win.close();
                    if (closeWindow) {
                        close();

                    }
                });

                $("#popupWindow .confirm_yes").click(() => {
                    result = true;
                    //if total values is 100% and course weight for gpa is enabled OR course weight is disabled
                    if (($("#CourseWeightFooterValue").text() === "100.00 %" && $("#CourseWeightForGPA").is(":checked")) || !$("#CourseWeightForGPA").is(":checked")) {
                        $("#btnSave").click();
                        sessionStorage.setItem('programVersionDefinitionFormHasChanged', false);
                    } else {
                        if ($("#CourseWeightForGPA").is(":checked") && $("#CourseWeightFooterValue").text() !== "100.00 %") {
                            alert("Total weight percentage is not equal to 100%. In order to save, total weight percentage must be 100%.");
                        }
                    }
                    win.close();
                    if (btnName) {
                        $("#" + btnName).click();
                    }
                    if (closeWindow) {
                        close();
                    }
                });

                return dfd.promise();
            } else {
                if (btnName) {
                    $("#" + btnName).click();
                }
                if (closeWindow) {
                    close();

                }
            }

        };
    </script>
</head>
<body runat="server">
    <uc1:DialogTemplates runat="server" ID="dialogTemplates" />
    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="scriptmanager1" runat="server"></asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="../images/advantage_program_def.jpg">
                </td>
                <td class="topemail">
                    <a class="close" onclick="CloseEvent(true)" href="#">X Close</a>
                </td>
            </tr>
        </table>
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
            <tr>
                <td class="DetailsFrameTop">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="MenuFrame" align="right">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel4">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                            Enabled="False" OnClientClick="CloseEvent(false, 'btnNewServer')"></asp:Button>
                                        <asp:Button ID="btnNewServer" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                            Enabled="True" Visible="False"></asp:Button>
                                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"></asp:Button>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table5">
                        <tr>
                            <td class="detailsframe">
                                <div style="width: 90%; padding: 25px;">
                                    <!-- begin table content-->
                                    <table width="50%" cellpadding="0" align="center" cellspacing="0" class="contenttable">
                                        <tr>
                                            <td class="cellprogverdef">
                                                <asp:Label ID="lblPrgVer" CssClass="Label" runat="server">Program Version</asp:Label>
                                            </td>
                                            <td class="cellprogverdef2">
                                                <asp:TextBox ID="txtProgVerDescrip" runat="server" ReadOnly="True" CssClass="TextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="cellprogverdef">
                                                <asp:Label ID="lblCredits" CssClass="Label" runat="server">Credits</asp:Label>
                                            </td>
                                            <td class="cellprogverdef2">
                                                <asp:TextBox ID="txtCredits" runat="server" ReadOnly="True" CssClass="TextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="cellprogverdef">
                                                <asp:Label ID="lblHours" CssClass="Label" runat="server">Hours</asp:Label>
                                            </td>
                                            <td class="cellprogverdef2">
                                                <asp:TextBox ID="txtHours" runat="server" ReadOnly="True" CssClass="TextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="cellprogverdef">
                                                <asp:Label ID="lblMinGrdReq" CssClass="Label" runat="server">Min. Grade Required</asp:Label>
                                            </td>
                                            <td class="cellprogverdef2">
                                                <asp:TextBox ID="txtMinGrdReq" runat="server" ReadOnly="True" CssClass="TextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="PnlHrsCrdts" runat="server" CssClass="Frame" Width="100%">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="50%" align="center">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap>
                                                    <asp:Label ID="Label4" runat="server" CssClass="Label" Font-Bold="True">Selected</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table cellspacing="0" cellpadding="0" width="50%" align="center">
                                            <tr>
                                                <td class="cellprogverdef">
                                                    <asp:Label ID="lblCredits1" CssClass="Label" runat="server">Credits</asp:Label>
                                                </td>
                                                <td class="cellprogverdef2">
                                                    <asp:TextBox ID="txtCredits2" runat="server" CssClass="TextBox" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="cellprogverdef">
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label">Hours</asp:Label>
                                                </td>
                                                <td class="cellprogverdef2">
                                                    <asp:TextBox ID="txtHours2" runat="server" CssClass="TextBox" ReadOnly="True"></asp:TextBox><br>
                                                    <br>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <table width="100%" cellpadding="0" cellspacing="0" class="contenttable">
                                        <%--headers--%>
                                        <tr>
                                            <td class="fivecolumnheader">
                                                <asp:Label ID="Label2" runat="server" CssClass="Label" Font-Bold="True">Available Courses</asp:Label>
                                            </td>
                                            <td class="fivecolumnspacer">&nbsp;
                                            </td>
                                            <td class="fivecolumnheader" runat="server" ID="tdLabelAvailableCourse">
                                                <asp:Label ID="lblAvailableCourses" runat="server" CssClass="Label" Font-Bold="True">Available Course Groups</asp:Label>
                                            </td>
                                            <td class="fivecolumnspacer">&nbsp;
                                            </td>
                                            <td class="fivecolumnheader">
                                                <asp:Label ID="lblSelected" runat="server" CssClass="Label" Font-Bold="True">Selected</asp:Label>
                                            </td>
                                        </tr>
                                        <%--Grids--%>
                                        <tr>
                                            <td class="fivecolumncontent">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel7">
                                                    <ContentTemplate>
                                                        <asp:ListBox ID="lbxAvailCourses" runat="server" CssClass="ToStudentskillsprog" Rows="12"
                                                            SelectionMode="Multiple"></asp:ListBox>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            
                                            <td class="fivecolumnspacer">&nbsp;
                                            </td>
                                            <td class="fivecolumncontent" runat="server" ID="tdListboxAvailableCourse">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel6">
                                                    <ContentTemplate>
                                                        <asp:ListBox ID="lbxAvailCourseGrps" runat="server" CssClass="ToStudentskillsprog"
                                                            Rows="12" SelectionMode="Multiple"></asp:ListBox>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            
                                            <td class="fivecolumnspacer">&nbsp;
                                            </td>
                                            <td class="fivecolumncontent">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel5">
                                                    <ContentTemplate>
                                                        <asp:ListBox ID="lbxSelected" runat="server" CssClass="ToStudentskillsprog" Rows="12"
                                                            SelectionMode="Multiple" AutoPostBack="True"></asp:ListBox>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <%--Buttons--%>
                                        <tr>
                                            <td class="fivecolumnfooter">
                                                <asp:Button ID="btnRequired" runat="server" Width="88px" Text="Required"></asp:Button>
                                                <asp:Button ID="btnElective" runat="server" Text="Elective"></asp:Button>
                                            </td>
                                            
                                            <td class="fivecolumnspacer">&nbsp;
                                            </td>
                                            <td class="fivecolumnfooter" runat="server" ID="tdButtonsAvailableCourse">
                                                <asp:Button ID="btnAdd" runat="server" Width="59px" Text="Add"></asp:Button>
                                                <asp:Button ID="btnDetails" runat="server" Width="57px" Text="Details"></asp:Button>
                                            </td>
                                            
                                            <td class="fivecolumnspacer">&nbsp;
                                            </td>
                                            <td class="fivecolumnfooter">
                                                <asp:Button ID="btnRemove" runat="server" Text="Remove"></asp:Button>
                                                <asp:Button ID="btnUp" runat="server" Width="59px" Text="Up"></asp:Button>
                                                <asp:Button ID="btnDown" runat="server" Width="59px" Text="Down"></asp:Button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fivecolumnfooter" colspan="5">
                                                <asp:CheckBox ID="chkTrkForCompletion" CssClass="Label" runat="server" Visible="False"
                                                    Text="Track For Completion?"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fivecolumnfooter" colspan="1">
                                                <asp:Label ID="lblGrdOverride" CssClass="Label" runat="server">Override Min.Grd Required</asp:Label><br>
                                            </td>
                                            <td class="fivecolumnfooter" colspan="4">
                                                
                                                <asp:DropDownList ID="ddlGrdSysDetailId" runat="server" CssClass="DropDownListff">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:TextBox ID="txtProgVerId" CssClass="Label" Width="146px" Visible="false" MaxLength="128"
                                        runat="server"></asp:TextBox><asp:TextBox ID="txtProgVerDefId" CssClass="Label" Width="146px"
                                            Visible="false" MaxLength="128" runat="server"></asp:TextBox><asp:TextBox ID="txtRowIds"
                                                Style="visibility: hidden" runat="server" CssClass="Label" Width="10%"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="Label"
                                        Width="10%"></asp:TextBox>
                                    <!--end table content-->
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>

                                <div style="width: 90%; padding: 25px;">
                                    <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" id="percentageTable">
                                        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                                            <AjaxSettings>
                                                <telerik:AjaxSetting AjaxControlID="courseWeightTextBox">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="CourseWeightFooterLabel" LoadingPanelID="RadAjaxLoadingPanel1" />
                                                        <telerik:AjaxUpdatedControl ControlID="CourseWeightFooterValue" LoadingPanelID="RadAjaxLoadingPanel1" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                            </AjaxSettings>
                                        </telerik:RadAjaxManager>


                                        <tr>

                                            <td class="fivecolumnfooter">
                                                <asp:CheckBox ID="CourseWeightForGPA" runat="server" OnCheckedChanged="CourseWeightForGPA_OnCheckedChanged" AutoPostBack="True"></asp:CheckBox>
                                                <asp:Label ID="CourseWeightForGPALabel" CssClass="Label" runat="server">Are the courses weighted into overall GPA?</asp:Label>
                                            </td>
                                            <td class="fivecolumnfooter"></td>
                                        </tr>


                                        <tr class="detailsframe">
                                            <td colspan="2">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                                    <ContentTemplate>
                                                        <telerik:RadGrid ID="courseWeightTable" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                                                            HeaderStyle-HorizontalAlign="Center" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                                                            AllowPaging="False" AutoGenerateColumns="False" Width="100%" AllowMultiRowEdit="false"
                                                             Visible="False">
                                                            <MasterTableView Width="100%" CommandItemDisplay="top" DataKeyNames="CourseName"
                                                                AutoGenerateColumns="False" EditMode="InPlace" NoMasterRecordsText="No Instruction Type Hours to display." PageSize="10">
                                                                <CommandItemSettings ShowAddNewRecordButton="False" ShowRefreshButton="False" />
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="CourseName" UniqueName="CourseName" DataType="System.String" HeaderText="Course"></telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="CourseWeight" HeaderStyle-Width="40%" ItemStyle-Width="40%" HeaderText="Weight">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox CssClass="text-box courseWeightTextBox" data-definitionId='<%# Eval("ProgramVersionDefinitionId") %>' data-requirementId='<%# Eval("RequirementId") %>' ID="courseWeightTextBox" runat="server" Text='<%# Convert.ToDecimal(Eval("CourseWeight")).ToString("N") %>' OnTextChanged="CourseWeightTextBox_OnTextChanged" AutoPostBack="True"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                                <EditFormSettings>
                                                                </EditFormSettings>
                                                            </MasterTableView>
                                                            <ClientSettings>
                                                                <Selecting AllowRowSelect="False" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fivecolumnfooter">
                                                <telerik:RadAjaxPanel runat="server" ID="UpdatePanel2">
                                                    
                                                        <asp:Label ID="CourseWeightFooterLabel" CssClass="Label" Visible="False" runat="server">Total</asp:Label>
                                                    
                                                </telerik:RadAjaxPanel>
                                            </td>
                                            <td class="fivecolumnfooter">
                                                <telerik:RadAjaxPanel runat="server" ID="UpdatePanel3">
                                                    
                                                        <asp:Label ID="CourseWeightFooterValue" CssClass="Label" Visible="False" runat="server">Total Here</asp:Label>
                                                    
                                                </telerik:RadAjaxPanel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>


                </td>
                <td>
                    <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                    <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                    <asp:TextBox ID="txtMinGradeId" runat="server" Visible="False"></asp:TextBox>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
