Imports Fame.AdvantageV1.Common.AR
Imports Fame.Common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports Fame.Advantage.Common
Imports Fame.Advantage.Reporting

Partial Class PostServicesByStudent
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

#End Region

#Region "Variables"

    Protected StudentId, campusid As String
    Protected LeadId As String
    Protected resourceId As Integer
    Private _ordersExpandedState As Hashtable
    Private _selectedState As Hashtable


    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected clinicServicesScoresEnabled As Boolean = True
    Private pObj As New UserPagePermissionInfo



    Private ReadOnly Property SelectedStates() As Hashtable
        Get
            If Me._selectedState Is Nothing Then
                _selectedState = TryCast(Me.Session("_selectedState"), Hashtable)
                If _selectedState Is Nothing Then
                    _selectedState = New Hashtable()
                    Me.Session("_selectedState") = _selectedState
                End If
            End If

            Return Me._selectedState
        End Get
    End Property

    Private ReadOnly Property ExpandedStates() As Hashtable
        Get
            If Me._ordersExpandedState Is Nothing Then
                _ordersExpandedState = TryCast(Me.Session("_ordersExpandedState"), Hashtable)
                If _ordersExpandedState Is Nothing Then
                    _ordersExpandedState = New Hashtable()
                    Me.Session("_ordersExpandedState") = _ordersExpandedState
                End If
            End If

            Return Me._ordersExpandedState
        End Get
    End Property


#End Region

#Region "Page Events"

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString

        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(541) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId.ToString, campusid)
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'Set flag whether school has clinic service scores enabled
        SetClinicServicesScoresEnabled()

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            Me._ordersExpandedState = Nothing
            Me.Session("_ordersExpandedState") = Nothing
            Me._selectedState = Nothing
            Me.Session("_selectedState") = Nothing
            PopulateEnrollments()
            rgPosts.Rebind()

            If Master.IsSwitchedCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        End If
    End Sub

#End Region

#Region "Grid Events"

    Private Sub rgPosts_DetailTableDataBind(ByVal source As Object, ByVal e As GridDetailTableDataBindEventArgs) Handles rgPosts.DetailTableDataBind
        Dim dataItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)
        Select Case e.DetailTableView.Name
            Case "Class"
                Dim termID As String = dataItem.GetDataKeyValue("TermID").ToString()
                Dim dt As DataTable = (New ExamsFacade).GetClassroomWorkClass(rcbEnrollment.SelectedValue, termID)
                e.DetailTableView.DataSource = dt
            Case "LabWork"
                Dim clsSectionID As String = dataItem.GetDataKeyValue("ClsSectionID").ToString()
                Dim dt As DataTable = (New ExamsFacade).GetClassroomWorkServices(rcbEnrollment.SelectedValue, clsSectionID, SysGradeBookComponentTypes.LabWork)
                e.DetailTableView.DataSource = dt
            Case "LabHours"
                Dim clsSectionID As String = dataItem.GetDataKeyValue("ClsSectionID").ToString()
                Dim dt As DataTable = (New ExamsFacade).GetClassroomWorkServices(rcbEnrollment.SelectedValue, clsSectionID, SysGradeBookComponentTypes.LabHours)
                e.DetailTableView.DataSource = dt
            Case "LabWorkDetail"
                Dim clsSectionID As String = dataItem.GetDataKeyValue("ClsSectionID").ToString()
                Dim instrGrdBkWgtDetailId As String = dataItem.GetDataKeyValue("InstrGrdBkWgtDetailId").ToString()
                Dim dt As DataTable = (New ExamsFacade).GetClassroomWorkServicePosts(rcbEnrollment.SelectedValue, clsSectionID, instrGrdBkWgtDetailId)
                e.DetailTableView.DataSource = dt
            Case "LabHourDetail"
                Dim clsSectionID As String = dataItem.GetDataKeyValue("ClsSectionID").ToString()
                Dim instrGrdBkWgtDetailId As String = dataItem.GetDataKeyValue("InstrGrdBkWgtDetailId").ToString()
                Dim dt As DataTable = (New ExamsFacade).GetClassroomWorkServicePosts(rcbEnrollment.SelectedValue, clsSectionID, instrGrdBkWgtDetailId)
                e.DetailTableView.DataSource = dt
        End Select
    End Sub

    Protected Sub rgPosts_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles rgPosts.ItemCommand
        ''save the expanded/selected state in the session
        If e.CommandName = RadGrid.ExpandCollapseCommandName Then
            'Is the item about to be expanded or collapsed
            If Not e.Item.Expanded Then
                'Save its unique index among all the items in the hierarchy
                Me.ExpandedStates(e.Item.ItemIndexHierarchical) = True
            Else
                'collapsed
                Me.ExpandedStates.Remove(e.Item.ItemIndexHierarchical)
                Me.ClearSelectedChildren(e.Item.ItemIndexHierarchical)
                Me.ClearExpandedChildren(e.Item.ItemIndexHierarchical)
            End If
            'Is the item about to be selected 
        ElseIf e.CommandName = RadGrid.SelectCommandName Then
            'Save its unique index among all the items in the hierarchy
            Me.SelectedStates(e.Item.ItemIndexHierarchical) = True
            'Is the item about to be deselected 
        ElseIf e.CommandName = RadGrid.DeselectCommandName Then
            Me.SelectedStates.Remove(e.Item.ItemIndexHierarchical)
        End If
        If e.CommandName = "removeService" Then
            Dim dataItem As GridDataItem = e.Item
            Dim GrdBkResultId As String = dataItem.GetDataKeyValue("GrdBkResultId").ToString()
            Dim dt As DataTable = (New ExamsFacade).DeleteClassroomWorkServices(GrdBkResultId)
            rgPosts.Rebind()
        End If
    End Sub


    Protected Sub rgPosts_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles rgPosts.DataBound
        'Expand all items using our custom storage
        Dim indexes As String() = New String(Me.ExpandedStates.Keys.Count - 1) {}
        'If indexes.Length > 0 Then
        '    If Not indexes(0) Is Nothing Then

        Me.ExpandedStates.Keys.CopyTo(indexes, 0)

        Dim arr As New ArrayList(indexes)
        'Sort so we can guarantee that a parent item is expanded before any of 
        'its children
        arr.Sort()

        For Each key As String In arr
            Dim value As Boolean = CBool(Me.ExpandedStates(key))
            If value Then
                rgPosts.Items(key).Expanded = True
            End If
        Next

        'Select all items using our custom storage
        indexes = New String(Me.SelectedStates.Keys.Count - 1) {}
        Me.SelectedStates.Keys.CopyTo(indexes, 0)

        arr = New ArrayList(indexes)
        'Sort to ensure that a parent item is selected before any of its children
        arr.Sort()

        For Each key As String In arr
            Dim value As Boolean = CBool(Me.SelectedStates(key))
            If value Then
                rgPosts.Items(key).Selected = True
            End If
        Next
        '    End If
        'End If
    End Sub

    Protected Sub rgPosts_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rgPosts.NeedDataSource
        BindServicePostsGrid()
    End Sub

#End Region

#Region "Dropdown Events"

    Protected Sub rcbEnrollment_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles rcbEnrollment.SelectedIndexChanged
        Me.Session("_ordersExpandedState") = Nothing
        rgPosts.Rebind()
    End Sub

#End Region

#Region "Button Click Events"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim result As String = String.Empty
        Dim score As Nullable(Of Double) = 0
        Dim adjustment As Integer = 0
        Dim postedHours As New List(Of StdGrdBkInfo)
        Dim postDate As String = ""
        Dim examsFacade = New ExamsFacade
        For Each item As GridDataItem In rgPosts.MasterTableView.Items
            If item.Expanded Then

                For Each child As GridDataItem In item.ChildItem.NestedTableViews(0).Items
                    If child.Expanded Then
                        For Each grandChild As GridDataItem In child.ChildItem.NestedTableViews(0).Items
                            score = CType(grandChild.FindControl("txtScore"), RadNumericTextBox).Value

                            If Not clinicServicesScoresEnabled And Not CType(grandChild.FindControl("txtAdj"), RadNumericTextBox).Value Is Nothing Then
                                score = 100
                                CType(grandChild.FindControl("txtDate"), RadDatePicker).SelectedDate = Date.Parse(grandChild.GetDataKeyValue("EnrollDate").ToString()).AddDays(1)
                            End If

                            If Not score Is Nothing AndAlso score <> -1 Then
                                If Not CType(grandChild.FindControl("txtAdj"), RadNumericTextBox).Value Is Nothing Then
                                    adjustment = CType(grandChild.FindControl("txtAdj"), RadNumericTextBox).Value
                                End If
                                If CType(grandChild.FindControl("txtDate"), RadDatePicker).SelectedDate Is Nothing Then
                                    DisplayErrorMessage("Please enter a post date.")
                                    Exit Sub
                                Else

                                    Dim enrollDate = Date.Parse(grandChild.GetDataKeyValue("EnrollDate").ToString())

                                    If Date.Parse(CType(grandChild.FindControl("txtDate"), RadDatePicker).SelectedDate) > enrollDate Then
                                        postDate = Date.Parse(CType(grandChild.FindControl("txtDate"), RadDatePicker).SelectedDate)
                                    Else
                                        DisplayErrorMessage("The post date has to be greater than the student enrollment date.")
                                        Exit Sub
                                    End If

                                End If
                                If (adjustment > 0) Then
                                    Dim post As New StdGrdBkInfo
                                    post.GrdBkWgtDetailId = grandChild.GetDataKeyValue("InstrGrdBkWgtDetailId").ToString()
                                    post.ClassId = grandChild.GetDataKeyValue("ClsSectionID").ToString()
                                    post.Score = score
                                    post.Comments = CType(grandChild.FindControl("txtComment"), RadTextBox).Text
                                    post.StuEnrollId = rcbEnrollment.SelectedValue
                                    post.IsCompGraded = True
                                    post.PostDate = postDate
                                    examsFacade.PostServiceBatch(post, Session("UserName").ToString(), adjustment)
                                End If
                            End If
                        Next
                        For Each grandChild As GridDataItem In child.ChildItem.NestedTableViews(1).Items

                            If Not CType(grandChild.FindControl("txtAdj"), RadNumericTextBox).Value Is Nothing Then

                                If Not CType(grandChild.FindControl("txtAdj"), RadNumericTextBox).Value Is Nothing Then
                                    adjustment = CType(grandChild.FindControl("txtAdj"), RadNumericTextBox).Value
                                End If

                                If Not clinicServicesScoresEnabled Then
                                    CType(grandChild.FindControl("txtDate"), RadDatePicker).SelectedDate = Date.Parse(grandChild.GetDataKeyValue("EnrollDate").ToString()).AddDays(1)
                                End If

                                If CType(grandChild.FindControl("txtDate"), RadDatePicker).SelectedDate Is Nothing Then
                                    DisplayErrorMessage("Please enter a post date.")
                                    Exit Sub
                                Else

                                    Dim enrollDate = Date.Parse(grandChild.GetDataKeyValue("EnrollDate").ToString())

                                    If Date.Parse(CType(grandChild.FindControl("txtDate"), RadDatePicker).SelectedDate) > enrollDate Then
                                        postDate = Date.Parse(CType(grandChild.FindControl("txtDate"), RadDatePicker).SelectedDate)
                                    Else
                                        DisplayErrorMessage("The post date has to be greater than the student enrollment date.")
                                        Exit Sub
                                    End If

                                End If

                                Dim post As New StdGrdBkInfo
                                post.GrdBkWgtDetailId = grandChild.GetDataKeyValue("InstrGrdBkWgtDetailId").ToString()
                                post.ClassId = grandChild.GetDataKeyValue("ClsSectionID").ToString()
                                post.Score = adjustment
                                post.Comments = CType(grandChild.FindControl("txtComment"), RadTextBox).Text
                                post.StuEnrollId = rcbEnrollment.SelectedValue
                                post.IsCompGraded = True
                                post.PostDate = postDate
                                postedHours.Add((post))
                            End If

                        Next
                    End If
                Next
            End If
        Next
        If postedHours.Count > 0 Then
            result = examsFacade.PostServices(postedHours, Session("UserName").ToString())
            If Not result = "" Then
                DisplayErrorMessage(result)
                Exit Sub
            Else
            End If
        End If

        rgPosts.Rebind()
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToPdf.Click
        Dim getReportAsBytes As [Byte]()
        Dim intTabId As Integer = 0

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim intSchoolOptions As Integer = CommonWebUtilities.SchoolSelectedOptions(myAdvAppSettings.AppSettings("SchedulingMethod"))
        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") '"/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
        getReportAsBytes = (New Logic.PostServicesReportGenerator).RenderReport("pdf", Session("User"), rcbEnrollment.SelectedValue,
                                                                                  "", "",
                                                                                  "", "",
                                                                                  "",
                                                                                  strReportPath,
                                                                                  0,
                                                                                  0,
                                                                                  AdvantageSession.UserState.CampusId.ToString, intSchoolOptions)
        ExportReport("pdf", getReportAsBytes)
    End Sub

#End Region

#Region "Private Functions"

    Private Sub PopulateEnrollments()
        rcbEnrollment.ClearSelection()
        rcbEnrollment.Items.Clear()

        Dim ds As New DataSet
        ds = (New StudentsAccountsFacade).GetAllEnrollmentsPerStudent(StudentId)
        With rcbEnrollment
            .DataSource = ds
            .DataBind()
        End With
    End Sub

    Private Sub SetClinicServicesScoresEnabled()

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim value As String = myAdvAppSettings.AppSettings("ClinicServicesScoresEnabled")

        If value IsNot Nothing AndAlso value.ToLower() = "no" Then
            clinicServicesScoresEnabled = False
        End If
    End Sub

    Private Sub BindServicePostsGrid()
        If Not rcbEnrollment.IsEmpty Then
            Dim dt As DataTable = (New ExamsFacade).GetClassroomWorkTerm(rcbEnrollment.SelectedValue)
            rgPosts.DataSource = dt
        Else
            rgPosts.DataSource = String.Empty
        End If
    End Sub

    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub ClearExpandedChildren(ByVal parentHierarchicalIndex As String)
        'Clear the state for all expanded children if a parent item is collapsed
        Dim indexes As String() = New String(Me.ExpandedStates.Keys.Count - 1) {}
        Me.ExpandedStates.Keys.CopyTo(indexes, 0)
        For Each index As String In indexes
            'all indexes of child items
            If index.StartsWith(parentHierarchicalIndex + "_") OrElse index.StartsWith(parentHierarchicalIndex + ":") Then
                Me.ExpandedStates.Remove(index)
            End If
        Next
    End Sub

    Private Sub ClearSelectedChildren(ByVal parentHierarchicalIndex As String)
        Dim indexes As String() = New String(Me.SelectedStates.Keys.Count - 1) {}
        Me.SelectedStates.Keys.CopyTo(indexes, 0)
        For Each index As String In indexes
            'all indexes of child items
            If index.StartsWith(parentHierarchicalIndex + "_") OrElse index.StartsWith(parentHierarchicalIndex + ":") Then
                Me.SelectedStates.Remove(index)
            End If
        Next
    End Sub

    Private Sub ExportReport(ByVal ExportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case ExportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script As String = String.Format("window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub

#End Region

End Class
