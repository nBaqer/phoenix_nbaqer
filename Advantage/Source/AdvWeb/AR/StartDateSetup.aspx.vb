
Imports System.Xml
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Data

Partial Class StartDateSetup
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Dim studentCount As Integer = 0
    Dim campusId As String
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Dim objCommon As New FAME.common.CommonUtilities

        Dim userId As String
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Not IsPostBack Then
            objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"

            If Request.QueryString("ProgId") <> "" Then
                ViewState("PrgId") = Request.QueryString("ProgId")
                txtProgId.Text = ViewState("PrgId")
            End If
            If Request.QueryString("Program") <> "" Then
                ViewState("Program") = Server.UrlDecode(Request.QueryString("Program"))
                txtProgDescrip.Text = ViewState("Program")
            End If

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList()

            '   bind an empty new StartDateInfo
            BindStartDateData(New StartDateInfo)

            '   initialize buttons
            InitButtonsForLoad()
            lnkClsStartDate.Enabled = False
        Else
            lnkClsStartDate.Enabled = True
            objCommon.PageSetup(Form1, "EDIT")

        End If

    End Sub
    Public Sub GetCampusGroupsByProgram(ByVal PrgVerId As String)
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = (New ProgVerFacade).GetAllCampusGroupsByProgram(txtProgId.Text, PrgVerId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindDataList()

        '   create row filter and sort expression
        Dim rowFilter As String = ""
        Dim sortExpression As String

        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        'filter by ProgramId
        If txtProgId.Text <> "" Then
            If rowFilter <> "" Then
                rowFilter &= " AND "
            End If
            rowFilter &= "ProgId='" & txtProgId.Text & "'"
        End If

        '   bind StartDates datalist
        dlstSDateSetup.DataSource = New DataView((New StartDatesFacade).GetAllStartDates().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstSDateSetup.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        GetCampusGroupsByProgram("")
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    'Private Sub BuildCampusGroupsDDL()
    '    '   bind the CampusGroups DDL
    '    Dim campusGroups As New CampusGroupsFacade

    '    With ddlCampGrpId
    '        .DataTextField = "CampGrpDescrip"
    '        .DataValueField = "CampGrpId"
    '        .DataSource = campusGroups.GetAllCampusGroups()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With

    'End Sub
    Private Sub dlstSDateSetup_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstSDateSetup.ItemCommand

        '   get the StartDateId from the backend and display it
        ViewState("MODE") = "EDIT"
        GetStartDateId(e.CommandArgument)

        If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) = "ModuleStart" Then
            studentCount = (New StartDatesFacade).GetStudentCount(e.CommandArgument)
            If Session("UserName") = "sa" Then
                btnSave.Enabled = True
            Else
                If (studentCount > 0) Then
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            End If
        End If


        lnkClsStartDate.Enabled = True
        Session("OldCampusGroup") = ddlCampGrpId.SelectedItem.Text


        '   set Style to Selected Item
        CommonWebUtilities.SetStyleToSelectedItem(dlstSDateSetup, e.CommandArgument, ViewState)

        '   initialize buttons
        InitButtonsForEdit()
        txtOldCampGrpId.Text = ddlCampGrpId.SelectedValue

    End Sub
    Private Sub BindStartDateData(ByVal StartDates As StartDateInfo)
        With StartDates
            chkIsInDB.Checked = .IsInDB
            txtSDateSetupId.Text = .StartDateId
            txtSDateSetupCode.Text = .Code
            If Not (StartDates.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = StartDates.StatusId
            txtSDateSetupDescrip.Text = .Description
            If Not (StartDates.ShiftId = Guid.Empty.ToString) Then ddlShiftId.SelectedValue = StartDates.ShiftId Else ddlShiftId.SelectedIndex = 0
            If (StartDates.CampGrpId IsNot Nothing Or StartDates.ShiftId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = StartDates.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            If .MidPtDate <> "" Then
                txtMidPtDate.SelectedDate = .MidPtDate
            Else
                txtMidPtDate.Clear()
            End If

            Try
                txtSDate.SelectedDate = .StartDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtSDate.Clear()
            End Try
            Try
                txtEndDate.SelectedDate = .EndDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtEndDate.Clear()
            End Try
            Try
                txtMaxGradDate.SelectedDate = .AllowedMaxGradDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtMaxGradDate.Clear()
            End Try

            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Function ValidateEndDate(ByVal termId As String, ByVal endDate As String) As String
        Dim rtn As String = String.Empty

        Try
            If CDate(endDate) < CDate(txtSDate.SelectedDate) Then
                rtn = "The end date must be greater than the start date"
            Else
                rtn = (New StartDateFacade).ValidateEndDate(termId)
                If rtn <> "" Then
                    If (CDate(rtn) <= endDate) Then
                        rtn = ""
                    Else
                        rtn = "The end date must be greater than or equal to the end date of the last class which is " + CDate(rtn).Date

                    End If
                End If
            End If
        Catch
            rtn = "Please Check the End Date"
        End Try
        Return rtn


    End Function

    Private Function shiftText(ByVal shiftValue As String) As String
        Dim count As Integer = ddlShiftId.Items.Count
        Dim i As Integer
        For i = 0 To count - 1
            If (ddlShiftId.Items(i).Value = shiftValue) Then
                Return ddlShiftId.Items(i).Text
            End If
        Next
        Return ""
    End Function
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim fac As New StartDatesFacade
        Dim errorMessage As String = String.Empty
        If (ViewState("MODE") = "EDIT") Then
            If (dlstSDateSetup.Items.Count > 1) Then
                Dim StartDatesShift As String = fac.GetStartDatesShiftForProgram(txtProgId.Text)
                If (StartDatesShift = "") Then
                    DisplayErrorMessage("All starts must have the same shift.")
                    Exit Sub
                ElseIf (StartDatesShift <> ddlShiftId.SelectedValue.ToString) Then

                    DisplayErrorMessage("There are existing starts with " & shiftText(StartDatesShift) & " shift." & vbLf & "All starts must have the same shift.")
                    Exit Sub
                End If
            End If
        ElseIf ViewState("MODE") = "NEW" Then
            If (dlstSDateSetup.Items.Count > 0) Then
                Dim StartDatesShift As String = fac.GetStartDatesShiftForProgram(txtProgId.Text)
                If (StartDatesShift = "") Then
                    DisplayErrorMessage("There is a mismatch in the shift")
                    Exit Sub
                ElseIf (StartDatesShift <> ddlShiftId.SelectedValue.ToString) Then
                    DisplayErrorMessage("There is a mismatch in the shift")
                    Exit Sub
                End If
            End If
        End If

        errorMessage = ValidateEndDate(txtSDateSetupId.Text, txtEndDate.SelectedDate)


        '   instantiate component
        If (errorMessage = "") Then
            If (CDate(txtMaxGradDate.SelectedDate) <= CDate(txtEndDate.SelectedDate)) Then
                DisplayErrorMessage("Please Check the Allowed Max Grad Date")
            Else
                If ViewState("MODE") = "EDIT" Then
                    If Trim(txtOldCampGrpId.Text) <> Trim(ddlCampGrpId.SelectedValue) Then
                        Dim winSettings As String = "toolbar=no,status=no,width=650px,height=400px,left=250px,top=200px,modal=yes"
                        Dim name As String = "AdmReqSummary1"
                        Dim intClsSectionExists As Integer = (New TermProgressFacade).IsThereAnyClsSectionForTerm(txtSDateSetupId.Text, txtOldCampGrpId.Text, ddlCampGrpId.SelectedValue)
                        If intClsSectionExists >= 1 Then
                            'Dim URL As String = "DisplayClassSections.aspx?TermId=" + txtTermId.Text + "&CampGrpId=" + txtOldCampGrpId.Text + "&Term=" + txtTermDescrip.Text + "&NewCampus=" + ddlCampGrpId.SelectedItem.Text + "&OldCampusGroup=" + Session("OldCampusGroup") + "&NewCampGrpId=" + ddlCampGrpId.SelectedValue
                            'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                            Exit Sub
                        End If
                    End If
                End If

                Dim result As String
                With New StartDatesFacade
                    '   update StartDateInfo Info 
                    result = .UpdateStartDateInfo(BuildStartDateInfo(txtSDateSetupId.Text), Context.User.Identity.Name)
                End With


                '   bind datalist
                BindDataList()

                '   set Style to Selected Item
                CommonWebUtilities.SetStyleToSelectedItem(dlstSDateSetup, txtSDateSetupId.Text, ViewState)

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    '   get the StartDateId from the backend and display it
                    ViewState("MODE") = "EDIT"
                    GetStartDateId(txtSDateSetupId.Text)

                    lnkClsStartDate.Enabled = True
                End If

                '   if there are no errors bind a new entity and init buttons
                If Page.IsValid Then

                    '   set the property IsInDB to true in order to avoid an error if the user
                    '   hits "save" twice after adding a record.
                    chkIsInDB.Checked = True

                    'note: in order to display a new page after "save".. uncomment next lines
                    '   bind an empty new StartDateInfo
                    'BindStartDateData(New StartDateInfo)

                    '   initialize buttons
                    'InitButtonsForLoad()
                    InitButtonsForEdit()

                End If
            End If
        Else
            DisplayErrorMessage(errorMessage)
        End If
    End Sub
    Private Function BuildStartDateInfo(ByVal StartDateId As String) As StartDateInfo
        'instantiate class
        Dim StartDatesInfo As New StartDateInfo

        With StartDatesInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get StartDateId
            .StartDateId = StartDateId

            'get Code
            .Code = txtSDateSetupCode.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get StartDate's Description
            .Description = txtSDateSetupDescrip.Text.Trim


            .ShiftId = ddlShiftId.SelectedValue

            If txtMidPtDate.SelectedDate Is Nothing Then
                .MidPtDate = ""
            Else
                .MidPtDate = Date.Parse(txtMidPtDate.SelectedDate)
            End If

            .StartDate = Date.Parse(txtSDate.SelectedDate)

            If txtEndDate.SelectedDate Is Nothing Then
                .EndDate = ""
            Else
                .EndDate = Date.Parse(txtEndDate.SelectedDate)
            End If

            .AllowedMaxGradDate = Date.Parse(txtMaxGradDate.SelectedDate)

            .ProgId = txtProgId.Text

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)
            .CampGrpId = ddlCampGrpId.SelectedValue

        End With
        'return data
        Return StartDatesInfo
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        ViewState("MODE") = "NEW"
        txtProgId.Text = ViewState("PrgId")
        txtProgDescrip.Text = ViewState("Program")

        '   bind an empty new StartDateInfo
        BindStartDateData(New StartDateInfo)

        lnkClsStartDate.Enabled = False

        'Reset Style in the Datalist
        CommonWebUtilities.SetStyleToSelectedItem(dlstSDateSetup, Guid.Empty.ToString, ViewState)

        'initialize buttons
        InitButtonsForLoad()
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtSDateSetupId.Text = Guid.Empty.ToString) Then

            'update StartDate Info 
            Dim result As String = (New StartDatesFacade).DeleteStartDateInfo(txtSDateSetupId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new StartDateInfo
                BindStartDateData(New StartDateInfo)

                '   initialize buttons
                InitButtonsForLoad()
                ViewState("MODE") = "NEW"
            End If

        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If
        If Session("UserName") = "sa" Then
            btnSave.Enabled = True
        Else
            If (studentCount > 0) Then
                btnSave.Enabled = False
            Else
                btnSave.Enabled = True
            End If
        End If
    End Sub
    Private Sub GetStartDateId(ByVal StartDateId As String)

        '   bind StartDate properties
        BindStartDateData((New StartDatesFacade).GetStartDateInfo(StartDateId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList()
    End Sub

    Protected Sub lnkClsStartDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClsStartDate.Click
        Dim sb As New StringBuilder
        Dim campusId As String

       campusid = CampusObjects.GetQueryParameterValueStrCampusId(Request)  

        '   append TermId to the querystring
        sb.Append("&TermId=" + txtSDateSetupId.Text)
        '   append Term Description to the querystring
        sb.Append("&Term=" + Server.UrlEncode(txtSDateSetupDescrip.Text))
        '   append ProgramId to the querystring
        sb.Append("&ProgramId=" + txtProgId.Text)
        '   append Program description to the querystring
        sb.Append("&Program=" + Server.UrlEncode(txtProgDescrip.Text))
        ' append the shift 
        sb.Append("&shift=" + Server.UrlEncode(ddlShiftId.SelectedValue))



        '   setup the properties of the new window
        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsFull
        Dim name As String = "StartDateClasses"
        Dim url As String = "../AR/" + name + ".aspx?resid=482&mod=AR&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
