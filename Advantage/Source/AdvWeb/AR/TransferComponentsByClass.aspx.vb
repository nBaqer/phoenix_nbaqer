﻿Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports Advantage.Business.Objects
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Partial Class TransferComponentsByClass
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


#End Region

#Region "Variables"
    Protected ResourceId As Integer
    Protected campusId, userId As String
    Protected ModuleId As String
    Dim pobj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings
    Public arList As New ArrayList()
#End Region

#Region "Page Events"

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SetVariables()
        If Not Page.IsPostBack Then
            PopulateTerms()
            rgStudents.Rebind()
            SetSelectedRowsSession()
        End If
        SetSelectedRowsSession()
    End Sub

#End Region

#Region "Grid Events"

    Protected Sub rgStudents_DataBound(sender As Object, e As EventArgs) Handles rgStudents.DataBound
        CheckSelectedRows()
        SetSelectedRowsSession()
    End Sub

    Protected Sub rgStudents_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgStudents.ItemCommand
        If e.CommandName = "Filter" Then
            ClearSelections()
        End If
        SetSelectedRowsSession()
    End Sub

    Protected Sub rgStudents_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles rgStudents.NeedDataSource
        If Not e.IsFromDetailTable Then
            BindStudentGrid()
        End If
    End Sub

    Protected Sub rgStudents_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles rgStudents.ItemDataBound

        If TypeOf e.Item Is GridDataItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            If item.OwnerTableView.Name = "Students" Then
                If item("ValidStudent").Text = "0" Then
                    item.SelectableMode = GridItemSelectableMode.None
                    item.ForeColor = Drawing.Color.Gray
                Else
                    item.SelectableMode = GridItemSelectableMode.ServerAndClientSide
                    item.ForeColor = Drawing.Color.Black
                End If
            End If
        End If

        If TypeOf e.Item Is GridHeaderItem Then
            Dim headeritem As GridHeaderItem = DirectCast(e.Item, GridHeaderItem)
            If headeritem.OwnerTableView.Name = "Students" Then
                Dim check As CheckBox = DirectCast(headeritem("CheckboxSelectColumn").Controls(0), CheckBox)
                check.AutoPostBack = True
            End If
        End If

    End Sub

    Protected Sub rgStudents_DetailTableDataBind(sender As Object, e As GridDetailTableDataBindEventArgs) Handles rgStudents.DetailTableDataBind
        Dim item As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)

        Select Case e.DetailTableView.Name
            Case "CompletedComponents"
                Dim ResultId As String = item.GetDataKeyValue("ResultId").ToString()
                Dim dsCompletedComponents As New DataSet
                Dim facade As New TransferComponentsFacade
                dsCompletedComponents = facade.GetCompletedComponents(ResultId)
                If dsCompletedComponents.Tables.Count > 0 Then
                    e.DetailTableView.DataSource = dsCompletedComponents.Tables(0)
                End If
        End Select
    End Sub

#End Region

#Region "Dropdown Events"

    Protected Sub rcbTerm_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles rcbTerm.SelectedIndexChanged
        If rcbTerm.SelectedIndex > 0 Then
            PopulateCourses(rcbTerm.SelectedValue)
        Else
            PopulateCourses()
        End If

        rgStudents.Rebind()

        SetSelectedRowsSession()
    End Sub

    Protected Sub rcbCourse_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles rcbCourse.SelectedIndexChanged
        If rcbCourse.SelectedIndex > 0 Then
            PopulateAvailableClasses(rcbCourse.SelectedValue)
        Else
            PopulateAvailableClasses()
        End If

        rgStudents.Rebind()
        SetSelectedRowsSession()
    End Sub

    Protected Sub rcbAvailableClasses_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles rcbAvailableClasses.SelectedIndexChanged
        rgStudents.Rebind()
        SetSelectedRowsSession()
    End Sub

#End Region

#Region "Button Click Events"

    Protected Sub btnTransferComponents_Click(sender As Object, e As EventArgs) Handles btnTransferComponents.Click
        Dim RegistrationProcessFacade As New RegistrationProcessFacade
        Dim StudentCount As Integer = GetStudentCount()
        If StudentCount < 1 Then
            DisplayRADAlert(CallbackType.Postback, "Error1", "No students are available to transfer.", "Unable to Transfer")
        Else
            Dim StudentRegistrationList As List(Of StudentRegistration.Lib.Student) = GetStudentRegistrationList()
            If StudentRegistrationList.Count < 1 Then
                DisplayRADAlert(CallbackType.Postback, "Error2", "Please select a student to transfer.", "Unable to Transfer")
            Else
                RegistrationProcessFacade.RegisterStudentIncompleteCourses(StudentRegistrationList, AdvantageSession.UserState.UserName)
                DisplayRADAlert(CallbackType.Postback, "Complete", StudentRegistrationList.Count & " transfer" & IIf(StudentRegistrationList.Count <> 1, "s", "") & " completed.", "Transfer Complete")
                rgStudents.Rebind()
            End If
        End If
    End Sub

#End Region

#Region "Private Functions"

    Private Sub BindStudentGrid()
        Dim intConfigConsolidate As String = MyAdvAppSettings.AppSettings("ConsolidateCourseComponents")
        Dim facade As New TransferComponentsFacade
        Dim dsStudents As New DataSet

        If intConfigConsolidate.ToString.ToLower() = "true" AndAlso rcbCourse.SelectedIndex > 0 Then
            dsStudents = facade.GetStudentsWithPartiallyCompletedCourses(rcbCourse.SelectedValue, rcbAvailableClasses.SelectedValue, rcbTerm.SelectedValue)
            If dsStudents.Tables(0).Rows.Count > 0 Then

                rgStudents.DataSource = dsStudents.Tables(0)
            Else
                rgStudents.DataSource = String.Empty
            End If
        Else
            rgStudents.DataSource = String.Empty
        End If
    End Sub

    Private Function GetStudentCount() As Integer
        Dim count As Integer = 0
        For Each item As GridDataItem In rgStudents.Items
            If item.OwnerTableView.Name = "Students" Then
                count += 1
            End If
        Next
        Return count
    End Function

    Private Function GetStudentRegistrationList() As List(Of StudentRegistration.Lib.Student)
        Dim StudentRegistrationList As New List(Of StudentRegistration.Lib.Student)
        If rcbAvailableClasses.SelectedIndex > 0 Then
            Dim newClassId As String = rcbAvailableClasses.SelectedValue
            For Each item As GridDataItem In rgStudents.Items
                If item.OwnerTableView.Name = "Students" Then
                    If item.Selected Then
                        Dim oStudentRegistration As New StudentRegistration.Lib.Student
                        Dim ClassList As New List(Of StudentRegistration.Lib.Class)
                        Dim oClass As New StudentRegistration.Lib.Class
                        oClass.oldClassId = item.Item("ClsSectionId").Text
                        oClass.newClassId = newClassId
                        ClassList.Add(oClass)
                        oStudentRegistration.StudentEnrollmentId = item.Item("StuEnrollId").Text
                        oStudentRegistration.Classes = ClassList
                        StudentRegistrationList.Add(oStudentRegistration)
                    End If

                End If
            Next
        End If

        Return StudentRegistrationList
    End Function

    Private Sub PopulateTerms()
        rcbTerm.ClearSelection()
        rcbTerm.Items.Clear()
        rcbCourse.ClearSelection()
        rcbCourse.Items.Clear()
        rcbAvailableClasses.ClearSelection()
        rcbAvailableClasses.Items.Clear()
        Session.Remove("SelectedRows")
        Session.Remove("AvailableClasses")

        Dim dt As New DataTable
        dt = (New TermsFacade).GetAllTerms_SP(True, campusId)
        With rcbTerm
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, New RadComboBoxItem("Select"))
        End With
    End Sub

    Private Sub PopulateCourses(Optional ByVal TermId As String = "00000000-0000-0000-0000-000000000000")
        rcbCourse.ClearSelection()
        rcbCourse.Items.Clear()
        rcbAvailableClasses.ClearSelection()
        rcbAvailableClasses.Items.Clear()
        Session.Remove("SelectedRows")
        Session.Remove("AvailableClasses")

        If TermId <> "00000000-0000-0000-0000-000000000000" Then
            Dim dt As New DataTable
            dt = (New CoursesFacade).GetCoursesFromClassSectionsForTerm_SP(TermId)
            With rcbCourse
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New RadComboBoxItem("Select"))
            End With
        End If


    End Sub

    Private Sub PopulateAvailableClasses(Optional ByVal ReqId As String = "00000000-0000-0000-0000-000000000000")
        rcbAvailableClasses.ClearSelection()
        rcbAvailableClasses.Items.Clear()
        Session.Remove("SelectedRows")
        Session.Remove("AvailableClasses")

        If ReqId <> "00000000-0000-0000-0000-000000000000" Then
            Dim dtClasses As New DataTable
            dtClasses = (New TransferComponentsFacade).GetAvailableClassesForPartialTransferByClass(ReqId, campusId)
            With rcbAvailableClasses
                .DataSource = dtClasses
                .DataBind()
                .Items.Insert(0, New RadComboBoxItem("Select"))
            End With
            Session("AvailableClasses") = dtClasses
        End If
    End Sub

    Protected Sub SetSelectedRowsSession()
        arList = New ArrayList
        arList = SessionToArrayList()

        For Each item As GridDataItem In rgStudents.MasterTableView.Items
            Dim ResultId As String = item.GetDataKeyValue("ResultId").ToString()

            If item.Selected = True Then
                If Not arList.Contains(ResultId) Then
                    arList.Add(ResultId)
                End If
            Else
                If arList.Contains(ResultId) Then
                    arList.Remove(ResultId)
                End If
            End If
        Next

        Session("SelectedRows") = arList

        If rcbAvailableClasses.SelectedIndex > 0 Then
            Dim ClassSectionRemaining As Integer = GetClassSectionRemaining(rcbAvailableClasses.SelectedValue)
            Dim Remaining As Integer = ClassSectionRemaining - arList.Count
            If Remaining < 0 Then
                lblMessage.Text = "Too many students selected"
                EnableTransfer(False)
            Else
                lblMessage.Text = Remaining & " Remaining Seat" & IIf(Remaining <> 1, "s", "")
                EnableTransfer(True)
            End If
        Else
            lblMessage.Text = ""
            EnableTransfer(False)
        End If
    End Sub

    Protected Function SessionToArrayList() As ArrayList
        arList = New ArrayList
        If Not Session("SelectedRows") Is Nothing Then
            arList = CType(Session("SelectedRows"), ArrayList)
        End If
        Return arList
    End Function

    Private Function GetClassSectionRemaining(ByVal SelectedClsSectionId As String) As Integer
        Dim remaining As Integer
        Dim dt As New DataTable
        If Not Session("AvailableClasses") Is Nothing Then
            dt = CType(Session("AvailableClasses"), DataTable)
        End If

        For Each dr As DataRow In dt.Rows
            If dr("ClsSectionId").ToString = SelectedClsSectionId Then
                remaining = dr("Remaining")
                Exit For
            End If
        Next

        Return remaining
    End Function

    Protected Sub CheckSelectedRows()
        arList = New ArrayList
        arList = CType(Session("SelectedRows"), ArrayList)
        If Not arList Is Nothing Then
            For Each item As GridDataItem In rgStudents.MasterTableView.Items
                Dim ResultId As String = item.GetDataKeyValue("ResultId").ToString()
                If arList.Contains(ResultId) Then
                    item.Selected = True
                Else
                    item.Selected = False
                End If
            Next
        End If
    End Sub

    Private Sub ClearSelections()
        Session.Remove("SelectedRows")
        For Each item As GridDataItem In rgStudents.MasterTableView.Items
            item.Selected = False
        Next
    End Sub

    Private Sub EnableTransfer(ByVal enable As Boolean)
        btnTransferComponents.Enabled = enable
        If enable Then
            lblMessage.ForeColor = Drawing.Color.Black
        Else
            lblMessage.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Private Sub SetVariables()
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ResourceId = Trim(Request.QueryString("resid"))

        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        pobj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pobj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

    End Sub
    Protected Sub rgStudents_PreRender(sender As Object, e As EventArgs) Handles rgStudents.PreRender

        rgStudents.Columns.FindByUniqueName("StuEnrollId").Display = False
        rgStudents.Columns.FindByUniqueName("TermId").Display = False
        rgStudents.Columns.FindByUniqueName("ClsSectionId").Display = False
    End Sub
#End Region


End Class