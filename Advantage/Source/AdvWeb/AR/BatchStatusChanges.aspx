﻿<%@ Page Title="Status Changes" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="BatchStatusChanges.aspx.vb" Inherits="AdvWeb.AR.BatchStatusChanges" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script src="../js/AuditHist.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender) {
            window.$telerik.repaintChildren(sender);
        }


        function DateInputRangeError(sender) {

            sender.clear();
            sender.focus();

        }

    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table style="padding: 0; width: 100%; border-collapse: collapse;">
                <tr>
                    <td class="listframetop">
                        <table style="padding: 0; width: 100%; border-collapse: collapse; text-align: center">
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblPrgVersion" runat="server" CssClass="label">Program Version</asp:Label>
                                </td>
                                <td class="contentcell4blue">
                                    <asp:DropDownList ID="ddlPrgVerId" Width="200px" TabIndex="1" runat="server" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblStartDate" runat="server" CssClass="label">Start Date</asp:Label>
                                </td>
                                <td class="contentcell4blue" style="text-align: left">
                                    <telerik:RadDatePicker ID="txtStartDate" runat="server" MinDate="1/1/2001" Width="200px">
                                    </telerik:RadDatePicker>

                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblFromStatusCodeId" runat="server" CssClass="label">Current Status</asp:Label>
                                </td>
                                <td class="contentcell4blue" style="text-align: left">
                                    <asp:DropDownList ID="ddlFromStatusCodeId"  Width="200px" TabIndex="3" runat="server" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblleadGroup" runat="server" CssClass="label">Student Group(s)</asp:Label>
                                </td>
                                <td class="contentcell4blue" style="text-align: left">
                                    <telerik:RadComboBox RenderMode="Lightweight" ID="comboLeadGroups" runat="server"  Width="200px"
                                        EmptyMessage="Select"
                                        EnableVirtualScrolling="true" CheckBoxes="true" EnableCheckAllItemsCheckBox="true">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblCohortStartDate" runat="server" CssClass="label">Cohort Start Date</asp:Label>
                                </td>
                                <td class="contentcell4blue" style="text-align: left">
                                    <telerik:RadDatePicker ID="txtCohortStartDate" runat="server" MinDate="1/1/2001" Width="200px">
                                    </telerik:RadDatePicker>

                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblLastName" runat="server" CssClass="label">Last Name</asp:Label>
                                </td>
                                <td class="contentcell4blue" style="text-align: left">
                                    <asp:TextBox ID="txtLastname"  TabIndex="2" runat="server" CssClass="textbox"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblFirstName" runat="server" CssClass="label">First Name</asp:Label>
                                </td>
                                <td class="contentcell4blue" style="text-align: left">
                                    <asp:TextBox ID="txtFirstName"  TabIndex="2" runat="server" CssClass="textbox"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblssn" runat="server" CssClass="label">SSN</asp:Label>
                                </td>
                                <td class="contentcell4blue" style="text-align: left">
                                    <asp:TextBox ID="txtSSN"  TabIndex="2" runat="server" CssClass="textbox"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                
                                <td class="contentcell4blue" colspan="2" style="text-align: right">
                                    <telerik:RadButton ID="btnBuildList" TabIndex="4" runat="server"
                                        Text="Build List" CausesValidation="False">
                                    </telerik:RadButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfltr4rows">
                        </div>
                    </td>
                </tr>
            </table>


        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table style="padding: 0; width: 100%; border-collapse: collapse;">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" style="text-align: right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>

                </tr>
            </table>
            <table class="maincontenttable" style="padding: 0; width: 99%; border-collapse: collapse;">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                        <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->

                            <asp:Panel ID="pnlNotes" runat="server">
                                <table class="contenttable" style="padding: 0; width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblNote" CssClass="labelbold" runat="server">Note : Please select a Current Status from the dropdown list, then click on the Build List button and then select New Status from the drop down list.</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlStatusChanges" runat="server">
                                <table class="contenttable" style="width: 50%; text-align: center; border-collapse: collapse">
                                    <asp:HiddenField ID="txtShowIt" runat="server"></asp:HiddenField>
                                    <tr>
                                        <td class="contentcellBatchStatusChange" style="vertical-align: middle;">
                                            <asp:Label ID="lblToStatusCodeId" runat="server" CssClass="label">New Status</asp:Label>
                                        </td>
                                        <td class="contentcell4" style="text-align: left">
                                            <asp:DropDownList ID="ddlStatusCodeId" TabIndex="5" runat="server" CssClass="dropdownlist"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <asp:Panel ID="pnlSuspendedDate" runat="server" Visible="false">
                                        <tr>
                                            <td class="contentcellBatchStatusChange">
                                                <asp:Label ID="lblSuspensionStartDate" runat="server" CssClass="label">Suspension Start Date</asp:Label>
                                            </td>
                                            <td class="contentcell4" style="text-align: left">

                                                <telerik:RadDatePicker ID="txtSuspensionStartDate" runat="server" MinDate="1/1/2001">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellBatchStatusChange">
                                                <asp:Label ID="lblSuspensionEndDate" runat="server" CssClass="label">Suspension End Date</asp:Label>
                                            </td>
                                            <td class="contentcell4" style="text-align: left">

                                                <telerik:RadDatePicker ID="txtSuspensionEndDate" runat="server" MinDate="1/1/2001">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellBatchStatusChange">
                                                <asp:Label ID="lblSuspensionComments" runat="server" CssClass="label">Reason:</asp:Label>
                                            </td>
                                            <td class="contentcell4" style="text-align: left">
                                                <asp:TextBox ID="txtSuspensionComments" TabIndex="6" CssClass="textboxdate" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlLOADate" runat="server" Visible="false">
                                        <tr>
                                            <td class="contentcellBatchStatusChange">
                                                <asp:Label ID="lblLOAStartDate" runat="server" CssClass="label">LOA Start Date</asp:Label>
                                            </td>
                                            <td class="contentcell4" style="text-align: left">

                                                <telerik:RadDatePicker ID="txtLOAStartDate" runat="server" MinDate="1/1/2001">
                                                </telerik:RadDatePicker>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellBatchStatusChange">
                                                <asp:Label ID="lblLOAEndDate" runat="server" CssClass="label">LOA End Date</asp:Label>
                                            </td>
                                            <td class="contentcell4" style="text-align: left">

                                                <telerik:RadDatePicker ID="txtLOAEndDate" runat="server" MinDate="1/1/2001">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellBatchStatusChange">
                                                <asp:Label ID="lblLOAComments" runat="server" CssClass="label">Reason:</asp:Label>
                                            </td>
                                            <td class="contentcell4" style="text-align: left">
                                                <asp:DropDownList ID="ddlLOAReasons" TabIndex="6" CssClass="dropdownlist" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                    <tr>
                                        <asp:Panel ID="pnlDate" runat="server">
                                            <td class="contentcell">
                                                <asp:Label ID="lblDate" runat="server" CssClass="label">LOA Return Date</asp:Label>
                                            </td>
                                            <td class="contentcell4" style="text-align: left">

                                                <telerik:RadDatePicker ID="txtDate" runat="server" MinDate="1/1/2001" AutoPostBack="true">
                                                </telerik:RadDatePicker>

                                            </td>
                                        </asp:Panel>
                                    </tr>
                                    <tr>
                                        <td class="contentcellBatchStatusChange"></td>
                                        <td class="contentcell4" style="text-align: left;">
                                            <telerik:RadButton ID="btnChangeStatus" runat="server" TabIndex="7" Text="Change Status" Style="margin-left: 0">
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                                <table style="text-align: center; width: 100%; border: none;" border="0">
                                    <tr>
                                        <td style="text-align: right" width="99%">
                                            <asp:Label ID="lblStudentCount" CssClass="labelbold" runat="server" Visible="False"
                                                Font-Bold="True">Student Count:</asp:Label>
                                        </td>
                                        <td style="text-align: right" width="1%"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <telerik:RadGrid ID="dgrdStuEnrollments" TabIndex="8" runat="server" Width="99%" BorderWidth="1px" Visible="false" GridLines="Both"
                                                BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0"
                                                ShowHeader="True">
                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>

                                                <MasterTableView>
                                                    <Columns>
                                                        <telerik:GridTemplateColumn HeaderText="Student">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStudentName" runat="server" CssClass="label" Text='<%# Container.DataItem("StudentName") %>'>
                                                                </asp:Label>
                                                                <asp:Label ID="lblStuEnrollId" runat="server" Text='<%# Container.DataItem("StuEnrollId") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lblStatusCodeId" runat="server" Text='<%# Container.DataItem("StatusCodeId") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lblSysStatusId" runat="server" Text='<%# Container.DataItem("SysStatusId") %>'
                                                                    Visible="False"> 
                                                                </asp:Label>
                                                                <asp:Label ID="lblLOAStartDate1" runat="server" Text='<%# Container.DataItem("LOAStartDate")%>'
                                                                    Visible="False"> 
                                                                </asp:Label>
                                                                <asp:Label ID="lblLOAEndDate1" runat="server" Text='<%# Container.DataItem("LOAEndDate")%>'
                                                                    Visible="False"> 
                                                                </asp:Label>
                                                                <asp:Label ID="lblPreviousLOAStudentStatusChangeId" runat="server" Text='<%# Container.DataItem("PreviousLOAStudentStatusChangeId")%>'
                                                                    Visible="False"> 
                                                                </asp:Label>
                                                                <asp:Label ID="lblPreviousStatusCodeIdToLOA" runat="server" Text='<%# Container.DataItem("PreviousStatusCodeIdToLOA")%>'
                                                                    Visible="False"> 
                                                                </asp:Label>
                                                                <asp:Label ID="lblLoaPreviousSysStatusId" runat="server" Text='<%# Container.DataItem("LoaPreviousSysStatusId")%>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lblSusStartDate1" runat="server" Text='<%# Container.DataItem("SusStartDate")%>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lblSusEndDate1" runat="server" Text='<%# Container.DataItem("SusEndDate")%>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lblPreviousSusStudentStatusChangeId" runat="server" Text='<%# Container.DataItem("PreviousSusStudentStatusChangeId")%>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lblPreviousStatusCodeIdToSus" runat="server" Text='<%# Container.DataItem("PreviousStatusCodeIdToSus")%>'
                                                                    Visible="False">                                                
                                                                </asp:Label>
                                                                <asp:Label ID="lblSusPreviousSysStatusId" runat="server" Text='<%# Container.DataItem("SusPreviousSysStatusId")%>'
                                                                    Visible="False">                                                
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="SSN">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="7%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSSN" runat="server" CssClass="label" Text='<%# Container.DataItem("SSN") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Program Version">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPrgVerDescrip" runat="server" CssClass="label" Text='<%# Container.DataItem("PrgVerDescrip") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Start Date">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="5%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEnrollStartDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate", "{0:d}") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Expected Graduation Date">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExpGradDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ExpGradDate", "{0:d}") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Last Date Attended">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLDA" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.LDA", "{0:d}") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Date Determined">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDroppedDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DateDetermined", "{0:d}")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="LOA Period">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLOAPeriod" runat="server" CssClass="label" Text='<%# Container.DataItem("LOAPeriod") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Suspension Period">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSuspension" runat="server" CssClass="label" Text='<%# Container.DataItem("SuspensionPeriod") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Attendance?" Display="false" UniqueName="hasattendance">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="6%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAtt" runat="server" CssClass="label" Text='<%# Container.DataItem("HasAttendance") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Grades?" Display="false" UniqueName="hasgrades">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="6%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrades" runat="server" CssClass="label" Text='<%# Container.DataItem("HasGrades") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>

                                                        <telerik:GridTemplateColumn HeaderText="Select All">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <HeaderTemplate>
                                                                <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkChangeStatus', document.forms[0].chkAllItems.checked)" />
                                                                <asp:Label ID="lblSelectAll" runat="server" Text='Select All' />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkChangeStatus" runat="server" Text='' />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Re-Enroll Date" Display="false"
                                                            UniqueName="reenrolldate">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>

                                                                <telerik:RadDatePicker runat="server" ID="txtReenrollDate" SelectedDate='<%# GetReenRollDate(CType(Container.DataItem("StartDate"), Date))%>'
                                                                    ToolTip="The Re-Enroll date can only be set to 180 days greater the Date Determined">
                                                                    <DatePopupButton />
                                                                    <DateInput runat="server" ClientEvents-OnError="DateInputRangeError"></DateInput>
                                                                </telerik:RadDatePicker>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="New Start Date" Display="false"
                                                            UniqueName="newstartdate">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="6%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>

                                                                <telerik:RadDatePicker runat="server" ID="txtnewStartDate" SelectedDate='<%# GetReenRollDate(CType(Container.DataItem("StartDate"), Date))%>'
                                                                    ToolTip="Please enter a new start date">
                                                                    <DatePopupButton />
                                                                    <DateInput runat="server" ClientEvents-OnError="DateInputRangeError"></DateInput>
                                                                </telerik:RadDatePicker>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblAcademicProbation" CssClass="labelbold" runat="server" Visible="False"
                                                Font-Bold="True"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>
