﻿Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports FAME.DataAccessLayer

Partial Class GrdBkWeightings
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected ResourceId As String
    Protected ModuleId As String
    Protected userName As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim userId As String
        'pObj = Header1.UserPagePermission
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        'ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities
        'Dim dt As New DataTable
        Dim facade As New GrdBkWgtsFacade
        Dim instructorId As String
        'Dim ds1 As New DataSet


       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        Try
            If Not Page.IsPostBack Then


                'InstructorId = "E62FE198-18D9-4244-821D-E89A740F4160"
                instructorId = userId

                ViewState("Instructor") = instructorId

                'Bind the Grd Bk Weights Datalist 
                ds = facade.PopulateDataList(instructorId)

                dlstGrdBkWgt.SelectedIndex = -1

                BindDatalist(ds)

                'Populate and bind ListBoxes
                PopulateStatusesListBox()

                BuildDropDownLists()
                'PopulateGrdComponentTypes()

                'To color required fields - Description and Status.
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Set the text box to a new guid
                txtInstrGrdBkWgtId.Text = System.Guid.NewGuid.ToString

            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BindDatalist(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/22/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radStatus.SelectedItem.Text.ToLower = "active") Or (radStatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radStatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "Descrip", DataViewRowState.CurrentRows)
        With dlstGrdBkWgt
            .DataSource = dv
            .DataBind()
        End With

        ViewState("StoredDs") = ds
    End Sub


    Private Sub BindInstrGrdBkWgtListBox(ByRef ds As DataSet)
        lbxInstrGrdBkWgts.DataSource = ds
        lbxInstrGrdBkWgts.DataTextField = "Descrip"
        lbxInstrGrdBkWgts.DataValueField = "InstrGrdBkWgtDetailId"
        lbxInstrGrdBkWgts.DataBind()
    End Sub
    Private Sub SaveDataSet()
        Dim dt As New DataTable

        'Create a datable with a new column called CampusId, make this the primary key.
        Dim col2 As DataColumn = dt.Columns.Add("InstrGrdBkWgtId", GetType(String))

        'Make the CampusId column the primary key
        With dt
            .PrimaryKey = New DataColumn() {.Columns("InstrGrdBkWgtId")}
        End With

        'Save Dataset to Session
        Session("GrdBkWgtDetails") = dt
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control

        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
            ' I have to manually clear the label for the date completed field

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub dlstGrdBkWgt_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstGrdBkWgt.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        Dim ds2 As New DataSet
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim dv2 As New DataView
        '   Dim sStatusId As String
        Dim db As New DataAccess
        'Dim dr As OleDbDataReader
        Dim dt As New DataTable
        'Dim tbl, tblN As DataTable
        'Dim dc As DataColumn
        Dim facade As New GrdBkWgtsFacade

        Try
            strGUID = dlstGrdBkWgt.DataKeys(e.Item.ItemIndex).ToString()

            txtInstrGrdBkWgtId.Text = strGUID

            'Get the GrdWgt Description and status fields.
            ds1 = facade.GetGrdBkWgtDescStatus(strGUID)
            PopulateDescStatusIdFlds(ds1)

            ds = facade.GetGrdBkWgtsOnItemCmd(strGUID)
            ds = facade.ProcessLstBoxInfo(ds)


            'Enable the add , remove and save buttons
            btnAdd.Enabled = True
            btnRemove.Enabled = True
            'btnSave.Enabled = True

            'If the lbxAssigned control has entries clear it
            If lbxInstrGrdBkWgts.Items.Count > 0 Then
                lbxInstrGrdBkWgts.Items.Clear()
            End If

            'Bind only if the FldsAssignedR datatable is not empty
            If ds.Tables("GrdBkWgtDetails").Rows.Count > 0 Then
                With lbxInstrGrdBkWgts
                    .DataSource = ds.Tables("GrdBkWgtDetails")
                    .DataTextField = "GrdBkWgtDescrip"
                    .DataValueField = "InstrGrdBkWgtDetailId"
                    .DataBind()
                End With
            End If
            If ds.Tables("GrdBkWgtDetails").Rows.Count > 0 Then
                'Save the datatable to session
                Session("GrdBkWgtDetails") = ds.Tables("GrdBkWgtDetails")
            End If

            ViewState("MODE") = "EDIT"
            'objCommon.SetBtnState(Form1, "EDIT", pObj)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)

            ds = facade.PopulateDataList(ViewState("Instructor"))
            'dlstGrdBkWgt.SelectedIndex = -1
            BindDatalist(ds)

            'selIndex = e.Item.ItemIndex
            'dlstGrdBkWgt.SelectedIndex = selIndex

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstGrdBkWgt, e.CommandArgument, ViewState, Header1)
            'CommonWebUtilities.SetStyleToSelectedItem(dlstGrdBkWgt, e.CommandArgument, ViewState)

            'Save the selIndex to Viewstate, so it can be used by other subs.
            ViewState("SELINDEX") = CStr(selIndex)

            'Clear the fields
            txtWgtDetailCode.Text = ""
            txtWgtDetailDescrip.Text = ""
            txtWeight.Text = ""

            'btnSave.Enabled = True
            btnUpdate.Enabled = False 'Fix # 02359
            CommonWebUtilities.RestoreItemValues(dlstGrdBkWgt, strGUID)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstCampGrps_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstCampGrps_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub lbxInstrGrdBkWgts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxInstrGrdBkWgts.SelectedIndexChanged

        'Enable the edit button
        btnEdit.Enabled = True
        btnRemove.Enabled = True

        'Disable the update button
        btnUpdate.Enabled = False
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim ds As New DataSet
        Dim dt1 As DataTable
        Dim sGrdBkWgtDetailId As String
        Dim row As DataRow
        Dim objCommon As New CommonUtilities

        If lbxInstrGrdBkWgts.SelectedIndex = -1 Then
            'DisplayErrorMessage("Please select a Weight detail before editing")
            DisplayRADAlert(CallbackType.Postback, "Error1", "Please select a Weight detail before editing", "Weight Not Entered Error")
            Exit Sub
        End If
        'disable the add and remove buttons.
        btnAdd.Enabled = False
        btnRemove.Enabled = False
        '*******************************************************************

        'retrieve the datatable from session
        dt1 = Session("GrdBkWgtDetails")
        '*******************************************************************

        sGrdBkWgtDetailId = lbxInstrGrdBkWgts.SelectedItem.Value.ToString

        row = dt1.Rows.Find(sGrdBkWgtDetailId)

        txtWgtDetailCode.Text = row("Code")

        txtWgtDetailDescrip.Text = row("Descrip")

        txtWeight.Text = row("Weight")

        objCommon.SelValInDDL(ddlGrdComponentTypeId, row("GrdComponentTypeId").ToString)

        btnUpdate.Enabled = True
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim ds As New DataSet
        Dim dt1 As DataTable
        Dim sGrdBkWgtDetailId As String
        Dim row As DataRow
        Dim tbl1 As DataTable
        '        Dim WgtRow As DataRow
        Dim percentExceed As Boolean = False
        ' Dim weight As Integer

        If (txtWgtDetailCode.Text = "" Or txtWgtDetailDescrip.Text = "" Or txtWeight.Text = "" Or ddlGrdComponentTypeId.SelectedItem.Text = "Select") Then
            'DisplayErrorMessage("Grade Component Type, Code, Description or Weight fields cannot be empty")
            DisplayRADAlert(CallbackType.Postback, "Error2", "Grade Component Type, Code, Description or Weight fields cannot be empty", "Not Entered Error")
            Exit Sub
        End If
        'retrieve the datatable from session
        dt1 = Session("GrdBkWgtDetails")
        '*******************************************************************

        sGrdBkWgtDetailId = lbxInstrGrdBkWgts.SelectedItem.Value.ToString

        row = dt1.Rows.Find(sGrdBkWgtDetailId)
        row("Code") = txtWgtDetailCode.Text

        row("Descrip") = txtWgtDetailDescrip.Text
        row("GrdComponentTypeId") = XmlConvert.ToGuid(ddlGrdComponentTypeId.SelectedItem.Value)
        row("ModUser") = AdvantageSession.UserState.UserName
        row("ModDate") = Date.Now

        '********************************************************************************************************
        'Commented out on 1/13/06 to fix the problem where validation was being done before hitting save. The 
        'following situation was not working: Two weights were created fro 80% and 20% initially. When the 
        'user was trying to modify the details to change the 20% to 40% and 80% to 60% hitting update button, error msg
        'would appear upon modifying the first record(20 to 40) saying that weight details cannot exceed 100%.
        '********************************************************************************************************
        'If Not IsNothing(dt1) Then
        '    For Each WgtRow In dt1.Rows
        '        If WgtRow.RowState <> DataRowState.Deleted Then
        '            If WgtRow("InstrGrdBkWgtDetailId").ToString <> sGrdBkWgtDetailId Then
        '                weight += WgtRow("Weight")
        '                If (weight + txtWeight.Text) > 100 Then
        '                    percentExceed = True
        '                End If
        '            End If
        '        End If

        '    Next
        'End If
        'If (percentExceed = True) Then
        '    DisplayErrorMessage("The weight for the Grade Book weight details cannot exceed 100%")
        '    Exit Sub
        'Else
        '    row("Weight") = txtWeight.Text
        'End If
        row("Weight") = txtWeight.Text

        row("GrdBkWgtDescrip") = row("Descrip") & " ( " & row("Weight") & "%" & " ) "

        'Save the datatable to session
        Session("GrdBkWgtDetails") = dt1

        tbl1 = ds.Tables("GrdBkWgtDetails")

        'rebind to the datalist
        With lbxInstrGrdBkWgts
            .DataSource = dt1
            .DataTextField = "GrdBkWgtDescrip"
            .DataValueField = "InstrGrdBkWgtDetailId"
            .DataBind()
        End With

        'Clear the fields
        txtWgtDetailCode.Text = ""
        txtWgtDetailDescrip.Text = ""
        txtWeight.Text = ""
        ddlGrdComponentTypeId.SelectedIndex = 0
        btnAdd.Enabled = True
        btnUpdate.Enabled = False
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        '        Dim SelIndex As Integer
        Dim dt As New DataTable
        Dim dr As DataRow
        Dim ds As New DataSet
        'Dim dt2 As DataTable
        'Dim row As DataRow
        'Dim selhours As Integer
        'Dim selcredits As Integer
        'Dim sCourseGrpId As String
        'Dim childID As String
        Dim strGuid As Guid
        '   Dim WgtRow As DataRow
        Dim percentExceed As Boolean = False
        '  Dim weight As Integer

        If (txtWgtDetailCode.Text = "" Or txtWgtDetailDescrip.Text = "" Or txtWeight.Text = "" Or ddlGrdComponentTypeId.SelectedItem.Text = "Select") Then
            'DisplayErrorMessage("Grade Component Type, Code, Description or Weight fields cannot be empty")
            DisplayRADAlert(CallbackType.Postback, "Error3", "Grade Component Type, Code, Description or Weight fields cannot be empty", "Not Entered Error")
            Exit Sub
        End If

        'Set this field to a new guid 
        strGuid = Guid.NewGuid
        'Add selected item to the relevant assigned list. 



        'Get the dataset from session
        If Not Session("GrdBkWgtDetails") Is Nothing Then
            dt = Session("GrdBkWgtDetails")
        Else
            Dim col1 As DataColumn = dt.Columns.Add("InstrGrdBkWgtId", GetType(System.Guid))
            Dim col2 As DataColumn = dt.Columns.Add("InstrGrdBkWgtDetailId", GetType(System.Guid))
            Dim col3 As DataColumn = dt.Columns.Add("Code", GetType(String))
            Dim col4 As DataColumn = dt.Columns.Add("Descrip", GetType(String))
            Dim col5 As DataColumn = dt.Columns.Add("Seq", GetType(String))
            Dim col6 As DataColumn = dt.Columns.Add("Weight", GetType(String))
            Dim col7 As DataColumn = dt.Columns.Add("GrdBkWgtDescrip", GetType(String))
            Dim col8 As DataColumn = dt.Columns.Add("GrdComponentTypeId", GetType(System.Guid))
            Dim col9 As DataColumn = dt.Columns.Add("ModUser", GetType(String))
            Dim col10 As DataColumn = dt.Columns.Add("ModDate", GetType(String))

            With dt
                'Create primary key
                .PrimaryKey = New DataColumn() {dt.Columns("InstrGrdBkWgtDetailId")}
            End With
        End If

        'Dim dvrs As DataViewRowState
        'dvrs = DataViewRowState.Deleted
        'Dim arows As DataRow() = dt.Select("InstrGrdBkWgtDetailId = '" & lbxInstrGrdBkWgts.SelectedItem.Value.ToString & "' ", "", dvrs)
        'If arows.Length > 0 Then
        '    dt.Rows.Remove(arows(0))
        'End If

        dr = dt.NewRow()
        dr("InstrGrdBkWgtId") = XmlConvert.ToGuid(txtInstrGrdBkWgtId.Text)
        dr("InstrGrdBkWgtDetailId") = strGuid
        dr("Code") = txtWgtDetailCode.Text
        dr("Descrip") = txtWgtDetailDescrip.Text
        dr("Seq") = lbxInstrGrdBkWgts.Items.Count
        dr("GrdComponentTypeId") = XmlConvert.ToGuid(ddlGrdComponentTypeId.SelectedItem.Value)
        dr("ModUser") = AdvantageSession.UserState.UserName
        dr("ModDate") = Date.Now

        '********************************************************************************************************
        'Commented out on 1/13/06 to fix the problem where validation was being done before hitting save. The 
        'following situation was not working: Two weights were created fro 80% and 20% initially. When the 
        'user was trying to modify the details to change the 20% to 40% and 80% to 60% hitting update button, error msg
        'would appear upon modifying the first record(20 to 40) saying that weight details cannot exceed 100%.
        '********************************************************************************************************
        'If Not (dt Is Nothing) Then
        '    For Each WgtRow In dt.Rows
        '        If WgtRow.RowState <> DataRowState.Deleted Then
        '            weight += WgtRow("Weight")
        '            If (weight + txtWeight.Text) > 100 Then
        '                percentExceed = True
        '            End If
        '        End If
        '    Next
        'End If

        'If (percentExceed = True) Then
        '    DisplayErrorMessage("The weight for the Grade Book weight details cannot exceed 100%")
        '    Exit Sub
        'Else
        '    dr("Weight") = txtWeight.Text
        '    dr("GrdBkWgtDescrip") = dr("Descrip") & "(" & dr("Weight") & ")"
        'End If
        '************************************************************************************************************
        dr("Weight") = txtWeight.Text
        dr("GrdBkWgtDescrip") = dr("Descrip") & "(" & dr("Weight") & ")"
        dt.Rows.Add(dr)

        'Save the dataset to session?
        Session("GrdBkWgtDetails") = dt

        lbxInstrGrdBkWgts.Items.Add(New ListItem(txtWgtDetailDescrip.Text & " ( " & txtWeight.Text & "%" & " )", strGuid.ToString))

        'Clear the fields
        txtWgtDetailCode.Text = ""
        txtWgtDetailDescrip.Text = ""
        txtWeight.Text = ""
        ddlGrdComponentTypeId.SelectedIndex = 0
        btnUpdate.Enabled = False
        'btnSave.Enabled = True
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim SelIndex As Integer
        ' Dim iChildId As String
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        'Dim ds As New DataSet
        'Dim intPos As Integer
        'Dim dt2 As DataTable
        'Dim row As DataRow
        'Dim selhours As Integer
        'Dim selcredits As Integer
        'Dim sCourseId As String
        'Dim sCourseGrpId As String

        If lbxInstrGrdBkWgts.SelectedIndex = -1 Then
            'DisplayErrorMessage("Please select a Weight detail before removing")
            DisplayRADAlert(CallbackType.Postback, "Error4", "Please select a Weight detail before removing", "Weight Not Selected Error")
            Exit Sub
        End If
        'Get the datatable from session
        If Not Session("GrdBkWgtDetails") Is Nothing Then
            dt = CType(Session("GrdBkWgtDetails"), DataTable)
        End If

        Dim aRows As DataRow()
        Dim row1 As DataRow
        aRows = dt.Select("Seq > " & lbxInstrGrdBkWgts.SelectedIndex)
        For Each row1 In aRows
            row1("Seq") = row1("Seq") - 1
        Next

        'Dim tRow As DataRow
        'Dim tSeq As String
        'For Each tRow In dt.Rows
        '    tSeq = tRow("seq").ToString()
        'Next

        dr = dt.Rows.Find(lbxInstrGrdBkWgts.SelectedItem.Value.ToString)
        If dr.RowState <> DataRowState.Added Then
            'Mark the row as deleted
            dr.Delete()
        Else
            dt.Rows.Remove(dr)
        End If
        'Remove the item from the lbxSelectCamps list
        SelIndex = lbxInstrGrdBkWgts.SelectedIndex
        lbxInstrGrdBkWgts.Items.RemoveAt(SelIndex)

        'Save the dataset to session
        Session("GrdBkWgtDetails") = dt


    End Sub

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Dim i As Integer
    '    Dim db As New DataAccess
    '    Dim strGuid As String
    '    Dim ID As String
    '    Dim Description As String
    '    Dim Code As String
    '    Dim sb As New System.Text.StringBuilder
    '    Dim dt1, dt2 As DataTable
    '    Dim dr As DataRow
    '    Dim objCommon As New CommonUtilities
    '    Dim iCampusId As String
    '    Dim ds1 As New DataSet
    '    Dim objListGen As New DataListGenerator
    '    Dim sw As New System.IO.StringWriter
    '    Dim dv2 As New DataView
    '    Dim storedGuid As String
    '    Dim sStatusId As String
    '    Dim objGrdBkWgt As New GrdBkWgtsInfo
    '    Dim objGrdBkWgtDet As New GrdBkWgtDetailsInfo
    '    Dim facade As New GrdBkWgtsFacade
    '    Dim ds As New DataSet
    '    Dim result As String
    '    Dim errMsg As String
    '    db.ConnectionString = "ConString"

    '    ' CODE BELOW HANDLES A FRESH INSERT INTO THE TABLE(S)
    '    If ViewState("MODE") = "NEW" Then

    '        'Generate a new guid to insert the record
    '        strGuid = txtInstrGrdBkWgtId.Text

    '        objGrdBkWgt.GrdBkWgtId = strGuid
    '        objGrdBkWgt.InstructorId = ViewState("Instructor")
    '        objGrdBkWgt.StatusId = ddlStatusId.SelectedItem.Value.ToString
    '        objGrdBkWgt.Descrip = txtGrdBkWgtDescrip.Text

    '        'Insert CampusGroup into syCampGrps Table
    '        result = facade.InsertGrdBkWgt(objGrdBkWgt, AdvantageSession.UserState.UserName)
    '        If Not result = "" Then
    '            '   Display Error Message
    '            DisplayErrorMessage(result)
    '        Else
    '            'Save the CampGrpId (GUID) for later use(during edit)
    '            txtInstrGrdBkWgtId.Text = strGuid
    '            objCommon.SetBtnState(Form1, "EDIT", pObj)
    '            ViewState("MODE") = "EDIT"
    '        End If

    '        ' CODE BELOW HANDLES AN UPDATE INTO THE TABLE(S)
    '    ElseIf viewstate("MODE") = "EDIT" Then

    '        'Retrieve the Guid to update particular record.
    '        objGrdBkWgt.GrdBkWgtId = txtInstrGrdBkWgtId.Text
    '        objGrdBkWgt.InstructorId = ViewState("Instructor")
    '        objGrdBkWgt.StatusId = ddlStatusId.SelectedItem.Value.ToString
    '        objGrdBkWgt.Descrip = txtGrdBkWgtDescrip.Text

    '        facade.UpdateGrdBkWgt(objGrdBkWgt, AdvantageSession.UserState.UserName)
    '    End If

    '    Try

    '        'Pull the dataset from Session.
    '        If Not Session("GrdBkWgtDetails") Is Nothing Then
    '            dt1 = Session("GrdBkWgtDetails")

    '        End If


    '        ' CODE BELOW HANDLES AN INSERT/UPDATE INTO THE TABLE(S)

    '        'Run getchanges method on the dataset to see which rows have changed.
    '        'This section handles the changes to the Advantage Required fields
    '        If Not dt1 Is Nothing Then
    '            errMsg = ValidateWeight(dt1)
    '            If errMsg <> "" Then
    '                DisplayErrorMessage(errMsg)
    '                Exit Sub
    '            End If
    '            dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted Or DataRowState.Modified)
    '            Try
    '                If Not IsNothing(dt2) Then
    '                    For Each dr In dt2.Rows
    '                        If dr.RowState = DataRowState.Added Then
    '                            'Try

    '                            objGrdBkWgtDet.Descrip = dr("Descrip")
    '                            objGrdBkWgtDet.Code = dr("Code")
    '                            objGrdBkWgtDet.weight = dr("Weight")
    '                            objGrdBkWgtDet.GrdBkWgtId = dr("InstrGrdBkWgtId").ToString
    '                            objGrdBkWgtDet.GrdBkWgtDetailId = dr("InstrGrdBkWgtDetailId").ToString
    '                            objGrdBkWgtDet.Seq = dr("Seq")
    '                            objGrdBkWgtDet.GrdCompTypeId = dr("GrdComponentTypeId").ToString

    '                            Dim result2 As String = facade.InsertGrdBkWgtDetail(objGrdBkWgtDet, AdvantageSession.UserState.UserName)
    '                            If Not result2 = "" Then
    '                                '   Display Error Message
    '                                DisplayErrorMessage(result2)
    '                                'Exit Sub
    '                            End If
    '                            'Catch ex As System.Exception
     '                            '	Dim exTracker = new AdvApplicationInsightsInitializer()
    '                            '	exTracker.TrackExceptionWrapper(ex)

    '                            '    Throw New BaseException(ex.InnerException.Message)
    '                            'End Try
    '                        ElseIf dr.RowState = DataRowState.Deleted Then
    '                            objGrdBkWgtDet.GrdBkWgtDetailId = dr("InstrGrdBkWgtDetailId", DataRowVersion.Original).ToString

    '                            Dim result4 As String = facade.DeleteGrdBkWgtDetail(objGrdBkWgtDet, Date.Parse(txtModDate.Text))
    '                            If Not result4 = "" Then
    '                                '   Display Error Message
    '                                DisplayErrorMessage(result4)
    '                                ' Exit Sub
    '                            End If
    '                        ElseIf dr.RowState = DataRowState.Modified Then
    '                            objGrdBkWgtDet.GrdBkWgtDetailId = dr("InstrGrdBkWgtDetailId").ToString
    '                            objGrdBkWgtDet.Seq = dr("Seq")
    '                            objGrdBkWgtDet.Code = dr("Code")
    '                            objGrdBkWgtDet.Descrip = dr("Descrip")
    '                            objGrdBkWgtDet.weight = dr("Weight")
    '                            objGrdBkWgtDet.GrdCompTypeId = dr("GrdComponentTypeId").ToString

    '                            Dim result3 As String = facade.UpdateGrdBkWgtDetail(objGrdBkWgtDet, AdvantageSession.UserState.UserName)
    '                            If Not result3 = "" Then
    '                                '   Display Error Message
    '                                DisplayErrorMessage(result3)
    '                                'Exit Sub
    '                            End If
    '                        End If
    '                    Next            
    '                End If
    '                'Commit the transaction if successful
    '                ContextUtil.SetComplete()
    '                dt1.AcceptChanges()
    '                Session("GrdBkWgtDetails") = dt1
    '            Catch exx As System.Exception
     '            	Dim exTracker = new AdvApplicationInsightsInitializer()
    '            	exTracker.TrackExceptionWrapper(exx)

    '                ContextUtil.SetAbort()
    '            End Try

    '        End If
    '        'Bind the Grd Bk Weights Datalist 
    '        ds = facade.PopulateDataList(Viewstate("Instructor"))

    '        BindDatalist(ds)

    '        '   set Style to Selected Item
    '        CommonWebUtilities.SetStyleToSelectedItem(dlstGrdBkWgt, txtInstrGrdBkWgtId.Text, ViewState, Header1)

    '    Catch ex As System.Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try
    'End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '        Dim i As Integer
        Dim db As New DataAccess
        Dim strGuid As String
        'Dim ID As String
        'Dim Description As String
        'Dim Code As String
        ' Dim sb As New System.Text.StringBuilder
        Dim dt1 As DataTable = New DataTable()
        Dim dt2 As DataTable
        '  Dim dr As DataRow
        Dim objCommon As New CommonUtilities
        '   Dim iCampusId As String
        ' Dim ds1 As New DataSet
        'Dim objListGen As New DataListGenerator
        ' Dim sw As New System.IO.StringWriter
        ' Dim dv2 As New DataView
        'Dim storedGuid As String
        'Dim sStatusId As String
        Dim objGrdBkWgt As New GrdBkWgtsInfo
        ' Dim objGrdBkWgtDet As New GrdBkWgtDetailsInfo
        Dim facade As New GrdBkWgtsFacade
        ' Dim ds As New DataSet
        Dim result As String
        Dim errMsg As String
        db.ConnectionString = "ConString"

        ' CODE BELOW HANDLES A FRESH INSERT INTO THE TABLE(S)
        If ViewState("MODE") = "NEW" Then

            'Generate a new guid to insert the record
            strGuid = txtInstrGrdBkWgtId.Text

            objGrdBkWgt.GrdBkWgtId = strGuid
            objGrdBkWgt.InstructorId = ViewState("Instructor")
            objGrdBkWgt.StatusId = ddlStatusId.SelectedItem.Value.ToString
            objGrdBkWgt.Descrip = txtGrdBkWgtDescrip.Text
            objGrdBkWgt.ModDate = Date.Now
            txtModDate.Text = objGrdBkWgt.ModDate.ToString

            'Insert CampusGroup into syCampGrps Table
            result = facade.InsertGrdBkWgt(objGrdBkWgt, AdvantageSession.UserState.UserName)
            If Not result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error6", result, "Save Error")
            Else
                'Save the CampGrpId (GUID) for later use(during edit)
                txtInstrGrdBkWgtId.Text = strGuid
                'objCommon.SetBtnState(Form1, "EDIT", pObj)
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
            End If

            ' CODE BELOW HANDLES AN UPDATE INTO THE TABLE(S)
        ElseIf ViewState("MODE") = "EDIT" Then

            'Retrieve the Guid to update particular record.
            objGrdBkWgt.GrdBkWgtId = txtInstrGrdBkWgtId.Text
            objGrdBkWgt.InstructorId = ViewState("Instructor")
            objGrdBkWgt.StatusId = ddlStatusId.SelectedItem.Value.ToString
            objGrdBkWgt.Descrip = txtGrdBkWgtDescrip.Text

            facade.UpdateGrdBkWgt(objGrdBkWgt, AdvantageSession.UserState.UserName)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        End If

        Try

            'Pull the dataset from Session.
            If Not Session("GrdBkWgtDetails") Is Nothing Then
                dt1 = CType(Session("GrdBkWgtDetails"), DataTable)

            End If


            'Run getchanges method on the dataset to see which rows have changed.
            'This section handles the changes to the GrdBkWgtDetails table.
            If Not dt1 Is Nothing Then
                errMsg = ValidateWeight(dt1)
                If errMsg <> "" Then
                    'DisplayErrorMessage(errMsg)
                    DisplayRADAlert(CallbackType.Postback, "Error7", errMsg, "Save Error")
                    Exit Sub
                End If
                dt2 = dt1.GetChanges(DataRowState.Added Or DataRowState.Deleted Or DataRowState.Modified)
                'Try
                '    If Not IsNothing(dt2) Then
                '        For Each dr In dt2.Rows
                '            If dr.RowState = DataRowState.Added Then
                '                'Try

                '                objGrdBkWgtDet.Descrip = dr("Descrip")
                '                objGrdBkWgtDet.Code = dr("Code")
                '                objGrdBkWgtDet.weight = dr("Weight")
                '                objGrdBkWgtDet.GrdBkWgtId = dr("InstrGrdBkWgtId").ToString
                '                objGrdBkWgtDet.GrdBkWgtDetailId = dr("InstrGrdBkWgtDetailId").ToString
                '                objGrdBkWgtDet.Seq = dr("Seq")
                '                objGrdBkWgtDet.GrdCompTypeId = dr("GrdComponentTypeId").ToString

                '                Dim result2 As String = facade.InsertGrdBkWgtDetail(objGrdBkWgtDet, AdvantageSession.UserState.UserName)
                '                If Not result2 = "" Then
                '                    '   Display Error Message
                '                    DisplayErrorMessage(result2)
                '                    'Exit Sub
                '                End If
                '                'Catch ex As System.Exception
                 '                '	Dim exTracker = new AdvApplicationInsightsInitializer()
                '                '	exTracker.TrackExceptionWrapper(ex)

                '                '    Throw New BaseException(ex.InnerException.Message)
                '                'End Try
                '            ElseIf dr.RowState = DataRowState.Deleted Then
                '                objGrdBkWgtDet.GrdBkWgtDetailId = dr("InstrGrdBkWgtDetailId", DataRowVersion.Original).ToString

                '                Dim result4 As String = facade.DeleteGrdBkWgtDetail(objGrdBkWgtDet, Date.Parse(txtModDate.Text))
                '                If Not result4 = "" Then
                '                    '   Display Error Message
                '                    DisplayErrorMessage(result4)
                '                    ' Exit Sub
                '                End If
                '            ElseIf dr.RowState = DataRowState.Modified Then
                '                objGrdBkWgtDet.GrdBkWgtDetailId = dr("InstrGrdBkWgtDetailId").ToString
                '                objGrdBkWgtDet.Seq = dr("Seq")
                '                objGrdBkWgtDet.Code = dr("Code")
                '                objGrdBkWgtDet.Descrip = dr("Descrip")
                '                objGrdBkWgtDet.weight = dr("Weight")
                '                objGrdBkWgtDet.GrdCompTypeId = dr("GrdComponentTypeId").ToString

                '                Dim result3 As String = facade.UpdateGrdBkWgtDetail(objGrdBkWgtDet, AdvantageSession.UserState.UserName)
                '                If Not result3 = "" Then
                '                    '   Display Error Message
                '                    DisplayErrorMessage(result3)
                '                    'Exit Sub
                '                End If
                '            End If
                '        Next
                '    End If
                '    'Commit the transaction if successful
                '    ContextUtil.SetComplete()
                '    dt1.AcceptChanges()
                '    Session("GrdBkWgtDetails") = dt1
                'Catch exx As System.Exception
                 '	Dim exTracker = new AdvApplicationInsightsInitializer()
                '	exTracker.TrackExceptionWrapper(exx)

                '    ContextUtil.SetAbort()
                'End Try

                '   try to update the DB and show any error message
                UpdateDB(dt2)

            End If
            CommonWebUtilities.RestoreItemValues(dlstGrdBkWgt, txtInstrGrdBkWgtId.Text)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub UpdateDB(ByVal dt As DataTable)
        Dim ds As New DataSet
        Dim result As String
        '   update DB
        With New GrdBkWgtsFacade
            'Dim resultsExist As Integer

            'resultsExist = .DoGrdScalesExistFrGrdSys(Session("GrdSystemId"))
            'If resultsExist > 0 Then
            '    DisplayErrorMessage("Cannot change the Grade System since one or more Grade Scales use this Grade System")
            'End If
            If Not IsNothing(dt) Then
                result = .UpdateGradeBookWeightsDS(dt)
            Else
                Exit Sub
            End If
            If Not result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error8", result, "Update DB Error")
            Else
                'if there were no errors then bind and set selected style to the datalist
                'Bind the Grd Bk Weights Datalist 
                ds = .PopulateDataList(ViewState("Instructor"))

                BindDatalist(ds)

                '   set Style to Selected Item
                'CommonWebUtilities.SetStyleToSelectedItem(dlstGrdBkWgt, txtInstrGrdBkWgtId.Text, ViewState, Header1)
                'CommonWebUtilities.SetStyleToSelectedItem(dlstGrdBkWgt, txtInstrGrdBkWgtId.Text, ViewState)

                'update datatable
                dt.AcceptChanges()
                Session("GrdBkWgtDetails") = dt
            End If
        End With
    End Sub
    Private Sub PopulateStatusesListBox()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New GrdBkWgtsFacade

        ' Bind the Dataset to the CheckBoxList
        ddlStatusId.DataSource = facade.GetAllStatuses()
        ddlStatusId.DataTextField = "Status"
        ddlStatusId.DataValueField = "StatusId"
        ddlStatusId.DataBind()
    End Sub
    Private Sub PopulateGrdComponentTypes()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New GrdBkWgtsFacade

        ' Bind the Dataset to the CheckBoxList
        ddlGrdComponentTypeId.DataSource = facade.GetGrdComponentTypes()
        ddlGrdComponentTypeId.DataTextField = "Descrip"
        ddlGrdComponentTypeId.DataValueField = "GrdComponentTypeId"
        ddlGrdComponentTypeId.DataBind()
        ddlGrdComponentTypeId.Items.Insert(0, New ListItem("Select", ""))
        ddlGrdComponentTypeId.SelectedIndex = 0
    End Sub

    Private Sub PopulateDescStatusIdFlds(ByVal ds As DataSet)
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim dt As DataTable
        Dim row As DataRow
        Dim objCommon As New CommonUtilities

        dt = ds.Tables("GrdBkWgtInfo")

        For Each row In dt.Rows

            txtGrdBkWgtDescrip.Text = row("Descrip")

            objCommon.SelValInDDL(ddlStatusId, row("StatusId").ToString)
            txtModUser.Text = row("ModUser")
            txtModDate.Text = row("ModDate").ToString
        Next

    End Sub
    Private Function ValidateWeight(ByVal dt As DataTable) As String

        Dim row As DataRow
        Dim objCommon As New CommonUtilities
        Dim weight As Decimal
        Dim percentLess As Boolean = False
        Dim errstr As String

        For Each row In dt.Rows
            If row.RowState <> DataRowState.Deleted Then
                weight += row("Weight")
            End If
        Next

        If (weight) < 100 Then
            errstr = "The sum of weights for the Grade Book weight details should add upto 100%"
        ElseIf (weight) > 100 Then
            errstr = "The weight for the Grade Book weight details cannot exceed 100%"
        Else
            errstr = ""
        End If

        Return errstr
    End Function

    Private Sub btnUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUp.Click
        Dim count As Integer
        Dim selIndex As Integer
        Dim tmpTxt As String
        Dim tmpVal As String
        Dim dt As DataTable
        Dim row1, row2 As DataRow
        Dim tmpSeq, curSeq As Integer

        If lbxInstrGrdBkWgts.SelectedIndex > 0 Then
            For count = 0 To count = lbxInstrGrdBkWgts.Items.Count
                selIndex = lbxInstrGrdBkWgts.SelectedIndex
                tmpTxt = lbxInstrGrdBkWgts.Items(selIndex - 1).Text
                tmpVal = lbxInstrGrdBkWgts.Items(selIndex - 1).Value.ToString

                'You need to swap the sequence numbers of these elements as well
                dt = Session("GrdBkWgtDetails")

                'Get the sequence of the item above the selected item.
                row1 = dt.Rows.Find(lbxInstrGrdBkWgts.Items(selIndex - 1).Value)
                tmpSeq = row1("Seq")

                'Get the sequence of the selected item
                row2 = dt.Rows.Find(lbxInstrGrdBkWgts.Items(selIndex).Value)
                curSeq = row2("Seq")

                'Swap the sequences of the selected item w/ the item above in the datatable
                row2("Seq") = tmpSeq
                row1("Seq") = curSeq

                'Save the datatable to session
                Session("GrdBkWgtDetails") = dt

                'Swap the items in the selected index w/ the index above.
                lbxInstrGrdBkWgts.Items(selIndex - 1).Text = lbxInstrGrdBkWgts.Items(selIndex).Text
                lbxInstrGrdBkWgts.Items(selIndex - 1).Value = lbxInstrGrdBkWgts.Items(selIndex).Value

                lbxInstrGrdBkWgts.Items(selIndex).Text = tmpTxt
                lbxInstrGrdBkWgts.Items(selIndex).Value = tmpVal

                lbxInstrGrdBkWgts.SelectedIndex = selIndex - 1

            Next
        End If
    End Sub

    Private Sub btnDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDown.Click
        Dim count As Integer
        Dim selIndex As Integer
        Dim tmpTxt As String
        Dim tmpVal As String
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim row1, row2 As DataRow
        Dim tmpSeq, curSeq As Integer

        'commented out for bug fix 3241
        'If lbxInstrGrdBkWgts.SelectedIndex <> (lbxInstrGrdBkWgts.Items.Count - 1) Then
        If lbxInstrGrdBkWgts.SelectedIndex >= 0 Then
            If lbxInstrGrdBkWgts.SelectedIndex < (lbxInstrGrdBkWgts.Items.Count - 1) Then
                For count = 0 To count = lbxInstrGrdBkWgts.Items.Count
                    selIndex = lbxInstrGrdBkWgts.SelectedIndex
                    tmpTxt = lbxInstrGrdBkWgts.Items(selIndex + 1).Text
                    tmpVal = lbxInstrGrdBkWgts.Items(selIndex + 1).Value.ToString

                    'You need to swap the sequence numbers of these elements as well
                    dt = Session("GrdBkWgtDetails")

                    'Get the sequence of the item above the selected item.
                    row1 = dt.Rows.Find(lbxInstrGrdBkWgts.Items(selIndex + 1).Value)
                    tmpSeq = row1("Seq")

                    'Get the sequence of the selected item
                    row2 = dt.Rows.Find(lbxInstrGrdBkWgts.Items(selIndex).Value)
                    curSeq = row2("Seq")

                    'Swap the sequences of the selected item w/ the item above in the datatable
                    row2("Seq") = tmpSeq
                    row1("Seq") = curSeq

                    'Save the datatable to session
                    Session("GrdBkWgtDetails") = dt

                    'Swap the items in the selected index w/ the index above.
                    lbxInstrGrdBkWgts.Items(selIndex + 1).Text = lbxInstrGrdBkWgts.Items(selIndex).Text
                    lbxInstrGrdBkWgts.Items(selIndex + 1).Value = lbxInstrGrdBkWgts.Items(selIndex).Value

                    lbxInstrGrdBkWgts.Items(selIndex).Text = tmpTxt
                    lbxInstrGrdBkWgts.Items(selIndex).Value = tmpVal

                    lbxInstrGrdBkWgts.SelectedIndex = selIndex + 1

                Next
            End If
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim dt As DataTable
        Dim ds1 As New DataSet
        Dim ds As New DataSet
        Dim facade As New GrdBkWgtsFacade

        'Dim sr As New System.IO.StringReader(CStr(ViewState("ds1")))
        Try
            'Clear the dataset to be synchronized with the database
            dt = Session("GrdBkWgtDetails")
            If Not (dt Is Nothing) Then
                dt.Clear()
                Session("GrdBkWgtDetails") = dt
            End If

            lbxInstrGrdBkWgts.Items.Clear()
            ClearRHS()
            'ds1.ReadXml(sr)
            ds = facade.PopulateDataList(ViewState("Instructor"))
            dlstGrdBkWgt.SelectedIndex = -1
            BindDatalist(ds)

            'disable new and delete buttons.
            'objcommon.SetBtnState(Form1, "NEW", pObj)
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtInstrGrdBkWgtId.Text = System.Guid.NewGuid.ToString
            'btnSave.Enabled = False

            'Clear the fields
            txtWgtDetailCode.Text = ""
            txtWgtDetailDescrip.Text = ""
            txtWeight.Text = ""

            'Enable the add , remove and save buttons
            btnAdd.Enabled = True
            btnRemove.Enabled = True
            'btnSave.Enabled = True
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstGrdBkWgt, System.Guid.Empty.ToString, ViewState, Header1)
            'CommonWebUtilities.SetStyleToSelectedItem(dlstGrdBkWgt, System.Guid.Empty.ToString, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstGrdBkWgt, Guid.Empty.ToString)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim Info As New GrdBkWgtsFacade
        Dim dt As DataTable
        Try
            Dim msg As String = Info.DeleteGrdBkWgtDetails(txtInstrGrdBkWgtId.Text, Date.Parse(txtModDate.Text))
            If msg <> "" Then
                '   Display Error Message
                'DisplayErrorMessage(msg)
                DisplayRADAlert(CallbackType.Postback, "Error9", msg, "Delete Error")
            Else
                Dim result As String = Info.DeleteGrdBkWeight(txtInstrGrdBkWgtId.Text, Date.Parse(txtModDate.Text))
                If result <> "" Then
                    '   Display Error Message
                    'DisplayErrorMessage(result)
                    DisplayRADAlert(CallbackType.Postback, "Error10", result, "Delete Error")
                Else
                    ClearRHS()
                    'Clear the dataset to be synchronized with the database
                    dt = Session("GrdBkWgtDetails")
                    If Not (dt Is Nothing) Then
                        dt.Clear()
                        Session("GrdBkWgtDetails") = dt
                    End If
                    lbxInstrGrdBkWgts.Items.Clear()
                    ds = Info.PopulateDataList(ViewState("Instructor"))
                    dlstGrdBkWgt.SelectedIndex = -1
                    BindDatalist(ds)
                    'objCommon.SetBtnState(Form1, "NEW", pObj)
                    objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                    ViewState("MODE") = "NEW"
                    'Set the text box to a new guid
                    txtInstrGrdBkWgtId.Text = System.Guid.NewGuid.ToString
                End If
            End If
            CommonWebUtilities.RestoreItemValues(dlstGrdBkWgt, Guid.Empty.ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        'Blank out the currently selected item since we just deleted it
        ViewState("ItemSelected") = ""
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Dim facade As New GrdBkWgtsFacade

        Try
            ClearRHS()
            'objCommon.SetBtnState(Form1, "NEW", pObj)

            ViewState("MODE") = "NEW"

            'Set the text box to a new guid
            txtInstrGrdBkWgtId.Text = System.Guid.NewGuid.ToString
            ds2 = facade.PopulateDataList(ViewState("Instructor"))
            dlstGrdBkWgt.SelectedIndex = -1
            BindDatalist(ds2)

            'If the lbxAssigned control has entries clear it
            If lbxInstrGrdBkWgts.Items.Count > 0 Then
                lbxInstrGrdBkWgts.Items.Clear()
            End If

            'Clear the fields
            txtWgtDetailCode.Text = ""
            txtWgtDetailDescrip.Text = ""
            txtWeight.Text = ""
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            CommonWebUtilities.RestoreItemValues(dlstGrdBkWgt, Guid.Empty.ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Public Sub TextValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        'Dim strMinus As String = Mid(args.Value, 1, 1)

        Dim parsed As Boolean = True
        Dim validchars As String = "0123456789"
        Dim checkStr As String = args.Value

        ' for (var i=0; i < checkStr.length; i++) {
        'var letter = checkStr.charAt(i);

        '   if (validchars.indexOf(letter) != -1)
        '     continue;
        '   alert(letter + " not allowed");
        '   parsed = false;
        '   break;
        '       If strMinus = "-" Then
        '           DisplayErrorMessage("The Value Cannot be Negative")
        '           validatetxt = True
        '       End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnDown)
        controlsToIgnore.Add(btnUp)
        controlsToIgnore.Add(btnEdit)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnUpdate)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        'BIndToolTip()
    End Sub
    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        '        Dim SchoolItem As String

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlGrdComponentTypeId, AdvantageDropDownListName.GradCompTyp, campusId, True, True, String.Empty))


        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub


End Class
