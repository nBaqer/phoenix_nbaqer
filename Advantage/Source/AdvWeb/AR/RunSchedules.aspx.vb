﻿
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects

Partial Class AR_RunSchedules
    Inherits BasePage

    Private pObj As New UserPagePermissionInfo

    Protected ModuleId As String
    Protected campusId As String
    Protected ResourceId As String
    Protected username As String


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'disable history button
        'Header1.EnableHistoryButton(False)
        Dim userId As String
        'Dim fac As New UserSecurityFacade

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        username = AdvantageSession.UserState.UserName

        ModuleId = HttpContext.Current.Request.Params("Mod").ToString


        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        'disable history button
        'Header1.EnableHistoryButton(False)
        If Not Page.IsPostBack Then
            BuildDropDownLists()
        End If
        'ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
    End Sub

    Private Sub BuildDropDownLists()
        BuildProgramsDDL()
        BuildTermsDDL(ddlProgId.SelectedValue.ToString)
        populateLeadGroups()
    End Sub

    Private Sub BuildProgramsDDL()
        Dim fac As New TranscriptFacade

        With ddlProgId
            .DataSource = fac.GetPrograms(campusId)
            .DataTextField = "ProgDescrip"
            .DataValueField = "ProgId"
            .DataBind()
            .Items.Insert(0, New ListItem("All Programs", ""))
            .SelectedIndex = 0
        End With


    End Sub

    Private Sub BuildTermsDDL(ByVal progId As String)
        Dim fac As New TranscriptFacade

        With ddlTermId
            .DataSource = fac.GetTerms(campusId, progId)
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataBind()
            .Items.Insert(0, New ListItem("All Terms", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub ddlProgId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlProgId.SelectedIndexChanged
        'Get the terms for the selected program
        BuildTermsDDL(ddlProgId.SelectedValue.ToString)
    End Sub

    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        'Dim fac As New TranscriptFacade
        'Dim dt As New DataTable

        ''dt = fac.RegisterStudentsInClassesRestrictedByStudentStart(campusId, ddlProgId.SelectedValue.ToString, ddlTermId.SelectedValue.ToString, user)
        'dt = fac.RegisterStudentsInClassesRestrictedByStudentStart(campusId, ddlProgId.SelectedValue.ToString, ddlTermId.SelectedValue.ToString, user)

        'Dim dv As New DataView(dt, Nothing, "Program,StartDate,Code,Course,Section,LastName", DataViewRowState.CurrentRows)
        'dgrdProcessedRecords.DataSource = dt
        'dgrdProcessedRecords.DataBind()
        Dim fac As New TranscriptFacade
        Dim dtStudentClasses As New DataTable
        Dim dtResults As New DataTable
        Dim boolConflictClassWithinTerm As Boolean = False
        Dim boolConflictClassIgnoreRoom As Boolean = False

        Dim dsScheduleConflicts As New DataSet
        Dim strClsSectionList As String = ""
        Dim strCourseDescrip As String = ""
        Dim strStudent As String = ""
        Dim strStuEnrollIdList As String = ""
        Dim strTermId As String = ""

        dtStudentClasses.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("ClsSectionId", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("Program", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("Course", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("Code", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("Section", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("StartDate", Type.GetType("System.DateTime"))
        dtStudentClasses.Columns.Add("EndDate", Type.GetType("System.DateTime"))
        dtStudentClasses.Columns.Add("LastName", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("FirstName", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("TermId", Type.GetType("System.String"))

        Try

            'Loop thru the collection to retrieve the chkbox value
            For Each row As GridViewRow In GridView1.Rows
                If (CType(row.FindControl("chkCopy"), CheckBox).Checked = True) Then


                    Dim drSC As DataRow = dtStudentClasses.NewRow
                    drSC("StuEnrollId") = CType(row.FindControl("txtStuEnrollId"), TextBox).Text
                    drSC("Program") = CType(row.FindControl("lblProgram"), Label).Text
                    drSC("FirstName") = CType(row.FindControl("lblFirstName"), Label).Text
                    drSC("LastName") = CType(row.FindControl("lblLastName"), Label).Text
                    drSC("Course") = CType(row.FindControl("lblCourse"), Label).Text
                    drSC("Code") = CType(row.FindControl("LblCode"), Label).Text
                    drSC("StartDate") = CType(row.FindControl("lblStartDate"), Label).Text
                    drSC("EndDate") = CType(row.FindControl("lblEndDate"), Label).Text
                    drSC("Section") = CType(row.FindControl("lblSection"), Label).Text
                    drSC("ClsSectionId") = CType(row.FindControl("txtClsSectionId"), TextBox).Text
                    drSC("TermId") = CType(row.FindControl("lblTermId"), Label).Text


                    dtStudentClasses.Rows.Add(drSC)

                    'dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(drSC("ClsSectionId"), drSC("StuEnrollId"))
                    'If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                    '    strCourseDescrip &= drSC("Course") & ","
                    '    strStuEnrollIdList &= drSC("StuEnrollId") & ","
                    '    strClsSectionList &= drSC("ClsSectionId") & ","
                    '    strTermId &= drSC("TermId") & ","
                    'Else
                    '    'Check for conflicts inside the same term
                    '    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsForClassesInsideTerm(drSC("ClsSectionId"), CType(row.FindControl("lblTermId"), Label).Text, campusId)
                    '    If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                    '        strCourseDescrip &= drSC("Course") & ","
                    '        strStuEnrollIdList &= drSC("StuEnrollId") & ","
                    '        strClsSectionList &= drSC("ClsSectionId") & ","
                    '        strTermId &= drSC("TermId") & ","
                    '        boolConflictClassWithinTerm = True
                    '    Else
                    '        'Check for conflicts, but ignore room
                    '        dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsForClassesInsideTermIgnoreRooms(drSC("ClsSectionId"), CType(row.FindControl("lblTermId"), Label).Text, campusId)
                    '        If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                    '            strCourseDescrip &= drSC("Course") & ","
                    '            strStuEnrollIdList &= drSC("StuEnrollId") & ","
                    '            strClsSectionList &= drSC("ClsSectionId") & ","
                    '            strTermId &= drSC("TermId") & ","
                    '            boolConflictClassIgnoreRoom = True
                    '        Else
                    '            dtStudentClasses.Rows.Add(drSC)
                    '        End If
                    '    End If
                    'End If
                End If
                'set flag to true if option is selected
                'retrieve clssection id from the datagrid
            Next

            'Only the students with out any schedule conflicts will be registered in classes 
            'Only students with no conflicts will be in dtStudentClasses table

            'Commented by Balaji on 12/1/2011 as this part of code defeats the purpose of the page
            'page is supposed to allow user to register a student or students in a class
            'this section of code hides grid after one student is registered
            'GridView1.Columns(8).Visible = False
            'btnBuildList.Visible = False

            'GridView1.DataSource = dtStudentClasses
            'GridView1.DataBind()

            'If dtStudentClasses.Rows.Count >= 1 Then
            '    lblRegisterStudents.Visible = True
            '    lblRegisterStudents.Text = "List of students registered in classes"
            'Else
            '    lblRegisterStudents.Visible = False
            'End If

            dtResults = fac.RegisterStudentsInClassesRestrictedByStudentStart(dtStudentClasses, AdvantageSession.UserState.UserName)
            Dim dv As New DataView(dtStudentClasses, Nothing, "Program,StartDate,Code,Course,Section,LastName", DataViewRowState.CurrentRows)
            BuildList()


            If dtStudentClasses.Rows.Count >= 1 Then
                pnlRegisteredStudents.Visible = True
                Label2.Visible = True
                GridView2.Visible = True
                GridView2.Columns(8).Visible = False
                GridView2.DataSource = dtStudentClasses
                GridView2.DataBind()
            Else
                pnlRegisteredStudents.Visible = False
                Label2.Visible = False
                GridView2.Visible = False
            End If

            'If Not strStuEnrollIdList.ToString.Trim = "" Then
            '    strCourseDescrip = Mid(strCourseDescrip, 1, InStrRev(strCourseDescrip, ",") - 1)
            '    strStuEnrollIdList = Mid(strStuEnrollIdList, 1, InStrRev(strStuEnrollIdList, ",") - 1)
            '    strClsSectionList = Mid(strClsSectionList, 1, InStrRev(strClsSectionList, ",") - 1)
            '    strTermId = Mid(strTermId, 1, InStrRev(strTermId, ",") - 1)
            '    If Not strStuEnrollIdList.ToString.Trim = "" Then
            '        'dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(strClsSectionList.ToString, strStuEnrollIdList.Trim)
            '        ' If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
            '        ShowScheduleConflict(strClsSectionList.ToString, strStuEnrollIdList, "", "All", strCourseDescrip, strTermId, boolConflictClassWithinTerm, boolConflictClassIgnoreRoom) 'Show the radWindow
            '        Exit Sub
            '        'End If
            '    End If
            'End If


            'dgrdProcessedRecords.DataSource = dtStudentClasses
            'dgrdProcessedRecords.DataBind()
        Catch

        End Try
    End Sub
    'Protected Sub ShowScheduleConflict(ByVal ClsSectId As String, ByVal StuEnrollId As String, ByVal StudentId As String, _
    '                                   Optional ByVal buttonState As String = "Add", Optional ByVal Classes As String = "", _
    '                                   Optional ByVal TermId As String = "", Optional ByVal flag As Boolean = False, Optional ByVal boolConflictClassIgnoreRoom As Boolean = False)
    '    Dim strURL As String = "ViewScheduleConflicts.aspx?resid=909&mod=AR" + "&ClsSectionId=" + ClsSectId.ToString + "&StuEnrollId=" + StuEnrollId.ToString + "&Student=" + StudentId.ToString + "&buttonState=" + buttonState + "&Classes=" + Server.UrlEncode(Classes) + "&TermId=" + TermId.ToString + "&boolconflictinsideterm=" + flag.ToString + "&CampusId=" + campusId + "&boolConflictClassIgnoreRoom=" + boolConflictClassIgnoreRoom.ToString
    '    ScheduleConflictWindow.Visible = True
    '    ScheduleConflictWindow.Windows(0).NavigateUrl = strURL
    '    ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = True
    'End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub BuildList()
        Dim fac As New TranscriptFacade
        Dim dt As New DataTable
        'dgrdProcessedRecords.Columns(8).Visible = True
        GridView1.Columns(8).Visible = True

        'dt = fac.RegisterStudentsInClassesRestrictedByStudentStart(campusId, ddlProgId.SelectedValue.ToString, ddlTermId.SelectedValue.ToString, user)

        dt = fac.StudentsInClassesRestrictedByStudentStart(campusId, ddlProgId.SelectedValue.ToString, ddlTermId.SelectedValue.ToString, AdvantageSession.UserState.UserName, ddlLeadGrp.SelectedValue)
        If dt.Rows.Count > 0 Then
            btnBuildList.Visible = True
            lblRegisterStudents.Visible = True
        Else
            btnBuildList.Visible = False
            lblRegisterStudents.Visible = False
        End If

        Dim dv As New DataView(dt, Nothing, "Program,StartDate,Code,Course,Section,LastName", DataViewRowState.CurrentRows)
        'dgrdProcessedRecords.DataSource = dt
        'dgrdProcessedRecords.DataBind()
        'Cache("[dvStudentList]") = dt
        'Cache.Insert("mydata", dt, Nothing, DateTime.Now.AddHours(12), Cache.NoSlidingExpiration)
        Cache.Insert("mydata", dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        'ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
        'lblRegisterStudents.Visible = True
        'If Not dt Is Nothing Then
        '    If dt.Rows.Count >= 1 Then

        '    End If
        'End If
    End Sub

    Protected Sub btnGetList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetList.Click
        BuildList()
        pnlRegisteredStudents.Visible = False
    End Sub
    'Sub SortData(ByVal sender As Object, ByVal e As DataGridSortCommandEventArgs)
    '    Dim fac As New TranscriptFacade
    '    Dim dt As New DataTable
    '    dgrdProcessedRecords.Columns(8).Visible = True
    '    'dt = fac.RegisterStudentsInClassesRestrictedByStudentStart(campusId, ddlProgId.SelectedValue.ToString, ddlTermId.SelectedValue.ToString, user)
    '    dt = fac.RegisterStudentsInClassesRestrictedByStudentStart(campusId, ddlProgId.SelectedValue.ToString, ddlTermId.SelectedValue.ToString, user)

    '    Dim dv As New DataView(dt, Nothing, e.SortExpression, DataViewRowState.CurrentRows)

    '    dgrdProcessedRecords.DataSource = dt
    '    dgrdProcessedRecords.DataBind()
    'End Sub



    Protected Sub GridView1_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridView1.Sorting
        Dim dv As New DataView(CType(Cache("mydata"), DataTable))
        'dv = Cache("[dvStudentList]")

        'dv = New DataView(ds.Tables(0))

        dv.Sort = e.SortExpression

        GridView1.DataSource = dv
        GridView1.DataBind()
    End Sub



    Private Sub populateLeadGroups()
        ddlLeadGrp.Items.Clear()
        Dim dt As New DataTable
        dt = (New LeadFacade).GetLeadGroups_SP(True, campusId)
        ddlLeadGrp.DataSource = dt
        ddlLeadGrp.DataBind()
        ddlLeadGrp.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddlLeadGrp.SelectedIndex = 0

    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        BindToolTip()
    End Sub
End Class
