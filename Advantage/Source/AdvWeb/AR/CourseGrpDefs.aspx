<%@ Page Language="vb" AutoEventWireup="false" inherits="CourseGrpDefs" CodeFile="CourseGrpDefs.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
    <HEAD>
        <title>Course Group Definitions</title>
        <meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
        <meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <LINK href="../CSS/localhost.css" type="text/css" rel="stylesheet">
		<LINK href="../CSS/systememail.css" type="text/css" rel="stylesheet">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    </HEAD>

	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><IMG src="../images/advantage_course_grp_def.jpg"></td>
					<td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
				</tr>
			</table>
			<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" height="100%" border="0">
				<tr>
					<td class="DetailsFrameTopemail">
						<table id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="MenuFrame" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" Enabled="False" CausesValidation="False"></asp:button><asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" Enabled="False" CausesValidation="False"></asp:button></td>
								
							</tr>
						</table>
						<div style="padding:20px;width:90%;">
						<!--begin content cell -->
						<table class="contenttable" cellSpacing="0" cellPadding="0" width="55%" align="center">
                                            <tr>
                                                <td class="twocolumnlabelcell"><asp:label id="lblCourseGrpDescrip" runat="server" CssClass="Label">Description</asp:label></td>
                                                <td class="twocolumncontentcell"><asp:textbox id="txtCourseGrpDescrip" runat="server" CssClass="TextBox" ReadOnly="True"></asp:textbox></td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell"><asp:label id="lblCredits" runat="server" CssClass="Label">Credits</asp:label></td>
                                                <td class="twocolumncontentcell"><asp:textbox id="txtCredits" runat="server" CssClass="TextBox" ReadOnly="True"></asp:textbox></td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell"><asp:label id="lblHours" runat="server" CssClass="Label">Hours</asp:label></td>
                                                <td class="twocolumncontentcell"><asp:textbox id="txtHours" runat="server" CssClass="TextBox" ReadOnly="True"></asp:textbox></td>
                                            </tr>
                                            <asp:textbox id="txtCourseGrpId" runat="server" Visible="False"></asp:textbox><asp:textbox id="txtCourseGrpDefId" runat="server" CssClass="TextBox" Visible="False"></asp:textbox></table>
                                        	<asp:panel id="PnlHrsCrdts" runat="server" CssClass="Frame" Width="100%">
                                            <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="55%" align="center">
                                                <TR>
                                                    <TD class="spacertables"></TD>
                                                </TR>
                                                <TR>
                                                    <TD class="contentcellheader" noWrap colSpan="2">
                                                        <asp:Label id="Label4" runat="server" CssClass="Label" Font-Bold="True">Selected</asp:Label></TD>
                                                </TR>
                                                <TR>
                                                    <TD class="spacertables"></TD>
                                                </TR>
                                            </TABLE>
                                            <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="55%" align="center">
                                                <TR>
                                                    <TD class="twocolumnlabelcell">
                                                        <asp:Label id="lblCredits1" CssClass="Label" Runat="server">Credits</asp:Label></TD>
                                                    <TD class="twocolumncontentcell">
                                                        <asp:textbox id="txtCredits2" runat="server" CssClass="TextBox" ReadOnly="True"></asp:textbox></TD>
                                                </TR>
                                                <TR>
                                                    <TD class="twocolumnlabelcell">
                                                        <asp:label id="Label1" runat="server" CssClass="Label">Hours</asp:label></TD>
                                                    <TD class="twocolumncontentcell">
                                                        <asp:textbox id="txtHours2" runat="server" CssClass="TextBox" ReadOnly="True"></asp:textbox><BR>
                                                        <BR>
                                                    </TD>
                                                </TR>
                                            </TABLE>
                                        </asp:panel>
                                        <table class="contenttable" cellSpacing="0" cellPadding="0" width="100%">
                                            <tr>
                                                <td class="fivecolumnheader" noWrap><asp:label id="lblAvailCourses" runat="server" CssClass="Labelbold">Available Courses</asp:label></td>
                                                <td class="fivecolumnspacer" noWrap>&nbsp;</td>
                                                <td class="fivecolumnheader" noWrap><asp:label id="Label3" runat="server" CssClass="Labelbold">Available Course Groups</asp:label></td>
                                                <td class="fivecolumnspacer" noWrap>&nbsp;</td>
                                                <td class="fivecolumnheader" noWrap><asp:label id="lblSelected" runat="server" CssClass="Labelbold">Selected</asp:label></td>
                                            </tr>
                                            <tr>
                                                <td class="fivecolumncontent" noWrap><asp:listbox id="lbxAvailCourses" runat="server" AutoPostBack="True" cssClass="listboxes"
                                                        Rows="13"></asp:listbox></td>
                                                <td class="fivecolumnspacer" noWrap>&nbsp;</td>
                                                <td class="fivecolumncontent" noWrap><asp:listbox id="lbxAvailCourseGrps" runat="server" AutoPostBack="True" cssClass="listboxes"
                                                        Rows="13"></asp:listbox></td>
                                                <td class="fivecolumnspacer" noWrap>&nbsp;</td>
                                                <td class="fivecolumncontent" noWrap><asp:listbox id="lbxSelected" runat="server" cssClass="listboxes" Rows="13"></asp:listbox></td>
                                            </tr>
                                            <tr>
                                                <td class="fivecolumnfooter" noWrap><asp:button id="btnRequired" runat="server" CssClass="Button" Width="88px" Text="Required"></asp:button><asp:button id="btnElective" runat="server" CssClass="Button" Text="Elective" Width="80px"></asp:button></td>
                                                <td class="fivecolumnspacer" noWrap>&nbsp;</td>
                                                <td class="fivecolumnfooter" noWrap><asp:button id="btnAdd" runat="server" CssClass="Button" Width="88px" Text="Add"></asp:button><asp:button id="btnDetails" runat="server" CssClass="Button" Text="Details" Width="80px"></asp:button></td>
                                                <td class="fivecolumnspacer" noWrap>&nbsp;</td>
                                                <td class="fivecolumnfooter" noWrap><asp:button id="btnRemove" runat="server" CssClass="Button" Width="70px" Text="Remove"></asp:button> <asp:button id="btnUp" Width="70px" runat="server" CssClass="Button" Text="Up"></asp:button>
                                                    <asp:button id="btnDown" CssClass="Button" Width="70px" runat="server" Text="Down"></asp:button></td>
                                            </tr>
                                        </table>
										<!--end content cell -->
										<asp:TextBox id="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
										<asp:TextBox id="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
						</div>
					</td>
				</tr>
			</table>

		</form>
	</body>

</HTML>
