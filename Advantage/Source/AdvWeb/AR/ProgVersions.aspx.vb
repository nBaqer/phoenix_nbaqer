Imports Fame.Common
Imports Fame.DataAccessLayer
Imports System.Data
Imports System.Threading.Tasks
Imports Advantage.AFA.Integration

Imports Fame.Advantage.Api.Library.AcademicRecords
Imports Fame.Advantage.Api.Library.Models
Imports Fame.Advantage.Api.Library.Models.AcademicRecords
Imports Fame.Advantage.Api.Library.Models.Common
Imports Fame.Advantage.Api.Library.SystemCatalog
Imports Fame.Advantage.Common
Imports Fame.Advantage.DataAccess.LINQ
Imports Fame.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports Fame.Advantage.Domain.MultiTenant.Infrastructure.API
' Imports FAME.Advantage.Domain.Student.Enrollments
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports Fame.AdvantageV1.Common.Tables
Imports Fame.Integration.Adv.Afa.Messages.WebApi.Entities
Imports FluentAssertions
'Imports Fame.Integration.Adv.Afa.Messages.Bus.Entities

Partial Class ProgVersions
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblPrgTypeId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPrgTypeId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblFullTime As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Public PrgVerInstructionTypeTable As DataTable
    Public InstructionTypeCodes As DataTable
    Public IsNonTermProgram As Boolean
    Public IsNonStandardTermProgram As Boolean
    Public IsClockHour As Boolean

    Private Const toolTiptxtBillingMethodDescrip = "Please setup the Charging Method in the Maintenance Module under Student Accounts\Charging Methods"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Dim linkobj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected resourceId As String
    Private validatetxt As Boolean = False
    Private validatetxt1 As Boolean = False
    Private validatetxt2 As Boolean = False
    Dim tokenResponse As TokenResponse
    Private Property BillingMethodId As String
        Get
            If (Me.ViewState("BillingMethodId") Is Nothing) Then
                Return False
            Else
                Return DirectCast(Me.ViewState("BillingMethodId"), String)
            End If
        End Get
        Set(value As String)
            Me.ViewState.Add("BillingMethodId", value)
        End Set
    End Property
    Protected decHoursSum As Decimal = 0.0
    Protected blnInEditMode As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings

#Region "Private Methods"

    Private Sub BuildSession()
        With ddlRegentCode
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("CurrCode", txtPrgVerId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub PopulateSchedulingMethods()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New ProgVerFacade

        ' Bind the Dataset to the CheckBoxList
        ddlSchedMethodId.DataSource = facade.GetSchedulingMethods()
        ddlSchedMethodId.DataTextField = "Descrip"
        ddlSchedMethodId.DataValueField = "SchedMethodId"
        ddlSchedMethodId.DataBind()
        ddlSchedMethodId.Items.Insert(0, New ListItem("Select", 0))
        ddlSchedMethodId.SelectedIndex = 0
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    If CType(ctl, DropDownList).Items.Count > 0 Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                    End If
                End If
            Next

            'Need to explicitly set to 0 because this control is hidden,
            'and it would not get cleared on above for loop.
            txtTardiesMakingAbsence.Text = 0

            txtLTHalfTime.Text = 0
            txtHalfTime.Text = 0
            txtThreeQuartTime.Text = 0

            'Hide panel that contains the TardiesMakingAbsence textbox.
            pnlTardies.Visible = chkTrackTardies.Checked

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub DisplayWarningMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayWarningInMessageBox(Me.Page, errorMessage)
    End Sub


    Private Sub DisplaySuccessMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim dv2 As New DataView
        Dim objListGen As New DataListGenerator
        Dim sStatusId As String
        Dim facade As New ProgVerFacade
        Dim isTitleIVFlag As String
        Dim dsProgVersion As New DataSet
        Dim isFameApproved As String
        Dim termSubEqualInLength As String
        Dim fameTitleIvService As Object = MyAdvAppSettings.AppSettings("FAMETitleIVService")

        ds2 = objListGen.StatusIdGenerator()
        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}
        If (radStatus.SelectedItem.Text = "Active") Then   'Show Active Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays active records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "PrgVerDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        ElseIf (radStatus.SelectedItem.Text = "Inactive") Then   'Show InActive Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "PrgVerDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        Else  'Show All
            'ds = objListGen.SummaryListGeneratorSort()
            ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows)
            dv2 = dv
        End If

        GetProgramVersionDetails(sStatusId, facade, fameTitleIvService)

        With dlstProgVer
            .DataSource = dv2
            .DataBind()
        End With

        dlstProgVer.SelectedIndex = -1


    End Sub

    ''' <summary>
    ''' This method gets the program details from program version table.
    ''' </summary>
    ''' <param name="sStatusId"></param>
    ''' <param name="facade"></param>
    ''' <param name="fameTitleIvService"></param>
    Private Sub GetProgramVersionDetails(sStatusId As String, facade As ProgVerFacade, fameTitleIvService As Object)
        Dim dsProgVersion As DataSet
        Dim termSubEqualInLength As String = String.Empty

        dsProgVersion = facade.GetProgVersionsByProgramVerId(txtPrgVerId.Text, campusId)

        If Not String.IsNullOrEmpty(ddlStatusId.SelectedItem.Value) Then
            sStatusId = ddlStatusId.SelectedItem.Value
        End If

        If dsProgVersion.Tables.Count > 0 And dsProgVersion.Tables(0).Rows.Count > 0 Then
            Dim dvProgVersion As New DataView(dsProgVersion.Tables(0), "StatusId = '" & sStatusId & "'", "PrgVerDescrip", DataViewRowState.CurrentRows)

            If (Not dvProgVersion(0)(5) Is Nothing And Not dvProgVersion(0)(5) Is DBNull.Value) Then
                txtAllowExcusAbsPerPayPrd.Text = dvProgVersion(0)(5).ToString()
            End If

            If fameTitleIvService = False Then

                If (Not dvProgVersion(0)(3) Is Nothing And Not dvProgVersion(0)(3) Is DBNull.Value) Then
                    chkTitleIV.Checked = CType(dvProgVersion(0)(3), Boolean)
                End If

                If ViewState("IsNonStandardTermProgram") = True And chkTitleIV.Checked = True Then
                    spnTermLength.Visible = True
                    termLengthValidtor.Enabled = True
                Else
                    spnTermLength.Visible = False
                    termLengthValidtor.Enabled = False
                End If
            Else
                If (Not dvProgVersion(0)(3) Is Nothing And Not dvProgVersion(0)(3) Is DBNull.Value) Then
                    chkTitleIVFameApproved.Checked = CType(dvProgVersion(0)(3), Boolean)
                End If

                If ViewState("IsNonStandardTermProgram") = True And chkTitleIVFameApproved.Checked = True Then
                    spnTermLength.Visible = True
                    termLengthValidtor.Enabled = True
                Else
                    spnTermLength.Visible = False
                    termLengthValidtor.Enabled = False
                End If
            End If

            If (Not dvProgVersion(0)(6) Is Nothing And Not dvProgVersion(0)(6) Is DBNull.Value) Then
                termSubEqualInLength = dvProgVersion(0)(6).ToString()
            End If
            If Not String.IsNullOrEmpty(termSubEqualInLength) Then
                If termSubEqualInLength = "True" Then
                    ddlTermLength.SelectedValue = "1"
                Else
                    ddlTermLength.SelectedValue = "0"
                End If
            Else
                ddlTermLength.SelectedValue = ""
            End If
            If (Not dvProgVersion(0)(7) Is Nothing And Not dvProgVersion(0)(7) Is DBNull.Value) Then
                ddlCalculationPeriodTypeId.SelectedValue = Convert.ToString(dvProgVersion(0)(7))
            End If
            If fameTitleIvService = False Then
                ddlCalculationPeriodTypeId.Enabled = True
            Else
                If AdvantageSession.UserState.IsUserSupport Then
                    ddlCalculationPeriodTypeId.Enabled = True
                Else
                    ddlCalculationPeriodTypeId.Enabled = False
                End If
            End If

            If chkTitleIV.Style.Item("display") = "block" And chkTitleIV.Checked = True Then
                R2T4CalculationPeriodValidator.Enabled = True
                spnCalculationPeriodTypeId.Visible = True
            ElseIf chkTitleIVFameApproved.Style.Item("display") = "block" And chkTitleIVFameApproved.Checked = True Then
                R2T4CalculationPeriodValidator.Enabled = True
                spnCalculationPeriodTypeId.Visible = True
            Else
                R2T4CalculationPeriodValidator.Enabled = False
                spnCalculationPeriodTypeId.Visible = False
            End If

            If (Not dsProgVersion.Tables(0).Rows(0)("ProgramRegistrationType") Is Nothing) Then
                If (Not dsProgVersion.Tables(0).Rows(0)("ProgramRegistrationType") Is DBNull.Value) Then
                    If (dsProgVersion.Tables(0).Rows(0)("ProgramRegistrationType").ToString() = CType(Enums.ProgramRegistrationType.ByProgram, Integer).ToString()) Then
                        ddlRegistrationType.Items(1).Enabled = True

                        Me.ShowGradeScaleDropdown(True)

                        BindGradeScale()
                    End If

                    ddlRegistrationType.SelectedValue = dsProgVersion.Tables(0).Rows(0)("ProgramRegistrationType").ToString()
                Else
                    ddlRegistrationType.SelectedValue = "0"
                    Me.ShowGradeScaleDropdown(False)
                End If
                'The IsRegistrationByProgram enables a program version to take care of the Student Registration upon enrollment. When a Program Version is set to true for the IsRegistrationByProgram 
                'A term and class section will be automatically be created. The term and class section won't be editable on the page.
            End If

            If (Not dsProgVersion.Tables(0).Rows(0)("GradeScaleId") Is Nothing) Then
                If (Not dsProgVersion.Tables(0).Rows(0)("GradeScaleId") Is DBNull.Value and ddlGradeScales.Items.FindByValue(dsProgVersion.Tables(0).Rows(0)("GradeScaleId").ToString()) Isnot nothing) Then
                    ddlGradeScales.SelectedValue = dsProgVersion.Tables(0).Rows(0)("GradeScaleId").ToString()
                    ShowGradeScaleDropdown(True)
                End If
            End If
        Else
            ResetCampusProgVersionsData()
        End If
    End Sub

    Private Sub BindGradeScale()
        If (ddlGrdSystemId.SelectedIndex > 0) Then
            Dim gradeScaleRequest As New GradeScaleRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Dim gradeScales As IList(Of ListItem(Of String, Guid)) = gradeScaleRequest.GetGradeScales(ddlGrdSystemId.SelectedValue)
            If (Not gradeScales Is Nothing) Then
                ddlGradeScales.DataSource = gradeScales
                ddlGradeScales.DataTextField = "Text"
                ddlGradeScales.DataValueField = "Value"
                ddlGradeScales.DataBind()
            Else
                ddlGradeScales.DataSource = Nothing
                ddlGradeScales.DataBind()
            End If
            ddlGradeScales.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select", Nothing))
        End If
    End Sub
    Private Sub ddlRegistrationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegistrationType.SelectedIndexChanged
        If (ddlRegistrationType.SelectedValue = CType(Enums.ProgramRegistrationType.ByProgram, Integer).ToString()) Then
            BindGradeScale()
            Me.ShowGradeScaleDropdown(True)
        Else
            Me.ShowGradeScaleDropdown(False)
        End If
    End Sub

    Private Sub ddlGrdSystemId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrdSystemId.SelectedIndexChanged
        If (ddlRegistrationType.SelectedValue = CType(Enums.ProgramRegistrationType.ByProgram, Integer).ToString()) Then
            BindGradeScale()
            Me.ShowGradeScaleDropdown(True)
        Else
            Me.ShowGradeScaleDropdown(False)
        End If
    End Sub

    Private Sub ShowGradeScaleDropdown(ByVal shown As Boolean)
        panelGradeScaleLabel.Visible = shown
        panelGradeScalesDropdown.Visible = shown
    End Sub

    Private Sub SetFeesHyperlink(ByVal prgVerId As String)

        '   enable Set Up Fees button
        lbtSetUpFees.Enabled = True

        '   set Style to Selected Item
        CommonWebUtilities.SetStyleToSelectedItem(dlstProgVer, prgVerId, ViewState)

    End Sub
    Private Function GetWebConfigValueForAllowAddingCoursesToProgramVersionsAfterStudentsHaveBeenEnrolled() As Boolean

        'return boolean value
        Return MyAdvAppSettings.AppSettings("AllowAddingCoursesToProgramVersionsAfterStudentsHaveBeenEnrolled")

    End Function

    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, True, False, String.Empty))

        'Education Level DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrgGrpId, AdvantageDropDownListName.Program_Groups, Nothing, True, False, String.Empty))

        'States DDL
        'ddlList.Add(New AdvantageDDLDefinition(ddlCampGrpId, AdvantageDropDownListName.CampGrps, Nothing, True, False, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlDeptId, AdvantageDropDownListName.Departments, Nothing, True, False, String.Empty))

        'Shifts DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlBillingMethodId, AdvantageDropDownListName.Billing_Methods, Nothing, True, False, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlGrdSystemId, AdvantageDropDownListName.Grade_Systems, Nothing, True, False, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlProgTypId, AdvantageDropDownListName.Program_Types, Nothing, True, False, String.Empty))

        'States DDL
        'ddlList.Add(New AdvantageDDLDefinition(ddlTestingModelId, AdvantageDropDownListName.TestingModels, Nothing, True, False, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlDegreeId, AdvantageDropDownListName.Degrees, Nothing, True, False, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSAPId, AdvantageDropDownListName.SAPs, Nothing, True, False, String.Empty))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlTuitionEarningId, AdvantageDropDownListName.Earning_Method, Nothing, True, False, String.Empty))

        If (MyAdvAppSettings.AppSettings("TrackFASAP") = "True") Then
            ddlList.Add(New AdvantageDDLDefinition(ddlFASAPId, AdvantageDropDownListName.FASAPs, Nothing, True, True, String.Empty))
        End If

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlUnitTypeId, AdvantageDropDownListName.AttUnitTypes, Nothing, True, False, "None")) ' AdvantageCommonValues.NoneGuid

        'R2T4CalculationPeriod DDL
        GetR2T4CalculationPeriodTypes()

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        If ddlUnitTypeId.SelectedItem.Selected = False Or ddlUnitTypeId.SelectedItem.Text = "Select" Then
            ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.NoneGuid
            ddlUnitTypeId.SelectedItem.Text = "None"
        End If

        'Build Campus Groups based on the Campus group selected for Parent Program
        GetCampusGroupsByProgramNew("")
    End Sub

    Private Function GetTable() As DataTable
        Dim tbl As New DataTable
        tbl.Columns.Add(New DataColumn("PrgVerInstructionTypeId", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("PrgVerID", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("InstructionTypeId", GetType(System.Guid)))
        tbl.Columns.Add(New DataColumn("InstructionTypeDescrip", GetType(String)))
        tbl.Columns.Add(New DataColumn("Hours", GetType(Decimal)))
        tbl.Columns.Add(New DataColumn("CmdType", GetType(Integer)))
        Return tbl
    End Function

    Private Function IsPrgVerAttTypeMatchWithCourses(ByVal prgverid As String, ByVal unitTypeid As String) As String
        Dim facade As New ProgVerFacade
        Dim rtn As String = String.Empty
        Dim dt As DataTable = facade.IsPrgVerAttTypeMatchWithCourses(prgverid, unitTypeid)
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                rtn = rtn & "," & dt.Rows(i)(0)
            Next
        End If
        Return rtn

    End Function

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub

    Private Function ValidateStatus(ByVal prgverid As String) As Boolean
        'Validate is Program Version is changing to Active the The Program should be Active
        'is not give an message. do not allow the change.
        Dim IsValid As Boolean = False


        Return IsValid
    End Function

    ''' <summary>
    ''' Initialize the campus program version model
    ''' </summary>
    ''' <returns></returns>
    Private Function InitializeCampusPrgVersion() As Dictionary(Of String, Object)
        Dim campusPrgVersion = New Dictionary(Of String, Object)
        With campusPrgVersion
            .Add("PrgVerId", txtPrgVerId.Text)
            .Add("CampusId", campusId)
            .Add("IsSelfPaced", Nothing)
            .Add("CalculationPeriodTypeId", ddlCalculationPeriodTypeId.SelectedItem.Value)
            .Add("ModUser", HttpContext.Current.Session("UserName").ToString())
            .Add("ModDate", Date.Now)
        End With

        If ddlTermLength.SelectedItem.Value = "" Then
            campusPrgVersion.Add("TermSubEqualInLen", Nothing)
        ElseIf ddlTermLength.SelectedItem.Value = "0" Then
            campusPrgVersion.Add("TermSubEqualInLen", False)
        Else
            campusPrgVersion.Add("TermSubEqualInLen", True)
        End If

        If txtAllowExcusAbsPerPayPrd.Text <> "" Then
            campusPrgVersion.Add("AllowExcusAbsPerPayPrd", Convert.ToDouble(txtAllowExcusAbsPerPayPrd.Text))
        Else
            campusPrgVersion.Add("AllowExcusAbsPerPayPrd", Nothing)
        End If

        Dim fameTitleIvService As Object = MyAdvAppSettings.AppSettings("FAMETitleIVService")

        If fameTitleIvService = True Then
            campusPrgVersion.Add("IsTitleIV", chkTitleIVFameApproved.Checked)
            campusPrgVersion.Add("IsFAMEApproved", True)
        Else
            campusPrgVersion.Add("IsTitleIV", chkTitleIV.Checked)
            campusPrgVersion.Add("IsFAMEApproved", False)
        End If

        Return campusPrgVersion
    End Function

    ''' <summary>
    ''' This method resets the controls for campus specific to default values.
    ''' </summary>
    Private Sub ResetCampusProgVersionsData()
        txtAllowExcusAbsPerPayPrd.Text = ""
        chkTitleIV.Checked = False
        chkTitleIVFameApproved.Checked = False
        ddlTermLength.SelectedValue = ""
        spnTermLength.Visible = False
        termLengthValidtor.Enabled = False
        termLengthValidtor.Enabled = False
        ddlCalculationPeriodTypeId.SelectedValue = ""
        ddlSelfPaced.SelectedValue = ""
        spnCalculationPeriodTypeId.Visible = False
        R2T4CalculationPeriodValidator.Enabled = False
        spnSelfPaced.Visible = False
        SelfPacedValidator.Enabled = False
    End Sub

#End Region
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        If (MyAdvAppSettings.AppSettings("TrackFASAP") = "False") Then
            lblFASAPId.Style.Add("display", "none")
            ddlFASAPId.Style.Add("display", "none")
            FASAPId.Enabled = False
        Else
            FASAPId.Enabled = True
            lblFASAPId.Style.Add("display", "block")
            ddlFASAPId.Style.Add("display", "block")
        End If

        termLengthValidtor.Enabled = False
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ViewState("IsUserSa") = advantageUserState.IsUserSA
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = CInt(m_Context.Items("ResourceId"))

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        Dim sStatusId As String
        Dim facade As New ProgVerFacade
        Dim campusProgVerFacade As New CampusProgVerFacade
        Dim fac As New UserSecurityFacade


        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        tokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        txtCampusId.Text = campusId

        Dim Resource As String

        Resource = "Setup Schedules"
        'Try
        If Not Page.IsPostBack Then
            ShowGradeScaleDropdown(False)

            'txtProgId.Text = "AFEB62D4-2DEB-4E05-977D-B2C2EE3F1E57"
            If Request.QueryString("Program") <> "" Then
                ViewState("ProgDesc") = Request.QueryString("Program")
                txtProgDescrip.Text = ViewState("ProgDesc")
            End If

            If Request.QueryString("ProgId") <> "" Then
                txtProgId.Text = Request.QueryString("ProgId")
                ViewState("ProgId") = txtProgId.Text
            End If

            If Request.QueryString("ProgType") <> "" Then
                txtProgType.Text = Request.QueryString("ProgType")
            End If
            'txtProgDescrip.Text = "Program 200"
            objCommon.PageSetupPopups(Form1, "NEW")

            'Populate the trig unit types and trig offset types
            'PopulateSchedulingMethods()

            'Set the text box to a new guid
            txtPrgVerId.Text = System.Guid.NewGuid.ToString

            'build dropdownlists
            BuildDropDownLists()

            'keep appart BillingMethodId to validate it before change it
            If (ddlBillingMethodId.SelectedIndex > 0) Then
                BillingMethodId = ddlBillingMethodId.SelectedValue
                txtBillingMethodDescrip.Text = ddlBillingMethodId.SelectedItem.Text
                txtBillingMethodDescrip.ToolTip = String.Empty
            Else
                txtBillingMethodDescrip.Text = String.Empty
                txtBillingMethodDescrip.ToolTip = toolTiptxtBillingMethodDescrip
            End If

            ds = facade.GetProgVersionsByProgram(ViewState("ProgId")) 'facade.GetProgVersions(ViewState("ProgId"), campusId)
            'Generate the status id
            ds2 = objListGen.StatusIdGenerator()

            'Build Campus Groups based on the Campus group selected for Parent Program
            GetCampusGroupsByProgramNew("")

            'Set up the primary key on the datatable
            ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

            Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
            sStatusId = row("StatusId").ToString()

            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "PrgVerDescrip", DataViewRowState.CurrentRows)

            With dlstProgVer
                .DataSource = dv
                .DataBind()
            End With

            'PopulateDataList(ds)

            'Disable the new and delete buttons
            objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Linkbutton1.Enabled = False
            Linkbutton2.Enabled = False
            lbSetSchedules.Enabled = False
            'ddlUnitTypeId.Enabled = True
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                BuildSession()
            End If


            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("PrgVerCampGrpId") = ds.Tables(0).Rows.Item(0).Item("CampGrpId").ToString
            End If
            InstructionTypeCodes = (New InstructionTypeFacade).GetInstructionTypesByStatus("true")
            ViewState("InstructionTypeCodes") = InstructionTypeCodes
            decHoursSum = 0.0
            txtHoursSum.Text = "0.0"
            txtOrigHours.Text = "0.0"
            ViewState("InEditMode") = blnInEditMode
            ViewState("GridChg") = 0
            If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                lblInstructionTypeHours.Visible = True
                grdPrgVerInstructionType.Visible = True
                chkUseTimeClock.Visible = False
            Else
                lblInstructionTypeHours.Visible = False
                grdPrgVerInstructionType.Visible = False
                chkUseTimeClock.Visible = True
            End If
            Dim dt1 As New DataTable
            dt1 = GetTable.Clone()
            ViewState("PrgVerInstructionTypeTable") = dt1
            PrgVerInstructionTypeTable = ViewState("PrgVerInstructionTypeTable")
            grdPrgVerInstructionType.MasterTableView.ClearEditItems()
            grdPrgVerInstructionType.DataSource = PrgVerInstructionTypeTable
            grdPrgVerInstructionType.Rebind()

            InitButtonsForLoad()

            Dim campusPrgVersions = campusProgVerFacade.GetCampusPrgVersionData(txtPrgVerId.Text, campusId)

            Dim fameTitleIvService As Object = MyAdvAppSettings.AppSettings("FAMETitleIVService")
            If (fameTitleIvService = False) Then
                chkTitleIV.Style.Add("display", "block")
                chkTitleIVFameApproved.Style.Add("display", "none")
                If txtProgType.Text = "Clock Hour" Then
                    txtAllowExcusAbsPerPayPrd.Visible = True
                    txtAllowExcusAbsPerPayPrd.Enabled = True
                    lblExcusedAbsence.Visible = True
                End If
            Else
                chkTitleIV.Style.Add("display", "none")
                chkTitleIVFameApproved.Style.Add("display", "block")
                If txtProgType.Text = "Clock Hour" Then
                    If AdvantageSession.UserState.IsUserSupport Then
                        lblExcusedAbsence.Visible = True
                        txtAllowExcusAbsPerPayPrd.Visible = True
                        txtAllowExcusAbsPerPayPrd.Enabled = True
                    Else
                        txtAllowExcusAbsPerPayPrd.Visible = True
                        txtAllowExcusAbsPerPayPrd.Enabled = False
                        lblExcusedAbsence.Visible = True
                    End If
                End If
                If AdvantageSession.UserState.IsUserSupport Then
                    ddlCalculationPeriodTypeId.Enabled = True
                    chkTitleIVFameApproved.Enabled = True
                Else
                    ddlCalculationPeriodTypeId.Enabled = False
                    chkTitleIVFameApproved.Enabled = False
                End If
            End If

            IsNonTermProgram = facade.GetProgramType(txtPrgVerCode.Text.Trim(), txtProgDescrip.Text.Trim, ddlCampGrpId.SelectedValue, Convert.ToInt32(AdvantageSystemAcademicCalendars.NonTerm))
            IsNonStandardTermProgram = facade.GetProgramType(txtPrgVerCode.Text.Trim(), txtProgDescrip.Text.Trim, ddlCampGrpId.SelectedValue, Convert.ToInt32(AdvantageSystemAcademicCalendars.NonStandardTerm))
            IsClockHour = facade.GetProgramType(txtPrgVerCode.Text.Trim(), txtProgDescrip.Text.Trim, ddlCampGrpId.SelectedValue, Convert.ToInt32(AdvantageSystemAcademicCalendars.ClockHour))
            ViewState("IsNonTermProgram") = IsNonTermProgram
            ViewState("IsNonStandardTermProgram") = IsNonStandardTermProgram
            ViewState("IsClockHourProgram") = IsClockHour


            If IsNonTermProgram = True Then
                ddlSelfPaced.Visible = lblSelfPaced.Visible = True
                If fameTitleIvService = False Then
                    ddlSelfPaced.Enabled = True
                    If chkTitleIVFameApproved.Checked = True Or chkTitleIV.Checked = True Then
                        SelfPacedValidator.Enabled = True
                        spnSelfPaced.Visible = True
                    Else
                        SelfPacedValidator.Enabled = False
                        spnSelfPaced.Visible = False
                    End If
                    ddlTermLength.Enabled = True
                Else
                    If AdvantageSession.UserState.IsUserSupport Then
                        ddlSelfPaced.Enabled = True
                        termLengthValidtor.Enabled = True
                        If chkTitleIVFameApproved.Checked = True Or chkTitleIV.Checked = True Then
                            SelfPacedValidator.Enabled = True
                            spnSelfPaced.Visible = True
                        Else
                            SelfPacedValidator.Enabled = False
                            spnSelfPaced.Visible = False
                        End If
                    Else
                        ddlSelfPaced.Enabled = False
                        termLengthValidtor.Enabled = False
                        SelfPacedValidator.Enabled = False
                        spnSelfPaced.Visible = False
                    End If
                End If
            Else
                ddlSelfPaced.Visible = False
                lblSelfPaced.Visible = False
            End If

            Dim isTitleIvChecked As Boolean = chkTitleIVFameApproved.Checked And chkTitleIV.Checked
            R2T4CalculationPeriodValidator.Enabled = isTitleIvChecked
            spnCalculationPeriodTypeId.Visible = isTitleIvChecked

            If IsNonStandardTermProgram = True Then
                ddlTermLength.Visible = True
                lblTermLength.Visible = True
                If fameTitleIvService = False Then
                    ddlTermLength.Enabled = True
                Else
                    If AdvantageSession.UserState.IsUserSupport Then
                        ddlTermLength.Enabled = True
                    Else
                        ddlTermLength.Enabled = False
                    End If
                End If
            Else
                ddlTermLength.Visible = False
                lblTermLength.Visible = False
                spnTermLength.Visible = False
            End If

        Else
            objCommon.PageSetupPopups(Form1, "EDIT")

            ' 08/08/2011 JRobinson Clock Hour Project 
            InstructionTypeCodes = ViewState("InstructionTypeCodes")
            decHoursSum = CType(txtHoursSum.Text, Decimal)
            blnInEditMode = ViewState("InEditMode")
            InitButtonsForEdit()

        End If


        If txtPrgVerId.Text <> "" AndAlso dlstProgVer.SelectedValue <> "" Then

            Dim isByProgram = (New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).IsProgramRegistrationType(txtPrgVerId.Text)
            lbSetUpExamSequence.Enabled = isByProgram
            lbSetUpExamSequence.Visible = isByProgram
        End If

        'Special case: Do not allow more than 3 digits on Tardies Making An Absence
        If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) = "ModuleStart" Then
            lbSetUpCourses.Visible = False
        Else
            lbSetUpCourses.Visible = True
        End If
        txtTardiesMakingAbsence.MaxLength = 3
    End Sub

    ''' <summary>
    ''' This event is used to handle the Title IV checkbox change.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub chkTitleIV_OnCheckedChanged(sender As Object, e As EventArgs)
        If chkTitleIV.Checked = True Then
            spnSelfPaced.Visible = True
            SelfPacedValidator.Enabled = True
            R2T4CalculationPeriodValidator.Enabled = True
            spnCalculationPeriodTypeId.Visible = True
            If ViewState("IsNonStandardTermProgram") = True Then
                spnTermLength.Visible = True
                termLengthValidtor.Enabled = True
            End If
        Else
            spnSelfPaced.Visible = False
            SelfPacedValidator.Enabled = False
            R2T4CalculationPeriodValidator.Enabled = False
            spnCalculationPeriodTypeId.Visible = False
            If ViewState("IsNonStandardTermProgram") = True Then
                spnTermLength.Visible = False
                termLengthValidtor.Enabled = False
            End If
        End If
    End Sub

    ''' <summary>
    ''' This event is used to handle the Fame Approved Title IV checkbox change.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub chkTitleIVFameApproved_OnCheckedChanged(sender As Object, e As EventArgs)
        If chkTitleIVFameApproved.Checked = True Then
            spnSelfPaced.Visible = True
            SelfPacedValidator.Enabled = True
            R2T4CalculationPeriodValidator.Enabled = True
            spnCalculationPeriodTypeId.Visible = True
            If ViewState("IsNonStandardTermProgram") = True Then
                spnTermLength.Visible = True
                termLengthValidtor.Enabled = True
            End If
        Else
            spnSelfPaced.Visible = False
            SelfPacedValidator.Enabled = False
            R2T4CalculationPeriodValidator.Enabled = False
            spnCalculationPeriodTypeId.Visible = False
            If ViewState("IsNonStandardTermProgram") = True Then
                spnTermLength.Visible = False
                termLengthValidtor.Enabled = False
            End If
        End If
    End Sub

    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Dim facade As New ProgVerFacade
        lbtSetUpFees.Enabled = False
        ''Added by Saraswathi Lakshmanan on mAy 05 2010
        ''For default charges 
        lbSetUpDefaultCharges.Enabled = False
        lbSetUpExamSequence.Enabled = False
        Try
            ClearRHS()
            objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtPrgVerId.Text = System.Guid.NewGuid.ToString
            txtProgDescrip.Text = ViewState("ProgDesc")
            ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))
            PopulateDataList(ds)


            ' 08/08/2011 JRobinson Clock Hour Project 
            ViewState("PrgVerInstructionTypeTable") = GetTable.Clone()
            PrgVerInstructionTypeTable = ViewState("PrgVerInstructionTypeTable")
            grdPrgVerInstructionType.DataSource = PrgVerInstructionTypeTable
            grdPrgVerInstructionType.Rebind()
            If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                lblInstructionTypeHours.Visible = True
                grdPrgVerInstructionType.Visible = True
            Else
                lblInstructionTypeHours.Visible = False
                grdPrgVerInstructionType.Visible = False
            End If
            decHoursSum = 0.0
            txtHoursSum.Text = "0.0"
            txtOrigHours.Text = "0.0"
            blnInEditMode = False
            ViewState("InEditMode") = blnInEditMode
            ViewState("GridChg") = 0

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim facade As New ProgVerFacade
        Dim campusProgVerFacade As New CampusProgVerFacade
        Dim msg As String
        Dim progFac As New ProgFacade
        Dim rtnCourse As String = String.Empty
        Dim doesCampusPrgVersionExists As Boolean
        If validatetxt = True OrElse validatetxt1 = True Then
            Exit Sub
        End If

        If ddlUnitTypeId.SelectedItem.Selected = False Or ddlUnitTypeId.SelectedItem.Text = "Select" Then
            ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.NoneGuid
            ddlUnitTypeId.SelectedItem.Text = "None"
        End If

        If Not (ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.PresentAbsentGuid Or
                    ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.MinutesGuid Or
                    ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.ClockHoursGuid Or
                    ddlUnitTypeId.SelectedItem.Text.ToLower() = "clock hours") And
                    chkTrackTardies.Checked Then
            DisplayErrorMessage("Please select an Attendance Unit Type or deselect Track Tardies.")
            Exit Sub
        End If

        If (ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.PresentAbsentGuid Or
                   ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.MinutesGuid Or
                   ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.ClockHoursGuid Or
                   ddlUnitTypeId.SelectedItem.Text.ToLower() = "clock hours") Then
            'If Not facade.IsPrgVerAttTypeMatchWithCourses(txtPrgVerId.Text, ddlUnitTypeId.SelectedValue) Then
            '    DisplayErrorMessage("Please select an Attendance Unit Type that matches with the courses.")
            '    Exit Sub
            'End If
            rtnCourse = IsPrgVerAttTypeMatchWithCourses(txtPrgVerId.Text, ddlUnitTypeId.SelectedValue)
            If rtnCourse <> String.Empty Then
                DisplayErrorMessage("The below courses attendance type does not match." & vbCrLf & rtnCourse.Substring(1, rtnCourse.Length - 1))
                Exit Sub
            End If


        End If

        'Do not allow to save a Prog. Version that does not track attendance 
        'with a SAP policy that accounts for attendance.
        If Not (ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.PresentAbsentGuid Or
                    ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.MinutesGuid Or
                    ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.ClockHoursGuid Or
                    ddlUnitTypeId.SelectedItem.Text.ToLower() = "clock hours") Then
            If (New SAPFacade).GetSAPPolicyMinAttendanceValue(ddlSAPId.SelectedItem.Value) > 0 Then
                DisplayErrorMessage("The selected SAP Policy tracks attendance." & vbCr &
                                    "Select an Attendance Unit Type.")
                Exit Sub
            End If
        End If

        'Do not allow to change the Attendance Unit Type if attendance has already been posted
        If txtUnitTypeId.Text <> ddlUnitTypeId.SelectedValue And
                facade.DoesProgVerHasAttendanceRecords(txtPrgVerId.Text) Then
            'If txtUnitTypeId.Text = "" Then
            '    DisplayErrorMessage("You cannot change the attendance unit type because there are attendance records related to the courses of the program version definition.")
            '    Exit Sub
            'Else
            If Not facade.DoesProgVerHasSameAttenddanceTypeAsCourse(txtPrgVerId.Text, ddlUnitTypeId.SelectedValue) Then
                DisplayErrorMessage("You cannot change the attendance unit type because there are attendance records related to this program version.")
                Exit Sub
            End If
            'End If

        End If

        '   All versions of a program must have the same units of Terms,Hours,weeks,credits.
        If CheckForDupInstructionTypes("") = True Then
            DisplayErrorMessage("Duplicate Instruction Type Found")
            Exit Sub
        End If
        ' 08/11/2011 JRobinson Clock Hour Project - validate PrgVerInstructionTypes grid before saving
        SumHours()
        If CheckHoursSumValidateGreater() = True Then
            DisplayErrorMessage("The sum of all Instruction Hours cannot exceed the Prg Version Hours")
            Exit Sub
        End If
        '08/20/2012 JTorres DE11070 -- Validate if PrgVersion change to Active  the Program should be Active
        If ddlStatusId.SelectedItem.ToString() = "Active" Then
            'Validate if Program  have  Program Versions Active  
            Dim boolThisPrgVersionBelowToInactiveProgram As Boolean = False
            boolThisPrgVersionBelowToInactiveProgram = (New ProgVerFacade).PrgVersionBelowToInactiveProgram(txtProgId.Text)
            If boolThisPrgVersionBelowToInactiveProgram = True Then
                msg = "Program version cannot be made 'Active' while Program status is 'Inactive'. "
                msg &= "Change Program status to Active first, save the change, then you can activate the program version. "
                DisplayErrorMessage(msg)
                Exit Sub
            End If
        End If

        If ViewState("GridChg") = 1 Then
            If CheckHoursSumValidateLessThan() = True Then
                DisplayErrorMessage("The sum of all Instruction Hours must equal the Prg Version Hours")
                Exit Sub
            End If
        End If
        Dim gradeScaled As Guid?
        If (ddlRegistrationType.SelectedValue = CType(Enums.ProgramRegistrationType.ByProgram, Integer).ToString()) Then
            If (ddlGradeScales.SelectedIndex = 0) Then
                DisplayWarningMessage("Registration Type by Program was not set, missing required parameter Grade Scale.")
                ShowGradeScaleDropdown(True)
                Exit Sub
            Else
                gradeScaled = Guid.Parse(ddlGradeScales.SelectedValue)
            End If
        End If
        Try
            If txtTardiesMakingAbsence.Text = "" Then txtTardiesMakingAbsence.Text = "0"
            If ViewState("MODE") = "NEW" Then

                If txtPrgVerCode.Text <> "" And txtPrgVerDescrip.Text <> "" Then
                    If facade.DoesProgVerCodeAlreadyExists(txtPrgVerCode.Text.Trim(), txtPrgVerDescrip.Text.Trim, ddlCampGrpId.SelectedValue) Then
                        DisplayErrorMessage("Please give a different Program Version Code or Program Version Definition")
                        Exit Sub
                    End If
                    doesCampusPrgVersionExists = campusProgVerFacade.DoesCampusProgVerAlreadyExists(txtPrgVerId.Text.Trim, campusId)
                End If

                Dim fameTitleIvServiceType As Object = MyAdvAppSettings.AppSettings("FAMETitleIVService")

                If (ViewState("IsNonStandardTermProgram") = True) Then
                    If (fameTitleIvServiceType = False) Then
                        If chkTitleIVFameApproved.Checked And ddlTermLength.SelectedItem.ToString() = "Select" Then
                            termLengthValidtor.Enabled = True
                        ElseIf chkTitleIV.Checked And ddlTermLength.SelectedItem.ToString() = "Select" Then
                            termLengthValidtor.Enabled = True
                        End If
                    Else
                        If AdvantageSession.UserState.IsUserSupport Then
                            If chkTitleIVFameApproved.Checked And ddlTermLength.SelectedItem.ToString() = "Select" Then
                                termLengthValidtor.Enabled = True
                            ElseIf chkTitleIV.Checked And ddlTermLength.SelectedItem.ToString() = "Select" Then
                                termLengthValidtor.Enabled = True
                            End If
                        End If
                    End If
                End If

                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtPrgVerId.Text
                msg = objCommon.DoInsert(Form1)
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                Else
                    Try
                        'Program Version has been inserted, communicate it to AFA only if itsTitleIV
                        If chkTitleIVFameApproved.Checked = True Or chkTitleIV.Checked = True Then
                            SyncNewProgramVersionWithAFA()
                        End If
                    Catch

                    End Try
                End If

                Dim campusPrgVersion = InitializeCampusPrgVersion()

                If doesCampusPrgVersionExists = False Then
                    campusProgVerFacade.InsertIntoCampusPrgVerTable(campusPrgVersion, campusId)
                Else
                    Dim termSubEqualInLength As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, campusPrgVersion.Item("TermSubEqualInLen"), "TermSubEqualInLen", campusId, HttpContext.Current.Session("UserName").ToString())
                    Dim titleIvValue As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, chkTitleIVFameApproved.Checked, "IsTitleIV", campusId, HttpContext.Current.Session("UserName").ToString())
                End If

                If MyAdvAppSettings.AppSettings("TrackFASAP") = "True" Then
                    Dim ProgId As String = txtPrgVerId.Text
                    Dim b As Boolean = facade.UpdateFASAP(ddlFASAPId.SelectedValue, ProgId, HttpContext.Current.Session("UserName"))
                    If b = False Then
                        DisplayErrorMessage("Update FASAP is a problem")
                    End If
                End If
                ''Insert the charge periods and their sequences
                ''Added by Saraswathi Lakshmanan on 28 May 2010
                msg = facade.InsertIntoPrgChargePeriodSeqTable(txtPrgVerId.Text.Trim)
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If


                If CType(ViewState("PrgVerInstructionTypeTable"), DataTable).Rows.Count >= 1 Then
                    Try
                        Dim gPrgVerId As New Guid(txtPrgVerId.Text.Trim)
                        msg = facade.UpdateProgVersionInstructionType(CType(ViewState("PrgVerInstructionTypeTable"), DataTable), AdvantageSession.UserState.UserName, gPrgVerId)
                        If msg = "" Then
                            PrgVerInstructionTypeTable = GetTable.Clone
                            ViewState("PrgVerInstructionTypeTable") = PrgVerInstructionTypeTable
                            PrgVerInstructionTypeTable = facade.GetAllProgVersionInstructionType(txtPrgVerId.Text)
                            ViewState("PrgVerInstructionTypeTable") = PrgVerInstructionTypeTable
                            grdPrgVerInstructionType.MasterTableView.ClearEditItems()
                            grdPrgVerInstructionType.MasterTableView.IsItemInserted = False
                            grdPrgVerInstructionType.DataSource = PrgVerInstructionTypeTable
                            grdPrgVerInstructionType.Rebind()
                        End If
                    Catch ex As System.Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage(ex.Message)
                    End Try
                End If


                txtRowIds.Text = objCommon.PagePK
                'ds = objListGen.SummaryListGenerator()
                'dlstProgVer.SelectedIndex = -1
                'ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))
                'PopulateDataList(ds)
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    Dim strUpdate As String
                    strUpdate = (New regentFacade).updateRegentCurriculumCode(txtPrgVerId.Text, ddlCurrCodeId.SelectedValue, Session("User"))
                End If

                ' ds.WriteXml(sw)
                'ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(Form1, "EDIT", pObj)
                ViewState("MODE") = "EDIT"

            ElseIf ViewState("MODE") = "EDIT" Then
                If txtPrgVerCode.Text <> "" And txtPrgVerDescrip.Text <> "" Then
                    If facade.DoesProgVerCodeAlreadyExists(txtPrgVerCode.Text.Trim(), txtPrgVerDescrip.Text.Trim, ddlCampGrpId.SelectedValue, txtPrgVerId.Text.Trim) Then
                        DisplayErrorMessage("Please give a different Program Version Code or Program Version Definition")
                        Exit Sub
                    End If
                    doesCampusPrgVersionExists = campusProgVerFacade.DoesCampusProgVerAlreadyExists(txtPrgVerId.Text.Trim, campusId)
                End If
                ''Code added to test if, program version has any Schedule attached to it. If so, cannot modify the usetimeclock checkbox
                ''Added by saraswathi lakshmanan on may 12 2009
                If facade.DoesprogramVersionHasScheduleAttached(txtPrgVerId.Text, chkUseTimeClock.Checked) Then
                    DisplayErrorMessage("Please delete the Schedules, related to Program Version Code or Program Version Definition. And then modify the UseTimeClock checkbox ")
                    Exit Sub
                End If

                If hdnCampGrpId.Value <> ddlCampGrpId.SelectedValue Then
                    Dim result As Boolean = facade.DoesCampusIsASubsetOfCampusGroup(ddlCampGrpId.SelectedValue, campusId, txtPrgVerId.Text)
                    If Not result Then
                        'ddlCampGrpId.SelectedValue = hdnCampGrpId.Value
                        DisplayErrorMessage("You cannot change the campus group since the campus is not a subset of the new campus group")
                        Exit Sub
                    End If
                End If


                msg = objCommon.DoUpdate(Form1)
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                Else
                    Try
                        'Program Version has been updated, communicate it to AFA only if program is TitleIV
                        If chkTitleIVFameApproved.Checked = True Or chkTitleIV.Checked = True Then
                            SyncUpdatedProgramVersionWithAFA()
                        End If
                    Catch

                    End Try
                End If
                If MyAdvAppSettings.AppSettings("TrackFASAP") = "True" Then
                    Dim ProgId As String = txtPrgVerId.Text
                    Dim b As Boolean = facade.UpdateFASAP(ddlFASAPId.SelectedValue, ProgId, HttpContext.Current.Session("UserName"))
                    If b = False Then
                        DisplayErrorMessage("Update FASAP is a problem")
                    End If
                End If
                ''Insert the charge periods and their sequences
                ''Added by Saraswathi Lakshmanan on 28 May 2010
                msg = facade.InsertIntoPrgChargePeriodSeqTable(txtPrgVerId.Text.Trim)
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If

                If CheckForDupInstructionTypes("") = True Then
                    DisplayErrorMessage("Duplicate Instruction Type Found")
                    Exit Sub
                End If

                SumHours()
                If CheckHoursSumValidateGreater() = True Then
                    DisplayErrorMessage("The sum of all Instruction Hours cannot exceed the Prg Version Hours")
                    Exit Sub
                End If
                If ViewState("GridChg") = 1 Then
                    If CheckHoursSumValidateLessThan() = True Then
                        DisplayErrorMessage("The sum of all Instruction Hours must equal the Prg Version Hours")
                        Exit Sub
                    End If
                End If
                ' 08/08/2011 JRobinson Clock Hour Project - write PrgVerInstructionTypes out
                If CType(ViewState("PrgVerInstructionTypeTable"), DataTable).Rows.Count >= 1 Then
                    Try
                        Dim gPrgVerId As New Guid(txtPrgVerId.Text.Trim)
                        msg = facade.UpdateProgVersionInstructionType(CType(ViewState("PrgVerInstructionTypeTable"), DataTable), Session("UserName"), gPrgVerId)
                        If msg = "" Then
                            PrgVerInstructionTypeTable = GetTable.Clone
                            ViewState("PrgVerInstructionTypeTable") = PrgVerInstructionTypeTable
                            PrgVerInstructionTypeTable = facade.GetAllProgVersionInstructionType(txtPrgVerId.Text)
                            ViewState("PrgVerInstructionTypeTable") = PrgVerInstructionTypeTable
                            grdPrgVerInstructionType.MasterTableView.ClearEditItems()
                            grdPrgVerInstructionType.MasterTableView.IsItemInserted = False
                            grdPrgVerInstructionType.DataSource = PrgVerInstructionTypeTable
                            grdPrgVerInstructionType.Rebind()
                        End If
                    Catch ex As System.Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage(ex.Message)
                    End Try
                End If

                Dim fameTitleIvServiceType As Object = MyAdvAppSettings.AppSettings("FAMETitleIVService")
                If (ViewState("IsNonStandardTermProgram") = True) Then
                    If (fameTitleIvServiceType = False) Then
                        If chkTitleIVFameApproved.Checked And ddlTermLength.SelectedItem.ToString() = "Select" Then
                            termLengthValidtor.Enabled = True
                        ElseIf chkTitleIV.Checked And ddlTermLength.SelectedItem.ToString() = "Select" Then
                            termLengthValidtor.Enabled = True
                        End If
                    Else
                        If AdvantageSession.UserState.IsUserSupport Then
                            If chkTitleIVFameApproved.Checked And ddlTermLength.SelectedItem.ToString() = "Select" Then
                                termLengthValidtor.Enabled = True
                            ElseIf chkTitleIV.Checked And ddlTermLength.SelectedItem.ToString() = "Select" Then
                                termLengthValidtor.Enabled = True
                            End If
                        End If
                    End If
                End If

                Dim campusPrgVersion = InitializeCampusPrgVersion()

                If doesCampusPrgVersionExists = True Then
                    If ddlTermLength.SelectedItem.ToString() = "Select" And chkTitleIVFameApproved.Checked = False Then
                        Dim termSubEqualInLength As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, "", "TermSubEqualInLen", campusId, HttpContext.Current.Session("UserName").ToString())
                    Else
                        Dim termSubEqualInLength As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, ddlTermLength.SelectedItem.Value, "TermSubEqualInLen", campusId, HttpContext.Current.Session("UserName").ToString())
                    End If
                    Dim allowExcusAbsPerPayPrd = ""
                    If Convert.ToString(campusPrgVersion.Item("AllowExcusAbsPerPayPrd")) <> "" Then
                        allowExcusAbsPerPayPrd = campusPrgVersion.Item("AllowExcusAbsPerPayPrd")
                    End If
                    If fameTitleIvServiceType = True Then
                        Dim titleIvValue As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, chkTitleIVFameApproved.Checked, "IsTitleIV", campusId, HttpContext.Current.Session("UserName").ToString())
                        Dim titleIvFameApproved As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, True, "IsFAMEApproved", campusId, HttpContext.Current.Session("UserName").ToString())
                    Else
                        Dim titleIvValue As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, chkTitleIV.Checked, "IsTitleIV", campusId, HttpContext.Current.Session("UserName").ToString())
                        Dim titleIvFameApproved As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, False, "IsFAMEApproved", campusId, HttpContext.Current.Session("UserName").ToString())
                    End If
                    Dim calculationPeriodType As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, campusPrgVersion.Item("CalculationPeriodTypeId"), "CalculationPeriodTypeId", campusId, HttpContext.Current.Session("UserName").ToString())
                    Dim excusedAbsence As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, allowExcusAbsPerPayPrd, "AllowExcusAbsPerPayPrd", campusId, HttpContext.Current.Session("UserName").ToString())
                Else
                    campusProgVerFacade.InsertIntoCampusPrgVerTable(campusPrgVersion, campusId)
                End If

                'ds = objListGen.SummaryListGenerator()
                'dlstProgVer.SelectedIndex = -1
                'ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))

                'PopulateDataList(ds)
                'ds.WriteXml(sw)
                'ViewState("ds") = sw.ToString()
                'Response.Write(objCommon.strText)


            End If
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                Dim regCode As String = ""
                Dim pnlInnerPanel As Panel
                pnlInnerPanel = CType(pnlRegent.FindControl("pnlRegentAwardChild"), Panel)
                regCode = CType(pnlInnerPanel.FindControl("ddlRegentCode"), DropDownList).SelectedValue
                'Response.Write(regCode)
                Dim strGetRegentDegree As String = (New regentFacade).updateMaintenanceData("arPrgVersions", txtPrgVerId.Text, regCode, "MapId", "PrgVerId")
            End If
            'Linkbutton1.Enabled = True
            If chkIsContinuingEd.Checked = False Then
                Linkbutton2.Enabled = True
            End If
            '   set Fees Hyperlink
            SetFeesHyperlink(txtPrgVerId.Text)

            CommonWebUtilities.RestoreItemValues(dlstProgVer, txtPrgVerId.Text)
            'keep appart BillingMethodId to validate it before change it
            If (ddlBillingMethodId.SelectedIndex > 0) Then
                BillingMethodId = ddlBillingMethodId.SelectedValue
                txtBillingMethodDescrip.Text = ddlBillingMethodId.SelectedItem.Text
                txtBillingMethodDescrip.ToolTip = String.Empty
            Else
                txtBillingMethodDescrip.Text = String.Empty
                txtBillingMethodDescrip.ToolTip = toolTiptxtBillingMethodDescrip
            End If

            Dim fameTitleIvService As Object = MyAdvAppSettings.AppSettings("FAMETitleIVService")
            If AdvantageSession.UserState.IsUserSupport Then
                If ViewState("IsNonTermProgram") = True Then
                    If fameTitleIvService = True Or chkTitleIV.Checked = True Then
                        Dim selfPaced As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, ddlSelfPaced.SelectedItem.Value, "IsSelfPaced", campusId, HttpContext.Current.Session("UserName").ToString())
                    End If
                End If
            Else
                If fameTitleIvService = False Then
                    Dim selfPaced As String = (New CampusProgVerFacade).UpdateCampusPrgVersionDetails("arCampusPrgVersions", txtPrgVerId.Text, ddlSelfPaced.SelectedItem.Value, "IsSelfPaced", campusId, HttpContext.Current.Session("UserName").ToString())
                End If
            End If

            Dim programVersionRequest As New ProgramVersionRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Dim programVersionResponse As ActionResult(Of ProgramVersion) = programVersionRequest.SetProgramRegistrationType(CType(ddlRegistrationType.SelectedValue, Enums.ProgramRegistrationType),
                                                                                                                             Guid.Parse(txtPrgVerId.Text),
                                                                                                                             gradeScaled)


            If (ddlRegistrationType.SelectedValue > 0) Then
                ShowGradeScaleDropdown(True)
            End If

            If (Not programVersionResponse Is Nothing) Then
                If (programVersionResponse.ResultStatus = Enums.ResultStatus.Error Or programVersionResponse.ResultStatus = Enums.ResultStatus.NotFound) Then
                    Me.DisplayErrorMessage(programVersionResponse.ResultStatusMessage)
                ElseIf (programVersionResponse.ResultStatus = Enums.ResultStatus.Warning) Then
                    Me.DisplayWarningMessage(programVersionResponse.ResultStatusMessage)
                ElseIf (programVersionResponse.ResultStatus = Enums.ResultStatus.Success) Then
                    Me.DisplaySuccessMessage("Changes Successfully saved")
                End If
            End If
            'ds = objListGen.SummaryListGenerator()
            'dlstProgVer.SelectedIndex = -1
            ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))

            PopulateDataList(ds)
            CommonWebUtilities.SetStyleToSelectedItem(dlstProgVer, txtPrgVerId.Text, ViewState)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim facade As New ProgVerFacade
        'Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Try
            ClearRHS()
            'ds.ReadXml(sr)
            ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))
            ddlRegistrationType.Enabled = True
            ddlRegistrationType.SelectedValue = "0"
            PopulateDataList(ds)

            If chkTitleIV.Style.Item("display") = "block" Then
                chkTitleIV.Checked = False
            ElseIf chkTitleIVFameApproved.Style.Item("display") = "block" Then
                chkTitleIVFameApproved.Checked = False
            End If
            SelfPacedValidator.Enabled = False
            termLengthValidtor.Enabled = False
            spnSelfPaced.Visible = False
            spnTermLength.Visible = False
            ddlTermLength.SelectedValue = ""
            txtAllowExcusAbsPerPayPrd.Text = ""

            R2T4CalculationPeriodValidator.Enabled = False
            spnCalculationPeriodTypeId.Visible = False

            ' 08/08/2011 JRobinson Clock Hour Project
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("PrgVerCampGrpId") = ds.Tables(0).Rows.Item(0).Item("CampGrpId").ToString
            End If
            InstructionTypeCodes = (New InstructionTypeFacade).GetInstructionTypesByStatus("true")
            ViewState("InstructionTypeCodes") = InstructionTypeCodes
            If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                lblInstructionTypeHours.Visible = True
                grdPrgVerInstructionType.Visible = True
            Else
                lblInstructionTypeHours.Visible = False
                grdPrgVerInstructionType.Visible = False
            End If
            ViewState("PrgVerInstructionTypeTable") = GetTable.Clone()
            PrgVerInstructionTypeTable = ViewState("PrgVerInstructionTypeTable")
            grdPrgVerInstructionType.MasterTableView.ClearEditItems()
            grdPrgVerInstructionType.MasterTableView.IsItemInserted = False
            grdPrgVerInstructionType.DataSource = PrgVerInstructionTypeTable
            grdPrgVerInstructionType.Rebind()
            decHoursSum = 0.0
            txtHoursSum.Text = "0.0"
            txtOrigHours.Text = "0.0"
            blnInEditMode = False
            ViewState("InEditMode") = blnInEditMode
            ViewState("GridChg") = 0

            txtProgDescrip.Text = ViewState("ProgDesc")
            'disable new and delete buttons.
            objcommon.SetBtnState(Form1, "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtPrgVerId.Text = System.Guid.NewGuid.ToString
            lbtSetUpFees.Enabled = False
            'Linkbutton1.Enabled = False
            Linkbutton2.Enabled = False
            lbSetSchedules.Enabled = False
            lbSetUpCourses.Enabled = False
            ''Added by Saraswathi Lakshmanan on May 5 2010
            ''to show the default Charges link
            lbSetUpDefaultCharges.Enabled = False
            lbSetUpExamSequence.Enabled = False

            GetCampusGroupsByProgramNew("")
            ddlCalculationPeriodTypeId.SelectedIndex = -1
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                BuildSession()
            End If
            If (MyAdvAppSettings.AppSettings("TrackFASAP") = "False") Then
                lblFASAPId.Style.Add("display", "none")
                ddlFASAPId.Style.Add("display", "none")
            End If
            'ddlUnitTypeId.Enabled = True

            CommonWebUtilities.RestoreItemValues(dlstProgVer, Guid.Empty.ToString)

            ShowGradeScaleDropdown(False)
            BindGradeScale()


        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim facade As New ProgVerFacade
        Dim campusProgVerFacade As New CampusProgVerFacade
        Dim msg As String
        Try
            campusProgVerFacade.DeleteCampusProgramVersion(txtPrgVerId.Text, campusId)

            Dim DA As New TermDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

            Dim deleteTermMessage = DA.DeleteTermByProgramVersion(txtPrgVerId.Text)

            If deleteTermMessage <> String.Empty Then
                DisplayErrorMessage(deleteTermMessage)
            End If

            msg = objCommon.DoDelete(Form1)
            If msg <> "" Then
                DisplayErrorMessage(msg)
            End If
            ClearRHS()
            spnCalculationPeriodTypeId.Visible = False
            R2T4CalculationPeriodValidator.Enabled = False
            spnSelfPaced.Visible = False
            SelfPacedValidator.Enabled = False
            chkTitleIVFameApproved.Checked = False
            chkTitleIV.Checked = False
            spnTermLength.Visible = False
            'ds = objListGen.SummaryListGenerator()
            dlstProgVer.SelectedIndex = -1
            ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))
            PopulateDataList(ds)
            'With dlstProgVer
            '    .DataSource = ds
            '    .DataBind()
            'End With
            'ds.WriteXml(sw)

            ' 08/08/2011 JRobinson Clock Hour Project 
            If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                lblInstructionTypeHours.Visible = True
                grdPrgVerInstructionType.Visible = True
            Else
                lblInstructionTypeHours.Visible = False
                grdPrgVerInstructionType.Visible = False
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("PrgVerCampGrpId") = ds.Tables(0).Rows.Item(0).Item("CampGrpId").ToString
            End If
            InstructionTypeCodes = (New InstructionTypeFacade).GetInstructionTypesByStatus("true")
            ViewState("InstructionTypeCodes") = InstructionTypeCodes
            ViewState("PrgVerInstructionTypeTable") = GetTable.Clone()
            PrgVerInstructionTypeTable = ViewState("PrgVerInstructionTypeTable")
            grdPrgVerInstructionType.MasterTableView.ClearEditItems()
            'grdPrgVerInstructionType.MasterTableView.IsItemInserted = False
            grdPrgVerInstructionType.DataSource = PrgVerInstructionTypeTable
            grdPrgVerInstructionType.Rebind()
            decHoursSum = 0.0
            txtHoursSum.Text = "0.0"
            txtOrigHours.Text = "0.0"
            'txtAvailInstTypes.Text = "0"
            blnInEditMode = False
            ViewState("InEditMode") = blnInEditMode


            ViewState("ds") = sw.ToString()
            objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtPrgVerId.Text = System.Guid.NewGuid.ToString
            lbtSetUpFees.Enabled = False
            'Linkbutton1.Enabled = False
            Linkbutton2.Enabled = False
            lbSetSchedules.Enabled = False
            lbSetUpCourses.Enabled = False
            ''Added by Saraswathi Lakshmanan on May 5 2010
            ''to show the default Charges link
            lbSetUpDefaultCharges.Enabled = False
            lbSetUpExamSequence.Enabled = False
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                BuildSession()
            End If
            'ddlUnitTypeId.Enabled = True

            CommonWebUtilities.RestoreItemValues(dlstProgVer, Guid.Empty.ToString)

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub dlstPrgGrp_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstProgVer.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim dsFASAP As New DataSet
        Dim objListGenerator As New DataListGenerator
        'Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim facade As New ProgVerFacade
        Try
            'Reset the Scheduling Method DDL
            ddlSchedMethodId.SelectedIndex = -1

            strGUID = dlstProgVer.DataKeys(e.Item.ItemIndex).ToString()
            txtRowIds.Text = e.CommandArgument.ToString

            GetCampusGroupsByProgram(strGUID)
            objCommon.PopulatePage(Form1, strGUID)
            'save Attendance Unit Type into hidden textbox
            txtUnitTypeId.Text = ddlUnitTypeId.SelectedValue.ToString
            hdnCampGrpId.Value = ddlCampGrpId.SelectedValue
            '
            objCommon.SetBtnState(Form1, "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstProgVer.SelectedIndex = selIndex
            'ds.ReadXml(sr)
            'PopulateDataList(ds)
            ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))

            If dlstProgVer.SelectedValue.ToString() <> "" Then

                Dim isByProgram = (New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).IsProgramRegistrationType(dlstProgVer.SelectedValue.ToString())
                lbSetUpExamSequence.Enabled = isByProgram
                lbSetUpExamSequence.Visible = isByProgram
            End If

            PopulateDataList(ds)
            ''US4383 ---TT
            If (MyAdvAppSettings.AppSettings("TrackFASAP") = "True") Then

                dsFASAP = facade.GetAllFASAPByProgram(strGUID)
                If dsFASAP.Tables(0).Rows.Count > 0 Then
                    Dim row As DataRow = dsFASAP.Tables(0).Rows(0)
                    If AdvantageCommonValues.InactiveGuid.ToLower.ToString = row("StatusId").ToString.ToLower Then
                        Dim strFASAPDescrip As String = "(X) " & row("SAPDescrip").ToString
                        ddlFASAPId.Items.Insert(1, New ListItem(strFASAPDescrip, row("FASAPid").ToString()))
                        ddlFASAPId.SelectedIndex = 1
                    Else
                        ddlFASAPId.SelectedValue = row("FASAPid").ToString()
                    End If

                Else
                    ddlFASAPId.SelectedIndex = 0
                End If
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("PrgVerCampGrpId") = hdnCampGrpId.Value
            End If
            InstructionTypeCodes = (New InstructionTypeFacade).GetInstructionTypesByStatus("true")
            ViewState("InstructionTypeCodes") = InstructionTypeCodes
            PrgVerInstructionTypeTable = GetTable.Clone
            ViewState("PrgVerInstructionTypeTable") = PrgVerInstructionTypeTable
            PrgVerInstructionTypeTable = facade.GetAllProgVersionInstructionType(strGUID)
            ViewState("PrgVerInstructionTypeTable") = PrgVerInstructionTypeTable
            grdPrgVerInstructionType.MasterTableView.ClearEditItems()
            grdPrgVerInstructionType.MasterTableView.IsItemInserted = False
            grdPrgVerInstructionType.DataSource = PrgVerInstructionTypeTable
            grdPrgVerInstructionType.Rebind()
            decHoursSum = 0.0
            txtHoursSum.Text = "0.0"
            blnInEditMode = False
            ViewState("GridChg") = 0
            ViewState("InEditMode") = blnInEditMode
            If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                lblInstructionTypeHours.Visible = True
                grdPrgVerInstructionType.Visible = True
            Else
                lblInstructionTypeHours.Visible = False
                grdPrgVerInstructionType.Visible = False
            End If

            'setup save button according security, students registered in Program Version and Web Config Settings
            Dim b As Boolean = CType((New ProgFacade).DoesPrgVerHaveStdsReg(txtRowIds.Text), Boolean)

            btnSave.Enabled = btnSave.Enabled And ((b And GetWebConfigValueForAllowAddingCoursesToProgramVersionsAfterStudentsHaveBeenEnrolled()) Or Not b)
            Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)

            If Not isUserSa Then
                If facade.HasThisProgramVersionStudentsGraded(txtPrgVerId.Text) Then
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            End If

            'Validate if the programversion is part of the campus user is logged in to
            If ddlCampGrpId.SelectedItem.Text.ToLower <> "all" Then

                Dim boolDoesProgVerBelongToCampus As Boolean = False
                If Not isUserSa Then
                    boolDoesProgVerBelongToCampus = (New ProgVerFacade).IsProgramVersionPartOfCampusUserIsLoggedInTo(campusId, ViewState("ProgId"), txtPrgVerId.Text)
                Else
                    boolDoesProgVerBelongToCampus = True
                End If

                If boolDoesProgVerBelongToCampus = True Then
                    btnSave.Enabled = True
                Else
                    btnSave.Enabled = False
                End If
            End If


            '   set Fees Hyperlink
            SetFeesHyperlink(e.CommandArgument)
            'Linkbutton1.Enabled = True
            If chkIsContinuingEd.Checked = False Then
                Linkbutton2.Enabled = True
            End If
            lbtSetTermModules.Enabled = True

            If ddlUnitTypeId.SelectedItem.Selected = False Or ddlUnitTypeId.SelectedItem.Text = "Select" Then
                ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.NoneGuid
                ddlUnitTypeId.SelectedItem.Text = "None"
            End If

            'Dim bShow As Boolean = ddlUnitTypeId.SelectedValue = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.UseTimeClockGuid
            If (ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.PresentAbsentGuid Or
                ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.MinutesGuid Or
                ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.ClockHoursGuid Or
                ddlUnitTypeId.SelectedItem.Text.ToLower() = "clock hours") Then
                ''Added by Saraswathi on Feb 10 2009
                ' If linkobj.HasFull Then
                lbSetSchedules.Enabled = True

            Else
                lbSetSchedules.Enabled = False
            End If
            '   set Tardies panel
            If chkTrackTardies.Checked Then
                pnlTardies.Visible = True
            Else
                pnlTardies.Visible = False
            End If
            If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) = "ModuleStart" Then
                lbSetUpCourses.Enabled = False
            ElseIf facade.HasThisProgramVersionhaveCourses(strGUID) Then
                lbSetUpCourses.Enabled = True
            Else
                lbSetUpCourses.Enabled = False
            End If
            ''Added by Saraswathi Lakshmanan on May 5 2010
            ''to show the default Charges link
            lbSetUpDefaultCharges.Enabled = True
            lbSetUpExamSequence.Enabled = True
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                Try
                    ddlRegentCode.SelectedValue = (New regentFacade).getAllMaintenanceDataByPrimaryKey("arPrgVersions", "PrgVerId", strGUID)
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlRegentCode.SelectedIndex = 0
                End Try
            End If
            CommonWebUtilities.RestoreItemValues(dlstProgVer, strGUID)
            'keep appart BillingMethodId to validate it before change it
            If (ddlBillingMethodId.SelectedIndex > 0) Then
                BillingMethodId = ddlBillingMethodId.SelectedValue
                txtBillingMethodDescrip.Text = ddlBillingMethodId.SelectedItem.Text
                txtBillingMethodDescrip.ToolTip = String.Empty
            Else
                txtBillingMethodDescrip.Text = String.Empty
                txtBillingMethodDescrip.ToolTip = toolTiptxtBillingMethodDescrip
            End If

            If ViewState("IsNonTermProgram") = True Then
                Dim fameTitleIvService As Object = MyAdvAppSettings.AppSettings("FAMETitleIVService")
                If fameTitleIvService = False Then
                    ddlSelfPaced.Enabled = True
                    If chkTitleIVFameApproved.Checked = True Or chkTitleIV.Checked = True Then
                        SelfPacedValidator.Enabled = True
                        spnSelfPaced.Visible = True
                    Else
                        SelfPacedValidator.Enabled = False
                        spnSelfPaced.Visible = False
                    End If
                Else
                    If AdvantageSession.UserState.IsUserSupport Then
                        ddlSelfPaced.Enabled = True
                        If chkTitleIVFameApproved.Checked = True Or chkTitleIV.Checked = True Then
                            SelfPacedValidator.Enabled = True
                            spnSelfPaced.Visible = True
                        Else
                            SelfPacedValidator.Enabled = False
                            spnSelfPaced.Visible = False
                        End If
                    Else
                        ddlSelfPaced.Enabled = False
                        SelfPacedValidator.Enabled = False
                        spnSelfPaced.Visible = False
                    End If
                End If
                ddlSelfPaced.SelectedIndex = (New CampusProgVerFacade).GetSelfPacedValue("arCampusPrgVersions", "IsSelfPaced", "PrgVerId", strGUID, campusId)
            End If

            Dim programVersionRequest As New ProgramVersionRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            If (programVersionRequest.HaveEnrollments(Guid.Parse(txtPrgVerId.Text))) Then
                ddlRegistrationType.Enabled = False
            Else
                ddlRegistrationType.Enabled = True
            End If
            If (ddlRegistrationType.SelectedValue = CType(Enums.ProgramRegistrationType.ByProgram, Integer).ToString()) Then
                Me.ShowGradeScaleDropdown(True)
            Else
                Me.ShowGradeScaleDropdown(False)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstPrgGrp_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstPrgGrp_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Dim facade As New ProgVerFacade
        Try
            ClearRHS()
            objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtPrgVerId.Text = System.Guid.NewGuid.ToString
            'ds = objListGen.SummaryListGenerator()
            ds = facade.GetProgVersionsByProgram(ViewState("ProgId"))
            With dlstProgVer
                .DataSource = ds
                .DataBind()
            End With

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub lbtSetUpFees_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtSetUpFees.Click

        Dim sb As New StringBuilder
        '   append PrgVerId to the querystring
        sb.Append("&PrgVerId=" + txtPrgVerId.Text)
        '   append Course Description to the querystring
        sb.Append("&PrgVer=" + Server.UrlEncode(txtPrgVerDescrip.Text))

        '   setup the properties of the new window
        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "ProgramVersionFees"
        Dim url As String = "../SA/" + name + ".aspx?resid=224&mod=SA&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub

    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkbutton2.Click
        Dim sb As New StringBuilder
        '   append PrgVersion to the querystring
        sb.Append("&PrgVersion=" + Server.UrlEncode(txtPrgVerDescrip.Text))
        '   append ProgVerId Description to the querystring
        sb.Append("&ProgVerId=" + txtPrgVerId.Text)
        '   append Hours to the querystring
        sb.Append("&Hours=" + txtHours.Text)
        '   append Credits to the querystring
        sb.Append("&Credits=" + txtCredits.Text)
        '   append trackAttendance to the querystring
        Dim trackAttendance As Boolean = False
        If (ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.PresentAbsentGuid Or
                ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.MinutesGuid) Then
            trackAttendance = True
        End If
        sb.Append("&TrackAttendance=" + trackAttendance.ToString)
        '   setup the properties of the new window
        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "ProgVerDef"
        Dim url As String = "../AR/" + name + ".aspx?resid=42&mod=AR&cmpid=" + campusId + sb.ToString

        'open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        'theChild = window.open('../AR/ProgVerDef.aspx?resid=42&PrgVersion=' + PrgVersion + '&ProgVerId=' + ProgVerId + '&Hours=' + ProgVerHours + '&Credits=' + ProgVerCredits + '&CampusId=' + CampId,'ProgVerDef','Width=850,height=600,ScreenX=0,ScreenY=0,toolbar=yes');

    End Sub

    Protected Sub chkTrackTardies_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTrackTardies.CheckedChanged
        If chkTrackTardies.Checked Then
            pnlTardies.Visible = True
            SetFocus(txtTardiesMakingAbsence)
        Else
            pnlTardies.Visible = False
            txtTardiesMakingAbsence.Text = 0
        End If
    End Sub
    Protected Sub lbtSetTermModules_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtSetTermModules.Click
        Dim sb As New StringBuilder
        '   append ProgVerId to the querystring
        sb.Append("&ProgVerId=" + txtPrgVerId.Text)
        '   append ProgVerId Description to the querystring
        sb.Append("&PrgVersion=" + Server.UrlEncode(txtPrgVerDescrip.Text))
        '   setup the properties of the new window
        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "SetTermModule"
        Dim url As String = "../AR/" + name + ".aspx?resid=379&mod=AR&cmpid=" + campusId + sb.ToString

        'open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub

    ''' <summary>
    ''' Called in response to the user clicking the Set Up Schedules button.
    ''' This button is always enabled and uses MaintPopup as a generic method of opening maintenance pages.
    ''' Added 1/29/07 - ThinkTron Corporation
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lbSetSchedules_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSetSchedules.Click
        'TODO: Get the correct RESID for SetupSchedules page
        Dim url As String = String.Format("../MaintPopup.aspx?mod={0}&resid=511&cmpid={1}&pid={2}&ascx={3}",
                    "AR", campusId, txtPrgVerId.Text, "~/AR/IMaint_SetupSchedule.ascx")
        'open new window and pass parameters
        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        CommonWebUtilities.OpenChildWindow(Page, url, "SetupSchedules", winSettings)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim schedulingMethod As String = MyAdvAppSettings.AppSettings("SchedulingMethod")
        If schedulingMethod.ToLower.Equals("rotational") Then
            lbtSetTermModules.Visible = True
        Else
            lbtSetTermModules.Visible = False
        End If


        ' 08/08/2011 JRobinson Clock Hour Project 
        'compute running total of course type hours
        If blnInEditMode = False Then
            SumHours()
        End If


        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(lbtSetTermModules)
        controlsToIgnore.Add(Linkbutton2)
        controlsToIgnore.Add(lbtSetUpFees)
        ''Added by Saraswathi Lakshmanan on may 05 2010
        controlsToIgnore.Add(lbSetUpDefaultCharges)
        controlsToIgnore.Add(lblInstructionTypeHours)
        controlsToIgnore.Add(lbSetUpExamSequence)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    Protected Sub ddlUnitTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUnitTypeId.SelectedIndexChanged
        'Dim bShow As Boolean = ddlUnitTypeId.SelectedValue = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.UseTimeClockGuid
        'lbSetSchedules.Enabled = bShow
        'lbSetSchedules.Enabled = bShow
        If (ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.PresentAbsentGuid Or
            ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.MinutesGuid Or
            ddlUnitTypeId.SelectedItem.Value = AdvantageCommonValues.ClockHoursGuid Or
            ddlUnitTypeId.SelectedItem.Text.ToLower() = "clock hours") Then
            ' ''Added by SAraswathi on Feb 10 2009
            'If pObj.HasFull Then
            lbSetSchedules.Enabled = True
            'Else
            '    lbSetSchedules.Enabled = False
            'End If

        Else
            lbSetSchedules.Enabled = False
        End If
    End Sub

    Protected Sub chkIsContinuingEd_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIsContinuingEd.CheckedChanged
        If chkIsContinuingEd.Checked = True Then
            Linkbutton2.Enabled = False
        Else
            Linkbutton2.Enabled = True
        End If
    End Sub

    Protected Sub lbSetUpCourses_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSetUpCourses.Click

        Dim sb As New StringBuilder
        '   append PrgVerId to the querystring
        sb.Append("&PrgVerId=" + txtPrgVerId.Text)
        '   append Course Description to the querystring
        sb.Append("&PrgVer=" + Server.UrlEncode(txtPrgVerDescrip.Text))

        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "SetUpCourses"
        Dim url As String = "../AR/" + name + ".aspx?resid=520&mod=AR&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)


    End Sub

    Protected Sub lbSetUpDefaultCharges_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSetUpDefaultCharges.Click

        Dim sb As New StringBuilder
        '   append PrgVerId to the querystring
        sb.Append("&PrgVerId=" + txtPrgVerId.Text)
        '   append Course Description to the querystring
        sb.Append("&PrgVer=" + Server.UrlEncode(txtPrgVerDescrip.Text))

        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "PrgVersionDefaultCharge"
        Dim url As String = "../AR/" + name + ".aspx?resid=622&mod=AR&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub

    Protected Sub lbSetUpExamSequence_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSetUpExamSequence.Click

        Dim sb As New StringBuilder
        '   append PrgVerId to the querystring
        sb.Append("&PrgVerId=" + txtPrgVerId.Text)
        '   append Course Description to the querystring
        sb.Append("&PrgVer=" + Server.UrlEncode(txtPrgVerDescrip.Text))

        Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "ProgramVersionExamSequence"
        Dim url As String = "../AR/" + name + ".aspx?resid=622&mod=AR&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub

    ' 08/08/2011 JRobinson Clock Hour Project 
    Protected Sub grdPrgVerInstructionType_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles grdPrgVerInstructionType.DeleteCommand
        Try

            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            'Get the primary key value using the DataKeyValue.
            Dim PrgVerInstructionTypeId As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("PrgVerInstructionTypeId").ToString()


            Dim deletedRow() As Data.DataRow
            deletedRow = CType(ViewState("PrgVerInstructionTypeTable"), DataTable).Select("PrgVerInstructionTypeId='" + PrgVerInstructionTypeId + "'")

            Try
                decHoursSum = CType(txtHoursSum.Text, Decimal)
                decHoursSum = decHoursSum - deletedRow(0)("Hours")
                txtHoursSum.Text = CType(decHoursSum, String)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                decHoursSum = 0
                txtHoursSum.Text = 0
            End Try

            'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
            deletedRow(0)("CmdType") = 3
            e.Canceled = True
            grdPrgVerInstructionType.Rebind()
            'DE6762
            If decHoursSum = 0 Then
                ViewState("GridChg") = 0
            Else
                ViewState("GridChg") = 1
            End If


        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try

        'End Sub
    End Sub

    Protected Sub grdPrgVerInstructionType_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles grdPrgVerInstructionType.ItemDataBound
        Try

            If (TypeOf e.Item Is GridDataItem) AndAlso e.Item.IsInEditMode Then
                If Not TypeOf e.Item Is GridDataInsertItem Then
                    txtOrigHours.Text = e.Item.DataItem("Hours")
                    blnInEditMode = True
                    ViewState("InEditMode") = blnInEditMode
                End If
                Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)


                Dim dropDownList As DropDownList = DirectCast(gridEditFormItem("InstructionTypeDescrip").FindControl("ddlInstructionType"), DropDownList)

                If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                    dropDownList.Enabled = False
                    dropDownList.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(2).ToString()
                End If
                dropDownList.DataBind()
            End If

            If CType(ViewState("InstructionTypeCodes"), DataTable).Rows.Count = 0 Then
                grdPrgVerInstructionType.MasterTableView.NoMasterRecordsText = ""
            End If
            If (TypeOf e.Item Is GridCommandItem) Then
                Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
                cmditm.FindControl("RefreshButton").Visible = False
                If (Not cmditm.FindControl("RebindGridButton") Is Nothing) Then
                    cmditm.FindControl("RebindGridButton").Visible = False
                End If


            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        Finally

        End Try
    End Sub

    Protected Sub grdPrgVerInstructionType_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles grdPrgVerInstructionType.NeedDataSource

        Dim dt As New DataTable


        If ViewState("PrgVerInstructionTypeTable") Is Nothing Then
            grdPrgVerInstructionType.DataSource = dt

        Else
            If CType(ViewState("PrgVerInstructionTypeTable"), DataTable).Rows.Count > 0 Then
                grdPrgVerInstructionType.DataSource = CType(ViewState("PrgVerInstructionTypeTable"), DataTable).Select("CmdType not in (3)")
            Else
                grdPrgVerInstructionType.DataSource = dt
            End If
        End If
        'End If

    End Sub

    Protected Sub grdPrgVerInstructionType_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles grdPrgVerInstructionType.ItemCommand

        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked

            e.Canceled = True

            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            newValues("Hours") = 0
            newValues("CmdType") = 1
            ''Insert the item and rebind
            e.Item.OwnerTableView.InsertItem(newValues)
            'grdPrgVerInstructionType.MasterTableView.IsItemInserted = True

        End If

        If e.CommandName = RadGrid.PerformInsertCommandName Then 'insert
            grdPrgVerInstructionType_InsertCommand(source, e)
        End If

        If e.CommandName = RadGrid.EditCommandName Then 'edit
            blnInEditMode = True
        End If

    End Sub

    Protected Sub grdPrgVerInstructionType_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles grdPrgVerInstructionType.InsertCommand
        Try

            If validatetxt1 = True OrElse validatetxt2 = True Then Exit Sub


            If ViewState("PrgVerInstructionTypeTable") Is Nothing Then
                ViewState("PrgVerInstructionTypeTable") = GetTable.Clone()
            End If

            Dim insertedItem As GridDataInsertItem = DirectCast(e.Item, GridDataInsertItem)

            If CheckForDupInstructionTypes(TryCast(insertedItem("InstructionTypeDescrip").Controls(1), DropDownList).SelectedItem.Text) = True Then
                DisplayErrorMessage("Instruction Type already used")
                Exit Sub
            End If

            If CheckHoursSumValidateGreater(TryCast(insertedItem("Hours").Controls(1), TextBox).Text) = True Then
                DisplayErrorMessage("The sum of all Instruction Hours cannot exceed the Prg Version Hours")
                Exit Sub
            End If

            Dim insertedRow As Data.DataRow = CType(ViewState("PrgVerInstructionTypeTable"), DataTable).NewRow

            insertedRow("PrgVerInstructionTypeId") = Guid.NewGuid.ToString()
            insertedRow("PrgVerID") = txtPrgVerId.Text
            insertedRow("InstructionTypeId") = TryCast(insertedItem("InstructionTypeDescrip").Controls(1), DropDownList).SelectedItem.Value
            insertedRow("InstructionTypeDescrip") = TryCast(insertedItem("InstructionTypeDescrip").Controls(1), DropDownList).SelectedItem.Text
            insertedRow("Hours") = TryCast(insertedItem("Hours").Controls(1), TextBox).Text
            insertedRow("CmdType") = 1

            Try
                decHoursSum = CType(txtHoursSum.Text, Decimal)
                decHoursSum = decHoursSum + insertedRow("Hours")
                txtHoursSum.Text = CType(decHoursSum, String)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try


            CType(ViewState("PrgVerInstructionTypeTable"), DataTable).Rows.Add(insertedRow)
            CType(ViewState("PrgVerInstructionTypeTable"), DataTable).AcceptChanges()


            e.Item.Edit = False
            e.Canceled = True
            grdPrgVerInstructionType.MasterTableView.IsItemInserted = False
            grdPrgVerInstructionType.Rebind()
            ViewState("GridChg") = 1

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Protected Sub grdPrgVerInstructionType_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles grdPrgVerInstructionType.UpdateCommand
        Try

            If validatetxt1 = True OrElse validatetxt2 = True Then Exit Sub

            Dim editedItem As GridEditableItem = TryCast(e.Item, GridEditableItem)

            SumHours()
            If CheckHoursSumValidateGreater(TryCast(editedItem("Hours").Controls(1), TextBox).Text) = True Then
                DisplayErrorMessage("The sum of all Instruction Hours cannot exceed the Prg Version Hours")
                Exit Sub
            End If

            ''Get the primary key value using the DataKeyValue.
            Dim PrgVerInstructionTypeId As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("PrgVerInstructionTypeId").ToString()
            Dim upDatedRow() As Data.DataRow
            upDatedRow = CType(ViewState("PrgVerInstructionTypeTable"), DataTable).Select("PrgVerInstructionTypeId='" + PrgVerInstructionTypeId + "'")
            upDatedRow(0)("PrgVerId") = txtPrgVerId.Text
            upDatedRow(0)("InstructionTypeId") = TryCast(editedItem("InstructionTypeDescrip").Controls(1), DropDownList).SelectedItem.Value
            upDatedRow(0)("InstructionTypeDescrip") = TryCast(editedItem("InstructionTypeDescrip").Controls(1), DropDownList).SelectedItem.Text
            upDatedRow(0)("Hours") = TryCast(editedItem("Hours").Controls(1), TextBox).Text
            Try
                decHoursSum = CType(txtHoursSum.Text, Decimal)
                decHoursSum = decHoursSum + upDatedRow(0)("Hours") - CType(txtOrigHours.Text, Decimal)
                If decHoursSum > CType(txtHours.Text, Decimal) Then
                    DisplayErrorMessage("The sum of all Instruction Hours cannot exceed the Prg Version Hours")
                    Exit Sub
                End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try

            upDatedRow(0)("CmdType") = TryCast(editedItem("ContainerHidden").Controls(1), Label).Text
            'cmdType for Insert 1 ,edit  2 and delete 3 by default from database its 0
            If upDatedRow(0)("CmdType") <> "1" Then upDatedRow(0)("CmdType") = 2
            e.Canceled = True
            blnInEditMode = False
            txtOrigHours.Text = "0.0"
            txtHoursSum.Text = CType(decHoursSum, String)
            ViewState("InEditMode") = blnInEditMode
            grdPrgVerInstructionType.MasterTableView.ClearEditItems()
            grdPrgVerInstructionType.Rebind()
            ViewState("GridChg") = 1

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Protected Sub SumHours()

        Dim lbHours As New Label
        'Dim tbEditHours As New TextBox
        Dim i As Integer = 0
        decHoursSum = 0.0

        For i = 0 To grdPrgVerInstructionType.Items.Count - 1
            lbHours = CType(grdPrgVerInstructionType.Items(i).FindControl("lblHours"), Label)
            'tbEditHours = CType(grdPrgVerInstructionType.Items(i).FindControl("txtEditHours"), TextBox)
            'label not available during edit
            If Not lbHours Is Nothing Then
                decHoursSum = decHoursSum + CType(lbHours.Text, Decimal)
                'ElseIf Not tbEditHours Is Nothing Then
                '    decHoursSum = decHoursSum + CType(tbEditHours.Text, Decimal)
            End If
        Next
        txtHoursSum.Text = CType(decHoursSum, String)

    End Sub

    Public Sub ddlBillingMethodId_SelectedIndexChanged(sender As Object, e As EventArgs)
        If Not String.IsNullOrEmpty(BillingMethodId) Then
            Dim saf As New StudentsAccountsFacade
            'Check if the BillingMethod can be changed or reallocated
            If (saf.BillingMethodCheckForChargedPaymentPeriods(BillingMethodId)) Then
                'error deleting PmtPeriods
                DisplayErrorMessage("Charging method could not be changed because is being used.")
                ddlBillingMethodId.SelectedValue = BillingMethodId
            End If
        End If
    End Sub

    Public Sub TextValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        Dim strMinus As String = Mid(args.Value, 1, 1)
        If strMinus = "-" Then
            DisplayErrorMessage("The fields cannot be negative values.")
            validatetxt = True
        End If
    End Sub

    Public Sub TextValidateGreaterThanOne(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If IsNumeric(args.Value) Then
            If args.Value < 1 Then
                DisplayErrorMessage("Pay periods/academic year cannot be less than 1")
                validatetxt = True
            End If
            If ((ViewState("IsNonTermProgram") = True OrElse ViewState("IsClockHourProgram")) AndAlso (chkTitleIVFameApproved.Checked)) Then

                If args.Value > 2 Then
                    DisplayErrorMessage("There cannot be more than 2 payment periods per academic year for a Non-Term or Clock hour Title IV program.")
                    validatetxt = True
                End If

            End If
        Else
            DisplayErrorMessage("Pay periods/academic year should contain only numerics between to 0 to 10")
            validatetxt = True
        End If
    End Sub

    Public Sub TextValidateExcusAbs(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If IsNumeric(args.Value) Then
            If args.Value > 10 Then
                DisplayErrorMessage("Excused Absence Percentage should not be greater than 10")
                validatetxt = True
            End If
            If args.Value < 0 Then
                DisplayErrorMessage("Excused Absence Percentage should be greater than or equal to 0 and less than 10")
                validatetxt = True
            End If
        Else
            DisplayErrorMessage("Excused Absence Percentage should contain only numerics between to 0 to 10")
            validatetxt = True
        End If
    End Sub
    Public Sub BillingMethodValidation(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        args.IsValid = True
    End Sub

    Public Sub GetCampusGroupsByProgram(ByVal PrgVerId As String)
        ddlCampGrpId.Items.Clear()
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = (New ProgVerFacade).GetAllCampusGroupsByProgramAndProgramVersion(txtProgId.Text, PrgVerId) '(New ProgVerFacade).GetAllCampusGroupsByProgram(txtProgId.Text, PrgVerId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Public Sub GetFASAPGroupsByProgram(ByVal PrgVerId As String)
        ddlCampGrpId.Items.Clear()
        With ddlCampGrpId
            .DataTextField = "SAPID"
            .DataValueField = "SAPDescrip"
            .DataSource = (New ProgVerFacade).GetAllFASAPByProgram(PrgVerId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    ''' <summary>
    ''' This method is to fetch the R2T4 Calculation Period Types
    ''' </summary>
    Public Sub GetR2T4CalculationPeriodTypes()
        ddlCalculationPeriodTypeId.Items.Clear()
        With ddlCalculationPeriodTypeId
            .DataTextField = "Description"
            .DataValueField = "CalculationPeriodTypeId"
            .DataSource = (New CampusProgVerFacade).GetR2T4CalculationPeriodTypes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Public Sub GetCampusGroupsByProgramNew(ByVal PrgVerId As String)
        ddlCampGrpId.Items.Clear()
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = (New ProgVerFacade).GetAllCampusGroupsByProgram(txtProgId.Text, PrgVerId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub

    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    Public Function CheckForDupInstructionTypes(Optional ByVal EnteredInstructionType As String = "") As Boolean

        Dim lbInstType As New Label
        Dim lbinstType1 As New Label
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim intCnt As Integer
        If grdPrgVerInstructionType.Items.Count >= 1 Then
            If EnteredInstructionType = "" Then
                For i = 0 To grdPrgVerInstructionType.Items.Count - 1
                    lbInstType = CType(grdPrgVerInstructionType.Items(i).FindControl("lblInstructionTypeDesc"), Label)
                    intCnt = 0
                    For j = 0 To grdPrgVerInstructionType.Items.Count - 1
                        lbinstType1 = CType(grdPrgVerInstructionType.Items(j).FindControl("lblInstructionTypeDesc"), Label)
                        If lbinstType1.Text.Trim = lbInstType.Text.Trim Then intCnt = intCnt + 1
                    Next
                    If intCnt > 1 Then
                        Return True
                    End If
                Next
            Else
                For i = 0 To grdPrgVerInstructionType.Items.Count - 1
                    intCnt = 0
                    For j = 0 To grdPrgVerInstructionType.Items.Count - 1
                        lbinstType1 = CType(grdPrgVerInstructionType.Items(j).FindControl("lblInstructionTypeDesc"), Label)
                        If lbinstType1.Text.Trim = EnteredInstructionType Then intCnt = intCnt + 1
                    Next
                    If intCnt > 0 Then
                        Return True
                    End If
                Next
            End If

        End If

        Return False
    End Function

    Public Function CheckHoursSumValidateGreater(Optional ByVal GridHoursEntered As String = "0") As Boolean

        If (CType(txtHoursSum.Text, Decimal) + (CType(GridHoursEntered, Decimal))) > CType(txtHours.Text, Decimal) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function CheckHoursSumValidateLessThan(Optional ByVal GridHoursEntered As String = "0") As Boolean

        If (CType(txtHoursSum.Text, Decimal) + (CType(GridHoursEntered, Decimal))) < CType(txtHours.Text, Decimal) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub HoursSumValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        If CType(args.Value, Decimal) > CType(txtHours.Text, Decimal) Then
            DisplayErrorMessage("Instruction Hours cannot exceed the Prg Version Hours")
            validatetxt1 = True
        End If
    End Sub

    Private Function GetAFALocations() As List(Of String)
        Dim campGrpId = ddlCampGrpId.SelectedValue
        Dim locationsHelper = New LocationsHelper(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim locations = locationsHelper.GetAFALocationsByCampusGroup(campGrpId)
        Return locations
    End Function

    Private Sub SyncUpdatedProgramVersionWithAFA()

        Dim gPrgVerId As New Guid(txtPrgVerId.Text.Trim)
        Dim time = DateTime.Now
        Dim locations = GetAFALocations()
        If (locations.Count > 0) Then
            Dim wapiSettingsDA = New WapiSettingsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            If (wapiSetting IsNot Nothing) Then
                Dim afaPrograms = locations _
                        .Select(Function(loc) New AdvStagingProgramV1 With {.LocationCMSID = loc, .ProgramVersionID = gPrgVerId, .ProgramName = txtPrgVerDescrip.Text.Trim, .DateCreated = time,
                                   .UserCreated = AdvantageSession.UserState.UserName, .DateUpdated = time, .UserUpdated = AdvantageSession.UserState.UserName}).ToList()
                Try
                    Dim pvReq = New Fame.AFA.Api.Library.AcademicRecords.ProgramVersionRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)

                    Dim result = pvReq.SendUpdatedProgramVersion(afaPrograms)
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)


                End Try
            End If
        End If
    End Sub

    Private Sub SyncNewProgramVersionWithAFA()

        Dim gPrgVerId As New Guid(txtPrgVerId.Text.Trim)
        Dim time = DateTime.Now
        Dim locations = GetAFALocations()
        If (locations.Count > 0) Then
            Dim wapiSettingsDA = New WapiSettingsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            If (wapiSetting IsNot Nothing) Then
                Dim afaPrograms = locations _
                        .Select(Function(loc) New AdvStagingProgramV1 With {.LocationCMSID = loc, .ProgramVersionID = gPrgVerId, .DateCreated = time, .ProgramName = txtPrgVerDescrip.Text.Trim,
                                   .DateUpdated = time, .UserCreated = AdvantageSession.UserState.UserName, .UserUpdated = AdvantageSession.UserState.UserName}).ToList()
                Try
                    Dim pvReq = New Fame.AFA.Api.Library.AcademicRecords.ProgramVersionRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)

                    Dim result = pvReq.SendNewProgramVersion(afaPrograms)
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)


                End Try

            End If
        End If

    End Sub

End Class