﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentAttendanceFirst.aspx.vb" Inherits="AdvWeb.AR.StudentAttendance" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script src="../js/AuditHist.js" type="text/javascript"></script>
    <script type="text/javascript">
        function preventBackspace(e) {
            var evt = e || window.event;
            if (evt) {
                var keyCode = evt.charCode || evt.keyCode;
                if (keyCode === 8) {
                    if (evt.preventDefault) {
                        evt.preventDefault();
                    }
                    else {
                        evt.returnValue = false;
                    }
                }
            }
        }
    </script>
    <script>
        var StudentAttendanceStudentId = "<%=StudentId %>";

    </script>
    <script type="text/javascript">

            // Call javascript support code.

            $(document).ready(function () {
                var studentAttendance = new StudentAttendance.StudentAttendance();
            });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="../Scripts/Advantage.Client.AR.js"></asp:ScriptReference>
        </Scripts>

    </asp:ScriptManagerProxy>

    <div id="StudentAttendanceViewModelRegion">
        <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
            <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="400px" MaxWidth="400" MinWidth="400" Orientation="HorizontalTop">
                <!-- TAB Strip Left Zone. -->
                <div id="example" class="k-content">

                    <ul id="StudentAttendancePanelBar" style="margin: 0;">
                        <li class="k-state-active">
                            <span class="k-link k-state-selected">Select Filters:</span>
                            <div style="margin-left: 5px; margin-right: 5px; margin-top: 10px; padding-bottom: 5px">
                                <table style="width: 100%">
                                    <tr>
                                        <td>Enrollment:</td>
                                    </tr>
                                    <tr style="height: 30px">

                                        <td>
                                            <input id="StudentAttendanceFilterEnrollment" data-bind="value: selectedEnrollmentId" style="width: 100%;" /></td>
                                    </tr>
                                    <tr>
                                        <td>Term:</td>
                                    </tr>
                                    <tr style="height: 30px">
                                        <td>
                                            <input id="StudentAttendanceFilterTerm" value="1" style="width: 100%;" /></td>
                                    </tr>
                                    <tr>
                                        <td>Class:</td>
                                    </tr>
                                    <tr style="height: 30px">

                                        <td>
                                            <input id="StudentAttendanceFilterClass" value="1" style="width: 100%;"
                                                data-role="dropdownlist" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                            View Attendance by: 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <div class="k-block">

                                                <span>Days: </span>
                                                <input type="radio" id="StudentAttendanceSelectedDataView" value="Daily" name="report" data-bind=" checked: SelectedView" />
                                                <span>Weeks: </span>
                                                <input type="radio" value="Weekly" name="report" data-bind="checked: SelectedView" />

                                            </div>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <%--   <button id="StudentAttendanceGetSummaryButton" type="button" style="width: 150px; margin-right: 10px">Get Enrollment Summary </button>
                                            --%>
                                            <button id="StudentAttendanceGetButton" type="button" style="width: 130px">View attendance</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li id="PostMakeUp">Post Makeup to Class:&nbsp; <span id="makeupclass"></span>
                            <div style="margin-left: 5px; margin-right: 5px; margin-top: 10px; padding-bottom: 5px">
                                <div id="OnlyAbsentPresent" style="display: none">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>Select Makeup Day:</td>
                                            <td></td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td>
                                                <input id="StudentAttendanceAbsentPresentDateMakeUp" style="width: 100%" />
                                            </td>
                                            <td style="text-align: right">
                                                <button id="StudentAttendanceAbsentPresentPostMakeUp" title="Post Make Up Hours" type="button" style="width: 80px; margin-right: 10px">Post</button>
                                                <button id="StudentAttendanceAbsentPresentDeleteMakeUp" type="button" title="Delete Make Up Hours" style="width: 80px">Delete</button>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                                <div id="NoAbsentPresent" style="display: none">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>Post Time: 
                                            </td>
                                            <td>Select Make Up Date:</td>
                                            <td></td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td>

                                                <input id="StudentAttendanceMakeUpTb" value="1" />
                                            </td>
                                            <td style="text-align: right">
                                                <input id="StudentAttendanceMakeUpDatepicker" value="10/10/2011" style="width: 100%" />

                                            </td>

                                        </tr>
                                        <tr style="text-align: right">
                                            <td></td>
                                            <td style="text-align: right">
                                                <button id="StudentAttendancePostMakeUpButton" title="Post Make Up Hours" type="button" style="width: 100px; margin-right: 10px">Post</button>
                                                <button id="StudentAttendanceDeleteMakeUpButton" type="button" title="Delete Make Up Hours" style="width: 100px">Delete</button>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </li>
                        <li id="EnrollmentSummary"><span>Per Enrollment Attendance Summary</span>
                            <div id="StudentAttendanceSummaryContainer" style="margin-left: 5px; margin-right: 5px; margin-top: 10px; padding-bottom: 5px; visibility: hidden">
                                <div id="StudentAttendanceFSummaryGrid"></div>
                                <table style="background-color: #DAECF4; width: 100%; border: 1px solid #94C0D2;">
                                    <tr>
                                        <td>Total Scheduled: </td>
                                        <td><span id="StudentAttendanceTotalScheduled"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Porcentage of Present:</td>
                                        <td><span id="StudentAttendancePercentagePresent"></span></td>
                                    </tr>
                                </table>
                                <button id="ShowLoaInfo" type="button" style="width: 100%; margin: 10px auto">Student LOA, Status Change Details </button>
                                <%-- <button id="ShowEnrollmentInfo" type="button" style="width: 100%; margin: 10px auto">Enrollment: Show Enrollment Information </button>
                                <button id="ShowSuspension" type="button" style="width: 100%; margin: 10px auto">Suspension: Show Suspension Information </button>--%>
                                <%--  --%>
                            </div>
                            <li><span>Reports</span>
                                <div style="margin-left: 10px; margin-right: 10px; margin-top: 10px; padding-bottom: 5px">

                                    <table style="width: 100%">
                                        <tr>
                                            <td><span>Select Report: </span>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">

                                            <td style="height: 34px;">
                                                <input id="StudentAttendanceFilterReport" value="1" style="width: 100%;" />
                                            </td>
                                        </tr>
                                        <tr style="height: 30px; text-align: right">


                                            <td>
                                                <button id="StudentAttendanceReportButton" type="button" style="width: 100px">Go! </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </li>

                    </ul>

                </div>

            </telerik:RadPane>
            <telerik:RadSplitBar runat="server" CollapseMode="Forward"></telerik:RadSplitBar>
            <telerik:RadPane ID="RadPaneRight" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
                <div id="StudentAttendancePopUp" style="text-align: center;">
                    <div style="margin-left: 10px; margin-right: 10px; margin-top: 10px; padding-bottom: 5px">
                        <br />
                        <div style="background-color: #DAECF4; width: 100%; height: 30px; border: 1px solid #94C0D2;">LOA DAYS</div>
                        <div id="StudentAttendanceLoaGrid"></div>
                        <br />
                        <div style="background-color: #DAECF4; width: 100%; height: 30px; border: 1px solid #94C0D2;">ENROLLMENT STATUS CHANGES</div>
                        <div id="StudentAttendanceEnrollmentChanges"></div>
                        <br />
                        <div style="background-color: #DAECF4; width: 100%; height: 30px; border: 1px solid #94C0D2;">SUSPENDED DAYS</div>
                        <div id="StudentAttendanceSuspendedDays"></div>
                        <p />
                    </div>
                </div>

                <div id="lowPane" style="width: 99%; height: 90%; align-content: center; margin: 0 auto; visibility: hidden">
                    <table class="k-header">
                        <tr>
                            <th style="text-align: left">Class Summary:</th>
                        </tr>
                        <tr>
                            <td>
                                <div id="StudentAttendanceGridCourseSummary" style="margin-top: 2px; display: none;"></div>

                            </td>
                        </tr>
                    </table>
                    <br />
                    <table class="k-header">
                        <tr>
                            <th style="text-align: left">Post Attendance by Periods: </th>
                        </tr>
                        <tr>
                            <td>
                                <div id="StudentAttendanceGrid" style="margin-top: 5px; min-height: 100px; display: none"></div>
                                <div id="StudentAttendanceGridWeek" style="margin-top: 5px; min-height: 100px; display: none"></div>
                                <div id="StudentAttendanceAbsentPresentGrid" style="margin-top: 5px; min-height: 100px; display: none"></div>
                                <div id="StudentAttendanceAbsentPresentGridWeek" style="margin-top: 5px; min-height: 100px; display: none"></div>
                            </td>
                        </tr>
                    </table>

                </div>



            </telerik:RadPane>
        </telerik:RadSplitter>
    </div>

    <!--end validation panel-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>


