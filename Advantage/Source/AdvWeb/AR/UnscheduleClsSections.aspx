﻿<%@ Page Title="Cancel Class Meetings" Language="VB" MasterPageFile="~/NewSite.master"
    AutoEventWireup="false" CodeFile="UnscheduleClsSections.aspx.vb" Inherits="UnscheduleClsSections" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ShowInsertForm() {
            window.radopen("EditFormVB.aspx", "UserListDialog");
            return false;
        }
        function ShowEditForm(id, rowIndex) {
            var grid = $find("<%= RadGrid2.ClientID %>");
            var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
            grid.get_masterTableView().selectItem(rowControl, true);

            var reschedule = $telerik.findElement(rowControl, "chkReschedule");
            //reschedule.checked = true;

            var term = $telerik.findElement(rowControl, "lblTermDescrip");
            var coursecode = $telerik.findElement(rowControl, "lblCode");
            var course = $telerik.findElement(rowControl, "lblCodeDescrip");
            var instructor = $telerik.findElement(rowControl, "lblInstructor");
            var cancelfrom = $telerik.findElement(rowControl, "txtCancelFrom");
            var cancelto = $telerik.findElement(rowControl, "txtCancelTo");
            var campusid = "<%= campusid %>";
            var instructiontype = $telerik.findElement(rowControl, "lblInstructionTypeDescrip");
            var instructiontypeid = $telerik.findElement(rowControl, "lblInstructionTypeId");

            var termid = $telerik.findElement(rowControl, "lblTermId");
            var reqid = $telerik.findElement(rowControl, "lblReqId");

            if (reschedule.checked == true) {
                showwindow(id, term.innerHTML, coursecode.innerHTML, course.innerHTML, instructor.innerHTML, cancelfrom.value, cancelto.value, campusid, rowIndex, instructiontype.innerHTML, termid.value, reqid.value, instructiontypeid.value);
            }

        }
        function showwindow(id, term, coursecode, coursedescrip, instructor, cancelfrom, cancelto, campusid, rowindex, instructiontype, termid, reqid, instructiontypeid) {

            var oWnd = $find("<%= DialogWindow.ClientID %>");
            //alert(instructiontypeid);
            oWnd.setUrl("EditFormVB.aspx?ClsSectionId=" + id + '&term=' + term + '&coursecode=' + coursecode + '&coursedescrip=' + coursedescrip + '&instructor=' + instructor + '&cancelfrom=' + cancelfrom + '&cancelto=' + cancelto + '&campusid=' + campusid + '&rowindex=' + rowindex + '&instructiontype=' + instructiontype + '&termid=' + termid + '&reqid=' + reqid + '&instructiontypeid=' + instructiontypeid);
            oWnd.show();
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                var seldate = arg.selDate;
                var enddate = arg.endDate;
                var reschedulemeetingid = arg.reschedulemeetingid;
                var roomid = arg.roomid;
                var clssectionid = arg.clssectionid;
                var periodid = arg.periodid;
                var instructiontypeid = arg.instructiontypeid;
                var ismeetingrescheduled = arg.ismeetingrescheduled;

                //alert(enddate);

                // alert(roomid);
                var rowIndex = arg.rowindex;
                var grid = $find("<%= RadGrid2.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                var details = $telerik.findElement(rowControl, "txtDetails");
                details.innerHTML = seldate + '-' + enddate;
                $telerik.findElement(rowControl, "Hidden1").value = details.innerHTML;
                $telerik.findElement(rowControl, "HiddenRescheduledDate").value = reschedulemeetingid;
                $telerik.findElement(rowControl, "roomid").value = roomid;
                $telerik.findElement(rowControl, "clssectionid").value = clssectionid;
                $telerik.findElement(rowControl, "periodid").value = periodid;
                $telerik.findElement(rowControl, "instructiontypeid").value = instructiontypeid;
                $telerik.findElement(rowControl, "ismeetingrescheduled").value = ismeetingrescheduled;
            }
            function OnClientShow(oWnd) {
                if (document.documentElement && document.documentElement.scrollTop) {
                    var oTop = document.documentElement.scrollTop;
                    document.documentElement.scroll = "no";
                    document.documentElement.style.overflow = "hidden";
                    document.documentElement.scrollTop = oTop;

                } else if (document.body) {
                    var oTop = document.body.scrollTop;
                    document.body.scroll = "no";
                    document.body.style.overflow = "hidden";
                    document.body.scrollTop = oTop;
                }
            }



        }
    </script>
    <style type="text/css">
        select {
            font-family: Verdana, Arial, sans-serif;
            font-size: 0.8em;
        }

        .window {
            overflow: scroll;
        }
    </style>
    <style type="text/css">
        div.RadWindow_Web20 a.rwCloseButton {
            margin-right: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ddlTerm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnBuildList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dtlClsSectList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dtlClsSectList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlTermId">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlReqId" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnGetClsSects">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtSDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtSDate" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtEDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtEDate" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadWindowManager ID="radWindowManager1" runat="server" Font-Names="Verdana"
        Font-Size="10px" AutoSize="True">
    </telerik:RadWindowManager>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <asp:Panel ID="pnlDate" runat="server" Visible="True"
                                                CssClass="label" Width="750px" HorizontalAlign="Center">
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                    <tr>
                                                        <td class="contentcell"></td>
                                                        <td class="contentcell4"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblDate" runat="server" CssClass="label">Cancellation Start Date</asp:Label>
                                                        </td>
                                                        <td class="contentcell4">

                                                            <telerik:RadDatePicker ID="txtSDate" runat="server" Width="200px" MinDate='1/1/2005'
                                                                CssClass="labelgrid" Calendar-Font-Size="9px" Calendar-Font-Names="verdana"
                                                                Font-Size="9px" TabIndex="1">
                                                                <Calendar ID="Calendar1" runat="server">
                                                                    <SpecialDays>
                                                                        <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday" ItemStyle-Font-Size="9px" />
                                                                    </SpecialDays>
                                                                </Calendar>
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblTerm" runat="server" CssClass="Label">Term</asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <telerik:RadComboBox ID="ddlTerm" runat="server" DataTextField="TermDescrip" DataValueField="TermId"
                                                                AutoPostBack="True" CausesValidation="False" AppendDataBoundItems="true" Width="200px"
                                                                CssClass="labelgrid" DropDownCssClass="labelgrid"
                                                                TabIndex="4">
                                                            </telerik:RadComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="label5" runat="server" CssClass="label">Cancellation End Date</asp:Label>
                                                        </td>
                                                        <td class="contentcell4">

                                                            <telerik:RadDatePicker ID="txtEDate" runat="server" Width="200px" MinDate='<%# txtSDate.SelectedDate %>'
                                                                CssClass="labelgrid" Calendar-Font-Size="9px" Calendar-Font-Names="verdana"
                                                                TabIndex="2">
                                                                <Calendar ID="Calendar2" runat="server">
                                                                    <SpecialDays>
                                                                        <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday" />
                                                                    </SpecialDays>
                                                                </Calendar>
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblCourse" runat="server" CssClass="label">Course</asp:Label>
                                                        </td>
                                                        <td class="contentcell4">
                                                            <telerik:RadComboBox ID="ddlCourse" runat="server" DataTextField="Descrip" DataValueField="Reqid"
                                                                AppendDataBoundItems="true" Width="200px" CssClass="labelgrid" DropDownCssClass="labelgrid"
                                                                TabIndex="5">
                                                            </telerik:RadComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblInstructor" runat="server" CssClass="label">Instructor</asp:Label>
                                                        </td>
                                                        <td class="contentcell4" >
                                                            <telerik:RadComboBox ID="ddlInstructor" runat="server" DataTextField="InstructorDescrip"
                                                                DataValueField="InstructorId" AppendDataBoundItems="true" Width="200px"
                                                                CssClass="labelgrid" DropDownCssClass="labelgrid" TabIndex="6">
                                                            </telerik:RadComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell4blue" colspan="4">
                                                            <telerik:RadButton ID="btnGetClsSects" TabIndex="3" runat="server"
                                                               Text="Get ClassSections" CausesValidation="False">
                                                            </telerik:RadButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel2" runat="server" Visible="True" Width="90%" HorizontalAlign="center">
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                    <tr>
                                                        <td class="spacertables"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                                                AllowPaging="True" PageSize="8" GridLines="None" ShowGroupPanel="false"
                                                                Width="100%" AllowMultiRowSelection="true" Visible="false" CellPadding="0"
                                                                AllowFilteringByColumn="True">
                                                                <PagerStyle Mode="NextPrevAndNumeric"></PagerStyle>
                                                                <GroupingSettings CaseSensitive="false" />
                                                                <MasterTableView DataKeyNames="ClsSectionId" AllowMultiColumnSorting="True" GroupLoadMode="Server"
                                                                    Name="ClassSection" Width="1000px" CommandItemSettings-AddNewRecordText="Post new score"
                                                                    CommandItemDisplay="Top" HeaderStyle-Font-Size="10px" HeaderStyle-Font-Bold="true"
                                                                    HeaderStyle-VerticalAlign="Top" AllowFilteringByColumn="True" FilterItemStyle-Font-Size="9px"
                                                                    TableLayout="Auto" FilterItemStyle-HorizontalAlign="Left">
                                                                    <CommandItemTemplate>
                                                                        <div>
                                                                            <div style="float: left; text-align: left; vertical-align: top; width: 400px; height: 20px;">
                                                                                <asp:Label ID="Label2" runat="server" Font-Size="12px" Font-Bold="true">Cancel Classes and Reschedule</asp:Label>
                                                                            </div>
                                                                        </div>
                                                                    </CommandItemTemplate>
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn HeaderText="Code" ItemStyle-VerticalAlign="Top" DataField="Code"
                                                                            AutoPostBackOnFilter="true" CurrentFilterFunction="equalto" FilterDelay="4000"
                                                                            ShowFilterIcon="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCode" runat="server" Text='<%# Container.DataItem("Code") %>' CssClass="labelgrid"
                                                                                    Width="100px"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Course Description" ItemStyle-VerticalAlign="Top"
                                                                            AllowFiltering="true" DataField="Descrip" AutoPostBackOnFilter="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCodeDescrip" runat="server" Text='<%# Container.DataItem("Descrip") %>'
                                                                                    CssClass="labelgrid" Width="150px"></asp:Label>
                                                                                <asp:HiddenField ID="lblReqId" runat="server" Value='<%# Container.DataItem("ReqId") %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Section" ItemStyle-VerticalAlign="Top" AllowFiltering="True"
                                                                            DataField="ClsSection" AutoPostBackOnFilter="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblClsSection" runat="server" Text='<%# Container.DataItem("ClsSection") %>'
                                                                                    CssClass="labelgrid" Width="50px"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Meeting Schedule" ItemStyle-VerticalAlign="Top"
                                                                            AllowFiltering="False" DataField="Schedule">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSchedule" runat="server" Text='<%# Container.DataItem("Schedule") %>'
                                                                                    CssClass="labelgrid" Width="130px"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Instruction Type" ItemStyle-VerticalAlign="Top"
                                                                            AllowFiltering="True" DataField="InstructionTypeDescrip" AutoPostBackOnFilter="true"
                                                                            FilterControlWidth="50px">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblInstructionTypeDescrip" runat="server" Text='<%# Container.DataItem("InstructionTypeDescrip") %>'
                                                                                    CssClass="labelgrid" Width="50px"></asp:Label>
                                                                                <asp:HiddenField ID="lblInstructionTypeId" runat="server" Value='<%# Container.DataItem("InstructionTypeId") %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridClientSelectColumn HeaderText="Cancel" HeaderAbbr="Cancel" HeaderButtonType="LinkButton"
                                                                            HeaderTooltip="Cancel" Text="Cancel" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top"
                                                                            UniqueName="Cancel">
                                                                        </telerik:GridClientSelectColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Cancel from" ItemStyle-Width="100px" ItemStyle-VerticalAlign="Top"
                                                                            AllowFiltering="False">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblClsSectionId" runat="server" Visible="false" Text='<%# Container.DataItem("ClsSectionId") %>'></asp:Label>
                                                                                <asp:Label ID="lblClsMeetingId" runat="server" Visible="false" Text='<%# Container.DataItem("ClsSectMeetingId") %>'></asp:Label>
                                                                                <telerik:RadDatePicker ID="txtCancelFrom" runat="server" Width="100px" MinDate='<%# txtSDate.SelectedDate %>'
                                                                                    MaxDate='<%# txtEDate.SelectedDate %>' CssClass="labelgrid" Calendar-Font-Size="9px"
                                                                                    Calendar-Font-Names="verdana" DateInput-Font-Size="9px">
                                                                                    <Calendar runat="server" ID="CancelStartCalendar">
                                                                                        <SpecialDays>
                                                                                            <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday" />
                                                                                        </SpecialDays>
                                                                                    </Calendar>
                                                                                </telerik:RadDatePicker>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Cancel to" ItemStyle-Width="100px" ItemStyle-VerticalAlign="Top"
                                                                            AllowFiltering="False">
                                                                            <ItemTemplate>
                                                                                <telerik:RadDatePicker ID="txtCancelTo" runat="server" Width="100px" MinDate='<%# txtSDate.SelectedDate %>'
                                                                                    MaxDate='<%# txtEDate.SelectedDate %>' Calendar-Font-Size="9px" Calendar-Font-Names="verdana"
                                                                                    CssClass="labelgrid" DateInput-Font-Size="9px">
                                                                                    <Calendar ID="CancelEndCalendar" runat="server">
                                                                                        <SpecialDays>
                                                                                            <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday" />
                                                                                        </SpecialDays>
                                                                                    </Calendar>
                                                                                </telerik:RadDatePicker>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkReschedule" runat="server" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Rescheduled Date" AllowFiltering="false"
                                                                            HeaderStyle-Width="200px">
                                                                            <ItemStyle VerticalAlign="Top" Width="200px" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="txtDetails" runat="server" Visible="true" CssClass="labelgrid"></asp:Label>
                                                                                <asp:HiddenField ID="Hidden1" runat="server" />
                                                                                <asp:HiddenField ID="HiddenRescheduledDate" runat="server" />
                                                                                <asp:HiddenField ID="roomid" runat="server" />
                                                                                <asp:HiddenField ID="clssectionid" runat="server" />
                                                                                <asp:HiddenField ID="periodid" runat="server" />
                                                                                <asp:HiddenField ID="startdate" runat="server" />
                                                                                <asp:HiddenField ID="enddate" runat="server" />
                                                                                <asp:HiddenField ID="instructiontypeid" runat="server" />
                                                                                <asp:HiddenField ID="ismeetingrescheduled" runat="server" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Comments" ItemStyle-VerticalAlign="Top" AllowFiltering="False"
                                                                            Visible="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblComments" runat="server" CssClass="labelgrid" Width="100px"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Term Descrip" ItemStyle-VerticalAlign="Top"
                                                                            AllowFiltering="True" DataField="TermDescrip" AutoPostBackOnFilter="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTermDescrip" runat="server" Text='<%# Container.DataItem("TermDescrip") %>'
                                                                                    CssClass="labelgrid" Width="100px"></asp:Label>
                                                                                <asp:HiddenField ID="lblTermId" runat="server" Value='<%# Container.DataItem("TermId") %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Instructor" ItemStyle-VerticalAlign="Top"
                                                                            AllowFiltering="True" DataField="FullName" AutoPostBackOnFilter="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblInstructor" runat="server" Text='<%# Container.DataItem("FullName") %>'
                                                                                    CssClass="labelgrid" Width="100px"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                                <ClientSettings EnableRowHoverStyle="true">
                                                                    <Selecting AllowRowSelect="True" />
                                                                </ClientSettings>
                                                            </telerik:RadGrid>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:Panel>
                                        <!--end table content-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--end table content-->
            <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
            <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>

            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                Display="None" ErrorMessage="CustomValidator">
            </asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadWindow ID="DialogWindow" Behaviors="Close, Move" VisibleStatusbar="false"
        ReloadOnShow="false" runat="server" VisibleOnPageLoad="false" Top="0" Left="20"
        OnClientClose="OnClientClose" AutoSize="false" Modal="true" OffsetElementID="offsetElement"
        Width="800px" Height="400px" AutoSizeBehaviors="Width, Height">
    </telerik:RadWindow>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
