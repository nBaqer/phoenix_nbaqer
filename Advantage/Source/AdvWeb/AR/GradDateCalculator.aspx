﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="GradDateCalculator.aspx.vb" Inherits="GradDateCalculator" %>

<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Grad Date Calculator</title>

    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />


    <%: Styles.Render("~/bundles/popupstyle") %>
    <%: Scripts.Render("~/bundles/kwindowscripts") %>

    <script type="text/javascript" src="../Scripts/Fame.Advantage.Common.js"></script>
    <script type="text/javascript" src="../Scripts/Fame.Advantage.Tools.js"></script>

    <script id="javascriptTemplate" type="text/x-kendo-template">
 
        <span>
        #:description# (
        #= startDate.substr(0,10) # to #= endDate.substr(0,10) #)
   
        </span>
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var resizeAll = function () {
                expandContentDivs(tabStripElement.children(".k-content"));
            }

            var tabStripElement = $("#tabstrip").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fade"
                    }
                }
            });

            tabStripElement.parent().attr("id", "tabstrip-parent");

            var tabStrip = tabStripElement.data("kendoTabStrip");

            var expandContentDivs = function (divs) {
                var visibleDiv = divs.filter(":visible");
                divs.height(tabStripElement.innerHeight()
                    - tabStripElement.children(".k-tabstrip-items").outerHeight()
                    - parseFloat(visibleDiv.css("padding-top"))
                    - parseFloat(visibleDiv.css("padding-bottom"))
                    - parseFloat(visibleDiv.css("border-top-width"))
                    - parseFloat(visibleDiv.css("border-bottom-width"))
                    - parseFloat(visibleDiv.css("margin-bottom")));

                // all of the above padding/margin/border calculations can be replaced by a single hard-coded number for improved performance
            }


            tabStripElement.parent().width(425);
            //tabStripElement.parent().css({"margin":"auto"});

            var gradDateCalculator = new Api.ViewModels.Tools.GradDateCalculator();
            gradDateCalculator.initialize();
            resizeAll();


        });
    </script>

    <style type="text/css">
        .calculate-container {
            display: inline-block;
            color: rgba(0,0,0,0.75);
            border: 1px solid #0d47a1;
            font-size: 10px;
            background: #fff;
            cursor: pointer;
            vertical-align: middle;
            max-width: 100px;
            height: 60px;
            width: 120px;
            padding: 5px;
            text-align: center;
            box-shadow: 1px 1px 0px 0px rgba(163, 208, 228, 1);
            -webkit-box-shadow: 1px 1px 0px 0px rgba(163, 208, 228, 1);
            -moz-box-shadow: 1px 1px 0px 0px rgba(163, 208, 228, 1);
        }

        #gradDate {
            border: 2px inset #1565c0;
            height: 25px;
            width: 75px;
            text-align: center;
        }


        .inLineBlock {
            display: inline-block;
        }

        .left-margin {
            margin-left: 15px;
        }

        .sm-num-input {
            width: 60px;
        }

        .formRow {
            margin-top: 10px;
            margin-bottom: 10px;
            display: block;
        }

        #tabstrip-parent,
        #tabstrip {
            height: 99%;
            margin: 0;
            padding: 0;
            border-width: 0;
        }

        .k-datepicker .k-icon {
            margin-left: -8px !important;
        }

        .k-calendar {
            overflow-y: scroll;
        }

        #calculate-btn:hover {
            background-color: #0d47a1;
            color: white !important;
        }

        #calculate-btn:active {
            background-color: #0d47a1;
            transform: translateY(4px);
        }

        #calculate-btn span:hover {
            color: white !important;
        }

        #calculate-btn {
            color: #0d47a1;
        }

        #tabstrip {
            background-color: #1565c0 !important;
        }

        .k-window-titlebar.k-header {
            background-color: #0d47a1 !important
        }

        .k-tabstrip > .k-tabstrip-items > .k-state-default {
            background-color: #1565c0 !important;
            border-right: 1px solid #0d47a1 !important;
            color: white;
        }

        .k-tabstrip > .k-tabstrip-items > .k-state-active {
            background-color: #0d47a1 !important;
        }

            .k-tabstrip > .k-tabstrip-items > .k-state-active:hover {
                border-style: solid !important;
                border-width: 1px 1px 0 !important;
            }

        .k-tabstrip > .k-tabstrip-items > .k-state-default:hover {
            background-color: #0d47a1 !important;
            border-style: solid !important;
            border-width: 1px 1px 0 !important;
            border-top: 0px solid #1565c0 !important;
            border-left: 0px solid !important;
        }

        .k-tabstrip > .k-tabstrip-items > .k-state-default > .k-link {
            color: #fff !important;
        }
    </style>
</head>
<body runat="server">

    <div class="grad-calculator" id="tabstrip">
        <ul>
            <li class="k-state-active">Grad Date Calculation</li>
            <li>Holidays & Closed Days</li>
        </ul>
        <div>
            <p>
                Enter the student's hours for each day the school is open, the student's start date and the total hours in the program. Verify that the "Holidays & Closed Days" are accurate,
                then click the "Calculate Graduation Date" button.
            </p>

            <div class="formRow">
                <div class="left leftLable" style="width: 120px;">
                    Campus
                </div>
                <div class="inLineBlock left-margin">
                    <input id="ddlCampus" class="fieldlabel1 inputCustom form-control k-textbox" required />
                </div>
            </div>
            <div class="formRow">
                <div class="left leftLable" style="width: 120px;">
                    Program Version
                </div>
                <div class="inLineBlock left-margin">
                    <input id="ddlProgramVersion" class="fieldlabel1 inputCustom form-control k-textbox" required />
                </div>
            </div>
            <div class="formRow">
                <div class="left leftLable" style="width: 120px;">
                    Load from schedule
                </div>
                <div class="inLineBlock left-margin">
                    <input id="ddlLoadFromSchedule" class="fieldlabel1 inputCustom form-control k-textbox" required />
                </div>
            </div>
            <div class="formRow">
                <div class="left leftLable" style="width: 80px;">
                    Student Start Date
                </div>
                <div class="left left-margin">
                    <input id="dpStudentStartDate" class="inputCustom form-control k-textbox" style="width: 120px;" required />
                </div>
                <div class="left leftLable" style="margin-left: 10px; width: 80px;">
                    Total Prog. Hours
                </div>
                <div class="left">
                    <input id="txtHoursInProgram" class="inputCustom form-control k-textbox" style="width: 80px;" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="formRow">
                <div class="left leftLable" style="width: 75px;">
                    Sunday
                </div>
                <div class="left left-margin">
                    <input id="txtSundayHours" class="sm-num-input inputCustom form-control k-textbox" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                </div>

                <div class="left leftLable" style="margin-left: 25px; width: 75px;">
                    Monday
                </div>
                <div class="left left-margin">
                    <input id="txtMondayHours" class="sm-num-input inputCustom form-control k-textbox" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                </div>

                <div style="clear: both;"></div>
            </div>


            <div class="formRow">
                <div class="left leftLable" style="width: 75px;">
                    Tuesday
                </div>
                <div class="left left-margin">
                    <input id="txtTuesdayHours" class="sm-num-input inputCustom form-control k-textbox" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                </div>
                <div class="left leftLable" style="margin-left: 25px; width: 75px;">
                    Wednesday
                </div>
                <div class="left left-margin">
                    <input id="txtWednesdayHours" class="sm-num-input inputCustom form-control k-textbox" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                </div>
                <div style="clear: both;"></div>

            </div>

            <div class="formRow">
                <div class="left leftLable" style="width: 75px;">
                    Thursday
                </div>
                <div class="left left-margin">
                    <input id="txtThursdayHours" class="sm-num-input inputCustom form-control k-textbox" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                </div>
                <div class="left leftLable" style="margin-left: 25px; width: 75px;">
                    Friday
                </div>
                <div class="left left-margin">
                    <input id="txtFridayHours" class="sm-num-input inputCustom form-control k-textbox" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                </div>
                <div style="clear: left;"></div>

            </div>

            <div class="formRow">
                <div class="left leftLable" style="width: 75px;">
                    Saturday
                </div>
                <div class="left left-margin">
                    <input id="txtSaturdayHours" class="sm-num-input inputCustom form-control k-textbox" step='0.01' value='0.00' placeholder="0.00" type="number" min="0" required />
                </div>
                <div class="calculate-container" id="calculate-btn" style="float: right;">
                    <span class="k-icon k-i-calculator" id="calculator-icon" style="font-size: 24px;"></span>
                    <span style="display: block;">Calculate<br />
                        Graduation Date</span>
                </div>
                <div style="clear: left;"></div>

            </div>
            <div class="formRow">

                <span class="left leftLable" style="width: 125px;">Graduation Date:
                </span>
                <span class="left left-margin" id="gradDate" style="line-height: 2;">N/A
                </span>
            </div>
        </div>
        <div>

            <div class="formRow" style="height: 25px;">
                <div class="left leftLable" style="width: 100%;">
                    Holidays & Closed Days
                </div>


            </div>
            <div class="formRow" style="height: 350px;">
                <div class="left leftLable" style="width: 100%;">
                    <select id="holidaysListBox" style="width: 100%; height: 350px;"></select>
                </div>

            </div>

        </div>
    </div>



</body>


</html>

