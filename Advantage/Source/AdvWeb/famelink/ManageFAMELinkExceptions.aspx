﻿<%@ Page Language="VB"  MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ManageFAMELinkExceptions.aspx.vb" Inherits="FameLink_ManageFAMELinkExceptions" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
     <title>Manage FAMELink Exceptions</title>
   <link rel="stylesheet" type="text/css" href="../css/localhost.css" />
   <script src="common.js" type="text/javascript">
       function InitComboFooter(sender) {
           // get a reference to the footer DOM element
           var footer = sender._getFooterElement();
           // set its innerHTML to the initial text of the combo box
           footer.innerHTML = sender.get_text();
       }

       function IndexChanged(sender, args) {
           // get a reference to the footer DOM element
           var footer = sender._getFooterElement();
           // set its innerHTML to the selected item text
           footer.innerHTML = args.get_item().get_text();
       }

       function mngRequestStarted(ajaxManager, eventArgs) {
           if (eventArgs.EventTarget == "btnExportToExcel") {
               eventArgs.EnableAjax = false;
           }
       }
       function CheckAll(rg, ck) {
           var RadGridAward;
           RadGridAward = rg;
           var checkboxes = RadGridAward.getElementsByTagName("input");
           var index;
           for (index = 0; index < checkboxes.length; index++) {
               if (ck.checked == true) {
                   var str = checkboxes[index].name
                   var pos1 = str.indexOf("chkReprocess")
                   if (pos1 >= 0) {
                       checkboxes[index].checked = true;
                   }

               }
               else {
                   checkboxes[index].checked = false;
               }
           }
       }


       function SetDivPosition() {
           var intY = document.getElementById("grdWithScroll").scrollTop;
           document.cookie = "yPos=!~" + intY + "~!";
       }
   </script>
  <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
   <style>
   h2 { font: bold 11px verdana; color: #000066; background-color: transparent; width: auto; margin: 0; padding: 0; border: 0;text-align: left;}
   .FileInput
    {
	    margin: 4px;
	    vertical-align: middle;
	    font: normal 10px verdana;
	    text-align:left;
	    color: #000066;
	    width: 300px;
    }
    .ProcessFile
    {
	    margin: 4px;
	    vertical-align: middle;
	    font: normal 10px verdana;
	    text-align:center;
	    color: #000066;
    }
   </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
     <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table2">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="DetailsFrameTop">
        
                <table id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="MenuFrame" align="right"><asp:button id="btnSave" runat="server" CssClass="save" Text="Save" CausesValidation="false" Enabled="false"></asp:button><asp:button id="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:button><asp:button id="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false"></asp:button></td>
							
							</tr>
				</table>
        
         
           
            
                 <asp:Table runat="server" ID="tbSelectFile" Width="50%">
                <asp:TableRow>
                    <asp:TableCell Width="15%" HorizontalAlign="Left" VerticalAlign="Top">
                        <asp:Label ID="lblCampus" Text="Campus" runat="Server" CssClass="LabelBold"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="25%" HorizontalAlign="Left" VerticalAlign="Top">
                        <asp:DropDownList ID="ddlCampus" runat="server" CssClass="DropDownLists" Width="230px" AutoPostBack="true"></asp:DropDownList>
                        <br />
                         <asp:RequiredFieldValidator runat="server" ID="rfvCampus" ControlToValidate="ddlCampus" Text="You must select a campus."></asp:RequiredFieldValidator>                       
                    </asp:TableCell> 
                    <asp:TableCell width="15%" HorizontalAlign="left" wrap="false" VerticalAlign="top">
                        <asp:Label ID="lblFileName" Text="Exception File" runat="Server" CssClass="LabelBold"></asp:Label>                    
                    </asp:TableCell> 
                    <asp:TableCell Width="25%" HorizontalAlign="Left" VerticalAlign="Top">
                        <asp:DropDownList ID="ddlFileNames" runat="server" CssClass="DropDownLists" Width="230px" Enabled="false" AutoPostBack="true">
                         </asp:DropDownList><br />
                         <asp:RequiredFieldValidator runat="server" ID="rfvFileNames" ControlToValidate="ddlFileNames" Text="You must select a file."></asp:RequiredFieldValidator>                      
                    </asp:TableCell>                    
                    <asp:TableCell ID="TableCell1" width="20%" HorizontalAlign="left" VerticalAlign="Top" wrap="false" runat="Server"> 
                         <asp:CheckBox ID="chkShowRemoved" runat="server" Width="120px" Text="Show Hidden" AutoPostBack="true"/>
                         <asp:Button ID="btnFilter" runat="server" CssClass="button" Text="Get Exceptions" width="120px" />
                    </asp:TableCell>                   
                </asp:TableRow>
            </asp:Table>
             <table>
                <tr>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <br />
             
             <asp:Table runat="server" ID="tblException" CellPadding="0" CellSpacing="0" cssClass="DataGridHeader"
              Width="98%" Visible="false" Height="85%">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell CssClass="TableHeader" Wrap="false">
                                 <asp:Table ID="Table1" runat="server" Width="100%" >
                                    <asp:TableRow>                                   
                                        <asp:TableCell>
                                            <asp:Label ID="Label6" runat="server" Text="List of data not posted successfully" Width="300px" visible="false"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" Width="150px" Visible="false" ></asp:Label>
                                        </asp:TableCell>                                        
                                        <asp:TableCell Wrap="false" HorizontalAlign="Right">
                                            <asp:Button ID="btnExportToExcel" runat="Server" Text=" Export to Excel " cssClass="button" width="150px" />
                                            <asp:Button ID="btnSubmit" runat="Server" Text="Submit" cssClass="button" width="150px" />
                                        </asp:TableCell>                                          
                                    </asp:TableRow>
                                 </asp:Table>
                        </asp:TableHeaderCell> 
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell >
                        <div >
                            <telerik:RadGrid ID="dgExceptionReport" runat="server" ShowStatusBar="true" AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgExceptionReport_NeedDataSource" ShowFooter="false" >
                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                    <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                    <MasterTableView AutoGenerateColumns="False" EnableColumnsViewState="false">
                                        <Columns>
                                            <telerik:GridTemplateColumn SortExpression="SSN" AllowFiltering="true" DataField="SSN">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlbnSSN" runat="server" Text="SSN"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSSN" runat="Server" Text='<%# Container.DataItem("SSN") %>' CssClass="Label"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn SortExpression="FAID" DataField="FAID">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlbnFAID" runat="server" Text="FA Identifier"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFAID" Text='<%# Container.DataItem("FAID") %>' CssClass="Label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="false">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblSE" runat="server" Text="Student Enrollment" Width="250px"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlStuEnrollId" runat="server" CssClass="DropDownList" Width="250px"></asp:DropDownList>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn SortExpression="Fund" DataField="Fund">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblFund" runat="server" Text="Fund"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFund" Text='<%# Container.DataItem("Fund") %>' CssClass="Label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="GrossAmount">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblGA" runat="server" Text="Gross Amount"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGrossAmount" Text='<%# Container.DataItem("GrossAmount") %>' CssClass="Label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn >                                            
                                            <telerik:GridTemplateColumn SortExpression="Message" DataField="Message">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblR" runat="server" Text="Reason"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMessage" Text='<%# Container.DataItem("Message") %>' CssClass="Label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>                                           
                                            <telerik:GridTemplateColumn Visible="true" AllowFiltering="false">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblAY" runat="server" Text="Award Year"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAwardYear" Text='<%# Container.DataItem("AwardYear") %>' CssClass="Label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn Visible="false">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblLF" runat="server" Text="Loan Fees"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLoanFees" Text='<%# Container.DataItem("LoanFees") %>' CssClass="Label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                             <telerik:GridTemplateColumn Visible="false">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblLSD" runat="server" Text="Loan Start Date"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLoanBeginDate" Text='<%# Container.DataItem("LoanStartDate") %>' CssClass="Label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                             <telerik:GridTemplateColumn Visible="false">
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblLED" runat="server" Text="Loan End Date"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLoanEndDate" Text='<%# Container.DataItem("LoanEndDate") %>' CssClass="Label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>                                                                                                                          
                                            <telerik:GridTemplateColumn AllowFiltering="false">
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeader" Text="Reprocess" runat="server"></asp:Label><br />
                                                    <input id="chkReprocessCheck" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkReprocess',document.forms[0].chkReprocessCheck.checked)" />                                                    
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkReprocess" runat="server" />
                                                </ItemTemplate>                                            
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="false" >
                                                <HeaderTemplate>
                                                    <asp:Label ID="hlblHide" runat="server" Text="Hide?"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkRemove" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Hide") %>'  />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn  HeaderText="ExceptionId" UniqueName="ExceptionReportId" Visible="false" AllowFiltering="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExceptionReportId" Text='<%# Container.DataItem("ExceptionReportId") %>' CssClass="Label" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                             <telerik:GridTemplateColumn  HeaderText="RecordPosition" UniqueName="RecordPosition" Visible="false" AllowFiltering="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRecordPosition" Text='<%# Container.DataItem("RecordPosition") %>' CssClass="Label" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                   

                                   
                            </telerik:RadGrid>  
                          </div>  
                        </asp:TableCell>
                    </asp:TableRow>
                     <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="dgExceptionReportExportToExcel" runat="server" ShowStatusBar="true" AllowFilteringByColumn="false" AutoGenerateColumns="False" PageSize="25" AllowSorting="false" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgExceptionReport_NeedDataSource" ShowFooter="false">
                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                    <MasterTableView AutoGenerateColumns="False">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="strSSN"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="strFAID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>                                            
                                            <telerik:GridBoundColumn DataField="GrossAmount" HeaderText="Gross Amount" SortExpression="GrossAmount" UniqueName="GrossAmount"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message" UniqueName="Message"></telerik:GridBoundColumn>      
                                            <telerik:GridBoundColumn DataField="DisbursementDate" HeaderText="Disb Date" SortExpression="DisbursementDate" UniqueName="DisbDate"></telerik:GridBoundColumn>   
                                            <telerik:GridBoundColumn DataField="AwardYear" HeaderText="Award Year" SortExpression="AwardYear" UniqueName="AwardYear"></telerik:GridBoundColumn>   
                                            <telerik:GridBoundColumn DataField="LoanFees" HeaderText="Loan Fees" SortExpression="LoanFees" UniqueName="LoanFees"></telerik:GridBoundColumn>   
                                            <telerik:GridBoundColumn DataField="LoanStartDate" HeaderText="Loan Start Date" SortExpression="LoanStartDate" UniqueName="LoanStartDate"></telerik:GridBoundColumn>   
                                            <telerik:GridBoundColumn DataField="LoanEndDate" HeaderText="Loan End Date" SortExpression="LoanEndDate" UniqueName="LoanEndDate"></telerik:GridBoundColumn>                                                                                       
                                            
                                        </Columns>
                                    </MasterTableView>
                            </telerik:RadGrid>  
                        </asp:TableCell>
                    </asp:TableRow>
               </asp:Table>
                
           </td>
                    <!-- end rightcolumn -->
                </tr>
                <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
            </table>
  </telerik:RadPane>
    </telerik:RadSplitter> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server"></asp:Content>         
          
