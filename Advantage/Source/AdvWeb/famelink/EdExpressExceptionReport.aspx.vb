﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common
Imports FAME.DataAccessLayer
Imports System.IO

Partial Class FameLink_EdExpressExceptionReport
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Protected CampusId As String
    Dim userId As String
    Dim resourceId As Integer
    Private myEDFacade As New EDFacade
    'Private myEdExImport As New ImportEdExpObject
    Private strLogEntry As New System.Text.StringBuilder(10000)
    Private strFileName As String
    Private myMsgCollection As New EDCollectionMessageInfos
    Private ConString As String  '"Provider=SQLOLEDB;Data Source=GOVINDARAJULU\DEVSERVER2005;Initial Catalog=ClockHourDB;User ID=sa;Password=test1"
    Dim arrREFU As New ArrayList
    Dim incrRCVD As Integer = 0
    Dim arrayRCVD() As String
    Dim dtLogMessage, dtPATSData, dtPATSDisb, dtDirectLoan, dtDLSchDisb, dtDLActualDisb As New DataTable()
    Dim strAppendMessage As New StringBuilder
    Dim intTotalRecordCount As Integer = 0
    Protected MyAdvAppSettings As AdvAppSettings

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim blnResult As Boolean = False
        Dim clsflFile As New EDFileInfo
        Dim strLogMessage As New StringBuilder
        Dim intInitialImportSuccessful As Integer = 0
        Dim strValidMessage As String = ""
        Dim intFAIDExists As Integer = 0
        Dim intHEADExists As Integer = 0
        Dim intDISBExists As Integer = 0
        Dim strAcademicYearId As String = ""
        Dim strAwardStartDate As String = ""
        Dim strAwardEndDate As String = ""

        strLogEntry = New StringBuilder

        ''dgExceptionReportParent.Visible = False
        ''dgExceptionReportChild.Visible = False
        ''tblException.Visible = False
        If ddlFileNames.SelectedValue = "" Then
            DisplayErrorMessage("Please select EDExpress File to View Exception Report")
            ClearDataGrid()
            Exit Sub
        End If
        ClearDataGrid()
        Session("FileName") = ddlFileNames.SelectedValue

        Dim str1 As String = String.Concat(MyAdvAppSettings.AppSettings("ExceptionPathEdExp"), ddlFileNames.SelectedItem.Text)
        If MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer").ToLower = "yes" Then
            str1 = String.Concat(MyAdvAppSettings.AppSettings("RemoteEDExpressException"), ddlFileNames.SelectedItem.Text)
            clsflFile.datafromremotecomputer = True
            clsflFile.networkuser = MyAdvAppSettings.AppSettings("RemoteEDExpressUsername")
            clsflFile.networkPassword = MyAdvAppSettings.AppSettings("RemoteEDExpressPassword")
        End If
        clsflFile.strFileName = str1

        With strLogEntry
            .Append("Start Processing of importation of EDExpress file: " & UCase(clsflFile.strFileName) + ControlChars.CrLf)
        End With
        With strLogMessage
            .Append("Start Importing EDExpress file: " & UCase(clsflFile.strFileName) + ControlChars.CrLf)
        End With

        If Len(clsflFile.strError) = 0 Then
            If clsflFile.iRecordCount <> 0 Then
                Session("RecordCount") = clsflFile.iRecordCount

                If MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer") = "yes" Then
                    Dim strSourceFileLocation As String = MyAdvAppSettings.AppSettings("RemoteEDExpressException") + "\" + ddlFileNames.SelectedItem.Text
                    blnResult = myEDFacade.ProcessExceptionEDFile(clsflFile, myMsgCollection, ddlFileNames.SelectedItem.Text, strAcademicYearId, strAwardStartDate, strAwardEndDate, strSourceFileLocation)
                Else
                    Dim strLocalFileLocation As String = MyAdvAppSettings.AppSettings("ExceptionPathEdExp") + ddlFileNames.SelectedItem.Text
                    blnResult = myEDFacade.ProcessExceptionEDFile(clsflFile, myMsgCollection, ddlFileNames.SelectedItem.Text, strAcademicYearId, strAwardStartDate, strAwardEndDate, strLocalFileLocation)

                End If
                If Not strAcademicYearId = "" Then
                    ddlAcademicYear.SelectedValue = strAcademicYearId
                End If

                If strAwardStartDate <> "" Then
                    txtAwardYearStartDate.SelectedDate = strAwardStartDate
                End If

                If strAwardEndDate <> "" Then
                    txtAwardYearEndDate.SelectedDate = strAwardEndDate
                End If

                updatelistMessages(myMsgCollection)
                imgProcess.Visible = False
                If blnResult Then
                    With strLogEntry
                        .Append("SUCCESSFUL Processsing of EDExpress file: " + clsflFile.strFileName + ControlChars.CrLf)
                        .Append("File Size: " + clsflFile.iFileSize.ToString + ControlChars.CrLf)
                        .Append("with " + clsflFile.iRecordCount.ToString + " records of type " + clsflFile.strMsgType + ControlChars.CrLf + "PARSED and waiting to be processed" + ControlChars.CrLf)
                    End With
                    With strLogMessage
                        .Append(".Process Successful " + ControlChars.CrLf)
                    End With
                Else
                    With strLogEntry
                        .Append("FAILED Processing of EDExpress file: " & clsflFile.strFileName + ControlChars.CrLf)
                        .Append("Additional Information: " & clsflFile.strError + ControlChars.CrLf)
                    End With
                    With strLogMessage
                        .Append(".Process Failed " + ControlChars.CrLf)
                    End With

                End If
            Else
                With strLogEntry
                    .Append("FAILED Processing of EDExpress file:" & clsflFile.strFileName + ControlChars.CrLf)
                    .Append("File Size: " + clsflFile.iFileSize.ToString + ControlChars.CrLf)
                    .Append("with " + clsflFile.iRecordCount.ToString + " records of type " + clsflFile.strMsgType + ControlChars.CrLf + "PARSED and waiting to be processed" + ControlChars.CrLf)
                    .Append("Additional Information: " & clsflFile.strError + ControlChars.CrLf)
                End With
                With strLogMessage
                    .Append("Processing of EDExpress file:" & clsflFile.strFileName + " FAILED" + ControlChars.CrLf)
                End With
            End If
        Else
            With strLogEntry
                .Append("Processing of EDExpress file: " & clsflFile.strFileName + " FAILED" + ControlChars.CrLf)
                .Append("Additional Information: " & clsflFile.strError + ControlChars.CrLf)
            End With
            With strLogMessage
                .Append("Processing of EDExpress file:" & clsflFile.strFileName + " FAILED" + ControlChars.CrLf)
            End With
        End If
        'Response.Write(clsflFile.strError)
        strLogEntry.Append(vbCrLf)
        strLogEntry.Append("---------------------------------------------------------------------------------------")
        'BuildLogEntry(strLogMessage)
        ''dgLogMessages.DataSource = Session("dtLogMessage")
        ''dgLogMessages.DataBind()
        txtLog.Text = txtLog.Text + strLogEntry.ToString
    End Sub
    Private Sub updatelistMessages(ByVal myMsgCollection As EDCollectionMessageInfos)
        Dim nbrMessages As Integer = myMsgCollection.Count
        Dim intNdx As Integer = 0
        Dim strEntry As New System.Text.StringBuilder(1000)
        Dim fName As String
        ' Dim strFormat As String
        'Dim intOldSSN As String = ""
        Dim strMessageType As String = ""
        Dim strDBIndicator As String = ""
        Dim strOrigSSN As String = ""
        Dim strAwardId As String = ""
        Dim strMPNStatus As String = ""
        Dim strFileNameOnly As String = ""
        Dim strM As String = ""
        Dim strASD As String = ""
        Dim strAED As String = ""
        Dim boolIsParsed As Boolean
        Dim strAwardYear As String = ""
        Dim dbRebateAmount As Double = 0.0
        Dim intDLID As Integer
        'lstMessages.UseTabStops = True
        'lstMessages.BeginUpdate()
        For intNdx = 0 To nbrMessages - 1 Step 1
            strMessageType = myMsgCollection.Items(intNdx).strMsgType
            strDBIndicator = myMsgCollection.Items(intNdx).strDatabaseIndicator
            fName = UCase(System.IO.Path.GetFileName(Trim(myMsgCollection.Items(intNdx).strFileNameSource)))

            strEntry = New StringBuilder

            boolIsParsed = myMsgCollection.Items(intNdx).blnIsParsed

            If boolIsParsed = True Then
                Select Case strMessageType.ToUpper
                    Case "PELL"
                        Session("MsgType") = "PELL"
                        ShowPATS()
                        Select Case strDBIndicator.ToUpper
                            Case "T"
                                'Process Data For Pell ACG, SMART and TEACH Main
                                strOrigSSN = myMsgCollection.Items(intNdx).strOriginalSSN
                                strAwardId = myMsgCollection.Items(intNdx).strAwardId
                                myMsgCollection.Items(intNdx).strAcademicYearId = ddlAcademicYear.SelectedValue
                                Try
                                    myMsgCollection.Items(intNdx).strAwardYearStartDate = txtAwardYearStartDate.SelectedDate
                                Catch ex As Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    myMsgCollection.Items(intNdx).strAwardYearStartDate = ""
                                End Try

                                Try
                                    myMsgCollection.Items(intNdx).strAwardYearEndDate = txtAwardYearEndDate.SelectedDate
                                Catch ex As Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    myMsgCollection.Items(intNdx).strAwardYearEndDate = ""
                                End Try

                                BuildPATSData(intNdx, myMsgCollection.Items(intNdx).strAwardAmountForEntireYear, myMsgCollection.Items(intNdx).strAwardId, myMsgCollection.Items(intNdx).strGrandType, myMsgCollection.Items(intNdx).strPellAddDate, myMsgCollection.Items(intNdx).strPellAddTime, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strError)

                            Case "H"
                                'Process Data For Pell ACG, SMART and TEACH Main
                                strOrigSSN = myMsgCollection.Items(intNdx).strOriginalSSN
                                strAwardId = myMsgCollection.Items(intNdx).strAwardId
                                myMsgCollection.Items(intNdx).strAcademicYearId = ddlAcademicYear.SelectedValue
                                'myMsgCollection.Items(intNdx).strAwardYearStartDate = txtAwardYearStartDate.SelectedDate
                                'myMsgCollection.Items(intNdx).strAwardYearEndDate = txtAwardYearEndDate.SelectedDate
                                Try
                                    myMsgCollection.Items(intNdx).strAwardYearStartDate = txtAwardYearStartDate.SelectedDate
                                Catch ex As Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    myMsgCollection.Items(intNdx).strAwardYearStartDate = ""
                                End Try

                                Try
                                    myMsgCollection.Items(intNdx).strAwardYearEndDate = txtAwardYearEndDate.SelectedDate
                                Catch ex As Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    myMsgCollection.Items(intNdx).strAwardYearEndDate = ""
                                End Try

                                BuildPATSData(intNdx, myMsgCollection.Items(intNdx).strAwardAmountForEntireSchoolYear, myMsgCollection.Items(intNdx).strAwardId, myMsgCollection.Items(intNdx).strGrandType, myMsgCollection.Items(intNdx).strTeachAddDate, myMsgCollection.Items(intNdx).strTeachAddTime, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strError)

                            Case "S", "C"
                                'Process Data For Pell ACG, SMART and TEACH Detail
                                myMsgCollection.Items(intNdx).strOriginalSSN = strOrigSSN
                                myMsgCollection.Items(intNdx).strAwardId = strAwardId
                                myMsgCollection.Items(intNdx).strAcademicYearId = ddlAcademicYear.SelectedValue
                                'myMsgCollection.Items(intNdx).strAwardYearStartDate = txtAwardYearStartDate.SelectedDate
                                'myMsgCollection.Items(intNdx).strAwardYearEndDate = txtAwardYearEndDate.SelectedDate
                                Try
                                    myMsgCollection.Items(intNdx).strAwardYearStartDate = txtAwardYearStartDate.SelectedDate
                                Catch ex As Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    myMsgCollection.Items(intNdx).strAwardYearStartDate = ""
                                End Try

                                Try
                                    myMsgCollection.Items(intNdx).strAwardYearEndDate = txtAwardYearEndDate.SelectedDate
                                Catch ex As Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    myMsgCollection.Items(intNdx).strAwardYearEndDate = ""
                                End Try

                                BuildPATSDisb(intNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strAwardId, myMsgCollection.Items(intNdx).strDisbursementDate, myMsgCollection.Items(intNdx).strDisbursementRealeaseIndicator, myMsgCollection.Items(intNdx).strActionStatusDisbursement, myMsgCollection.Items(intNdx).strDisbursementNumber, myMsgCollection.Items(intNdx).strSubmittedDisbursementAmount, myMsgCollection.Items(intNdx).strAcceptedDisbursementAmount, myMsgCollection.Items(intNdx).strError)

                        End Select
                    Case "DL"
                        ShowDL()
                        Session("MsgType") = "DL"
                        Select Case strDBIndicator.ToUpper
                            Case "A"
                                ' Do Nothing
                            Case "B"
                                ' Do Nothing
                            Case "D"
                                'Process Data For DL Direct Loan
                                strOrigSSN = myMsgCollection.Items(intNdx).strOriginalSSN
                                strMPNStatus = myMsgCollection.Items(intNdx).strMPNStatus
                                strASD = myMsgCollection.Items(intNdx).strLoanPeriodStartDate
                                strAED = myMsgCollection.Items(intNdx).strLoanPeriodStartDate
                                myMsgCollection.Items(intNdx).strAwardYearStartDate = myMsgCollection.Items(intNdx).strLoanPeriodStartDate
                                myMsgCollection.Items(intNdx).strAwardYearEndDate = myMsgCollection.Items(intNdx).strLoanPeriodEndDate
                                strAwardYear = GetAwardYear(myMsgCollection.Items(intNdx).strLoanId)
                                myMsgCollection.Items(intNdx).strAcademicYearId = strAwardYear
                                ddlAcademicYear.SelectedValue = strAwardYear
                                ''BuildDirectLoan(intNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strLoanType, myMsgCollection.Items(intNdx).strLoanId, myMsgCollection.Items(intNdx).strLoanAddDate, myMsgCollection.Items(intNdx).strLoanAddTime, myMsgCollection.Items(intNdx).strLoanAmountApproved, myMsgCollection.Items(intNdx).strLoanFeePercentage)
                                lblAwardYearHeader.Text = "Direct Loans  Award Year : " + ddlAcademicYear.SelectedItem.Text
                                BuildDirectLoan(intNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strLoanType, myMsgCollection.Items(intNdx).strLoanId, myMsgCollection.Items(intNdx).strLoanAddDate, myMsgCollection.Items(intNdx).strLoanAddTime, myMsgCollection.Items(intNdx).strLoanAmountApproved, myMsgCollection.Items(intNdx).strLoanFeePercentage, myMsgCollection.Items(intNdx).strAnticipatedDisbursementInterestRebateAmount, myMsgCollection.Items(intNdx).strLoanPeriodStartDate, myMsgCollection.Items(intNdx).strLoanPeriodEndDate, myMsgCollection.Items(intNdx).strError)
                                dbRebateAmount = 0.0
                                intDLID = intNdx
                            Case "M"
                                'Process Data For DL Actual Disbusrement
                                myMsgCollection.Items(intNdx).strOriginalSSN = strOrigSSN
                                myMsgCollection.Items(intNdx).strMPNStatus = strMPNStatus
                                myMsgCollection.Items(intNdx).strAwardYearStartDate = strASD
                                myMsgCollection.Items(intNdx).strAwardYearEndDate = strAED
                                strAwardYear = GetAwardYear(myMsgCollection.Items(intNdx).strLoanId)
                                myMsgCollection.Items(intNdx).strAcademicYearId = strAwardYear
                                ddlAcademicYear.SelectedValue = strAwardYear
                                BuildDLActualDisb(intNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strLoanId, myMsgCollection.Items(intNdx).strActualDisbursementDate, myMsgCollection.Items(intNdx).strActualDisbursementStatus, myMsgCollection.Items(intNdx).strActualDisbursementNumber, myMsgCollection.Items(intNdx).strActualDisbursementGrossAmount, myMsgCollection.Items(intNdx).strActualDisbursementLoanFeeAmount, myMsgCollection.Items(intNdx).strActualDisbursementNetAmount, myMsgCollection.Items(intNdx).strError)
                            Case "N"
                                'Process Data For DL Anticipated Disburesment
                                myMsgCollection.Items(intNdx).strOriginalSSN = strOrigSSN
                                myMsgCollection.Items(intNdx).strMPNStatus = strMPNStatus
                                myMsgCollection.Items(intNdx).strAcademicYearId = ddlAcademicYear.SelectedValue
                                myMsgCollection.Items(intNdx).strAwardYearStartDate = strASD
                                myMsgCollection.Items(intNdx).strAwardYearEndDate = strAED
                                BuildDLSchDisb(intNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strLoanId, myMsgCollection.Items(intNdx).strAnticipatedDisbursementDate, myMsgCollection.Items(intNdx).strMPNStatus, myMsgCollection.Items(intNdx).strAnticipatedDisbursementNumber, myMsgCollection.Items(intNdx).strAnticipatedDisbursementGrossAmount, myMsgCollection.Items(intNdx).strAnticipatedDisbursementFeeAmount, myMsgCollection.Items(intNdx).strAnticipatedDisbursementNetAmount, myMsgCollection.Items(intNdx).strError)
                        End Select
                End Select
                'BuildLogEntry(strLogEntry)
            End If
            'lstMessages.Items.Add(strEntry.ToString)
        Next
        ''Response.Write(strM)

        'Session("FileName") = strFileNameOnly
        Session("MessageCollection") = myMsgCollection


        'Bind Log 
        'dgLogMessages.DataSource = Session("dtLogMessage")
        'dgLogMessages.DataBind()
        SortPellGrids()

        SortDLGrids()
    End Sub
    Private Sub BuildDirectLoan(ByVal IndexID As Integer, ByVal OriginalSSN As String, ByVal LoanType As String, ByVal LoanId As String, ByVal AddDate As String, ByVal AddTime As String, ByVal AwardAmount As String, ByVal FeesPer As String, ByVal strRebate As String, ByVal strLoanStartDate As String, ByVal strLoanEndDate As String, ByVal ErrorMsg As String)
        If Not OriginalSSN = "" Then
            Dim drDirectLoan As DataRow
            Try
                Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
                Dim dcLastName As New DataColumn("LastName", GetType(String))
                Dim dcFirstName As New DataColumn("FirstName", GetType(String))
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcCampus As New DataColumn("Campus", GetType(String))
                Dim dcLoanType As New DataColumn("LoanType", GetType(String))
                Dim dcAwardId As New DataColumn("AwardId", GetType(String))
                Dim dcAddDate As New DataColumn("AddDate", GetType(String))
                Dim dcAddTime As New DataColumn("AddTime", GetType(String))
                Dim dcAwardAmount As New DataColumn("AwardAmount", GetType(String))
                Dim dcFees As New DataColumn("Fees", GetType(String))
                Dim dcNetAmount As New DataColumn("NetAmount", GetType(String))
                Dim dcErrorMsg As New DataColumn("ErrorMsg", GetType(String))

                dtDirectLoan.Columns.Add(dcIndexID)
                dtDirectLoan.Columns.Add(dcLastName)
                dtDirectLoan.Columns.Add(dcFirstName)
                dtDirectLoan.Columns.Add(dcSSN)
                dtDirectLoan.Columns.Add(dcCampus)
                dtDirectLoan.Columns.Add(dcLoanType)
                dtDirectLoan.Columns.Add(dcAwardId)
                dtDirectLoan.Columns.Add(dcAddDate)
                dtDirectLoan.Columns.Add(dcAddTime)
                dtDirectLoan.Columns.Add(dcAwardAmount)
                dtDirectLoan.Columns.Add(dcFees)
                dtDirectLoan.Columns.Add(dcNetAmount)
                dtDirectLoan.Columns.Add(dcErrorMsg)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drDirectLoan = dtDirectLoan.NewRow()
            drDirectLoan("IndexID") = IndexID
            GetStudentData(drDirectLoan, OriginalSSN)
            If LoanType.ToUpper = "P" Then
                drDirectLoan("LoanType") = "DL-PLUS"
            ElseIf LoanType.ToUpper = "S" Then
                drDirectLoan("LoanType") = "DL-SUB"
            ElseIf LoanType.ToUpper = "U" Then
                drDirectLoan("LoanType") = "DL-UNSUB"
            Else
                drDirectLoan("LoanType") = ""
            End If
            drDirectLoan("AwardId") = LoanId
            'drDirectLoan("AddDate") = FormatDate(AddDate)
            'drDirectLoan("AddTime") = FormatTime(AddTime)
            drDirectLoan("AddDate") = AddDate
            drDirectLoan("AddTime") = AddTime
            Dim dbFeesPer As Double
            Dim dbRebate As Double
            Dim dbAwardAmount As Double
            Try
                dbFeesPer = Convert.ToDouble(FeesPer)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbFeesPer = 0.0
            End Try
            Try
                dbAwardAmount = Convert.ToDouble(AwardAmount)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbAwardAmount = 0.0
            End Try
            Try
                dbRebate = Convert.ToDouble(strRebate)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbRebate = 0.0
            End Try
            Dim dbFeesAmount As Double = (dbFeesPer * dbAwardAmount) / 100
            drDirectLoan("AwardAmount") = dbAwardAmount.ToString("0.00")
            drDirectLoan("Fees") = Math.Round(dbFeesPer).ToString()
            drDirectLoan("NetAmount") = (dbAwardAmount - Math.Round(dbFeesPer)).ToString("0.00")
            drDirectLoan("ErrorMsg") = ErrorMsg
            dtDirectLoan.Rows.Add(drDirectLoan)
            Session("dtDirectLoan") = dtDirectLoan
        End If
    End Sub
    Private Sub BuildPATSData(ByVal IndexID As Integer, ByVal AwardAmtYr As String, ByVal AwardId As String, ByVal GrantType As String, ByVal AddDate As String, ByVal AddTime As String, ByVal OriginalSSN As String, ByVal ErrorMsg As String)
        If Not OriginalSSN = "" Then
            Dim drPATS As DataRow
            Dim dbDouble As Double
            Try
                Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
                Dim dcLastName As New DataColumn("LastName", GetType(String))
                Dim dcFirstName As New DataColumn("FirstName", GetType(String))
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcCampus As New DataColumn("Campus", GetType(String))
                Dim dcGrantType As New DataColumn("GrantType", GetType(String))
                Dim dcAwardId As New DataColumn("AwardId", GetType(String))
                Dim dcAddDate As New DataColumn("AddDate", GetType(String))
                Dim dcAddTime As New DataColumn("AddTime", GetType(String))
                Dim dcAwardAmount As New DataColumn("AwardAmount", GetType(String))
                Dim dcErrorMsg As New DataColumn("ErrorMsg", GetType(String))

                dtPATSData.Columns.Add(dcIndexID)
                dtPATSData.Columns.Add(dcLastName)
                dtPATSData.Columns.Add(dcFirstName)
                dtPATSData.Columns.Add(dcSSN)
                dtPATSData.Columns.Add(dcCampus)
                dtPATSData.Columns.Add(dcGrantType)
                dtPATSData.Columns.Add(dcAwardId)
                dtPATSData.Columns.Add(dcAddDate)
                dtPATSData.Columns.Add(dcAddTime)
                dtPATSData.Columns.Add(dcAwardAmount)
                dtPATSData.Columns.Add(dcErrorMsg)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drPATS = dtPATSData.NewRow()
            drPATS("IndexID") = IndexID
            GetStudentData(drPATS, OriginalSSN)
            Select Case GrantType
                Case "", "P"
                    drPATS("GrantType") = "Pell"
                Case "A"
                    drPATS("GrantType") = "ACG"
                Case "S", "T"
                    drPATS("GrantType") = "SMART"
                Case "TT"
                    drPATS("GrantType") = "TEACH"
            End Select
            drPATS("AwardId") = AwardId
            'drPATS("AddDate") = FormatDate(AddDate)
            'drPATS("AddTime") = FormatTime(AddTime)
            drPATS("AddDate") = AddDate
            drPATS("AddTime") = AddTime
            Try
                dbDouble = CType(AwardAmtYr, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drPATS("AwardAmount") = dbDouble.ToString("0.00")
            drPATS("ErrorMsg") = ErrorMsg
            dtPATSData.Rows.Add(drPATS)
            Session("dtPATSData") = dtPATSData
        End If
    End Sub
    Private Sub BuildPATSDisb(ByVal IndexID As Integer, ByVal OriginalSSN As String, ByVal AwardId As String, ByVal DisbDate As String, ByVal DisbRelIndi As String, ByVal ActionStatus As String, ByVal DisbNum As String, ByVal SubDisbAmt As String, ByVal AccDisbAmt As String, ByVal ErrorMsg As String)
        If Not OriginalSSN = "" Then
            Dim drPATSDisb As DataRow
            Dim dbDouble As Double
            Try
                Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
                Dim dcLastName As New DataColumn("LastName", GetType(String))
                Dim dcFirstName As New DataColumn("FirstName", GetType(String))
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcCampus As New DataColumn("Campus", GetType(String))
                Dim dcAwardId As New DataColumn("AwardId", GetType(String))
                Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
                Dim dcDisbRelInd As New DataColumn("DisbRelInd", GetType(String))
                Dim dcActionStatus As New DataColumn("ActionStatus", GetType(String))
                Dim dcDisbNum As New DataColumn("DisbNum", GetType(String))
                Dim dcSubDisbAmt As New DataColumn("SubDisbAmt", GetType(String))
                Dim dcAccDisbAmt As New DataColumn("AccDisbAmt", GetType(String))
                Dim dcErrorMsg As New DataColumn("ErrorMsg", GetType(String))

                dtPATSDisb.Columns.Add(dcIndexID)
                dtPATSDisb.Columns.Add(dcLastName)
                dtPATSDisb.Columns.Add(dcFirstName)
                dtPATSDisb.Columns.Add(dcSSN)
                dtPATSDisb.Columns.Add(dcCampus)
                dtPATSDisb.Columns.Add(dcAwardId)
                dtPATSDisb.Columns.Add(dcDisbDate)
                dtPATSDisb.Columns.Add(dcDisbRelInd)
                dtPATSDisb.Columns.Add(dcActionStatus)
                dtPATSDisb.Columns.Add(dcDisbNum)
                dtPATSDisb.Columns.Add(dcSubDisbAmt)
                dtPATSDisb.Columns.Add(dcAccDisbAmt)
                dtPATSDisb.Columns.Add(dcErrorMsg)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drPATSDisb = dtPATSDisb.NewRow()
            drPATSDisb("IndexID") = IndexID
            GetStudentData(drPATSDisb, OriginalSSN)

            drPATSDisb("AwardId") = AwardId
            'drPATSDisb("DisbDate") = FormatDate(DisbDate)
            drPATSDisb("DisbDate") = DisbDate
            drPATSDisb("DisbRelInd") = DisbRelIndi
            drPATSDisb("ActionStatus") = ActionStatus
            drPATSDisb("DisbNum") = DisbNum
            Try
                dbDouble = CType(SubDisbAmt, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drPATSDisb("SubDisbAmt") = dbDouble.ToString("0.00")
            Try
                dbDouble = CType(AccDisbAmt, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drPATSDisb("AccDisbAmt") = dbDouble.ToString("0.00")
            drPATSDisb("ErrorMsg") = ErrorMsg
            dtPATSDisb.Rows.Add(drPATSDisb)
            Session("dtPATSDisb") = dtPATSDisb
        End If
    End Sub

    Private Sub BuildDLSchDisb(ByVal IndexID As Integer, ByVal OriginalSSN As String, ByVal LoanId As String, ByVal DisbDate As String, ByVal MPNStatus As String, ByVal DisbNum As String, ByVal GrossAmount As String, ByVal Fees As String, ByVal NetAmount As String, ByVal ErrorMsg As String)
        If Not OriginalSSN = "" Then
            Dim drDLSchDisb As DataRow
            Dim dbDouble As Double
            Try
                Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
                Dim dcLastName As New DataColumn("LastName", GetType(String))
                Dim dcFirstName As New DataColumn("FirstName", GetType(String))
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcCampus As New DataColumn("Campus", GetType(String))
                Dim dcLoanId As New DataColumn("LoanId", GetType(String))
                Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
                Dim dcMPNStatus As New DataColumn("MPNStatus", GetType(String))
                Dim dcDisbNum As New DataColumn("DisbNum", GetType(String))
                Dim dcGrossAmount As New DataColumn("GrossAmount", GetType(String))
                Dim dcFees As New DataColumn("Fees", GetType(String))
                Dim dcNetAmount As New DataColumn("NetAmount", GetType(String))
                Dim dcErrorMsg As New DataColumn("ErrorMsg", GetType(String))

                dtDLSchDisb.Columns.Add(dcIndexID)
                dtDLSchDisb.Columns.Add(dcLastName)
                dtDLSchDisb.Columns.Add(dcFirstName)
                dtDLSchDisb.Columns.Add(dcSSN)
                dtDLSchDisb.Columns.Add(dcCampus)
                dtDLSchDisb.Columns.Add(dcLoanId)
                dtDLSchDisb.Columns.Add(dcDisbDate)
                dtDLSchDisb.Columns.Add(dcMPNStatus)
                dtDLSchDisb.Columns.Add(dcDisbNum)
                dtDLSchDisb.Columns.Add(dcGrossAmount)
                dtDLSchDisb.Columns.Add(dcFees)
                dtDLSchDisb.Columns.Add(dcNetAmount)
                dtDLSchDisb.Columns.Add(dcErrorMsg)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drDLSchDisb = dtDLSchDisb.NewRow()
            drDLSchDisb("IndexID") = IndexID
            GetStudentData(drDLSchDisb, OriginalSSN)
            drDLSchDisb("LoanId") = LoanId
            'drDLSchDisb("DisbDate") = FormatDate(DisbDate)
            drDLSchDisb("DisbDate") = DisbDate
            drDLSchDisb("MPNStatus") = MPNStatus
            drDLSchDisb("DisbNum") = DisbNum
            Try
                dbDouble = CType(GrossAmount, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drDLSchDisb("GrossAmount") = dbDouble.ToString("0.00")
            Try
                dbDouble = CType(Fees, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drDLSchDisb("Fees") = dbDouble.ToString("0.00")
            Try
                dbDouble = CType(NetAmount, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drDLSchDisb("NetAmount") = dbDouble.ToString("0.00")
            drDLSchDisb("ErrorMsg") = ErrorMsg
            dtDLSchDisb.Rows.Add(drDLSchDisb)
            Session("dtDLSchDisb") = dtDLSchDisb
        End If
    End Sub
    Private Sub BuildDLActualDisb(ByVal IndexID As Integer, ByVal OriginalSSN As String, ByVal LoanId As String, ByVal DisbDate As String, ByVal DisbStatus As String, ByVal DisbNum As String, ByVal GrossAmount As String, ByVal Fees As String, ByVal NetAmount As String, ByVal ErrorMsg As String)
        If Not OriginalSSN = "" Then
            Dim dbDouble As Double
            Dim drDLActualDisb As DataRow
            Try
                Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
                Dim dcLastName As New DataColumn("LastName", GetType(String))
                Dim dcFirstName As New DataColumn("FirstName", GetType(String))
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcCampus As New DataColumn("Campus", GetType(String))
                Dim dcLoanId As New DataColumn("LoanId", GetType(String))
                Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
                Dim dcDisbStatus As New DataColumn("DisbStatus", GetType(String))
                Dim dcDisbNum As New DataColumn("DisbNum", GetType(String))
                Dim dcGrossAmount As New DataColumn("GrossAmount", GetType(String))
                Dim dcFees As New DataColumn("Fees", GetType(String))
                Dim dcNetAmount As New DataColumn("NetAmount", GetType(String))
                Dim dcErrorMsg As New DataColumn("ErrorMsg", GetType(String))

                dtDLActualDisb.Columns.Add(dcIndexID)
                dtDLActualDisb.Columns.Add(dcLastName)
                dtDLActualDisb.Columns.Add(dcFirstName)
                dtDLActualDisb.Columns.Add(dcSSN)
                dtDLActualDisb.Columns.Add(dcCampus)
                dtDLActualDisb.Columns.Add(dcLoanId)
                dtDLActualDisb.Columns.Add(dcDisbDate)
                dtDLActualDisb.Columns.Add(dcDisbStatus)
                dtDLActualDisb.Columns.Add(dcDisbNum)
                dtDLActualDisb.Columns.Add(dcGrossAmount)
                dtDLActualDisb.Columns.Add(dcFees)
                dtDLActualDisb.Columns.Add(dcNetAmount)
                dtDLActualDisb.Columns.Add(dcErrorMsg)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drDLActualDisb = dtDLActualDisb.NewRow()
            drDLActualDisb("IndexID") = IndexID
            GetStudentData(drDLActualDisb, OriginalSSN)
            drDLActualDisb("LoanId") = LoanId
            'drDLActualDisb("DisbDate") = FormatDate(DisbDate)
            drDLActualDisb("DisbDate") = DisbDate
            drDLActualDisb("DisbStatus") = DisbStatus
            drDLActualDisb("DisbNum") = DisbNum
            Try
                dbDouble = CType(GrossAmount, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drDLActualDisb("GrossAmount") = dbDouble.ToString("0.00")
            Try
                dbDouble = CType(Fees, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drDLActualDisb("Fees") = dbDouble.ToString("0.00")
            Try
                dbDouble = CType(NetAmount, Double)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                dbDouble = 0.0
            End Try
            drDLActualDisb("NetAmount") = dbDouble.ToString("0.00")
            drDLActualDisb("ErrorMsg") = ErrorMsg
            dtDLActualDisb.Rows.Add(drDLActualDisb)
            Session("dtDLActualDisb") = dtDLActualDisb
        End If
    End Sub
    Private Function GetStudentData(ByVal dr As DataRow, ByVal OriginalSSN As String) As Boolean
        Dim db As New DataAccess
        ''Dim strSQL As String = "SELECT S.LastName, S.FirstName, S.SSN FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId Where S.SSN='" + OriginalSSN + "' AND SC.SysStatusId=9 "
        Dim strSQL As String = "SELECT TOP 1 S.LastName, S.FirstName, S.SSN, C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId Where S.SSN='" + OriginalSSN + "'  order by EnrollDate Desc "
        Dim dsTemp As DataSet = db.RunSQLDataSet(strSQL, "StudentInfo")
        If dsTemp.Tables("StudentInfo").Rows.Count > 0 Then
            dr("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName").ToString
            dr("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName").ToString
            If OriginalSSN.Length = 9 Then
                dr("SSN") = FormatSSN(OriginalSSN)
            Else
                dr("SSN") = OriginalSSN
            End If
            dr("Campus") = dsTemp.Tables("StudentInfo").Rows(0)("CampDescrip").ToString
            Return True
        Else
            dr("LastName") = ""
            dr("FirstName") = ""
            If OriginalSSN.Length = 9 Then
                dr("SSN") = FormatSSN(OriginalSSN)
            Else
                dr("SSN") = OriginalSSN
            End If

            Return False
        End If
    End Function
    Public Function FormatDate(ByRef DateToFormat As String) As String
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String
        Try
            strYYYY = DateToFormat.Substring(0, 4)
            strMM = DateToFormat.Substring(4, 2)
            strDD = DateToFormat.Substring(6, 2)
            Return strMM + "/" + strDD + "/" + strYYYY
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return DateToFormat
        End Try

    End Function
    Public Function FormatTime(ByRef TimeToFormat As String) As String
        Dim strHH As String
        Dim strMM As String
        Dim strSS As String
        Try
            strHH = TimeToFormat.Substring(0, 2)
            strMM = TimeToFormat.Substring(2, 2)
            strSS = TimeToFormat.Substring(4, 2)
            Return strHH + ":" + strMM + ":" + strSS
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return TimeToFormat
        End Try
    End Function
    Public Function FormatSSN(ByRef SSNToFormat As String) As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Return facInputMasks.ApplyMask(strSSNMask, "*****" & SSNToFormat.Substring(5))
    End Function
    Private Sub btnTransferDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTransferDB.Click

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ConString = MyAdvAppSettings.AppSettings("ConString")
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, CampusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            BuildFilesList()
            BuildAcademicYear()
            ClearDataGrid()
        End If
    End Sub


    Private Sub BuildLogEntry(ByVal Message As StringBuilder)
        If Not Message.ToString = "" Then
            Dim drMessage As DataRow
            'tbLogMessage.Visible = False
            'dgLogMessages.Visible = False
            Try
                Dim dcMessage As New DataColumn("LogMessage", GetType(String))
                dtLogMessage.Columns.Add(dcMessage)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drMessage = dtLogMessage.NewRow()
            drMessage("LogMessage") = Message.ToString
            dtLogMessage.Rows.Add(drMessage)
            Session("dtLogMessage") = dtLogMessage
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub
    Private Sub ShowPATS()
        tbPellAwards.Visible = True
        dgPellAward.Visible = True
        tbPellDisb.Visible = True
        dgPellDisb.Visible = True
    End Sub

    Private Sub ShowDL()
        tbDL.Visible = True
        dgDL.Visible = True

        tbDLSDisb.Visible = True
        dgDLSDisb.Visible = True

        tbDLADisb.Visible = True
        dgDLADisb.Visible = True
    End Sub

    Private Sub BuildFilesList()
        Dim i As Integer
        Dim filename As String
        Dim uriSource As New Uri(MyAdvAppSettings.AppSettings("ExceptionPathEdExp"))
        Dim strSourcePath As String = uriSource.AbsolutePath
        Dim filenames() As String
        If MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer") = "yes" Then
            filenames = Directory.GetFiles(MyAdvAppSettings.AppSettings("RemoteEDExpressException"))
        Else
            filenames = Directory.GetFiles(strSourcePath)
        End If
        ddlFileNames.Items.Clear()
        With ddlFileNames
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        For i = 0 To filenames.Length - 1
            filename = Path.GetFileName(filenames(i)) 'This is much better than substring
            ddlFileNames.Items.Add(filename)
        Next i
    End Sub
    Private Sub BuildAcademicYear()
        Dim dsAY As DataSet = (New StudentsAccountsFacade).GetAllAcademicYears()
        ddlAcademicYear.DataTextField = "AcademicYearDescrip"
        ddlAcademicYear.DataValueField = "AcademicYearId"
        ddlAcademicYear.DataSource = dsAY
        ddlAcademicYear.DataBind()
        With ddlAcademicYear
            .Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
            .SelectedIndex = 0
        End With
    End Sub

    Public Function GetAcademicYear() As DataSet
        Dim dsAY As DataSet = (New StudentsAccountsFacade).GetAllAcademicYears()
        Dim dr As DataRow = dsAY.Tables(0).NewRow()
        dr("AcademicYearId") = "00000000-0000-0000-0000-000000000000"
        dr("AcademicYearDescrip") = "Select"
        dsAY.Tables(0).Rows.InsertAt(dr, 0)
        Return dsAY
    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub ClearDataGrid()
        BuildAcademicYear()
        txtAwardYearEndDate.SelectedDate = Nothing
        txtAwardYearStartDate.SelectedDate = Nothing
        dgPellAward.Visible = False
        dgPellDisb.Visible = False
        tbPellAwards.Visible = False
        tbPellDisb.Visible = False
        dgDL.Visible = False
        dgDLSDisb.Visible = False
        dgDLADisb.Visible = False
        tbDL.Visible = False
        tbDLSDisb.Visible = False
        tbDLADisb.Visible = False
        'dgrdHEAD.Visible = False
        'tbHEAD.Visible = False
        Session("Pell_Message") = ""
        Session("DL_Message") = ""
        Session("dtLogMessage") = Nothing
        Session("dtPATSData") = Nothing
        Session("dtPATSDisb") = Nothing
        Session("dtDirectLoan") = Nothing
        Session("dtDLSchDisb") = Nothing
        Session("dtDLActualDisb") = Nothing
        Session("MsgType") = Nothing

        'dgExceptionReportParent.Visible = False
        'dgExceptionReportChild.Visible = False
        'tblException.Visible = False

        Session("SortExp") = Nothing
        Session("OldSortOrder") = Nothing
        Session("ExpParent") = Nothing
        Session("ExpChild") = Nothing
    End Sub

    Protected Sub dgPellAward_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPellAward.ItemDataBound
        '   Dim i As Integer
        Dim ddl As DropDownList
        Dim lbSD As Label
        Dim lbED As Label
        Dim lbAY As Label
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                ddl = CType(e.Item.FindControl("ddlAcademicYear"), DropDownList)
                ddl.SelectedValue = ddlAcademicYear.SelectedValue
                lbAY = CType(e.Item.FindControl("lblAcademicYear"), Label)
                lbAY.Text = ddl.SelectedItem.Text
                ddl.Visible = False
                lbSD = CType(e.Item.FindControl("lblAYSD"), Label)
                lbED = CType(e.Item.FindControl("lblAYED"), Label)
                Try
                    lbSD.Text = txtAwardYearStartDate.SelectedDate
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    lbSD.Text = ""
                End Try

                Try
                    lbED.Text = txtAwardYearEndDate.SelectedDate
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    lbED.Text = ""
                End Try

        End Select
    End Sub
    Protected Sub dgDL_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDL.ItemDataBound
        '  Dim i As Integer
        Dim ddl As DropDownList
        Dim lbAY As Label
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                ddl = CType(e.Item.FindControl("ddlAcademicYear"), DropDownList)
                lbAY = CType(e.Item.FindControl("lblAcademicYear"), Label)
                ddl.SelectedValue = ddlAcademicYear.SelectedValue
                lbAY.Text = ddl.SelectedItem.Text
                ddl.Visible = False
        End Select
    End Sub

    Protected Sub dgDL_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDL.SortCommand
        If Not Session("SortExp") Is Nothing Then
            If Session("OldSortOrder") = e.SortExpression Then
                If Session("SortExp").ToString = "ASC" Then
                    Session("SortExp") = "DESC"
                Else
                    Session("SortExp") = "ASC"
                End If
            Else
                Session("SortExp") = "ASC"
                Session("OldSortOrder") = e.SortExpression
            End If

        Else
            Session("OldSortOrder") = e.SortExpression
            Session("SortExp") = "ASC"
        End If
        Dim dvDL As New DataView(Session("dtDirectLoan"))
        Dim dvDLSD As New DataView(Session("dtDLSchDisb"))
        Dim dvDLAD As New DataView(Session("dtDLActualDisb"))
        If Not Session("dtDirectLoan") Is Nothing Then
            dvDL.Sort = e.SortExpression & " " & Session("SortExp").ToString
        End If
        If Not Session("dtDLSchDisb") Is Nothing Then
            dvDLSD.Sort = e.SortExpression & " " & Session("SortExp").ToString
        End If
        If Not Session("dtDLActualDisb") Is Nothing Then
            dvDLAD.Sort = e.SortExpression & " " & Session("SortExp").ToString
        End If

        dgDL.Columns(0).Visible = True
        dgDL.DataSource = dvDL
        dgDL.DataBind()
        dgDL.Columns(0).Visible = False

        dgDLSDisb.Columns(0).Visible = True
        dgDLSDisb.DataSource = dvDLSD
        dgDLSDisb.DataBind()
        dgDLSDisb.Columns(0).Visible = False

        dgDLADisb.Columns(0).Visible = True
        dgDLADisb.DataSource = dvDLAD
        dgDLADisb.DataBind()
        dgDLADisb.Columns(0).Visible = False
    End Sub

    Protected Sub dgPellAward_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgPellAward.SortCommand
        If Not Session("SortExp") Is Nothing Then
            If Session("OldSortOrder") = e.SortExpression Then
                If Session("SortExp").ToString = "ASC" Then
                    Session("SortExp") = "DESC"
                Else
                    Session("SortExp") = "ASC"
                End If
            Else
                Session("OldSortOrder") = e.SortExpression
                Session("SortExp") = "ASC"
            End If

        Else
            Session("OldSortOrder") = e.SortExpression
            Session("SortExp") = "ASC"
        End If

        Dim dvPellAward As New DataView(Session("dtPATSData"))
        Dim dvPellDisb As New DataView(Session("dtPATSDisb"))

        dvPellAward.Sort = e.SortExpression & " " & Session("SortExp").ToString
        dgPellAward.Columns(0).Visible = True
        dgPellAward.DataSource = dvPellAward
        dgPellAward.DataBind()
        dgPellAward.Columns(0).Visible = False

        dvPellDisb.Sort = e.SortExpression & " " & Session("SortExp").ToString
        dgPellDisb.Columns(0).Visible = True
        dgPellDisb.DataSource = dvPellDisb
        dgPellDisb.DataBind()
        dgPellDisb.Columns(0).Visible = False
    End Sub
    Public Function GetAwardYear(ByVal strLoanId As String) As String
        Dim strAYV As String = ""
        Dim strAYNum As String = ""
        Dim strAYLNum As String
        strAYNum = strLoanId.Substring(10, 2)
        For Each itm As ListItem In ddlAcademicYear.Items
            strAYLNum = itm.Text.Substring(itm.Text.Length - 2, 2)
            If strAYNum = strAYLNum Then
                strAYV = itm.Value
                Session("AYV") = itm.Value
                Exit For
            End If
        Next
        If strAYV = "" Then
            strAYNum = strLoanId.Substring(strLoanId.IndexOf("S") + 1, 2)
            For Each itm As ListItem In ddlAcademicYear.Items
                strAYLNum = itm.Text.Substring(itm.Text.Length - 2, 2)
                If strAYNum = strAYLNum Then
                    strAYV = itm.Value
                    Session("AYV") = itm.Value
                    Exit For
                End If
            Next
        End If
        If strAYV = "" Then
            strAYV = "00000000-0000-0000-0000-000000000000"
        End If
        Return strAYV
    End Function
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim dg As New DataGrid
        Dim ds As New DataSet()
        Dim dtPrint As New DataTable
        Dim drPrint As DataRow

        Dim dc1 As New DataColumn("", GetType(String))
        Dim dc2 As New DataColumn("", GetType(String))
        Dim dc3 As New DataColumn("", GetType(String))
        Dim dc4 As New DataColumn("", GetType(String))
        Dim dc5 As New DataColumn("", GetType(String))
        Dim dc6 As New DataColumn("", GetType(String))
        Dim dc7 As New DataColumn("", GetType(String))
        Dim dc8 As New DataColumn("", GetType(String))
        Dim dc9 As New DataColumn("", GetType(String))
        Dim dc10 As New DataColumn("", GetType(String))
        Dim dc11 As New DataColumn("", GetType(String))
        Dim dc12 As New DataColumn("", GetType(String))
        Dim dc13 As New DataColumn("", GetType(String))

        dtPrint.Columns.Add(dc1)
        dtPrint.Columns.Add(dc2)
        dtPrint.Columns.Add(dc3)
        dtPrint.Columns.Add(dc4)
        dtPrint.Columns.Add(dc5)
        dtPrint.Columns.Add(dc6)
        dtPrint.Columns.Add(dc7)
        dtPrint.Columns.Add(dc8)
        dtPrint.Columns.Add(dc9)
        dtPrint.Columns.Add(dc10)
        dtPrint.Columns.Add(dc11)
        dtPrint.Columns.Add(dc12)
        dtPrint.Columns.Add(dc13)

        If Not Session("MsgType") Is Nothing Then
            If Session("MsgType").ToString = "PELL" Then
                drPrint = dtPrint.NewRow()
                drPrint(0) = "Last Name"
                drPrint(1) = "First Name"
                drPrint(2) = "SSN"
                drPrint(3) = "Campus"
                drPrint(4) = "Grant Type"
                drPrint(5) = "Award Id"
                drPrint(6) = "Add Date"
                drPrint(7) = "Add Time"
                drPrint(8) = "Award Amount($)"
                drPrint(9) = "Award Year"
                drPrint(10) = "Award Start Date"
                drPrint(11) = "Award End Date"
                drPrint(12) = "Reason"
                dtPrint.Rows.Add(drPrint)

                If Not Session("dtPATSData") Is Nothing Then
                    For Each dr As DataRow In CType(Session("dtPATSData"), DataTable).Rows
                        drPrint = dtPrint.NewRow()
                        drPrint(0) = dr(1)
                        drPrint(1) = dr(2)
                        drPrint(2) = dr(3)
                        drPrint(3) = dr(4)
                        drPrint(4) = dr(5)
                        drPrint(5) = dr(6)
                        drPrint(6) = dr(7)
                        drPrint(7) = dr(8)
                        drPrint(8) = dr(9)
                        drPrint(9) = ddlAcademicYear.SelectedValue
                        drPrint(10) = txtAwardYearStartDate.SelectedDate
                        drPrint(11) = txtAwardYearEndDate.SelectedDate
                        drPrint(12) = dr(10)
                        dtPrint.Rows.Add(drPrint)
                    Next
                End If
                drPrint = dtPrint.NewRow()
                drPrint(0) = ""
                drPrint(1) = ""
                drPrint(2) = ""
                drPrint(3) = ""
                drPrint(4) = ""
                drPrint(5) = ""
                drPrint(6) = ""
                drPrint(7) = ""
                drPrint(8) = ""
                drPrint(9) = ""
                drPrint(10) = ""
                drPrint(11) = ""
                drPrint(12) = ""
                dtPrint.Rows.Add(drPrint)

                drPrint = dtPrint.NewRow()
                drPrint(0) = "Last Name"
                drPrint(1) = "First Name"
                drPrint(2) = "SSN"
                drPrint(3) = "Campus"
                drPrint(4) = "Award Id"
                drPrint(5) = "Disbursement Date"
                drPrint(6) = "Disbursement Release Indicator"
                drPrint(7) = "Disbursement Number"
                drPrint(8) = "Submitted Disbursement Amount($)"
                drPrint(9) = "Accepted Disbursement Amount($)"
                drPrint(10) = "Reason"
                drPrint(11) = ""
                drPrint(12) = ""
                dtPrint.Rows.Add(drPrint)

                If Not Session("dtPATSDisb") Is Nothing Then
                    For Each dr As DataRow In CType(Session("dtPATSDisb"), DataTable).Rows
                        drPrint = dtPrint.NewRow()
                        drPrint(0) = dr(1)
                        drPrint(1) = dr(2)
                        drPrint(2) = dr(3)
                        drPrint(3) = dr(4)
                        drPrint(4) = dr(5)
                        drPrint(5) = dr(6)
                        drPrint(6) = dr(7)
                        drPrint(7) = dr(9)
                        drPrint(8) = dr(10)
                        drPrint(9) = dr(11)
                        drPrint(10) = dr(12)
                        drPrint(11) = ""
                        drPrint(12) = ""
                        dtPrint.Rows.Add(drPrint)
                    Next
                End If
            Else
                '' For DL, DLSub, DLActual
                drPrint = dtPrint.NewRow()
                drPrint(0) = "Last Name"
                drPrint(1) = "First Name"
                drPrint(2) = "SSN"
                drPrint(3) = "Campus"
                drPrint(4) = "Loan Type"
                drPrint(5) = "Loan Id"
                drPrint(6) = "Add Date"
                drPrint(7) = "Add Time"
                drPrint(8) = "Gross Amount($)"
                drPrint(9) = "Fees"
                drPrint(10) = "Net Amount($)"
                drPrint(11) = "Award Year"
                drPrint(12) = "Reason"
                dtPrint.Rows.Add(drPrint)

                If Not Session("dtDirectLoan") Is Nothing Then
                    For Each dr As DataRow In CType(Session("dtDirectLoan"), DataTable).Rows
                        drPrint = dtPrint.NewRow()
                        drPrint(0) = dr(1)
                        drPrint(1) = dr(2)
                        drPrint(2) = dr(3)
                        drPrint(3) = dr(4)
                        drPrint(4) = dr(5)
                        drPrint(5) = dr(6)
                        drPrint(6) = dr(7)
                        drPrint(7) = dr(8)
                        drPrint(8) = dr(9)
                        drPrint(9) = dr(10)
                        drPrint(10) = dr(11)
                        drPrint(11) = dr(12)
                        drPrint(12) = ""
                        dtPrint.Rows.Add(drPrint)
                    Next
                End If
                drPrint = dtPrint.NewRow()
                drPrint(0) = ""
                drPrint(1) = ""
                drPrint(2) = ""
                drPrint(3) = ""
                drPrint(4) = ""
                drPrint(5) = ""
                drPrint(6) = ""
                drPrint(7) = ""
                drPrint(8) = ""
                drPrint(9) = ""
                drPrint(10) = ""
                drPrint(11) = ""
                drPrint(12) = ""
                dtPrint.Rows.Add(drPrint)

                drPrint = dtPrint.NewRow()
                drPrint(0) = "Last Name"
                drPrint(1) = "First Name"
                drPrint(2) = "SSN"
                drPrint(3) = "Campus"
                drPrint(4) = "Loan Id"
                drPrint(5) = "Disbursement Date"
                drPrint(6) = "Disbursement Number"
                drPrint(7) = "Gross Amount($)"
                drPrint(8) = "Fee($)"
                drPrint(9) = "Net Amount($)"
                drPrint(10) = "Reason"
                drPrint(11) = ""
                drPrint(12) = ""
                dtPrint.Rows.Add(drPrint)

                If Not Session("dtDLSchDisb") Is Nothing Then
                    For Each dr As DataRow In CType(Session("dtDLSchDisb"), DataTable).Rows
                        drPrint = dtPrint.NewRow()
                        drPrint(0) = dr(1)
                        drPrint(1) = dr(2)
                        drPrint(2) = dr(3)
                        drPrint(3) = dr(4)
                        drPrint(4) = dr(5)
                        drPrint(5) = dr(6)
                        drPrint(6) = dr(8)
                        drPrint(7) = dr(9)
                        drPrint(8) = dr(10)
                        drPrint(9) = dr(11)
                        drPrint(10) = dr(12)
                        drPrint(11) = ""
                        drPrint(12) = ""
                        dtPrint.Rows.Add(drPrint)
                    Next
                End If
                drPrint = dtPrint.NewRow()
                drPrint(0) = ""
                drPrint(1) = ""
                drPrint(2) = ""
                drPrint(3) = ""
                drPrint(4) = ""
                drPrint(5) = ""
                drPrint(6) = ""
                drPrint(7) = ""
                drPrint(8) = ""
                drPrint(9) = ""
                drPrint(10) = ""
                drPrint(11) = ""
                drPrint(12) = ""
                dtPrint.Rows.Add(drPrint)

                drPrint = dtPrint.NewRow()
                drPrint(0) = "Last Name"
                drPrint(1) = "First Name"
                drPrint(2) = "SSN"
                drPrint(3) = "Campus"
                drPrint(4) = "Loan Id"
                drPrint(5) = "Disbursement Date"
                drPrint(6) = "Disbursement Status"
                drPrint(7) = "Disbursement Number"
                drPrint(8) = "Gross Amount($)"
                drPrint(9) = "Fess($)"
                drPrint(10) = "Net Amount($)"
                drPrint(11) = "Reason"
                drPrint(12) = ""
                dtPrint.Rows.Add(drPrint)

                If Not Session("dtDLActualDisb") Is Nothing Then
                    For Each dr As DataRow In CType(Session("dtDLActualDisb"), DataTable).Rows
                        drPrint = dtPrint.NewRow()
                        drPrint(0) = dr(1)
                        drPrint(1) = dr(2)
                        drPrint(2) = dr(3)
                        drPrint(3) = dr(4)
                        drPrint(4) = dr(5)
                        drPrint(5) = dr(6)
                        drPrint(6) = dr(7)
                        drPrint(7) = dr(8)
                        drPrint(8) = dr(9)
                        drPrint(9) = dr(10)
                        drPrint(10) = dr(11)
                        drPrint(11) = dr(12)
                        drPrint(12) = ""
                        dtPrint.Rows.Add(drPrint)
                    Next
                End If
            End If
        End If
        ds.Tables.Add(dtPrint)
        Response.Clear()
        Response.Charset = ""
        Response.AddHeader("content-disposition", "attachment;filename=EDExpressReport.xls")
        'Response.AddHeader("content-disposition", "attachment;filename=EDExpressReport.doc")
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"
        'Response.ContentType = "application/vnd.ms-word"
        'create a string writer
        Dim stringWrite As New System.IO.StringWriter
        'create an htmltextwriter which uses the stringwriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'instantiate a datagrid
        'set the datagrid datasource to the dataset passed in
        dg.DataSource = ds.Tables(0)
        dg.ShowHeader = False
        'bind the datagrid
        dg.DataBind()
        'tell the datagrid to render itself to our htmltextwriter
        dg.RenderControl(htmlWrite)
        'all that's left is to output the html
        Response.Write(stringWrite.ToString)
        Response.End()
    End Sub
    Private Sub SortPellGrids()

        If Not Session("dtPATSData") Is Nothing Then
            Dim dvPellAward As New DataView(Session("dtPATSData"))
            Dim dvPellDisb As New DataView(Session("dtPATSDisb"))

            dvPellAward.Sort = "Campus ASC"
            dgPellAward.Columns(0).Visible = True
            dgPellAward.DataSource = dvPellAward
            dgPellAward.DataBind()
            dgPellAward.Columns(0).Visible = False

            dvPellDisb.Sort = "Campus ASC"
            dgPellDisb.Columns(0).Visible = True
            dgPellDisb.DataSource = dvPellDisb
            dgPellDisb.DataBind()
            dgPellDisb.Columns(0).Visible = False
        End If
    End Sub
    Private Sub SortDLGrids()
        If Not Session("dtDirectLoan") Is Nothing Then
            Dim dvDL As New DataView(Session("dtDirectLoan"))
            Dim dvDLSD As New DataView(Session("dtDLSchDisb"))
            Dim dvDLAD As New DataView(Session("dtDLActualDisb"))
            If Not Session("dtDirectLoan") Is Nothing Then
                dvDL.Sort = "Campus ASC"
            End If
            If Not Session("dtDLSchDisb") Is Nothing Then
                dvDLSD.Sort = "Campus ASC"
            End If
            If Not Session("dtDLActualDisb") Is Nothing Then
                dvDLAD.Sort = "Campus ASC"
            End If

            dgDL.Columns(0).Visible = True
            dgDL.DataSource = dvDL
            dgDL.DataBind()
            dgDL.Columns(0).Visible = False

            dgDLSDisb.Columns(0).Visible = True
            dgDLSDisb.DataSource = dvDLSD
            dgDLSDisb.DataBind()
            dgDLSDisb.Columns(0).Visible = False

            dgDLADisb.Columns(0).Visible = True
            dgDLADisb.DataSource = dvDLAD
            dgDLADisb.DataBind()
            dgDLADisb.Columns(0).Visible = False
        End If
    End Sub

    Protected Sub ddlFileNames_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlFileNames.SelectedIndexChanged

    End Sub
End Class