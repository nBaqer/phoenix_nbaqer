﻿Imports System
Imports System.Text
Imports System.IO
Imports FAME.Advantage.Common
Imports Telerik.Web.UI
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports System.Collections
Imports BO = Advantage.Business.Objects


Partial Class FameLink_ManageFAMELinkExceptions
    Inherits BasePage

    Private myFLFacade As New FLFacade
    Private strLogEntry As New System.Text.StringBuilder(10000)
    Private strFileName As String
    Private myMsgCollection As New FLCollectionMessageInfos
    Private ConString As String  '"Provider=SQLOLEDB;Data Source=GOVINDARAJULU\DEVSERVER2005;Initial Catalog=ClockHourDB;User ID=sa;Password=test1"
    Dim arrREFU As New ArrayList
    Dim incrRCVD As Integer = 0
    Dim arrayRCVD() As String
    Dim dt, dtREFU, dtDISB, dtHEAD, dtLogMessage, dtFAID As New DataTable()
    Dim strAppendMessage As New StringBuilder
    Dim intTotalRecordCount As Integer = 0
    Dim strOverride As String = ""
    Dim selectedPrgVerId() As String
    Dim selectedSSN() As String
    Dim selectedFAID() As String
    Dim selectedFund() As String
    Dim selectedGross() As Decimal
    Dim selectedLoanFees() As Decimal
    Dim selectedAwardYear() As String
    Dim selectedLoanBeginDate() As Date
    Dim selectedLoanEndDate() As Date
    Dim selectedToHide() As String
    Dim selectedRecordPosition() As String
    '' Code Added by Kamalesh Ahuja on 27 May 2010 to resolve mantis issue id 18352
    Dim CampusId As String = ""
    Dim strSourceFilePath As String = ""
    Dim strTargetFilePath As String = ""
    Dim strExceptionFilePath As String = ""
    Dim strRemoteUserName As String = ""
    Dim strRemotePassword As String = ""
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Protected MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnSubmit.Attributes.Add("onclick", "if(confirm('Are you sure you want to submit your changes?')){}else{return false}")
        '' Code Added by Kamalesh Ahuja on 27 May 2010 to resolve mantis issue id 18352
        CampusId = HttpContext.Current.Request.Params("cmpid")
        Dim strESPDataFromFameESP As String = ""
        myFLFacade.GetSourceFilePath(CampusId, strSourceFilePath, strTargetFilePath, strExceptionFilePath, strRemoteUserName, strRemotePassword)
        '''''''''''''''''''''''''''''''''''''

        Dim m_Context As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        CampusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, CampusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        ConString = MyAdvAppSettings.AppSettings("ConString")
        If Not Page.IsPostBack Then
            'The user might click on this page in the menu even though the FAMELink folders have not been setup as yet.
            'We want to alert the user right away.



            'Move Files from EspIn to ExceptionFolder
            Dim ds1 As New DataSet
            ds1 = myFLFacade.getAllFilesNamesFromExceptionReport("")
            For Each row As DataRow In ds1.Tables(0).Rows
                Try
                    '' Code changed by Kamalesh Ahuja on 27 May 2010 to resolve mantis issue id 18352
                    ''MoveFileFromInToExceptionFolder(SingletonAppSettings.AppSettings("SourcePath"), SingletonAppSettings.AppSettings("ExceptionPath"), row("FileName"))
                    MoveFileFromInToExceptionFolder(strSourceFilePath, strExceptionFilePath, row("FileName"))
                    ''''''''''''''''''''''''''''''''''''''''''
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            Next
            'BuildFilesList()
            BuildCampusesList()
        End If

    End Sub

    Private Sub MoveFileFromInToExceptionFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String)
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String = uriSource.AbsolutePath + FileName
        Dim strTargetFile As String = uriTarget.AbsolutePath + FileName
        Dim strMessage As String = ""
        '' Code changed by Kamalesh Ahuja on 27 May 2010 to resolve mantis issue id 18352
        ''If SingletonAppSettings.AppSettings("ESPDataFromRemoteComputer") = "yes" Then
        If myFLFacade.IsRemoteServer(CampusId) Then
            '''''''''''''''''''''''''''''''''
            'strSourceFile = SourcePath + "\" + FileName
            'strTargetFile = TargetPath + "\" + FileName
            strSourceFile = SourcePath + FileName
            strTargetFile = TargetPath + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If
        Try
            'modified by balaji on 10/11/2007
            'based on input from patrick - files from Fame ESP will have an extension that is sequencial
            'in other words we won't have a case where there will be two HEAD Files (or any other file types)
            'with same extension, the extension will be .000,.001,.002
            'so when that is the case there is no need to rename the file.
            If File.Exists(strSourceFile) Then
                'Dim fs As FileStream = New FileStream(strSourceFile, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
                'fs.Close()
                'Directory.Move(strSourceFile, strTargetFile)
                RenameExistingFile(strTargetFile)
                File.Move(strSourceFile, strTargetFile)
            End If
        Catch e As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(e)

        End Try
    End Sub

    Private Sub RenameExistingFile(ByVal strTargetPathWithFile As String)
        'Dim strRandomNumber As New Random
        'Dim strTargetPathWithNewFile As String = strTargetPathWithFile + Date.Now.ToString '+ strRandomNumber.Next.ToString
        'If File.Exists(strTargetPathWithFile) Then
        '    'Dim fs As FileStream = New FileStream(strTargetPathWithFile, FileMode.Open)
        '    'fs.Close()
        '    File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
        'End If
        Dim sMessage As String = ""
        Dim strRandomNumber As New Random
        Dim rightNow As DateTime = DateTime.Now
        Dim s As String
        s = rightNow.ToString("MMddyyyy")
        Dim strTargetPathWithNewFile As String = strTargetPathWithFile + s   '+ strRandomNumber.Next.ToString
        If File.Exists(strTargetPathWithFile) Then
            File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
        End If
    End Sub

    Private Sub BuildFilesList(Optional ByVal CampusId As String = "")
        Dim i As Integer
        Dim filename As String
        Dim strSourcePath As String

        myFLFacade.GetSourceFilePath(CampusId, strSourceFilePath, strTargetFilePath, strExceptionFilePath, strRemoteUserName, strRemotePassword)
        '' Code changed by Kamalesh Ahuja on 27 May 2010 to resolve mantis issue id 18352
        ''Dim uriSource As New Uri(SingletonAppSettings.AppSettings("ExceptionPath"))
        Try
            Dim uriSource As New Uri(strExceptionFilePath)
            strSourcePath = uriSource.AbsolutePath
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            If ex.InnerException Is Nothing Then
                If ex.Message.ToLower.Contains("the uri is empty") Then
                    DisplayErrorMessage("Advantage could not get the FAMELink files " & vbCrLf & "because there is no import path specified on the campus page.")
                End If
            ElseIf ex.InnerException.Message.ToString.ToLower.Contains("the uri is empty") Then
                DisplayErrorMessage("Advantage could not get the FAMELink files " & vbCrLf & "because there is no import path specified on the campus page.")
            Else
                DisplayErrorMessage("Advantage encountered a problem trying to get the FAMELink files." & vbCrLf & "Please make sure you have enetered an import path on the campus page." & vbCrLf & "Details for FAME Support: " & ex.Message)
            End If
            Exit Sub
        End Try

        Dim filenames() As String
        '' Code changed by Kamalesh Ahuja on 27 May 2010 to resolve mantis issue id 18352
        'If SingletonAppSettings.AppSettings("ESPDataFromRemoteComputer") = "yes" Then
        '    filenames = Directory.GetFiles(SingletonAppSettings.AppSettings("ExceptionFolderLocation"))
        'Else
        '    filenames = Directory.GetFiles(strSourcePath)
        'End If

        Try
            If myFLFacade.IsRemoteServer(CampusId) Then
                filenames = Directory.GetFiles(strExceptionFilePath)
            Else
                filenames = Directory.GetFiles(strSourcePath)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            If ex.InnerException Is Nothing Then
                DisplayErrorMessage("Advantage encountered a problem trying to get the FAMELink files." & vbCrLf & "Please make sure the path you entered is correct and exists." & vbCrLf & "Details for FAME Support: " & ex.Message)
            Else
                DisplayErrorMessage("Advantage encountered a problem trying to get the FAMELink files." & vbCrLf & "Please make sure the path you entered is correct and exists." & vbCrLf & "Details for FAME Support: " & ex.InnerException.Message.ToString)
            End If
            Exit Sub
        End Try

        ''''''''''''''''''''''''''''''''
        Dim strFileStatus As String = ""
        ddlFileNames.Items.Clear()
        With ddlFileNames
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        For i = 0 To filenames.Length - 1
            If myFLFacade.IsRemoteServer(CampusId) Then
                filename = Path.GetFullPath(filenames(i))
            Else
                filename = Path.GetFileName(filenames(i)) 'This is much better than substring
            End If

            strFileStatus = filename
            'strFileStatus &= (New FLFacade).getFileNameWithStatus(filename)
            'If a campus is passed in we want to return just those files with SSNs that have enrollments
            'for the campus.
            If CampusId = "" Then
                ddlFileNames.Items.Add(strFileStatus)
            Else
                'Check the syFAMEESPException table to make sure this filename has SSNS with enrollments
                'for the passed in campus.
                If myFLFacade.FileHasEnrollmentsForCampus(filename, ddlCampus.SelectedValue) Then
                    ddlFileNames.Items.Add(strFileStatus)
                Else
                    'Check if the file only has students with invalid ssns. For example, a head file import
                    'could have created an exception file with just one record with an invalid SSN. In that
                    'case the file would not belong to any campus since the SSN is invalid. In that case
                    'we want the file to be added to the list.
                    If myFLFacade.FileHasEnrollmentsForNoCampuses(filename) Then
                        ddlFileNames.Items.Add(strFileStatus)
                    End If


                End If

            End If

        Next i

        If ddlFileNames.Items.Count > 1 Then
            ddlFileNames.Enabled = True
        Else
            ddlFileNames.Enabled = False
            DisplayErrorMessage("There are no exception files for the campus you selected.")
        End If

        'Whenever the file list is rebuilt we want to make sure that the exception grid is not visible
        'and its datasource is set to nothing
        If dgExceptionReport.Visible = True Then
            dgExceptionReport.Visible = False
            dgExceptionReport.DataSource = Nothing
            dgExceptionReport.DataBind()
            tblException.Visible = False
        End If

    End Sub

    Private Sub BuildCampusesList()
        Dim fl As New FLFacade


        ddlCampus.Items.Clear()

        ddlCampus.DataSource = fl.GetAllCampuses()
        ddlCampus.DataTextField = "CampDescrip"
        ddlCampus.DataValueField = "CampusId"
        ddlCampus.DataBind()

        With ddlCampus
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    'Protected Sub RadComboBox1_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles RadComboBox1.ItemDataBound
    '    'Use the data item to set the combo box item's Text
    '    e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("FileName").ToString().Trim() 
    'End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        'If RadComboBox1.SelectedValue = "" Then
        '    DisplayErrorMessage("Exception File Name is required")
        '    Exit Sub
        'End If

        bindgrid()
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub bindgrid()
        Dim ds1 As New DataSet
        Dim showRemoved As Integer

        If Not ddlFileNames.SelectedValue = "" Then
            Dim intSelectedFile As Integer = 0
            Dim strSelectedFile As String = ""
            Try
                intSelectedFile = InStr(ddlFileNames.SelectedItem.Text, "(")
                strSelectedFile = Mid(ddlFileNames.SelectedItem.Text, 1, intSelectedFile - 2)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                intSelectedFile = 0
                strSelectedFile = ddlFileNames.SelectedItem.Text
            End Try
            ProcessFile()
            Session("FileName") = strSelectedFile
        End If
        'ds1 = (New FLFacade).getExceptionReportByFileName(Trim(Session("FileName")), sortby)
        If Session("FileName").ToString.ToLower = "select" Then
            Session("FileName") = ""
        End If

        If chkShowRemoved.Checked Then
            showRemoved = 1
        Else
            showRemoved = 0
        End If

        ds1 = (New FLFacade).getExceptionReport("", "", Session("FileName"), "", "", showRemoved, ddlCampus.SelectedValue)
        With dgExceptionReport
            .Visible = True
            .DataSource = ds1
            .DataBind()
        End With
        lblCount.Text = "(Records:" & dgExceptionReport.Items.Count & ")"
        tblException.Visible = True
        If ds1.Tables(0).Rows.Count >= 1 Then
            btnSubmit.Enabled = True
        Else
            btnSubmit.Enabled = False
        End If
        Session("RecordCount") = ds1.Tables(0).Rows.Count
        Session("FileList") = ds1
        BuildProgramVersionDDL()

    End Sub

    Protected Sub dgExceptionReport_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgExceptionReport.NeedDataSource
        If Not Session("FileList") Is Nothing Then
            dgExceptionReport.MasterTableView.DataSource = Session("FileList")
        End If

    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        dgExceptionReportExportToExcel.DataSource = Session("FileList")
        dgExceptionReportExportToExcel.DataBind()
        dgExceptionReportExportToExcel.ExportSettings.FileName = Session("FileName").ToString
        dgExceptionReportExportToExcel.ExportSettings.ExportOnlyData = True
        dgExceptionReportExportToExcel.ExportSettings.IgnorePaging = True
        dgExceptionReportExportToExcel.ExportSettings.OpenInNewWindow = True
        dgExceptionReportExportToExcel.MasterTableView.ExportToExcel()
        dgExceptionReport.Visible = True
        dgExceptionReport.Dispose()
        dgExceptionReportExportToExcel.Dispose()
    End Sub

    Private Sub BuildProgramVersionDDL()
        Dim iitem As GridDataItem
        Dim i As Integer
        Dim dtLoanStartDate As Date
        Dim dtLoanEndDate As Date


        Dim iitems As GridDataItemCollection
        iitems = dgExceptionReport.Items
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            Dim lblSSN As Label = CType(iitem.FindControl("lblSSN"), Label)
            Dim lblMessage As Label = CType(iitem.FindControl("lblMessage"), Label)
            Dim lblFileName As Label = CType(iitem.FindControl("lblFileName"), Label)
            If Not CType(iitem.FindControl("lblLoanBeginDate"), Label).Text = "" Then
                dtLoanStartDate = CDate(CType(iitem.FindControl("lblLoanBeginDate"), Label).Text)
            End If
            If Not CType(iitem.FindControl("lblLoanEndDate"), Label).Text = "" Then
                dtLoanEndDate = CDate(CType(iitem.FindControl("lblLoanEndDate"), Label).Text)
            End If
            Dim strFAID As String = CType(iitem.FindControl("lblFAID"), Label).Text
            If CType(iitem.FindControl("lblLoanBeginDate"), Label).Text = "" Or _
                CType(iitem.FindControl("lblLoanEndDate"), Label).Text = "" Then
                Dim nbrMessages As Integer = myMsgCollection.Count
                Dim intNdx As Integer
                For intNdx = 0 To nbrMessages - 1 Step 1
                    If strFAID.Trim = myMsgCollection.Items(intNdx).strFAID.Trim Then
                        If myMsgCollection.Items(intNdx).strFund = "02" Or myMsgCollection.Items(intNdx).strFund = "03" _
                            Or myMsgCollection.Items(intNdx).strFund = "04" Then
                            dtLoanStartDate = "07/01/" + Mid(myMsgCollection.Items(intNdx).strAwdyr, 1, 4)
                            dtLoanEndDate = "06/30/" + Mid(myMsgCollection.Items(intNdx).strAwdyr, 6, 2)
                        Else
                            dtLoanStartDate = myMsgCollection.Items(intNdx).dateDate1
                            dtLoanEndDate = myMsgCollection.Items(intNdx).dateDate2
                        End If
                    End If
                Next
            End If
            'The Schools will be allowed to select the program version only for multiple enrollments
            Try
                '' New Code Added By Vijay Ramteke On September 03, 2010
                'If InStr(lblMessage.Text, "multiple programs") >= 1 And _
                '   (InStr(lblFileName.Text.ToUpper, "FAID") >= 1 Or InStr(lblFileName.Text.ToUpper, "HEAD") >= 1) Then
                If (InStr(ddlFileNames.SelectedItem.Text.ToUpper, "FAID") >= 1 Or InStr(ddlFileNames.SelectedItem.Text.ToUpper, "HEAD") >= 1) Then
                    CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).Enabled = True
                    CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).DataSource = (New FLFacade).GetEnrollmentsDDLBySSN(lblSSN.Text)
                    CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).DataTextField = "PrgVerDescrip"
                    CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).DataValueField = "StuEnrollId"
                    CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).DataBind()
                    CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).Items.Insert(0, New ListItem("Select", ""))
                    Dim strStuEnrollId As String = (New FLFacade).GetStuEnrollId(lblSSN.Text, dtLoanStartDate, dtLoanEndDate)
                    Try
                        CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).SelectedValue = strStuEnrollId
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).SelectedIndex = 0
                    End Try
                Else
                    CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).Enabled = False
                End If
                '' New Code Added By Vijay Ramteke On September 03, 2010
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        Next
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim cxnString As String
        myMsgCollection = Session("MessageCollection")
        strOverride = ""
        Dim nbrMessages As Integer = myMsgCollection.Count
        Dim intNdx As Integer = 0
        Dim fName As String = ""
        Dim strLogMessage As New StringBuilder
        Dim strMessage As String = ""
        Dim strProcessResult As String = ""
        Dim strExceptionGUID As String = Guid.NewGuid.ToString
        Dim ds As New DataSet
        Dim intFailedRecords As Integer = 0
        Dim intSelectedFile As Integer = 0
        Dim intExceptionRecords As Integer = 0
        Dim strSelectedFile As String = ""
        Dim errStartRecod As Integer = 0
        Dim errTotalRecords As Integer = 0 'Total Records that errored out unexpectedly. This is not the same as processed exceptions.

        Try
            cxnString = ConString

            'Update the IsEligibleToReprocess Field in syFameESPExceptionReport Table
            Dim strMarkMessage As String = MarkRecordsToReprocess()
            If Not strMarkMessage = "" Then
                DisplayErrorMessage(strMarkMessage)
                Exit Sub
            End If

            BuildArrayList()

            Dim strESPDataFromFameESP As String = ""
            '' Code changed by Kamalesh Ahuja on 27 May 2010 to resolve mantis issue id 18352
            ''strESPDataFromFameESP = SingletonAppSettings.AppSettings("ESPDataFromRemoteComputer")
            strESPDataFromFameESP = IIf(myFLFacade.IsRemoteServer(CampusId), "yes", "no")

            If strESPDataFromFameESP = "yes" Then
                strProcessResult = myFLFacade.ProcessFLMsgCollection(cxnString, myMsgCollection, _
                                                                   strSourceFilePath, _
                                                                   strTargetFilePath, _
                                                                   Trim(Session("FileName")), _
                                                                   strExceptionFilePath, _
                                                                   strExceptionGUID, "exceptiontotarget", _
                                                                   strOverride, , "yes", selectedPrgVerId, _
                                                                   selectedSSN, selectedFAID, selectedFund, _
                                                                   selectedGross, selectedLoanFees, selectedAwardYear, _
                                                                   selectedLoanBeginDate, selectedLoanEndDate, _
                                                                   selectedToHide, chkShowRemoved.Checked, selectedRecordPosition)

            Else

                strProcessResult = myFLFacade.ProcessFLMsgCollection(cxnString, myMsgCollection, _
                                                                    strSourceFilePath, _
                                                                    strTargetFilePath, _
                                                                    Trim(Session("FileName")), _
                                                                    strExceptionFilePath, _
                                                                    strExceptionGUID, "exceptiontotarget", _
                                                                    strOverride, , "no", selectedPrgVerId, _
                                                                    selectedSSN, selectedFAID, selectedFund, selectedGross, _
                                                                    selectedLoanFees, selectedAwardYear, selectedLoanBeginDate, _
                                                                    selectedLoanEndDate, selectedToHide, chkShowRemoved.Checked, _
                                                                    selectedRecordPosition)

            End If

            If Not strProcessResult = "" Then
                dgExceptionReport.Visible = True

                'ds = myFLFacade.getExceptionReport(strExceptionGUID)
                ds = (New FLFacade).getExceptionReport("", "", Session("FileName"), "", "", chkShowRemoved.Checked, ddlCampus.SelectedValue)
                'Dim ds1 As New DataSet
                Dim intRecordsWithAwardCutoffdate As Integer = 0
                With dgExceptionReport
                    .DataSource = ds
                    .DataBind()
                End With
                Session("FileList") = ds
                intFailedRecords = ds.Tables(0).Rows.Count
                intTotalRecordCount = CType(Session("RecordCount"), Integer)

                If strProcessResult.Contains("ErrorStartingAtRecord") Then
                    errStartRecod = CInt(Mid(strProcessResult, 23))
                    errTotalRecords = intTotalRecordCount - errStartRecod + 1
                End If

                'intTotalRecordCount = intTotalRecordCount - intRecordsWithAwardCutoffdate
                Dim strSuccessMessage As String = "Number of  records posted:" & (intTotalRecordCount - intFailedRecords - errTotalRecords)
                Dim strFailedMessage As String = "Number of records not posted:" & (intFailedRecords + errTotalRecords).ToString
                If errTotalRecords > 0 Then
                    strFailedMessage &= vbCrLf & "Number of records unexpectedly error out:" & errTotalRecords.ToString & vbCrLf & "PLEASE CONTACT FAME SUPPORT ABOUT THE RECORDS THAT UNEXPECTEDLY ERROR OUT."
                End If
                intExceptionRecords = CType(Session("MarkRecords"), Integer)
                Dim intRecNotPosted As Integer = intExceptionRecords - intFailedRecords - errTotalRecords

                myFLFacade.UpdateExceptionCount(Trim(Session("FileName")), intExceptionRecords - errTotalRecords, intFailedRecords + errTotalRecords)
                strMessage &= "Total number of records in exception:" & intTotalRecordCount
                strMessage &= vbCrLf
                strMessage &= "Number of records selected to post:" & intExceptionRecords
                strMessage &= vbCrLf
                strMessage &= "Number of records actually posted:" & intRecNotPosted

                'BuildFilesList()
                'BuildCampusesList()

                'Build exception report
                dgExceptionReport.Visible = True
                tblException.Visible = True
                Dim getFundSourceValues As String = ""
                Dim strFilterFileName As String = ""

                If Trim(Session("FileName")).ToString.ToLower = "select" Or Trim(Session("FileName")).ToString = "" Then
                    strFilterFileName = ""
                Else
                    strFilterFileName = Trim(Session("FileName"))
                End If
                'With dgExceptionReport
                '    .DataSource = myFLFacade.getExceptionReport("", "", strFilterFileName, "", "")
                '    .DataBind()
                'End With
                BuildProgramVersionDDL()
                lblCount.Text = "(Records:" & dgExceptionReport.Items.Count & ")"

            Else
                dgExceptionReport.Visible = False
                tblException.Visible = False
                Dim strMoveFile As String = ""
                Dim strMoveFileMessage As String = ""
                Dim strInsertArchiveFiles As String = ""
                'If we get to this point it means that there are no exceptions for the selected file
                'and the file has been moved to the archive folder. We therefore need to remove the file
                'from the list. Otherwise, if the user clicks the Get Exceptions button again they will get
                'an error message that the file cannot be found at the specified path. (DE
                ddlFileNames.Items.RemoveAt(ddlFileNames.SelectedIndex)
                ddlFileNames.SelectedIndex = 0

            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage("unexpected error occured: " + ex.Message)
        End Try
    End Sub

    Private Function MarkRecordsToReprocess() As String
        Dim iitems As GridDataItemCollection
        Dim iitem As GridDataItem
        Dim i As Integer
        Dim intArrCount, intHideCount As Integer
        Dim z As Integer
        Dim strFAID, strSSN, strFileName, strFund, strMessage As String
        Dim strFLFacade As New FLFacade
        Dim strReturnMessage As String


        iitems = dgExceptionReport.Items

        Session("RecordCount") = iitems.Count
        Try
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkReprocess"), CheckBox).Checked = True) Then
                    intArrCount += 1
                End If
                If (CType(iitem.FindControl("chkRemove"), CheckBox).Checked = True) Then
                    intHideCount += 1
                End If
            Next

            Session("MarkRecords") = intArrCount

            'The user need to select at least one record to reprocess or hide. However, we have to also handle the scenario
            'where there was a record marked as hidden and the user now uncheck it. In that case we would get the validation
            'message but it is not correct because the user did make a change. We therefore also have to check if there are
            'any records marked as hidden for that file. If there are no records it is okay to fire the validation since it
            'means that the user did not make any change.
            If intArrCount < 1 And intHideCount < 1 Then
                If chkShowRemoved.Checked = False Then
                    Return "Please use the check box to select at least one record to post data"
                    Exit Function
                Else
                    If strFLFacade.GetCountOfHiddenForFile(Session("FileName")) = 0 Then
                        Return "Please use the check box to select at least one record to post data"
                        Exit Function
                    End If
                End If
            End If
            Dim selectedFAID() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedSSN() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedFileName() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedFund() As String = Array.CreateInstance(GetType(String), intArrCount)
            Dim selectedMessage() As String = Array.CreateInstance(GetType(String), intArrCount)

            'Loop Through The Collection To Get ClassSection
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                'retrieve clssection id from the datagrid
                strFAID = CType(iitem.FindControl("lblFAID"), Label).Text
                strSSN = CType(iitem.FindControl("lblSSN"), Label).Text
                strFileName = ddlFileNames.SelectedItem.Text
                strFund = CType(iitem.FindControl("lblFund"), Label).Text
                strMessage = CType(iitem.FindControl("lblMessage"), Label).Text
                If (CType(iitem.FindControl("chkReprocess"), CheckBox).Checked = True) Then

                    If InStr(strMessage, "multiple programs") >= 1 And _
                        (InStr(strFileName, "FAID") >= 1 Or InStr(strFileName, "HEAD") >= 1) And _
                        CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).SelectedValue = "" Then
                        Return "Please select the program version for student with SSN:" & strSSN & " and FA Identifier:" & strFAID
                        Exit Function
                    End If
                    selectedFAID.SetValue(strFAID, z)
                    selectedSSN.SetValue(strSSN, z)
                    selectedFileName.SetValue(strFileName, z)
                    selectedFund.SetValue(strFund, z)
                    selectedMessage.SetValue(strMessage, z)
                    z += 1
                End If
            Next

            'resize the array
            If z > 0 Then ReDim Preserve selectedFAID(z - 1)
            If z > 0 Then ReDim Preserve selectedSSN(z - 1)
            If z > 0 Then ReDim Preserve selectedFileName(z - 1)
            If z > 0 Then ReDim Preserve selectedFund(z - 1)
            If z > 0 Then ReDim Preserve selectedMessage(z - 1)

            strReturnMessage = strFLFacade.MarkEligibleRecordsToReprocess(selectedFAID, selectedSSN, selectedFileName, selectedFund, selectedMessage)
            Return strReturnMessage
        Finally
        End Try
    End Function

    Private Sub BuildArrayList()
        Dim y As Integer = 0
        Dim z As Integer = 0
        Dim iitems As GridDataItemCollection = dgExceptionReport.Items
        Dim iitem As GridDataItem
        Dim i As Integer
        Dim strPrgVerId, strSSN, strFAID, strFund, strExceptionReportId, strRecordPosition As String
        Dim strAwardYear As String = String.Empty
        Dim decGross As Decimal
        Dim dtLoanStartDate As Date
        Dim dtLoanEndDate As Date
        Dim decLoanFees As Decimal = 0


        Dim intSelectedRows As Integer = 0
        Dim intSelectedRowsToHide As Integer = 0

        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            If CType(iitem.FindControl("chkReprocess"), CheckBox).Checked = True Then
                intSelectedRows += 1
            End If
            If CType(iitem.FindControl("chkRemove"), CheckBox).Checked = True Then
                intSelectedRowsToHide += 1
            End If
        Next

        If intSelectedRows = 0 And intSelectedRowsToHide = 0 Then
            Exit Sub
        End If

        selectedPrgVerId = Array.CreateInstance(GetType(String), intSelectedRows)
        selectedSSN = Array.CreateInstance(GetType(String), intSelectedRows)
        selectedFAID = Array.CreateInstance(GetType(String), intSelectedRows)
        selectedFund = Array.CreateInstance(GetType(String), intSelectedRows)
        selectedGross = Array.CreateInstance(GetType(Decimal), intSelectedRows)
        selectedLoanFees = Array.CreateInstance(GetType(Decimal), intSelectedRows)
        selectedAwardYear = Array.CreateInstance(GetType(String), intSelectedRows)
        selectedLoanBeginDate = Array.CreateInstance(GetType(Date), intSelectedRows)
        selectedLoanEndDate = Array.CreateInstance(GetType(Date), intSelectedRows)
        selectedToHide = Array.CreateInstance(GetType(String), intSelectedRowsToHide)
        selectedRecordPosition = Array.CreateInstance(GetType(String), intSelectedRows)

        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            strPrgVerId = CType(iitem.FindControl("ddlStuEnrollId"), DropDownList).SelectedValue
            strSSN = CType(iitem.FindControl("lblSSN"), Label).Text
            strFAID = CType(iitem.FindControl("lblFAID"), Label).Text
            strFund = CType(iitem.FindControl("lblFund"), Label).Text
            decGross = CType(iitem.FindControl("lblGrossAmount"), Label).Text
            If Not CType(iitem.FindControl("lblLoanFees"), Label).Text = "" Then
                decLoanFees = CType(iitem.FindControl("lblLoanFees"), Label).Text
            End If
            If Not CType(iitem.FindControl("lblAwardYear"), Label).Text = "" Then
                strAwardYear = CType(iitem.FindControl("lblAwardYear"), Label).Text
            End If
            If Not CType(iitem.FindControl("lblLoanBeginDate"), Label).Text = "" Then
                dtLoanStartDate = CDate(CType(iitem.FindControl("lblLoanBeginDate"), Label).Text)
            End If
            If Not CType(iitem.FindControl("lblLoanEndDate"), Label).Text = "" Then
                dtLoanEndDate = CDate(CType(iitem.FindControl("lblLoanEndDate"), Label).Text)
            End If
            If CType(iitem.FindControl("lblLoanBeginDate"), Label).Text = "" Or _
                CType(iitem.FindControl("lblLoanEndDate"), Label).Text = "" Then
                Dim nbrMessages As Integer = myMsgCollection.Count
                Dim intNdx As Integer
                For intNdx = 0 To nbrMessages - 1 Step 1
                    If strFAID.Trim = myMsgCollection.Items(intNdx).strFAID.Trim Then
                        If myMsgCollection.Items(intNdx).strFund = "02" Or myMsgCollection.Items(intNdx).strFund = "03" _
                            Or myMsgCollection.Items(intNdx).strFund = "04" Then
                            dtLoanStartDate = "07/01/" + Mid(myMsgCollection.Items(intNdx).strAwdyr, 1, 4)
                            dtLoanEndDate = "06/30/" + Mid(myMsgCollection.Items(intNdx).strAwdyr, 6, 2)
                        Else
                            dtLoanStartDate = myMsgCollection.Items(intNdx).dateDate1
                            dtLoanEndDate = myMsgCollection.Items(intNdx).dateDate2
                        End If
                        decLoanFees = myMsgCollection.Items(intNdx).decAmount2
                        strAwardYear = myMsgCollection.Items(intNdx).strAwdyr
                    End If
                Next
            End If

            strExceptionReportId = CType(iitem.FindControl("lblExceptionReportId"), Label).Text
            strRecordPosition = CType(iitem.FindControl("lblRecordPosition"), Label).Text

            If CType(iitem.FindControl("chkReprocess"), CheckBox).Checked = True Then
                selectedPrgVerId.SetValue(strPrgVerId, z)
                selectedSSN.SetValue(strSSN, z)
                selectedFAID.SetValue(strFAID, z)
                selectedFund.SetValue(strFund, z)
                selectedGross.SetValue(decGross, z)
                selectedLoanFees.SetValue(decLoanFees, z)
                selectedAwardYear.SetValue(strAwardYear, z)
                selectedLoanBeginDate.SetValue(dtLoanStartDate, z)
                selectedLoanEndDate.SetValue(dtLoanEndDate, z)
                selectedRecordPosition.SetValue(strRecordPosition, z)
                z += 1
            End If

            If CType(iitem.FindControl("chkRemove"), CheckBox).Checked = True Then
                selectedToHide.SetValue(strExceptionReportId, y)
                y += 1
            End If

        Next
    End Sub

    Private Sub ProcessFile()
        Dim blnResult As Boolean = False
        Dim clsflFile As New FLFileInfo
        Dim strLogMessage As New StringBuilder
        Dim ds1 As New DataSet

        strLogEntry = New StringBuilder

        dgExceptionReport.Visible = False
        tblException.Visible = False
        If ddlFileNames.SelectedValue = "" Then
            DisplayErrorMessage("Please select FAME ESP File to ReProcess")
            Exit Sub
        End If

        If ddlFileNames.SelectedValue = "" Then
            Session("FileName") = ""
        Else
            Session("FileName") = ddlFileNames.SelectedItem.Text
        End If

        Dim str1 As String
        If myFLFacade.IsRemoteServer(ddlCampus.SelectedValue) Then
            clsflFile.datafromremotecomputer = True
            clsflFile.networkuser = strRemoteUserName
            clsflFile.networkPassword = strRemotePassword
            str1 = Trim(ddlFileNames.SelectedItem.Text)
        Else
            str1 = String.Concat(strExceptionFilePath, Trim(ddlFileNames.SelectedItem.Text))
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''
        'Dim intCheckIfFileRenamed As Integer = 0
        'intCheckIfFileRenamed = InStrRev(str1, ".")
        'str1 = Mid(str1, 1, intCheckIfFileRenamed + 3)
        clsflFile.strFileName = str1
        If Len(clsflFile.strError) = 0 Then
            If clsflFile.iRecordCount <> 0 Then
                Session("RecordCount") = clsflFile.iRecordCount

                Dim strSourceFileLocation As String
                If myFLFacade.IsRemoteServer(ddlCampus.SelectedValue) Then
                    'Code commented out by Troy as the "\" is part of the path on the campus page
                    'Dim strSourceFileLocation As String = strExceptionFilePath + "\" + ddlFileNames.SelectedItem.Text
                    strSourceFileLocation = ddlFileNames.SelectedItem.Text
                    blnResult = myFLFacade.ProcessFLFile(clsflFile, myMsgCollection, strSourceFileLocation)
                Else

                    blnResult = myFLFacade.ProcessFLFile(clsflFile, myMsgCollection, "")
                End If
                ''''''''''''''''''''''''''''''''''''''
                updatelistMessages(myMsgCollection)
            Else
            End If
        Else
        End If
        'ds1 = (New FLFacade).getExceptionReportByFileName(Session("FileName"))
        'With dgExceptionReport
        '    .Visible = True
        '    .DataSource = ds1
        '    .DataBind()
        'End With
        'tblException.Visible = True
        'If ds1.Tables(0).Rows.Count >= 1 Then
        '    btnSubmit.Enabled = True
        'Else
        '    btnSubmit.Enabled = False
        'End If
        'Session("RecordCount") = ds1.Tables(0).Rows.Count
        'Session("FileList") = ds1
    End Sub

    Private Sub updatelistMessages(ByVal myMsgCollection As FLCollectionMessageInfos)
        Dim nbrMessages As Integer = myMsgCollection.Count
        Dim intNdx As Integer = 0
        Dim strEntry As New System.Text.StringBuilder(1000)
        Dim fName As String
        Dim strFormat As String
        Dim intOldSSN As String = ""
        Dim strMessageType As String = ""
        Dim strFileNameOnly As String = ""
        Dim strM As String = ""
        Dim boolIsParsed As Boolean
        For intNdx = 0 To nbrMessages - 1 Step 1
            strMessageType = myMsgCollection.Items(intNdx).strMsgType
            fName = UCase(System.IO.Path.GetFileName(Trim(myMsgCollection.Items(intNdx).strFileNameSource)))
            strEntry = New StringBuilder
            boolIsParsed = myMsgCollection.Items(intNdx).blnIsParsed
            If boolIsParsed = False And strMessageType = "HEAD" Then
                boolIsParsed = True
            End If
            If boolIsParsed = True Then
                With strEntry
                    .Append(Trim(myMsgCollection.Items(intNdx).strMsgType) + ControlChars.Tab)
                    .Append(Trim(myMsgCollection.Items(intNdx).strFAID) + ControlChars.Tab)
                    .Append(Trim(myMsgCollection.Items(intNdx).strSSN) + ControlChars.Tab)
                    If Len(Trim(myMsgCollection.Items(intNdx).strFund)) > 0 Then
                        .Append(Trim(myMsgCollection.Items(intNdx).strFund) + ControlChars.Tab)
                    End If
                    If Len(Trim(myMsgCollection.Items(intNdx).strAwdyr)) > 0 Then
                        .Append(Trim(myMsgCollection.Items(intNdx).strAwdyr) + ControlChars.Tab)
                    End If
                    Select Case myMsgCollection.Items(intNdx).strMsgType
                        Case "FAID"
                            Select Case Trim(myMsgCollection.Items(intNdx).strFund)
                                Case "02", "03", "05"
                                    'do nothing
                                Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate2, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                            End Select
                            strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                            .Append(strFormat + ControlChars.Tab)
                            strFormat = Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00")
                            .Append(strFormat + ControlChars.Tab)
                            If Len(Trim(myMsgCollection.Items(intNdx).strCheckNo)) > 0 Then
                                .Append(Trim(myMsgCollection.Items(intNdx).strCheckNo) + ControlChars.Tab)
                            End If
                            If Len(Trim(myMsgCollection.Items(intNdx).strChgCode)) > 0 Then
                                .Append(Trim(myMsgCollection.Items(intNdx).strChgCode) + ControlChars.Tab)
                            End If

                        Case "HEAD"
                            Select Case Trim(myMsgCollection.Items(intNdx).strFund)
                                Case "02", "03", "05"
                                    'do nothing
                                Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate2, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                            End Select
                            strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                            .Append(strFormat + ControlChars.Tab)
                            strFormat = Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00")
                            .Append(strFormat + ControlChars.Tab)

                        Case "DISB"
                            strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                            .Append(strFormat + ControlChars.Tab)
                            strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                            .Append(strFormat + ControlChars.Tab)

                        Case "RCVD"
                            strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                            .Append(strFormat + ControlChars.Tab)
                            strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                            .Append(strFormat + ControlChars.Tab)
                            If Len(Trim(myMsgCollection.Items(intNdx).strCheckNo)) > 0 Then
                                .Append(Trim(myMsgCollection.Items(intNdx).strCheckNo) + ControlChars.Tab)
                            End If

                        Case "REFU"
                            Select Case Trim(myMsgCollection.Items(intNdx).strFund)
                                Case "02", "03", "05"
                                    Try
                                        strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                        .Append(strFormat + ControlChars.Tab)
                                    Catch ex As System.Exception
                                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                                    	exTracker.TrackExceptionWrapper(ex)

                                        .Append("" + ControlChars.Tab)
                                    End Try
                                Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                                    Try
                                        strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                        .Append(strFormat + ControlChars.Tab)
                                    Catch ex As System.Exception
                                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                                    	exTracker.TrackExceptionWrapper(ex)

                                        .Append("" + ControlChars.Tab)
                                    End Try
                            End Select
                            strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                            .Append(strFormat + ControlChars.Tab)

                        Case "CHNG"
                            Select Case myMsgCollection.Items(intNdx).strChgCode
                                Case "C"
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate2, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                                    strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                                    .Append(strFormat + ControlChars.Tab)
                                    strFormat = Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00")
                                    .Append(strFormat + ControlChars.Tab)


                                Case "N"
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate2, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                                    strFormat = Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00")
                                    .Append(strFormat + ControlChars.Tab)


                                Case "D"
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                                    strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                                    .Append(strFormat + ControlChars.Tab)

                            End Select
                            If Len(Trim(myMsgCollection.Items(intNdx).strChgCode)) > 0 Then
                                .Append(Trim(myMsgCollection.Items(intNdx).strChgCode) + ControlChars.Tab)
                            End If
                    End Select
                    .Append(fName)
                End With
            Else
            End If
        Next
        Session("FileName") = strFileNameOnly
        Session("MessageCollection") = myMsgCollection
    End Sub

    Protected Sub ddlCampus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampus.SelectedIndexChanged
        'Before we even attempt to build the list of files we should make sure that the FAMELink exception and archive folders are configured correctly.


        BuildFilesList(ddlCampus.SelectedValue)
    End Sub

    Protected Sub ddlFileNames_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFileNames.SelectedIndexChanged
        'Whenever a file is selected we want to make sure that the exception grid is not visible
        'and its datasource is set to nothing
        If dgExceptionReport.Visible = True Then
            dgExceptionReport.Visible = False
            dgExceptionReport.DataSource = Nothing
            dgExceptionReport.DataBind()
            tblException.Visible = False
            Session("FileList") = Nothing
        End If
    End Sub

    Protected Sub chkShowRemoved_CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowRemoved.CheckedChanged
        'Whenever there is a change in the state of the Show Removed checkbox we want to make sure that the exception grid is not visible
        'and its datasource is set to nothing
        If dgExceptionReport.Visible = True Then
            dgExceptionReport.Visible = False
            dgExceptionReport.DataSource = Nothing
            dgExceptionReport.DataBind()
            tblException.Visible = False
            Session("FileList") = Nothing
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub




End Class
