﻿<%@ Page Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ProcessFAMELinkFile_NEW.aspx.vb" Inherits="FameLink_ProcessFAMELinkFile_NEW" MaintainScrollPositionOnPostback="true"%>
<%@ MasterType  virtualPath="~/NewSite.master"%> 


<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <title>Import from Fame ESP</title>
    <link rel="stylesheet" type="text/css" href="../css/localhost.css" />
    <%--<script src="common.js" type="text/javascript"></script>--%>
  <%--  <script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript">
        function mngRequestStarted(ajaxManager, eventArgs) {
            if (eventArgs.EventTarget == "btnExportToExcel") {
                eventArgs.EnableAjax = false;
            }
        }

        function msgconfirm() {
            var msg;
            msg = "Are you sure you want to process?"

            var answer = confirm(msg);
            if (answer) {
                return true;
            }
            else {
                return false;
            }
        }
    
    
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="DetailsFrameTop">
    
        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
            <!-- begin top menu (save,new,reset,delete,history)-->
            <tr>
                <td class="MenuFrame" align="right">
                    <asp:button id="btnSave" runat="server" CssClass="save" Text="Save" CausesValidation="false" Enabled="false"></asp:button>
                    <asp:button id="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:button>
                    <asp:button id="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false"></asp:button>
                </td>
            </tr>
        </table>
    
   
        
      
            <telerik:RadSplitter ID="RadSplitter1" runat="server" Height="10px" Width="100%" Orientation="Vertical">
                <telerik:RadPane ID="RadPane1" Runat="server" Width="30px" Scrolling="Both"  >
                     <telerik:RadSlidingZone ID="RadSlidingZone1" runat="server">
                        <telerik:RadSlidingPane ID="RadSlidingPane1" runat="server" Title="Queue">
                            <telerik:RadGrid ID="dgQueue" runat="server" ShowStatusBar="true" AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgQueue_NeedDataSource" ShowFooter="false">
                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                    <MasterTableView AutoGenerateColumns="False">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="strMsgType" HeaderText="Type" SortExpression="strMsgType" UniqueName="strMsgType"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="strFileShortName" HeaderText="File Name" SortExpression="strFileShortName" UniqueName="strFileShortName"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CreationDate" HeaderText="Creation Date" SortExpression="CreationDate" UniqueName="CreationDate"></telerik:GridBoundColumn>                                            
                                        </Columns>
                                    </MasterTableView>
                            </telerik:RadGrid>                
                        
                        
                        </telerik:RadSlidingPane>
                    </telerik:RadSlidingZone>
                </telerik:RadPane>
                <telerik:RadSplitBar ID="RadSplitBar1" runat="server"></telerik:RadSplitBar>
                <telerik:RadPane ID="RadPane2" Runat="server" >
                    <asp:Table runat="server" ID="tbSelectFile" Width="80%">
                        <asp:TableRow >
                            <asp:TableCell width="25%" HorizontalAlign="right" >
                                <asp:Label ID="lblFileName" Text="Next FAME ESP File to Import" runat="Server" CssClass="LabelBold"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell Width="25%" HorizontalAlign="left">
                                <asp:Textbox ID="txtNextFileToProcess" runat="server" CssClass="textboxReadOnly" Visible="true" ReadOnly="true"></asp:Textbox>
                                <asp:DropDownList ID="ddlFileNames" runat="server" Visible="false" CssClass="DropDownLists"></asp:DropDownList>
                            </asp:TableCell>
                            <asp:TableCell Width="10%">
                                <asp:Button ID="btnProcess" runat="server" Text=" Read File " CssClass="ProcessFile" />
                            </asp:TableCell>
                            <asp:TableCell Width="10%">
                                <asp:Button ID="btnSaveData" runat="Server" Text=" Post Data " cssClass="button" Visible="false" />
                            </asp:TableCell>
                            <asp:TableCell Width="10%">
                                <asp:Image ID="imgProcess" runat="Server" ImageUrl="../Images/processfile.gif" visible="false"/>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                    <asp:Table runat="server" ID="tbFAID" CellPadding="0" CellSpacing="0"
                        cssClass="DataGridHeader" Width="100%" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="TableHeader">
                                    <asp:Label ID="lblMessageHead" runat="server" cssClass="LabelBold"><%=Session("FAID_Message") %></asp:Label>
                                </asp:TableHeaderCell>
                                <asp:TableCell HorizontalAlign="Right"> <asp:Button ID="btnExportToExcelFAID" runat="server" Text=" Export To Excel " cssClass="button" Width="125px" />
                                    <asp:Button ID="btnImportFAID" runat="server" Text="Import Records" cssClass="button" Width="125px" /></asp:TableCell>
                            </asp:TableHeaderRow>
                  </asp:Table>
                  <telerik:RadGrid ID="dgFAID" runat="server" ShowStatusBar="true" AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgFAID_NeedDataSource" ShowFooter="false" Visible="false">
                        
                            <MasterTableView AutoGenerateColumns="False">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AwardYear" HeaderText="Award Year" SortExpression="AwardYear" UniqueName="AwardYear"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LoanBeginDate" HeaderText="Loan Begin Date" SortExpression="LoanBeginDate" UniqueName="LoanBeginDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LoanEndDate" HeaderText="Loan End Date" SortExpression="LoanEndDate" UniqueName="LoanEndDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="GrossAmount" HeaderText="Disb Amt1" SortExpression="GrossAmount" UniqueName="GrossAmount"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="dgFAIDExportToExcel" runat="server" ShowStatusBar="true"  AutoGenerateColumns="False" PageSize="25" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  ShowFooter="false" Visible="false">
                        
                            <MasterTableView AutoGenerateColumns="False">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AwardYear" HeaderText="Award Year" SortExpression="AwardYear" UniqueName="AwardYear"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LoanBeginDate" HeaderText="Loan Begin Date" SortExpression="LoanBeginDate" UniqueName="LoanBeginDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LoanEndDate" HeaderText="Loan End Date" SortExpression="LoanEndDate" UniqueName="LoanEndDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="GrossAmount" HeaderText="Disb Amt1" SortExpression="GrossAmount" UniqueName="GrossAmount"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                    </telerik:RadGrid>
                      <asp:Table runat="server" ID="tbchngheader" CellPadding="0" CellSpacing="0"
                        cssClass="DataGridHeader" Width="100%" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="TableHeader">
                                    <asp:Label ID="Label5" runat="server" cssClass="LabelBold"><%=Session("CHNG_Message") %></asp:Label>                                   
                                </asp:TableHeaderCell>
                                <asp:TableCell HorizontalAlign="Right"> <asp:Button ID="btnExportToExcelCHNG" runat="server" Text=" Export To Excel " cssClass="button" Width="125px" />
                                    <asp:Button ID="btnImportCHNG" runat="server" Text="Import Records" cssClass="button" Width="125px" /></asp:TableCell>
                            </asp:TableHeaderRow>
                  </asp:Table>
                    <telerik:RadGrid ID="dgchng" runat="server" ShowStatusBar="true" AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgchng_NeedDataSource" ShowFooter="false" Visible="false">
                            
                            <MasterTableView AutoGenerateColumns="False">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbDate" HeaderText="Disb Date" SortExpression="DisbDate" UniqueName="DisbDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbAmt" HeaderText="Disb Amt" SortExpression="DisbAmt" UniqueName="DisbAmt"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbDate1" HeaderText="Disb Date1" SortExpression="DisbDate1" UniqueName="DisbDate1"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbAmt1" HeaderText="Disb Amt1" SortExpression="DisbAmt1" UniqueName="DisbAmt1" DataFormatString="{0:c}" HtmlEncode="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CheckNo" HeaderText="Check No" SortExpression="CheckNo" UniqueName="CheckNo"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="dgchngExportToExcel" runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="25" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  ShowFooter="false" Visible="false">
                            
                            <MasterTableView AutoGenerateColumns="False">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN"  UniqueName="SSN" AllowFiltering="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID"  UniqueName="FAID" AllowFiltering="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type"  UniqueName="Fund" AllowFiltering="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbDate" HeaderText="Disb Date"  UniqueName="DisbDate" AllowFiltering="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbAmt" HeaderText="Disb Amt"  UniqueName="DisbAmt" DataFormatString="{0:c}" AllowFiltering="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbDate1" HeaderText="Disb Date1"  UniqueName="DisbDate1" AllowFiltering="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbAmt1" HeaderText="Disb Amt1"  UniqueName="DisbAmt1" AllowFiltering="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CheckNo" HeaderText="Check No"  UniqueName="CheckNo" AllowFiltering="false"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName" ></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                    </telerik:RadGrid>
                    <asp:Table runat="server" ID="tbRCVDHeader" CellPadding="0" CellSpacing="0" cssClass="DataGridHeader" Width="100%" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="TableHeader">
                                      <asp:Label ID="Label1" runat="server" cssClass="LabelBold"><%=Session("RCVD_Message") %></asp:Label>
                                </asp:TableHeaderCell>
                                <asp:TableCell HorizontalAlign="Right"> <asp:Button ID="btnExportToExcelRCVD" runat="server" Text=" Export To Excel " cssClass="button" Width="125px" />
                                    <asp:Button ID="btnImportRCVD" runat="server" Text="Import Records" cssClass="button" Width="125px" /></asp:TableCell>
                            </asp:TableHeaderRow>
                  </asp:Table>
                  <telerik:RadGrid ID="dgrdRCVD" runat="server" ShowStatusBar="true" AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgrdRCVD_NeedDataSource" ShowFooter="false" Visible="false">
                        
                        <MasterTableView AutoGenerateColumns="False">
                            <Columns>
                                <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbDate" HeaderText="Disb Date" SortExpression="DisbDate" UniqueName="DisbDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbAmt" HeaderText="Disb Amt" SortExpression="DisbAmt" UniqueName="DisbAmt"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CheckNo" HeaderText="Check No" SortExpression="CheckNo" UniqueName="CheckNo"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="dgrdRCVDExportToExcel" runat="server" ShowStatusBar="true" AllowFilteringByColumn="false" AutoGenerateColumns="False" PageSize="25" AllowSorting="false" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  ShowFooter="false" >
                        
                        <MasterTableView AutoGenerateColumns="False" EnableColumnsViewState="false">
                            <Columns>
                               <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID" AllowFiltering="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund" AllowFiltering="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DisbDate" HeaderText="Disb Date" SortExpression="DisbDate" AllowFiltering="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DisbAmt" HeaderText="Disb Amt" SortExpression="DisbAmt" AllowFiltering="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CheckNo" HeaderText="Check No" SortExpression="CheckNo" AllowFiltering="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <asp:Table runat="server" ID="tbREFUHeader" CellPadding="0" CellSpacing="0"
                        cssClass="DataGridHeader" Width="100%" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="TableHeader">
                                      <asp:Label ID="Label2" runat="server" cssClass="LabelBold"><%=Session("REFU_Message") %></asp:Label>
                                </asp:TableHeaderCell>
                                <asp:TableCell HorizontalAlign="Right"> <asp:Button ID="btnExportToExcelREFU" runat="server" Text=" Export To Excel " cssClass="button" Width="125px" />
                                    <asp:Button ID="btnImportREFU" runat="server" Text="Import Records" cssClass="button" Width="125px" /></asp:TableCell>
                            </asp:TableHeaderRow>
                    </asp:Table> 
                    <telerik:RadGrid ID="dgrdREFU" runat="server" ShowStatusBar="true" AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgrdREFU_NeedDataSource" ShowFooter="false" Visible="false">
                       
                         <MasterTableView AutoGenerateColumns="False">
                              <Columns>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbDate" HeaderText="Disb Date" SortExpression="DisbDate" UniqueName="DisbDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbAmt" HeaderText="Disb Amt" SortExpression="DisbAmt" UniqueName="DisbAmt"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                              </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="dgrdREFUExportToExcel" runat="server" ShowStatusBar="true" AllowFilteringByColumn="False" AutoGenerateColumns="False" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  ShowFooter="false">
                       
                         <MasterTableView AutoGenerateColumns="False">
                              <Columns>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbDate" HeaderText="Disb Date" SortExpression="DisbDate" UniqueName="DisbDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisbAmt" HeaderText="Disb Amt" SortExpression="DisbAmt" UniqueName="DisbAmt"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                              </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <asp:Table runat="server" ID="tbDISBHeader" CellPadding="0" CellSpacing="0"
                        cssClass="DataGridHeader" Width="100%" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="TableHeader">
                                         <asp:Label ID="Label3" runat="server" cssClass="LabelBold"><%=Session("DISB_Message") %></asp:Label>
                                </asp:TableHeaderCell>
                                <asp:TableCell HorizontalAlign="Right"> <asp:Button ID="btnExportToExcelDISB" runat="server" Text=" Export To Excel " cssClass="button" Width="125px" />
                                    <asp:Button ID="btnImportDISB" runat="server" Text="Import Records" cssClass="button" Width="125px" /></asp:TableCell>
                            </asp:TableHeaderRow>
                    </asp:Table> 
                    <telerik:RadGrid ID="dgrdDISB" runat="server" ShowStatusBar="true" AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgrdDISB_NeedDataSource" ShowFooter="false" Visible="false">
                        
                        <MasterTableView AutoGenerateColumns="False">
                            <Columns>
                                <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DisbDate" HeaderText="Disb Date" SortExpression="DisbDate" UniqueName="DisbDate"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DisbAmt" HeaderText="Disb Amt" SortExpression="DisbAmt" UniqueName="DisbAmt"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="dgrdDISBExportToExcel" runat="server" ShowStatusBar="true" AllowFilteringByColumn="False" AutoGenerateColumns="False" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  ShowFooter="false">
                        
                        <MasterTableView AutoGenerateColumns="False">
                            <Columns>
                                <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DisbDate" HeaderText="Disb Date" SortExpression="DisbDate" UniqueName="DisbDate"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DisbAmt" HeaderText="Disb Amt" SortExpression="DisbAmt" UniqueName="DisbAmt"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <asp:Table runat="server" ID="tbHEAD" CellPadding="0" CellSpacing="0"
                        cssClass="DataGridHeader" Width="99%" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="TableHeader">
                                         <asp:Label ID="Label4" runat="server" cssClass="LabelBold"><%=Session("HEAD_Message") %></asp:Label>
                                </asp:TableHeaderCell>
                                <asp:TableCell HorizontalAlign="Right"> <asp:Button ID="btnExportToExcelHEAD" runat="server" Text=" Export To Excel " cssClass="button" Width="115px" />
                                    <asp:Button ID="btnImportHEAD" runat="server" Text="Import Records" cssClass="button" Width="115px" /></asp:TableCell>
                            </asp:TableHeaderRow>
                    </asp:Table>
                    <telerik:RadGrid ID="dgrdHEAD" runat="server" ShowStatusBar="true" AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  OnNeedDataSource="dgrdHEAD_NeedDataSource" ShowFooter="false" Visible="false">
                        
                        <MasterTableView AutoGenerateColumns="False">
                            <Columns>
                                 <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="GrossAmount" HeaderText="Gross Amount" SortExpression="GrossAmount" UniqueName="GrossAmount"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NetAmount" HeaderText="Net Amount" SortExpression="NetAmount" UniqueName="NetAmount"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="dgrdHEADExportToExcel" runat="server" ShowStatusBar="true" AllowFilteringByColumn="False" AutoGenerateColumns="False" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  ShowFooter="false">
                       
                        <MasterTableView AutoGenerateColumns="False">
                            <Columns>
                                 <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund Type" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="GrossAmount" HeaderText="Gross Amount" SortExpression="GrossAmount" UniqueName="GrossAmount"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NetAmount" HeaderText="Net Amount" SortExpression="NetAmount" UniqueName="NetAmount"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" AllowFiltering="false" UniqueName="FileName"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <p></p><p></p>
                     <asp:Table runat="server" ID="tblException" CellPadding="0" CellSpacing="0"
                        cssClass="DataGridHeader" Width="100%" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="TableHeader">
                                         <asp:Label ID="Label6" runat="server" cssClass="LabelBold" Text="List of data not posted successfully"></asp:Label>
                                </asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                    </asp:Table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                        <td width="100%" align="right"><asp:Button ID="btnExportToExceldgExceptionReport" runat="server" Text=" Export To Excel " cssClass="button" Width="125px" /></td>                        
                        </tr>
                    </table>
                    <telerik:RadGrid ID="dgExceptionReport" CellPadding="0" BorderWidth="1px" BorderStyle="Solid" OnNeedDataSource="dgExceptionReport_NeedDataSource" AllowFilteringByColumn="true"
                        BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal" Width="100%" runat="server" AllowPaging="True" PageSize="25"  ShowFooter="false">
                        <EditItemStyle Wrap="False"></EditItemStyle>
                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                        <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                        <MasterTableView AutoGenerateColumns="False">
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="SSN" SortExpression="SSN" DataField="SSN">
                                <HeaderStyle CssClass="DataGridHeaderStyle" width="10%"></HeaderStyle>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSSN" runat="Server" Text='<%# Container.DataItem("SSN") %>' CssClass="Label"></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="FA Identifier" SortExpression="FAID" DataField="FAID">
                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="20%"></HeaderStyle>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFAID" Text='<%# Container.DataItem("FAID") %>' CssClass="Label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Fund" SortExpression="Fund" DataField="Fund">
                                <HeaderStyle CssClass="DataGridHeaderStyle" width="5%"></HeaderStyle>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFund" Text='<%# Container.DataItem("Fund") %>' CssClass="Label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Gross Amount" SortExpression="GrossAmount" DataField="GrossAmount" >
                                <HeaderStyle CssClass="DataGridHeaderStyle" width="10%"></HeaderStyle>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblGrossAmount" Text='<%# Container.DataItem("GrossAmount") %>' CssClass="Label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="File" AllowFiltering="false">
                                <HeaderStyle CssClass="DataGridHeaderStyle" width="10%"></HeaderStyle>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFileName" Text='<%# Container.DataItem("FileName") %>' CssClass="Label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Reason" SortExpression="Message" DataField="Message">
                                <HeaderStyle CssClass="DataGridHeaderStyle" width="40%"></HeaderStyle>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblMessage" Text='<%# Container.DataItem("Message") %>' CssClass="Label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                           <telerik:GridTemplateColumn HeaderText="Imported Date" AllowFiltering="false">
                                <HeaderStyle CssClass="DataGridHeaderStyle" width="10%"></HeaderStyle>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblImportedDate" Text='<%# DataBinder.Eval(Container.DataItem,"ModDate", "{0:d}") %>' CssClass="Label"
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        </MasterTableView>
                    </Telerik:RadGrid>
                   
                     <telerik:RadGrid ID="dgExceptionReportExportToExcel" runat="server" ShowStatusBar="true" AllowFilteringByColumn="False" AutoGenerateColumns="False" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false"
                                    AllowPaging="True"  ShowFooter="false">
                       
                        <MasterTableView AutoGenerateColumns="False">
                            <Columns>
                                 <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" UniqueName="SSN"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FAID" HeaderText="FAID" SortExpression="FAID" UniqueName="FAID"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Fund" HeaderText="Fund" SortExpression="Fund" UniqueName="Fund"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="GrossAmount" HeaderText="Gross Amount" SortExpression="GrossAmount" UniqueName="GrossAmount"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FileName" HeaderText="File" SortExpression="NetAmount" UniqueName="FileName"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Message" HeaderText="Reason" AllowFiltering="false" UniqueName="Message"></telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="ModDate" HeaderText="Imported Date" AllowFiltering="false" UniqueName="ModDate"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>

                    <asp:Table runat="server" ID="tbLogMessage" CellPadding="0" CellSpacing="0"
                        cssClass="DataGridHeader" Width="100%" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="TableHeader">
                                      <asp:Label ID="lblLog" runat="server" CssClass="LabelBold">File Processing/Data Transfer Status</asp:Label>
                                </asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="TableHeader" HorizontalAlign=right VerticalAlign=top>
                                    <asp:Button ID="btnResetLog" runat="server" Text="Reset Log"  />
                                </asp:TableHeaderCell> 
                            </asp:TableHeaderRow>
                    </asp:Table>
                    
                    <asp:DataGrid ID="dgLogMessages" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal" Width="100%" runat="server" Visible=false>
                        <EditItemStyle Wrap="False"></EditItemStyle>
                        <ItemStyle CssClass="DataGridAlternatingStyle"></ItemStyle>
                        <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSSN" runat="Server" Text='<%# Container.DataItem("LogMessage") %>' CssClass="Label"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </telerik:RadPane>       
            </telerik:RadSplitter>
        
   
   <br />
            <asp:TextBox ID="txtLog" runat="server" Rows="10" Columns="6" Height="67px" Width="539px"
                Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtFileName" runat="Server" Visible="false"></asp:TextBox>
            <asp:ListBox ID="lstMessages" runat="Server" Visible="false"></asp:ListBox>
            <asp:Button ID="btnTransferDB" runat="server" Text="Transfer Data"  Visible="false" />
            <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>

    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
  </telerik:RadPane>
    </telerik:RadSplitter>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>