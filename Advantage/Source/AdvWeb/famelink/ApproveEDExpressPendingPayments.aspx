﻿<%@ Page Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" EnableEventValidation="false" CodeFile="ApproveEDExpressPendingPayments.aspx.vb" Inherits="FameLink_ApproveEDExpressPendingPayments" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <title>Approve ED Express Pending Payments</title>
    <link rel="stylesheet" type="text/css" href="../css/localhost.css" />
    <script src="common.js" type="text/javascript"></script>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script language="javascript" type="text/javascript">
        function checkMutuallyExclusivePay(chkPay, chkRFL, TotAmtPay, TotAmtRem, amt, TotPay, TotRem) {
            var vamt = 0;
            vamt = parseFloat(amt.value);
            var tap = 0;
            tap = parseFloat(TotAmtPay.value);
            var tar = 0;
            tar = parseFloat(TotAmtRem.value);
            var vpay = 0;
            vpay = parseInt(TotPay.value);
            var vrem = 0;
            vrem = parseInt(TotRem.value);
            if (chkPay.checked == true) {
                vpay = vpay + 1;
                tap = tap + vamt;
            }
            else {
                vpay = vpay - 1;
                tap = tap - vamt;
            }
            if (chkRFL.checked == true) {
                chkRFL.checked = false;
                vrem = vrem - 1;
                tar = tar - vamt;
            }

            TotAmtPay.value = tap;
            TotAmtRem.value = tar;
            TotPay.value = vpay;
            TotRem.value = vrem;
        }
        function checkMutuallyExclusiveRemove(chkRFL, chkPay, TotAmtPay, TotAmtRem, amt, TotPay, TotRem) {
            var vamt = 0;
            vamt = parseFloat(amt.value);
            var tap = 0;
            tap = parseFloat(TotAmtPay.value);
            var tar = 0;
            tar = parseFloat(TotAmtRem.value);
            var vpay = 0;
            vpay = parseInt(TotPay.value);
            var vrem = 0;
            vrem = parseInt(TotRem.value);
            if (chkRFL.checked == true) {
                vrem = vrem + 1;
                tar = tar + vamt;
            }
            else {
                vrem = vrem - 1;
                tar = tar - vamt;
            }
            if (chkPay.checked == true) {
                chkPay.checked = false;
                vpay = vpay - 1;
                tap = tap - vamt;
            }
            TotAmtPay.value = tap;
            TotAmtRem.value = tar;
            TotPay.value = vpay;
            TotRem.value = vrem;
        }
        function msgconfirm(tr, trp, trr, ap, ar, hfP, hfR) {
            var vtr = tr.value;
            var vtrp = trp.value;
            var vtrr = trr.value;
            var vap = ap.value;
            var vvar = ar.value;
            var msg;

            if (vtrp > 0 || vtrr > 0) {
                msg = "Are you sure you want to process?\nTotal Records : " + vtr + "\nTotal number records to be Paid : " + vtrp + "\nTotal number of records to be removed : " + vtrr + "\nTotal Amount to be paid : " + formatCurrency(vap) + "\nTotal Amount to be removed : " + formatCurrency(vvar);

                var answer = confirm(msg);
                if (answer) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                msg = "Please select the payment that needs to be posted";
                alert(msg);
                return false;
            }

        }

        
        function CheckAll(rg, ck) {
            var RadGridAward;
            RadGridAward = rg;
            var checkboxes = RadGridAward.getElementsByTagName("input");
            var index;
            for (index = 0; index < checkboxes.length; index++) {
                if (ck.checked == true) {
                    var str = checkboxes[index].name
                    var pos1 = str.indexOf("chkPay")
                    var pos2 = str.indexOf("ColumnCB")
                    if (pos1 >= 0 && pos2 < 0) {
                        checkboxes[index].checked = true;
                    }
                    else {
                        if (pos2 < 0) {
                            checkboxes[index].checked = false;
                        }

                    }
                }
                else {
                    checkboxes[index].checked = false;
                }
            }
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
            num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + '$' + num + '.' + cents);
        }
     </script>
    <style type="text/css">
        h2 { font: bold 11px verdana; color: #000066; background-color: transparent; width: auto; margin: 0; padding: 0; border: 0;text-align: left;}
        .FileInput
        {
	        margin: 4px;
	        vertical-align: middle;
	        font: normal 10px verdana;
	        text-align:left;
	        color: #000066;
	        width: 300px;
        }
        .ProcessFile
        {
	        margin: 4px;
	        vertical-align: middle;
	        font: normal 10px verdana;
	        text-align:center;
	        color: #000066;
        }
    </style>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">

     <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
     <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="DetailsFrameTop">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
    <!-- begin top menu (save,new,reset,delete,history)-->
    <tr>
    <td class="MenuFrame" align="right">
    <asp:button id="btnSave" runat="server" CssClass="save" Text="Save" CausesValidation="false" Enabled="false"></asp:button>
    <asp:button id="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:button>
    <asp:button id="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false"></asp:button>
    </td>
    </tr>
    </table>
    
   
    <asp:HiddenField ID="hfCC" Value="0" runat="server" />
    <telerik:RadAjaxManagerProxy ID="ramPellAward" runat="server" >
    <AjaxSettings>
    <telerik:AjaxSetting AjaxControlID="lstboxCampus">
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="lstboxCampus"  />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="lstboxEnrollmentStatus">
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="lstboxEnrollmentStatus"  />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="lstboxFundSource">
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="lstboxFundSource" />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="lstboxLender">
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="lstboxLender" />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="lstboxLender">
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="lstboxLender"  />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="lstboxLender">
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="lstboxLender" />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="rdoExpectedDate">
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="rdoSpecifyDate" />
            <telerik:AjaxUpdatedControl ControlID="rapPD" />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="rdoSpecifyDate">
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="rdoExpectedDate" />
            <telerik:AjaxUpdatedControl ControlID="rapPD" />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="rgPellAward" >
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="rgPellAward" />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="btnGetPendingPayments" >
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="btnGetPendingPayments" />
            <telerik:AjaxUpdatedControl ControlID="rgPellAward" />
            <telerik:AjaxUpdatedControl ControlID="tblPellHeader" />
            <telerik:AjaxUpdatedControl ControlID="btnExportToExcel" />
            <telerik:AjaxUpdatedControl ControlID="btnProcess" />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="btnExportToExcel" >
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="rgPellAward" />
            <telerik:AjaxUpdatedControl ControlID="tblPellHeader" />
            <telerik:AjaxUpdatedControl ControlID="btnExportToExcel" />
            <telerik:AjaxUpdatedControl ControlID="btnProcess" />
        </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="btnProcess" >
        <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward"  />
            <telerik:AjaxUpdatedControl ControlID="rgPellAward" />
            <telerik:AjaxUpdatedControl ControlID="tblPellHeader" />
            <telerik:AjaxUpdatedControl ControlID="btnExportToExcel" />
            <telerik:AjaxUpdatedControl ControlID="btnProcess"/>
        </UpdatedControls>
    </telerik:AjaxSetting>
    </AjaxSettings>
    
    </telerik:RadAjaxManagerProxy>
   <telerik:RadAjaxLoadingPanel ID="ralpPellAward" Runat="server" 
        >
    </telerik:RadAjaxLoadingPanel>
    <div>
    <asp:Panel ID="pnlMain" runat="server">
    <asp:HiddenField ID="hfTR" runat="server" Value="0" />
    <asp:HiddenField ID="hfTRP" runat="server" Value="0" />
    <asp:HiddenField ID="hfTRR" runat="server" Value="0" />
    <asp:HiddenField ID="hfAP" runat="server" Value="0" />
    <asp:HiddenField ID="hfAR" runat="server" Value="0" />
    <asp:Table runat="server" ID="tbSelectFile" Width="100%">
     <asp:TableRow Visible="false" >
    <asp:TableCell ColumnSpan="5" HorizontalAlign="Center">
    <asp:Label ID="lblDefaultOptions" Text="DEFAULT PAYMENT OPTIONS" runat="Server" CssClass="LabelBold"></asp:Label>
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell HorizontalAlign="Left" Width="25%" VerticalAlign="Top">
    <asp:Label ID="lblPaymentOptions" Text="Payment Options" runat="Server" CssClass="Label"></asp:Label>
    </asp:TableCell>
    <asp:TableCell HorizontalAlign="left" VerticalAlign="Top" Width="50%">
    <asp:CheckBox ID="chkPOSS" Runat="server" Text="Pay out-of-school students" CssClass="Label"></asp:CheckBox><br>
	<asp:Label ID="Label3" Text="Post Date" runat="Server" CssClass="Label"></asp:Label><br />
	<table border="0" runat="server" id="tblPaymentOptions" cellpadding="0" cellspacing="0">
	<tr>
	<td colspan="2"><asp:RadioButton ID="rdoExpectedDate" AutoPostBack="true" runat="server" CssClass="Label" Checked="true" Text="Use Expected Date" GroupName="Date"/></td>
	</tr>
	<tr>
	<td width="100px"><asp:RadioButton ID="rdoSpecifyDate" AutoPostBack="true" runat="server" CssClass="Label" Text="Specify Date" GroupName="Date" Width="100px"/></td>
	<td>
	<asp:Panel ID="rapPD" runat="server" Enabled="false">
	    <asp:textbox id="txtPostDate" CssClass="TextBoxDate" Runat="server" Width="75px"></asp:textbox>&nbsp;<asp:HyperLink ID="hlPostDate" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink>
	</asp:Panel>
	</td>
	</tr>
	<tr>
	<td><asp:Label ID="lblRefRecid" Text="Reference/Recid" runat="Server" CssClass="Label" Width="100px"></asp:Label></td>
	<td><asp:TextBox ID="txtRefRecid" runat="server" CssClass="TextBox" Width="205px"></asp:TextBox></td>
	</tr>
	<tr>
	<td><asp:Label ID="lblPaymentType" Text="Payment Type" runat="Server" CssClass="Label" Width="100px"></asp:Label></td>
	<td><asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="DropDownLists" Width="205px"></asp:DropDownList></td>
	</tr>
	</table>
    </asp:TableCell>
    <asp:TableCell HorizontalAlign="right" Width="25%">&nbsp;
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Width="100%"  >
    <telerik:RadPanelBar  runat="server" ID="rpbAdvanceOption" Width="100%" >
    <Items>
    <telerik:RadPanelItem  Expanded="false" Text="Advanced Options" runat="server">
    <Items>
    <telerik:RadPanelItem  Value="AdvanceOption" runat="server">
    <ItemTemplate>
    <asp:Table runat="server" ID="tbSelectFile" Width="100%" CellPadding="0" CellSpacing="0">
    <asp:TableRow>
    <asp:TableCell width="25%" HorizontalAlign="Left" VerticalAlign="Top" >
    <asp:Label ID="lblPaymentExpected" Text="Payments Expected Between" runat="Server" CssClass="Label"></asp:Label>
    </asp:TableCell>
    <asp:TableCell ColumnSpan="2" Width="75%" HorizontalAlign="Left">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td><asp:textbox id="txtPaymentExpFrom" CssClass="TextBoxDate" Runat="server" Width="75px"></asp:textbox></td>
        <td>&nbsp;</td>
        <td><asp:HyperLink ID="hlPaymentExpFrom" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink></td>
        <td>&nbsp;&nbsp;</td>
        <td><asp:Label ID="lblPEDAnd" Text=" and " runat="Server" CssClass="Label" Width="25px"></asp:Label></td>
        <td>&nbsp;</td>
        <td><asp:textbox id="txtPaymentExpTo" CssClass="TextBoxDate" Runat="server" Width="75px"></asp:textbox></td>
        <td>&nbsp;</td>
        <td><asp:HyperLink ID="hlPaymentExpTo" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink> </td>
        </tr>
        </table>
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell width="25%" HorizontalAlign="Left" VerticalAlign="Top" >
    <asp:Label ID="lblCampus" Text="Campus" runat="Server" CssClass="Label"></asp:Label>
    </asp:TableCell>
    <asp:TableCell Width="50%" HorizontalAlign="Left">
        <asp:ListBox ID="lstboxCampus" runat="server" CssClass="listboxclasssection" AutoPostBack="true" OnSelectedIndexChanged="lstboxCampus_SelectedIndexChanged"></asp:ListBox>
    </asp:TableCell>
    <asp:TableCell Width="25%">&nbsp;
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell width="25%" HorizontalAlign="Left" VerticalAlign="Top" >
    <asp:Label ID="lblEntollmentStatus" Text="Enrollment Status" runat="Server" CssClass="Label"></asp:Label>
    </asp:TableCell>
    <asp:TableCell Width="50%" HorizontalAlign="Left">
        <asp:ListBox ID="lstboxEnrollmentStatus" runat="server" CssClass="listboxclasssection" AutoPostBack="true" OnSelectedIndexChanged="lstboxEnrollmentStatus_SelectedIndexChanged"></asp:ListBox>
    </asp:TableCell>
    <asp:TableCell Width="25%">&nbsp;
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell width="25%" HorizontalAlign="Left" VerticalAlign="Top" >
    <asp:Label ID="lblFundSource" Text="Fund Source" runat="Server" CssClass="Label"></asp:Label>
    </asp:TableCell>
    <asp:TableCell Width="50%" HorizontalAlign="Left">
        <asp:ListBox ID="lstboxFundSource" runat="server" CssClass="listboxclasssection" AutoPostBack="true" OnSelectedIndexChanged="lstboxFundSource_SelectedIndexChanged"></asp:ListBox>
    </asp:TableCell>
    <asp:TableCell Width="25%">&nbsp;
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell width="25%" HorizontalAlign="Left" VerticalAlign="Top" >
    <asp:Label ID="lblLender" Text="Lender" runat="Server" CssClass="Label"></asp:Label>
    </asp:TableCell>
    <asp:TableCell Width="50%" HorizontalAlign="Left">
        <asp:ListBox ID="lstboxLender" runat="server" CssClass="listboxclasssection" AutoPostBack="true" OnSelectedIndexChanged="lstboxLender_SelectedIndexChanged"></asp:ListBox>
    </asp:TableCell>
    <asp:TableCell Width="25%">&nbsp;
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell width="25%" HorizontalAlign="Left" >
    <asp:Label ID="lblFileName" Text="Select EDExpress File to Import" runat="Server" CssClass="Label"></asp:Label>
    </asp:TableCell>
    <asp:TableCell Width="50%" HorizontalAlign="Left">
    <asp:DropDownList ID="ddlFileNames" runat="server" CssClass="DropDownLists"></asp:DropDownList>
    </asp:TableCell>
    <asp:TableCell Width="25%">&nbsp;
    </asp:TableCell>
    </asp:TableRow>
    </asp:Table>
    </ItemTemplate>
    </telerik:RadPanelItem>
    </Items>
    </telerik:RadPanelItem>
    </Items>
    </telerik:RadPanelBar>
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell Width="100%" ColumnSpan="4" HorizontalAlign="Center">
    <asp:Button ID="btnGetPendingPayments" runat="Server" Text="Get Pending Payments" Width="150px" />
    </asp:TableCell>
    </asp:TableRow>
    </asp:Table>
    <br />
    <asp:Table ID="tblPellRecords" runat="server" CssClass="DataGridHeader" Width="100%">
    <asp:TableRow>
    <asp:TableCell HorizontalAlign="Right">
    <asp:Label ID="lblLegend" Text="Records with multiple enrollments are highlighted" runat="Server" CssClass="LabelBold"></asp:Label>
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="trPellHeader" runat="server">
    <asp:TableCell>
    <table border="0" id="tblPellHeader" runat="server" cellpadding="0" cellspacing="0" width="100%">
    <tr>
    <td align="center"><asp:Label ID="lblMessageHead" runat="server" cssClass="LabelBold">Direct Loan / Pell, SMART, ACG and TEACH Pending Disbursement Records</asp:Label></td>
    </tr>
    <tr>
    <td align="right">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="130px"><asp:Button ID="btnExportToExcel" runat="server" Text=" Export To Excel "   /></td>
    <td width="80px"><input  type="submit" runat="server" id="btnProcess" value=" Post Payments "  /></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="trPellAward">
        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
        <telerik:RadGrid ID="rgPellAward" runat="server" Width="100%" ShowStatusBar="true"
            AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False" AllowFilteringByColumn="true" GroupingSettings-CaseSensitive="false"
            AllowPaging="True"  OnNeedDataSource="rgPellAward_NeedDataSource" >
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <ExportSettings IgnorePaging="true" OpenInNewWindow="true">
            </ExportSettings>
            <MasterTableView Width="100%" Name="PellAward" AllowMultiColumnSorting="false">
            <Columns>
                <telerik:GridBoundColumn AllowSorting="false" HeaderText="ASID" HeaderButtonType="TextButton"
                    DataField="AwardScheduleId" Display="false" UniqueName="ASID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowSorting="false" HeaderText="OrigSSN" HeaderButtonType="TextButton"
                    DataField="OriginalSSN" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowSorting="false" HeaderText="SEID" HeaderButtonType="TextButton"
                    DataField="StuEnrollmentId" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="true" SortExpression="LastName" HeaderText="Last Name" HeaderButtonType="TextButton"
                    DataField="LastName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="true" SortExpression="FirstName" HeaderText="First Name" HeaderButtonType="TextButton"
                    DataField="FirstName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="true" SortExpression="SSN" HeaderText="SSN" HeaderButtonType="TextButton"
                    DataField="SSN" >
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="true" SortExpression="CampusName" HeaderText="Campus" HeaderButtonType="TextButton"
                    DataField="CampusName">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn AllowFiltering="false" DataField="StuEnrollmentId" HeaderText="Program (Status)" UniqueName="ProgramStatus">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlStuEnrollment" runat="server" Width="150px" CssClass="DropDownLists">
                        </asp:DropDownList>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="LDA" HeaderButtonType="TextButton"
                    DataField="LDA" DataFormatString="{0:d}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Award Id" HeaderButtonType="TextButton"
                    DataField="AwardId">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Fund Source" HeaderButtonType="TextButton"
                    DataField="GrantType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Exp Disb Date" HeaderButtonType="TextButton"
                    DataField="ExpDisbDate" DataFormatString="{0:d}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Disb Number" HeaderButtonType="TextButton"
                    DataField="DisbNum">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Disb Sequence" HeaderButtonType="TextButton"
                    DataField="DusbSeqNum">
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Submitted<br/>Amount($)" HeaderButtonType="TextButton"
                    DataField="SimittedDisbAmount" DataFormatString="{0:c}">
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Accepted<br/>Amount($)" HeaderButtonType="TextButton"
                    DataField="AccDisbAmount" DataFormatString="{0:c}">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn AllowFiltering="false" DataField="PaymentAmount" HeaderText="Payment<br/>Amount($)" ItemStyle-Width="150px" UniqueName="PaymentAmount">
                    <ItemTemplate>
                        <asp:TextBox ID="txtPaymentAmount" runat="server" CssClass="TextBoxDate" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem,"PaymentAmount","{0:#.00}") %>'></asp:TextBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn AllowFiltering="false" DataField="DisbDate" HeaderText="Transaction Date" UniqueName="DisbDate">
                    <ItemTemplate>
                    <table runat="server" id="tblAYED" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                    <td style="border:0;padding:0;"><asp:TextBox ID="txtDisbDate" runat="server" CssClass="TextBoxDate" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem,"DisbDate","{0:d}") %>'></asp:TextBox></td>
                    <td style="border:0;padding:0;"><asp:HyperLink ID="hlDisbDate" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink></td>
                    </tr>
                    </table>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn AllowFiltering="false">
                <HeaderTemplate>
                    <asp:Label ID="lblHeader" Text="Pay" runat="server"></asp:Label><br />
                    <asp:CheckBox ID="chkPayCheck" runat="server" OnCheckedChanged="chkPayCheck_CheckedChanged" AutoPostBack="true" />
                </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkPay" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Pay") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                 <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Remove from List">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkRemove" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"RemoveFromList") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
            </MasterTableView>
        </telerik:RadGrid>
         <telerik:RadGrid ID="rgExp" runat="server" Width="100%" ShowStatusBar="true" Visible="false"
            AutoGenerateColumns="False" >
            <ExportSettings IgnorePaging="true" OpenInNewWindow="true">
            </ExportSettings>
            <MasterTableView Width="100%" Name="PellAward">
            <Columns>
                <telerik:GridBoundColumn HeaderText="Last Name" HeaderButtonType="TextButton"
                    DataField="LastName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="First Name" HeaderButtonType="TextButton"
                    DataField="FirstName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="SSN" HeaderButtonType="TextButton"
                    DataField="SSN" >
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Campus" HeaderButtonType="TextButton"
                    DataField="CampusName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Program Status" HeaderButtonType="TextButton" UniqueName="ProgramStatus"
                    DataField="ProgramStatus">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="LDA" HeaderButtonType="TextButton"
                    DataField="LDA" DataFormatString="{0:d}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Award Id" HeaderButtonType="TextButton"
                    DataField="AwardId">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Fund Source" HeaderButtonType="TextButton"
                    DataField="GrantType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Exp Disb Date" HeaderButtonType="TextButton"
                    DataField="ExpDisbDate" DataFormatString="{0:d}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Disb Number" HeaderButtonType="TextButton"
                    DataField="DisbNum">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Disb Sequence" HeaderButtonType="TextButton"
                    DataField="DusbSeqNum">
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn HeaderText="Submitted<br/>Amount($)" HeaderButtonType="TextButton"
                    DataField="SimittedDisbAmount" DataFormatString="{0:c}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Accepted<br/>Amount($)" HeaderButtonType="TextButton"
                    DataField="AccDisbAmount" DataFormatString="{0:c}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Payment<br/>Amount($)" HeaderButtonType="TextButton" UniqueName="PaymentAmount"
                    DataField="PaymentAmount" DataFormatString="{0:c}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Transaction Date" HeaderButtonType="TextButton" UniqueName="DisbDate"
                    DataField="DisbDate" DataFormatString="{0:d}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Pay" HeaderButtonType="TextButton"
                    DataField="Pay">
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn HeaderText="Remove from List" HeaderButtonType="TextButton" 
                    DataField="RemoveFromList">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="" UniqueName="BGColor" DataField="BGColor" Visible="false">
                </telerik:GridBoundColumn>
            </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        </asp:TableCell>
        </asp:TableRow>
    </asp:Table> 
    </asp:Panel>  
    </div>
   
    </td>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
                    <!-- end rightcolumn -->
                </tr>
            </table>
    </telerik:RadPane>
    </telerik:RadSplitter> 
   </asp:Content>