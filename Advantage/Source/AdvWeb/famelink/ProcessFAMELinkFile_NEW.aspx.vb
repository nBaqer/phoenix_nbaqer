﻿Imports System.Collections
Imports System
Imports System.Text
Imports System.IO
Imports Telerik.Web.UI
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports BO = Advantage.Business.Objects


Partial Class FameLink_ProcessFAMELinkFile_NEW
    Inherits BasePage

    Private myFLFacade As New FLFacade
    Private strLogEntry As New System.Text.StringBuilder(10000)
    Dim strLogMessage As New StringBuilder
    Private strFileName As String
    Private myMsgCollection As New FLCollectionMessageInfos
    Private ConString As String
    Dim arrREFU As New ArrayList
    Dim incrRCVD As Integer = 0
    Dim arrayRCVD() As String
    Dim dt, dtREFU, dtDISB, dtHEAD, dtLogMessage, dtFAID As New DataTable()
    Dim strAppendMessage As New StringBuilder
    Dim intTotalRecordCount As Integer = 0
    Dim CampusId As String = ""
    Dim strESPDataFromFameESP As String = ""
    Dim strSourceFilePath As String = ""
    Dim strTargetFilePath As String = ""
    Dim strExceptionFilePath As String = ""
    Dim strRemoteUserName As String = ""
    Dim strRemotePassword As String = ""
    Dim userId As String
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        btnImportFAID.Attributes.Add("onclick", "if(confirm('Are you sure you want to import this file?')){}else{return false}")
        btnImportHEAD.Attributes.Add("onclick", "if(confirm('Are you sure you want to import this file?')){}else{return false}")
        btnImportDISB.Attributes.Add("onclick", "if(confirm('Are you sure you want to import this file?')){}else{return false}")
        btnImportCHNG.Attributes.Add("onclick", "if(confirm('Are you sure you want to import this file?')){}else{return false}")
        btnImportRCVD.Attributes.Add("onclick", "if(confirm('Are you sure you want to import this file?')){}else{return false}")
        btnImportREFU.Attributes.Add("onclick", "if(confirm('Are you sure you want to import this file?')){}else{return false}")

        Dim m_Context As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        CampusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, CampusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        ConString = MyAdvAppSettings.AppSettings("ConString")
        '' Code changed by Kamalesh Ahuja on 27 May 2010 to resolve mantis issue id 18352
        CampusId = HttpContext.Current.Request.Params("cmpid")
        myFLFacade.GetSourceFilePath(CampusId, strSourceFilePath, strTargetFilePath, strExceptionFilePath, strRemoteUserName, strRemotePassword)
        ''''''''''''''''
        If Not Page.IsPostBack Then
            'A regualr user should only see the next file available for processing. That is displayed in the txtNextFileToProcess textbox. However,
            'when FAME Support is logged in we want them to see the full list of files in the IN FAMELink folder. Sometimes a file or part of a file
            'is lost during transmission. Once the issue is fixed, FAME Support will need to be able to log in and import that file even though
            'it is not the next file in the queue.
            BuildFilesList()

            If dgExceptionReport.Visible = True Then
                dgExceptionReport.Visible = False
                dgExceptionReport.DataSource = Nothing
                btnExportToExceldgExceptionReport.Visible = False
            End If

        End If



    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim blnResult As Boolean = False
        Dim blnUpdateMessageList As Boolean = False
        Dim clsflFile As New FLFileInfo
        Dim intInitialImportSuccessful As Integer = 0
        Dim strValidMessage As String = ""
        Dim intFAIDExists As Integer = 0
        Dim intHEADExists As Integer = 0
        Dim intDISBExists As Integer = 0

        strLogEntry = New StringBuilder

        dgExceptionReport.Visible = False
        tblException.Visible = False
        btnExportToExceldgExceptionReport.Visible = "false"

        If Session("UserName").ToString.ToLower = "support" Then
            If ddlFileNames.SelectedValue = "" Then
                DisplayErrorMessage("Please select FAME ESP File to Import")
                Exit Sub
            End If
        End If

        ClearDataGrid()

        If Session("UserName").ToString.ToLower = "support" Then
            Session("FileName") = ddlFileNames.SelectedValue
        Else
            Session("FileName") = txtNextFileToProcess.Text
        End If

        Dim strSourceFilePath As String = ""
        Dim strRemoteUserName As String = ""
        Dim strRemotePassword As String = ""
        myFLFacade.GetSourceFilePath(CampusId, strSourceFilePath, "", "", strRemoteUserName, strRemotePassword)

        Dim str1 As String
        If Session("UserName").ToString.ToLower = "support" Then
            str1 = String.Concat(strSourceFilePath, ddlFileNames.SelectedItem.Text)
        Else
            str1 = String.Concat(strSourceFilePath, txtNextFileToProcess.Text)
        End If

        If myFLFacade.IsRemoteServer(CampusId) Then
            clsflFile.datafromremotecomputer = True
            clsflFile.networkuser = strRemoteUserName
            clsflFile.networkPassword = strRemotePassword
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        clsflFile.strFileName = str1

        With strLogEntry
            .Append("Start Processing of importation of FameLink file: " & UCase(clsflFile.strFileName) + ControlChars.CrLf)
        End With
        With strLogMessage
            .Append("Start Importing FameLink file: " & UCase(clsflFile.strFileName) + ControlChars.CrLf)
        End With
        ClearDataGrid()
        If Len(clsflFile.strError) = 0 Then
            If clsflFile.iRecordCount <> 0 Then
                Session("RecordCount") = clsflFile.iRecordCount
                Session("FileObj") = clsflFile

                If myFLFacade.IsRemoteServer(CampusId) Then
                    Dim strSourceFileLocation As String
                    If Session("UserName").ToString.ToLower = "support" Then
                        strSourceFileLocation = strSourceFilePath + "\" + ddlFileNames.SelectedItem.Text
                    Else
                        strSourceFileLocation = strSourceFilePath + "\" + txtNextFileToProcess.Text
                    End If

                    blnResult = myFLFacade.ProcessFLFile(clsflFile, myMsgCollection, strSourceFileLocation)
                Else
                    blnResult = myFLFacade.ProcessFLFile(clsflFile, myMsgCollection, "")
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                blnUpdateMessageList = updatelistMessages(myMsgCollection)

                If blnResult And blnUpdateMessageList Then
                    ShowGrid(clsflFile)
                ElseIf blnResult = False Then
                    SetErrorMessage(clsflFile)
                ElseIf blnUpdateMessageList = False Then
                    DisplayErrorMessage("The file could not be processed as it is not in the correct format")
                End If
            Else
                With strLogEntry
                    .Append("FAILED Processing of FameLink file:" & clsflFile.strFileName + ControlChars.CrLf)
                    .Append("File Size: " + clsflFile.iFileSize.ToString + ControlChars.CrLf)
                    .Append("with " + clsflFile.iRecordCount.ToString + " records of type " + clsflFile.strMsgType + ControlChars.CrLf + "PARSED and waiting to be processed" + ControlChars.CrLf)
                    .Append("Additional Information: " & clsflFile.strError + ControlChars.CrLf)
                End With
                With strLogMessage
                    .Append("Processing of FameLink file:" & clsflFile.strFileName + " FAILED" + ControlChars.CrLf)
                End With
            End If
        Else
            With strLogEntry
                .Append("Processing of FameLink file: " & clsflFile.strFileName + " FAILED" + ControlChars.CrLf)
                .Append("Additional Information: " & clsflFile.strError + ControlChars.CrLf)
            End With
            With strLogMessage
                .Append("Processing of FameLink file:" & clsflFile.strFileName + " FAILED" + ControlChars.CrLf)
            End With
        End If
        Response.Write(clsflFile.strError)
        strLogEntry.Append(vbCrLf)
        strLogEntry.Append("---------------------------------------------------------------------------------------")
        'BuildLogEntry(strLogMessage)
        dgLogMessages.DataSource = Session("dtLogMessage")
        dgLogMessages.DataBind()
        txtLog.Text = txtLog.Text + strLogEntry.ToString
    End Sub

    Protected Sub BuildFilesList(Optional ByVal OverrideDisplayErrorMessage As Boolean = False)
        Dim i As Integer
        Dim filenames() As String
        Dim strSourcePath As String

        Try
            Dim uriSource As New Uri(strSourceFilePath)
            strSourcePath = uriSource.AbsolutePath
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            If ex.InnerException Is Nothing Then
                If ex.Message.ToLower.Contains("the uri is empty") Then
                    DisplayErrorMessage("Advantage could not get the FAMELink files " & vbCrLf & "because there is no import path specified on the campus page.")
                End If
            ElseIf ex.InnerException.Message.ToString.ToLower.Contains("the uri is empty") Then
                DisplayErrorMessage("Advantage could not get the FAMELink files " & vbCrLf & "because there is no import path specified on the campus page.")
            Else
                DisplayErrorMessage("Advantage encountered a problem trying to get the FAMELink files." & vbCrLf & "Please make sure you have enetered an import path on the campus page." & vbCrLf & "Details for FAME Support: " & ex.Message)
            End If
            Exit Sub
        End Try

        Try
            If myFLFacade.IsRemoteServer(CampusId) Then
                filenames = Directory.GetFiles(strSourceFilePath)
            Else
                filenames = Directory.GetFiles(strSourcePath)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            If ex.InnerException Is Nothing Then
                DisplayErrorMessage("Advantage encountered a problem trying to get the FAMELink files." & vbCrLf & "Please make sure the path you entered is correct and exists." & vbCrLf & "Details for FAME Support: " & ex.Message)
            Else
                DisplayErrorMessage("Advantage encountered a problem trying to get the FAMELink files." & vbCrLf & "Please make sure the path you entered is correct and exists." & vbCrLf & "Details for FAME Support: " & ex.InnerException.Message.ToString)
            End If
            Exit Sub
        End Try

        ''''''''''''''''''''''''''''''''''''''

        ddlFileNames.Items.Clear()
        txtNextFileToProcess.Text = ""
        dgQueue.DataSource = Nothing
        dgQueue.DataBind()

        With ddlFileNames
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        If filenames.Length > 0 Then    'This is to make sure that there are files in the directory
            Dim arrFI(100) As FLFileInfo
            Dim fi_comparer As New FAMELinkFileComparer

            For i = 0 To filenames.Length - 1
                'We want to build a collection of FAMELinkInfo objects that we can have sorted by their dates and types.
                Dim fi As New FLFileInfo

                fi.strFileName = Path.GetFullPath(filenames(i))
                arrFI.SetValue(fi, i)

            Next i

            Array.Resize(arrFI, i)
            Array.Sort(arrFI, fi_comparer)

            For i = 0 To arrFI.Length - 1
                ddlFileNames.Items.Add(Path.GetFileName(arrFI(i).strFileName))
            Next i

            dgQueue.DataSource = arrFI
            dgQueue.DataBind()

            Session("dgQueue") = arrFI

            If Session("UserName").ToString.ToLower = "support" Then
                ddlFileNames.Visible = "true"
                txtNextFileToProcess.Visible = "false"
                lblFileName.Text = "Select FAME ESP file to import"
            Else
                'For a regular user we want to put the name of the next available file in txtNextFileToProcess.
                'Note that we are getting it from the ddl that is built but only shown for the FAME support user.
                txtNextFileToProcess.Text = ddlFileNames.Items(1).Text
                'If there are exceptions in the syFAMEESPExceptionReport table we should disable the Read File button
                'and alert the user that they need to take care of the exceptions before they can process any file.
                'DE6215:QA: Import FAME ESP page is not checking for campus criteria when determining if exceptions exist.
                'To fix this issue we will pass in the campus the user is logged in to. If there are any exceptions for any
                'campus that points to the same folder as the logged in campus then we alert the user that they need to take
                'care of the exceptions before they can process any files.
                If myFLFacade.GetCountOfExceptions(CampusId) > 0 Then
                    btnProcess.Enabled = False
                    If OverrideDisplayErrorMessage = False Then
                        DisplayErrorMessage("You will not be able to import any files as there are exceptions")
                    End If

                End If
            End If
        Else    'No files in the directory. Alert the user.
            btnProcess.Enabled = False
            ddlFileNames.Enabled = False
            If OverrideDisplayErrorMessage = False Then
                DisplayErrorMessage("There are no files in the source folder to import.")
            End If

        End If



    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub ClearDataGrid()
        dgFAID.Visible = False
        dgchng.Visible = False
        dgrdRCVD.Visible = False
        dgrdREFU.Visible = False
        dgrdDISB.Visible = False
        dgrdHEAD.Visible = False
        tbFAID.Visible = False
        tbchngheader.Visible = False
        tbRCVDHeader.Visible = False
        tbREFUHeader.Visible = False
        tbDISBHeader.Visible = False
        tbHEAD.Visible = False
        Session("FAID_Message") = ""
        Session("DISB_Message") = ""
        Session("RCVD_Message") = ""
        Session("CHNG_Message") = ""
        Session("REFU_Message") = ""
        Session("HEAD_Message") = ""
    End Sub

    Private Function CheckHierarchy(ByVal MessageType As String) As Integer
        Dim intFAIDExists As Integer = 0
        For Each item As ListItem In ddlFileNames.Items
            If Mid(item.Text, 1, 8) = MessageType Then
                intFAIDExists = 1
                Exit For
            End If
        Next
        Return intFAIDExists
    End Function

    Private Function updatelistMessages(ByVal myMsgCollection As FLCollectionMessageInfos) As Boolean
        Dim nbrMessages As Integer = myMsgCollection.Count
        Dim intNdx As Integer = 0
        Dim strEntry As New System.Text.StringBuilder(1000)
        Dim fName As String
        Dim strFormat As String
        Dim intOldSSN As String = ""
        Dim strMessageType As String = ""
        Dim strFileNameOnly As String = ""
        Dim strM As String = ""
        Dim boolIsParsed As Boolean

        'We want to parse the contents of the file first to make sure that the file is in the correct format DE:DE8386
        For intNdx = 0 To nbrMessages - 1 Step 1
            boolIsParsed = myMsgCollection.Items(intNdx).blnIsParsed
            If boolIsParsed = False And strMessageType = "HEAD" Then
                boolIsParsed = True
            End If

            'Note that even if one record is bad then we should not process the file.
            If boolIsParsed = False Then
                Return False
                Exit Function
            End If
        Next


        'If we get to this point it means that the File is in correct format so we can process it
        For intNdx = 0 To nbrMessages - 1 Step 1
            strMessageType = myMsgCollection.Items(intNdx).strMsgType
            fName = UCase(System.IO.Path.GetFileName(Trim(myMsgCollection.Items(intNdx).strFileNameSource)))
            strEntry = New StringBuilder

            With strEntry
                .Append(Trim(myMsgCollection.Items(intNdx).strMsgType) + ControlChars.Tab)
                .Append(Trim(myMsgCollection.Items(intNdx).strFAID) + ControlChars.Tab)
                .Append(Trim(myMsgCollection.Items(intNdx).strSSN) + ControlChars.Tab)
                If Len(Trim(myMsgCollection.Items(intNdx).strFund)) > 0 Then
                    .Append(Trim(myMsgCollection.Items(intNdx).strFund) + ControlChars.Tab)
                End If
                If Len(Trim(myMsgCollection.Items(intNdx).strAwdyr)) > 0 Then
                    .Append(Trim(myMsgCollection.Items(intNdx).strAwdyr) + ControlChars.Tab)
                End If
                Select Case myMsgCollection.Items(intNdx).strMsgType
                    Case "FAID"
                        Select Case Trim(myMsgCollection.Items(intNdx).strFund)
                            Case "02", "03", "05"
                                'do nothing
                            Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                                strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                .Append(strFormat + ControlChars.Tab)
                                strFormat = Format(myMsgCollection.Items(intNdx).dateDate2, " MMM d, yyyy ")
                                .Append(strFormat + ControlChars.Tab)
                        End Select
                        strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                        .Append(strFormat + ControlChars.Tab)
                        strFormat = Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00")
                        .Append(strFormat + ControlChars.Tab)
                        If Len(Trim(myMsgCollection.Items(intNdx).strCheckNo)) > 0 Then
                            .Append(Trim(myMsgCollection.Items(intNdx).strCheckNo) + ControlChars.Tab)
                        End If
                        If Len(Trim(myMsgCollection.Items(intNdx).strChgCode)) > 0 Then
                            .Append(Trim(myMsgCollection.Items(intNdx).strChgCode) + ControlChars.Tab)
                        End If
                        Dim intStartIndex As Integer = InStrRev(myMsgCollection.Items(intNdx).strFileNameSource, "\")
                        If Session("UserName").ToString.ToLower = "support" Then
                            strFileNameOnly = ddlFileNames.SelectedValue
                        Else
                            strFileNameOnly = txtNextFileToProcess.Text
                        End If

                        BuildFAID(myMsgCollection.Items(intNdx).strSSN, myMsgCollection.Items(intNdx).strFAID, myMsgCollection.Items(intNdx).strFund, myMsgCollection.Items(intNdx).strAwdyr, myMsgCollection.Items(intNdx).dateDate1, myMsgCollection.Items(intNdx).dateDate2, Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00"), Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00"), Trim(myMsgCollection.Items(intNdx).strCheckNo), Trim(myMsgCollection.Items(intNdx).strChgCode), strFileNameOnly)
                    Case "HEAD"
                        Select Case Trim(myMsgCollection.Items(intNdx).strFund)
                            Case "02", "03", "05"
                                'do nothing
                            Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                                strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                .Append(strFormat + ControlChars.Tab)
                                strFormat = Format(myMsgCollection.Items(intNdx).dateDate2, " MMM d, yyyy ")
                                .Append(strFormat + ControlChars.Tab)
                        End Select
                        strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                        .Append(strFormat + ControlChars.Tab)
                        strFormat = Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00")
                        .Append(strFormat + ControlChars.Tab)
                        Dim intStartIndex As Integer = InStrRev(myMsgCollection.Items(intNdx).strFileNameSource, "\")
                        If Session("UserName").ToString.ToLower = "support" Then
                            strFileNameOnly = ddlFileNames.SelectedValue
                        Else
                            strFileNameOnly = txtNextFileToProcess.Text
                        End If

                        BuildHEAD(myMsgCollection.Items(intNdx).strSSN, myMsgCollection.Items(intNdx).strFAID, myMsgCollection.Items(intNdx).strFund, Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00"), Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00"), strFileNameOnly)
                    Case "DISB"
                        strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                        .Append(strFormat + ControlChars.Tab)
                        strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                        .Append(strFormat + ControlChars.Tab)
                        Dim intStartIndex As Integer = InStrRev(myMsgCollection.Items(intNdx).strFileNameSource, "\")

                        If Session("UserName").ToString.ToLower = "support" Then
                            strFileNameOnly = ddlFileNames.SelectedValue
                        Else
                            strFileNameOnly = txtNextFileToProcess.Text
                        End If

                        BuildDISB(myMsgCollection.Items(intNdx).strSSN, myMsgCollection.Items(intNdx).strFAID, Trim(myMsgCollection.Items(intNdx).strFund), Trim(myMsgCollection.Items(intNdx).dateDate1), Trim(myMsgCollection.Items(intNdx).decAmount1), strFileNameOnly)
                    Case "RCVD"
                        strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                        .Append(strFormat + ControlChars.Tab)
                        strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                        .Append(strFormat + ControlChars.Tab)
                        If Len(Trim(myMsgCollection.Items(intNdx).strCheckNo)) > 0 Then
                            .Append(Trim(myMsgCollection.Items(intNdx).strCheckNo) + ControlChars.Tab)
                        End If
                        Dim intStartIndex As Integer = InStrRev(myMsgCollection.Items(intNdx).strFileNameSource, "\")

                        If Session("UserName").ToString.ToLower = "support" Then
                            strFileNameOnly = ddlFileNames.SelectedValue
                        Else
                            strFileNameOnly = txtNextFileToProcess.Text
                        End If

                        BuildRCVD(myMsgCollection.Items(intNdx).strSSN, myMsgCollection.Items(intNdx).strFAID, Trim(myMsgCollection.Items(intNdx).strFund), Trim(myMsgCollection.Items(intNdx).dateDate1), Trim(myMsgCollection.Items(intNdx).decAmount1), Trim(myMsgCollection.Items(intNdx).strCheckNo), strFileNameOnly)
                    Case "REFU"
                        Select Case Trim(myMsgCollection.Items(intNdx).strFund)
                            Case "02", "03", "05"
                                'do nothing
                                Try
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    .Append("" + ControlChars.Tab)
                                End Try
                            Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                                Try
                                    strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                    .Append(strFormat + ControlChars.Tab)
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    .Append("" + ControlChars.Tab)
                                End Try
                        End Select
                        strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                        .Append(strFormat + ControlChars.Tab)
                        Dim intStartIndex As Integer = InStrRev(myMsgCollection.Items(intNdx).strFileNameSource, "\")

                        If Session("UserName").ToString.ToLower = "support" Then
                            strFileNameOnly = ddlFileNames.SelectedValue
                        Else
                            strFileNameOnly = txtNextFileToProcess.Text
                        End If

                        Select Case Trim(myMsgCollection.Items(intNdx).strFund)
                            Case "02", "03", "05"
                                BuildREFU(myMsgCollection.Items(intNdx).strSSN, myMsgCollection.Items(intNdx).strFAID, Trim(myMsgCollection.Items(intNdx).strFund), Trim(myMsgCollection.Items(intNdx).dateDate1), Trim(myMsgCollection.Items(intNdx).decAmount1), "", "", "", "", "", strFileNameOnly)
                            Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                                Try
                                    BuildREFU(myMsgCollection.Items(intNdx).strSSN, myMsgCollection.Items(intNdx).strFAID, Trim(myMsgCollection.Items(intNdx).strFund), Trim(myMsgCollection.Items(intNdx).dateDate1), Trim(myMsgCollection.Items(intNdx).decAmount1), "", "", "", "", "", strFileNameOnly)
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    BuildREFU(myMsgCollection.Items(intNdx).strSSN, myMsgCollection.Items(intNdx).strFAID, Trim(myMsgCollection.Items(intNdx).strFund), "1/1/0001", Trim(myMsgCollection.Items(intNdx).decAmount1), "", "", "", "", "", strFileNameOnly)
                                End Try
                        End Select
                    Case "CHNG"
                        Select Case myMsgCollection.Items(intNdx).strChgCode
                            Case "C"
                                strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                .Append(strFormat + ControlChars.Tab)
                                strFormat = Format(myMsgCollection.Items(intNdx).dateDate2, " MMM d, yyyy ")
                                .Append(strFormat + ControlChars.Tab)
                                strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                                .Append(strFormat + ControlChars.Tab)
                                strFormat = Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00")
                                .Append(strFormat + ControlChars.Tab)

                                Dim intStartIndex As Integer = InStrRev(myMsgCollection.Items(intNdx).strFileNameSource, "\")

                                If Session("UserName").ToString.ToLower = "support" Then
                                    strFileNameOnly = ddlFileNames.SelectedValue
                                Else
                                    strFileNameOnly = txtNextFileToProcess.Text
                                End If

                                'Build Change Table
                                BuildCHNG(myMsgCollection.Items(intNdx).strSSN, _
                                myMsgCollection.Items(intNdx).strFAID, _
                                Trim(myMsgCollection.Items(intNdx).strFund), _
                                myMsgCollection.Items(intNdx).strChgCode, _
                                Trim(myMsgCollection.Items(intNdx).dateDate1), _
                                Trim(myMsgCollection.Items(intNdx).decAmount1), _
                                Trim(myMsgCollection.Items(intNdx).dateDate2), _
                                Trim(myMsgCollection.Items(intNdx).decAmount2), _
                                strFileNameOnly)
                            Case "N"
                                strFormat = Format(myMsgCollection.Items(intNdx).dateDate2, " MMM d, yyyy ")
                                .Append(strFormat + ControlChars.Tab)
                                strFormat = Format(myMsgCollection.Items(intNdx).decAmount2, "$##,##0.00")
                                .Append(strFormat + ControlChars.Tab)
                                Dim intStartIndex As Integer = InStrRev(myMsgCollection.Items(intNdx).strFileNameSource, "\")

                                If Session("UserName").ToString.ToLower = "support" Then
                                    strFileNameOnly = ddlFileNames.SelectedValue
                                Else
                                    strFileNameOnly = txtNextFileToProcess.Text
                                End If

                                BuildCHNG(myMsgCollection.Items(intNdx).strSSN, _
                                    myMsgCollection.Items(intNdx).strFAID, _
                                    Trim(myMsgCollection.Items(intNdx).strFund), _
                                    myMsgCollection.Items(intNdx).strChgCode, _
                                    "01/01/1900", _
                                    "0.0", _
                                    Trim(myMsgCollection.Items(intNdx).dateDate2), _
                                    Trim(myMsgCollection.Items(intNdx).decAmount2), _
                                    strFileNameOnly)
                                'Build Change Table

                            Case "D"
                                strFormat = Format(myMsgCollection.Items(intNdx).dateDate1, " MMM d, yyyy ")
                                .Append(strFormat + ControlChars.Tab)
                                strFormat = Format(myMsgCollection.Items(intNdx).decAmount1, "$##,##0.00")
                                .Append(strFormat + ControlChars.Tab)

                                Dim intStartIndex As Integer = InStrRev(myMsgCollection.Items(intNdx).strFileNameSource, "\")

                                If Session("UserName").ToString.ToLower = "support" Then
                                    strFileNameOnly = ddlFileNames.SelectedValue
                                Else
                                    strFileNameOnly = txtNextFileToProcess.Text
                                End If

                                'Build Change Table
                                BuildCHNG(myMsgCollection.Items(intNdx).strSSN, _
                                        myMsgCollection.Items(intNdx).strFAID, _
                                        Trim(myMsgCollection.Items(intNdx).strFund), _
                                        myMsgCollection.Items(intNdx).strChgCode, _
                                        Trim(myMsgCollection.Items(intNdx).dateDate1), _
                                        Trim(myMsgCollection.Items(intNdx).decAmount1), _
                                        "01/01/1900", _
                                        "0.0", _
                                        strFileNameOnly)
                        End Select
                        If Len(Trim(myMsgCollection.Items(intNdx).strChgCode)) > 0 Then
                            .Append(Trim(myMsgCollection.Items(intNdx).strChgCode) + ControlChars.Tab)
                        End If
                End Select
                .Append(fName)
            End With

        Next


        Session("FileName") = strFileNameOnly
        Session("MessageCollection") = myMsgCollection

        'Bind DataSource Based on message type
        Select Case strMessageType
            Case "FAID"
                dgFAID.DataSource = Session("dtFAID")
                dgFAID.DataBind()
            Case "RCVD"
                dgrdRCVD.DataSource = Session("RCVD")
                dgrdRCVD.DataBind()
            Case "REFU"
                dgrdREFU.DataSource = Session("dtREFU")
                dgrdREFU.DataBind()
            Case "DISB"
                dgrdDISB.DataSource = Session("dtDISB")
                dgrdDISB.DataBind()
            Case "HEAD"
                dgrdHEAD.DataSource = Session("dtHEAD")
                dgrdHEAD.DataBind()
            Case "CHNG"
                dgchng.DataSource = Session("chng")
                dgchng.DataBind()
        End Select

        'Bind Log 
        dgLogMessages.DataSource = Session("dtLogMessage")
        dgLogMessages.DataBind()

        Return True




    End Function

    Private Sub BuildDISB(ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal DisbDate As Date, ByVal DisbAmt As Decimal, ByVal FileName As String)
        ShowDISB()
        If Not SSN = "" Then
            tbDISBHeader.Visible = True
            Try
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcFAID As New DataColumn("FAID", GetType(String))
                Dim dcFund As New DataColumn("Fund", GetType(String))
                Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
                Dim dcDisbAmt As New DataColumn("DisbAmt", GetType(String))
                Dim dcFileName As New DataColumn("FileName", GetType(String))

                dtDISB.Columns.Add(dcSSN)
                dtDISB.Columns.Add(dcFAID)
                dtDISB.Columns.Add(dcFund)
                dtDISB.Columns.Add(dcDisbDate)
                dtDISB.Columns.Add(dcDisbAmt)
                dtDISB.Columns.Add(dcFileName)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            Dim drDISB As DataRow
            drDISB = dtDISB.NewRow()
            drDISB("SSN") = SSN
            drDISB("FAID") = FAID
            drDISB("Fund") = Fund
            If DisbDate = "1/1/0001" Then
                drDISB("DisbDate") = ""
            Else
                drDISB("DisbDate") = DisbDate.ToShortDateString
            End If
            drDISB("DisbAmt") = DisbAmt
            drDISB("FileName") = FileName
            dtDISB.Rows.Add(drDISB)
            Session("dtDISB") = dtDISB
        End If
    End Sub

    Private Sub BuildCHNG(ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal ChgCode As String, ByVal DisbDate As Date, ByVal DisbAmt As Decimal, ByVal newDisbDate As Date, ByVal newDisbAmt As Decimal, ByVal FileName As String)
        If Not SSN = "" Then
            Dim drchng As DataRow
            Dim strChangeCode As String
            tbchngheader.Visible = True
            Try
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcFAID As New DataColumn("FAID", GetType(String))
                Dim dcFund As New DataColumn("Fund", GetType(String))
                Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
                Dim dcDisbAmt As New DataColumn("DisbAmt", GetType(String))
                Dim dcDisbDate1 As New DataColumn("DisbDate1", GetType(String))
                Dim dcDisbAmt1 As New DataColumn("DisbAmt1", GetType(String))
                Dim dcCheckNo As New DataColumn("CheckNo", GetType(String))
                Dim dcFileName As New DataColumn("FileName", GetType(String))

                dt.Columns.Add(dcSSN)
                dt.Columns.Add(dcFAID)
                dt.Columns.Add(dcFund)
                dt.Columns.Add(dcDisbDate)
                dt.Columns.Add(dcDisbAmt)
                dt.Columns.Add(dcDisbDate1)
                dt.Columns.Add(dcDisbAmt1)
                dt.Columns.Add(dcCheckNo)
                dt.Columns.Add(dcFileName)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drchng = dt.NewRow()
            drchng("SSN") = SSN
            drchng("FAID") = FAID
            drchng("Fund") = Fund
            Try
                If Year(DisbDate) >= "1990" Then
                    drchng("DisbDate") = DisbDate.ToShortDateString
                    Exit Try
                End If
                If DisbDate = "1/1/1900" Or DisbDate = "" Or DisbDate = "1/1/0001" Then
                    drchng("DisbDate") = ""
                Else
                    drchng("DisbDate") = DisbDate.ToShortDateString
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                drchng("DisbDate") = ""
            End Try
            drchng("DisbAmt") = DisbAmt
            Try
                If Year(newDisbDate) >= "1990" Then
                    drchng("DisbDate1") = newDisbDate.ToShortDateString
                    Exit Try
                End If
                If newDisbDate = "1/1/1900" Or newDisbDate = "" Or newDisbDate = "1/1/0001" Then
                    drchng("DisbDate1") = ""
                Else
                    drchng("DisbDate1") = newDisbDate.ToShortDateString
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                drchng("DisbDate1") = ""
            End Try

            drchng("DisbAmt1") = newDisbAmt
            Select Case ChgCode
                Case "N"
                    strChangeCode = "New"
                Case "D"
                    strChangeCode = "Delete"
                Case "C"
                    strChangeCode = "Change"
                Case Else
                    strChangeCode = ""
            End Select
            drchng("CheckNo") = strChangeCode
            drchng("FileName") = FileName
            dt.Rows.Add(drchng)
            Session("chng") = dt
        End If
    End Sub

    Private Sub BuildRCVD(ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal DisbDate As Date, ByVal DisbAmt As Decimal, ByVal checkno As String, ByVal FileName As String)
        ShowRCVD()
        If Not SSN = "" Then
            Dim drRCVD As DataRow
            tbRCVDHeader.Visible = True
            Try
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcFAID As New DataColumn("FAID", GetType(String))
                Dim dcFund As New DataColumn("Fund", GetType(String))
                Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
                Dim dcDisbAmt As New DataColumn("DisbAmt", GetType(String))
                Dim dcCheckNo As New DataColumn("CheckNo", GetType(String))
                Dim dcFileName As New DataColumn("FileName", GetType(String))

                dt.Columns.Add(dcSSN)
                dt.Columns.Add(dcFAID)
                dt.Columns.Add(dcFund)
                dt.Columns.Add(dcDisbDate)
                dt.Columns.Add(dcDisbAmt)
                dt.Columns.Add(dcCheckNo)
                dt.Columns.Add(dcFileName)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drRCVD = dt.NewRow()
            drRCVD("SSN") = SSN
            drRCVD("FAID") = FAID
            drRCVD("Fund") = Fund
            If DisbDate = "1/1/0001" Then
                drRCVD("DisbDate") = ""
            Else
                drRCVD("DisbDate") = DisbDate.ToShortDateString
            End If
            drRCVD("DisbAmt") = DisbAmt
            drRCVD("CheckNo") = checkno
            drRCVD("FileName") = FileName
            dt.Rows.Add(drRCVD)
            Session("RCVD") = dt
        End If
    End Sub

    Private Sub BuildREFU(ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal DisbDate As Date, ByVal DisbAmt As Decimal, ByVal Period As String, ByVal DeliveryMethod As String, ByVal checkno As String, ByVal reference As String, ByVal Payee As String, ByVal FileName As String)
        ShowREFU()
        If Not SSN = "" Then
            Try
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcFAID As New DataColumn("FAID", GetType(String))
                Dim dcFund As New DataColumn("Fund", GetType(String))
                Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
                Dim dcDisbAmt As New DataColumn("DisbAmt", GetType(String))
                Dim dcPeriod As New DataColumn("Period", GetType(String))
                Dim dcDeliveryMethod As New DataColumn("DeliveryMethod", GetType(String))
                Dim dcCheckNo As New DataColumn("CheckNo", GetType(String))
                Dim dcReference As New DataColumn("Reference", GetType(String))
                Dim dcPayee As New DataColumn("Payee", GetType(String))
                Dim dcFileName As New DataColumn("FileName", GetType(String))

                dtREFU.Columns.Add(dcSSN)
                dtREFU.Columns.Add(dcFAID)
                dtREFU.Columns.Add(dcFund)
                dtREFU.Columns.Add(dcDisbDate)
                dtREFU.Columns.Add(dcDisbAmt)
                dtREFU.Columns.Add(dcPeriod)
                dtREFU.Columns.Add(dcDeliveryMethod)
                dtREFU.Columns.Add(dcCheckNo)
                dtREFU.Columns.Add(dcReference)
                dtREFU.Columns.Add(dcPayee)
                dtREFU.Columns.Add(dcFileName)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            Dim drREFU As DataRow
            drREFU = dtREFU.NewRow()
            drREFU("SSN") = SSN
            drREFU("FAID") = FAID
            drREFU("Fund") = Fund
            Try
                If DisbDate = "1/1/0001" Then
                    drREFU("DisbDate") = ""
                Else
                    drREFU("DisbDate") = DisbDate.ToShortDateString
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                drREFU("Disbdate") = ""
            End Try
            drREFU("DisbAmt") = DisbAmt
            drREFU("Period") = Period
            drREFU("DeliveryMethod") = DeliveryMethod
            drREFU("CheckNo") = checkno
            drREFU("Reference") = reference
            drREFU("FileName") = FileName
            dtREFU.Rows.Add(drREFU)
            Session("dtREFU") = dtREFU
        End If
    End Sub

    Private Sub BuildFAID(ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal AwardYear As String, ByVal dateDate1 As String, ByVal dateDate2 As String, ByVal decAmount1 As String, ByVal decAmount2 As String, ByVal CheckNo As String, ByVal ChgCode As String, ByVal FileName As String)
        ShowFAID()
        If Not SSN = "" Then
            Dim drFAID As DataRow
            tbFAID.Visible = True
            Try
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcFAID As New DataColumn("FAID", GetType(String))
                Dim dcFund As New DataColumn("Fund", GetType(String))
                Dim dcAwardYear As New DataColumn("AwardYear", GetType(String))
                Dim dcLoanBeginDate As New DataColumn("LoanBeginDate", GetType(String))
                Dim dcLoanEndDate As New DataColumn("LoanEndDate", GetType(String))
                Dim dcGrossAmount As New DataColumn("GrossAmount", GetType(String))
                Dim dcFileName As New DataColumn("FileName", GetType(String))

                dtFAID.Columns.Add(dcSSN)
                dtFAID.Columns.Add(dcFAID)
                dtFAID.Columns.Add(dcFund)
                dtFAID.Columns.Add(dcAwardYear)
                dtFAID.Columns.Add(dcLoanBeginDate)
                dtFAID.Columns.Add(dcLoanEndDate)
                dtFAID.Columns.Add(dcGrossAmount)
                dtFAID.Columns.Add(dcFileName)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drFAID = dtFAID.NewRow()
            drFAID("SSN") = SSN
            drFAID("FAID") = FAID
            drFAID("Fund") = Fund
            drFAID("AwardYear") = AwardYear
            If Fund = "02" Or Fund = "03" Or Fund = "04" Then
                If Mid(AwardYear, 1, 4).Length = 4 Then
                    drFAID("LoanBeginDate") = "07/01/" & Mid(AwardYear, 1, 4)
                Else
                    drFAID("LoanBeginDate") = "NA"
                End If
            Else
                drFAID("LoanBeginDate") = dateDate1
            End If
            If Fund = "02" Or Fund = "03" Or Fund = "04" Then
                If Mid(AwardYear, 6, 2).Length = 2 Then
                    drFAID("LoanEndDate") = "06/30/20" & Mid(AwardYear, 6, 2)
                Else
                    drFAID("LoanEndDate") = "NA"
                End If
            Else
                drFAID("LoanEndDate") = dateDate2
            End If
            drFAID("GrossAmount") = decAmount1
            drFAID("FileName") = FileName
            dtFAID.Rows.Add(drFAID)
            Session("dtFAID") = dtFAID
        End If
    End Sub

    Private Sub BuildHEAD(ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal GrossAmount As Decimal, ByVal NetAmount As Decimal, ByVal FileName As String)
        ShowHEAD()
        If Not SSN = "" Then
            Dim drHEAD As DataRow
            tbHEAD.Visible = True
            Try
                Dim dcSSN As New DataColumn("SSN", GetType(String))
                Dim dcFAID As New DataColumn("FAID", GetType(String))
                Dim dcFund As New DataColumn("Fund", GetType(String))
                Dim dcGrossAmount As New DataColumn("GrossAmount", GetType(String))
                Dim dcNetAmount As New DataColumn("NetAmount", GetType(String))
                Dim dcFileName As New DataColumn("FileName", GetType(String))

                dtHEAD.Columns.Add(dcSSN)
                dtHEAD.Columns.Add(dcFAID)
                dtHEAD.Columns.Add(dcFund)
                dtHEAD.Columns.Add(dcGrossAmount)
                dtHEAD.Columns.Add(dcNetAmount)
                dtHEAD.Columns.Add(dcFileName)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            drHEAD = dtHEAD.NewRow()
            drHEAD("SSN") = SSN
            drHEAD("FAID") = FAID
            drHEAD("Fund") = Fund
            drHEAD("GrossAmount") = GrossAmount
            drHEAD("NetAmount") = NetAmount
            drHEAD("FileName") = FileName
            dtHEAD.Rows.Add(drHEAD)
            Session("dtHEAD") = dtHEAD
        End If
    End Sub

    Private Sub ShowFAID()
        tbRCVDHeader.Visible = False
        dgrdRCVD.Visible = False
        tbREFUHeader.Visible = False
        dgrdREFU.Visible = False
        tbDISBHeader.Visible = False
        dgrdDISB.Visible = False
        tbHEAD.Visible = False
        dgrdHEAD.Visible = False
        tbFAID.Visible = True
        dgFAID.Visible = True
    End Sub

    Private Sub ShowDISB()
        tbFAID.Visible = False
        dgFAID.Visible = False
        tbRCVDHeader.Visible = False
        dgrdRCVD.Visible = False
        tbREFUHeader.Visible = False
        dgrdREFU.Visible = False
        tbHEAD.Visible = False
        dgrdHEAD.Visible = False
        tbDISBHeader.Visible = True
        dgrdDISB.Visible = True
    End Sub

    Private Sub ShowRCVD()
        tbFAID.Visible = False
        dgFAID.Visible = False
        tbREFUHeader.Visible = False
        dgrdREFU.Visible = False
        tbDISBHeader.Visible = False
        dgrdDISB.Visible = False
        tbHEAD.Visible = False
        dgrdHEAD.Visible = False
        tbRCVDHeader.Visible = True
        dgrdRCVD.Visible = True
    End Sub

    Private Sub ShowREFU()
        tbFAID.Visible = False
        dgFAID.Visible = False
        tbRCVDHeader.Visible = False
        dgrdRCVD.Visible = False
        tbDISBHeader.Visible = False
        dgrdDISB.Visible = False
        tbHEAD.Visible = False
        dgrdHEAD.Visible = False
        tbREFUHeader.Visible = True
        dgrdREFU.Visible = True
    End Sub

    Private Sub ShowHEAD()
        tbFAID.Visible = False
        dgFAID.Visible = False
        tbRCVDHeader.Visible = False
        dgrdRCVD.Visible = False
        tbREFUHeader.Visible = False
        dgrdREFU.Visible = False
        tbDISBHeader.Visible = False
        dgrdDISB.Visible = False
        tbHEAD.Visible = True
        dgrdHEAD.Visible = True
    End Sub

    Protected Sub dgchng_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgchng.NeedDataSource
        If Not Session("chng") Is Nothing Then
            dgchng.MasterTableView.DataSource = Session("chng")
        End If

    End Sub

    Protected Sub dgFAID_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgFAID.NeedDataSource
        If Not Session("dtFAID") Is Nothing Then
            dgFAID.MasterTableView.DataSource = Session("dtFAID")
        End If

    End Sub

    Protected Sub dgrdRCVD_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgrdRCVD.NeedDataSource
        If Not Session("RCVD") Is Nothing Then
            dgrdRCVD.MasterTableView.DataSource = Session("RCVD")
        End If

    End Sub

    Protected Sub dgrdREFU_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgrdREFU.NeedDataSource
        If Not Session("dtREFU") Is Nothing Then
            dgrdREFU.MasterTableView.DataSource = Session("dtREFU")
        End If

    End Sub

    Protected Sub dgrdDISB_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgrdDISB.NeedDataSource
        If Not Session("dtDISB") Is Nothing Then
            dgrdDISB.MasterTableView.DataSource = Session("dtDISB")
        End If

    End Sub

    Protected Sub dgrdHEAD_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgrdHEAD.NeedDataSource
        If Not Session("dtHEAD") Is Nothing Then
            dgrdHEAD.MasterTableView.DataSource = Session("dtHEAD")
        End If

    End Sub

    Protected Sub dgQueue_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgQueue.NeedDataSource
        If Not Session("dgQueue") Is Nothing Then
            dgQueue.MasterTableView.DataSource = Session("dgQueue")
        End If

    End Sub

    Protected Sub btnSaveData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveData.Click
        'ImportData()
    End Sub

    Private Sub ImportData()
        Dim cxnString As String
        myMsgCollection = Session("MessageCollection")
        Dim nbrMessages As Integer = myMsgCollection.Count
        Dim intNdx As Integer = 0
        Dim fName As String = ""
        Dim strLogMessage As New StringBuilder
        Dim strMessage As String = ""
        Dim strProcessResult As String = ""
        Dim strExceptionGUID As String = Guid.NewGuid.ToString
        Dim ds As New DataSet
        Dim intFailedRecords As Integer = 0
        Dim intInitialImportSuccessful As Integer = 0
        Dim strValidMessage As String = ""
        Dim intFAIDExists As Integer = 0
        Dim intHEADExists As Integer = 0
        Dim intDISBExists As Integer = 0
        Dim errStartRecod As Integer = 0
        Dim errTotalRecords As Integer = 0 'Total Records that errored out unexpectedly. This is not the same as processed exceptions.

        Try
            cxnString = ConString
            ClearDataGrid()

            If Session("UserName").ToString.ToLower = "support" Then
                Session("FileName") = ddlFileNames.SelectedValue
            Else
                Session("FileName") = txtNextFileToProcess.Text
            End If

            strESPDataFromFameESP = IIf(myFLFacade.IsRemoteServer(CampusId), "yes", "no")

            If strESPDataFromFameESP = "yes" Then
                strProcessResult = myFLFacade.ProcessFLMsgCollection(cxnString, myMsgCollection, strSourceFilePath, strTargetFilePath, Trim(Session("FileName")), strExceptionFilePath, strExceptionGUID, "sourcetotarget", "", MyAdvAppSettings.AppSettings("AwardsCutOffDate"), "yes", , , , , , , , , , )
            Else
                strProcessResult = myFLFacade.ProcessFLMsgCollection(cxnString, myMsgCollection, strSourceFilePath, strTargetFilePath, Trim(Session("FileName")), strExceptionFilePath, strExceptionGUID, "sourcetotarget", "", MyAdvAppSettings.AppSettings("AwardsCutOffDate"), , , , , , , , , , , )
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            If Not strProcessResult = "" Then
                Dim intAwardCutOffCount As Integer = 0
                intAwardCutOffCount = myFLFacade.CountByAwardDate(strExceptionGUID)

                ds = myFLFacade.getExceptionReport(strExceptionGUID)

                intFailedRecords = ds.Tables(0).Rows.Count
                intTotalRecordCount = CType(Session("RecordCount"), Integer)

                If strProcessResult.Contains("ErrorStartingAtRecord") Then
                    errStartRecod = CInt(Mid(strProcessResult, 23))
                    errTotalRecords = intTotalRecordCount - errStartRecod + 1
                End If

                If Mid(Trim(Session("FileName")), 1, 8) = "CLFAFAID" Then
                    intAwardCutOffCount = myFLFacade.CountByAwardDate(strExceptionGUID)
                    Dim strSuccessMessage As String = "Number of  records posted: " & (intTotalRecordCount - intFailedRecords - intAwardCutOffCount - errTotalRecords)
                    Dim strFailedMessage As String = "Number of records not posted: " & intFailedRecords
                    Dim strFailedDueToCutoffDate As String = "Number of records not posted due to cutoff date: " & intAwardCutOffCount
                    If errTotalRecords > 0 Then
                        strFailedMessage &= vbCrLf & "Number of records unexpectedly error out: " & errTotalRecords.ToString & vbCrLf & "PLEASE CONTACT FAME SUPPORT ABOUT THE RECORDS THAT UNEXPECTEDLY ERROR OUT."
                    End If
                    myFLFacade.UpdateExceptionCount(Trim(Session("FileName")), (intTotalRecordCount - intFailedRecords - intAwardCutOffCount - errTotalRecords), intFailedRecords + errTotalRecords)

                    strMessage &= strSuccessMessage
                    strMessage &= vbCrLf
                    strMessage &= strFailedMessage
                    strMessage &= vbCrLf
                    strMessage &= strFailedDueToCutoffDate
                    'BuildFilesList(True)
                Else
                    Dim strSuccessMessage As String = "Number of  records posted:" & (intTotalRecordCount - intFailedRecords - errTotalRecords)
                    Dim strFailedMessage As String = "Number of records not posted:" & intFailedRecords.ToString
                    If errTotalRecords > 0 Then
                        strFailedMessage &= vbCrLf & "Number of records unexpectedly error out:" & errTotalRecords.ToString & vbCrLf & "PLEASE CONTACT FAME SUPPORT ABOUT THE RECORDS THAT UNEXPECTEDLY ERROR OUT."
                    End If
                    myFLFacade.UpdateExceptionCount(Trim(Session("FileName")), (intTotalRecordCount - intFailedRecords - errTotalRecords), intFailedRecords + errTotalRecords)
                    strMessage &= strSuccessMessage
                    strMessage &= vbCrLf
                    strMessage &= strFailedMessage
                    'BuildFilesList(True)
                End If


                'Build exception report
                dgExceptionReport.Visible = True
                tblException.Visible = True
                btnExportToExceldgExceptionReport.Visible = True
                With dgExceptionReport
                    .DataSource = ds
                    .DataBind()
                End With

                'Put the DataSet in Session so it can be used by the NeedDataSource to allow paging on the exception report grid
                Session("ExceptionReport") = ds

                ClearDataGrid()
                Dim strMoveFile As String = ""

                strMoveFile = MoveFileFromInToExceptionFolder(strSourceFilePath, strExceptionFilePath, Trim(Session("FileName")))

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''
                BuildFilesList(True)

                If Not strMoveFile = "" Then
                    Dim strErrorMsg As String = ""
                    strErrorMsg &= strMessage & vbLf
                    strErrorMsg &= strMoveFile & vbLf
                    DisplayErrorMessage(strErrorMsg)
                    Exit Sub
                Else
                    DisplayErrorMessage(strMessage)
                End If



            Else
                Dim strMoveFile As String = ""

                strMoveFile = MoveFileFromInToExceptionFolder(strSourceFilePath, strTargetFilePath, Trim(Session("FileName")))
                '''''''''''''''''''''''''''''''''''''''''''''''
                ClearDataGrid()
                BuildFilesList(True)

                If Not strMoveFile = "" Then
                    Dim strErrorMsg As String = ""
                    strErrorMsg = "Financial aid data (" + Trim(Session("FileName")) + ") successfully posted " & vbLf
                    strErrorMsg &= strMoveFile & vbLf
                    DisplayErrorMessage(strErrorMsg)
                    Exit Sub
                Else
                    DisplayErrorMessage("Financial aid data (" + Trim(Session("FileName")) + ") successfully posted and archived")
                End If

            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage(ex.Message)
        End Try
    End Sub

    Private Sub MoveFileToESPExceptionFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String)
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String = uriSource.AbsolutePath + FileName
        Dim strTargetFile As String = uriTarget.AbsolutePath + FileName
        Dim strMessage As String = ""
        Try
            If Not File.Exists(strTargetFile) Then
                'Directory.Move(strSourceFile, strTargetFile)
                RenameExistingFile(strSourceFile)
                File.Move(strSourceFile, strTargetFile)
            End If
        Catch e As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(e)

        End Try
    End Sub

    Private Sub RenameExistingFile(ByVal strTargetPathWithFile As String)
        Dim sMessage As String = ""
        Dim strRandomNumber As New Random
        Dim rightNow As DateTime = DateTime.Now
        Dim s As String
        s = rightNow.ToString("MMddyyyyhms")
        Dim strTargetPathWithNewFile As String = strTargetPathWithFile + s   '+ strRandomNumber.Next.ToString
        If File.Exists(strTargetPathWithFile) Then
            File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
        End If
    End Sub

    Private Function MoveFileFromInToExceptionFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String) As String
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        If myFLFacade.IsRemoteServer(CampusId) Then
            strSourceFile = SourcePath + FileName
            strTargetFile = TargetPath + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If

        'Dim strMessage As String = ""
        Try
            'modified by balaji on 10/11/2007
            'based on input from patrick - files from Fame ESP will have an extension that is sequencial
            'in other words we won't have a case where there will be two HEAD Files (or any other file types)
            'with same extension, the extension will be .000,.001,.002
            'so when that is the case there is no need to rename the file.
            If File.Exists(strSourceFile) Then
                RenameExistingFile(strTargetFile)
                File.Move(strSourceFile, strTargetFile)
                Return ""
            End If
        Catch e As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(e)

            Return "The Process cannot access the file " & FileName & " because it is being used by another process."
        End Try
        Return String.Empty
    End Function

    Private Sub ShowGrid(ByVal FLInfo As FLFileInfo)
        With strLogEntry
            .Append("SUCCESSFUL Processsing of FameLink file: " + FLInfo.strFileName + ControlChars.CrLf)
            .Append("File Size: " + FLInfo.iFileSize.ToString + ControlChars.CrLf)
            .Append("with " + FLInfo.iRecordCount.ToString + " records of type " + FLInfo.strMsgType + ControlChars.CrLf + "PARSED and waiting to be processed" + ControlChars.CrLf)
        End With
        With strLogMessage
            .Append(".Process Successful " + ControlChars.CrLf)
        End With

        If FLInfo.strMsgType = "FAID" Then
            dgFAID.Visible = True
            tbFAID.Visible = True
            Session("FAID_Message") = "Message Type:FAID  (Successfully Received: " + FLInfo.iRecordCount.ToString + " records)"
            'Else
            '    Session("FAID_Message") = "Message Type:FAID"
        End If
        If FLInfo.strMsgType = "DISB" Then
            dgrdDISB.Visible = True
            tbDISBHeader.Visible = True
            Session("DISB_Message") = "Message Type:DISB  (Successfully Received: " + FLInfo.iRecordCount.ToString + " records)"
            'Else
            '    Session("DISB_Message") = "Message Type:DISB"
        End If
        If FLInfo.strMsgType = "REFU" Then
            dgrdREFU.Visible = True
            tbREFUHeader.Visible = True
            Session("REFU_Message") = "Message Type:REFU  (Successfully Received: " + FLInfo.iRecordCount.ToString + " records)"
            'Else
            '    Session("REFU_Message") = "Message Type:REFU"
        End If
        If FLInfo.strMsgType = "RCVD" Then
            dgrdRCVD.Visible = True
            tbRCVDHeader.Visible = True
            Session("RCVD_Message") = "Message Type:RCVD  (Successfully Received: " + FLInfo.iRecordCount.ToString + " records)"
            'Else
            '    Session("RCVD_Message") = "Message Type:RCVD"
        End If
        If FLInfo.strMsgType = "HEAD" Then
            dgrdHEAD.Visible = True
            tbHEAD.Visible = True
            Session("HEAD_Message") = "Message Type:HEAD  (Successfully Received: " + FLInfo.iRecordCount.ToString + " records)"
        End If
        If FLInfo.strMsgType = "CHNG" Then
            dgchng.Visible = True
            tbchngheader.Visible = True
            Session("CHNG_Message") = "Message Type:CHNG  (Successfully Received: " + FLInfo.iRecordCount.ToString + " records)"

        End If
    End Sub

    Private Sub SetErrorMessage(ByVal FLInfo As FLFileInfo)
        With strLogEntry
            .Append("FAILED Processing of FameLink file: " & FLInfo.strFileName + ControlChars.CrLf)
            .Append("Additional Information: " & FLInfo.strError + ControlChars.CrLf)
        End With
        With strLogMessage
            .Append(".Process Failed " + ControlChars.CrLf)
        End With

        If FLInfo.strMsgType = "FAID" Then
            Session("FAID_Message") = "Message Type:FAID  (Failed Processing of " + FLInfo.iRecordCount.ToString + " records)"
        Else
            Session("FAID_Message") = "Message Type:FAID"
        End If
        If FLInfo.strMsgType = "DISB" Then
            Session("DISB_Message") = "Message Type:DISB  (Failed Processing of " + FLInfo.iRecordCount.ToString + " records)"
        Else
            Session("DISB_Message") = "Message Type:DISB"
        End If
        If FLInfo.strMsgType = "REFU" Then
            Session("REFU_Message") = "Message Type:REFU  (Failed Processing of " + FLInfo.iRecordCount.ToString + " records)"
        Else
            Session("REFU_Message") = "Message Type:REFU"
        End If
        If FLInfo.strMsgType = "RCVD" Then
            Session("RCVD_Message") = "Message Type:RCVD  (Failed Processing of " + FLInfo.iRecordCount.ToString + " records)"
        Else
            Session("RCVD_Message") = "Message Type:RCVD"
        End If
        If FLInfo.strMsgType = "HEAD" Then
            Session("HEAD_Message") = "Message Type:HEAD  (Failed Processing of " + FLInfo.iRecordCount.ToString + " records)"
        Else
            Session("HEAD_Message") = "Message Type:HEAD"
        End If
        If FLInfo.strMsgType = "CHNG" Then
            Session("CHNG_Message") = "Message Type:CHNG  (Failed Processing of " + FLInfo.iRecordCount.ToString + " records)"
        Else
            Session("CHNG_Message") = "Message Type:CHNG"
        End If
    End Sub

    Protected Sub btnExportToExcelCHNG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcelCHNG.Click
        dgchngExportToExcel.DataSource = Session("chng")
        dgchngExportToExcel.DataBind()
        dgchngExportToExcel.ExportSettings.FileName = Session("FileName").ToString
        dgchngExportToExcel.ExportSettings.ExportOnlyData = True
        dgchngExportToExcel.ExportSettings.IgnorePaging = True
        dgchngExportToExcel.ExportSettings.OpenInNewWindow = True
        dgchngExportToExcel.MasterTableView.ExportToExcel()
        dgchng.Visible = True
        dgchng.Dispose()
        dgchngExportToExcel.Dispose()
    End Sub

    Protected Sub btnExportToExcelFAID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcelFAID.Click
        dgFAIDExportToExcel.DataSource = Session("dtFAID")
        dgFAIDExportToExcel.DataBind()
        dgFAIDExportToExcel.ExportSettings.FileName = Session("FileName").ToString
        dgFAIDExportToExcel.ExportSettings.ExportOnlyData = True
        dgFAIDExportToExcel.ExportSettings.IgnorePaging = True
        dgFAIDExportToExcel.ExportSettings.OpenInNewWindow = True
        dgFAIDExportToExcel.MasterTableView.ExportToExcel()
        dgFAID.Visible = True
        dgFAID.Dispose()
        dgFAIDExportToExcel.Dispose()
    End Sub

    Protected Sub btnExportToExcelRCVD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcelRCVD.Click
        dgrdRCVDExportToExcel.DataSource = Session("RCVD")
        dgrdRCVDExportToExcel.DataBind()
        dgrdRCVDExportToExcel.ExportSettings.FileName = Session("FileName").ToString
        dgrdRCVDExportToExcel.ExportSettings.ExportOnlyData = True
        dgrdRCVDExportToExcel.ExportSettings.IgnorePaging = True
        dgrdRCVDExportToExcel.ExportSettings.OpenInNewWindow = True
        dgrdRCVDExportToExcel.MasterTableView.ExportToExcel()
        dgrdRCVD.Visible = True
        dgrdRCVD.Dispose()
        dgrdRCVDExportToExcel.Dispose()
    End Sub

    Protected Sub btnExportToExcelREFU_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcelREFU.Click
        dgrdREFUExportToExcel.DataSource = Session("dtREFU")
        dgrdREFUExportToExcel.DataBind()
        dgrdREFUExportToExcel.ExportSettings.FileName = Session("FileName").ToString
        dgrdREFUExportToExcel.ExportSettings.ExportOnlyData = True
        dgrdREFUExportToExcel.ExportSettings.IgnorePaging = True
        dgrdREFUExportToExcel.ExportSettings.OpenInNewWindow = True
        dgrdREFUExportToExcel.MasterTableView.ExportToExcel()
        dgrdREFU.Visible = True
        dgrdREFU.Dispose()
        dgrdREFUExportToExcel.Dispose()
    End Sub

    Protected Sub btnExportToExcelDISB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcelDISB.Click
        dgrdDISBExportToExcel.DataSource = Session("dtDISB")
        dgrdDISBExportToExcel.DataBind()
        dgrdDISBExportToExcel.ExportSettings.FileName = Session("FileName").ToString
        dgrdDISBExportToExcel.ExportSettings.ExportOnlyData = True
        dgrdDISBExportToExcel.ExportSettings.IgnorePaging = True
        dgrdDISBExportToExcel.ExportSettings.OpenInNewWindow = True
        dgrdDISBExportToExcel.MasterTableView.ExportToExcel()
        dgrdDISB.Visible = True
        dgrdDISB.Dispose()
        dgrdDISBExportToExcel.Dispose()
    End Sub

    Protected Sub btnExportToExcelHEAD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcelHEAD.Click
        dgrdHEADExportToExcel.DataSource = Session("dtHEAD")
        dgrdHEADExportToExcel.DataBind()
        dgrdHEADExportToExcel.ExportSettings.FileName = Session("FileName").ToString
        dgrdHEADExportToExcel.ExportSettings.ExportOnlyData = True
        dgrdHEADExportToExcel.ExportSettings.IgnorePaging = True
        dgrdHEADExportToExcel.ExportSettings.OpenInNewWindow = True
        dgrdHEADExportToExcel.MasterTableView.ExportToExcel()
        dgrdHEAD.Visible = True
        dgrdHEAD.Dispose()
        dgrdHEADExportToExcel.Dispose()
    End Sub

    Protected Sub btnImportFAID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportFAID.Click
        ImportData()
    End Sub

    Protected Sub btnImportRCVD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportRCVD.Click
        ImportData()
    End Sub

    Protected Sub btnImportREFU_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportREFU.Click
        ImportData()
    End Sub

    Protected Sub btnImportDISB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportDISB.Click
        ImportData()
    End Sub

    Protected Sub btnImportHEAD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportHEAD.Click
        ImportData()
    End Sub

    Protected Sub btnImportCHNG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportCHNG.Click
        ImportData()
    End Sub

    Protected Sub dgExceptionReport_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgExceptionReport.NeedDataSource
        dgExceptionReport.MasterTableView.DataSource = Session("ExceptionReport")
    End Sub


    Protected Sub btnExportToExceldgExceptionReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExceldgExceptionReport.Click
        dgExceptionReportExportToExcel.DataSource = Session("ExceptionReport")
        dgExceptionReportExportToExcel.DataBind()
        dgExceptionReportExportToExcel.ExportSettings.FileName = Session("FileName").ToString
        dgExceptionReportExportToExcel.ExportSettings.ExportOnlyData = True
        dgExceptionReportExportToExcel.ExportSettings.IgnorePaging = True
        dgExceptionReportExportToExcel.ExportSettings.OpenInNewWindow = True
        dgExceptionReportExportToExcel.MasterTableView.ExportToExcel()
        dgExceptionReport.Visible = True
        dgExceptionReport.Dispose()
        dgExceptionReportExportToExcel.Dispose()
    End Sub

End Class
