﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EdExpressExceptionReport.aspx.vb" Inherits="FameLink_EdExpressExceptionReport" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>

   <style type="text/css">
   h2 { font: bold 11px verdana; color: #000066; background-color: transparent; width: auto; margin: 0; padding: 0; border: 0;text-align: left;}
   .FileInput
    {
	    margin: 4px;
	    vertical-align: middle;
	    font: normal 10px verdana;
	    text-align:left;
	    color: #000066;
	    width: 300px;
    }
    .ProcessFile
    {
	    margin: 4px;
	    vertical-align: middle;
	    font: normal 10px verdana;
	    text-align:center;
	    color: #000066;
    }
   </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<TR>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
							align="center">
							<tr>
								<td class="detailsframe">
                                      <div class="scrollsingleframe">


                                          <div>
                                              <asp:Table runat="server" ID="tbSelectFile" Width="80%">
                                                  <asp:TableRow Visible="false">
                                                      <asp:TableCell Width="25%" HorizontalAlign="right">
                                                          <asp:Label ID="Label7" Text="Select Academic Year" runat="Server" CssClass="labelbold"></asp:Label>
                                                      </asp:TableCell>
                                                      <asp:TableCell Width="25%" HorizontalAlign="left">
                                                          <asp:DropDownList ID="ddlAcademicYear" runat="server" CssClass="dropdownlists">
                                                          </asp:DropDownList>
                                                      </asp:TableCell>
                                                      <asp:TableCell Width="10%" HorizontalAlign="right">
                                                          <asp:Button ID="btnSetDefault" runat="Server" Text=" Set Default "  />
                                                      </asp:TableCell>
                                                  </asp:TableRow>
                                                  <asp:TableRow>
                                                      <asp:TableCell Width="35%" HorizontalAlign="right">
                                                          <asp:Label ID="lblFileName" Text="Select EDExpress File to View Report" runat="Server"
                                                              CssClass="labelbold"></asp:Label>
                                                      </asp:TableCell>
                                                      <asp:TableCell Width="25%" HorizontalAlign="left">
                                                          <asp:DropDownList ID="ddlFileNames" runat="server" Width="400px" CssClass="drowdownlists">
                                                          </asp:DropDownList>
                                                      </asp:TableCell>
                                                      <asp:TableCell Width="10%">
                                                          <asp:Button ID="btnProcess" runat="server" Text="View Report" />
                                                      </asp:TableCell>
                                                      <asp:TableCell Width="10%">
                                                          <asp:Button ID="btnPrint" runat="Server" Text="Print"  />
                                                          <asp:Button ID="btnSaveData" runat="Server" Text="Post Data" 
                                                              Visible="false" />
                                                      </asp:TableCell>
                                                      <asp:TableCell Width="0%">
                                                          <asp:Image ID="imgProcess" runat="Server" ImageUrl="../Images/processfile.gif" Visible="false" />
                                                      </asp:TableCell>
                                                  </asp:TableRow>
                                                  <asp:TableRow ID="AYSD" Visible="false">
                                                      <asp:TableCell Width="25%" HorizontalAlign="right">
                                                          <asp:Label ID="Label8" Text="Award Year Date" runat="Server" CssClass="labelbold"></asp:Label>
                                                      </asp:TableCell>
                                                      <asp:TableCell Width="25%" HorizontalAlign="left">

<%--                                                          <asp:TextBox ID="txtAwardYearStartDate" TabIndex="2" CssClass="textboxdate" runat="server"></asp:TextBox>
                                                          <a onclick="javascript:OpenCalendar('ClsSect','txtAwardYearStartDate', true, 1945)">
                                                              <img id="Img1" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                                                  border="0" runat="server" /></a>--%>

                                                          <telerik:RadDatePicker ID="txtAwardYearStartDate" MinDate="1/1/1945" runat="server">
                                                          </telerik:RadDatePicker>

                                                      </asp:TableCell>
                                                      <asp:TableCell Width="10%" HorizontalAlign="right">
                                                          <asp:Button ID="btnSetDefaultAYSD" runat="Server" Text=" Set Default "  />
                                                      </asp:TableCell>
                                                  </asp:TableRow>
                                                  <asp:TableRow ID="AYED" Visible="false">
                                                      <asp:TableCell Width="25%" HorizontalAlign="right">
                                                          <asp:Label ID="Label9" Text="Award End Date" runat="Server" CssClass="labelbold"></asp:Label>
                                                      </asp:TableCell>
                                                      <asp:TableCell Width="25%" HorizontalAlign="left">

<%--                                                          <asp:TextBox ID="txtAwardYearEndDate" TabIndex="2" CssClass="textboxdate" runat="server"></asp:TextBox>
                                                          <a onclick="javascript:OpenCalendar('ClsSect','txtAwardYearEndDate', true, 1945)">
                                                              <img id="Img6" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                                                  border="0" runat="server" /></a>--%>

                                                          <telerik:RadDatePicker ID="txtAwardYearEndDate" MinDate="1/1/1945" runat="server">
                                                          </telerik:RadDatePicker>


                                                      </asp:TableCell>
                                                      <asp:TableCell Width="10%" HorizontalAlign="right">
                                                          <asp:Button ID="btnSetDefaultAYED" runat="Server" Text=" Set Default "  />
                                                      </asp:TableCell>
                                                  </asp:TableRow>
                                              </asp:Table>
                                              <br />
                                              <asp:Table runat="server" ID="tbPellAwards" CellPadding="0" CellSpacing="0" CssClass="datagridheader"
                                                  Width="100%">
                                                  <asp:TableHeaderRow>
                                                      <asp:TableHeaderCell CssClass="tableheader" HorizontalAlign="Center">
                                                          <asp:Label ID="lblMessageHead" runat="server" CssClass="labelbold">Pell, SMART, ACG and TEACH Awards</asp:Label>
                                                      </asp:TableHeaderCell>
                                                  </asp:TableHeaderRow>
                                              </asp:Table>
                                              <asp:DataGrid ID="dgPellAward" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                                  BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal"
                                                  Width="100%" runat="server">
                                                  <EditItemStyle Wrap="False"></EditItemStyle>
                                                  <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                  <Columns>
                                                      <asp:TemplateColumn HeaderText="IndexID">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblIndexID" Text='<%# Container.DataItem("IndexID") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Last Name" SortExpression="LastName">
                                                          <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblLastName" Text='<%# Container.DataItem("LastName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="First Name" SortExpression="FirstName">
                                                          <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblFirstName" Text='<%# Container.DataItem("FirstName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="SSN" SortExpression="SSN">
                                                          <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblSSN" runat="Server" Text='<%# Container.DataItem("SSN") %>' CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Campus" SortExpression="Campus">
                                                          <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblCampus" runat="Server" Text='<%# Container.DataItem("Campus") %>'
                                                                  CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Grant<br/>Type">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="6%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblGrantType" Text='<%# Container.DataItem("GrantType") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Award Id">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAwardId" Text='<%# Container.DataItem("AwardId") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Add Date">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAddDate" Text='<%# Container.DataItem("AddDate") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Add Time">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAddTime" Text='<%# Container.DataItem("AddTime") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Award<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAwardAmount" Text='<%# Container.DataItem("AwardAmount") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Award<br/>Year">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:DropDownList ID="ddlAcademicYear" runat="server" DataSource="<%#GetAcademicYear() %>"
                                                                  DataTextField="AcademicYearDescrip" DataValueField="AcademicYearId" CssClass="dropdownlists">
                                                              </asp:DropDownList>
                                                              <asp:Label ID="lblAcademicYear" CssClass="label" runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Award &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>Start Date">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAYSD" CssClass="label" runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Award &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>End Date">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAYED" CssClass="label" runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Reason">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblErrorMsg" Text='<%# Container.DataItem("ErrorMsg") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                  </Columns>
                                              </asp:DataGrid>
                                              <br />
                                              <asp:Table runat="server" ID="tbPellDisb" CellPadding="0" CellSpacing="0" CssClass="datagridheader"
                                                  Width="100%">
                                                  <asp:TableHeaderRow>
                                                      <asp:TableHeaderCell CssClass="tableheader" HorizontalAlign="Center">
                                                          <asp:Label ID="Label5" runat="server" CssClass="labelbold">Pell, SMART, ACG and TEACH Disbursments</asp:Label>
                                                      </asp:TableHeaderCell>
                                                  </asp:TableHeaderRow>
                                              </asp:Table>
                                              <asp:DataGrid ID="dgPellDisb" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                                  BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal"
                                                  Width="100%" runat="server">
                                                  <EditItemStyle Wrap="False"></EditItemStyle>
                                                  <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                  <Columns>
                                                      <asp:TemplateColumn HeaderText="IndexID">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblIndexID" Text='<%# Container.DataItem("IndexID") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Last Name">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle" Width="10%"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblLastName" Text='<%# Container.DataItem("LastName") %>' CssClass="label"
                                                                  runat="server" Width="10px">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="First Name">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblFirstName" Text='<%# Container.DataItem("FirstName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="SSN">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="15%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblSSN" runat="Server" Text='<%# Container.DataItem("SSN") %>' CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Campus">
                                                          <HeaderStyle CssClass="datagridheader" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblCampus" runat="Server" Text='<%# Container.DataItem("Campus") %>'
                                                                  CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Award Id">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAwardId" Text='<%# Container.DataItem("AwardId") %>' CssClass="label"
                                                                  runat="server" Width="15px">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Disbursement<br/>Date">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblDisbDate" Text='<%# Container.DataItem("DisbDate") %>' CssClass="label"
                                                                  runat="server" Width="10px">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Disbursement<br/>Release<br/>Indicator">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblDisbRelInd" Text='<%# Container.DataItem("DisbRelInd") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Action Status" Visible="false">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblActionStatus" Text='<%# Container.DataItem("ActionStatus") %>'
                                                                  CssClass="label" runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Disbursement<br/>Number">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblDisbNum" Text='<%# Container.DataItem("DisbNum") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Submitted<br/>Disbursement<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblSubDisbAmt" Text='<%# Container.DataItem("SubDisbAmt") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Accepted<br/>Disbursement<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAccDisbAmt" Text='<%# Container.DataItem("AccDisbAmt") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Reason">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblErrorMsg" Text='<%# Container.DataItem("ErrorMsg") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                  </Columns>
                                              </asp:DataGrid>
                                              <br />
                                              <br />
                                              <asp:Table runat="server" ID="tbDL" CellPadding="0" CellSpacing="0" CssClass="datagridheader"
                                                  Width="100%">
                                                  <asp:TableHeaderRow>
                                                      <asp:TableHeaderCell CssClass="tableheader" HorizontalAlign="Center">
                                                          <asp:Label ID="lblAwardYearHeader" runat="server" CssClass="labelbold">Direct Loans Award Year</asp:Label>
                                                      </asp:TableHeaderCell>
                                                  </asp:TableHeaderRow>
                                              </asp:Table>
                                              <asp:DataGrid ID="dgDL" CellPadding="0" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0"
                                                  AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal" Width="100%"
                                                  runat="server">
                                                  <EditItemStyle Wrap="False"></EditItemStyle>
                                                  <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                  <Columns>
                                                      <asp:TemplateColumn HeaderText="IndexID">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblIndexID" Text='<%# Container.DataItem("IndexID") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Last Name" SortExpression="LastName">
                                                          <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblLastName" Text='<%# Container.DataItem("LastName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="First Name" SortExpression="FirstName">
                                                          <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblFirstName" Text='<%# Container.DataItem("FirstName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="SSN" SortExpression="SSN">
                                                          <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblSSN" runat="Server" Text='<%# Container.DataItem("SSN") %>' CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Campus" SortExpression="Campus">
                                                          <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblCampus" runat="Server" Text='<%# Container.DataItem("Campus") %>'
                                                                  CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Loan<br/>Type">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblLoanType" Text='<%# Container.DataItem("LoanType") %>' CssClass="label"
                                                                  runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Loan Id">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAwardId" Text='<%# Container.DataItem("AwardId") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Add Date">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAddDate" Text='<%# Container.DataItem("AddDate") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Add Time">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblAddTime" Text='<%# Container.DataItem("AddTime") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Award<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblAwardAmount" Text='<%# Container.DataItem("AwardAmount") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Fees">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblFees" Text='<%# Container.DataItem("Fees") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Net<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblNetAmount" Text='<%# Container.DataItem("NetAmount") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Award<br/>Year">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:DropDownList ID="ddlAcademicYear" runat="server" DataSource="<%#GetAcademicYear() %>"
                                                                  DataTextField="AcademicYearDescrip" DataValueField="AcademicYearId" CssClass="dropdownlists">
                                                              </asp:DropDownList>
                                                              <asp:Label ID="lblAcademicYear" CssClass="label" runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Reason">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblErrorMsg" Text='<%# Container.DataItem("ErrorMsg") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                  </Columns>
                                              </asp:DataGrid>
                                              <br />
                                              <asp:Table runat="server" ID="tbDLSDisb" CellPadding="0" CellSpacing="0" CssClass="datagridheader"
                                                  Width="100%">
                                                  <asp:TableHeaderRow>
                                                      <asp:TableHeaderCell CssClass="tableheader" HorizontalAlign="Center">
                                                          <asp:Label ID="Label2" runat="server" CssClass="labelbold">DL Scheduled Disbursements</asp:Label>
                                                      </asp:TableHeaderCell>
                                                  </asp:TableHeaderRow>
                                              </asp:Table>
                                              <asp:DataGrid ID="dgDLSDisb" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                                  BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal"
                                                  Width="100%" runat="server">
                                                  <EditItemStyle Wrap="False"></EditItemStyle>
                                                  <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                  <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                  <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                  <Columns>
                                                      <asp:TemplateColumn HeaderText="IndexID">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblIndexID" Text='<%# Container.DataItem("IndexID") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Last Name">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblLastName" Text='<%# Container.DataItem("LastName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="First Name">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblFirstName" Text='<%# Container.DataItem("FirstName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="SSN">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblSSN" runat="Server" Text='<%# Container.DataItem("SSN") %>' CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Campus">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblCampus" runat="Server" Text='<%# Container.DataItem("Campus") %>'
                                                                  CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Loan Id">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblLoanId" Text='<%# Container.DataItem("LoanId") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Disbursment<br/>Date">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblDisbDate" Text='<%# Container.DataItem("DisbDate") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="MPN Status" Visible="false">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblMPNStatus" Text='<%# Container.DataItem("MPNStatus") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Disbursement<br/>Number">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblDisbNo" Text='<%# Container.DataItem("DisbNum") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Gross<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblGrossAmount" Text='<%# Container.DataItem("GrossAmount") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Fees">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblFees" Text='<%# Container.DataItem("Fees") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Net<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblNetAmount" Text='<%# Container.DataItem("NetAmount") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Reason">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblErrorMsg" Text='<%# Container.DataItem("ErrorMsg") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                  </Columns>
                                              </asp:DataGrid>
                                              <br />
                                              <asp:Table runat="server" ID="tbDLADisb" CellPadding="0" CellSpacing="0" CssClass="datagridheader"
                                                  Width="100%">
                                                  <asp:TableHeaderRow>
                                                      <asp:TableHeaderCell CssClass="tableheader" HorizontalAlign="Center">
                                                          <asp:Label ID="Label3" runat="server" CssClass="labelbold">DL Actual Disbursements</asp:Label>
                                                      </asp:TableHeaderCell>
                                                  </asp:TableHeaderRow>
                                              </asp:Table>
                                              <asp:DataGrid ID="dgDLADisb" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                                  BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" GridLines="Horizontal"
                                                  Width="100%" runat="server">
                                                  <EditItemStyle Wrap="False"></EditItemStyle>
                                                  <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                  <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                  <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                  <Columns>
                                                      <asp:TemplateColumn HeaderText="IndexID">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblIndexID" Text='<%# Container.DataItem("IndexID") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Last Name">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblLastName" Text='<%# Container.DataItem("LastName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="First Name">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblFirstName" Text='<%# Container.DataItem("FirstName") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="SSN">
                                                          <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblSSN" runat="Server" Text='<%# Container.DataItem("SSN") %>' CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Campus">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblCampus" runat="Server" Text='<%# Container.DataItem("Campus") %>'
                                                                  CssClass="label"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Loan Id">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblLoanId" Text='<%# Container.DataItem("LoanId") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Disbursement<br/>Date">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblDisbDate" Text='<%# Container.DataItem("DisbDate") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Disbursement<br/>Status">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblDisbStatus" Text='<%# Container.DataItem("DisbStatus") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Disbursement<br/>Number">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblDisbNo" Text='<%# Container.DataItem("DisbNum") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Gross<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblGrossAmount" Text='<%# Container.DataItem("GrossAmount") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Fees">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblFees" Text='<%# Container.DataItem("Fees") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Net<br/>Amount($)">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label runat="server" ID="lblNetAmount" Text='<%# Container.DataItem("NetAmount") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                      <asp:TemplateColumn HeaderText="Reason">
                                                          <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                          <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                          <ItemTemplate>
                                                              <asp:Label ID="lblErrorMsg" Text='<%# Container.DataItem("ErrorMsg") %>' CssClass="label"
                                                                  runat="server">
                                                              </asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateColumn>
                                                  </Columns>
                                              </asp:DataGrid>
                                          </div>

                                          <br />
                                          <asp:TextBox ID="txtLog" runat="server" Rows="10" Columns="6" Height="67px" Width="539px"
                                              Visible="false"></asp:TextBox>
                                          <asp:TextBox ID="txtFileName" runat="Server" Visible="false"></asp:TextBox>
                                          <asp:ListBox ID="lstMessages" runat="Server" Visible="false"></asp:ListBox>
                                          <asp:Button ID="btnTransferDB" runat="server" Text="Transfer Data" 
                                          Visible="false" />
                                     </div>	
								</td>
							</tr>
						</table>
					</td>
					<!-- end rightcolumn --></TR>
			</table>
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

