﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ImportEDExpressFile.aspx.vb" Inherits="FameLink_ImportEDExpressFile" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
 
 <script language="javascript" type="text/javascript">
     function msgconfirm(ar, dr, AA, SA) {
         var at = ar.value;
         var dt = dr.value;
         var AwAmt = AA.value;
         var SchAmt = SA.value;
         var msg;
         if (at == "0" && dt == "0") {
             msg = "Are you sure you want to process?";
         } else {
             msg = "Are you sure you want to process?\n" + at + "\n" + dt + "\n" + AwAmt + "\n" + SchAmt;
         }
         var answer = confirm(msg);
         if (answer) {
             return true;
         }
         else {
             return false;
         }
     }
    </script>
    <style type="text/css">
        h2 { font: bold 11px verdana; color: #000066; background-color: transparent; width: auto; margin: 0; padding: 0; border: 0;text-align: left;}
        .FileInput
        {
	        margin: 4px;
	        vertical-align: middle;
	        font: normal 10px verdana;
	        text-align:left;
	        color: #000066;
	        width: 300px;
        }
        .ProcessFile
        {
	        margin: 4px;
	        vertical-align: middle;
	        font: normal 10px verdana;
	        text-align:center;
	        color: #000066;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="DetailsFrameTop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="MenuFrame" align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" CausesValidation="false"
                                        Enabled="false"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"
                                        Enabled="false"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"
                                        Enabled="false"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <!--begin content here-->
                                       
                                        <telerik:RadAjaxManagerProxy ID="ramPellAward" runat="server">
                                            <AjaxSettings>
                                                <telerik:AjaxSetting AjaxControlID="ddlFileNames">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="ddlFileNames" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                                <telerik:AjaxSetting AjaxControlID="ddlAcademicYear">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="ddlAcademicYear" />
                                                        <telerik:AjaxUpdatedControl ControlID="rapASD" />
                                                        <telerik:AjaxUpdatedControl ControlID="rapAED" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                                <telerik:AjaxSetting AjaxControlID="chkPostPayment">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                        <telerik:AjaxUpdatedControl ControlID="chkPostPayment" />
                                                        <telerik:AjaxUpdatedControl ControlID="chkPOSS" />
                                                        <telerik:AjaxUpdatedControl ControlID="rdoExpectedDate" />
                                                        <telerik:AjaxUpdatedControl ControlID="rdoSpecifyDate" />
                                                        <telerik:AjaxUpdatedControl ControlID="rapPD" />
                                                        <telerik:AjaxUpdatedControl ControlID="txtRefRecid" />
                                                        <telerik:AjaxUpdatedControl ControlID="ddlPaymentType" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                                <telerik:AjaxSetting AjaxControlID="rdoExpectedDate">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                        <telerik:AjaxUpdatedControl ControlID="rdoExpectedDate" />
                                                        <telerik:AjaxUpdatedControl ControlID="rdoSpecifyDate" />
                                                        <telerik:AjaxUpdatedControl ControlID="rapPD" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                                <telerik:AjaxSetting AjaxControlID="rdoSpecifyDate">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                        <telerik:AjaxUpdatedControl ControlID="rdoSpecifyDate" />
                                                        <telerik:AjaxUpdatedControl ControlID="rdoExpectedDate" />
                                                        <telerik:AjaxUpdatedControl ControlID="rapPD" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                                <telerik:AjaxSetting AjaxControlID="rgPellAward">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                        <telerik:AjaxUpdatedControl ControlID="rgPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellHeader" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellRecords" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellException" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                                <telerik:AjaxSetting AjaxControlID="rgDirectLoan">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                        <telerik:AjaxUpdatedControl ControlID="rgPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellHeader" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellRecords" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellException" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                                <telerik:AjaxSetting AjaxControlID="btnPreview">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                        <telerik:AjaxUpdatedControl ControlID="pnlBtn" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellHeader" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellRecords" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellException" />
                                                        <telerik:AjaxUpdatedControl ControlID="rgPellAward" LoadingPanelID="ralpPellAward" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                                <telerik:AjaxSetting AjaxControlID="btnProcessData">
                                                    <UpdatedControls>
                                                        <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="ralpPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblAwardYear" />
                                                        <telerik:AjaxUpdatedControl ControlID="pnlBtn" />
                                                        <telerik:AjaxUpdatedControl ControlID="rgPellAward" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellRecords" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellHeader" />
                                                        <telerik:AjaxUpdatedControl ControlID="tblPellException" />
                                                    </UpdatedControls>
                                                </telerik:AjaxSetting>
                                            </AjaxSettings>
                                        </telerik:RadAjaxManagerProxy>
                                        <telerik:RadAjaxLoadingPanel ID="ralpPellAward" runat="server">
                                        </telerik:RadAjaxLoadingPanel>
                                        <div>
                                            <asp:Panel ID="pnlMain" runat="server">
                                                <asp:HiddenField ID="hfAS" runat="server" Value="0" />
                                                <asp:HiddenField ID="hfAD" runat="server" Value="0" />
                                                <asp:HiddenField ID="hfAA" runat="server" Value="0" />
                                                <asp:HiddenField ID="hfSA" runat="server" Value="0" />
                                                <asp:Table runat="server" ID="tbSelectFile" Width="100%">
                                                    <asp:TableRow>
                                                        <asp:TableCell Width="25%" HorizontalAlign="Left">
                                                            <asp:Label ID="lblFileName" Text="Select EDExpress File to Import" runat="Server"
                                                                CssClass="Label" Width="277px"></asp:Label>
                                                        </asp:TableCell>
                                                        <asp:TableCell Width="50%" HorizontalAlign="Left">
                                                            <asp:DropDownList ID="ddlFileNames" Width="600px" runat="server" CssClass="drowdownlists" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </asp:TableCell>
                                                        <asp:TableCell Width="50%">&nbsp;
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow Visible="false">
                                                        <asp:TableCell ColumnSpan="5" HorizontalAlign="Center">
                                                            <asp:Label ID="Label1" Text="IMPORT OPTIONS" runat="Server" CssClass="LabelBold"></asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="5" HorizontalAlign="Center">
                                                            <asp:Table ID="tblAwardYear" runat="server" Width="100%">
                                                                <asp:TableRow>
                                                                    <asp:TableCell HorizontalAlign="Left" Width="25%">
                                                                        <asp:Label ID="Label7" Text="Select Award Year for awards not in Advantage" runat="Server"
                                                                            CssClass="Label" Width="275px"></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell HorizontalAlign="Left" Width="50%">
                                                                        <asp:DropDownList ID="ddlAcademicYear" Width="600px" runat="server" CssClass="dropdownlists" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell HorizontalAlign="right" Width="25%">
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                                <asp:TableRow>
                                                                    <asp:TableCell HorizontalAlign="Left" Width="25%">
                                                                        <asp:Label ID="Label4" Text="Award Start Date" runat="Server" CssClass="Label"></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell HorizontalAlign="Left" Width="50%">
                                                                        <telerik:RadAjaxPanel ID="rapASD" runat="server">
                                                                            <%--<asp:TextBox ID="txtAwardStartDate" CssClass="TextBoxDate" runat="server" Width="75px"></asp:TextBox>&nbsp;<asp:HyperLink
                                                                                ID="hlAwardStartDate" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink>--%>
                                                                            <telerik:RadDatePicker ID="txtAwardStartDate" MinDate="1/1/1945" runat="server"></telerik:RadDatePicker>
                                                                        </telerik:RadAjaxPanel>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell HorizontalAlign="right" Width="25%">
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                                <asp:TableRow>
                                                                    <asp:TableCell HorizontalAlign="Left" Width="25%">
                                                                        <asp:Label ID="Label5" Text="Award End Date" runat="Server" CssClass="Label"></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell HorizontalAlign="Left" Width="50%">
                                                                        <telerik:RadAjaxPanel ID="rapAED" runat="server">
                                                                         <%--   <asp:TextBox ID="txtAwardEndDate" CssClass="TextBoxDate" runat="server" Width="75px"></asp:TextBox>&nbsp;<asp:HyperLink
                                                                                ID="hlAwardEndDate" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink>--%>
                                                                         <telerik:RadDatePicker ID="txtAwardEndDate" MinDate="1/1/1945" runat="server"></telerik:RadDatePicker>
                                                                        </telerik:RadAjaxPanel>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell HorizontalAlign="right" Width="25%">
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Width="100%">
                                                            <telerik:RadPanelBar runat="server" ID="rpbAdvanceOption" Width="100%">
                                                                <Items>
                                                                    <telerik:RadPanelItem Expanded="false" Text="Advanced Options" runat="server">
                                                                        <Items>
                                                                            <telerik:RadPanelItem Value="AdvanceOption" runat="server">
                                                                                <ItemTemplate>
                                                                                    <asp:Table runat="server" ID="tbSelectFile" Width="100%" CellPadding="0" CellSpacing="0">
                                                                                        <asp:TableRow>
                                                                                            <asp:TableCell Width="20%" HorizontalAlign="Left" VerticalAlign="Top">
                                                                                                <asp:Label ID="lblMatchingRecords" Text="Matching Records&nbsp" runat="Server" CssClass="Label"
                                                                                                    Width="275px"></asp:Label>
                                                                                            </asp:TableCell>
                                                                                            <asp:TableCell HorizontalAlign="left" Width="50%">
                                                                                                <asp:CheckBox ID="chkOADWED" runat="server" Text="Override Advantage date with EDExpress date"
                                                                                                    CssClass="label" Checked="true"></asp:CheckBox><br>
                                                                                                <asp:CheckBox ID="chkOAAWEA" runat="server" Text="Override Advantage amount with EDExpress amount"
                                                                                                    CssClass="label" Checked="true"></asp:CheckBox><br>
                                                                                            </asp:TableCell>
                                                                                            <asp:TableCell Width="25%">&nbsp;
                                                                                            </asp:TableCell>
                                                                                        </asp:TableRow>
                                                                                        <asp:TableRow>
                                                                                            <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top" Width="20%">
                                                                                                <asp:Label ID="Label2" Text="Payment Options" runat="Server" CssClass="Label"></asp:Label>
                                                                                            </asp:TableCell>
                                                                                            <asp:TableCell HorizontalAlign="left" VerticalAlign="Middle" Width="50%">
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td colspan="2">
                                                                                                            <asp:CheckBox ID="chkPostPayment" runat="server" Text="Post Payment" CssClass="label"
                                                                                                                AutoPostBack="true" OnCheckedChanged="chkPostPayment_CheckedChanged" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="20px">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkPOSS" runat="server" Text="Pay out-of-school students" CssClass="Label"
                                                                                                                Enabled="false"></asp:CheckBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="20px">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblPostDate" Text="Post Date" runat="Server" CssClass="Label"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="20px">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td colspan="2">
                                                                                                                        <asp:RadioButton ID="rdoExpectedDate" AutoPostBack="true" runat="server" CssClass="Label"
                                                                                                                            Checked="true" Text="Use Expected Date" GroupName="Date" Enabled="false" OnCheckedChanged="rdoExpectedDate_CheckedChanged" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td width="100px">
                                                                                                                        <asp:RadioButton ID="rdoSpecifyDate" AutoPostBack="true" runat="server" CssClass="Label"
                                                                                                                            Text="Specify Date" GroupName="Date" Width="100px" Enabled="false" OnCheckedChanged="rdoSpecifyDate_CheckedChanged" />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Panel runat="server" ID="rapPD" Enabled="false">
                                                                                                                            <asp:TextBox ID="txtPostDate" CssClass="TextBoxDate" runat="server" Width="75px"></asp:TextBox>&nbsp;<asp:HyperLink
                                                                                                                                ID="hlPostDate" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink>
                                                                                                                        </asp:Panel>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <%--<asp:RadioButtonList id="rdolstPaymentOptions" runat="server" AutoPostBack="true"  CssClass="Label" RepeatDirection="Vertical">
	<asp:ListItem Value="False" Selected="True">Do not post payments</asp:ListItem>
	<asp:ListItem Value="True">Post payments</asp:ListItem>
	    </asp:RadioButtonList>--%>
                                                                                            </asp:TableCell>
                                                                                            <asp:TableCell HorizontalAlign="right" Width="25%">
                                                                                            </asp:TableCell>
                                                                                        </asp:TableRow>
                                                                                        <asp:TableRow>
                                                                                            <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top" Width="20%">
    &nbsp;
                                                                                            </asp:TableCell>
                                                                                            <asp:TableCell HorizontalAlign="left" VerticalAlign="Middle" Width="50%">
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td width="20px">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblRefRecid" Text="Reference/Recid" runat="Server" CssClass="Label"
                                                                                                                            Width="100px"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtRefRecid" runat="server" CssClass="TextBox" Width="250px"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="20px">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblPaymentType" Text="Payment Type" runat="Server" CssClass="Label"
                                                                                                                            Width="100px"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="DropDownLists" Width="250px">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </asp:TableCell>
                                                                                            <asp:TableCell HorizontalAlign="right" Width="25%">
                                                                                            </asp:TableCell>
                                                                                        </asp:TableRow>
                                                                                    </asp:Table>
                                                                                </ItemTemplate>
                                                                            </telerik:RadPanelItem>
                                                                        </Items>
                                                                    </telerik:RadPanelItem>
                                                                </Items>
                                                            </telerik:RadPanelBar>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Width="100%" ColumnSpan="4" HorizontalAlign="Center">
                                                            <asp:Panel ID="pnlBtn" runat="server">
                                                                <asp:Button ID="btnPreview" runat="Server" Text="Preview File"
                                                                    Width="100px" />&nbsp;&nbsp;<input type="submit" runat="server" id="btnProcessData"
                                                                        value="Import File" style="width: 100px;" />
                                                            </asp:Panel>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                                <br />
                                                <asp:Table ID="tblPellRecords" runat="server" CssClass="DataGridHeader" Width="100%">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <table border="0" runat="server" id="tblPellHeader" cellpadding="0" cellspacing="0"
                                                                width="100%">
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblMsgHeadPell" runat="server" CssClass="LabelBold">Pell, SMART, ACG and TEACH Awards</asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblAwardCount" runat="server" CssClass="LabelBold"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblDisbCount" runat="server" CssClass="LabelBold"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                                                            <telerik:RadGrid ID="rgPellAward" runat="server" Width="100%" ShowStatusBar="true"
                                                                AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True"
                                                                AllowMultiRowSelection="False" GroupingSettings-CaseSensitive="false" AllowPaging="True"
                                                                OnDetailTableDataBind="rgPellAward_DetailTableDataBind"  OnNeedDataSource="rgPellAward_NeedDataSource">
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <MasterTableView Width="100%" Name="PellAward" DataKeyNames="IndexID" AllowMultiColumnSorting="false">
                                                                    <DetailTables>
                                                                        <telerik:GridTableView DataKeyNames="IndexID" Name="PellDisb" Width="100%">
                                                                            <Columns>
                                                                                <telerik:GridBoundColumn SortExpression="IndexID" HeaderText="IndexID" HeaderButtonType="TextButton"
                                                                                    DataField="IndexID">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbDate"
                                                                                    HeaderText="Disb Date" HeaderButtonType="TextButton" DataField="DisbDate" UniqueName="DisbDate">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbRelInd"
                                                                                    HeaderText="Release Indicator" HeaderButtonType="TextButton" DataField="DisbRelInd"
                                                                                    UniqueName="DisbRelInd">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="ActionStatus"
                                                                                    HeaderText="Action Status" HeaderButtonType="TextButton" DataField="ActionStatus"
                                                                                    UniqueName="ActionStatus">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbNum"
                                                                                    HeaderText="Disb Number" HeaderButtonType="TextButton" DataField="DisbNum" UniqueName="DisbNum">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="SeqNum"
                                                                                    HeaderText="Disb Sequence" HeaderButtonType="TextButton" DataField="SeqNum" UniqueName="SeqNum">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="SubDisbAmt"
                                                                                    HeaderText="Submitted Amount($)" HeaderButtonType="TextButton" DataField="SubDisbAmt"
                                                                                    UniqueName="SubDisbAmt">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="AccDisbAmt"
                                                                                    HeaderText="Accepted Amount($)" HeaderButtonType="TextButton" DataField="AccDisbAmt"
                                                                                    UniqueName="AccDisbAmt">
                                                                                </telerik:GridBoundColumn>
                                                                            </Columns>
                                                                        </telerik:GridTableView>
                                                                    </DetailTables>
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn SortExpression="IndexID" HeaderText="IndexID" HeaderButtonType="TextButton"
                                                                            DataField="IndexID">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="LastName" HeaderText="Last Name"
                                                                            HeaderButtonType="TextButton" DataField="LastName">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="FirstName" HeaderText="First Name"
                                                                            HeaderButtonType="TextButton" DataField="FirstName">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="SSN" HeaderText="SSN"
                                                                            HeaderButtonType="TextButton" DataField="SSN">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="Campus" HeaderText="Campus"
                                                                            HeaderButtonType="TextButton" DataField="Campus">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Grant Type"
                                                                            HeaderButtonType="TextButton" DataField="GrantTypeDesc">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Award Id"
                                                                            HeaderButtonType="TextButton" DataField="AwardId">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Add Date"
                                                                            HeaderButtonType="TextButton" DataField="AddDate">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Add Time"
                                                                            HeaderButtonType="TextButton" DataField="AddTime">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Award Amount($)"
                                                                            HeaderButtonType="TextButton" DataField="AwardAmount">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn AllowFiltering="false" DataField="AcademicYearId" HeaderText="Award Year">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlAcademicYear" runat="server" Width="150px" DataSource="<%#GetAcademicYear() %>"
                                                                                    DataTextField="AcademicYearDescrip" DataValueField="AcademicYearId" CssClass="DropDownLists"
                                                                                    SelectedValue='<%# Container.DataItem("AcademicYearId") %>'>
                                                                                </asp:DropDownList>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn AllowFiltering="false" DataField="AwardStartDate" HeaderText="Award Start Date"
                                                                            ItemStyle-Width="150px">
                                                                            <ItemTemplate>
                                                                                <table runat="server" id="tblAYSD" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr valign="middle">
                                                                                        <td style="border: 0; padding: 0;">
                                                                                            <asp:TextBox ID="txtAYSD" runat="server" CssClass="TextBoxDate" Width="75px" Text='<%# Container.DataItem("AwardStartDate") %>'></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="border: 0; padding: 0;">
                                                                                            <asp:HyperLink ID="hlAYSD" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn AllowFiltering="false" DataField="AwardEndDate" HeaderText="Award End Date">
                                                                            <ItemTemplate>
                                                                                <table runat="server" id="tblAYED" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td style="border: 0; padding: 0;">
                                                                                            <asp:TextBox ID="txtAYED" runat="server" CssClass="TextBoxDate" Width="75px" Text='<%# Container.DataItem("AwardEndDate") %>'></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="border: 0; padding: 0;">
                                                                                            <asp:HyperLink ID="hlAYED" runat="server" ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif"></asp:HyperLink>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                                <br />
                                                <asp:Table ID="tblDLRecords" runat="server" CssClass="DataGridHeader" Width="100%">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <table border="0" runat="server" id="tblDLHeader" cellpadding="0" cellspacing="0"
                                                                width="100%">
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblMsgHeadDL" runat="server" CssClass="LabelBold">Direct Loans</asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblDLAcademicYear" runat="server" CssClass="LabelBold"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblDLCount" runat="server" CssClass="LabelBold"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblDLSDCount" runat="server" CssClass="LabelBold"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblDLADCount" runat="server" CssClass="LabelBold"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                                                            <telerik:RadGrid ID="rgDirectLoan" runat="server" Width="100%" ShowStatusBar="true"
                                                                AllowFilteringByColumn="true" AutoGenerateColumns="False" PageSize="25" AllowSorting="True"
                                                                AllowMultiRowSelection="False" AllowPaging="True" OnItemCommand="rgDirectLoan_ItemCommand"
                                                                 OnNeedDataSource="rgDirectLoan_NeedDataSource">
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <MasterTableView Width="100%" Name="DirectLoan" DataKeyNames="IndexID" AllowMultiColumnSorting="True">
                                                                    <NestedViewTemplate>
                                                                        <asp:Panel runat="server" ID="ICDetailRows" Visible="true">
                                                                            <asp:Table ID="tblDLActDisb" runat="server" CssClass="DataGridHeader" Width="100%">
                                                                                <asp:TableRow>
                                                                                    <asp:TableCell>
                                                                                        <table border="0" runat="server" id="tblActDisbHeader" cellpadding="0" cellspacing="0"
                                                                                            width="100%">
                                                                                            <tr>
                                                                                                <td align="center">
                                                                                                    <asp:Label ID="lblDLActDisb" runat="server" CssClass="LabelBold">Actual Disbursements</asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:TableCell>
                                                                                </asp:TableRow>
                                                                                <asp:TableRow>
                                                                                    <asp:TableCell>
                                                                                        <telerik:RadGrid ID="rgActDisb" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                             Width="100%">
                                                                                            <MasterTableView DataKeyNames="IndexID" Name="DLActDisb" Width="100%">
                                                                                                <Columns>
                                                                                                    <telerik:GridBoundColumn SortExpression="IndexID" HeaderText="IndexID" HeaderButtonType="TextButton"
                                                                                                        DataField="IndexID" Visible="false">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbDate"
                                                                                                        HeaderText="Disb Date" HeaderButtonType="TextButton" DataField="DisbDate" UniqueName="DisbDate">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbRelInd"
                                                                                                        HeaderText="Release Indicator" HeaderButtonType="TextButton" DataField="DisbRelInd"
                                                                                                        UniqueName="DisbRelInd">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbNum"
                                                                                                        HeaderText="Disb Number" HeaderButtonType="TextButton" DataField="DisbNum" UniqueName="DisbNum">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="SeqNum"
                                                                                                        HeaderText="Disb Sequence" HeaderButtonType="TextButton" DataField="SeqNum" UniqueName="SeqNum">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="GrossAmt"
                                                                                                        HeaderText="Gross Amount($)" HeaderButtonType="TextButton" DataField="GrossAmt"
                                                                                                        UniqueName="GrossAmt">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="Fees"
                                                                                                        HeaderText="Fees" HeaderButtonType="TextButton" DataField="Fees" UniqueName="GrossAmt">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="AccDisbAmt"
                                                                                                        HeaderText="Net Amount($)" HeaderButtonType="TextButton" DataField="NetAmt" UniqueName="NetAmt">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                </Columns>
                                                                                            </MasterTableView>
                                                                                        </telerik:RadGrid>
                                                                                    </asp:TableCell>
                                                                                </asp:TableRow>
                                                                            </asp:Table>
                                                                            <asp:Table ID="tblDLSubDisb" runat="server" CssClass="DataGridHeader" Width="100%">
                                                                                <asp:TableRow>
                                                                                    <asp:TableCell>
                                                                                        <table border="0" runat="server" id="tblSubDisbHeader" cellpadding="0" cellspacing="0"
                                                                                            width="100%">
                                                                                            <tr>
                                                                                                <td align="center">
                                                                                                    <asp:Label ID="lblDLSubDisb" runat="server" CssClass="LabelBold">Anticipated Disbursements</asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:TableCell>
                                                                                </asp:TableRow>
                                                                                <asp:TableRow>
                                                                                    <asp:TableCell>
                                                                                        <telerik:RadGrid ID="rgSubDisb" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                             Width="100%">
                                                                                            <MasterTableView DataKeyNames="IndexID" Name="DLActDisb" Width="100%">
                                                                                                <Columns>
                                                                                                    <telerik:GridBoundColumn SortExpression="IndexID" HeaderText="IndexID" HeaderButtonType="TextButton"
                                                                                                        DataField="IndexID" Visible="false">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbDate"
                                                                                                        HeaderText="Disb Date" HeaderButtonType="TextButton" DataField="DisbDate" UniqueName="DisbDate">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbRelInd"
                                                                                                        HeaderText="Release Indicator" HeaderButtonType="TextButton" DataField="DisbRelInd"
                                                                                                        UniqueName="DisbRelInd">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="DisbNum"
                                                                                                        HeaderText="Disb Number" HeaderButtonType="TextButton" DataField="DisbNum" UniqueName="DisbNum">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="SeqNum"
                                                                                                        HeaderText="Disb Sequence" HeaderButtonType="TextButton" DataField="SeqNum" UniqueName="SeqNum">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="GrossAmt"
                                                                                                        HeaderText="Gross Amount($)" HeaderButtonType="TextButton" DataField="GrossAmt"
                                                                                                        UniqueName="GrossAmt">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="Fees"
                                                                                                        HeaderText="Fees" HeaderButtonType="TextButton" DataField="Fees" UniqueName="Fees">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridBoundColumn AllowSorting="false" AllowFiltering="false" SortExpression="NetAmt"
                                                                                                        HeaderText="Net Amount($)" HeaderButtonType="TextButton" DataField="NetAmt" UniqueName="NetAmt">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                </Columns>
                                                                                            </MasterTableView>
                                                                                        </telerik:RadGrid>
                                                                                    </asp:TableCell>
                                                                                </asp:TableRow>
                                                                            </asp:Table>
                                                                        </asp:Panel>
                                                                    </NestedViewTemplate>
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn SortExpression="IndexID" HeaderText="IndexID" HeaderButtonType="TextButton"
                                                                            DataField="IndexID">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="LastName" HeaderText="Last Name"
                                                                            HeaderButtonType="TextButton" DataField="LastName">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="FirstName" HeaderText="First Name"
                                                                            HeaderButtonType="TextButton" DataField="FirstName">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="SSN" HeaderText="SSN"
                                                                            HeaderButtonType="TextButton" DataField="SSN">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="Campus" HeaderText="Campus"
                                                                            HeaderButtonType="TextButton" DataField="Campus">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Loan Type"
                                                                            HeaderButtonType="TextButton" DataField="LoanTypeDesc">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Loan Id"
                                                                            HeaderButtonType="TextButton" DataField="LoanId">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Add Date"
                                                                            HeaderButtonType="TextButton" DataField="AddDate">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Add Time"
                                                                            HeaderButtonType="TextButton" DataField="AddTime">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Gross Amount($)"
                                                                            HeaderButtonType="TextButton" DataField="GrossAmount">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Fees"
                                                                            HeaderButtonType="TextButton" DataField="Fees">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Net Amount($)"
                                                                            HeaderButtonType="TextButton" DataField="NetAmount">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Loan Start Date"
                                                                            HeaderButtonType="TextButton" DataField="LoanStartDate">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowFiltering="false" AllowSorting="false" HeaderText="Loan End Date"
                                                                            HeaderButtonType="TextButton" DataField="LoanEndDate">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                                <br />
                                                <asp:Table ID="tblPellException" runat="server" CssClass="DataGridHeader" Width="100%">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:Label ID="lblExpReport" runat="server" CssClass="LabelBold" Text="List of data not posted successfully"></asp:Label></asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:Label ID="lblExpParent" runat="server" CssClass="LabelBold" Text=""></asp:Label></asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="trPellException">
                                                        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                                                            <telerik:RadGrid ID="rgPellException" runat="server" Width="100%" ShowStatusBar="true"
                                                                AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False"
                                                                AllowPaging="True" OnDetailTableDataBind="rgPellException_DetailTableDataBind"
                                                                 OnNeedDataSource="rgPellException_NeedDataSource">
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <MasterTableView Width="100%" Name="PellException" DataKeyNames="ExceptionReportId"
                                                                    AllowMultiColumnSorting="false">
                                                                    <DetailTables>
                                                                        <telerik:GridTableView DataKeyNames="ExceptionReportId" Name="PellDisbExp" Width="100%">
                                                                            <Columns>
                                                                                <telerik:GridBoundColumn SortExpression="DisbDate" HeaderText="Disbursement Date"
                                                                                    HeaderButtonType="TextButton" DataField="DisbDate" UniqueName="DisbDate">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" HeaderText="Accepted Disbursement Amount($)"
                                                                                    HeaderButtonType="TextButton" DataField="AccDisbAmtDisbGrossAmt" UniqueName="AccDisbAmtDisbGrossAmt">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" HeaderText="Submitted Disbursement Amount($)"
                                                                                    HeaderButtonType="TextButton" DataField="SubDisbAmtDisbNetAmt" UniqueName="SubDisbAmtDisbNetAmt">
                                                                                </telerik:GridBoundColumn>
                                                                            </Columns>
                                                                        </telerik:GridTableView>
                                                                    </DetailTables>
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="ExceptionReportId" HeaderButtonType="TextButton"
                                                                            DataField="ExceptionReportId">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn SortExpression="SSN" HeaderText="SSN" HeaderButtonType="TextButton"
                                                                            DataField="SSN">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="Amount($)" HeaderButtonType="TextButton"
                                                                            DataField="Amount">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="Award Id" HeaderButtonType="TextButton"
                                                                            DataField="AwardIdLoanId">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="Grant Type" HeaderButtonType="TextButton"
                                                                            DataField="GrantTypeLoanType">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="File" HeaderButtonType="TextButton"
                                                                            DataField="FileName">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="Reason" HeaderButtonType="TextButton"
                                                                            DataField="Message">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn DataField="ModDate" HeaderText="Imported Date">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblImportedDate" Text='<%# DataBinder.Eval(Container.DataItem,"ModDate", "{0:d}") %>'
                                                                                    CssClass="Label" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                            <telerik:RadGrid ID="rgDLException" runat="server" Width="100%" ShowStatusBar="true"
                                                                AutoGenerateColumns="False" PageSize="25" AllowSorting="True" AllowMultiRowSelection="False"
                                                                AllowPaging="True" OnDetailTableDataBind="rgDLException_DetailTableDataBind"
                                                                 OnNeedDataSource="rgDLException_NeedDataSource">
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <MasterTableView Width="100%" Name="DLException" DataKeyNames="ExceptionReportId"
                                                                    AllowMultiColumnSorting="false">
                                                                    <DetailTables>
                                                                        <telerik:GridTableView DataKeyNames="ExceptionReportId" Name="DLDisbExp" Width="100%">
                                                                            <Columns>
                                                                                <telerik:GridBoundColumn SortExpression="ExceptionReportId" HeaderText="ExceptionReportId"
                                                                                    HeaderButtonType="TextButton" DataField="ExceptionReportId" UniqueName="ExceptionReportId">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn SortExpression="DisbType" HeaderText="Disbursement Type"
                                                                                    HeaderButtonType="TextButton" DataField="DisbType" UniqueName="DisbType">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn SortExpression="DisbDate" HeaderText="Disbursement Date"
                                                                                    HeaderButtonType="TextButton" DataField="DisbDate" UniqueName="DisbDate">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" HeaderText="Disbursement Gross Amount($)"
                                                                                    HeaderButtonType="TextButton" DataField="AccDisbAmtDisbGrossAmt" UniqueName="AccDisbAmtDisbGrossAmt">
                                                                                </telerik:GridBoundColumn>
                                                                                <telerik:GridBoundColumn AllowSorting="false" HeaderText="Disbursement Net Amount($)"
                                                                                    HeaderButtonType="TextButton" DataField="SubDisbAmtDisbNetAmt" UniqueName="SubDisbAmtDisbNetAmt">
                                                                                </telerik:GridBoundColumn>
                                                                            </Columns>
                                                                        </telerik:GridTableView>
                                                                    </DetailTables>
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="ExceptionReportId" HeaderButtonType="TextButton"
                                                                            DataField="ExceptionReportId">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn SortExpression="SSN" HeaderText="SSN" HeaderButtonType="TextButton"
                                                                            DataField="SSN">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="Amount($)" HeaderButtonType="TextButton"
                                                                            DataField="Amount">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="Loan Id" HeaderButtonType="TextButton"
                                                                            DataField="AwardIdLoanId">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="Loan Type" HeaderButtonType="TextButton"
                                                                            DataField="GrantTypeLoanType">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="File" HeaderButtonType="TextButton"
                                                                            DataField="FileName">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn AllowSorting="false" HeaderText="Reason" HeaderButtonType="TextButton"
                                                                            DataField="Message">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn DataField="ModDate" HeaderText="Imported Date">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblImportedDate" Text='<%# DataBinder.Eval(Container.DataItem,"ModDate", "{0:d}") %>'
                                                                                    CssClass="Label" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <!--end content here-->
                                    <!-- end footer -->
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

