﻿Imports System
Imports System.Text
Imports Fame.AdvantageV1.DataAccess
Imports Telerik.Web.UI
Imports Fame.Advantage.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports Fame.AdvantageV1.Common
Imports System.Drawing
Imports BO = Advantage.Business.Objects
Imports System.Web.UI.WebControls 

Partial Class FameLink_ApproveEDExpressPendingPayments
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Private myEDFacade As New EDFacade_New
    Dim ds As New DataSet
    Private campusId As String
    Protected MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim m_Context As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ViewState("IsUserSa") = advantageUserState.IsUserSA

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        campusId = Master.CurrentCampusId
        If Not Page.IsPostBack Then
            ''btnExportToExcel.Attributes.Add("onclick", String.Format("realPostBack('{0}', ''); return false;", btnExportToExcel.UniqueID))
            hlPostDate.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtPostDate.ClientID + "',true,1945)"
            Dim hlPaymentExpFrom As HyperLink = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("hlPaymentExpFrom"), HyperLink)
            Dim hlPaymentExpTo As HyperLink = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("hlPaymentExpTo"), HyperLink)
            Dim txtPaymentExpFrom As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPaymentExpFrom"), TextBox)
            Dim txtPaymentExpTo As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPaymentExpTo"), TextBox)
            hlPaymentExpFrom.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtPaymentExpFrom.ClientID + "',true,1945)"
            hlPaymentExpTo.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtPaymentExpTo.ClientID + "',true,1945)"
            BuildFilesList()
            BuildAllList()
            ClearAll()
            btnProcess.Attributes.Add("onclick", "return msgconfirm(" + hfTR.ClientID + "," + hfTRP.ClientID + "," + hfTRR.ClientID + "," + hfAP.ClientID + "," + hfAR.ClientID + ");")
        End If
    End Sub

    Private Sub BuildFilesList()
        Dim ddlFileNames As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlFileNames"), DropDownList)
        Dim fn As New EDFacade_New
        Dim ds As New DataSet
        ds = fn.GetFileNames()
        ddlFileNames.Items.Clear()
        With ddlFileNames
            .DataValueField = "FileName"
            .DataTextField = "FileName"
            .DataSource = ds.Tables("FileNames").DefaultView
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub rdoSpecifyDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoSpecifyDate.CheckedChanged
        'txtPostDate.Enabled = True
        'hlPostDate.Enabled = True
        rapPD.Enabled = True
    End Sub

    Protected Sub rdoExpectedDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoExpectedDate.CheckedChanged
        txtPostDate.Text = String.Empty
        'txtPostDate.Enabled = False
        'hlPostDate.Enabled = False
        rapPD.Enabled = False
    End Sub
    Private Sub BuildAllList()
        BuildCampusListBox()
        BuildStatusListBox()
        BuildFundSourcesListBox()
        BuildPaymentTypesDDL()
        BuildLenderListBox()
    End Sub
    Private Sub BuildStatusListBox()
        Dim lstboxEnrollmentStatus As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxEnrollmentStatus"), ListBox)
        Dim statuses As New StatusCodeFacade
        With lstboxEnrollmentStatus
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"

            .DataSource = statuses.GetAllStatusCodesForStudents(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("All", Guid.Empty.ToString))
            '.SelectedIndex = 0
            .SelectionMode = ListSelectionMode.Single
        End With
    End Sub
    Private Sub BuildFundSourcesListBox()
        Dim lstboxFundSource As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxFundSource"), ListBox)
        Dim fundsources As New StudentsAccountsFacade
        With lstboxFundSource
            .DataTextField = "FundSourceDescrip"
            .DataValueField = "FundSourceId"

            .DataSource = fundsources.GetAllFundSources("True")
            .DataBind()
            .Items.Insert(0, New ListItem("All", Guid.Empty.ToString))
            '.SelectedIndex = 0
            .SelectionMode = ListSelectionMode.Single
        End With
    End Sub
    Private Sub BuildLenderListBox()
        Dim lstboxLender As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxLender"), ListBox)
        Dim db As New Fame.DataAccessLayer.DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("Select L.LenderId, L.LenderDescrip From faLenders L, syStatuses ST ")
            .Append(" WHERE L.StatusId = ST.StatusId ")
            .Append("AND    ST.Status = 'Active' ")
            .Append("ORDER BY L.LenderDescrip ")
        End With
        ds = db.RunSQLDataSet(sb.ToString, "Lenders")

        With lstboxLender
            .DataTextField = "LenderDescrip"
            .DataValueField = "LenderId"
            .DataSource = ds.Tables("Lenders").DefaultView
            .DataBind()
            .Items.Insert(0, New ListItem("All", Guid.Empty.ToString))
            '.SelectedIndex = 0
            .SelectionMode = ListSelectionMode.Single
        End With
    End Sub
    Private Sub BuildCampusListBox()
        Dim lstboxCampus As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxCampus"), ListBox)
        Dim userId As String
        userId = AdvantageSession.UserState.UserId.ToString
        Dim fac As New UserSecurityFacade
        Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)

        If isUserSa Then
            With lstboxCampus
                .DataTextField = "CampDescrip"
                .DataValueField = "CampusId"
                .DataSource = fac.GetAllCampuses()
                .DataBind()
                .Items.Insert(0, New ListItem("All", Guid.Empty.ToString))
                '.SelectedIndex = 0
                .SelectionMode = ListSelectionMode.Single
            End With
        Else
            With lstboxCampus
                .DataTextField = "CampDescrip"
                .DataValueField = "CampusId"
                .DataSource = fac.GetUserCampuses(userId)
                .DataBind()
                .Items.Insert(0, New ListItem("All", Guid.Empty.ToString))
                '.SelectedIndex = 0
                .SelectionMode = ListSelectionMode.Single
            End With
        End If
    End Sub
    Private Sub BuildPaymentTypesDDL()
        Dim ds As New DataSet
        Dim paymenttype As New EDFacade_New
        ds = paymenttype.GetPaymentTypes()
        ddlPaymentType.Items.Clear()
        With ddlPaymentType
            .DataValueField = "PaymentTypeId"
            .DataTextField = "Description"
            .DataSource = ds.Tables("PaymentTypes").DefaultView
            .DataBind()
        End With
        With ddlPaymentType
            .Items.Insert(0, New ListItem("Select", "0"))
            .SelectedIndex = 0
        End With
    End Sub
    Protected Sub rgPellAward_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rgPellAward.ItemDataBound

        If e.Item.ItemType = GridItemType.Header Then
            Dim chkAll As New CheckBox
            chkAll = CType(e.Item.FindControl("chkPayCheck"), CheckBox)
            Dim chk As New CheckBox
            If Not Session("HeaderCB") Is Nothing And Not Session("HeaderChecked") Is Nothing Then
                chk = DirectCast(Session("HeaderCB"), CheckBox)
                chkAll.Checked = chk.Checked
                Session("HeaderCB") = Nothing
                Session("HeaderChecked") = Nothing
            End If
        End If

        If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            If e.Item.OwnerTableView.Name = "PellAward" Then
                Dim OriginalSSN As String = ""
                OriginalSSN = e.Item.Cells(3).Text
                Dim StuEnrollmentId As String = ""
                StuEnrollmentId = e.Item.Cells(4).Text
                Dim ddl As DropDownList
                Dim txtDD As TextBox
                Dim hlDD As HyperLink
                Dim ckPay As CheckBox
                Dim ckRFL As CheckBox
                Dim txtP As TextBox


                ckPay = CType(e.Item.FindControl("chkPay"), CheckBox)
                ckRFL = CType(e.Item.FindControl("chkRemove"), CheckBox)
                txtP = CType(e.Item.FindControl("txtPaymentAmount"), TextBox)
                ckPay.Attributes.Add("onclick", "checkMutuallyExclusivePay(" + ckPay.ClientID + "," + ckRFL.ClientID + "," + hfAP.ClientID + "," + hfAR.ClientID + "," + txtP.ClientID + "," + hfTRP.ClientID + "," + hfTRR.ClientID + ")")
                ckRFL.Attributes.Add("onclick", "checkMutuallyExclusiveRemove(" + ckRFL.ClientID + "," + ckPay.ClientID + "," + hfAP.ClientID + "," + hfAR.ClientID + "," + txtP.ClientID + "," + hfTRP.ClientID + "," + hfTRR.ClientID + ")")

                ddl = CType(e.Item.FindControl("ddlStuEnrollment"), DropDownList)
                Dim dsPS As New DataSet
                dsPS = GetProgramStatus(OriginalSSN)
                With ddl
                    .DataTextField = "ProgramStatus"
                    .DataValueField = "StuEnrollId"
                    .DataSource = dsPS.Tables(0).DefaultView
                    .DataBind()
                End With
                ddl.SelectedValue = StuEnrollmentId
                If ddl.Items.Count > 1 Then
                    e.Item.BackColor = Color.LightPink
                End If
                txtDD = CType(e.Item.FindControl("txtDisbDate"), TextBox)
                hlDD = CType(e.Item.FindControl("hlDisbDate"), HyperLink)
                hlDD.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtDD.ClientID + "',true,1945)"
            End If
        End If
    End Sub
    Protected Sub rgPellAward_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles rgPellAward.NeedDataSource
        If Not e.IsFromDetailTable Then
            If Not Session("PendingPayments") Is Nothing Then
                Dim ds As DataSet = Session("PendingPayments")
                If Session("HeaderChecked") Is Nothing Then
                    Dim AwardScheduleId As String
                    For Each gi As GridDataItem In rgPellAward.Items
                        AwardScheduleId = gi.Cells(2).Text
                        Dim ddlPS As New DropDownList()
                        ddlPS = DirectCast(gi.FindControl("ddlStuEnrollment"), DropDownList)
                        Dim ckPay As New CheckBox()
                        ckPay = DirectCast(gi.FindControl("chkPay"), CheckBox)
                        Dim ckRFL As New CheckBox()
                        ckRFL = DirectCast(gi.FindControl("chkRemove"), CheckBox)
                        Dim tbDisbDate As New TextBox()
                        tbDisbDate = DirectCast(gi.FindControl("txtDisbDate"), TextBox)
                        Dim tbPayAmt As New TextBox()
                        tbPayAmt = DirectCast(gi.FindControl("txtPaymentAmount"), TextBox)
                        ''Dim drTemp() As DataRow = ds.Tables("PendingPayments").Select("AwardScheduleId='" + AwardScheduleId + "'")
                        Dim drTemp() As DataRow = ds.Tables("PendingPayments").Select("AwardScheduleId='" + AwardScheduleId + "'  And DisbNum=" + gi.Cells(14).Text + " And DusbSeqNum=" + gi.Cells(15).Text)
                        For Each dr As DataRow In drTemp
                            dr("StuEnrollmentId") = ddlPS.SelectedValue.ToString
                            dr("PaymentAmount") = tbPayAmt.Text
                            dr("DisbDate") = tbDisbDate.Text
                            dr("Pay") = ckPay.Checked
                            dr("RemoveFromList") = ckRFL.Checked
                        Next

                    Next
                    ds.Tables("PendingPayments").AcceptChanges()

                End If
                'rgPellAward.Columns(0).Visible = True
                rgPellAward.MasterTableView.DataSource = ds
                'rgPellAward.Columns(0).Visible = False
            End If
        End If
    End Sub
    Public Function GetAcademicYear() As DataSet
        Dim dsAY As DataSet = GetActiveAcademicYears()
        Dim dr As DataRow = dsAY.Tables(0).NewRow()
        dr("AcademicYearId") = "00000000-0000-0000-0000-000000000000"
        dr("AcademicYearDescrip") = "Select"
        dsAY.Tables(0).Rows.InsertAt(dr, 0)
        Return dsAY
    End Function
    Public Function GetActiveAcademicYears() As DataSet
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CCT.AcademicYearId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         CCT.AcademicYearCode, ")
            .Append("         CCT.AcademicYearDescrip ")
            .Append("FROM     saAcademicYears CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId AND ST.Status='Active' ")
            .Append("ORDER BY ST.Status,CCT.AcademicYearDescrip asc")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Protected Sub btnGetPendingPayments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetPendingPayments.Click
        Dim lstboxCampus As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxCampus"), ListBox)
        Dim lstboxEnrollmentStatus As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxEnrollmentStatus"), ListBox)
        Dim lstboxFundSource As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxFundSource"), ListBox)
        Dim lstboxLender As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxLender"), ListBox)
        Dim ddlFileNames As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlFileNames"), DropDownList)
        Dim txtPaymentExpFrom As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPaymentExpFrom"), TextBox)
        Dim txtPaymentExpTo As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPaymentExpTo"), TextBox)

        Dim filterCampus As String = ""
        Dim filterEnrollment As String = ""
        Dim filterFundSource As String = ""
        Dim filterLender As String = ""
        Dim filterExpDate As String = ""
        Dim filter As String = ""
        If lstboxCampus.SelectionMode = ListSelectionMode.Multiple Then
            For Each li As ListItem In lstboxCampus.Items
                If li.Selected = True Then
                    If filterCampus = "" Then
                        filterCampus = " ST.CampusId in ('" & li.Value & "'"
                        ''filterCampus = "'" & li.Value & "'"
                    Else
                        filterCampus = filterCampus & ",'" & li.Value & "'"
                    End If
                End If
            Next
            If filterCampus <> "" Then
                filterCampus = filterCampus & ")"
                filterCampus = " and " & filterCampus
            End If
        End If
        If lstboxEnrollmentStatus.SelectionMode = ListSelectionMode.Multiple Then
            For Each li As ListItem In lstboxEnrollmentStatus.Items
                If li.Selected = True Then
                    If filterEnrollment = "" Then
                        filterEnrollment = " SE.StatusCodeId in ('" & li.Value & "'"
                        ''filterEnrollment = "'" & li.Value & "'"
                    Else
                        filterEnrollment = filterEnrollment & ",'" & li.Value & "'"
                    End If
                End If
            Next
            If filterEnrollment <> "" Then
                filterEnrollment = filterEnrollment & ")"
                filterEnrollment = " and " & filterEnrollment
            End If
        End If
        If lstboxFundSource.SelectionMode = ListSelectionMode.Multiple Then
            For Each li As ListItem In lstboxFundSource.Items
                If li.Selected = True Then
                    If filterFundSource = "" Then
                        filterFundSource = "ST.AwardTypeId in ('" & li.Value & "'"
                        ''filterFundSource = "'" & li.Value & "'"
                    Else
                        filterFundSource = filterFundSource & ",'" & li.Value & "'"
                    End If
                End If
            Next
            If filterFundSource <> "" Then
                filterFundSource = filterFundSource & ")"
                filterFundSource = " and " & filterFundSource
            End If
        End If
        If lstboxLender.SelectionMode = ListSelectionMode.Multiple Then
            For Each li As ListItem In lstboxLender.Items
                If li.Selected = True Then
                    If filterLender = "" Then
                        filterLender = "SA.LenderId in ('" & li.Value & "'"
                        ''filterLender = "'" & li.Value & "'"
                    Else
                        filterLender = filterLender & ",'" & li.Value & "'"
                    End If
                End If
            Next
            If filterLender <> "" Then
                filterLender = filterLender & ")"
                filterLender = " and " & filterLender
            End If
        End If

        ds = myEDFacade.GetPendingPayments(ddlFileNames.SelectedValue.ToString, filterCampus, filterEnrollment, filterFundSource, filterLender, txtPaymentExpFrom.Text, txtPaymentExpTo.Text)
        Dim dc As New DataColumn("RemoveFromList")
        dc.DataType = System.Type.GetType("System.Boolean")
        dc.DefaultValue = False
        Try
            ds.Tables("PendingPayments").Columns.Add(dc)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        Dim TRP As Integer = 0
        Dim TRR As Integer = 0
        Dim DisbAmt As Double = 0.0
        Dim DisbRemAmt As Double = 0.0
        If Not txtPostDate.Text & "" = "" Or chkPOSS.Checked = True Then
            For Each dr As DataRow In ds.Tables("PendingPayments").Rows
                If Not txtPostDate.Text & "" = "" Then
                    dr("DisbDate") = CType(txtPostDate.Text, DateTime)
                End If
                If chkPOSS.Checked = True Then
                    If dr("Pay").ToString = "False" And dr("IsInSchool").ToString = False Then
                        dr("Pay") = True
                        TRP += 1
                        Try
                            DisbAmt += Convert.ToDecimal(dr("PaymentAmount").ToString)
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            DisbAmt += 0.0
                        End Try
                    End If
                Else
                    If dr("Pay").ToString = "True" Then
                        TRP += 1
                        Try
                            DisbAmt += Convert.ToDecimal(dr("PaymentAmount").ToString)
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            DisbAmt += 0.0
                        End Try
                    End If
                End If
            Next
        Else
            For Each dr As DataRow In ds.Tables("PendingPayments").Rows
                If dr("Pay").ToString = "True" Then
                    TRP += 1
                    Try
                        DisbAmt += Convert.ToDecimal(dr("PaymentAmount").ToString)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisbAmt += 0.0
                    End Try
                End If
            Next
        End If
        hfTR.Value = ds.Tables("PendingPayments").Rows.Count.ToString
        hfTRP.Value = TRP.ToString
        hfTRR.Value = TRR.ToString
        hfAP.Value = DisbAmt.ToString("0.00")
        hfAR.Value = DisbRemAmt.ToString("0.00")

        ds.Tables("PendingPayments").AcceptChanges()
        Session("PendingPayments") = ds
        rgPellAward.DataSource = ds
        rgPellAward.DataBind()
        rgPellAward.Visible = True
        tblPellHeader.Visible = True
        tblPellRecords.Visible = True
    End Sub
    Private Sub ClearAll()

        Dim lstboxCampus As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxCampus"), ListBox)
        Dim lstboxEnrollmentStatus As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxEnrollmentStatus"), ListBox)
        Dim lstboxFundSource As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxFundSource"), ListBox)
        Dim lstboxLender As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxLender"), ListBox)
        Dim ddlFileNames As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlFileNames"), DropDownList)
        Dim txtPaymentExpFrom As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPaymentExpFrom"), TextBox)
        Dim txtPaymentExpTo As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPaymentExpTo"), TextBox)

        Session("PendingPayments") = Nothing
        tblPellRecords.Visible = False
        tblPellHeader.Visible = False
        rgPellAward.Visible = False
        lstboxCampus.ClearSelection()
        lstboxEnrollmentStatus.ClearSelection()
        lstboxFundSource.ClearSelection()
        lstboxLender.ClearSelection()
        txtPaymentExpFrom.Text = String.Empty
        txtPaymentExpTo.Text = String.Empty
        chkPOSS.Checked = False
        rdoExpectedDate.Checked = True
        rdoSpecifyDate.Checked = False
        txtPostDate.Text = String.Empty
        'txtPostDate.Enabled = False
        'hlPostDate.Enabled = False
        rapPD.Enabled = False
        txtRefRecid.Text = String.Empty
        ddlPaymentType.SelectedIndex = 0
        rgExp.Visible = False
    End Sub
    Public Function GetProgramStatus(ByVal SSN As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" Select SE.StuEnrollId, PV.PrgVerCode +' ('+SC.StatusCodeDescrip+')' as ProgramStatus  ")
            .Append(" from arStuEnrollments SE, arPrgVersions PV, syStatusCodes SC  ")
            .Append(" where SE.StatusCodeId=SC.StatusCodeId and SE.PrgVerId=PV.PrgVerId and ")
            .Append("   StudentId in (Select StudentId from arStudent where SSN='" & SSN & "') ")
            .Append("ORDER BY ProgramStatus asc")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    Protected Sub lstboxCampus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lstboxCampus As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxCampus"), ListBox)
        If lstboxCampus.SelectedValue <> Guid.Empty.ToString Then
            lstboxCampus.SelectionMode = ListSelectionMode.Multiple
        Else
            lstboxCampus.ClearSelection()
            lstboxCampus.SelectedValue = Guid.Empty.ToString
            lstboxCampus.SelectionMode = ListSelectionMode.Single
        End If
    End Sub

    Protected Sub lstboxEnrollmentStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lstboxEnrollmentStatus As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxEnrollmentStatus"), ListBox)
        If lstboxEnrollmentStatus.SelectedValue <> Guid.Empty.ToString Then
            lstboxEnrollmentStatus.SelectionMode = ListSelectionMode.Multiple
        Else
            lstboxEnrollmentStatus.ClearSelection()
            lstboxEnrollmentStatus.SelectedValue = Guid.Empty.ToString
            lstboxEnrollmentStatus.SelectionMode = ListSelectionMode.Single
        End If
    End Sub

    Protected Sub lstboxFundSource_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lstboxFundSource As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxFundSource"), ListBox)
        If lstboxFundSource.SelectedValue <> Guid.Empty.ToString Then
            lstboxFundSource.SelectionMode = ListSelectionMode.Multiple
        Else
            lstboxFundSource.ClearSelection()
            lstboxFundSource.SelectedValue = Guid.Empty.ToString
            lstboxFundSource.SelectionMode = ListSelectionMode.Single
        End If
    End Sub

    Protected Sub lstboxLender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lstboxLender As ListBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("lstboxLender"), ListBox)
        If lstboxLender.SelectedValue <> Guid.Empty.ToString Then
            lstboxLender.SelectionMode = ListSelectionMode.Multiple
        Else
            lstboxLender.ClearSelection()
            lstboxLender.SelectedValue = Guid.Empty.ToString
            lstboxLender.SelectionMode = ListSelectionMode.Single
        End If
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Display error in message box in the client

        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        Dim rgEx As RadGrid = New RadGrid
        Dim dsExp As New DataSet
        rgEx = DirectCast(rgPellAward, RadGrid)
        rgEx.AllowPaging = False
        rgEx.Rebind()
        dsExp = DirectCast(Session("PendingPayments"), DataSet)
        Dim dcPS As New DataColumn("ProgramStatus")
        dcPS.DataType = System.Type.GetType("System.String")
        Try
            dsExp.Tables("PendingPayments").Columns.Add(dcPS)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        Dim dcC As New DataColumn("BGColor")
        dcC.DataType = System.Type.GetType("System.String")
        Try
            dsExp.Tables("PendingPayments").Columns.Add(dcC)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        For Each gi As GridDataItem In rgEx.Items
            Dim AwardScheduleId As String = gi("ASID").Text
            Dim drExp() As DataRow = dsExp.Tables("PendingPayments").Select("AwardScheduleId='" & AwardScheduleId & "'")
            Dim ddlPS As New DropDownList()
            ddlPS = DirectCast(gi.FindControl("ddlStuEnrollment"), DropDownList)
            Dim tbDisbDate As New TextBox()
            tbDisbDate = DirectCast(gi.FindControl("txtDisbDate"), TextBox)
            Dim tbPayAmt As New TextBox()
            tbPayAmt = DirectCast(gi.FindControl("txtPaymentAmount"), TextBox)
            For Each dr As DataRow In drExp
                dr("ProgramStatus") = ddlPS.SelectedItem.Text
                dr("DisbDate") = tbDisbDate.Text
                dr("PaymentAmount") = tbPayAmt.Text
                dr("BGColor") = IIf(ddlPS.Items.Count > 1, "1", "0")
            Next
        Next
        rgExp.DataSource = dsExp.Tables("PendingPayments").DefaultView
        rgExp.DataBind()
        rgExp.ExportSettings.FileName = "Pending Payment"
        rgExp.ExportSettings.ExportOnlyData = True
        rgExp.ExportSettings.IgnorePaging = True
        rgExp.MasterTableView.ExportToExcel()
        rgExp.Visible = True
        rgEx.Dispose()
        'rgExp.Dispose()
        dsExp.Dispose()
    End Sub


    Private Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String
        '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
        Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")
    End Function
    Protected Sub btnProcess_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.ServerClick
        'Troy: 8/23/2011. DE6161:QA: Messages incorrect when we chose to pay only certain students on the Approve EDExpress Pending Payments
        'The original code does not take into account the effect of the user filtering the grid. When the user filters the grid they
        'expect that only those values will be used when posting payments. For eg. there are 10 pending payments for a certain file.
        'When the list is first displayed, all 10 pending payments are checked to be paid. However, if the user filters out the list
        'for just a particular student with 3 pending payments then only those should be posted and the message should say that 3 payments
        'are to be posted. 
        Dim ddlFileNames As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlFileNames"), DropDownList)
        Dim result As String = ""
        Dim IsValid As Boolean = False
        If rdoSpecifyDate.Checked = True Then
            If txtPostDate.Text & "" = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please specify date');", True)
                ''DisplayErrorMessage("Please specify date")
                Exit Sub
            End If
        End If
        If ddlPaymentType.SelectedValue = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please select payment type');", True)
            ''DisplayErrorMessage("Please select payment type")
            Exit Sub
        End If
        Dim ds As DataSet = Session("PendingPayments")
        Dim AwardScheduleId As String

        'New code added by Troy on 8/23/2011 to fix DE6161
        '--------------------------------------------------
        Dim dtPay As New DataTable
        dtPay = ds.Tables("PendingPayments").Clone
        '-----------------------------------------------------

        'Disable paging on the grid temporarily
        rgPellAward.AllowPaging = False
        rgPellAward.Rebind()

        For Each gi As GridDataItem In rgPellAward.Items
            AwardScheduleId = gi.Cells(2).Text
            Dim ddlPS As New DropDownList()
            ddlPS = DirectCast(gi.FindControl("ddlStuEnrollment"), DropDownList)
            Dim ckPay As New CheckBox()
            ckPay = DirectCast(gi.FindControl("chkPay"), CheckBox)
            Dim ckRFL As New CheckBox()
            ckRFL = DirectCast(gi.FindControl("chkRemove"), CheckBox)
            Dim tbDisbDate As New TextBox()
            tbDisbDate = DirectCast(gi.FindControl("txtDisbDate"), TextBox)
            Dim tbPayAmt As New TextBox()
            tbPayAmt = DirectCast(gi.FindControl("txtPaymentAmount"), TextBox)
            ''Dim drTemp() As DataRow = ds.Tables("PendingPayments").Select("AwardScheduleId='" + AwardScheduleId + "'")
            Dim drTemp() As DataRow = ds.Tables("PendingPayments").Select("AwardScheduleId='" + AwardScheduleId + "'  And DisbNum=" + gi.Cells(14).Text + " And DusbSeqNum=" + gi.Cells(15).Text)

            For Each dr As DataRow In drTemp
                dr("StuEnrollmentId") = ddlPS.SelectedValue.ToString
                dr("PaymentAmount") = tbPayAmt.Text
                dr("DisbDate") = tbDisbDate.Text
                dr("Pay") = ckPay.Checked
                dr("RemoveFromList") = ckRFL.Checked
                dtPay.ImportRow(dr)
            Next
        Next

        'Reenable paging on the grid
        rgPellAward.AllowPaging = True
        rgPellAward.Rebind()

        Dim TRP As Integer = 0
        Dim TRR As Integer = 0
        Dim DisbAmt As Double = 0.0
        Dim DisbRemAmt As Double = 0.0
        For Each drF As DataRow In dtPay.Rows 'Changed by Troy on 8/23/2011 to use dtPay which has the filtered values instead of ds.Tables("PendingPayments")
            If drF("Pay").ToString = "True" Then
                TRP += 1
                Try
                    DisbAmt += Convert.ToDecimal(drF("PaymentAmount").ToString)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    DisbAmt += 0.0
                End Try
            End If
            If drF("RemoveFromList").ToString = "True" Then
                TRR += 1
                Try
                    DisbRemAmt += Convert.ToDecimal(drF("PaymentAmount").ToString)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    DisbRemAmt += 0.0
                End Try
            End If
        Next
        hfTR.Value = dtPay.Rows.Count.ToString 'Changed by Troy on 8/23/2011 to use dtPay which has the filtered values instead of ds.Tables("PendingPayments")
        hfTRP.Value = TRP.ToString
        hfTRR.Value = TRR.ToString
        hfAP.Value = DisbAmt.ToString("0.00")
        hfAR.Value = DisbRemAmt.ToString("0.00")
        Dim strEDExpDataFrom As String = ""
        strEDExpDataFrom = MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer")
        If strEDExpDataFrom = "yes" Then
            result = myEDFacade.ProcessEDPendingPaymentNew("PELL", dtPay, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, MyAdvAppSettings.AppSettings("RemoteEDExpressNotPost"), MyAdvAppSettings.AppSettings("RemoteEDExpressArchive"), ddlFileNames.SelectedValue.ToString, , , , "yes") 'Changed by Troy on 8/23/2011 to use dtPay which has the filtered values instead of ds.Tables("PendingPayments")
        Else
            result = myEDFacade.ProcessEDPendingPaymentNew("PELL", dtPay, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, MyAdvAppSettings.AppSettings("NotPostPathEdExp"), MyAdvAppSettings.AppSettings("TargetPathEdExp"), ddlFileNames.SelectedValue.ToString, , , , "no") 'Changed by Troy on 8/23/2011 to use dtPay which has the filtered values instead of ds.Tables("PendingPayments")
        End If
        If Not result = "" Then
            Dim msgResult As String = "alert('" + ReplaceSpecialCharactersInJavascriptMessage(result) + "');"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", msgResult, True)
        End If
        ClearAll()

    End Sub
    Protected Sub chkPayCheck_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dsCK As New DataSet
        dsCK = DirectCast(Session("PendingPayments"), DataSet)
        Dim chk As New CheckBox
        chk = DirectCast(sender, CheckBox)
        Dim TRP As Integer = 0
        Dim TRR As Integer = 0
        Dim DisbAmt As Double = 0.0
        Dim DisbRemAmt As Double = 0.0
        For Each dr As DataRow In dsCK.Tables("PendingPayments").Rows
            If chk.Checked = True Then
                dr("Pay") = True
                TRP += 1
                Try
                    DisbAmt += Convert.ToDecimal(dr("PaymentAmount").ToString)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    DisbAmt += 0.0
                End Try
            Else
                dr("Pay") = False
                TRP = 0
            End If
            dr("RemoveFromList") = False
            TRR = 0
            DisbRemAmt = 0.0
        Next
        hfTR.Value = dsCK.Tables("PendingPayments").Rows.Count.ToString
        hfTRP.Value = TRP.ToString
        hfTRR.Value = TRR.ToString
        hfAP.Value = DisbAmt.ToString("0.00")
        hfAR.Value = DisbRemAmt.ToString("0.00")
        Session("PendingPayments") = dsCK
        Session("HeaderChecked") = True
        Session("HeaderCB") = chk
        dsCK.Dispose()
        rgPellAward.Rebind()
    End Sub

    Protected Sub rgExp_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rgExp.ItemDataBound
        If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            If e.Item.OwnerTableView.Name = "PellAward" Then
                If item("BGColor").Text = "1" Then
                    e.Item.BackColor = Color.LightPink
                End If
            End If
        End If
    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub
End Class
