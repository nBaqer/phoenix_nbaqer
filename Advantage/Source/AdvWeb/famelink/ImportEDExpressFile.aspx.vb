﻿

Imports System.Text
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports System.IO
Imports Fame.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports Fame.Advantage.Common
Imports Fame.AdvantageV1.DataAccess

Partial Class FameLink_ImportEDExpressFile
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected CampusId As String
    'Dim userId As String
    Dim resourceId As Integer
    Protected m_context As HttpContext
    Private ReadOnly myEdFacade As New EDFacade_New
    Private strLogEntry As New StringBuilder(10000)
    'Private strFileName As String
    'Private myMsgCollection As New EDCollectionMessageInfos_New
    Private ConString As String  '"Provider=SQLOLEDB;Data Source=GOVINDARAJULU\DEVSERVER2005;Initial Catalog=ClockHourDB;User ID=sa;Password=test1"
    'Dim arrREFU As New ArrayList
    'Dim incrRCVD As Integer = 0
    'Dim arrayRCVD() As String
    Dim dtPatsData, dtPatsDisb, dtDirectLoan, dtDlSchDisb, dtDlActualDisb As New DataTable()
    'Dim strAppendMessage As New StringBuilder
    'Dim intTotalRecordCount As Integer = 0
    Protected MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Dim fac As New UserSecurityFacade

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = AdvantageSession.UserState.UserId.ToString
        Dim advantageUserState As User = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, CampusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            'Dim hlPostDate As HyperLink = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("hlPostDate"), HyperLink)
            'Dim txtPostDate As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPostDate"), TextBox)

            'hlPostDate.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtPostDate.ClientID + "',true,1945)"
            'hlAwardStartDate.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtAwardStartDate.ClientID + "',true,1945)"
            'hlAwardEndDate.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtAwardEndDate.ClientID + "',true,1945)"
            ''tblPellRecords.Visible = False
            tblPellRecords.Visible = False
            rgPellAward.Visible = False
            tblPellHeader.Visible = False
            tblPellException.Visible = False
            BuildFilesList()
            BuildPaymentTypesDDL()
            BuildAcademicYear()
            ClearAll()
            ClearDataGrid()
            btnProcessData.Attributes.Add("onclick", "return msgconfirm(" + hfAS.ClientID + "," + hfAD.ClientID + "," + hfAA.ClientID + "," + hfSA.ClientID + ");")
        End If
    End Sub
    Private Sub BuildFilesList()
        Dim i As Integer
        Dim filename As String
        Dim uriSource As New Uri(MyAdvAppSettings.AppSettings("SourcePathEdExp"))
        Dim strSourcePath As String = uriSource.AbsolutePath
        Dim filenames() As String
        If MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer") = "yes" Then
            filenames = Directory.GetFiles(MyAdvAppSettings.AppSettings("RemoteEDExpressIn"))
        Else
            filenames = Directory.GetFiles(strSourcePath)
        End If
        ddlFileNames.Items.Clear()
        With ddlFileNames
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        For i = 0 To filenames.Length - 1
            filename = Path.GetFileName(filenames(i)) 'This is much better than substring
            ddlFileNames.Items.Add(filename)
        Next i
    End Sub
    Private Sub BuildAcademicYear()
        Dim dsAY As DataSet = GetActiveAcademicYears()
        ddlAcademicYear.DataTextField = "AcademicYearDescrip"
        ddlAcademicYear.DataValueField = "AcademicYearId"
        ddlAcademicYear.DataSource = dsAY
        ddlAcademicYear.DataBind()
        With ddlAcademicYear
            .Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
            .SelectedIndex = 0
        End With
    End Sub
    Public Function GetActiveAcademicYears() As DataSet
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CCT.AcademicYearId, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         CCT.AcademicYearCode, ")
            .Append("         CCT.AcademicYearDescrip ")
            .Append("FROM     saAcademicYears CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId AND ST.Status='Active' ")
            .Append("ORDER BY ST.Status,CCT.AcademicYearDescrip asc")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Protected Sub ProcessData()
        Dim blnResult As Boolean = False
        Dim clsflFile As EDFileInfo_New
        Dim strLogMessage As New StringBuilder
        'Dim intInitialImportSuccessful As Integer = 0
        'Dim strValidMessage As String = ""
        'Dim intFAIDExists As Integer = 0
        'Dim intHEADExists As Integer = 0
        'Dim intDISBExists As Integer = 0

        strLogEntry = New StringBuilder
        'ClearDataGrid()

        If ddlFileNames.SelectedValue = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please select EDExpress File to Import');", True)
            ''DisplayErrorMessage("Please select EDExpress File to Import")
            Exit Sub
        End If
        clsflFile = Session("clsflFile")
        Session("FileName") = ddlFileNames.SelectedValue
        Session("MsgType") = clsflFile.strMsgType
        If clsflFile.strMsgType.ToUpper = "PELL" Then
            If ddlAcademicYear.SelectedValue = Guid.Empty.ToString Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please select Award Year');", True)
                ''DisplayErrorMessage("Please select Award Year")
                Exit Sub
            End If
            'If txtAwardStartDate.Text + "" = "" Then
            If txtAwardStartDate.SelectedDate Is Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please select Award Start Date');", True)
                ''DisplayErrorMessage("Please select Award Start Date")
                Exit Sub
            End If
            'If txtAwardEndDate.Text + "" = "" Then
            If txtAwardEndDate.SelectedDate Is Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please select Award End Date');", True)
                ''DisplayErrorMessage("Please select Award End Date")
                Exit Sub
            End If
        End If
        With strLogEntry
            .Append("Start Processing of importation of EDExpress file: " & UCase(Session("FileName").ToString) + ControlChars.CrLf)
        End With
        If Len(clsflFile.strError) = 0 Then
            If clsflFile.iRecordCount <> 0 Then
                Session("RecordCount") = clsflFile.iRecordCount
                If clsflFile.strMsgType.ToUpper = "PELL" Then
                    dtPATSData = Session("dtPATSData")
                    dtPATSDisb = Session("dtPATSDisb")
                    Dim strDate As String = String.Empty
                    If Not (txtAwardStartDate.SelectedDate Is Nothing) Then
                        strDate = txtAwardStartDate.SelectedDate.ToString
                    End If

                    Dim strDate1 As String = String.Empty
                    If Not (txtAwardEndDate.SelectedDate Is Nothing) Then
                        strDate1 = txtAwardEndDate.SelectedDate.ToString
                    End If
                    If MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer") = "yes" Then
                        Dim strSourceFileLocation As String = MyAdvAppSettings.AppSettings("RemoteEDExpressIn") + "\" + ddlFileNames.SelectedItem.Text
                        'blnResult = myEDFacade.ProcessEDFile(clsflFile, myMsgCollection, strSourceFileLocation, ddlAcademicYear.SelectedValue, txtAwardStartDate.Text, txtAwardEndDate.Text, chkOADWED.Checked, chkOAAWEA.Checked, rdolstPaymentOptions.SelectedValue, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text)
                        blnResult = myEDFacade.ProcessEDFileNew(clsflFile, dtPATSData, dtPATSDisb, , strSourceFileLocation, ddlAcademicYear.SelectedValue, strDate, strDate1)
                        Session("dtPATSData") = dtPATSData
                        Session("dtPATSDisb") = dtPATSDisb
                    Else
                        Dim strLocalFileLocation As String = MyAdvAppSettings.AppSettings("SourcePathEdExp") + ddlFileNames.SelectedItem.Text
                        'blnResult = myEDFacade.ProcessEDFile(clsflFile, myMsgCollection, strLocalFileLocation, ddlAcademicYear.SelectedValue, txtAwardStartDate.Text, txtAwardEndDate.Text, chkOADWED.Checked, chkOAAWEA.Checked, rdolstPaymentOptions.SelectedValue, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text)
                        blnResult = myEDFacade.ProcessEDFileNew(clsflFile, dtPATSData, dtPATSDisb, , strLocalFileLocation, ddlAcademicYear.SelectedValue, strDate, strDate1)
                        Session("dtPATSData") = dtPATSData
                        Session("dtPATSDisb") = dtPATSDisb
                    End If
                Else
                    dtDirectLoan = Session("dtDirectLoan")
                    dtDLSchDisb = Session("dtDLSchDisb")
                    dtDLActualDisb = Session("dtDLActualDisb")
                    Dim strAcademicYearId As String = ""
                    If MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer") = "yes" Then
                        Dim strSourceFileLocation As String = MyAdvAppSettings.AppSettings("RemoteEDExpressIn") + "\" + ddlFileNames.SelectedItem.Text
                        'blnResult = myEDFacade.ProcessEDFile(clsflFile, myMsgCollection, strSourceFileLocation, ddlAcademicYear.SelectedValue, txtAwardStartDate.Text, txtAwardEndDate.Text, chkOADWED.Checked, chkOAAWEA.Checked, rdolstPaymentOptions.SelectedValue, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text)
                        blnResult = myEDFacade.ProcessEDFileNew(clsflFile, dtDirectLoan, dtDLSchDisb, dtDLActualDisb, strSourceFileLocation, strAcademicYearId)
                        ddlAcademicYear.SelectedValue = strAcademicYearId
                        Session("dtDirectLoan") = dtDirectLoan
                        Session("dtDLSchDisb") = dtDLSchDisb
                        Session("dtDLActualDisb") = dtDLActualDisb
                    Else
                        Dim strLocalFileLocation As String = MyAdvAppSettings.AppSettings("SourcePathEdExp") + ddlFileNames.SelectedItem.Text
                        'blnResult = myEDFacade.ProcessEDFile(clsflFile, myMsgCollection, strLocalFileLocation, ddlAcademicYear.SelectedValue, txtAwardStartDate.Text, txtAwardEndDate.Text, chkOADWED.Checked, chkOAAWEA.Checked, rdolstPaymentOptions.SelectedValue, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text)
                        blnResult = myEDFacade.ProcessEDFileNew(clsflFile, dtDirectLoan, dtDLSchDisb, dtDLActualDisb, strLocalFileLocation, strAcademicYearId)
                        ddlAcademicYear.SelectedValue = strAcademicYearId
                        Session("dtDirectLoan") = dtDirectLoan
                        Session("dtDLSchDisb") = dtDLSchDisb
                        Session("dtDLActualDisb") = dtDLActualDisb
                    End If
                End If

                'updatelistMessages(myMsgCollection, clsflFile)

                If clsflFile.strMsgType.ToUpper = "PELL" Then
                    'myEDFacade.ValidateEDMsgCollection(ConString, myMsgCollection, clsflFile.strMsgType, dtParent, dtChild)
                    'If Session("Process").ToString <> "1" Then
                    Dim dtParent As DataTable = Session("dtPATSData")
                    Dim dtChild As DataTable = Session("dtPATSDisb")
                    myEDFacade.ValidateEDMsgCollectionNew(ConString, clsflFile.strMsgType, dtParent, dtChild)
                    Session("dtPATSData") = dtParent
                    Session("dtPATSDisb") = dtChild
                    'End If
                    If Session("Process").ToString = "1" Then
                        SaveProcessFile()
                    Else
                        SortPellGrids()
                        tblPellRecords.Visible = True
                        tblPellHeader.Visible = True
                        rgPellAward.Visible = True

                        tblDLRecords.Visible = False
                        tblDLHeader.Visible = False
                        rgDirectLoan.Visible = False
                    End If
                Else
                    'myEDFacade.ValidateEDMsgCollection(ConString, myMsgCollection, clsflFile.strMsgType, dtParent, dtChild)
                    'If Session("Process").ToString <> "1" Then
                    Dim dtParent As DataTable = Session("dtDirectLoan")
                    Dim dtChildSD As DataTable = Session("dtDLSchDisb")
                    Dim dtChildAD As DataTable = Session("dtDLActualDisb")
                    myEDFacade.ValidateEDMsgCollectionNew(ConString, clsflFile.strMsgType, dtParent, dtChildSD, dtChildAD)
                    Session("dtDirectLoan") = dtParent
                    Session("dtDLSchDisb") = dtChildSD
                    Session("dtDLActualDisb") = dtChildAD
                    'End If
                    If Session("Process").ToString = "1" Then
                        SaveProcessFile()
                    Else
                        SortDLGrids()
                        tblDLRecords.Visible = True
                        tblDLHeader.Visible = True
                        rgDirectLoan.Visible = True

                        tblPellRecords.Visible = False
                        tblPellHeader.Visible = False
                        rgPellAward.Visible = False
                    End If
                End If
                If blnResult Then
                    With strLogEntry
                        .Append("SUCCESSFUL Processsing of EDExpress file: " + clsflFile.strFileName + ControlChars.CrLf)
                        .Append("File Size: " + clsflFile.iFileSize.ToString + ControlChars.CrLf)
                        .Append("with " + clsflFile.iRecordCount.ToString + " records of type " + clsflFile.strMsgType + ControlChars.CrLf + "PARSED and waiting to be processed" + ControlChars.CrLf)
                    End With
                Else
                    With strLogEntry
                        .Append("FAILED Processing of EDExpress file: " & clsflFile.strFileName + ControlChars.CrLf)
                        .Append("Additional Information: " & clsflFile.strError + ControlChars.CrLf)
                    End With
                End If
            Else
                With strLogEntry
                    .Append("FAILED Processing of EDExpress file:" & clsflFile.strFileName + ControlChars.CrLf)
                    .Append("File Size: " + clsflFile.iFileSize.ToString + ControlChars.CrLf)
                    .Append("with " + clsflFile.iRecordCount.ToString + " records of type " + clsflFile.strMsgType + ControlChars.CrLf + "PARSED and waiting to be processed" + ControlChars.CrLf)
                    .Append("Additional Information: " & clsflFile.strError + ControlChars.CrLf)
                End With
                With strLogMessage
                    .Append("Processing of EDExpress file:" & clsflFile.strFileName + " FAILED" + ControlChars.CrLf)
                End With
            End If
        Else
            With strLogEntry
                .Append("Processing of EDExpress file: " & Session("FileName").ToString + " FAILED" + ControlChars.CrLf)
                .Append("Additional Information: " & Session("FileName").ToString + ControlChars.CrLf)
                Dim ArrRecord() As String
                Dim splitChar As Char = ":"
                ArrRecord = clsflFile.strError.Split(splitChar)
                Dim dbTR As Integer
                Dim dbAWR As Integer
                Dim dbDR As Integer
                Try
                    dbAWR = CType(ArrRecord(0), Integer)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    dbAWR = 0
                End Try
                Try
                    dbDR = CType(ArrRecord(1), Integer)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    dbDR = 0
                End Try
                dbTR = dbAWR + dbDR
                .Append(Session("FileName").ToString & "not in proper format" + ControlChars.CrLf)
                .Append("Total number of records posted : " & dbTR.ToString + ControlChars.CrLf)
                .Append("Number of Direct Loan awards posted : " & "0" + ControlChars.CrLf)
                .Append("Number of Direct Loan awards not posted : " & dbAWR.ToString + ControlChars.CrLf)
                .Append("Number of Direct Loan Disbursements posted : " & "0" + ControlChars.CrLf)
                .Append("Number of Direct Loan Disbursements not posted : " & dbDR.ToString + ControlChars.CrLf)
                .Append(ControlChars.CrLf + ControlChars.CrLf + "The data in the file (" + Trim(Session("FileName")) + ") was not successfully posted and moved to exception folder.")
                ''SaveException()
                Dim strEDExpDataFrom As String = ""
                strEDExpDataFrom = MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer")

                If strEDExpDataFrom = "yes" Then
                    MoveFileFromInToExceptionFolder(MyAdvAppSettings.AppSettings("RemoteEDExpressIn"), MyAdvAppSettings.AppSettings("RemoteEDExpressException"), Trim(Session("FileName")))
                Else
                    MoveFileFromInToExceptionFolder(MyAdvAppSettings.AppSettings("SourcePathEdExp"), MyAdvAppSettings.AppSettings("ExceptionPathEdExp"), Trim(Session("FileName")))
                End If
            End With

            ''DisplayErrorMessage(strLogEntry.ToString)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SM", "alert('" + ReplaceSpecialCharactersInJavascriptMessage(strLogEntry.ToString) + "');", True)
            ''ClearDataGrid()
            BuildFilesList()
        End If
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    'Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub
    'Private Sub MoveFileToESPExceptionFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String)
    '    Dim uriSource As New Uri(SourcePath.ToString.ToLower)
    '    Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
    '    Dim strSourceFile As String = uriSource.AbsolutePath + FileName
    '    Dim strTargetFile As String = uriTarget.AbsolutePath + FileName
    '    Dim strMessage As String = ""
    '    Try
    '        If Not File.Exists(strTargetFile) Then
    '            'Directory.Move(strSourceFile, strTargetFile)
    '            RenameExistingFile(strSourceFile)
    '            File.Move(strSourceFile, strTargetFile)
    '        End If
    '    Catch e As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(e)

    '    End Try
    'End Sub
    Private Sub RenameExistingFile(ByVal strTargetPathWithFile As String)
        'Dim sMessage As String = ""
        'Dim strRandomNumber As New Random
        Dim rightNow As DateTime = DateTime.Now
        Dim s As String
        s = rightNow.ToString("MMddyyyy")
        Dim strTargetPathWithNewFile As String = strTargetPathWithFile + s   '+ strRandomNumber.Next.ToString
        If File.Exists(strTargetPathWithFile) Then
            File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
        End If
    End Sub
    Private Sub MoveFileFromInToExceptionFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String)
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        If MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer") = "yes" Then
            'strSourceFile = SourcePath + "\" + FileName
            'strTargetFile = TargetPath + "\" + FileName
            strSourceFile = SourcePath + FileName
            strTargetFile = TargetPath + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + "/" + FileName
            strTargetFile = uriTarget.AbsolutePath + "/" + FileName
        End If

        'Dim strMessage As String = ""
        Try
            'modified by balaji on 10/11/2007
            'based on input from patrick - files from Fame ESP will have an extension that is sequencial
            'in other words we won't have a case where there will be two HEAD Files (or any other file types)
            'with same extension, the extension will be .000,.001,.002
            'so when that is the case there is no need to rename the file.
            If File.Exists(strSourceFile) Then
                'Dim fs As FileStream = New FileStream(strSourceFile, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
                'fs.Close()
                'Directory.Move(strSourceFile, strTargetFile)
                RenameExistingFile(strTargetFile)
                File.Move(strSourceFile, strTargetFile)
                Return
            End If
        Catch e As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(e)

            Return
        End Try
    End Sub
    'Private Sub updatelistMessages(ByVal myMsgCollection As EDCollectionMessageInfos_New, ByVal myFile As EDFileInfo_New)
    '    Dim nbrMessages As Integer = myMsgCollection.Count
    '    Dim intNdx As Integer = 0
    '    Dim intPNdx As Integer = 0
    '    Dim strEntry As New System.Text.StringBuilder(1000)
    '    Dim fName As String
    '    '        Dim strFormat As String
    '    'Dim intOldSSN As String = ""
    '    Dim strMessageType As String = ""
    '    Dim strDBIndicator As String = ""
    '    Dim strOrigSSN As String = ""
    '    Dim strAwardId As String = ""
    '    Dim strMPNStatus As String = ""
    '    Dim strFileNameOnly As String = ""
    '    Dim strM As String = ""
    '    Dim strASD As String = ""
    '    Dim strAED As String = ""
    '    Dim boolIsParsed As Boolean
    '    Dim strAwardYear As String = ""
    '    Dim strGrantType As String = ""
    '    Dim dbRebateAmount As Double = 0.0
    '    Dim intDLID As Integer
    '    'lstMessages.UseTabStops = True
    '    'lstMessages.BeginUpdate()
    '    For intNdx = 0 To nbrMessages - 1 Step 1
    '        strMessageType = myMsgCollection.Items(intNdx).strMsgType
    '        strDBIndicator = myMsgCollection.Items(intNdx).strDatabaseIndicator
    '        fName = UCase(System.IO.Path.GetFileName(Trim(myMsgCollection.Items(intNdx).strFileNameSource)))

    '        strEntry = New StringBuilder

    '        boolIsParsed = myMsgCollection.Items(intNdx).blnIsParsed

    '        If boolIsParsed = True Then
    '            Select Case strMessageType.ToUpper
    '                Case "PELL"
    '                    ShowPATS()
    '                    Select Case strDBIndicator.ToUpper
    '                        Case "T"
    '                            'Process Data For Pell ACG, SMART and Teach Main
    '                            strOrigSSN = myMsgCollection.Items(intNdx).strOriginalSSN
    '                            strAwardId = myMsgCollection.Items(intNdx).strAwardId
    '                            strGrantType = myMsgCollection.Items(intNdx).strGrandType
    '                            intPNdx = intNdx
    '                            BuildPATSData(intNdx, myMsgCollection.Items(intNdx).strAwardAmountForEntireYear, myMsgCollection.Items(intNdx).strAwardId, myMsgCollection.Items(intNdx).strGrandType, myMsgCollection.Items(intNdx).strPellAddDate, myMsgCollection.Items(intNdx).strPellAddTime, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strAcademicYearId, myMsgCollection.Items(intNdx).strAwardYearStartDate, myMsgCollection.Items(intNdx).strAwardYearEndDate)
    '                        Case "H"
    '                            'Process Data For Pell ACG, SMART and Teach Main
    '                            strOrigSSN = myMsgCollection.Items(intNdx).strOriginalSSN
    '                            strAwardId = myMsgCollection.Items(intNdx).strAwardId
    '                            strGrantType = myMsgCollection.Items(intNdx).strGrandType
    '                            intPNdx = intNdx
    '                            BuildPATSData(intNdx, myMsgCollection.Items(intNdx).strAwardAmountForEntireSchoolYear, myMsgCollection.Items(intNdx).strAwardId, myMsgCollection.Items(intNdx).strGrandType, myMsgCollection.Items(intNdx).strTeachAddDate, myMsgCollection.Items(intNdx).strTeachAddTime, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strAcademicYearId, myMsgCollection.Items(intNdx).strAwardYearStartDate, myMsgCollection.Items(intNdx).strAwardYearEndDate)
    '                        Case "S", "C"
    '                            'Process Data For Pell ACG, SMART and Teach Detail
    '                            myMsgCollection.Items(intNdx).strOriginalSSN = strOrigSSN
    '                            myMsgCollection.Items(intNdx).strAwardId = strAwardId
    '                            myMsgCollection.Items(intNdx).strGrandType = strGrantType
    '                            BuildPATSDisb(intNdx, intPNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strAwardId, myMsgCollection.Items(intNdx).strDisbursementDate, myMsgCollection.Items(intNdx).strDisbursementRealeaseIndicator, myMsgCollection.Items(intNdx).strActionStatusDisbursement, myMsgCollection.Items(intNdx).strDisbursementNumber, myMsgCollection.Items(intNdx).strDisbursementSequenceNumber, myMsgCollection.Items(intNdx).strSubmittedDisbursementAmount, myMsgCollection.Items(intNdx).strAcceptedDisbursementAmount)
    '                    End Select
    '                Case "DL"
    '                    ShowDL()
    '                    Select Case strDBIndicator.ToUpper
    '                        Case "A"
    '                            ' Do Nothing
    '                        Case "B"
    '                            ' Do Nothing
    '                        Case "D"
    '                            'Process Data For DL Direct Loan
    '                            strOrigSSN = myMsgCollection.Items(intNdx).strOriginalSSN
    '                            strMPNStatus = myMsgCollection.Items(intNdx).strMPNStatus
    '                            strASD = FormatDate(myMsgCollection.Items(intNdx).strLoanPeriodStartDate)
    '                            strAED = FormatDate(myMsgCollection.Items(intNdx).strLoanPeriodStartDate)
    '                            myMsgCollection.Items(intNdx).strAwardYearStartDate = FormatDate(myMsgCollection.Items(intNdx).strLoanPeriodStartDate)
    '                            myMsgCollection.Items(intNdx).strAwardYearEndDate = FormatDate(myMsgCollection.Items(intNdx).strLoanPeriodEndDate)
    '                            strAwardYear = GetAwardYear(myMsgCollection.Items(intNdx).strLoanId)
    '                            myMsgCollection.Items(intNdx).strAcademicYearId = strAwardYear
    '                            'ddlAcademicYear.SelectedValue = strAwardYear
    '                            'lblAwardYearHeader.Text = "Direct Loans  Award Year : " + ddlAcademicYearDL.SelectedItem.Text
    '                            intPNdx = intNdx
    '                            BuildDirectLoan(intNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strLoanType, myMsgCollection.Items(intNdx).strLoanId, myMsgCollection.Items(intNdx).strLoanAddDate, myMsgCollection.Items(intNdx).strLoanAddTime, myMsgCollection.Items(intNdx).strLoanAmountApproved, myMsgCollection.Items(intNdx).strLoanFeePercentage, myMsgCollection.Items(intNdx).strAnticipatedDisbursementInterestRebateAmount, myMsgCollection.Items(intNdx).strLoanPeriodStartDate, myMsgCollection.Items(intNdx).strLoanPeriodEndDate)
    '                            dbRebateAmount = 0.0
    '                            intDLID = intNdx
    '                        Case "M"
    '                            'Process Data For DL Actual Disbusrement
    '                            If myMsgCollection.Items(intNdx - 1).blnIsParsed = False Then
    '                                Dim msgEntry As EDMessageInfo_New
    '                                msgEntry = myMsgCollection.Items(intNdx - 1)
    '                                msgEntry.strLoanType = myMsgCollection.Items(intNdx).strActualDisbursementType
    '                                BindDataToCollection(msgEntry, myMsgCollection.Items(intNdx).strLoanId)
    '                                strOrigSSN = msgEntry.strOriginalSSN
    '                                strASD = msgEntry.strAwardYearStartDate
    '                                strAED = msgEntry.strAwardYearEndDate
    '                            End If
    '                            myMsgCollection.Items(intNdx).strOriginalSSN = strOrigSSN
    '                            myMsgCollection.Items(intNdx).strMPNStatus = strMPNStatus
    '                            myMsgCollection.Items(intNdx).strAwardYearStartDate = strASD
    '                            myMsgCollection.Items(intNdx).strAwardYearEndDate = strAED
    '                            strAwardYear = GetAwardYear(myMsgCollection.Items(intNdx).strLoanId)
    '                            'myMsgCollection.Items(intNdx).strAcademicYearId = strAwardYear
    '                            'ddlAcademicYear.SelectedValue = strAwardYear
    '                            BuildDLActualDisb(intNdx, intPNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strLoanId, myMsgCollection.Items(intNdx).strActualDisbursementDate, myMsgCollection.Items(intNdx).strActualDisbursementStatus, myMsgCollection.Items(intNdx).strActualDisbursementNumber, myMsgCollection.Items(intNdx).strActualDisbursementGrossAmount, myMsgCollection.Items(intNdx).strActualDisbursementLoanFeeAmount, myMsgCollection.Items(intNdx).strActualDisbursementNetAmount)
    '                        Case "N"
    '                            'Process Data For DL Anticipated Disburesment
    '                            myMsgCollection.Items(intNdx).strOriginalSSN = strOrigSSN
    '                            myMsgCollection.Items(intNdx).strMPNStatus = strMPNStatus
    '                            myMsgCollection.Items(intNdx).strAcademicYearId = ddlAcademicYear.SelectedValue
    '                            myMsgCollection.Items(intNdx).strAwardYearStartDate = strASD
    '                            myMsgCollection.Items(intNdx).strAwardYearEndDate = strAED
    '                            BuildDLSchDisb(intNdx, intPNdx, myMsgCollection.Items(intNdx).strOriginalSSN, myMsgCollection.Items(intNdx).strLoanId, myMsgCollection.Items(intNdx).strAnticipatedDisbursementDate, myMsgCollection.Items(intNdx).strMPNStatus, myMsgCollection.Items(intNdx).strAnticipatedDisbursementNumber, myMsgCollection.Items(intNdx).strAnticipatedDisbursementGrossAmount, myMsgCollection.Items(intNdx).strAnticipatedDisbursementFeeAmount, myMsgCollection.Items(intNdx).strAnticipatedDisbursementNetAmount)
    '                            UpdateNetAmount(intDLID, myMsgCollection, myMsgCollection.Items(intNdx).strAnticipatedDisbursementInterestRebateAmount, dbRebateAmount)
    '                    End Select
    '            End Select
    '            'BuildLogEntry(strLogEntry)
    '        End If
    '        'lstMessages.Items.Add(strEntry.ToString)
    '    Next
    '    ''Response.Write(strM)

    '    'Session("FileName") = strFileNameOnly
    '    Session("MessageCollection") = myMsgCollection

    '    lblAwardCount.Text = "Total Number of Award Records : " + myFile.iAwardCount.ToString()
    '    lblDisbCount.Text = "Total Number of Disbursements Records : " + myFile.iDisbCount.ToString()
    '    hfAS.Value = "Total Number of Award Records : " + myFile.iAwardCount.ToString()
    '    hfAD.Value = "Total Number of Disbursements Records : " + myFile.iDisbCount.ToString()
    '    'SortPellGrids()

    '    'lblAwardDL.Text = "Total Number of Award Records : " + myFile.iAwardCount.ToString()
    '    'lblDisbDL.Text = "Total Number of Anticipated Disbursement Records : " + myFile.iDisbCount.ToString()
    '    'lblActualDisbDL.Text = "Total Number of Actual Disbursement Records : " + myFile.iActualDisbCount.ToString()
    '    SortDLGrids()

    'End Sub
    Public Function GetAwardYear(ByVal strLoanId As String) As String
        Dim strAyv As String = ""
        Dim strAyNum As String
        Dim strAylNum
        strAyNum = strLoanId.Substring(10, 2)
        For Each itm As ListItem In ddlAcademicYear.Items
            strAylNum = itm.Text.Substring(itm.Text.Length - 2, 2)
            If strAyNum = strAylNum Then
                strAyv = itm.Value
                Session("AYV") = itm.Value
                Exit For
            End If
        Next
        If strAyv = "" Then
            strAyNum = strLoanId.Substring(strLoanId.IndexOf("S") + 1, 2)
            For Each itm As ListItem In ddlAcademicYear.Items
                strAylNum = itm.Text.Substring(itm.Text.Length - 2, 2)
                If strAyNum = strAylNum Then
                    strAyv = itm.Value
                    Session("AYV") = itm.Value
                    Exit For
                End If
            Next
        End If
        If strAyv = "" Then
            strAyv = "00000000-0000-0000-0000-000000000000"
        End If
        Return strAyv
    End Function
    Protected Sub BindDataToCollection(ByRef myEntry As EDMessageInfo_New, ByVal strLoanId As String)
        Dim sbCollection As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        ''StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,LoanFees,AwardStartDate,AwardEndDate,Disbursements,LoanId
        With sbCollection
            .Append(" Select SA.AwardTypeID, SA.AcademicYearId, SA.fa_id, SA.GrossAmount, SA.LoanFees, SA.AwardStartDate, SA.AwardEndDate, SA.Disbursements, SA.LoanId, S.SSN ")
            .Append(" From faStudentAwards SA, arStuEnrollments SE, arStudent S ")
            .Append(" Where SA.StuEnrollID=SE.StuEnrollID And S.StudentId=SE.StudentId And SA.LoanId='" & strLoanId & "'")
        End With
        Try
            ds = db.RunSQLDataSet(sbCollection.ToString)
            For Each dr As DataRow In ds.Tables(0).Rows
                myEntry.strOriginalSSN = dr("SSN").ToString
                myEntry.strAwardId = dr("fa_id").ToString
                myEntry.strLoanAmountApproved = dr("GrossAmount").ToString
                myEntry.strAwardYearStartDate = dr("AwardStartDate").ToString
                myEntry.strAwardYearEndDate = dr("AwardEndDate").ToString
                myEntry.strLoanId = strLoanId
                ddlAcademicYear.SelectedValue = dr("AcademicYearId").ToString
                'txtAwardYearStartDate.Text = dr("AwardStartDate").ToString
                'txtAwardYearEndDate.Text = dr("AwardEndDate").ToString
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    'Private Sub BuildDLActualDisb(ByVal IndexID As Integer, ByVal ParentIndexID As Integer, ByVal OriginalSSN As String, ByVal LoanId As String, ByVal DisbDate As String, ByVal DisbStatus As String, ByVal DisbNum As String, ByVal GrossAmount As String, ByVal Fees As String, ByVal NetAmount As String)
    '    If Not OriginalSSN = "" Then
    '        Dim drDLActualDisb As DataRow
    '        '            Dim drDLChild As DataRow
    '        Dim dbDouble As Double
    '        Try
    '            Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
    '            Dim dcLastName As New DataColumn("LastName", GetType(String))
    '            Dim dcFirstName As New DataColumn("FirstName", GetType(String))
    '            Dim dcSSN As New DataColumn("SSN", GetType(String))
    '            Dim dcCampus As New DataColumn("Campus", GetType(String))
    '            Dim dcLoanId As New DataColumn("LoanId", GetType(String))
    '            Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
    '            Dim dcDisbStatus As New DataColumn("DisbStatus", GetType(String))
    '            Dim dcDisbNum As New DataColumn("DisbNum", GetType(String))
    '            Dim dcGrossAmount As New DataColumn("GrossAmount", GetType(String))
    '            Dim dcFees As New DataColumn("Fees", GetType(String))
    '            Dim dcNetAmount As New DataColumn("NetAmount", GetType(String))

    '            dtDLActualDisb.Columns.Add(dcIndexID)
    '            dtDLActualDisb.Columns.Add(dcLastName)
    '            dtDLActualDisb.Columns.Add(dcFirstName)
    '            dtDLActualDisb.Columns.Add(dcSSN)
    '            dtDLActualDisb.Columns.Add(dcCampus)
    '            dtDLActualDisb.Columns.Add(dcLoanId)
    '            dtDLActualDisb.Columns.Add(dcDisbDate)
    '            dtDLActualDisb.Columns.Add(dcDisbStatus)
    '            dtDLActualDisb.Columns.Add(dcDisbNum)
    '            dtDLActualDisb.Columns.Add(dcGrossAmount)
    '            dtDLActualDisb.Columns.Add(dcFees)
    '            dtDLActualDisb.Columns.Add(dcNetAmount)
    '        Catch ex As System.Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '        End Try

    '        drDLActualDisb = dtDLActualDisb.NewRow()

    '        drDLActualDisb("IndexID") = IndexID
    '        GetStudentData(drDLActualDisb, OriginalSSN)
    '        drDLActualDisb("LoanId") = LoanId
    '        drDLActualDisb("DisbDate") = FormatDate(DisbDate)
    '        drDLActualDisb("DisbStatus") = DisbStatus
    '        drDLActualDisb("DisbNum") = DisbNum
    '        Try
    '            dbDouble = CType(GrossAmount, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drDLActualDisb("GrossAmount") = dbDouble.ToString("0.00")
    '        Try
    '            dbDouble = CType(Fees, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drDLActualDisb("Fees") = dbDouble.ToString("0.00")
    '        Try
    '            dbDouble = CType(NetAmount, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drDLActualDisb("NetAmount") = dbDouble.ToString("0.00")
    '        dtDLActualDisb.Rows.Add(drDLActualDisb)
    '        Session("dtDLActualDisb") = dtDLActualDisb
    '    End If
    'End Sub

    'Private Function GetStudentData(ByVal dr As DataRow, ByVal OriginalSSN As String) As Boolean
    '      Dim db As New DataAccess
    '      ''Dim strSQL As String = "SELECT S.LastName, S.FirstName, S.SSN FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId Where S.SSN='" + OriginalSSN + "' AND SC.SysStatusId=9 "
    '      Dim strSQL As String = "SELECT TOP 1 S.LastName, S.FirstName, S.SSN, C.CampDescrip FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syCampuses C ON SE.CampusId=C.CampusId Where S.SSN='" + OriginalSSN + "'  order by EnrollDate Desc "
    '      Dim dsTemp As DataSet = db.RunSQLDataSet(strSQL, "StudentInfo")
    '      If dsTemp.Tables("StudentInfo").Rows.Count > 0 Then
    '          dr("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName").ToString
    '          dr("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName").ToString
    '          If OriginalSSN.Length = 9 Then
    '              dr("SSN") = FormatSSN(OriginalSSN)
    '          Else
    '              dr("SSN") = OriginalSSN
    '          End If
    '          dr("Campus") = dsTemp.Tables("StudentInfo").Rows(0)("CampDescrip").ToString
    '          Return True
    '      Else
    '          dr("LastName") = ""
    '          dr("FirstName") = ""
    '          If OriginalSSN.Length = 9 Then
    '              dr("SSN") = FormatSSN(OriginalSSN)
    '          Else
    '              dr("SSN") = OriginalSSN
    '          End If

    '          Return False
    '      End If
    '  End Function
    Public Function FormatDate(ByRef DateToFormat As String) As String
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String
        Try
            strYYYY = DateToFormat.Substring(0, 4)
            strMM = DateToFormat.Substring(4, 2)
            strDD = DateToFormat.Substring(6, 2)
            Return strMM + "/" + strDD + "/" + strYYYY
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return DateToFormat
        End Try

    End Function
    Public Function FormatTime(ByRef TimeToFormat As String) As String
        Dim strHH As String
        Dim strMM As String
        Dim strSS As String
        Try
            strHH = TimeToFormat.Substring(0, 2)
            strMM = TimeToFormat.Substring(2, 2)
            strSS = TimeToFormat.Substring(4, 2)
            Return strHH + ":" + strMM + ":" + strSS
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return TimeToFormat
        End Try
    End Function
    Public Function FormatSSN(ByRef SSNToFormat As String) As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Return facInputMasks.ApplyMask(strSSNMask, "*****" & SSNToFormat.Substring(5))
    End Function
    'Private Sub BuildDLSchDisb(ByVal IndexID As Integer, ByVal ParentIndexID As Integer, ByVal OriginalSSN As String, ByVal LoanId As String, ByVal DisbDate As String, ByVal MPNStatus As String, ByVal DisbNum As String, ByVal GrossAmount As String, ByVal Fees As String, ByVal NetAmount As String)
    '    If Not OriginalSSN = "" Then
    '        Dim drDLSchDisb As DataRow
    '        '            Dim drDLChild As DataRow
    '        Dim dbDouble As Double
    '        Try
    '            Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
    '            Dim dcLastName As New DataColumn("LastName", GetType(String))
    '            Dim dcFirstName As New DataColumn("FirstName", GetType(String))
    '            Dim dcSSN As New DataColumn("SSN", GetType(String))
    '            Dim dcCampus As New DataColumn("Campus", GetType(String))
    '            Dim dcLoanId As New DataColumn("LoanId", GetType(String))
    '            Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
    '            Dim dcMPNStatus As New DataColumn("MPNStatus", GetType(String))
    '            Dim dcDisbNum As New DataColumn("DisbNum", GetType(String))
    '            Dim dcGrossAmount As New DataColumn("GrossAmount", GetType(String))
    '            Dim dcFees As New DataColumn("Fees", GetType(String))
    '            Dim dcNetAmount As New DataColumn("NetAmount", GetType(String))

    '            dtDLSchDisb.Columns.Add(dcIndexID)
    '            dtDLSchDisb.Columns.Add(dcLastName)
    '            dtDLSchDisb.Columns.Add(dcFirstName)
    '            dtDLSchDisb.Columns.Add(dcSSN)
    '            dtDLSchDisb.Columns.Add(dcCampus)
    '            dtDLSchDisb.Columns.Add(dcLoanId)
    '            dtDLSchDisb.Columns.Add(dcDisbDate)
    '            dtDLSchDisb.Columns.Add(dcMPNStatus)
    '            dtDLSchDisb.Columns.Add(dcDisbNum)
    '            dtDLSchDisb.Columns.Add(dcGrossAmount)
    '            dtDLSchDisb.Columns.Add(dcFees)
    '            dtDLSchDisb.Columns.Add(dcNetAmount)
    '        Catch ex As System.Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '        End Try

    '        drDLSchDisb = dtDLSchDisb.NewRow()

    '        drDLSchDisb("IndexID") = IndexID
    '        GetStudentData(drDLSchDisb, OriginalSSN)
    '        drDLSchDisb("LoanId") = LoanId
    '        drDLSchDisb("DisbDate") = FormatDate(DisbDate)
    '        drDLSchDisb("MPNStatus") = MPNStatus
    '        drDLSchDisb("DisbNum") = DisbNum
    '        Try
    '            dbDouble = CType(GrossAmount, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drDLSchDisb("GrossAmount") = dbDouble.ToString("0.00")
    '        Try
    '            dbDouble = CType(Fees, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drDLSchDisb("Fees") = dbDouble.ToString("0.00")
    '        Try
    '            dbDouble = CType(NetAmount, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drDLSchDisb("NetAmount") = dbDouble.ToString("0.00")
    '        dtDLSchDisb.Rows.Add(drDLSchDisb)
    '        Session("dtDLSchDisb") = dtDLSchDisb
    '    End If
    'End Sub
    'Private Sub BuildPATSData(ByVal IndexID As Integer, ByVal AwardAmtYr As String, ByVal AwardId As String, ByVal GrantType As String, ByVal AddDate As String, ByVal AddTime As String, ByVal OriginalSSN As String, ByVal AcedamicYearId As String, ByVal AwardStartDate As String, ByVal AwardEndDate As String)
    '    If Not OriginalSSN = "" Then
    '        Dim drPATS As DataRow
    '        '            Dim drPellParent As DataRow
    '        Dim dbDouble As Double
    '        Try
    '            Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
    '            Dim dcLastName As New DataColumn("LastName", GetType(String))
    '            Dim dcFirstName As New DataColumn("FirstName", GetType(String))
    '            Dim dcSSN As New DataColumn("SSN", GetType(String))
    '            Dim dcSSNOriginal As New DataColumn("SSNOriginal", GetType(String))
    '            Dim dcCampus As New DataColumn("Campus", GetType(String))
    '            Dim dcGrantType As New DataColumn("GrantType", GetType(String))
    '            Dim dcAwardId As New DataColumn("AwardId", GetType(String))
    '            Dim dcAddDate As New DataColumn("AddDate", GetType(String))
    '            Dim dcAddTime As New DataColumn("AddTime", GetType(String))
    '            Dim dcAwardAmount As New DataColumn("AwardAmount", GetType(String))
    '            Dim dcAcademicYearId As New DataColumn("AcademicYearId", GetType(String))
    '            Dim dcAwardStartDate As New DataColumn("AwardStartDate", GetType(String))
    '            Dim dcAwardEndDate As New DataColumn("AwardEndDate", GetType(String))
    '            Dim dcErrorType As New DataColumn("ErrorType", GetType(Integer))

    '            dtPATSData.Columns.Add(dcIndexID)
    '            dtPATSData.Columns.Add(dcLastName)
    '            dtPATSData.Columns.Add(dcFirstName)
    '            dtPATSData.Columns.Add(dcSSN)
    '            dtPATSData.Columns.Add(dcSSNOriginal)
    '            dtPATSData.Columns.Add(dcCampus)
    '            dtPATSData.Columns.Add(dcGrantType)
    '            dtPATSData.Columns.Add(dcAwardId)
    '            dtPATSData.Columns.Add(dcAddDate)
    '            dtPATSData.Columns.Add(dcAddTime)
    '            dtPATSData.Columns.Add(dcAwardAmount)
    '            dtPATSData.Columns.Add(dcAcademicYearId)
    '            dtPATSData.Columns.Add(dcAwardStartDate)
    '            dtPATSData.Columns.Add(dcAwardEndDate)
    '            dtPATSData.Columns.Add(dcErrorType)
    '        Catch ex As System.Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '        End Try

    '        drPATS = dtPATSData.NewRow()

    '        drPATS("IndexID") = IndexID
    '        drPATS("SSNOriginal") = OriginalSSN
    '        GetStudentData(drPATS, OriginalSSN)
    '        Select Case GrantType
    '            Case "", "P"
    '                drPATS("GrantType") = "Pell"
    '            Case "A"
    '                drPATS("GrantType") = "ACG"
    '            Case "S", "T"
    '                drPATS("GrantType") = "SMART"
    '            Case "TT"
    '                drPATS("GrantType") = "TEACH"
    '        End Select
    '        drPATS("AwardId") = AwardId
    '        drPATS("AddDate") = FormatDate(AddDate)
    '        drPATS("AddTime") = FormatTime(AddTime)
    '        Try
    '            dbDouble = CType(AwardAmtYr, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drPATS("AwardAmount") = dbDouble.ToString("0.00")
    '        drPATS("AcademicYearId") = AcedamicYearId
    '        drPATS("AwardStartDate") = AwardStartDate
    '        drPATS("AwardEndDate") = AwardEndDate
    '        drPATS("ErrorType") = 0
    '        dtPATSData.Rows.Add(drPATS)
    '        Session("dtPATSData") = dtPATSData
    '    End If
    'End Sub
    'Private Sub BuildPATSDisb(ByVal IndexID As Integer, ByVal ParentIndexID As Integer, ByVal OriginalSSN As String, ByVal AwardId As String, ByVal DisbDate As String, ByVal DisbRelIndi As String, ByVal ActionStatus As String, ByVal DisbNum As String, ByVal SeqNum As String, ByVal SubDisbAmt As String, ByVal AccDisbAmt As String)
    '    If Not OriginalSSN = "" Then
    '        Dim drPATSDisb As DataRow
    '        '            Dim drPellChild As DataRow
    '        Dim dbDouble As Double
    '        Try
    '            Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
    '            Dim dcParentIndexID As New DataColumn("ParentIndexID", GetType(Integer))
    '            Dim dcLastName As New DataColumn("LastName", GetType(String))
    '            Dim dcFirstName As New DataColumn("FirstName", GetType(String))
    '            Dim dcSSN As New DataColumn("SSN", GetType(String))
    '            Dim dcSSNOrg As New DataColumn("SSNOriginal", GetType(String))
    '            Dim dcCampus As New DataColumn("Campus", GetType(String))
    '            Dim dcAwardId As New DataColumn("AwardId", GetType(String))
    '            Dim dcDisbDate As New DataColumn("DisbDate", GetType(String))
    '            Dim dcDisbRelInd As New DataColumn("DisbRelInd", GetType(String))
    '            Dim dcActionStatus As New DataColumn("ActionStatus", GetType(String))
    '            Dim dcDisbNum As New DataColumn("DisbNum", GetType(String))
    '            Dim dcSeqNum As New DataColumn("SeqNum", GetType(String))
    '            Dim dcSubDisbAmt As New DataColumn("SubDisbAmt", GetType(String))
    '            Dim dcAccDisbAmt As New DataColumn("AccDisbAmt", GetType(String))

    '            dtPATSDisb.Columns.Add(dcIndexID)
    '            dtPATSDisb.Columns.Add(dcParentIndexID)
    '            dtPATSDisb.Columns.Add(dcLastName)
    '            dtPATSDisb.Columns.Add(dcFirstName)
    '            dtPATSDisb.Columns.Add(dcSSN)
    '            dtPATSDisb.Columns.Add(dcSSNOrg)
    '            dtPATSDisb.Columns.Add(dcCampus)
    '            dtPATSDisb.Columns.Add(dcAwardId)
    '            dtPATSDisb.Columns.Add(dcDisbDate)
    '            dtPATSDisb.Columns.Add(dcDisbRelInd)
    '            dtPATSDisb.Columns.Add(dcActionStatus)
    '            dtPATSDisb.Columns.Add(dcDisbNum)
    '            dtPATSDisb.Columns.Add(dcSeqNum)
    '            dtPATSDisb.Columns.Add(dcSubDisbAmt)
    '            dtPATSDisb.Columns.Add(dcAccDisbAmt)
    '        Catch ex As System.Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '        End Try

    '        drPATSDisb = dtPATSDisb.NewRow()

    '        drPATSDisb("IndexID") = IndexID
    '        drPATSDisb("SSNOriginal") = OriginalSSN
    '        GetStudentData(drPATSDisb, OriginalSSN)
    '        drPATSDisb("ParentIndexID") = ParentIndexID
    '        drPATSDisb("AwardId") = AwardId
    '        drPATSDisb("DisbDate") = FormatDate(DisbDate)
    '        drPATSDisb("DisbRelInd") = DisbRelIndi
    '        drPATSDisb("ActionStatus") = ActionStatus
    '        drPATSDisb("DisbNum") = DisbNum
    '        drPATSDisb("SeqNum") = SeqNum
    '        Try
    '            dbDouble = CType(SubDisbAmt, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drPATSDisb("SubDisbAmt") = dbDouble.ToString("0.00")
    '        Try
    '            dbDouble = CType(AccDisbAmt, Double)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbDouble = 0.0
    '        End Try
    '        drPATSDisb("AccDisbAmt") = dbDouble.ToString("0.00")
    '        dtPATSDisb.Rows.Add(drPATSDisb)
    '        Session("dtPATSDisb") = dtPATSDisb
    '    End If
    'End Sub
    'Private Sub BuildDirectLoan(ByVal IndexID As Integer, ByVal OriginalSSN As String, ByVal LoanType As String, ByVal LoanId As String, ByVal AddDate As String, ByVal AddTime As String, ByVal AwardAmount As String, ByVal FeesPer As String, ByVal strRebate As String, ByVal strLoanStartDate As String, ByVal strLoanEndDate As String)
    '    If Not OriginalSSN = "" Then
    '        Dim drDirectLoan As DataRow
    '        '            Dim drDLParent As DataRow
    '        Try
    '            Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
    '            Dim dcLastName As New DataColumn("LastName", GetType(String))
    '            Dim dcFirstName As New DataColumn("FirstName", GetType(String))
    '            Dim dcSSN As New DataColumn("SSN", GetType(String))
    '            Dim dcCampus As New DataColumn("Campus", GetType(String))
    '            Dim dcLoanType As New DataColumn("LoanType", GetType(String))
    '            Dim dcLoanId As New DataColumn("LoanId", GetType(String))
    '            Dim dcAddDate As New DataColumn("AddDate", GetType(String))
    '            Dim dcAddTime As New DataColumn("AddTime", GetType(String))
    '            Dim dcAwardAmount As New DataColumn("AwardAmount", GetType(String))
    '            Dim dcFees As New DataColumn("Fees", GetType(String))
    '            Dim dcNetAmount As New DataColumn("NetAmount", GetType(String))
    '            Dim dcLoanStartDate As New DataColumn("LoanStartDate", GetType(String))
    '            Dim dcLoanEndDate As New DataColumn("LoanEndDate", GetType(String))

    '            dtDirectLoan.Columns.Add(dcIndexID)
    '            dtDirectLoan.Columns.Add(dcLastName)
    '            dtDirectLoan.Columns.Add(dcFirstName)
    '            dtDirectLoan.Columns.Add(dcSSN)
    '            dtDirectLoan.Columns.Add(dcCampus)
    '            dtDirectLoan.Columns.Add(dcLoanType)
    '            dtDirectLoan.Columns.Add(dcLoanId)
    '            dtDirectLoan.Columns.Add(dcAddDate)
    '            dtDirectLoan.Columns.Add(dcAddTime)
    '            dtDirectLoan.Columns.Add(dcAwardAmount)
    '            dtDirectLoan.Columns.Add(dcFees)
    '            dtDirectLoan.Columns.Add(dcNetAmount)
    '            dtDirectLoan.Columns.Add(dcLoanStartDate)
    '            dtDirectLoan.Columns.Add(dcLoanEndDate)
    '        Catch ex As System.Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '        End Try

    '        drDirectLoan = dtDirectLoan.NewRow()

    '        drDirectLoan("IndexID") = IndexID
    '        GetStudentData(drDirectLoan, OriginalSSN)
    '        If LoanType.ToUpper = "P" Then
    '            drDirectLoan("LoanType") = "DL-PLUS"
    '        ElseIf LoanType.ToUpper = "S" Then
    '            drDirectLoan("LoanType") = "DL-SUB"
    '        ElseIf LoanType.ToUpper = "U" Then
    '            drDirectLoan("LoanType") = "DL-UNSUB"
    '        Else
    '            drDirectLoan("LoanType") = ""
    '        End If
    '        drDirectLoan("LoanId") = LoanId
    '        drDirectLoan("AddDate") = FormatDate(AddDate)
    '        drDirectLoan("AddTime") = FormatTime(AddTime)
    '        Dim dbFeesPer As Double
    '        Dim dbRebate As Double
    '        Dim dbAwardAmount As Double
    '        Try
    '            dbFeesPer = Convert.ToDouble(FeesPer)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbFeesPer = 0.0
    '        End Try
    '        Try
    '            dbAwardAmount = Convert.ToDouble(AwardAmount)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbAwardAmount = 0.0
    '        End Try
    '        Try
    '            dbRebate = Convert.ToDouble(strRebate)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            dbRebate = 0.0
    '        End Try
    '        Dim dbFeesAmount As Double = (dbFeesPer * dbAwardAmount) / 100
    '        drDirectLoan("AwardAmount") = dbAwardAmount.ToString("0.00")
    '        drDirectLoan("Fees") = Math.Round(dbFeesAmount).ToString()
    '        drDirectLoan("NetAmount") = (dbAwardAmount - Math.Round(dbFeesAmount)).ToString("0.00")
    '        drDirectLoan("LoanStartDate") = FormatDate(strLoanStartDate)
    '        drDirectLoan("LoanEndDate") = FormatDate(strLoanEndDate)
    '        dtDirectLoan.Rows.Add(drDirectLoan)
    '        Session("dtDirectLoan") = dtDirectLoan
    '    End If
    'End Sub

    Public Sub UpdateNetAmount(ByVal intDLID As Integer, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByVal RebateAmount As String, ByRef dbTotalRebateAmount As Double)
        Dim dbTRAmt As Double
        Dim dbARAmt As Double
        Dim dbNetAmt As Double
        Try
            dbARAmt = Convert.ToDouble(RebateAmount)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            dbARAmt = 0.0
        End Try
        dbTRAmt = dbARAmt + dbTotalRebateAmount

        For Each dr As DataRow In dtDirectLoan.Rows
            If dr("IndexID").ToString = intDLID Then
                Try
                    dbNetAmt = Convert.ToDouble(dr("NetAmount").ToString)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    dbNetAmt = 0.0
                End Try
                dr("NetAmount") = (dbNetAmt + dbARAmt).ToString("0.00")
            End If
        Next
        myMsgCollection.Items(intDLID).strAnticipatedDisbursementInterestRebateAmount = dbTRAmt.ToString("0.00")
    End Sub

    Protected Sub ddlFileNames_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFileNames.SelectedIndexChanged
        If ddlFileNames.SelectedValue.ToString = "" Then
            'btnProcessData.Enabled = False
            'btnPreview.Enabled = False
            ClearAll()
            ClearDataGrid()
            Exit Sub
        Else
            Dim clsflFile As New EDFileInfo_New
            Dim str1 As String = String.Concat(MyAdvAppSettings.AppSettings("SourcePathEdExp"), ddlFileNames.SelectedItem.Text)
            If MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer").ToLower = "yes" Then
                str1 = String.Concat(MyAdvAppSettings.AppSettings("RemoteEDExpressIn"), ddlFileNames.SelectedItem.Text)
                clsflFile.datafromremotecomputer = True
                clsflFile.networkuser = MyAdvAppSettings.AppSettings("RemoteEDExpressUsername")
                clsflFile.networkPassword = MyAdvAppSettings.AppSettings("RemoteEDExpressPassword")
            End If
            clsflFile.strFileName = str1
            Session("clsflFile") = clsflFile
            If clsflFile.strMsgType.ToUpper = "PELL" Then
                tblAwardYear.Visible = True
                ddlAcademicYear.Enabled = True
                txtAwardStartDate.Enabled = True
                'hlAwardStartDate.Enabled = True
                txtAwardEndDate.Enabled = True
                'hlAwardEndDate.Enabled = True
                ClearDataGrid()
            ElseIf clsflFile.strMsgType.ToUpper = "DL" Then
                tblAwardYear.Visible = False
                ddlAcademicYear.Enabled = False
                ddlAcademicYear.SelectedValue = Guid.Empty.ToString
                'txtAwardStartDate.Text = String.Empty
                txtAwardStartDate.Clear()
                txtAwardStartDate.Enabled = False
                'hlAwardStartDate.Enabled = False
                'txtAwardEndDate.Text = String.Empty
                txtAwardEndDate.Clear()
                txtAwardEndDate.Enabled = False
                'hlAwardEndDate.Enabled = False
                ClearDataGrid()
            Else
                ddlAcademicYear.Enabled = False
                ddlAcademicYear.SelectedValue = Guid.Empty.ToString
                'txtAwardStartDate.Text = String.Empty
                txtAwardStartDate.Clear()
                txtAwardStartDate.Enabled = False
                'hlAwardStartDate.Enabled = False
                'txtAwardEndDate.Text = String.Empty
                txtAwardEndDate.Clear()
                txtAwardEndDate.Enabled = False
                'hlAwardEndDate.Enabled = False
            End If
            'ClearDataGrid()
            'btnPreview.Enabled = True
            'btnProcessData.Enabled = True
        End If
    End Sub
    Private Sub CreateDataTable()
        Try
            '' (PELL PARENT TABLE)
            Dim dcIndexID As New DataColumn("IndexID", GetType(Integer))
            Dim dcLastName As New DataColumn("LastName", GetType(String))
            Dim dcFirstName As New DataColumn("FirstName", GetType(String))
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcCampus As New DataColumn("Campus", GetType(String))
            Dim dcCampusId As New DataColumn("CampusId", GetType(String))
            Dim dcStuEnrollmentId As New DataColumn("StuEnrollmentId", GetType(String))
            Dim dcGrantType As New DataColumn("GrantType", GetType(String))
            Dim dcGrantTypeDesc As New DataColumn("GrantTypeDesc", GetType(String))
            Dim dcAwardId As New DataColumn("AwardId", GetType(String))
            Dim dcAddDate As New DataColumn("AddDate", GetType(String))
            Dim dcAddTime As New DataColumn("AddTime", GetType(String))
            Dim dcAwardAmount As New DataColumn("AwardAmount", GetType(Decimal))
            Dim dcAcademicYearId As New DataColumn("AcademicYearId", GetType(String))
            Dim dcAwardStartDate As New DataColumn("AwardStartDate", GetType(String))
            Dim dcAwardEndDate As New DataColumn("AwardEndDate", GetType(String))
            Dim dcErrorType As New DataColumn("ErrorType", GetType(Integer))
            Dim dcdbIndicator As New DataColumn("dbIndicator", GetType(String))
            Dim dcdbFilter As New DataColumn("dbFilter", GetType(String))
            Dim dcOriginationStatus As New DataColumn("OriginationStatus", GetType(String))
            Dim dcSSNOriginal As New DataColumn("SSNOriginal", GetType(String))
            Dim dcUpdatedDate As New DataColumn("UpdatedDate", GetType(String))
            Dim dcUpdatedTime As New DataColumn("UpdatedTime", GetType(String))
            'Dim dcIsOverrideAdvDateWithEDExpDate As New DataColumn("IsOverrideAdvDateWithEDExpDate", GetType(Boolean))
            'Dim dcIsOverrideAdvAmtWithEDExpAm As New DataColumn("IsOverrideAdvAmtWithEDExpAm", GetType(Boolean))
            'Dim dcIsPostPayment As New DataColumn("IsPostPayment", GetType(Boolean))
            'Dim dcIsPayOutOfSchoolStudent As New DataColumn("IsPayOutOfSchoolStudent", GetType(Boolean))
            'Dim dcIsUseExpDate As New DataColumn("IsUseExpDate", GetType(Boolean))
            'Dim dcSpecifiedDate As New DataColumn("SpecifiedDate", GetType(String))
            Dim dcIsParsed As New DataColumn("IsParsed", GetType(Integer))
            Dim dcIsInSchool As New DataColumn("IsInSchool", GetType(Boolean))

            dtPATSData.Columns.Add(dcIndexID)
            dtPATSData.Columns.Add(dcLastName)
            dtPATSData.Columns.Add(dcFirstName)
            dtPATSData.Columns.Add(dcSSN)
            dtPATSData.Columns.Add(dcCampus)
            dtPATSData.Columns.Add(dcGrantType)
            dtPATSData.Columns.Add(dcGrantTypeDesc)
            dtPATSData.Columns.Add(dcAwardId)
            dtPATSData.Columns.Add(dcAddDate)
            dtPATSData.Columns.Add(dcAddTime)
            dtPATSData.Columns.Add(dcAwardAmount)
            dtPATSData.Columns.Add(dcAcademicYearId)
            dtPATSData.Columns.Add(dcAwardStartDate)
            dtPATSData.Columns.Add(dcAwardEndDate)
            dtPATSData.Columns.Add(dcErrorType)
            dtPATSData.Columns.Add(dcdbIndicator)
            dtPATSData.Columns.Add(dcdbFilter)
            dtPATSData.Columns.Add(dcOriginationStatus)
            dtPATSData.Columns.Add(dcSSNOriginal)
            dtPATSData.Columns.Add(dcUpdatedDate)
            dtPATSData.Columns.Add(dcUpdatedTime)
            'dtPATSData.Columns.Add(dcIsOverrideAdvDateWithEDExpDate)
            'dtPATSData.Columns.Add(dcIsOverrideAdvAmtWithEDExpAm)
            'dtPATSData.Columns.Add(dcIsPostPayment)
            'dtPATSData.Columns.Add(dcIsPayOutOfSchoolStudent)
            'dtPATSData.Columns.Add(dcIsUseExpDate)
            'dtPATSData.Columns.Add(dcSpecifiedDate)
            dtPATSData.Columns.Add(dcIsParsed)
            dtPATSData.Columns.Add(dcStuEnrollmentId)
            dtPATSData.Columns.Add(dcCampusId)
            dtPATSData.Columns.Add(dcIsInSchool)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        '' (PELL CHILD TABLE)
        Try
            Dim dccIndexID As New DataColumn("IndexID", GetType(Integer))
            Dim dccParentIndexID As New DataColumn("ParentIndexID", GetType(Integer))
            Dim dccLastName As New DataColumn("LastName", GetType(String))
            Dim dccFirstName As New DataColumn("FirstName", GetType(String))
            Dim dccSSN As New DataColumn("SSN", GetType(String))
            Dim dccSSNOrg As New DataColumn("SSNOriginal", GetType(String))
            Dim dccStuenrollmentId As New DataColumn("StuenrollmentId", GetType(String))
            Dim dccCampusId As New DataColumn("CampusId", GetType(String))
            Dim dccCampus As New DataColumn("Campus", GetType(String))
            Dim dccAwardId As New DataColumn("AwardId", GetType(String))
            Dim dccDisbDate As New DataColumn("DisbDate", GetType(String))
            Dim dccDisbRelInd As New DataColumn("DisbRelInd", GetType(String))
            Dim dccActionStatus As New DataColumn("ActionStatus", GetType(String))
            Dim dccDisbNum As New DataColumn("DisbNum", GetType(String))
            Dim dccSeqNum As New DataColumn("SeqNum", GetType(String))
            Dim dccSubDisbAmt As New DataColumn("SubDisbAmt", GetType(Decimal))
            Dim dccAccDisbAmt As New DataColumn("AccDisbAmt", GetType(Decimal))
            Dim dccdbIndicator As New DataColumn("dbIndicator", GetType(String))
            Dim dccAcademicYrId As New DataColumn("AcademicYrId", GetType(String))
            Dim dccAwardYrStartDate As New DataColumn("AwardYrStartDate", GetType(String))
            Dim dccAwardYrEndDate As New DataColumn("AwardYrEndDate", GetType(String))
            Dim dccErrorType As New DataColumn("ErrorType", GetType(Integer))
            Dim dccShow As New DataColumn("Show", GetType(Integer))
            'Dim dccIsOverrideAdvDateWithEDExpDate As New DataColumn("IsOverrideAdvDateWithEDExpDate", GetType(Boolean))
            'Dim dccIsOverrideAdvAmtWithEDExpAmt As New DataColumn("IsOverrideAdvAmtWithEDExpAmt", GetType(Boolean))
            'Dim dccIsPostPayment As New DataColumn("IsPostPayment", GetType(Boolean))
            'Dim dccIsPayOutOfSchoolStudent As New DataColumn("IsPayOutOfSchoolStudent", GetType(Boolean))
            'Dim dccIsUseExpDate As New DataColumn("IsUseExpDate", GetType(Boolean))
            'Dim dccSpecifiedDate As New DataColumn("SpecifiedDate", GetType(String))
            Dim dccIsInSchool As New DataColumn("IsInSchool", GetType(Boolean))
            Dim dccIsParsed As New DataColumn("IsParsed", GetType(Integer))
            Dim dccFilter As New DataColumn("dbFilter", GetType(String))

            dtPATSDisb.Columns.Add(dccIndexID)
            dtPATSDisb.Columns.Add(dccParentIndexID)
            dtPATSDisb.Columns.Add(dccLastName)
            dtPATSDisb.Columns.Add(dccFirstName)
            dtPATSDisb.Columns.Add(dccSSN)
            dtPATSDisb.Columns.Add(dccSSNOrg)
            dtPATSDisb.Columns.Add(dccCampus)
            dtPATSDisb.Columns.Add(dccAwardId)
            dtPATSDisb.Columns.Add(dccDisbDate)
            dtPATSDisb.Columns.Add(dccDisbRelInd)
            dtPATSDisb.Columns.Add(dccActionStatus)
            dtPATSDisb.Columns.Add(dccDisbNum)
            dtPATSDisb.Columns.Add(dccSeqNum)
            dtPATSDisb.Columns.Add(dccSubDisbAmt)
            dtPATSDisb.Columns.Add(dccAccDisbAmt)
            dtPATSDisb.Columns.Add(dccdbIndicator)
            dtPATSDisb.Columns.Add(dccAcademicYrId)
            dtPATSDisb.Columns.Add(dccAwardYrStartDate)
            dtPATSDisb.Columns.Add(dccAwardYrEndDate)
            'dtPATSDisb.Columns.Add(dccIsOverrideAdvDateWithEDExpDate)
            'dtPATSDisb.Columns.Add(dccIsOverrideAdvAmtWithEDExpAmt)
            'dtPATSDisb.Columns.Add(dccIsPostPayment)
            'dtPATSDisb.Columns.Add(dccIsPayOutOfSchoolStudent)
            'dtPATSDisb.Columns.Add(dccIsUseExpDate)
            'dtPATSDisb.Columns.Add(dccSpecifiedDate)
            dtPATSDisb.Columns.Add(dccIsParsed)
            dtPATSDisb.Columns.Add(dccIsInSchool)
            dtPATSDisb.Columns.Add(dccStuenrollmentId)
            dtPATSDisb.Columns.Add(dccCampusId)
            dtPATSDisb.Columns.Add(dccFilter)
            dtPATSDisb.Columns.Add(dccErrorType)
            dtPATSDisb.Columns.Add(dccShow)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        Session("dtPATSData") = dtPATSData
        Session("dtPATSDisb") = dtPATSDisb

        Try
            '' (DL PARENT TABLE)
            Dim dcDLIndexID As New DataColumn("IndexID", GetType(Integer))
            Dim dcDLLastName As New DataColumn("LastName", GetType(String))
            Dim dcDLFirstName As New DataColumn("FirstName", GetType(String))
            Dim dcDLSSN As New DataColumn("SSN", GetType(String))
            Dim dcDLCampus As New DataColumn("Campus", GetType(String))
            Dim dcDLCampusId As New DataColumn("CampusId", GetType(String))
            Dim dcDLStuEnrollmentId As New DataColumn("StuEnrollmentId", GetType(String))
            Dim dcDLLoanType As New DataColumn("LoanType", GetType(String))
            Dim dcDLLoanTypeDesc As New DataColumn("LoanTypeDesc", GetType(String))
            Dim dcDLLoanId As New DataColumn("LoanId", GetType(String))
            Dim dcDLAddDate As New DataColumn("AddDate", GetType(String))
            Dim dcDLAddTime As New DataColumn("AddTime", GetType(String))
            Dim dcDLGrossAmount As New DataColumn("GrossAmount", GetType(String))
            Dim dcDLFees As New DataColumn("Fees", GetType(String))
            Dim dcDLNetAmount As New DataColumn("NetAmount", GetType(String))
            Dim dcDLAcademicYearId As New DataColumn("AcademicYearId", GetType(String))
            Dim dcDLLoanStartDate As New DataColumn("LoanStartDate", GetType(String))
            Dim dcDLLoanEndDate As New DataColumn("LoanEndDate", GetType(String))
            Dim dcDLErrorType As New DataColumn("ErrorType", GetType(Integer))
            Dim dcDLIndicator As New DataColumn("dbIndicator", GetType(String))
            Dim dcDLFilter As New DataColumn("dbFilter", GetType(String))
            Dim dcDLSSNOriginal As New DataColumn("SSNOriginal", GetType(String))
            Dim dcDLUpdatedDate As New DataColumn("UpdatedDate", GetType(String))
            Dim dcDLUpdatedTime As New DataColumn("UpdatedTime", GetType(String))
            Dim dcDLIsParsed As New DataColumn("IsParsed", GetType(Integer))
            Dim dcDLIsInSchool As New DataColumn("IsInSchool", GetType(Boolean))

            dtDirectLoan.Columns.Add(dcDLIndexID)
            dtDirectLoan.Columns.Add(dcDLLastName)
            dtDirectLoan.Columns.Add(dcDLFirstName)
            dtDirectLoan.Columns.Add(dcDLSSN)
            dtDirectLoan.Columns.Add(dcDLCampus)
            dtDirectLoan.Columns.Add(dcDLCampusId)
            dtDirectLoan.Columns.Add(dcDLStuEnrollmentId)
            dtDirectLoan.Columns.Add(dcDLLoanType)
            dtDirectLoan.Columns.Add(dcDLLoanTypeDesc)
            dtDirectLoan.Columns.Add(dcDLLoanId)
            dtDirectLoan.Columns.Add(dcDLAddDate)
            dtDirectLoan.Columns.Add(dcDLAddTime)
            dtDirectLoan.Columns.Add(dcDLGrossAmount)
            dtDirectLoan.Columns.Add(dcDLFees)
            dtDirectLoan.Columns.Add(dcDLNetAmount)
            dtDirectLoan.Columns.Add(dcDLAcademicYearId)
            dtDirectLoan.Columns.Add(dcDLLoanStartDate)
            dtDirectLoan.Columns.Add(dcDLLoanEndDate)
            dtDirectLoan.Columns.Add(dcDLErrorType)
            dtDirectLoan.Columns.Add(dcDLIndicator)
            dtDirectLoan.Columns.Add(dcDLFilter)
            dtDirectLoan.Columns.Add(dcDLSSNOriginal)
            dtDirectLoan.Columns.Add(dcDLUpdatedDate)
            dtDirectLoan.Columns.Add(dcDLUpdatedTime)
            dtDirectLoan.Columns.Add(dcDLIsParsed)
            dtDirectLoan.Columns.Add(dcDLIsInSchool)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        '' (DL CHILD SCHEDULE DISBURSEMENT TABLE)
        Try
            Dim dcDLSDIndexID As New DataColumn("IndexID", GetType(Integer))
            Dim dcDLSDParentIndexID As New DataColumn("ParentIndexID", GetType(Integer))
            Dim dcDLSDLastName As New DataColumn("LastName", GetType(String))
            Dim dcDLSDFirstName As New DataColumn("FirstName", GetType(String))
            Dim dcDLSDSSN As New DataColumn("SSN", GetType(String))
            Dim dcDLSDSSNOrg As New DataColumn("SSNOriginal", GetType(String))
            Dim dcDLSDStuenrollmentId As New DataColumn("StuenrollmentId", GetType(String))
            Dim dcDLSDCampusId As New DataColumn("CampusId", GetType(String))
            Dim dcDLSDCampus As New DataColumn("Campus", GetType(String))
            Dim dcDLSDLoanId As New DataColumn("LoanId", GetType(String))
            Dim dcDLSDDisbDate As New DataColumn("DisbDate", GetType(String))
            Dim dcDLSDDisbRelInd As New DataColumn("DisbRelInd", GetType(String))
            Dim dcDLSDDisbNum As New DataColumn("DisbNum", GetType(String))
            Dim dcDLSDSeqNum As New DataColumn("SeqNum", GetType(String))
            Dim dcDLSDGrossAmt As New DataColumn("GrossAmt", GetType(String))
            Dim dcDLSDFees As New DataColumn("Fees", GetType(String))
            Dim dcDLSDNetAmt As New DataColumn("NetAmt", GetType(String))
            Dim dcDLSDdbIndicator As New DataColumn("dbIndicator", GetType(String))
            Dim dcDLSDAcademicYrId As New DataColumn("AcademicYrId", GetType(String))
            Dim dcDLSDErrorType As New DataColumn("ErrorType", GetType(Integer))
            Dim dcDLSDShow As New DataColumn("Show", GetType(Integer))
            Dim dcDLSDIsInSchool As New DataColumn("IsInSchool", GetType(Boolean))
            Dim dcDLSDIsParsed As New DataColumn("IsParsed", GetType(Integer))
            Dim dcDLSDFilter As New DataColumn("dbFilter", GetType(String))

            dtDLSchDisb.Columns.Add(dcDLSDIndexID)
            dtDLSchDisb.Columns.Add(dcDLSDParentIndexID)
            dtDLSchDisb.Columns.Add(dcDLSDLastName)
            dtDLSchDisb.Columns.Add(dcDLSDFirstName)
            dtDLSchDisb.Columns.Add(dcDLSDSSN)
            dtDLSchDisb.Columns.Add(dcDLSDSSNOrg)
            dtDLSchDisb.Columns.Add(dcDLSDStuenrollmentId)
            dtDLSchDisb.Columns.Add(dcDLSDCampus)
            dtDLSchDisb.Columns.Add(dcDLSDCampusId)
            dtDLSchDisb.Columns.Add(dcDLSDLoanId)
            dtDLSchDisb.Columns.Add(dcDLSDDisbDate)
            dtDLSchDisb.Columns.Add(dcDLSDDisbRelInd)
            dtDLSchDisb.Columns.Add(dcDLSDDisbNum)
            dtDLSchDisb.Columns.Add(dcDLSDSeqNum)
            dtDLSchDisb.Columns.Add(dcDLSDGrossAmt)
            dtDLSchDisb.Columns.Add(dcDLSDFees)
            dtDLSchDisb.Columns.Add(dcDLSDNetAmt)
            dtDLSchDisb.Columns.Add(dcDLSDdbIndicator)
            dtDLSchDisb.Columns.Add(dcDLSDAcademicYrId)
            dtDLSchDisb.Columns.Add(dcDLSDErrorType)
            dtDLSchDisb.Columns.Add(dcDLSDShow)
            dtDLSchDisb.Columns.Add(dcDLSDIsInSchool)
            dtDLSchDisb.Columns.Add(dcDLSDIsParsed)
            dtDLSchDisb.Columns.Add(dcDLSDFilter)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        '' (DL CHILD ACTUAL DISBURSEMENT TABLE)
        Try
            Dim dcDLADIndexID As New DataColumn("IndexID", GetType(Integer))
            Dim dcDLADParentIndexID As New DataColumn("ParentIndexID", GetType(Integer))
            Dim dcDLADLastName As New DataColumn("LastName", GetType(String))
            Dim dcDLADFirstName As New DataColumn("FirstName", GetType(String))
            Dim dcDLADSSN As New DataColumn("SSN", GetType(String))
            Dim dcDLADSSNOrg As New DataColumn("SSNOriginal", GetType(String))
            Dim dcDLADStuenrollmentId As New DataColumn("StuEnrollmentId", GetType(String))
            Dim dcDLADCampusId As New DataColumn("CampusId", GetType(String))
            Dim dcDLADCampus As New DataColumn("Campus", GetType(String))
            Dim dcDLADLoanId As New DataColumn("LoanId", GetType(String))
            Dim dcDLADDisbDate As New DataColumn("DisbDate", GetType(String))
            Dim dcDLADDisbRelInd As New DataColumn("DisbRelInd", GetType(String))
            Dim dcDLADDisbNum As New DataColumn("DisbNum", GetType(String))
            Dim dcDLADSeqNum As New DataColumn("SeqNum", GetType(String))
            Dim dcDLADGrossAmt As New DataColumn("GrossAmt", GetType(Decimal))
            Dim dcDLADFees As New DataColumn("Fees", GetType(Decimal))
            Dim dcDLADNetAmt As New DataColumn("NetAmt", GetType(Decimal))
            Dim dcDLADdbIndicator As New DataColumn("dbIndicator", GetType(String))
            Dim dcDLADAcademicYrId As New DataColumn("AcademicYrId", GetType(String))
            Dim dcDLADErrorType As New DataColumn("ErrorType", GetType(Integer))
            Dim dcDLADShow As New DataColumn("Show", GetType(Integer))
            Dim dcDLADIsInSchool As New DataColumn("IsInSchool", GetType(Boolean))
            Dim dcDLADIsParsed As New DataColumn("IsParsed", GetType(Integer))
            Dim dcDLADFilter As New DataColumn("dbFilter", GetType(String))

            dtDLActualDisb.Columns.Add(dcDLADIndexID)
            dtDLActualDisb.Columns.Add(dcDLADParentIndexID)
            dtDLActualDisb.Columns.Add(dcDLADLastName)
            dtDLActualDisb.Columns.Add(dcDLADFirstName)
            dtDLActualDisb.Columns.Add(dcDLADSSN)
            dtDLActualDisb.Columns.Add(dcDLADSSNOrg)
            dtDLActualDisb.Columns.Add(dcDLADStuenrollmentId)
            dtDLActualDisb.Columns.Add(dcDLADCampus)
            dtDLActualDisb.Columns.Add(dcDLADCampusId)
            dtDLActualDisb.Columns.Add(dcDLADLoanId)
            dtDLActualDisb.Columns.Add(dcDLADDisbDate)
            dtDLActualDisb.Columns.Add(dcDLADDisbRelInd)
            dtDLActualDisb.Columns.Add(dcDLADDisbNum)
            dtDLActualDisb.Columns.Add(dcDLADSeqNum)
            dtDLActualDisb.Columns.Add(dcDLADGrossAmt)
            dtDLActualDisb.Columns.Add(dcDLADFees)
            dtDLActualDisb.Columns.Add(dcDLADNetAmt)
            dtDLActualDisb.Columns.Add(dcDLADdbIndicator)
            dtDLActualDisb.Columns.Add(dcDLADAcademicYrId)
            dtDLActualDisb.Columns.Add(dcDLADErrorType)
            dtDLActualDisb.Columns.Add(dcDLADShow)
            dtDLActualDisb.Columns.Add(dcDLADIsInSchool)
            dtDLActualDisb.Columns.Add(dcDLADIsParsed)
            dtDLActualDisb.Columns.Add(dcDLADFilter)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Session("dtDirectLoan") = dtDirectLoan
        Session("dtDLSchDisb") = dtDLSchDisb
        Session("dtDLActualDisb") = dtDLActualDisb


    End Sub
    Private Sub ClearDataGrid()

        tblPellRecords.Visible = False
        tblPellHeader.Visible = False
        tblPellException.Visible = False
        rgPellAward.Visible = False

        tblDLRecords.Visible = False
        tblDLRecords.Visible = False
        tblDLHeader.Visible = False
        rgDirectLoan.Visible = False

        Session("Pell_Message") = ""
        Session("DL_Message") = ""
        Session("dtLogMessage") = Nothing
        Session("dtPATSData") = Nothing
        Session("dtPATSDisb") = Nothing
        Session("dtDirectLoan") = Nothing
        Session("dtDLSchDisb") = Nothing
        Session("dtDLActualDisb") = Nothing

        'dgExceptionReportParent.Visible = False
        'dgExceptionReportChild.Visible = False
        'tblPellException.Visible = False
        'tblExpParent.Visible = False
        'tblExpChild.Visible = False

        Session("SortExp") = Nothing
        Session("OldSortOrder") = Nothing
        Session("ExpParent") = Nothing
        Session("ExpChild") = Nothing
    End Sub
    Private Sub ClearAll()

        Dim txtRefRecid As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtRefRecid"), TextBox)
        Dim ddlPaymentType As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlPaymentType"), DropDownList)

        'btnPreview.Enabled = False
        'btnProcessData.Enabled = False
        'txtAwardEndDate.Text = String.Empty
        'txtAwardStartDate.Text = String.Empty
        txtAwardStartDate.Clear()
        txtAwardEndDate.Clear()
        ddlFileNames.SelectedValue = ""
        ddlAcademicYear.SelectedValue = Guid.Empty.ToString
        ddlPaymentType.SelectedValue = "0"
        ddlPaymentType.Enabled = False
        txtRefRecid.Text = String.Empty
        txtRefRecid.Enabled = False
        ClearDataGrid()
        Session("IsProcess") = Nothing
        Session("Process") = Nothing
    End Sub
    'Private Sub ShowPATS()
    '    tblPellRecords.Visible = True
    '    tblPellHeader.Visible = True
    '    rgPellAward.Visible = True
    'End Sub

    'Private Sub ShowDL()
    '    'tbDL.Visible = True
    '    'dgDL.Visible = True

    '    'tbDLSDisb.Visible = True
    '    'dgDLSDisb.Visible = True

    '    'tbDLADisb.Visible = True
    '    'dgDLADisb.Visible = True
    'End Sub
    Public Function GetAcademicYear() As DataSet
        Dim dsAY As DataSet = GetActiveAcademicYears()
        Dim dr As DataRow = dsAY.Tables(0).NewRow()
        dr("AcademicYearId") = "00000000-0000-0000-0000-000000000000"
        dr("AcademicYearDescrip") = "Select"
        dsAY.Tables(0).Rows.InsertAt(dr, 0)
        Return dsAY
    End Function
    Private Sub SortPellGrids()
        If Not Session("dtPATSData") Is Nothing Then
            rgPellAward.Visible = True
            Dim dtPellAwardFinal As DataTable = Session("dtPATSData")
            Dim dtPellAwardScheduleFinal As DataTable = Session("dtPATSDisb")
            Dim drErrRecords() As DataRow = dtPellAwardFinal.Select("ErrorType<>0")
            Dim drErrRecordsSD() As DataRow = dtPellAwardScheduleFinal.Select("ErrorType<>0 And Show=1")
            Dim Awardamount As Object
            Dim SchedAmt As Object
            lblAwardCount.Text = "Total Number of Award Records : " + drErrRecords.Length.ToString
            lblDisbCount.Text = "Total Number of Disbursements Records : " + drErrRecordsSD.Length.ToString
            hfAS.Value = "Total Number of Award Records : " + drErrRecords.Length.ToString
            hfAD.Value = "Total Number of Disbursements Records : " + drErrRecordsSD.Length.ToString

            Awardamount = dtPellAwardFinal.Compute("Sum(AwardAmount)", "ErrorType<>0 and FirstName<>'' and LastName<>''")
            SchedAmt = dtPellAwardScheduleFinal.Compute("Sum(AccDisbAmt)", "ErrorType<>0 And Show=1 and FirstName<>'' and LastName<>''")
            hfAA.Value = "Total Amount of Awards: " + String.Format("{0:c}", Awardamount)
            hfSA.Value = "Total Amount of Disbursments: " + String.Format("{0:c}", SchedAmt)

            rgPellAward.Columns(0).Visible = True
            rgPellAward.MasterTableView.DataSource = drErrRecords
            rgPellAward.DataBind()
            rgPellAward.Columns(0).Visible = False

        End If
    End Sub
    Private Sub SortDLGrids()
        If Not Session("dtDirectLoan") Is Nothing Then
            rgPellAward.Visible = False
            rgDirectLoan.Visible = True

            Dim dtDLFinal As DataTable = Session("dtDirectLoan")
            Dim dtDLSDFinal As DataTable = Session("dtDLSchDisb")
            Dim dtDLADFinal As DataTable = Session("dtDLActualDisb")

            Dim drErrRecords() As DataRow = dtDLFinal.Select("ErrorType<>0")
            Dim drErrRecordsSD() As DataRow = dtDLSDFinal.Select("ErrorType<>0 And Show=1")
            Dim drErrRecordsAD() As DataRow = dtDLADFinal.Select("ErrorType<>0 And Show=1")

            rgDirectLoan.Columns(0).Visible = True
            rgDirectLoan.MasterTableView.DataSource = drErrRecords
            rgDirectLoan.DataBind()
            rgDirectLoan.Columns(0).Visible = False
            lblDLAcademicYear.Text = "Direct Loan Award Year : " + ddlAcademicYear.SelectedItem.Text
            lblDLCount.Text = "Total Number Of Award Records : " + drErrRecords.Length.ToString
            lblDLSDCount.Text = "Total Number Of Anticipated Disbursement Records : " + drErrRecordsSD.Length.ToString
            lblDLADCount.Text = "Total Number Of Actual Disbursement Records : " + drErrRecordsAD.Length.ToString
        End If
    End Sub
    Private Sub SortExceptionGirds()
        If Not Session("ExpParent") Is Nothing Then

            Dim dvEP As New DataView(Session("ExpParent"))
            'Dim dvEC As New DataView(Session("ExpChild"))
            If Session("MsgType").ToString.ToUpper = "PELL" Then
                rgPellException.Columns(0).Visible = True
                rgPellException.MasterTableView.DataSource = dvEP
                rgPellException.DataBind()
                rgPellException.Columns(0).Visible = False
            Else
                rgDLException.Columns(0).Visible = True
                rgDLException.MasterTableView.DataSource = dvEP
                rgDLException.DataBind()
                rgDLException.Columns(0).Visible = False
            End If

        End If
    End Sub

    Protected Sub rgPellAward_DetailTableDataBind(ByVal source As Object, ByVal e As GridDetailTableDataBindEventArgs) Handles rgPellAward.DetailTableDataBind
        Dim dvPellDisb As New DataView(Session("dtPATSDisb"))
        'Dim dvPellDisbRows As New DataView()
        Dim drSelectRows() As DataRow
        Dim dataItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)
        Select Case e.DetailTableView.Name
            Case "PellDisb"
                Dim IndexID As String = dataItem.GetDataKeyValue("IndexID").ToString()
                drSelectRows = dvPellDisb.Table.Select("ParentIndexID=" + IndexID + " And ErrorType<>0 And Show=1")
                e.DetailTableView.Columns(0).Visible = True
                e.DetailTableView.DataSource = drSelectRows
                e.DetailTableView.Columns(0).Visible = False
        End Select
    End Sub
    Protected Sub rgPellAward_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rgPellAward.ItemDataBound
        If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
            'Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            If e.Item.OwnerTableView.Name = "PellAward" Then
                Dim ddl As DropDownList
                Dim txtSD As TextBox
                Dim txtED As TextBox
                Dim hlSD As HyperLink
                Dim hlED As HyperLink
                ddl = CType(e.Item.FindControl("ddlAcademicYear"), DropDownList)
                ddl.SelectedValue = ddlAcademicYear.SelectedValue
                txtSD = CType(e.Item.FindControl("txtAYSD"), TextBox)
                txtED = CType(e.Item.FindControl("txtAYED"), TextBox)
                hlSD = CType(e.Item.FindControl("hlAYSD"), HyperLink)
                hlSD.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtSD.ClientID + "',true,1945)"
                hlED = CType(e.Item.FindControl("hlAYED"), HyperLink)
                hlED.NavigateUrl = "javascript:OpenCalendar('ActAssign','" + txtED.ClientID + "',true,1945)"
            End If
        End If
    End Sub
    Protected Sub rgPellAward_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles rgPellAward.NeedDataSource
        If Not e.IsFromDetailTable Then
            If Not Session("dtPATSData") Is Nothing Then
                Dim dtPellAwardFinal As DataTable = Session("dtPATSData")
                Dim dtPell As DataTable = DirectCast(dtPellAwardFinal.Clone, DataTable)
                Dim drErrRecords() As DataRow
                drErrRecords = dtPellAwardFinal.Select("ErrorType<>0")
                For Each dr As DataRow In drErrRecords
                    dtPell.ImportRow(dr)
                Next
                'Dim dvPellAward As New DataView(Session("dtPATSData"))
                rgPellAward.Columns(0).Visible = True
                rgPellAward.MasterTableView.DataSource = dtPell
                rgPellAward.Columns(0).Visible = False
            End If
        End If
    End Sub
    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        ClearDataGrid()
        CreateDataTable()
        Session("Process") = "0"
        ProcessData()
        Session("IsProcess") = "1"
    End Sub
    Protected Sub SaveProcessFile()
        Dim cxnString As String
        'myMsgCollection = Session("MessageCollection")
        Try
            'Dim nbrMessages As Integer = myMsgCollection.Count
            'Dim intNdx As Integer = 0
            'Dim fName As String = ""
            'Dim strLogMessage As New StringBuilder
            'Dim strMessage As String = ""
            Dim strProcessResult As String = ""
            Dim strExceptionGUID As String = Guid.NewGuid.ToString
            Dim ds As DataSet
            'Dim intFailedRecords As Integer = 0
            'Dim intInitialImportSuccessful As Integer = 0
            'Dim strValidMessage As String = ""
            'Dim intFAIDExists As Integer = 0
            'Dim intHEADExists As Integer = 0
            'Dim intDISBExists As Integer = 0

            Dim chkPostPayment As CheckBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("chkPostPayment"), CheckBox)
            Dim chkPOSS As CheckBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("chkPOSS"), CheckBox)
            Dim chkOADWED As CheckBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("chkOADWED"), CheckBox)
            Dim chkOAAWEA As CheckBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("chkOAAWEA"), CheckBox)
            Dim rdoExpectedDate As RadioButton = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("rdoExpectedDate"), RadioButton)
            Dim rdoSpecifyDate As RadioButton = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("rdoSpecifyDate"), RadioButton)
            ' Dim hlPostDate As HyperLink = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("hlPostDate"), HyperLink)
            Dim txtPostDate As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPostDate"), TextBox)
            'Dim rapPD As Panel = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("rapPD"), Panel)
            Dim txtRefRecid As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtRefRecid"), TextBox)
            Dim ddlPaymentType As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlPaymentType"), DropDownList)

            If rdoSpecifyDate.Checked = True Then
                If txtPostDate.Text + "" = "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please select Specify Date');", True)
                    ''DisplayErrorMessage("Please select Specify Date")
                    Exit Sub
                End If
            End If
            ''If rdolstPaymentOptions.SelectedValue.ToString = "True" Then
            ''    If ddlPaymentType.SelectedValue.ToString = "0" Then
            ''        DisplayErrorMessage("Please select payment type")
            ''        Exit Sub
            ''    End If
            ''End If


            If chkPostPayment.Checked = True Then
                If ddlPaymentType.SelectedValue.ToString = "0" Then
                    ''DisplayErrorMessage("Please select payment type")
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please select payment type');", True)
                    Exit Sub
                End If
            End If
            cxnString = ConString
            Session("FileName") = ddlFileNames.SelectedValue
            Dim strEDExpDataFrom As String = ""
            strEDExpDataFrom = MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer")

            If strEDExpDataFrom.ToLower = "yes" Then
                If Session("MsgType").ToString = "PELL" Then
                    'strProcessResult = myEDFacade.ProcessEDMsgCollectionNew(cxnString, Session("MsgType"), Session("dtPATSData"), Session("dtPATSDisb"), , chkOADWED.Checked, chkOAAWEA.Checked, rdolstPaymentOptions.SelectedValue, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, SingletonAppSettings.AppSettings("RemoteEDExpressIn"), SingletonAppSettings.AppSettings("RemoteEDExpressArchive"), Trim(Session("FileName")), SingletonAppSettings.AppSettings("RemoteEDExpressException"), strExceptionGUID, "sourcetotarget", "", "yes", SingletonAppSettings.AppSettings("RemoteEDExpressNotPost"))
                    ''strProcessResult = myEDFacade.ProcessEDMsgCollectionNew(cxnString, Session("MsgType"), Session("dtPATSData"), Session("dtPATSDisb"), , chkOADWED.Checked, chkOAAWEA.Checked, chkPostPayment.Checked, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, SingletonAppSettings.AppSettings("RemoteEDExpressIn"), SingletonAppSettings.AppSettings("RemoteEDExpressArchive"), Trim(Session("FileName")), SingletonAppSettings.AppSettings("RemoteEDExpressException"), strExceptionGUID, "sourcetotarget", "", "yes", SingletonAppSettings.AppSettings("RemoteEDExpressNotPost"))
                    strProcessResult = myEDFacade.ProcessEDMsgCollectionNew(cxnString, Session("MsgType"), Session("dtPATSData"), Session("dtPATSDisb"), , chkOADWED.Checked, chkOAAWEA.Checked, chkPostPayment.Checked, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, MyAdvAppSettings.AppSettings("RemoteEDExpressIn"), MyAdvAppSettings.AppSettings("RemoteEDExpressArchive"), Trim(Session("FileName")), MyAdvAppSettings.AppSettings("RemoteEDExpressException"), strExceptionGUID, "sourcetotarget", "", "yes")
                Else
                    'strProcessResult = myEDFacade.ProcessEDMsgCollection(cxnString, myMsgCollection, Session("MsgType"), Session("dtDirectLoan"), Session("dtDLSchDisb"), Session("dtDLActualDisb"), SingletonAppSettings.AppSettings("RemoteEDExpressIn"), SingletonAppSettings.AppSettings("RemoteEDExpressArchive"), Trim(Session("FileName")), SingletonAppSettings.AppSettings("RemoteEDExpressException"), strExceptionGUID, "sourcetotarget", "", "yes")
                    strProcessResult = myEDFacade.ProcessEDMsgCollectionNew(cxnString, Session("MsgType"), Session("dtDirectLoan"), Session("dtDLSchDisb"), Session("dtDLActualDisb"), chkOADWED.Checked, chkOAAWEA.Checked, chkPostPayment.Checked, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, MyAdvAppSettings.AppSettings("RemoteEDExpressIn"), MyAdvAppSettings.AppSettings("RemoteEDExpressArchive"), Trim(Session("FileName")), MyAdvAppSettings.AppSettings("RemoteEDExpressException"), strExceptionGUID, "sourcetotarget", "", "yes")
                End If
            Else
                If Session("MsgType").ToString = "PELL" Then
                    'strProcessResult = myEDFacade.ProcessEDMsgCollection(cxnString, myMsgCollection, Session("MsgType"), Session("dtPATSData"), Session("dtPATSDisb"), , SingletonAppSettings.AppSettings("SourcePathEdExp"), SingletonAppSettings.AppSettings("TargetPathEdExp"), Trim(Session("FileName")), SingletonAppSettings.AppSettings("ExceptionPathEdExp"), strExceptionGUID, "sourcetotarget", "", "no")
                    'strProcessResult = myEDFacade.ProcessEDMsgCollectionNew(cxnString, Session("MsgType"), Session("dtPATSData"), Session("dtPATSDisb"), , chkOADWED.Checked, chkOAAWEA.Checked, rdolstPaymentOptions.SelectedValue, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, SingletonAppSettings.AppSettings("SourcePathEdExp"), SingletonAppSettings.AppSettings("TargetPathEdExp"), Trim(Session("FileName")), SingletonAppSettings.AppSettings("ExceptionPathEdExp"), strExceptionGUID, "sourcetotarget", "", "no", SingletonAppSettings.AppSettings("NotPostPathEdExp"))
                    ''strProcessResult = myEDFacade.ProcessEDMsgCollectionNew(cxnString, Session("MsgType"), Session("dtPATSData"), Session("dtPATSDisb"), , chkOADWED.Checked, chkOAAWEA.Checked, chkPostPayment.Checked, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, SingletonAppSettings.AppSettings("SourcePathEdExp"), SingletonAppSettings.AppSettings("TargetPathEdExp"), Trim(Session("FileName")), SingletonAppSettings.AppSettings("ExceptionPathEdExp"), strExceptionGUID, "sourcetotarget", "", "no", SingletonAppSettings.AppSettings("NotPostPathEdExp"))
                    strProcessResult = myEDFacade.ProcessEDMsgCollectionNew(cxnString, Session("MsgType"), Session("dtPATSData"), Session("dtPATSDisb"), , chkOADWED.Checked, chkOAAWEA.Checked, chkPostPayment.Checked, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, MyAdvAppSettings.AppSettings("SourcePathEdExp"), MyAdvAppSettings.AppSettings("TargetPathEdExp"), Trim(Session("FileName")), MyAdvAppSettings.AppSettings("ExceptionPathEdExp"), strExceptionGUID, "sourcetotarget", "", "no")
                Else
                    'strProcessResult = myEDFacade.ProcessEDMsgCollection(cxnString, myMsgCollection, Session("MsgType"), Session("dtDirectLoan"), Session("dtDLSchDisb"), Session("dtDLActualDisb"), SingletonAppSettings.AppSettings("SourcePathEdExp"), SingletonAppSettings.AppSettings("TargetPathEdExp"), Trim(Session("FileName")), SingletonAppSettings.AppSettings("ExceptionPathEdExp"), strExceptionGUID, "sourcetotarget", "", "no")
                    strProcessResult = myEDFacade.ProcessEDMsgCollectionNew(cxnString, Session("MsgType"), Session("dtDirectLoan"), Session("dtDLSchDisb"), Session("dtDLActualDisb"), chkOADWED.Checked, chkOAAWEA.Checked, chkPostPayment.Checked, chkPOSS.Checked, rdoExpectedDate.Checked, txtPostDate.Text, txtRefRecid.Text, ddlPaymentType.SelectedValue.ToString, MyAdvAppSettings.AppSettings("SourcePathEdExp"), MyAdvAppSettings.AppSettings("TargetPathEdExp"), Trim(Session("FileName")), MyAdvAppSettings.AppSettings("ExceptionPathEdExp"), strExceptionGUID, "sourcetotarget", "", "no")
                End If
            End If

            'Dim dtParent As New DataTable
            'dtParent = Session("dtPATSData")
            'Dim dtChild As New DataTable
            'dtChild = Session("dtPATSDisb")

            If strProcessResult.ToUpper.Contains("EXCEPTION") Then
                ds = myEDFacade.getExceptionReport(strExceptionGUID, IIf(Session("MsgType").ToString.ToUpper = "PELL", "PELL", "DL"))
                If Session("MsgType").ToString.ToUpper = "PELL" Then
                    lblExpParent.Text = "Pell, SMART, ACG and TEACH Awards"
                    rgPellException.Visible = True
                    rgDLException.Visible = False
                Else
                    lblExpParent.Text = "Direct Loans"
                    rgPellException.Visible = False
                    rgDLException.Visible = True
                End If
                ''tblPellRecords.Visible = False
                rgPellAward.Visible = False
                tblPellRecords.Visible = False
                tblPellHeader.Visible = False

                rgDirectLoan.Visible = False
                tblDLRecords.Visible = False
                tblDLHeader.Visible = False

                tblPellException.Visible = True
                Session("ExpParent") = ds.Tables("Parent")
                Session("ExpChild") = ds.Tables("Child")
                SortExceptionGirds()
                'dgExceptionReportParent.Visible = True
                'dgExceptionReportChild.Visible = True
                strProcessResult = strProcessResult.Replace("Exception", "")
                'strProcessResult = strProcessResult & vbLf & vbLf & "The data in the file (" + Trim(Session("FileName")) + ") was not successfully posted and moved to exception folder."
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('" + ReplaceSpecialCharactersInJavascriptMessage(strProcessResult) + "');", True)
                'DisplayErrorMessage(strProcessResult)
            Else
                rgPellAward.Visible = False
                tblPellRecords.Visible = False
                tblPellHeader.Visible = False
                tblPellException.Visible = False

                rgDirectLoan.Visible = False
                tblDLRecords.Visible = False
                tblDLHeader.Visible = False
                tblPellException.Visible = False

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('" + ReplaceSpecialCharactersInJavascriptMessage(strProcessResult) + "');", True)
                'DisplayErrorMessage(strProcessResult)
            End If
            BuildFilesList()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('" + ReplaceSpecialCharactersInJavascriptMessage(ex.Message) + "');", True)
            ''DisplayErrorMessage(ex.Message)
        End Try
    End Sub

    Protected Sub rdoSpecifyDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rapPD As Panel = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("rapPD"), Panel)
        rapPD.Enabled = True
    End Sub

    Protected Sub rdoExpectedDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtPostDate As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPostDate"), TextBox)
        Dim rapPD As Panel = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("rapPD"), Panel)
        txtPostDate.Text = String.Empty
        rapPD.Enabled = False
    End Sub

    Protected Sub rgPellException_DetailTableDataBind(ByVal source As Object, ByVal e As GridDetailTableDataBindEventArgs) Handles rgPellException.DetailTableDataBind
        If Not Session("ExpChild") Is Nothing Then
            Dim dvPellDisbExp As New DataView(Session("ExpChild"))
            'Dim dvPellDisbRows As New DataView()
            Dim drSelectRows() As DataRow
            Dim dataItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)
            Select Case e.DetailTableView.Name
                Case "PellDisbExp"
                    Dim ExceptionReportId As String = dataItem.GetDataKeyValue("ExceptionReportId").ToString()
                    drSelectRows = dvPellDisbExp.Table.Select("ExceptionReportId='" + ExceptionReportId + "'")
                    e.DetailTableView.Columns(0).Visible = True
                    e.DetailTableView.DataSource = drSelectRows
                    e.DetailTableView.Columns(0).Visible = False
            End Select
        End If
    End Sub

    Protected Sub rgPellException_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles rgPellException.NeedDataSource
        If Not e.IsFromDetailTable Then
            If Not Session("ExpParent") Is Nothing Then
                Dim dvPellAwardExp As New DataView(Session("ExpParent"))
                rgPellException.Columns(0).Visible = True
                rgPellException.MasterTableView.DataSource = dvPellAwardExp
                rgPellException.Columns(0).Visible = False
            End If
        End If
    End Sub

    Protected Sub rgDLException_DetailTableDataBind(ByVal source As Object, ByVal e As GridDetailTableDataBindEventArgs) Handles rgDLException.DetailTableDataBind
        If Not Session("ExpChild") Is Nothing Then
            Dim dvDLDisbExp As New DataView(Session("ExpChild"))
            'Dim dvDLDisbRows As New DataView()
            Dim drSelectRows() As DataRow
            Dim dataItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)
            Select Case e.DetailTableView.Name
                Case "DLDisbExp"
                    Dim ExceptionReportId As String = dataItem.GetDataKeyValue("ExceptionReportId").ToString()
                    drSelectRows = dvDLDisbExp.Table.Select("ExceptionReportId='" + ExceptionReportId + "'")
                    e.DetailTableView.Columns(0).Visible = True
                    e.DetailTableView.DataSource = drSelectRows
                    e.DetailTableView.Columns(0).Visible = False
            End Select
        End If
    End Sub

    Protected Sub rgDLException_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles rgDLException.NeedDataSource
        If Not e.IsFromDetailTable Then
            If Not Session("ExpParent") Is Nothing Then
                Dim dvDLAwardExp As New DataView(Session("ExpParent"))
                rgDLException.Columns(0).Visible = True
                rgDLException.MasterTableView.DataSource = dvDLAwardExp
                rgDLException.Columns(0).Visible = False
            End If
        End If
    End Sub
    Private Sub BuildPaymentTypesDDL()
        Dim ds As DataSet
        Dim paymenttype As New EDFacade_New
        ds = paymenttype.GetPaymentTypes()
        Dim ddlPaymentType As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlPaymentType"), DropDownList)
        ddlPaymentType.Items.Clear()
        With ddlPaymentType
            .DataValueField = "PaymentTypeId"
            .DataTextField = "Description"
            .DataSource = ds.Tables("PaymentTypes").DefaultView
            .DataBind()
        End With
        With ddlPaymentType
            .Items.Insert(0, New ListItem("Select", "0"))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub chkPostPayment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim chkPostPayment As CheckBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("chkPostPayment"), CheckBox)
        Dim chkPOSS As CheckBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("chkPOSS"), CheckBox)
        Dim rdoExpectedDate As RadioButton = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("rdoExpectedDate"), RadioButton)
        Dim rdoSpecifyDate As RadioButton = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("rdoSpecifyDate"), RadioButton)
        'Dim hlPostDate As HyperLink = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("hlPostDate"), HyperLink)
        Dim txtPostDate As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtPostDate"), TextBox)
        Dim rapPD As Panel = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("rapPD"), Panel)
        Dim txtRefRecid As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtRefRecid"), TextBox)
        Dim ddlPaymentType As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlPaymentType"), DropDownList)

        If chkPostPayment.Checked = False Then
            chkPOSS.Checked = False
            chkPOSS.Enabled = False
            rdoExpectedDate.Checked = True
            rdoSpecifyDate.Checked = False
            rdoExpectedDate.Enabled = False
            rdoSpecifyDate.Enabled = False
            txtPostDate.Text = String.Empty
            rapPD.Enabled = False
            txtRefRecid.Text = String.Empty
            txtRefRecid.Enabled = False
            ddlPaymentType.SelectedValue = "0"
            ddlPaymentType.Enabled = False
        Else
            chkPOSS.Enabled = True
            rdoExpectedDate.Enabled = True
            rdoSpecifyDate.Enabled = True
            rapPD.Enabled = True
            ddlPaymentType.Enabled = True
            txtRefRecid.Enabled = True
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Dim strAcademicYear As String
        Dim StartYear As String
        Dim EndYear As String
        Dim IntStartYear As Integer
        Dim IntEndYear As Integer
        strAcademicYear = ddlAcademicYear.SelectedItem.Text
        If strAcademicYear.Length = 9 Or strAcademicYear.Length = 5 Then
            If strAcademicYear.Length = 9 Then
                StartYear = strAcademicYear.Substring(0, 4)
                EndYear = strAcademicYear.Substring(5, 4)
                Try
                    IntStartYear = CType(StartYear, Integer)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    IntStartYear = 0
                End Try
                Try
                    IntEndYear = CType(EndYear, Integer)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    IntEndYear = 0
                End Try
                If Not IntStartYear = 0 And Not IntEndYear = 0 Then
                    'txtAwardStartDate.Text = "07/01/" & StartYear
                    'txtAwardEndDate.Text = "06/30/" & EndYear
                    txtAwardStartDate.SelectedDate = "07/01/" & StartYear
                    txtAwardEndDate.SelectedDate = "06/30/" & EndYear
                End If
            ElseIf strAcademicYear.Length = 5 Then
                StartYear = strAcademicYear.Substring(0, 2)
                EndYear = strAcademicYear.Substring(3, 2)
                Try
                    IntStartYear = CType(StartYear, Integer)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    IntStartYear = 0
                End Try
                Try
                    IntEndYear = CType(EndYear, Integer)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    IntEndYear = 0
                End Try
                If Not IntStartYear = 0 And Not IntEndYear = 0 Then
                    'txtAwardStartDate.Text = "07/01/20" & StartYear
                    'txtAwardEndDate.Text = "06/30/20" & EndYear
                    txtAwardStartDate.SelectedDate = "07/01/20" & StartYear
                    txtAwardEndDate.SelectedDate = "06/30/20" & EndYear
                End If
            Else
                'txtAwardStartDate.Text = String.Empty
                'txtAwardEndDate.Text = String.Empty
                txtAwardStartDate.Clear()
                txtAwardEndDate.Clear()
            End If
        Else
            'txtAwardStartDate.Text = String.Empty
            'txtAwardEndDate.Text = String.Empty
            txtAwardStartDate.Clear()
            txtAwardEndDate.Clear()
        End If
    End Sub
    Private Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String
        '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
        Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")
    End Function

    Protected Sub btnProcessData_ServerClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnProcessData.ServerClick
        If Not Session("IsProcess") = Nothing Then
            SaveProcessFile()
        Else
            ClearDataGrid()
            CreateDataTable()
            Session("Process") = "1"
            ProcessData()
        End If
    End Sub
    Protected Sub rgDirectLoan_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles rgDirectLoan.ItemCommand

        If e.CommandName = "ExpandCollapse" Then
            If Not e.Item.Expanded = True Then
                Dim dtDlsd As New DataView(CType(Session("dtDLSchDisb"), DataTable))
                Dim dtDlad As New DataView(CType(Session("dtDLActualDisb"), DataTable))
                Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)
                Dim drSchDisRecords() As DataRow
                Dim drActDisRecords() As DataRow

                Dim rgActDisb As RadGrid = DirectCast(dataItem.ChildItem.FindControl("rgActDisb"), RadGrid)
                Dim tblDlActDisb As Table = DirectCast(dataItem.ChildItem.FindControl("tblDLActDisb"), Table)
                Dim indexID As String = dataItem.GetDataKeyValue("IndexID").ToString()
                drActDisRecords = dtDlad.Table.Select("ParentIndexID=" + indexID + "And Show=1")
                rgActDisb.DataSource = drActDisRecords
                rgActDisb.DataBind()
                If rgActDisb.Items.Count > 0 Then
                    tblDlActDisb.Visible = True
                Else
                    tblDlActDisb.Visible = False
                End If

                Dim rgSubDisb As RadGrid = DirectCast(dataItem.ChildItem.FindControl("rgSubDisb"), RadGrid)
                Dim tblDLSubDisb As Table = DirectCast(dataItem.ChildItem.FindControl("tblDLSubDisb"), Table)
                drSchDisRecords = dtDlsd.Table.Select("ParentIndexID=" + indexID + "And Show=1")
                rgSubDisb.DataSource = drSchDisRecords
                rgSubDisb.DataBind()
                If rgSubDisb.Items.Count > 0 Then
                    tblDLSubDisb.Visible = True
                Else
                    tblDLSubDisb.Visible = False
                End If
            End If
        End If
    End Sub
    Protected Sub rgDirectLoan_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles rgDirectLoan.NeedDataSource
        If Not e.IsFromDetailTable Then
            If Not Session("dtDirectLoan") Is Nothing Then
                Dim dtDLAwardFinal As DataTable = Session("dtDirectLoan")
                Dim drErrRecords() As DataRow = dtDLAwardFinal.Select("ErrorType<>0")
                rgDirectLoan.Columns(0).Visible = True
                rgDirectLoan.MasterTableView.DataSource = drErrRecords
                rgDirectLoan.Columns(0).Visible = False
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub


End Class