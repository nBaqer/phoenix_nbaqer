<%@ Page language="VB" AutoEventWireup="false" codefile="maintPopup.aspx.vb" inherits="maintPopup" %>


<html>
<head id="head1" runat="server">
    <title>maintenance Popup</title>
    <base target="_self"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="-1"/>
<%--    <link rel="stylesheet" type="text/css" href="~/css/localhost_lowercase.css" />--%>
    <link href="css/systememail.css" type="text/css" rel="stylesheet"/>
    <link href="css/form.css" type="text/css" rel="stylesheet"/>
    <style>
        .schedheader
        {
            font: bold 9px verdana;
            color: #000066;
            background-color: #E9Edf2;
            padding: 2px;
            border-left: 0;
            border-top: 0;
            border-right: 1px solid #ebebeb;
            border-bottom: 1px solid #ebebeb;
            vertical-align: bottom;
            text-align: center;
        }
        
        .scheditem
        {
            font: normal 9px verdana;
            color: #000066;
            border-left: 0;
            border-top: 0;
            border-right: 1px solid #ebebeb;
            border-bottom: 1px solid #ebebeb;
            vertical-align: middle;
            padding: 2px;
        }
        
        .schedtextBox
        {
            font: normal 9px arial;
            color: #111111;
            width: 66%;
            text-align: left;
            background-color: #fAfAfA;
            border: 1px solid #ebebeb;
            padding: 0;
        }
        .listframe
        {
            width: 18%;
        }
        .classsectioncontentcell
        {
            padding-right: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <!-- begin header -->
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="toppopupheader">
                    <img src="images/maint_popup_header.jpg">
                </td>
                <td nowrap class="headerstyle">
                    <asp:label id="lblheader" runat="server" />
                </td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href="#">X close</a>
                </td>
            </tr>
        </table>
        <!-- end header -->
        <table cellspacing="0" cellpadding="0" width="100%" border="0"  height="100%">
            <tr>
                <td class="listframe">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="table2">
                        <tr>
                            <td class="listframetop2">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" runat="server" id="tblstatus">
                                    <tr>
                                        <td width="15%" nowrap align="left">
                                            <asp:label id="lblshow" runat="server" cssclass="label" text="show" />
                                        </td>
                                        <td width="85%" nowrap>
                                            <asp:RadioButtonlist id="radstatus" cssclass="label" AutoPostBack="true" runat="server"
                                                Repeatdirection="horizontal">
                                                <asp:listitem text="Active" selected="true" />
                                                <asp:listitem text="Inactive" />
                                                <asp:listitem text="All" />
                                            </asp:RadioButtonlist>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="listframebottom">
                                <div class="scrollleftpopups">
                                    <asp:datalist id="dllhs" runat="server">
                                        <selecteditemstyle cssclass="selecteditem" />
                                        <itemstyle cssclass="nonselecteditem" />
                                        <itemtemplate>
                                            <asp:imageButton id="ibinactive" imageUrl="~/images/inactive.gif" runat="server"
                                                Visible='<%# (Not ctype(container.dataitem("Active"), Boolean)).tostring %>'
                                                commandArgument='<%# container.dataitem("id")%>' causesValidation="false" />
                                            <asp:imageButton id="ibActive" imageUrl="~/images/Active.gif" runat="server" Visible='<%# ctype(container.dataitem("Active"), Boolean).tostring %>'
                                                commandArgument='<%# container.dataitem("id")%>' causesValidation="false" />
                                            <!--    PostBackUrl="_self" /> -->
                                            <asp:linkButton text='<%# container.dataitem("descrip")%>' runat="server" cssclass="nonselecteditem"
                                                commandArgument='<%# container.dataitem("id")%>' id="lblhsitem" causesValidation="false" />
                                        </itemtemplate>
                                    </asp:datalist>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="leftside">
                    <table cellspacing="0" cellpadding="0" width="9" border="0">
                        <tbody>
                        </tbody>
                    </table>
                </td>
                <td class="detailsframetop">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="menuframe" align="right" style="height: 23px">
                                <asp:Button id="btnsave" runat="server" text="Save" cssclass="save" />
                                <asp:Button id="btnNew" runat="server" text="New" cssclass="new" causesValidation="false" />
                                <asp:Button id="btndelete" runat="server" text="Delete" cssclass="delete" causesValidation="false" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="table5">
                        <tr>
                            <td class="detailsframe">
                                <asp:Panel id="pnlRhs" runat="server" Width="100%" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <%--    <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
    </div>
    </form>
</body>
</html>
