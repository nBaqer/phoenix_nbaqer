﻿Imports System.Diagnostics
Imports BusinessLogicUtilities
Imports FAME.Advantage.Common
Imports FAME.Advantage.Site.Lib.Infrastruct.Helpers

Partial Class ErrorPage
    Inherits BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        Dim errMessage As String
        lblApplicationHeaderVersion.Text = "Advantage - " & GetBuildVersion()
        Dim ex As Exception
        ''Application ErrorPage is saved in Global.asax Application error
        If Not Session("Error") Is Nothing Then
            If Not TryCast(Session("Error"), Exception) Is Nothing Then
                ex = CType(Session("Error"), Exception)
                If TryCast(ex, HttpException) Is Nothing Then
                    errMessage = ex.Message.ToString
                    errMessage = errMessage + "<br/>" + ex.StackTrace.ToString
                Else
                    ex = TryCast(ex, HttpException)
                    If (Not ex.InnerException Is Nothing) Then
                        errMessage = ex.InnerException.ToString
                    Else
                        errMessage = ex.ToString
                    End If
                    If (Not ex.StackTrace Is Nothing) Then
                        errMessage = errMessage + "<br/>" + ex.StackTrace.ToString
                    End If
                End If
            Else

                errMessage = CType(Session("Error"), String)
            End If
            If Not Application("ErrorPage") Is Nothing Then
                lblerror.Text = "Error on page:" + Application("ErrorPage").ToString + "<br/>*** The Error is : " + errMessage
            Else
                lblerror.Text = errMessage
            End If

        Else
            If Not HttpContext.Current.Request.Params("Message") Is Nothing Then
                lblerror.Text = Server.UrlDecode(HttpContext.Current.Request.Params("Message").ToString)
            Else
                If Not Application("ErrorPage") Is Nothing Then
                    lblerror.Text = "Un handled Error on Page: " + Application("ErrorPage").ToString
                Else
                    lblerror.Text = "Un Handled Error on Page"
                End If

            End If

        End If
        ' Send Email option is enabled when the client Mode is set to Yes and when the Error email from and To address  are set in manage configurations
        If UCase(ConfigurationManager.AppSettings("ClientMode").ToString) = "YES" Then
            lblErrorheader.Visible = False
            lblerror.Visible = False

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            ''Send Email option is Visible only when the Error Email Address are configured in Manage Configurations page
            If Not myAdvAppSettings.AppSettings("ErrorEmailToAddress") Is Nothing And Not myAdvAppSettings.AppSettings("ErrorEmailFromAddress") Is Nothing Then
                If myAdvAppSettings.AppSettings("ErrorEmailToAddress").ToString = String.Empty Or myAdvAppSettings.AppSettings("ErrorEmailFromAddress").ToString = String.Empty Then
                    btnSendEmail.Visible = False
                Else
                    btnSendEmail.Visible = True
                End If

            Else
                btnSendEmail.Visible = False
            End If
        Else
            lblErrorheader.Visible = True
            lblerror.Visible = True
            btnSendEmail.Visible = False
        End If
        If Not Page.IsPostBack Then

            If Not Request.UrlReferrer Is Nothing Then
                hdnpreviousURL.Value = Request.UrlReferrer.ToString()
            End If
        End If
    End Sub
    Protected Sub lnkLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkLogin.Click
        Session.Abandon()

        MultiTenantHostHelper.RedirectToLogin()
    End Sub
    Private Function GetBuildVersion() As String
        Dim versionString As String
        Try
            'Dim MyAssemblyName As AssemblyName = AssemblyName.GetAssemblyName(Server.MapPath("~/Bin/Common.dll"))
            'VersionString = MyAssemblyName.Version.ToString()
            Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Server.MapPath("~/Bin/FAME.Advantage.Site.Lib.dll"))
            versionString = myFileVersionInfo.FileVersion.ToString()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            versionString = String.Empty
        End Try
        Return versionString
    End Function

    Protected Sub lnkPreviousPage_Click(sender As Object, e As EventArgs) Handles lnkPreviousPage.Click
        If hdnpreviousURL.Value.Contains("ErrorPage.aspx") Then
            Session.Abandon()

            MultiTenantHostHelper.RedirectToLogin()
        Else
            Response.Redirect(hdnpreviousURL.Value.ToString)
        End If
    End Sub

    Protected Sub btnSendEmail_Click(sender As Object, e As EventArgs) Handles btnSendEmail.Click

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        Dim messageBody As New StringBuilder
        With messageBody
            .AppendLine("Customer Support,")
            .AppendLine()
            .AppendLine(" There is an error in Advantage:" & GetBuildVersion())
            .AppendLine()
            If Not Application("ErrorPage") Is Nothing Then
                .AppendLine("Error Page URL: " & Application("ErrorPage").ToString)
            End If
            If Not myAdvAppSettings.AppSettings("SchoolName") Is Nothing Then
                .AppendLine("School Name: " & myAdvAppSettings.AppSettings("SchoolName").ToString)
            End If
            If Not AdvantageSession.UserName Is Nothing Then
                .AppendLine("Logged In user name: " & AdvantageSession.UserName.ToString)
                .AppendLine("Logged In Campus : " & AdvantageSession.UserState.CampusId.ToString)
            End If

            .AppendLine()
            .AppendLine(" The inner exception of the message is :")
            .AppendLine(lblerror.Text)
            .AppendLine()
            .AppendLine("Thank You")
            .AppendLine()
        End With

        Dim fromAddress As String
        Dim toAddress As String

        If Not myAdvAppSettings.AppSettings("ErrorEmailFromAddress") Is Nothing Then
            fromAddress = CType(myAdvAppSettings.AppSettings("ErrorEmailFromAddress"), String)
        Else
            fromAddress = "Donotreply@fameinc.com"

        End If
        If Not myAdvAppSettings.AppSettings("ErrorEmailToAddress") Is Nothing Then
            toAddress = CType(myAdvAppSettings.AppSettings("ErrorEmailToAddress"), String)
        Else
            toAddress = "AdvantageHelp@fameinc.com"

        End If

        Utilities.SendEmailregardingErrorPage(fromAddress, toAddress, "Advantage Error - " & GetBuildVersion(), messageBody.ToString, "")
        Application("ErrorPage") = Nothing
        Session("Error") = Nothing
        DisplayNotificationMessage("Notification was sent to Advantage Customer Support." + vbCrLf + "Contact Support for further information.")
    End Sub


    Private Sub DisplayNotificationMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub
End Class
