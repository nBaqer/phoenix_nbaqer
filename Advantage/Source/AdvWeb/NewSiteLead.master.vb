﻿
Imports FAME.Advantage.Common
Imports Advantage.Business.Objects
Imports Newtonsoft.Json

Namespace AdvWeb
    Partial Class NewSiteLead
        Inherits MasterPage

        Public IsClockHour As String

        Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

            'Get the clock hour setting for the active campus.
            Dim setting = AdvAppSettings.GetAppSettings()
            IsClockHour = CType(setting.AppSettings("TimeClockClassSection"), String)

            'Put user code to initialize the page here
            Dim advantageUserState As User = AdvantageSession.UserState
            Dim campusId = Master.CurrentCampusId
            Dim resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            Dim userId = AdvantageSession.UserState.UserId.ToString
            Master.UserId = userId
            Dim pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

            'Check if this page still exists in the menu while switching campus
        End Sub
    End Class
End Namespace