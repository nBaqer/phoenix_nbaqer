<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\Verification_Letter.xsd" workingxmlfile="Verification_Letter.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
				</children>
			</paragraph>
			<text fixtext="To Whom It May Concern:">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="This letter will confirm that ">
				<styles font-weight="bold"/>
			</text>
			<template>
				<match match="name"/>
				<children>
					<template>
						<match match="firstName"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<text fixtext=" ">
						<styles font-weight="bold"/>
					</text>
					<template>
						<match match="lastName"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
				</children>
			</template>
			<text fixtext=" is enrolled as a full time student at Acupuncture &amp; Massage College, (AMC) in the following(s) programs:">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<template>
				<match match="verificationData"/>
				<children>
					<newline/>
					<table dynamic="1">
						<properties border="1"/>
						<children>
							<tableheader>
								<children>
									<tablerow>
										<children>
											<tablecol>
												<properties align="center"/>
												<children>
													<text fixtext="Program">
														<styles font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<properties align="center"/>
												<children>
													<text fixtext="Start Date">
														<styles font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<properties align="center"/>
												<children>
													<text fixtext="Expected Graduation Date">
														<styles font-weight="bold"/>
													</text>
												</children>
											</tablecol>
										</children>
									</tablerow>
								</children>
							</tableheader>
							<tablebody>
								<children>
									<tablerow>
										<children>
											<tablecol>
												<properties align="right"/>
												<children>
													<template>
														<match match="program"/>
														<children>
															<xpath allchildren="1">
																<styles font-weight="bold"/>
															</xpath>
														</children>
													</template>
												</children>
											</tablecol>
											<tablecol>
												<properties align="center"/>
												<children>
													<text fixtext=" ">
														<styles font-weight="bold"/>
													</text>
													<autovalue>
														<styles font-weight="bold"/>
														<editorproperties editable="0"/>
														<autocalc>
															<xpath value="substring( startDate , 6 , 2 )"/>
														</autocalc>
													</autovalue>
													<text fixtext="/">
														<styles font-weight="bold"/>
													</text>
													<autovalue>
														<styles font-weight="bold"/>
														<editorproperties editable="0"/>
														<autocalc>
															<xpath value="substring(  startDate  , 9,2 )"/>
														</autocalc>
													</autovalue>
													<text fixtext="/">
														<styles font-weight="bold"/>
													</text>
													<autovalue>
														<styles font-weight="bold"/>
														<editorproperties editable="0"/>
														<autocalc>
															<xpath value="substring( startDate , 1 , 4 )"/>
														</autocalc>
													</autovalue>
												</children>
											</tablecol>
											<tablecol>
												<properties align="center"/>
												<children>
													<autovalue>
														<styles font-weight="bold"/>
														<editorproperties editable="0"/>
														<autocalc>
															<xpath value="substring( graduationDate , 6 , 2 )"/>
														</autocalc>
													</autovalue>
													<text fixtext="/">
														<styles font-weight="bold"/>
													</text>
													<autovalue>
														<styles font-weight="bold"/>
														<editorproperties editable="0"/>
														<autocalc>
															<xpath value="substring(  graduationDate  , 9,2 )"/>
														</autocalc>
													</autovalue>
													<text fixtext="/">
														<styles font-weight="bold"/>
													</text>
													<autovalue>
														<styles font-weight="bold"/>
														<editorproperties editable="0"/>
														<autocalc>
															<xpath value="substring( graduationDate , 1 , 4 )"/>
														</autocalc>
													</autovalue>
												</children>
											</tablecol>
										</children>
									</tablerow>
								</children>
							</tablebody>
						</children>
					</table>
				</children>
			</template>
			<newline/>
			<newline/>
			<text fixtext="Please feel free to contact the school should you require any additional information.">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="Sincerely,">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<text fixtext="Maria Garcia">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<text fixtext="Registrar/Student Services Director">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
