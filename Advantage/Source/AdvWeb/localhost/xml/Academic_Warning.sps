<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\Academic_Warning.xsd" workingxmlfile="Academic_Warning.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext="  "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
				</children>
			</paragraph>
			<paragraph paragraphtag="p">
				<properties align="center"/>
				<children>
					<text fixtext="ACADEMIC WARNING">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<paragraph paragraphtag="p">
						<properties align="left"/>
						<children>
							<newline/>
							<text fixtext="To: ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<match match="name"/>
								<children>
									<template>
										<match match="firstName"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
								</children>
							</template>
							<text fixtext="  "/>
							<template>
								<match match="name"/>
								<children>
									<template>
										<match match="lastName"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
								</children>
							</template>
							<newline/>
							<text fixtext="From: Dr. Provitera, Academic Dean">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<text fixtext="This letter is to inform you that you have Failed the module ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<match match="academicWarning"/>
								<children>
									<template>
										<match match="moduleName"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
									<text fixtext="  ">
										<styles font-weight="bold"/>
									</text>
									<text fixtext="that ended ">
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring( moduleEndDate , 6 , 2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/">
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring(  moduleEndDate , 9,2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/">
										<styles font-weight="bold"/>
									</text>
									<text>
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring( moduleEndDate , 1 , 4 )"/>
										</autocalc>
									</autovalue>
									<template>
										<match match="moduleEndDate"/>
										<children>
											<datepicker ownvalue="1" datatype="date"/>
										</children>
									</template>
									<text fixtext=". We, at AMC, are concerned about you maintaining Satisfactory Academic Progress, and our policies are designed to help you with this. Unsatisfactory academic progress, will result in the lost of your financial aid.">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<newline/>
									<text fixtext="You have two weeks from the date of this letter to resolve your &quot;F&quot; by retaking or making up your final.">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<newline/>
									<text fixtext="If your final exam has caused the failure, you can make-up the final. First you need to schedule the make-up or re-take with the receptionist. The make-ups and re-takes are given in the Library, Mondays, from 2-6:30 p.m., Tuesday, from 2-6 p.m., and Thursday, from 2 to 9 p.m. You must pay the $100 fee and get a receipt that you paid. Then you can make-up or retake the exam at the time you have scheduled. You will be notified in writing of the grade you received.">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<newline/>
									<text fixtext="Also, remember that the highest grade you can receive after re-taking and passing a failed final is &quot;C&quot;.">
										<styles font-weight="bold"/>
									</text>
									<newline/>
								</children>
							</template>
							<newline/>
							<newline/>
							<text fixtext="Please make sure the contact information we have on file for you is correct.">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<template>
								<match match="contactInfo"/>
								<children>
									<text fixtext="Home phone: ">
										<styles font-weight="bold"/>
									</text>
									<template>
										<match match="homePhone"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
									<text fixtext="   Work phone: ">
										<styles font-weight="bold"/>
									</text>
									<template>
										<match match="workPhone"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
									<newline/>
									<newline/>
									<text fixtext="Email: ">
										<styles font-weight="bold"/>
									</text>
									<template>
										<match match="email"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
									<text fixtext="   Emergency Contact: ">
										<styles font-weight="bold"/>
									</text>
									<template>
										<match match="emergencyContact"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
									<newline/>
									<newline/>
								</children>
							</template>
							<newline/>
							<text fixtext=" "/>
							<newline/>
							<text fixtext="Student signature__________________________________________________Date:_____________">
								<styles font-weight="bold"/>
							</text>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
