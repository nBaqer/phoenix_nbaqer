<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\Maximum_Time_Frame.xsd" workingxmlfile="Maximum_Time_Frame.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
				</children>
			</paragraph>
			<text fixtext="To Whom It May Concern:">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="This letter is to remind you that all your course work and clinic hours are needed to be completed by the maximum allowable time frame.  This time frame cannot exceed 150% of the normal course length.  Your scheduled grad date is ">
				<styles font-weight="bold"/>
			</text>
			<template>
				<styles font-weight="bold"/>
				<match match="maximumTimeFrame"/>
				<children>
					<autovalue>
						<styles font-weight="bold"/>
						<editorproperties editable="0"/>
						<autocalc>
							<xpath value="substring( graduationDate , 6 , 2 )"/>
						</autocalc>
					</autovalue>
					<text fixtext="/"/>
					<autovalue>
						<styles font-weight="bold"/>
						<editorproperties editable="0"/>
						<autocalc>
							<xpath value="substring(  graduationDate  , 9,2 )"/>
						</autocalc>
					</autovalue>
					<text fixtext="/"/>
					<text>
						<styles font-weight="bold"/>
					</text>
					<autovalue>
						<styles font-weight="bold"/>
						<editorproperties editable="0"/>
						<autocalc>
							<xpath value="substring( graduationDate , 1 , 4 )"/>
						</autocalc>
					</autovalue>
					<template>
						<match match="graduationDate"/>
						<children>
							<datepicker ownvalue="1" datatype="date"/>
						</children>
					</template>
					<text fixtext=".  This mean your maximum time frame is ">
						<styles font-weight="bold"/>
					</text>
					<autovalue>
						<styles font-weight="bold"/>
						<editorproperties editable="0"/>
						<autocalc>
							<xpath value="substring( maximumTimeFrameDate , 6 , 2 )"/>
						</autocalc>
					</autovalue>
					<text fixtext="/"/>
					<autovalue>
						<styles font-weight="bold"/>
						<editorproperties editable="0"/>
						<autocalc>
							<xpath value="substring(  maximumTimeFrameDate  , 9,2 )"/>
						</autocalc>
					</autovalue>
					<text fixtext="/"/>
					<text>
						<styles font-weight="bold"/>
					</text>
					<autovalue>
						<styles font-weight="bold"/>
						<editorproperties editable="0"/>
						<autocalc>
							<xpath value="substring( maximumTimeFrameDate , 1 , 4 )"/>
						</autocalc>
					</autovalue>
					<template>
						<styles font-weight="bold"/>
						<match match="maximumTimeFrameDate"/>
						<children>
							<datepicker ownvalue="1" datatype="date"/>
						</children>
					</template>
					<text fixtext=".">
						<styles font-weight="bold"/>
					</text>
				</children>
			</template>
			<newline/>
			<newline/>
			<text fixtext="Failure to comply can result in the students` dismissal from school.  A student will be dismissed when it is determined that he / she will not be able to complete the program within the 150% maximum time frame.

If this occurs, a student wishing to reenroll will be charged a $50 reentry fee and increase in tuition will also apply.">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="His / her transcript will be re-evaluated and the number of allowable credits toward the program determined.

If you have any questions regarding when your maximum allowable time frame is, please contact the Registrar`s Office
">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<newline/>
			<text fixtext="Sincerely,">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<text fixtext="Maria Garcia">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<text fixtext="Registrar/Student Services Director">
				<styles font-weight="bold"/>
			</text>
			<newline/>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
