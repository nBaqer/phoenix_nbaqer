<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\BASIC_STUDENT_INFO.xsd" workingxmlfile="Basic_Student_Info_Documents.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
					<newline/>
					<text fixtext="Dear ">
						<styles font-weight="bold"/>
					</text>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<text fixtext=":">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<paragraph paragraphtag="p">
						<children>
							<newline/>
							<text fixtext="As of, ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<styles font-weight="bold"/>
								<editorproperties adding="all" autoaddname="1" editable="1" markupmode="hide"/>
								<match match="optionalData"/>
								<children>
									<text fixtext=" ">
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring( date , 6 , 2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/"/>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring(  date  , 9,2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/"/>
									<text>
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring( date , 1 , 4 )"/>
										</autocalc>
									</autovalue>
									<template>
										<match match="date"/>
										<children>
											<datepicker ownvalue="1" datatype="date"/>
										</children>
									</template>
									<text fixtext=", you are being administratively dropped from Acupuncture Massage College for the following reasons:">
										<styles font-weight="bold"/>
									</text>
									<text fixtext="
"/>
									<text fixtext="
">
										<styles font-weight="bold"/>
									</text>
									<text fixtext="
 "/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox0"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" We have not received any money from you.
"/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox1"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" We have not received proof of your high school diploma."/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox2"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" We have not received two passport size photos.
"/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox3"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" You have not made up or attempted to make up the finals / modules you failed.
"/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox4"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" You have failed to meet the term of your probation agreement.
"/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox5"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" You have a failing GPA of 
"/>
									<template>
										<match match="text"/>
										<children>
											<xpath allchildren="1"/>
										</children>
									</template>
									<text fixtext=".">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox6"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" You will be unable to complete the program in the 150% maximum time frame, required by the State of Florida.
"/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox7"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" You are no longer eligible for Financial Aid.
"/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox8"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext=" You have not successfully made up the finals / module you failed.
  
         
 

"/>
									<newline/>
								</children>
							</template>
							<newline/>
							<text fixtext="Sincerely,">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<text fixtext="Maria Garcia">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<text fixtext="Registrar/Student Services Director">
								<styles font-weight="bold"/>
							</text>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
