<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\BASIC_STUDENT_INFO.xsd" workingxmlfile="Basic_Student_Info_Documents.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
					<newline/>
					<text fixtext="Dear ">
						<styles font-weight="bold"/>
					</text>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<text fixtext=":">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<paragraph paragraphtag="p">
						<children>
							<newline/>
							<text fixtext="Congratulations!  You have been accepted for admission to the Acupuncture &amp; Massage College.  It is a pleasure to welcome you as a member of our student body.  You have taken the first step towards achieving your career goals by enrolling in the Massage Therapy Program that begins 9/12/2005.
Over the years the Acupuncture &amp; Massage College has developed an outstanding faculty and staff, a strong curriculum, and effective student support services.  As the Director of Admissions, I am personally involved in helping you with any questions or concerns so you may be successful in your career goals.  My door is always open so please feel free to come in and meet with me.
As you embark on this new beginning, I want to wish you good luck in your classes.  All students entering into our program are required to attend a New Student Orientation. You have been scheduled to attend orientation on ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<styles font-weight="bold"/>
								<match match="optionalData"/>
								<children>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring(date , 6 , 2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/"/>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring(date  , 9,2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/"/>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring(date , 1 , 4 )"/>
										</autocalc>
									</autovalue>
									<template>
										<match match="date"/>
										<children>
											<text>
												<styles font-weight="bold"/>
											</text>
											<datepicker ownvalue="1" datatype="date"/>
										</children>
									</template>
									<text fixtext=" at "/>
									<template>
										<match match="text"/>
										<children>
											<xpath allchildren="1"/>
										</children>
									</template>
								</children>
							</template>
							<text fixtext=".  Should you have any questions, problems, or concerns, please feel free to give me a call.
The documents checked below are still missing from your file.  Please submit them as soon as possible.  Students may not begin classes until all required documents are received.
">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<template>
								<styles font-weight="bold"/>
								<editorproperties adding="all" autoaddname="1" editable="1" markupmode="hide"/>
								<match match="optionalData"/>
								<children>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox1"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  Essay, &quot;Why do I want to be a Massage Therapist?&quot;. "/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox2"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  2 Passport Size Photos."/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox3"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  High School Diploma, GED, or official college transcript."/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox4"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  Please submit $800 down payment to ensure your enrollment in the program."/>
									<newline/>
									<template>
										<match match="checkbox5"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  Your $800 down payment is being provided by Financial Aid."/>
									<newline/>
								</children>
							</template>
							<newline/>
							<newline/>
							<text fixtext="Sincerely,">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<newline/>
							<newline/>
							<text fixtext="Joseph Calareso">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<text fixtext="Admissions Director">
								<styles font-weight="bold"/>
							</text>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
