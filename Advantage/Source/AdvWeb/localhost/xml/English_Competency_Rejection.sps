<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\English_Competency_Rejection.xsd" workingxmlfile="English_Competency_Rejection.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<paragraph paragraphtag="p">
						<children>
							<paragraph paragraphtag="p">
								<children>
									<paragraph paragraphtag="p">
										<properties align="center"/>
										<children>
											<paragraph paragraphtag="p">
												<properties align="left"/>
												<children>
													<text fixtext="Dear ">
														<styles font-weight="bold"/>
													</text>
													<template>
														<match match="name"/>
														<children>
															<template>
																<match match="firstName"/>
																<children>
																	<xpath allchildren="1">
																		<styles font-weight="bold"/>
																	</xpath>
																</children>
															</template>
															<text fixtext="  "/>
															<template>
																<match match="lastName"/>
																<children>
																	<xpath allchildren="1">
																		<styles font-weight="bold"/>
																	</xpath>
																</children>
															</template>
														</children>
													</template>
													<text fixtext=":">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="Thank you for your interest in the Acupuncture &amp; Massage College. As you know,
our ">
														<styles font-weight="bold"/>
													</text>
													<template>
														<styles font-weight="bold"/>
														<match match="program"/>
														<children>
															<template>
																<match match="name"/>
																<children>
																	<xpath allchildren="1"/>
																</children>
															</template>
														</children>
													</template>
													<text fixtext=" is entirely in English; therefore, we require all of our 
students to be English proficient. 

Our English competency exam is designed to make sure that prospective students can 
succeed in our school. Unfortunately you did not pass this exam, and we cannot, at this 
time, accept you into the ">
														<styles font-weight="bold"/>
													</text>
													<template>
														<styles font-weight="bold"/>
														<match match="program"/>
														<children>
															<template>
																<match match="name"/>
																<children>
																	<xpath allchildren="1"/>
																</children>
															</template>
														</children>
													</template>
													<text fixtext=".

You have a few options which are outlined in our school catalog under English Competency.

">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<text fixtext="1) You can take the TOEFL exam and submit proof of scoring a minimum of 500. 
">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<text fixtext="2) You can take the TSE test and submit proof of scoring a minimum of 45.
">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<text fixtext="3) You can take an English composition or literature class at an accredited college and
submit proof of passing the course.

You can also take English as a Second Language (ESL) course(s) to improve your 
English skills, and then try our competency exam again. 

On behalf of the school, I wish you the best of luck in your endeavors.
">
														<styles font-weight="bold"/>
													</text>
													<paragraph paragraphtag="p">
														<properties align="left"/>
														<children>
															<newline/>
															<newline/>
															<text fixtext="Sincerely,">
																<styles font-weight="bold"/>
															</text>
															<newline/>
															<newline/>
															<newline/>
															<text fixtext="Dr. Provitera">
																<styles font-weight="bold"/>
															</text>
															<newline/>
															<text fixtext="Academic Dean">
																<styles font-weight="bold"/>
															</text>
														</children>
													</paragraph>
												</children>
											</paragraph>
										</children>
									</paragraph>
								</children>
							</paragraph>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
