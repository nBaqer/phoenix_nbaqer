<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\BASIC_STUDENT_INFO.xsd" workingxmlfile="Basic_Student_Info.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<newline/>
			<newline/>
			<template>
				<match match="name"/>
				<children>
					<template>
						<match match="firstName"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<text fixtext=" "/>
					<template>
						<match match="lastName"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
				</children>
			</template>
			<template>
				<match match="address"/>
				<children>
					<newline/>
					<template>
						<match match="address1"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<newline/>
					<template>
						<match match="address2"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<newline/>
					<template>
						<match match="city"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<text fixtext=","/>
					<template>
						<match match="state"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<text fixtext=" "/>
					<template>
						<match match="zip"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<newline/>
					<template>
						<match match="country"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
				</children>
			</template>
			<newline/>
			<newline/>
			<text fixtext="Dear ">
				<styles font-weight="bold"/>
			</text>
			<template>
				<match match="name"/>
				<children>
					<template>
						<match match="firstName"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
				</children>
			</template>
			<text fixtext=":">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="As of this date, we have received no response to our request for payment on your account balance of $">
				<styles font-weight="bold"/>
			</text>
			<template>
				<styles font-weight="bold"/>
				<match match="financial"/>
				<children>
					<template>
						<match match="balanceAmount"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
				</children>
			</template>
			<text fixtext=" This is the final letter you will receive from us as your account will be assigned to a collection agency and reported to the credit bureau for none payment.">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="Please contact the business office at 305-595-9500 today to stop these proceedings. If we do not hear from you there is no other option than to continue with the collection efforts.">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="Sincerely,">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<text fixtext="Financial Aid Department">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<text fixtext="Barbara Arechavaleta">
				<styles font-weight="bold"/>
			</text>
			<newline/>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
