<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\Basic_Student_Info_Account.xsd" workingxmlfile="" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<newline/>
			<newline/>
			<template>
				<match match="name"/>
				<children>
					<template>
						<match match="firstName"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<text fixtext=" "/>
					<template>
						<match match="lastName"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
				</children>
			</template>
			<template>
				<match match="address"/>
				<children>
					<newline/>
					<template>
						<match match="address1"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<newline/>
					<template>
						<match match="address2"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<newline/>
					<template>
						<match match="city"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<text fixtext=","/>
					<template>
						<match match="state"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<text fixtext=" "/>
					<template>
						<match match="zip"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
					<newline/>
					<template>
						<match match="country"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
				</children>
			</template>
			<newline/>
			<newline/>
			<text fixtext="Dear ">
				<styles font-weight="bold"/>
			</text>
			<template>
				<match match="name"/>
				<children>
					<template>
						<match match="firstName"/>
						<children>
							<xpath allchildren="1">
								<styles font-weight="bold"/>
							</xpath>
						</children>
					</template>
				</children>
			</template>
			<text fixtext=":">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="I have been notified that you are a ">
				<styles font-weight="bold"/>
			</text>
			<template>
				<styles font-weight="bold"/>
				<match match="financial"/>
				<children>
					<template>
						<match match="selectedStatus"/>
						<children>
							<select ownvalue="1">
								<properties size="0"/>
								<xpath value="//statuses/status" name="xpath"/>
							</select>
						</children>
					</template>
				</children>
			</template>
			<text fixtext=" status from school. Your student account has a balance of ">
				<styles font-weight="bold"/>
			</text>
			<template>
				<styles font-weight="bold"/>
				<match match="financial"/>
				<children>
					<template>
						<match match="balanceAmount"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
				</children>
			</template>
			<text fixtext=". This balance is your responsability. Please contact the business office within 10 days of this letter to discuss your payment arrangements.">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="I would like to offer my assistance in helping you clear up this financial obligation. I can be reached at 305-595-9500  from 9:00 am to 5:00 pm Monday thru Friday.">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="Sincerely,">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<text fixtext="Financial Aid Department">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<text fixtext="Barbara Arechavaleta">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
