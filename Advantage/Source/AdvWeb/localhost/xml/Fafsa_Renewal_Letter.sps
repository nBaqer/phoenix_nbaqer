<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\BASIC_STUDENT_INFO.xsd" workingxmlfile="Basic_Student_Info.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<styles background-attachment="fixed" background-image="url(http://www.fameinc.com/images/waterMkFAMEtrans.gif)" background-position="center center" background-repeat="no-repeat"/>
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=","/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
					<newline/>
					<text fixtext="Dear ">
						<styles font-weight="bold"/>
					</text>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<text fixtext=":">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<paragraph paragraphtag="p">
						<children>
							<newline/>
							<text fixtext="It is time to renew your Financial Aid due to a new award year starting on July 1, 2005. Please go to the following website www.fafsa.ed.gov and complete the 05-06 Free Application for Federal Student Aid (FAFSA). You will need to have your PIN number and tax returns and if you had to provide your parent&apos;s taxes last award year, please remember to have that information as well.">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<text fixtext="Once you complete the application, AMCollege will receive your application information. We will then call you to setup an appointment to review your application and determine your award. If you wish to receive Financial Aid for the Fall 2005, you must submit the application no later than August 1, 2005.">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<text fixtext="We look forward to seeing you soon.">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<newline/>
							<text fixtext="Sincerely,">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<newline/>
							<newline/>
							<text fixtext="Financial Aid Department">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<text fixtext="Barbara Arechavaleta">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<text fixtext="P.S. If you do not remember your PIN, please go to www.pin.ed.gov to request a duplicate.">
								<styles font-weight="bold"/>
							</text>
							<paragraph paragraphtag="p">
								<children>
									<newline/>
								</children>
							</paragraph>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
