<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\Academic_Probation_Form.xsd" workingxmlfile="Academic_Probation_Form.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext="  "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
				</children>
			</paragraph>
			<paragraph paragraphtag="p">
				<properties align="center"/>
				<children>
					<text fixtext="ACADEMIC PROBATION FORM">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<paragraph paragraphtag="p">
						<properties align="left"/>
						<children>
							<newline/>
							<template>
								<match match="name"/>
								<children>
									<template>
										<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
										<match match="firstName"/>
										<children>
											<xpath allchildren="1"/>
										</children>
									</template>
								</children>
							</template>
							<text fixtext="  "/>
							<template>
								<match match="name"/>
								<children>
									<template>
										<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
										<match match="lastName"/>
										<children>
											<xpath allchildren="1"/>
										</children>
									</template>
								</children>
							</template>
							<text fixtext=", you are on Academic Probation as of today for "/>
							<template>
								<match match="academicProbation"/>
								<children>
									<template>
										<match match="reason"/>
										<children>
											<xpath allchildren="1"/>
										</children>
									</template>
									<text fixtext="."/>
									<newline/>
									<text fixtext="Academic Probation is extremely serious. It means you have not maintained Satisfactory Progress at AMC. If you are a financial aid student, this means you may lose your financial aid. While you are on Academic Probation your attendance and academic progress will be carefully monitored."/>
									<newline/>
									<newline/>
									<text fixtext="You will remain on Academic Probation until you "/>
									<template>
										<match match="conditions"/>
										<children>
											<xpath allchildren="1"/>
										</children>
									</template>
									<text fixtext="."/>
									<newline/>
									<text fixtext="You have until "/>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring( endDate , 6 , 2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/"/>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring(  endDate , 9,2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/"/>
									<text>
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring( endDate , 1 , 4 )"/>
										</autocalc>
									</autovalue>
									<template>
										<styles font-weight="bold"/>
										<match match="endDate"/>
										<children>
											<datepicker ownvalue="1" datatype="date"/>
										</children>
									</template>
									<text fixtext="  to fulfill the requirements of this Academic Probation. If you have fulfilled these requirements by the above date, you will be removed from Academic Probation. At this time, your financial aid will be reinstated. However, you must continue to demonstrate satisfactory progress during the length of time you are a student at AMC."/>
									<newline/>
									<newline/>
									<text fixtext="Student&apos;s signature _____________________________"/>
									<newline/>
									<text fixtext="Academic Dean&apos;s signature_______________________"/>
									<newline/>
									<newline/>
								</children>
							</template>
						</children>
					</paragraph>
				</children>
			</paragraph>
			<text fixtext="Date: "/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="."/>
			<newline/>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
