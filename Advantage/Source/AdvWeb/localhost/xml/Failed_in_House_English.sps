<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\BASIC_STUDENT_INFO.xsd" workingxmlfile="Basic_Student_Info.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<paragraph paragraphtag="p">
						<children>
							<paragraph paragraphtag="p">
								<children>
									<paragraph paragraphtag="p">
										<properties align="center"/>
										<children>
											<paragraph paragraphtag="p">
												<properties align="left"/>
												<children>
													<text fixtext="Dear ">
														<styles font-weight="bold"/>
													</text>
													<template>
														<match match="name"/>
														<children>
															<template>
																<match match="firstName"/>
																<children>
																	<xpath allchildren="1">
																		<styles font-weight="bold"/>
																	</xpath>
																</children>
															</template>
															<text fixtext="  "/>
															<template>
																<match match="lastName"/>
																<children>
																	<xpath allchildren="1">
																		<styles font-weight="bold"/>
																	</xpath>
																</children>
															</template>
														</children>
													</template>
													<text fixtext=":">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="Unfortunately, you have not passed our in house English competency exam.  As a result, you must make an appointment to have an in person interview with me, so I can do an oral evaluation of your English language skills.">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="Sincerely,">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<newline/>
													<newline/>
													<text fixtext="Dr. Provitera">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<text fixtext="Academic Dean">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<text fixtext="
 
">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<newline/>
													<newline/>
													<text fixtext="Student&apos;s Signature______________________________________
">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<text fixtext="Date__________________________________________________">
														<styles font-weight="bold"/>
													</text>
													<paragraph paragraphtag="p">
														<properties align="left"/>
														<children>
															<newline/>
														</children>
													</paragraph>
												</children>
											</paragraph>
										</children>
									</paragraph>
								</children>
							</paragraph>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
