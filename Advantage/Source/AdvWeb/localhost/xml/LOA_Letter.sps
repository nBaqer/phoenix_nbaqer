<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\Leave_Of_Absence.xsd" workingxmlfile="" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<paragraph paragraphtag="p">
						<children>
							<paragraph paragraphtag="p">
								<children>
									<paragraph paragraphtag="p">
										<properties align="center"/>
										<children>
											<paragraph paragraphtag="p">
												<properties align="left"/>
												<children>
													<text fixtext="Dear ">
														<styles font-weight="bold"/>
													</text>
													<template>
														<match match="name"/>
														<children>
															<template>
																<match match="firstName"/>
																<children>
																	<xpath allchildren="1">
																		<styles font-weight="bold"/>
																	</xpath>
																</children>
															</template>
															<text fixtext="  "/>
															<template>
																<match match="lastName"/>
																<children>
																	<xpath allchildren="1">
																		<styles font-weight="bold"/>
																	</xpath>
																</children>
															</template>
														</children>
													</template>
													<text fixtext=":">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="You are presently scheduled to return from your Leave of Absence on ">
														<styles font-weight="bold"/>
													</text>
													<template>
														<styles font-weight="bold"/>
														<match match="leaveOfAbsence"/>
														<children>
															<template>
																<match match="date"/>
																<children>
																	<datepicker ownvalue="1" datatype="date"/>
																</children>
															</template>
															<autovalue>
																<styles font-weight="bold"/>
																<editorproperties editable="0"/>
																<autocalc>
																	<xpath value="substring( date , 6 , 2 )"/>
																</autocalc>
															</autovalue>
															<text fixtext="/"/>
															<autovalue>
																<styles font-weight="bold"/>
																<editorproperties editable="0"/>
																<autocalc>
																	<xpath value="substring(  date  , 9,2 )"/>
																</autocalc>
															</autovalue>
															<text fixtext="/"/>
															<text>
																<styles font-weight="bold"/>
															</text>
															<autovalue>
																<styles font-weight="bold"/>
																<editorproperties editable="0"/>
																<autocalc>
																	<xpath value="substring( date , 1 , 4 )"/>
																</autocalc>
															</autovalue>
															<text fixtext=". Please call the registrar&apos;s office at (305) 595-9500 to confirm your return. Unless we hear from you, classes will not be scheduled for you. We do hope that you plan to return and continue on with your program of studies at AMCollege. We have missed you during these past ">
																<styles font-weight="bold"/>
															</text>
															<template>
																<styles font-weight="bold"/>
																<match match="numberOfWeeks"/>
																<children>
																	<xpath allchildren="1"/>
																</children>
															</template>
															<text fixtext="  weeks.">
																<styles font-weight="bold"/>
															</text>
														</children>
													</template>
													<newline/>
													<newline/>
													<newline/>
													<text fixtext="Sincerel">
														<styles font-weight="bold"/>
													</text>
													<text fixtext="y,"/>
													<newline/>
													<newline/>
													<newline/>
													<newline/>
													<text fixtext="Maria Garcia">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<text fixtext="Registrar/Student Services Director
">
														<styles font-weight="bold"/>
													</text>
													<paragraph paragraphtag="p">
														<properties align="left"/>
													</paragraph>
												</children>
											</paragraph>
										</children>
									</paragraph>
								</children>
							</paragraph>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
