<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\Missing_Documents_Letter.xsd" workingxmlfile="Missing_Documents_Letter.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=","/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
					<newline/>
					<text fixtext="Dear ">
						<styles font-weight="bold"/>
					</text>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<text fixtext=":">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<paragraph paragraphtag="p">
						<children>
							<newline/>
							<text fixtext="The following document(s) are needed in your file to complete your Financial Aid Processing:">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<template>
								<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
								<match match="financialAidDocumentsRequired"/>
								<children>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="0" markupmode="hide"/>
										<match match="referenceInformation"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Reference Information"/>
									<newline/>
									<template>
										<match match="entranceInterviewForm"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Entrance Interview Form"/>
									<newline/>
									<template>
										<match match="understandingStudentLoanForm"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Understanding Student Loan Form"/>
									<newline/>
									<template>
										<match match="statementsAndAuthorizationForm"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
											<text fixtext=" "/>
										</children>
									</template>
									<text fixtext="Statement and Authorization Form"/>
									<newline/>
									<template>
										<match match="signatureOnLoanApplication"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Signature on Loan Application"/>
									<newline/>
									<template>
										<match match="taxesYearXXXX"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Taxes Year 2004"/>
									<newline/>
									<template>
										<match match="parentsTaxesYearXXXX"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Parent&apos;s Taxes Year 2004"/>
									<newline/>
									<template>
										<match match="copyOfNaturalizationCertificate"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Copy of Naturalization Certificate"/>
									<newline/>
									<template>
										<match match="copyOfPermanentResidencyCard"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Copy of Permanent Residency Card"/>
									<newline/>
									<template>
										<match match="signatureOnRevisedMethodOfPayment"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Signature on Revised Method of Payment"/>
									<newline/>
									<template>
										<match match="otherOrComments"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
											<text fixtext="  "/>
										</children>
									</template>
									<text fixtext="Other or Comments:"/>
									<newline/>
									<newline/>
								</children>
							</template>
							<newline/>
							<newline/>
							<text fixtext="Sincerely,">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<text fixtext="Financial Aid Department">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<text fixtext="Barbara Arechavaleta">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<paragraph paragraphtag="p">
								<children>
									<newline/>
									<newline/>
									<newline/>
									<newline/>
									<newline/>
									<newline/>
									<newline/>
									<newline/>
								</children>
							</paragraph>
						</children>
					</paragraph>
				</children>
			</paragraph>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
