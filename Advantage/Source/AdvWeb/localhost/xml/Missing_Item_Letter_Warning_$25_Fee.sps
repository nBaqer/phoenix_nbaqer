<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\Missing_Item_Letter_Warning_$25.xsd" workingxmlfile="" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
					<newline/>
					<text fixtext="Dear ">
						<styles font-weight="bold"/>
					</text>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<text fixtext=":">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<template>
						<match match="missingItems"/>
						<children>
							<paragraph paragraphtag="p">
								<children>
									<text fixtext="You have been notified numerous times that your student file is incomplete.  Unfortunately, you have not complied with our request to submit the missing item/ s listed below.  Applicants are required to submit all necessary documents prior to admission.">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<newline/>
									<text fixtext="You have until ">
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring( date , 6 , 2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/">
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring(  date  , 9,2 )"/>
										</autocalc>
									</autovalue>
									<text fixtext="/">
										<styles font-weight="bold"/>
									</text>
									<text>
										<styles font-weight="bold"/>
									</text>
									<autovalue>
										<styles font-weight="bold"/>
										<editorproperties editable="0"/>
										<autocalc>
											<xpath value="substring( date , 1 , 4 )"/>
										</autocalc>
									</autovalue>
									<template>
										<styles font-weight="bold"/>
										<match match="date"/>
										<children>
											<datepicker ownvalue="1" datatype="date"/>
										</children>
									</template>
									<text fixtext=" to submit the missing item/ s to the Registrar&apos;s Office. ">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<newline/>
									<text fixtext=" Failure to comply, will result in one or more of the following:">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<list>
										<styles margin-bottom="0" margin-top="0"/>
										<children>
											<listrow>
												<styles font-weight="bold"/>
												<children>
													<text fixtext="
a charge of $25.00 / a week fine, until all documents are turned in.">
														<styles font-weight="bold"/>
													</text>
												</children>
											</listrow>
											<listrow>
												<styles font-weight="bold"/>
												<children>
													<text fixtext="
suspension from classes until all documents are turned in. ">
														<styles font-weight="bold"/>
													</text>
												</children>
											</listrow>
											<listrow>
												<styles font-weight="bold"/>
												<children>
													<text fixtext="
dismissal from school">
														<styles font-weight="bold"/>
													</text>
												</children>
											</listrow>
										</children>
									</list>
									<newline/>
									<text fixtext="Items missing:">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<template>
										<match match="firstItem"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
									<newline/>
									<template>
										<match match="secondItem"/>
										<children>
											<xpath allchildren="1">
												<styles font-weight="bold"/>
											</xpath>
										</children>
									</template>
									<newline/>
									<newline/>
									<text fixtext="Sincerely,">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<newline/>
									<newline/>
									<newline/>
									<text fixtext="Maria Garcia">
										<styles font-weight="bold"/>
									</text>
									<newline/>
									<text fixtext="Registrar/Student Services Director">
										<styles font-weight="bold"/>
									</text>
								</children>
							</paragraph>
						</children>
					</template>
					<newline/>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
