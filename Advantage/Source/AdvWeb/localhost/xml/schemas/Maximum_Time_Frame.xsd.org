<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSpy v2005 rel. 3 U (http://www.altova.com) by Anatoly Sljussar (Fame, Inc.) -->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:element name="advantageMessage">
		<xs:annotation>
			<xs:documentation>Comment describing your root element</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="name">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="firstName"/>
							<xs:element name="middleName" minOccurs="0"/>
							<xs:element name="lastName"/>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="address">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="address1"/>
							<xs:element name="address2" minOccurs="0"/>
							<xs:element name="city"/>
							<xs:element name="state" minOccurs="0"/>
							<xs:element name="zip" minOccurs="0"/>
							<xs:element name="country" minOccurs="0"/>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="maximumTimeFrame">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="graduationDate" type="xs:date"/>
							<xs:element name="maximumTimeFrameDate" type="xs:date"/>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="name" type="xs:string" use="required"/>
			<xs:attribute name="recipientId" type="xs:string" use="required"/>
			<xs:attribute name="email" type="xs:string" use="optional"/>
			<xs:attribute name="studentId" type="xs:string" use="optional"/>
			<xs:attribute name="dateCreated" use="required">
				<xs:simpleType>
					<xs:restriction base="xs:dateTime">
						<xs:pattern value=""/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
</xs:schema>
