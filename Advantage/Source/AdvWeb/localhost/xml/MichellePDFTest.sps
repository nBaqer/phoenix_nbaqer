<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\MichellePDFTest.xsd" workingxmlfile="MichellePDFTest.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="document"/>
		<children>
			<text fixtext="Student Name: ">
				<styles font-weight="bold"/>
			</text>
			<template>
				<match match="studentName"/>
				<children>
					<xpath allchildren="1"/>
				</children>
			</template>
			<newline/>
			<newline/>
			<newline/>
			<template>
				<match match="enrollment"/>
				<children>
					<xpath allchildren="1"/>
				</children>
			</template>
			<template>
				<match match="term"/>
				<children>
					<xpath allchildren="1"/>
				</children>
			</template>
			<template>
				<match match="clsSection"/>
				<children>
					<xpath allchildren="1"/>
				</children>
			</template>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
			<template>
				<match match="attendance"/>
				<children>
					<table dynamic="1">
						<properties border="1"/>
						<children>
							<tableheader>
								<children>
									<tablerow>
										<children>
											<tablecol>
												<properties align="center"/>
												<children>
													<text fixtext="date">
														<styles font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<properties align="center"/>
												<children>
													<text fixtext="time">
														<styles font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<properties align="center"/>
												<children>
													<text fixtext="attend">
														<styles font-weight="bold"/>
													</text>
												</children>
											</tablecol>
										</children>
									</tablerow>
								</children>
							</tableheader>
							<tablebody>
								<children>
									<tablerow>
										<children>
											<tablecol>
												<children>
													<template>
														<match match="date"/>
														<children>
															<xpath allchildren="1"/>
														</children>
													</template>
												</children>
											</tablecol>
											<tablecol>
												<children>
													<template>
														<match match="time"/>
														<children>
															<xpath allchildren="1"/>
														</children>
													</template>
												</children>
											</tablecol>
											<tablecol>
												<children>
													<template>
														<match match="attend"/>
														<children>
															<xpath allchildren="1"/>
														</children>
													</template>
												</children>
											</tablecol>
										</children>
									</tablerow>
								</children>
							</tablebody>
						</children>
					</table>
				</children>
			</template>
			<newline/>
			<template>
				<match match="totalPresent"/>
				<children>
					<xpath allchildren="1"/>
				</children>
			</template>
			<template>
				<match match="totalAbsent"/>
				<children>
					<xpath allchildren="1"/>
				</children>
			</template>
			<template>
				<match match="totalTardy"/>
				<children>
					<xpath allchildren="1"/>
				</children>
			</template>
			<template>
				<match match="totalExcused"/>
				<children>
					<xpath allchildren="1"/>
				</children>
			</template>
			<newline/>
			<newline/>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
