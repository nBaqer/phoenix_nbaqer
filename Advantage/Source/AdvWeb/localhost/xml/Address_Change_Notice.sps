<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="http://localhost/AdvantageV1App/localhost/Xml/Schemas/Address_Change_Notice.xsd" workingxmlfile="Address_Change_Notice1.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<paragraph paragraphtag="h4">
				<properties align="center"/>
				<children>
					<text fixtext="Universal College">
						<styles font-family="Verdana"/>
					</text>
					<newline/>
					<text fixtext="Miami Dade Campus">
						<styles font-family="Verdana"/>
					</text>
				</children>
			</paragraph>
			<paragraph paragraphtag="h4">
				<properties align="center"/>
				<children>
					<text fixtext="(954)-123-4567"/>
				</children>
			</paragraph>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="SSN"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="address1"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<text fixtext="Miami,">
				<styles font-weight="bold"/>
			</text>
			<text fixtext=" "/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<newline/>
			<newline/>
			<template>
				<match match="lender"/>
				<children>
					<newline/>
					<template>
						<match match="name"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
					<newline/>
					<template>
						<match match="address1"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
					<newline/>
					<template>
						<match match="address2"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
					<newline/>
					<template>
						<match match="city"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
					<text fixtext=" "/>
					<template>
						<match match="state"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
					<text fixtext=" "/>
					<template>
						<match match="zip"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
					<newline/>
					<template>
						<match match="country"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
					<newline/>
				</children>
			</template>
			<newline/>
			<newline/>
			<paragraph paragraphtag="p">
				<properties align="center"/>
				<children>
					<text fixtext="NOTICE OF ADDRESS CHANGE">
						<styles font-weight="bold" text-decoration="underline"/>
					</text>
					<newline/>
				</children>
			</paragraph>
			<template>
				<match match="student"/>
				<children>
					<newline/>
					<template>
						<match match="name"/>
						<children>
							<newline/>
							<text fixtext="Re:">
								<styles font-weight="bold"/>
							</text>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<newline/>
						</children>
					</template>
					<newline/>
					<text fixtext="SSN: ">
						<styles font-weight="bold"/>
					</text>
					<template>
						<match match="SSN"/>
						<children>
							<xpath allchildren="1"/>
						</children>
					</template>
					<newline/>
					<newline/>
					<text fixtext="Attention Loan Service Department">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<newline/>
					<text fixtext="The above referenced student has changed his/her address.">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<newline/>
					<text fixtext="From:">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<template>
						<match match="oldAddress"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<newline/>
							<text fixtext="City: ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<text fixtext=" State:">
								<styles font-weight="bold"/>
							</text>
							<text fixtext=" "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<text fixtext="  "/>
							<text fixtext="Zip: ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<text fixtext=" "/>
							<newline/>
							<text fixtext="Country:">
								<styles font-weight="bold"/>
							</text>
							<text fixtext=" "/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<newline/>
						</children>
					</template>
					<newline/>
					<text fixtext="To:">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<template>
						<match match="newAddress"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<newline/>
							<text fixtext="City: ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<text fixtext=" State:">
								<styles font-weight="bold"/>
							</text>
							<text fixtext="  "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<text fixtext="  "/>
							<text fixtext="Zip: ">
								<styles font-weight="bold"/>
							</text>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<newline/>
							<text fixtext="Country:">
								<styles font-weight="bold"/>
							</text>
							<text fixtext=" "/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1"/>
								</children>
							</template>
							<newline/>
						</children>
					</template>
					<newline/>
				</children>
			</template>
			<newline/>
			<newline/>
			<newline/>
			<text fixtext="If you require further information, please contact me.">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<text fixtext="Sincerely,">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
			<newline/>
			<text fixtext="School Official">
				<styles font-weight="bold"/>
			</text>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="name"/>
		<children>
			<xpath allchildren="1"/>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" pageorientationportrait="0" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
		<coverpage>
			<template>
				<match overwrittenxslmatch="/"/>
				<children>
					<table>
						<properties align="center" width="80%"/>
						<children>
							<tablebody>
								<children>
									<tablerow>
										<children>
											<tablecol>
												<properties rowspan="2" width="20"/>
											</tablecol>
											<tablecol>
												<properties height="50"/>
											</tablecol>
										</children>
									</tablerow>
									<tablerow>
										<children>
											<tablecol>
												<children>
													<paragraph paragraphtag="fieldset">
														<children>
															<paragraph paragraphtag="center">
																<children>
																	<paragraph paragraphtag="h1">
																		<children>
																			<newline/>
																			<text fixtext="Document Title"/>
																			<newline/>
																			<newline/>
																		</children>
																	</paragraph>
																	<paragraph paragraphtag="h2">
																		<children>
																			<text fixtext="Author"/>
																			<newline/>
																			<newline/>
																			<newline/>
																		</children>
																	</paragraph>
																</children>
															</paragraph>
														</children>
													</paragraph>
												</children>
											</tablecol>
										</children>
									</tablerow>
								</children>
							</tablebody>
						</children>
					</table>
				</children>
			</template>
		</coverpage>
		<footerall>
			<template>
				<match overwrittenxslmatch="/"/>
				<children>
					<table topdown="0">
						<properties width="100%"/>
						<children>
							<tablebody>
								<children>
									<tablerow>
										<children>
											<tablecol>
												<styles padding="0"/>
												<properties colspan="2" height="30"/>
											</tablecol>
										</children>
									</tablerow>
									<tablerow>
										<children>
											<tablecol>
												<styles padding="0"/>
												<properties colspan="2"/>
												<children>
													<line>
														<properties color="black" size="1"/>
													</line>
												</children>
											</tablecol>
										</children>
									</tablerow>
									<tablerow>
										<children>
											<tablecol>
												<styles font-size="smaller" padding="0"/>
												<properties align="left"/>
												<children>
													<text fixtext="Document: C:\Projects\AdvantageV1\AdvantageV1AppSolution\AdvantageV1App\localhost\Xml\Address_Change_Notice1.xml">
														<styles font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<styles font-size="smaller" padding="0"/>
												<properties align="right" width="150"/>
												<children>
													<text fixtext="Page: ">
														<styles font-weight="bold"/>
													</text>
													<pagenumber>
														<styles font-weight="bold"/>
													</pagenumber>
												</children>
											</tablecol>
										</children>
									</tablerow>
								</children>
							</tablebody>
						</children>
					</table>
				</children>
			</template>
		</footerall>
		<headerall>
			<template>
				<match overwrittenxslmatch="/"/>
				<children>
					<table topdown="0">
						<properties width="100%"/>
						<children>
							<tablebody>
								<children>
									<tablerow>
										<children>
											<tablecol>
												<styles padding="0"/>
												<properties colspan="2" height="30"/>
											</tablecol>
										</children>
									</tablerow>
									<tablerow>
										<children>
											<tablecol>
												<styles font-size="smaller" padding="0"/>
												<properties align="left"/>
												<children>
													<text fixtext="Title: ">
														<styles font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<styles font-size="smaller" padding="0"/>
												<properties align="right" width="150"/>
												<children>
													<text fixtext="Page: ">
														<styles font-weight="bold"/>
													</text>
													<pagenumber>
														<styles font-weight="bold"/>
													</pagenumber>
												</children>
											</tablecol>
										</children>
									</tablerow>
									<tablerow>
										<children>
											<tablecol>
												<styles padding="0"/>
												<properties colspan="2"/>
												<children>
													<line>
														<properties color="black" size="1"/>
													</line>
												</children>
											</tablecol>
										</children>
									</tablerow>
								</children>
							</tablebody>
						</children>
					</table>
				</children>
			</template>
		</headerall>
	</pagelayout>
</structure>
