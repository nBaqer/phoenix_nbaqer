<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\BASIC_STUDENT_INFO.xsd" workingxmlfile="Basic_Student_Info.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<paragraph paragraphtag="p">
						<children>
							<paragraph paragraphtag="p">
								<children>
									<paragraph paragraphtag="p">
										<properties align="center"/>
										<children>
											<newline/>
											<paragraph paragraphtag="p">
												<properties align="left"/>
												<children>
													<text fixtext="Dear ">
														<styles font-weight="bold"/>
													</text>
													<template>
														<match match="name"/>
														<children>
															<template>
																<match match="firstName"/>
																<children>
																	<xpath allchildren="1">
																		<styles font-weight="bold"/>
																	</xpath>
																</children>
															</template>
															<text fixtext="  "/>
															<template>
																<match match="lastName"/>
																<children>
																	<xpath allchildren="1">
																		<styles font-weight="bold"/>
																	</xpath>
																</children>
															</template>
														</children>
													</template>
													<text fixtext=":">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="This letter is for informational purposes only, and does not constitute, in any way, legal advice.">
														<styles font-style="italic" font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="As a student with a criminal record, you should know your studies at the Acupuncture &amp; Massage College will not be affected.  Your privacy and confidentiality will be protected by all reasonable efforts, and you will be able to graduate from our educational program in:
  ">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="                     Massage Therapy or Oriental Medicine">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="However, because of your record, the National Examination Boards and the Florida State Licensing Boards for applicants for:">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="                     Massage Therapy or Oriental Medicine">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<text fixtext="will more than likely review your case individually prior to granting their approval for you to sit for administration of the national exam, or to grant state licensure.">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<newline/>
													<text fixtext="Sincerely,">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<newline/>
													<newline/>
													<text fixtext="I hereby acknowledge that I have read and understood the content of this notice.  I have raised any questions that I had at the time of this notice, and those have been addressed by the Acupuncture &amp; Massage College Administration.  I fully understand the above notice.  I am aware that any discussion about the legal implications of this notice should be addressed privately with an attorney, and/or for appropriate State Licensing Board.
">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<newline/>
													<newline/>
													<text fixtext="Student&apos;s Signature______________________________________
">
														<styles font-weight="bold"/>
													</text>
													<newline/>
													<text fixtext="Date__________________________________________________">
														<styles font-weight="bold"/>
													</text>
													<paragraph paragraphtag="p">
														<properties align="left"/>
														<children>
															<newline/>
														</children>
													</paragraph>
												</children>
											</paragraph>
										</children>
									</paragraph>
								</children>
							</paragraph>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
