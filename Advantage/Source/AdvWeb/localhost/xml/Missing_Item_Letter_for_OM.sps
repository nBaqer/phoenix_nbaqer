<?xml version="1.0" encoding="UTF-8"?>
<structure version="3" schemafile="Schemas\BASIC_STUDENT_INFO.xsd" workingxmlfile="Basic_Student_Info.xml" templatexmlfile="" xsltversion="1" encodinghtml="UTF-8" encodingrtf="ISO-8859-1" encodingpdf="UTF-8">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<newline/>
			<xpath allchildren="1"/>
			<newline/>
			<newline/>
			<newline/>
			<newline/>
		</children>
	</template>
	<template>
		<match match="advantageMessage"/>
		<children>
			<newline/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 6 , 2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring(  @dateCreated  , 9,2 )"/>
				</autocalc>
			</autovalue>
			<text fixtext="/"/>
			<text>
				<styles font-weight="bold"/>
			</text>
			<autovalue>
				<styles font-weight="bold"/>
				<editorproperties editable="0"/>
				<autocalc>
					<xpath value="substring( @dateCreated , 1 , 4 )"/>
				</autocalc>
			</autovalue>
			<paragraph paragraphtag="p">
				<children>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="lastName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<template>
						<match match="address"/>
						<children>
							<newline/>
							<template>
								<match match="address1"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="address2"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="city"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=", "/>
							<template>
								<match match="state"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" "/>
							<template>
								<match match="zip"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<template>
								<match match="country"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<newline/>
					<newline/>
					<text fixtext="Dear ">
						<styles font-weight="bold"/>
					</text>
					<template>
						<match match="name"/>
						<children>
							<template>
								<match match="firstName"/>
								<children>
									<xpath allchildren="1">
										<styles font-weight="bold"/>
									</xpath>
								</children>
							</template>
						</children>
					</template>
					<text fixtext=":">
						<styles font-weight="bold"/>
					</text>
					<newline/>
					<paragraph paragraphtag="p">
						<children>
							<newline/>
							<text fixtext="This letter is to inform you that we have reviewed your student file.  Unfortunately, we 
have found that your file is not yet complete and is missing the items highlighted below. 
">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<template>
								<styles font-weight="bold"/>
								<editorproperties adding="all" autoaddname="1" editable="1" markupmode="hide"/>
								<match match="optionalData"/>
								<children>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox1"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  Application Form."/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox2"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  Signed Enrollment Agreement"/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox3"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  Essay, &quot;Why do I want to be an Acupuncture Therapist?&quot;. "/>
									<newline/>
									<template>
										<editorproperties adding="mandatory" autoaddname="0" editable="1" markupmode="hide"/>
										<match match="checkbox4"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  2 Passport Size Photos."/>
									<newline/>
									<template>
										<match match="checkbox5"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  2 Letters of Recommendation."/>
									<newline/>
									<template>
										<match match="checkbox6"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  1 letter of recommendation arrived.
"/>
									<newline/>
									<template>
										<match match="checkbox7"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  College Transcripts / with 60 semester credits."/>
									<newline/>
									<template>
										<match match="checkbox8"/>
										<children>
											<checkbox ownvalue="1">
												<properties type="checkbox"/>
											</checkbox>
										</children>
									</template>
									<text fixtext="  Evaluated Transcripts (International students only)."/>
									<newline/>
									<text fixtext="
"/>
									<text fixtext="  
"/>
									<newline/>
									<newline/>
									<text fixtext="Not providing any of the above missing documents will affect the  processing of your financial aid.  This means you will be responsible for any tuition and fees incurred while in the program.">
										<styles text-decoration="underline"/>
									</text>
									<text fixtext=" Please submit these items to me as soon as possible.  It is very important that your file be completed immediately.  "/>
									<newline/>
									<newline/>
									<text fixtext="Thanking you in advance for your prompt attention to this matter.  Should you have any questions, please feel free to stop by my office or give me a call at your convenience.
  
 

"/>
									<newline/>
								</children>
							</template>
							<newline/>
							<text fixtext="Sincerely,">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<newline/>
							<newline/>
							<text fixtext="Maria Garcia">
								<styles font-weight="bold"/>
							</text>
							<newline/>
							<text fixtext="Registrar/Student Services Director">
								<styles font-weight="bold"/>
							</text>
						</children>
					</paragraph>
				</children>
			</paragraph>
		</children>
	</template>
	<pagelayout>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
	</pagelayout>
</structure>
