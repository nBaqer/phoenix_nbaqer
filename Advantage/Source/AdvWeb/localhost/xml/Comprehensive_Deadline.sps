<?xml version="1.0" encoding="UTF-8"?>
<structure version="4" xsltversion="1">
	<schemasources>
		<namespaces/>
		<schemasources>
			<xsdschemasource name="$XML" main="1" schemafile="Schemas\Comprehensive_Deadline.xsd">
				<xmltablesupport/>
				<textstateicons/>
			</xsdschemasource>
		</schemasources>
	</schemasources>
	<parameters/>
	<scripts>
		<javascript name="javascript"/>
	</scripts>
	<globalstyles/>
	<parts>
		<editorproperties/>
		<properties/>
		<styles/>
		<children>
			<globaltemplate match="/">
				<editorproperties/>
				<properties/>
				<styles/>
				<children>
					<template match="$XML">
						<editorproperties elementstodisplay="1"/>
						<properties/>
						<styles/>
						<children>
							<content>
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
								<addvalidations/>
								<format/>
							</content>
						</children>
						<addvalidations/>
					</template>
				</children>
			</globaltemplate>
			<globaltemplate match="advantageMessage">
				<editorproperties/>
				<properties/>
				<styles/>
				<children>
					<template match="advantageMessage">
						<editorproperties elementstodisplay="1"/>
						<properties/>
						<styles/>
						<children>
							<newline>
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
							</newline>
							<autocalc xpath="substring( @dateCreated , 6 , 2 )">
								<editorproperties/>
								<properties/>
								<styles font-weight="bold"/>
								<children/>
								<addvalidations/>
								<format/>
							</autocalc>
							<text fixtext="/">
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
							</text>
							<autocalc xpath="substring(  @dateCreated  , 9,2 )">
								<editorproperties/>
								<properties/>
								<styles font-weight="bold"/>
								<children/>
								<addvalidations/>
								<format/>
							</autocalc>
							<text fixtext="/">
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
							</text>
							<autocalc xpath="substring( @dateCreated , 1 , 4 )">
								<editorproperties/>
								<properties/>
								<styles font-weight="bold"/>
								<children/>
								<addvalidations/>
								<format/>
							</autocalc>
							<paragraph paragraphtag="p">
								<editorproperties/>
								<properties/>
								<styles/>
								<children>
									<template match="name">
										<editorproperties elementstodisplay="1"/>
										<properties/>
										<styles/>
										<children>
											<template match="firstName">
												<editorproperties editable="0" markupmode="hide" adding="mandatory" autoaddname="1"/>
												<properties/>
												<styles/>
												<children>
													<content>
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format datatype="anyType"/>
													</content>
												</children>
												<addvalidations/>
											</template>
											<text fixtext=" ">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
											<template match="lastName">
												<editorproperties editable="0" markupmode="hide" adding="mandatory" autoaddname="1"/>
												<properties/>
												<styles/>
												<children>
													<content>
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format datatype="anyType"/>
													</content>
												</children>
												<addvalidations/>
											</template>
										</children>
										<addvalidations/>
									</template>
									<template match="address">
										<editorproperties elementstodisplay="1"/>
										<properties/>
										<styles/>
										<children>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<template match="address1">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<content>
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format datatype="anyType"/>
													</content>
												</children>
												<addvalidations/>
											</template>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<template match="address2">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<content>
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format datatype="anyType"/>
													</content>
												</children>
												<addvalidations/>
											</template>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<template match="city">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<content>
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format datatype="anyType"/>
													</content>
												</children>
												<addvalidations/>
											</template>
											<text fixtext=", ">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
											<template match="state">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<content>
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format datatype="anyType"/>
													</content>
												</children>
												<addvalidations/>
											</template>
											<text fixtext="  ">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
											<template match="zip">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<content>
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format datatype="anyType"/>
													</content>
												</children>
												<addvalidations/>
											</template>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<template match="country">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<content>
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format datatype="anyType"/>
													</content>
												</children>
												<addvalidations/>
											</template>
										</children>
										<addvalidations/>
									</template>
									<newline>
										<editorproperties/>
										<properties/>
										<styles/>
										<children/>
									</newline>
								</children>
							</paragraph>
							<paragraph paragraphtag="p">
								<editorproperties/>
								<properties align="center"/>
								<styles/>
								<children>
									<text fixtext="MT Comprehensive Deadline">
										<editorproperties/>
										<properties/>
										<styles font-weight="bold"/>
										<children/>
									</text>
									<newline>
										<editorproperties/>
										<properties/>
										<styles/>
										<children/>
									</newline>
									<paragraph paragraphtag="p">
										<editorproperties/>
										<properties align="left"/>
										<styles/>
										<children>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<template match="name">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<template match="firstName">
														<editorproperties editable="0" markupmode="hide" adding="mandatory" autoaddname="1"/>
														<properties/>
														<styles/>
														<children>
															<content>
																<editorproperties/>
																<properties/>
																<styles/>
																<children/>
																<addvalidations/>
																<format datatype="anyType"/>
															</content>
														</children>
														<addvalidations/>
													</template>
												</children>
												<addvalidations/>
											</template>
											<text fixtext="  ">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
											<template match="name">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<template match="lastName">
														<editorproperties editable="0" markupmode="hide" adding="mandatory" autoaddname="1"/>
														<properties/>
														<styles/>
														<children>
															<content>
																<editorproperties/>
																<properties/>
																<styles/>
																<children/>
																<addvalidations/>
																<format datatype="anyType"/>
															</content>
														</children>
														<addvalidations/>
													</template>
												</children>
												<addvalidations/>
											</template>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<text fixtext="This letter is to acknowledge that you still need to pass the Comprehensive exam for the Massage Therapy program. We know that you have appreciated our efforts to ensure your success in this program and the accommodations we have made to date for you regarding retaking this exam.">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<text fixtext="You have until  ">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
											<template match="comprehensiveDeadline">
												<editorproperties elementstodisplay="1"/>
												<properties/>
												<styles/>
												<children>
													<autocalc xpath="substring( deadline , 6 , 2 )">
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format/>
													</autocalc>
													<text fixtext="/">
														<editorproperties/>
														<properties/>
														<styles/>
														<children/>
													</text>
													<autocalc xpath="substring(  deadline , 9,2 )">
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format/>
													</autocalc>
													<text fixtext="/">
														<editorproperties/>
														<properties/>
														<styles/>
														<children/>
													</text>
													<autocalc xpath="substring( deadline , 1 , 4 )">
														<editorproperties/>
														<properties/>
														<styles font-weight="bold"/>
														<children/>
														<addvalidations/>
														<format/>
													</autocalc>
													<template match="deadline">
														<editorproperties elementstodisplay="1"/>
														<properties/>
														<styles/>
														<children>
															<button>
																<editorproperties/>
																<properties/>
																<styles/>
																<children/>
																<action>
																	<datepicker/>
																</action>
															</button>
														</children>
														<addvalidations/>
													</template>
													<text fixtext=" ">
														<editorproperties/>
														<properties/>
														<styles/>
														<children/>
													</text>
												</children>
												<addvalidations/>
											</template>
											<text fixtext=" to fulfill the requirements of this Academic Probation. If you have fulfilled these requirements by the above date, you will be removed from Academic Probation. At this time, your financial aid will be reinstated. However, you must continue to demonstrate satisfactory progress during the length of time you are a student at AMC.">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<text fixtext="Student&apos;s signature _____________________________">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
											<newline>
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</newline>
											<text fixtext="Academic Dean&apos;s signature_______________________">
												<editorproperties/>
												<properties/>
												<styles/>
												<children/>
											</text>
										</children>
									</paragraph>
								</children>
							</paragraph>
							<text fixtext="Date: ">
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
							</text>
							<autocalc xpath="substring( @dateCreated , 6 , 2 )">
								<editorproperties/>
								<properties/>
								<styles font-weight="bold"/>
								<children/>
								<addvalidations/>
								<format/>
							</autocalc>
							<text fixtext="/">
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
							</text>
							<autocalc xpath="substring(  @dateCreated  , 9,2 )">
								<editorproperties/>
								<properties/>
								<styles font-weight="bold"/>
								<children/>
								<addvalidations/>
								<format/>
							</autocalc>
							<text fixtext="/">
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
							</text>
							<autocalc xpath="substring( @dateCreated , 1 , 4 )">
								<editorproperties/>
								<properties/>
								<styles font-weight="bold"/>
								<children/>
								<addvalidations/>
								<format/>
							</autocalc>
							<text fixtext=".">
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
							</text>
							<newline>
								<editorproperties/>
								<properties/>
								<styles/>
								<children/>
							</newline>
						</children>
						<addvalidations/>
					</template>
				</children>
			</globaltemplate>
		</children>
	</parts>
	<pagelayout>
		<editorproperties/>
		<properties pagemultiplepages="0" pagenumberingformat="1" pagenumberingstartat="1" paperheight="11in" papermarginbottom="0.79in" papermarginleft="0.6in" papermarginright="0.6in" papermargintop="0.79in" paperwidth="8.5in"/>
		<styles/>
		<children/>
	</pagelayout>
</structure>
