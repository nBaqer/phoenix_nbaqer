<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:strip-space elements='*'/>

   <xsl:template match="document">
   
         <fo:root>
  
      	<fo:layout-master-set>

	        <fo:simple-page-master master-name="page" margin='2.5cm'>
	          <fo:region-body region-name="body" />
	        </fo:simple-page-master>
	
		</fo:layout-master-set>

		<xsl:apply-templates select="deposits"/>

         </fo:root>
    	     
</xsl:template>
	
	
<xsl:template match="deposits">
        	<fo:page-sequence master-reference="page">
        	
        		<fo:flow flow-name="body">
										
										
        		<fo:block text-align='center' font='bold 14pt helvetica' space-after='16pt'>
        			Deposits
        		</fo:block>
        		
        		<fo:block font='bold 14pt helvetica'> </fo:block>
          		
  				<fo:table>
					<fo:table-body>
 						<fo:table-row>
							<fo:table-cell padding='5pt'>
								<fo:block font='bold 10pt helvetica'>Student Name</fo:block>
							</fo:table-cell>
							<fo:table-cell padding='5pt'>
								<fo:block font='bold 10pt helvetica'>Bank</fo:block>
							</fo:table-cell>
							<fo:table-cell padding='5pt'>
								<fo:block font='bold 10pt helvetica'>Bank Account</fo:block>
							</fo:table-cell>
							<fo:table-cell padding='5pt'>
								<fo:block font='bold 10pt helvetica'>Date</fo:block>
							</fo:table-cell>
							<fo:table-cell padding='5pt'>
								<fo:block font='bold 10pt helvetica'>Reference</fo:block>
							</fo:table-cell>
							<fo:table-cell padding='5pt'>
								<fo:block text-align='right' font='bold 10pt helvetica'>Amount</fo:block>
							</fo:table-cell>
						</fo:table-row>				
   						
   						<xsl:apply-templates select="deposit"/>
 						
 						<fo:table-row>
							<fo:table-cell></fo:table-cell>
							<fo:table-cell></fo:table-cell>
							<fo:table-cell></fo:table-cell>
							<fo:table-cell></fo:table-cell>
							<fo:table-cell></fo:table-cell>
							<fo:table-cell>
								<fo:block text-align='right' font='bold 10pt helvetica' background-color='black' color='white'>
      								<xsl:value-of select="@totalAmount"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
   					</fo:table-body>
				</fo:table>
				
				<fo:block space-before='16pt' text-align='center'>
					<fo:external-graphic src="url(http://www.fameinc.com/images/news_and_events/Jungle.jpg)" />
				</fo:block> 
				    	
	        	</fo:flow>
        	</fo:page-sequence>
</xsl:template>

<xsl:template match="deposit">

			<fo:table-row>
				<fo:table-cell padding='2pt'>
					<fo:block font='8pt helvetica'>
	      				<xsl:value-of select="@studentName"/>				
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding='2pt'>
					<fo:block font='8pt helvetica'>
      					<xsl:value-of select="@bank"/> 
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding='2pt'>
					<fo:block font='8pt helvetica'>
      					<xsl:value-of select="@bankAccount"/> 
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding='2pt'>
					<fo:block font='8pt helvetica'>
      					<xsl:value-of select="@date"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding='2pt'>
					<fo:block font='8pt helvetica'>
      					<xsl:value-of select="@reference"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding='2pt'>
					<fo:block text-align='right' font='8pt helvetica'>
      					<xsl:value-of select="@amount"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>

        	
</xsl:template>
	
</xsl:stylesheet>
