<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:strip-space elements='*'/>
  <xsl:template match="document">
    <xsl:apply-templates select="deposits"/>
  </xsl:template>

  <xsl:template match="deposits">
    <HEAD>
      <title>Print Deposits</title>
      
      <style>
        .subheader
        {
        font: bold 11pt Verdana;
        color: #000066;
        text-align: center;
        background-color: #fff;
        border: 1px solid #ebebeb;
        padding: 3px;
        vertical-align: middle;
        }
        .SubSubHeader
        {
        font: bold 11pt Verdana;
        color: #000066;
        text-align: left;
        background-color: #E9EDF2;
        border-top: 1px solid #ebebeb;
        border-left: 1px solid #ebebeb;
        border-bottom: 1px solid #ebebeb;
        padding: 3px;
        vertical-align: middle;
        }
        .contentcell
        {
        font: normal 11pt Verdana;
        color: #000066;
        text-align: left;
        background-color: #fff;
        border-top: 1px solid #ebebeb;
        border-left: 1px solid #ebebeb;
        border-bottom: 1px solid #ebebeb;
        padding: 3px;
        vertical-align: middle;
        }
        .contentcells
        {
        font: normal 11pt Verdana;
        color: #000066;
        text-align: left;
        padding: 3px;
        border-top: 1px solid #ebebeb;
        border-left: 1px solid #ebebeb;
        border-bottom: 1px solid #ebebeb;
        vertical-align: middle;
        }
        .scrollframe
        {
        overflow: visible;
        width: 100%;
        bottom: 0;
        position: absolute;
        top: 0;
        height: auto;
        left:0;
        padding: 20px;
        margin: 0;
        }
      </style>
    </HEAD>
    <body>
      <div class="scrollframe">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="6" class="SubHeader" style="border-bottom: 0px">Deposits</td>
          </tr>
          <tr>
            <td class="SubSubHeader" style="width: 20%">Name</td>
            <td class="SubSubHeader" style="width: 20%">Lead&#47;Student</td>
            <td class="SubSubHeader" style="width: 20%">Student Number</td>
            <td class="SubSubHeader" style="width: 20%">Description</td>
            <td class="SubSubHeader" style="width: 20%">Bank Account</td>
            <td class="SubSubHeader" style="width: 15%">Date</td>
            <td class="SubSubHeader" style="width: 15%">Reference</td>
            <td class="SubSubHeader" style="width: 15%">Deposited&#63;</td>
            <td class="SubSubHeader" style="border-right: 1px solid #ebebeb; width: 10%">Amount</td>
          </tr>

          <xsl:apply-templates select="deposit"/>

          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="SubSubHeader" style="border-right: 1px solid #ebebeb; border-top: 0px">
              <xsl:value-of select="@totalAmount"/>
            </td>
          </tr>
          <tr>
            <td colspan="6" align="center">
              <P>
                <!--<img src="http://www.fameinc.com/images/news_and_events/Jungle.jpg" />-->
              </P>
            </td>
          </tr>
        </table>
      </div>
    </body>

  </xsl:template>

  <xsl:template match="deposit">

    <tr>
      <td class="contentcells">
        <xsl:value-of select="@Name"/>
      </td>
      <td class="contentcells">
        <xsl:value-of select="@Lead-Student"/>
      </td>
      <td class="contentcells">
        <xsl:value-of select="@StudentNumber"/>
      </td>
      <td class="contentcells">
        <xsl:value-of select="@Description"/>
      </td>
      <td class="contentcells">
        <xsl:value-of select="@BankAccount"/>
      </td>
      <td class="contentcells">
        <xsl:value-of select="@Date"/>
      </td>
      <td class="contentcells">
        <xsl:value-of select="@Reference"/>
      </td>
      <td class="contentcells">
        <xsl:value-of select="@Deposited"/>
      </td>
      <td class="contentcells" style="border-right: 1px solid #ebebeb; width: 10%">
        <xsl:value-of select="@Amount"/>
      </td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
