<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns="http://my.netscape.com/rdf/simple/0.9/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:rss="http://purl.org/rss/1.0/" xmlns="http://purl.org/rss/1.0/">
<xsl:output method="html" omit-xml-declaration="yes" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" />
	<xsl:template match="/">
		<style type="text/css">
			TABLE.links {
				border-collapse : collapse;
				border-color : gray; 
				margin-left: 14px;
			}
			.desc {
				color : darkgray;
				font : 8pt Verdana Bold, Helvetica, sans-serif;
			}
		</style>
		<div Style=" overflow: auto; height=465; ">
		<table width="100%">
			<tr>
				<td valign="middle"><span class="SubsubHead"><xsl:value-of select="//rss:channel/rss:title|//channel/title|//ns:channel/ns:title"/></span></td>
			</tr>
		</table>
		
		<p>
			<table border="1" class="links" rules="rows" frame="void">
				<xsl:apply-templates select="//rss:item|//item|//ns:item"/>
			</table>
		</p>
		
		<table width="100%">
			<tr>
				<td valign="middle"><xsl:apply-templates select="//rss:image|//image|//ns:image"/></td>
			</tr>
		</table>
		</div>
	</xsl:template>
	
	<xsl:template match="rss:item|item|ns:item">
		<tr><td>
		<a class="Normal" target="RSS">
			<xsl:attribute name="href"><xsl:value-of select="./rss:link|./link|./ns:link"/></xsl:attribute>
			<xsl:value-of select="./rss:title|./title|./ns:title"/>
		</a>
		<xsl:if test="./rss:description|./description|./ns:description">
			<br/>
			<span class="desc"><xsl:value-of select="./rss:description|./description|./ns:descripton" disable-output-escaping="yes"/></span><br/>
		</xsl:if>
		</td></tr>
	</xsl:template>	
	
	<xsl:template match="rss:image|image|ns:image">
		<xsl:if test="./rss:url|./url|./ns:url">
			<a target="RSS">
				<xsl:attribute name="href"><xsl:value-of select="./rss:link|./link|./ns:link"/></xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="./rss:url|./url|./ns:url"/></xsl:attribute>
					<xsl:attribute name="alt"><xsl:value-of select="./rss:title|./title|./ns:title"/></xsl:attribute>
					<xsl:if test="./rss:width|./width|./ns:width">
						<xsl:attribute name="width"><xsl:value-of select="./rss:width|./width|./ns:width"/></xsl:attribute>
						<xsl:attribute name="height"><xsl:value-of select="./rss:height|./height|./ns:height"/></xsl:attribute>
					</xsl:if>
				</img>
			</a>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>

  