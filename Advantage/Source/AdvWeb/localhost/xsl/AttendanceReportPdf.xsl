<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:variable name="fo:layout-master-set">
        <fo:layout-master-set>
            <fo:simple-page-master master-name="default-page" page-height="11in" page-width="8.5in" margin-left="0.6in" margin-right="0.6in">
                <fo:region-body margin-top="0.79in" margin-bottom="0.79in" />
            </fo:simple-page-master>
        </fo:layout-master-set>
    </xsl:variable>
    <xsl:output version="1.0" encoding="UTF-8" indent="no" omit-xml-declaration="no" media-type="text/html" />
    <xsl:template match="/">
        <fo:root>
            <xsl:copy-of select="$fo:layout-master-set" />
            <fo:page-sequence master-reference="default-page" initial-page-number="1" format="1">
                <fo:flow flow-name="xsl-region-body">
                    <fo:block>
                        <xsl:apply-templates />
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template match="document">
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:block>
            <xsl:text>&#xA;</xsl:text>
        </fo:block>
        <fo:table width="100%" space-before.optimum="1pt" space-after.optimum="2pt">
            <fo:table-column column-width="50%" />
            <fo:table-column column-width="50%" />
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" text-align="right" width="50%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Student Name:</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                        <fo:block>
                            <xsl:for-each select="studentName">
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:apply-templates />
                                </fo:inline>
                            </xsl:for-each>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" text-align="right" width="50%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Enrollment:</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                        <fo:block>
                            <xsl:for-each select="enrollment">
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:apply-templates />
                                </fo:inline>
                            </xsl:for-each>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" text-align="right" width="50%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Term:</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                        <fo:block>
                            <xsl:for-each select="term">
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:apply-templates />
                                </fo:inline>
                            </xsl:for-each>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" text-align="right" width="50%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Class Section:</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                        <fo:block>
                            <xsl:for-each select="clsSection">
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:apply-templates />
                                </fo:inline>
                            </xsl:for-each>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:table width="100%" space-before.optimum="1pt" space-after.optimum="2pt">
			<fo:table-column />
            <fo:table-column />
            <fo:table-column />
            <fo:table-column />
            <fo:table-header>
                <fo:table-row>					
                    <fo:table-cell border-style="solid" border-width="1pt" border-color="black" text-align="left" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Date</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="1pt" border-color="black" text-align="left" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Time</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="1pt" border-color="black" text-align="left" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Attendance</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-header>
            <fo:table-body>
                <xsl:for-each select="attendance">
                    <fo:table-row>						
                        <fo:table-cell border-style="solid" border-width="1pt" border-color="black" text-align="left" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                            <fo:block>
								<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:value-of select="substring(dayOfWeek,1,3)" />
                                </fo:inline>
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">, </fo:inline>
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:value-of select="substring(date,6,2)" />
                                </fo:inline>
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">/</fo:inline>
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:value-of select="substring(date,9,2)" />
                                </fo:inline>
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">/</fo:inline>
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:value-of select="substring(date,1,4)" />
                                </fo:inline>
                                <xsl:for-each select="date" />
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell border-style="solid" border-width="1pt" border-color="black" text-align="left" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                            <fo:block>
                                <xsl:for-each select="time">
                                    <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                        <xsl:apply-templates />
                                    </fo:inline>
                                </xsl:for-each>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell border-style="solid" border-width="1pt" border-color="black" text-align="left" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                            <fo:block>
                                <xsl:for-each select="attend">
                                    <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                        <xsl:apply-templates />
                                    </fo:inline>
                                </xsl:for-each>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </xsl:for-each>
            </fo:table-body>
        </fo:table>
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:block>
            <xsl:text>&#xA;</xsl:text>
        </fo:block>
        <fo:table width="100%" space-before.optimum="1pt" space-after.optimum="2pt">
            <fo:table-column column-width="50%" />
            <fo:table-column column-width="50%" />
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" text-align="right" width="50%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Total Present:</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                        <fo:block>
                            <xsl:for-each select="totalPresent">
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:apply-templates />
                                </fo:inline>
                            </xsl:for-each>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" text-align="right" width="50%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Total Absent:</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                        <fo:block>
                            <xsl:for-each select="totalAbsent">
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:apply-templates />
                                </fo:inline>
                            </xsl:for-each>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" text-align="right" width="50%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Total Tardy:</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                        <fo:block>
                            <xsl:for-each select="totalTardy">
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:apply-templates />
                                </fo:inline>
                            </xsl:for-each>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" text-align="right" width="50%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center">
                        <fo:block>
                            <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold">Total Excused:</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-style="solid" border-width="0pt" border-color="black" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                        <fo:block>
                            <xsl:for-each select="totalExcused">
                                <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
                                    <xsl:apply-templates />
                                </fo:inline>
                            </xsl:for-each>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">&#160;</fo:inline>
        <fo:block>
            <fo:leader leader-pattern="space" />
        </fo:block>
        <fo:block>
            <xsl:text>&#xA;</xsl:text>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>
