<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:strip-space elements='*'/>
   <xsl:template match="document">
		<xsl:apply-templates select="notes"/>
</xsl:template>
	
<xsl:template match="notes">
  	<HEAD>
		<title>Print Lead Notes</title>
		
      <style>
        .subheader
        {
        font: bold 11px Verdana;
        color: #000066;
        text-align: center;
        background-color: #fff;
        border: 1px solid #ebebeb;
        padding: 5px;
        vertical-align: middle;
        }
        .SubSubHeader
        {
        font: bold 11px Verdana;
        color: #000066;
        text-align: left;
        background-color: #E9EDF2;
        border: 1px solid #ebebeb;
        padding: 5px;
        vertical-align: middle;
        }
        .contentcell
        {
        font: normal 11px Verdana;
        color: #000066;
        text-align: left;
        background-color: #fff;
        border: 1px solid #ebebeb;
        padding: 5px;
        vertical-align: middle;
        }
        .scrollframe
        {
        overflow: visible;
        width: 100%;
        bottom: 0;
        position: absolute;
        top: 0;
        height: auto;
        left:0;
        padding: 20px;
        margin: 0;
        }
      </style>
	</HEAD>
			<body>
        <div class="scrollframe">
          <table width="100%" cellpadding="0" cellspacing="0">

            <xsl:apply-templates select="notehdr"/>

           
           
            <xsl:apply-templates select="note"/>

          
          </table>
        </div>   	
			</body>  		
	        	
</xsl:template>


<xsl:template match="notehdr">

      <tr>
        <td colspan="4" class="SubHeader" style="border-bottom: 0px">
          <xsl:value-of select="@leadName"/>
        </td>
      </tr>

      <tr>
        <td class="SubSubHeader" style="width: 20%">Date</td>
        <td class="SubSubHeader" style="width: 20%">Module</td>
        <td class="SubSubHeader" style="width: 20%">User</td>
        <td class="SubSubHeader" style="width: 40%">Note</td>
      </tr>

    
</xsl:template>
  
  
<xsl:template match="note">

			<tr>       
        <td class="contentcell">
          <xsl:value-of select="@date"/>
        </td>
        <td class="contentcell">
          <xsl:value-of select="@module"/>
        </td>
        <td class="contentcell">
          <xsl:value-of select="@user"/>
        </td>
        <td class="contentcell" style="width: 400px;">
      		<xsl:value-of select="@noteDesc"/>
				</td>			
			</tr>
	
</xsl:template>
	
</xsl:stylesheet>
