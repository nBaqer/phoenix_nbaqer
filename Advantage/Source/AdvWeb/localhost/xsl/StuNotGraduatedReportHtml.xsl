<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template match="/">

<html>
	<head>
		<title>Status Changes - List of Students Not Graduated</title>
		<link href="../CSS/localhost.css" type="text/css" rel="stylesheet" />
		<link href="../CSS/print.css" type="text/css" media="print" rel="stylesheet" />
	</head>
	
	<body>			
		<xsl:for-each select="document">
			<table border="0" cellspacing="2" cellpadding="2">
				<thead>
					<tr>
						<td></td>
					</tr>
					<tr>
						<th align="center">These students were not graduated because they have not completed the program or their graduation requirements are not met.			
						</th>					
					</tr>				
				</thead>
				<tbody>
					<tr>
						<td></td>
					</tr>				
				</tbody>				
			</table>		
							
			<xsl:for-each select="studentsNoGraduated">
				<table border="2" align="center" width="80%" bordercolor="#003399" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th>No.</th>
							<th>Student Name</th>
							<th>Program Version</th>
							<th>Reason</th>
						</tr>
					</thead>
					<tbody>
						<xsl:for-each select="student">
							<tr>
								<td>
									<xsl:value-of select="counter" />
								</td>
								<td>
									<xsl:value-of select="studentName" />
								</td>
								<td>
									<xsl:value-of select="progVersion" />
								</td>
								
								<td>
									<xsl:value-of select="Reason" />
									<xsl:for-each select="documents">
										<xsl:value-of select="docs" />
										<br/>
									</xsl:for-each>
									</td>
								
							</tr>
						</xsl:for-each>						
					</tbody>
				</table>
			</xsl:for-each>				
		</xsl:for-each>		
	</body>
</html>
</xsl:template>
</xsl:stylesheet>