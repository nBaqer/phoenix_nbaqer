<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:strip-space elements='*'/>

  <xsl:template match="receipts">
    <fo:root >
      <fo:layout-master-set>
        <fo:simple-page-master master-name="page" margin=".5in">
          <fo:region-body region-name="Content" margin="0.5in 0in" padding="6pt" />
          <fo:region-before region-name="Header" extent="0.5in" padding="6pt"/>
          <fo:region-after region-name="Footer" extent="0.5in" padding="6pt"
                                precedence="true" />
          <fo:region-start region-name="LeftSide" extent="0in"/>
          <fo:region-end region-name="RightSide" extent="0in"/>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="default-sequence">
          <fo:repeatable-page-master-reference master-reference="page" />
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <xsl:apply-templates select="receipt"/>
    </fo:root>
  </xsl:template>

  <xsl:template match="receipt">
    <fo:page-sequence master-reference="default-sequence">
      <fo:static-content flow-name="Header">
        <fo:block text-align="end"  font-size="22pt" background-color="#b3b4b6" color="#333" padding-top=".1in" border-width=".05mm" border-style="solid" border-color="#666">
        STUDENT RECEIPT
        </fo:block>
      </fo:static-content>
      <fo:static-content flow-name="Footer">
        <fo:block>
          <fo:block text-align="end" font-size="22pt" background-color="#b3b4b6" color="#333" padding-top=".1in" line-height=".3in"  border-width=".05mm" border-style="solid" border-color="#666">
            
          </fo:block>
        </fo:block>
      </fo:static-content>
      <fo:static-content flow-name="LeftSide">
        <fo:block></fo:block>
      </fo:static-content>
      <fo:static-content flow-name="RightSide">
        <fo:block></fo:block>
      </fo:static-content>
      <fo:flow flow-name="Content">
        <fo:block text-align='start' font-size="16pt" font-weight="bold" space-before="20pt" space-after="4pt">
          <xsl:value-of select="schoolName"/>
        </fo:block>
        <fo:block text-align='start' font-size="12pt" font-style="italic" font-weight="normal" space-before="1pt" space-after="1pt">
          <xsl:value-of select="schoolAddress1"/>
        </fo:block>
        <fo:block text-align='start' font-size="12pt" font-style="italic" font-weight="normal" space-before="1pt" space-after="1pt">
          <xsl:value-of select="schoolAddress2"/>
        </fo:block>
        <fo:block text-align='start' font-size="12pt" font-style="italic" font-weight="normal" space-before="1pt" space-after="1pt">
          <xsl:value-of select="city"/><xsl:if test=". !=''">, </xsl:if><xsl:value-of select="state"/>
          <xsl:if test=". !=''">, </xsl:if><xsl:value-of select="zip"/>
        </fo:block>
		  <fo:block text-align='start' font-size="12pt" font-style="italic" font-weight="normal" space-before="1pt" space-after="1pt">
			  <xsl:value-of select="country"/>
		  </fo:block>
        <fo:block space-before="25pt" space-after="0pt">
          <fo:inline font-weight="bold">Received From: </fo:inline>
          <fo:inline>
            <xsl:value-of select="name"/>
          </fo:inline>
          <fo:block text-align='start' font-size="12pt" font-style="italic" font-weight="normal" space-before="1pt" space-after="1pt">
            <xsl:value-of  select="studentAddress1"/>
          </fo:block>
          <fo:block text-align='start' font-size="12pt" font-style="italic" font-weight="normal" space-before="1pt" space-after="1pt">
            <xsl:value-of select="studentAddress2"/>
          </fo:block>
          <fo:block text-align='start' font-size="12pt" font-style="italic" font-weight="normal" space-before="1pt" space-after="1pt">
            <xsl:value-of select="studentCity"/><xsl:if test="string-length(studentCity)!=0">, </xsl:if><xsl:value-of select="studentState"/>
            <xsl:if test="string-length(studentState)!=0">, </xsl:if> <xsl:value-of select="studentZip"/>
          </fo:block>
			<fo:block text-align='start' font-size="12pt" font-style="italic" font-weight="normal" space-before="1pt" space-after="1pt">
				<xsl:value-of select="studentCountry"/>
			</fo:block>
        </fo:block>
        <!--date and time block-->
        <fo:block text-align='center' space-after='25pt' space-before="25pt" font-size="11pt">
          <fo:table text-align="start" border-top="3pt solid #666" border-bottom=".2pt solid #666">
            <fo:table-column column-width="20%"/>
            <fo:table-column column-width="20%"/>
            <fo:table-column column-width="20%"/>
			  <fo:table-column column-width="20%"/>
			  <fo:table-column column-width="20%"/>
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
                  <fo:block>
                    DATE
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
                  <fo:block>
                    TIME
                  </fo:block>
                </fo:table-cell>
				  <fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
					  <fo:block>
					
					  </fo:block>
				  </fo:table-cell>
				  <fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
					  <fo:block>
						
					  </fo:block>
				  </fo:table-cell>
				  <fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
					  <fo:block>

					  </fo:block>
				  </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="6pt" text-align="center" border-left=".2pt solid #666">
                  <fo:block>
                    <xsl:value-of select="date"/>
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="6pt" text-align="center" border-left=".2pt solid #666" border-right=".2pt solid #666">
                  <fo:block>
                    <xsl:value-of select="time"/>
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="6pt" text-align="center" border-right=".2pt solid #666">
                  <fo:block>
        
                  </fo:block>
                </fo:table-cell>
				  <fo:table-cell padding="6pt" text-align="center" border-right=".2pt solid #666">
					  <fo:block>
					  </fo:block>
				  </fo:table-cell>
				  <fo:table-cell padding="6pt" text-align="center" border-right=".2pt solid #666">
					  <fo:block>
					  </fo:block>
				  </fo:table-cell>
              </fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
						<fo:block>
							Transaction ID
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
						<fo:block>
							Transaction Code
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
						<fo:block>
							Payment Type
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
						<fo:block>
							Document ID
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="4pt" font-size="10pt" text-align="center" background-color="#ebeced" border=".2pt solid #666">
						<fo:block>
							Amount Received
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="6pt" text-align="center" border-left=".2pt solid #666">
						<fo:block>
							<xsl:value-of select="uniqueId"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="6pt" text-align="center" border-left=".2pt solid #666" border-right=".2pt solid #666">
						<fo:block>
							<xsl:value-of select="TransDesc"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="6pt" text-align="center" border-right=".2pt solid #666">
						<fo:block>
							<xsl:value-of select="paymenttypeDesc"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="6pt" text-align="center" border-right=".2pt solid #666">
						<fo:block>
							<xsl:value-of select="CheckNumber"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="6pt" text-align="center" border-right=".2pt solid #666">
						<fo:block>
							$<xsl:value-of select="amount"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
            </fo:table-body>
          </fo:table>
        </fo:block>
      </fo:flow>
    </fo:page-sequence>
    
  </xsl:template>
</xsl:stylesheet>
