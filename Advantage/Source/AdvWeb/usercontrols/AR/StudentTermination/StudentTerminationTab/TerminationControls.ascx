﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TerminationControls.ascx.vb" Inherits="usercontrols_AR_StudentTermination_StudentTerminationTab_TerminationControls" %>
<div class="buttonDiv">
    <hr class="top-margin20px">
    <div class="inLineBlock top-margin20px">
        <span class="disabled">
            <button type="button">
                &lt; Back
            </button>
        </span>
        <button type="button" id="btnNext">
            Next &gt;
        </button>
    </div>
    <div class="inlineBtn">
        <span class="disabled" id="spnSave">
            <button type="button" id="btnSaveTermination" style="color: rgba(255, 255, 255, 0.9) !important;background-color: #1976d2 !important;">
                Save
            </button>
        </span>
        <span class="disabled" id="spnCancel">
        <button type="button" id="btnCancelTermination">
            Cancel
        </button>
            </span>
    </div>
    <div class="disabled inLineBlock top-margin7px">
        <input type="hidden" id="hdnTerminationId" />
        <input type="hidden" id="hdnEnrollmentId" />
        <button type="button" style="display:none;">
            Approve Termination
        </button>
    </div>
</div>