﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EnrollmentDetails.ascx.vb" Inherits="usercontrols_AR_StudentTermination_StudentTerminationTab_EnrollmentDetails" %>
<div id="dvEnrollment">
    <div class="formRow">
        <div class="left leftLable vertical-align-top">
            Enrollment <span class="red-color">*</span>
        </div>
        <div class="inLineBlock">
            <div id="studentEnrollment"></div>
            <span id="spnEnrollment" style="color: red; display: none;">Please select an active enrollment. It is required.</span>
        </div>
    </div>
    <div class="formRow">
        <div class="left leftLable">
            Status <span class="red-color">*</span>
        </div>
        <div class="inLineBlock left-margin">
            <input id="ddlStatus" class="state-dropdown-mine form-control k-textbox fieldlabel1 inputCustom " />
        </div>
    </div>
    <div class="formRow">
        <div class="left leftLable">
            Drop reason <span class="red-color">*</span>
        </div>
        <div class="inLineBlock left-margin">
            <input id="ddlDropReason" name="ddlStatus" class="form-control k-textbox fieldlabel1 inputCustom" />
        </div>
    </div>
    <div class="formRow">
        <div class="left leftLable">
            Date of determination <span class="red-color">*</span>
        </div>
        <div class="inLineBlock">
            <input id="dtDateOfDetermination" name="dtDateOfDetermination" placeholder="MM/DD/YYYY" class="k-datepicker dateCustom" />
            <span>&nbsp;&nbsp;&nbsp;(Date withdrawal determined)</span>
        </div>
    </div>
    <div class="formRow">
        <div class="left leftLable">
            <span id="LDA">Last date attended</span> <span id="spLDA" class="red-color"></span>
        </div>
        <div class="inLineBlock">
            <input id="dtLastDateAttended" name="dtLastDateAttended" placeholder="MM/DD/YYYY" class="k-datepicker dateCustom" />
        </div>
    </div>
    <div class="formRow">
        <div class="left leftLable">
            Perform R2T4
        </div>
        <div class="inLineBlock">
            <label>
                <input type="checkbox" id="chkClaculationPeriod" readonly />
            </label>
        </div>
    </div>
    <div class="formRow u-hide" id="dvCalculationPeriod">
        <div class="left leftLable ">
            Period used for calculation
        </div>
        <div class="inLineBlock">
            <div id="periodName"></div>
        </div>
    </div>
</div>