﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentSearch.ascx.vb" Inherits="usercontrols_AR_StudentTermination_StudentTerminationTab_StudentSearch" %>
<div class="formRow">
    <div class="left leftLable">
        Student <span class="red-color">*</span>
    </div>
    <div class="inLineBlock">
        <input type="text" id="txtStudentSearch" class="form-control k-textbox inputCustom" maxlength="152" required ; autofocus="autofocus"/>
    </div>
</div>