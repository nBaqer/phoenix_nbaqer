﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentInformation.ascx.vb" Inherits="usercontrols_AR_StudentTermination_ApprovalTerminationTab_StudentInformation" %>
<div>
    <div class="firstRow">
        <div class="inLineBlock width15PerMargin20">
            <span class="firstRowLabel">Student:</span>
            <span id="lblStundentNameAppTerm"></span>
        </div>

        <div class="inLineBlock width12PerMargin20">
            <span class="firstRowLabel">SSN: </span>
            <span id="lblStundentSSNAppTerm"></span>
        </div>

        <div class="inLineBlock width30PerMargin20">
            <span class="firstRowLabel">Enrollment:</span>
            <span id="lblEnrollmentNameAppTerm"></span>
        </div>
    </div>
</div>