﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApproveTerminationButtons.ascx.vb" Inherits="usercontrols_AR_StudentTermination_ApprovalTerminationTab_ApproveTerminationButtons" %>
<div class="buttonDiv">
    <hr/>
    <div class="inLineBlock top-margin20px">
        <button type="button" id="btnApproveTerminationBack">
            &lt; Back
        </button>
        <span class="disabled">
            <button type="button">
                Next &gt;
            </button>
        </span> 
    </div>   
    <div class="inLineBlock top-margin20px"> 
        <button type="button" id="btnApprovalTerminationCancel">
            Cancel
        </button>
    </div>

    <div class="inLineBlock top-margin20px">
        <button type="button" id="btnApproveTermination" style="color: rgba(255, 255, 255, 0.9) !important;background-color: #558b2f !important;">
            Approve Termination
        </button>
    </div>
</div>