﻿﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AdditionalInformation.ascx.vb" Inherits="usercontrols_AR_StudentTermination_ApprovalTerminationTab_AdditionalInfo" %>
<div class="k-state-active dvAdditionalInfo" id="dvAdditionalInfoApproval"> 
    <span>Additional information</span>
    <div>
        <div class="step1-margin" id="dvR2T4AdditionalInfo">
            <div class="approvalTermForm" id="dv50CreditHrsAdditionalInfo">
                <div class="inLineBlock">
                    <span id="spIndex_1"></span>. Step 2: Percentage of Title IV aid earned was marked as 50% because the school 
                    is not required to take attendance for this program. This was indicated by <span id="spUserName">USER_FNAME_LNAME</span> 
                    under the 'Percentage of period completed' section on the 'R2T4 Input' tab.
                </div>
            </div>
            <div class="approvalTermForm" id="dv100PerCreditHrsAdditionalInfo">
                <div class="inLineBlock">
                    <span id="spIndex_2"></span>. As the result of division of Completed days/Total days 
                    is greater than 60% the value of Box H under Subsection H of 'Step 2: Percentage of Title IV 
                    aid earned' section on 'R2T4 Results' tab was marked as 100%.
                </div>
            </div>
            <div class="approvalTermForm" id="dv100PerClockHrsAdditionalInfo">
                <div class="inLineBlock">
                    <span id="spIndex_3"></span>. As the result of division of Hours scheduled to complete/Total hours
                    in the period is greater than 60% the value of Box H under Subsection H of 'Step 2: Percentage of
                    Title IV aid earned' section on 'R2T4 Results' tab was marked as 100%.
                </div>
            </div>
            <div class="approvalTermForm" id="dvOverriddenR2T4AdditionalInfo">
                <div class="inLineBlock">
                    <span id="spIndex_4"></span>. Following fields were overridden by <span id="spUserNameOverridden">USER_FNAME_LNAME</span>. See ticket number <a id="aTicketNumber" target="_blank"></a> for further details."
                </div>
                <div class="inLineBlock padding5px width100 align-center">
                    <table class="table-bordered">
                        <thead>
                            <tr>
                                <td class="left-padding4px width25"><strong>Field</strong></td>
                                <td class="width12"></td>
                                <td class="align-center padding5px width30"><strong>Calculated by application</strong></td>
                                <td class="width12"></td>
                                <td class="align-center padding5px width25"><strong>Overridden values</strong></td>
                            </tr>
                        </thead>
                        <tbody id="tbodyOverriddenR2T4Info">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="approvalTermForm" id="dvLessThan50PerR2T4AdditionalInfo">
                <div class="inLineBlock">
                    <span id="spIndex_5"></span>. The student is not responsible for returning the following funds because the
                    amount to return is less than or equal to $50:
                </div>
                <div class="inLineBlock padding5px width100">
                    <table class="table-bordered">
                        <thead>
                            <tr>
                                <td class="align-left padding5px width30"><strong>Title IV grant Program</strong></td>
                                <td class="align-right padding5px width30"><strong>Amount to return</strong></td>
                            </tr>
                        </thead>
                        <tbody id="tbodyLoanToRefund">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="approvalTermForm" id="dvTuitionNotChargedByPaymentPeriodAdditionalInfo">
                <div class="inLineBlock">
                    <span id="spIndex_6"></span>. The Tuition is not charged by payment period and ‘Payment Period’ is the Period used for calculation.
                    As a result, the higher of Box L or Box E minus credit balance refunded to student was used for Box L under subsection N of 
                    ‘Step 5: Amount of unearned Title IV aid due from the school'. 
                    The following values were used in this comparison:
                </div>
                <div>
                    <div class="approvalTermForm marginBtm0">
                        <div class="inLineBlock terminationDetailLabel width200px">
                            Total Instituitional charges (Box L):
                        </div>
                        <div class="inLineBlock r2T4CalcResultsLabel width150px">
                            <span id="lblTotalInstituitionalChargesAppTerm"></span>
                        </div>
                    </div>

                    <div class="approvalTermForm marginBtm0">
                        <div class="inLineBlock terminationDetailLabel width200px">
                            Credit balance refunded to student:
                        </div>
                        <div class="inLineBlock r2T4CalcResultsLabel width150px">
                            <span id="lblCreditBalanceRefundedToStudentAppTerm"></span>
                        </div>
                    </div>

                    <div class="approvalTermForm marginBtm0">
                        <div class="inLineBlock terminationDetailLabel width200px">
                            Total Title IV aid disbursed for the period (Box E):
                        </div>
                        <div class="inLineBlock r2T4CalcResultsLabel width150px">
                            <span id="lblTotalTitleIVAidDisbursedAppTerm"></span>
                        </div>
                    </div>
                    
                </div>
            </div>
            

        </div>
    </div>
</div>