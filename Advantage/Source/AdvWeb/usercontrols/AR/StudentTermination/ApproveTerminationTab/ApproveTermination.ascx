﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApproveTermination.ascx.vb" Inherits="usercontrols_AR_StudentTermination_ApproveTerminationTab_ApproveTermination" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/ApproveTerminationTab/StudentInformation.ascx" TagPrefix="FAME" TagName="StudentInformation" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/ApproveTerminationTab/ApproveTerminationButtons.ascx" TagPrefix="FAME" TagName="ApproveTerminationButtons" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/ApproveTerminationTab/AdditionalInformation.ascx" TagPrefix="FAME" TagName="AdditionalInformation" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/ApproveTerminationTab/R2T4CalculationApproval.ascx" TagPrefix="FAME" TagName="R2T4CalculationApproval" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/ApproveTerminationTab/ApproveTerminationDetail.ascx" TagPrefix="FAME" TagName="ApproveTerminationDetail" %>

<div id="divApproveTermination" class="m-tab-pane m-detailbox-body">
    <FAME:StudentInformation runat="server" id="StudentInformationSection" />
    <div class="panelbar" id="panelApproveTermination">
        <FAME:ApproveTerminationDetail runat="server" ID="TerminationDetailApproval" /> 
        <FAME:R2T4CalculationApproval runat="server" ID="R2T4CalculationApproval" />
        <FAME:AdditionalInformation runat="server" ID="AdditionalInformation" />
    </div>
    <FAME:ApproveTerminationButtons runat="server" ID="ButtonSection" />
</div>