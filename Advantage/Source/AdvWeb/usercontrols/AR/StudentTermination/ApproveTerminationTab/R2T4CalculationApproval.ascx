﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="R2T4CalculationApproval.ascx.vb" Inherits="usercontrols_AR_StudentTermination_ApprovalTerminationTab_R2T4CalculationApproval" %>
<div class="k-state-active" id="dvR2T4CalculationApproval">
    <span>R2T4 Calculation Results</span>
    <div>
        <div class="step1-margin">
            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel width200px">
                    Total charges:
                </div>
                <div class="inLineBlock r2T4CalcResultsLabel width150px">
                    <span id="lblTotalChargesAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel width200px">
                    Total Title IV aid:
                </div>
                <div class="inLineBlock r2T4CalcResultsLabel width150px">
                    <span id="lblTotalTitleIVAidAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel width200px">
                    Total Title IV aid disbursed:
                </div>
                <div class="inLineBlock r2T4CalcResultsLabel width150px">
                    <span id="lblTotalTitleIVAidDisburseAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel width200px">
                    Percentage of Title IV aid earned:
                </div>
                <div class="inLineBlock r2T4CalcResultsLabel width150px">
                    <span id="lblPercentageEarnedAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0" id="dvPWDAmount">
                <div class="inLineBlock terminationDetailLabel width200px">
                    Post withdrawal disbursement amount:
                </div>
                <div class="inLineBlock r2T4CalcResultsLabel width150px">
                    <span id="lblPWDAmountAppTerm"></span>
                </div>
            </div> 
            
            <div id="dvAmountReturnedBySchool">
                <div class="approvalTermForm marginBtm0">
                    <div class="inLineBlock terminationDetailLabel width200px">
                        Total Title IV aid to return:
                    </div>
                    <div class="inLineBlock r2T4CalcResultsLabel width150px">
                        <span id="lblTotalAidReturnAppTerm"></span>
                    </div>
                </div>

                <div class="approvalTermForm marginBtm0">
                    <div class="inLineBlock terminationDetailLabel width200px">
                        Amount to be returned by school:
                    </div>
                </div>

                <div id="dvLoanAndGrantReturnBySchool" class="approvalTermForm marginBtm0"> 
                </div>

                <div class="approvalTermForm marginBtm0">
                    <div class="inLineBlock r2T4CalcResultsTotal width200px">
                        Total:
                    </div>
                    <div class="inLineBlock r2T4CalcResultsTotal width150px">
                        <span id="lblTotalReturnBySchoolAppTerm"></span>
                    </div>
                </div>
            </div>
            
            <div id="dvAmountReturnedByStudent">
                <div class="approvalTermForm marginBtm0">
                    <div class="inLineBlock terminationDetailLabel width200px">
                        Amount to be returned by student:
                    </div>
                </div> 
                <div id="dvLoanAndGrantReturnByStudent" class="approvalTermForm marginBtm0">
                     
                </div>
                <div class="approvalTermForm marginBtm0">
                    <div class="inLineBlock r2T4CalcResultsTotal width200px">
                        Total:
                    </div>
                    <div class="inLineBlock r2T4CalcResultsTotal width150px">
                        <span id="lblTotalReturnByStudentAppTerm"></span>
                    </div>
                </div>
            </div>

            <div class="previewButton right align-preview">
                <button type="button" id="btnR2T4ResultPreview">
                    Preview
                </button>
            </div><br /> 
        </div>
    </div>
</div>