﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApproveTerminationDetail.ascx.vb" Inherits="usercontrols_AR_StudentTermination_ApprovalTerminationTab_ApproveTerminationDetail" %>
<div class="k-state-active k-item k-first k-state-highlight" id="dvTerminationDetailApproval" style="margin-bottom: -16px;">
    <span>Termination Details</span>
    <div class="step1-margin-issue">
        <div class="step1-margin">
            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel">
                    Enrollment:
                </div>
                <div class="inLineBlock terminationDetailLabel">
                    <span id="lblEnrollmentAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel">
                    Status:
                </div>
                <div class="inLineBlock terminationDetailLabel">
                    <span id="lblStatusAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel">
                    Drop reason:
                </div>
                <div class="inLineBlock terminationDetailLabel">
                    <span id="lblDropreasonAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel">
                    Last date attended:
                </div>
                <div class="inLineBlock terminationDetailLabel">
                    <span id="lblLDAAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0" id="dvTerminationWithdrawalDate">
                <div class="inLineBlock terminationDetailLabel">
                    Withdrawal date:
                </div>
                <div class="inLineBlock terminationDetailLabel">
                    <span id="lblWithdrawalDateAppTerm"></span>
                </div>
            </div>

            <div class="approvalTermForm marginBtm0">
                <div class="inLineBlock terminationDetailLabel">
                    Date of determination:
                </div>
                <div class="inLineBlock terminationDetailLabel">
                    <span id="lblDODAppTerm"></span>
                </div>
            </div>

            <div class="previewButton right align-preview">
                <button type="button" id="btnterminationDetailsPreview">
                    Preview
                </button>
            </div>
        </div>
    </div>
</div>