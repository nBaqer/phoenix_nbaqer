﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step3.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step3" %>
<div class="k-state-active" id="dvAmountEarned">
    <span>3. Amount of Title IV Aid Earned by the Student</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">
        <div class="textR2T4Results padding5px">
          <div id="amount-title-mine-div">
                 <span>Multiply the percentage of Title IV aid earned  (Box H)  by the total of the Title IV aid disbursed and the Title IV aid that could have been disbursed for the period (Box G).</span>
                <div class="br-space"></div>
                  <div class="div1">
                      <div class="text-center">Box H</div>
                       <div class="input-group">
                           <input name="txtBoxH" type="text" id="txtBoxH" class="form-control k-textbox text-right " readonly />
                           <span class="input-group-addon input-height-sub2">%</span>
                         </div>
                  </div>
                  <div class="small small-topspace"><br />x</div>
                   <div class="div2">
                      <div class="text-center">Box G</div>
                       <div class="input-group">
                          <input name="txtBoxG" type="text" id="txtBoxG" class="form-control k-textbox money border-textbox text-right" readonly="">
                      </div>
                  </div>
                    <div class="small small-topspace"><br />=</div>
                   <div class="small small-topspace"><br />I.</div>       
                  <div class="div3"><br />
                      <div class="input-group input-br-space">
                          <span class="input-group-addon input-height-sub">$</span>
                          <input name="txtBoxI" type="text" id="txtBoxI" class="form-control k-textbox money text-right margin-4" readonly="">
                           </div>
                       </div>
               </div>
    </div>
            </div>
    </div>
</div>