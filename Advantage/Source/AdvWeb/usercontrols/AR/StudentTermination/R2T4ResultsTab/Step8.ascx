﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step8.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step8" %>
<div class="k-state-active" id="dvRepayment">
    <span>8. Repayment of the Student's loans</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">
        <div class="textR2T4Results padding5px">
            <div id="table-scroll-bar">
            <div id="amount-title-mine-div">

             <div class="step4-line-hight">From the Net loans disbursed to the student (Box B) subtract the Total loans the school must return (Box P) to find the amount of Title IV loans the student is still responsible for repaying (Box R).
                 <div class="br-space"></div>
                 These loans consist of loans the student has earned, or unearned loan funds the school is not responsible for repaying. They are repaid to the loan holders according to the terms of the borrower’s promissory note.
                
             </div>

                <div class="br-space"></div>
                  <div class="div1">
                      <div class="text-center">Box B</div>
                       <div class="input-group">
                           <input name="txt8boxB" id="txt8boxB" type="text" class="form-control k-textbox money text-right border-textbox" readonly="">
                                                                                                                                                             
                         </div>
                  </div>
      
                  <div class="small small-topspace"><br><i class="fa fa-minus"></i></div>
            
                  <div class="div2">
                      <div class="text-center">Box P</div>
                       <div class="input-group">
                           <input name="txt8P" id="txt8P" type="text" class="form-control k-textbox money text-right border-textbox" readonly="">
                                                                                                                 
                      </div>
                  </div>
      
                   <div class="small small-topspace"><br>=</div>

                   <div class="small small-topspace"><br>R.</div>
      
                        
                  <div class="div3"><br>
                      <div class="input-group input-br-space">
                           <span class="input-group-addon input-height-sub">$</span>
                          <input name="txt8BoxR" type="text" id="txt8BoxR" class="form-control k-textbox money text-right amount-textfield" readonly="">
                          </div>
                  </div>
              
            </div>
        <div id="amount-title-mine-div">
               <div class="step4-line-hight"><i class="fa fa-hand-o-right"></i>&nbsp; If Box Q is less than or equal to Box R, <strong>STOP</strong>.<br />
                   
                    &nbsp &nbsp The only action a school must take is to notify the holders of the loans of the student's withdrawal date.
                   <div class="br-space"></div>
                   <i class="fa fa-hand-o-right"></i>&nbsp; If Box Q is greater than Box R, proceed to Step 9
             </div>
            </div>
            </div>
        </div>
    </div>
</div>
</div>