﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step2.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step2" %>
<div class="k-state-active" id="dvPercentagebar">

    <span>2. Percentage of Title IV Aid Earned</span>

    
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="textR2T4Results border-lightblue">
         <div class="step1-margin">
        <div class="padding-1">
            <div id="table-scroll-bar">
                <div class="step4-line-hight">
                    <div class="inputGroup" id="divStep2StartDate">
                        <div class="panelLables text-center-two">
                            <label class="label-color">Start date</label>
                        </div>
                        <div class="divCalender u-hide" id="dvcalStartDateResult">
                            <input name="calStartDate" id="calStartDate" class=" mydate-picker k-datepicker dateCustom" data-validation="dateWithFormat" />
                        </div>
                        <div id="dvStartDateResult">
                            <input id="txtStartDate" type="text" name="txtStartDate" class="form-control k-textbox money" disabled="disabled" />
                        </div>
                    </div>
                    <div class="inputGroup" id="divStep2EndDate">
                        <div class="panelLables text-center-two">
                            <label class="label-color">Scheduled End Date</label>
                        </div>
                        <div class="divCalender u-hide" id="dvCalEndDateResult">
                            <input name="calEndDate" id="calEndDate" class="mydate-picker k-datepicker dateCustom " data-validation="dateWithFormat" />
                        </div>
                        <div id="dvEndDateResult">
                            <input name="txtEndDate" type="text" id="txtEndDate" class="form-control k-textbox money" disabled="disabled" />
                        </div>
                    </div>
                    <div class="inputGroup">
                        <div class="panelLables text-center-two">
                            <label class="label-color">Withdrawal date</label>
                        </div>
                        <div class="u-hide" id="divCalender">
                            <input name="cal2CreditWithdrawalDateEnabled" id="cal2CreditWithdrawalDateEnabled" class="mydate-picker k-datepicker dateCustom" data-validation="dateWithFormat" />
                        </div>
                        <div id="divDate">
                            <input name="txt2CreditWithdrawalDate" type="text" disabled="disabled" id="txt2CreditWithdrawalDate" class="form-control k-textbox money" data-validation="dateWithFormat" />
                        </div>
                    </div>
                </div>
                 <div class="step4-subtext-width">
                    <div id="divClockDesc1" style="display:none">A school that is not required to take attendance may, for a student who withdraws without notification, enter 50% in Box H and proceed to Step 3. Or, the school may enter the last date of attendance at an academically related activity for the “withdrawal date,” and proceed with the calculation as instructed. For a student who officially withdraws, enter the withdrawal date. </div>
                    <div class="br-space"></div>
                    <strong>H. Determine the percentage of period completed </strong>  <br />
                    <div id="divClockDesc">Divide the clock hours scheduled to have been completed as of the withdrawal date in the period by the total clock hours in the period.</div>
                    <div id="divCreditDesc">
                     Divide the calendar days completed in the period by the total calendar days in the period (excluding scheduled breaks of five days or more <strong>AND</strong> days that the student was on an approved leave of absence).
                    </div>
                    <div class="br-space"></div>
                    <div id="amount-title-mine-div">
                        <div class="div1">
                            <div class="text-center" id="resultTabCompletedDays">Hours scheduled to complete</div>
                            <input name="txtCompletedDays" type="text" readonly id="txtCompletedDays" class="form-control k-textbox money text-right" />
                        </div>
                         <div class="small small-topspace">
                            <br />
                            <img src="../images/divider.png"></div>
                           <div class="div2">
                            <div class="text-center" id="resultTabTotalDays">Total hours in period</div>
                            <div class="input-group">
                            <input name="txtTotalDays" type="text" readonly="" id="txtTotalDays" class="form-control k-textbox  money border-textbox text-right">
                            </div>
                        </div>
                         <div class="small small-topspace"> <br> =</div>
                          <div class="div3"> <br> <div class="input-group input-br-space">
                               <input name="txtCreditActual" type="text" id="txtCreditActual" readonly class="form-control k-textbox border-textbox money text-right" readonly="">
                            </div>
                        </div>
                    </div>
                    <div id="amount-title-mine-div">
                        <div class="step4-line-hight">
                            <i class="fa fa-hand-o-right"></i>&nbsp If this percentage is greater than 60%, enter 100% in Box H and proceed to Step 3.
                  <div class="br-space"></div>
                            <i class="fa fa-hand-o-right"></i>&nbsp If this percentage is less than or equal to 60%, enter that percentage in Box H, and proceed to Step 3.
                        </div>
                        <div class="small small-topspace">
                            <br> H.</div>

                       <div class="div3">
                            <br>
                            <div class="input-group input-br-space">
                                <input name="txtCreditAttendencePerc" type="text" id="txtCreditAttendencePerc" class="form-control k-textbox text-right" readonly/>
                                <span class="input-group-addon input-height-sub2">%</span>
                            </div>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>
</div>
