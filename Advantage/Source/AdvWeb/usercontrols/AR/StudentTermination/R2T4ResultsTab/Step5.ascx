﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step5.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step5" %>
<div class="k-state-active" id="dvAidDue">
    <span>5. Amount of Unearned Title IV Aid Due from the School</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">
        <div id="table-scroll-bar">
        <div class="textR2T4Results padding5px">
           <table class="step5-table-width">
                <tbody>
                    <tr>
                        <th>L.</th>
                        <td colspan="2" class="width-350"><strong> Institutional charges for the period</strong> </td>
                        <td>Tuition</td>
                        <td colspan="3">
                            <input name="txt5Tuition" type="text" readonly id="txt5Tuition" class="form-control k-textbox money text-right text-right" /></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td colspan="2"></td>
                        <td>Room</td>
                        <td colspan="3">
                            <input name="txt5Room" type="text" readonly id="txt5Room" class="form-control k-textbox money text-right text-right" /></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td colspan="2"></td>
                        <td>Board</td>
                        <td colspan="3">
                            <input name="txt5Board" type="text" readonly id="txt5Board" class="form-control k-textbox money text-right text-right" /></td>
                    </tr>
                    <tr>
                      <th></th>
                      <td colspan="2"></td>
                      <td>Other</td>
                      <td colspan="3"><input name="txt5Other" type="text" readonly="readonly" id="txt5Other" class="form-control k-textbox money text-right text-right" /></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td colspan="2" ><strong>Total Institutional Charges</strong><br>
                        (Add all the charges together)</td>
                        <td class="text-right"><strong>L.</strong>&nbsp </td>
                        <td colspan="3"> <div class="input-group step-5-margin-left">
                        <span class="input-group-addon input-height-sub">$</span>
                        <input name="txt5L" type="text" id="txt5L" class="form-control k-textbox money text-right amount-textfield" readonly=""/>
                        </div></td>
                    </tr>
                </tbody>
            </table>
        </div>
             <div id="amount-title-mine-div">
             <div class="step4-line-hight">
                     <strong>M.  Percentage of unearned Title IV aid</strong>
                    <div class="br-space"></div>
                 </div>
             <div class="small small-topspace"></div>
             <div class="small small-topspace"><br /> 100%</div>
            <div class="small small-topspace" style="margin-left:10px;"><br><i class="fa fa-minus"></i></div>
                    <div class="div1">
                      <div class="text-center">Box H</div>
                       <div class="input-group">
                           <input name="txt5BoxH" type="text" id="txt5BoxH" class="form-control k-textbox text-right" readonly="">
                           <span class="input-group-addon input-height-sub2">%</span> </div>
                                                   
                         </div>
                 <div class="small small-topspace"><br>=</div>
                   <div class="small small-topspace"><br>M.</div>
                  <div class="div3"><br>
                      <div class="input-group input-br-space">
                           <input name="txt5BoxMResult" type="text" id="txt5BoxMResult" class="form-control k-textbox text-right amount-textfield " readonly="">
                           <span class="input-group-addon input-height-sub2">%</span> </div>
                  </div><br />
                  </div>
                   <div id="amount-title-mine-div">
             <div class="step4-line-hight">
                     <strong>N.  Amount of unearned charges</strong><br>
                     &nbsp &nbsp Multiply institutional charges for the period (Box L) times the percentage of unearned Title IV aid (Box M)
                   </div>
                 <div class="br-space"></div>
                 <div class="step4-line-hight">
                  <div class="div1">
                      <div class="text-center">Box L</div>
                       <div class="input-group">
                           <input name="txt5BoxL" type="text" id="txt5BoxL" class="form-control k-textbox money text-right border-textbox" readonly="" />
                            </div>    </div>
                        <div class="small small-topspace"><br><i class="fa fa-times"></i></div>
                       <div class="div2">
                      <div class="text-center">Box M</div>
                       <div class="input-group">
                           <input name="txt5BoxM" type="text" id="txt5BoxM" class="form-control k-textbox text-right" readonly=""/>
                           <span class="input-group-addon input-height-sub2">%</span>
                        </div>
                  </div>
                  <div class="small small-topspace"><br>=</div>
                   <div class="small small-topspace"><br>N.</div>
                 <div class="div3"><br>
                      <div class="input-group input-br-space">
                           <span class="input-group-addon input-height-sub">$</span>
                          <input name="txt5BoxNResult" type="text" id="txt5BoxNResult" class="form-control k-textbox money text-right amount-textfield" readonly="">
                        </div>
                  </div> </div>
            </div>          
            <div id="amount-title-mine-div">
             <div class="step4-line-hight">
                     <strong>O. Amount for school to return</strong><br>
                     &nbsp &nbsp Compare the amount of Title IV aid to be returned (Box K) to amount of unearned charges (Box N), and enter the lesser amount.
                   </div>
                  <div class="small small-topspace"><br>O.</div>
                  <div class="div3"><br>
                      <div class="input-group input-br-space">
                           <span class="input-group-addon input-height-sub">$</span>
                          <input name="txt5BoxOresult" type="text" id="txt5BoxOresult" class="form-control k-textbox money text-right amount-textfield "readonly="">
                          </div>
                      </div>
                 </div>
             </div>
            </div>
        </div>
    </div>