﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="R2T4ResultsControls.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_R2T4ResultsControls" %>
<div class="buttonDiv">
    <hr class="top-margin20px">
    <div class="inLineBlock top-margin7px">
        <span>
            <button type="button" id="btnR2T4ResultBack">
                &lt; Back
            </button>
        </span>
        <button type="button" id="btnR2T4ResultNext">
            Next &gt;
        </button>
    </div>

    <div class="inlineBtn">
        <button type="button" id="btnR2T4ResultSave" style="color: rgba(255, 255, 255, 0.9) !important;background-color: #1976d2 !important;">
            Save
        </button>
        <button type="button" id="btnCancelTerminationR2T4Result">
            Cancel
        </button>
    </div>
    <div class="disabled inLineBlock top-margin7px">
        <button type="button" style="color: rgba(255, 255, 255, 0.9) !important;background-color: #558b2f !important;">
            Approve Termination
        </button>
    </div>
</div>