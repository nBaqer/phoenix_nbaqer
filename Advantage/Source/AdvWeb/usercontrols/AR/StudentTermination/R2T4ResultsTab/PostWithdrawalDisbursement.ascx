﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PostWithdrawalDisbursement.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_PostWithdrawalDisbursement" %>
<div class="k-state-active" id="dvPostWithDrawl">
    <span>11. POST-WITHDRAWAL DISBURSEMENT TRACKING SHEET</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">
        <div class="textR2T4Results padding5px">
             <div id="table-scroll-bar">
            <div class="step11-mine-div">
                <div class="pwd-header" >
            <div class="inLineBlockPwd">
                <span class="firstRowLabel">Student:</span>
                <span id="lblStudentNameR2T4ResultPWD"></span>
            </div>

            <div class="inLineBlockPwd">
                <span class="firstRowLabel">SSN: </span>
                <span id="lblSSNR2T4ResultPWD"></span>
            </div>

            <div class="inLineBlockPwd">
                <span class="firstRowLabel">Date of determination:</span>
                <span id="dateDeterminationPWD"></span>
            </div>
        </div>
                <div class="br-space"></div> <div class="br-space"></div>
               
                <div class="step11-left-div"><div class="br-space"></div>
                    <strong>I. Amount of Post-withdrawal Disbursement (PWD)</strong><br />

                    Amount from "Box J" of the Treatment of Title IV Funds When a Student Withdraws worksheet

                     <div class="br-space"></div><div class="br-space"></div>

                     <strong>II. Outstanding Charges for Educationally Related Expenses Remaining on Student's Account</strong>

                   Total Outstanding charges Scheduled to be Paid from PWD<br /> <i>(Note: Prior-year charges cannot exceed $200.)</i>

                     
                </div>

                 <div class="step11-right-div">
                                        
                      <div class="text-center"><strong>Box 1</strong></div>
                       <div class="input-group">
                           <span class="input-group-addon input-height-sub">$</span>
                           <input type="text" id="txtPWD" name="txtPWD" class="form-control k-textbox money text-right" readonly>
                                                                   
                      </div>
                 <div class="br-space"></div>

                      <div class="text-center"><strong>Box 2</strong></div>
                       <div class="input-group">
                           <span class="input-group-addon input-height-sub">$</span>
                           <input type="text" id="txtPWDBox2" name="txtPWDBox2" class="form-control k-textbox money text-right" readonly>
                           
                                                                   
                      </div>

                 </div>

                
                <div class="step11-full-width"><div class="br-space"></div>
                     <strong>III. Post-withdrawal Disbursement Offered Directly to Student and/or Parent</strong><br /><div class="br-space"></div>

                   From the total Post-withdrawal Disbursement due (Box 1), subtract the Post-withdrawal Disbursement to be credited to the student's account (Box 2). This is the amount you must make to the student (grant) or offer to the student or parent (Loan) as a Direct Disbursement.
                    <div class="br-space"></div>
                </div>
                <div class="br-space"></div>
                <div class="pwd-disbursement-mine">
                   
                <div class="step11-left-div">
                     <div class="blankdiv">&nbsp</div>
                  <div class="step11-width-fix">
                    <div class="text-center"><strong>Box 1</strong></div>
                       <div class="input-group step11-textbox-width">
                           <span class="input-group-addon input-height-sub">$</span>
                           <input type="text" id="txtPWDOffered" class="form-control k-textbox money text-right" readonly="">
                                                              
                      </div>
                         
                        </div>
                    <div class="step-small small-topspace"><br><i class="fa fa-minus"></i></div>

                      <div class="step11-width-fix">
                        <div class="text-center"><strong>Box 2</strong></div>
                       <div class="input-group step11-textbox-width">
                           <span class="input-group-addon input-height-sub">$</span>
                           <input type="text" id="txtPWDBox2Offered" name="txtPWDBox2Offered" class="form-control k-textbox money text-right" readonly="">
                                                               
                      </div></div>
                   
                     <div class="step-small-multiple small-topspace"><br>=</div>
                   

                

                </div>

                 <div class="step11-right-div"> 
                     <div class="text-center"><strong>Box 3</strong></div>
                       <div class="input-group">
                           <span class="input-group-addon input-height-sub">$</span>
                           <input type="text" id="txtPWDBox3" name="txtPWDBox3" class="form-control k-textbox  money text-right" readonly="">
                                                     
                                                                   
                      </div>

                 </div>

                </div></div>

  <!-- <table class="table-striped" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
            <div class="pwd-header" >
            <div class="inLineBlockPwd">
                <span class="firstRowLabel">Student:</span>
                <span id="lblStudentNameR2T4ResultPWD"></span>
            </div>

            <div class="inLineBlockPwd">
                <span class="firstRowLabel">SSN: </span>
                <span id="lblSSNR2T4ResultPWD"></span>
            </div>

            <div class="inLineBlockPwd">
                <span class="firstRowLabel">Date of determination:</span>
                <span id="dateDeterminationPWD"></span>
            </div>
        </div>
                </tr>
                    <tr>
                        <th width="80%">I. Amount of Post-withdrawal Disbursement (PWD)</th>
                        <th width="20%" colspan="2">IF SCHOOL DEEMS ELIGIBLE</th>
                    </tr>
                    <tr>
                        <td>
                            <div class="padding-1">Amount from "Box J" of the Treatment of Title IV Funds When a Student Withdraws worksheet</div>
                        </td>
                        <td width="5%" nowrap="nowrap"><strong>Box 1</strong></td>
                        <td width="15%"><span class="inputUnit abs-position"><strong>$</strong></span>
                            <input type="text" id="txtPWD" name="txtPWD" class="k-textbox money text-right" readonly></td>
                    </tr>
                    <tr>
                        <th colspan="3">II. Outstanding Charges for Educationally Related Expenses Remaining on Student's Account</th>
                    </tr>
                    <tr>
                        <td width="80%">
                            <div class="padding-1">Total Outstanding charges Scheduled to be Paid from PWD (Note: Prior-year charges cannot exceed $200.)</div>
                        </td>
                        <td width="5%" nowrap="nowrap"><strong>Box 2</strong></td>
                        <td width="15%"><span class="inputUnit abs-position"><strong>$</strong></span><input type="text" id="txtPWDBox2" name="txtPWDBox2" class="k-textbox money text-left" readonly></td>
                    </tr>
                    <tr>
                        <th colspan="3">III. Post-withdrawal Disbursement Offered Directly to Student and/or Parent</th>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="padding-1">From the total Post-withdrawal Disbursement due (Box 1), subtract the Post-withdrawal Disbursement to be credited to the student's account (Box 2). This is the amount you must make to the student (grant) or offer to the student or parent (Loan) as a Direct Disbursement.</div>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="clear-both"></div>
            <div class="withdrawal">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="80"><span class="inputUnit abs-position"><strong>$</strong></span><input type="text" id="txtPWDOffered" class="k-textbox money text-right" readonly></td>
                            <td width="100" align="center"><i class="fa fa-minus"></i></td>
                            <td width="80"><span class="inputUnit abs-position"><strong>$</strong></span><input type="text" id="txtPWDBox2Offered" name="txtPWDBox2Offered"  class="k-textbox money text-right" readonly></td>
                            <td width="80" align="center"><strong>=</strong></td>
                            <td width="150" align="right" class="col-nowrap">
                                <strong class="padding-right-1">Box 3</strong>
                                <span class="inputUnit abs-position"><strong>$</strong></span>
                                <input type="text" id="txtPWDBox3" name="txtPWDBox3" class="k-textbox  money text-right" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td width="80" align="center">
                                <label>Box 1</label></td>
                            <td width="100" align="center"></td>
                            <td width="80" align="center">
                                <label>Box 2</label></td>
                            <td width="80"></td>
                            <td width="150"></td>
                        </tr>
                    </tbody>
                </table>
            </div>-->
            <div class="clear-both"></div>
          
                <table class="table-withdrawal-disbursement" >
                    <thead>
                        <tr>
                            <th colspan="7">IV. Allocation of Post-withdrawal Disbursement</th>
                        </tr>
                        <tr>
                            <th>Type of Aid</th>
                            <th>Loan Amount School Seeks to Credit to Account</th>
                            <th>Loan Amount Authorized to Credit to Account</th>
                            <th>Title IV Aid Credited to Account</th>
                            <th>Loan Amount Offered as Direct Disbursement</th>
                            <th>Loan Amount Accepted as Direct Disbursement</th>
                            <th>Title IV Disbursed Directly to Student</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="steo-11-unbold">Pell Grant</th>
                            <td class="text-center">N/A</td>
                            <td class="text-center">N/A</td>
                            <td><input name="pwdPell3" type="text" id="pwdPell3" class="k-textbox money post-withdrawal-disbursement text-right" readonly=""></td>
                            <td class="text-center">N/A</td>
                            <td class="text-center">N/A</td>
                            <td><input name="pwdPell6" type="text" id="pwdPell6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th class="steo-11-unbold">FSEOG</th>
                            <td class="text-center">N/A</td>
                            <td class="text-center">N/A</td>
                            <td><input name="pwdFSEOG3" type="text" id="pwdFSEOG3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td class="text-center">N/A</td>
                            <td class="text-center">N/A</td>
                            <td><input name="pwdFSEOG6" type="text" id="pwdFSEOG6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th class="steo-11-unbold">TEACH Grant</th>
                            <td class="text-center">N/A</td>
                            <td class="text-center">N/A</td>
                            <td><input name="pwdTeach3" type="text" id="pwdTeach3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td class="text-center">N/A</td>
                            <td class="text-center">N/A</td>
                            <td><input name="pwdTeach6" type="text" id="pwdTeach6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th class="steo-11-unbold">Iraq and Afganistan Service Grant</th>
                            <td class="text-center">N/A</td>
                            <td class="text-center">N/A</td>
                            <td><input name="pwdIASG3" type="text" id="pwdIASG3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td class="text-center">N/A</td>
                            <td class="text-center">N/A</td>
                            <td><input name="pwdIASG6" type="text" id="pwdIASG6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th class="steo-11-unbold">Perkins loan</th>
                            <td><input name="pwdPerkins1" type="text" id="pwdPerkins1" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdPerkins2" type="text" id="pwdPerkins2" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdPerkins3" type="text" id="pwdPerkins3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdPerkins4" type="text" id="pwdPerkins4" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdPerkins5" type="text" id="pwdPerkins5" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdPerkins6" type="text" id="pwdPerkins6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th class="steo-11-unbold">Subsidized Direct loan</th>
                            <td><input name="pwdSub1" type="text" id="pwdSub1" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdSub2" type="text" id="pwdSub2" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdSub3" type="text" id="pwdSub3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdSub4" type="text" id="pwdSub4" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdSub5" type="text" id="pwdSub5" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdSub6" type="text" id="pwdSub6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th class="steo-11-unbold">Unsubsidized Direct loan</th>
                            <td><input name="pwdUnSub1" type="text" id="pwdUnSub1" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdUnSub2" type="text" id="pwdUnSub2" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdUnSub3" type="text" id="pwdUnSub3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdUnSub4" type="text" id="pwdUnSub4" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdUnSub5" type="text" id="pwdUnSub5" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdUnSub6" type="text" id="pwdUnSub6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th class="steo-11-unbold">Direct Graduate Plus loan</th>
                            <td><input name="pwdGrad1" type="text" id="pwdGrad1" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdGrad2" type="text" id="pwdGrad2" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdGrad3" type="text" id="pwdGrad3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdGrad4" type="text" id="pwdGrad4" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdGrad5" type="text" id="pwdGrad5" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdGrad6" type="text" id="pwdGrad6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th class="steo-11-unbold"> Direct Parent Plus loan</th>
                            <td><input name="pwdParent1" type="text" id="pwdParent1" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdParent2" type="text" id="pwdParent2" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdParent3" type="text" id="pwdParent3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdParent4" type="text" id="pwdParent4" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdParent5" type="text" id="pwdParent5" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdParent6" type="text" id="pwdParent6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td align="center"><strong>Totals</strong></td>
                            <td><input name="pwdTotal1" type="text" id="pwdTotal1" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdTotal2" type="text" id="pwdTotal2" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdTotal3" type="text" id="pwdTotal3" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdTotal4" type="text" id="pwdTotal4" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdTotal5" type="text" id="pwdTotal5" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                            <td><input name="pwdTotal6" type="text" id="pwdTotal6" class="k-textbox money post-withdrawal-disbursement text-right" readonly="readonly" /></td>
                        </tr>
                    </tbody>
                </table>
            

            <div class="clear-both"></div>
            <div>
                <table class="table-striped table-bottom-bordered">
                    <tbody>
                        <tr>
                            <th colspan="3">V. Authorizations and Notifications</th>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="padding-1">Post-withdrawal disbursement loan notification sent to student and/or parent on</div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="inLineBlock">
                                        <input id="dtPostWithdrwal" name="dtPostWithdrwal" placeholder="MM/DD/YYYY" class="mydate-picker k-datepicker dateCustom" />

                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="padding-1">Deadline for student and/or parent to respond</div>
                            </td>
                            <td>
                                <div class="inLineBlock">
                                    <input id="dtDeadline" name="dtDeadline" placeholder="MM/DD/YYYY" class="mydate-picker k-datepicker dateCustom" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="padding-1">
                                    <input type="checkbox" id="chkResponseReceived">
                                    Response received from Student and/or parent on
                                </div>
                            </td>
                            <td>
                                <div class="inLineBlock">
                                    <input id="dtResponseReceived" name="dtResponseReceived" placeholder="MM/DD/YYYY" class="mydate-picker k-datepicker dateCustom" />
                                </div>
                            </td>
                            <td>
                                <input type="checkbox" id="chkResponseNotReceived">
                                Response not received</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="padding-1">
                                    <input type="checkbox" id="chkNotAccept">
                                    School does not accept late response
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3">VI. Date Funds Sent</th>
                        </tr>
                        <tr>
                            <td>
                                <div class="padding-1">Date Direct Disbursement mailed or transferred</div>
                            </td>
                            <td>
                                <label>Grant</label>
                                <div class="inLineBlock">
                                    <input id="dtGrantTransferred" name="dtGrantTransferred" placeholder="MM/DD/YYYY" class="mydate-picker k-datepicker dateCustom" />
                                </div>
                            </td>
                            <td>
                                <label>Loan</label>
                                <div class="inLineBlock">
                                    <input id="dtLoanTransferred" name="dtLoanTransferred" placeholder="MM/DD/YYYY" class="mydate-picker k-datepicker dateCustom" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>