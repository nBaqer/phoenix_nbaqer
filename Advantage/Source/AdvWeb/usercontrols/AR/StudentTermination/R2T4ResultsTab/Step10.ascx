﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step10.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step10" %>
<div class="k-state-active" id="dvReturnGrant">
    <span>10. Return of Grant Funds by the Student</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">

        <div class="textR2T4Results padding5px">
   <div class="step4-line-hight">
                Except as noted below, the student must return the unearned grant funds for which he or she is responsible (Box U). The grant funds returned by the student are applied to the following sources in the order indicated, up to the total amount disbursed from that grant program minus any grant funds the school is responsible for returning to that program in Step 6.
                 <div class="br-space"></div>
                <strong>Note that the student is not responsible for returning funds to any program to which the student owes $50.00 or less.</strong>
     <div class="br-space"></div>
                
       <table  class="step5-table-width">
         <tbody>
             <tr>
                 <th colspan="2"  class="normal-font text-left"><strong>Title IV Grant Programs</strong></th>
                 <th colspan="2" class="normal-font text-center"><strong>Amount to return</strong></th>
             </tr>
             <tr>
                 <td class="width-20">1.</td>
                 <td class="width-350">Pell Grant</td>
                 <td>
                     <input name="txt10Pell" type="text" id="txt10Pell" class="form-control k-textbox money text-right" readonly /></td>
             </tr>
             <!-- <tr>
                            <td width="3%">2.</td>
                            <td>Academic Competitiveness Grant</td>
                            <td>
                                <input name="txt10ACG" type="text" id="txt10ACG" class="k-textbox money text-right" readonly /></td>
                        </tr>
                        <tr>
                            <td width="3%">3.</td>
                            <td>National SMART Grant</td>
                            <td>
                                <input name="txt10NSG" type="text" id="txt10NSG" class="k-textbox money text-right" readonly /></td>
                        </tr>-->
             <tr>
                 <td>2.</td>
                 <td>FSEOG</td>
                 <td>
                     <input name="txt10FSEOG" type="text" id="txt10FSEOG" class="form-control k-textbox money text-right" readonly /></td>
             </tr>
             <tr>
                 <td>3.</td>
                 <td>TEACH Grant</td>
                 <td>
                     <input name="txt10TG" type="text" id="txt10TG" class="form-control k-textbox money text-right" readonly /></td>
             </tr>
             <tr>
                 <td>4.</td>
                 <td>Iraq Afghanistan Service Grant</td>
                 <td>
                     <input name="txt10IFSG" type="text" id="txt10IFSG" class="form-control k-textbox money text-right" readonly /></td>
             </tr>
         </tbody>
     </table>

       </div>
            
        </div>
    </div>
</div>
</div>