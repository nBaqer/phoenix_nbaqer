﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="OverrideR2T4Results.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_OverrideR2T4Results" %>
<div class="formRow top-margin15px" id="dvOverride">
    <div class="inLineBlock">
        <label class="ove-checkbox">
            <input type="checkbox" id="ChkOverride" class="width20px">
            Override
        </label>
    </div>
    <div class="inLineBlock ove-ticket">
        Ticket# <span class="red-color">*</span>
        <input name="txtTicket" type="text" id="txtTicket" class="k-textbox text-right" readonly />
    </div>
</div>