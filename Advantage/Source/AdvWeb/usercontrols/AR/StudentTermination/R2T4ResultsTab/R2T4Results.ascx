﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="R2T4Results.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_R2T4Results" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step1.ascx" TagPrefix="FAME" TagName="Step1" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step2.ascx" TagPrefix="FAME" TagName="Step2" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step3.ascx" TagPrefix="FAME" TagName="Step3" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step4.ascx" TagPrefix="FAME" TagName="Step4" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step5.ascx" TagPrefix="FAME" TagName="Step5" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step6.ascx" TagPrefix="FAME" TagName="Step6" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step7.ascx" TagPrefix="FAME" TagName="Step7" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step8.ascx" TagPrefix="FAME" TagName="Step8" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step9.ascx" TagPrefix="FAME" TagName="Step9" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/Step10.ascx" TagPrefix="FAME" TagName="Step10" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/R2T4ResultsControls.ascx" TagPrefix="FAME" TagName="R2T4ResultsControls" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/PostWithdrawalDisbursement.ascx" TagPrefix="FAME" TagName="PostWithdrawalDisbursement" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/StudentInformation.ascx" TagPrefix="FAME" TagName="StudentInformation" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/PeriodUsedForCalculation.ascx" TagPrefix="FAME" TagName="PeriodUsedForCalculation" %>
<%@ Register Src="~/usercontrols/AR/StudentTermination/R2T4ResultsTab/OverrideR2T4Results.ascx" TagPrefix="FAME" TagName="OverrideR2T4Results" %>

<div id="divR2T4Result" class="m-tab-pane m-detailbox-body">
    <FAME:StudentInformation runat="server" ID="StudentInformation" />

    <FAME:PeriodUsedForCalculation runat="server" ID="PeriodUsedForCalculation" />

    <div id="Studentbar" class="panelbarR2T4Result">
        <!-- panels starts Section 1-->

        <FAME:Step1 runat="server" ID="Step1" />

        <!-- panels ends -->

        <!-- panels starts Section 2-->

        <FAME:Step2 runat="server" ID="Step2" />

        <!-- panels ends -->
        <!-- panels starts Section 3-->

        <FAME:Step3 runat="server" ID="Step3" />

        <!-- panels ends -->
        <!-- panels starts Section 4-->

        <FAME:Step4 runat="server" ID="Step4" />

        <!-- panels ends -->
        <!-- panels starts Section 5-->

        <FAME:Step5 runat="server" ID="Step5" />

        <!-- panels ends -->
        <!-- panels starts Section 6-->

        <FAME:Step6 runat="server" ID="Step6" />

        <!-- panels ends -->
        <!-- panels starts Section 7-->

        <FAME:Step7 runat="server" ID="Step7" />

        <!-- panels ends -->
        <!-- panels starts Section 8-->

        <FAME:Step8 runat="server" ID="Step8" />

        <!-- panels ends -->
        <!-- panels starts Section 9-->

        <FAME:Step9 runat="server" ID="Step9" />

        <!-- panels ends -->
        <!-- panels starts Section 10-->

        <FAME:Step10 runat="server" ID="Step10" />

        <!-- panels ends -->
        <!-- panels starts Section PWD-->

        <FAME:PostWithdrawalDisbursement runat="server" ID="PostWithdrawalDisbursement" />
        
        <!-- panels ends -->
    </div>
    <FAME:OverrideR2T4Results runat="server" ID="OverrideR2T4Results" />
    

    <FAME:R2T4ResultsControls runat="server" ID="R2T4ResultsControls" />
</div>