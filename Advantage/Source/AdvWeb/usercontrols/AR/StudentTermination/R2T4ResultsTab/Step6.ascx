﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step6.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step6" %>
<div class="k-state-active" id="dvReturnFund">
    <span>6. Return of Funds by the School</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%></span>
    <div class="border-lightblue">
        <div class="step1-margin">
        <div class="textR2T4Results padding5px">
            <div class="step4-line-hight">The school must return the unearned aid for which the school is responsible (Box O) by repaying funds to the following sources, in order, up to the total net amount disbursed from each source.</div>
            <div class="step4-line-hight">
           <table class="step5-table-width">
   <tr>
     <td colspan="2" class="text-left"><strong>Title IV Programs</strong></td>
     <td class="text-center"><strong>Amount for School to Return</strong></td>
   </tr>
   <tr>
     <td colspan="2">1. &nbsp Unsubsidized Direct Loan</td>
     <td><input name="txtUnSubLoan6" type="text" id="txtUnSubLoan6" class="form-control k-textbox money text-right" readonly=""></td>
   </tr>
   <tr>
     <td colspan="2" class="width-350">2. &nbsp Subsidized Direct Loan</td> 
     <td><input name="txtSubLoan6" type="text" id="txtSubLoan6" class="form-control k-textbox money text-right" readonly="">  </td>
   </tr>
   <tr>
     <td colspan="2">3. &nbsp Perkins Loan</td>
     <td><input name="txtPerkinsLoan6" type="text" id="txtPerkinsLoan6" class="form-control k-textbox money text-right" readonly=""></td>
   </tr>
   <tr>
     <td colspan="2">4. &nbsp Direct Grad PLUS Loan</td>
     <td> <input name="txtFFELStudent6" type="text" id="txtFFELStudent6" class="form-control k-textbox money text-right" readonly=""></td>
   </tr>
   <tr>
     <td colspan="2">5. &nbsp Direct Parent PLUS Loan</td>
     <td><input name="txtFFELParent6" type="text" id="txtFFELParent6" class="form-control k-textbox money text-right" readonly=""></td>
   </tr>
   <tr>
     <td><strong>Total loans the school must return</strong></td>
     <td class="text-right"><b>= &nbsp P.</b>&nbsp </td>
     <td><div class="input-group">
     <span class="input-group-addon input-height-sub">$</span>
      <input name="txtStep6P" type="text" id="txtStep6P" class="form-control k-textbox money text-right amount-textfield" readonly="">      
      </div></td>
   </tr>
   <tr>
     <td colspan="2">6. &nbsp Pell Grant</td>
     <td><input name="txtPellGrant6" type="text" id="txtPellGrant6" class="form-control k-textbox money text-right" readonly=""></td>
   </tr>
   <tr>
     <td colspan="2">7. &nbsp FSEOG</td>
     <td><input name="txtFSEOGGrant6" type="text" id="txtFSEOGGrant6" class="form-control k-textbox money text-right" readonly=""></td>
   </tr>
   <tr>
     <td colspan="2">8. &nbsp TEACH Grant</td>
     <td><input name="txtTeachGrant6" type="text" id="txtTeachGrant6" class="form-control k-textbox money text-right" readonly=""></td>
   </tr>
   <tr>
     <td colspan="2">9. &nbsp Iraq Afghanistan Service Grant</td>
     <td><input name="txtIASGrant6" type="text" id="txtIASGrant6" class="form-control k-textbox money text-right" readonly=""></td>
   </tr>
 </table>
  </div></div>
    </div>
</div>
</div>