﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step9.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step9" %>
<div class="k-state-active" id="dvGrantFund">
    <span>9. Grant Funds to be Returned</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">
        <div class="textR2T4Results padding5px">
            <div id="table-scroll-bar">
            <div id="amount-title-mine-div">

             <div class="step4-line-hight">
                     <strong>S.  Initial amount of Title IV grants for student to return</strong><br>
                     &nbsp; &nbsp; Subtract the amount of loans to be repaid by the student (Box R) from the initial amount of unearned Title IV aid due from the student (Box Q).<br>
  
                 </div>
                <div class="br-space"></div>

                 
                  <div class="div1">
                      <div class="text-center">Box Q</div>
                       <div class="input-group">
                           <input name="txt9BoxQ" type="text" id="txt9BoxQ" class="form-control k-textbox money text-right border-textbox" readonly="">
                                                                                                                                   
                         </div>
                  </div>
      
                  <div class="small small-topspace"><br><i class="fa fa-minus"></i></div>
            
                  <div class="div2">
                      <div class="text-center">Box R</div>
                       <div class="input-group">
                            <input name="txt9BoxR" type="text" id="txt9BoxR" class="form-control k-textbox money text-right border-textbox" readonly />
                                                                                          
                      </div>
                  </div>
      
                   <div class="small small-topspace"><br>=</div>

                   <div class="small small-topspace"><br>S.</div>
      
                        
                  <div class="div3"><br>
                      <div class="input-group input-br-space">
                           <span class="input-group-addon input-height-sub">$</span>
                          <input name="txt9BoxSResult" type="text" id="txt9BoxSResult" class="form-control k-textbox money text-right amount-textfield " readonly="">
                          
                        </div>
                  </div>

       
            </div>
               

            <div id="amount-title-mine-div">

             <div class="step4-line-hight">
                     <strong>T.  Amount of Title IV grant protection</strong><br>
                     &nbsp; &nbsp; Multiply the total of Title IV grant aid that was disbursed and could have been disbursed for the payment period or period of enrollment (Box F) by 50%<br>
  
                 </div>
                <div class="br-space"></div>
                 
                  <div class="div1">
                      <div class="text-center">Box F</div>
                       <div class="input-group">
                           <input name="txt9BoxF" type="text" id="txt9BoxF" class="form-control k-textbox money text-right border-textbox" readonly="">
                                                                                                                                                              
                         </div>
                  </div>
      
                  <div class="small small-topspace"><br><i class="fa fa-times"></i></div>
            
                  <div class="div2 step-9-tabs">
                     <h4>50%</h4>
                      
                  </div>
      
                   <div class="small small-topspace"><br>=</div>

                   <div class="small small-topspace"><br>T.</div>
      
                        
                  <div class="div3"><br>
                      <div class="input-group input-br-space">
                           <span class="input-group-addon input-height-sub">$</span>
                          <input name="txt9BoxTResult" type="text" id="txt9BoxTResult" class="form-control k-textbox money text-right amount-textfield" readonly="">
                          
                          
                        </div>
                  </div>

       
            </div>

            <div id="amount-title-mine-div">

             <div class="step4-line-hight">
                     <strong>U. Title IV grant funds for student to return</strong><br>
                     &nbsp; &nbsp; From the Inital amount of Title IV grants for student to return (Box S) subtract the Amount of Title IV grant protection(Box T)<br>
                   </div>

                <div class="br-space"></div>
                
                  <div class="div1">
                      <div class="text-center">Box S</div>
                       <div class="input-group">
                           <input name="txt9Boxs" type="text" id="txt9Boxs" class="form-control k-textbox money text-right border-textbox" readonly="">
                                                                                                                                                              
                         </div>
                  </div>
      
                  <div class="small small-topspace"><br><i class="fa fa-minus"></i></div>
            
                  <div class="div2">
                      <div class="text-center">Box T</div>
                       <div class="input-group">
                           <input name="txt9BoxT" type="text" id="txt9BoxT" class="form-control k-textbox money text-right border-textbox" readonly />
                                                                                                                     
                      </div>
                  </div>
      
                   <div class="small small-topspace"><br>=</div>

                   <div class="small small-topspace"><br>U.</div>
      
                        
                  <div class="div3"><br>
                      <div class="input-group input-br-space">
                           <span class="input-group-addon input-height-sub">$</span>
                          <input name="txtBox9Result" type="text" id="txtBox9Result" class="form-control k-textbox text-right amount-textfield" readonly/>
                                                   
                        </div>
                  </div>

       
            </div>

            </div>
        </div>
    </div>
</div>
</div>