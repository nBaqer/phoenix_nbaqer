﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step7.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step7" %>
<div class="k-state-active" id="dvAmountUnearned">
    <span>7. Initial Amount of Unearned Title IV Aid Due from the Student</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">
        <div class="textR2T4Results padding5px">
           
            <div id="amount-title-mine-div">
                
             <div class="step4-line-hight">From the amount of Title IV aid to be returned (Box K) subtract the amount for the school to return (Box O).</div>
                 <div class="br-space"></div>
                  <div class="div1">
                      <div class="text-center">Box K</div>
                       <div class="input-group">
                           <input name="txt7K" type="text" id="txt7K" class="form-control k-textbox money text-right border-textbox" readonly="">
                                                                                                                                   
                         </div>
                  </div>
      
                  <div class="small small-topspace"><br><i class="fa fa-minus"></i></div>
            
                  <div class="div2">
                      <div class="text-center">Box O</div>
                       <div class="input-group">
                            <input name="txt7O" type="text" id="txt7O" class="form-control k-textbox money text-right border-textbox" readonly="">
                                                                                      
                      </div>
                  </div>
      
                   <div class="small small-topspace"><br>=</div>

                   <div class="small small-topspace"><br>Q.</div>
      
                        
                  <div class="div3"><br>
                      <div class="input-group input-br-space">
                           <span class="input-group-addon input-height-sub">$</span>
                           <input name="txtStep7Result" type="text" id="txtStep7Result" class="form-control k-textbox money text-right amount-textfield" readonly="">
                         </div>
                  </div>
                
       
            </div>
            <div id="amount-title-mine-div">
            <div class="step4-line-hight"><i class="fa fa-hand-o-right"></i>&nbsp If Box Q is ≤ zero. <strong>Stop</strong> If qreater than zero, go to Step 8</div>
            </div>
            
        </div>
    </div>
</div>
</div>