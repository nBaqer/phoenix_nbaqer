﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step1.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step1" %>
<div class="k-state-active" id="dvAidInfo">
    <span>1. Student's Title IV Aid Information</span>
    <span class="helpIcon"><a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">

        <div class="textR2T4Results padding5px">
            <div id="table-scroll-bar">
           <table id="aidInfo">
                <tbody>
                    <tr>
                      <td  class="text-align-texttop"><table>
                        <thead>
                          <tr>
                            <th colspan="2" class="text-left">Title IV Grant Programs</th>
                            <th valign="bottom" class="topLeftlables text-center">Amount Disbursed</th>
                            <th class="stup1-width-small">&nbsp;</th>
                            <th valign="middle"><span class="topLabels text-center">Amount That Could Have Been Disbursed</span></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="width-20">1.</td>
                            <td class="width-160">Pell Grant</td>
                            <td class="stup1-width"><input name="txtPellA" type="text" id="txtPellA" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td class="stup1-width"><input name="txtPellB" type="text" id="txtPellB" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td >2.</td>
                            <td>FSEOG</td>
                            <td ><input name="txtFSEOGA" type="text" id="txtFSEOGA" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td><input name="txtFSEOGB" type="text" id="txtFSEOGB" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td >3.</td>
                            <td>TEACH Grant</td>
                            <td><input name="txtTeachA" type="text" id="txtTeachA" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td><input name="txtTeachB" type="text" id="txtTeachB" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td valign="top" >4.</td>
                            <td valign="top">Iraq Afghanistan Service Grant</td>
                            <td><input name="txtIraqAfganA" type="text" id="txtIraqAfganA" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td><input name="txtIraqAfganB" type="text" id="txtIraqAfganB" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td><div class="text-right">Subtotal</div></td>
                            <td align="center"><div class="input-group top-margin5px left-float">
                                                 
                       <div class="input-group">
                        <span class="input-group-addon input-height">A</span>
                         <input name="txtSubTotalA" type="text" id="txtSubTotalA" class="form-control k-textbox money text-right sub-width select-numiric-box amount-textfield" readonly="readonly" />
                          
                         </div>
                          </div></td>
                            <td align="center">&nbsp;</td>
                            <td align="center"><div class="input-group left-float">
                            
                       <div class="input-group">
                        <span class="input-group-addon input-height">C</span>
                         <input name="txtSubTotalC" type="text" id="txtSubTotalC" class="form-control k-textbox money sub-width text-right select-numiric-box amount-textfield" readonly="readonly" />
                         </div>
                 
                            </div></td>
                          </tr>
                          <tr>
                            <th colspan="5">&nbsp;</th>
                          </tr>
                          <tr >
                            <th colspan="5">&nbsp;</th>
                          </tr>
                          <tr>
                            <th colspan="2" class="text-left">Title IV Loan Programs</th>
                            <th valign="bottom" class="topLeftlables text-center">Net Amount Disbursed</th>
                            <td>&nbsp;</td>
                            <th valign="middle"><span class="topLabels text-center">Net Amount That Could Have  Been Disbursed</span></th>
                          </tr>
                          <tr>
                            <td class="width-20" valign="top" >5.</td>
                            <td valign="top">Unsubsidized Direct Loan</td>
                            <td><input name="txtUnSubFFEL" type="text" id="txtUnSubFFEL" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td><input name="txtUnSubFFElM" type="text" id="txtUnSubFFElM" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td >6.</td>
                            <td>Subsidized Direct Loan</td>
                            <td><input name="txtSubFFEL" type="text" id="txtSubFFEL" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td><input name="txtSubFFElM" type="text" id="txtSubFFElM" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td >7.</td>
                            <td>Perkins Loan</td>
                            <td><input name="txtPerkins" type="text" id="txtPerkins" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td><input name="txtPerkinsM" type="text" id="txtPerkinsM" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td >8.</td>
                            <td>Direct Grad PLUS Loan</td>
                            <td><input name="txtFFElStudent" type="text" id="txtFFElStudent" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td><input name="txtFFELStudentM" type="text" id="txtFFELStudentM" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td valign="top" >9.</td>
                            <td valign="top">Direct Parent PLUS Loan</td>
                            <td><input name="txtFFELParent" type="text" id="txtFFELParent" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                            <td>&nbsp;</td>
                            <td><input name="txtFFELParentM" type="text" id="txtFFELParentM" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td class="text-right">Subtotal</td>
                            <td><div class="input-group top-margin5px left-float">
                            
                            <div class="input-group">
                        <span class="input-group-addon input-height">B</span>
                        
                         <input name="txtSubTotalB" type="text" id="txtSubTotalB" class="form-control k-textbox money text-right sub-width select-numiric-box amount-textfield" readonly="readonly" />
                        
                            </div></div></td>

                            <td>&nbsp;</td>
                            <td><div class="input-group top-margin5px left-float">
                            <div class="input-group">
                        <span class="input-group-addon input-height">D</span>
                         <input name="txtSubtotalD" type="text" id="txtSubtotalD" class="form-control k-textbox money text-right sub-width select-numiric-box amount-textfield" readonly="readonly" />
                        </div>
                            </div></td>
                          </tr>
                        </tbody>
                    </table></td>
                      <td width="25" class="text-align-texttop">&nbsp;</td>
                        <td align="right" valign="top"><table>
                          <thead>
                            <tr class="text-left height35px">
                              <th><span class="inputUnit">E</span> Total Title IV aid disbursed for the period.</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="innerTables"><table>
                                <tbody>
                                  <tr>
                                    <th class="steo-1-width">&nbsp;</th>
                                    <th class="steo-1-width">A.</th>
                                    <td><input name="txtEA" id="txtEA" type="text" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                                  </tr>
                                  <tr>
                                    <th><i class="fa fa-plus"></i></th>
                                    <th>B.</th>
                                    <td><input name="txtEB" type="text" id="txtEB" class="form-control k-textbox money text-right" readonly="readonly" /></td>
                                  </tr>
                                  <tr>
                                    <th> <h4>=</h4>
                                    </th>
                                    <th>E.</th>
                                    <td><div class="input-group left-float top-margin5px">
                                      <div class="input-group"> <span class="input-group-addon input-height">$</span>
                                        <input name="txtResultE" type="text" id="txtResultE" class="form-control k-textbox money text-right select-numiric-box amount-textfield" readonly="readonly" />
                                      </div>
                                    </div></td>
                                  </tr>
                                </tbody>
                              </table></td>
                            </tr>
                          </tbody>
                          <thead>
                            <tr class="badgeTextLine">
                              <th class="valign-btm"> <div class="mine-inupt-unit">
                                <div class="inputUnit2">F</div>
                                <div class="input-unit2"> <strong> Total Title IV grant aid disbursed and that could have been disbursed for the period.</strong></div>
                              </div>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="innerTables"><table>
                                <tbody>
                                  <tr>
                                    <th  class="steo-1-width">&nbsp;</th>
                                    <th  class="steo-1-width">A.</th>
                                    <td><input name="txtFA" type="text" id="txtFA" readonly="readonly" class="form-control k-textbox money text-right sectionF" /></td>
                                  </tr>
                                  <tr>
                                    <th><i class="fa fa-plus"></i></th>
                                    <th><span>C.</span></th>
                                    <td><input name="txtFC" type="text" id="txtFC" readonly="readonly" class="form-control k-textbox money text-right sectionF" /></td>
                                  </tr>
                                  <tr>
                                    <th> <h4>=</h4>
                                    </th>
                                    <th>F.</th>
                                    <td><div class="input-group left-float top-margin5px">
                                      <div class="input-group"> <span class="input-group-addon input-height">$</span>
                                        <input name="txtstep1F" type="text" id="txtstep1F" class="form-control k-textbox money text-right select-numiric-box amount-textfield" readonly="readonly" />
                                      </div>
                                    </div></td>
                                  </tr>
                                </tbody>
                              </table></td>
                            </tr>
                          </tbody>
                          <thead>
                            <tr class="badgeTextLine">
                              <th> <div class="mine-inupt-unit">
                                <div class="inputUnit2">G</div>
                                <div class="input-unit2"> <strong> Total Title IV aid disbursed and that could have been disbursed for the period.</strong></div>
                              </div>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="innerTables"><table>
                                <tbody>
                                  <tr>
                                    <td><table>
                                      <tbody>
                                        <tr>
                                          <th class="steo-1-width">&nbsp;</th>
                                          <th class="steo-1-width">A.</th>
                                          <td><input name="txtStep1FA" type="text" id="txtStep1FA" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                                        </tr>
                                        <tr>
                                          <th></th>
                                          <th>B.</th>
                                          <td><input name="txtStep1FB" type="text" id="txtStep1FB" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                                        </tr>
                                        <tr>
                                          <th></th>
                                          <th>C.</th>
                                          <td><input name="txtStep1FC" type="text" id="txtStep1FC" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                                        </tr>
                                        <tr>
                                          <th><i class="fa fa-plus"></i></th>
                                          <th>D.</th>
                                          <td><input name="txtStep1FD" type="text" id="txtStep1FD" readonly="readonly" class="form-control k-textbox money text-right" /></td>
                                        </tr>
                                        <tr>
                                          <th> <h4>=</h4>
                                          </th>
                                          <th>G.</th>
                                          <td><div class="input-group left-float top-margin5px">
                                            <div class="input-group"> <span class="input-group-addon input-height">$</span>
                                              <input name="txtStep1G" type="text" id="txtStep1G" class="form-control k-textbox money text-right select-numiric-box amount-textfield" readonly="readonly" />
                                            </div>
                                          </div></td>
                                        </tr>
                                      </tbody>
                                    </table></td>
                                  </tr>
                                </tbody>
                              </table></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
      </div>
    </div>
</div>