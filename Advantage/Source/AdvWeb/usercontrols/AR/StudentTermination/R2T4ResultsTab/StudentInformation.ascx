﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentInformation.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_StudentInformation" %>
<div class="firstRow width100">
    <div class="inLineBlock width15 right-margin10px">
        <span class="firstRowLabel">Student:</span>
        <span id="lblStudentNameR2T4Result"></span>
    </div>

    <div class="inLineBlock width9 right-margin10px">
        <span class="firstRowLabel">SSN: </span>
        <span id="lblSSNR2T4Result"></span>
    </div>

    <div class="inLineBlock width17 right-margin10px">
        <span class="firstRowLabel">Date Form Completed:</span>
        <span id="dateCompleted"></span>
    </div>

    <div class="inLineBlock width18 right-margin10px">
        <span class="firstRowLabel">Date of determination:</span>
        <span id="dateDetermination"></span>
    </div>
</div>