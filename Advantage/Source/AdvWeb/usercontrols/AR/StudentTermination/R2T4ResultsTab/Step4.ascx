﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Step4.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_Step4" %>
<div class="k-state-active" id="dvDisbursed">
    <span>4. Title IV Aid to be Disbursed or Returned</span>
    <span class="helpIcon"><%--<a href="../help/AdvantageHelp.htm" id="MainMenu_a2" target="_blank" class="border0"></a>--%>
    </span>
    <div class="border-lightblue">
        <div class="step1-margin">
            <div class="textR2T4Results padding5px">
                <div id="table-scroll-bar">
                    <div id="amount-title-mine-div">
                        <div class="step4-line-hight">
                            <i class="fa fa-hand-o-right"></i>&nbsp; If the amount in Box I is greater than the amount in Box E, go to Item J (post-withdrawal disbursement).
                 <div class="br-space"></div>

                            <i class="fa fa-hand-o-right"></i>&nbsp; If the amount in Box I is less than the amount in Box E, go to Title IV aid to be returned (Item K).

                  <div class="br-space"></div>

                            <i class="fa fa-hand-o-right"></i>&nbsp;  If the Amounts in box I and Box E are equal, <strong>STOP</strong>. No further action is necessary.
                     <div class="br-space"></div>
                            <strong>J.  Post-withdrawal disbursement</strong><br />
                            From the Amount of Title IV aid earned by the student (Box I) subtract the Total Title IV aid disbursed for the period (Box E). This is the amount of the post-withdrawal disbursement.
                        </div>
                        <div class="br-space"></div>

                        <div class="div1">
                            <div class="text-center">Box I</div>
                            <div class="input-group">
                                <input name="txt4JI" type="text" id="txt4JI" class="form-control k-textbox money text-right border-textbox" readonly />

                            </div>
                        </div>

                        <div class="small small-topspace">
                            <br>
                            <i class="fa fa-minus"></i></div>

                        <div class="div2">
                            <div class="text-center">Box E</div>
                            <div class="input-group">
                                <input name="txt4JE" type="text" id="txt4JE" class="form-control k-textbox money border-textbox text-right" readonly />
                            </div>
                        </div>

                        <div class="small small-topspace">
                            <br>
                            =</div>

                        <div class="small small-topspace">
                            <br>
                            J.</div>
                        <div class="div3">
                            <br>
                            <div class="input-group input-br-space">
                                <span class="input-group-addon input-height-sub">$</span>
                                <input name="txtResultJ" type="text" id="txtResultJ" class="form-control k-textbox money text-right select-numiric-box amount-textfield" readonly="">
                            </div>
                        </div>
                        <div class="br-space"></div>

                    </div>
                    <div class="step4-line-hight"><strong>Stop here,</strong> and enter the amount in “J” in Box 1 on page 3 (Post-withdrawal disbursement tracking sheet).</div>
                    <div class="br-space"></div>
                    <div class="step4-line-hight">
                        <strong>K.  Title IV aid to be returned</strong><br />
                        From the Total Title IV aid disbursed for the period (Box E) subtract the amount of Title IV aid earned by the student (Box I). This is the amount of Title IV aid that must be returned.
                     <div class="br-space"></div>

                    </div>

                    <div id="amount-title-mine-div">

                        <div class="div1">
                            <div class="text-center">Box E</div>
                            <div class="input-group">

                                <input name="txt4KE" type="text" id="txt4KE" class="form-control k-textbox money border-textbox text-right" readonly />

                            </div>
                        </div>

                        <div class="small small-topspace">
                            <br>
                            <i class="fa fa-minus"></i></div>

                        <div class="div2">
                            <div class="text-center">Box I</div>
                            <div class="input-group">
                                <input name="txt4KI" type="text" id="txt4KI" class="form-control k-textbox money border-textbox text-right" readonly />

                            </div>
                        </div>

                        <div class="small small-topspace">
                            <br>
                            =</div>

                        <div class="small small-topspace">
                            <br>
                            K.</div>


                        <div class="div3">
                            <br>
                            <div class="input-group input-br-space">
                                <span class="input-group-addon input-height-sub">$</span>
                                <input name="txtResultK" type="text" id="txtResultK" class="form-control k-textbox money text-right select-numiric-box amount-textfield" readonly="">
                            </div>
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
