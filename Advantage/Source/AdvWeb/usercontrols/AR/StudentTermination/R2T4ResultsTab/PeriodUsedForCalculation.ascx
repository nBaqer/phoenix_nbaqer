﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PeriodUsedForCalculation.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4ResultsTab_PeriodUsedForCalculation" %>
<div class="secondRow top-margin10px">
    <table width="100%">
        <tbody>
            <tr>
                <td width="60%">
                    <label>Period used for calculation:</label>
                </td>
                <td>
                    <label>Program type :</label>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="rdbPayPeriod" type="radio" name="rdbPayPeriod" value="rdbPayPeriod" checked />
                    Payment period
                    <input id="rdbPeriodEnroll" type="radio" name="rdbPayPeriod" value="rdbPeriodEnroll" />
                    Period of enrollment
                </td>
                <td>
                    <input id="rdbClock" type="radio" name="rdbClock" value="rdbClock" checked />
                    Clock Hour

                    <input id="rdbCredit" type="radio" name="rdbClock" value="rdbCredit" />
                    Credit Hour
                </td>
            </tr>
        </tbody>
    </table>
</div>