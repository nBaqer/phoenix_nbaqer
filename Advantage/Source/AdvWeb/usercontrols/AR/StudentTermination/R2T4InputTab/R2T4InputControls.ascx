﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="R2T4InputControls.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4InputTab_R2T4InputControls" %>
<div class="buttonDiv">
    <hr class="top-margin20px">
    <div class="inLineBlock top-margin7px">
        <span>
            <button type="button" id="btnR2T4InputBack">
                &lt; Back
            </button>
        </span>
        <button type="button" id="btnR2T4InputNext">
            Next &gt;
        </button>
    </div>
    <div class="inlineBtn">
        <input type="hidden" id="hdnR2T4InputId" />
        <button type="button" id="btnR2T4InputSave" style="color: rgba(255, 255, 255, 0.9) !important;background-color: #1976d2 !important;">
            Save
        </button>
        <button type="button" id="btnCancelTerminationR2T4Input">
            Cancel
        </button>
    </div>
    <div class="inLineBlock top-margin7px disabled" >
        <button type="button" style="color: rgba(255, 255, 255, 0.9) !important;background-color: #558b2f !important;">
            Approve Termination
        </button>
    </div>
</div>