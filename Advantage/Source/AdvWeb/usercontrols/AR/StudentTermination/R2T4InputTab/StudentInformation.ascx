﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentInformation.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4InputTab_StudentInformation" %>
<div class="firstRow">
    <div class="inLineBlock width15 right-margin10px">
        <span class="firstRowLabel">Student:</span>
        <span id="lblStundentName"></span>
    </div>

    <div class="inLineBlock width9 right-margin10px">
        <span class="firstRowLabel">SSN: </span>
        <span id="lblStundentSSN"></span>
    </div>

    <div class="inLineBlock right-margin20px">
        <span class="firstRowLabel">Enrollment:</span>
        <span id="lblEnrollmentName"></span>
    </div>
</div>