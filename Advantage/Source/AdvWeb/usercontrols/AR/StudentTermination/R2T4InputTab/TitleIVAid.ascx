﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TitleIVAid.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4InputTab_TitleIVAid" %>
<div class="k-state-active" id="dvTitleIVAid">

    <span>1. Title IV aid</span>
    <span class="helpIcon" id="title4AidHelp">
        <img src="../images/HelpIcon.png" alt="Help">
    </span>

    <div class="titleIV-alignment">
         <div  class="step1-margin">
      
       
        <div class="formRow marginBtm0">
            <div class="left leftLable">
               <strong>Title IV Grant Programs</strong>
            </div>
            <div class="inLineBlock amount-disbursed">
                Amount disbursed
            </div>
            <div class="inLineBlock amount-could-be-disbursed">
                Amount that could have been disbursed
            </div>
        </div>

        <div class="formRow marginTop0">
            <div class="left leftLable">
                1. Pell Grant
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtPellGrantDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtPellGrantCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>
        <div class="formRow">
            <div class="left leftLable">
                2. FSEOG
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtFseogDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtFseogCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>
        <div class="formRow">

            <div class="left leftLable">
                3. TEACH Grant
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtTeachGrantDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtTeachGrantCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>

        <div class="formRow">
            <div class="left leftLable">
                4. Iraq and Afghanistan Service Grant
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtIraqAfgGrantDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtIraqAfgGrantCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>
            </div>
    </div>
    <div class="titleIV-alignment">
        <!--second block-->
        <div class="step1-margin">
       

        <div class="formRow marginBtm0">
            <div class="left leftLable">
               <strong>Title IV Loan Programs</strong>
            </div>
            <div class="inLineBlock amount-disbursed">
                Net amount disbursed
            </div>
            <div class="inLineBlock amount-could-be-disbursed">
                Net amount that could have been disbursed
            </div>
        </div>

        <div class="formRow marginTop0">
            <div class="left leftLable">
                5. Unsubsidized Direct Loan
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtUnsubLoanNetAmountDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtUnsubLoanNetAmountCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>
        <div class="formRow">
            <div class="left leftLable">
                6. Subsidized Direct Loan
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtSubLoanNetAmountDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtSubLoanNetAmountCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>
        <div class="formRow">
            <div class="left leftLable">
                7. Perkins Loan
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtPerkinsLoanDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtPerkinsLoanCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>
        <div class="formRow">
            <div class="left leftLable">
                8. Direct Graduate PLUS Loan
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtDirectGraduatePlusLoanDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtDirectGraduatePlusLoanCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>

        <div class="formRow">
            <div class="left leftLable">
                9. Direct Parent PLUS Loan
            </div>
            <div class="inLineBlock">
                <input type="text" id="txtDirectParentPlusLoanDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
            <div class="inLineBlock left-margin20px">
                <input type="text" id="txtDirectParentPlusLoanCouldDisbursed" placeholder="$0.00" class="form-control k-textbox money text-right sub-table" maxlength="14" />
            </div>
        </div>
            </div>
    </div>
</div>