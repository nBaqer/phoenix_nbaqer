﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstitutionalCharges.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4InputTab_InstitutionalCharges" %>
<div class="k-state-active" id="dvInstitutionalCharges">

    <span>3. Institutional charges posted</span>
    <span class="helpIcon" id="institutionalChargeHelp">
        <img src="../images/HelpIcon.png" alt="Help">
    </span>

    <div>
        <div class="step1-margin">
            <div class="percentage-period-completed">
                <div class="formRow top-margin15px" id="dvTuitionCharged" style="display: block">
                    <div class="table-spacing-td">
                        <div class="left leftLable">
                            &nbsp; Tuition charged by payment period
                        </div>
                        <div class="inLineBlock">
                            <label>
                                <input type="checkbox" id="chkTuitionCharged" class="rfdInputDisabled">
                            </label>
                        </div>
                    </div>
                    <div class="top-margin10px table-spacing-td">
                        <div class="left leftLable">
                            &nbsp; Title IV credit balance refunded to student
                        </div>
                        <div class="inLineBlock">
                            <input type="text" id="txtcreditBalanceRefunded" placeholder="$0.00" class="sub-table form-control k-textbox money text-right" maxlength="14">
                        </div>
                    </div>
                </div>
                <div id="dvInstitutionalChargesPosted">
                    <table class="table-left-padding table-up-spacing" id="tblInstitutionalChargesPosted">
                        <tr>
                            <td>&nbsp;</td>
                            <td class="leftLable-width ">&nbsp;</td>
                            <td class="text-center">Amount</td>
                        </tr>
                        <tr>
                            <td>Tuition</td>
                            <td>&nbsp;</td>
                            <td class="sub-table">
                                <input type="text" id="txtTuitionFee" placeholder="$0.00" class="form-control k-textbox money text-right" maxlength="14"></td>
                        </tr>
                        <tr>
                            <td>Room</td>
                            <td>&nbsp;</td>
                            <td class="sub-table">
                                <input type="text" id="txtRoomFee" placeholder="$0.00" class="form-control k-textbox money text-right" maxlength="14"></td>
                        </tr>
                        <tr>
                            <td>Board</td>
                            <td>&nbsp;</td>
                            <td class="sub-table">
                                <input id="txtBoardFee" placeholder="$0.00" class="form-control k-textbox money text-right" maxlength="14" type="text"></td>
                        </tr>
                        <tr>
                            <td>Other</td>
                            <td>&nbsp;</td>
                            <td class="sub-table">
                                <input id="txtOtherFee" placeholder="$0.00" class="form-control k-textbox money text-right" maxlength="14" type="text"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
