﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PercentageOfPeriod.ascx.vb" Inherits="usercontrols_AR_StudentTermination_R2T4InputTab_PercentageOfPeriod" %>
<div class="k-state-active" id="dvPercentageOfPeriod">
    <span>2. Percentage of period completed </span>
    <span id="helpIcon">
        <span id="dialog"></span>
        <span class="helpIcon" id="percentOfPaymentHelp">
            <img src="../images/HelpIcon.png" alt="Help">
        </span>
    </span>
    <span id="studentAwardsDialog" style="display: none;"></span>
    <div>
        <div class="step1-margin">
            <div class="percentage-period-completed">
                <div class="formRow top-margin15px" id="dvAttendanceRequired">
                    <div class="left leftLable">
                        Not required to take attendance
                    </div>
                    <div class="inLineBlock">
                        <label>
                            <input type="checkbox" id="chkAttendance" />
                        </label>
                    </div>
                </div>
                <div class="formRow" id="startDate">
                    <div class="left leftLable">
                        Start date <span id="spStartDt" class="red-color"></span>
                    </div>
                    <div class="inLineBlock">
                        <input id="dtStartDate" name="dtStartDate" class="mydate-picker k-datepicker dateCustom" data-validation="dateWithFormat" placeholder="MM/DD/YYYY" />
                    </div>
                </div>
                <div class="formRow top-margin15px" id="endDate">
                    <div class="left leftLable">
                        Scheduled End date <span id="spEndDt" class="red-color"></span>
                    </div>
                    <div class="inLineBlock">
                        <input id="dtEndDate" name="dtEndDate" class="mydate-picker k-datepicker dateCustom" data-validation="dateWithFormat" placeholder="MM/DD/YYYY" />
                    </div>
                </div>
                <div class="formRow top-margin15px" id="dvWithdrawalDate">
                    <div class="left leftLable">
                        Withdrawal date<span id="spWithdrawDt" class="red-color"></span>
                    </div>
                    <div class="inLineBlock">
                        <input id="dtWithdrawDate" name="dtWithdrawDate" placeholder="MM/DD/YYYY" class="mydate-picker k-datepicker dateCustom" data-validation="dateWithFormat" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left leftLable" id="Hours">
                        <span id="lblHoursCompleted">Hours scheduled to complete</span>
                        <span id="spHoursCompleted" class="red-color"></span>
                    </div>
                    <div class="inLineBlock">
                        <input type="text" id="txtScheduledHours" name="ScheduledHours" class="form-control k-textbox sub-table" maxlength="4" />
                    </div>
                </div>
                <div class="formRow formRow-sub">
                    <div class="left leftLable" id="periods">
                        <span id="lblHoursPeriod">Total hours in period</span>
                        <span id="spHoursPeriod" class="red-color"></span>
                    </div>
                    <div class="inLineBlock">
                        <input type="text" id="txtTotalHours" name="TotalHours" class="form-control k-textbox sub-table" maxlength="4" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="window" style="display: none;">
    <table class="tblPercentageOfPeriodPopup">
        <tr>
            <td>
                <span id="messageContent"></span>
            </td>
        </tr>
        <tr>
            <td height="10" colspan="3" id="tdEndDateHeight">&nbsp;</td>
        </tr>
        <tr id="trPeriodEndDate">
            <td><span id="lblPeriodEndDate">End Date</span> </td>
            <td>&nbsp</td>
            <td><input id="dtPeriodEndDate" name="dtPeriodEndDate" placeholder="MM/DD/YYYY" class="mydate-picker k-datepicker dateCustom" data-validation="dateWithFormat" />
                <span class="red-color" id="spEnddate">Please enter the End Date. </span>
            </td>
        </tr>
        <tr>
            <td height="10" colspan="3">&nbsp;</td>
        </tr>
        <tr id="trNoOfDays">
            <td>Number of Days</td>
            <td>&nbsp</td>
            <td class="txtNoOfDaysWidth"><input type="text" id="txtNoOfDays"  class="form-control k-textbox">
                <span class="red-color" id="spNoOfDays">Please enter the number of days. </span>
            </td>
        </tr>
        <tr id="trNoOfDaysHeight">
            <td height="10" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="middle"><input id="btnEndDate" type="button" class="k-button btnPercentageOfPeriod" value="Ok"/></td>
        </tr>
    </table>
</div>