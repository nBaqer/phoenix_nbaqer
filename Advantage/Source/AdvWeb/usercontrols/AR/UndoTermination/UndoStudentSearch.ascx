﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UndoStudentSearch.ascx.vb" Inherits="usercontrols_AR_UndoTermination_UndoStudentSearch" %>

<div class="formRow">
    <div class="left leftLable">
        Student <span class="red-color">*</span>
    </div>
    <div class="inLineBlock">
        <input type="text" id="txtUndoStudentSearch"  class="form-control k-textbox inputCustom"  maxlength="152" required ; autofocus="autofocus"/>
    </div>
</div>
<div id="dvUndoEnrollment" style="display: none;" >
    <div class="formRow">
        <div class="left leftLable vertical-align-top">
            Enrollment <span class="red-color">*</span>
        </div>
        <div class="inLineBlock">
            <div id="UndoStudentEnrollment"></div>
            <span id="UndoSpnEnrollment" style="color: red; display: none;">Please select inactive enrollment. It is required.</span>
        </div>
    </div>
</div>
