﻿
Imports System.Data
Imports System.Linq.Expressions
Imports System.Reflection
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.Common.AR
Imports Telerik.Web.UI
Imports Campus = FAME.Advantage.Domain.Campuses.Campus

Partial Class StudentSchedule
    Inherits System.Web.UI.UserControl
    Property panelSchedule As Panel
    Property ddlSched As DropDownList
    Property textBadgeNumber As TextBox
    Property enrollmentId As String
        Get
            If (ViewState("scheduler_enrollmentId") Is Nothing) Then

                Return Guid.Empty.ToString()
            Else
                Return CType(ViewState("scheduler_enrollmentId"), String)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("scheduler_enrollmentId") = value
        End Set
    End Property
    Property campusId As String
        Get
            If (ViewState("scheduler_campusId") Is Nothing) Then

                Return Guid.Empty.ToString()
            Else
                Return CType(ViewState("scheduler_campusId"), String)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("scheduler_campusId") = value
        End Set
    End Property
    Property leadId As String
        Get
            If (ViewState("scheduler_leadId") Is Nothing) Then

                Return Guid.Empty.ToString()
            Else
                Return CType(ViewState("scheduler_leadId"), String)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("scheduler_leadId") = value
        End Set
    End Property

    Property studentId As String
        Get
            If (ViewState("scheduler_studentId") Is Nothing) Then

                Return Guid.Empty.ToString()
            Else
                Return CType(ViewState("scheduler_studentId"), String)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("scheduler_studentId") = value
        End Set
    End Property
    Property resourceId As Integer
        Get
            If (ViewState("scheduler_resourceId") Is Nothing) Then

                Return Integer.MinValue
            Else
                Return CType(ViewState("scheduler_resourceId"), Integer)
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("scheduler_resourceId") = value
        End Set
    End Property
    Property MyAdvAppSettings As AdvAppSettings
        Get
            If (ViewState("scheduler_AppSettings") Is Nothing) Then

                Return New AdvAppSettings()
            Else
                Return CType(ViewState("scheduler_AppSettings"), AdvAppSettings)
            End If
        End Get
        Set(ByVal value As AdvAppSettings)
            ViewState("scheduler_AppSettings") = value
        End Set
    End Property
    Property Pobj As UserPagePermissionInfo
        Get
            If (ViewState("scheduler_Permissions") Is Nothing) Then

                Return New UserPagePermissionInfo()
            Else
                Return CType(ViewState("scheduler_Permissions"), UserPagePermissionInfo)
            End If
        End Get
        Set(ByVal value As UserPagePermissionInfo)
            ViewState("scheduler_Permissions") = value
        End Set
    End Property

    Protected DtAvail As DataTable
    Protected BoolAction As Boolean
    Property IsByProgramEnrollment As Boolean
        Get
            If (ViewState("scheduler_isByProgramEnrollment") Is Nothing) Then

                Return False
            Else
                Return CType(ViewState("scheduler_isByProgramEnrollment"), String)
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("scheduler_isByProgramEnrollment") = value
        End Set
    End Property
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        If Me.IsPostBack Then
            Dim eventTarget As String
            Dim eventArgument As String

            If ((Me.Request("__EVENTTARGET") Is Nothing)) Then
                eventTarget = String.Empty
            Else
                eventTarget = Me.Request("__EVENTTARGET")
                If ((Me.Request("__EVENTARGUMENT") Is Nothing)) Then
                    eventArgument = String.Empty
                Else
                    eventArgument = Me.Request("__EVENTARGUMENT")

                    If eventTarget = "ClosedRadWindow" Then
                        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
                    End If
                End If
            End If
        End If
        panelSchedule = panelSchedule
        ddlSched = ddlSchedule
        textBadgeNumber = txtBadgeNumber

        If (String.IsNullOrEmpty(enrollmentId)) Then Return


        Dim facade As New RegFacade
        If Not facade.IsStudentHold(enrollmentId) = "0" Then

            btnAdd.Enabled = False
        Else
            If Pobj.HasAdd Then
                btnAdd.Enabled = True
            Else
                btnAdd.Enabled = False
            End If
        End If
        '''''''''''''''''
        If (Not campusId Is Nothing AndAlso campusId <> Guid.Empty.ToString()) Then
            Try
                'DE13245.If classes don't use time clock then the fields are not required
                If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "no" Then
                    lblBadgeNumPrompt.Text = Replace(lblBadgeNumPrompt.Text, "*", "")
                    lblBadgeNumPrompt.Visible = False
                    txtBadgeNumber.Enabled = False
                    txtBadgeNumber.Visible = False
                    'lblSchedulePrompt.Text = Replace(lblSchedulePrompt.Text, "*", "")
                    'ddlSchedule.Enabled = False
                    'btnSaveSchedule.Enabled = False
                    btnSaveSchedule.Text = "Save Schedule"
                Else
                    btnSaveSchedule.Text = "Save Schedule and Badge Number"
                End If
            Catch
                btnSaveSchedule.Text = "Save Schedule and Badge Number"
            End Try

        End If


        If pnlSchedule.Visible = True Then
            btnSaveSchedule.Visible = True
            If (ddlSchedule.Visible = True And ddlSchedule.Enabled = True) Or (txtBadgeNumber.Visible = True And txtBadgeNumber.Enabled = True) Then
                btnSaveSchedule.Enabled = True
            Else
                btnSaveSchedule.Enabled = False
            End If
        Else
            btnSaveSchedule.Visible = False
        End If

        If (IsByProgramEnrollment) Then
         
            scheduleTable.Visible = False
        Else
          
            scheduleTable.Visible = True
        End If
    End Sub

    Public Sub Initialize(ByVal enrollmentId As String, ByVal campusId As String, ByVal resourceId As Integer, ByVal leadId As String, ByVal studentId As String, ByVal appSettings As AdvAppSettings,
                           ByVal permissions As UserPagePermissionInfo)
        Me.enrollmentId = enrollmentId
        Me.campusId = campusId
        Me.resourceId = resourceId
        Me.leadId = leadId
        Me.studentId = studentId
        Me.MyAdvAppSettings = appSettings
        Me.Pobj = permissions
        IsByProgramEnrollment = New StudentEnrollmentDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString).IsEnrollmentByProgram(Guid.Parse(enrollmentId))

        If ((Not enrollmentId Is Nothing AndAlso enrollmentId <> Guid.Empty.ToString()) AndAlso (Not campusId Is Nothing AndAlso campusId <> Guid.Empty.ToString())) Then
            BuildGradedAndScheduledLbx()
            BuildAvailableLbx()
            ' 2/1/07 ThinkTron - add support for Time Clock Schedules
            BindTimeClockSchedules()
            btnRemove.OnClientClicking = "ConfirmAttendance"
        End If

    End Sub


    Public Sub ToggleButtons()
        If Not String.IsNullOrEmpty(enrollmentId) Then
            If New TranscriptFacade().IsEnrollmentInSchool(enrollmentId) = False Then
                btnAdd.Enabled = False
                btnRemove.Enabled = False
            Else
                btnAdd.Enabled = True
                btnRemove.Enabled = True
            End If
        End If
    End Sub


    Public Sub Reset(ByVal enrollmentId As String, ByVal campusId As String)
        Me.enrollmentId = enrollmentId
        Me.campusId = campusId

        If ((Not enrollmentId Is Nothing AndAlso enrollmentId <> Guid.Empty.ToString()) AndAlso (Not campusId Is Nothing AndAlso campusId <> Guid.Empty.ToString())) Then
            Me.BuildGradedAndScheduledLbx()
            Me.BuildAvailableLbx()
            Me.BindTimeClockSchedules()
            Me.ToggleButtons()
        End If


    End Sub
    ''' <summary>
    ''' Helper function for BindTimeClockSchedules.  Used to build javascript that we attach
    ''' to the To Maintenance Page button to show Setup Schedules as a popup
    ''' </summary>
    ''' <param name="popupUrl"></param>
    ''' <param name="width"></param>
    ''' <param name="height"></param>
    ''' <param name="retControlId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetJavsScriptPopup(ByVal popupUrl As String,
                                              ByVal width As Integer,
                                              ByVal height As Integer,
                                              ByVal retControlId As String) As String
        Dim js As New StringBuilder
        js.Append("var strReturn; " & vbCrLf)
        js.Append("strReturn=window.open('")
        js.Append(popupUrl)
        js.Append("',null,'re-sizable:yes;status:no;scrollbars=yes;minimize:yes; maximize:yes; dialogWidth:")
        js.Append(width.ToString())
        js.Append("px;dialogHeight:")
        js.Append(height.ToString())
        js.Append("px;dialogHide:true;help:no;scroll:yes'); " & vbCrLf)
        If Not retControlId Is Nothing Then
            If retControlId.Length <> 0 Then
                js.Append("if (strReturn != null) " & vbCrLf)
                js.Append("     document.getElementById('")
                js.Append(retControlId)
                js.Append("').value=strReturn;" & vbCrLf)
            End If
        End If
        Return js.ToString()
    End Function


    Private Sub MarkCompletedCoursesInRed(ByVal dtAvailable As DataTable)
        Dim isCourseCompleted As Boolean
        ' US4172 7/02/2013 Janet Robinson clear visual indicator of when a course is completed on the schedules page
        If Not dtAvailable Is Nothing Then
            For i As Integer = 0 To dtAvailable.Rows.Count - 1
                isCourseCompleted = CType(dtAvailable.Rows.Item(i)("IsCourseCompleted"), Boolean)
                If isCourseCompleted = True Then
                    lbxAvailableClasses.Items(i).Attributes.Add("style", "color: red")
                End If
            Next
        End If
    End Sub
    Public Sub BuildGradedAndScheduledLbx()
        If (Not IsByProgramEnrollment) Then

            Dim fac As New TranscriptFacade
            Dim dtSched As DataTable

            Dim sIndex As Int32 = 0
            If lbxScheduledClasses.Items.Count > 0 And Page.IsPostBack Then
                If lbxScheduledClasses.SelectedIndex >= 0 Then
                    sIndex = lbxScheduledClasses.SelectedIndex
                End If

            End If


            dtSched = fac.GetGradedAndScheduledClassesForEnrollmentSP(enrollmentId, campusId)
            Session("ScheduledClasses") = dtSched

            lbxScheduledClasses.Items.Clear()

            If dtSched.Rows.Count > 0 Then
                With lbxScheduledClasses
                    .DataSource = dtSched
                    .DataTextField = "DisplayedCol"
                    .DataValueField = "TestId"
                    .DataBind()
                    Try
                        .SelectedIndex = sIndex
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        .SelectedIndex = 0
                    End Try

                End With
            End If
        End If
    End Sub
    Protected Sub UpdatePanel_Unload(ByVal sender As Object, ByVal e As EventArgs)
        RegisterUpdatePanel(DirectCast(sender, UpdatePanel))
    End Sub
    Protected Sub RegisterUpdatePanel(ByVal panel As UpdatePanel)
        Dim sType = GetType(ScriptManager)
        Dim mInfo = sType.GetMethod("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel", BindingFlags.NonPublic Or BindingFlags.Instance)
        If mInfo IsNot Nothing Then
            mInfo.Invoke(ScriptManager.GetCurrent(Page), New Object() {panel})
        End If
    End Sub
    Public Sub BuildAvailableLbx()
        If (Not IsByProgramEnrollment) Then
            Dim fac As New TranscriptFacade
            ' Dim dtAvail As DataTable

            Dim sIndex As Int32 = 0
            If lbxAvailableClasses.Items.Count > 0 And Page.IsPostBack Then
                If lbxAvailableClasses.SelectedIndex >= 0 Then
                    sIndex = lbxAvailableClasses.SelectedIndex
                End If

            End If

            DtAvail = fac.GetAvailableClassesForEnrollmentUsingStartDateSP(enrollmentId)

            lbxAvailableClasses.Items.Clear()

            Session("AvailableClasses") = DtAvail

            If DtAvail.Rows.Count > 0 Then
                With lbxAvailableClasses
                    .DataSource = DtAvail
                    .DataTextField = "DisplayedCol"
                    .DataValueField = "ClsSectionId"
                    .DataBind()
                    Try
                        .SelectedIndex = sIndex
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        .SelectedIndex = 0
                    End Try
                End With
            End If

        End If
    End Sub
    ''' <summary>
    ''' Checks if the program version uses time-clock schedules.  If so, enable the panel.
    ''' Also, it binds the students current schedule and badge number on the form
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub BindTimeClockSchedules()
        Dim Show_ibToMaintPage_Schedule As Boolean = False
        Dim stuEnrollId As String = enrollmentId
        Dim prgVerId As String = UtiltiesFacade.GetPrgVerId(stuEnrollId)
        If prgVerId = "" Then
            ' no prgverid?, just clear out the info
            ddlSchedule.Items.Clear()
            txtBadgeNumber.Text = ""
            ibToMaintPage_Schedule.Visible = False
            Return
        End If

        Show_ibToMaintPage_Schedule = SchedulesFacade.IsPrgVerUsingTimeClockSchedule(prgVerId)
        'pnlSchedule.Visible = Show_ibToMaintPage_Schedule
        If Show_ibToMaintPage_Schedule Then
            ' update the drop down with a list of schedules for the current program version
            ddlSchedule.DataSource = SchedulesFacade.GetSchedules(prgVerId, True, False)
            ddlSchedule.DataTextField = "Descrip"
            ddlSchedule.DataValueField = "ID"
            ddlSchedule.DataBind()
            ddlSchedule.Items.Insert(0, New ListItem("---Select---", ""))
            ddlSchedule.SelectedIndex = 0

            ' Retrieve the student's active schedule info and bind it to the form
            Dim info As StudentScheduleInfo
            info = SchedulesFacade.GetActiveStudentScheduleInfo(stuEnrollId)
            Dim badgeNumber As String
            badgeNumber = SchedulesFacade.GetBadgeNumberforStudentEnrollment(stuEnrollId)

            txtBadgeNumber.Text = badgeNumber
            Try
                ddlSchedule.SelectedValue = info.ScheduleId

                ' add javascript to show the schedule maintenance page
                Dim parentId As String = prgVerId
                Dim objId As String = info.ScheduleId
                Dim url As String = String.Format("../MaintPopup.aspx?mod={0}&resid=511&cmpid={1}&pid={2}&objid={3}&ascx={4}",
                                                  Session("mod"), campusId, parentId, objId, "~/AR/IMaint_SetupSchedule.ascx")
                Dim js As String = GetJavsScriptPopup(url, 900, 600, Nothing)
                ibToMaintPage_Schedule.Visible = True
                ibToMaintPage_Schedule.Attributes.Add("onclick", js)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        Else
            ddlSchedule.Items.Clear()
            ddlSchedule.Items.Insert(0, New ListItem("---Select---", ""))
            ddlSchedule.SelectedIndex = 0
            ibToMaintPage_Schedule.Visible = False
        End If
    End Sub


    <System.Web.Services.WebMethod()>
    Public Sub HideWindow(variable As String)
        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False
    End Sub

    Protected Sub ShowScheduleConflict(ByVal clsSectId As String, ByVal stuEnrollId As String, ByVal studentId As String, ByVal classes As String, Optional ByVal buttonState As String = "")
        Dim strUrl As String = "ViewScheduleConflicts.aspx?resid=909&mod=AR" + "&ClsSectionId=" + clsSectId.ToString + "&StuEnrollId=" + stuEnrollId + "&Student=" + studentId + "&buttonState=" + buttonState + "&Classes=" + Server.UrlEncode(classes)
        ScheduleConflictWindow.Visible = True
        AppRelativeTemplateSourceDirectory = "~/AR/"
        ScheduleConflictWindow.Windows(0).NavigateUrl = strUrl
        Dim script As String = "function f(){$find(""" + DialogWindow.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
    End Sub
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim msg As String
        ' Dim dtAvail As DataTable
        Dim dtSched As DataTable
        Dim dr As DataRow
        Dim rows() As DataRow
        Dim index As Integer

        Try

            dtSched = CType(Session("ScheduledClasses"), DataTable)
            DtAvail = CType(Session("AvailableClasses"), DataTable)

            If lbxAvailableClasses.Items.Count = 0 Or lbxAvailableClasses.SelectedIndex = -1 Then
                'If there are no items selected in the available list display message
                DisplayErrorMessage("Please select a class in the available list to add.")
            Else
                If CanClassBeAdded() Then
                    rows = DtAvail.Select("ClsSectionId = '" & lbxAvailableClasses.SelectedValue & "'")

                    dr = dtSched.NewRow()
                    dr("TestId") = rows(0)("ClsSectionId")
                    dr("ShortStartDate") = rows(0)("ShortStartDate")
                    dr("EndDate") = rows(0)("EndDate")
                    dr("Descrip") = rows(0)("Descrip")
                    dr("ReqId") = rows(0)("ReqId")
                    dr("ClsSection") = rows(0)("ClsSection")
                    dr("ShortTermStartDate") = rows(0)("ShortTermStartDate")
                    dr("Code") = rows(0)("Code")


                    Dim sb As StringBuilder = New StringBuilder()


                    Dim stEnrId = enrollmentId
                    Dim clsSectId = rows(0)("ClsSectionId").ToString()

                    If New RegFacade().GetTransfersForStudent_SP(stEnrId, clsSectId) = True Then
                        Dim sb2 As StringBuilder = New StringBuilder()
                        sb2.Append("This student has at least one enrollment with a transfer grade for this course (" & rows(0)("Descrip").ToString() & ").")
                        sb2.Append("If you want to apply that transfer grade to the existing student enrollment, navigate to Academics/Common Tasks/Grade Posting/Record Transfer Grades for Student" & vbCrLf)
                        DisplayErrorMessage(sb2.ToString())
                    End If


                    If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
                        Dim dsScheduleConflicts As DataSet
                        dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(rows(0)("ClsSectionId").ToString, enrollmentId)
                        If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                            ShowScheduleConflict(lbxAvailableClasses.SelectedValue, enrollmentId, "", CType(rows(0)("Descrip"), String)) 'Show the radWindow
                            Exit Sub
                        End If
                    End If
                    If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        Dim ccrErrorMessage As String = CheckIfStudentHasMetCcrSpeedRequirements(enrollmentId, rows(0)("ReqId").ToString, CType(rows(0)("Descrip"), String))
                        If Not ccrErrorMessage.Trim = "" Then
                            DisplayErrorMessage(ccrErrorMessage)
                            Exit Sub
                        End If
                    End If

                    dtSched.Rows.Add(dr)

                    For Each dr In dtSched.Rows
                        If dr.RowState <> DataRowState.Deleted Then
                            dr("ShortStartDate") = dr("ShortStartDate").ToString
                            dr("ShortTermStartDate") = dr("ShortTermStartDate").ToString
                        End If
                    Next

                    dtSched.Columns("DisplayedCol").Expression = "' (' + Code + ')' + Descrip + ' (' + ShortStartDate + ')' + ' (' + ClsSection + ')' + ' (' + IsNull(Grade,'Not Graded') + ')' "

                    Session("ScheduledClasses") = dtSched

                    'We need to display the classes in start date order so we will create a DataView
                    'and bind it to the scheduled classes lbx
                    Dim dv As New DataView(dtSched, "", "ShortStartDate ASC", DataViewRowState.CurrentRows)
                    With lbxScheduledClasses
                        .DataSource = dv
                        .DataTextField = "DisplayedCol"
                        .DataValueField = "TestId"
                        .DataBind()
                    End With

                    'Remove the associated row from the dtAvail DataTable
                    'We won't need to rebind the lbxAvailableClasses control since the items will
                    'still be in order on a removal
                    DtAvail.Rows.Remove(rows(0))
                    Session("AvailableClasses") = DtAvail

                    'Remove the class from the available list
                    index = lbxAvailableClasses.SelectedIndex
                    lbxAvailableClasses.Items.RemoveAt(index)


                    BoolAction = True
                    SaveScheduledClasses()
                Else

                    msg = "This class cannot be added because the course associated with it " & vbCr
                    msg += "has already been scheduled or prereqs not met."

                    DisplayErrorMessage(msg)
                End If

            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage("ex.Message")
        End Try
    End Sub

    Private Sub SaveScheduledClasses()
        Dim dtSched As DataTable
        Dim dtChanges As DataTable
        Dim dr As DataRow
        Dim facTranscript As New TranscriptFacade

        dtSched = CType(Session("ScheduledClasses"), DataTable)

        If Not dtSched Is Nothing Then
            dtChanges = dtSched.GetChanges

            If Not dtChanges Is Nothing Then
                If dtChanges.Rows.Count > 0 Then
                    For Each dr In dtChanges.Rows
                        Dim iStuEnrollID = enrollmentId
                        Dim clsSectId = If(dr.RowState = DataRowState.Deleted, dr("TestId", DataRowVersion.Original).ToString, dr("TestId").ToString)
                        'When create the ArRegisterClass the object get all OnSchool Enrollment of the Student (get student from enrollment)
                        Dim reg As ArRegisterClass = New ArRegisterClass(iStuEnrollID, clsSectId)
                        reg.UserName = AdvantageSession.UserName
                        reg.CampusId = campusId

                        If dr.RowState = DataRowState.Deleted Then
                            If (reg.ValidateRegistrationDelete()) Then
                                reg.UnRegisteredClassWidthEnrollments()
                                RegisterOnLine(reg)
                            End If
                        ElseIf dr.RowState = DataRowState.Added Then

                            ' If all enrollments were registered skip the registration
                            If (reg.ValidateRegistrationAdd()) Then

                                ' No all enrollments were registered, then proceed to register. The object know what are not registered if any
                                reg.RegisterClassWithEnrollments()
                                RegisterOnLine(reg)
                            End If
                        End If
                    Next

                    dtSched.AcceptChanges()
                    Session("ScheduledClasses") = dtSched
                    DisplayInfoMessage("Changes have been saved successfully.")

                    'We need to recalculate the expected grad date of the student
                    If MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) = "Rotational" Then
                        facTranscript.UpdateExpectedGradDateUsingRotationalSchedule(enrollmentId)
                    End If
                End If
            End If
        End If

        ' 2/1/07 ThinkTron - save Time Clock schedule

    End Sub

    Private Sub RegisterOnLine(ByVal reg As ArRegisterClass)
        Dim onlinefac As OnLineFacade = New OnLineFacade()

        For Each info As RegistrationClassInfo In reg.ValidatedEnrollmentClassRegistrationList
            If OnLineLib.IsOnLineClassSection(info.ClassId) Then
                Dim onLineInfo As OnLineInfo = onlinefac.GetOnLineInfoByStuEnrollId(info.StuEnrollmentId)
                OnLineLib.UpdateStudentOnLineEnrollment(onLineInfo, info.UserName)
            End If
        Next
    End Sub

    Private Sub SaveScheduleandBadgeNumber()
        ' 2/1/07 ThinkTron - save Time Clock schedule
        'If the configuration setting is Time Clock then the schedule and the badge ID should both be required.
        'If the Class Is Not time clock Then neither Of the items are required To save the page
        If MyAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToString.ToLower = "yes" Then
            If (txtBadgeNumber.Text = "" Or ddlSchedule.SelectedValue = "") Then
                DisplayErrorMessage("Badge number and schedule are required when classes use a time clock.")
                Exit Sub
            End If
        End If

        Dim stuEnrollId As String = enrollmentId
        If txtBadgeNumber.Text.Length > 0 Then
            If Not IsNumeric(txtBadgeNumber.Text) Then
                DisplayErrorMessage("The badge number should be numeric.")
                Exit Sub
            End If

        End If

        If txtBadgeNumber.Text.Length > 0 AndAlso SchedulesFacade.IsBadgeNumberInUse(txtBadgeNumber.Text, stuEnrollId,campusId) Then
            DisplayErrorMessage("The entered badge number is currently used by another student.  Please choose another badge number.")
            Exit Sub
        End If

        If stuEnrollId <> "" Then

            Dim schedInfo As New StudentScheduleInfo()

            schedInfo.ScheduleId = ddlSchedule.SelectedValue
            If String.IsNullOrEmpty(ddlSchedule.SelectedValue) Then
                schedInfo.ScheduleId = Nothing
            End If

            schedInfo.StuEnrollId = stuEnrollId

            schedInfo.BadgeNumber = txtBadgeNumber.Text
            If String.IsNullOrEmpty(txtBadgeNumber.Text) Then
                schedInfo.BadgeNumber = Nothing
            End If

            schedInfo.Active = True
            schedInfo.ModUser = AdvantageSession.UserName

            If SchedulesFacade.AssignStudentScheduleFromSchedulePage(schedInfo, CType(Session("UserName"), String)) <> 0 Then
                DisplayErrorMessage("An error occurred while attempting to save schedule!")
            End If

        End If

    End Sub
    Protected Sub btnSaveSchedule_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveSchedule.Click

        SaveScheduleandBadgeNumber()

    End Sub
    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click
        Dim dtSched As DataTable
        ' Dim dtAvail As DataTable
        Dim dr As DataRow
        Dim rows() As DataRow
        Dim index As Integer
        Dim clsRegStd As New RegStdManuallyFacade
        Dim bReturn As Boolean

        ScheduleConflictWindow.Windows(0).VisibleOnPageLoad = False

        dtSched = CType(Session("ScheduledClasses"), DataTable)
        DtAvail = CType(Session("AvailableClasses"), DataTable)

        If lbxScheduledClasses.Items.Count = 0 Or lbxScheduledClasses.SelectedIndex = -1 Then
            'If there are no items selected in the available list display message
            DisplayErrorMessage("Please select a class in the selected list to remove.")

        Else
            If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                Dim strReturnMessage As String
                'Dim strCourseGuid As String = ""
                strReturnMessage = (New ExamsFacade).UpdateScoreForComponent(enrollmentId, lbxScheduledClasses.SelectedValue, CType(Session("User"), String))
                If Not strReturnMessage = "" Then
                    DisplayErrorMessage(strReturnMessage)
                    Exit Sub
                End If
            End If

            If CanClassBeRemoved() Then

                rows = dtSched.Select("TestId = '" & lbxScheduledClasses.SelectedValue & "'")

                dr = DtAvail.NewRow()
                dr("ClsSectionId") = rows(0)("TestId")
                dr("ShortStartDate") = rows(0)("ShortStartDate")
                dr("EndDate") = rows(0)("EndDate")
                dr("Descrip") = rows(0)("Descrip")
                dr("ReqId") = rows(0)("ReqId")
                dr("ClsSection") = rows(0)("ClsSection")
                dr("ShortTermStartDate") = rows(0)("ShortTermStartDate")
                dr("Code") = rows(0)("Code")
                bReturn = CType(clsRegStd.IsCourseCompleted(enrollmentId, dr("ClsSectionId").ToString()), Boolean)
                dr("IsCourseCompleted") = bReturn
                DtAvail.Rows.Add(dr)


                For Each dr In DtAvail.Rows
                    If dr.RowState <> DataRowState.Deleted Then
                        dr("ShortStartDate") = dr("ShortStartDate").ToString
                        dr("ShortTermStartDate") = dr("ShortTermStartDate").ToString
                    End If

                Next

                DtAvail.Columns("DisplayedCol").Expression = " ' (' + Code + ')' + Descrip + ' (' + ShortTermStartDate + ')' + ' (' + ShortStartDate + ')' + ' (' + ClsSection + ')' "

                Session("AvailableClasses") = DtAvail

                'We need to display the classes in start date order so we will create a DataView
                'and bind it to the available classes lbx
                Dim dv As New DataView(DtAvail, "", "ShortStartDate ASC", DataViewRowState.CurrentRows)
                With lbxAvailableClasses
                    .DataSource = dv
                    .DataTextField = "DisplayedCol"
                    .DataValueField = "ClsSectionId"
                    .DataBind()
                End With

                'Remove the associated row from the dtSched DataTable
                'We won't need to rebind the lbxScheduledClasses control since the items will
                'still be in order on a removal
                If rows(0).RowState <> DataRowState.Added Then
                    'Mark the row as deleted
                    rows(0).Delete()
                Else
                    dtSched.Rows.Remove(rows(0))
                End If

                Session("ScheduledClasses") = dtSched

                'Remove the class from the scheduled list
                index = lbxScheduledClasses.SelectedIndex
                lbxScheduledClasses.Items.RemoveAt(index)

                BoolAction = False
                SaveScheduledClasses()

            Else
                DisplayErrorMessage("This class cannot be removed because it is already graded.")
            End If
        End If
    End Sub

    Private Function CanClassBeAdded() As Boolean
        'We can add available class if the student has not been scheduled for that course 
        ' 11/7/2011 Janet Robinson Clock Hr. Project - added term as part of the check
        '   OR
        'The student has been scheduled for that course with a failing grade
        ' Dim dtAvail As DataTable
        Dim facade As New RegFacade
        Dim rows() As DataRow
        Dim reqId As String
        Dim termId As String
        ' Dim overRidePreReqs As Boolean = False
        Dim metPrereqs As Boolean
        DtAvail = CType(Session("AvailableClasses"), DataTable)
        Dim prgVerId As String = UtiltiesFacade.GetPrgVerId(enrollmentId)
        'Get the ReqId associated with the class selected in the available list.
        rows = DtAvail.Select("ClsSectionId = '" & lbxAvailableClasses.SelectedValue & "'")
        reqId = rows(0)("ReqId").ToString()
        termId = rows(0)("TermId").ToString()
        'Check if this class has been already scheduled.
        If HasCourseAlreadyBeenScheduled(reqId, termId) Then
            Return False
        Else
            If facade.DoesStdHaveOverride(enrollmentId, lbxAvailableClasses.SelectedValue) Then Return True
            metPrereqs = facade.HasStudMetPrereqs(enrollmentId, lbxAvailableClasses.SelectedValue, prgVerId, campusId)
            Return metPrereqs
        End If
    End Function
    Private Function CanClassBeRemoved() As Boolean
        'We can remove a class if it has a null grade
        Dim dtSched As DataTable
        Dim rows() As DataRow
        dtSched = CType(Session("ScheduledClasses"), DataTable)
        rows = dtSched.Select("TestId = '" & lbxScheduledClasses.SelectedValue & "'")
        If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
            If rows(0)("Grade").ToString() = "0" Or rows(0)("Grade").ToString() = "" Or rows(0)("Grade").ToString() = "0.00" Then
                Return True
            Else
                Return False
            End If
        End If
        If rows(0)("Grade").ToString() = "" Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function HasCourseAlreadyBeenScheduled(ByVal reqId As String, ByVal termId As String) As Boolean
        'Loop through each class in the selected list and check if the associated course
        'matches the one passed in. We can exit the loop as soon as we find a match
        Dim rows() As DataRow
        Dim sReqId As String
        Dim sTermId As String
        Dim isPass As String
        Dim allowCompletedCourseRetake As Boolean
        'Dim score As Decimal = 0
        Dim dtSched As DataTable
        'Dim passingScore As Decimal = (New StuProgressReportObject).GetMinPassingScore
        dtSched = CType(Session("ScheduledClasses"), DataTable)


        For i As Integer = 0 To lbxScheduledClasses.Items.Count - 1
            'Get the ReqId associated with the class
            rows = dtSched.Select("TestId = '" & lbxScheduledClasses.Items(i).Value & "'")
            sReqId = rows(0)("ReqId").ToString()
            isPass = rows(0)("IsPass").ToString
            sTermId = rows(0)("TermId").ToString

            '6/17/2013 Janet Robinson US4112 Add logic to check AllowCompletedCourseRetake flag
            ' This code allows course retakes in current and future terms
            If sReqId.ToString = reqId AndAlso sTermId.ToString = termId Then
                'Check if the course has a passing or a null grade
                allowCompletedCourseRetake = CType(rows(0)("AllowCompletedCourseRetake"), Boolean)
                ' null - no grade scheduled but not graded
                If rows(0)("Grade").ToString = "" Then
                    Return True
                Else
                    ' has grade completed course - if student has failing grade then course can be retaken regardless of the AllowCompletedCourseRetake flag
                    If allowCompletedCourseRetake = True Then
                        Return False
                    Else
                        If isPass = "0" Then
                            Return False
                        Else
                            Return True
                        End If
                    End If
                End If


            End If
        Next
        Return False
    End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub DisplayInfoMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayInfoInMessageBox(Page, errorMessage)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        MarkCompletedCoursesInRed(DtAvail)

        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnRemove)

        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        'BindToolTip()


    End Sub


    ''' <summary>
    ''' Open up the schedules maintenance page when the user clicks the image
    ''' next to the schedules drop-down
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ibToMaintPage_Schedule_Click(ByVal sender As Object, ByVal e As EventArgs ) Handles ibToMaintPage_Schedule.Click
        BindTimeClockSchedules()
    End Sub

    Private Function CheckIfStudentHasMetCcrSpeedRequirements(ByVal stuEnrollId As String,
                                                              ByVal courseId As String,
                                                              ByVal descrip As String) As String
        Dim boolIsStudentEligibleForRegisteration As Boolean
        Dim ccrErrorMessage As String = ""
        boolIsStudentEligibleForRegisteration = (New RegisterStudentsBR).CCR_CheckIfStudentMetRequirements(stuEnrollId, courseId)

        If boolIsStudentEligibleForRegisteration = False Then
            ccrErrorMessage &= "Student cannot be registered in course " & descrip & ", as thelbxScheduledClasses Student has not satisfied " & vbCrLf
            ccrErrorMessage &= "the prerequisites, mentor/proctored test requirements, the student has exceeded the minimum course repetition limit (" & MyAdvAppSettings.AppSettings("CCR_RepeatSHCourses").ToString.Trim & " times)" & vbCrLf
            ccrErrorMessage &= "set for SH Level courses or the student failed the prerequisite course."
            Return ccrErrorMessage
        Else
            Return ""
        End If
    End Function
End Class
