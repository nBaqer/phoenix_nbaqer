﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentSchedule.ascx.vb" Inherits="StudentSchedule" ClassName="StudentSchedule" %>
<script type="text/javascript">
    function OnClientclose(sender, eventArgs) {
        __doPostBack('ClosedRadWindow');
    }
</script>

<telerik:RadWindowManager ID="ScheduleConflictWindow" runat="server" Behaviors="Default" InitialBehaviors="None" Visible="false">
    <Windows>
        <telerik:RadWindow ID="DialogWindow" Behaviors="Close" VisibleStatusbar="false"
            ReloadOnShow="true" Modal="true" runat="server" Height="400px" Width="700px"
            Top="20" Left="20">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
<asp:Panel runat="server">

    <table class="maincontenttable" align="center">
        <tr>
            <td class="detailsframe">
                <div class="scrollsingleframe">
                    <!-- begin table content-->

                    <!-- begin clock hour schedule -->
                    <asp:Panel ID="pnlSchedule" runat="server">
                        <h3>Enrollment Schedule Details</h3>
                        <table style="width: 98%;" align="center">
                            <tr>
                                <td>
                                    <div>
                                        <table class="contenttable">
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblSchedulePrompt" CssClass="label" runat="server">Schedule <span class="font-red">*</span> </asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlSchedule" runat="server" Width="200px" />

                                                    <asp:LinkButton ID="ibToMaintPage_Schedule" runat="server">
                                                        <span class="k-icon k-i-edit"></span>
                                                    </asp:LinkButton>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblBadgeNumPrompt" CssClass="label" runat="server">Badge Number <span class="font-red">*</span></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBadgeNumber" runat="server" CssClass="textbox" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="align-right" style="margin-top: 10px;">
                                            <asp:Button ID="btnSaveSchedule" runat="server" Visible="true" Text="Save Schedule and Badge Number" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </asp:Panel>
                    <!-- end clock hour schedule -->

                    <table class="contenttable" style="width: 100%;" align="center" runat="server" id="scheduleTable">
                        <tr>
                            <td colspan="3">
                                <h3>Enrollment Class Assgiments.</h3>
                            </td>
                        </tr>
                        <tr>
                            <td class="threecolumnheaderreg">
                                <asp:Label ID="lblAvailStuds" CssClass="labelbold" runat="server">Available Classes</asp:Label>
                            </td>
                            <td class="threecolumnspacerreg"></td>
                            <td class="threecolumnheaderreg">
                                <asp:Label ID="lblSelStuds" CssClass="labelbold" runat="server">Graded and Scheduled Classes</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="threecolumncontentreg" style="text-align: center">
                                <asp:ListBox ID="lbxAvailableClasses" runat="server" Width="100%" Height="100%" Rows="30"></asp:ListBox>
                            </td>
                            <td class="threecolumnbuttonsreg" style="vertical-align: top;">
                                <telerik:RadButton ID="btnAdd" CssClass="rfdPrimaryButton" Text="Add >" Enabled="True" runat="server" Width="100px" Style="margin-bottom: 10px;" />
                                <br />
                                <telerik:RadButton ID="btnRemove" CssClass="buttons1" Text="< Remove" Enabled="True" runat="server" Width="100px" />
                            </td>
                            <td class="threecolumncontentreg" style="text-align: center">
                                <asp:ListBox ID="lbxScheduledClasses" runat="server" Width="100%" Height="100%" Rows="30" />
                            </td>
                        </tr>
                    </table>

                    <asp:TextBox ID="txtStudentId" runat="server" Visible="false" />
                    <asp:TextBox ID="txtStEmploymentId" runat="server" Visible="false" />
                    <asp:CheckBox ID="ChkIsInDB" runat="server" Checked="False" Visible="false" />
                    <asp:TextBox ID="txtStudentDocs" runat="server" Visible="False" />
                    <!--end table content-->
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
