﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="footer.ascx.vb" Inherits="usercontrols_footer" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy>
<div style="width:100%">
    <telerik:RadToolBar ID="FooterToolBar" runat="server" dir="rtl" Width="100%" Height="30px">
        <Items>
            <telerik:RadToolBarButton runat="server" Enabled="False" 
                Text="Copyright FAME 2005 - 2010. All rights reserved" 
                Font-Size="XX-Small">
            </telerik:RadToolBarButton>
        </Items>
    </telerik:RadToolBar>
</div>
