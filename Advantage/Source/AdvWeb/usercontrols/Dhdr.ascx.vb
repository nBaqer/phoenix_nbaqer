﻿Imports Telerik.Web.UI

Partial Class usercontrols_Dhdr
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ItemsPerRequest = 10
        Dim mylayout As String = Page.Request.Params("layout")
        Select Case mylayout
            Case "start"
                'MyWonderBar.Visible = False
                SchoolLogo.Visible = True
            Case "start2"
                ' MyWonderBar.Visible = False
                SchoolLogo.Visible = True

            Case "student"
                ' MyWonderBar.Visible = False
                SchoolLogo.Visible = True
                CType(AdvToolBar.Controls(0).FindControl("bread"), Image).ImageUrl = "images\action_add.png"
                CType(AdvToolBar.Controls(0).FindControl("bc1"), Literal).Text = "Academics /"
                CType(AdvToolBar.Controls(0).FindControl("bc2"), HyperLink).Text = "Info"

            Case "students"
                ' MyWonderBar.Visible = False
                SchoolLogo.Visible = True
                CType(AdvToolBar.Controls(0).FindControl("bread"), Image).ImageUrl = "images\action_add.png"
                CType(AdvToolBar.Controls(0).FindControl("bc1"), Literal).Text = "Academics /"
                CType(AdvToolBar.Controls(0).FindControl("bc2"), HyperLink).Text = "Info"
            Case "none"
                ' MyWonderBar.Visible = False
                SchoolLogo.Visible = True

            Case Else

        End Select
        If Page.Title = "Student Info" Then
            SchoolLogo.Visible = True
            CType(AdvToolBar.Controls(0).FindControl("bread"), Image).ImageUrl = "images\action_add.png"
            CType(AdvToolBar.Controls(0).FindControl("bc1"), Literal).Text = "Academics /"
            CType(AdvToolBar.Controls(0).FindControl("bc2"), HyperLink).Text = "Student Info"
        End If

    End Sub

    Protected Sub AdvToolBar_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        If e.Item.Index = 1 Then
            If e.Item.ToolTip = "Search Students" Then
                e.Item.ImageUrl = "images/gear1.png"
                e.Item.ToolTip = "Search Leads"
            Else
                e.Item.ImageUrl = "images/hr2.png"
                e.Item.ToolTip = "Search Students"
            End If

        End If

    End Sub
#Region "Search Control"
    Private _ItemsPerRequest As Integer
    'Public _FullName As String
    Public Property ItemsPerRequest() As Integer
        Get
            Return _ItemsPerRequest
        End Get
        Set(ByVal Value As Integer)
            _ItemsPerRequest = Value
        End Set
    End Property
    Protected Function FormatSSN(ByVal SSN As String) As String
        If Not SSN = String.Empty Then
            Return Mid(SSN, 1, 3) & "-" & Mid(SSN, 4, 2) & "-" & Mid(SSN, 6, 4)
            'Return "*****" & Mid(SSN, 6, 4)
        Else
            Return String.Empty
        End If
    End Function
    Private Function PadDefaultDisplayData(ByVal strData As String) As String
        If String.IsNullOrWhiteSpace(strData) = True Then
            strData = "[NO DATA]"
        End If
        Return strData
    End Function
    Private Shared Function GetStatusMessage(ByVal offset As Integer, ByVal total As Integer) As String
        If total <= 0 Then
            Return "No student matches"
        End If

        Return [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total)
    End Function
    'Private Function QueryStudents(ByVal SearchText As String) As Object 'IQueryable(Of NewStudentSearch)

    '    'Dim DA As New StudentEnrollmentDA(CStr(SingletonAppSettings.AppSettings("ConnectionString")))

    '    'Try
    '    '    Dim UserId = New Guid(Session("UserId").ToString())
    '    '    Dim Users(1) As String
    '    '    Users(0) = UserId.ToString
    '    '    Query = DA.SearchStudents()
    '    '    Dim MyStudents As IQueryable(Of NewStudentSearch)
    '    '    If String.IsNullOrWhiteSpace(SearchText) = True Then
    '    '        MyStudents = From s In Query Where s.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By s.LastName.Trim, s.FirstName.Trim
    '    '    Else
    '    '        Dim StudentIdentifier As String = SingletonAppSettings.AppSettings("StudentIdentifier").ToStringhttp://localhost/EntityTest2/usercontrols/StudentDisclosure.ascx.vb
    '    '        If StudentIdentifier = "SSN" Then
    '    '            MyStudents = From s In Query Where (s.FullName.Contains(SearchText) Or s.SSN.Contains(SearchText)) And s.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By s.LastName.Trim, s.FirstName.Trim
    '    '        ElseIf StudentIdentifier = "EnrollmentId" Then
    '    '            MyStudents = From s In Query Where (s.FullName.Contains(SearchText) Or s.EnrollmentId.Contains(SearchText)) And s.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By s.LastName.Trim, s.FirstName.Trim
    '    '        Else
    '    '            MyStudents = From s In Query Where (s.FullName.Contains(SearchText) Or s.StudentNumber.Contains(SearchText)) And s.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By s.LastName.Trim, s.FirstName.Trim
    '    '        End If

    '    '    End If

    '    '    Return MyStudents

    '    'Catch ex As Exception
     '    '	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    '	exTracker.TrackExceptionWrapper(ex)

    '    '    Throw ex

    '    'End Try

    'End Function
    Protected Sub DDLStudentSearch_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)
        'Dim ctrl As RadComboBox = CType(sender, RadComboBox)
        'ctrl.
        'Dim MyStudents = QueryStudents(e.Text)
        'Dim itemOffset As Integer = e.NumberOfItems
        ''Dim endOffset As Integer = Math.Min(itemOffset + ItemsPerRequest, MyStudents.Count)
        ''e.EndOfItems = endOffset = MyStudents.Count

        'Dim MyStudentsDisplay = (From s In MyStudents
        '                   Select New NewStudentSearch With {.FirstName = s.FirstName, .LastName = s.LastName, .FullName = s.FullName, .SSN = s.SSN, .StudentId = s.StudentId}).Distinct

        'Dim MyStudentsDisplaySorted = (From s In MyStudentsDisplay
        '                   Order By s.LastName.Trim Ascending, s.FirstName.Trim Ascending)

        'Dim endOffset As Integer = Math.Min(itemOffset + ItemsPerRequest, MyStudentsDisplaySorted.Count)
        'e.EndOfItems = endOffset = MyStudentsDisplaySorted.Count


        'Dim StudentList As List(Of NewStudentSearch) = MyStudentsDisplaySorted.ToList
        'Dim DisplayedStudents As New List(Of NewStudentSearch)

        'For i As Integer = itemOffset To endOffset - 1
        '    DisplayedStudents.Add(StudentList(i))
        'Next
        'DDLStudentSearch.DataSource = DisplayedStudents
        'DDLStudentSearch.DataTextField = "FullName"
        ''DDLStudentSearch.DataValueField = "StuEnrollId"
        'DDLStudentSearch.DataValueField = "StudentId"
        'DDLStudentSearch.DataBind()

        'e.Message = GetStatusMessage(endOffset, MyStudents.Count)
    End Sub
    Protected Sub DDLStudentSearch_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)

        '    Dim Student As NewStudentSearch = DirectCast(e.Item.DataItem, NewStudentSearch)
        '    'Dim Student As StudentEnrollmentSearch = DirectCast(e.Item.DataItem, StudentEnrollmentSearch)
        '    e.Item.Text = Student.FullName
        '    e.Item.Value = Student.StudentId.ToString
        '    Dim NameLabel As Label = CType(e.Item.FindControl("lblFullName"), Label)
        '    Dim SSNLabel As Label = CType(e.Item.FindControl("lblSSN"), Label)
        '    'Dim PrgVerLabel As Label = CType(e.Item.FindControl("lblPrgDescrip"), Label)
        '    'Dim StatusLabel As Label = CType(e.Item.FindControl("lblStatusCodeDescrip"), Label)

        '    NameLabel.Text = PadDefaultDisplayData(Student.FullName)
        '    SSNLabel.Text = PadDefaultDisplayData(FormatSSN(Student.SSN))

        '    'PrgVerLabel.Text = PadDefaultDisplayData(Student.PrgVerDescrip)
        '    'StatusLabel.Text = PadDefaultDisplayData(Student.StatusCodeDescrip)
    End Sub
    Protected Sub DDLStudentSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        'stuid = e.Value
        'BuildDropDowns(e.Value)
        'ddlEnrollments.Enabled = True
        'ddlTerms.Enabled = True
        'ddlAcademicYears.Enabled = True

    End Sub
#End Region
End Class
