﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentGroupDropDown.ascx.vb" Inherits="usercontrols_StudentGroupDropDown" %>

<div id="StudentGroupSelector" class="CaptionSelector MultiFilterReportContainer" runat="server" style="padding-left: 2px;">
    <div id="StudentGroupHeader" class="CaptionLabel" runat="server">Student Group</div>
    <telerik:RadComboBox ID="RadComboStudentGroup" runat="server" AutoPostBack="true" Width="350px" tooltip="Select a student group to run the report"  CssClass="ManualInput ReportLeftMarginInput">
    </telerik:RadComboBox>
</div>
