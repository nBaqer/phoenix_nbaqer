﻿Imports Telerik.Web.UI

Partial Class usercontrols_widgets_WidgetEventCalendar
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init


    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim i As Integer = 0
        Dim Holiday As Date = New DateTime(2011, 12, 23)
        Do While i < 14
            Dim h As New RadCalendarDay()
            h.Date = Holiday.AddDays(i)
            h.ToolTip = "Winter Break"
            h.ItemStyle.CssClass = "rcSelected"
            RadCalendar1.SpecialDays.Add(h)
            i = i + 1
        Loop
        i = 0
        Holiday = New DateTime(2012, 6, 25)
        Do While i < 14
            Dim h As New RadCalendarDay()
            h.Date = Holiday.AddDays(i)
            h.ToolTip = "Summer Break"
            h.ItemStyle.CssClass = "rcSelected"
            RadCalendar1.SpecialDays.Add(h)
            i = i + 1
        Loop
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

    End Sub
End Class
