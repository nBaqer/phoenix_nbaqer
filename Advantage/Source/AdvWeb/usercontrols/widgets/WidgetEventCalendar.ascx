﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WidgetEventCalendar.ascx.vb" Inherits="usercontrols_widgets_WidgetEventCalendar" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" LoadingPanelID="RadAjaxLoadingPanelWidgetEventCalendar"/>
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel 
      ID="RadAjaxLoadingPanelWidgetEventCalendar" Runat="server">
  </telerik:RadAjaxLoadingPanel>
   <asp:Panel ID="MainPanel" runat="server">
        <div style="margin:10px;">
            <div style="float:left;margin-right:5px;">Show holidays for the </div>
            <div style="float:left;margin-right:5px;">
            <telerik:RadComboBox ID="ddlCampusGroup" runat="server">

                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="All" Value="All" />
                    <telerik:RadComboBoxItem runat="server" Text="Green Street Campus" 
                        Value="Green Street Campus" />
                    <telerik:RadComboBoxItem runat="server" Text="Loop Campus" 
                        Value="Loop Campus" />
                </Items>
            </telerik:RadComboBox>
            </div>
             <div style="float:left;"> Campus Group</div>
             <div style="clear:both;"></div>
        </div>
       <telerik:RadCalendar ID="RadCalendar1" runat="server" AutoPostBack="true" MultiViewColumns="2"
        MultiViewRows="3" EnableMultiSelect="false"  ShowOtherMonthsDays="false" PresentationType="Preview">
                <SpecialDays>
            <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-CssClass="rcToday" ToolTip="Today" />
             <telerik:RadCalendarDay ItemStyle-CssClass="rcSelected"  />
        </SpecialDays>
       </telerik:RadCalendar>
   </asp:Panel>