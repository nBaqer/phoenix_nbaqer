<%@ Control Language="vb" AutoEventWireup="false" Inherits="EmailEditor" CodeFile="EmailEditor.ascx.vb" %>
<HTML>
	<asp:panel id="pnlMaileditor" HorizontalAlign="Center" runat="server">
		<TABLE class="contenttableemail" cellSpacing="0" cellPadding="2" width="100%" align="center">
			<TR>
				<TD class="contentcellemail" noWrap align="right">
					<asp:Label id="lblFrom" runat="server" CssClass="Label">From</asp:Label></TD>
				<TD class="contentcell4email">
					<asp:TextBox id="txtFrom" runat="server" CssClass="TextBox"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD class="contentcellemail" noWrap align="right">
					<asp:Label id="lblTo" runat="server" CssClass="Label">To</asp:Label></TD>
				<TD class="contentcell4email">
					<asp:TextBox id="txtTo" runat="server" CssClass="TextBox" TextMode="MultiLine" Rows="4"></asp:TextBox>
					<asp:RegularExpressionValidator id="revToAddress" runat="server" CssClass="Label" Enabled="False" ErrorMessage="Invalid 'To' Email Address"
						Display="None" ControlToValidate="txtTo" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Invalid 'To' Email Address</asp:RegularExpressionValidator></TD>
			</TR>
			<TR>
				<TD class="contentcellemail" noWrap align="right">
					<asp:Label id="lblSubject" runat="server" CssClass="Label">Subject</asp:Label></TD>
				<TD class="contentcell4email">
					<asp:TextBox id="txtSubject" runat="server" CssClass="TextBox"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD class="contentcellemail2" noWrap>&nbsp;</TD>
				<TD class="contentcell4email2">
					<asp:TextBox id="txtBody" runat="server" CssClass="TextBox" TextMode="MultiLine" Rows="16"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD class="contentcellemail" noWrap>&nbsp;</TD>
				<TD class="contentcell4email" align="center">
					<asp:Button id="btnSend" runat="server"  Text="Send"></asp:Button></TD>
			</TR>
		</TABLE>
	</asp:panel>
</HTML>
