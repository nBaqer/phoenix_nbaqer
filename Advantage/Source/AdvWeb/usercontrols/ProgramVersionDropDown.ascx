﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProgramVersionDropDown.ascx.vb" Inherits="usercontrols_ProgramVersionDropDown" %>

<div id="ProgramVersionSelector" class="ProgramVersionSelector MultiFilterReportContainer" runat="server" style="padding-left: 2px;">
    <div id="ProgramVersionHeader" class="CaptionLabel" runat="server">Program Version</div>
    <telerik:RadComboBox ID="RadComboProgramVersion" runat="server" AutoPostBack="true" Width="350px" tooltip="Select a program version to run the report"  CssClass="ManualInput ReportLeftMarginInput">
    </telerik:RadComboBox>
</div>
