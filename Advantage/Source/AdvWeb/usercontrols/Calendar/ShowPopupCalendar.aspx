<%@ Page Language="vb" AutoEventWireup="false" inherits="ShowPopupCalendar" CodeFile="ShowPopupCalendar.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>ShowPopupCalendar</title>
<meta content="Microsoft Visual Studio.NET 7.0" name=GENERATOR/>
<meta content="Visual Basic 7.0" name=CODE_LANGUAGE/>
<meta content=JavaScript name=vs_defaultClientScript/>
<meta content="http://schemas.microsoft.com/intellisense/ie5" name=vs_targetSchema/>
  </head>
<body>
<form id=Form1 method=post runat="server">
<p><asp:Label id="lblSnippetHeader" runat="server" Width="100%" Font-Names="Arial" ForeColor="MidnightBlue" Font-Size="Large">Snippet: PopUp Calendar in .NET</asp:Label></p>
<p><asp:Label id="lblVersionDesc1Hdr" runat="server" Font-Bold="True" Font-Names="Arial" BackColor="SteelBlue" ForeColor="White">Version 1: Server-Side Implementation</asp:Label></p>
<p><asp:Label id="lblVersion1Desc" runat="server" Width="660px" Font-Names="Arial" Font-Size="10pt">This version is purely a server-side implementation.  This means that there is no javascript code in the aspx page and it is mere a call to a function in your codebehind called "GetPopupDate".  The nice thing about this implementation is that you can move the function into a library and for your entire web app, you can call popup calendars with just a single line of code.  The downside to this implementation is that the screen will do a refresh when it makes the call to the server.  </asp:Label></p>
<p><asp:label id="lblDate1" runat="server" Width="55px" Font-Names="Arial">Date1:</asp:label><asp:textbox id="txtDate1" runat="server" Width="80px"></asp:textbox><asp:imagebutton id="imgPopupCal1" runat="server" ImageUrl="PopupCalendar.gif"></asp:imagebutton></p>
<p>&nbsp;</p>
<p><strong><asp:Label id="Label1" runat="server" Font-Names="Arial" BackColor="SteelBlue" ForeColor="White">Version 2: Client-side  Implementation</asp:Label></strong></p>
<p><br><asp:Label id="lblVersion2Desc" runat="server" Width="660px" Font-Names="Arial" Font-Size="10pt">This version is Client-side implementation. It uses javascript on the client-side to open a Popup Calendar web form.  The nice thing about this implementation is that it does not do the screen refresh that the server side version does.  However, it does require a bit of javascript (but not that much).</asp:Label></p>
<p><asp:label id="lblDate2"  runat="server" font-names="Arial">Date2:</asp:label>&nbsp;&nbsp; 
<asp:textbox id="txtDate2" runat="server" Width="80px"></asp:textbox><a 
onclick="window.open('PopupCalendar.aspx?textbox=txtDate2','cal','width=250,height=225,left=270,top=180')" 
href="javascript:;"><img src="PopupCalendar.gif" border=0 ></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p></form>

  </body>
</html>
