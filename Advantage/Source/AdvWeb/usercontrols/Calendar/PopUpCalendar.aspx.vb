Imports System.Diagnostics

Partial Class PopUpCalendar
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
    End Sub

    Protected Sub Change_Date(ByVal sender As Object, ByVal e As EventArgs)
        If Not Request.QueryString("textbox") Is Nothing Then
            Dim strScript As String = "<script>window.opener.document.forms(0)." + Request.QueryString("textbox").ToString + ".value = '"
            strScript += CalPopup.SelectedDate.ToString("MM/dd/yyyy")
            strScript += "';self.close()"
            strScript += "</" + "script>"
            'RegisterClientScriptBlock("Calendar_ChangeDate", strScript)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Calendar_ChangeDate", strScript, False)
        End If
    End Sub

End Class
