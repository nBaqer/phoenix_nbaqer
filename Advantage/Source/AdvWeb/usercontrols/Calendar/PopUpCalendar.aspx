<%@ Page Language="vb" AutoEventWireup="false" inherits="PopUpCalendar" CodeFile="PopUpCalendar.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>PopUp</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body>

    <form id="Form1" method="post" runat="server">

<asp:Calendar id="CalPopup" OnSelectionChanged="Change_Date" runat="server" backcolor="White" width="200px" daynameformat="FirstLetter" forecolor="Black" height="180px" font-size="8pt" font-names="Verdana" bordercolor="#999999" cellpadding="4">
<todaydaystyle forecolor="Black" backcolor="#CCCCCC">
</TodayDayStyle>

<selectorstyle backcolor="#CCCCCC">
</SelectorStyle>

<nextprevstyle verticalalign="Bottom">
</NextPrevStyle>

<dayheaderstyle font-size="7pt" font-bold="True" backcolor="#CCCCCC">
</DayHeaderStyle>

<selecteddaystyle font-bold="True" forecolor="White" backcolor="#666666">
</SelectedDayStyle>

<titlestyle font-bold="True" bordercolor="Black" backcolor="#999999">
</TitleStyle>

<weekenddaystyle backcolor="#FFFFCC">
</WeekendDayStyle>

<othermonthdaystyle forecolor="#808080">
</OtherMonthDayStyle></asp:Calendar>

    </form>

  </body>
</html>
