Imports System.Diagnostics

Partial Class ShowPopupCalendar
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

        Private Sub imgPopupCal1_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgPopupCal1.Click
                GetPopupDate(txtDate1, Page)
        End Sub

        Sub GetPopupDate(ByVal targetTextBox As TextBox, ByVal targetPage As Page)
                Dim strScript As String = "<script language=javascript>window.open('PopUpCalendar.aspx?textbox=" & targetTextBox.ID & "','cal','width=250,height=225,left=270,top=180')</script>"
                'targetPage.RegisterClientScriptBlock("GetPopupDate", strScript)
                targetPage.ClientScript.RegisterClientScriptBlock(Me.GetType(),"GetPopupDate", strScript,false )

        End Sub

End Class
