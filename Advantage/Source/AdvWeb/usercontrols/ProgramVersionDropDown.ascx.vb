﻿
Imports System.Diagnostics
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports Telerik.Web.UI

Partial Class usercontrols_ProgramVersionDropDown
    Inherits System.Web.UI.UserControl

    Protected MyAdvAppSettings As AdvAppSettings
    Public Label As String
    Public Mandatory As Boolean
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim _label = "Program Version"

        If Not String.IsNullOrEmpty(Label) Then
            _label = Label
        End If
        If Mandatory Then
            _label += " <font color=""red"">*</font>"
        End If
        ProgramVersionHeader.InnerHtml = _label
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
    Public Function GetItems() As RadComboBoxItemCollection
        Return RadComboProgramVersion.Items
    End Function
    Public Function GetSelected() As RadComboBoxItem
        Return RadComboProgramVersion.SelectedItem
    End Function
    Public Function GetId() As String
        Return RadComboProgramVersion.ID
    End Function
    Public Function GetInnerControl() As RadComboBox
        Return RadComboProgramVersion
    End Function
    Public Sub Reload(ByVal selectedCampusId As Guid, Optional ByVal progId As Nullable(Of Guid) = Nothing, Optional ByVal includeSelect As Boolean = False)
        Try
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim ds = New List(Of GenericListItem)

            If (includeSelect) Then
                ds.Insert(0, New GenericListItem() With {.Description = "Select", .Id = "-1"})
            End If
            ds.AddRange(GetProgramVersionByCampusId(selectedCampusId, True, progId) _
                           .Select(Function(x As arPrgVersion) New GenericListItem With { .Description = x.PrgVerDescrip, .Id = x.PrgVerId.ToString()} ).ToList())
            RadComboProgramVersion.DataTextField = "Description"
            RadComboProgramVersion.DataValueField = "Id"
            RadComboProgramVersion.DataSource = ds
            RadComboProgramVersion.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try

    End Sub
    Private Function GetProgramVersionByCampusId(ByVal selectedCampus As Guid, ByVal filteredBySelected As Boolean,
                                                 Optional ByVal progId As Nullable(Of Guid) = Nothing) As List(Of arPrgVersion)
        Dim da As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As List(Of arPrgVersion)
        Dim statuses = New List(Of String)() From
                {
                "Active"
                }
        Try
            Dim selectedProgramVersions As List(Of String) = Nothing
            If selectedProgramVersions Is Nothing Or filteredBySelected = False Then
                If progId Is Nothing Then
                    result = da.GetProgramVersionsOnlyClockHoursByCampusId(selectedCampus, statuses)
                Else
                    result = da.GetProgramVersionsOnlyClockHoursByCampusIdandProgId(selectedCampus, statuses, CType(progId, Guid))
                End If

            Else
                'If progId = Guid.Empty Then
                If progId Is Nothing OrElse progId = Guid.Empty Then
                    result = da.GetProgramVersionsOnlyClockHoursByCampusId(selectedCampus, statuses, selectedProgramVersions)
                Else
                    result = da.GetProgramVersionsOnlyClockHoursByCampusIdandProgramIdCollection(selectedCampus, statuses,
                                                                                                 selectedProgramVersions, CType(progId, Guid))
                End If

            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Debug.WriteLine("Error in GetProgramVersionByCampusId exception message: {0}", ex.Message)
            Throw
        End Try
    End Function
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim statusValues As New List(Of String)

        Try
            Dim CKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If CKbox.Checked = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return statusValues
    End Function

End Class
