﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentContacts.ascx.vb" Inherits="usercontrols_StudentContacts" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server"  >
    </telerik:RadAjaxManagerProxy>


   <div style="margin:10px 10px 10px 10px;">
   <h4>Addresses</h4>
                 <telerik:RadGrid ID="StudentAddressGrid" runat="server" AllowPaging="True" AllowSorting="True" Width="650px"
        CellSpacing="0" GridLines="None"  DataSourceID="XmlDataSource1"
        ShowStatusBar="True" AutoGenerateColumns="False" 
                     PageSize="3" >
                     <ClientSettings AllowColumnsReorder="True">
                         <Selecting AllowRowSelect="True"  />
<Selecting AllowRowSelect="True"></Selecting>
                     </ClientSettings>
<MasterTableView AllowFilteringByColumn="False" AllowPaging="True"  PageSize="3" AllowSorting="True" 
                         CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="StdAddressId">
<CommandItemSettings ExportToPdfText="Export to PDF"  addnewrecordtext="Add New Address"></CommandItemSettings>
<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="StdAddressId" 
            FilterControlAltText="Filter StdAddressId column" UniqueName="StdAddressId" 
            HeaderText="StdAddressId" SortExpression="StdAddressId" 
            ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small" DataType="System.Guid" ReadOnly="True" 
            Visible="False">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="StudentId" 
            FilterControlAltText="Filter StudentId column" UniqueName="StudentId" 
            HeaderText="StudentId" SortExpression="StudentId" 
            ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small" DataType="System.Guid" Visible="False">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Address1" 
            FilterControlAltText="Filter Address1 column" UniqueName="Address1" 
            HeaderText="Address 1" SortExpression="Address1" 
            ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Address2" 
            FilterControlAltText="Filter Address2 column" 
            HeaderText="Address 2" SortExpression="Address2" 
            UniqueName="Address2" ItemStyle-Font-Size="X-Small" 
            HeaderStyle-Font-Size="X-Small" FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="City" 
            FilterControlAltText="Filter City column" UniqueName="City" 
            HeaderText="City" SortExpression="City" ItemStyle-Font-Size="X-Small" 
            HeaderStyle-Font-Size="X-Small" FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="StateDescrip" 
            FilterControlAltText="Filter StateDescrip column" HeaderText="State" 
            SortExpression="StateDescrip" UniqueName="StateDescrip" 
            ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Zip" 
            FilterControlAltText="Filter Zip column" HeaderText="Zip"
            SortExpression="Zip" UniqueName="Zip" ItemStyle-Font-Size="X-Small" 
            HeaderStyle-Font-Size="X-Small" FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CountryDescrip" 
            FilterControlAltText="Filter CountryDescrip column" HeaderText="Country"
            SortExpression="Country" UniqueName="CountryDescrip" 
            ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="AddressDescrip" 
            FilterControlAltText="Filter AddressDescrip column" HeaderText="Type" 
            SortExpression="AddressDescrip" UniqueName="AddressDescrip" 
            ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
        <telerik:GridCheckBoxColumn DataField="default1" DataType="System.Boolean" 
            FilterControlAltText="Filter default1 column" HeaderText="Is Default?" 
            SortExpression="default1" UniqueName="default1" 
            ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridCheckBoxColumn>
        <telerik:GridCheckBoxColumn DataField="ForeignZip" DataType="System.Boolean" 
            FilterControlAltText="Filter ForeignZip column" HeaderText="International?" 
            SortExpression="ForeignZip" UniqueName="ForeignZip" ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridCheckBoxColumn>
        <telerik:GridBoundColumn DataField="OtherState" 
            FilterControlAltText="Filter OtherState column" HeaderText="OtherState" 
            SortExpression="OtherState" UniqueName="OtherState" Visible="False" ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" 
            FooterStyle-Font-Size="X-Small">
<FooterStyle Font-Size="X-Small"></FooterStyle>

<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
    </telerik:RadGrid>
   </div>
   <div style="margin:10px;clear:both;">
   <div style="margin:10px 150px 10px 0px;float:left;">
   <h4>Phone Numbers</h4>
       <telerik:RadGrid ID="StudentPhoneGrid" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellSpacing="0" GridLines="None" PageSize="3" 
       width="300px"  DataSourceID="XmlDataSource2">
<MasterTableView datakeynames="StudentPhoneId" CommandItemDisplay="Top" 
           >
<CommandItemSettings   ExportToPdfText="Export to PDF"  addnewrecordtext="Add New Phone"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="StudentPhoneId" DataType="System.Guid" 
            FilterControlAltText="Filter StudentPhoneId column" HeaderText="StudentPhoneId" 
            ReadOnly="True" SortExpression="StudentPhoneId" UniqueName="StudentPhoneId" 
            Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="StudentId" DataType="System.Guid" 
            FilterControlAltText="Filter StudentId column" HeaderText="StudentId" 
            SortExpression="StudentId" UniqueName="StudentId" Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Phone" 
            FilterControlAltText="Filter Phone column" HeaderText="Phone" 
            SortExpression="Phone" UniqueName="Phone">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PhoneTypeDescrip" 
            FilterControlAltText="Filter PhoneTypeDescrip column" HeaderText="Type" 
            SortExpression="PhoneTypeDescrip" UniqueName="PhoneTypeDescrip">
        </telerik:GridBoundColumn>
<%--        <telerik:GridBoundColumn DataField="BestTime" 
            FilterControlAltText="Filter BestTime column" HeaderText="BestTime" 
            SortExpression="BestTime" UniqueName="BestTime">
        </telerik:GridBoundColumn>--%>
        <telerik:GridBoundColumn DataField="StatusId" DataType="System.Guid" 
            FilterControlAltText="Filter StatusId column" HeaderText="StatusId" 
            SortExpression="StatusId" UniqueName="StatusId" Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Ext" 
            FilterControlAltText="Filter Ext column" HeaderText="Ext" SortExpression="Ext" 
            UniqueName="Ext">
        </telerik:GridBoundColumn>
        <telerik:GridCheckBoxColumn DataField="default1" DataType="System.Boolean" 
            FilterControlAltText="Filter default1 column" HeaderText="Is Default?" 
            SortExpression="default1" UniqueName="default1">
        </telerik:GridCheckBoxColumn>
        <telerik:GridCheckBoxColumn DataField="ForeignPhone" DataType="System.Boolean" 
            FilterControlAltText="Filter ForeignPhone column" HeaderText="International?" 
            SortExpression="ForeignPhone" UniqueName="ForeignPhone">
        </telerik:GridCheckBoxColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
    <ItemStyle Font-Size="XX-Small" />
    <groupheaderitemstyle font-size="XX-Small" />
    <AlternatingItemStyle Font-Size="XX-Small" />
    <EditItemStyle Font-Size="XX-Small" />
    <PagerStyle Font-Size="XX-Small" />
    <HeaderStyle Font-Size="XX-Small" />
    <filteritemstyle font-size="XX-Small" />
    <commanditemstyle font-size="XX-Small" />
    <FooterStyle Font-Size="XX-Small" />
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
    </telerik:RadGrid>
    </div>
   </div>

<%--                 <asp:SqlDataSource ID="StudentPhoneDataSource" runat="server" 
        ConnectionString="<%$ ConnectionStrings:UIDemoConnectionString %>" SelectCommand="SELECT [StudentPhoneId]
      ,[StudentId]
      ,PT.PhoneTypeDescrip
      ,[Phone]
      ,[BestTime]
      ,SP.[StatusId]
      ,[Ext]
      ,[default1]
      ,[ForeignPhone]
  FROM [dbo].[arStudentPhone] AS SP
  INNER JOIN dbo.syPhoneType AS PT ON SP.PhoneTypeId = PT.PhoneTypeId
  WHERE studentid IN 
  ('B98AFBD1-7C75-4B66-A203-002CF8148260')"></asp:SqlDataSource>

                   <asp:SqlDataSource ID="StudentAddressDataSource" runat="server" 
        ConnectionString="<%$ ConnectionStrings:UIDemoConnectionString %>" SelectCommand="SELECT [StdAddressId],[StudentId],[Address1],[Address2],[City],ST.StateDescrip,[Zip],C.CountryDescrip,AT.AddressDescrip,[default1],[ForeignZip],[OtherState] FROM [dbo].[arStudAddresses] AS A INNER JOIN dbo.syStates AS ST ON A.StateId = ST.StateId INNER JOIN dbo.adCountries AS C ON A.CountryId = C.CountryId INNER JOIN dbo.plAddressTypes AS AT ON A.AddressTypeId = AT.AddressTypeId WHERE StudentId IN   ('B98AFBD1-7C75-4B66-A203-002CF8148260')"></asp:SqlDataSource>--%>



        <asp:XmlDataSource ID="XmlDataSource1" runat="server" 
    DataFile="~/StudentAddressCollection.xml"></asp:XmlDataSource>

<asp:XmlDataSource ID="XmlDataSource2" runat="server" 
    DataFile="~/StudentPhoneCollection.xml"></asp:XmlDataSource>