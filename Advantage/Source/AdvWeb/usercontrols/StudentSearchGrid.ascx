﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentSearchGrid.ascx.vb" Inherits="usercontrols_StudentSearchGrid" %>
            <script type="text/javascript">
                function returnToParent() {
                    window.close();
                }
            </script>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server">
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGrid2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid2" 
                    LoadingPanelID="RadAjaxLoadingPanel1" UpdatePanelHeight="" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadTabStrip ID="RadTabStrip1" runat="server" 
    MultiPageID="RadMultiPage1" SelectedIndex="0">
    <Tabs>
        <telerik:RadTab runat="server" Text="Most Recently Used" Selected="True">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Student Search">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
    <telerik:RadPageView ID="RadPageView1" runat="server">
        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
            <asp:ListItem>test1</asp:ListItem>
            <asp:ListItem>test2</asp:ListItem>
            <asp:ListItem>test3</asp:ListItem>
            <asp:ListItem>test4</asp:ListItem>
        </asp:CheckBoxList>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageView2" runat="server">
       <telerik:RadGrid ID="RadGrid2" runat="server" DataSourceID="SqlDataSource1" 
    GridLines="None" AllowPaging="True">
<MasterTableView AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
<CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="FirstName" HeaderText="FirstName" 
            SortExpression="FirstName" UniqueName="FirstName">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="LastName" HeaderText="LastName" 
            SortExpression="LastName" UniqueName="LastName">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="StudentStatus" DataType="System.Guid" 
            HeaderText="StudentStatus" SortExpression="StudentStatus" 
            UniqueName="StudentStatus">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="StudentNumber" HeaderText="StudentNumber" 
            SortExpression="StudentNumber" UniqueName="StudentNumber">
        </telerik:GridBoundColumn>
    </Columns>
</MasterTableView>
 <PagerStyle Mode="NumericPages"/>
</telerik:RadGrid>
    </telerik:RadPageView>
</telerik:RadMultiPage>

<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:Ross_2_6ConnectionString %>" 
    SelectCommand="SELECT [FirstName], [LastName], [StudentStatus], [StudentNumber] FROM [arStudent]">
</asp:SqlDataSource>

<%--<button onclick="returnToParent(); return false;">
    Go
</button>--%>




