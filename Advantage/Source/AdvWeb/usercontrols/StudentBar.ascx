﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentBar.ascx.vb" Inherits="usercontrols_StudentBar" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy> 
<telerik:RadToolTipManager ID="RadToolTipManager2" runat="server" Position="Center"
         Animation="Fade" HideEvent="LeaveTargetAndToolTip" RelativeTo="Element"
          EnableShadow="true" ShowCallout="false"
        RenderInPageRoot="true" AnimationDuration="200" >
<%--        OnAjaxUpdate="OnAjaxUpdate" OnClientHide="OnClientHide"--%>
</telerik:RadToolTipManager>
         <script type="text/javascript">
             function itemOpened(s, e) {
                 if ($telerik.isIE8) {
                     // Fix an IE 8 bug that causes the list bullets to disappear (standards mode only)
                     $telerik.$("li", e.get_item().get_element())
     .each(function () { this.style.cssText = this.style.cssText; });
                 }
             }
    
        </script>
<div id="wonderbar" style="width:100%;height:105px;background-color:#EFEFEF">
<div id="infobar" style="width:100%;">
  <div style="float:left;">
      <asp:Image ID="studentphoto" runat="server"  ImageUrl="~/images/renee.jpg" ToolTip="view student info page" style="margin:5px 5px 0px 10px;"/>
 </div>

     <div style="margin:5px 5px 0px 5px;" >
        <span style="margin:5px 5px 0px 5px;width:250px;font-size:small;">Name: <asp:HyperLink CssClass="advlink"  ID="StudentNameLink" runat="server" ToolTip="view student info page" NavigateUrl="~/studentinfo.aspx" style="margin:5px;">Renee Johnson </asp:HyperLink></span>
        <span style="margin:5px 5px 0px 5px;width:250px;font-size:small;">Student ID: <b>20110912RJ0004</b> <asp:HyperLink CssClass="advlink"  ID="HyperLink2" runat="server" ToolTip="view student info page" NavigateUrl="~/studentinfo.aspx" style="margin:5px;"></asp:HyperLink></span>
   <%-- <span style="margin: 5px 5px 0px 2px;font-size:small;">Student Balance: <asp:HyperLink CssClass="advlink" ID="StudentBalanceLink" runat="server" ToolTip="view student balance details" NavigateUrl="~/studentinfo.aspx" style="margin:5px 5px 0px 5px;color:Red;font-weight:bold;">$2,134.50</asp:HyperLink></span> --%>
<%--        <span style="margin:5px 5px 0px 5px;width:250px;font-size:small;"> <asp:HyperLink CssClass="advlink" ID="HyperLink2" runat="server" ToolTip="send email" NavigateUrl="~/studentinfo.aspx" style="margin:5px;">rjohnson@fameuniversity.edu</asp:HyperLink></span>        
        <span style="margin:5px 5px 0px 5px;width:250px;font-size:small;"><asp:HyperLink CssClass="advlink" ID="HyperLink4" runat="server" ToolTip="view detailed student contact info" NavigateUrl="~/studentinfo.aspx" style="margin:5px;">954-817-5315</asp:HyperLink></span> --%>
<%--        <span style="margin:5px 5px 0px 5px;width:250px;font-size:small;"> <asp:HyperLink CssClass="advlink" ID="HyperLink3" runat="server" ToolTip="send email" NavigateUrl="~/studentinfo.aspx" style="margin:5px;">rjohnson@fameuniversity.edu</asp:HyperLink></span>        
        <span style="margin:5px 5px 0px 5px;width:250px;font-size:small;"><asp:HyperLink CssClass="advlink" ID="HyperLink1" runat="server" ToolTip="view detailed student contact info" NavigateUrl="~/studentinfo.aspx" style="margin:5px;">954-817-5315</asp:HyperLink></span>--%>
    </div>
    <div style="margin-top:5px;">
        <span style="margin: 5px 0px 0px 5px;font-size:small;">
        <telerik:RadComboBox ID="ddlEnrollment" runat="server" Skin="Telerik" Width="620px">  
        <Items>
            <telerik:RadComboBoxItem runat="server" Text="Medical Assistant Day v2 at the Fort Lauderdale Campus" 
                Value="RadComboBoxItem1" />
            <telerik:RadComboBoxItem runat="server" Text="Dental Assistant Day v3 at the West Palm Beach Campus" 
                Value="RadComboBoxItem2" />
        </Items>
    </telerik:RadComboBox></span> 
    </div>
     <div style="margin:5px 5px 0px 5px;" >
 <%--  <span style="margin: 5px 5px 0px 2px;font-size:small;"><asp:HyperLink CssClass="advlink" ID="StudentHoldGroups" runat="server" ToolTip="view the student hold groups" NavigateUrl="~/studentinfo.aspx" style="margin:5px 15px 0px 5px;color:Red;font-weight:bold;">Student in 2 Hold Groups</asp:HyperLink></span>--%>
<%--   
    <span style="margin: 5px 5px 0px 2px;font-size:small;">Student Balance: <asp:HyperLink CssClass="advlink" ID="StudentBalance" runat="server" ToolTip="view account details" NavigateUrl="~/studentinfo.aspx" style="margin:5px 15px 0px 5px;color:Red;font-weight:bold;">$2,134.50</asp:HyperLink></span> 
  --%>
<span style="margin:5px 0px 0px 2px;font-size:small;">Status: <asp:HyperLink CssClass="advlink" ID="StudentStatusLink" runat="server" ToolTip="click to view detailed status info" NavigateUrl="~/studentinfo.aspx">Currently Attending</asp:HyperLink></span>
<%-- <span style="margin:5px 5px 0px 0px;font-size:small;">in <asp:HyperLink CssClass="advlinkRed" ID="HyperLink1" runat="server" ToolTip="view detailed hold group info" NavigateUrl="~/studentinfo.aspx">2 Hold Groups</asp:HyperLink></span>    --%>
       <span style="margin:5px 5px 0px 2px;font-size:small;">Start Date: <asp:HyperLink CssClass="advlink" ID="StartDate" runat="server" ToolTip="view detailed enrollment info" NavigateUrl="~/studentinfo.aspx" >03/09/2011</asp:HyperLink></span>
    <span style="margin: 5px 5px 0px 2px;font-size:small;">Grad Date: <asp:HyperLink CssClass="advlink" ID="GradDate" runat="server" ToolTip="view detailed academics info" NavigateUrl="~/studentinfo.aspx">10/22/2012</asp:HyperLink></span>   
    </div>
    <div style="margin-top:5px;">
<%--    <span style="margin: 5px 5px 0px 80px;font-size:small;">Enrollment Balance: <asp:HyperLink CssClass="advlink" ID="EnrollmentBalanceLink" runat="server" ToolTip="view enrollment balance details" NavigateUrl="~/studentinfo.aspx" style="margin:5px 5px 0px 5px;color:Red;font-weight:bold;">$1,130.50</asp:HyperLink></span> 
   <span style="margin:5px 5px 0px 2px;font-size:small;">GPA: <asp:HyperLink CssClass="advlink" ID="GPA" runat="server" ToolTip="view detailed academics info" NavigateUrl="~/studentinfo.aspx">3.45</asp:HyperLink></span> 
   <span style="margin: 5px 5px 0px 2px;font-size:small;">Recent Attendance: <asp:HyperLink CssClass="advlink" ID="StudentAttendance" runat="server" ToolTip="view attendance details" NavigateUrl="~/studentinfo.aspx" style="margin:5px 5px 0px 5px;color:Red;font-weight:bold;">55%</asp:HyperLink></span>  
    <span style="margin: 5px 5px 0px 2px;font-size:small; ">SAP: <asp:HyperLink CssClass="advlink" ID="SAPStatus" runat="server" ToolTip="student failed last SAP check on 01/30/2011" NavigateUrl="~/studentinfo.aspx" style="margin:5px 15px 0px 5px;color:Red;font-weight:bold;">Failed</asp:HyperLink></span>                --%>

<%--       <span style="margin: 5px 2px 0px 2px;font-size:small;"><asp:HyperLink CssClass="advlink" ID="StudentSchedule" runat="server" ToolTip="view detailed student schedule" NavigateUrl="~/studentinfo.aspx" style="margin:5px 15px 0px 5px;">View Schedule</asp:HyperLink></span>       --%>
<%--   <span style="margin: 5px 5px 0px 2px;font-size:small;">Recent Attendance: <asp:HyperLink CssClass="advlink" ID="HyperLink4" runat="server" ToolTip="view attendance details" NavigateUrl="~/studentinfo.aspx" style="margin:5px 5px 0px 5px;color:Red;font-weight:bold;">55%</asp:HyperLink></span>   
    <span style="margin: 5px 5px 0px 2px;font-size:small; ">SAP: <asp:HyperLink CssClass="advlink" ID="HyperLink6" runat="server" ToolTip="student failed last SAP check on 01/30/2011" NavigateUrl="~/studentinfo.aspx" style="margin:5px 5px 0px 5px;color:Red;font-weight:bold;">Failed</asp:HyperLink></span> --%>  
    </div>
</div>
</div>
<div id="ToolBarDiv">
        <telerik:RadToolBar ID="MyToolBar" runat="server" Width="100%" AutoPostBack="true">
        <Items>
        <telerik:RadToolBarButton>
        <ItemTemplate>
        <telerik:RadMenu runat="server" ID="RadMenu1" OnClientItemOpened="itemOpened" EnableShadows="true">
            <Items>
                <telerik:RadMenuItem Text="Menu" PostBack="false">
                    <Items>
                        <telerik:RadMenuItem CssClass="Wrapper" Width="550px">
                            <ItemTemplate>
                                <div id="CatWrapper1" class="Wrapper" style="width: 435px;">
                                    <telerik:RadSiteMap ID="RadSiteMap1" runat="server">
                                        <LevelSettings>
                                            <telerik:SiteMapLevelSetting Level="0">
                                                <ListLayout RepeatColumns="2" RepeatDirection="Vertical" />
                                            </telerik:SiteMapLevelSetting>
                                        </LevelSettings>
                                        <Nodes>
                                            <telerik:RadSiteMapNode NavigateUrl="#" Text="Student Details" BackColor="White" Width="250px">
                                                <Nodes>
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="General Info" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Address and Phone" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Emergency Contacts" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Attendance" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Education" />
                                                </Nodes>
                                            </telerik:RadSiteMapNode>
                                            <telerik:RadSiteMapNode NavigateUrl="#" Text="Documents and Test Scores">
                                                <Nodes>
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Entrance Test Scores" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Documents" />
                                                </Nodes>
                                            </telerik:RadSiteMapNode>
                                            <telerik:RadSiteMapNode NavigateUrl="#" Text="Academics">
                                                <Nodes>
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Enrollments" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Schedule" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Clinic Hours" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Clinic Services" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Term Progress" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Transcript" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Graduate Audit" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="Student SAP Check List" />
                                                    <telerik:RadSiteMapNode NavigateUrl="#" Text="FERPA" />
                                                </Nodes>
                                            </telerik:RadSiteMapNode>
                                        </Nodes>
                                    </telerik:RadSiteMap>
                                </div>
                            </ItemTemplate>
                        </telerik:RadMenuItem>
                    </Items>
                </telerik:RadMenuItem>
            </Items>
        </telerik:RadMenu>
        </ItemTemplate>
        </telerik:RadToolBarButton>
        <telerik:RadToolBarButton runat="server"  Owner="" Text="Save" ToolTip="Save Student Record" ImageUrl="~/images/save.png" ValidationGroup="Main" >
        </telerik:RadToolBarButton>
        <telerik:RadToolBarButton runat="server"  Owner="" Text="New" ToolTip="Save Student Record" ImageUrl="~/images/action_add.png" ValidationGroup="Main" >
        </telerik:RadToolBarButton>
        <telerik:RadToolBarButton runat="server"  Owner="" Text="Delete" ToolTip="Save Student Record" ImageUrl="~/images/action_delete.png" ValidationGroup="Main" >
        </telerik:RadToolBarButton>
        </Items>
        </telerik:RadToolBar> 
</div>
