﻿
Partial Class usercontrols_CampusSwitcher
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            CampusSwitcherMsg.Text = "<h2>Current Campus</h2>"
    End Sub

    Protected Sub ddlCampusSwitcher_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "closeActiveTip", "CloseActiveToolTip();", True)
    End Sub
End Class
