﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeadBarKendo.ascx.vb" Inherits="AdvWeb.usercontrols.UsercontrolsLeadBarKendo" %>


<div id="kendoleadBarControl" style="width: 100%;">
    <link rel="stylesheet" type="text/css" href="../css/AD/LeadInfoBar.css">
    <div style="width: 100%; background-color: #E9EDF2;">
        <script type="text/kendo-x-tmpl" id="templateInfoBarReqs">
                <div class="classTemplateInfoBar" >
                      <img  id="infoBarImage" class= "templatimgClass" src="#:ImagePath#" />
                      <span class= "templatimgClass" >#:RequirementName#</span>
                </div>
        </script>

        <div style="margin-right: 250px; min-width: 1247px; width: 100%;">
            <div id="leftpane" class="left">
                <table class="tablekendoInfoBar" style="text-align: left;">
                    <tr style="vertical-align: top">
                        <td rowspan="2" style="width: 55px; text-align: left; vertical-align: middle; margin-left: 2px; padding-left: 0; z-index: 999">
                            <span id="infobarImage" class="k-icon k-i-user k-icon-64"> </span>
                            <%--<img id="infobarImage" class="infoBarTableImage" src="../images/face75.png" style="z-index: 999; overflow: visible; position: relative" alt="Lead Image" />--%>
                        </td>
                        <td class="infobarTableLabelC1">Lead:</td>
                        <td class="infobarInfo">
                            <a id="infobarToLeadInfoPage" href="#">
                                <label id="infoBarTableFullname">-</label>
                            </a>
                        </td>
                        <td class="infobarTableLabelC2">Program Version:</td>
                        <td class="infobarInfo">
                            <label id="infoBarTableProgramVersion">-</label></td>
                        <td class="infobarTableLabelC3">Admission Rep:</td>
                        <td class="infobarInfo">
                            <label  id="infoBarTableAdmissionRep">-</label></td>

                    </tr>
                    <tr style="vertical-align: text-top">
                        <td class="infobarTableLabelC1">Status:</td>
                        <td class="infobarInfo">
                            <label  id="infoBarTableStatus">-</label></td>
                        <td class="infobarTableLabelC2">Expected Start:</td>
                        <td class="infobarInfo">
                            <label  id="infoBarTableExpectedStart">-</label></td>
                        <td class="infobarTableLabelC3">Date Assigned:</td>
                        <td class="infobarInfo">
                            <label id="infoBarTableDateAssigned">-</label>

                        </td>
                    </tr>
                </table>
            </div>
            <div id="rightPaneRequirements" class="right">
                <ul id="leadRequirementsMet">
                    <li class="loading hidden">
                        <div class="progressbar k-loading"></div>
                    </li>
                    <%--<li class="met">Document</li>
                    <li class="not-defined">Fee</li>
                    <li class="met">Test</li>
                    <li class="not-defined">Tour</li>
                    <li class="met">Interview</li>
                    <li class="not-defined">Event</li>--%>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</div>
<%-- ReSharper disable UnusedLocals --%>
<script type="text/javascript">
    $(document).ready(function () {

        var leadinfoBar = new AD.LeadInfoBar();
    });
</script>
<%-- ReSharper restore UnusedLocals --%>   
