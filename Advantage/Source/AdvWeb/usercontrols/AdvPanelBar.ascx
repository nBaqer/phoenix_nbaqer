﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AdvPanelBar.ascx.vb" Inherits="usercontrols_AdvPanelBar" %>
<%@ Register TagPrefix="fame" TagName="MRUStudent" Src="~/UserControls/RecentStudentControl.ascx" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy>
<telerik:RadPanelBar ID="RadPanelBar1" Runat="server" Height="100%"  ExpandMode="FullExpandedItem" BorderStyle="None">
    <Items>
        <telerik:RadPanelItem runat="server" Text="Settings" Value="SettingsHeader" Expanded="True" BorderStyle="None">
            <Items>
                <telerik:RadPanelItem runat="server" Value="Settings" BorderStyle="None">
                <ItemTemplate>
                  <telerik:RadToolBar ID="SettingsToolBar" runat="server" AutoPostBack="True" Width="100%">
                    <Items>
                        <telerik:RadToolBarButton runat="server" Text="New" ImageUrl="~/images/action_add.png">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" Text="Save" ImageUrl="~/images/save.png">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" Text="Delete" ImageUrl="~/images/action_delete.png">
                        </telerik:RadToolBarButton>
                    </Items>
                  </telerik:RadToolBar>
                </ItemTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
        <telerik:RadPanelItem runat="server" Text="Recent Students">
            <Items>
                <telerik:RadPanelItem runat="server" Value="StudentMRU" BackColor="#B4C6E4">
                <ItemTemplate>
                 <fame:MRUStudent ID="RecentStudent1" runat="server" />
                </ItemTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
        <telerik:RadPanelItem runat="server" Text="Student Search">
            <Items>
                <telerik:RadPanelItem runat="server" Value="StudentSearch">
                <ItemTemplate>
                    <p>Jeffrey A. Spring</p>
                </ItemTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>

