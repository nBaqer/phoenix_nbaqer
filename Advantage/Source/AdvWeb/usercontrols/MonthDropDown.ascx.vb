﻿
Imports FAME.Advantage.Common
Imports Telerik.Web.UI

Partial Class usercontrols_MonthDropDown
    Inherits System.Web.UI.UserControl
    Public Label As String 
    Public Mandatory As Boolean
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        dim _label = "Month"
        If Not String.IsNullOrEmpty(Label) Then
            _label = Label
        End If
        If Mandatory Then 
            _label += " <font color=""red"">*</font>"
        End If
        MonthHeader.InnerHtml = _label
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            FillMonths()
        End If
    End Sub
    public Function GetItems() As RadComboBoxItemCollection
        return RadComboMonth.Items
    End Function

    public Function GetId() As String
        return RadComboMonth.ID
    End Function
    Public Function GetSelected() As RadComboBoxItem
        Return RadComboMonth.SelectedItem
    End Function
    Public Function GetInnerControl() As RadComboBox
        Return RadComboMonth
    End Function

    Public Sub FillMonths()
        Try
            Dim listOfMonth As List(Of Object) = New List(Of Object)() From
                    {
                    New With {
                    .Text = "January",
                    .Value = "1"
                    },
                    New With {
                    .Text = "February",
                    .Value = "2"
                    },
                    New With {
                    .Text = "March",
                    .Value = "3"
                    },
                    New With {
                    .Text = "April",
                    .Value = "4"
                    },
                    New With {
                    .Text = "May",
                    .Value = "5"
                    },
                    New With {
                    .Text = "June",
                    .Value = "6"
                    },
                    New With {
                    .Text = "July",
                    .Value = "7"
                    },
                    New With {
                    .Text = "August",
                    .Value = "8"
                    },
                    New With {
                    .Text = "September",
                    .Value = "9"
                    }, New With {
                    .Text = "October",
                    .Value = "10"
                    },
                    New With {
                    .Text = "November",
                    .Value = "11"
                    },
                    New With {
                    .Text = "December",
                    .Value = "12"
                    }
                    }
            RadComboMonth.DataTextField = "Text"
            RadComboMonth.DataValueField = "Value"
            RadComboMonth.DataSource = listOfMonth
            RadComboMonth.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
End Class
