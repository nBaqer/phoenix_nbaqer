' ===============================================================================
'
' FAME AdvantageV1
'
' EmailEditor.ascx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports System.Text
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common.smtp

Partial Class EmailEditor
    Inherits UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cbxUseSelector As CheckBox
    Protected WithEvents lblBody As Label


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            'Put user code to initialize the page here
            If txtFrom.Text = "" And Not Session("UserName") Is Nothing Then
                Dim usi As UserSecurityInfo = (New UserSecurityFacade).GetUserInfoUsingUserName(Session("UserName"))
                'txtFrom.Text = usi.FullName
                txtFrom.Text = usi.Email
            End If
        End If
    End Sub

    Private Sub btnSend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSend.Click

        If ValidateInput() Then
            '   send all emails
            SendAllEmails()
        Else
            DisplayErrorMessage(GetErrorMessage())
        End If

    End Sub
    Private Sub SendAllEmails()

        '   send all emails
        Dim numberOfMessages As Integer
        If Not txtTo.Text = "" Then
            '   loop throughout the list of email addresses
            Dim recipients() As String = txtTo.Text.Split(";")
            numberOfMessages = recipients.Length
            '   check that if the last item ends with ";" we will have one additional item
            If recipients(numberOfMessages - 1) = "" Then numberOfMessages -= 1

            'validate email addresses of all messages
            Dim regEx As Regex = New Regex("([\w-]+@([\w-]+\.)+[\w-]+)")

            If numberOfMessages > 0 Then
                For i As Integer = 0 To numberOfMessages - 1
                    Dim email As String = recipients(i)
                    Dim idx1 As Integer = email.IndexOf("[")
                    Dim idx2 As Integer = email.IndexOf("]")
                    If idx1 >= 0 And idx2 >= 0 Then
                        'validate email with regex
                        If Not regEx.IsMatch(email.Substring(idx1 + 1, idx2 - 1 - idx1)) Then
                            '   There was an error in one of the email strings
                            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "The email address: " + email + " is invalid.")
                            Exit Sub
                        End If
                    Else
                        'validate email with regex
                        If Not regEx.IsMatch(email) Then
                            '   There was an error in one of the email strings
                            CommonWebUtilities.DisplayErrorInMessageBox(Page, "The email address: " + email + " is invalid.")
                            Exit Sub
                        End If
                    End If
                Next
            End If

            If numberOfMessages > 0 Then
                For i = 0 To numberOfMessages - 1
                    Dim recipient As String = ExtractRecipient(recipients(i))
                    Dim smtp = New SmtpSendMessageInputModel()
                    smtp.From = txtFrom.Text
                    Dim bytebody = Encoding.UTF8.GetBytes(txtBody.Text)
                    smtp.Body64 = Convert.ToBase64String(bytebody)
                    smtp.Command = 1
                    smtp.ContentType = "text/html"
                    smtp.Subject = txtSubject.Text
                    smtp.To = recipient

                    Dim result As String = FAME.AdvantageV1.BusinessFacade.MSG.Mail.SendEmailService(smtp)
                    'Dim result As String = CommonWebUtilities.SendEmailMessage(CommonWebUtilities.BuildEmailMessage(txtFrom.Text, recipient, txtSubject.Text, txtBody.Text, Nothing))
                    If Not result = "" Then
                        '   There was an error sending the message, probably SMTP server not configured properly.
                        CommonWebUtilities.DisplayErrorInMessageBox(Page, result)
                        Exit Sub
                    End If
                Next
                '   clear fields
                txtTo.Text = ""
            End If

            '   Display number of messages sent in a box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, numberOfMessages.ToString + " email messages sent.")

        End If

    End Sub
    Private Function ExtractRecipient(email As String) As String
        Try
            Return email.Substring(email.IndexOf("[") + 1, email.IndexOf("]") - email.IndexOf("[") - 1)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return email
        End Try
    End Function
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
        '   add new items to the list
        If Not Session("EmailList") Is Nothing Then
            Dim emailList(,) As String = CType(Session("EmailList"), String(,))
            For i As Integer = 0 To emailList.GetUpperBound(1)
                txtTo.Text += emailList(1, i) + " [" + emailList(0, i) + "];"
            Next

            '   delete duplicate names from the list
            txtTo.Text = DeleteDuplicates(txtTo.Text)

            '   kill session variable in order to avoid duplication
            Session("EmailList") = Nothing
        End If
    End Sub
    Private Function DeleteDuplicates(ByVal str As String) As String

        '   delete the record after the last semicolon
        Dim arr As String() = str.Split(";")
        If arr(arr.Length - 1) = "" Then ReDim Preserve arr(arr.Length - 2)

        '   delete items with no emails (marked with an (*))
        Dim upperLimit As Integer = arr.Length - 1
        For i As Integer = 0 To arr.Length - 1
            If arr(i).IndexOf("(*)") > -1 Then
                For j As Integer = i To upperLimit - 1
                    arr(j) = arr(j + 1)
                Next
                upperLimit -= 1
            End If
        Next
        ReDim Preserve arr(upperLimit)

        '   if there is one or zero records return
        If arr.Length < 2 Then Return RebuildString(arr)

        '   delete duplicates
        Array.Sort(arr)
        Dim noMoreDuplicates As Boolean = False
        upperLimit = arr.Length - 1
        While Not noMoreDuplicates
            For i As Integer = 1 To upperLimit
                If arr(i) = arr(i - 1) Then
                    For j As Integer = i To upperLimit - 1
                        arr(j) = arr(j + 1)
                    Next
                    ReDim Preserve arr(upperLimit - 1)
                    upperLimit -= 1
                    If upperLimit = 0 Then noMoreDuplicates = True Else noMoreDuplicates = False
                    Exit For
                End If
                noMoreDuplicates = True
            Next
        End While

        '   return string 
        Return RebuildString(arr)

    End Function
    Private Function RebuildString(ByVal arr As String()) As String
        '   rebuild the string back;
        Dim sb As New StringBuilder
        For i As Integer = 0 To arr.Length - 1
            sb.Append(arr(i) + ";")
        Next

        '   Return string
        Return sb.ToString

    End Function
    Private Function ValidateInput() As Boolean
        If txtTo.Text = "" Then Return False
        If txtFrom.Text = "" Then Return False
        Return True
    End Function
    Private Function GetErrorMessage() As String
        GetErrorMessage = ""
        If txtTo.Text = "" Then
            GetErrorMessage &= "'To:' field should not be blank." & vbCrLf
        End If
        If txtFrom.Text = "" Then
            GetErrorMessage &= "'From:' field should not be blank." & vbCrLf
        End If
    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub
End Class
