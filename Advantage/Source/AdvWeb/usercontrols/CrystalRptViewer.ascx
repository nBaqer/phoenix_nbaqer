<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="CrystalRptViewer" CodeFile="CrystalRptViewer.ascx.vb" %>
<link rel="stylesheet" type="text/css" href="../css/localhost_lowercase.css" />
<table align="center" class="contenttable">
    <tr>
        <td class='rptspacer"'>&nbsp;</td>
    </tr>
</table>
<!-- Report Navigator toolbar !-->
<asp:Panel ID="pnlOne" runat="server" CssClass="rptviewerpanel">
    <table cellspacing="0" cellpadding="1" align="center">
        <tr>
            <td></td>
            <td>
                <table cellspacing="1" cellpadding="0" width="60%" align="center" border="0">
                    <tr>
                        <td nowrap="nowrap">

                            <asp:Button ID="btnFirst" Width="35px" runat="server" Text=" |< " CssClass="reportbuttons"></asp:Button>
                            <asp:Button ID="btnPrevious" Width="35px" runat="server" Text=" < " CssClass="reportbuttons"></asp:Button>
                            <asp:Button ID="btnNext" Width="35px" runat="server" Text=" > " CssClass="reportbuttons"></asp:Button>
                            <asp:Button ID="btnLast" Width="35px" runat="server" Text=" >| " CssClass="reportbuttons"></asp:Button></td>
                        <td>
                            <asp:Label ID="lbPage" runat="server" CssClass="label">Page</asp:Label></td>
                        <td nowrap="nowrap">
                            <asp:TextBox ID="tbGoToPage" CssClass="textbox" runat="server" Width="50px"></asp:TextBox></td>
                        <td>
                            <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="reportbuttons"></asp:Button></td>
                        <td nowrap="nowrap" align="right">
                            <asp:TextBox ID="tbSearchFor" CssClass="textbox" runat="server" Width="100px"></asp:TextBox></td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text=" Search " CssClass="reportbuttons"></asp:Button></td>
                    </tr>
                </table>
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <table cellspacing="1" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlZoom" CssClass="label" runat="server" Width="60px" AutoPostBack="True">
                                <asp:ListItem Value="25"> 25% </asp:ListItem>
                                <asp:ListItem Value="50"> 50% </asp:ListItem>
                                <asp:ListItem Value="75"> 75% </asp:ListItem>
                                <asp:ListItem Value="100"> 100% </asp:ListItem>
                                <asp:ListItem Value="150"> 150% </asp:ListItem>
                                <asp:ListItem Value="200"> 200% </asp:ListItem>
                                <asp:ListItem Value="300"> 300% </asp:ListItem>
                                <asp:ListItem Value="400"> 400% </asp:ListItem>
                            </asp:DropDownList></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">
                            <asp:DropDownList ID="ddlExportTo" CssClass="label" runat="server" Width="150px">
                                <asp:ListItem Value="pdf">Adobe Acrobat</asp:ListItem>
                                <asp:ListItem Value="xls">Microsoft Excel</asp:ListItem>
                                <asp:ListItem Value="doc">Microsoft Word</asp:ListItem>
                                <asp:ListItem Value="xls1">Microsoft Excel (Data)</asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left">
                            <asp:Button ID="btnExport" runat="server" Text=" Export " CssClass="reportbuttons"></asp:Button></td>
                        <td align="right">
                            <asp:Button ID="btnFriendlyPrint" CssClass="largereportbuttons" runat="server" Text="Printer Friendly Report" Visible="false"></asp:Button></td>
                    </tr>
                </table>
            </td>
            <td></td>
        </tr>
    </table>
</asp:Panel>
<!-- End of Report Navigator Toolbar !-->
<table cellpadding="0" align="center" class="contenttable">
    <tr>
        <td class="rptspacer">&nbsp;</td>
    </tr>
</table>
<!-- Report Viewer !-->
<table class="maincontenttable, Table100">
    <tr>
        <td style="width: 100%; text-align: center" class="reportdetailsframe">
            <CR:CrystalReportViewer ID="crViewer" runat="server" CssClass="rptviewer" BestFitPage="False"
                Width="1050px" Height="550px" HasRefreshButton="True" HasToggleGroupTreeButton="False"
                EnableDrillDown="False" EnableToolTips="False" DisplayToolbar="False"
                HasToggleParameterPanelButton="false" ToolPanelView="None"
                DisplayStatusbar="False" HasDrilldownTabs="False" ToolPanelWidth="0px">
            </CR:CrystalReportViewer>
        </td>
    </tr>
</table>
