﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MasterPageCampusDropDown.ascx.vb" Inherits="AdvWeb.usercontrols.UsercontrolsMasterPageCampusDropDown" %>
<!-- You need to add the jquery and kendo support in the page or master page to use the component -->
<%--<script src="<%= Page.ResolveUrl("~")%>Scripts/Advantage.Client.MasterPage.js" type="text/javascript"></script>--%>

<span id="MasterCampusDropDown1Content" class="k-content ">
    <div class="ddmasterTooltip">
        <%--<input />--%>
        <select id="MasterCampusDropDown" data-role="dropdownlist" style="background-color: transparent; height: 32px;" data-bind="value:SelectedCampusId " ></select>
    </div>
    <asp:HiddenField ID="MpCurrentSessionId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="MpPreviousSessionId" runat="server" ClientIDMode="Static" />
</span>
<script>
    // Support for DropDownList.
    var XMASTER_GET_BASE_URL = "<%= Page.ResolveUrl("~")%>";
    var XMASTER_GET_CAMPUSES_URL = "<%= Page.ResolveUrl("~")%>" + "proxy/api/Campuses/";
    var XMASTER_GET_USER_ID = "<%=Session("UserId") %>";
    var XMASTER_GET_CURRENT_CAMPUS_ID = "<%=CurrentCampusId%>";
    var XMASTER_GET_SWITCHING_CAMPUS = "<%=AdvantageSession.ShowNotificationSwitchingCampus%>";
    var XMASTER_GET_ENABLE_CAMPUS_CB = "<%=AdvantageSession.EnableCampusComboBox%>";
</script>


