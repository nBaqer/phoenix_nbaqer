﻿
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports Telerik.Web.UI

Partial Class usercontrols_ReportDropDown
    Inherits System.Web.UI.UserControl
    Protected MyAdvAppSettings As AdvAppSettings
    Public Label As String
    Public Mandatory As Boolean
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim _label = "Month"
        If Not String.IsNullOrEmpty(Label) Then
            _label = Label
        End If
        If Mandatory Then
            _label += " <font color=""red"">*</font>"
        End If
        ReportHeader.InnerHtml = _label
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
        End If
    End Sub
    Public Function GetItems() As RadComboBoxItemCollection
        Return RadComboReport.Items
    End Function

    Public Function GetId() As String
        Return RadComboReport.ID
    End Function
    Public Function GetSelected() As RadComboBoxItem
        Return RadComboReport.SelectedItem
    End Function
    Public Function GetInnerControl() As RadComboBox
        Return RadComboReport
    End Function

    Public Sub Reload(ByVal selectedStateId As Guid, Optional ByVal includeSelect As Boolean = False)
        Try
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim ds = New List(Of GenericListItem)

            If (includeSelect) Then
                ds.Insert(0, New GenericListItem() With {.Description = "Select", .Id = "-1"})
            End If

            If (Not selectedStateId = Guid.Empty) Then
                ds.AddRange((New StateReportsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetStateReportsForState(selectedStateId).Select(Function(x As syReport) New GenericListItem With {.Description = x.ReportDescription, .Id = x.ResourceId.ToString()}).ToList())

            End If

            RadComboReport.DataTextField = "Description"
            RadComboReport.DataValueField = "Id"
            RadComboReport.DataSource = ds
            RadComboReport.DataBind()

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try

    End Sub


End Class
