﻿
Imports FAME.Advantage.Common

Partial Class usercontrols_GoogleAnalytics
    Inherits System.Web.UI.UserControl
    Protected LoadGoogleAnalyticsScript As Boolean
    Protected GoogleAnalyticsCode As String
    Protected MyAdvAppSettings As AdvAppSettings


    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        GoogleAnalyticsCode = GetGoogleAnalyticsCode()
        LoadGoogleAnalyticsScript = string.IsNullOrEmpty(GoogleAnalyticsCode) = false

    End sub

    Private Function GetGoogleAnalyticsCode() As String
        Return MyAdvAppSettings.AppSettings("GoogleAnalyticsCode")
       
    End Function

End Class
