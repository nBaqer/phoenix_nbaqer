﻿
Imports System.Diagnostics
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI

Partial Class usercontrols_StateDropDown
    Inherits System.Web.UI.UserControl
    Protected MyAdvAppSettings As AdvAppSettings
    Public Label As String
    Public Mandatory As Boolean
    Public Event StateIndexChange(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim _label = "Month"
        If Not String.IsNullOrEmpty(Label) Then
            _label = Label
        End If
        If Mandatory Then
            _label += " <font color=""red"">*</font>"
        End If
        StateHeader.InnerHtml = _label
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
        End If
    End Sub
    Public Function GetItems() As RadComboBoxItemCollection
        Return RadComboState.Items
    End Function

    Public Function GetId() As String
        Return RadComboState.ID
    End Function
    Public Function GetSelected() As RadComboBoxItem
        Return RadComboState.SelectedItem
    End Function
    Public Function GetInnerControl() As RadComboBox
        Return RadComboState
    End Function

    Public Sub Reload(Optional ByVal includeSelect As Boolean = False)
        Try
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim ds = New List(Of GenericListItem)

            If (includeSelect) Then
                ds.Insert(0, New GenericListItem() With {.Description = "Select", .Id = "-1"})
            End If
            ds.AddRange((New StatesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetStateBoardReportStates().Select(Function(x As syState) New GenericListItem With {.Description = x.StateDescrip, .Id = x.StateId.ToString()}).ToList())

            RadComboState.DataTextField = "Description"
            RadComboState.DataValueField = "Id"
            RadComboState.DataSource = ds
            RadComboState.DataBind()

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try

    End Sub
    
    Protected Sub RadComboState_OnSelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) 
        RaiseEvent StateIndexChange(sender, e)
    End Sub
End Class
