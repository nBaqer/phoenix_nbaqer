﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentInfoBar.ascx.vb" Inherits="UserControls_StudentInfoBar" %>
<%@ Register Src="~/usercontrols/LeadBarKendoWebCam.ascx" TagPrefix="uc1" TagName="LeadBarKendoWebCam" %>
<script src="../Scripts/Fame.Advantage.API.Client.js"></script>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy>
<uc1:LeadBarKendoWebCam runat="server" ID="LeadBarKendoWebCam" />
<telerik:RadToolTipManager ID="RadToolTipManager2" runat="server" Position="Center"
    Animation="Fade" HideEvent="LeaveTargetAndToolTip" RelativeTo="Element"
    EnableShadow="true" ShowCallout="false"
    RenderInPageRoot="true" AnimationDuration="200">
</telerik:RadToolTipManager>
<script type="text/javascript">

    goToSearchPage = function () {

        location.href = jQuery("#existingStudentsLink").val();
    };

</script>
<div id="wonderbar" class="studentInfoBar">
    <%--<asp:HiddenField  ID="shadowLead1" runat="server" ClientIDMode="Static"/>--%>

    <div id="infobar" style="width: 100%;">
        <asp:Table ID="tblLeadBar" runat="server" Width="100%" BorderStyle="None" Style="padding-top: 10px; font-family: Verdana; font-size: 12px;">
            <asp:TableRow>
                <asp:TableCell Width="5%" HorizontalAlign="Center">
<%--                    <asp:Image ID="studentphoto" ClientIDMode="Static" runat="server" CssClass="infoBarTableImage" ImageUrl="~/images/face75.png" Style="margin: -35px 5px 0 10px; width: 60px; height: 60px;" />--%>
                    <asp:Literal ID="studentphotoLt" runat="server"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="60%">
                    <asp:Table ID="tblLeadInfoBar" runat="server" Width="100%">
                        <asp:TableRow>
                            <asp:TableCell Width="20px">
                                <a href="javascript:void(0);"  id="imgSearch" runat="server" onclick="goToSearchPage()" >
                                    <span class="k-icon k-i-search font-black"></span>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                <asp:Label ID="lblName" runat="server" Text="Student: " Font-Bold="true"></asp:Label>
                                <a id="infobarToStudentSummaryPage" href="#">
                                    <asp:Label ID="lblInfoBarNameValue" runat="server" ClientIDMode="static"></asp:Label>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                <asp:Label ID="lblProgramVersion" runat="server" Text="Program Version: " Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblProgramVersionValue" runat="server" ClientIDMode="static"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date: " Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblInfoBarStartDateValue" runat="server" ClientIDMode="static"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" ID="tcEnrollmentCount" runat="server">
                                <%--<asp:Label ID="lblEnrollmentCount" runat="server" Text="Number of Enrollments: " Font-Bold="true" Width="160px"></asp:Label>--%>
                                <asp:LinkButton ID="lbEnrollmentCountValue" runat="server" CssClass="label" Font-Bold="true"></asp:LinkButton>
                            </asp:TableCell>
                            <asp:TableCell Width="150px" Wrap="false" ID="tcFaSapStatus" runat="server">
                                <asp:LinkButton Width="130px" ID="lbFaSapStatus" runat="server" Font-Bold="true" CausesValidation="false" calss="pull-left"></asp:LinkButton>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <a href="javascript:void(0);"  onclick="openTaskRadWindow();" id="img1" runat="server" >
                                    <span class="k-icon k-i-email font-black"></span>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                <asp:Label ID="lblIdentifier" runat="server" Text="Student ID: " Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblIdentifierValue" runat="server" ClientIDMode="static"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                <asp:Label ID="lblStatus" runat="server" Text="Status: " Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblStatusValue" runat="server" ClientIDMode="static"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                <asp:Label ID="lblGradDate" runat="server" Text="Grad Date: " Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblGradDateValue" runat="server" ClientIDMode="static"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" ID="tcActiveEnrollments" runat="server" >
                                <%--<asp:Label ID="lblActiveEnrollments" runat="server" Text="Active Enrollments: " Font-Bold="true" Width="160px"></asp:Label>--%>
                                <telerik:RadComboBox ID="rcbActiveEnrollments" runat="server" Width="350px" AutoPostBack="True"></telerik:RadComboBox>
                            </asp:TableCell>
                            <asp:TableCell Width="150px" Wrap="false" ID="tcHold" runat="server" calss="pull-left">
                                <asp:LinkButton Width="130px" ID="lbHold" runat="server" Font-Bold="true" CssClass="redButton" CausesValidation="false" calss="pull-left" hidden="true"></asp:LinkButton>
                            </asp:TableCell>
                        </asp:TableRow>

                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>

    </div>
    <div id="hiddenVariables" style="display: none;">
        <asp:Label ID="shadowLead1" runat="server" ClientIDMode="Static"></asp:Label>
        <asp:HiddenField ID="hdnInfoBarCampusDescrip" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="existingStudentsLink" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="studentIdInfo" runat="server" ClientIDMode="Static" />
        <asp:Label ID="studentIdForHOLD" runat="server" ClientIDMode="Static"></asp:Label>
        <asp:HiddenField ID="currentStudentEnrollment" runat="server" ClientIDMode="Static" />
    </div>
    <%-- ReSharper disable UnusedLocals --%>
    <script type="text/javascript">
        $(document).ready(function () {

            var sinfoBar = new MasterPage.StudentInfoBar();
            $('#<%= lbHold.ClientId %>').hide()

        });

        function getHold() {
            var studentId = { studentId: $("#studentIdForHOLD").text() };
            var studentApi = new Api.Student();

            studentApi.getHoldStatusByStudentId(studentId, function (data) {

                if (data == true) {

                    $('#<%= lbHold.ClientId %>').show()


                } else {

                    $('#<%= lbHold.ClientId %>').hide()

                }


            });
        }

    </script>
    <style>
        .redButton {
            background-color: #ffd2d4;
            padding: 3%;
            border: 1px solid #b71c1c;
            color: #b71c1c;
            text-align: center;
        }
            .redButton:hover {
                color: red !important;
            }
        .yellowButton {
            background-color: #fff523;
            padding: 3%;
            border: 1px solid #a56412;
            color: #a56412;
            text-align: center;
        }

        .greenButton {
            background-color: #07b11a;
            padding: 3%;
            border: 1px solid #246522;
            color: #d7ffd5;
            text-align: center;
        }
            .greenButton:hover {
                color: #FFF !important;
            }
    </style>
    <%-- ReSharper restore UnusedLocals --%>
</div>

