﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentInfo.ascx.vb" Inherits="usercontrols_StudentInfo" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    </telerik:RadAjaxManagerProxy>
    <telerik:RadCodeBlock ID="RadCodeBloc1" runat="server">

        <script type="text/javascript">
                 //<![CDATA[
            function clearInput(sender, args) {
                $find('<%= RadDateDOB.ClientID %>').clear();
            }

            function Error(sender, args) {
                switch (args.get_reason()) {
                    case Telerik.Web.UI.InputErrorReason.ParseError:
                        // HandleDateParseError(sender, args.get_inputText().toUpperCase(), args);
                         $find('<%= RadDateDOB.ClientID %>').clear();
                        break;
                    case Telerik.Web.UI.InputErrorReason.OutOfRange:
                        //HandleDateOutOfRange(sender, args);
                         $find('<%= RadDateDOB.ClientID %>').clear();
                        break;
                }
            }

            function trim(str) {
                return str.replace(/ /g, "");
            }

            function HandleDateParseError(radDateInput, inputText, args) {
                var now = new Date();

                args.set_cancel(true);

                var specialDays =
                         new Array(
                                 { DayName: "CHRISTMAS", Date: new Date(now.getFullYear(), 11, 25) },
                                 { DayName: "TODAY", Date: new Date() },
                                 { DayName: "TOMORROW", Date: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1) },
                                 { DayName: "YESTERDAY", Date: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1) }
                                 );
                var i;
                for (i = 0; i < specialDays.length; i++) {
                    if (specialDays[i].DayName == trim(inputText).toUpperCase()) {
                        radDateInput.set_selectedDate(specialDays[i].Date);
                        break;
                    }
                }
            }

            function HandleDateOutOfRange(radDateInput, args) {
                args.set_cancel(true);
                radDateInput.set_selectedDate(new Date());
            }
                 //]]>
            function Focus(id) {
                var radInput = $find(id);
                radInput.focus();
            }
        </script>

    </telerik:RadCodeBlock>              
    <asp:Panel ID="Panel1" style="clear:both;"  runat="server">
        <telerik:RadToolBar ID="StudentInfoToolBar" runat="server" Width="100%" AutoPostBack="true">
        <Items>
    <telerik:RadToolBarButton runat="server"  Owner="" Text="Save" ToolTip="Save Student Record" ImageUrl="~/images/save.png" ValidationGroup="Main" >
    </telerik:RadToolBarButton>
        </Items>
        </telerik:RadToolBar> 
        <div class="StudentInfoRow" style="margin:15px;clear:both;">
                <h4 style="margin:5px;">Personal Info</h4>
        <div  class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Prefix</div>
                    <telerik:RadComboBox ID="RadComboBox1" runat="server" Width="50px"  font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--" Value="none"  />
                            <telerik:RadComboBoxItem runat="server" Text="Mr" Value="Mr"  />
                            <telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs" />
                            <telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms" Selected="True"/>
                            <telerik:RadComboBoxItem runat="server" Text="Miss" Value="Miss" />
                        </Items>
                </telerik:RadComboBox>
        </div>
        <div class="StudentInfo_ElePositions">
                <div style="font-size:small;">First Name</div>
                <div><telerik:RadTextBox ID="RadTextBox3" runat="server" font-size="Small" Text="Renee">
                </telerik:RadTextBox ><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ErrorMessage="First Name is a required field." ControlToValidate="RadTextBox3" ValidationGroup="Main" Display="Dynamic"></asp:RequiredFieldValidator></div>
        </div>
        <div class="StudentInfo_ElePositions">
            <div style="font-size:small;">Middle Name</div>
            <div><telerik:RadTextBox ID="RadTextBox1" runat="server" font-size="Small" Text="Mary">
                </telerik:RadTextBox></div>
        </div>
        <div class="StudentInfo_ElePositions">
            <div style="font-size:small;">Last Name</div>
            <div><telerik:RadTextBox ID="RadTextBox2" runat="server" font-size="Small" Text="Johnson">
                </telerik:RadTextBox></div>
        </div>
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Suffix</div>
                    <telerik:RadComboBox ID="RadComboBox2" runat="server" Width="50px"  font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"  />
                            <telerik:RadComboBoxItem runat="server" Text="Jr" />
                            <telerik:RadComboBoxItem runat="server" Text="Sr" />
                            <telerik:RadComboBoxItem runat="server" Text="II"  />
                            <telerik:RadComboBoxItem runat="server" Text="III"  />
                            <telerik:RadComboBoxItem runat="server" Text="IV"  />
                        </Items>
                </telerik:RadComboBox>
        </div>
        </div>

       <div class="StudentInfoRow"  style="margin:15px 15px 25px 15px;clear:both;"> 
        <div class="StudentInfo_ElePositions">
         <div style="font-size:small;">Date of Birth</div>

              <div>
                  <telerik:RadDatePicker ID="RadDateDOB" runat="server" width="100px" font-size="Small" selecteddate="10/14/1991">
                      <DateInput runat="server">
                        <ClientEvents OnError="Error" />
                      </DateInput>
                  </telerik:RadDatePicker>
              </div>
        </div>

        <div class="StudentInfo_ElePositions">
        <div style="font-size:small;">SSN</div>
           <div>
                <telerik:RadMaskedTextBox ID="RadMaskedTextBox3" Runat="server" 
                Mask="###-##-####" Width="80px" font-size="Small" Text="013679845">
                </telerik:RadMaskedTextBox>
           </div>
       </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="Panel2" style="margin-top:15px;clear:both;" runat="server">
<div class="StudentInfoRow" style="margin:15px 15px 15px 15px;clear:both;">
        <h4 style="margin:5px;">Demographic Info</h4>
        <div class="StudentInfo_ElePositions">

                        <div style="font-size:small;">Gender</div>
              <div>                    <telerik:RadComboBox ID="RadComboBox72" runat="server" Width="75px"  font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"  />
                            <telerik:RadComboBoxItem runat="server" Text="Male"  />
                            <telerik:RadComboBoxItem runat="server" Text="Female" Selected="true" />
                        </Items>
                </telerik:RadComboBox></div>
        </div>
      <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Family Income</div>
            <telerik:RadNumericTextBox Runat="server" ID="RadNumericFamilyIncome" width="100px" DataType="System.Decimal" MaxValue="99999999" MinValue="0" Type="Currency" Value="55000.75">
              <numberformat positivepattern="$n" />
            </telerik:RadNumericTextBox>
        </div>
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Martial Status</div>
                    <telerik:RadComboBox ID="RadComboBoxms" runat="server"   font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="Married"  />
                            <telerik:RadComboBoxItem runat="server" Text="Divorced"/>
                            <telerik:RadComboBoxItem runat="server" Text="Single" Selected="true"/>
                            <telerik:RadComboBoxItem runat="server" Text="Widowed" />
                            <telerik:RadComboBoxItem runat="server" Text="Unknown" />
                        </Items>
                </telerik:RadComboBox>
        </div>
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Children</div>
            <telerik:RadNumericTextBox Runat="server" ID="RadNumberOfChildren" width="40" MaxLength="3" 
            MaxValue="999" MinValue="0" DataType="System.Int16" Type="Number" value="0">
            <numberformat  decimaldigits="0" allowrounding="False" />
            </telerik:RadNumericTextBox>
        </div>
</div>
<div class="StudentInfoRow" style="margin:15px;clear:both;">
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Citizenship</div>
                    <telerik:RadComboBox ID="RadComboBoxCitizenship" runat="server"   font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="Citizen" Selected="true" />
                            <telerik:RadComboBoxItem runat="server" Text="Elgible"  />
                            <telerik:RadComboBoxItem runat="server" Text="Non-Citizen"  />
                        </Items>
                </telerik:RadComboBox>
        </div>
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Nationality</div>
                    <telerik:RadComboBox ID="RadComboBoxdNationality" runat="server"  font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="American" Selected="true"    />
                            <telerik:RadComboBoxItem runat="server" Text="Non-American"/>
                        </Items>
                </telerik:RadComboBox>
        </div>
         <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Race</div>
                    <telerik:RadComboBox ID="RadComboBoxr" runat="server"  font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="White, Non Hispanic"  Selected="true"   />
                            <telerik:RadComboBoxItem runat="server" Text="Black, Non Hispanic"/>
                            <telerik:RadComboBoxItem runat="server" Text="Hispanic"/>
                            <telerik:RadComboBoxItem runat="server" Text="Non Resident Alien" />
                        </Items>
                </telerik:RadComboBox>
</div>
</div>
<div class="StudentInfoRow" style="margin:15px;clear:both;">
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Dependency Type</div>
                    <telerik:RadComboBox ID="RadComboBoxdDepType" runat="server"  font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="Dependent"   />
                            <telerik:RadComboBoxItem runat="server" Text="Independent" Selected="true" />
                            <telerik:RadComboBoxItem runat="server" Text="Unknown"/>
                        </Items>
                </telerik:RadComboBox>
        </div>
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Housing Type</div>
                    <telerik:RadComboBox ID="RadComboBoxht" runat="server"   font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="All"  Selected="true"  />
                        </Items>
                </telerik:RadComboBox>
        </div>
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Geography Type</div>
                    <telerik:RadComboBox ID="RadComboBoxGeoType" runat="server"   font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="Urban"  Selected="true" />
                        </Items>
                </telerik:RadComboBox>
        </div>
</div>
<div class="StudentInfoRow"  style="margin:15px;clear:both;"> 
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Admin Criteria</div>
                    <telerik:RadComboBox ID="RadComboBoxAC" runat="server"   font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="All"   Selected="true" />
                        </Items>
                </telerik:RadComboBox>
        </div>
        <div class="StudentInfo_ElePositions">
                        <div style="font-size:small;">Company Sponsor</div>
                    <telerik:RadComboBox ID="RadComboBoxCompanySponsor" runat="server"   font-size="Small">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--"/>
                            <telerik:RadComboBoxItem runat="server" Text="Veteran's Benefit"  />
                            <telerik:RadComboBoxItem runat="server" Text="Google"  />
                            <telerik:RadComboBoxItem runat="server" Text="Microsoft"  />
                             <telerik:RadComboBoxItem runat="server" Text="None" Selected="true" />
                        </Items>
                </telerik:RadComboBox>
        </div>
</div>    
    </asp:Panel>

    <asp:Panel ID="Panel3" style="margin-top:15px;clear:both;" runat="server">
        <div class="StudentInfoRow" style="margin:15px 15px 15px 15px;clear:both;">
            <h4 style="margin:5px;">Custom Fields</h4>
            <div class="StudentInfo_ElePositions">
            <div style="font-size:small;">Custom 1</div><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
            ErrorMessage="Custom 1 is a required field." ControlToValidate="RadTextBox4444" ValidationGroup="Main" Display="None"></asp:RequiredFieldValidator>
            <div><telerik:RadTextBox ID="RadTextBox4444" runat="server" font-size="Small" Width="300px">
            </telerik:RadTextBox ></div>
        </div>

        <div class="StudentInfo_ElePositions">
            <div style="font-size:small;">Custom 2</div><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
            ErrorMessage="Custom 2 is a required field." ControlToValidate="RadTextBox5555" ValidationGroup="Main" Display="None"></asp:RequiredFieldValidator>
            <div><telerik:RadTextBox ID="RadTextBox5555" runat="server" font-size="Small" Width="300px">
            </telerik:RadTextBox></div>
        </div>
        </div>

        <div class="StudentInfoRow" style="margin:15px 15px 15px 15px;clear:both;">
            <div class="StudentInfo_ElePositions">
            <div style="font-size:small;">Custom 3</div><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
            ErrorMessage="Custom 3 is a required field." ControlToValidate="RadTextBox4" ValidationGroup="Main" Display="None"></asp:RequiredFieldValidator>
            <div><telerik:RadTextBox ID="RadTextBox4" runat="server" font-size="Small" Width="300px">
            </telerik:RadTextBox >
        </div>
        </div>

        <div class="StudentInfo_ElePositions">
        <div style="font-size:small;">Custom 4</div><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
        ErrorMessage="Custom 4 is a required field." ControlToValidate="RadTextBox4" ValidationGroup="Main" Display="None"></asp:RequiredFieldValidator>
        <div><telerik:RadTextBox ID="RadTextBox5" runat="server" font-size="Small" Width="300px">
        </telerik:RadTextBox></div>
        </div>
        </div>
        <%-- <asp:Button ID="Button1" runat="server" Text="Save" ValidationGroup="Main" />--%>
    </asp:Panel>

