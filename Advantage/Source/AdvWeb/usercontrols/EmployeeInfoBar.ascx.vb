﻿

Imports FAME.AdvantageV1.Common

Partial Class UserControls_EmployeeInfoBar
    Inherits System.Web.UI.UserControl
    Private strVID As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal strEmployeeObjectPointer As String)
        strVID = strEmployeeObjectPointer
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        
    End Sub
    'Public Function GetStudentImagePath(ByVal strStudentId As String) As String
    '    Dim facade As New DocumentManagementFacade
    '    Dim strDocumentType As String = facade.GetLeadPhotoDocumentType(strStudentId, "Photo", "StudentId")
    '    Dim strPath As String = SingletonAppSettings.AppSettings("StudentImagePath") 'SingletonAppSettings.AppSettings("DocumentPath")
    '    Dim strFileNameOnly As String = facade.GetStudentPhotoFileName(strStudentId, "Photo")
    '    Dim strFullPath As String = strPath + strDocumentType + "/" + strFileNameOnly
    '    Return strFullPath

    'End Function
    Protected Sub NavigateToSearchPage(ByVal sender As Object, ByVal e As EventArgs)
        Dim strDesc As String = Server.HtmlEncode("View Existing Employees")
        'Response.Redirect("../sy/employeesearch.aspx?resid=309&mod=HR&cmpid=" + Request.QueryString("cmpid") + "&desc=" + strDesc + "&Type=3" + "&bar=hidden", True)
        Response.Redirect("../sy/employeesearch.aspx?resid=309&mod=HR&cmpid=" + Request.QueryString("cmpid") + "&desc=" + strDesc, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        Dim EmployeeId As String

        EmployeeId = AdvantageSession.MasterEmployeeId
        lblNameValue.Text = AdvantageSession.MasterEmployeeName

        Dim strAddress As String = ""
        If Not AdvantageSession.MasterEmployeeAddress1 Is Nothing Then
            strAddress = AdvantageSession.MasterEmployeeAddress1
            If Not AdvantageSession.MasterEmployeeAddress2 Is Nothing Then
                strAddress &= " " 'Line Break
            End If
        End If
        If Not AdvantageSession.MasterEmployeeAddress2 Is Nothing Then
            strAddress &= AdvantageSession.MasterEmployeeAddress2
        End If
        If Not AdvantageSession.MasterEmployeeCity Is Nothing Then
            If Not AdvantageSession.MasterEmployeeAddress1 Is Nothing Or _
                Not AdvantageSession.MasterEmployeeAddress2 Is Nothing Then
                'strAddress &= "<br>" 'Line Break
                strAddress &= ",  "
            End If
            strAddress &= AdvantageSession.MasterEmployeeCity
        End If
        If Not AdvantageSession.MasterEmployeeState Is Nothing Then
            If Not AdvantageSession.MasterEmployeeCity Is Nothing Then
                strAddress &= ", "
            End If
            strAddress &= AdvantageSession.MasterEmployeeState
        End If
        If Not AdvantageSession.MasterEmployeeZip Is Nothing Then
            If Not AdvantageSession.MasterEmployeeCity Is Nothing Or _
                Not AdvantageSession.MasterEmployeeState Is Nothing Then
                strAddress &= " - "
            End If
            strAddress &= AdvantageSession.MasterEmployeeZip
        End If
        lblAddressValue.Text = strAddress

        Dim strDesc As String = Server.HtmlEncode("View Existing Leads")

        existingStudentsLink.Value = "../sy/employeesearch.aspx?resid=309&mod=HR&cmpid=" + Request.QueryString("cmpid") + "&desc=" + strDesc

        SetTaskRecipientProperties()

    End Sub

    ''' <summary>
    ''' Set properties in session related to task recipient.
    ''' Will be used by Email task window when user clicks the Email link 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetTaskRecipientProperties()

        Dim val As Integer
        val = CInt(System.Enum.Parse(GetType(AdvantageEntityId), AdvantageEntityId.Employees.ToString()))

        AdvantageSession.TaskRecipientId = AdvantageSession.MasterEmployeeId
        AdvantageSession.TaskRecipientTypeId = val.ToString()
        AdvantageSession.TaskRecipientFullName = lblNameValue.Text



        If Request.QueryString("mod") IsNot Nothing Then
            Select Case Request.QueryString("mod").ToString().ToUpper()
                Case "AD"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Admissions.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "AR"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Academics.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "FA"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.FinancialAid.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "HR"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.HumanResources.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "PL"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Placement.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "SA"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.StudentAccounts.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case Else
                    AdvantageSession.TaskRecipientModule = Nothing
            End Select
        End If
    End Sub
End Class
