﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EmployerInfoBar.ascx.vb" Inherits="usercontrols_EmployerInfoBar" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy> 
<telerik:RadToolTipManager ID="RadToolTipManager2" runat="server" Position="Center"
         Animation="Fade" HideEvent="LeaveTargetAndToolTip" RelativeTo="Element"
          EnableShadow="true" ShowCallout="false"
        RenderInPageRoot="true" AnimationDuration="200" >
<%--        OnAjaxUpdate="OnAjaxUpdate" OnClientHide="OnClientHide"--%>
</telerik:RadToolTipManager>
         <script type="text/javascript">
             function itemOpened(s, e) {
                 if ($telerik.isIE8) {
                     // Fix an IE 8 bug that causes the list bullets to disappear (standards mode only)
                     $telerik.$("li", e.get_item().get_element())
     .each(function () { this.style.cssText = this.style.cssText; });
                 }
             }
    
        </script>
<div id="wonderbar" style="width:100%;height:75px;background-color:#E9EDF2;">
<div id="infobar" style="width:100%;">
    <asp:Table ID="tblLeadBar" runat="server" Width="100%" BorderStyle="None" style="padding-left: 10px; padding-top: 5px;">
        <asp:TableRow>
            <asp:TableCell Width="60%">
                    <asp:Table ID="tblLeadInfoBar" runat="server" Width="100%" >
                        <asp:TableRow>
                            <asp:TableCell Width="20px" >
                                <asp:LinkButton runat="server" ID="imgSearch"  ToolTip="Search for Employers" OnClick="NavigateToSearchPage" CausesValidation="false">
                                    <span class="k-icon k-i-search font-black"></span>
                                </asp:LinkButton>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                
                                <asp:Label ID="lblName" runat="server" Text="Employer:" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblNameValue" runat="server" style="padding-left:4px;"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell >
                                <a href="javascript:void(0);"  onclick="openTaskRadWindow();" id="img2" runat="server"  title="Send Email" >
                                    <span class="k-icon k-i-email font-black"></span>
                                </a>
                            </asp:TableCell>
                              <asp:TableCell Wrap="false" VerticalAlign="Top" >
                                <asp:Label ID="lblIdentifier" runat="server" Text="Address:" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblAddressValue" runat="server"></asp:Label>
                              </asp:TableCell>
                              <asp:TableCell>
                               
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell >
                                &nbsp;
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" VerticalAlign="Top" >
                                <asp:Label ID="Label1" runat="server" Text="Phone:" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblPhone" runat="server"></asp:Label>
                              </asp:TableCell>
                              <asp:TableCell VerticalAlign="Top">
                                
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
    </div>