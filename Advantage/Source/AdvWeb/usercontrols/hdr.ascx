﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdr.ascx.vb" Inherits="usercontrols_hdr" %>
<%@ Register TagPrefix="fame" TagName="WonderBar" Src="~/UserControls/WonderBar.ascx" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy> 
<div id="header" style="width:100%;height:130px;background-color:#FFFFFF">
<div id="othercontrolcontainer" style="height:100px;">
    <asp:Image ID="SchoolLogo" runat="server"  AlternateText="Paul Mitchell The School" Visible="false"
     ImageUrl="~/images/LogoRoss.jpg" style="margin:   10px 10px 5px 10px;"  height="75px" />
<fame:WonderBar ID="MyWonderBar" runat="server" />
    <telerik:RadDockLayout runat="server" ID="StudentSearchWidgetLayout">
    <telerik:RadDockZone runat="server" ID="StudentSearchWidgetZone" Orientation="Horizontal" 
    MinHeight="30px" Width="100%"  FitDocks="false" HighlightedCssClass="DockZoneHighLight" BorderStyle="None">
       
        <telerik:RadDock runat="server" ID="RadDock1" Title="J. Smith" ToolTip="John Smith"  CloseText="Close"
        DockHandle="TitleBar" Skin="Telerik"  Width="163px" DockMode="Docked" DefaultCommands="Close" Collapsed="true">
        </telerik:RadDock>
        <telerik:RadDock runat="server" ID="RadDock2" Title="B. Shanblatt" ToolTip="Bruce Shanblatt"
         DockHandle="TitleBar"    Width="163px" DockMode="Docked" DefaultCommands="Close" Collapsed="true">
        </telerik:RadDock>
        <telerik:RadDock runat="server" ID="RadDock3" Title="B. Govindarajulu" ToolTip="Balaji Govindarajulu"
         DockHandle="TitleBar"    Width="163px" DockMode="Docked" DefaultCommands="Close" Collapsed="true">
        </telerik:RadDock>
        <telerik:RadDock runat="server" ID="RadDock4" Title="C. Kagoo" ToolTip="Cynthia Kagoo"
         DockHandle="TitleBar"    Width="163px" DockMode="Docked" DefaultCommands="Close" Collapsed="true">
        </telerik:RadDock>
        <telerik:RadDock runat="server" ID="RadDock6" Title="M. McHenry" ToolTip="Mitch McHenry"
         DockHandle="TitleBar"    Width="163px" DockMode="Docked" DefaultCommands="Close" Collapsed="true">
        </telerik:RadDock>
       <telerik:RadDock runat="server" ID="RadDock7" Title="S. Lakshmanan" ToolTip="Sarasathi Lakshmanan"
         DockHandle="TitleBar"    Width="163px" DockMode="Docked" DefaultCommands="Close" Collapsed="true">
        </telerik:RadDock>
    </telerik:RadDockZone>
        </telerik:RadDockLayout>
</div>
                        <telerik:RadToolBar ID="AdvToolBar" runat="server" Width="100%" > 
    <Items>
    <telerik:RadToolBarSplitButton runat="server" Text="Modules">
    <Buttons>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Admissions" ImageUrl="images/admissions.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Academics" ImageUrl="images/academic.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Accounts" ImageUrl="images/studentaccounts.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Faculty" ImageUrl="images/faculty.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Fin Aid" ImageUrl="images/finaid2.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Placement" ImageUrl="images/placement.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="HR" ImageUrl="images/hr2.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server"  Owner="" Text="System" ImageUrl="images/gear1.png">
    </telerik:RadToolBarButton>
    </Buttons>
    </telerik:RadToolBarSplitButton>
    <telerik:RadToolBarButton runat="server" Text="sep1" IsSeparator="true">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarDropDown runat="server" Text="Common Tasks">
    </telerik:RadToolBarDropDown>
    <telerik:RadToolBarDropDown runat="server" Text="Maintenance">
    </telerik:RadToolBarDropDown>
    <telerik:RadToolBarDropDown runat="server" Text="Reports">
    </telerik:RadToolBarDropDown>
    <telerik:RadToolBarButton runat="server" Text="sep3" IsSeparator="true">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarSplitButton runat="server" Text="Actions">
    <Buttons>
    <telerik:RadToolBarButton runat="server" Owner="" Text="App &amp; Tasks" ImageUrl="images/appointment.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Email" ImageUrl="images/email.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Messages" ImageUrl="images/messages.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="Documents" ImageUrl="images/docs.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server" Owner="" Text="History" ImageUrl="images/history.png">
    </telerik:RadToolBarButton>
    </Buttons>
    </telerik:RadToolBarSplitButton>
    <telerik:RadToolBarButton runat="server" Text="sep4" IsSeparator="true">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton>
    <ItemTemplate>
    <telerik:RadComboBox ID="DDLStudentSearch" Runat="server" ValidationGroup="studentsearch"     
    ShowMoreResultsBox="true"
    EnableVirtualScrolling="True" ItemsPerRequest="10" AllowCustomText="False" 
    LoadingMessage="Searching..." 
    ShowToggleImage="False" Height="190px"        
    Width="325px" DropDownWidth="325px" OnItemsRequested="DDLStudentSearch_ItemsRequested" 
    onselectedindexchanged="DDLStudentSearch_SelectedIndexChanged" 
    OnItemDataBound="DDLStudentSearch_ItemDataBound"
    AutoPostBack="true"
    EnableLoadOnDemand="True" NoWrap="True"
    EnableEmbeddedScripts="True" 
    MarkFirstMatch="True" HighlightTemplatedItems="True" EmptyMessage="Search Student Name or SSN">
    <HeaderTemplate>
    <ul>
    <li class="col1">
    <asp:Label ID="hdrlblFullName" runat="server" Text="Full Name"></asp:Label></li>
    <li class="col2">
    <asp:Label ID="hdrlblStuNumType" runat="server" Text="SSN"></asp:Label></li>
    </ul>
    </HeaderTemplate>
    <ItemTemplate>
    <ul>
    <li class="col1">                    
    <asp:Label ID="lblFullName" runat="server" Text="Full Name"></asp:Label></li>
    <li class="col2">
    <asp:Label ID="lblSSN" runat="server" Text="SSN"></asp:Label></li>
    </ul>
    </ItemTemplate>
    </telerik:RadComboBox>
    
    </ItemTemplate>
    </telerik:RadToolBarButton>

        </Items>
    </telerik:RadToolBar>
                <telerik:RadContextMenu ID="RadContextMenu1" runat="server"> 
        <Items> 
            <telerik:RadMenuItem runat="server" Text="Root RadMenuItem1"> 
                <Items> 
                    <telerik:RadMenuItem runat="server" Text="Child RadMenuItem 1"> 
                    <Items>
                    <telerik:RadMenuItem runat="server" Text="GrandChild RadMenuItem 1"></telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="GrandChild RadMenuItem 2"></telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="GrandChild RadMenuItem 3"></telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="GrandChild RadMenuItem 4"></telerik:RadMenuItem>
                    </Items>
                    </telerik:RadMenuItem> 
                    <telerik:RadMenuItem runat="server" Text="Child RadMenuItem 2"> 
                    </telerik:RadMenuItem> 
                </Items> 
            </telerik:RadMenuItem> 
            <telerik:RadMenuItem runat="server" Text="Root RadMenuItem2"> 
                <Items> 
                    <telerik:RadMenuItem runat="server" Text="Child RadMenuItem 1"> 
                    </telerik:RadMenuItem> 
                    <telerik:RadMenuItem runat="server" Text="Child RadMenuItem 2"> 
                    </telerik:RadMenuItem> 
                </Items> 
            </telerik:RadMenuItem> 
            <telerik:RadMenuItem runat="server" Text="Root RadMenuItem3"> 
                <Items> 
                    <telerik:RadMenuItem runat="server" Text="Child RadMenuItem 1"> 
                    </telerik:RadMenuItem> 
                    <telerik:RadMenuItem runat="server" Text="Child RadMenuItem 2"> 
                    </telerik:RadMenuItem> 
                </Items> 
            </telerik:RadMenuItem> 
        </Items> 
    </telerik:RadContextMenu>
</div>
