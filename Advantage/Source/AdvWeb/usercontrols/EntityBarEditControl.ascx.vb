﻿Imports Telerik.Web.UI

Partial Class usercontrols_EntityBarEditControl
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txtTabDisplayName.Focus()
        If Not Page.IsPostBack Then
            EditPanelMsg.Text = ""
        End If
    End Sub

    Protected Sub lstEditSavedPages_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles lstEditSavedPages.SelectedIndexChanged
        txtTabDisplayName.Enabled = True
        btnAddTab.Visible = True
        btnAddTab.Text = "Rename"
        EditPanelMsg.Text = ""
        txtTabDisplayName.Text = lstEditSavedPages.SelectedItem.Text
    End Sub
    Protected Sub btnAddTab_Click(sender As Object, e As System.EventArgs) Handles btnAddTab.Click
        If btnAddTab.Text = "Add" Then
            Dim myitem As New RadListBoxItem(txtTabDisplayName.Text, Request.RawUrl)
            lstEditSavedPages.Items.Insert(0, myitem)
            lstEditSavedPages.SelectedIndex = 0
            btnAddTab.Text = "Rename"
            'EditPanelMsg.Text = "<h3>Tab Added</h3>"
        Else

            lstEditSavedPages.Items(lstEditSavedPages.SelectedIndex).Text = txtTabDisplayName.Text
            btnAddTab.Text = "Add"
            txtTabDisplayName.Text = ""
            'EditPanelMsg.Text = "<h3>Tab Renamed</h3>"
        End If
    End Sub
    Protected Sub lstEditSavedPages_Deleted(sender As Object, e As Telerik.Web.UI.RadListBoxEventArgs) Handles lstEditSavedPages.Deleted
        If lstEditSavedPages.Items.Count = 0 Then
            btnAddTab.Text = "Add"
        End If
        txtTabDisplayName.Text = ""
        txtTabDisplayName.Enabled = False
        btnAddTab.Visible = False
    End Sub
    Protected Sub lstEditSavedPages_Deleting(sender As Object, e As Telerik.Web.UI.RadListBoxEventArgs) Handles lstEditSavedPages.Deleting

    End Sub
    Protected Sub btnDone_Click(sender As Object, e As System.EventArgs) Handles btnDone.Click

    End Sub


End Class
