﻿Option Strict On
Imports Telerik.Web.UI
Imports System.Linq
Imports System.Data
Imports System.Collections.Generic
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common
Imports System.Xml

Public Class StudentSearchControl
    Inherits BaseUserControl
    Implements AdvControls.IAdvControl

    Protected Query As IQueryable(Of NewStudentSearch)
    Protected stuid, campusid, enrollid, fullname, termid, termdescrip, academicyearid, academicyeardescrip, hourtype As String
    Protected isAdmin As Boolean = False
    Protected selectedEnrollment As Guid
    Dim MyAdvAppSettings As AdvAppSettings
    Public Event StudentIndexChange(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
#Region "Properties"
    Private _ItemsPerRequest As Integer
    Public _FullName As String
    Public _ShowEnrollment As Boolean
    Public _ShowTerm As Boolean
    Public _ShowAcaYr As Boolean
    Public Property ShowEnrollment() As Boolean
        Get

            Return _ShowEnrollment
        End Get

        Set(ByVal Value As Boolean)

            _ShowEnrollment = Value

            If Value = True Then
                lblEnrollmentId.Visible = True
                ddlEnrollments.Visible = True
            Else
                lblEnrollmentId.Visible = False
                ddlEnrollments.Visible = False
            End If

        End Set
    End Property
    Public Property ShowTerm() As Boolean
        Get

            Return _ShowTerm
        End Get

        Set(ByVal Value As Boolean)

            _ShowTerm = Value

            If Value = True Then
                'Dim LabelControl As Label = CType(FindControl("lblTermId"), Label)
                'LabelControl.Visible = True

                lblTermId.Visible = True
                ddlTerms.Visible = True
            Else
                'Dim LabelControl As Label = CType(FindControl("lblTermId"), Label)
                'LabelControl.Visible = False

                lblTermId.Visible = False
                ddlTerms.Visible = False
            End If

        End Set
    End Property
    Public Property ShowAcaYr() As Boolean
        Get

            Return _ShowAcaYr
        End Get

        Set(ByVal Value As Boolean)

            _ShowAcaYr = Value

            If Value = True Then
                lblAcademicYearId.Visible = True
                ddlAcademicYears.Visible = True
            Else
                lblAcademicYearId.Visible = False
                ddlAcademicYears.Visible = False
            End If

        End Set
    End Property

    Public Property ItemsPerRequest() As Integer
        Get
            Return _ItemsPerRequest
        End Get
        Set(ByVal Value As Integer)
            _ItemsPerRequest = Value
        End Set
    End Property



#End Region
  
    Public Event TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, ByVal hourtype As String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

       campusid = CampusObjects.GetQueryParameterValueStrCampusId(Request) 
 
        'set ddlStudentSearch headers and empty message
        Dim rcbDDLStudentSearch As RadComboBox = Me.DDLStudentSearch
        Dim StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower
        If StudentIdentifier = "ssn" Then
            rcbDDLStudentSearch.EmptyMessage = "Search Name or SSN"
            CType(DDLStudentSearch.Header.FindControl("hdrlblStuNumType"), Label).Text = "SSN"
        ElseIf StudentIdentifier = "enrollmentid" Then
            rcbDDLStudentSearch.EmptyMessage = "Search Name or EnrollmentId"
            CType(DDLStudentSearch.Header.FindControl("hdrlblStuNumType"), Label).Text = "Enrollment ID"
        Else
            rcbDDLStudentSearch.EmptyMessage = "Search Name or Student Number"
            CType(DDLStudentSearch.Header.FindControl("hdrlblStuNumType"), Label).Text = "Student Number"
        End If

        If Not Page.IsPostBack Then
            '
        Else
            QueryStudents(String.Empty)
        End If
        ItemsPerRequest = 10
    End Sub
    Protected Sub DDLStudentSearch_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)

        Dim MyStudents = QueryStudents(e.Text)
        Dim itemOffset As Integer = e.NumberOfItems

        Dim MyStudentsDisplay As System.Linq.IQueryable(Of FAME.Advantage.Common.LINQ.Entities.NewStudentSearch)

        Dim StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower
        If StudentIdentifier = "ssn" Then
            MyStudentsDisplay = (From s In MyStudents
                           Select New NewStudentSearch With {.FirstName = s.FirstName, .LastName = s.LastName, .FullName = s.FullName, .SSN = s.SSN, .StudentId = s.StudentId}).Distinct
        ElseIf StudentIdentifier = "enrollmentid" Then
            MyStudentsDisplay = (From s In MyStudents
                          Select New NewStudentSearch With {.FirstName = s.FirstName, .LastName = s.LastName, .FullName = s.FullName, .EnrollmentId = s.EnrollmentId, .StudentId = s.StudentId}).Distinct
        Else
            MyStudentsDisplay = (From s In MyStudents
                          Select New NewStudentSearch With {.FirstName = s.FirstName, .LastName = s.LastName, .FullName = s.FullName, .StudentNumber = s.StudentNumber, .StudentId = s.StudentId}).Distinct
        End If

        Dim MyStudentsDisplaySorted = (From s In MyStudentsDisplay
                           Order By s.FirstName.Trim Ascending, s.LastName.Trim Ascending)
        'Order By s.LastName.Trim Ascending, s.FirstName.Trim Ascending)

        Dim endOffset As Integer = Math.Min(itemOffset + ItemsPerRequest, MyStudentsDisplaySorted.Count)
        e.EndOfItems = endOffset = MyStudentsDisplaySorted.Count

        Dim StudentList As List(Of NewStudentSearch) = MyStudentsDisplaySorted.ToList
        Dim DisplayedStudents As New List(Of NewStudentSearch)

        For i As Integer = itemOffset To endOffset - 1
            DisplayedStudents.Add(StudentList(i))
        Next
        DDLStudentSearch.DataSource = DisplayedStudents
        DDLStudentSearch.DataTextField = "FullName"
        DDLStudentSearch.DataValueField = "StudentId"
        DDLStudentSearch.DataBind()

        e.Message = GetStatusMessage(endOffset, MyStudentsDisplaySorted.Count)

    End Sub
    Protected Sub DDLStudentSearch_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)

        Dim Student As NewStudentSearch = DirectCast(e.Item.DataItem, NewStudentSearch)
        'Dim Student As StudentEnrollmentSearch = DirectCast(e.Item.DataItem, StudentEnrollmentSearch)
        e.Item.Text = Student.FullName
        e.Item.Value = Student.StudentId.ToString
        Dim NameLabel As Label = CType(e.Item.FindControl("lblFullName"), Label)
        Dim SSNLabel As Label = CType(e.Item.FindControl("lblSSN"), Label)
        'Dim PrgVerLabel As Label = CType(e.Item.FindControl("lblPrgDescrip"), Label)
        'Dim StatusLabel As Label = CType(e.Item.FindControl("lblStatusCodeDescrip"), Label)

        NameLabel.Text = PadDefaultDisplayData(Student.FullName)

        If MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "ssn" Then
            SSNLabel.Text = PadDefaultDisplayData(FormatSSN(Student.SSN))
        ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "enrollmentid" Then
            SSNLabel.Text = PadDefaultDisplayData(Student.EnrollmentId)
        Else
            SSNLabel.Text = PadDefaultDisplayData(Student.StudentNumber)
        End If

        'PrgVerLabel.Text = PadDefaultDisplayData(Student.PrgVerDescrip)
        'StatusLabel.Text = PadDefaultDisplayData(Student.StatusCodeDescrip)
    End Sub
    Private Function PadDefaultDisplayData(ByVal strData As String) As String
        If String.IsNullOrWhiteSpace(strData) = True Then
            strData = "[NO DATA]"
        End If
        Return strData
    End Function
    Private Shared Function GetStatusMessage(ByVal offset As Integer, ByVal total As Integer) As String
        If total <= 0 Then
            Return "No student matches"
        End If

        Return [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total)
    End Function

    Private Function QueryStudents(ByVal SearchText As String) As IQueryable(Of NewStudentSearch)

        Dim DA As New StudentEnrollmentDA(CStr(MyAdvAppSettings.AppSettings("ConnectionString")))

        Try
            Dim UserId As string = (AdvantageSession.UserState.UserId.ToString)
            Dim isSystemAdministrator As Boolean = False
            Dim StudentSearchFacade As New FAME.AdvantageV1.BusinessFacade.StudentSearchFacade
            isSystemAdministrator = StudentSearchFacade.GetIsRoleSystemAdministarator(UserId)


            If isSystemAdministrator = True Or Session("UserName") Is "sa" Then
                isAdmin = True
            End If

            'Dim Users(1) As String
            'Users(0) = UserId.ToString
            Query = DA.SearchStudents(isAdmin)
            Dim MyStudents As IQueryable(Of NewStudentSearch)
            If String.IsNullOrWhiteSpace(SearchText) = True Then
                MyStudents = From s In Query Where s.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By s.LastName.Trim, s.FirstName.Trim
            Else
                Dim StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower
                If StudentIdentifier = "ssn" Then
                    MyStudents = From s In Query Where (s.FullName.Contains(SearchText) Or s.FirstLastName.Contains(SearchText) Or s.SSN.Contains(SearchText)) And s.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By s.LastName.Trim, s.FirstName.Trim
                ElseIf StudentIdentifier = "enrollmentid" Then
                    MyStudents = From s In Query Where (s.FullName.Contains(SearchText) Or s.FirstLastName.Contains(SearchText) Or s.EnrollmentId.Contains(SearchText)) And s.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By s.LastName.Trim, s.FirstName.Trim
                Else
                    MyStudents = From s In Query Where (s.FullName.Contains(SearchText) Or s.FirstLastName.Contains(SearchText) Or s.StudentNumber.Contains(SearchText)) And s.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By s.LastName.Trim, s.FirstName.Trim
                End If

            End If

            Return MyStudents

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex

        End Try

    End Function

    Protected Sub DDLStudentSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        stuid = e.Value
        BuildDropDowns(e.Value)
        ddlEnrollments.Enabled = True
        ddlTerms.Enabled = True
        ddlAcademicYears.Enabled = True

       RaiseEvent StudentIndexChange(sender, e)

    End Sub

    Protected Sub BuildDropDowns(ByVal studentid As String)

        Try
            'campusid = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString

            If studentid = "" Then
                'Dim sArray() As String = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Split(CChar("/"))
                'Response.Redirect(sArray(6), True)
                Response.Redirect(System.Web.HttpContext.Current.Request.Url.ToString(), True)
            End If

            'BuildEnrollmentDDL(studentid)


            Dim MyStudentsCurrent As IQueryable(Of NewStudentSearch)
            Dim MyStudentsNonCurrent As IQueryable(Of NewStudentSearch)

            MyStudentsCurrent = (From s In Query Where s.StudentId.Equals(New Guid(studentid)) _
                          Order By s.IsCurrentEnrollment, s.StartDate Descending _
                          Where s.IsCurrentEnrollment = 1
                          Select s)

            MyStudentsNonCurrent = (From s In Query Where s.StudentId.Equals(New Guid(studentid)) _
                          Order By s.IsCurrentEnrollment, s.StartDate Descending _
                          Where s.IsCurrentEnrollment = 0
                          Select s)

            'create table to load students  
            Dim dtStuEnrollments As New DataTable("students")
            dtStuEnrollments.Columns.Add("PrgVerId")
            dtStuEnrollments.Columns.Add("PrgVerDescripandStatus")
            dtStuEnrollments.Columns.Add("StuEnrollId")
            dtStuEnrollments.Columns.Add("StartDate")
            dtStuEnrollments.Columns.Add("StudentId")
            dtStuEnrollments.Columns.Add("IsCurrEnroll")
            dtStuEnrollments.Columns.Add("HourType")

            'if the user is not an sa or admin
            If isAdmin = False Then
                'first we check to see if we have More then one current enrollment
                If MyStudentsCurrent.Count > 0 Then
                    'since we have current enrollment(s) we will show them only
                    For Each student As NewStudentSearch In MyStudentsCurrent
                        dtStuEnrollments.Rows.Add(New Object() {student.PrgVerId, (student.PrgVerDescrip & " / " & student.StatusCodeDescrip), student.StuEnrollId, student.StartDate, student.StudentId, student.IsCurrentEnrollment, student.HourType})
                    Next
                Else
                    'we have only non current enrollments - show em
                    For Each student As NewStudentSearch In MyStudentsNonCurrent
                        dtStuEnrollments.Rows.Add(New Object() {student.PrgVerId, (student.PrgVerDescrip & " / " & student.StatusCodeDescrip), student.StuEnrollId, student.StartDate, student.StudentId, student.IsCurrentEnrollment, student.HourType})
                    Next

                End If
            Else
                'if the user is an sa or admin give them all enrollments
                Dim MyStudents As IQueryable(Of NewStudentSearch) = _
                    (From s In Query Where s.StudentId.Equals(New Guid(studentid))
                     Select s)
                For Each student In MyStudents
                    dtStuEnrollments.Rows.Add(New Object() {student.PrgVerId, (student.PrgVerDescrip & " / " & student.StatusCodeDescrip), student.StuEnrollId, student.StartDate, student.StudentId, student.IsCurrentEnrollment, student.HourType})
                Next

            End If

            hourtype = dtStuEnrollments.Rows(0).Item("hourtype").ToString()

            'bind the enrollment/status dropdown
            With ddlEnrollments
                .DataTextField = "PrgVerDescripandStatus"
                .DataValueField = "StuEnrollId"
                .DataSource = dtStuEnrollments
                .DataBind()
                .SelectedIndex = 0
            End With

            enrollid = ddlEnrollments.SelectedValue.ToString()
            fullname = Me.DDLStudentSearch.Text.ToString()

            RaiseEvent TransferToParent(enrollid, fullname, termid, termdescrip, academicyearid, academicyeardescrip, hourtype)

            BuildTermsDDL(New Guid(ddlEnrollments.SelectedValue), CDate(dtStuEnrollments.Rows(ddlEnrollments.SelectedIndex)("startdate")), dtStuEnrollments.Rows(ddlEnrollments.SelectedIndex)("studentid").ToString)

            Session("stuenrollid") = ddlEnrollments.SelectedValue


            BuildAcademicYearsDDL()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex

        End Try

    End Sub

    Protected Sub BuildTermsDDL(ByVal stuenrollid As Guid, ByVal startdate As Date, ByVal studentid As String)
        'build terms dropdown
        Dim ResultsTermWith As IEnumerable(Of USP_SA_GetStudentTerms_WithTermsResult)
        Dim ResultsTermWithout As IEnumerable(Of USP_SA_GetStudentTerms_WithoutTermsResult)
        Dim DATerm As New TermDA(CStr(MyAdvAppSettings.AppSettings("ConnectionString")))
        Dim dtTerms As New DataTable("terms")
        dtTerms.Columns.Add("TermId")
        dtTerms.Columns.Add("TermDescrip")
        dtTerms.Columns.Add("stuenrollid")

        ResultsTermWith = DATerm.GetTermsForStudentEnrollments_WithTerms(stuenrollid, New Guid(CStr(campusid)), startdate).ToList()

        If ResultsTermWith.Count = 0 Then
            ResultsTermWithout = DATerm.GetTermsForStudentEnrollments_WithoutTerms(stuenrollid, New Guid(CStr(studentid)), New Guid(CStr(campusid)), startdate, isAdmin).ToList()
            If ResultsTermWithout.Count = 0 Then
                dtTerms.Rows.Add(New Object() {String.Empty, String.Empty, stuenrollid})
                'Exit Sub
            End If

            For Each datarow As USP_SA_GetStudentTerms_WithoutTermsResult In ResultsTermWithout
                dtTerms.Rows.Add(New Object() {datarow.TermId, datarow.TermDescrip, stuenrollid})
            Next
        Else
            For Each datarow As USP_SA_GetStudentTerms_WithTermsResult In ResultsTermWith
                dtTerms.Rows.Add(New Object() {datarow.TermId, datarow.TermDescrip, stuenrollid})
            Next
        End If

        With ddlTerms
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = dtTerms
            .DataBind()
            .SelectedIndex = 0
        End With
        fullname = Me.DDLStudentSearch.Text.ToString()
        termid = ddlTerms.SelectedValue.ToString()
        termdescrip = ddlTerms.SelectedItem.Text.ToString()
       
        RaiseEvent TransferToParent(stuenrollid.ToString, fullname, termid, termdescrip, academicyearid, academicyeardescrip, hourtype)
    End Sub

    Protected Sub BuildAcademicYearsDDL()
        'build academic years dropdown
        Dim DAAcadYears As New AcademicYearsDA(CStr(MyAdvAppSettings.AppSettings("ConnectionString")))
        'Dim ResultsAcadYears As IQueryable(Of saAcademicYear)
       Dim ResultsAcadYears = DAAcadYears.GetAcademicYearsOrdered()
        Dim dtAcadYears As New DataTable("acadyears")
        dtAcadYears.Columns.Add("AcademicYearId")
        dtAcadYears.Columns.Add("AcademicYearDescrip")
        For Each acadyear As saAcademicYear In ResultsAcadYears
            dtAcadYears.Rows.Add(New Object() {acadyear.AcademicYearId, acadyear.AcademicYearDescrip})
        Next
        With ddlAcademicYears
            .DataTextField = "AcademicYearDescrip"
            .DataValueField = "AcademicYearId"
            .DataSource = dtAcadYears
            .DataBind()
            .SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYears.Items)

            enrollid = ddlEnrollments.SelectedValue.ToString()
            fullname = Me.DDLStudentSearch.Text.ToString()
            termid = ddlTerms.SelectedValue.ToString()
            termdescrip = ddlTerms.SelectedItem.Text.ToString()
            academicyearid = ddlAcademicYears.SelectedValue.ToString()
            academicyeardescrip = ddlAcademicYears.SelectedItem.Text.ToString()
            RaiseEvent TransferToParent(enrollid, fullname, termid, termdescrip, academicyearid, academicyeardescrip, hourtype)
        End With

    End Sub

    Protected Sub ddlEnrollments_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles ddlEnrollments.ItemDataBound
        Dim iIndex As Integer = e.Item.Text.LastIndexOf("/")
        Dim iLen As Integer = e.Item.Text.Length
        Dim EnrollmentLabel As Label = CType(e.Item.FindControl("lblEnrollment"), Label)
        Dim StatusLabel As Label = CType(e.Item.FindControl("lblStatus"), Label)

        EnrollmentLabel.Text = e.Item.Text.Substring(0, iIndex)
        StatusLabel.Text = e.Item.Text.Substring(iIndex + 1)

        Dim dataSourceRow As DataRowView = CType(e.Item.DataItem, DataRowView)
        'set custom attributes from the datasource:
        e.Item.Attributes("StartDate") = dataSourceRow("StartDate").ToString()
        e.Item.Attributes("StudentId") = dataSourceRow("StudentId").ToString()


    End Sub

    Protected Sub ddlEnrollments_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnrollments.DataBound
        'enrollid = CType(sender, RadComboBox).SelectedValue.ToString()
        'fullname = Me.DDLStudentSearch.Text.ToString()

        'RaiseEvent TransferToParent(enrollid, fullname, termid, termdescrip, academicyearid, academicyeardescrip)
    End Sub

    Protected Sub ddlTerms_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerms.DataBound
        'termid = CType(sender, RadComboBox).SelectedValue.ToString()
        'termdescrip = CType(sender, RadComboBox).SelectedItem.Text.ToString()
        'RaiseEvent TransferToParent(enrollid, fullname, termid, termdescrip, academicyearid, academicyeardescrip)
    End Sub

    Protected Sub ddlAcademicYears_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYears.DataBound
        'academicyearid = CType(sender, RadComboBox).SelectedValue.ToString()
        'academicyeardescrip = CType(sender, RadComboBox).SelectedItem.Text.ToString()
        'RaiseEvent TransferToParent(enrollid, fullname, termid, termdescrip, academicyearid, academicyeardescrip)
    End Sub

    Protected Sub ddlEnrollments_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlEnrollments.SelectedIndexChanged
        RaiseEvent TransferToParent( _
                                    CType(o, RadComboBox).SelectedValue.ToString(), _
                                    CType(FindControl("DDLStudentSearch"), RadComboBox).Text.ToString(), _
                                    CType(FindControl("ddlTerms"), RadComboBox).SelectedValue.ToString(), _
                                    CType(FindControl("ddlTerms"), RadComboBox).SelectedItem.Text.ToString(), _
                                    CType(FindControl("ddlAcademicYears"), RadComboBox).SelectedValue.ToString(), _
                                    CType(FindControl("ddlAcademicYears"), RadComboBox).SelectedItem.Text.ToString(), _
                                    hourtype
                                   )

        enrollid = ddlEnrollments.SelectedValue.ToString()
        fullname = Me.DDLStudentSearch.Text.ToString()
        BuildTermsDDL(New Guid(ddlEnrollments.SelectedValue), CDate(ddlEnrollments.SelectedItem.Attributes("StartDate")), ddlEnrollments.SelectedItem.Attributes("StudentId").ToString)
        BuildAcademicYearsDDL()
    End Sub

    Protected Sub ddlTerms_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlTerms.SelectedIndexChanged
        RaiseEvent TransferToParent( _
                                    CType(FindControl("ddlEnrollments"), RadComboBox).SelectedValue.ToString(), _
                                    CType(FindControl("DDLStudentSearch"), RadComboBox).Text.ToString(), _
                                    CType(o, RadComboBox).SelectedValue.ToString(), _
                                    CType(o, RadComboBox).SelectedItem.Text.ToString(), _
                                    CType(FindControl("ddlAcademicYears"), RadComboBox).SelectedValue.ToString(), _
                                     CType(FindControl("ddlAcademicYears"), RadComboBox).SelectedItem.Text.ToString(), _
                                    hourtype
                                   )

    End Sub

    Protected Sub ddlAcademicYears_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlAcademicYears.SelectedIndexChanged
        RaiseEvent TransferToParent( _
                                    CType(FindControl("ddlEnrollments"), RadComboBox).SelectedValue.ToString(), _
                                    CType(FindControl("DDLStudentSearch"), RadComboBox).Text.ToString(), _
                                    CType(FindControl("ddlTerms"), RadComboBox).SelectedValue.ToString(), _
                                    CType(FindControl("ddlTerms"), RadComboBox).SelectedItem.Text.ToString(), _
                                    CType(o, RadComboBox).SelectedValue.ToString(), _
                                    CType(o, RadComboBox).SelectedItem.Text.ToString(), _
                                    hourtype
                                   )

    End Sub

    Protected Function FormatSSN(ByVal SSN As String) As String
        If Not SSN = String.Empty Then
            Return Mid(SSN, 1, 3) & "-" & Mid(SSN, 4, 2) & "-" & Mid(SSN, 6, 4)
            'Return "*****" & Mid(SSN, 6, 4)
        Else
            Return String.Empty
        End If
    End Function
    Public Sub Clear() Implements AdvControls.IAdvControl.Clear

        'This is how you clear the selection when AllowCustomText = false
        DDLStudentSearch.ClearSelection()
        DDLStudentSearch.Text = String.Empty

        'This is how you clear the selection when AllowCustomText = true or has no value
        ddlEnrollments.Items.Clear()
        ddlEnrollments.Text = String.Empty
        ddlEnrollments.Enabled = False

        'This is how you clear the selection when AllowCustomText = true or has no value
        ddlTerms.Items.Clear()
        ddlTerms.Text = String.Empty
        ddlTerms.Enabled = False

        'This is how you clear the selection when AllowCustomText = true or has no value
        ddlAcademicYears.Items.Clear()
        ddlAcademicYears.Text = String.Empty
        ddlAcademicYears.Enabled = False
    End Sub

    Public Function UpdateSearchControl() As String
        Dim enrollmentIndex As Integer = ddlEnrollments.SelectedIndex

        BuildDropDowns(DDLStudentSearch.SelectedValue)

        ddlEnrollments.SelectedIndex = enrollmentIndex

        ddlEnrollments.Enabled = True
        ddlTerms.Enabled = True
        ddlAcademicYears.Enabled = True

        Return ddlEnrollments.SelectedValue.ToString()

    End Function
End Class