﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeadSearchControl.ascx.vb"
    Inherits="LeadSearchControl" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="DDLLeadSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<asp:Panel ID="SearchPanel" runat="server">
    <div class="contenttable" style="width: 100%;">
        <table runat="server" id="leadsearchtbl" width="100%" cellpadding="0" cellspacing="0"
            class="contenttable">
            <tr>
               <%-- <td class="contentcell" nowrap="nowrap">
                    <asp:Label ID="lblLead" runat="server" CssClass="label">Lead</asp:Label>
                </td>--%>
                <td>
                    <telerik:RadComboBox ID="DDLLeadSearch" runat="server" ValidationGroup="ucValGrp"
                        ShowMoreResultsBox="true" EnableVirtualScrolling="True" ItemsPerRequest="10"
                        AllowCustomText="False" LoadingMessage="Searching..." ShowToggleImage="False"
                        Height="205px" Width="325px" DropDownWidth="325px" OnItemsRequested="DDLLeadSearch_ItemsRequested"
                        OnSelectedIndexChanged="DDLLeadSearch_SelectedIndexChanged" OnItemDataBound="DDLLeadSearch_ItemDataBound"
                        AutoPostBack="true" EnableLoadOnDemand="True" NoWrap="True" EnableEmbeddedScripts="True"
                        MarkFirstMatch="True" HighlightTemplatedItems="True">
                        <HeaderTemplate>
                            <ul>
                                <li class="col1">
                                    <asp:Label ID="hdrlblFullName" runat="server" Text="Full Name"></asp:Label></li>
                            </ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <ul>
                                <li class="col1">
                                    <asp:Label ID="lblFullName" runat="server" Text="Full Name"></asp:Label></li>
                            </ul>
                        </ItemTemplate>
                    </telerik:RadComboBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="ucValGrp"
                                ErrorMessage="Lead can not be blank " ControlToValidate="DDLLeadSearch" />
                </td>
            </tr>
        </table>
    </div>
      
   <%-- <asp:Literal ID="Literal1" runat="server"></asp:Literal>--%>
</asp:Panel>
