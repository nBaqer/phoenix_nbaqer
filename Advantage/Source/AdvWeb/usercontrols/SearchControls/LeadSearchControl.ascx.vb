﻿Option Strict On
Imports Telerik.Web.UI
Imports System.Linq
Imports System.Collections.Generic
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common
Imports System.Xml

Public Class LeadSearchControl
    Inherits BaseUserControl
    Implements AdvControls.IAdvControl

    Protected Query As IQueryable(Of adLead)
    Protected campusid As String
    Protected leadid, leadname As String
    Protected isAdmin As Boolean = False
    Protected selectedEnrollment As Guid
    Protected MyAdvAppSettings As AdvAppSettings
#Region "Properties"
    Private _ItemsPerRequest As Integer
    Public _FullName As String
    Public _ShowEnrollment As Boolean
    Public _ShowTerm As Boolean
    Public _ShowAcaYr As Boolean

    Public Property ItemsPerRequest() As Integer
        Get
            Return _ItemsPerRequest
        End Get
        Set(ByVal Value As Integer)
            _ItemsPerRequest = Value
        End Set
    End Property

#End Region

    Public Event TransferLeadSearch(ByVal leadid As String, ByVal leadname As String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        campusid = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString


        Dim rcbDDLLeadSearch As RadComboBox = Me.DDLLeadSearch
        rcbDDLLeadSearch.EmptyMessage = "Search Name"

        If Not Page.IsPostBack Then
            '
        Else
            QueryLeads(String.Empty)
        End If
        ItemsPerRequest = 10
    End Sub
    Protected Sub DDLLeadSearch_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)

        Dim MyLeads = QueryLeads(e.Text)
        Dim itemOffset As Integer = e.NumberOfItems

        Dim MyLeadsDisplay As System.Linq.IQueryable(Of FAME.Advantage.Common.LINQ.Entities.adLead)

        MyLeadsDisplay = (From l In MyLeads).Distinct
        Dim MyLeadsDisplaySorted = (From l In MyLeadsDisplay
                           Order By l.LastName.Trim Ascending, l.FirstName.Trim Ascending)
        Dim endOffset As Integer = Math.Min(itemOffset + ItemsPerRequest, MyLeadsDisplaySorted.Count)
        e.EndOfItems = endOffset = MyLeadsDisplaySorted.Count
        Dim LeadList As List(Of adLead) = MyLeadsDisplaySorted.ToList
        Dim DisplayedLeads As New List(Of adLead)
        For i As Integer = itemOffset To endOffset - 1
            DisplayedLeads.Add(LeadList(i))
        Next
        DDLLeadSearch.DataSource = DisplayedLeads
        DDLLeadSearch.DataTextField = "FirstName"
        DDLLeadSearch.DataValueField = "LeadId"
        DDLLeadSearch.DataBind()
        e.Message = GetStatusMessage(endOffset, MyLeadsDisplaySorted.Count)

    End Sub

    Protected Sub DDLLeadSearch_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)

        Dim Lead As adLead = DirectCast(e.Item.DataItem, adLead)
        e.Item.Text = Lead.FirstName & " " & Lead.LastName
        e.Item.Value = Lead.LeadId.ToString
        Dim NameLabel As Label = CType(e.Item.FindControl("lblFullName"), Label)
        NameLabel.Text = PadDefaultDisplayData(Lead.FirstName & " " & Lead.LastName)

    End Sub

    Private Function PadDefaultDisplayData(ByVal strData As String) As String
        If String.IsNullOrWhiteSpace(strData) = True Then
            strData = "[NO DATA]"
        End If
        Return strData
    End Function
    Private Shared Function GetStatusMessage(ByVal offset As Integer, ByVal total As Integer) As String
        If total <= 0 Then
            Return "No student matches"
        End If

        Return [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total)
    End Function

    Private Function QueryLeads(ByVal SearchText As String) As IQueryable(Of adLead)

        Dim DA As New LeadDA(CStr(MyAdvAppSettings.AppSettings("ConnectionString")))
        Try
            Dim UserId As String = (AdvantageSession.UserState.UserId.ToString)
            Dim isSystemAdministrator As Boolean = False
            Dim StudentSearchFacade As New FAME.AdvantageV1.BusinessFacade.StudentSearchFacade
            isSystemAdministrator = StudentSearchFacade.GetIsRoleSystemAdministarator(UserId)


            If isSystemAdministrator = True Or Session("UserName") Is "sa" Then
                isAdmin = True
            End If

            Query = DA.GetLeads()

            Dim MyLeads As IQueryable(Of adLead)
            If String.IsNullOrWhiteSpace(SearchText) = True Then
                MyLeads = From l In Query Where l.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString) Order By l.LastName.Trim, l.FirstName.Trim
            Else
                MyLeads = From l In Query Where ((l.FirstName & " " & l.LastName).Contains(SearchText) And l.CampusId.Equals(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)) Order By l.LastName.Trim, l.FirstName.Trim
            End If

            Return MyLeads

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex

        End Try

    End Function

    Protected Sub DDLLeadSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        leadid = e.Value
        'DE8771
        If leadid = "" Then
            e.Text = ""
            DDLLeadSearch.Text = String.Empty
        End If
        leadname = e.Text
        Try
            RaiseEvent TransferLeadSearch(leadid, leadname)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Public Sub Clear() Implements AdvControls.IAdvControl.Clear

        'This is how you clear the selection when AllowCustomText = false
        DDLLeadSearch.ClearSelection()
        DDLLeadSearch.Text = String.Empty

    End Sub
End Class