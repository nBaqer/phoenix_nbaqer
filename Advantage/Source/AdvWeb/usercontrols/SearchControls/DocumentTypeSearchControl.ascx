﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocumentTypeSearchControl.ascx.cs" Inherits="UsercontrolsSearchControlsTypeSearchControl" %>
<link href="../../css/DocumentTypeSearch.css" rel="stylesheet" />
<telerik:RadComboBox 
    RenderMode="Lightweight" 
    ID="CmbBxDocumentTypeSearch"
    AllowCustomText="True"
    runat="server" 
    ItemsPerRequest="2"
    MaxLength="80"
    MaxHeight="250"
    Width="200px"  
    EmptyMessage="Search or Select Type"
    EnableLoadOnDemand="True"
    EnableItemCaching="true"
    IsCaseSensitive="false"
    LoadingMessage="Loading"
    ShowMoreResultsBox="True"
    Filter="Contains"
    OnItemsRequested="CmbBxDocumentTypeSearch_OnItemsRequested">
</telerik:RadComboBox>

