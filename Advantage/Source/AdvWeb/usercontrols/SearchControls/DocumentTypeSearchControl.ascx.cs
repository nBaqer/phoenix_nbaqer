﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentTypeSearchControl.ascx.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The usercontrols_ search controls_ type search control.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq; 
using Telerik.Web.UI; 
using DataSet = System.Data.DataSet;

/// <inheritdoc />
/// <summary>
/// The UsercontrolsSearchControlsTypeSearchControl search control.
/// </summary>
public partial class UsercontrolsSearchControlsTypeSearchControl : System.Web.UI.UserControl
{  
    #region Properties

    /// <summary>
    /// Gets or sets the data text field.
    /// </summary>
    public string DataTextField
    {
        get
        {
            return this.CmbBxDocumentTypeSearch.DataTextField;
        }

        set
        {
            this.CmbBxDocumentTypeSearch.DataTextField = value;
        }
    }

    /// <summary>
    /// Gets or sets the data value field.
    /// </summary>
    public string DataValueField
    {
        get
        {
            return this.CmbBxDocumentTypeSearch.DataValueField;
        }

        set
        {
            this.CmbBxDocumentTypeSearch.DataValueField = value;
        }
    }

    /// <summary>
    /// Sets the data source.
    /// </summary>
    public DataSet DataSource
    {
        set
        {
            this.CmbBxDocumentTypeSearch.DataSource = value;
        }
    }

    /// <summary>
    /// Gets or sets the selected value.
    /// </summary>
    public string SelectedValue
    {
        get
        {
            return this.CmbBxDocumentTypeSearch.SelectedValue;
        }

        set
        {
            this.CmbBxDocumentTypeSearch.SelectedValue = value;
        }
    }

    /// <summary>
    /// Gets or sets the selected text.
    /// </summary>
    public string SelectedText
    {
        get
        {
            return this.CmbBxDocumentTypeSearch.Text;
        }

        set
        {
            this.CmbBxDocumentTypeSearch.Text = value;
        }
    }

    #endregion

    /// <summary>
    /// The page_ load.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void Page_Load(object sender, EventArgs e)
    { 
    }

    /// <summary>
    /// The Combobox document type search on items requested. This is a handler for OnItemRequested event.
    /// In this event, we are checking the list of item for the search criteria and based on that shows message if no data found.
    /// </summary>
    /// <param name="sender">
    /// The sender, is the component which sends a request for Items.
    /// </param>
    /// <param name="e">
    /// The RadComboBoxItemsRequestedEventArgs, this is a event dispatched by system.
    /// </param>
    protected void CmbBxDocumentTypeSearch_OnItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    { 
        e.Message = ((RadComboBox)sender).Items.Any(x => x.Text.ToLower().Contains(e.Text.ToLower())) ? string.Empty : "No data found";
    }
}