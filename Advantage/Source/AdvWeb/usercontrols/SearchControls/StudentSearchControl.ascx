﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudentSearchControl.ascx.vb"
    Inherits="StudentSearchControl" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">

    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="DDLStudentSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ddlEnrollments">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ddlTerms">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ddlAcademicYears">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>

<%--<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" 
        Skin="Default">
    </telerik:RadAjaxLoadingPanel>--%>

<div class="boxContainer">
    <h3>Student Enrollment Search</h3>
    <asp:Panel ID="SearchPanel" runat="server">

        <div class="contenttable" style="width: 100%;">
            <table runat="server" id="stusearchtbl" width="100%" cellpadding="0" cellspacing="0" class="contenttable">
                <%--<tr><td  style="height:3px;"></td></tr>  --%>
                <tr>
                    <td class="contentcell" nowrap="nowrap" style="width: 90px; vertical-align: middle;">
                        <asp:Label ID="lblStudent" runat="server" CssClass="label">Student</asp:Label>
                    </td>
                    <td class="contentcell4">
                        <%-- <img alt="more info" src="../images/infoCircle.png" />   --%>
                        <telerik:RadComboBox ID="DDLStudentSearch" runat="server" ValidationGroup="ucValGrp"
                            ShowMoreResultsBox="true"
                            EnableVirtualScrolling="True" ItemsPerRequest="10" AllowCustomText="False"
                            LoadingMessage="Searching..."
                            ShowToggleImage="False" Height="205px"
                            Width="325px" DropDownWidth="325px" OnItemsRequested="DDLStudentSearch_ItemsRequested"
                            OnSelectedIndexChanged="DDLStudentSearch_SelectedIndexChanged"
                            OnItemDataBound="DDLStudentSearch_ItemDataBound"
                            AutoPostBack="true"
                            EnableLoadOnDemand="True" NoWrap="True"
                            EnableEmbeddedScripts="True"
                            MarkFirstMatch="True" HighlightTemplatedItems="True"
                                             >
                            <HeaderTemplate>
                                <ul>
                                    <li class="col1 ">
                                        <asp:Label ID="hdrlblFullName" runat="server" Text="Full Name"></asp:Label></li>
                                    <li class="col2">
                                        <asp:Label ID="hdrlblStuNumType" runat="server"></asp:Label></li>
                                </ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <ul>
                                    <li class="col1">
                                        <asp:Label ID="lblFullName" runat="server" Text="Full Name"></asp:Label></li>
                                    <li class="col2">
                                        <asp:Label ID="lblSSN" runat="server" Text="SSN"></asp:Label></li>
                                </ul>
                            </ItemTemplate>
                        </telerik:RadComboBox>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="contentcell" nowrap="nowrap" style="width: 90px; vertical-align: middle;">
                        <asp:Label ID="lblEnrollmentId" runat="server" CssClass="label">Enrollment</asp:Label>
                    </td>
                    <td class="contentcell4">
                        <telerik:RadComboBox ID="ddlEnrollments" runat="server" AutoPostBack="true" Enabled="false" BackColor="White"
                            DropDownWidth="325px" MarkFirstMatch="true"
                            OnDataBound="ddlEnrollments_DataBound" ValidationGroup="ucValGrp"
                            OnSelectedIndexChanged="ddlEnrollments_SelectedIndexChanged"
                            CssClass="dropdownlist"
                            Width="325px">
                            <HeaderTemplate>
                                <ul>
                                    <li class="col1">
                                        <asp:Label ID="hdrlblEnrollment" runat="server" Text="Enrollment"></asp:Label>
                                    </li>
                                    <li class="col2">
                                        <asp:Label ID="hdrlblStatus" runat="server" Text="Status"></asp:Label>
                                    </li>
                                </ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <ul>
                                    <li class="col1">
                                        <asp:Label ID="lblEnrollment" runat="server" Text="Enrollment"></asp:Label>
                                    </li>
                                    <li class="col2">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                    </li>
                                    <%--<li class="col3">
                                    <asp:Label ID="lblPrgDescrip" runat="server" Text="PrgVerDescrip" Visible="false"></asp:Label></li>
                                <li class="col4">
                                    <asp:Label ID="lblStatusCodeDescrip" runat="server" Text="StatusCodeDescrip" Visible="false"></asp:Label></li>--%>
                                </ul>
                            </ItemTemplate>
                        </telerik:RadComboBox>
                        <br />
                    </td>
                </tr>




                <tr runat="server" id="trTerms">
                    <td class="contentcell" nowrap="nowrap">
                        <asp:Label ID="lblTermId" runat="server" CssClass="label">Term</asp:Label>
                    </td>
                    <td class="contentcell4">
                        <telerik:RadComboBox ID="ddlTerms" runat="server" Width="325px" AutoPostBack="true" Enabled="false" BackColor="White" CssClass="dropdownlist"
                            MarkFirstMatch="true" DropDownWidth="325px" ValidationGroup="ucValGrp">
                        </telerik:RadComboBox>
                        <br />
                    </td>
                </tr>




                <tr runat="server" id="trAcadYears">
                    <td class="contentcell" nowrap="nowrap">
                        <asp:Label ID="lblAcademicYearId" runat="server" CssClass="label">Academic Year</asp:Label>
                    </td>
                    <td class="contentcell4">
                        <telerik:RadComboBox ID="ddlAcademicYears" runat="server" Width="325px" AutoPostBack="true" Enabled="false" BackColor="White" CssClass="dropdownlist"
                            MarkFirstMatch="true" DropDownWidth="325px" ValidationGroup="ucValGrp">
                        </telerik:RadComboBox>
                        <br />
                    </td>
                </tr>


            </table>
        </div>

    </asp:Panel>
</div>



