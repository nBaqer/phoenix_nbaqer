﻿
Imports FAME.Advantage.Common
Imports Telerik.Web.UI

Partial Class usercontrols_YearDropDown
    Inherits System.Web.UI.UserControl
    Public Label As String
    Public Mandatory As Boolean
    Public BaseYear As String
    Public Offset As String
    Public SelectBaseYear As Boolean
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim _label = "Year"

        If Not String.IsNullOrEmpty(Label) Then
            _label = Label
        End If
        If Mandatory Then
            _label += " <font color=""red"">*</font>"
        End If
        YearHeader.InnerHtml = _label
    End Sub
    Public Function GetItems() As RadComboBoxItemCollection
        Return RadComboYear.Items
    End Function
    Public Function GetSelected() As RadComboBoxItem
        Return RadComboYear.SelectedItem
    End Function
    Public Function GetId() As String
        Return RadComboYear.ID
    End Function
    Public Function GetInnerControl() As RadComboBox
        Return RadComboYear
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            FillYears()
        End If
    End Sub

    Public Sub Reload(year As String, Optional ofset As String = "2")
        BaseYear = year
        Offset = ofset
        FillYears()
    End Sub
    Private Sub FillYears()
        Try
            Dim currentYear = If(String.IsNullOrEmpty(BaseYear), DateTime.Today.Year, Integer.Parse(BaseYear))
            Dim _offset = If(String.IsNullOrEmpty(Offset), 2, Integer.Parse(Offset))

            Dim listOfYear As List(Of Object) = New List(Of Object)()
            For i As Integer = 0 To (_offset * 2)
                Dim year = (currentYear + _offset) - i
                listOfYear.Add(New With {.Text = year.ToString(), .Value = year.ToString()})
            Next

            RadComboYear.DataTextField = "Text"
            RadComboYear.DataValueField = "Value"
            RadComboYear.DataSource = listOfYear
            RadComboYear.DataBind()

            If SelectBaseYear Then
                'set selected to current year'
                Dim index = RadComboYear.FindItemIndexByValue(currentYear.ToString())
                RadComboYear.SelectedIndex = index
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
End Class
