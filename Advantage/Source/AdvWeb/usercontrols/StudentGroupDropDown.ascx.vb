﻿
Imports System.Diagnostics
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports Telerik.Web.UI

Partial Class usercontrols_StudentGroupDropDown
    Inherits System.Web.UI.UserControl
    Protected MyAdvAppSettings As AdvAppSettings
    Public Label As String 
    Public Mandatory As Boolean
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        dim _label = "Student Group"

        If Not String.IsNullOrEmpty(Label) Then
            _label = Label
        End If
        If Mandatory Then 
            _label += " <font color=""red"">*</font>"
        End If
        StudentGroupHeader.InnerHtml = _label
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub

    public Function GetItems() As RadComboBoxItemCollection
        return RadComboStudentGroup.Items
    End Function
    Public Function GetSelected() As RadComboBoxItem
        Return RadComboStudentGroup.SelectedItem
    End Function
    public Function GetId() As String
        return RadComboStudentGroup.ID
    End Function
    Public Function GetInnerControl() As RadComboBox
        Return RadComboStudentGroup
    End Function
    Public Sub Reload(ByVal selectedCampusId As Guid, Optional ByVal includeSelect As Boolean = False, Optional ByVal includeSelectText _
                         As String = "Select")
        Try
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim ds = New List(Of GenericListItem)
            If (includeSelect) Then
                ds.Insert(0, New GenericListItem() With {.Description = includeSelectText, .Id = "-1"})
            End If

            ds.AddRange(GetStudentGroupsByCampusId(selectedCampusId))
            RadComboStudentGroup.DataTextField = "Description"
            RadComboStudentGroup.DataValueField = "Id"
            RadComboStudentGroup.DataSource = ds
            RadComboStudentGroup.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Private Function GetStudentGroupsByCampusId(ByVal selectedCampus As Guid) As List(Of GenericListItem)
        Dim da As New LeadGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Try
            Return da.GetStudentGroups(selectedCampus)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Debug.WriteLine("Error in GetStudentGroupsByCampusId exception message: {0}", ex.Message)
            Throw
        End Try
    End Function

End Class
