﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ReportDropDown.ascx.vb" Inherits="usercontrols_ReportDropDown" %>

<div id="ReportSelector" class="ReportSelector MultiFilterReportContainer" runat="server" style="padding-left: 2px;">
    <div id="ReportHeader" class="CaptionLabel" runat="server">Report <font color="red">*</font></div>

    <telerik:RadComboBox ID="RadComboReport" runat="server" AutoPostBack="true"
                         Width="350px" ToolTip="Select a report to run the report"    CssClass="ManualInput ReportLeftMarginInput">
    </telerik:RadComboBox>
</div>
