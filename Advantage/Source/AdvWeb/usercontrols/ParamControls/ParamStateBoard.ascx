﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamStateBoard.ascx.vb" Inherits="ParamStateBoard" %>
<%@ Register Src="~/usercontrols/StateDropDown.ascx" TagPrefix="fame" TagName="StateDropDown" %>
<%@ Register Src="~/usercontrols/MonthDropDown.ascx" TagPrefix="fame" TagName="MonthDropDown" %>
<%@ Register Src="~/usercontrols/YearDropDown.ascx" TagPrefix="fame" TagName="YearDropDown" %>
<%@ Register Src="~/usercontrols/StudentGroupDropDown.ascx" TagPrefix="fame" TagName="StudentGroupDropDown" %>
<%@ Register Src="~/usercontrols/ProgramVersionDropDown.ascx" TagPrefix="fame" TagName="ProgramVersionDropDown" %>
<%@ Register Src="~/usercontrols/ReportDropDown.ascx" TagPrefix="fame" TagName="ReportDropDown" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function checkFilter(sender, args) {
        //if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgram2") > 1) {
        //    handleSourceToDestinationTransfer(args, 1, 'Only one program selection is allowed');
        //}
        //else if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgramVersion2") > 1) {
        //    handleSourceToDestinationTransfer(args, 1, 'Only one program version selection is allowed');
        //}
        //else if (args.get_destinationListBox().get_id().indexOf("RadListBoxCourse2") > 1) {
        //    handleSourceToDestinationTransfer(args, 1, 'Only one course selection is allowed');
        //}
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
        //var maxNumberOfItems = 1;
        var dest = args.get_destinationListBox();
        var totalCount = dest.get_items().get_count();
        var itemsToTransferCount = args.get_items().length;
        if (totalCount == maxNumberOfItems) {
            alert(message);
            args.set_cancel(true);
        } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
            while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                itemsToTransferCount--;
                args.get_items().pop();
            }
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div><font color="red"> *</font>Filters are required and the filters need to be selected in the following order.</div>
    <div id="MainContainer" class="MainContainer">

        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer" runat="server" >
            <div id="CampusHeader" class="CaptionLabel" runat="server">1. Campus <font color="red">*</font></div>

            <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged" OnItemDataBound="CampusComboBox_ItemDataBound"
                Width="350px" ToolTip="Select a campus to run the report" CssClass="ManualInput ReportLeftMarginInput">
            </telerik:RadComboBox>
        </div>
        <fame:StateDropDown ID="stateDropDown" Label="2. State" Mandatory="True" runat="server" />
        <fame:ReportDropDown ID="reportDropDown" Label="3. Report" Mandatory="True" runat="server" />
        <fame:MonthDropDown ID="monthDropDown" Label="4. Month" Mandatory="True" runat="server" />
        <fame:YearDropDown ID="yearDropDown" Label="5. Year" Mandatory="True" SelectBaseYear="True" runat="server" />
        <fame:StudentGroupDropDown ID="studentGroupDropDown" Label="6. Student Group" Mandatory="True" runat="server" />
        <fame:ProgramVersionDropDown ID="programVersionDropDown" Label="7. Program Version" Mandatory="False" runat="server" />
    </div>




</asp:Panel>
