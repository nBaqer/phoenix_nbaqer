﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamIPEDS.ascx.vb" Inherits="ParamIPEDS" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">

        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer">
            <div id="CampusHeader" class="CaptionLabel" runat="server">Campus</div>

            <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged" OnItemDataBound="CampusComboBox_ItemDataBound"
                Width="350px" ToolTip="Select a campus to run the report" CssClass="ManualInput ReportLeftMarginInput">
            </telerik:RadComboBox>
        </div>
        <div id="ProgramSelector" class="ProgramSelector MultiFilterReportContainer" runat="server">
            <div id="captionlabel2" class="CaptionLabel" runat="server">Program</div>
            <div class="FilterInput">

                <telerik:RadListBox ID="RadListBoxProgram" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxProgram2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxProgram2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactivePrograms" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="ProgramCounterAvailable" class="RadListBox1Counter">
                        <asp:Label ID="lblProgramCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="ProgramCounterSelected" class="RadListBox2Counter">
                        <asp:Label ID="lblProgramCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>

        </div>
        <br />
        <div id="LargestProgramArea" class="LargestProgramArea MultiFilterReportContainer">
            <div id="LargestProgramText" class="ReportLeftMarginInput" runat="server" visible="false">Select the program with the most enrollments </div>
            <telerik:RadComboBox Visible="false" ID="RadComboLProgram" runat="server" CssClass="ReportLeftMarginInput"
                Width="350px" ToolTip="Select the school's largest program to run the report">
            </telerik:RadComboBox>
        </div>
        <div id="AreasOfInterestSelector" class="AreasOfInterestSelector MultiFilterReportContainer" runat="server">
            <div id="Div2" class="CaptionLabel" runat="server">Area of Interest</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxAreasOfInterest" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxAreasOfInterest2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxAreasOfInterest2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div3" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveAreasOfInterest" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span1" class="RadListBox1Counter">
                        <asp:Label ID="lblAreasOfInterestCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span2" class="RadListBox2Counter">
                        <asp:Label ID="lblAreasOfInterestCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
            </div>
        </div>
        <div id="SchoolandCohortReportTypeSelector" runat="server" class="SchoolReportTypeSelector">
            <div id="Div4" class="CaptionLabel" runat="server">School Type</div>
            <div class="FilterInput ReportLeftMarginInput">
                <table>
                    <tr>
                        <td style="width: 50%">
                            <div id="Div5" runat="server">
                                <asp:RadioButtonList ID="rblSchoolTypeList" runat="server"
                                    RepeatDirection="Horizontal" ToolTip="Select the school type of IPEDS reporting your school uses." AutoPostBack="True"
                                    OnSelectedIndexChanged="rblSchoolType_SelectedIndexChanged" Visible="False">
                                    <asp:ListItem Selected="True" Value="4">4 year</asp:ListItem>
                                    <asp:ListItem Value="2">2 year</asp:ListItem>
                                    <asp:ListItem Value="<2"> < 2 year</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="SchoolReportTypeSelector" class="SchoolReportTypeSelector MultiFilterReportContainer">
            <div id="SchoolReportingTypeHeader" class="CaptionLabel" runat="server">School Reporting Type</div>
            <div class="ReportLeftMarginInput">
                <table>
                    <tr>
                        <td style="width: 50%">
                            <div id="rblSchoolTypeSelector" runat="server">
                                <asp:RadioButtonList ID="rblSchoolType" runat="server"
                                    RepeatDirection="Horizontal" ToolTip="Select the type of IPEDS reporting your school uses." AutoPostBack="True"
                                    OnSelectedIndexChanged="rblSchoolType_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Value="Program">Program Reporter</asp:ListItem>
                                    <asp:ListItem Value="Academic">Academic Year Reporter</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RadioButtonList ID="rblFullorFall" runat="server"
                                    RepeatDirection="Horizontal" ToolTip="Select the type of IPEDS reporting your school uses." AutoPostBack="True"
                                    OnSelectedIndexChanged="rblSchoolType_SelectedIndexChanged" Visible="False">
                                    <asp:ListItem Selected="True" Value="Full">Full Year</asp:ListItem>
                                    <asp:ListItem Value="Fall">Fall</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </td>
                        <td style="width: 50%;">
                            <div id="radComboBoxInstitutionTypeLabel" runat="server" visible="false">Control of Institution:  </div>
                            <telerik:RadComboBox ID="radComboBoxInstitutionType" runat="server" Visible="false" Width="200px">
                                <Items>
                                    <telerik:RadComboBoxItem Text="Academic Reporters - Public" ToolTip="Academic Reporters - Public" Value="0" />
                                    <telerik:RadComboBoxItem Text="Academic Reporters - Private" ToolTip="Academic Reporters - Private" Value="1" />
                                    <telerik:RadComboBoxItem Text="Program Reporters - Public" ToolTip="Program Reporters - Public" Value="2" />
                                    <telerik:RadComboBoxItem Text="Program Reporters - Private" ToolTip="Program Reporters - Private" Value="3" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="AcademicYearMsg" runat="server" Visible="false" Style="width: 95%">
                                <p>
                                    <b>IMPORTANT MESSAGE:</b>
                                    <br />
                                    Use the school's "Official Report Date" or October 15th in the "Report Date" field below. The student counts on this report must tie into
        student counts on other IPEDS Surveys, otherwise the U.S. Department of Education will flag these reports as inconsistent
        and will require futher explanations.
                           
                                </p>
                                <p>
                                    In addition, the "Report End Date" chosen for this report must be used for all other IPEDS reports that have this warning message.
        Please double check your "Report Date" settings before submitting the data.
                           
                                </p>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="SubCohortTypeSelector" runat="server" class="SchoolReportTypeSelector">
            <div id="Div7" class="CaptionLabel" runat="server">Sub Cohort Type</div>
            <div class="FilterInput ReportLeftMarginInput">
                <table>
                    <tr>
                        <td style="width: 50%">
                            <div id="Div8" runat="server">
                                <asp:RadioButtonList ID="radsubcohort" runat="server"
                                    RepeatDirection="Horizontal" ToolTip="Select the sub cohort type of IPEDS reporting your school uses."
                                    OnSelectedIndexChanged="rblSchoolType_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Value="0">Bachelor's or equivalent degree seeking</asp:ListItem>
                                    <asp:ListItem Value="1">Other degree/certificate seeking</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="DateOptionsSelector" class="DateOptionsSelector MultiFilterReportContainer" runat="server">
            <div id="DateOptionsHeader" class="CaptionLabel" runat="server">Date Options</div>
            <div class="ReportLeftMarginInput">
                <table width="100%">
                    <tr>
                        <td style="width: 50%;">
                            <div id="DatePopUps" class="DatePops">
                                <div>
                                    <asp:Label ID="lblDisplayReportDate" Visible="false" runat="server">Report Date: </asp:Label>
                                </div>
                                <div class="LabelFilterInput">
                                    <asp:Label ID="DatePickerRDateText" class="DatePicker1Text" runat="server">Report Date </asp:Label>
                                </div>
                                <%--    <div class="DatePicker1Text" id="DatePickerRDateText" runat="server">Report Date </div>--%>
                                <telerik:RadDatePicker ID="RadDateReportDate" runat="server"
                                    Width="151px" DateInput-ReadOnly="true" CssClass="ManualInput">
                                    <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                                    <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy"></DateInput>
                                </telerik:RadDatePicker>
                                <div class="DatePicker2Text LabelFilterInput" id="DatePickerPriorToText" runat="server">Exclude students who left school prior to </div>
                                <telerik:RadDatePicker ID="RadDatePriorTo" runat="server"
                                    Width="151px" DateInput-ReadOnly="true" CssClass="ManualInput">
                                    <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                                    <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy"></DateInput>
                                </telerik:RadDatePicker>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">
                            <div id="YearSelector" class="YearSelector" runat="server">

                                <div id="RadComboBoxCohortYearLabel" runat="server">Cohort Year </div>
                                <telerik:RadComboBox ID="RadComboBoxCohortYear" runat="server" ToolTip="Select a Cohort Year" OnSelectedIndexChanged="CohortYearComboBox_SelectedIndexChanged" AutoPostBack="true">
                                </telerik:RadComboBox>


                                <div id="RadComboBoxReportingYearLabel" runat="server">Reporting Year </div>
                                <telerik:RadComboBox ID="RadComboBoxReportingYear" runat="server" ToolTip="Select a Reporting Year" OnSelectedIndexChanged="ReportingYearComboBox_SelectedIndexChanged" AutoPostBack="true">
                                </telerik:RadComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="ReportDateRangeMsg" runat="server"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="DisplaySort" class="DisplaySort MultiFilterReportContainer" runat="server">
            <div id="Div1" class="CaptionLabel" runat="server">Sort Options</div>
            <div class="ReportLeftMarginInput">


                <table>
                    <tr>
                        <td>
                            <div>Sort By</div>
                        </td>
                        <td>
                            <div id="divrblsortby" runat="server">
                                <asp:RadioButtonList ID="rblSortBy" runat="server" AutoPostBack="true"
                                    Width="300px" RepeatDirection="Horizontal" ToolTip="Sort the report by the Selected Student Indentifier (from selection above) or Last Name">
                                    <asp:ListItem Selected="True" Value="SSN">SSN</asp:ListItem>
                                    <asp:ListItem Value="LastName">Last Name</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="ResetDiv"></div>
    </div>
</asp:Panel>

