﻿Option Strict On

Imports System.Data
Imports System.Diagnostics
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports NHibernate.Hql.Ast.ANTLR
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports ASP
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common
Imports Telerik.Windows.Documents.Spreadsheet.Expressions.Functions

Partial Class ParamStateBoard
    Inherits UserControl
    Implements ICustomControl

    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString
    Private _StudentIdentifier As String
    Private _ProgramReporterStart As String
    Private _ProgramReporterEnd As String
    Private _AcademicYearStart As String
    Private _AcademicYearEnd As String
    Private _StartDatePartProgram As String
    Private _StartDatePartAcademic As String
    Private _EndDatePartProgram As String
    Private _EndDatePartAcademic As String
    Private _SelectedDatePartProgram As String
    Private _SelectedDatePartAcademic As String

    Protected MyAdvAppSettings As AdvAppSettings

    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        SetProperties()
        'IPEDSControlType = ControlType.GradRates
        If Not Page.IsPostBack Then
            FillCampusControl()
            'FillStateControls()
            RadComboCampus.SelectedValue = HttpContext.Current.Request.Params("cmpid").ToString
            FillStateControls()
            FillReportControls()
            FillStudentGroupControls()
            FillProgramversionControls()




            AddHandler stateDropDown.StateIndexChange, AddressOf StateComboBox_SelectedIndexChanged


        End If
    End Sub



#Region "Methods"
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillStudentGroupControls()
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            studentGroupDropDown.Reload(selectedCampus, True, "Include All")
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillProgramversionControls(Optional ByVal progId As Nullable(Of Guid) = Nothing)
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            programVersionDropDown.Reload(selectedCampus, progId, True)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillStateControls()
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            stateDropDown.Reload(True)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Public Sub FillReportControls()
        Try
            Dim result As guid
            Dim selectedState As Guid =  If(Guid.TryParse(stateDropDown.GetInnerControl().SelectedValue, result), result, Guid.Empty) 
            reportDropDown.Reload(selectedState,True)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim da As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As List(Of syCampus)
        Try
            result = da.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString))
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Debug.WriteLine("Error in GetCampusesByUserId exception message: {0}", ex.Message)
            Throw
        End Try
    End Function
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim statusValues As New List(Of String)

        Try
            Dim CKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If CKbox.Checked = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return statusValues
    End Function

    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
    End Sub
    Protected Sub CampusComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        FillStateControls()
        FillStudentGroupControls()
        FillProgramversionControls()
    End Sub

    Protected Sub StateComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles stateDropDown.StateIndexChange
        FillReportControls()

    End Sub

    Protected Sub CampusComboBox_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        Dim myItem As RadComboBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is syCampus Then
                Dim MysyCampus As syCampus = DirectCast(myItem.DataItem, syCampus)
                If MysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride

            'Campus'
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo

            ctrlSettingCampus.ControlName = RadComboCampus.ID
            Dim selectedCampus As RadComboBoxItem = RadComboCampus.SelectedItem
            objControlValueInfo.DisplayText = selectedCampus.Text.Replace("'", "''")
            objControlValueInfo.KeyData = selectedCampus.Value.Replace("'", "''")
            ctrlValuesCampus.Add(objControlValueInfo)
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            settingsCollection.Add(ctrlSettingCampus)

            'State'
            Dim ctrlSettingState As New ControlSettingInfo
            Dim ctrlValuesState As New List(Of ControlValueInfo)
            Dim objControlValueInfo2 As New ControlValueInfo

            ctrlSettingState.ControlName = stateDropDown.GetId()
            Dim selectedState As RadComboBoxItem = stateDropDown.GetSelected()
            objControlValueInfo2.DisplayText = selectedState.Text.Replace("'", "''")
            objControlValueInfo2.KeyData = selectedState.Value.Replace("'", "''")
            ctrlValuesState.Add(objControlValueInfo2)
            ctrlSettingState.ControlValueCollection = ctrlValuesState
            settingsCollection.Add(ctrlSettingState)

            'Report'
            Dim ctrlSettingReport As New ControlSettingInfo
            Dim ctrlValuesReport As New List(Of ControlValueInfo)
            Dim objControlValueInfoReport As New ControlValueInfo

            ctrlSettingReport.ControlName = reportDropDown.GetId()
            Dim selectedReport As RadComboBoxItem = reportDropDown.GetSelected()
            objControlValueInfoReport.DisplayText = selectedReport.Text.Replace("'", "''")
            objControlValueInfoReport.KeyData = selectedReport.Value.Replace("'", "''")
            ctrlValuesReport.Add(objControlValueInfoReport)
            ctrlSettingReport.ControlValueCollection = ctrlValuesReport
            settingsCollection.Add(ctrlSettingReport)

            'Month
            Dim ctrlSettingMonth As New ControlSettingInfo
            Dim ctrlValuesMonth As New List(Of ControlValueInfo)
            Dim objControlValueInfo3 As New ControlValueInfo

            ctrlSettingMonth.ControlName = monthDropDown.GetId()
            Dim selectedMonth As RadComboBoxItem = monthDropDown.GetSelected()
            objControlValueInfo3.DisplayText = selectedMonth.Text.Replace("'", "''")
            objControlValueInfo3.KeyData = selectedMonth.Value.Replace("'", "''")
            ctrlValuesMonth.Add(objControlValueInfo3)
            ctrlSettingMonth.ControlValueCollection = ctrlValuesMonth
            settingsCollection.Add(ctrlSettingMonth)

            'Year
            Dim ctrlSettingYear As New ControlSettingInfo
            Dim ctrlValuesYear As New List(Of ControlValueInfo)
            Dim objControlValueInfo4 As New ControlValueInfo

            ctrlSettingYear.ControlName = yearDropDown.GetId()
            Dim selectedYear As RadComboBoxItem = yearDropDown.GetSelected()
            objControlValueInfo4.DisplayText = selectedYear.Text.Replace("'", "''")
            objControlValueInfo4.KeyData = selectedYear.Value.Replace("'", "''")
            ctrlValuesYear.Add(objControlValueInfo4)
            ctrlSettingYear.ControlValueCollection = ctrlValuesYear
            settingsCollection.Add(ctrlSettingYear)

            'Student Group
            Dim ctrlSettingStudentGroup As New ControlSettingInfo
            Dim ctrlValuesStudentGroup As New List(Of ControlValueInfo)
            Dim objControlValueInfo5 As New ControlValueInfo

            ctrlSettingStudentGroup.ControlName = studentGroupDropDown.GetId()
            Dim selectedStudentGroup As RadComboBoxItem = studentGroupDropDown.GetSelected()
            objControlValueInfo5.DisplayText = selectedStudentGroup.Text.Replace("'", "''")
            objControlValueInfo5.KeyData = selectedStudentGroup.Value.Replace("'", "''")
            ctrlValuesStudentGroup.Add(objControlValueInfo5)
            ctrlSettingStudentGroup.ControlValueCollection = ctrlValuesStudentGroup
            settingsCollection.Add(ctrlSettingStudentGroup)

            'Program Version
            Dim ctrlSettingProgramVersion As New ControlSettingInfo
            Dim ctrlValuesProgramVersion As New List(Of ControlValueInfo)
            Dim objControlValueInfo6 As New ControlValueInfo

            ctrlSettingProgramVersion.ControlName = programVersionDropDown.GetId()
            Dim selectedProgramVersion As RadComboBoxItem = programVersionDropDown.GetSelected()
            objControlValueInfo6.DisplayText = selectedProgramVersion.Text.Replace("'", "''")
            objControlValueInfo6.KeyData = selectedProgramVersion.Value.Replace("'", "''")
            ctrlValuesProgramVersion.Add(objControlValueInfo6)
            ctrlSettingProgramVersion.ControlValueCollection = ctrlValuesProgramVersion
            settingsCollection.Add(ctrlSettingProgramVersion)

            userSettings.ControlSettingsCollection = settingsCollection


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        Return userSettings

    End Function
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim DisplayTree As New RadTreeView
        DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            'Campus'
            Dim HeaderNode1 As New RadTreeNode
            HeaderNode1.Text = "Campus"
            HeaderNode1.Value = "Campus"
            HeaderNode1.CssClass = "TreeParentNode"
            Dim CampusNode As New RadTreeNode
            CampusNode.Text = RadComboCampus.SelectedItem.Text
            HeaderNode1.Nodes.Add(CampusNode)
            DisplayTree.Nodes.Add(HeaderNode1)
            'State'
            Dim HeaderNode2 As New RadTreeNode
            HeaderNode2.Text = "State"
            HeaderNode2.Value = "State"
            HeaderNode2.CssClass = "TreeParentNode"
            Dim StateNode As New RadTreeNode
            StateNode.Text = stateDropDown.GetSelected().Text
            HeaderNode2.Nodes.Add(StateNode)
            DisplayTree.Nodes.Add(HeaderNode2)
            'Report'
            Dim HeaderNodeReport As New RadTreeNode
            HeaderNodeReport.Text = "Report"
            HeaderNodeReport.Value = "Report"
            HeaderNodeReport.CssClass = "TreeParentNode"
            Dim ReportNode As New RadTreeNode
            ReportNode.Text = reportDropDown.GetSelected().Text
            HeaderNodeReport.Nodes.Add(ReportNode)
            DisplayTree.Nodes.Add(HeaderNodeReport)
            'Month'
            Dim HeaderNode3 As New RadTreeNode
            HeaderNode3.Text = "Month"
            HeaderNode3.Value = "Month"
            HeaderNode3.CssClass = "TreeParentNode"
            Dim MonthNode As New RadTreeNode
            MonthNode.Text = monthDropDown.GetSelected().Text
            HeaderNode3.Nodes.Add(MonthNode)
            DisplayTree.Nodes.Add(HeaderNode3)

            'Year'
            Dim HeaderNode4 As New RadTreeNode
            HeaderNode4.Text = "Year"
            HeaderNode4.Value = "Year"
            HeaderNode4.CssClass = "TreeParentNode"
            Dim YearNode As New RadTreeNode
            YearNode.Text = yearDropDown.GetSelected().Text
            HeaderNode4.Nodes.Add(YearNode)
            DisplayTree.Nodes.Add(HeaderNode4)

            'Student Group'
            Dim HeaderNode5 As New RadTreeNode
            HeaderNode5.Text = "Student Group"
            HeaderNode5.Value = "StudentGroup"
            HeaderNode5.CssClass = "TreeParentNode"
            Dim StudentGroupNode As New RadTreeNode
            StudentGroupNode.Text = studentGroupDropDown.GetSelected().Text
            HeaderNode5.Nodes.Add(StudentGroupNode)
            DisplayTree.Nodes.Add(HeaderNode5)

            'Program Version'
            Dim HeaderNode6 As New RadTreeNode
            HeaderNode6.Text = "Program Version"
            HeaderNode6.Value = "ProgramVersion"
            HeaderNode6.CssClass = "TreeParentNode"
            Dim ProgramVersionNode As New RadTreeNode
            ProgramVersionNode.Text = If(programVersionDropDown.GetSelected().Text = "Select", "None", programVersionDropDown.GetSelected().Text)
            HeaderNode6.Nodes.Add(ProgramVersionNode)
            DisplayTree.Nodes.Add(HeaderNode6)
            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            'Set component controls list'
            Dim ComponentControls = New List(Of Control)({RadComboCampus, stateDropDown.GetInnerControl(), monthDropDown.GetInnerControl(), yearDropDown.GetInnerControl(), studentGroupDropDown.GetInnerControl(), programVersionDropDown.GetInnerControl()})
            If Not SavedSettings Is Nothing AndAlso Not ComponentControls Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = If(ComponentControls.FirstOrDefault(Function(x) x.ID = Setting.ControlName), Me.FindControl(Setting.ControlName))
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim ComboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If Not ItemValue.KeyData = "-1" Then
                                    If (ctrl.ID = yearDropDown.GetId()) Then
                                        'Reload year dropdown with appropriate base year before selecting year'
                                        yearDropDown.Reload(ItemValue.KeyData)
                                    End If
                                    ComboBox.SelectedValue = ItemValue.KeyData
                                End If

                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim LstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            LstBox.Items.Clear()
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = ItemValue.DisplayText
                                newItem.Value = ItemValue.KeyData
                                LstBox.SelectedValue = ItemValue.KeyData
                                LstBox.Items.Add(newItem)
                            Next
                        ElseIf TypeOf ctrl Is RadDatePicker Then
                            Dim DPicker As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If Not ItemValue.KeyData = "01/01/1900" Then
                                    'Get the current min and max dates
                                    Dim MinDate As Date = DPicker.MinDate
                                    Dim MaxDate As Date = DPicker.MaxDate
                                    'loosen the validation to allow the population of the datepicker from the saved settings
                                    'since it has already been validated on entry
                                    DPicker.MinDate = DPicker.MinDate.AddYears(-20)
                                    DPicker.MaxDate = DPicker.MaxDate.AddYears(20)
                                    'populate the date picker
                                    DPicker.SelectedDate = CDate(ItemValue.KeyData)
                                    'restore the min and max dates to the proper values
                                    DPicker.MinDate = MinDate
                                    DPicker.MaxDate = MaxDate
                                End If
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else

                'RadListBoxProgramVersion.Items.Clear()
                'RadListBoxProgramVersion2.Items.Clear()

            End If
            'ListBoxCounts()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
#End Region
#Region "Set Properties"
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub

#End Region
#Region "Events"
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim Lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arProgram Then
                Dim MyarProgram As arProgram = DirectCast(myItem.DataItem, arProgram)
                If MyarProgram.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarProgram.ProgDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is arPrgVersion Then
                Dim myarPrgVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                Dim myarProgramVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                If myarProgramVersion.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarPrgVersion.PrgVerDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
#End Region

End Class
