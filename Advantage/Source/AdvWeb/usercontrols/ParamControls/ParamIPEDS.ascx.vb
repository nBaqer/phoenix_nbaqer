﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common

Partial Class ParamIPEDS
    Inherits UserControl
    Implements ICustomControl

    Public Enum IPEDSReportingType
        [ProgramReporter] = 0
        [AcademicYearReporter] = 1
        [Fall] = 2
    End Enum
    Public Enum InstitutionType
        [Public] = 0
        [Private] = 1
    End Enum
    Public Enum ControlType
        [Instructions] = 0
        [MissingData] = 1
        [TwelveMonthDetail] = 2
        [TwelveMonthSummary] = 3
        [BDetail] = 4
        [CDetail] = 5
        [BCSummary] = 6
        [COMDetail] = 7
        [COMSummary] = 8
        [Finance] = 9
        [GradRates4] = 10 'For GradRate 4 Year reports
        [FinAidA] = 11 ' FinAid Part A report
        [FinAidBCDE] = 12 'FinAid Part B,C,D,and E
        [GradRates2Years] = 13 'For GradRate 2 Year and less than two year report
        [FallEnrollment] = 14 ' For Fall Enrollment Reports
        [GradRates200Less2Yrs] = 15 'For GradRate 200 Less than 2 Years report
        [COMCIPDetailSumamry] = 16  'For Completion CIP Detail & Summary Report
        [COMAllCompletesDetailSumamry] = 17 ' For Completion All Completers Detail & Summary Report
        [COMCompletesByLevelDetailSumamry] = 18 ' For Completion Completers By level Detail & Summary Report
        [InstB3Summary] = 19
        [OutcomeMeasures] = 20
        [DisabilityService] = 21 'Disability Service
        [None] = 999 'for testing only
    End Enum
#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString
    Private _IPEDSControlType As ControlType
    Private _StudentIdentifier As String
    Private _ProgramReporterStart As String
    Private _ProgramReporterEnd As String
    Private _AcademicYearStart As String
    Private _AcademicYearEnd As String
    Private _StartDatePartProgram As String
    Private _StartDatePartAcademic As String
    Private _EndDatePartProgram As String
    Private _EndDatePartAcademic As String
    Private _SelectedDatePartProgram As String
    Private _SelectedDatePartAcademic As String
    Private _IsOutComeMeasures As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings

    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = Value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
    Public Property IPEDSControlType() As ControlType
        Get
            Return _IPEDSControlType
        End Get
        Set(ByVal Value As ControlType)
            _IPEDSControlType = Value
        End Set
    End Property
    Public Property StudentIdentifier() As String
        Get
            Return _StudentIdentifier
        End Get
        Set(ByVal Value As String)
            _StudentIdentifier = Value
        End Set
    End Property
    Public Property ProgramReporterStart() As String
        Get
            Return _ProgramReporterStart
        End Get
        Set(ByVal Value As String)
            _ProgramReporterStart = Value
        End Set
    End Property
    Public Property ProgramReporterEnd() As String
        Get
            Return _ProgramReporterEnd
        End Get
        Set(ByVal Value As String)
            _ProgramReporterEnd = Value
        End Set
    End Property
    Public Property AcademicYearStart() As String
        Get
            Return _AcademicYearStart
        End Get
        Set(ByVal Value As String)
            _AcademicYearStart = Value
        End Set
    End Property
    Public Property AcademicYearEnd() As String
        Get
            Return _AcademicYearEnd
        End Get
        Set(ByVal Value As String)
            _AcademicYearEnd = Value
        End Set
    End Property

    Public Property StartDatePartProgram() As String
        Get
            If String.IsNullOrWhiteSpace(_StartDatePartProgram) Then
                _StartDatePartProgram = "08/01/"
            End If
            Return _StartDatePartProgram
        End Get
        Set(ByVal value As String)
            _StartDatePartProgram = Value
        End Set
    End Property

    Public Property StartDatePartAcademic() As String
        Get
            If String.IsNullOrWhiteSpace(_StartDatePartAcademic) Then
                _StartDatePartAcademic = "09/01/"
            End If
            Return _StartDatePartAcademic
        End Get
        Set(ByVal value As String)
            _StartDatePartAcademic = Value
        End Set
    End Property

    Public Property EndDatePartProgram() As String
        Get
            If String.IsNullOrWhiteSpace(_EndDatePartProgram) Then
                _EndDatePartProgram = "10/31/"
            End If
            Return _EndDatePartProgram
        End Get
        Set(ByVal value As String)
            _EndDatePartProgram = Value
        End Set
    End Property

    Public Property EndDatePartAcademic() As String
        Get
            If String.IsNullOrWhiteSpace(_EndDatePartAcademic) Then
                _EndDatePartAcademic = "12/15/"
            End If
            Return _EndDatePartAcademic
        End Get
        Set(ByVal value As String)
            _EndDatePartAcademic = Value
        End Set
    End Property
    Public Property SelectedDatePartProgram() As String
        Get
            If String.IsNullOrWhiteSpace(_SelectedDatePartProgram) Then
                _SelectedDatePartProgram = "08/31/"
            End If
            Return _SelectedDatePartProgram
        End Get
        Set(ByVal value As String)
            _SelectedDatePartProgram = Value
        End Set
    End Property

    Public Property SelectedDatePartAcademic() As String
        Get
            If String.IsNullOrWhiteSpace(_SelectedDatePartAcademic) Then
                _SelectedDatePartAcademic = "10/15/"
            End If
            Return _SelectedDatePartAcademic
        End Get
        Set(ByVal value As String)
            _SelectedDatePartAcademic = Value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        SetProperties()
        'IPEDSControlType = ControlType.GradRates
        If Not Page.IsPostBack Then
            FillCampusControl()
            RadComboCampus.SelectedValue = HttpContext.Current.Request.Params("cmpid").ToString
            FillProgramControls()
            SetControlDefaults()
        End If
        'FillProgramControls()
        FillAreasofInterestControl()
        ListBoxCounts()
        If HttpContext.Current.Request.Params("RESID").ToString = "822" Then DateOptionsSelector.Visible = False
        If HttpContext.Current.Request.Params("desc").ToString.ToLower().Substring(0,4) = "pell" Then 
            rblSchoolTypeList.visible=True
            SchoolReportingTypeHeader.InnerText="Cohort Type"
            rblSchoolType.Visible = False
            rblFullorFall.Visible = True 
            SubCohortTypeSelector.Visible=True
            lblDisplayReportDate.Text = "Report Date: 09/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "8/31/" & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
        ElseIf IPEDSControlType = ControlType.OutcomeMeasures Then
            rblSchoolTypeList.visible=False
            rblFullorFall.Visible = True 
            SchoolandCohortReportTypeSelector.Visible=False
            SubCohortTypeSelector.Visible=False
        Else
            rblSchoolTypeList.visible=False
            rblFullorFall.Visible = False 
            SchoolandCohortReportTypeSelector.Visible=False
            SubCohortTypeSelector.Visible=False
        End If 
    End Sub
    Protected Sub ReportingYearComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        If IPEDSControlType = ControlType.COMCIPDetailSumamry Or IPEDSControlType = ControlType.COMAllCompletesDetailSumamry Or IPEDSControlType = ControlType.COMCompletesByLevelDetailSumamry Then
            lblDisplayReportDate.Text = "Report Date: " & "07/01/" & Mid(RadComboBoxReportingYear.SelectedItem.Text, 1, 4) & " thru 06/30/" & CStr(CInt(Mid(RadComboBoxReportingYear.SelectedItem.Text, 1, 4)) + 1)
        End If
    End Sub
    Protected Sub CohortYearComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        Try
            'Dim YearPart As String = e.Value
            RadDateReportDate.SelectedDate = Nothing
            RadDateReportDate.MinDate = Nothing
            RadDateReportDate.MaxDate = Nothing
            'Dim StartMonthDayProgram As String = String.Empty
            'Dim EndMonthDayProgram As String = String.Empty
            'Dim StartMonthDayAcademic As String = String.Empty
            'Dim EndMonthDayAcademic As String = String.Empty



            'If rblSchoolType.SelectedIndex = 1 Then
            '    RadDateReportDate.MinDate = CreateDateValueYearPart(StartDatePartAcademic, YearPart)
            '    RadDateReportDate.MaxDate = CreateDateValueYearPart(EndDatePartAcademic, YearPart)
            '    RadDateReportDate.SelectedDate = RadDateReportDate.MaxDate.AddMonths(-2)
            'Else
            '    RadDateReportDate.MinDate = CreateDateValueYearPart(StartDatePartProgram, YearPart)
            '    RadDateReportDate.MaxDate = CreateDateValueYearPart(EndDatePartProgram, YearPart)
            'End If
            CreateReporterDateRange(CType(rblSchoolType.SelectedIndex, IPEDSReportingType), IPEDSControlType)
            Dim ReportDateStart As String = String.Empty
            Dim ReportDateEnd As String = String.Empty
            If Not String.IsNullOrWhiteSpace(lblDisplayReportDate.Text) Then
                'DE8476 - QA: IPEDS: Selecting another year on the Inst Chars reports is leading to an error page.
                If Len(lblDisplayReportDate.Text) > 30 Then
                    ReportDateStart = lblDisplayReportDate.Text.Substring(13, 6)
                    ReportDateEnd = lblDisplayReportDate.Text.Substring(29, 6)
                ElseIf (IPEDSControlType = ControlType.CDetail Or IPEDSControlType = ControlType.InstB3Summary) And
                           Len(lblDisplayReportDate.Text) > 20 Then
                    ReportDateStart = lblDisplayReportDate.Text.Substring(13, 6)
                End If
            End If

            If IPEDSControlType = ControlType.OutcomeMeasures AndAlso rblSchoolType.SelectedIndex = 0  Then
                ReportDateStart = "09/01/" 
                ReportDateEnd = "08/31/"
            ElseIf  Request.QueryString("desc").ToString().ToLower.Substring(0,4) = "pell" AndAlso rblFullorFall.SelectedIndex = 0
                 ReportDateStart = "09/01/" 
                ReportDateEnd = "08/31/"
            End If

            'DE8476 - QA: IPEDS: Selecting another year on the Inst Chars reports is leading to an error page.
            'And DE8478 - QA: IPEDS: Sorting by last name is printing the Enrollment ID at all times on the Fall reports.
            'If IPEDSControlType = ControlType.FallEnrollment Then
            If IPEDSControlType = ControlType.CDetail Or IPEDSControlType = ControlType.InstB3Summary Then
                lblDisplayReportDate.Text = "Report Date: " & ReportDateStart & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text))  '10/15/"
            ElseIf IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
                lblDisplayReportDate.Text = "Report Date: " & ReportDateStart & RadComboBoxCohortYear.SelectedItem.Text & " thru " & ReportDateEnd & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
            ElseIf IPEDSControlType = ControlType.GradRates2Years Or IPEDSControlType = ControlType.GradRates200Less2Yrs Or IPEDSControlType = ControlType.OutcomeMeasures or Request.QueryString("desc").ToString().ToLower.Substring(0,4) = "pell" Then
                lblDisplayReportDate.Text = "Report Date: " & ReportDateStart & RadComboBoxCohortYear.SelectedItem.Text & " thru " & ReportDateEnd & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
            Else
                lblDisplayReportDate.Text = "Report Date: " & ReportDateStart & RadComboBoxCohortYear.SelectedItem.Text & " thru " & ReportDateEnd & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text))
            End If

            'Else
            'lblDisplayReportDate.Text = "Report Date: 06/30/" & RadComboBoxCohortYear.SelectedItem.Text
            'lblDisplayReportDate.Text = "Report Date: " & ReportDateStart & RadComboBoxCohortYear.SelectedItem.Text & " thru " & ReportDateEnd & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
            'End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
            'Dim x As String = ex.ToString
        End Try

    End Sub
    Protected Sub CampusComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        RadListBoxProgram.Items.Clear()
        RadListBoxProgram2.Items.Clear()
        FillProgramControls()
        ListBoxCounts()
    End Sub
    Protected Sub CampusComboBox_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        Dim myItem As RadComboBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is syCampus Then
                Dim MysyCampus As syCampus = DirectCast(myItem.DataItem, syCampus)
                If MysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
        Dim LBox As RadListBox = DirectCast(sender, RadListBox)
        If LBox.ID.Contains("Program") Then
            FillProgramControls()
        ElseIf LBox.ID.Contains("AreasOfInterest") Then
            FillAreasofInterestControl()
        End If
        ListBoxCounts()
    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ChkBox As CheckBox = DirectCast(sender, CheckBox)
        If ChkBox.ID.Contains("Program") Then
            FillProgramControls()
        ElseIf ChkBox.ID.Contains("AreasOfInterest") Then
            FillAreasofInterestControl()
        End If
        ListBoxCounts()
    End Sub
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim Lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arProgram Then
                Dim MyarProgram As arProgram = DirectCast(myItem.DataItem, arProgram)
                If MyarProgram.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarProgram.ProgDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is arPrgGrp Then
                Dim MyarPrgGrp As arPrgGrp = DirectCast(myItem.DataItem, arPrgGrp)
                If MyarPrgGrp.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarPrgGrp.PrgGrpDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Protected Sub rblSchoolType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        If rblSchoolType.SelectedValue = "Academic" Or rblFullorFall.SelectedValue = "Fall" Then
            If Not IPEDSControlType = ControlType.GradRates2Years Or IPEDSControlType = ControlType.GradRates4 Or IPEDSControlType = ControlType.GradRates200Less2Yrs Then
                AcademicYearMsg.Visible = True
            Else
                AcademicYearMsg.Visible = False
            End If
            EnableReportDate(True)
            'RadDateReportDate.SelectedDate = Nothing
            'RadDateReportDate.MinDate = Nothing
            'RadDateReportDate.MaxDate = Nothing
            'RadDateReportDate.MinDate = CreateDateValueYearPart(StartDatePartAcademic, RadComboBoxCohortYear.SelectedValue)
            'RadDateReportDate.MaxDate = CreateDateValueYearPart(EndDatePartAcademic, RadComboBoxCohortYear.SelectedValue)
            'RadDateReportDate.SelectedDate = CreateDateValueYearPart(SelectedDatePartAcademic, RadComboBoxCohortYear.SelectedValue)
            'If IPEDSControlType = ControlType.FinAid Then
            '    radComboBoxInstitutionType.Visible = True
            '    radComboBoxInstitutionTypeLabel.Visible = True
            'End If

            If IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
                LargestProgramText.Visible = False
                RadComboLProgram.Visible = False
                ProgramSelector.Visible = True
            End If
            If rblFullorFall.SelectedValue.ToLower = "fall" Then _IsOutComeMeasures = True
        Else
            Dim reportname As String = Page.Title
            If IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
                If reportname.Contains("Part A") Then
                    RadComboLProgram.Visible = True
                    LargestProgramText.Visible = True
                    ProgramSelector.Visible = True
                End If
                If reportname.Contains("Part D") _
                    Or reportname.Contains("Part E") Then
                    RadComboLProgram.Visible = True
                    LargestProgramText.Visible = True
                    ProgramSelector.Visible = False
                End If
            End If
            AcademicYearMsg.Visible = False
            EnableReportDate(False)
            radComboBoxInstitutionType.Visible = False
            radComboBoxInstitutionTypeLabel.Visible = False
        End If
        HideShowInstitutionType()
        If rblFullorFall.SelectedValue = "Fall" Then
            CreateReporterDateRange(CType(rblFullorFall.SelectedIndex, IPEDSReportingType), IPEDSControlType)
        Else
            CreateReporterDateRange(CType(rblSchoolType.SelectedIndex, IPEDSReportingType), IPEDSControlType)
        End If

    End Sub
#End Region
#Region "Methods"
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillProgramControls()
        Try
            Dim SelectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxProgram.DataTextField = "ProgDescrip"
            RadListBoxProgram.DataValueField = "ProgId"
            RadListBoxProgram.DataSource = GetProgramsByCampusId(SelectedCampus, True)
            RadListBoxProgram.DataBind()


            RadComboLProgram.DataTextField = "ProgDescrip"
            RadComboLProgram.DataValueField = "ProgId"
            RadComboLProgram.DataSource = GetProgramsByCampusId(SelectedCampus, False)
            RadComboLProgram.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillAreasofInterestControl()
        Try
            RadListBoxAreasOfInterest.DataTextField = "PrgGrpDescrip"
            RadListBoxAreasOfInterest.DataValueField = "PrgGrpId"
            RadListBoxAreasOfInterest.DataSource = GetAreasOfInterest()
            RadListBoxAreasOfInterest.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Function GetCurrentlySelectedPrograms() As List(Of String)
        Dim SelectedPrograms As New List(Of String)
        Try
            If RadListBoxProgram2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim selected As String = item.Value
                    SelectedPrograms.Add(selected)
                Next
                Return SelectedPrograms
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCurrentlySelectedAreasOfInterest() As List(Of String)
        Dim SelectedAreasOfInterest As New List(Of String)
        Try
            If RadListBoxAreasOfInterest2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxAreasOfInterest2.Items
                    Dim selected As String = item.Value
                    SelectedAreasOfInterest.Add(selected)
                Next
                Return SelectedAreasOfInterest
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampus)
        Try
            result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString))
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetAreasOfInterest() As List(Of arPrgGrp)
        Dim DA As New ProgramGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arPrgGrp)
        Try
            Dim SelectedAreasOfInterest As List(Of String) = GetCurrentlySelectedAreasOfInterest()
            If SelectedAreasOfInterest Is Nothing Then
                result = DA.GetProgramGroups(GetStatusFilters("cbkInactiveAreasOfInterest"))
            Else
                result = DA.GetProgramGroups(GetStatusFilters("cbkInactiveAreasOfInterest"), SelectedAreasOfInterest)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetProgramsByCampusId(ByVal SelectedCampus As Guid, ByVal FilteredBySelected As Boolean) As List(Of arProgram)
        Dim DA As New ProgramDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arProgram)
        Try
            Dim SelectedPrograms As List(Of String) = GetCurrentlySelectedPrograms()
            If SelectedPrograms Is Nothing Or FilteredBySelected = False Then
                result = DA.GetProgramsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrograms"))
            Else
                result = DA.GetProgramsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrograms"), SelectedPrograms)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim StatusValues As New List(Of String)

        Try
            Dim CKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If CKbox.Checked = True Then
                StatusValues.Add("Active")
                StatusValues.Add("Inactive")
            Else
                StatusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return StatusValues
    End Function
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim DisplayTree As New RadTreeView
        DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim HeaderNode1 As New RadTreeNode
            HeaderNode1.Text = "Campus"
            HeaderNode1.Value = "Campus"
            HeaderNode1.CssClass = "TreeParentNode"
            Dim CampusNode As New RadTreeNode
            CampusNode.Text = RadComboCampus.SelectedItem.Text
            HeaderNode1.Nodes.Add(CampusNode)
            DisplayTree.Nodes.Add(HeaderNode1)
            If Not IPEDSControlType = ControlType.Instructions Then
                If RadListBoxProgram2.Items.Count > 0 Then
                    Dim reportname As String = Page.Title

                    Dim HeaderNode2 As New RadTreeNode
                    HeaderNode2.Text = "Program"
                    HeaderNode2.Value = "Program"
                    HeaderNode2.CssClass = "TreeParentNode"
                    For Each item As RadListBoxItem In RadListBoxProgram2.Items
                        Dim ProgramNode As New RadTreeNode
                        ProgramNode.Text = item.Text
                        HeaderNode2.Nodes.Add(ProgramNode)
                    Next
                    DisplayTree.Nodes.Add(HeaderNode2)

                End If
                If RadListBoxAreasOfInterest2.Items.Count > 0 Then
                    Dim HeaderNode3 As New RadTreeNode
                    HeaderNode3.Text = "Area Of Interest"
                    HeaderNode3.Value = "Area Of Interest"
                    HeaderNode3.CssClass = "TreeParentNode"
                    For Each item As RadListBoxItem In RadListBoxAreasOfInterest2.Items
                        Dim InterestNode As New RadTreeNode
                        InterestNode.Text = item.Text
                        HeaderNode3.Nodes.Add(InterestNode)
                    Next
                    DisplayTree.Nodes.Add(HeaderNode3)
                End If
                If IsDate(RadDateReportDate.SelectedDate) = True Then
                    If IPEDSControlType = ControlType.BDetail  _
                    Or IPEDSControlType = ControlType.CDetail _
                    Or IPEDSControlType = ControlType.BCSummary _
                    Or IPEDSControlType = ControlType.MissingData _
                    Or IPEDSControlType = ControlType.TwelveMonthDetail _
                    Or IPEDSControlType = ControlType.TwelveMonthSummary _
                    Or IPEDSControlType = ControlType.GradRates4 _
                    Or IPEDSControlType = ControlType.GradRates2Years _
                    Or IPEDSControlType = ControlType.GradRates200Less2Yrs _
                    Or IPEDSControlType = ControlType.FinAidA _
                    Or IPEDSControlType = ControlType.FinAidBCDE _
                    Or IPEDSControlType = ControlType.FallEnrollment _
                    Or IPEDSControlType = ControlType.InstB3Summary _
                    Or IPEDSControlType = ControlType.DisabilityService Then
                        Dim HeaderNode4 As New RadTreeNode
                        Dim ReportDateNode As New RadTreeNode
                        HeaderNode4.Text = "Report Date"
                        HeaderNode4.Value = "Report Date"
                        HeaderNode4.CssClass = "TreeParentNode"

                        If (   IPEDSControlType = ControlType.FinAidA _
                            Or IPEDSControlType = ControlType.FinAidBCDE) _
                        And rblSchoolType.SelectedValue = "Program" Then
                            Dim DisplayDateStr As String = lblDisplayReportDate.Text.Replace("Report Date: ", "")
                            ReportDateNode.Text = DisplayDateStr
                        Else
                            Dim DisplayDate As Date
                            DisplayDate = CDate(RadDateReportDate.SelectedDate)
                            ReportDateNode.Text = DisplayDate.ToString("d")
                        End If


                        HeaderNode4.Nodes.Add(ReportDateNode)
                        DisplayTree.Nodes.Add(HeaderNode4)
                    End If
                End If
                If IsDate(RadDatePriorTo.SelectedDate) = True Then
                    If IPEDSControlType = ControlType.MissingData Then
                        Dim HeaderNode5 As New RadTreeNode
                        Dim PriorToNode As New RadTreeNode
                        HeaderNode5.Text = "Exclude Students Prior To"
                        HeaderNode5.Value = "Exclude Students Prior To"
                        HeaderNode5.CssClass = "TreeParentNode"
                        Dim DisplayDate2 As Date = CDate(RadDatePriorTo.SelectedDate)
                        PriorToNode.Text = DisplayDate2.ToString("d")
                        HeaderNode5.Nodes.Add(PriorToNode)
                        DisplayTree.Nodes.Add(HeaderNode5)
                    End If
                End If
                If IPEDSControlType = ControlType.BDetail _
                Or IPEDSControlType = ControlType.CDetail _
                Or IPEDSControlType = ControlType.BCSummary _ 
                Or IPEDSControlType = ControlType.GradRates4 _
                Or IPEDSControlType = ControlType.GradRates2Years _
                Or IPEDSControlType = ControlType.GradRates200Less2Yrs _
                Or IPEDSControlType = ControlType.FinAidA _
                Or IPEDSControlType = ControlType.FinAidBCDE _
                Or IPEDSControlType = ControlType.FallEnrollment _
                Or IPEDSControlType = ControlType.InstB3Summary _ 
                Or IPEDSControlType = ControlType.DisabilityService Then
                    Dim HeaderNode6 As New RadTreeNode
                    Dim CohortYearNode As New RadTreeNode
                    HeaderNode6.Text = "Cohort Year"
                    HeaderNode6.Value = "Cohort Year"
                    HeaderNode6.CssClass = "TreeParentNode"
                    CohortYearNode.Text = RadComboBoxCohortYear.SelectedItem.Text
                    HeaderNode6.Nodes.Add(CohortYearNode)
                    DisplayTree.Nodes.Add(HeaderNode6)
                End If
                If IPEDSControlType = ControlType.COMSummary _
                    Or IPEDSControlType = ControlType.COMDetail _
                    Or IPEDSControlType = ControlType.COMCIPDetailSumamry _
                    Or IPEDSControlType = ControlType.COMAllCompletesDetailSumamry _
                    Or IPEDSControlType = ControlType.COMCompletesByLevelDetailSumamry Then
                    Dim HeaderNode7 As New RadTreeNode
                    Dim ReportingYearNode As New RadTreeNode
                    HeaderNode7.Text = "Reporting Year"
                    HeaderNode7.Value = "Reporting Year"
                    HeaderNode7.CssClass = "TreeParentNode"
                    ReportingYearNode.Text = RadComboBoxReportingYear.SelectedItem.Text
                    HeaderNode7.Nodes.Add(ReportingYearNode)
                    DisplayTree.Nodes.Add(HeaderNode7)
                End If
                If IPEDSControlType = ControlType.BDetail _ 
                Or IPEDSControlType = ControlType.CDetail _
                Or IPEDSControlType = ControlType.COMDetail _
                Or IPEDSControlType = ControlType.TwelveMonthDetail _
                Or IPEDSControlType = ControlType.MissingData _
                Or IPEDSControlType = ControlType.GradRates4 _
                Or IPEDSControlType = ControlType.GradRates2Years _
                Or IPEDSControlType = ControlType.GradRates200Less2Yrs _
                Or IPEDSControlType = ControlType.FinAidA _
                Or IPEDSControlType = ControlType.FinAidBCDE _
                Or IPEDSControlType = ControlType.FallEnrollment _
                Or IPEDSControlType = ControlType.COMCIPDetailSumamry _
                Or IPEDSControlType = ControlType.COMAllCompletesDetailSumamry _
                Or IPEDSControlType = ControlType.COMCompletesByLevelDetailSumamry _
                Or IPEDSControlType = ControlType.DisabilityService Then
                    Dim HeaderNode8 As New RadTreeNode
                    HeaderNode8.Text = "Sort Options"
                    HeaderNode8.Value = "Sort Options"
                    HeaderNode8.CssClass = "TreeParentNode"
                    Dim SortOptionsNode As New RadTreeNode
                    SortOptionsNode.Text = rblSortBy.SelectedItem.Text
                    HeaderNode8.Nodes.Add(SortOptionsNode)
                    DisplayTree.Nodes.Add(HeaderNode8)
                End If
                If IPEDSControlType = ControlType.BDetail _
                Or IPEDSControlType = ControlType.CDetail _
                Or IPEDSControlType = ControlType.COMDetail _
                Or IPEDSControlType = ControlType.BCSummary _
                Or IPEDSControlType = ControlType.GradRates4 _
                Or IPEDSControlType = ControlType.GradRates2Years _
                Or IPEDSControlType = ControlType.GradRates200Less2Yrs _
                Or IPEDSControlType = ControlType.FinAidA _
                Or IPEDSControlType = ControlType.FinAidBCDE _
                Or IPEDSControlType = ControlType.FallEnrollment _
                Or IPEDSControlType = ControlType.InstB3Summary _  
                Or IPEDSControlType = ControlType.DisabilityService Then
                    '
                    ' DE8446 - School Reporting whats to be hidden
                    '   Or IPEDSControlType = ControlType.COMCIPDetailSumamry _
                    '   Or IPEDSControlType = ControlType.COMAllCompletesDetailSumamry _
                    '   Or IPEDSControlType = ControlType.COMCompletesByLevelDetailSumamry Then
                    Dim HeaderNode9 As New RadTreeNode
                    If IPEDSControlType = ControlType.GradRates4 _
                    Or IPEDSControlType = ControlType.GradRates2Years _
                    Or IPEDSControlType = ControlType.GradRates200Less2Yrs Then
                        HeaderNode9.Text = "Cohort"
                        HeaderNode9.Value = "Cohort"
                    Else
                        HeaderNode9.Text = "School Reporting Type"
                        HeaderNode9.Value = "School Reporting Type"
                    End If
                    HeaderNode9.CssClass = "TreeParentNode"
                    Dim SchoolReportingTypeNode As New RadTreeNode
                    SchoolReportingTypeNode.Text = rblSchoolType.SelectedItem.Text
                    HeaderNode9.Nodes.Add(SchoolReportingTypeNode)
                    DisplayTree.Nodes.Add(HeaderNode9)
                End If
                If IPEDSControlType = ControlType.FinAidA _
                Or IPEDSControlType = ControlType.FinAidBCDE Then
                    If rblSchoolType.SelectedIndex = 1 Then
                        Dim HeaderNode10 As New RadTreeNode
                        HeaderNode10.Text = "Control Of Institution"
                        HeaderNode10.Value = "Control Of Institution"
                        HeaderNode10.CssClass = "TreeParentNode"
                        Dim ControlOfInstitutionNode As New RadTreeNode
                        ControlOfInstitutionNode.Text = radComboBoxInstitutionType.SelectedItem.Text
                        HeaderNode10.Nodes.Add(ControlOfInstitutionNode)
                        DisplayTree.Nodes.Add(HeaderNode10)
                    End If
                End If
                If IPEDSControlType = ControlType.FinAidA _
                Or IPEDSControlType = ControlType.FinAidBCDE Then
                    ' If rblSchoolType.SelectedIndex = 0 Then
                    Dim reportname As String = Page.Title
                    If reportname.Contains("Part A") _
                    Or reportname.Contains("Part D") _
                    Or reportname.Contains("Part E") Then
                        If rblSchoolType.SelectedValue = "Program" Then
                            Dim HeaderNode11 As New RadTreeNode
                            HeaderNode11.Text = "Largest Program"
                            HeaderNode11.Value = "Largest Program"
                            HeaderNode11.CssClass = "TreeParentNode"
                            Dim LargestProgramNode As New RadTreeNode
                            LargestProgramNode.Text = RadComboLProgram.SelectedItem.Text
                            HeaderNode11.Nodes.Add(LargestProgramNode)
                            DisplayTree.Nodes.Add(HeaderNode11)
                        End If
                    End If
                    'End If
                End If
            End If
            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            UserSettings.ItemName = ItemDetail.ItemName
            UserSettings.ItemId = ItemDetail.ItemId
            UserSettings.DetailId = ItemDetail.DetailId
            UserSettings.FriendlyName = ItemDetail.CaptionOverride

            'Campus
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo

            ctrlSettingCampus.ControlName = RadComboCampus.ID
            Dim selectedCampus As RadComboBoxItem = RadComboCampus.SelectedItem
            objControlValueInfo.DisplayText = selectedCampus.Text.Replace("'", "''")
            objControlValueInfo.KeyData = selectedCampus.Value.Replace("'", "''")
            ctrlValuesCampus.Add(objControlValueInfo)
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            SettingsCollection.Add(ctrlSettingCampus)

            'Program1
            Dim ctrlSettingProgram As New ControlSettingInfo
            Dim ctrlValuesProgram As New List(Of ControlValueInfo)
            ctrlSettingProgram.ControlName = RadListBoxProgram.ID
            For Each item As RadListBoxItem In RadListBoxProgram.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram.Add(objControlValueInfo2)
            Next
            ctrlSettingProgram.ControlValueCollection = ctrlValuesProgram
            SettingsCollection.Add(ctrlSettingProgram)
            'Program2
            Dim ctrlSettingProgram2 As New ControlSettingInfo
            Dim ctrlValuesProgram2 As New List(Of ControlValueInfo)


            ctrlSettingProgram2.ControlName = RadListBoxProgram2.ID
            For Each item As RadListBoxItem In RadListBoxProgram2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram2.Add(objControlValueInfo3)
            Next
            ctrlSettingProgram2.ControlValueCollection = ctrlValuesProgram2
            SettingsCollection.Add(ctrlSettingProgram2)

            'LargestProgram
            'Dim ctrlSettingLargestProgram As New ControlSettingInfo
            'Dim ctrlValuesLargestProgram As New List(Of ControlValueInfo)
            'ctrlSettingLargestProgram.ControlName = RadComboLProgram.ID
            'For Each item As RadComboBoxItem In RadComboLProgram.Items
            '    Dim objControlValueInfoLP As New ControlValueInfo
            '    objControlValueInfoLP.DisplayText = item.Text.Replace("'", "''")
            '    objControlValueInfoLP.KeyData = item.Value.Replace("'", "''")
            '    ctrlValuesLargestProgram.Add(objControlValueInfoLP)
            'Next
            'ctrlSettingLargestProgram.ControlValueCollection = ctrlValuesLargestProgram
            'SettingsCollection.Add(ctrlSettingLargestProgram)


            'LargestProgram
            Dim ctrlSettingLargestProgram As New ControlSettingInfo
            Dim ctrlValuesLargestProgram As New List(Of ControlValueInfo)
            Dim objControlValueInfoLP As New ControlValueInfo
            ctrlSettingLargestProgram.ControlName = RadComboLProgram.ID
            Dim selectedLargeProgram As RadComboBoxItem = RadComboLProgram.SelectedItem
            objControlValueInfoLP.DisplayText = selectedLargeProgram.Text.Replace("'", "''")
            objControlValueInfoLP.KeyData = selectedLargeProgram.Value.Replace("'", "''")

            ctrlValuesLargestProgram.Add(objControlValueInfoLP)
            ctrlSettingLargestProgram.ControlValueCollection = ctrlValuesLargestProgram
            SettingsCollection.Add(ctrlSettingLargestProgram)

            'AreasOfInterest1
            Dim ctrlSettingAreasOfInterest As New ControlSettingInfo
            Dim ctrlValuesAreasOfInterest As New List(Of ControlValueInfo)


            ctrlSettingAreasOfInterest.ControlName = RadListBoxAreasOfInterest.ID
            For Each item As RadListBoxItem In RadListBoxAreasOfInterest.Items
                Dim objControlValueInfo4 As New ControlValueInfo
                objControlValueInfo4.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo4.KeyData = item.Value.Replace("'", "''")
                ctrlValuesAreasOfInterest.Add(objControlValueInfo4)
            Next

            ctrlSettingAreasOfInterest.ControlValueCollection = ctrlValuesAreasOfInterest
            SettingsCollection.Add(ctrlSettingAreasOfInterest)
            'AreasOfInterest2
            Dim ctrlSettingAreasOfInterest2 As New ControlSettingInfo
            Dim ctrlValuesAreasOfInterest2 As New List(Of ControlValueInfo)


            ctrlSettingAreasOfInterest2.ControlName = RadListBoxAreasOfInterest2.ID
            For Each item As RadListBoxItem In RadListBoxAreasOfInterest2.Items
                Dim objControlValueInfo5 As New ControlValueInfo
                objControlValueInfo5.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo5.KeyData = item.Value.Replace("'", "''")
                ctrlValuesAreasOfInterest2.Add(objControlValueInfo5)
            Next
            ctrlSettingAreasOfInterest2.ControlValueCollection = ctrlValuesAreasOfInterest2
            SettingsCollection.Add(ctrlSettingAreasOfInterest2)

            'CohortYear
            Dim ctrlSettingCohortYear As New ControlSettingInfo
            Dim ctrlValuesCohortYear As New List(Of ControlValueInfo)
            Dim objControlValueInfo6 As New ControlValueInfo

            ctrlSettingCohortYear.ControlName = RadComboBoxCohortYear.ID

            If RadComboBoxCohortYear.SelectedItem Is Nothing Then
                objControlValueInfo6.DisplayText = "1900"
                objControlValueInfo6.KeyData = "1900"
            Else
                Dim selectedCohortYear As RadComboBoxItem = RadComboBoxCohortYear.SelectedItem
                objControlValueInfo6.DisplayText = selectedCohortYear.Text.Replace("'", "''")
                objControlValueInfo6.KeyData = selectedCohortYear.Value.Replace("'", "''")
            End If

            ctrlValuesCohortYear.Add(objControlValueInfo6)
            ctrlSettingCohortYear.ControlValueCollection = ctrlValuesCohortYear
            SettingsCollection.Add(ctrlSettingCohortYear)

            UserSettings.ControlSettingsCollection = SettingsCollection
            'ReportingYear
            Dim ctrlSettingReportingYear As New ControlSettingInfo
            Dim ctrlValuesReportingYear As New List(Of ControlValueInfo)
            Dim objControlValueInfo7 As New ControlValueInfo

            ctrlSettingReportingYear.ControlName = RadComboBoxReportingYear.ID
            If RadComboBoxReportingYear.SelectedItem Is Nothing Then
                objControlValueInfo7.DisplayText = "1900"
                objControlValueInfo7.KeyData = "1900"
            Else
                Dim selectedReportingYear As RadComboBoxItem = RadComboBoxReportingYear.SelectedItem
                objControlValueInfo7.DisplayText = selectedReportingYear.Text.Replace("'", "''")
                objControlValueInfo7.KeyData = selectedReportingYear.Value.Replace("'", "''")
            End If


            ctrlValuesReportingYear.Add(objControlValueInfo7)
            ctrlSettingReportingYear.ControlValueCollection = ctrlValuesReportingYear
            SettingsCollection.Add(ctrlSettingReportingYear)

            'School Reporting Type
            Dim ctrlSettingSchoolReportingType As New ControlSettingInfo
            Dim ctrlValuesSchoolReportingType As New List(Of ControlValueInfo)
            Dim objControlValueInfo8 As New ControlValueInfo
            Dim isOutComeMeasures As Boolean = False
            Dim reportDesc As String = ""
            Try
                reportDesc = HttpContext.Current.Request.Params("desc").ToString.ToLower
                If Not String.IsNullOrEmpty(reportDesc) AndAlso reportDesc = "outcome measures" Then
                    isOutComeMeasures = True
                End If

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                isOutComeMeasures = False
            End Try
            If rblFullorFall.SelectedValue.ToLower = "fall" AndAlso isOutComeMeasures = True Then
                ctrlSettingSchoolReportingType.ControlName = rblSchoolType.ID
                objControlValueInfo8.DisplayText = rblFullorFall.SelectedItem.Text.Replace("'", "''")
                objControlValueInfo8.KeyData = rblFullorFall.SelectedValue.Replace("'", "''")
            ElseIf rblFullorFall.SelectedValue.ToLower = "full" AndAlso isOutComeMeasures = True Then
                ctrlSettingSchoolReportingType.ControlName = rblSchoolType.ID
                objControlValueInfo8.DisplayText = rblFullorFall.SelectedItem.Text.Replace("'", "''")
                objControlValueInfo8.KeyData = rblFullorFall.SelectedValue.Replace("'", "''")
             ElseIf rblFullorFall.SelectedValue.ToLower = "fall" AndAlso Request.QueryString("desc").ToString().ToLower.Substring(0,4) = "pell" Then
                ctrlSettingSchoolReportingType.ControlName = rblFullorFall.ID
                objControlValueInfo8.DisplayText = rblFullorFall.SelectedItem.Text.Replace("'", "''")
                objControlValueInfo8.KeyData = rblFullorFall.SelectedValue.Replace("'", "''")
            ElseIf rblFullorFall.SelectedValue.ToLower = "full" AndAlso Request.QueryString("desc").ToString().ToLower.Substring(0,4) = "pell" Then
                ctrlSettingSchoolReportingType.ControlName = rblFullorFall.ID
                objControlValueInfo8.DisplayText = rblFullorFall.SelectedItem.Text.Replace("'", "''")
                objControlValueInfo8.KeyData = rblFullorFall.SelectedValue.Replace("'", "''")
            Else
                ctrlSettingSchoolReportingType.ControlName = rblSchoolType.ID
                objControlValueInfo8.DisplayText = rblSchoolType.SelectedItem.Text.Replace("'", "''")
                objControlValueInfo8.KeyData = rblSchoolType.SelectedValue.Replace("'", "''")
            End If

            ctrlValuesSchoolReportingType.Add(objControlValueInfo8)
            ctrlSettingSchoolReportingType.ControlValueCollection = ctrlValuesSchoolReportingType
            SettingsCollection.Add(ctrlSettingSchoolReportingType)

            Dim ctrlSettingSchoolReportingType15 As New ControlSettingInfo
            Dim ctrlValuesSchoolReportingType15 As New List(Of ControlValueInfo)
            Dim objControlValueInfo15 As New ControlValueInfo

            ctrlSettingSchoolReportingType15.ControlName = rblSchoolTypeList.ID
            objControlValueInfo15.DisplayText = rblSchoolTypeList.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo15.KeyData = rblSchoolTypeList.SelectedValue.Replace("'", "''")

            ctrlValuesSchoolReportingType15.Add(objControlValueInfo15)
            ctrlSettingSchoolReportingType15.ControlValueCollection = ctrlValuesSchoolReportingType15
            SettingsCollection.Add(ctrlSettingSchoolReportingType15)



             Dim ctrlSettingSchoolReportingType16 As New ControlSettingInfo
            Dim ctrlValuesSchoolReportingType16 As New List(Of ControlValueInfo)
            Dim objControlValueInfo16 As New ControlValueInfo

            ctrlSettingSchoolReportingType16.ControlName = radsubcohort.ID
            objControlValueInfo16.DisplayText = radsubcohort.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo16.KeyData = radsubcohort.SelectedValue.Replace("'", "''")

            ctrlValuesSchoolReportingType16.Add(objControlValueInfo16)
            ctrlSettingSchoolReportingType16.ControlValueCollection = ctrlValuesSchoolReportingType16
            SettingsCollection.Add(ctrlSettingSchoolReportingType16)

            'Sort Options
            Dim ctrlSettingSortOptions As New ControlSettingInfo
            Dim ctrlValuesSortOptions As New List(Of ControlValueInfo)
            Dim objControlValueInfo9 As New ControlValueInfo

            ctrlSettingSortOptions.ControlName = rblSortBy.ID
            objControlValueInfo9.DisplayText = rblSortBy.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo9.KeyData = rblSortBy.SelectedValue.Replace("'", "''")

            ctrlValuesSortOptions.Add(objControlValueInfo9)
            ctrlSettingSortOptions.ControlValueCollection = ctrlValuesSortOptions
            SettingsCollection.Add(ctrlSettingSortOptions)

            'Report Date
            Dim ctrlSettingReportDate As New ControlSettingInfo
            Dim ctrlValuesReportDate As New List(Of ControlValueInfo)
            Dim objControlValueInfo10 As New ControlValueInfo

            ctrlSettingReportDate.ControlName = RadDateReportDate.ID
            If RadDateReportDate.SelectedDate Is Nothing Then
                objControlValueInfo10.DisplayText = "01/01/1900"
                objControlValueInfo10.KeyData = "01/01/1900"
            Else
                objControlValueInfo10.DisplayText = RadDateReportDate.SelectedDate.ToString
                objControlValueInfo10.KeyData = RadDateReportDate.SelectedDate.ToString
            End If
            ctrlValuesReportDate.Add(objControlValueInfo10)
            ctrlSettingReportDate.ControlValueCollection = ctrlValuesReportDate
            SettingsCollection.Add(ctrlSettingReportDate)

            'PriorTo
            Dim ctrlSettingPriorTo As New ControlSettingInfo
            Dim ctrlValuesPriorTo As New List(Of ControlValueInfo)
            Dim objControlValueInfo11 As New ControlValueInfo

            ctrlSettingPriorTo.ControlName = RadDatePriorTo.ID
            If RadDatePriorTo.SelectedDate Is Nothing Then
                objControlValueInfo11.DisplayText = "01/01/1900"
                objControlValueInfo11.KeyData = "01/01/1900"
            Else
                objControlValueInfo11.DisplayText = RadDatePriorTo.SelectedDate.ToString
                objControlValueInfo11.KeyData = RadDatePriorTo.SelectedDate.ToString
            End If
            ctrlValuesPriorTo.Add(objControlValueInfo11)
            ctrlSettingPriorTo.ControlValueCollection = ctrlValuesPriorTo
            SettingsCollection.Add(ctrlSettingPriorTo)

            'Institution Type
            Dim ctrlSettingInstitutionType As New ControlSettingInfo
            Dim ctrlValuesInstitutionType As New List(Of ControlValueInfo)
            Dim objControlValueInfo12 As New ControlValueInfo

            ctrlSettingInstitutionType.ControlName = radComboBoxInstitutionType.ID
            objControlValueInfo12.DisplayText = radComboBoxInstitutionType.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo12.KeyData = radComboBoxInstitutionType.SelectedValue.Replace("'", "''")

            ctrlValuesInstitutionType.Add(objControlValueInfo12)
            ctrlSettingInstitutionType.ControlValueCollection = ctrlValuesInstitutionType
            SettingsCollection.Add(ctrlSettingInstitutionType)


            UserSettings.ControlSettingsCollection = SettingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return UserSettings
    End Function
    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim ComboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If Not ItemValue.KeyData = "1900" Then
                                    ComboBox.SelectedValue = ItemValue.KeyData
                                End If

                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim LstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            LstBox.Items.Clear()
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = ItemValue.DisplayText
                                newItem.Value = ItemValue.KeyData
                                LstBox.SelectedValue = ItemValue.KeyData
                                LstBox.Items.Add(newItem)
                            Next
                        ElseIf TypeOf ctrl Is RadDatePicker Then
                            Dim DPicker As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If Not ItemValue.KeyData = "01/01/1900" Then
                                    'Get the current min and max dates
                                    Dim MinDate As Date = DPicker.MinDate
                                    Dim MaxDate As Date = DPicker.MaxDate
                                    'loosen the validation to allow the population of the datepicker from the saved settings
                                    'since it has already been validated on entry
                                    DPicker.MinDate = DPicker.MinDate.AddYears(-20)
                                    DPicker.MaxDate = DPicker.MaxDate.AddYears(20)
                                    'populate the date picker
                                    DPicker.SelectedDate = CDate(ItemValue.KeyData)
                                    'restore the min and max dates to the proper values
                                    DPicker.MinDate = MinDate
                                    DPicker.MaxDate = MaxDate
                                End If
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadListBoxProgram.Items.Clear()
                RadListBoxProgram2.Items.Clear()
                RadListBoxAreasOfInterest.Items.Clear()
                RadListBoxAreasOfInterest2.Items.Clear()
            End If
            ListBoxCounts()
            HideShowInstitutionType()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub ListBoxCounts()
        Dim boxcount1 As Integer = RadListBoxProgram.Items.Count()
        Dim boxcount2 As Integer = RadListBoxProgram2.Items.Count()
        Dim boxcount3 As Integer = RadListBoxAreasOfInterest.Items.Count()
        Dim boxcount4 As Integer = RadListBoxAreasOfInterest2.Items.Count()

        lblProgramCounterAvailable.Text = boxcount1.ToString.Trim & " available"
        lblProgramCounterAvailable.ToolTip = boxcount1.ToString.Trim & " available " & Caption & "(s)"
        lblProgramCounterSelected.Text = boxcount2.ToString.Trim & " selected"
        lblProgramCounterSelected.ToolTip = boxcount2.ToString.Trim & " selected " & Caption & "(s)"

        lblAreasOfInterestCounterAvailable.Text = boxcount3.ToString.Trim & " available"
        lblAreasOfInterestCounterAvailable.ToolTip = boxcount3.ToString.Trim & " available " & Caption & "(s)"
        lblAreasOfInterestCounterSelected.Text = boxcount4.ToString.Trim & " selected"
        lblAreasOfInterestCounterSelected.ToolTip = boxcount4.ToString.Trim & " selected " & Caption & "(s)"

    End Sub
    Private Sub CreateYearComboBoxValues(ByVal ctrl As RadComboBox, ByVal YearMod As Integer)
        ctrl.Items.Clear()
        Do Until YearMod = -11
            Dim result As String = Year(DateTime.Now.AddYears(YearMod)).ToString
            ctrl.Items.Add(New RadComboBoxItem(result, result))
            YearMod = YearMod - 1
        Loop
    End Sub
    Private Sub CreateYearComboBoxValues(ByVal ctrl As RadComboBox, ByVal strMonth As String, ByVal strDay As String, ByVal YearMod As Integer)
        ctrl.Items.Clear()
        Do Until YearMod = -11
            Dim result As String = Year(DateTime.Now.AddYears(YearMod)).ToString
            ctrl.Items.Add(New RadComboBoxItem(result, result))
            YearMod = YearMod - 1
        Loop
    End Sub
    Private Sub CreateYearRangeComboBoxValues(ByVal ctrl As RadComboBox, ByVal YearMod As Integer, ByVal YearRange As Integer)
        ctrl.Items.Clear()
        Do Until YearMod = -9
            Dim StartRange As String = Year(DateTime.Now.AddYears(YearMod - YearRange)).ToString
            Dim EndRange As String = Year(DateTime.Now.AddYears(YearMod)).ToString
            Dim result As String = StartRange & "-" & EndRange
            ctrl.Items.Add(New RadComboBoxItem(result, result))
            YearMod = YearMod - 1
        Loop
    End Sub
    Private Function CreateDateValueYearMod(ByVal MonthDayPart As String, ByVal YearMod As Integer) As Date
        Try
            Dim PriorToDate As Date
            Dim YearPart As String = Year(DateTime.Now.AddYears(YearMod)).ToString
            Dim DateString As String = MonthDayPart & YearPart
            PriorToDate = CDate(DateString)
            If IsDate(PriorToDate) = True Then
                Return PriorToDate
            Else
                Throw New Exception("Invalid Date")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function CreateDateValueYearPart(ByVal MonthDayPart As String, ByVal YearPart As String) As Date
        Try
            Dim PriorToDate As Date
            Dim DateString As String = MonthDayPart & YearPart
            PriorToDate = CDate(DateString)
            If IsDate(PriorToDate) = True Then
                Return PriorToDate
            Else
                Throw New Exception("Invalid Date")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Sub EnableReportDate(ByVal IsEnabled As Boolean)
        Try
            If IsEnabled = True Then
                DatePickerRDateText.Enabled = True
                DatePickerRDateText.ForeColor = Nothing
                RadDateReportDate.Enabled = True
                RadDateReportDate.ToolTip = Nothing
                RadDateReportDate.Visible = True
                DatePickerRDateText.Visible = True
                lblDisplayReportDate.Visible = False
            Else
                'DatePickerRDateText.Enabled = False
                'DatePickerRDateText.ForeColor = System.Drawing.ColorTranslator.FromHtml("#848484")
                'RadDateReportDate.Enabled = False
                RadDateReportDate.SelectedDate = Nothing
                'RadDateReportDate.ToolTip = "Disabled"
                RadDateReportDate.Visible = False
                DatePickerRDateText.Visible = False
                lblDisplayReportDate.Visible = True
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Public Function CreateReporterDateRange(ByVal ReportingType As IPEDSReportingType, ByVal IPEDSControlType As ControlType) As String
        Dim Msg As String = String.Empty
        Dim StartMonthDayProgram As String = String.Empty
        Dim EndMonthDayProgram As String = String.Empty
        Dim StartMonthDayAcademic As String = String.Empty
        Dim EndMonthDayAcademic As String = String.Empty
        Dim SelectedMonthDayProgram As String = "08/31/"
        Dim SelectedMonthDayAcademic As String = "10/15/"
        If String.IsNullOrEmpty(ProgramReporterStart) Then
            StartMonthDayProgram = StartDatePartProgram
        Else
            StartMonthDayProgram = ProgramReporterStart
        End If
        If String.IsNullOrEmpty(ProgramReporterEnd) Then
            EndMonthDayProgram = EndDatePartProgram
            SelectedMonthDayProgram = SelectedDatePartProgram
        Else
            EndMonthDayProgram = ProgramReporterEnd
            SelectedMonthDayProgram = SelectedDatePartProgram
        End If
        If String.IsNullOrEmpty(AcademicYearStart) Then
            StartMonthDayAcademic = StartDatePartAcademic
        Else
            StartMonthDayAcademic = AcademicYearStart
        End If
        If String.IsNullOrEmpty(AcademicYearEnd) Then
            EndMonthDayAcademic = EndDatePartAcademic
            SelectedMonthDayAcademic = SelectedDatePartAcademic
        Else
            EndMonthDayAcademic = AcademicYearEnd
        End If
        Dim CohortYear As String = RadComboBoxCohortYear.SelectedValue
        If (IPEDSControlType = ControlType.FinAidA And ReportingType = IPEDSReportingType.ProgramReporter) Or _
             (IPEDSControlType = ControlType.GradRates4 And ReportingType = IPEDSReportingType.ProgramReporter) Or _
              (IPEDSControlType = ControlType.GradRates2Years And ReportingType = IPEDSReportingType.ProgramReporter) Or _
               (IPEDSControlType = ControlType.GradRates200Less2Yrs And ReportingType = IPEDSReportingType.ProgramReporter) Or _
            IPEDSControlType = ControlType.FinAidBCDE Then
            CohortYear = CStr(CInt(CohortYear) + 1)
        End If
        Dim StartDate As Date
        Dim EndDate As Date
        Dim SelectedDate As Date

        Try
            Select Case ReportingType
                Case IPEDSReportingType.ProgramReporter
                    If Not String.IsNullOrEmpty(CohortYear) = True Then
                        If IPEDSControlType = ControlType.FinAidBCDE Or IPEDSControlType = ControlType.FinAidA Then
                            StartDate = CDate(EndMonthDayProgram & CohortYear)
                            EndDate = CDate(EndMonthDayProgram & CohortYear)
                            SelectedDate = CDate(SelectedMonthDayProgram & CohortYear)
                        ElseIf IPEDSControlType = ControlType.Finance Then
                            Dim FinStartYear As String = CStr(CInt(CohortYear) - 20)
                            StartDate = CDate(StartMonthDayProgram & FinStartYear)
                            EndDate = CDate(EndMonthDayProgram & CohortYear)
                            SelectedDate = CDate(SelectedMonthDayProgram & CohortYear)
                        ElseIf IPEDSControlType = ControlType.OutcomeMeasures Then
                            StartDate = CDate(StartMonthDayProgram & CohortYear)
                            EndDate = CDate(EndMonthDayProgram & CohortYear)
                            SelectedDate = CDate("10/15/" + RadComboBoxCohortYear.SelectedValue)
                        Else
                            StartDate = CDate(StartMonthDayProgram & CohortYear)
                            EndDate = CDate(EndMonthDayProgram & CohortYear)
                            SelectedDate = CDate(SelectedMonthDayProgram & CohortYear)
                        End If


                        If EndDate < SelectedDate Then
                            EndDate = SelectedDate
                        End If


                        RadDateReportDate.MinDate = StartDate
                        RadDateReportDate.MaxDate = EndDate
                        RadDateReportDate.SelectedDate = SelectedDate


                        'RadDateReportDate.MinDate = EndDate
                        'RadDateReportDate.MaxDate = EndDate
                        'RadDateReportDate.SelectedDate = SelectedDate
                        'End If
                        Msg = StartDate.ToString("d") & " to " & EndDate.ToString("d") & " using Program Reporting"
                    End If
                Case IPEDSReportingType.AcademicYearReporter
                    If Not String.IsNullOrEmpty(CohortYear) = True Then
                        StartDate = CDate(StartMonthDayAcademic & CohortYear)
                        EndDate = CDate(EndMonthDayAcademic & CohortYear)
                        SelectedDate = CDate(SelectedMonthDayAcademic & CohortYear)
                        If EndDate < SelectedDate Then
                            EndDate = SelectedDate
                        End If

                        RadDateReportDate.MinDate = Nothing
                        RadDateReportDate.MaxDate = Nothing
                        RadDateReportDate.SelectedDate = Nothing
                        Select Case IPEDSControlType
                            'Case ControlType.FinAidA, ControlType.GradRates4, ControlType.GradRates2Years
                            '    RadDateReportDate.MinDate = SelectedDate
                            '    RadDateReportDate.MaxDate = SelectedDate
                            '    RadDateReportDate.SelectedDate = SelectedDate
                            Case ControlType.FinAidBCDE
                                'RadDateReportDate.MinDate = DateAdd(DateInterval.Year, -1, StartDate)
                                'RadDateReportDate.MaxDate = EndDate
                                'RadDateReportDate.SelectedDate = SelectedDate
                                RadDateReportDate.MinDate = CDate("09/01/" & RadComboBoxCohortYear.SelectedItem.Text)
                                RadDateReportDate.MaxDate = CDate("12/15/" & RadComboBoxCohortYear.SelectedItem.Text)
                                RadDateReportDate.SelectedDate = CDate("10/15/" & RadComboBoxCohortYear.SelectedItem.Text)
                            Case ControlType.OutcomeMeasures
                                RadDateReportDate.MinDate = CDate("09/01/" & RadComboBoxCohortYear.SelectedItem.Text)
                                RadDateReportDate.MaxDate = CDate("12/15/" & RadComboBoxCohortYear.SelectedItem.Text)
                                RadDateReportDate.SelectedDate = CDate("10/15/" + RadComboBoxCohortYear.SelectedValue)
                            Case Else
                                RadDateReportDate.MinDate = StartDate
                                RadDateReportDate.MaxDate = EndDate
                                RadDateReportDate.SelectedDate = SelectedDate
                        End Select
                        Msg = StartDate.ToString("d") & " to " & EndDate.ToString("d") & " using Academic Year Reporting"
                    End If
                Case IPEDSReportingType.Fall
                    If Not String.IsNullOrEmpty(CohortYear) = True Then
                        StartDate = CDate(StartMonthDayAcademic & CohortYear)
                        EndDate = CDate(EndMonthDayAcademic & CohortYear)
                        SelectedDate = CDate(SelectedMonthDayAcademic & CohortYear)
                        If EndDate < SelectedDate Then
                            EndDate = SelectedDate
                        End If

                        RadDateReportDate.MinDate = Nothing
                        RadDateReportDate.MaxDate = Nothing
                        RadDateReportDate.SelectedDate = Nothing

                        Select Case IPEDSControlType
                            Case ControlType.OutcomeMeasures
                                RadDateReportDate.MinDate = StartDate
                                RadDateReportDate.MaxDate = EndDate
                                RadDateReportDate.SelectedDate = CDate("10/15/" + RadComboBoxCohortYear.SelectedValue)
                        End Select
                    End If
                Case Else
                    Throw New Exception("Unknown IPEDS Reporting Type")
            End Select
            Return Msg
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Sub SetControlDefaults()
        Try
            Dim MyDate As Date = DateTime.Now

            StudentIdentifier = MyAdvAppSettings.AppSettings("StudentIdentifier").ToString
            If StudentIdentifier = "SSN" Then
                rblSortBy.Items(0).Value = "SSN"
                rblSortBy.Items(0).Text = "SSN"
            Else
                'Fin Aid reports not using Enrollment id for sorting - 11/27/2012
                If IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
                    rblSortBy.Items(0).Value = "Student Number"
                    rblSortBy.Items(0).Text = "Student Number"
                Else
                    If StudentIdentifier = "EnrollmentId" Then
                        rblSortBy.Items(0).Value = "EnrollmentId"
                        rblSortBy.Items(0).Text = "Enrollment ID"
                    Else
                        rblSortBy.Items(0).Value = "Student Number"
                        rblSortBy.Items(0).Text = "Student Number"
                    End If
                End If
            End If


            Select Case IPEDSControlType
                Case ControlType.Instructions
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = False
                    DisplaySort.Visible = False
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False
                    DateOptionsHeader.Visible = False
                    DatePickerRDateText.Visible = False
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    RadDateReportDate.Visible = False
                    RadComboBoxCohortYear.Visible = False
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = False
                    RadComboBoxReportingYearLabel.Visible = False
                Case ControlType.MissingData
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = True
                    RadDatePriorTo.Visible = True
                    RadDateReportDate.Visible = True
                    EnableReportDate(True)
                    RadComboBoxCohortYear.Visible = False
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = False
                    RadComboBoxReportingYearLabel.Visible = False
                    RadDateReportDate.MinDate = Nothing
                    RadDateReportDate.MaxDate = Nothing
                    RadDateReportDate.SelectedDate = Nothing
                    RadDateReportDate.MinDate = MyDate.AddYears(-6)
                    RadDateReportDate.MaxDate = MyDate
                    RadDateReportDate.SelectedDate = MyDate
                    RadDatePriorTo.SelectedDate = CreateDateValueYearMod("09/01/", -4)
                    RadDatePriorTo.MinDate = MyDate.AddYears(-10)
                    RadDatePriorTo.MaxDate = MyDate
                Case ControlType.TwelveMonthDetail
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    EnableReportDate(True)
                    RadComboBoxCohortYear.Visible = False
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = False
                    RadComboBoxReportingYearLabel.Visible = False
                    RadDateReportDate.MinDate = Nothing
                    RadDateReportDate.MaxDate = Nothing
                    RadDateReportDate.SelectedDate = Nothing
                    RadDateReportDate.MinDate = CreateDateValueYearMod("06/30/", -6)
                    RadDateReportDate.MaxDate = CreateDateValueYearMod("06/30/", 0)
                    RadDateReportDate.SelectedDate = CreateDateValueYearMod("06/30/", 0)
                Case ControlType.TwelveMonthSummary
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = False
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    RadDateReportDate.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    RadComboBoxCohortYear.Visible = False
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = False
                    RadComboBoxReportingYearLabel.Visible = False
                    RadDateReportDate.MinDate = Nothing
                    RadDateReportDate.MaxDate = Nothing
                    RadDateReportDate.SelectedDate = Nothing
                    RadDateReportDate.MinDate = CreateDateValueYearMod("06/30/", -6)
                    RadDateReportDate.MaxDate = CreateDateValueYearMod("06/30/", 0)
                    RadDateReportDate.SelectedDate = CreateDateValueYearMod("06/30/", 0)
                Case ControlType.BDetail, ControlType.FallEnrollment, ControlType.DisabilityService
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = True
                    rblSchoolType.Visible = True
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    EnableReportDate(False)
                    RadComboBoxCohortYear.Visible = True
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = True
                    RadComboBoxReportingYearLabel.Visible = False

                    If IPEDSControlType = ControlType.BDetail Then
                        CreateYearComboBoxValues(RadComboBoxCohortYear, 0)
                    Else If IPEDSControlType = ControlType.DisabilityService Then
                          CreateYearComboBoxValues(RadComboBoxCohortYear, -1)
                    Else
                        'DE8457 - QA: IPEDS: Report date on Fall enrollment reports not driven by system date.
                        'CreateYearComboBoxValues(RadComboBoxCohortYear, -1)
                        If Month(Now) >= 9 Then
                            CreateYearComboBoxValues(RadComboBoxCohortYear, 0)
                        Else
                            CreateYearComboBoxValues(RadComboBoxCohortYear, -1)
                        End If
                    End If
                    'DE8476 - QA: IPEDS: Selecting another year on the Inst Chars reports is leading to an error page.
                    'If IPEDSControlType = ControlType.FallEnrollment Then
                    lblDisplayReportDate.Visible = True
                    lblDisplayReportDate.Text = "Report Date: 08/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "10/31/" & RadComboBoxCohortYear.SelectedItem.Text
                    'End If
                Case ControlType.OutcomeMeasures
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = True
                    rblSchoolType.Visible = False
                    rblFullorFall.Visible = True
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    EnableReportDate(False)
                    RadComboBoxCohortYear.Visible = True
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = True
                    RadComboBoxReportingYearLabel.Visible = False



                    If IPEDSControlType = ControlType.BDetail Then
                        CreateYearComboBoxValues(RadComboBoxCohortYear, 0)
                    Else
                        'DE8457 - QA: IPEDS: Report date on Fall enrollment reports not driven by system date.
                        'CreateYearComboBoxValues(RadComboBoxCohortYear, -1)
                        If Month(Now) >= 9 Then
                            CreateYearComboBoxValues(RadComboBoxCohortYear, 0)
                        Else
                            CreateYearComboBoxValues(RadComboBoxCohortYear, -9)
                        End If
                    End If
                    RadComboBoxCohortYear.SelectedValue = "2007"
                    'DE8476 - QA: IPEDS: Selecting another year on the Inst Chars reports is leading to an error page.
                    'If IPEDSControlType = ControlType.FallEnrollment Then
                    lblDisplayReportDate.Visible = True

                    If rblFullorFall.SelectedValue = "Full" Then
                        lblDisplayReportDate.Text = "Report Date: 09/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "8/31/" & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
                    Else
                        lblDisplayReportDate.Text = "Report Date: 09/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "12/15/" & RadComboBoxCohortYear.SelectedItem.Text
                    End If


                    'End If
                Case ControlType.CDetail
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = True
                    SchoolReportingTypeHeader.Visible = True
                    rblSchoolType.Visible = True
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    EnableReportDate(False)
                    RadComboBoxCohortYear.Visible = True
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = True
                    RadComboBoxReportingYearLabel.Visible = False
                    CreateYearComboBoxValues(RadComboBoxCohortYear, 0)
                    lblDisplayReportDate.Visible = True
                    lblDisplayReportDate.Text = "Report Date: 10/15/" & RadComboBoxCohortYear.SelectedItem.Text
                Case ControlType.BCSummary, ControlType.InstB3Summary
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = False
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = True
                    rblSchoolType.Visible = True
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    EnableReportDate(False)
                    RadComboBoxCohortYear.Visible = True
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = True
                    RadComboBoxReportingYearLabel.Visible = False
                    CreateYearComboBoxValues(RadComboBoxCohortYear, 0)
                    lblDisplayReportDate.Visible = True
                    'DE8476 - QA: IPEDS: Selecting another year on the Inst Chars reports is leading to an error page.
                    If IPEDSControlType = ControlType.InstB3Summary Then
                        lblDisplayReportDate.Text = "Report Date: 10/15/" & RadComboBoxCohortYear.SelectedItem.Text
                    Else
                        lblDisplayReportDate.Text = "Report Date: 08/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "10/31/" & RadComboBoxCohortYear.SelectedItem.Text
                    End If

                Case ControlType.COMDetail, ControlType.COMCIPDetailSumamry, ControlType.COMAllCompletesDetailSumamry, ControlType.COMCompletesByLevelDetailSumamry
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False
                    DateOptionsHeader.Visible = False
                    'DatePickerRDateText.Visible = True
                    EnableReportDate(False)
                    'DE8446 - Udate to show the datapicker control in the disabled state
                    RadDateReportDate.Visible = False
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    RadComboBoxCohortYear.Visible = False
                    RadComboBoxReportingYear.Visible = True
                    RadComboBoxCohortYearLabel.Visible = False
                    RadComboBoxReportingYearLabel.Visible = True
                    CreateYearRangeComboBoxValues(RadComboBoxReportingYear, 1, 1)
                    RadComboBoxReportingYear.SelectedIndex = 1
                    lblDisplayReportDate.Visible = True
                    lblDisplayReportDate.Text = "Report Date: " & "07/01/" & Mid(RadComboBoxReportingYear.SelectedItem.Text, 1, 4) & " thru 06/30/" & CStr(CInt(Mid(RadComboBoxReportingYear.SelectedItem.Text, 1, 4)) + 1)
                Case ControlType.COMSummary
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = False
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False
                    DateOptionsHeader.Visible = False
                    DatePickerRDateText.Visible = True
                    EnableReportDate(False)
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    RadComboBoxCohortYear.Visible = False
                    RadComboBoxReportingYear.Visible = True
                    RadComboBoxCohortYearLabel.Visible = False
                    RadComboBoxReportingYearLabel.Visible = True
                    CreateYearRangeComboBoxValues(RadComboBoxReportingYear, 1, 1)
                    RadComboBoxReportingYear.SelectedIndex = 1
                Case ControlType.Finance
                    RadComboCampus.Visible = True
                    CampusHeader.Visible = True
                    ProgramSelector.Visible = False
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    EnableReportDate(True)
                    RadComboBoxCohortYear.Visible = False
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = False
                    RadComboBoxReportingYearLabel.Visible = False
                    CreateYearComboBoxValues(RadComboBoxCohortYear, -1)
                    'RadDateReportDate.MinDate = Nothing
                    'RadDateReportDate.MaxDate = Nothing
                    'RadDateReportDate.SelectedDate = Nothing
                    'RadDateReportDate.MinDate = MyDate.AddYears(-6)
                    'RadDateReportDate.MaxDate = MyDate
                Case ControlType.GradRates4, ControlType.GradRates2Years, ControlType.GradRates200Less2Yrs
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = True
                    rblSchoolType.Visible = True
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    EnableReportDate(False)
                    RadComboBoxCohortYear.Visible = True
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = True
                    RadComboBoxReportingYearLabel.Visible = False
                    CreateYearComboBoxValues(RadComboBoxCohortYear, -1)
                    SchoolReportingTypeHeader.InnerText = "Cohort"
                    rblSchoolType.Items(0).Text = "Full Year (Program Reporter)"
                    rblSchoolType.Items(1).Text = "Fall (Academic Reporter)"
                    rblSchoolType.ToolTip = "Select the cohort type for IPEDS reporting your school uses."
                    lblDisplayReportDate.Visible = True
                    If IPEDSControlType = ControlType.GradRates4 Then
                        RadComboBoxCohortYear.SelectedValue = (Year(DateTime.Now) - 7).ToString
                        lblDisplayReportDate.Text = "Report Date: 09/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "08/31/" & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
                    ElseIf IPEDSControlType = ControlType.GradRates200Less2Yrs Then
                        RadComboBoxCohortYear.SelectedValue = (Year(DateTime.Now) - 5).ToString
                        lblDisplayReportDate.Text = "Report Date: 09/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "08/31/" & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
                    Else
                        RadComboBoxCohortYear.SelectedValue = (Year(DateTime.Now) - 4).ToString
                        lblDisplayReportDate.Text = "Report Date: 09/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "08/31/" & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
                    End If
                Case ControlType.FinAidA, ControlType.FinAidBCDE
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = True
                    rblSchoolType.Visible = True
                    DateOptionsHeader.Visible = True
                    DatePickerRDateText.Visible = True
                    DatePickerPriorToText.Visible = False
                    RadDatePriorTo.Visible = False
                    EnableReportDate(False)
                    RadComboBoxCohortYear.Visible = True
                    RadComboBoxReportingYear.Visible = False
                    RadComboBoxCohortYearLabel.Visible = True
                    RadComboBoxReportingYearLabel.Visible = False
                    CreateYearComboBoxValues(RadComboBoxCohortYear, -1)
                    If Month(DateTime.Now) > 10 Then
                        RadComboBoxCohortYear.SelectedValue = (Year(DateTime.Now) - 1).ToString
                    Else
                        RadComboBoxCohortYear.SelectedValue = (Year(DateTime.Now) - 2).ToString
                    End If
                    lblDisplayReportDate.Visible = True
                    lblDisplayReportDate.Text = "Report Date: 07/01/" & RadComboBoxCohortYear.SelectedItem.Text & " thru " & "06/30/" & CStr(CInt(RadComboBoxCohortYear.SelectedItem.Text) + 1)
                    Dim reportname As String
                    reportname = Page.Title
                    If reportname.Contains("Part A") Or reportname.Contains("Part D") _
                    Or reportname.Contains("Part E") Then
                        LargestProgramText.Visible = True
                        RadComboLProgram.Visible = True
                    End If
                    If reportname.Contains("Part B") Or reportname.Contains("Part C") Then
                        LargestProgramText.Visible = False
                        RadComboLProgram.Visible = False
                    End If
                    If reportname.Contains("Part E") Or reportname.Contains("Part D") Then
                        Dim ReportingType As IPEDSReportingType = CType(rblSchoolType.SelectedIndex, IPEDSReportingType)
                        If ReportingType = IPEDSReportingType.ProgramReporter Then
                            ProgramSelector.Visible = False
                        Else
                            ProgramSelector.Visible = True
                        End If
                    End If


                Case ControlType.None

                Case Else
                    Throw New Exception("Unknown Control Type")
            End Select
            CreateReporterDateRange(CType(rblSchoolType.SelectedIndex, IPEDSReportingType), IPEDSControlType)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub HideShowInstitutionType()
        If rblSchoolType.SelectedIndex = 1 Then
            If IPEDSControlType = ControlType.GradRates2Years Or IPEDSControlType = ControlType.GradRates4 Or IPEDSControlType = ControlType.GradRates200Less2Yrs Then
                AcademicYearMsg.Visible = False
            End If
            If IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
                radComboBoxInstitutionType.Visible = True
                radComboBoxInstitutionTypeLabel.Visible = True
            End If
        Else
            radComboBoxInstitutionType.Visible = False
            radComboBoxInstitutionTypeLabel.Visible = False
            AcademicYearMsg.Visible = False
        End If
    End Sub

    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
#End Region







End Class
