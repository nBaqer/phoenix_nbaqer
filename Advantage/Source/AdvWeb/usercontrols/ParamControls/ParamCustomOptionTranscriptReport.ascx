﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamCustomOptionTranscriptReport.ascx.vb" Inherits="ParamCustomOptionTranscriptReport" %>
<style type="text/css">
    .PanelOptions {
        display: inline-block;
        width: 200px;
        margin-left: 5px;
        margin-right: 5px;
        vertical-align: top
    }

    .PanelOptions001 {
        display: inline-block;
        width: 200px;
        margin-left: 20px;
        margin-right: 5px;
        vertical-align: top
    }

    .PanelOptions002 {
        display: inline-block;
        width: 200px;
        margin-left: 25px;
        margin-right: 5px;
        vertical-align: top;
    }

    .LineStyle {
        width: 700px;
        text-align: left;
    }

    .RadInput_Material {
        border: none !important;
    }
</style>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelCustomGroup1" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">
        <div id="HeaderTitle" class="CaptionLabel" runat="server">
            Header
        </div>
        <div id="HeaderOptions" runat="server" style="padding-left: 10px;">
            <asp:Panel ID="pnlHeaderOptions" runat="server" Visible="True">
                <asp:CheckBoxList ID="cblHeaderOptions" runat="server" CssClass="checkboxstyle" Width="700px" Height="20px" AutoPostBack="True"
                    RepeatColumns="3" RepeatDirection="Horizontal"
                    OnSelectedIndexChanged="cblHeaderOptions_SelectedIndexChanged">
                    <asp:ListItem Text="No logo or School Information"></asp:ListItem>
                    <asp:ListItem Text="Official Transcript"></asp:ListItem>
                    <asp:ListItem Text="Show Report Date" Selected="True"></asp:ListItem>
                </asp:CheckBoxList>
            </asp:Panel>
            <hr class="LineStyle" />
            <asp:Panel ID="pnlSchoolInformationOptions" runat="server" Visible="True" Enabled="True">
                <asp:CheckBoxList ID="cblSchoolInformationOptions" runat="server" CssClass="checkboxstyle" Width="700px" Height="40px"
                    RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Show School Name" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Campus Address" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Phone" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Fax" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Web Site" Selected="True"></asp:ListItem>
                </asp:CheckBoxList>
            </asp:Panel>
        </div>
        <div id="SchoolLogoTitle" class="CaptionLabel" runat="server">
            School Logo
        </div>
        <div id="SchoolLogoPositionOptions" runat="server" style="padding-left: 10px;">
            <asp:Panel ID="pnlLogoPosition" runat="server" Visible="True" Height="40">
                <span>Select Logo position on top of Report </span>&nbsp;&nbsp;&nbsp;
                    <telerik:RadComboBox ID="radSchoolLogoPosition" runat="server" Width="200px" DropDownWidth="200px"
                        ToolTip="Select position for Official Logo in the report">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Left Justified" Value="Left" Selected="True" />
                            <telerik:RadComboBoxItem runat="server" Text="Centered(School Info not shown)" Value="Center" />
                            <telerik:RadComboBoxItem runat="server" Text="Right Justified" Value="Right" />
                            <telerik:RadComboBoxItem runat="server" Text="No Logo Shown" Value="NoLogo" />
                        </Items>
                    </telerik:RadComboBox>
            </asp:Panel>
        </div>

        <div id="StudentTitle" class="CaptionLabel" runat="server">
            Student
        </div>
        <div id="StudentOptions" runat="server" style="padding-left: 10px;">
            <asp:Panel ID="Panel3" runat="server" Visible="True">
                <asp:CheckBoxList ID="cblStudentOptions" runat="server" Width="700px" Height="40px"
                    RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Show Phone" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Date of Birth" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Email Address" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show SSN" Selected="True"></asp:ListItem>
                </asp:CheckBoxList>
            </asp:Panel>
        </div>
        <div id="ProgramTitle" class="CaptionLabel" runat="server">
            Program
        </div>
        <div id="ProgramOptions" runat="server" style="padding-left: 10px;">
            <asp:Panel ID="Panel4" runat="server" Visible="True">
                <asp:CheckBoxList ID="cblProgramOptions" runat="server" Width="700px" Height="40px"
                    RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Show Multiple Enrollments on Single Transcript"></asp:ListItem>
                    <asp:ListItem Text="Show Graduation Date" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Last Day of Attendance" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Complete Hours" Selected="True"></asp:ListItem>
                </asp:CheckBoxList>
            </asp:Panel>
        </div>
        <div id="CourseTitle" class="CaptionLabel" runat="server">
            Courses
        </div>
        <div id="CoursesOptions" runat="server" style="padding-left: 10px;">
            <div>
                <asp:RadioButtonList ID="rblShowTermCourses" runat="server" Width="700px" Height="15px" AutoPostBack="True"
                    RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table"
                    OnSelectedIndexChanged="rblShowTermCourses_SelectedIndexChanged">
                    <asp:ListItem Value="ShowTerms/Modules" Text="Show Terms/Modules" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="ShowCoursesOnly" Text="Show Courses only"></asp:ListItem>
                    <asp:ListItem Value="ShowCourseCategories" Text="Show Course Categories"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div>
                <asp:Panel ID="pnlTermsModulesOptions" runat="server" CssClass="PanelOptions001" Visible="True" Enabled="True">
                    <asp:CheckBoxList ID="cblTermsModulesOptions" runat="server" Height="15px"
                        RepeatColumns="1" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Show Term Description" Selected="False"></asp:ListItem>
                    </asp:CheckBoxList>
                </asp:Panel>
                <%--  
                <asp:Panel ID="pnlShowUnits" runat="server" CssClass="PanelOptions" Visible="True" Enabled="False">  
                    <asp:CheckBoxList ID="cblShowWorkUnits" runat="server" AutoPostBack="True"
                        RepeatColumns="1" RepeatDirection="Horizontal" Width="230px">
                        <asp:ListItem Value="No Show" Text="Do not show work units under each subject" Selected="False"></asp:ListItem>
                        <asp:ListItem Value="Show" Text="Show course components" Selected="False"></asp:ListItem>
                    </asp:CheckBoxList>   
                </asp:Panel>
                --%>
                <asp:Panel ID="pnlShowCouseComponents" runat="server" CssClass="PanelOptions002" Visible="True" Enabled="False">
                    <asp:CheckBoxList ID="cblShowCourseComponents" runat="server" Height="15px"
                        RepeatColumns="1" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Show course components" Selected="False"></asp:ListItem>
                    </asp:CheckBoxList>
                </asp:Panel>

                <asp:Panel ID="pnlShowCourseCategoriesOptions" runat="server" CssClass="PanelOptions" Visible="True" Enabled="False">
                    <asp:CheckBoxList ID="cblCourseCategoriesOptions" runat="server" Height="40px"
                        RepeatColumns="1" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Show Date Issued" Selected="False"></asp:ListItem>
                        <asp:ListItem Text="Show Credits Column" Selected="False"></asp:ListItem>
                        <asp:ListItem Text="Show Hours Column" Selected="False"></asp:ListItem>
                    </asp:CheckBoxList>
                </asp:Panel>

            </div>
        </div>
        <div id="FooterTitle" class="CaptionLabel" runat="server">
            Footer
        </div>
        <div id="FooterOptions" runat="server" style="padding-left: 10px;">
            <asp:Panel ID="Panel8" runat="server" Visible="True">
                <asp:CheckBoxList ID="cblFooterOptions" runat="server" Width="700px" Height="60px"
                    RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Show Courses Summary" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Grade Points in Summary" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Grade Scale" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Student Signature Line" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show School Official Signature Line" Selected="False"></asp:ListItem>
                    <asp:ListItem Text="Show Page Number" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Disclaimer" Selected="True"></asp:ListItem>
                </asp:CheckBoxList>
            </asp:Panel>
        </div>
        <div id="DisclaimerTitle" class="CaptionLabel" runat="server">
            Disclaimer
        </div>
        <div id="DisclaimerOption" runat="server" style="padding-left: 10px;">
            <asp:Panel ID="Panel9" runat="server" Visible="True">
                <asp:Label ID="lblDisclaimerText" runat="server" CssClass="labelDisclaimer" Font-Italic="True"></asp:Label>
                <br />
                <br />
                <span>To create a custom disclaimer add the text to the section below.</span>
                <br />
                <telerik:RadTextBox ID="radTextBoxDisclaimer" runat="server" CssClass="textbox no-border" BorderWidth="0"
                    Resize="None" TextMode="MultiLine" EmptyMessage="Type the new disclaimer text"
                    OnTextChanged="ChangeDisclaimer">
                </telerik:RadTextBox>
            </asp:Panel>
        </div>
    </div>
</asp:Panel>
