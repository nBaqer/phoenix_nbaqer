﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamTransactionActivity.ascx.vb" Inherits="ParamTransactionActivity" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">

        <div id="divAcademicYeardate" class="MultiFilterReportContainer" runat="server">
            <div id="SchoolReportingTypeHeader" class="CaptionLabel" runat="server">Transaction Activity Report Date Range</div>
            <div class="FilterInput">
                <table>
                    <tr>
                        <td class="style1">
                            <div>
                                <asp:Label ID="StartDatetext" class="DatePicker1Text" runat="server">Start Date </asp:Label>
                            </div>
                            <telerik:RadDatePicker ID="RadDateStartDate" runat="server" CssClass="FilterInput"
                                Width="151px" DateInput-ReadOnly="true">
                                <Calendar ID="StartDateCal" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ReadOnly="false" runat="server"></DateInput>
                            </telerik:RadDatePicker>
                        </td>


                        <td>
                            <div>
                                <asp:Label ID="EndDateText" class="DatePicker1Text" runat="server">End Date </asp:Label>
                            </div>

                            <telerik:RadDatePicker ID="RadDateEndDate" runat="server" CssClass="FilterInput"
                                Width="151px" DateInput-ReadOnly="true">
                                <Calendar ID="EndDateCal" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ReadOnly="false" runat="server"></DateInput>
                            </telerik:RadDatePicker>
                        </td>


                </table>
            </div>
        </div>
        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer">
            <div id="CampusHeader" class="CaptionLabel" runat="server">Campus</div>

            <telerik:RadComboBox ID="RadComboCampus" runat="server"
                OnItemDataBound="CampusComboBox_ItemDataBound" AutoPostBack="true" OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged" CssClass="ManualInput ReportLeftMarginInput"
                Width="350px" ToolTip="Select a campus to run the report">
            </telerik:RadComboBox>
        </div>

        <div id="TransTypeSelector" class="TransTypeSelector MultiFilterReportContainer" runat="server">
            <div id="TransTypeHeader" class="CaptionLabel" runat="server">Transaction Type</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxTransType" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxTransType2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxTransType2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div3" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveTransType" runat="server"
                        OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true"
                        Text="Show Inactive"
                        ToolTip="Check this box to make inactive transaction codes selectable" />
                    <span id="Span1" class="RadListBox1Counter">
                        <asp:Label ID="lblTransTypeCounterAvailable" runat="server"></asp:Label>
                    </span>
                    <span id="Span2" class="RadListBox2Counter">
                        <asp:Label ID="lblTransTypeCounterSelected" runat="server"></asp:Label>
                    </span>
                </div>
            </div>
            <div id="RequiredFieldContainer" class="RequiredFieldContainer" runat="server">
                <asp:CustomValidator ID="CustomValidator1" runat="server"
                    Enabled="False" OnServerValidate="ValidateRequired" Display="Dynamic"> 
                </asp:CustomValidator>
                <asp:Label ID="lblSingleSelectMsg" runat="server" Text="" Style="color: Red;"></asp:Label>
            </div>
        </div>
    </div>
</asp:Panel>

