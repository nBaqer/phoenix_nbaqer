﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamApplicantFee.ascx.vb" Inherits="ParamApplicantFee" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadDateStartDate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadDateStartDate" />
                <telerik:AjaxUpdatedControl ControlID="RequiredFieldValidatorStartDate" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">

        <div id="divAcademicYeardate" class="MultiFilterReportContainer" runat="server">
            <div id="SchoolReportingTypeHeader" class="CaptionLabel" runat="server">
                Report Date
           
            </div>
            <div class="ReportLeftMarginInput">
                <table>
                    <tr>
                        <td class="style1">
                            <div>
                                <asp:Label ID="StartDatetext" class="DatePicker1Text" runat="server">Start Date </asp:Label>
                            </div>
                            <telerik:RadDatePicker ID="RadDateStartDate" runat="server"
                                Width="151px" DateInput-ReadOnly="true">
                                <Calendar ID="StartDateCal" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                            <DateInput ID="DateInput1" DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ReadOnly="false" runat="server" FocusedStyle-PaddingTop="0px"></DateInput>
                        </telerik:RadDatePicker>

                        </td>


                        <td>
                            <div>
                                <asp:Label ID="EndDateText" class="DatePicker1Text" runat="server">End Date </asp:Label>
                            </div>

                            <telerik:RadDatePicker ID="RadDateEndDate" runat="server" Width="151px" DateInput-ReadOnly="true">
                                <Calendar ID="EndDateCal" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                            <DateInput ID="DateInput2" DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy"  ReadOnly="false" runat="server"></DateInput>
                        </telerik:RadDatePicker>
                        


                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorStartDate" runat="server" Display="Dynamic"
                                ErrorMessage="Please enter Start Date" ControlToValidate="RadDateStartDate" ForeColor="Red"
                                Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEndDate" runat="server" Display="Dynamic"
                                ErrorMessage=" Please enter End Date" ControlToValidate="RadDateEndDate" ForeColor="Red"
                                Enabled="true"></asp:RequiredFieldValidator>

                            <asp:CompareValidator ID="CompareDatesValidator" runat="Server" ControlToCompare="RadDateStartDate"
                                ControlToValidate="RadDateEndDate" Operator="GreaterThan" Display="Dynamic" ForeColor="Red"
                                ErrorMessage=" Date range is not valid" Enabled="true" />

                        </td>
                    </tr>

                </table>

            </div>
        </div>
        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer">
            <div id="CampusHeader" class="CaptionLabel" runat="server">Campus</div>

            <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true" CssClass="ReportLeftMarginInput"
                OnItemDataBound="CampusComboBox_ItemDataBound"
                Width="350px" ToolTip="Select a campus to run the report">
            </telerik:RadComboBox>
        </div>
        <div id="RequiredFieldContainer" class="RequiredFieldContainer" runat="server">
            <asp:CustomValidator ID="CustomValidator1" runat="server"
                Enabled="False" OnServerValidate="ValidateRequired" Display="Dynamic"> 
            </asp:CustomValidator>
            <asp:Label ID="lblSingleSelectMsg" runat="server" Text="" Style="color: #b71c1c"></asp:Label>
        </div>

        <div id="SchoolReportTypeSelector" class="SchoolReportTypeSelector MultiFilterReportContainer">
            <div id="Div1" class="CaptionLabel" runat="server">Transactions</div>
            <div class="ReportLeftMarginInput">
                <table>
                    <tr>
                        <td style="width: 50%">
                            <div id="rblRevGradDateSelector" runat="server">
                                <asp:RadioButtonList ID="rblRevGradDateType" runat="server"
                                    RepeatDirection="Horizontal" ToolTip="Select the type of transaction" AutoPostBack="False">
                                    <asp:ListItem Value="All" Selected="True">All</asp:ListItem>
                                    <asp:ListItem Value="Voided">Voided</asp:ListItem>
                                    <asp:ListItem Value="Reversed">Adjustment</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="ResetDiv"></div>
        </div>
    </div>
</asp:Panel>

