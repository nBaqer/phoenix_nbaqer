﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common

Partial Class ParamApplicantFee
    Inherits UserControl
    Implements ICustomControl

#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString
    Protected MyAdvAppSettings As AdvAppSettings


    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = Value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load


        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        SetProperties()
        If Not Page.IsPostBack Then
            FillCampusControl()
            FillDates()
        End If
        ListBoxCounts()
    End Sub


    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
        Dim LBox As RadListBox = DirectCast(sender, RadListBox)

    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim Lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arPrgVersion Then
                Dim MyarPrgVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                If MyarPrgVersion.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarPrgVersion.PrgVerDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is syCampGrp Then
                Dim MysyCampGrp As syCampGrp = DirectCast(myItem.DataItem, syCampGrp)
                If MysyCampGrp.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampGrp.CampGrpDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Protected Sub ValidateRequired(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        If RadComboCampus.SelectedIndex > 0 Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub
#End Region
#Region "Methods"
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampus)
        Try
            result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString))
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Sub FillDates()
        Try

            RadDateStartDate.SelectedDate = CDate(Date.Now.ToShortDateString.ToString)
            RadDateEndDate.SelectedDate = CDate(Date.Now.ToShortDateString.ToString)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    Protected Sub CampusComboBox_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        Dim myItem As RadComboBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is syCampus Then
                Dim MysyCampus As syCampus = DirectCast(myItem.DataItem, syCampus)
                If MysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim DisplayTree As New RadTreeView
        DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim HeaderNode1 As New RadTreeNode
            HeaderNode1.Text = "Report Date Options"
            HeaderNode1.Value = "Report Date Options"
            HeaderNode1.CssClass = "TreeParentNode"
            Dim StartDate As New RadTreeNode
            StartDate.Text = "Start Date: " + Format(RadDateStartDate.SelectedDate, "M/d/yyyy")
            Dim EndDate As New RadTreeNode
            EndDate.Text = "End Date: " + Format(RadDateEndDate.SelectedDate, "M/d/yyyy")
            HeaderNode1.Nodes.Add(StartDate)
            HeaderNode1.Nodes.Add(EndDate)
            DisplayTree.Nodes.Add(HeaderNode1)

            If RadComboCampus.Items.Count > 0 Then
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Campus"
                HeaderNode2.Value = "Campus"
                HeaderNode2.CssClass = "TreeParentNode"
                Dim CmpGrpNode As New RadTreeNode
                CmpGrpNode.Text = RadComboCampus.SelectedItem.Text
                HeaderNode2.Nodes.Add(CmpGrpNode)
                DisplayTree.Nodes.Add(HeaderNode2)
            End If


            Dim HeaderNode4 As New RadTreeNode
            HeaderNode4.Text = "Transactions"
            HeaderNode4.Value = "RevGradDateType"

            HeaderNode4.CssClass = "TreeParentNode"
            Dim RevGradDateNode As New RadTreeNode
            RevGradDateNode.Text = rblRevGradDateType.SelectedItem.Text
            HeaderNode4.Nodes.Add(RevGradDateNode)
            DisplayTree.Nodes.Add(HeaderNode4)



            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            UserSettings.ItemName = ItemDetail.ItemName
            UserSettings.ItemId = ItemDetail.ItemId
            UserSettings.DetailId = ItemDetail.DetailId
            UserSettings.FriendlyName = ItemDetail.CaptionOverride


            'StartDate
            Dim ctrlSettingStartDate As New ControlSettingInfo
            Dim ctrlValuesStartDate As New List(Of ControlValueInfo)
            Dim objControlValueInfo0 As New ControlValueInfo

            ctrlSettingStartDate.ControlName = RadDateStartDate.ID
            objControlValueInfo0.DisplayText = RadDateStartDate.SelectedDate.ToString
            objControlValueInfo0.KeyData = RadDateStartDate.SelectedDate.ToString

            ctrlValuesStartDate.Add(objControlValueInfo0)
            ctrlSettingStartDate.ControlValueCollection = ctrlValuesStartDate
            SettingsCollection.Add(ctrlSettingStartDate)

            'EndDate
            Dim ctrlSettingEndDate As New ControlSettingInfo
            Dim ctrlValuesEndDate As New List(Of ControlValueInfo)
            Dim objControlValueInfo10 As New ControlValueInfo

            ctrlSettingEndDate.ControlName = RadDateEndDate.ID
            objControlValueInfo10.DisplayText = RadDateEndDate.SelectedDate.ToString
            objControlValueInfo10.KeyData = RadDateEndDate.SelectedDate.ToString

            ctrlValuesEndDate.Add(objControlValueInfo10)
            ctrlSettingEndDate.ControlValueCollection = ctrlValuesEndDate
            SettingsCollection.Add(ctrlSettingEndDate)




            'Campus Group
            Dim ctrlSettingCmpGrp As New ControlSettingInfo
            Dim ctrlValuesCmpGrp As New List(Of ControlValueInfo)
            Dim objControlValueInfo1 As New ControlValueInfo
            ctrlSettingCmpGrp.ControlName = RadComboCampus.ID
            objControlValueInfo1.DisplayText = RadComboCampus.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo1.KeyData = RadComboCampus.SelectedValue.Replace("'", "''")
            ctrlValuesCmpGrp.Add(objControlValueInfo1)
            ctrlSettingCmpGrp.ControlValueCollection = ctrlValuesCmpGrp
            SettingsCollection.Add(ctrlSettingCmpGrp)
            'Campus Group2
            'Dim ctrlSettingCmpGrp2 As New ControlSettingInfo
            'Dim ctrlValuesCmpGrp2 As New List(Of ControlValueInfo)
            'ctrlSettingCmpGrp2.ControlName = RadComboCampus.ID
            'For Each item As RadListBoxItem In RadComboCampus.Items
            '    Dim objControlValueInfo2 As New ControlValueInfo
            '    objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
            '    objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
            '    ctrlValuesCmpGrp2.Add(objControlValueInfo2)
            'Next
            'ctrlSettingCmpGrp2.ControlValueCollection = ctrlValuesCmpGrp2
            'SettingsCollection.Add(ctrlSettingCmpGrp2)

            ''Transactions
            Dim ctrlSettingRevGradDateType As New ControlSettingInfo
            Dim ctrlValuesRevGradDateType As New List(Of ControlValueInfo)
            Dim objControlValueInfo5 As New ControlValueInfo

            ctrlSettingRevGradDateType.ControlName = rblRevGradDateType.ID
            objControlValueInfo5.DisplayText = rblRevGradDateType.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo5.KeyData = rblRevGradDateType.SelectedValue.Replace("'", "''")

            ctrlValuesRevGradDateType.Add(objControlValueInfo5)
            ctrlSettingRevGradDateType.ControlValueCollection = ctrlValuesRevGradDateType
            SettingsCollection.Add(ctrlSettingRevGradDateType)

            UserSettings.ControlSettingsCollection = SettingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return UserSettings
    End Function
    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim LstBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            'LstBox.Items.Clear()
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                LstBox.SelectedValue = ItemValue.KeyData
                                'Dim newItem As New RadListBoxItem
                                'newItem.Text = ItemValue.DisplayText
                                'newItem.Value = ItemValue.KeyData
                                'LstBox.SelectedValue = ItemValue.KeyData
                                'LstBox.Items.Add(newItem)
                            Next
                            'ElseIf TypeOf ctrl Is RadNumericTextBox Then
                            '    Dim RadNumTextBox As RadNumericTextBox = DirectCast(ctrl, RadNumericTextBox)
                            '    For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                            '        RadNumTextBox.Text = ItemValue.KeyData
                            '    Next
                        ElseIf TypeOf ctrl Is RadDatePicker Then
                            Dim RadDtPicker As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadDtPicker.SelectedDate = CDate(ItemValue.KeyData)
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadComboCampus.Items.Clear()
            End If
            ListBoxCounts()
            ' HideShowInstitutionType()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub ListBoxCounts()
        Dim boxcount1 As Integer = RadComboCampus.Items.Count()


        'lblCmpGrpCounterAvailable.Text = boxcount1.ToString.Trim & " available"
        'lblCmpGrpCounterAvailable.ToolTip = boxcount1.ToString.Trim & " available " & Caption & "(s)"
        'lblCmpGrpCounterSelected.Text = boxcount2.ToString.Trim & " selected"
        'lblCmpGrpCounterSelected.ToolTip = boxcount2.ToString.Trim & " selected " & Caption & "(s)"

        'lblPrgVerCounterAvailable.Text = boxcount3.ToString.Trim & " available"
        'lblPrgVerCounterAvailable.ToolTip = boxcount3.ToString.Trim & " available " & Caption & "(s)"
        'lblPrgVerCounterSelected.Text = boxcount4.ToString.Trim & " selected"
        'lblPrgVerCounterSelected.ToolTip = boxcount4.ToString.Trim & " selected " & Caption & "(s)"

    End Sub

    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
#End Region


End Class
