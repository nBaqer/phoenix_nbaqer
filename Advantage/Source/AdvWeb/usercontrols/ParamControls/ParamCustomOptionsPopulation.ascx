﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamCustomOptionsPopulation.ascx.vb" Inherits="ParamCustomGroupControl1" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelCustomGroup1" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">
        <div class="ResetDiv">
        </div>
        <div id="Div1" class="CaptionLabel" runat="server">Population Analysis Options</div>
        <div id="OtherCustomOptions" class="OtherCustomOptions">
            <asp:CheckBoxList ID="cblCustomOptions" runat="server" Height="77px" Width="700px"
                RepeatColumns="3" RepeatDirection="Horizontal">
                <asp:ListItem>Exclude LOA's from Beginning Population </asp:ListItem>
            </asp:CheckBoxList>
        </div>


    </div>

    <div class="ResetDiv">
    </div>
</asp:Panel>
