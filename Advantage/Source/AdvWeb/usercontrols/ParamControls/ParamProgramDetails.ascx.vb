﻿Option Strict On

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports NHibernate.Hql.Ast.ANTLR
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common
Partial Class ParamProgramDetails
    Inherits UserControl
    Implements ICustomControl

    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString

    Private _StudentIdentifier As String
    Private _ProgramReporterStart As String
    Private _ProgramReporterEnd As String
    Private _AcademicYearStart As String
    Private _AcademicYearEnd As String
    Private _StartDatePartProgram As String
    Private _StartDatePartAcademic As String
    Private _EndDatePartProgram As String
    Private _EndDatePartAcademic As String
    Private _SelectedDatePartProgram As String
    Private _SelectedDatePartAcademic As String

    Protected MyAdvAppSettings As AdvAppSettings

    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        SetProperties()
        'IPEDSControlType = ControlType.GradRates
        If Not Page.IsPostBack Then
            FillCampusControl()
            RadComboCampus.SelectedValue = HttpContext.Current.Request.Params("cmpid").ToString
            FillProgramControls()
            FillProgramversionControls()
            FillCourseControls()
        End If
    End Sub
#Region "Methods"
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillProgramControls()
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxProgram.DataTextField = "ProgDescrip"
            RadListBoxProgram.DataValueField = "ProgId"
            RadListBoxProgram.DataSource = GetProgramsByCampusId(selectedCampus, True)
            RadListBoxProgram.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillProgramversionControls(Optional ByVal progId As Nullable(Of Guid) = Nothing)
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxProgramVersion.DataTextField = "PrgVerDescrip"
            RadListBoxProgramVersion.DataValueField = "PrgVerId"
            RadListBoxProgramVersion.DataSource = GetProgramVersionByCampusId(selectedCampus, True, progId)
            RadListBoxProgramVersion.DataBind()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillCourseControls(Optional ByVal progId As Nullable(Of Guid) = Nothing)
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxCourse.DataTextField = "Descrip"
            RadListBoxCourse.DataValueField = "reqId"
            RadListBoxCourse.DataSource = GetCoursesByCampusId(selectedCampus, True, progId)
            RadListBoxCourse.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampus)
        Try
            result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString))
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetProgramsByCampusId(ByVal SelectedCampus As Guid, ByVal FilteredBySelected As Boolean) As List(Of arProgram)
        Dim DA As New ProgramDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arProgram)
        Try
            Dim SelectedPrograms As List(Of String) = GetCurrentlySelectedPrograms()
            If SelectedPrograms Is Nothing Or FilteredBySelected = False Then
                result = DA.GetProgramsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrograms"))
            Else
                result = DA.GetProgramsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrograms"), SelectedPrograms)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetProgramVersionByCampusId(ByVal SelectedCampus As Guid, ByVal FilteredBySelected As Boolean, _
                                                 Optional ByVal progId As Nullable(Of Guid) = Nothing) As List(Of arPrgVersion)
        Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arPrgVersion)
        Try
            Dim SelectedProgramVersions As List(Of String) = GetCurrentlySelectedProgramVersions()
            If SelectedProgramVersions Is Nothing Or FilteredBySelected = False Then
                If progId Is Nothing Then
                    result = DA.GetProgramVersionsByCampusId(SelectedCampus, GetStatusFilters("cbkInactiveProgramVersions"))
                Else
                    result = DA.GetProgramVersionsByCampusIdandProgId(SelectedCampus, GetStatusFilters("cbkInactiveProgramVersions"), CType(progId, Guid))
                End If

            Else
                If progId = Guid.Empty Then
                    result = DA.GetProgramVersionsByCampusId(SelectedCampus, GetStatusFilters("cbkInactiveProgramVersions"), SelectedProgramVersions)
                Else
                    result = DA.GetProgramVersionsByCampusIdandProgramIdCollection(SelectedCampus, GetStatusFilters("cbkInactiveProgramVersions"), _
                                                                                   SelectedProgramVersions, CType(progId, Guid))
                End If

            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCoursesByCampusId(ByVal SelectedCampus As Guid, ByVal FilteredBySelected As Boolean, _
                                                 Optional ByVal progId As Nullable(Of Guid) = Nothing) As DataTable
        Dim DA As New CoursesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As DataTable
        Dim statusId As String
        Dim CourseFacade As New CoursesFacade
        Try
            Dim SelectedCourses As List(Of String) = GetCurrentlySelectedCourses()
            Dim CKbox As CheckBox = DirectCast(Me.FindControl("cbkInactiveCourses"), CheckBox)
            If CKbox.Checked = True Then
                statusId = "1AF592A6-8790-48EC-9916-5412C25EF49F"
            Else
                statusId = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"
            End If
            If SelectedCourses Is Nothing Or FilteredBySelected = False Then

                If progId Is Nothing Then
                    result = CourseFacade.GetCoursesByCampus(SelectedCampus.ToString)
                Else
                    result = CourseFacade.GetCoursesByProgramVersion(SelectedCampus.ToString, CType(progId, Guid).ToString, statusId)
                End If
            Else
                If progId Is Nothing Then
                    result = CourseFacade.GetCoursesByCampus(SelectedCampus.ToString)
                Else
                    result = CourseFacade.GetCoursesByProgramVersionAndIgnoreSelectedCourses(SelectedCampus.ToString, SelectedCourses.ToString(), CType(progId, Guid).ToString)
                End If
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim StatusValues As New List(Of String)

        Try
            Dim CKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If CKbox.Checked = True Then
                StatusValues.Add("Active")
                StatusValues.Add("Inactive")
            Else
                StatusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return StatusValues
    End Function
    Private Function GetCurrentlySelectedPrograms() As List(Of String)
        Dim SelectedPrograms As New List(Of String)
        Try
            If RadListBoxProgram2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim selected As String = item.Value
                    SelectedPrograms.Add(selected)
                Next
                Return SelectedPrograms
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCurrentlySelectedCourses() As List(Of String)
        Dim SelectedCourses As New List(Of String)
        Try
            If RadListBoxCourse2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxCourse2.Items
                    Dim selected As String = item.Value
                    SelectedCourses.Add(selected)
                Next
                Return SelectedCourses
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCurrentlySelectedProgramVersions() As List(Of String)
        Dim SelectedProgramVersions As New List(Of String)
        Try
            If RadListBoxProgramVersion2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgramVersion2.Items
                    Dim selected As String = item.Value
                    SelectedProgramVersions.Add(selected)
                Next
                Return SelectedProgramVersions
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Protected Sub RadListBoxes_OnTransferring(sender As Object, e As RadListBoxTransferringEventArgs)
        Dim LBox As RadListBox = DirectCast(sender, RadListBox)
        If LBox.ID.Contains("Program") Then
            RadListBoxProgramVersion.Items.Clear()
            RadListBoxProgramVersion2.Items.Clear()
            RadListBoxCourse.Items.Clear()
            RadListBoxCourse2.Items.Clear()
            If String.IsNullOrEmpty(RadListBoxProgram.SelectedValue) Then
                'In Firefox when Show InActive checkbox is checked, user gets an undefined alert
                'This condition takes care of that error
                FillProgramversionControls()
            Else
                FillProgramversionControls(New Guid(RadListBoxProgram.SelectedValue))
            End If
            FillCourseControls()
        End If
        ListBoxCounts()
        'If RadListBoxProgram2.Items.Count = 0 Then
        '    FillProgramversionControls()
        'End If
    End Sub
    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
    End Sub
    Protected Sub CampusComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        RadListBoxProgram.Items.Clear()
        RadListBoxProgram2.Items.Clear()
        RadListBoxProgramVersion.Items.Clear()
        RadListBoxProgramVersion2.Items.Clear()
        RadListBoxCourse.Items.Clear()
        RadListBoxCourse2.Items.Clear()
        FillProgramControls()
        FillProgramversionControls()
        FillCourseControls()
        ListBoxCounts()
    End Sub
    Protected Sub CampusComboBox_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        Dim myItem As RadComboBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is syCampus Then
                Dim MysyCampus As syCampus = DirectCast(myItem.DataItem, syCampus)
                If MysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ChkBox As CheckBox = DirectCast(sender, CheckBox)
        If ChkBox.ID.Contains("Program") Then
            FillProgramControls()
        ElseIf ChkBox.ID.Contains("ProgramVersion") Then
            FillProgramversionControls()
        ElseIf ChkBox.ID.Contains("Course") Then
            RadListBoxCourse.Items.Clear()
            RadListBoxCourse2.Items.Clear()
            If String.IsNullOrEmpty(RadListBoxProgramVersion.SelectedValue) Then
                FillCourseControls()
            Else
                FillCourseControls(New Guid(RadListBoxProgramVersion.SelectedValue))
            End If

        End If
        ListBoxCounts()
    End Sub
    Private Sub ListBoxCounts()
        Dim boxcount1 As Integer = RadListBoxProgram.Items.Count()
        Dim boxcount2 As Integer = RadListBoxProgram2.Items.Count()
        Dim boxcount3 As Integer = RadListBoxProgramVersion.Items.Count()
        Dim boxcount4 As Integer = RadListBoxProgramVersion2.Items.Count()
        Dim boxcount5 As Integer = RadListBoxCourse.Items.Count()
        Dim boxcount6 As Integer = RadListBoxCourse2.Items.Count()


        'boxcount6 = boxcount6 + 1

        lblProgramCounterAvailable.Text = boxcount1.ToString.Trim & " available"
        lblProgramCounterAvailable.ToolTip = boxcount1.ToString.Trim & " available " & Caption & "(s)"
        lblProgramCounterSelected.Text = boxcount2.ToString.Trim & " selected"
        lblProgramCounterSelected.ToolTip = boxcount2.ToString.Trim & " selected " & Caption & "(s)"

        lblProgramVersionCounterAvailable.Text = boxcount3.ToString.Trim & " available"
        lblProgramVersionCounterAvailable.ToolTip = boxcount3.ToString.Trim & " available " & Caption & "(s)"
        lblProgramVersionCounterSelected.Text = boxcount4.ToString.Trim & " selected"
        lblProgramVersionCounterSelected.ToolTip = boxcount4.ToString.Trim & " selected " & Caption & "(s)"

        lblCourseCounterAvailable.Text = boxcount5.ToString.Trim & " available"
        lblCourseCounterAvailable.ToolTip = boxcount5.ToString.Trim & " available " & Caption & "(s)"
        lblCourseCounterSelected.Text = boxcount6.ToString.Trim & " selected"
        lblCourseCounterSelected.ToolTip = boxcount6.ToString.Trim & " selected " & Caption & "(s)"
        
    End Sub
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim DisplayTree As New RadTreeView
        DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim HeaderNode1 As New RadTreeNode
            HeaderNode1.Text = "Campus"
            HeaderNode1.Value = "Campus"
            HeaderNode1.CssClass = "TreeParentNode"
            Dim CampusNode As New RadTreeNode
            CampusNode.Text = RadComboCampus.SelectedItem.Text
            HeaderNode1.Nodes.Add(CampusNode)
            DisplayTree.Nodes.Add(HeaderNode1)

            'Program
            If RadListBoxProgram2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Program"
                HeaderNode2.Value = "Program"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim ProgramNode As New RadTreeNode
                    ProgramNode.Text = item.Text
                    HeaderNode2.Nodes.Add(ProgramNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Program Version
            If RadListBoxProgramVersion2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "ProgramVersion"
                HeaderNode2.Value = "ProgramVersion"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgramVersion2.Items
                    Dim ProgramVersionNode As New RadTreeNode
                    ProgramVersionNode.Text = item.Text
                    HeaderNode2.Nodes.Add(ProgramVersionNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Course
            If RadListBoxCourse2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Courses"
                HeaderNode2.Value = "Courses"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxCourse2.Items
                    Dim CourseNode As New RadTreeNode
                    CourseNode.Text = item.Text
                    HeaderNode2.Nodes.Add(CourseNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If
            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            UserSettings.ItemName = ItemDetail.ItemName
            UserSettings.ItemId = ItemDetail.ItemId
            UserSettings.DetailId = ItemDetail.DetailId
            UserSettings.FriendlyName = ItemDetail.CaptionOverride

            'Campus
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo

            ctrlSettingCampus.ControlName = RadComboCampus.ID
            Dim selectedCampus As RadComboBoxItem = RadComboCampus.SelectedItem
            objControlValueInfo.DisplayText = selectedCampus.Text.Replace("'", "''")
            objControlValueInfo.KeyData = selectedCampus.Value.Replace("'", "''")
            ctrlValuesCampus.Add(objControlValueInfo)
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            SettingsCollection.Add(ctrlSettingCampus)

            'Program1
            Dim ctrlSettingProgram As New ControlSettingInfo
            Dim ctrlValuesProgram As New List(Of ControlValueInfo)
            ctrlSettingProgram.ControlName = RadListBoxProgram.ID
            For Each item As RadListBoxItem In RadListBoxProgram.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram.Add(objControlValueInfo2)
            Next
            ctrlSettingProgram.ControlValueCollection = ctrlValuesProgram
            SettingsCollection.Add(ctrlSettingProgram)

            'Program2
            Dim ctrlSettingProgram2 As New ControlSettingInfo
            Dim ctrlValuesProgram2 As New List(Of ControlValueInfo)


            ctrlSettingProgram2.ControlName = RadListBoxProgram2.ID
            For Each item As RadListBoxItem In RadListBoxProgram2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram2.Add(objControlValueInfo3)
            Next
            ctrlSettingProgram2.ControlValueCollection = ctrlValuesProgram2
            SettingsCollection.Add(ctrlSettingProgram2)


            'ProgramVersion1
            Dim ctrlSettingProgramVersion As New ControlSettingInfo
            Dim ctrlValuesProgramVersion As New List(Of ControlValueInfo)
            ctrlSettingProgramVersion.ControlName = RadListBoxProgramVersion.ID
            For Each item As RadListBoxItem In RadListBoxProgramVersion.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgramVersion.Add(objControlValueInfo2)
            Next
            ctrlSettingProgramVersion.ControlValueCollection = ctrlValuesProgramVersion
            SettingsCollection.Add(ctrlSettingProgramVersion)

            'ProgramVersion2
            Dim ctrlSettingProgramVersion2 As New ControlSettingInfo
            Dim ctrlValuesProgramVersion2 As New List(Of ControlValueInfo)


            ctrlSettingProgramVersion2.ControlName = RadListBoxProgramVersion2.ID
            For Each item As RadListBoxItem In RadListBoxProgramVersion2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgramVersion2.Add(objControlValueInfo3)
            Next
            ctrlSettingProgramVersion2.ControlValueCollection = ctrlValuesProgramVersion2
            SettingsCollection.Add(ctrlSettingProgramVersion2)

            'Course1
            Dim ctrlSettingCourse As New ControlSettingInfo
            Dim ctrlValuesCourse As New List(Of ControlValueInfo)
            ctrlSettingCourse.ControlName = RadListBoxCourse.ID
            For Each item As RadListBoxItem In RadListBoxCourse.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesCourse.Add(objControlValueInfo2)
            Next
            ctrlSettingCourse.ControlValueCollection = ctrlValuesCourse
            SettingsCollection.Add(ctrlSettingCourse)

            'Course2
            Dim ctrlSettingCourse2 As New ControlSettingInfo
            Dim ctrlValuesCourse2 As New List(Of ControlValueInfo)


            ctrlSettingCourse2.ControlName = RadListBoxCourse2.ID
            For Each item As RadListBoxItem In RadListBoxCourse2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesCourse2.Add(objControlValueInfo3)
            Next
            ctrlSettingCourse2.ControlValueCollection = ctrlValuesCourse2
            SettingsCollection.Add(ctrlSettingCourse2)
            UserSettings.ControlSettingsCollection = SettingsCollection


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return UserSettings
    End Function
    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim ComboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If Not ItemValue.KeyData = "1900" Then
                                    ComboBox.SelectedValue = ItemValue.KeyData
                                End If

                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim LstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            LstBox.Items.Clear()
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = ItemValue.DisplayText
                                newItem.Value = ItemValue.KeyData
                                LstBox.SelectedValue = ItemValue.KeyData
                                LstBox.Items.Add(newItem)
                            Next
                        ElseIf TypeOf ctrl Is RadDatePicker Then
                            Dim DPicker As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If Not ItemValue.KeyData = "01/01/1900" Then
                                    'Get the current min and max dates
                                    Dim MinDate As Date = DPicker.MinDate
                                    Dim MaxDate As Date = DPicker.MaxDate
                                    'loosen the validation to allow the population of the datepicker from the saved settings
                                    'since it has already been validated on entry
                                    DPicker.MinDate = DPicker.MinDate.AddYears(-20)
                                    DPicker.MaxDate = DPicker.MaxDate.AddYears(20)
                                    'populate the date picker
                                    DPicker.SelectedDate = CDate(ItemValue.KeyData)
                                    'restore the min and max dates to the proper values
                                    DPicker.MinDate = MinDate
                                    DPicker.MaxDate = MaxDate
                                End If
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadListBoxProgram.Items.Clear()
                RadListBoxProgram2.Items.Clear()
                RadListBoxProgramVersion.Items.Clear()
                RadListBoxProgramVersion2.Items.Clear()
                RadListBoxCourse.Items.Clear()
                RadListBoxCourse2.Items.Clear()
            End If
            ListBoxCounts()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
#End Region
#Region "Set Properties"
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub

#End Region
#Region "Events"
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim Lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arProgram Then
                Dim MyarProgram As arProgram = DirectCast(myItem.DataItem, arProgram)
                If MyarProgram.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarProgram.ProgDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is arPrgVersion Then
                Dim myarPrgVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                Dim myarProgramVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                If myarProgramVersion.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarPrgVersion.PrgVerDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
#End Region
    Protected Sub RadListBoxProgram_Transferred(sender As Object, e As RadListBoxTransferredEventArgs) Handles RadListBoxProgram.Transferred
        If RadListBoxProgram2.Items.Count = 0 Then
            RadListBoxProgram.SelectedIndex = -1
            FillProgramversionControls()
        End If
        ListBoxCounts()
    End Sub
    Protected Sub RadListBoxProgram_Transferring(sender As Object, e As RadListBoxTransferringEventArgs) Handles RadListBoxProgram.Transferring
    End Sub
    Protected Sub RadListBoxProgramVersion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadListBoxProgramVersion.SelectedIndexChanged
        RadListBoxCourse.Items.Clear()
        RadListBoxCourse2.Items.Clear()
        FillCourseControls(New Guid(RadListBoxProgramVersion.SelectedValue))
        ListBoxCounts()
    End Sub
    Protected Sub RadListBoxCourse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadListBoxCourse.SelectedIndexChanged
    End Sub

    Protected Sub RadListBoxCourse_Transferred(sender As Object, e As RadListBoxTransferredEventArgs) Handles RadListBoxCourse.Transferred
        ListBoxCounts()
    End Sub

    Protected Sub RadListBoxProgramVersion_Transferred(sender As Object, e As RadListBoxTransferredEventArgs) Handles RadListBoxProgramVersion.Transferred
        If RadListBoxProgramVersion2.Items.Count = 0 Then
            RadListBoxProgramVersion.SelectedIndex = -1
            RadListBoxCourse.Items.Clear()
            RadListBoxCourse2.Items.Clear()
            FillCourseControls()
        End If
        ListBoxCounts()
    End Sub
End Class
