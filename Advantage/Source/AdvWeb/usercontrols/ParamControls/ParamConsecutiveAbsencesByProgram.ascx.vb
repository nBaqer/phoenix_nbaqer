﻿Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common
Partial Class usercontrols_ParamControls_ParamConsecutiveAbsencesByProgram
    Inherits System.Web.UI.UserControl
    Implements ICustomControl
    Protected MyAdvAppSettings As AdvAppSettings
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString
    Private _IPEDSControlType As ControlType
    Private _ItemDetail As ParameterDetailItemInfo
    Public Enum ControlType
        [ConsectiveAbsence] = 0
    End Enum
    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
    Public Property IPEDSControlType() As ControlType
        Get
            Return _IPEDSControlType
        End Get
        Set(ByVal Value As ControlType)
            _IPEDSControlType = Value
        End Set
    End Property
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride

            'Campus
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo
            ctrlSettingCampus.ControlName = RadListBoxCampus2.ID
            For Each item As RadListBoxItem In RadListBoxCampus2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesCampus.Add(objControlValueInfo3)
            Next
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            settingsCollection.Add(ctrlSettingCampus)
            
            'Target Term
            Dim ctrlSettingProgramVersion2 As New ControlSettingInfo
            Dim ctrlValuesProgramVersion2 As New List(Of ControlValueInfo)
            ctrlSettingProgramVersion2.ControlName = RadListBoxProgramVersions2.ID
            For Each item As RadListBoxItem In RadListBoxProgramVersions2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgramVersion2.Add(objControlValueInfo3)
            Next
            ctrlSettingProgramVersion2.ControlValueCollection = ctrlValuesProgramVersion2
            settingsCollection.Add(ctrlSettingProgramVersion2)



            'Number of Days Absent
            Dim ctrlSettingDays As New ControlSettingInfo
            Dim objControlValueInfoDays As New ControlValueInfo
            Dim ctrlValuesDays As New List(Of ControlValueInfo)
            ctrlSettingDays.ControlName = txtNumberofDays.ID
            If String.IsNullOrEmpty(txtNumberofDays.Text) Then
                objControlValueInfoDays.DisplayText = ""
                objControlValueInfoDays.KeyData = ""
            Else
                objControlValueInfoDays.DisplayText = txtNumberofDays.Text
                objControlValueInfoDays.KeyData = txtNumberofDays.Text
                ctrlValuesDays.Add(objControlValueInfoDays)
            End If
            ctrlSettingDays.ControlValueCollection = ctrlValuesDays
            settingsCollection.Add(ctrlSettingDays)

            'CutOff Date
            Dim ctrlSettingCutoffDate As New ControlSettingInfo
            Dim objControlValueInfoCutOffDate As New ControlValueInfo
            Dim ctrlValuesDate As New List(Of ControlValueInfo)
            ctrlSettingCutoffDate.ControlName = RadDateReportDate.ID
            If RadDateReportDate.SelectedDate Is Nothing Then
                objControlValueInfoCutOffDate.DisplayText = ""
                objControlValueInfoCutOffDate.KeyData = ""
            Else
                objControlValueInfoCutOffDate.DisplayText = RadDateReportDate.SelectedDate.ToString
                objControlValueInfoCutOffDate.KeyData = RadDateReportDate.SelectedDate.ToString
                ctrlValuesDate.Add(objControlValueInfoCutOffDate)
            End If
            ctrlSettingCutoffDate.ControlValueCollection = ctrlValuesDate
            settingsCollection.Add(ctrlSettingCutoffDate)
            userSettings.ControlSettingsCollection = settingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        Return userSettings
    End Function

    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim displayTree As New RadTreeView
        displayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            'campus groups 
            If RadListBoxCmpGrp2.Items.Count > 0 Then
                Dim headerCampusGroupNode As New RadTreeNode
                headerCampusGroupNode.Text = "Campus Group"
                headerCampusGroupNode.Value = "CampusGroup"
                headerCampusGroupNode.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxCmpGrp2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerCampusGroupNode.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerCampusGroupNode)
            End If

            'campus
            If RadListBoxCampus2.Items.Count > 0 Then
                Dim headerCampusNode As New RadTreeNode
                headerCampusNode.Text = "Campus"
                headerCampusNode.Value = "Campus"
                headerCampusNode.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxCampus2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerCampusNode.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerCampusNode)
            End If
            
            'Program Version
            If RadListBoxProgramVersions2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Program Version"
                headerNode2.Value = "ProgramVersion"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgramVersions2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If


            Dim reportnameDays As String = Page.Title
            Dim headerNode21 As New RadTreeNode
            headerNode21.Text = "As of Date"
            headerNode21.Value = "Cutoffdate"
            headerNode21.CssClass = "TreeParentNode"
            Dim programNode21 As New RadTreeNode
            programNode21.Text = RadDateReportDate.SelectedDate.ToString
            headerNode21.Nodes.Add(programNode21)
            displayTree.Nodes.Add(headerNode21)

            Dim reportnameDaysMissed As String = Page.Title
            Dim headerNode22 As New RadTreeNode
            headerNode22.Text = "DaysMissed"
            headerNode22.Value = "DaysMissed"
            headerNode22.CssClass = "TreeParentNode"
            Dim programNode22 As New RadTreeNode
            programNode22.Text = txtNumberofDays.Text
            headerNode22.Nodes.Add(programNode22)
            displayTree.Nodes.Add(headerNode22)


            Return displayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Public Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim radioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                radioButList.SelectedValue = itemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim comboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                If Not itemValue.KeyData = "1900" Then
                                    comboBox.SelectedValue = itemValue.KeyData
                                End If
                            Next
                        ElseIf TypeOf ctrl Is TextBox Then
                            Dim txtDaysMissed As TextBox = DirectCast(ctrl, TextBox)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                txtNumberofDays.Text = itemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadDatePicker Then
                            Dim txtDaysMissed As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                RadDateReportDate.SelectedDate = CType(itemValue.KeyData, Date?)
                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim lstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            lstBox.Items.Clear()
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = itemValue.DisplayText
                                newItem.Value = itemValue.KeyData
                                lstBox.SelectedValue = itemValue.KeyData
                                lstBox.Items.Add(newItem)
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadListBoxProgramVersions.Items.Clear()
                RadListBoxProgramVersions2.Items.Clear()

            End If
            ListBoxCounts()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub ListBoxCounts()

        Dim boxProgramVersions1 As Integer = RadListBoxProgramVersions.Items.Count()
        Dim boxProgramVersions2 As Integer = RadListBoxProgramVersions2.Items.Count()
        lblProgramVersionsCounterAvailable.Text = boxProgramVersions1.ToString.Trim & " available"
        lblProgramVersionsCounterAvailable.ToolTip = boxProgramVersions1.ToString.Trim & " available " & Caption & "(s)"
        lblProgramVersionsCounterSelected.Text = boxProgramVersions2.ToString.Trim & " selected"
        lblProgramVersionsCounterSelected.ToolTip = boxProgramVersions2.ToString.Trim & " selected " & Caption & "(s)"

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        SetProperties()
        'IPEDSControlType = ControlType.GradRates
        If Not Page.IsPostBack Then
            FillCmpGrpControl()
            FillCampusControl()
            FillProgramVersions()
            RadDateReportDate.SelectedDate = DateTime.Now.Date
        End If
    End Sub
    Public Sub FillCmpGrpControl()
        Try
            RadListBoxCmpGrp.DataTextField = "Campgrpdescrip"
            RadListBoxCmpGrp.DataValueField = "CampgrpId"
            RadListBoxCmpGrp.DataSource = GetCampGrpByUserId()
            RadListBoxCmpGrp.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillCampusControl()
        Try
            RadListBoxCampus.DataTextField = "CampDescrip"
            RadListBoxCampus.DataValueField = "CampusId"
            RadListBoxCampus.DataSource = GetCampusesByUserId()
            RadListBoxCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Public Sub FillProgramVersions()
        Try

            RadListBoxProgramVersions.DataTextField = "PrgVerDescrip"
            RadListBoxProgramVersions.DataValueField = "PrgVerId"
            Dim selectedCampuses = GetCurrentlySelectedCampuses()

            If selectedCampuses IsNot Nothing Then
                RadListBoxProgramVersions.DataSource = GetProgramVersionsByCampusId(selectedCampuses, True)
            End If
            RadListBoxProgramVersions.DataBind()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Function GetCampGrpByUserId() As List(Of syCampGrp)
        Dim DA As New CampusGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampGrp)
        Dim SelectedCampusGroups As List(Of String) = GetCurrentlySelectedCampusGroups()
        Try
            If SelectedCampusGroups Is Nothing Then
                result = DA.GetCampusGrpsByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), GetStatusFilters("cbkInactiveCmpGrp"))
            Else
                result = DA.GetCampusGrpsByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), GetStatusFilters("cbkInactiveCmpGrp"), SelectedCampusGroups)
            End If

            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampus)
        Dim SelectedCampuses As List(Of String) = GetCurrentlySelectedCampuses()

        Try
            If SelectedCampuses Is Nothing Then
                result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), GetStatusFilters("checkboxInactiveCampuses"))
            Else
                result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), GetStatusFilters("checkboxInactiveCampuses"), SelectedCampuses )
            End If
            
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetProgramVersionsByCampusId(ByVal selectedCampus As List(Of String), ByVal filteredBySelected As Boolean) As List(Of arPrgVersion)
        Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arPrgVersion)
        Try
            Dim selectedProgramVersions As List(Of String) = GetCurrentlySelectedProgramVersions()
            If selectedProgramVersions Is Nothing Or filteredBySelected = False Then
                result = DA.GetProgramVersionsByCampusId(selectedCampus, GetStatusFilters("chkInActiveProgramVersions"))
            Else
                result = DA.GetProgramVersionsByCampusId(selectedCampus, GetStatusFilters("chkInActiveProgramVersions"), selectedProgramVersions)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetCurrentlySelectedCampuses() As List(Of String)
        Dim selectedCampuses As New List(Of String)
        Try
            If RadListBoxCampus2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxCampus2.Items
                    Dim selected As String = item.Value
                    selectedCampuses.Add(selected)
                Next
            End If
            Return selectedCampuses

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Private Function GetCurrentlySelectedCampusGroups() As List(Of String)
        Dim selectedCampusGroups As New List(Of String)
        Try
            If RadListBoxCmpGrp2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxCmpGrp2.Items
                    Dim selected As String = item.Value
                    selectedCampusGroups.Add(selected)
                Next
            End If
            Return selectedCampusGroups

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCurrentlySelectedProgramVersions() As List(Of String)
        Dim selectedProgramVersions As New List(Of String)
        Try
            If RadListBoxProgramVersions2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgramVersions2.Items
                    Dim selected As String = item.Value
                    selectedProgramVersions.Add(selected)
                Next
                Return selectedProgramVersions
            Else
                Return selectedProgramVersions
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim statusValues As New List(Of String)
        Try
            Dim cKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If cKbox.Checked = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        Return statusValues
    End Function
    Private Sub SetProperties()
        Try
            RadDateReportDate.MaxDate = DateTime.Now
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Protected Sub CampusComboBox_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        Dim myItem As RadComboBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is syCampus Then
                Dim mysyCampus As syCampus = DirectCast(myItem.DataItem, syCampus)
                If mysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = mysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    Protected Sub CampusComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        RadListBoxProgramVersions.Items.Clear()
        RadListBoxProgramVersions2.Items.Clear()
        FillProgramVersions()
        ListBoxCounts()
    End Sub
    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
        ListBoxCounts()
    End Sub
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arProgram Then
                Dim myarProgram As arProgram = DirectCast(myItem.DataItem, arProgram)
                If myarProgram.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarProgram.ProgDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is arPrgGrp Then
                Dim myarPrgGrp As arPrgGrp = DirectCast(myItem.DataItem, arPrgGrp)
                If myarPrgGrp.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarPrgGrp.PrgGrpDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ChkBox As CheckBox = DirectCast(sender, CheckBox)
        If ChkBox.ID.Contains("Course") Then

        End If
        ListBoxCounts()
    End Sub
    Protected Sub SelectedAreas_ItemCheck(sender As Object, e As RadListBoxItemEventArgs)
        For Each item As RadListBoxItem In RadListBoxProgramVersions2.Items
            item.Checked = False
        Next

        e.Item.Checked = True

    End Sub

    Protected Sub RadListBoxesCampusGroup_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
        'Pass campuses to the right that belong to the selected campus groups
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim campusesToSelect As New List(Of syCampus)
        Try
            RadListBoxCampus2.Items.Clear()
            RadListBoxCampus.Items.Clear()
            FillCampusControl()
            RadListBoxProgramVersions2.Items.Clear()
            FillProgramVersions()
            ListBoxCounts()
            campusesToSelect = DA.GetCampusesForCampusGroups(GetCurrentlySelectedCampusGroups())
            For Each item As syCampus In campusesToSelect
                Dim itemToTransfer = RadListBoxCampus.FindItemByValue(item.CampusId.ToString)
                If itemToTransfer IsNot Nothing Then
                    RadListBoxCampus.Transfer(itemToTransfer, RadListBoxCampus, RadListBoxCampus2)
                End If
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Protected Sub cbkInactiveCmpGrp_OnCheckedChanged(sender As Object, e As EventArgs)

        FillCmpGrpControl()
        FillCampusControl()
    End Sub

    Protected Sub RadListCampusBoxes_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
        RadListBoxProgramVersions2.Items.Clear()
        FillProgramVersions()
        ListBoxCounts()
    End Sub

    Protected Sub checkboxInactiveCampuses_OnCheckedChanged(sender As Object, e As EventArgs)
        FillCampusControl()
    End Sub
End Class

