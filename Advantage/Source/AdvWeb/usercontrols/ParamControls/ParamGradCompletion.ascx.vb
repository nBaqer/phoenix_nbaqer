﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common

Partial Class ParamGradCompletion
    Inherits UserControl
    Implements ICustomControl

#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo


    Protected MyAdvAppSettings As AdvAppSettings

    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString


    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = Value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        SetProperties()
        If Not Page.IsPostBack Then
            'FillCmpGrpControl()
            FillDates()
            FillCampusControl()
            FillProgramControls()
        End If
        'FillPrgVerControl()
        ListBoxCounts()
    End Sub
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim Lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arPrgVersion Then
                Dim MyarPrgVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                If MyarPrgVersion.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarPrgVersion.PrgVerDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is syCampGrp Then
                Dim MysyCampGrp As syCampGrp = DirectCast(myItem.DataItem, syCampGrp)
                If MysyCampGrp.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampGrp.CampGrpDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ChkBox As CheckBox = DirectCast(sender, CheckBox)
        If ChkBox.ID.Contains("Program") Then
            FillProgramControls()
        End If
        ListBoxCounts()
    End Sub
#End Region
#Region "Methods"
    Public Sub FillDates()
        Try 
            RadDateStartDate.SelectedDate = CDate("07/01/" + (Date.Now.Year - 2).ToString)
            RadDateEndDate.SelectedDate = CDate("06/30/" + (Date.Now.Year -1).ToString)
            'If Date.Now.Month > 6 Then
            '    RadDateStartDate.SelectedDate = CDate("07/01/" + (Date.Now.Year - 2).ToString)
            '    RadDateEndDate.SelectedDate = CDate("06/30/" + (Date.Now.Year).ToString)
            'Else
            '    RadDateStartDate.SelectedDate = CDate("07/01/" + (Date.Now.Year - 3).ToString)
            '    RadDateEndDate.SelectedDate = CDate("06/30/" + (Date.Now.Year - 1).ToString)
            'End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillProgramControls()
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxProgram.DataTextField = "ProgDescrip"
            RadListBoxProgram.DataValueField = "ProgId"
            RadListBoxProgram.DataSource = GetProgramsByCampusId(selectedCampus, True, True)
            RadListBoxProgram.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampus)
        Try
            result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString))
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetProgramsByCampusId(ByVal SelectedCampus As Guid, ByVal FilteredBySelected As Boolean, ByVal FilteredByGE As Boolean) As List(Of arProgram)
        Dim DA As New ProgramDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arProgram)
        Try
            Dim SelectedPrograms As List(Of String) = GetCurrentlySelectedPrograms()
            If SelectedPrograms Is Nothing Or FilteredBySelected = False Then
                result = DA.GetProgramsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrograms"), FilteredByGE )
            Else
                result = DA.GetProgramsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrograms"), SelectedPrograms, FilteredByGE)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCurrentlySelectedPrograms() As List(Of String)
        Dim SelectedPrograms As New List(Of String)
        Try
            If RadListBoxProgram2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim selected As String = item.Value
                    SelectedPrograms.Add(selected)
                Next
                Return SelectedPrograms
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Protected Sub RadListBoxes_OnTransferring(sender As Object, e As RadListBoxTransferringEventArgs)
    End Sub
    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
        Dim LBox As RadListBox = DirectCast(sender, RadListBox)
        If LBox.ID.Contains("Program") Then
            FillProgramControls()
        End If
         ListBoxCounts()
    End Sub
    Protected Sub CampusComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        RadListBoxProgram.Items.Clear()
        RadListBoxProgram2.Items.Clear()
        FillProgramControls()
        ListBoxCounts()
    End Sub
    Protected Sub CampusComboBox_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        Dim myItem As RadComboBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is syCampus Then
                Dim MysyCampus As syCampus = DirectCast(myItem.DataItem, syCampus)
                If MysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub


    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim StatusValues As New List(Of String)

        Try
            Dim CKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If CKbox.Checked = True Then
                StatusValues.Add("Active")
                StatusValues.Add("Inactive")
            Else
                StatusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return StatusValues
    End Function
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim DisplayTree As New RadTreeView
        DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim HeaderNode1 As New RadTreeNode
            HeaderNode1.Text = "Award Year Date Options"
            HeaderNode1.Value = "Award Year Date Options"
            HeaderNode1.CssClass = "TreeParentNode"
            Dim StartDate As New RadTreeNode
            StartDate.Text = "Start Date: " + Format(RadDateStartDate.SelectedDate, "M/d/yyyy")
            Dim EndDate As New RadTreeNode
            EndDate.Text = "End Date: " + Format(RadDateEndDate.SelectedDate, "M/d/yyyy")
            HeaderNode1.Nodes.Add(StartDate)
            HeaderNode1.Nodes.Add(EndDate)
            DisplayTree.Nodes.Add(HeaderNode1)

            Dim HeaderNode2 As New RadTreeNode
            HeaderNode2.Text = "Campus"
            HeaderNode2.Value = "Campus"
            HeaderNode2.CssClass = "TreeParentNode"
            Dim CampusNode As New RadTreeNode
            CampusNode.Text = RadComboCampus.SelectedItem.Text
            HeaderNode2.Nodes.Add(CampusNode)
            DisplayTree.Nodes.Add(HeaderNode2)

            'Program
            If RadListBoxProgram2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode3 As New RadTreeNode
                HeaderNode3.Text = "Program"
                HeaderNode3.Value = "Program"
                HeaderNode3.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim ProgramNode As New RadTreeNode
                    ProgramNode.Text = item.Text
                    HeaderNode3.Nodes.Add(ProgramNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode3)
            End If
            Dim HeaderNode4 As New RadTreeNode
            HeaderNode4.Text = "On Time Graduation Date By"
            HeaderNode4.Value = "RevGradDateType"

            HeaderNode4.CssClass = "TreeParentNode"
            Dim RevGradDateNode As New RadTreeNode
            RevGradDateNode.Text = rblrevgraddatetype.SelectedItem.Text
            HeaderNode4.Nodes.Add(RevGradDateNode)
            DisplayTree.Nodes.Add(HeaderNode4)
            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            UserSettings.ItemName = ItemDetail.ItemName
            UserSettings.ItemId = ItemDetail.ItemId
            UserSettings.DetailId = ItemDetail.DetailId
            UserSettings.FriendlyName = ItemDetail.CaptionOverride

            'StartDate
            Dim ctrlSettingStartDate As New ControlSettingInfo
            Dim ctrlValuesStartDate As New List(Of ControlValueInfo)
            Dim objControlValueInfo0 As New ControlValueInfo

            ctrlSettingStartDate.ControlName = RadDateStartDate.ID
            objControlValueInfo0.DisplayText = RadDateStartDate.SelectedDate.ToString
            objControlValueInfo0.KeyData = RadDateStartDate.SelectedDate.ToString

            ctrlValuesStartDate.Add(objControlValueInfo0)
            ctrlSettingStartDate.ControlValueCollection = ctrlValuesStartDate
            SettingsCollection.Add(ctrlSettingStartDate)

            'EndDate
            Dim ctrlSettingEndDate As New ControlSettingInfo
            Dim ctrlValuesEndDate As New List(Of ControlValueInfo)
            Dim objControlValueInfo10 As New ControlValueInfo

            ctrlSettingEndDate.ControlName = RadDateEndDate.ID
            objControlValueInfo10.DisplayText = RadDateEndDate.SelectedDate.ToString
            objControlValueInfo10.KeyData = RadDateEndDate.SelectedDate.ToString

            ctrlValuesEndDate.Add(objControlValueInfo10)
            ctrlSettingEndDate.ControlValueCollection = ctrlValuesEndDate
            SettingsCollection.Add(ctrlSettingEndDate)


            'Campus
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo

            ctrlSettingCampus.ControlName = RadComboCampus.ID
            Dim selectedCampus As RadComboBoxItem = RadComboCampus.SelectedItem
            objControlValueInfo.DisplayText = selectedCampus.Text.Replace("'", "''")
            objControlValueInfo.KeyData = selectedCampus.Value.Replace("'", "''")
            ctrlValuesCampus.Add(objControlValueInfo)
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            SettingsCollection.Add(ctrlSettingCampus)

            'Program1
            Dim ctrlSettingProgram As New ControlSettingInfo
            Dim ctrlValuesProgram As New List(Of ControlValueInfo)
            ctrlSettingProgram.ControlName = RadListBoxProgram.ID
            For Each item As RadListBoxItem In RadListBoxProgram.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram.Add(objControlValueInfo2)
            Next
            ctrlSettingProgram.ControlValueCollection = ctrlValuesProgram
            SettingsCollection.Add(ctrlSettingProgram)

            'Program2
            Dim ctrlSettingProgram2 As New ControlSettingInfo
            Dim ctrlValuesProgram2 As New List(Of ControlValueInfo)


            ctrlSettingProgram2.ControlName = RadListBoxProgram2.ID
            For Each item As RadListBoxItem In RadListBoxProgram2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram2.Add(objControlValueInfo3)
            Next
            ctrlSettingProgram2.ControlValueCollection = ctrlValuesProgram2
            SettingsCollection.Add(ctrlSettingProgram2)

            ''GradDate Calculation
            Dim ctrlSettingRevGradDateType As New ControlSettingInfo
            Dim ctrlValuesRevGradDateType As New List(Of ControlValueInfo)
            Dim objControlValueInfo5 As New ControlValueInfo

            ctrlSettingRevGradDateType.ControlName = rblrevgraddateType.ID
            objControlValueInfo5.DisplayText = rblrevgraddateType.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo5.KeyData = rblrevgraddateType.SelectedValue.Replace("'", "''")

            ctrlValuesRevGradDateType.Add(objControlValueInfo5)
            ctrlSettingRevGradDateType.ControlValueCollection = ctrlValuesRevGradDateType
            SettingsCollection.Add(ctrlSettingRevGradDateType)

            UserSettings.ControlSettingsCollection = SettingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return UserSettings
    End Function

    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim ComboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If Not ItemValue.KeyData = "1900" Then
                                    ComboBox.SelectedValue = ItemValue.KeyData
                                End If

                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim LstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            LstBox.Items.Clear()
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = ItemValue.DisplayText
                                newItem.Value = ItemValue.KeyData
                                LstBox.SelectedValue = ItemValue.KeyData
                                LstBox.Items.Add(newItem)
                            Next
                        ElseIf TypeOf ctrl Is RadNumericTextBox Then
                            Dim RadNumTextBox As RadNumericTextBox = DirectCast(ctrl, RadNumericTextBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadNumTextBox.Text = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadDatePicker Then
                            Dim RadDtPicker As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadDtPicker.SelectedDate = CDate(ItemValue.KeyData)
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadListBoxProgram.Items.Clear()
                RadListBoxProgram2.Items.Clear()
            End If
            ListBoxCounts()
            ' HideShowInstitutionType()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub ListBoxCounts()
        Dim boxcount1 As Integer = RadListBoxProgram.Items.Count()
        Dim boxcount2 As Integer = RadListBoxProgram2.Items.Count()


        lblProgramCounterAvailable.Text = boxcount1.ToString.Trim & " available"
        lblProgramCounterAvailable.ToolTip = boxcount1.ToString.Trim & " available " & Caption & "(s)"
        lblProgramCounterSelected.Text = boxcount2.ToString.Trim & " selected"
        lblProgramCounterSelected.ToolTip = boxcount2.ToString.Trim & " selected " & Caption & "(s)"
    End Sub


    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
#End Region


End Class
