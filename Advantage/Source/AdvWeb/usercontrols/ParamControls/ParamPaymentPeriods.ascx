﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamPaymentPeriods.ascx.vb" Inherits="usercontrols_ParamControls_ParamPaymentPeriods" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function checkFilter(sender, args) {
        if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgram2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one program selection is allowed');
        }
        else if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgramVersion2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one program version selection is allowed');
        }
        else if (args.get_destinationListBox().get_id().indexOf("RadListBoxChargingMethod2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one ChargingMethod selection is allowed');
        }
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
        //var maxNumberOfItems = 1;
        var dest = args.get_destinationListBox();
        var totalCount = dest.get_items().get_count();
        var itemsToTransferCount = args.get_items().length;
        if (totalCount == maxNumberOfItems) {
            alert(message);
            args.set_cancel(true);
        } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
            while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                itemsToTransferCount--;
                args.get_items().pop();
            }
        }
    }
 </script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div><font color="red"> *</font>All filters are required and the filters need to be selected in the following order.</div>
    <div id="MainContainer" class="MainContainer">

        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer" runat="server" style="padding-left: 0px;">
            <div id="CampusHeader" class="CaptionLabel" runat="server">1. Campus <font color="red">*</font></div>

            <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged" OnItemDataBound="CampusComboBox_ItemDataBound"
                Width="350px" ToolTip="Select a campus to run the report" CssClass="ManualInput ReportLeftMarginInput">
            </telerik:RadComboBox>
        </div>
    </div>
    <div id="ProgramSelector" class="ProgramSelector MultiFilterReportContainer" runat="server">
        <div id="captionlabel2" class="CaptionLabel" runat="server">2. Program <font color="red">*</font></div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxProgram" runat="server" Width="360px" Height="100px"
                OnTransferring="RadListBoxes_OnTransferring"
                OnClientTransferring="checkFilter"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                TransferToID="RadListBoxProgram2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" TransferButtons="Common" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxProgram2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                <ButtonSettings ShowDelete="False" ShowReorder="False" TransferButtons="Common"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactivePrograms" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                <span id="ProgramCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="ProgramCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div id="ProgramVersionSelector" class="ProgramVersionSelector MultiFilterReportContainer" runat="server">
        <div id="Div1" class="CaptionLabel" runat="server">3. Program Version</div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxProgramVersion" runat="server" Width="360px" Height="100px"
                AllowTransfer="True"
                OnClientTransferring="checkFilter"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single"
                TransferToID="RadListBoxProgramVersion2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" TransferButtons="Common" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxProgramVersion2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="Div2" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactiveProgramVersions" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive ProgramVersions selectable" />
                <span id="ProgramVersionCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramVersionCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="ProgramVersionCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramVersionCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div id="ChargingMethodSelector" class="ChargingMethodSelector MultiFilterReportContainer" runat="server">
        <div id="Div3" class="CaptionLabel" runat="server">4. Charging Method</div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxChargingMethod" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single"
                TransferToID="RadListBoxChargingMethod2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxChargingMethod2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="Div4" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactiveChargingMethods" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive ChargingMethods selectable" />
                <span id="ChargingMethodCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblChargingMethodCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="ChargingMethodCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblChargingMethodCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div class="DateOptionsSelector MultiFilterReportContainer" id="DateOptionsSelector" runat="server">

        <div id="DateOptionsHeader" class="CaptionLabel" runat="server">Transaction Date</div>
        <div class="ReportLeftMarginInput">
            <div id="DatePopUps" class="DatePops">
                <telerik:RadComboBox ID="radMeetingDateOperator" runat="server" Visible="true" Width="200px"  CssClass="LabelFilterInput">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="Greater Than or Equal To" Value="0" />
                    </Items>
                </telerik:RadComboBox>
                <telerik:RadDatePicker ID="RadDateReportDate" runat="server"  Width="151px" DateInput-ReadOnly="true"  CssClass="FilterInput">
                    <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                    <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy"></DateInput>
                </telerik:RadDatePicker>
            </div>
        </div>
        <br />
        <div class="ReportLeftMarginInput">
            <div id="DatePopUps" class="DatePops">
                <telerik:RadComboBox ID="RadComboBox1" runat="server" Visible="true" Width="200px"  CssClass="LabelFilterInput">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="Less Than or Equal To" Value="0" />
                    </Items>
                </telerik:RadComboBox>
                <telerik:RadDatePicker ID="RadDatePicker1" runat="server" Width="151px" DateInput-ReadOnly="true" CssClass="FilterInput">
                    <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                    <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ></DateInput>
                </telerik:RadDatePicker>
            </div>
        </div>

        <br />
        <div id="Div5" class="SummarySelector MultiFilterReportContainer" runat="server">
            <div id="Div6" class="CaptionLabel" runat="server">Summary Options</div>
            <div class="ReportLeftMarginInput FilterInput">
                <asp:CheckBox runat="server" ID="chkShowSummary" Text="Show payment period details for each program version" />
            </div>
        </div>
    </div>
</asp:Panel>
