﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamStudentSearchControl.ascx.vb"
    Inherits="ParamStudentSearchControl" %>
<style type="text/css">
    </style>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridStudentSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadGridSelectedStudents">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadGridStudentSearchMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadTextBox1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadTextBox2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="lbnReset">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="lbnResetMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveAllUp">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridStudentSearch" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveAllDown">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridSelectedStudents" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveSelectedUp">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridStudentSearch" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveSelectedDown">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridSelectedStudents" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveAllUpMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridStudentSearchMRU" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveAllDownMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridSelectedStudentsMRU" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveSelectedUpMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridStudentSearchMRU" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveSelectedDownMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridSelectedStudentsMRU" />
            </UpdatedControls>
        </telerik:AjaxSetting>

    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<link href="../../css/ParameterSetControl_Default.css" rel="stylesheet" />
<asp:Panel ID="MainPanel" runat="server" Style="margin-top: 10px; margin-left: 5px;">
    <br/>
    <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
        SelectedIndex="0" MultiPageID="RadMultiPage1">
        <Tabs>
            <telerik:RadTab Text="Students">
            </telerik:RadTab>
            <telerik:RadTab Text="Most Recently Used">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0" CssClass="SearchmultiPage" ScrollBars="None">
        <telerik:RadPageView runat="server" ID="RadPageView1" CssClass="corporatePageView">
            <div id="MainContainer" class="MainContainer">
                <div id="captionlabel" class="CaptionLabel" runat="server">
                    <%=Caption%></div>
                <asp:Panel ID="Panel1" runat="server">
                    <div id="StudentSearchFilters" class="StudentSearchFilters">
                        <div id="SearchNameBox" class="SearchBox">
                            <telerik:RadTextBox ID="RadTextBox1" runat="server" OnTextChanged="FilterDisplay"
                                EmptyMessage="Search By Name, Student Number, or SSN" Width="225px">
                                <ClientEvents OnKeyPress="TBValueChanged_13" />                                 
                            </telerik:RadTextBox>
                            <asp:Button ID="ibSearchStudent" runat="Server" Width="60px" Text="Search" OnClick="FilterDisplay">
                            </asp:Button>
                            <%--<asp:ImageButton ID="ibSearchStudent" runat="server" BorderWidth="0" ImageUrl="~/Images/search.gif"
                                AlternateText="Search Student" />--%>
                            <asp:LinkButton ID="lbnReset" runat="server" OnClick="lbnReset_Click" Visible="false"
                                EnableViewState="false">Reset Search</asp:LinkButton>
                        </div>
                    </div>
                    <div id="StudentSearchGrid" class="StudentSearchGrid">
                        <telerik:RadGrid runat="server" AllowPaging="True" OnNeedDataSource="RadGridReportSearch_NeedDataSource"
                            ID="RadGridStudentSearch" Width="700px" OnRowDrop="RadGridStudentSearch_RowDrop"
                            AllowMultiRowSelection="True" GridLines="None" PageSize="5" AllowSorting="true"
                            EnableLinqExpressions="false">
                            <SortingSettings EnableSkinSortStyles="false" />
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <GroupingSettings CaseSensitive="false" />
                            <MasterTableView DataKeyNames="StudentID" AutoGenerateColumns="False" AllowMultiColumnSorting="False">
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                                        UniqueName="FirstName" HeaderTooltip="Sort By First Name" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName"
                                        UniqueName="LastName" HeaderTooltip="Sort By Last Name" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentNumber" HeaderText="Student Number" SortExpression="StudentNumber"
                                        UniqueName="StudentNumber" HeaderTooltip="Sort By Student Number" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentId" DataType="System.Guid" HeaderText="StudentId"
                                        SortExpression="StudentId" UniqueName="StudentId" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSNSearch"
                                        UniqueName="SSN" HeaderTooltip="Sort By SSN" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SSNSearch" HeaderText="SSNSearch" SortExpression="SSNSearch"
                                        UniqueName="SSNSearch" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentType" HeaderText="StudentType" SortExpression="StudentType"
                                        UniqueName="StudentType" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages"/>
                            </MasterTableView>
                            <ClientSettings AllowRowsDragDrop="True">
                                <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                            </ClientSettings>
                            <PagerStyle Mode="NumericPages"/>
                        </telerik:RadGrid>
                    </div>
                </asp:Panel>
            </div>

            <div style="margin-top:10px;margin-left:300px;">
                 <asp:ImageButton ID="btnMoveSelectedDown" runat="server" BorderWidth="0" ImageUrl="~/Images/Down.bmp" OnClick="btnMoveSelectedDown_Click"
                                 EnableViewState="false" ToolTip="Move Selected Students Down" />     
                 <asp:ImageButton ID="btnMoveSelectedUp" runat="server" BorderWidth="0" ImageUrl="~/Images/Up.bmp" OnClick="btnMoveSelectedUp_Click"
                                 EnableViewState="false" ToolTip="Move Selected Students Up"/>    
                 <asp:ImageButton ID="btnMoveAllDown" runat="server" BorderWidth="0" ImageUrl="~/Images/AllDown.bmp" OnClick="btnMoveAllDown_Click"
                                 EnableViewState="false" ToolTip="Move All Students Down"/>  
                 <asp:ImageButton ID="btnMoveAllUp" runat="server" BorderWidth="0" ImageUrl="~/Images/AllUp.bmp" OnClick="btnMoveAllUp_Click"
                                 EnableViewState="false" ToolTip="Move All Students Up"/>                           
            </div>    

        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView2" CssClass="corporatePageView">
            <div id="MainContainer2" class="MainContainer">
                <div id="Div1" class="CaptionLabel" runat="server">
                    <%=Caption3%></div>
                <asp:Panel ID="Panel2" runat="server">
                    <div id="StudentSearchFilters2" class="StudentSearchFilters">
                        <div id="SearchNameBox2" class="SearchBox">
                            <telerik:RadTextBox ID="RadTextBox2" runat="server" OnTextChanged="FilterDisplayMRU"
                                EmptyMessage="Search By Name, Student Number, or SSN" Width="225px">
                                <ClientEvents OnKeyPress="TBValueChanged_13" />                                 
                            </telerik:RadTextBox>
                            <asp:Button ID="ibSearchStudentMRU" runat="Server" Width="60px" Text="Search" OnClick="FilterDisplayMRU">
                            </asp:Button>
                            <%--<asp:ImageButton ID="ibSearchStudentMRU" runat="server" BorderWidth="0" ImageUrl="~/Images/search.gif"
                                AlternateText="Search Student" />--%>
                            <asp:LinkButton ID="lbnResetMRU" runat="server" OnClick="lbnResetMRU_Click" Visible="false"
                                EnableViewState="false">Reset Search</asp:LinkButton>
                        </div>
                    </div>
                    <div id="StudentSearchGridMRU" class="StudentSearchGrid">
                        <telerik:RadGrid runat="server" AllowPaging="True" ID="RadGridStudentSearchMRU" OnNeedDataSource="RadGridReportSearchMRU_NeedDataSource"
                            Width="700px" OnRowDrop="RadGridStudentSearchMRU_RowDrop" AllowMultiRowSelection="True"
                            GridLines="None" PageSize="5" AllowSorting="true" EnableLinqExpressions="false">
                            <SortingSettings EnableSkinSortStyles="false" />
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <GroupingSettings CaseSensitive="false" />
                            <MasterTableView DataKeyNames="StudentID" AutoGenerateColumns="False" AllowMultiColumnSorting="False">
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                                        UniqueName="FirstName" HeaderTooltip="Sort By First Name" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName"
                                        UniqueName="LastName" HeaderTooltip="Sort By Last Name" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentNumber" HeaderText="Student Number" SortExpression="StudentNumber"
                                        UniqueName="StudentNumber" HeaderTooltip="Sort By Student Number" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentId" DataType="System.Guid" HeaderText="StudentId"
                                        SortExpression="StudentId" UniqueName="StudentId" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSNSearch"
                                        UniqueName="SSN" HeaderTooltip="Sort By SSN" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SSNSearch" HeaderText="SSNSearch" SortExpression="SSNSearch"
                                        UniqueName="SSNSearch" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentType" HeaderText="StudentType" SortExpression="StudentType"
                                        UniqueName="StudentType" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <PagerStyle Mode="Slider" />
                            </MasterTableView>
                            <ClientSettings AllowRowsDragDrop="True">
                                <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                            </ClientSettings>
                            <PagerStyle Mode="NumericPages"/>
                        </telerik:RadGrid>
                    </div>
                </asp:Panel>
            </div>
            <div style="margin-top:10px;margin-left:300px;">
                 <asp:ImageButton ID="btnMoveSelectedDownMRU" runat="server" BorderWidth="0" ImageUrl="~/Images/Down.bmp" OnClick="btnMoveSelectedDownMRU_Click"
                                 EnableViewState="false" ToolTip="Move Selected Students Down" />     
                 <asp:ImageButton ID="btnMoveSelectedUpMRU" runat="server" BorderWidth="0" ImageUrl="~/Images/Up.bmp" OnClick="btnMoveSelectedUpMRU_Click"
                                 EnableViewState="false" ToolTip="Move Selected Students Up"/>    
                 <asp:ImageButton ID="btnMoveAllDownMRU" runat="server" BorderWidth="0" ImageUrl="~/Images/AllDown.bmp" OnClick="btnMoveAllDownMRU_Click"
                                 EnableViewState="false" ToolTip="Move All Students Down"/>  
                 <asp:ImageButton ID="btnMoveAllUpMRU" runat="server" BorderWidth="0" ImageUrl="~/Images/AllUp.bmp" OnClick="btnMoveAllUpMRU_Click"
                                 EnableViewState="false" ToolTip="Move All Students Up"/>                           
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <div id="MainContainer3" class="MainContainer">
        <div id="Div3" class="CaptionLabel" runat="server">
            <%=Caption2%></div>
        <asp:Panel ID="Panel3" runat="server">
            <div id="StudentSearchFilters3" class="StudentSearchFilters" style="width: 770px;
                margin-left: 620px;">
                <div id="Div2" class="SearchBox">
                    <asp:LinkButton ID="lbnResetSelected" runat="server" OnClick="lbnResetSelected_Click"
                        Visible="false" EnableViewState="false">Reset Selected</asp:LinkButton>
                </div>
            </div>
            <div id="SelectedStudentGrid" class="SelectedStudentGrid">
                <telerik:RadGrid runat="server" AllowPaging="True" ID="RadGridSelectedStudents" OnNeedDataSource="RadGridSelectedReportItems_NeedDataSource"
                    Width="700px" OnRowDrop="RadGridSelectedStudents_RowDrop" AllowMultiRowSelection="True"
                    GridLines="None" PageSize="5" AllowSorting="true" EnableLinqExpressions="false">
                    <SortingSettings EnableSkinSortStyles="false" />
                    <HeaderContextMenu EnableAutoScroll="True">
                    </HeaderContextMenu>
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView DataKeyNames="StudentID" AutoGenerateColumns="False" AllowMultiColumnSorting="False">
                        <RowIndicatorColumn>
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn>
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </ExpandCollapseColumn>
                        <NoRecordsTemplate>
                            <div style="height: 60px; cursor: pointer;">
                                You currently have no students selected for the report</div>
                        </NoRecordsTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                                UniqueName="FirstName" HeaderTooltip="Sort By First Name" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName"
                                UniqueName="LastName" HeaderTooltip="Sort By Last Name" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="StudentNumber" HeaderText="Student Number" SortExpression="StudentNumber"
                                UniqueName="StudentNumber" HeaderTooltip="Sort By Student Number" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="StudentId" DataType="System.Guid" HeaderText="StudentId"
                                SortExpression="StudentId" UniqueName="StudentId" Visible="false" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSNSearch"
                                UniqueName="SSN" HeaderTooltip="Sort By SSN" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SSNSearch" HeaderText="SSNSearch" SortExpression="SSNSearch"
                                UniqueName="SSNSearch" Visible="false" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="StudentType" HeaderText="StudentType" SortExpression="StudentType"
                                UniqueName="StudentType" Visible="false" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages"/>
                    </MasterTableView>
                    <ClientSettings AllowRowsDragDrop="True">
                        <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                    </ClientSettings>
                    <PagerStyle Mode="NumericPages"/>
                </telerik:RadGrid>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>
<div class="ResetDiv">
</div>
