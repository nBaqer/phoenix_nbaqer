﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamTitleIvSapResultsReport.ascx.vb" Inherits="usercontrols_ParamControls_ParamTitleIvSapResultsReport" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>

<script>

</script>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>

<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">
        <div id="CmpGrpSelector" class="CmpGrpSelector MultiFilterReportContainer" runat="server">
            <div id="Div3" class="CaptionLabel" runat="server">Campus Group</div>
            <div class="FilterInput">

                <telerik:RadListBox ID="RadListBoxCmpGrp" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxesCampusGroup_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxCmpGrp2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxCmpGrp2" runat="server"
                    OnTransferred="RadListBoxesCampusGroup_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="InactiveCampusGroupCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveCmpGrp" runat="server"
                        OnCheckedChanged="cbkInactiveCmpGrp_OnCheckedChanged" AutoPostBack="true"
                        Text="Show Inactive"
                        ToolTip="Check this box to make inactive programs selectable" />
                    <span id="CampusGroupCounterAvailable" class="RadListBox1Counter">
                        <asp:Label ID="lblCmpGrpCounterAvailable" runat="server"></asp:Label>
                    </span>
                    <span id="CampusGroupCounterSelected" class="RadListBox2Counter">
                        <asp:Label ID="lblCmpGrpCounterSelected" runat="server"></asp:Label>
                    </span>
                </div>
            </div>
        </div>

        <div id="CampusSelectorDiv" class="CampusSelector MultiFilterReportContainer" runat="server">
            <div id="CampusSelectorLabel" class="CaptionLabel" runat="server">Campus</div>
            <div class="FilterInput">

                <telerik:RadListBox ID="RadListBoxCampus" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListCampusBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxCampus2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxCampus2" runat="server"
                    OnTransferred="RadListCampusBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="InactiveCampusCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="checkboxInactiveCampuses" runat="server"
                        OnCheckedChanged="checkboxInactiveCampuses_OnCheckedChanged" AutoPostBack="true"
                        Text="Show Inactive"
                        ToolTip="Check this box to make inactive programs selectable" />
                    <span id="CampusCounterAvailable" class="RadListBox1Counter">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </span>
                    <span id="CampusCounterSelected" class="RadListBox2Counter">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                    </span>
                </div>
            </div>
        </div>

        <div id="ProgramVersionSelector" class="ProgramVersionSelector MultiFilterReportContainer" runat="server">
            <div id="Div4" class="CaptionLabel" runat="server">Program Version</div>
            <div class="FilterInput">

                <telerik:RadListBox ID="RadListBoxProgramVersions" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxProgramVersions2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxProgramVersions2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="chkInActiveProgramVersions" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="lblProgramVersionsCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="lblProgramVersionsCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>

                <br />
            </div>
        </div>

        <div id="enrollmentStatusSelector" class="enrollmentStatusSelector MultiFilterReportContainer" runat="server">
            <div id="Div2" class="CaptionLabel" runat="server">Enrollment Status</div>
            <div class="FilterInput">

                <telerik:RadListBox ID="RadListBoxEnrollmentStatus" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxesEnrollmentStatus_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxEnrollmentStatus2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxEnrollmentStatus2" runat="server"
                    OnTransferred="RadListBoxesEnrollmentStatus_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="chkInActiveEnrollmentStatuses" runat="server" OnCheckedChanged="CheckBoxEnrollmentStatus_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>

        <div id="StudentStartDateSelector" class="StudentStartDateSelector MultiFilterReportContainer" runat="server">
            <div id="captionlabel" class="CaptionLabel" runat="server">Student Start Date</div>
            <div class="FilterInput">

                <div id="DateRow1" class="DateRow1">
                    <span id="DateModifier1" class="DateModifier1">
                        <telerik:RadComboBox ID="RadComboBox1" runat="server" Width="150px"  CssClass="LabelFilterInput">
                            <Items>
                                <%--<telerik:RadComboBoxItem runat="server" Text="IsEqualTo"
                                Value="0" />
                            <telerik:RadComboBoxItem runat="server" Text="IsNotEqualTo"
                                Value="1" />
                            <telerik:RadComboBoxItem runat="server" Text="GreaterThanOrEqualTo"
                                Value="2" />--%>
                                <telerik:RadComboBoxItem runat="server" Text="LessThanOrEqualTo"
                                    Value="3" />
                                <%--<telerik:RadComboBoxItem runat="server" Text="GreaterThan"
                                Value="4" />
                            <telerik:RadComboBoxItem runat="server" Text="LessThan"
                                Value="5" />
                            <telerik:RadComboBoxItem runat="server" Text="InList"
                                Value="6" />
                            <telerik:RadComboBoxItem runat="server" Text="IsNull" Value="7" />
                            <telerik:RadComboBoxItem runat="server" Text="IsNotNull" Value="8" />
                            <telerik:RadComboBoxItem runat="server" Text="IsEmpty" Value="9" />
                            <telerik:RadComboBoxItem runat="server" Text="IsNotEmpty" Value="10" />
                            <telerik:RadComboBoxItem runat="server" Text="Contains" Value="11" />
                            <telerik:RadComboBoxItem runat="server" Text="StartsWith" Value="12" />
                            <telerik:RadComboBoxItem runat="server" Text="EndsWith" Value="13" />--%>
                            </Items>
                        </telerik:RadComboBox>
                    </span>
                    <span id="DatePicker1" class="DatePicker1">
                        <telerik:RadDatePicker ID="StudentStartDatePicker" runat="server"
                            Width="151px" CssClass="ManualInput">
                            <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                            <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy"></DateInput>
                        </telerik:RadDatePicker>
                    </span>
                    <span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ErrorMessage="Please enter a Date" ControlToValidate="StudentStartDatePicker"
                            Enabled="false"></asp:RequiredFieldValidator>
                    </span>
                </div>
            </div>
        </div>
    </div>

</asp:Panel>
