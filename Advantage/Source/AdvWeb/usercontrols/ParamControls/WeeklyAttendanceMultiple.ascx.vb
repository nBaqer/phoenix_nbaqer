﻿
Imports System.Data
Imports System.Diagnostics
Imports System.Reflection
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Parameters.Info
Imports FAME.Parameters.Interfaces
Imports Telerik.Web.UI
Imports Telerik.Web.UI.Calendar

Partial Class WeeklyAttendanceMultiple
    Inherits UserControl
    Implements ICustomControl
    Protected MyAdvAppSettings As AdvAppSettings
    Private _sqlConn As String
    Private _savedSetting As ParamItemUserSettingsInfo
    Private _ItemDetail As ParameterDetailItemInfo

    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _savedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _savedSetting = value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _sqlConn
        End Get
        Set(ByVal value As String)
            _sqlConn = value
        End Set
    End Property
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride
            'Campus
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo
            ctrlSettingCampus.ControlName = RadComboCampus.ID
            Dim selectedCampus As RadComboBoxItem = RadComboCampus.SelectedItem
            objControlValueInfo.DisplayText = selectedCampus.Text.Replace("'", "''")
            objControlValueInfo.KeyData = selectedCampus.Value.Replace("'", "''")
            ctrlValuesCampus.Add(objControlValueInfo)
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            settingsCollection.Add(ctrlSettingCampus)
            'week end date
            Dim ctrlSettingCutoffDate As New ControlSettingInfo
            Dim objControlValueInfoCutOffDate As New ControlValueInfo
            Dim ctrlValuesDate As New List(Of ControlValueInfo)
            ctrlSettingCutoffDate.ControlName = RadWeekEndingDate.ID
            If RadWeekEndingDate.SelectedDate Is Nothing Then
                objControlValueInfoCutOffDate.DisplayText = ""
                objControlValueInfoCutOffDate.KeyData = ""
            Else
                objControlValueInfoCutOffDate.DisplayText = RadWeekEndingDate.SelectedDate.ToString
                objControlValueInfoCutOffDate.KeyData = RadWeekEndingDate.SelectedDate.ToString
                ctrlValuesDate.Add(objControlValueInfoCutOffDate)
            End If
            ctrlSettingCutoffDate.ControlValueCollection = ctrlValuesDate
            settingsCollection.Add(ctrlSettingCutoffDate)

            'Term
            Dim ctrlSettingTerm2 As New ControlSettingInfo
            Dim ctrlValuesTerm2 As New List(Of ControlValueInfo)
            ctrlSettingTerm2.ControlName = RadListBoxTerms2.ID
            For Each item As RadListBoxItem In RadListBoxTerms2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesTerm2.Add(objControlValueInfo3)
            Next
            ctrlSettingTerm2.ControlValueCollection = ctrlValuesTerm2
            settingsCollection.Add(ctrlSettingTerm2)

            'Course
            Dim ctrlSettingProgram As New ControlSettingInfo
            Dim ctrlValuesProgram As New List(Of ControlValueInfo)
            ctrlSettingProgram.ControlName = RadListBoxCourse2.ID
            For Each item As RadListBoxItem In RadListBoxCourse2.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram.Add(objControlValueInfo2)
            Next
            ctrlSettingProgram.ControlValueCollection = ctrlValuesProgram
            settingsCollection.Add(ctrlSettingProgram)

            'Instructor
            Dim ctrlSettingInstructor As New ControlSettingInfo
            Dim ctrlValuesInstructor As New List(Of ControlValueInfo)
            ctrlSettingInstructor.ControlName = RadListBoxInstructor2.ID
            For Each item As RadListBoxItem In RadListBoxInstructor2.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesInstructor.Add(objControlValueInfo2)
            Next
            ctrlSettingInstructor.ControlValueCollection = ctrlValuesInstructor
            settingsCollection.Add(ctrlSettingInstructor)

            'Shift
            Dim ctrlSettingShift As New ControlSettingInfo
            Dim ctrlValuesShift As New List(Of ControlValueInfo)
            ctrlSettingShift.ControlName = RadListBoxShift2.ID
            For Each item As RadListBoxItem In RadListBoxShift2.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesShift.Add(objControlValueInfo2)
            Next
            ctrlSettingShift.ControlValueCollection = ctrlValuesShift
            settingsCollection.Add(ctrlSettingShift)

            'Section
            Dim ctrlSettingSection As New ControlSettingInfo
            Dim ctrlValuesSection As New List(Of ControlValueInfo)
            ctrlSettingSection.ControlName = RadListBoxSection2.ID
            For Each item As RadListBoxItem In RadListBoxSection2.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesSection.Add(objControlValueInfo2)
            Next
            ctrlSettingSection.ControlValueCollection = ctrlValuesSection
            settingsCollection.Add(ctrlSettingSection)

            'Enrollment Status
            Dim ctrlSettingEnrollmentStatus As New ControlSettingInfo
            Dim ctrlValuesEnrollmentStatus As New List(Of ControlValueInfo)
            ctrlSettingEnrollmentStatus.ControlName = RadListBoxEnrollmentStatus2.ID
            For Each item As RadListBoxItem In RadListBoxEnrollmentStatus2.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesEnrollmentStatus.Add(objControlValueInfo2)
            Next
            ctrlSettingEnrollmentStatus.ControlValueCollection = ctrlValuesEnrollmentStatus
            settingsCollection.Add(ctrlSettingEnrollmentStatus)

            userSettings.ControlSettingsCollection = settingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        Return userSettings
    End Function
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim displayTree As New RadTreeView
        displayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim headerNode1 As New RadTreeNode
            headerNode1.Text = "Campus"
            headerNode1.Value = "Campus"
            headerNode1.CssClass = "TreeParentNode"
            Dim campusNode As New RadTreeNode
            campusNode.Text = RadComboCampus.SelectedItem.Text
            headerNode1.Nodes.Add(campusNode)
            displayTree.Nodes.Add(headerNode1)
            'week end date
            Dim headerNode21 As New RadTreeNode
            headerNode21.Text = "Attendance for week ending"
            headerNode21.Value = "Attendance for week ending"
            headerNode21.CssClass = "TreeParentNode"
            Dim programNode21 As New RadTreeNode
            programNode21.Text = RadWeekEndingDate.SelectedDate.ToString
            headerNode21.Nodes.Add(programNode21)
            displayTree.Nodes.Add(headerNode21)
            'Enrollment Status
            If RadListBoxEnrollmentStatus2.Items.Count > 0 Then
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Enrollment Status"
                headerNode2.Value = "Enrollment Status"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxEnrollmentStatus2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If
            'Term
            If RadListBoxTerms2.Items.Count > 0 Then
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Term"
                headerNode2.Value = "Term"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxTerms2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If
            'course
            If RadListBoxCourse2.Items.Count > 0 Then
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Course"
                headerNode2.Value = "Course"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxCourse2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If
            'Instructor
            If RadListBoxInstructor2.Items.Count > 0 Then
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Instructor"
                headerNode2.Value = "Instructor"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxInstructor2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If
            'Shift
            If RadListBoxShift2.Items.Count > 0 Then
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Shift"
                headerNode2.Value = "Shift"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxShift2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If
            'Section
            If RadListBoxSection2.Items.Count > 0 Then
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Section"
                headerNode2.Value = "Section"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxSection2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If
            Return displayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Public Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim radioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                radioButList.SelectedValue = itemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim comboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                If Not itemValue.KeyData = "1900" Then
                                    comboBox.SelectedValue = itemValue.KeyData
                                End If
                            Next
                        ElseIf TypeOf ctrl Is RadDatePicker Then
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                RadWeekEndingDate.SelectedDate = CType(itemValue.KeyData, Date?)
                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim lstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            lstBox.Items.Clear()
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = itemValue.DisplayText
                                newItem.Value = itemValue.KeyData
                                lstBox.SelectedValue = itemValue.KeyData
                                lstBox.Items.Add(newItem)
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                ResetAll("Campus")
            End If
            FillEnrollmentStatus()
            ListBoxCounts("Term")
            ListBoxCounts("Course")
            ListBoxCounts("Instructor")
            ListBoxCounts("Shift")
            ListBoxCounts("Section")
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub


    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetProperties()
        Try
            RadWeekEndingDate.MaxDate = DateTime.Now
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim da As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampus)
        Try
            result = da.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString))
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Sub ListBoxCounts(name As String)
        Dim box1 As Integer
        Dim box2 As Integer
        Select Case name
            Case "Term"
                box1 = RadListBoxTerms.Items.Count()
                box2 = RadListBoxTerms2.Items.Count()
                If Not (box1 = 0 And box2 = 0) Then
                    lblTermsCounterAvailable.Text = box1.ToString.Trim & " available"
                    lblTermsCounterAvailable.ToolTip = box1.ToString.Trim & " available " & Caption & "(s)"
                    lblTermsCounterSelected.Text = box2.ToString.Trim & " selected"
                    lblTermsCounterSelected.ToolTip = box2.ToString.Trim & " selected " & Caption & "(s)"
                End If
            Case "Course"
                box1 = RadListBoxCourse.Items.Count()
                box2 = RadListBoxCourse2.Items.Count()
                If Not (box1 = 0 And box2 = 0) Then
                    lblCourseCounterAvailable.Text = box1.ToString.Trim & " available"
                    lblCourseCounterAvailable.ToolTip = box1.ToString.Trim & " available " & Caption & "(s)"
                    lblCourseCounterSelected.Text = box2.ToString.Trim & " selected"
                    lblCourseCounterSelected.ToolTip = box2.ToString.Trim & " selected " & Caption & "(s)"
                End If
            Case "Instructor"
                box1 = RadListBoxInstructor.Items.Count()
                box2 = RadListBoxInstructor2.Items.Count()
                If Not (box1 = 0 And box2 = 0) Then
                    lblInstructorCounterAvailable.Text = box1.ToString.Trim & " available"
                    lblInstructorCounterAvailable.ToolTip = box1.ToString.Trim & " available " & Caption & "(s)"
                    lblInstructorCounterSelected.Text = box2.ToString.Trim & " selected"
                    lblInstructorCounterSelected.ToolTip = box2.ToString.Trim & " selected " & Caption & "(s)"
                End If
            Case "Shift"
                box1 = RadListBoxShift.Items.Count()
                box2 = RadListBoxShift2.Items.Count()
                If Not (box1 = 0 And box2 = 0) Then
                    lblShiftCounterAvailable.Text = box1.ToString.Trim & " available"
                    lblShiftCounterAvailable.ToolTip = box1.ToString.Trim & " available " & Caption & "(s)"
                    lblShiftCounterSelected.Text = box2.ToString.Trim & " selected"
                    lblShiftCounterSelected.ToolTip = box2.ToString.Trim & " selected " & Caption & "(s)"
                End If
            Case "Section"
                box1 = RadListBoxSection.Items.Count()
                box2 = RadListBoxSection2.Items.Count()
                If Not (box1 = 0 And box2 = 0) Then
                    lblSectionCounterAvailable.Text = box1.ToString.Trim & " available"
                    lblSectionCounterAvailable.ToolTip = box1.ToString.Trim & " available " & Caption & "(s)"
                    lblSectionCounterSelected.Text = box2.ToString.Trim & " selected"
                    lblSectionCounterSelected.ToolTip = box2.ToString.Trim & " selected " & Caption & "(s)"
                End If
            Case "EnrollmentStatus"
                box1 = RadListBoxEnrollmentStatus.Items.Count()
                box2 = RadListBoxEnrollmentStatus2.Items.Count()
                If Not (box1 = 0 And box2 = 0) Then
                    lblEnrollmentStatusCounterAvailable.Text = box1.ToString.Trim & " available"
                    lblEnrollmentStatusCounterAvailable.ToolTip = box1.ToString.Trim & " available " & Caption & "(s)"
                    lblEnrollmentStatusCounterSelected.Text = box2.ToString.Trim & " selected"
                    lblEnrollmentStatusCounterSelected.ToolTip = box2.ToString.Trim & " selected " & Caption & "(s)"
                End If
        End Select
    End Sub
    Private Function GetCurrentlySelectedSectionName() As String
        Dim selectedItems As String
        selectedItems = String.Empty
        If RadListBoxSection2.Items.Count > 0 Then
            For Each item As RadListBoxItem In RadListBoxSection2.Items
                Dim selected As String = item.Text
                selectedItems = selectedItems + selected + ","
            Next
            selectedItems = selectedItems.Remove(selectedItems.Length - 1)
            Return selectedItems
        Else
            Return selectedItems
        End If
    End Function
    Private Function ResetAll(ctrl As String)
        If ctrl = "Campus" Then
            RadListBoxEnrollmentStatus.Items.Clear()
            RadListBoxEnrollmentStatus2.Items.Clear()
            lblEnrollmentStatusCounterAvailable.Text = ""
            lblEnrollmentStatusCounterAvailable.ToolTip = ""
            lblEnrollmentStatusCounterSelected.Text = ""
            lblEnrollmentStatusCounterSelected.ToolTip = ""
            FillEnrollmentStatus()
        End If
        RadListBoxTerms.Items.Clear()
        RadListBoxTerms2.Items.Clear()
        RadListBoxCourse.Items.Clear()
        RadListBoxCourse2.Items.Clear()
        RadListBoxInstructor.Items.Clear()
        RadListBoxInstructor2.Items.Clear()
        RadListBoxShift.Items.Clear()
        RadListBoxShift2.Items.Clear()
        RadListBoxSection.Items.Clear()
        RadListBoxSection2.Items.Clear()

        lblTermsCounterAvailable.Text = ""
        lblTermsCounterAvailable.ToolTip = ""
        lblTermsCounterSelected.Text = ""
        lblTermsCounterSelected.ToolTip = ""
        lblCourseCounterAvailable.Text = ""
        lblCourseCounterAvailable.ToolTip = ""
        lblCourseCounterSelected.Text = ""
        lblCourseCounterSelected.ToolTip = ""
        lblInstructorCounterAvailable.Text = ""
        lblInstructorCounterAvailable.ToolTip = ""
        lblInstructorCounterSelected.Text = ""
        lblInstructorCounterSelected.ToolTip = ""
        lblShiftCounterAvailable.Text = ""
        lblShiftCounterAvailable.ToolTip = ""
        lblShiftCounterSelected.Text = ""
        lblShiftCounterSelected.ToolTip = ""
        lblSectionCounterAvailable.Text = ""
        lblSectionCounterAvailable.ToolTip = ""
        lblSectionCounterSelected.Text = ""
        lblSectionCounterSelected.ToolTip = ""

        FillTerms()

    End Function
    Private Function GetCurrentlySelectedItems(ByVal name As String) As String
        Dim selectedItems As String
        selectedItems = String.Empty
        Select Case name
            Case "Term"
                If RadListBoxTerms2.Items.Count > 0 Then
                    For Each item As RadListBoxItem In RadListBoxTerms2.Items
                        Dim selected As String = item.Value
                        selectedItems = selectedItems + selected + ","
                    Next
                    selectedItems = selectedItems.Remove(selectedItems.Length - 1)
                    Return selectedItems
                Else
                    Return selectedItems
                End If
            Case "Course"
                If RadListBoxCourse2.Items.Count > 0 Then
                    For Each item As RadListBoxItem In RadListBoxCourse2.Items
                        Dim selected As String = item.Value
                        selectedItems = selectedItems + selected + ","
                    Next
                    selectedItems = selectedItems.Remove(selectedItems.Length - 1)
                    Return selectedItems
                Else
                    Return selectedItems
                End If
            Case "Instructor"
                If RadListBoxInstructor2.Items.Count > 0 Then
                    For Each item As RadListBoxItem In RadListBoxInstructor2.Items
                        Dim selected As String = item.Value
                        selectedItems = selectedItems + selected + ","
                    Next
                    selectedItems = selectedItems.Remove(selectedItems.Length - 1)
                    Return selectedItems
                Else
                    Return selectedItems
                End If
            Case "Shift"
                If RadListBoxShift2.Items.Count > 0 Then
                    For Each item As RadListBoxItem In RadListBoxShift2.Items
                        Dim selected As String = item.Value
                        selectedItems = selectedItems + selected + ","
                    Next
                    selectedItems = selectedItems.Remove(selectedItems.Length - 1)
                    Return selectedItems
                Else
                    Return selectedItems
                End If
            Case "Section"
                If RadListBoxSection2.Items.Count > 0 Then
                    For Each item As RadListBoxItem In RadListBoxSection2.Items
                        Dim selected As String = item.Value
                        selectedItems = selectedItems + selected + ","
                    Next
                    selectedItems = selectedItems.Remove(selectedItems.Length - 1)
                    Return selectedItems
                Else
                    Return selectedItems
                End If
            Case "EnrollmentStatus"
                If RadListBoxSection2.Items.Count > 0 Then
                    For Each item As RadListBoxItem In RadListBoxEnrollmentStatus2.Items
                        Dim selected As String = item.Value
                        selectedItems = selectedItems + selected + ","
                    Next
                    selectedItems = selectedItems.Remove(selectedItems.Length - 1)
                    Return selectedItems
                Else
                    Return selectedItems
                End If
        End Select
    End Function
    Public Sub FillCourseByTerms()
        Try
            Dim selectedTerms As String
            selectedTerms = GetCurrentlySelectedItems("Term")
            If String.IsNullOrEmpty(selectedTerms) Then
                RadListBoxCourse.Items.Clear()
                RadListBoxCourse2.Items.Clear()
                lblCourseCounterAvailable.Text = ""
                lblCourseCounterAvailable.ToolTip = ""
                lblCourseCounterSelected.Text = ""
                lblCourseCounterSelected.ToolTip = ""
                RadListBoxInstructor.Items.Clear()
                RadListBoxInstructor2.Items.Clear()
                RadListBoxShift.Items.Clear()
                RadListBoxShift2.Items.Clear()
                RadListBoxSection.Items.Clear()
                RadListBoxSection2.Items.Clear()
                lblInstructorCounterAvailable.Text = ""
                lblInstructorCounterAvailable.ToolTip = ""
                lblInstructorCounterSelected.Text = ""
                lblInstructorCounterSelected.ToolTip = ""
                lblShiftCounterAvailable.Text = ""
                lblShiftCounterAvailable.ToolTip = ""
                lblShiftCounterSelected.Text = ""
                lblShiftCounterSelected.ToolTip = ""
                lblSectionCounterAvailable.Text = ""
                lblSectionCounterAvailable.ToolTip = ""
                lblSectionCounterSelected.Text = ""
                lblSectionCounterSelected.ToolTip = ""
            Else
                Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
                Dim dA As New CoursesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
                Dim result As IEnumerable(Of USP_getCourses_byCampusDate_andTermResult)
                Dim weekEndingDate As Date = DateTime.Now.Date
                If RadWeekEndingDate.SelectedDate IsNot Nothing Then
                    weekEndingDate = RadWeekEndingDate.SelectedDate
                End If
                result = dA.USP_getCourses_byCampusDate_andTerm(selectedCampus, weekEndingDate, selectedTerms, cbkInactiveCourses.Checked)
                Dim dtCourse As New DataTable("course")
                    dtCourse.Columns.Add("DescripCode")
                    dtCourse.Columns.Add("ReqId")

                    ' Delete from RadListBox items that are already selected

                    Dim selectedItems As String
                    selectedItems = GetCurrentlySelectedItems("Course")
                    Dim selectedItemIds() As String

                    Dim flag As Boolean = False
                    If Not String.IsNullOrEmpty(selectedItems) Then
                        selectedItemIds = selectedItems.Split(","c)
                        flag = True
                    End If
                    Dim val As String

                    For Each datarow As USP_getCourses_byCampusDate_andTermResult In result
                        If (flag) Then
                            'do not add if that item is already selected
                            val = System.Array.Find(selectedItemIds, Function(x) (x.Equals(datarow.ReqId.ToString())))
                            If String.IsNullOrEmpty(val) Then
                                dtCourse.Rows.Add(New Object() {datarow.DescripCode, datarow.ReqId})
                            End If
                        Else
                            dtCourse.Rows.Add(New Object() {datarow.DescripCode, datarow.ReqId})
                        End If
                    Next
                    RadListBoxCourse.DataTextField = "DescripCode"
                    RadListBoxCourse.DataValueField = "ReqId"
                    RadListBoxCourse.DataSource = dtCourse
                    RadListBoxCourse.DataBind()
                    ' Delete from RadListBoxCourse2 items do not belong to selected filters above
                    Dim RadListBoxItemList As New List(Of RadListBoxItem)
                    For Each item As RadListBoxItem In RadListBoxCourse2.Items
                        RadListBoxItemList.Add(item)
                    Next
                    For Each item As RadListBoxItem In RadListBoxItemList
                        Dim query = (From re In result Where re.ReqId = Guid.Parse(item.Value))
                        If Not query.Any() Then
                            RadListBoxCourse2.Items.Remove(item)
                        End If
                    Next
                    ListBoxCounts("Course")
                End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Public Sub FillInstructorByCourseAndTerms()
        Dim selectedTerms As String
        Dim selectedCourse As String
        selectedTerms = GetCurrentlySelectedItems("Term")
        selectedCourse = GetCurrentlySelectedItems("Course")
        If String.IsNullOrEmpty(selectedCourse) Then
            RadListBoxInstructor.Items.Clear()
            RadListBoxInstructor2.Items.Clear()
            RadListBoxShift.Items.Clear()
            RadListBoxShift2.Items.Clear()
            RadListBoxSection.Items.Clear()
            RadListBoxSection2.Items.Clear()
            lblInstructorCounterAvailable.Text = ""
            lblInstructorCounterAvailable.ToolTip = ""
            lblInstructorCounterSelected.Text = ""
            lblInstructorCounterSelected.ToolTip = ""
            lblShiftCounterAvailable.Text = ""
            lblShiftCounterAvailable.ToolTip = ""
            lblShiftCounterSelected.Text = ""
            lblShiftCounterSelected.ToolTip = ""
            lblSectionCounterAvailable.Text = ""
            lblSectionCounterAvailable.ToolTip = ""
            lblSectionCounterSelected.Text = ""
            lblSectionCounterSelected.ToolTip = ""
        Else

            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            Dim dA As New UserDa(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            Dim result As IEnumerable(Of USP_getInstructor_byCampusDate_andTermCourseResult)
            Dim weekEndingDate As Date = DateTime.Now.Date
            If RadWeekEndingDate.SelectedDate IsNot Nothing Then
                weekEndingDate = RadWeekEndingDate.SelectedDate
            End If
            result = dA.USP_getInstructor_byCampusDate_andTermCourse(selectedCampus, weekEndingDate, selectedTerms, selectedCourse, cbkInActiveInstructor.Checked)
            Dim dtInstructor As New DataTable("Instructor")
            dtInstructor.Columns.Add("InstructorId")
            dtInstructor.Columns.Add("FullName")

            ' Delete from RadListBox items that are already selected

            Dim selectedItems As String
            selectedItems = GetCurrentlySelectedItems("Instructor")
            Dim selectedItemIds() As String

            Dim flag As Boolean = False
            If Not String.IsNullOrEmpty(selectedItems) Then
                selectedItemIds = selectedItems.Split(","c)
                flag = True
            End If
            Dim val As String

            For Each datarow As USP_getInstructor_byCampusDate_andTermCourseResult In result
                If (flag) Then
                    'do not add if that item is already selected
                    val = System.Array.Find(selectedItemIds, Function(x) (x.Equals(datarow.InstructorId.ToString())))
                    If String.IsNullOrEmpty(val) Then
                        dtInstructor.Rows.Add(New Object() {datarow.InstructorId, datarow.FullName})
                    End If
                Else
                    dtInstructor.Rows.Add(New Object() {datarow.InstructorId, datarow.FullName})
                End If
            Next
            RadListBoxInstructor.DataTextField = "FullName"
            RadListBoxInstructor.DataValueField = "InstructorId"
            RadListBoxInstructor.DataSource = dtInstructor
            RadListBoxInstructor.DataBind()
            ' Delete from RadListBoxInstructor2 items do not belong to selected filters above
            Dim RadListBoxItemList As New List(Of RadListBoxItem)
            For Each item As RadListBoxItem In RadListBoxInstructor2.Items
                RadListBoxItemList.Add(item)
            Next
            For Each item As RadListBoxItem In RadListBoxItemList
                Dim query = (From re In result Where re.InstructorId = Guid.Parse(item.Value))
                If Not query.Any() Then
                    RadListBoxInstructor2.Items.Remove(item)
                End If
            Next
            ListBoxCounts("Instructor")
        End If
    End Sub
    Public Sub FillSectionByShiftInstructorCourseAndTerms()
        Dim selectedTerms As String
        Dim selectedCourse As String
        Dim selectedInstructor As String
        Dim selectedShift As String
        selectedTerms = GetCurrentlySelectedItems("Term")
        selectedCourse = GetCurrentlySelectedItems("Course")
        selectedInstructor = GetCurrentlySelectedItems("Instructor")
        selectedShift = GetCurrentlySelectedItems("Shift")
        If String.IsNullOrEmpty(selectedShift) Then
            RadListBoxSection.Items.Clear()
            RadListBoxSection2.Items.Clear()
            lblSectionCounterAvailable.Text = ""
            lblSectionCounterAvailable.ToolTip = ""
            lblSectionCounterSelected.Text = ""
            lblSectionCounterSelected.ToolTip = ""
        Else
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            Dim dA As New ClassSectionDa(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            Dim result As IEnumerable(Of USP_getClasses_byCampusDate_andTermCourseInstructorShiftResult)
            Dim weekEndingDate As Date = DateTime.Now.Date
            If RadWeekEndingDate.SelectedDate IsNot Nothing Then
                weekEndingDate = RadWeekEndingDate.SelectedDate
            End If
            result = dA.USP_getClasses_byCampusDate_andTermCourseInstructorShift(selectedCampus, weekEndingDate, selectedTerms, selectedCourse, selectedInstructor, selectedShift)
            Dim dtSection As New DataTable("Section")
            dtSection.Columns.Add("ClsSectionIds")
            dtSection.Columns.Add("ClsSection")
            ' Delete from RadListBox items that are already selected

            Dim selectedItems As String
            selectedItems = GetCurrentlySelectedSectionName()
            Dim selectedItemIds() As String

            Dim flag As Boolean = False
            If Not String.IsNullOrEmpty(selectedItems) Then
                selectedItemIds = selectedItems.Split(","c)
                flag = True
            End If
            Dim val As String
            For Each datarow As USP_getClasses_byCampusDate_andTermCourseInstructorShiftResult In result
                If (flag) Then
                    'do not add if that item is already selected
                    val = System.Array.Find(selectedItemIds, Function(x) (x.Equals(datarow.ClsSection.ToString())))
                    If String.IsNullOrEmpty(val) Then
                        dtSection.Rows.Add(New Object() {datarow.ClsSectionIds, datarow.ClsSection})
                    End If
                Else
                    dtSection.Rows.Add(New Object() {datarow.ClsSectionIds, datarow.ClsSection})
                End If

            Next
            RadListBoxSection.DataTextField = "ClsSection"
            RadListBoxSection.DataValueField = "ClsSectionIds"
            RadListBoxSection.DataSource = dtSection
            RadListBoxSection.DataBind()
            ' Delete from RadListBoxSection2 items do not belong to selected filters above
            Dim RadListBoxItemList As New List(Of RadListBoxItem)
            For Each item As RadListBoxItem In RadListBoxSection2.Items
                RadListBoxItemList.Add(item)
            Next
            For Each item As RadListBoxItem In RadListBoxItemList
                Dim query = (From re In result Where re.ClsSection = item.Text)
                If Not query.Any() Then
                    RadListBoxSection2.Items.Remove(item)
                End If
            Next
            ListBoxCounts("Section")
        End If
    End Sub
    Public Sub FillShiftByInstructorCourseAndTerms()
        Dim selectedTerms As String
        Dim selectedCourse As String
        Dim selectedInstructor As String
        selectedTerms = GetCurrentlySelectedItems("Term")
        selectedCourse = GetCurrentlySelectedItems("Course")
        selectedInstructor = GetCurrentlySelectedItems("Instructor")
        If String.IsNullOrEmpty(selectedInstructor) Then
            RadListBoxShift.Items.Clear()
            RadListBoxShift2.Items.Clear()
            RadListBoxSection.Items.Clear()
            RadListBoxSection2.Items.Clear()
            lblShiftCounterAvailable.Text = ""
            lblShiftCounterAvailable.ToolTip = ""
            lblShiftCounterSelected.Text = ""
            lblShiftCounterSelected.ToolTip = ""
            lblSectionCounterAvailable.Text = ""
            lblSectionCounterAvailable.ToolTip = ""
            lblSectionCounterSelected.Text = ""
            lblSectionCounterSelected.ToolTip = ""
        Else
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            Dim dA As New ShiftDa(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            Dim result As IEnumerable(Of USP_getShift_byCampusDate_andTermCourseInstructorResult)
            Dim weekEndingDate As Date = DateTime.Now.Date
            If RadWeekEndingDate.SelectedDate IsNot Nothing Then
                weekEndingDate = RadWeekEndingDate.SelectedDate
            End If
            result = dA.USP_getShift_byCampusDate_andTermCourseInstructor(selectedCampus, weekEndingDate, selectedTerms, selectedCourse, selectedInstructor, cbkInactiveShift.Checked)
            Dim dtShift As New DataTable("Shift")
            dtShift.Columns.Add("ShiftId")
            dtShift.Columns.Add("ShiftDescrip")
            ' Delete from RadListBox items that are already selected

            Dim selectedItems As String
            selectedItems = GetCurrentlySelectedItems("Shift")
            Dim selectedItemIds() As String

            Dim flag As Boolean = False
            If Not String.IsNullOrEmpty(selectedItems) Then
                selectedItemIds = selectedItems.Split(","c)
                flag = True
            End If
            Dim val As String
            For Each datarow As USP_getShift_byCampusDate_andTermCourseInstructorResult In result
                If (flag) Then
                    'do not add if that item is already selected
                    val = System.Array.Find(selectedItemIds, Function(x) (x.Equals(datarow.ShiftId.ToString())))
                    If String.IsNullOrEmpty(val) Then
                        dtShift.Rows.Add(New Object() {datarow.ShiftId, datarow.ShiftDescrip})
                    End If
                Else
                    dtShift.Rows.Add(New Object() {datarow.ShiftId, datarow.ShiftDescrip})
                End If
            Next
            RadListBoxShift.DataTextField = "ShiftDescrip"
            RadListBoxShift.DataValueField = "ShiftId"
            RadListBoxShift.DataSource = dtShift
            RadListBoxShift.DataBind()
            ' Delete from RadListBoxShift2 items do not belong to selected filters above
            Dim RadListBoxItemList As New List(Of RadListBoxItem)
            For Each item As RadListBoxItem In RadListBoxShift2.Items
                RadListBoxItemList.Add(item)
            Next
            For Each item As RadListBoxItem In RadListBoxItemList
                Dim query = (From re In result Where re.ShiftId = Guid.Parse(item.Value))
                If Not query.Any() Then
                    RadListBoxShift2.Items.Remove(item)
                End If
            Next
            ListBoxCounts("Shift")
        End If
    End Sub
    Public Sub FillTerms()
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            'If RadWeekEndingDate.SelectedDate Is Nothing Then
            'Else
            Dim result As IEnumerable(Of USP_getTerms_byCampusDateResult)
            Dim dA As New TermDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            Dim weekEndingDate As Date = DateTime.Now.Date
            If RadWeekEndingDate.SelectedDate IsNot Nothing Then
                weekEndingDate = RadWeekEndingDate.SelectedDate
            End If
            result = dA.USP_getTerms_byCampusDate(selectedCampus, weekEndingDate, chkInActiveTerms.Checked)

            Dim dtTerms As New DataTable("terms")
            dtTerms.Columns.Add("TermId")
            dtTerms.Columns.Add("TermDescrip")
            ' Delete from RadListBox items that are already selected

            Dim selectedItems As String
            selectedItems = GetCurrentlySelectedItems("Term")
            Dim selectedTermIds() As String

            Dim flag As Boolean = False
            If Not String.IsNullOrEmpty(selectedItems) Then
                selectedTermIds = selectedItems.Split(","c)
                flag = True
            End If
            Dim val As String
            For Each datarow As USP_getTerms_byCampusDateResult In result
                If (flag) Then
                    'do not add if that item is already selected
                    val = System.Array.Find(selectedTermIds, Function(x) (x.Equals(datarow.TermId.ToString())))
                    If String.IsNullOrEmpty(val) Then
                        dtTerms.Rows.Add(New Object() {datarow.TermId, datarow.TermDescrip})
                    End If
                Else
                    dtTerms.Rows.Add(New Object() {datarow.TermId, datarow.TermDescrip})
                End If
            Next

            RadListBoxTerms.DataTextField = "TermDescrip"
            RadListBoxTerms.DataValueField = "TermId"
            RadListBoxTerms.DataSource = dtTerms
            RadListBoxTerms.DataBind()

            'End If
            ListBoxCounts("Term")
            'End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillEnrollmentStatus()
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            Dim result As IEnumerable(Of USP_getEnrollment_byCampusResult)
            Dim dA As New StudentEnrollmentDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            result = dA.USP_getEnrollment_byCampus(selectedCampus, cbkInactiveEnrollmentStatus.Checked)
            Dim dtEnrollmentStatus As New DataTable("EnrollmentStatus")
            dtEnrollmentStatus.Columns.Add("statusCodeId")
            dtEnrollmentStatus.Columns.Add("StatusCodeDescrip")
            ' Delete from RadListBox items that are already selected

            Dim selectedItems As String
            selectedItems = GetCurrentlySelectedItems("EnrollmentStatus")
            Dim selectedItemIds() As String

            Dim flag As Boolean = False
            If Not String.IsNullOrEmpty(selectedItems) Then
                selectedItemIds = selectedItems.Split(","c)
                flag = True
            End If
            Dim val As String
            For Each datarow As USP_getEnrollment_byCampusResult In result
                If (flag) Then
                    'do not add if that item is already selected
                    val = System.Array.Find(selectedItemIds, Function(x) (x.Equals(datarow.StatusCodeId.ToString())))
                    If String.IsNullOrEmpty(val) Then
                        dtEnrollmentStatus.Rows.Add(New Object() {datarow.StatusCodeId, datarow.StatusCodeDescrip})
                    End If
                Else
                    dtEnrollmentStatus.Rows.Add(New Object() {datarow.StatusCodeId, datarow.StatusCodeDescrip})
                End If
            Next
            RadListBoxEnrollmentStatus.DataTextField = "StatusCodeDescrip"
            RadListBoxEnrollmentStatus.DataValueField = "statusCodeId"
            RadListBoxEnrollmentStatus.DataSource = dtEnrollmentStatus
            RadListBoxEnrollmentStatus.DataBind()
            ListBoxCounts("EnrollmentStatus")
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        SetProperties()
        If Not Page.IsPostBack Then
            FillCampusControl()
            RadComboCampus.SelectedValue = HttpContext.Current.Request.Params("cmpid").ToString
            'todo change the date
            RadWeekEndingDate.SelectedDate = DateTime.Now.Date
            FillTerms()
            FillEnrollmentStatus()
        End If
    End Sub
    Protected Sub CampusComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        ResetAll("Campus")
    End Sub
    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)

        Try
            Dim lbox As RadListBox = DirectCast(sender, RadListBox)
            Dim myItem As IList(Of RadListBoxItem) = e.Items
            Dim ctrl As String
            If DirectCast(myItem, Telerik.Web.UI.RadListBoxItem())(0).ClientID.Contains("Terms") Then
                ctrl = "Term"
            ElseIf DirectCast(myItem, Telerik.Web.UI.RadListBoxItem())(0).ClientID.Contains("Course") Then
                ctrl = "Course"
            ElseIf DirectCast(myItem, Telerik.Web.UI.RadListBoxItem())(0).ClientID.Contains("Instructor") Then
                ctrl = "Instructor"
            ElseIf DirectCast(myItem, Telerik.Web.UI.RadListBoxItem())(0).ClientID.Contains("Shift") Then
                ctrl = "Shift"
            ElseIf DirectCast(myItem, Telerik.Web.UI.RadListBoxItem())(0).ClientID.Contains("Section") Then
                ctrl = "Section"
            ElseIf DirectCast(myItem, Telerik.Web.UI.RadListBoxItem())(0).ClientID.Contains("Enrollment") Then
                ctrl = "EnrollmentStatus"
            End If
            Select Case ctrl
                Case "Term"
                    FillCourseByTerms()
                    ListBoxCounts("Term")
                Case "Course"
                    FillInstructorByCourseAndTerms()
                    ListBoxCounts("Course")
                Case "Instructor"
                    FillShiftByInstructorCourseAndTerms()
                    ListBoxCounts("Instructor")
                Case "Shift"
                    FillSectionByShiftInstructorCourseAndTerms()
                    ListBoxCounts("Shift")
                Case "Section"
                    ListBoxCounts("Section")
                Case "EnrollmentStatus"
                    ListBoxCounts("EnrollmentStatus")
            End Select
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try

    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim chkBox As CheckBox = DirectCast(sender, CheckBox)
        If chkBox.ID.Contains("Terms") Then
            FillTerms()
        ElseIf chkBox.ID.Contains("Course") Then
            FillCourseByTerms()
        ElseIf chkBox.ID.Contains("Instructor") Then
            FillInstructorByCourseAndTerms()
        ElseIf chkBox.ID.Contains("Shift") Then
            FillShiftByInstructorCourseAndTerms()
        ElseIf chkBox.ID.Contains("Enrollment") Then
            FillEnrollmentStatus()
        End If
    End Sub
    Protected Sub RadWeekEndingDate_OnSelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs)
        ResetAll("date")
    End Sub
End Class
