﻿Option Strict On

Imports FluentNHibernate.Conventions.AcceptanceCriteria
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Partial Class ParamCustomOptionTranscriptReport
    Inherits UserControl
    Implements ICustomControl
#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    Private Const strDisclaimerText As String = "This Transcript is official only when signed by the school official and embossed with the school’s raised seal.  Federal law prohibits the release of this document to a person or institution without the written consent of the student."
    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        SetProperties()
        If Not Page.IsPostBack Then
            lblDisclaimerText.Text = strDisclaimerText
            'WorkUnitsPanel.Visible = True
            'For Each cbk As ListItem In cbList___Options.Items
            '    cbk.Selected = True
            'Next
        End If
    End Sub
    Protected Sub cblHeaderOptions_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cblHeaderOptions.SelectedIndexChanged
        EnableDisableSchoolInformationOptions()
        EnableDisableSchoolOfficialSignatureLine()
    End Sub
    Protected Sub rblShowTermCourses_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rblShowTermCourses.SelectedIndexChanged
        EnableDisableTermsModulesOptions()
        SelectDefaultvalueTermsModulesOptions()
        EnableDisableCourseComponentsOptions()
        SelectDefaultvalueCourseComponentsOptions()
        EnableDisableCourseCategoriesOptions()
        SelectDefaultvalueCourseCategoriesOptions()
    End Sub


#End Region
#Region "Methods"
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Try
            Dim DisplayTree As New RadTreeView
            Dim ReportOptionsNode As New RadTreeNode
            DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
            ReportOptionsNode.Text = "Custom Options"
            ReportOptionsNode.Value = CStr(ItemDetail.DetailId)
            ReportOptionsNode.CssClass = "TreeParentNode"

            'Header Options

            Dim HeaderOptionsNode As New RadTreeNode
            HeaderOptionsNode.Text = "Header Option"
            HeaderOptionsNode.CssClass = "TreeParentNode"
            For Each item As ListItem In cblHeaderOptions.Items
                Dim SelectedHeaderOptionNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedHeaderOptionNode.Text = item.Text & " - Yes"
                Else
                    SelectedHeaderOptionNode.Text = item.Text & " - No"
                End If
                HeaderOptionsNode.Nodes.Add(SelectedHeaderOptionNode)
            Next
            ReportOptionsNode.Nodes.Add(HeaderOptionsNode)

            'School Information Options
            Dim SchoolInformationOptionsNode As New RadTreeNode
            SchoolInformationOptionsNode.Text = "School Information Options"
            SchoolInformationOptionsNode.CssClass = "TreeParentNode"
            For Each item As ListItem In cblSchoolInformationOptions.Items
                Dim SelectedSchoolInformationOptionsNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedSchoolInformationOptionsNode.Text = item.Text & " - Yes"
                Else
                    SelectedSchoolInformationOptionsNode.Text = item.Text & " - No"
                End If
                SchoolInformationOptionsNode.Nodes.Add(SelectedSchoolInformationOptionsNode)
            Next
            ReportOptionsNode.Nodes.Add(SchoolInformationOptionsNode)

            'School Logo Options

            Dim SchoolLogoOptionsNode As New RadTreeNode
            Dim SelectedSchoolLogoOptionsNode As New RadTreeNode
            SchoolLogoOptionsNode.Text = "Logo Positon"
            SchoolLogoOptionsNode.CssClass = "TreeParentNode"
            SelectedSchoolLogoOptionsNode.Text = radSchoolLogoPosition.SelectedItem.Text
            SelectedSchoolLogoOptionsNode.Value = radSchoolLogoPosition.SelectedItem.Value
            'SelectedSchoolLogoOptionsNode.Value = radSchoolLogoPosition.SelectedItem.Index.ToString()
            SchoolLogoOptionsNode.Nodes.Add(SelectedSchoolLogoOptionsNode)
            ReportOptionsNode.Nodes.Add(SchoolLogoOptionsNode)

            'Student Options

            Dim StudentOptionsNode As New RadTreeNode
            StudentOptionsNode.Text = "Student Option"
            StudentOptionsNode.CssClass = "TreeParentNode"
            For Each item As ListItem In cblStudentOptions.Items
                Dim SelectedStudentOptionNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedStudentOptionNode.Text = item.Text & " - Yes"
                Else
                    SelectedStudentOptionNode.Text = item.Text & " - No"
                End If
                StudentOptionsNode.Nodes.Add(SelectedStudentOptionNode)
            Next
            ReportOptionsNode.Nodes.Add(StudentOptionsNode)

            'Program Options

            Dim ProgramOptionsNode As New RadTreeNode
            ProgramOptionsNode.Text = "Program Option"
            ProgramOptionsNode.CssClass = "TreeParentNode"
            For Each item As ListItem In cblProgramOptions.Items
                Dim SelectedProgramOptionNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedProgramOptionNode.Text = item.Text & " - Yes"
                Else
                    SelectedProgramOptionNode.Text = item.Text & " - No"
                End If
                ProgramOptionsNode.Nodes.Add(SelectedProgramOptionNode)
            Next
            ReportOptionsNode.Nodes.Add(ProgramOptionsNode)

            'Courses Options (Terms / Modules - Courses Only  - CourseCategories)  

            Dim CoursesOptionsNode As New RadTreeNode
            CoursesOptionsNode.Text = "Courses Option"
            CoursesOptionsNode.CssClass = "TreeParentNode"
            ReportOptionsNode.Nodes.Add(CoursesOptionsNode)

            'Radio Button from (Terms / Modules - Courses Only  - CourseCategories) 
            Dim ShowTermCourses As ListItem = rblShowTermCourses.SelectedItem
            Dim ShowTermCoursesNode As New RadTreeNode
            ShowTermCoursesNode.Text = ShowTermCourses.Text
            CoursesOptionsNode.Nodes.Add(ShowTermCoursesNode)
            ReportOptionsNode.Nodes.Add(CoursesOptionsNode)

            ''Radio Button Show Work Units when  Courses Only radion button was selected
            'Dim Selected As ListItem = rblShowWorkUnits.SelectedItem
            'Dim ShowWorkUnitsNode As New RadTreeNode
            'ShowWorkUnitsNode.Text = Selected.Text
            'CoursesOptionsNode.Nodes.Add(ShowWorkUnitsNode)
            'ReportOptionsNode.Nodes.Add(CoursesOptionsNode)
            '
            'Check Box Terms Modules Options when ShowTerms/Modules radio button was selected
            For Each item As ListItem In cblTermsModulesOptions.Items
                Dim SelectedTermsModulesOptionsNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedTermsModulesOptionsNode.Text = item.Text & " - Yes"
                Else
                    SelectedTermsModulesOptionsNode.Text = item.Text & " - No"
                End If
                CoursesOptionsNode.Nodes.Add(SelectedTermsModulesOptionsNode)
            Next
            ReportOptionsNode.Nodes.Add(CoursesOptionsNode)

            'Check Box CourseCategories Options when ShowCoursesOnly radio button was selected
            For Each item As ListItem In cblShowCourseComponents.Items
                Dim SelectedShowCourseComponentsNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedShowCourseComponentsNode.Text = item.Text & " - Yes"
                Else
                    SelectedShowCourseComponentsNode.Text = item.Text & " - No"
                End If
                CoursesOptionsNode.Nodes.Add(SelectedShowCourseComponentsNode)
            Next
            ReportOptionsNode.Nodes.Add(CoursesOptionsNode)

            'Check Box CourseCategories Options when CourseCategories radio button was selected
            For Each item As ListItem In cblCourseCategoriesOptions.Items
                Dim SelectedCourseCategoriesOptionNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedCourseCategoriesOptionNode.Text = item.Text & " - Yes"
                Else
                    SelectedCourseCategoriesOptionNode.Text = item.Text & " - No"
                End If
                CoursesOptionsNode.Nodes.Add(SelectedCourseCategoriesOptionNode)
            Next
            ReportOptionsNode.Nodes.Add(CoursesOptionsNode)

            'Footer Options

            Dim FooterOptionsNode As New RadTreeNode
            FooterOptionsNode.Text = "Footer Option"
            FooterOptionsNode.CssClass = "TreeParentNode"
            For Each item As ListItem In cblFooterOptions.Items
                Dim SelectedFooterOptionNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedFooterOptionNode.Text = item.Text & " - Yes"
                Else
                    SelectedFooterOptionNode.Text = item.Text & " - No"
                End If
                FooterOptionsNode.Nodes.Add(SelectedFooterOptionNode)
            Next
            ReportOptionsNode.Nodes.Add(FooterOptionsNode)

            'Disclaimer Options

            Dim DisclaimerOptionsNode As New RadTreeNode
            Dim SelectedDisclaimerOptionsNode As New RadTreeNode
            DisclaimerOptionsNode.Text = "DisclaimerText"
            DisclaimerOptionsNode.CssClass = "TreeParentNode"
            SelectedDisclaimerOptionsNode.Text = radTextBoxDisclaimer.Text
            DisclaimerOptionsNode.Nodes.Add(SelectedDisclaimerOptionsNode)
            ReportOptionsNode.Nodes.Add(DisclaimerOptionsNode)

            DisplayTree.Nodes.Add(ReportOptionsNode)
            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            UserSettings.ItemName = ItemDetail.ItemName
            UserSettings.ItemId = ItemDetail.ItemId
            UserSettings.DetailId = ItemDetail.DetailId
            UserSettings.FriendlyName = ItemDetail.CaptionOverride


            Dim ctrlSettingOptions As ControlSettingInfo
            Dim ctrlValuesOptions As List(Of ControlValueInfo)
            Dim objControlValueInfo As ControlValueInfo
            Dim ValueInfo As ControlValueInfo

            'Header Options

            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = cblHeaderOptions.ID
            For Each item As ListItem In cblHeaderOptions.Items
                ValueInfo = New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesOptions.Add(ValueInfo)
            Next
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            'School Information Options

            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = cblSchoolInformationOptions.ID
            For Each item As ListItem In cblSchoolInformationOptions.Items
                ValueInfo = New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesOptions.Add(ValueInfo)
            Next
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            'School Logo Options

            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            objControlValueInfo = New ControlValueInfo

            ctrlSettingOptions.ControlName = radSchoolLogoPosition.ID
            objControlValueInfo.DisplayText = radSchoolLogoPosition.SelectedItem.Text
            objControlValueInfo.KeyData = radSchoolLogoPosition.SelectedItem.Value
            'objControlValueInfo.KeyData = radSchoolLogoPosition.SelectedItem.Index.ToString()
            ctrlValuesOptions.Add(objControlValueInfo)
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)


            'Student Options

            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = cblStudentOptions.ID
            For Each item As ListItem In cblStudentOptions.Items
                ValueInfo = New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesOptions.Add(ValueInfo)
            Next
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            'Program Options

            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = cblProgramOptions.ID
            For Each item As ListItem In cblProgramOptions.Items
                ValueInfo = New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesOptions.Add(ValueInfo)
            Next
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            'Courses Options (Terms / Modules - Courses Only  - CourseCategories) 

            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = rblShowTermCourses.ID
            objControlValueInfo = New ControlValueInfo

            'Radio Button from (Terms / Modules - Courses Only  - CourseCategories) 
            Dim ShowTermCourses As ListItem = rblShowTermCourses.SelectedItem
            objControlValueInfo.DisplayText = ShowTermCourses.Text
            objControlValueInfo.KeyData = ShowTermCourses.Value
            ctrlValuesOptions.Add(objControlValueInfo)
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            '' code functionality when the option instead checkbox it is a radio button
            '
            ''Radio Button Show Work Units when  Courses Only radion button was selected
            'ctrlSettingOptions = New ControlSettingInfo
            'ctrlValuesOptions = New List(Of ControlValueInfo)
            'ctrlSettingOptions.ControlName = rblShowWorkUnits.ID
            'objControlValueInfo = New ControlValueInfo
            'Dim Selected As ListItem = rblShowWorkUnits.SelectedItem
            'objControlValueInfo.DisplayText = Selected.Text
            'objControlValueInfo.KeyData = Selected.Value
            'ctrlValuesOptions.Add(objControlValueInfo)
            'ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            'SettingsCollection.Add(ctrlSettingOptions)

            'Check Box Terms/Modules Options when Show Terms/Modulesradio button was selected
            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = cblTermsModulesOptions.ID
            For Each item As ListItem In cblTermsModulesOptions.Items
                ValueInfo = New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesOptions.Add(ValueInfo)
            Next
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            'Check Box CourseComponents Options when Course only radio button was selected
            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = cblShowCourseComponents.ID
            For Each item As ListItem In cblShowCourseComponents.Items
                ValueInfo = New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesOptions.Add(ValueInfo)
            Next
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)


            'Check Box CourseCategories Options when CourseCategories radio button was selected
            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = cblCourseCategoriesOptions.ID
            For Each item As ListItem In cblCourseCategoriesOptions.Items
                ValueInfo = New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesOptions.Add(ValueInfo)
            Next
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            'Footer Options

            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            ctrlSettingOptions.ControlName = cblFooterOptions.ID
            For Each item As ListItem In cblFooterOptions.Items
                ValueInfo = New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesOptions.Add(ValueInfo)
            Next
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            'Custom Disclaimer

            ctrlSettingOptions = New ControlSettingInfo
            ctrlValuesOptions = New List(Of ControlValueInfo)
            objControlValueInfo = New ControlValueInfo
            ctrlSettingOptions.ControlName = radTextBoxDisclaimer.ID
            objControlValueInfo.DisplayText = radTextBoxDisclaimer.Text
            objControlValueInfo.KeyData = "DisclaimerText"
            ctrlValuesOptions.Add(objControlValueInfo)
            ctrlSettingOptions.ControlValueCollection = ctrlValuesOptions
            SettingsCollection.Add(ctrlSettingOptions)

            UserSettings.ControlSettingsCollection = SettingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try

        Return UserSettings

    End Function
    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each RadioItem As ListItem In RadioButList.Items
                                RadioItem.Selected = False
                            Next
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim rbtList As ListItem = RadioButList.Items.FindByValue(ItemValue.KeyData)
                                If Not rbtList Is Nothing Then
                                    rbtList.Selected = True
                                End If
                            Next
                        ElseIf TypeOf ctrl Is CheckBoxList Then
                            Dim ChkBoxList As CheckBoxList = DirectCast(ctrl, CheckBoxList)
                            For Each Chk As ListItem In ChkBoxList.Items
                                Chk.Selected = False
                            Next
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim chkitem As ListItem = ChkBoxList.Items.FindByText(ItemValue.DisplayText)
                                If Not chkitem Is Nothing Then
                                    chkitem.Selected = CBool(ItemValue.KeyData)
                                End If
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim radComboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim index As Integer = radComboBox.FindItemIndexByValue(ItemValue.KeyData.ToString())
                                radComboBox.SelectedIndex = index

                                'radComboBox.SelectedItem.Value = ItemValue.KeyData.ToString()
                                'radComboBox.SelectedItem.Text = ItemValue.DisplayText.ToString()
                            Next
                        ElseIf TypeOf ctrl Is RadTextBox Then
                            Dim radTextBox As RadTextBox = DirectCast(ctrl, RadTextBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                radTextBox.Text = ItemValue.DisplayText.ToString()
                            Next
                        Else
                            Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
                EnableDisableSchoolInformationOptions()
                EnableDisableTermsModulesOptions()
                EnableDisableCourseComponentsOptions()
                EnableDisableCourseCategoriesOptions()
                EnableDisableSchoolOfficialSignatureLine()
                'EnableDisableShowCoursesSummary()
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub EnableDisableSchoolInformationOptions()
        Try
            If cblHeaderOptions.Items.FindByText("No logo or School Information").Selected = True Then
                cblSchoolInformationOptions.Items.FindByText("Show School Name").Selected = False
                cblSchoolInformationOptions.Items.FindByText("Show School Name").Enabled = False
                cblSchoolInformationOptions.Items.FindByText("Show Campus Address").Selected = False
                cblSchoolInformationOptions.Items.FindByText("Show Campus Address").Enabled = False
                cblSchoolInformationOptions.Items.FindByText("Show Phone").Selected = False
                cblSchoolInformationOptions.Items.FindByText("Show Phone").Enabled = False
                cblSchoolInformationOptions.Items.FindByText("Show Fax").Selected = False
                cblSchoolInformationOptions.Items.FindByText("Show Fax").Enabled = False
                cblSchoolInformationOptions.Items.FindByText("Show Web Site").Selected = False
                cblSchoolInformationOptions.Items.FindByText("Show Web Site").Enabled = False
                pnlSchoolInformationOptions.Enabled = False

                pnlLogoPosition.Enabled = False
            Else
                cblSchoolInformationOptions.Items.FindByText("Show School Name").Selected = True
                cblSchoolInformationOptions.Items.FindByText("Show School Name").Enabled = True
                cblSchoolInformationOptions.Items.FindByText("Show Campus Address").Selected = True
                cblSchoolInformationOptions.Items.FindByText("Show Campus Address").Enabled = True
                cblSchoolInformationOptions.Items.FindByText("Show Phone").Selected = True
                cblSchoolInformationOptions.Items.FindByText("Show Phone").Enabled = True
                cblSchoolInformationOptions.Items.FindByText("Show Fax").Selected = True
                cblSchoolInformationOptions.Items.FindByText("Show Fax").Enabled = True
                cblSchoolInformationOptions.Items.FindByText("Show Web Site").Selected = True
                cblSchoolInformationOptions.Items.FindByText("Show Web Site").Enabled = True
                pnlSchoolInformationOptions.Enabled = True

                pnlLogoPosition.Enabled = True
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub EnableDisableTermsModulesOptions()
        If rblShowTermCourses.Items.FindByValue("ShowTerms/Modules").Selected = False Then
            pnlTermsModulesOptions.Enabled = False
        Else
            pnlTermsModulesOptions.Enabled = True
        End If
    End Sub
    Private Sub SelectDefaultvalueTermsModulesOptions()
        If rblShowTermCourses.Items.FindByValue("ShowTerms/Modules").Selected = False Then
            cblTermsModulesOptions.Items.FindByValue("Show Term Description").Selected = False
        Else
            cblTermsModulesOptions.Items.FindByValue("Show Term Description").Selected = False
        End If
    End Sub
    Private Sub EnableDisableCourseComponentsOptions()
        'If rblShowTermCourses.Items.FindByValue("ShowCoursesOnly").Selected = False Then
        '    pnlShowUnits.Visible = False
        '    pnlShowUnits.Enabled = False
        'Else
        '    pnlShowUnits.Visible = True
        '    pnlShowUnits.Enabled = True
        'End If
        If rblShowTermCourses.Items.FindByValue("ShowCoursesOnly").Selected = False Then
            'pnlShowUnits.Visible = False
            pnlShowCouseComponents.Enabled = False
        Else
            'pnlShowUnits.Visible = True
            pnlShowCouseComponents.Enabled = True
        End If
    End Sub
    Private Sub SelectDefaultvalueCourseComponentsOptions()
        'If rblShowTermCourses.Items.FindByValue("ShowCoursesOnly").Selected = False Then
        '    rblShowWorkUnits.Items.FindByValue("No Show").Selected = False
        '    rblShowWorkUnits.Items.FindByValue("Show").Selected = False
        'Else
        '    rblShowWorkUnits.Items.FindByValue("No Show").Selected = True
        '    rblShowWorkUnits.Items.FindByValue("Show").Selected = False
        'End If
        If rblShowTermCourses.Items.FindByValue("ShowCoursesOnly").Selected = False Then
            cblShowCourseComponents.Items.FindByValue("Show course components").Selected = False
        Else
            cblShowCourseComponents.Items.FindByValue("Show course components").Selected = False
        End If
    End Sub
    Private Sub EnableDisableCourseCategoriesOptions()
        If rblShowTermCourses.Items.FindByValue("ShowCourseCategories").Selected = False Then
            cblCourseCategoriesOptions.Items.FindByText("Show Date Issued").Enabled = False
            cblCourseCategoriesOptions.Items.FindByText("Show Credits Column").Enabled = False
            cblCourseCategoriesOptions.Items.FindByText("Show Hours Column").Enabled = False
            'pnlShowCourseCategoriesOptions.Visible = False
            pnlShowCourseCategoriesOptions.Enabled = False
        Else
            cblCourseCategoriesOptions.Items.FindByText("Show Date Issued").Enabled = True
            cblCourseCategoriesOptions.Items.FindByText("Show Credits Column").Enabled = True
            cblCourseCategoriesOptions.Items.FindByText("Show Hours Column").Enabled = True
            'pnlShowCourseCategoriesOptions.Visible = True
            pnlShowCourseCategoriesOptions.Enabled = True
        End If
    End Sub
    Private Sub SelectDefaultvalueCourseCategoriesOptions()
        Try
            If rblShowTermCourses.Items.FindByValue("ShowCourseCategories").Selected = False Then
                cblCourseCategoriesOptions.Items.FindByText("Show Date Issued").Selected = False
                cblCourseCategoriesOptions.Items.FindByText("Show Credits Column").Selected = False
                cblCourseCategoriesOptions.Items.FindByText("Show Hours Column").Selected = False
            Else
                cblCourseCategoriesOptions.Items.FindByText("Show Date Issued").Selected = True
                cblCourseCategoriesOptions.Items.FindByText("Show Credits Column").Selected = True
                cblCourseCategoriesOptions.Items.FindByText("Show Hours Column").Selected = True
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub EnableDisableSchoolOfficialSignatureLine()
        Try
            If cblHeaderOptions.Items.FindByText("Official Transcript").Selected = True Then
                cblFooterOptions.Items.FindByText("Show School Official Signature Line").Selected = True
                cblFooterOptions.Items.FindByText("Show School Official Signature Line").Enabled = True
            Else
                cblFooterOptions.Items.FindByText("Show School Official Signature Line").Selected = False
                cblFooterOptions.Items.FindByText("Show School Official Signature Line").Enabled = True
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    'Private Sub EnableDisableShowCoursesSummary()
    '    Try
    '        If rblShowTermCourses.Items.FindByValue("ShowCoursesOnly").Selected = True Then
    '            cblFooterOptions.Items.FindByText("Show Courses Summary").Selected = False
    '            cblFooterOptions.Items.FindByText("Show Courses Summary").Enabled = False
    '        Else
    '            cblFooterOptions.Items.FindByText("Show Courses Summary").Selected = True
    '            cblFooterOptions.Items.FindByText("Show Courses Summary").Enabled = True
    '        End If
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Sub
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Public Sub ChangeDisclaimer()

    End Sub
#End Region




End Class
