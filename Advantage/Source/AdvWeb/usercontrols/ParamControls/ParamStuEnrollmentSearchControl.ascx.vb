﻿Option Strict On
Imports Telerik.Web.UI
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports System
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports FAME.Parameters.Collections
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Parameters.Info
Imports FAME.Advantage.Reporting.Info
Imports System.Windows.Forms.VisualStyles

Partial Class ParamStuEnrollmentSearchControl
    Inherits UI.UserControl
    Implements ISearchControl

    Private UserId As Guid
    Private searchRequest As String

#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption, _Caption2, _Caption3 As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _PostbackCtrlName As String
    Private _Mode As ISearchControl.ModeType
    Private _ClientSearchFunction As String
    Private _SavedSettings As ParamItemUserSettingsInfo
    Private _SendData As Boolean
    Private studentsTranscripsInHoldMessage As String = "Students who are on a Transcript Hold will not appear for Transcript printing until the hold is released."

    Public Property ItemDetail() As ParameterDetailItemInfo Implements ISearchControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property

    Public Property DAClass() As String Implements ISearchControl.DAClass
        Get
            Return _DAClass
        End Get
        Set(ByVal Value As String)
            _DAClass = Value
        End Set
    End Property

    Public Property DAMethod() As String Implements ISearchControl.DAMethod
        Get
            Return _DAMethod
        End Get
        Set(ByVal Value As String)
            _DAMethod = Value
        End Set
    End Property

    Public Property BindingTextField() As String Implements ISearchControl.BindingTextField
        Get
            Return _BindingTextField
        End Get
        Set(ByVal Value As String)
            _BindingTextField = Value
        End Set
    End Property

    Public Property BindingValueField() As String Implements ISearchControl.BindingValueField
        Get
            Return _BindingValueField
        End Get
        Set(ByVal Value As String)
            _BindingValueField = Value
        End Set
    End Property

    Public Property Caption() As String Implements ISearchControl.Caption
        Get
            Return _Caption
        End Get
        Set(ByVal Value As String)
            _Caption = Value
        End Set
    End Property

    Public Property Caption2() As String Implements ISearchControl.Caption2
        Get
            Return _Caption2
        End Get
        Set(ByVal Value As String)
            _Caption2 = Value
        End Set
    End Property

    Public Property Caption3() As String Implements ISearchControl.Caption3
        Get
            Return _Caption3
        End Get
        Set(ByVal Value As String)
            _Caption3 = Value
        End Set
    End Property

    Public Property AssemblyFilePathDA() As String Implements ISearchControl.AssemblyFilePathDA
        Get
            Return _AssemblyFilePathDA
        End Get
        Set(ByVal Value As String)
            _AssemblyFilePathDA = Value
        End Set
    End Property

    Private Property AssemblyDA() As Assembly Implements ISearchControl.AssemblyDA
        Get
            Return _AssemblyDA

        End Get
        Set(ByVal Value As Assembly)
            _AssemblyDA = Value

        End Set
    End Property

    Public Property SqlConn() As String Implements ISearchControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property

    Private Property PostbackCtrlName() As String
        Get
            Return _PostbackCtrlName
        End Get
        Set(ByVal Value As String)
            _PostbackCtrlName = Value
        End Set
    End Property

    Public Property ClientSearchFunction() As String
        Get
            Return _ClientSearchFunction
        End Get
        Set(ByVal Value As String)
            _ClientSearchFunction = Value
        End Set
    End Property

    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ISearchControl.SavedSettings
        Get
            Return _SavedSettings
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSettings = value
        End Set
    End Property

    Public Property Mode() As ISearchControl.ModeType Implements ISearchControl.Mode
        Get
            Return _Mode
        End Get
        Set(ByVal value As ISearchControl.ModeType)
            _Mode = value
        End Set
    End Property

    Public Property SendData() As Boolean Implements ISearchControl.SendData
        Get
            Return _SendData
        End Get
        Set(ByVal Value As Boolean)
            _SendData = Value
        End Set
    End Property

    private Property StuEnrollmentSearchStore() As List(Of StudentEnrollmentInfoSearch)
        Get
            Try
                Dim studentEnrollmentInfoSearchList As List(Of StudentEnrollmentInfoSearch)

                If Session("StuEnrollmentSearch_VB") Is Nothing And Not Page.IsPostBack Then

                    studentEnrollmentInfoSearchList = GetStuEnrollments(GetAllDataFilters("Filtered", "SearchStore"), "S")

                    Session("StuEnrollmentSearch_VB") = studentEnrollmentInfoSearchList
                Else
                    studentEnrollmentInfoSearchList = DirectCast(Session("StuEnrollmentSearch_VB"), List(Of StudentEnrollmentInfoSearch))
                End If

                Return studentEnrollmentInfoSearchList
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw ex
            End Try
        End Get
        Set(ByVal value As List(Of StudentEnrollmentInfoSearch))
            If (value Is Nothing) Then
                Session("StuEnrollmentSearch_VB") = New List(Of StudentEnrollmentInfoSearch)
            Else
                Session("StuEnrollmentSearch_VB") = value
            End If
        End Set
    End Property

    private Property StuEnrollmentSelectedStore() As List(Of StudentEnrollmentInfoSearch)
        Get
            Try
                Dim studentEnrollmentInfoSearchList As List(Of StudentEnrollmentInfoSearch)

                If Session("StuEnrollmentSelected_VB") Is Nothing Then
                    studentEnrollmentInfoSearchList = New List(Of StudentEnrollmentInfoSearch)()
                    Session("StuEnrollmentSelected_VB") = studentEnrollmentInfoSearchList
                Else
                    studentEnrollmentInfoSearchList = DirectCast(Session("StuEnrollmentSelected_VB"), List(Of StudentEnrollmentInfoSearch))
                End If

                Return studentEnrollmentInfoSearchList
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw ex
            End Try
        End Get
        Set(ByVal value As List(Of StudentEnrollmentInfoSearch))
            If (value Is Nothing) Then
                Session("StuEnrollmentSelected_VB") = New List(Of StudentEnrollmentInfoSearch)()
            Else
                Session("StuEnrollmentSelected_VB") = value
            End If
        End Set
    End Property

    private Property MRUStuEnrollmentSearchStore() As List(Of StudentEnrollmentInfoSearch)
        Get
            Try
                Dim studentEnrollmentInfoSearchList As List(Of StudentEnrollmentInfoSearch)
                If Session("MRUStuEnrollmentSearch_VB") Is Nothing And Not Page.IsPostBack Then

                    studentEnrollmentInfoSearchList = GetStuEnrollments(GetAllDataFilters("Filtered", "MRUSearchStore"), "M")

                    Session("MRUStuEnrollmentSearch_VB") = studentEnrollmentInfoSearchList
                Else
                    studentEnrollmentInfoSearchList = DirectCast(Session("MRUStuEnrollmentSearch_VB"), List(Of StudentEnrollmentInfoSearch))
                End If
                Return studentEnrollmentInfoSearchList
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw ex
            End Try
        End Get
        Set(ByVal value As List(Of StudentEnrollmentInfoSearch))
            If (value Is Nothing) Then
                Session("MRUStuEnrollmentSearch_VB") = New List(Of StudentEnrollmentInfoSearch)
            Else
                Session("MRUStuEnrollmentSearch_VB") = value
            End If

        End Set
    End Property

#End Region

#Region "Methods"

    Private Sub AddTBValueChangedScript()
        'Form the script
        Dim sb As New StringBuilder
        Dim TBValueChanged As String
        sb.Append("<script type=" & Convert.ToChar(34) & "text/javascript" & Convert.ToChar(34) & ">").AppendLine()
        sb.Append("function ")

        If (ItemDetail.CaptionOverride = "Student Enrollment Search") Then
            TBValueChanged = "TBValueChanged_13"
        Else
            TBValueChanged = "TBValueChanged_" + ItemDetail.DetailId.ToString()
        End If
        sb.Append(TBValueChanged)
        sb.Append("(sender, e) {").AppendLine()
        sb.Append("getEvent = event.keyCode;").AppendLine()
        sb.Append("if (getEvent == ""13"") {").AppendLine()
        sb.Append("var radTextBox = sender;").AppendLine()
        sb.Append("var inputRequest = radTextBox.get_textBoxValue();").AppendLine()
        sb.Append("var searchRequest = inputRequest.replace(/^\s+|\s+$/g, '');").AppendLine()
        sb.Append("if (searchRequest.length == 0) {").AppendLine()
        sb.Append(" } else {").AppendLine()
        sb.Append(" e.set_cancel(true); ").AppendLine()
        sb.Append(" radTextBox.raisePostBackEvent(); ").AppendLine()
        sb.Append("} } }").AppendLine()
        sb.Append("</script>").AppendLine()
        If (Not Page.ClientScript.IsClientScriptBlockRegistered(TBValueChanged)) Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), TBValueChanged, sb.ToString)
        End If

    End Sub

    Protected Sub RadGridSelectedStuEnrollments_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGridSelectedStuEnrollments.DataSource = StuEnrollmentSelectedStore()
        
    End Sub
    Protected Sub RadGridStuEnrollmentSearch_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGridStuEnrollmentSearch.DataSource = StuEnrollmentSearchStore()
    End Sub
    Protected Sub RadGridStuEnrollmentSearch_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs)
        If (e.CommandName = "RowClick") Then
        End If
    End Sub
    Protected Sub RadGridStuEnrollmentSearchMRU_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGridStuEnrollmentSearchMRU.DataSource = MRUStuEnrollmentSearchStore()
    End Sub
    Protected Sub RadGridStuEnrollmentSearchMRU_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs)
        If (e.CommandName = "RowClick") Then
        End If
    End Sub
    Public Sub ClearControls() Implements ISearchControl.ClearControls
        Dim ClearedData As New List(Of StudentEnrollmentInfoSearch)
        RadGridSelectedStuEnrollments.DataSource = ClearedData
    End Sub
    Public Sub LoadComboBoxes()
        GetProgramVersions()
        GetStatusCodes()
    End Sub
    Public Function GetStuEnrollments(ByVal filters As MasterDictionary, ByVal SearchType As String) As List(Of StudentEnrollmentInfoSearch)
      
        Dim Query As IQueryable(Of StudentEnrollmentSearch)
        Dim DA As New StudentEnrollmentDA(SqlConn)
        Dim results As List(Of StudentEnrollmentInfoSearch) = New List(Of StudentEnrollmentInfoSearch)

        If SearchType = "S" Then
            Dim Users(1) As String
            Users(0) = UserId.ToString
            Query = DA.GetStudentEnrollments(Users, filters)
        Else
            'for now we are fudging the user and campus id's
            Dim campusGUID As Guid
            Dim Users(1) As String
            Users(0) = UserId.ToString
            campusGUID = New Guid(Request.QueryString("cmpid"))
            Query = DA.GetMRUStudentEnrollments(Users, campusGUID, filters)
        End If

        For Each studentEnrollmentInfoSearch As StudentEnrollmentSearch In Query
            Dim stuenrollmentid As Guid = studentEnrollmentInfoSearch.StuEnrollId
            Dim enrollmentid As String = studentEnrollmentInfoSearch.EnrollmentId
            Dim studentid As Guid = studentEnrollmentInfoSearch.StudentId
            Dim searchdisplay As String = String.Empty
            Dim fullname As String = String.Empty
            Dim firstname As String = studentEnrollmentInfoSearch.FirstName.Trim
            Dim lastname As String = studentEnrollmentInfoSearch.LastName.Trim
            Dim ssn As String = FormatSSN(studentEnrollmentInfoSearch.SSN)
            Dim ssnsearch As String = (studentEnrollmentInfoSearch.SSN)
            Dim studentnumber As String = studentEnrollmentInfoSearch.StudentNumber
            Dim prgverid As Guid = studentEnrollmentInfoSearch.PrgVerId
            Dim prgverdescrip As String = studentEnrollmentInfoSearch.PrgVerDescrip
            Dim shiftid As Nullable(Of Guid) = studentEnrollmentInfoSearch.ShiftId
            Dim shiftdescrip As String = studentEnrollmentInfoSearch.ShiftDescrip
            Dim statuscodeid As Nullable(Of Guid) = studentEnrollmentInfoSearch.StatusCodeId
            Dim statuscodedescrip As String = studentEnrollmentInfoSearch.StatusCodeDescrip
            Dim campgrpid As Guid = studentEnrollmentInfoSearch.CampGrpId
            Dim leadgrpid As Nullable(Of Guid) = studentEnrollmentInfoSearch.LeadGrpId
            Dim sysstatusid As Integer = studentEnrollmentInfoSearch.SysStatusId
            Dim isTransHold As Boolean = studentEnrollmentInfoSearch.IsTransHold
            If (filters.Name = "TranscriptReportFilterSet_Filters") _
            And (isTransHold = True) Then
                ''Message about fullname Student IsTransHold
                'Dim message As String = fullname + " is on a Transcript Hold"
                'DisplayWarningInMessageBox(message)
                lblMessagePanel1.Text = studentsTranscripsInHoldMessage
                lblMessagePanel1.Visible = True
                lblMessagePanel2.Text = studentsTranscripsInHoldMessage
                lblMessagePanel2.Visible = True
            Else
                results.Add(New StudentEnrollmentInfoSearch(stuenrollmentid, enrollmentid, studentid, searchdisplay, fullname, firstname, lastname, ssn, ssnsearch, studentnumber, prgverid, prgverdescrip, shiftid, shiftdescrip, statuscodeid, statuscodedescrip, sysstatusid, campgrpid, leadgrpid, SearchType, isTransHold))
            End If
        Next
       
        Return results

    End Function

    Public Sub GetProgramVersions()
        Dim Query As IQueryable(Of arPrgVersion)
        Dim DA As New ProgramVersionDA(SqlConn)

        Query = DA.GetProgramVersionsByUser(UserId.ToString())

        RadComboBox1.DataTextField = "PrgVerDescrip"
        RadComboBox1.DataValueField = "PrgVerId"
        RadComboBox3.DataTextField = "PrgVerDescrip"
        RadComboBox3.DataValueField = "PrgVerId"
        RadComboBox1.Items.Insert(0, New RadComboBoxItem(" -- Select Program Version -- ", "0"))
        RadComboBox3.Items.Insert(0, New RadComboBoxItem(" -- Select Program Version -- ", "0"))

        For Each pv As arPrgVersion In Query
            If pv.syStatus.Status.ToString = "Active" Then
                RadComboBox1.Items.Add(New RadComboBoxItem(pv.PrgVerDescrip.ToString, pv.PrgVerId.ToString))
                RadComboBox3.Items.Add(New RadComboBoxItem(pv.PrgVerDescrip.ToString, pv.PrgVerId.ToString))
            End If
        Next
    End Sub

    Public Sub GetStatusCodes()
        Dim query As IQueryable(Of syStatusCode)
        Dim DA As New SystemStatusCodeDA(SqlConn)

        query = DA.GetEnrollmentSystemStatusCodesByUser(UserId.ToString())

        RadComboBox2.DataSource = query
        RadComboBox2.DataTextField = "StatusCodeDescrip"
        RadComboBox2.DataValueField = "StatusCodeId"
        RadComboBox2.DataBind()
        RadComboBox2.Items.Insert(0, New RadComboBoxItem(" -- Select Status -- ", "0"))

        RadComboBox4.DataSource = query
        RadComboBox4.DataTextField = "StatusCodeDescrip"
        RadComboBox4.DataValueField = "StatusCodeId"
        RadComboBox4.DataBind()
        RadComboBox4.Items.Insert(0, New RadComboBoxItem(" -- Select Status -- ", "0"))
    End Sub

    Public Sub Fill()
        Try
            RadGridStuEnrollmentSearch.DataSource = StuEnrollmentSearchStore
            RadGridStuEnrollmentSearch.DataBind()
            RadGridSelectedStuEnrollments.DataSource = StuEnrollmentSelectedStore
            RadGridSelectedStuEnrollments.DataBind()
            RadGridStuEnrollmentSearchMRU.DataSource = MRUStuEnrollmentSearchStore
            RadGridStuEnrollmentSearchMRU.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Public Function GetStudent(ByVal studentsToSearchIn As IEnumerable(Of StudentEnrollmentInfoSearch), ByVal StuEnrollmentId As Guid) As StudentEnrollmentInfoSearch
        For Each student As StudentEnrollmentInfoSearch In studentsToSearchIn
            If student.StuEnrollId = StuEnrollmentId Then
                Return student
            End If
        Next
        Return Nothing
    End Function

    Private Function FillValueDictionary() As ParamValueDictionary
        Dim ValueDictionary As New ParamValueDictionary
        Dim ModDictionary As New ModDictionary
        Dim CurrentlyTransferredValues As List(Of String) = GetCurrentlyTransferredValues()
        Dim SourceListBoxName As String = GetSourceListBox()
        Dim ItemsToAdd As New List(Of String)
        Dim ItemsToRemove As New List(Of String)
        Dim CurrentRadGridValues As New List(Of String)

        Dim count As Integer = RadGridSelectedStuEnrollments.PageCount
        Dim flag As Integer = 0
        For i As Integer = 0 To count - 1
            RadGridSelectedStuEnrollments.CurrentPageIndex = i
            RadGridSelectedStuEnrollments.Rebind()
            For Each item As GridDataItem In RadGridSelectedStuEnrollments.Items
                CurrentRadGridValues.Add(item.KeyValues.Substring(item.KeyValues.IndexOf(":") + 2, 36).ToString())
            Next
        Next

        If PostbackCtrlName.Contains(ItemDetail.ItemName) Then
            If SourceListBoxName.Contains("RadGridSelectedStuEnrollments") Then
                ItemsToRemove = CurrentlyTransferredValues
                For Each deleted As String In ItemsToRemove
                    CurrentRadGridValues.Remove(deleted)
                Next
            End If
            If SourceListBoxName.Contains("RadGridStuEnrollmentSearch") Then
                ItemsToAdd = CurrentlyTransferredValues
            End If
        End If
        Try
            Select Case ItemDetail.ValueProp
                Case ParameterDetailItemInfo.ItemValueType.String
                    Dim PValues As New ParamValueList(Of String)
                    For Each item As String In CurrentRadGridValues
                        PValues.Add(item)
                    Next
                    For Each str As String In ItemsToAdd
                        PValues.Add(str)
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Long
                    Dim PValues As New ParamValueList(Of Long)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Decimal
                    Dim PValues As New ParamValueList(Of Decimal)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Date
                    Dim PValues As New ParamValueList(Of Date)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsDate(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Integer
                    Dim PValues As New ParamValueList(Of Integer)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Boolean
                    Dim PValues As New ParamValueList(Of Boolean)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsBoolean(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Guid
                    Dim PValues As New ParamValueList(Of Guid)
                    For Each item As String In CurrentRadGridValues
                        PValues.Add(New Guid(item))
                    Next
                    For Each str As String In ItemsToAdd
                        If IsGuid(str) = True Then
                            PValues.Add(New Guid(str))
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case Else
                    Throw New Exception("Can not create parameter value collection: Unknown type")
            End Select
            If ModDictionary.Count > 0 Then
                ValueDictionary.Add(ModDictionary.Modifier.InList, ModDictionary)
            End If
            Return ValueDictionary
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Private Function IsGuid(ByVal value As String) As Boolean
        Dim myGuid As Guid = Nothing
        Try
            myGuid = New Guid(value)
            Return True
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    Private Function IsBoolean(ByVal value As String) As Boolean
        Dim myBoolean As Boolean
        Try
            myBoolean = CBool(value)
            Return True
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function GetCurrentlyTransferredValues() As List(Of String)
        Try
            Dim txtbox As RadTextBox = CType(Me.Page().Controls(0).FindControl("ContentMain2").FindControl("CurrentlyTransferredValuesPage"), RadTextBox)
            Dim TransferredValues As New List(Of String)
            If Not txtbox.Text = String.Empty Then
                Dim transferred As String() = txtbox.Text.Split(New Char() {CChar(",")}, StringSplitOptions.RemoveEmptyEntries)
                For Each tran As String In transferred
                    TransferredValues.Add(tran)
                Next
            End If
            Return TransferredValues
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetSourceListBox() As String
        Dim rtnValue As String
        Try
            Dim txtbox As RadTextBox = CType(Me.Page.Controls(0).FindControl("ContentMain2").FindControl("SourceListBoxPage"), RadTextBox)
            If Not txtbox.Text Is String.Empty Then
                rtnValue = txtbox.Text
            Else
                rtnValue = String.Empty
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return rtnValue
    End Function

    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties(prop)
                Else
                    SetChildControlProperties(prop)
                End If
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Private Sub SetControlProperties(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub

    Private Sub SetChildControlProperties(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As UI.Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub

    Private Function GetCurrentlySelectedFilters() As ParamValueDictionary
        Dim ValueDictionary As New ParamValueDictionary
        Dim ModifierDictionary As New ModDictionary
        Dim SelectedValues As New ParamValueList(Of String)
        For Each row As StudentEnrollmentInfoSearch In StuEnrollmentSelectedStore
            Dim id As String = row.StuEnrollId.ToString
            SelectedValues.Add(id)
        Next
        ModifierDictionary.Name = "InList"
        ModifierDictionary.Add("CurrentlySelected", SelectedValues)
        ValueDictionary.Add(ModDictionary.Modifier.InList, ModifierDictionary)
        ValueDictionary.Name = "CurrentlySelected"
        ValueDictionary.CtrlName = ItemDetail.ItemName
        Return ValueDictionary
    End Function

    Private Function GetAllDataFilters(ByVal TypeofCall As String, ByVal CallingBlock As String) As MasterDictionary
        Dim objFilters As New DetailDictionary
        Dim objDetails As New DetailDictionary
        Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName & "_Filters"), MasterDictionary)
        objDetails.Name = ItemDetail.ItemName

        If objMaster.Contains(ItemDetail.ItemName) Then
            Dim x As String = ""
            objDetails = DirectCast(objMaster.Item(ItemDetail.ItemName), DetailDictionary)
        End If

        Dim userIdValueDictionary As New ParamValueDictionary()
        Dim currentlySelectedValueDictionary As New ParamValueDictionary()
        Dim campusIdValueDictionary As New ParamValueDictionary()

        objFilters.Name = ItemDetail.ItemName

        For Each d As ParamValueDictionary In objDetails.Values
            objFilters.Add(d.Name, d)
        Next

        userIdValueDictionary = GetUserFilters(UserId)
        If objFilters.Contains("UserId") Then
            objFilters.Remove("UserId")
        End If
        objFilters.Add("UserId", userIdValueDictionary)

        campusIdValueDictionary = GetCampusIdFilter()
        If objFilters.Contains("CampusId") Then
            objFilters.Remove("CampusId")
        End If
        objFilters.Add("CampusId", campusIdValueDictionary)

        Dim sourceListBoxName As String = GetSourceListBox()

        If Not sourceListBoxName.Contains("RadGridSelectedStuEnrollments") Then
            If RadGridSelectedStuEnrollments.MasterTableView.Items.Count > 0 Then
                currentlySelectedValueDictionary = GetCurrentlySelectedFilters()
                If objFilters.Contains("CurrentlySelected") Then
                    objFilters.Remove("CurrentlySelected")
                End If
                objFilters.Add("CurrentlySelected", currentlySelectedValueDictionary)
            End If
        End If

        If objMaster.Contains(ItemDetail.ItemName) Then
            objMaster.Remove(ItemDetail.ItemName)
        End If
        objMaster.Add(ItemDetail.ItemName, objFilters)
        Session(ItemDetail.SetName & "_Filters") = objMaster

        Return objMaster
    End Function


    Private Function GetUserFilters(ByVal UserId As Guid) As ParamValueDictionary
        Dim Valuedictionary As New ParamValueDictionary
        Dim ModifierDictionary As New ModDictionary
        Dim UserIds As New ParamValueList(Of Guid)
        Try
            UserIds.Add(UserId)
            ModifierDictionary.Name = "InList"
            ModifierDictionary.Add("UserId", UserIds)
            Valuedictionary.Add(ModDictionary.Modifier.InList, ModifierDictionary)
            Valuedictionary.Name = "UserId"
            Valuedictionary.CtrlName = ItemDetail.ItemName
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return Valuedictionary
    End Function

    Private Function GetCampusIdFilter() As ParamValueDictionary
        Dim Valuedictionary As New ParamValueDictionary
        Dim ModifierDictionary As New ModDictionary
        Dim CampusIds As New ParamValueList(Of Guid)
        Try
            CampusIds.Add(New Guid(Request.QueryString("cmpid")))
            ModifierDictionary.Name = "InList"
            ModifierDictionary.Add("CampusId", CampusIds)
            Valuedictionary.Add(ModDictionary.Modifier.InList, ModifierDictionary)
            Valuedictionary.Name = "CampusId"
            Valuedictionary.CtrlName = ItemDetail.ItemName
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return Valuedictionary
    End Function

    Public Function FormatSSN(ByVal SSN As String) As String
        If Not SSN = String.Empty Then
            Return "*****" & Mid(SSN, 6, 4)
        Else
            Return String.Empty
        End If
    End Function

    Protected Sub FilterDisplay()

        Dim rtb As RadTextBox = RadTextBox1
        Dim nameexpression As String
        Dim prgverexpression As String
        Dim statusexpression As String
        Dim expression As String = String.Empty
        Dim cb As RadComboBox

        'Me.lbnReset.Visible = True

        If rtb.Text.Trim.Length > 0 And rtb.Text <> rtb.EmptyMessage Then
            searchRequest = rtb.Text.Trim

            If IsNumeric(searchRequest.Substring(0, 1)) Then
                'on numeric we search the studentnumber the ssn and enrollmentid with an or condition
                nameexpression = "([StudentNumber] LIKE '" & searchRequest & "%' or [SSNSearch] LIKE '" & searchRequest & "%' or [EnrollmentId] LIKE '" & searchRequest & "%')"
            Else
                'on alpha we search the firstname and lastname with an or condition
                If searchRequest.IndexOf(" ") <> -1 Then
                    'we are assuming that a string with a space in the middle is a firstname/lastname request
                    nameexpression = "([FirstName] LIKE '" & searchRequest.Substring(0, searchRequest.IndexOf(" ")) & "%' and [LastName] LIKE '" & searchRequest.Substring(searchRequest.IndexOf(" ") + 1) & "%')"
                Else
                    'on alpha we search the firstname and lastname with an or condition
                    nameexpression = "([FirstName] LIKE '" & searchRequest & "%' or [LastName] LIKE '" & searchRequest & "%')"
                End If
            End If
        Else
            nameexpression = String.Empty
        End If

        cb = Me.RadComboBox1
        If cb.SelectedValue <> "0" Then
            prgverexpression = "([PrgVerId] = '" & cb.SelectedValue & "')"
        Else
            prgverexpression = String.Empty
        End If

        'cb = Nothing

        cb = Me.RadComboBox2
        If cb.SelectedValue <> "0" Then
            statusexpression = "([StatusCodeId] = '" & cb.SelectedValue & "')"
        Else
            statusexpression = String.Empty
        End If

        If nameexpression <> String.Empty Then
            expression = nameexpression
        End If

        If prgverexpression <> String.Empty Then
            If expression = String.Empty Then
                expression = prgverexpression
            Else
                expression = expression & " and " & prgverexpression
            End If
        End If

        If statusexpression <> String.Empty Then
            If expression = String.Empty Then
                expression = statusexpression
            Else
                expression = expression & " and " & statusexpression
            End If
        End If

        Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = True

        RadGridStuEnrollmentSearch.MasterTableView.FilterExpression = expression
        Try
            RadGridStuEnrollmentSearch.MasterTableView.Rebind()
            Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = False
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ' lblErrMsg.Text = ex.Message
            Throw ex
        End Try

        If (rtb.Text.Trim.Length > 0 And rtb.Text <> rtb.EmptyMessage) Or RadComboBox1.SelectedIndex > 0 Or RadComboBox2.SelectedIndex > 0 Then
            lbnReset.Visible = True
        End If
    End Sub

    Protected Sub FilterDisplayMRU()

        Dim rtb As RadTextBox = RadTextBox2
        Dim nameexpression As String
        Dim prgverexpression As String
        Dim statusexpression As String
        Dim expression As String = String.Empty
        Dim cb As RadComboBox

        'Me.lbnResetMRU.Visible = True

        If rtb.Text.Trim.Length > 0 And rtb.Text <> rtb.EmptyMessage Then
            searchRequest = rtb.Text.Trim

            If IsNumeric(searchRequest.Substring(0, 1)) Then
                'on numeric we search the studentnumber the ssn and enrollmentid with an or condition
                nameexpression = "([StudentNumber] LIKE '" & searchRequest & "%' or [SSNSearch] LIKE '" & searchRequest & "%' or [EnrollmentId] LIKE '" & searchRequest & "%')"
            Else
                'on alpha we search the firstname and lastname with an or condition
                If searchRequest.IndexOf(" ") <> -1 Then
                    'we are assuming that a string with a space in the middle is a firstname/lastname request
                    nameexpression = "([FirstName] LIKE '" & searchRequest.Substring(0, searchRequest.IndexOf(" ")) & "%' and [LastName] LIKE '" & searchRequest.Substring(searchRequest.IndexOf(" ") + 1) & "%')"
                Else
                    'on alpha we search the firstname and lastname with an or condition
                    nameexpression = "([FirstName] LIKE '" & searchRequest & "%' or [LastName] LIKE '" & searchRequest & "%')"
                End If
            End If
        Else
            nameexpression = String.Empty
        End If

        cb = Me.RadComboBox3
        If cb.SelectedValue <> "0" Then
            prgverexpression = "([PrgVerId] = '" & cb.SelectedValue & "')"
        Else
            prgverexpression = String.Empty
        End If


        cb = Me.RadComboBox4
        If cb.SelectedValue <> "0" Then
            statusexpression = "([StatusCodeId] = '" & cb.SelectedValue & "')"
        Else
            statusexpression = String.Empty
        End If

        If nameexpression <> String.Empty Then
            expression = nameexpression
        End If

        If prgverexpression <> String.Empty Then
            If expression = String.Empty Then
                expression = prgverexpression
            Else
                expression = expression & " and " & prgverexpression
            End If
        End If

        If statusexpression <> String.Empty Then
            If expression = String.Empty Then
                expression = statusexpression
            Else
                expression = expression & " and " & statusexpression
            End If
        End If

        Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = True

        RadGridStuEnrollmentSearchMRU.MasterTableView.FilterExpression = expression
        Try
            RadGridStuEnrollmentSearchMRU.MasterTableView.Rebind()
            Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ' lblErrMsg.Text = ex.Message
            Throw
        End Try

        If (rtb.Text.Trim.Length > 0 And rtb.Text <> rtb.EmptyMessage) Or RadComboBox3.SelectedIndex > 0 Or RadComboBox4.SelectedIndex > 0 Then
            lbnResetMRU.Visible = True
        End If

    End Sub

    Public Sub SetSearchMode()
        Try
            Select Case Mode
                Case ISearchControl.ModeType.UnFiltered
                    RadComboBox1.Visible = True
                    RadComboBox2.Visible = True
                    RadComboBox3.Visible = True
                    RadComboBox4.Visible = True
                Case ISearchControl.ModeType.Filtered
                    RadComboBox1.Visible = False
                    RadComboBox2.Visible = False
                    RadComboBox3.Visible = False
                    RadComboBox4.Visible = False
                Case Else
                    Throw New Exception("Unkwown Search Mode")
            End Select

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Public Function GetReturnValues() As DetailDictionary Implements ISearchControl.GetReturnValues
        Dim objDetails As New DetailDictionary
        Dim returnValues As ParamValueDictionary
        Try
            If SetFilterMode() = True Then

                returnValues = CreateValues()
                If returnValues.Count > 0 Then
                    objDetails.Add(ItemDetail.ItemName, returnValues)
                End If
                Return objDetails
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        Return objDetails
    End Function

    Private Sub HasSelectedValues()
        Try
            If RadGridSelectedStuEnrollments.Items.Count > 0 Then
                Session("SearchHasValues") = True
            Else
                Session("SearchHasValues") = False
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Public Function SetFilterMode() As Boolean Implements IFilterControl.SetFilterMode
        Dim rValue As Boolean
        Try
            Dim blnQuickFilterMode As Boolean = CType(Session("QuickFilterMode"), Boolean)

            If ItemDetail.SectionType = ParameterSectionInfo.SecType.Quick Then
                If RadGridSelectedStuEnrollments.Items.Count > 0 Then
                    blnQuickFilterMode = True
                    rValue = True
                Else
                    blnQuickFilterMode = False
                    rValue = False
                End If
            Else
                If ItemDetail.SectionType = ParameterSectionInfo.SecType.Advanced AndAlso blnQuickFilterMode = True Then
                    rValue = False
                Else
                    rValue = True
                End If
            End If

            Session("QuickFilterMode") = blnQuickFilterMode
            Return rValue
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ISearchControl.GetControlSettings
        HasSelectedValues()
        Dim UserSettings As New ParamItemUserSettingsInfo
        Try
            If SetFilterMode() = True Then
                Dim SettingsCollection As New List(Of ControlSettingInfo)
                Dim ctrlSetting As New ControlSettingInfo
                Dim ctrlValues As New List(Of ControlValueInfo)

                UserSettings.ItemName = ItemDetail.ItemName
                UserSettings.ItemId = ItemDetail.ItemId
                UserSettings.DetailId = ItemDetail.DetailId
                UserSettings.FriendlyName = ItemDetail.CaptionOverride

                ctrlSetting.ControlName = RadGridSelectedStuEnrollments.ID

                Dim count As Integer = RadGridSelectedStuEnrollments.PageCount
                For i As Integer = 0 To count - 1
                    RadGridSelectedStuEnrollments.CurrentPageIndex = i
                    RadGridSelectedStuEnrollments.Rebind()
                    For Each item As GridDataItem In RadGridSelectedStuEnrollments.Items
                        Dim objControlValue As New ControlValueInfo
                        objControlValue.DisplayText = item("LastName").Text & " " & item("FirstName").Text
                        objControlValue.KeyData = item("StuEnrollId").Text.ToString()
                        ctrlValues.Add(objControlValue)
                    Next
                Next
                ctrlSetting.ControlValueCollection = ctrlValues
                SettingsCollection.Add(ctrlSetting)
                UserSettings.ControlSettingsCollection = SettingsCollection

                RadGridSelectedStuEnrollments.CurrentPageIndex = 0
                RadGridSelectedStuEnrollments.Rebind()

                RadGridStuEnrollmentSearch.CurrentPageIndex = 0
                RadGridStuEnrollmentSearch.Rebind()

                RadGridStuEnrollmentSearchMRU.CurrentPageIndex = 0
                RadGridStuEnrollmentSearchMRU.Rebind()


                Return UserSettings
                'End If
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        'Return UserSettings
    End Function

    Public Sub LoadSavedReportSettings() Implements ISearchControl.LoadSavedReportSettings
        Dim query As IQueryable(Of StudentEnrollmentSearch)
        Dim DA As New StudentEnrollmentDA(SqlConn)
        Dim results As List(Of StudentEnrollmentInfoSearch) = New List(Of StudentEnrollmentInfoSearch)
        Dim searchType As String
        Dim emptylist As New List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollSearch As List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollSearchMRU As List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollselected As List(Of StudentEnrollmentInfoSearch)
        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")
        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")
        stuenrollSearch = StuEnrollmentSearchStore
        stuenrollSearchMRU = MRUStuEnrollmentSearchStore
        stuenrollselected = StuEnrollmentSelectedStore

        'first we determine if there are currently selected students
        'and move them back to the proper search grid
        Dim count As Integer = RadGridSelectedStuEnrollments.PageCount
        'Dim flag As Integer = 0
        For i As Integer = 0 To count - 1
            RadGridSelectedStuEnrollments.CurrentPageIndex = i
            RadGridSelectedStuEnrollments.Rebind()
            For Each gridDataItem As GridDataItem In RadGridSelectedStuEnrollments.Items
                Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollselected, New Guid(gridDataItem("StuEnrollId").Text.ToString()))
                If tmpStudent.StudentType = "M" Then
                    stuenrollselected.Remove(tmpStudent)
                    stuenrollSearchMRU.Add(tmpStudent)
                Else
                    stuenrollselected.Remove(tmpStudent)
                    stuenrollSearch.Add(tmpStudent)
                End If
            Next
        Next
        'sort em, bind em, put em back 
        Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
        StuEnrollmentSearchStore = stuenrollSearch
        MRUStuEnrollmentSearchStore = stuenrollSearchMRU
        StuEnrollmentSelectedStore = stuenrollselected
        RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridStuEnrollmentSearch.MasterTableView.Rebind()
        RadGridStuEnrollmentSearchMRU.MasterTableView.Rebind()
        RadGridSelectedStuEnrollments.DataSource = emptylist
        RadGridSelectedStuEnrollments.DataBind()
        Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = False

        'now we can load the save report settings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As UI.Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadGrid Then
                            'Dim myradgrid As RadGrid = DirectCast(ctrl, RadGrid)
                            Dim StuEnrollIds(Setting.ControlValueCollection.Count - 1) As String
                            Dim Users(0) As String
                            Users(0) = UserId.ToString
                            Dim i As Integer = 0
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                StuEnrollIds(i) = ItemValue.KeyData
                                i = i + 1
                            Next
                            'get data to fill grid
                            query = DA.GetStuEnrollmentsByStuEnrollId(Users, StuEnrollIds)
                            For Each studentEnrollmentInfoSearch As StudentEnrollmentSearch In query
                                Dim stuenrollmentid As Guid = studentEnrollmentInfoSearch.StuEnrollId
                                Dim enrollmentid As String = studentEnrollmentInfoSearch.EnrollmentId
                                Dim studentid As Guid = studentEnrollmentInfoSearch.StudentId
                                Dim searchdisplay As String = String.Empty
                                Dim fullname As String = String.Empty
                                Dim firstname As String = studentEnrollmentInfoSearch.FirstName.Trim
                                Dim lastname As String = studentEnrollmentInfoSearch.LastName.Trim
                                Dim ssn As String = FormatSSN(studentEnrollmentInfoSearch.SSN)
                                Dim ssnsearch As String = (studentEnrollmentInfoSearch.SSN)
                                Dim studentnumber As String = studentEnrollmentInfoSearch.StudentNumber
                                Dim prgverid As Guid = studentEnrollmentInfoSearch.PrgVerId
                                Dim prgverdescrip As String = studentEnrollmentInfoSearch.PrgVerDescrip
                                Dim shiftid As Nullable(Of Guid) = studentEnrollmentInfoSearch.ShiftId
                                Dim shiftdescrip As String = studentEnrollmentInfoSearch.ShiftDescrip
                                Dim statuscodeid As Nullable(Of Guid) = studentEnrollmentInfoSearch.StatusCodeId
                                Dim statuscodedescrip As String = studentEnrollmentInfoSearch.StatusCodeDescrip
                                Dim campgrpid As Guid = studentEnrollmentInfoSearch.CampGrpId
                                Dim leadgrpid As Nullable(Of Guid) = studentEnrollmentInfoSearch.LeadGrpId
                                Dim sysstatusid As Integer = studentEnrollmentInfoSearch.SysStatusId
                                Dim isTransHold As Boolean = studentEnrollmentInfoSearch.IsTransHold
                                Dim tmpStudent As StudentEnrollmentInfoSearch
                                'regular student
                                tmpStudent = GetStudent(stuenrollSearch, stuenrollmentid)
                                If tmpStudent Is Nothing Then
                                    'MRU student
                                    tmpStudent = GetStudent(stuenrollSearchMRU, stuenrollmentid)
                                    searchType = "M"
                                    stuenrollSearchMRU.Remove(tmpStudent)
                                    stuenrollselected.Add(tmpStudent)
                                Else
                                    stuenrollSearch.Remove(tmpStudent)
                                    stuenrollselected.Add(tmpStudent)
                                    searchType = "S"
                                End If

                                If (ItemDetail.ItemName = "StudentEnrollSearch") _
                                And (isTransHold = True And Request.QueryString("resid").ToString() = "238") Then
                                    ''Message about fullname Student IsTransHold
                                    'Dim message As String = fullname + " is on a Transcript Hold"
                                    'DisplayWarningInMessageBox(message)
                                    lblMessagePanel1.Text = studentsTranscripsInHoldMessage
                                    lblMessagePanel1.Visible = True
                                    lblMessagePanel2.Text = studentsTranscripsInHoldMessage
                                    lblMessagePanel2.Visible = True
                                Else
                                    results.Add(New StudentEnrollmentInfoSearch(stuenrollmentid, enrollmentid, studentid, searchdisplay, fullname, firstname, lastname, ssn, ssnsearch, studentnumber, prgverid, prgverdescrip, shiftid, shiftdescrip, statuscodeid, statuscodedescrip, sysstatusid, campgrpid, leadgrpid, searchType, isTransHold))
                                End If
                            Next
                            Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = True
                            Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
                            StuEnrollmentSearchStore = stuenrollSearch
                            MRUStuEnrollmentSearchStore = stuenrollSearchMRU
                            StuEnrollmentSelectedStore = stuenrollselected
                            RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
                            RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
                            RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
                            RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
                            RadGridStuEnrollmentSearch.MasterTableView.Rebind()
                            RadGridStuEnrollmentSearchMRU.MasterTableView.Rebind()
                            RadGridSelectedStuEnrollments.DataSource = results
                            RadGridSelectedStuEnrollments.DataBind()
                            Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = False
                            Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
                        Else
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                StuEnrollmentSelectedStore = emptylist
                RadGridSelectedStuEnrollments.DataSource = StuEnrollmentSelectedStore
                RadGridSelectedStuEnrollments.DataBind()
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            UserId = New Guid(AdvantageSession.UserState.UserId.ToString)
            Dim s As String
            s = UserId.ToString()

            AddTBValueChangedScript()
            If Page.IsPostBack = True Then
                PostbackCtrlName = Page.Request.Params.Get("__EVENTTARGET")
            Else
                'AddTBValueChangedScript()
                LoadComboBoxes()
                PostbackCtrlName = String.Empty
                Session("StuEnrollmentSearch_VB") = Nothing
                Session("StuEnrollmentSelected_VB") = Nothing
                Session("MRUStuEnrollmentSearch_VB") = Nothing
               
            End If
            RadTextBox1.ClientEvents.OnKeyPress = ClientSearchFunction
            RadTextBox2.ClientEvents.OnKeyPress = ClientSearchFunction
            SetProperties()
            Fill()
            GetMasterFilterCollection()
            HasSelectedValues()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ' lblErrMsg.Text = ex.Message
            Throw ex
        End Try
    End Sub

    Protected Sub RadGridStuEnrollmentSearch_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = RadGridStuEnrollmentSearch.ClientID Then
                If (e.DestDataItem Is Nothing AndAlso StuEnrollmentSelectedStore.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = RadGridSelectedStuEnrollments.ClientID) Then
                    Dim stuenrollSearch As List(Of StudentEnrollmentInfoSearch)
                    Dim stuenrollmentSelected As List(Of StudentEnrollmentInfoSearch)
                    stuenrollmentSelected = StuEnrollmentSelectedStore
                    stuenrollSearch = StuEnrollmentSearchStore
                    Dim destinationIndex As Int32 = -1
                    If (e.DestDataItem IsNot Nothing) Then
                        Dim studentGuid As Guid = New Guid(e.DestDataItem.KeyValues.Substring(e.DestDataItem.KeyValues.IndexOf(":") + 2, 36).ToString())
                        Dim student As StudentEnrollmentInfoSearch = GetStudent(stuenrollSearch, studentGuid)
                        If IsDBNull(student) = False Then
                            destinationIndex = stuenrollSearch.IndexOf(student)
                        End If
                    End If

                    For Each draggedItem As GridDataItem In e.DraggedItems
                        Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollSearch, DirectCast(draggedItem.GetDataKeyValue("StuEnrollId"), Guid))
                        If tmpStudent IsNot Nothing Then
                            If (ItemDetail.ItemName = "StudentEnrollSearch") _
                            And (tmpStudent.IsTransHold = True And Request.QueryString("resid").ToString() = "238") Then
                                'Messaje about fullname Student IsTransHold
                                'Dim message As String = tmpStudent.FirstName + " " _
                                '                      + tmpStudent.LastName _ 
                                '                      + " is on a Transcript Hold"
                            Else
                                If destinationIndex > -1 Then
                                    If e.DropPosition = GridItemDropPosition.Below Then
                                        destinationIndex += 1
                                    End If
                                    'stuenrollSearch.Insert(destinationIndex, tmpStudent)
                                    'stuenrollmentSelected.Insert((stuenrollmentSelected.Count + 1), tmpStudent)
                                End If
                            End If
                            stuenrollmentSelected.Add(tmpStudent)
                            stuenrollSearch.Remove(tmpStudent)
                        End If
                    Next

                    Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = True
                    Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = True

                    Dim expressions As New GridSortExpressionCollection

                    Dim expression1 As New GridSortExpression
                    expression1.FieldName = "LastName"
                    expression1.SetSortOrder("Ascending")

                    Dim expression2 As New GridSortExpression
                    expression2.FieldName = "FirstName"
                    expression2.SetSortOrder("Ascending")

                    StuEnrollmentSelectedStore = stuenrollmentSelected
                    StuEnrollmentSearchStore = stuenrollSearch

                    RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
                    RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
                    RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression1)
                    RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression2)

                    RadGridSelectedStuEnrollments.MasterTableView.Rebind()
                    RadGridStuEnrollmentSearch.MasterTableView.Rebind()

                    Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = False
                    Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = False

                End If
            End If
        End If
    End Sub

    Protected Sub RadGridSelectedStuEnrollments_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = RadGridSelectedStuEnrollments.ClientID Then
                Dim stuenrollSearch As List(Of StudentEnrollmentInfoSearch)
                Dim stuenrollmentSelected As List(Of StudentEnrollmentInfoSearch)
                For Each draggedItem As GridDataItem In e.DraggedItems
                    stuenrollmentSelected = StuEnrollmentSelectedStore
                    If draggedItem("StudentType").Text = "S" Then
                        stuenrollSearch = StuEnrollmentSearchStore
                    Else
                        stuenrollSearch = MRUStuEnrollmentSearchStore
                    End If
                    If (e.DestDataItem Is Nothing AndAlso stuenrollSearch.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso (e.DestDataItem.OwnerGridID = RadGridStuEnrollmentSearch.ClientID Or e.DestDataItem.OwnerGridID = RadGridStuEnrollmentSearchMRU.ClientID)) Then
                        Dim destinationIndex As Int32 = -1
                        If (e.DestDataItem IsNot Nothing) Then
                            Dim student As StudentEnrollmentInfoSearch = GetStudent(stuenrollmentSelected, DirectCast(e.DestDataItem.GetDataKeyValue("StuEnrollId"), Guid))
                            destinationIndex = stuenrollmentSelected.IndexOf(student)
                            If IsDBNull(student) = False Then
                                destinationIndex = stuenrollmentSelected.IndexOf(student)
                            End If
                        End If
                        Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollmentSelected, DirectCast(draggedItem.GetDataKeyValue("StuEnrollId"), Guid))
                        If tmpStudent IsNot Nothing Then
                            If destinationIndex > -1 Then
                                If e.DropPosition = GridItemDropPosition.Below Then
                                    destinationIndex += 1
                                End If
                                stuenrollSearch.Insert(destinationIndex, tmpStudent)
                            Else
                                stuenrollSearch.Add(tmpStudent)
                            End If
                            stuenrollmentSelected.Remove(tmpStudent)
                        End If

                        If draggedItem("StudentType").Text = "S" Then
                            StuEnrollmentSearchStore = stuenrollSearch
                        Else
                            MRUStuEnrollmentSearchStore = stuenrollSearch
                        End If
                        StuEnrollmentSelectedStore = stuenrollmentSelected

                    End If
                Next

                Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = True
                Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
                Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = True

                ' Dim expressions As New GridSortExpressionCollection

                Dim expression1 As New GridSortExpression
                expression1.FieldName = "LastName"
                expression1.SetSortOrder("Ascending")

                Dim expression2 As New GridSortExpression
                expression2.FieldName = "FirstName"
                expression2.SetSortOrder("Ascending")

                RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
                RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
                RadGridStuEnrollmentSearch.MasterTableView.Rebind()

                RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
                RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
                RadGridStuEnrollmentSearchMRU.MasterTableView.Rebind()

                RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression1)
                RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression2)
                RadGridSelectedStuEnrollments.MasterTableView.Rebind()

                Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = False
                Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
                Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = False

            End If
        End If
    End Sub

    Protected Sub RadGridStuEnrollmentSearchMRU_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = RadGridStuEnrollmentSearchMRU.ClientID Then
                If (e.DestDataItem Is Nothing AndAlso StuEnrollmentSelectedStore.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = RadGridSelectedStuEnrollments.ClientID) Then
                    Dim stuenrollSearchMRU As List(Of StudentEnrollmentInfoSearch)
                    Dim stuenrollmentSelected As List(Of StudentEnrollmentInfoSearch)

                    stuenrollmentSelected = StuEnrollmentSelectedStore
                    stuenrollSearchMRU = MRUStuEnrollmentSearchStore

                    Dim destinationIndex As Int32 = -1
                    If (e.DestDataItem IsNot Nothing) Then
                        Dim StudentGuid As Guid = New Guid(e.DestDataItem.KeyValues.Substring(e.DestDataItem.KeyValues.IndexOf(":") + 2, 36).ToString())
                        Dim student As StudentEnrollmentInfoSearch = GetStudent(stuenrollSearchMRU, StudentGuid)
                        destinationIndex = stuenrollSearchMRU.IndexOf(student)
                        If IsDBNull(student) = False Then
                            destinationIndex = stuenrollSearchMRU.IndexOf(student)
                        End If
                    End If

                    For Each draggedItem As GridDataItem In e.DraggedItems
                        Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollSearchMRU, DirectCast(draggedItem.GetDataKeyValue("StuEnrollId"), Guid))
                        If tmpStudent IsNot Nothing Then
                            If (ItemDetail.ItemName = "StudentEnrollSearch") _
                            And (tmpStudent.IsTransHold = True And Request.QueryString("resid").ToString() = "238") Then
                                ''Messaje about fullname Student IsTransHold
                                'Dim message As String = tmpStudent.FirstName + " " _
                                '                      + tmpStudent.LastName _ 
                                '                      + " is on a Transcript Hold"
                            Else
                                If destinationIndex > -1 Then
                                    If e.DropPosition = GridItemDropPosition.Below Then
                                        destinationIndex += 1
                                    End If
                                    stuenrollmentSelected.Insert((stuenrollmentSelected.Count + 1), tmpStudent)
                                Else
                                    stuenrollmentSelected.Add(tmpStudent)
                                End If
                                stuenrollSearchMRU.Remove(tmpStudent)
                            End If

                        End If

                    Next

                    Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
                    Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = True

                    Dim expression1 As New GridSortExpression
                    expression1.FieldName = "LastName"
                    expression1.SetSortOrder("Ascending")

                    Dim expression2 As New GridSortExpression
                    expression2.FieldName = "FirstName"
                    expression2.SetSortOrder("Ascending")

                    StuEnrollmentSelectedStore = stuenrollmentSelected
                    MRUStuEnrollmentSearchStore = stuenrollSearchMRU

                    RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
                    RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
                    RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression1)
                    RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression2)

                    RadGridSelectedStuEnrollments.MasterTableView.Rebind()
                    RadGridStuEnrollmentSearchMRU.MasterTableView.Rebind()

                    Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
                    Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = False

                End If
            End If
        End If
    End Sub

    Protected Sub lbnReset_Click(ByVal sender As Object, ByVal e As EventArgs)

        Me.RadGridStuEnrollmentSearch.MasterTableView.FilterExpression = String.Empty
        Me.RadGridStuEnrollmentSearch.DataSource = StuEnrollmentSearchStore()
        Me.RadGridStuEnrollmentSearch.DataBind()
        Me.RadTextBox1.Text = String.Empty
        Me.RadTextBox1.EmptyMessage = "Enter Name, Student#, Enrollment#, or SSN"
        Me.RadComboBox1.SelectedIndex = 0
        Me.RadComboBox2.SelectedIndex = 0
    End Sub

    Protected Sub lbnResetMRU_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.RadGridStuEnrollmentSearchMRU.MasterTableView.FilterExpression = String.Empty
        Me.RadGridStuEnrollmentSearchMRU.DataSource = MRUStuEnrollmentSearchStore()
        Me.RadGridStuEnrollmentSearchMRU.DataBind()
        Me.RadTextBox2.Text = String.Empty
        Me.RadTextBox2.EmptyMessage = "Enter Name, Student#, Enrollment#, or SSN"
        Me.RadComboBox3.SelectedIndex = 0
        Me.RadComboBox4.SelectedIndex = 0
    End Sub

    Protected Sub lbnResetSelected_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim stuenrollSearch As List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollSearchMRU As List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollmentSelected As List(Of StudentEnrollmentInfoSearch)

        stuenrollSearch = StuEnrollmentSearchStore
        stuenrollSearchMRU = MRUStuEnrollmentSearchStore
        stuenrollmentSelected = StuEnrollmentSelectedStore

        Dim count As Integer = RadGridSelectedStuEnrollments.MasterTableView.PageCount
        Dim flag As Integer = 0
        For i As Integer = 0 To count - 1
            RadGridSelectedStuEnrollments.CurrentPageIndex = i
            RadGridSelectedStuEnrollments.Rebind()
            For Each gridItem As GridDataItem In RadGridSelectedStuEnrollments.Items
                Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollmentSelected, DirectCast(gridItem.GetDataKeyValue("StuEnrollId"), Guid))
                If tmpStudent IsNot Nothing Then
                    If tmpStudent.StudentType = "S" Then
                        stuenrollSearch.Add(tmpStudent)
                        stuenrollmentSelected.Remove(tmpStudent)
                    Else
                        stuenrollSearchMRU.Add(tmpStudent)
                        stuenrollmentSelected.Remove(tmpStudent)
                    End If
                End If
            Next
        Next
        RadGridSelectedStuEnrollments.CurrentPageIndex = 0
        RadGridSelectedStuEnrollments.Rebind()

        StuEnrollmentSearchStore = stuenrollSearch
        MRUStuEnrollmentSearchStore = stuenrollSearchMRU
        StuEnrollmentSelectedStore = stuenrollmentSelected

        RadGridStuEnrollmentSearch.CurrentPageIndex = 0
        RadGridStuEnrollmentSearchMRU.CurrentPageIndex = 0
        RadGridStuEnrollmentSearch.Rebind()
        RadGridStuEnrollmentSearchMRU.Rebind()
    End Sub

    Private Sub GetMasterFilterCollection()
        Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName & "_Filters"), MasterDictionary)
        Dim objDetails As New DetailDictionary
        Dim ReturnValues As ParamValueDictionary
        Try
            ReturnValues = CreateValues()
            If Not objMaster Is Nothing Then
                If objMaster.Contains(ItemDetail.ItemName) Then
                    objDetails = DirectCast(objMaster.Item(ItemDetail.ItemName), DetailDictionary)
                End If
                If Not objDetails Is Nothing Then
                    If objDetails.Contains(ItemDetail.ItemName) Then
                        'take out old values if they exist
                        objDetails.Remove(ItemDetail.ItemName)
                    End If
                    If ReturnValues.Count > 0 Then
                        'add new values if there are any
                        objDetails.Add(ItemDetail.ItemName, ReturnValues)
                        objMaster.Remove(ItemDetail.ItemName)
                        objMaster.Add(ItemDetail.ItemName, objDetails)
                    End If

                    'store master collection in session
                    Session(ItemDetail.SetName & "_Filters") = objMaster
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Private Function CreateValues() As ParamValueDictionary
        Dim ReturnValues As New ParamValueDictionary
        Try
            ReturnValues = FillValueDictionary()
            ReturnValues.Name = ItemDetail.ReturnValueName
            ReturnValues.CtrlName = ItemDetail.ItemName
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return ReturnValues
    End Function

    Protected Sub btnMoveSelectedDown_Click(ByVal sender As Object, ByVal e As EventArgs)
        
        Dim stuenrollSearch As List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollmentSelected As List(Of StudentEnrollmentInfoSearch)
        stuenrollmentSelected = StuEnrollmentSelectedStore
        stuenrollSearch = StuEnrollmentSearchStore

        For Each dataItem As GridDataItem In RadGridStuEnrollmentSearch.Items
            If dataItem.Selected Then
                Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollSearch, New Guid(dataItem("StuEnrollId").Text.ToString()))
                If (ItemDetail.ItemName = "StudentEnrollSearch") _
                And (tmpStudent.IsTransHold = True And Request.QueryString("resid").ToString() = "238") Then
                    ''Messaje about fullname Student IsTransHold 
                    'Dim message As String = tmpStudent.FirstName + " " _
                    '                        + tmpStudent.LastName _ 
                    '                        + " is on a Transcript Hold"
                Else
                    stuenrollmentSelected.Add(tmpStudent)
                    stuenrollSearch.Remove(tmpStudent)
                End If
            End If
        Next

        Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StuEnrollmentSelectedStore = stuenrollmentSelected
        StuEnrollmentSearchStore = stuenrollSearch

        RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStuEnrollments.MasterTableView.Rebind()
        RadGridStuEnrollmentSearch.MasterTableView.Rebind()

        Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = False
        

    End Sub

    Protected Sub btnMoveSelectedDownMRU_Click(ByVal sender As Object, ByVal e As EventArgs)
       
        Dim stuenrollSearchMRU As List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollmentSelected As List(Of StudentEnrollmentInfoSearch)
        stuenrollmentSelected = StuEnrollmentSelectedStore
        stuenrollSearchMRU = MRUStuEnrollmentSearchStore

        For Each dataItem As GridDataItem In RadGridStuEnrollmentSearchMRU.Items
            If dataItem.Selected Then
                Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollSearchMRU, New Guid(dataItem("StuEnrollId").Text.ToString()))
                If (ItemDetail.ItemName = "StudentEnrollSearch") _
                And (tmpStudent.IsTransHold = True And Request.QueryString("resid").ToString() = "238") Then
                    ''Messaje about fullname Student IsTransHold 
                    'Dim message As String = tmpStudent.FirstName + " " _
                    '                        + tmpStudent.LastName _ 
                    '                        + " is on a Transcript Hold"
                Else
                    stuenrollmentSelected.Add(tmpStudent)
                    stuenrollSearchMRU.Remove(tmpStudent)
                End If
            End If
        Next

        Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StuEnrollmentSelectedStore = stuenrollmentSelected
        MRUStuEnrollmentSearchStore = stuenrollSearchMRU

        RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStuEnrollments.MasterTableView.Rebind()
        RadGridStuEnrollmentSearchMRU.MasterTableView.Rebind()

        Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = False
        
    End Sub

    Protected Sub btnMoveSelectedUp_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim stuenrollSearch As List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollmentSelected As List(Of StudentEnrollmentInfoSearch)
        stuenrollmentSelected = StuEnrollmentSelectedStore
        stuenrollSearch = StuEnrollmentSearchStore

        For Each dataItem As GridDataItem In RadGridSelectedStuEnrollments.Items
            If dataItem.Selected Then
                Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollmentSelected, New Guid(dataItem("StuEnrollId").Text.ToString()))
                stuenrollmentSelected.Remove(tmpStudent)
                stuenrollSearch.Add(tmpStudent)

            End If
        Next

        Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StuEnrollmentSelectedStore = stuenrollmentSelected
        StuEnrollmentSearchStore = stuenrollSearch

        RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStuEnrollmentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStuEnrollments.MasterTableView.Rebind()
        RadGridStuEnrollmentSearch.MasterTableView.Rebind()

        Me.RadGridStuEnrollmentSearch.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = False
       

    End Sub

    Protected Sub btnMoveSelectedUpMRU_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim stuenrollSearchMRU As List(Of StudentEnrollmentInfoSearch)
        Dim stuenrollmentSelected As List(Of StudentEnrollmentInfoSearch)
        stuenrollmentSelected = StuEnrollmentSelectedStore
        stuenrollSearchMRU = MRUStuEnrollmentSearchStore

        For Each dataItem As GridDataItem In RadGridSelectedStuEnrollments.Items
            If dataItem.Selected Then
                Dim tmpStudent As StudentEnrollmentInfoSearch = GetStudent(stuenrollmentSelected, New Guid(dataItem("StuEnrollId").Text.ToString()))
                stuenrollmentSelected.Remove(tmpStudent)
                stuenrollSearchMRU.Add(tmpStudent)

            End If
        Next

        Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StuEnrollmentSelectedStore = stuenrollmentSelected
        MRUStuEnrollmentSearchStore = stuenrollSearchMRU

        RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStuEnrollmentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStuEnrollments.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStuEnrollments.MasterTableView.Rebind()
        RadGridStuEnrollmentSearchMRU.MasterTableView.Rebind()

        Me.RadGridStuEnrollmentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStuEnrollments.MasterTableView.AllowMultiColumnSorting = False
       
    End Sub

    Protected Sub RadGridStuEnrollmentSearch_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles RadGridStuEnrollmentSearch.DataBound
        For Each headerItem As GridHeaderItem In RadGridStuEnrollmentSearch.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.Header)
            headerItem.Height = Unit.Pixel(40)
        Next
    End Sub

    Protected Sub RadGridStuEnrollmentSearch_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGridStuEnrollmentSearch.ItemDataBound

        Dim sPrgVerName As String
        If (TypeOf e.Item Is GridDataItem) Then
            Dim gridItem As GridDataItem = CType(e.Item, GridDataItem)
            sPrgVerName = gridItem("PrgVerDescrip").Text

            If sPrgVerName.Length > 20 Then
                DirectCast(e.Item, GridDataItem)("PrgVerDescrip").ToolTip = sPrgVerName
                gridItem("PrgVerDescrip").Text = sPrgVerName.Substring(0, 21) & " ..."
            End If

            gridItem.Height = Unit.Pixel(60)
            'if the student enrollment is Transcript Hold make unselecteble
            Dim isTransHoldItem As Boolean = (CType(gridItem.DataItem, StudentEnrollmentInfoSearch).IsTransHold)
            If (ItemDetail.ItemName = "StudentEnrollSearch") _
            And (isTransHoldItem = True And Request.QueryString("resid").ToString() = "238") Then
                gridItem.SelectableMode = GridItemSelectableMode.None
            End If
        End If
    End Sub
    Protected Sub RadGridStuEnrollmentSearchMRU_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles RadGridStuEnrollmentSearchMRU.DataBound
        For Each headerItem As GridHeaderItem In RadGridStuEnrollmentSearchMRU.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.Header)
            headerItem.Height = Unit.Pixel(40)
        Next
    End Sub
    Protected Sub RadGridStuEnrollmentSearchMRU_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGridStuEnrollmentSearchMRU.ItemDataBound

        Dim sPrgVerName As String
        If (TypeOf e.Item Is GridDataItem) Then
            Dim gridItem As GridDataItem = CType(e.Item, GridDataItem)
            sPrgVerName = gridItem("PrgVerDescrip").Text

            If sPrgVerName.Length > 20 Then
                DirectCast(e.Item, GridDataItem)("PrgVerDescrip").ToolTip = sPrgVerName
                gridItem("PrgVerDescrip").Text = sPrgVerName.Substring(0, 21) & " ..."
            End If

            gridItem.Height = Unit.Pixel(60)
            'If the student enrollment is Transcript Hold make unselecteble
            Dim isTransHoldItem As Boolean = (CType(gridItem.DataItem, StudentEnrollmentInfoSearch).IsTransHold)
            If (ItemDetail.ItemName = "StudentEnrollSearch") _
            And (isTransHoldItem = True And Request.QueryString("resid").ToString() = "238") Then
                gridItem.SelectableMode = GridItemSelectableMode.None
            End If
        End If

    End Sub

    Protected Sub RadGridSelectedStuEnrollments_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles RadGridSelectedStuEnrollments.DataBound

        'this if block was moved from the itemevent routine when we turned off paging on the selectd grid
        If RadGridSelectedStuEnrollments.Items.Count > 0 Then
            lbnResetSelected.Visible = True
        Else
            lbnResetSelected.Visible = False
        End If


        For Each headerItem As GridHeaderItem In RadGridSelectedStuEnrollments.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.Header)
            headerItem.Height = Unit.Pixel(40)
        Next
    End Sub

    Protected Sub RadGridSelectedStuEnrollments_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGridSelectedStuEnrollments.ItemDataBound

        Dim sPrgVerName As String
        If (TypeOf e.Item Is GridDataItem) Then
            Dim gridItem As GridDataItem = CType(e.Item, GridDataItem)
            sPrgVerName = gridItem("PrgVerDescrip").Text

            If sPrgVerName.Length > 20 Then
                DirectCast(e.Item, GridDataItem)("PrgVerDescrip").ToolTip = sPrgVerName
                gridItem("PrgVerDescrip").Text = sPrgVerName.Substring(0, 21) & " ..."
            End If
        End If

        If TypeOf e.Item Is GridDataItem Then
            Dim item As GridDataItem = CType(e.Item, GridDataItem)
            item.Height = Unit.Pixel(60)
        End If

    End Sub

    'Protected Function OnlyOneStudent()as Boolean 
    '    IF (Page.Request.QueryString("resid").ToString()  = "862" And StuEnrollmentSelectedStore.Count() > 0 or RadGridSelectedStuEnrollments.Items.Count > 0) Then
    '        btnMoveSelectedDown.Visible= false
    '        btnMoveSelectedDownMRU.Visible= false
    '        Return true
    '        Else 
    '            btnMoveSelectedDown.Visible= True
    '            btnMoveSelectedDownMRU.Visible= True
    '            Return False
    '    End If
    'End Function

#End Region
    Private Sub DisplayInfoMessage(message As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, message)
    End Sub
    Private Sub DisplayWarningMessage(message As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayWarningInMessageBox(Me.Page, message)
    End Sub
    'Private Sub DisplayErrorMessage(message As String)
    '    'Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, message)
    'End Sub
    Private Sub DisplayErrorInClientMessage(message As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInClientMessageBox(Me.Page, message)
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End If
    End Sub
End Class

