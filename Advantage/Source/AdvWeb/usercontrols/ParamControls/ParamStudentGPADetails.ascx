﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamStudentGPADetails.ascx.vb" Inherits="ParamStudentGPADetails" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function checkFilter(sender, args) {
        // if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgram2") > 1) {
        //handleSourceToDestinationTransfer(args, 1, 'Only one program selection is allowed');
        // }
        // else if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgramVersion2") > 1) {
        //     handleSourceToDestinationTransfer(args, 1, 'Only one program version selection is allowed');
        //  }
        //  else if (args.get_destinationListBox().get_id().indexOf("RadListBoxTerm2") > 1) {
        //      handleSourceToDestinationTransfer(args, 1, 'Only one course selection is allowed');
        //   }
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
        //var maxNumberOfItems = 1;
        var dest = args.get_destinationListBox();
        var totalCount = dest.get_items().get_count();
        var itemsToTransferCount = args.get_items().length;
        if (totalCount == maxNumberOfItems) {
            alert(message);
            args.set_cancel(true);
        } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
            while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                itemsToTransferCount--;
                args.get_items().pop();
            }
        }
    }

    function keyPress(sender, args) {
        if ((args.get_keyCode() == 109) || (args.get_keyCode() == 45)) {
            args.set_cancel(true);
        }
    }

    function setInitialValueForGPAGT(sender, args) {
        if (sender.isEmpty()) {
            sender.set_value(0.00);
        }
    }

    function setInitialValueForGPALT(sender, args) {
        if (sender.isEmpty()) {
            sender.set_value(4.00);
        }
    }
    function setInitialValueForGreditsGPALT(sender, args) {
        if (sender.isEmpty()) {
            sender.set_value(100.00);
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div><span style="color: #b71c1c">*</span> All filters are required and the filters need to be selected in the following order.</div>
    <div id="MainContainer" class="MainContainer ">

        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer" runat="server" style="padding-left: 0px;">
            <div id="CampusHeader" class="CaptionLabel" runat="server">1. Campus <span style="color: #b71c1c">*</span></div>

            <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged" OnItemDataBound="CampusComboBox_ItemDataBound"
                Width="350px" ToolTip="Select a campus to run the report" CssClass="ManualInput">
            </telerik:RadComboBox>
        </div>
    </div>
    <div id="ProgramSelector" class="ProgramSelector MultiFilterReportContainer" runat="server">
        <div id="ProgramHeader" class="CaptionLabel" runat="server">2. Program </div>
        <div class="FilterInput">


            <telerik:RadListBox ID="RadListBoxProgram1" runat="server" Width="360px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" TransferToID="RadListBoxProgram2" AutoPostBackOnTransfer="True"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxProgram_OnTransferred"
                OnClientTransferring="checkFilter">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" TransferButtons="Common" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxProgram2" runat="server" Width="340px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" TransferToID="RadListBoxProgram1" AutoPostBackOnTransfer="True"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxProgram_OnTransferred">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactivePrograms" runat="server" Text="Show Inactive" AutoPostBack="true"
                    ToolTip="Check this box to make inactive programs selectable"
                    OnCheckedChanged="CheckBox_CheckedChanged" />
                <span id="ProgramCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="ProgramCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div id="ProgramVersionSelector" class="ProgramVersionSelector MultiFilterReportContainer" runat="server">
        <div id="ProgramVersionHeader" class="CaptionLabel" runat="server">3. Program Version </div>
        <div class="FilterInput">

            <telerik:RadListBox ID="RadListBoxProgramVersion1" runat="server" Width="360px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" TransferToID="RadListBoxProgramVersion2" AutoPostBackOnTransfer="True"
                OnClientTransferring="checkFilter"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxes_OnTransferred">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" TransferButtons="Common" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxProgramVersion2" runat="server" Width="340px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxes_OnTransferred">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="Div2" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactivePrgVersions" runat="server" Text="Show Inactive"
                    ToolTip="Check this box to make inactive ProgramVersions selectable" AutoPostBack="true"
                    OnCheckedChanged="CheckBox_CheckedChanged" />
                <span id="ProgramVersionCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramVersionCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="ProgramVersionCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramVersionCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />

        </div>
    </div>
    <div id="TermSelector" class="TermSelector MultiFilterReportContainer" runat="server">
        <div id="TermHeader" class="CaptionLabel" runat="server">Term</div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxTerm1" runat="server" Width="360px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" TransferToID="RadListBoxTerm2" AutoPostBackOnTransfer="True"
                OnClientTransferring="checkFilter"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxes_OnTransferred">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxTerm2" runat="server" Width="340px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" TransferToID="RadListBoxTerm1" AutoPostBackOnTransfer="True"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxes_OnTransferred">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="Div3" class="InactiveCheckBoxContainer" style="vertical-align: top;">
                <asp:CheckBox ID="cbkInactiveTerms" runat="server" CssClass="checkbox" Text="Show Inactive" AutoPostBack="true"
                    OnCheckedChanged="CheckBox_CheckedChanged"
                    ToolTip="Check this box to make inactive terms selectable" />
                <span id="TermCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblTermCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="TermCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblTermCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <!-- 
        <div id="Div4">
            <asp:CheckBox ID="chkShowGPAbyTerm" runat="server"  Text="Show GPA scored in each Term"  
                CssClass="checkbox" Style="display: inline-block;" 
                OnCheckedChanged="CheckBox_CheckedChanged"
                ToolTip="Check this box to see the GPA by Term" />
        </div>  
        -->
            <br />
        </div>
    </div>
    <div id="EnrollmentSelector" class="EnrollmentSelector MultiFilterReportContainer" runat="server">
        <div id="EnrollmentStatusHeader" class="CaptionLabel" runat="server">Enrollment Status</div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxEnrollmentStatus1" runat="server" Width="360px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" TransferToID="RadListBoxEnrollmentStatus2" AutoPostBackOnTransfer="True"
                OnClientTransferring="checkFilter"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxes_OnTransferred">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxEnrollmentStatus2" runat="server" Width="340px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxes_OnTransferred">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="Div5" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactiveEnrollmentStatus" runat="server" Text="Show Inactive"
                    OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true"
                    ToolTip="Check this box to make inactive enrollment statuses selectable" />
                <span id="EnrollmentStatusCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblEnrollmentStatusCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="EnrollmentStatusCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblEnrollmentStatusCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div id="Div11" class="CourseSelector MultiFilterReportContainer" runat="server">
        <div id="StudentGroupHeader" class="CaptionLabel" runat="server">Student Group</div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxStudentGroup1" runat="server" Width="360px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" TransferToID="RadListBoxStudentGroup2" AutoPostBackOnTransfer="True"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxes_OnTransferred">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxStudentGroup2" runat="server" Width="340px" Height="100px"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single"
                AllowTransfer="True" AllowTransferOnDoubleClick="True" TransferToID="RadListBoxStudentGroup1" AutoPostBackOnTransfer="True"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                OnTransferred="RadListBoxes_OnTransferred">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="Div6" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactiveStudentGroup" runat="server" Text="Show Inactive" AutoPostBack="true"
                    OnCheckedChanged="CheckBox_CheckedChanged"
                    ToolTip="Check this box to make inactive student grops selectable" />
                <span id="StudentGroupAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblStudentGroupAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="StudentGroupSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblStudentGroupSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div id="GPAOptions" runat="server" class="DateOptionsSelector MultiFilterReportContainer" height="5px;">
        <div id="GPAOptionsHeader" class="CaptionLabel" runat="server">Cumulative GPA/Average Range</div>
        <div class="FilterInput">
            <div id="DatePopUps" class="DatePops">
                <asp:Label ID="Label4" runat="server" Text="Greater than or equal to" Width="150px"
                    Font-Bold="True" ForeColor="#000066" Style="padding-left: 20px;"></asp:Label>
                <telerik:RadNumericTextBox runat="server" ID="cumulativeGPA_GT" MaxLength="5" TabIndex="7"
                    ClientEvents-OnKeyPress="keyPress" ClientEvents-OnLoad="setInitialValueForGPAGT">
                </telerik:RadNumericTextBox>
                <asp:Label ID="Label5" runat="server" Text="Less than or equal to" Width="150px"
                    Font-Bold="True" ForeColor="#000066" Style="padding-left: 10px;"></asp:Label>
                <telerik:RadNumericTextBox runat="server" ID="cumulativeGPA_LT" MaxLength="5" TabIndex="8"
                    ClientEvents-OnKeyPress="keyPress" ClientEvents-OnLoad="setInitialValueForGPALT">
                </telerik:RadNumericTextBox>

            </div>
            <br />
        </div>
    </div>
    <div id="TermCreditRangeOptions" runat="server" class="DateOptionsSelector MultiFilterReportContainer" visible="true">
        <div id="TermCreditRangeHeader" class="CaptionLabel" runat="server">Term Credits Range</div>
        <div class="FilterInput">
            <div id="TermCreditRangeFields" class="DatePops">
                <asp:Label ID="lblGT" runat="server" Text="Greater than or equal to" Width="150px"
                    Font-Bold="True" ForeColor="#000066" Style="padding-left: 20px;"></asp:Label>
                <telerik:RadNumericTextBox runat="server" ID="radTermCredits_GT" MaxLength="5" TabIndex="9"
                    ClientEvents-OnKeyPress="keyPress" ClientEvents-OnLoad="setInitialValueForGPAGT">
                </telerik:RadNumericTextBox>
                <asp:Label ID="Label1" runat="server" Text="Less than or equal to" Width="150px"
                    Font-Bold="True" ForeColor="#000066" Style="padding-left: 10px;"></asp:Label>
                <telerik:RadNumericTextBox runat="server" ID="radTermCredits_LT" MaxLength="5" TabIndex="10"
                    ClientEvents-OnKeyPress="keyPress" ClientEvents-OnLoad="setInitialValueForGreditsGPALT">
                </telerik:RadNumericTextBox>
            </div>
        </div>
    </div>
    <div id="TermGPAAvgRangeOptions" runat="server" class="DateOptionsSelector MultiFilterReportContainer" visible="true">
        <div id="TermGPAAvgRangeHeader" class="CaptionLabel" runat="server">Term GPA/Avg Range</div>
        <div class="FilterInput">
            <div id="TermGPAAvgRangeFields" class="DatePops">
                <asp:Label ID="Label2" runat="server" Text="Greater than or equal to" Width="150px"
                    Font-Bold="True" ForeColor="#000066" Style="padding-left: 20px;"></asp:Label>
                <telerik:RadNumericTextBox runat="server" ID="radTermGPA_GT" MaxLength="5" TabIndex="11"
                    ClientEvents-OnKeyPress="keyPress" ClientEvents-OnLoad="setInitialValueForGPAGT">
                </telerik:RadNumericTextBox>
                <asp:Label ID="Label3" runat="server" Text="Less than or equal to" Width="150px"
                    Font-Bold="True" ForeColor="#000066" Style="padding-left: 10px;"></asp:Label>
                <telerik:RadNumericTextBox runat="server" ID="radTermGPA_LT" MaxLength="5" TabIndex="12"
                    ClientEvents-OnKeyPress="keyPress" ClientEvents-OnLoad="setInitialValueForGPALT">
                </telerik:RadNumericTextBox>
            </div>
        </div>
    </div>
</asp:Panel>
