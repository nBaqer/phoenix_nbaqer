﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports FAME.Parameters.Collections
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities

Partial Class ParamListSelectorControl
    Inherits UserControl
    Implements IDataListControl
    Private UserId As Guid
#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _PostbackCtrlName As String
    Private _SavedSettings As ParamItemUserSettingsInfo
    Private _SendData As Boolean
    Private _SingleSelectionOnly As Boolean
    Private _SingleSelectMsg As String
    Public Property ItemDetail() As ParameterDetailItemInfo Implements IDataListControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property DAClass() As String Implements IDataListControl.DAClass
        Get
            Return _DAClass
        End Get
        Set(ByVal Value As String)
            _DAClass = Value
        End Set
    End Property
    Public Property DAMethod() As String Implements IDataListControl.DAMethod
        Get
            Return _DAMethod
        End Get
        Set(ByVal Value As String)
            _DAMethod = Value
        End Set
    End Property
    Public Property BindingTextField() As String Implements IDataListControl.BindingTextField
        Get
            Return _BindingTextField
        End Get
        Set(ByVal Value As String)
            _BindingTextField = Value
        End Set
    End Property
    Public Property BindingValueField() As String Implements IDataListControl.BindingValueField
        Get
            Return _BindingValueField
        End Get
        Set(ByVal Value As String)
            _BindingValueField = Value
        End Set
    End Property
    Public Property Caption() As String Implements IDataListControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property AssemblyFilePathDA() As String Implements IDataListControl.AssemblyFilePathDA
        Get
            Return _AssemblyFilePathDA
        End Get
        Set(ByVal Value As String)
            _AssemblyFilePathDA = Value
        End Set
    End Property
    Private Property AssemblyDA() As Assembly Implements IDataListControl.AssemblyDA
        Get
            Return _AssemblyDA

        End Get
        Set(ByVal Value As Assembly)
            _AssemblyDA = Value

        End Set
    End Property
    Public Property SqlConn() As String Implements IDataListControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Private Property PostbackCtrlName() As String
        Get
            Return _PostbackCtrlName
        End Get
        Set(ByVal Value As String)
            _PostbackCtrlName = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements IDataListControl.SavedSettings
        Get
            Return _SavedSettings
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSettings = value
        End Set
    End Property
    Public Property SendData() As Boolean Implements IDataListControl.SendData
        Get
            Return _SendData
        End Get
        Set(ByVal Value As Boolean)
            _SendData = Value
        End Set
    End Property
    Public Property SingleSelectionOnly() As Boolean
        Get
            Return _SingleSelectionOnly
        End Get
        Set(ByVal Value As Boolean)
            _SingleSelectionOnly = Value
        End Set
    End Property
    Public Property SingleSelectMsg() As String
        Get
            Return _SingleSelectMsg
        End Get
        Set(ByVal Value As String)
            _SingleSelectMsg = Value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            'Dim xxxx As String = ItemDetail.ItemName
            'If xxxx = "ProgramVersion" Then
            '    Dim yy As String = ""
            'End If
            UserId = New Guid(AdvantageSession.UserState.UserId.ToString)
            'UserId = New Guid("A64D1E87-9673-4FA6-B90F-8F7163EFC490")
            'UserId = New Guid("A11CB992-2538-49A6-A726-6FD8DAD050FC")
            If Page.IsPostBack = True Then
                PostbackCtrlName = Page.Request.Params.Get("__EVENTTARGET")

            Else

                PostbackCtrlName = String.Empty
            End If
            lblSingleSelectMsg.Text = String.Empty
            SetProperties()
            Fill()

            GetMasterCollection()
            GetMasterFilterCollection()

            RadListBoxCounts()
            HideTransferAllSingleSelect()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            CommonWebUtilities.DisplayErrorInMessageBox(Page, ex.Message)
            ' lblErrMsg.Text = ex.Message
            'Throw ex
        End Try
    End Sub
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim myItem As RadListBoxItem = e.Item
        Dim MyDataItem As Object = DirectCast(myItem.DataItem, Object)
        Try
            If TypeOf MyDataItem Is syCampGrp Then
                Dim MysyCampGrp As syCampGrp = DirectCast(MyDataItem, syCampGrp)
                If MysyCampGrp.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampGrp.CampGrpDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf MyDataItem Is arPrgVersion Then
                Dim MyarPrgVersion As arPrgVersion = DirectCast(MyDataItem, arPrgVersion)
                myItem.Text = "[" & MyarPrgVersion.PrgVerCode & "] " & MyarPrgVersion.PrgVerDescrip
                If MyarPrgVersion.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarPrgVersion.PrgVerDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf MyDataItem Is syStatusCode Then
                Dim MysyStatusCode As syStatusCode = DirectCast(MyDataItem, syStatusCode)
                If MysyStatusCode.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyStatusCode.StatusCodeDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf MyDataItem Is arTerm Then
                Dim MyarTerm As arTerm = DirectCast(MyDataItem, arTerm)
                If MyarTerm.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarTerm.TermDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf MyDataItem Is adLeadGroup Then
                Dim MyadLeadGroup As adLeadGroup = DirectCast(MyDataItem, adLeadGroup)
                If MyadLeadGroup.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyadLeadGroup.Descrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf MyDataItem Is syCampus Then
                Dim MysyCampus As syCampus = DirectCast(MyDataItem, syCampus)
                If MysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            Else
                Throw New Exception("unknown listbox data type")
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
        'Dim listbox As RadListBox = DirectCast(sender, RadListBox)
        'RadListBoxes_Sort(listbox, RadListBoxSort.Ascending)
        Fill()
        RadListBoxCounts()
    End Sub
    Protected Sub RadListBoxes_OnTransferring(ByVal sender As Object, ByVal e As RadListBoxTransferringEventArgs)
        If SingleSelectionOnly = True Then
            If e.Items.Count > 1 Then
                e.Cancel = True
                lblSingleSelectMsg.Text = SingleSelectMsg
            ElseIf e.Items.Count = 1 Then
                If RadListBox2.Items.Count = 1 And e.DestinationListBox.ID = "RadListBox2" Then
                    e.Cancel = True
                    lblSingleSelectMsg.Text = SingleSelectMsg
                Else
                    lblSingleSelectMsg.Text = String.Empty
                End If
            Else
                lblSingleSelectMsg.Text = String.Empty
            End If
        End If
    End Sub
    Protected Sub RadListBoxes_OnInserted(ByVal sender As Object, ByVal e As RadListBoxEventArgs)
        ' Dim listbox As RadListBox = DirectCast(sender, RadListBox)
        ' RadListBoxes_Sort(listbox, RadListBoxSort.Ascending)
    End Sub
    Protected Sub RadListBoxes_OnDropped(ByVal sender As Object, ByVal e As RadListBoxDroppedEventArgs)
        'Dim listbox As RadListBox = DirectCast(sender, RadListBox)
        'RadListBoxes_Sort(listbox, RadListBoxSort.Ascending)
    End Sub
    Protected Sub RadListBoxes_OnReOrdered(ByVal sender As Object, ByVal e As RadListBoxEventArgs)
        'Dim listbox As RadListBox = DirectCast(sender, RadListBox)
        'RadListBoxes_Sort(listbox, RadListBoxSort.Ascending)
    End Sub
    'Private Sub RadListBoxes_Sort(ByVal ctrl As RadListBox, ByVal sortorder As RadListBoxSort)
    '    ctrl.Sort = sortorder
    '    ctrl.SortItems()
    '    ctrl.SelectedIndex = 0
    'End Sub
    Protected Sub ValidateRequired(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        If RadListBox2.Items.Count > 0 Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub
    Protected Sub ValidateSingleSelect(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        If RadListBox2.Items.Count <= 1 Then
            args.IsValid = True
        Else
            args.IsValid = False
            Dim ctrl As Control = CType(sender, Control)
            Dim MyPanelBar As RadPanelBar = CType(ctrl.Parent.Parent.Parent.Parent.Parent.Parent, RadPanelBar)
            Dim ErrPanel As RadPanelItem = CType(ctrl.Parent.Parent.Parent.Parent.Parent, RadPanelItem)

            MyPanelBar.FindItemByValue(ErrPanel.Value).Selected = True
            MyPanelBar.FindItemByValue(ErrPanel.Value).ExpandParentItems()
        End If
    End Sub
    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Fill()
        RadListBoxCounts()
    End Sub
#End Region
    Private Sub GetMasterFilterCollection()
        Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName & "_Filters"), MasterDictionary)
        Dim objDetails As New DetailDictionary
        Dim ReturnValues As ParamValueDictionary
        Try
            ReturnValues = CreateValues()
            If Not objMaster Is Nothing Then
                If objMaster.Contains(ItemDetail.ItemName) Then
                    objDetails = DirectCast(objMaster.Item(ItemDetail.ItemName), DetailDictionary)
                End If
                If Not objDetails Is Nothing Then
                    If objDetails.Contains(ItemDetail.ItemName) Then
                        'take out old values if they exist
                        objDetails.Remove(ItemDetail.ItemName)
                    End If
                    If ReturnValues.Count > 0 Then
                        'add new values if there are any
                        objDetails.Add(ItemDetail.ItemName, ReturnValues)
                        objMaster.Remove(ItemDetail.ItemName)
                        objMaster.Add(ItemDetail.ItemName, objDetails)
                    End If

                    'store master collection in session
                    Session(ItemDetail.SetName & "_Filters") = objMaster
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub GetMasterCollection()
        Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName), MasterDictionary)
        Dim objDetails As New DetailDictionary
        Dim ReturnValues As ParamValueDictionary
        Try
            ReturnValues = CreateValues()
            If Not objMaster Is Nothing Then
                If objMaster.Contains(ItemDetail.ItemName) Then
                    objDetails = DirectCast(objMaster.Item(ItemDetail.ItemName), DetailDictionary)
                End If
                If Not objDetails Is Nothing Then
                    If objDetails.Contains(ItemDetail.ItemName) Then
                        'take out old values if they exist
                        objDetails.Remove(ItemDetail.ItemName)
                    End If
                    If ReturnValues.Count > 0 Then
                        'add new values if there are any
                        objDetails.Add(ItemDetail.ItemName, ReturnValues)
                        objMaster.Remove(ItemDetail.ItemName)
                        objMaster.Add(ItemDetail.ItemName, objDetails)
                    End If

                    'store master collection in session
                    Session(ItemDetail.SetName) = objMaster
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Function CreateValues() As ParamValueDictionary
        Dim ReturnValues As New ParamValueDictionary
        Try
            ReturnValues = FillValueDictionary()
            ReturnValues.Name = ItemDetail.ReturnValueName
            ReturnValues.CtrlName = ItemDetail.ItemName
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return ReturnValues
    End Function
    Private Function GetAllDataFilters() As MasterDictionary
        Dim objFilters As New DetailDictionary
        Dim objDetails As New DetailDictionary
        'objDetails = DirectCast(Session("CollectionSet_" & ItemDetail.SetName), DetailDictionary)
        Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName & "_Filters"), MasterDictionary)
        'Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName), MasterDictionary)
        'objDetails.Name = ItemDetail.ItemName

        If objMaster.Contains(ItemDetail.ItemName) Then
            ' Dim x As String = ""
            objDetails = DirectCast(objMaster.Item(ItemDetail.ItemName), DetailDictionary)
        End If


        Dim StatusValueDictionary As New ParamValueDictionary()
        Dim UserIdValueDictionary As New ParamValueDictionary()
        Dim MainCampusSelectValueDictionary As New ParamValueDictionary()
        Dim CurrentlySelectedValueDictionary As New ParamValueDictionary()
        Try
            objFilters.Name = ItemDetail.ItemName
            Dim SourceListBoxName As String = GetSourceListBox()
            For Each detail As DetailDictionary In objMaster.Values
                For Each p As ParamValueDictionary In detail.Values
                    'Dim yyy As String = p.CtrlName
                    If objFilters.Contains(p.Name) Then
                        objFilters.Remove(p.Name)
                    End If
                    objFilters.Add(p.Name, p)
                    If SourceListBoxName.Contains("RadListBox2") Then
                        'Dim MyListBox As New RadListBox
                        'MyListBox = CType(Page.FindControl(PostbackCtrlName.Replace("RadListBox1", "RadListBox2")), RadListBox)
                        'Dim x As New List(Of String)
                        'Dim y As New List(Of String)
                        'For Each Myitem As RadListBoxItem In MyListBox.Items

                        '    x.Add(Myitem.Value.ToString)
                        'Next
                        If PostbackCtrlName.Contains(p.CtrlName) Then
                            objFilters.Remove(p.Name)
                        End If

                    End If

                Next
            Next
            '$$$$$
            If Not SourceListBoxName.Contains("RadListBox2") Then
                If RadListBox2.Items.Count > 0 Then
                    CurrentlySelectedValueDictionary = GetCurrentlySelectedFilters(RadListBox2)
                    If objFilters.Contains("CurrentlySelected") Then
                        objFilters.Remove("CurrentlySelected")
                    End If
                    objFilters.Add("CurrentlySelected", CurrentlySelectedValueDictionary)
                End If
            Else
                If objFilters.Contains("CurrentlySelected") Then
                    objFilters.Remove("CurrentlySelected")
                End If
                CurrentlySelectedValueDictionary = GetCurrentlySelectedFilters(RadListBox2)
                objFilters.Add("CurrentlySelected", CurrentlySelectedValueDictionary)
            End If

            StatusValueDictionary = GetStatusFilters(CheckBox1.Checked)
            If objFilters.Contains("Status") Then
                objFilters.Remove("Status")
            End If
            objFilters.Add("Status", StatusValueDictionary)
            UserIdValueDictionary = GetUserFilters(UserId)
            If objFilters.Contains("UserId") Then
                objFilters.Remove("UserId")
            End If
            objFilters.Add("UserId", UserIdValueDictionary)

            MainCampusSelectValueDictionary = GetMainCampusSelect()
            If objFilters.Contains("MainCampusIdSelect") Then
                objFilters.Remove("MainCampusIdSelect")
            End If
            objFilters.Add("MainCampusIdSelect", MainCampusSelectValueDictionary)
            If objMaster.Contains(ItemDetail.ItemName) Then
                objMaster.Remove(ItemDetail.ItemName)
            End If
            objMaster.Add(ItemDetail.ItemName, objFilters)
            Session(ItemDetail.SetName & "_Filters") = objMaster
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
            ' Dim x As String = ex.ToString
        End Try
        Return objMaster
    End Function

    Private Function GetMainCampusSelect() As ParamValueDictionary
        Dim valueDictionary As New ParamValueDictionary
        try
        Dim modifierDictionary As New ModDictionary
        Dim campusSelectValues As New ParamValueList(Of String)
            campusSelectValues.Add(AdvantageSession.UserState.CampusId.ToString)
        modifierDictionary.Name = "InList"
        modifierDictionary.Add("MainCampusSelectId", campusSelectValues)
        valueDictionary.Add(ModDictionary.Modifier.InList, modifierDictionary)
        valueDictionary.Name = "MainCampusSelectId"
        valueDictionary.CtrlName = ItemDetail.ItemName
        Catch ex As Exception
        Dim exTracker = new AdvApplicationInsightsInitializer()
        exTracker.TrackExceptionWrapper(ex)

        Throw ex
            Return valueDictionary
        End Try
        Return valueDictionary

    End Function


    Private Function FillValueDictionary() As ParamValueDictionary

        Dim ValueDictionary As New ParamValueDictionary
        Dim ModifierDictionary As New ModDictionary
        Dim CurrentlyTransferredValues As List(Of String) = GetCurrentlyTransferredValues()
        Dim SourceListBoxName As String = GetSourceListBox()
        Dim ItemsToAdd As New List(Of String)
        Dim ItemsToRemove As New List(Of String)
        Dim CurrentRadListBoxValues As New List(Of String)

        For Each item As RadListBoxItem In RadListBox2.Items
            CurrentRadListBoxValues.Add(item.Value)
        Next
        If PostbackCtrlName.Contains(ItemDetail.ItemName) Then
            If SourceListBoxName.Contains("RadListBox2") Then
                ItemsToRemove = CurrentlyTransferredValues
                For Each deleted As String In ItemsToRemove
                    CurrentRadListBoxValues.Remove(deleted)
                Next
            End If
            If SourceListBoxName.Contains("RadListBox1") Then
                ItemsToAdd = CurrentlyTransferredValues
            End If
        End If
        Try
            Select Case ItemDetail.ValueProp
                Case ParameterDetailItemInfo.ItemValueType.String
                    Dim PValues As New ParamValueList(Of String)
                    For Each item As String In CurrentRadListBoxValues
                        PValues.Add(item)
                    Next
                    For Each str As String In ItemsToAdd
                        PValues.Add(str)
                    Next
                    PValues.DataType = ValueList.ValueType.String
                    If PValues.Count > 0 Then
                        ModifierDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Long
                    Dim PValues As New ParamValueList(Of Long)
                    For Each item As String In CurrentRadListBoxValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    PValues.DataType = ValueList.ValueType.Long
                    If PValues.Count > 0 Then
                        ModifierDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Decimal
                    Dim PValues As New ParamValueList(Of Decimal)
                    For Each item As String In CurrentRadListBoxValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    PValues.DataType = ValueList.ValueType.Decimal
                    If PValues.Count > 0 Then
                        ModifierDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Date
                    Dim PValues As New ParamValueList(Of Date)
                    For Each item As String In CurrentRadListBoxValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsDate(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    PValues.DataType = ValueList.ValueType.Date
                    If PValues.Count > 0 Then
                        ModifierDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Integer
                    Dim PValues As New ParamValueList(Of Integer)
                    For Each item As String In CurrentRadListBoxValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    PValues.DataType = ValueList.ValueType.Integer
                    If PValues.Count > 0 Then
                        ModifierDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Boolean
                    Dim PValues As New ParamValueList(Of Boolean)
                    For Each item As String In CurrentRadListBoxValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsBoolean(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    PValues.DataType = ValueList.ValueType.Boolean
                    If PValues.Count > 0 Then
                        ModifierDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Guid
                    Dim PValues As New ParamValueList(Of Guid)
                    For Each item As String In CurrentRadListBoxValues
                        'If PValues.Contains(item) Then
                        'Else
                        PValues.Add(New Guid(item))
                        ' End If
                    Next
                    For Each str As String In ItemsToAdd
                        If IsGuid(str) = True Then
                            'If Not PValues.Contains(str) Then
                            PValues.Add(New Guid(str))
                            'End If
                        End If
                    Next
                    PValues.DataType = ValueList.ValueType.Guid
                    If PValues.Count > 0 Then
                        ModifierDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case Else
                    Throw New Exception("Can not create parameter value collection: Unknown type")
            End Select
            If ModifierDictionary.Count > 0 Then
                ValueDictionary.Add(ModDictionary.Modifier.InList, ModifierDictionary)
            End If
            Return ValueDictionary
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetReturnValues() As DetailDictionary Implements IDataListControl.GetReturnValues
        Dim objDetails As New DetailDictionary
        Dim returnValues As ParamValueDictionary
        Try
            If SetFilterMode() = True Then

                returnValues = CreateValues()
                If returnValues.Count > 0 Then
                    objDetails.Add(ItemDetail.ItemName, returnValues)
                End If

            End If
            Return objDetails
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try

    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements IDataListControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Try
            If SetFilterMode() = True Then

                userSettings.ItemName = ItemDetail.ItemName
                userSettings.ItemId = ItemDetail.ItemId
                userSettings.DetailId = ItemDetail.DetailId
                userSettings.FriendlyName = ItemDetail.CaptionOverride

                Dim SettingsCollection2 As New List(Of ControlSettingInfo)
                Dim ctrlSetting1 As New ControlSettingInfo
                Dim ctrlSetting2 As New ControlSettingInfo
                Dim ctrlValues1 As New List(Of ControlValueInfo)
                Dim ctrlValues2 As New List(Of ControlValueInfo)

                ctrlSetting1.ControlName = RadListBox1.ID
                For Each item As RadListBoxItem In RadListBox1.Items
                    Dim objControlValue As New ControlValueInfo
                    objControlValue.DisplayText = item.Text.Replace("'", "''")
                    objControlValue.KeyData = item.Value.Replace("'", "''")
                    ctrlValues1.Add(objControlValue)
                Next
                ctrlSetting2.ControlName = RadListBox2.ID
                For Each item As RadListBoxItem In RadListBox2.Items
                    Dim objControlValue As New ControlValueInfo
                    objControlValue.DisplayText = item.Text.Replace("'", "''")
                    objControlValue.KeyData = item.Value.Replace("'", "''")
                    ctrlValues2.Add(objControlValue)
                Next

                ctrlSetting1.ControlValueCollection = ctrlValues1
                ctrlSetting2.ControlValueCollection = ctrlValues2
                SettingsCollection2.Add(ctrlSetting1)
                SettingsCollection2.Add(ctrlSetting2)
                userSettings.ControlSettingsCollection = SettingsCollection2

                Return userSettings
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Public Sub LoadSavedReportSettings() Implements IDataListControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadListBox Then
                            Dim radBox As RadListBox = DirectCast(ctrl, RadListBox)
                            radBox.Items.Clear()
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                Dim savedListBoxItem As New RadListBoxItem
                                savedListBoxItem.Text = itemValue.DisplayText
                                savedListBoxItem.Value = itemValue.KeyData

                                radBox.Items.Add(savedListBoxItem)
                            Next
                        Else
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadListBox1.Items.Clear()
                RadListBox2.Items.Clear()
            End If
            'Call Fill()
            Call RadListBoxCounts()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Public Function SetFilterMode() As Boolean Implements IFilterControl.SetFilterMode
        Dim rValue As Boolean
        Try
            Dim blnQuickFilterMode As Boolean = CType(Session("QuickFilterMode"), Boolean)

            If ItemDetail.SectionType = ParameterSectionInfo.SecType.Quick Then
                If RadListBox2.Items.Count > 0 Then
                    blnQuickFilterMode = True
                    rValue = True
                Else
                    blnQuickFilterMode = False
                    rValue = False
                End If
            Else
                If ItemDetail.SectionType = ParameterSectionInfo.SecType.Advanced AndAlso blnQuickFilterMode = True Then
                    rValue = False
                Else
                    rValue = True
                End If
            End If

            Session("QuickFilterMode") = blnQuickFilterMode
            Return rValue
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Public Sub ClearControls() Implements IDataListControl.ClearControls
        'Dim radbox1 As RadListBox = DirectCast(Me.FindControl("RadListBox1"), RadListBox)
        'Dim radbox2 As RadListBox = DirectCast(Me.FindControl("RadListBox2"), RadListBox)
        RadListBox1.Items.Clear()
        RadListBox2.Items.Clear()
    End Sub

    Private Function IsGuid(ByVal value As String) As Boolean
        Dim myGuid As Guid = Nothing
        Return Guid.TryParse(value, myGuid)
        'Try
        '    myGuid = New Guid(value)
        '    Return True
        'Catch ex As Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    Return False
        'End Try
    End Function
    Private Function IsBoolean(ByVal value As String) As Boolean
        Dim myBoolean As Boolean
        Return Boolean.TryParse(value, myBoolean)
        'Try
        '    myBoolean = CBool(value)
        '    Return True
        'Catch ex As Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    Return False
        'End Try
    End Function
    Private Function GetCurrentlyTransferredValues() As List(Of String)
        Try
            'Dim txtbox As RadTextBox = CType(Me.Parent.Parent.Parent.Parent.FindControl("CurrentlyTransferredValues"), RadTextBox)
            Dim txtbox As RadTextBox = CType(Me.Page.Controls(0).FindControl("ContentMain2").FindControl("CurrentlyTransferredValuesPage"), RadTextBox)
            'Dim x As String = txtbox.Text
            'Dim y As String = txtbox.ClientID
            Dim transferredValues As New List(Of String)
            If Not txtbox.Text = String.Empty Then
                Dim transferred As String() = txtbox.Text.Split(New Char() {CChar(",")}, StringSplitOptions.RemoveEmptyEntries)
                For Each tran As String In transferred
                    transferredValues.Add(tran)
                Next
            End If
            Return transferredValues
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetSourceListBox() As String
        Try
            'Dim txtbox As RadTextBox = CType(Me.Parent.Parent.Parent.Parent.FindControl("SourceListBox"), RadTextBox)
            Dim txtbox As RadTextBox = CType(Me.Page.Controls(0).FindControl("ContentMain2").FindControl("SourceListBoxPage"), RadTextBox)
            Return txtbox.Text
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            If (ctrl Is Nothing) Then
            Else
                Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)
                If prop.ValueType = "Boolean" Then
                    p.SetValue(ctrl, CBool(prop.Value), Nothing)
                ElseIf prop.ValueType = "Integer" Then
                    p.SetValue(ctrl, CInt(prop.Value), Nothing)
                ElseIf prop.ValueType = "Date" Then
                    p.SetValue(ctrl, CDate(prop.Value), Nothing)
                ElseIf prop.ValueType = "Enum" Then
                    p.SetValue(ctrl, CInt(prop.Value), Nothing)
                Else
                    p.SetValue(ctrl, prop.Value, Nothing)
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub

    Private Function GetCurrentlySelectedFilters(ByVal ctrl As RadListBox) As ParamValueDictionary
        Dim valueDictionary As New ParamValueDictionary
        Dim modifierDictionary As New ModDictionary
        Dim radListBoxValues As New ParamValueList(Of String)
        For Each item As RadListBoxItem In ctrl.Items
            radListBoxValues.Add(item.Value)
        Next
        modifierDictionary.Name = "InList"
        modifierDictionary.Add("CurrentlySelected", radListBoxValues)
        valueDictionary.Add(ModDictionary.Modifier.InList, modifierDictionary)
        valueDictionary.Name = "CurrentlySelected"
        valueDictionary.CtrlName = ItemDetail.ItemName
        Return valueDictionary
    End Function
    'Private Function GetFiltersValuesFromControl(ByVal ctrl As RadListBox, ByVal filtername As String, ByVal ctrlname As String) As ParamValueDictionary
    '    Dim ValueDictionary As New ParamValueDictionary
    '    Dim ModifierDictionary As New ModDictionary
    '    Dim RadListBoxValues As New ParamValueList(Of String)
    '    For Each item As RadListBoxItem In ctrl.Items
    '        'Dim 
    '        RadListBoxValues.Add(item.Value)
    '    Next
    '    ModifierDictionary.Name = "InList"
    '    ModifierDictionary.Add(filtername, RadListBoxValues)
    '    ValueDictionary.Add(ModDictionary.Modifier.InList, ModifierDictionary)
    '    ValueDictionary.Name = filtername
    '    ValueDictionary.CtrlName = ctrlname
    '    Return ValueDictionary
    'End Function
    Private Function GetStatusFilters(ByVal ShowInActive As Boolean) As ParamValueDictionary
        Dim valueDictionary As New ParamValueDictionary
        Dim modifierDictionary As New ModDictionary
        Dim statusValues As New ParamValueList(Of String)
        Try
            If ShowInActive = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
                ' StatusValues.Add("Inactive")
            End If
            modifierDictionary.Name = "InList"
            modifierDictionary.Add("Status", statusValues)
            valueDictionary.Add(ModDictionary.Modifier.InList, modifierDictionary)
            valueDictionary.Name = "Status"
            valueDictionary.CtrlName = ItemDetail.ItemName
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return valueDictionary
    End Function
    Private Function GetUserFilters(ByVal UserId As Guid) As ParamValueDictionary
        Dim valuedictionary As New ParamValueDictionary
        Dim modifierDictionary As New ModDictionary
        Dim userIds As New ParamValueList(Of Guid)
        Try


            userIds.Add(UserId)
            modifierDictionary.Name = "InList"
            modifierDictionary.Add("UserId", userIds)
            valuedictionary.Add(ModDictionary.Modifier.InList, modifierDictionary)
            valuedictionary.Name = "UserId"
            valuedictionary.CtrlName = ItemDetail.ItemName

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        Return valuedictionary
    End Function
    Public Sub Fill()
        Try
            RadListBox1.DataTextField = BindingTextField
            RadListBox1.DataValueField = BindingValueField
            RadListBox1.DataSource = GetDisplayData(GetAllDataFilters)
            RadListBox1.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Function GetDisplayData(ByVal filters As MasterDictionary) As IQueryable
        Dim ty As Type
        Dim mm As MethodInfo
        Dim DAobj As Object
        Dim Query As IQueryable
        Try

            'If SendData = True Then
            If Session("Type_" & ItemDetail.ItemName) Is Nothing Then
                AssemblyDA = Assembly.LoadFrom(Server.MapPath(AssemblyFilePathDA))
                ty = AssemblyDA.GetType(DAClass, True)
                Session("Type_" & ItemDetail.ItemName) = ty
            Else
                ty = CType(Session("Type_" & ItemDetail.ItemName), Type)
            End If
            mm = ty.GetMethod(DAMethod)
            If (mm Is Nothing) Then
            Else
                DAobj = Activator.CreateInstance(ty, SqlConn)
                Dim params As Object() = {filters}
                Query = CType(mm.Invoke(DAobj, params), IQueryable)
                Return Query
            End If

            ' Else
             Return Nothing
            ' End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw new ApplicationException(String.Format("Error at ParamListSelectorControl.ascx.vb GetDisplayData. {0}",ex.Message))
        End Try
    End Function
    Private Sub HideTransferAllSingleSelect()
        If SingleSelectionOnly = True Then
            RadListBox1.ButtonSettings.ShowTransferAll = False
            RadListBox2.ButtonSettings.ShowTransferAll = False
        End If
    End Sub
    Private Sub RadListBoxCounts()
        Dim boxcount1 As Integer = RadListBox1.Items.Count()
        Dim boxcount2 As Integer = RadListBox2.Items.Count()
        Dim available As String
        Dim selected As String
        available = boxcount1.ToString
        selected = boxcount2.ToString

        lblListBoxCounter1.Text = available.Trim & " available"
        lblListBoxCounter1.ToolTip = available.Trim & " available " & Caption & "(s)"
        lblListBoxCounter2.Text = selected.Trim & " selected"
        lblListBoxCounter2.ToolTip = selected.Trim & " selected " & Caption & "(s)"
    End Sub
End Class
