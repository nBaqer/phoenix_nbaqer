﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WeeklyAttendanceMultiple.ascx.vb" Inherits="WeeklyAttendanceMultiple" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<asp:Panel ID="MainPanel" runat="server">
    <div><span style="color: #b71c1c">*</span> All filters are required and the filters need to be selected in the following order.</div>
    <div id="MainContainer" class="MainContainer">
        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer">
            <div id="CampusHeader" class="CaptionLabel" runat="server">1. Campus <span style="color: #b71c1c">*</span></div>
            <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true" CssClass="ManualInput ReportLeftMarginInput"
                OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged"
                Width="350px" ToolTip="Select a campus to run the report">
            </telerik:RadComboBox>
        </div>
        <div id="DateOptionsSelector" class="DateOptionsSelector MultiFilterReportContainer" runat="server">
            <div id="DateOptionsHeader" class="CaptionLabel" runat="server">2. Attendance for week ending <span style="color: #b71c1c">*</span></div>
            <div id="DatePopUps" class="DatePops">
                <telerik:RadComboBox ID="radWeekEndingDateOperator" CssClass="LabelFilterInput ReportLeftMarginInput" runat="server" Visible="true" Width="200px">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="Week of" Value="0" />
                    </Items>
                </telerik:RadComboBox>
                <telerik:RadDatePicker ID="RadWeekEndingDate" AutoPostBack="True" runat="server" CssClass="ManualInput"
                    Width="151px" DateInput-ReadOnly="true" OnSelectedDateChanged="RadWeekEndingDate_OnSelectedDateChanged">
                    <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                    <DatePopupButton></DatePopupButton>

                    <DateInput runat="server" DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy"></DateInput>
                </telerik:RadDatePicker>
            </div>
        </div>
        <div id="EnrollmentStatusSelector" class="TermSelector MultiFilterReportContainer" runat="server">
            <div id="Div6" class="CaptionLabel" runat="server">3. Enrollment Status</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxEnrollmentStatus" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxEnrollmentStatus2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxEnrollmentStatus2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveEnrollmentStatus" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="lblEnrollmentStatusCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="lblEnrollmentStatusCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>
        <div id="TermSelector" class="TermSelector MultiFilterReportContainer" runat="server">
            <div id="Div4" class="CaptionLabel" runat="server">4. Term</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxTerms" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxTerms2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxTerms2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="chkInActiveTerms" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="lblTermsCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="lblTermsCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>
        <div id="CourseSelector" class="ProgramSelector MultiFilterReportContainer" runat="server">
            <div id="captionlabel2" class="CaptionLabel" runat="server">5. Course</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxCourse" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxCourse2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxCourse2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
                </telerik:RadListBox>
                <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveCourses" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="ProgramCounterAvailable" class="RadListBox1Counter">
                        <asp:Label ID="lblCourseCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="ProgramCounterSelected" class="RadListBox2Counter">
                        <asp:Label ID="lblCourseCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>
        <div id="InstructorSelector" class="TermSelector MultiFilterReportContainer" runat="server">
            <div id="Div3" class="CaptionLabel" runat="server">6. Instructor</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxInstructor" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxInstructor2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxInstructor2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInActiveInstructor" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="lblInstructorCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="lblInstructorCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>
        <div id="ShiftSelector" class="TermSelector MultiFilterReportContainer" runat="server">
            <div id="Div7" class="CaptionLabel" runat="server">7. Shift</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxShift" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxShift2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxShift2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveShift" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="lblShiftCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="lblShiftCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>
        <div id="ClassSelector" class="TermSelector MultiFilterReportContainer" runat="server">
            <div id="Div2" class="CaptionLabel" runat="server">8. Section</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxSection" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxSection2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxSection2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <%--<asp:CheckBox ID="cbkInactiveClass" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />--%>
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="lblSectionCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="lblSectionCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>
        <br />
    </div>

</asp:Panel>
