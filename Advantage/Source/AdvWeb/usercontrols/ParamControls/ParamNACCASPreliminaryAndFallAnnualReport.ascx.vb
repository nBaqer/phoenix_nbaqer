﻿
Imports System.Data
Imports System.Diagnostics
Imports System.Reflection
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Parameters.Info
Imports FAME.Parameters.Interfaces
Imports Telerik.Web.UI

Partial Class ParamNACCASPreliminaryAndFallAnnualReport
    Inherits System.Web.UI.UserControl
    Implements ICustomControl
    Protected MyAdvAppSettings As AdvAppSettings
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    Private _ItemDetail As ParameterDetailItemInfo

    Protected CampusId As String

    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property

    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = value
        End Set
    End Property

    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property

    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        SetProperties()
        If Not Page.IsPostBack Then
            RadComboCampus.SelectedValue = HttpContext.Current.Request.Params("cmpid").ToString
            FillReportTypeControl()
            FillReportNameControl()
            FillYearControl()
            FillCampusControl()
            FillProgramVersionsControl()

        End If
    End Sub

    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride

            'Report Type
            Dim ctrlSetting1 As New ControlSettingInfo
            Dim ctrlValues1 As New List(Of ControlValueInfo)
            Dim objControlValue1 As New ControlValueInfo

            ctrlSetting1.ControlName = RadComboReportType.ID
            objControlValue1.DisplayText = RadComboReportType.Text
            objControlValue1.KeyData = RadComboReportType.SelectedValue
            ctrlValues1.Add(objControlValue1)
            ctrlSetting1.ControlValueCollection = ctrlValues1
            settingsCollection.Add(ctrlSetting1)

            'Report Name
            Dim ctrlSetting2 As New ControlSettingInfo
            Dim ctrlValues2 As New List(Of ControlValueInfo)
            Dim objControlValue2 As New ControlValueInfo

            ctrlSetting2.ControlName = RadComboReportName.ID
            objControlValue2.DisplayText = RadComboReportName.Text
            objControlValue2.KeyData = RadComboReportName.SelectedValue
            ctrlValues2.Add(objControlValue2)
            ctrlSetting2.ControlValueCollection = ctrlValues2
            settingsCollection.Add(ctrlSetting2)

            'Year
            Dim ctrlSetting3 As New ControlSettingInfo
            Dim ctrlValues3 As New List(Of ControlValueInfo)
            Dim objControlValue3 As New ControlValueInfo

            ctrlSetting3.ControlName = RadComboYear.ID
            objControlValue3.DisplayText = RadComboYear.Text
            objControlValue3.KeyData = RadComboYear.SelectedValue
            ctrlValues3.Add(objControlValue3)
            ctrlSetting3.ControlValueCollection = ctrlValues3
            settingsCollection.Add(ctrlSetting3)

            'Campus
            Dim ctrlSetting4 As New ControlSettingInfo
            Dim ctrlValues4 As New List(Of ControlValueInfo)
            Dim objControlValue4 As New ControlValueInfo

            ctrlSetting4.ControlName = RadComboCampus.ID
            objControlValue4.DisplayText = RadComboCampus.Text
            objControlValue4.KeyData = RadComboCampus.SelectedValue
            ctrlValues4.Add(objControlValue4)
            ctrlSetting4.ControlValueCollection = ctrlValues4
            settingsCollection.Add(ctrlSetting4)

            'Show Inactive
            Dim ctrlSetting5 As New ControlSettingInfo
            Dim ctrlValues5 As New List(Of ControlValueInfo)
            Dim objControlValue5 As New ControlValueInfo

            ctrlSetting5.ControlName = chkInActiveProgramVersions.ID
            objControlValue5.DisplayText = chkInActiveProgramVersions.Text
            objControlValue5.KeyData = chkInActiveProgramVersions.Checked
            ctrlValues5.Add(objControlValue5)
            ctrlSetting5.ControlValueCollection = ctrlValues5
            settingsCollection.Add(ctrlSetting5)

            'Program Versions
            Dim ctrlSettingProgramVersions As New ControlSettingInfo
            Dim ctrlValuesProgramVersions As New List(Of ControlValueInfo)
            ctrlSettingProgramVersions.ControlName = RadListBoxPrograms2.ID
            For Each item As RadListBoxItem In RadListBoxPrograms2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgramVersions.Add(objControlValueInfo3)
            Next
            ctrlSettingProgramVersions.ControlValueCollection = ctrlValuesProgramVersions
            settingsCollection.Add(ctrlSettingProgramVersions)

            userSettings.ControlSettingsCollection = settingsCollection
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
        End Try
        Return userSettings

    End Function

    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim displayTree As New RadTreeView
        displayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            'Report Type
            If RadComboReportType.SelectedValue <> "" Then
                Dim reportname As String = Page.Title
                Dim headerReportTypeNode2 As New RadTreeNode
                headerReportTypeNode2.Text = "Report Type"
                headerReportTypeNode2.Value = "ReportType"
                headerReportTypeNode2.CssClass = "TreeParentNode"
                Dim reportTypeNode As New RadTreeNode
                reportTypeNode.Text = RadComboReportType.SelectedItem.Text
                headerReportTypeNode2.Nodes.Add(reportTypeNode)

                displayTree.Nodes.Add(headerReportTypeNode2)
            End If

            'Report Name
            If RadComboReportName.SelectedValue <> "" Then
                Dim reportname As String = Page.Title
                Dim headerReportTNameypeNode2 As New RadTreeNode
                headerReportTNameypeNode2.Text = "Report Name"
                headerReportTNameypeNode2.Value = "ReportName"
                headerReportTNameypeNode2.CssClass = "TreeParentNode"
                Dim reportNameNode As New RadTreeNode
                reportNameNode.Text = RadComboReportName.SelectedItem.Text
                headerReportTNameypeNode2.Nodes.Add(reportNameNode)

                displayTree.Nodes.Add(headerReportTNameypeNode2)
            End If


            'Report Year
            If RadComboYear.SelectedValue <> "" Then
                Dim reportname As String = Page.Title
                Dim headerReportYearNode2 As New RadTreeNode
                headerReportYearNode2.Text = "Year"
                headerReportYearNode2.Value = "Year"
                headerReportYearNode2.CssClass = "TreeParentNode"
                Dim reportYearNode As New RadTreeNode
                reportYearNode.Text = RadComboYear.SelectedItem.Text
                headerReportYearNode2.Nodes.Add(reportYearNode)

                displayTree.Nodes.Add(headerReportYearNode2)
            End If

            'Campus
            If RadComboCampus.SelectedValue <> "" Then
                Dim reportname As String = Page.Title
                Dim headerCampusNode2 As New RadTreeNode
                headerCampusNode2.Text = "Campus"
                headerCampusNode2.Value = "Campus"
                headerCampusNode2.CssClass = "TreeParentNode"
                Dim campusNode As New RadTreeNode
                campusNode.Text = RadComboCampus.SelectedItem.Text
                headerCampusNode2.Nodes.Add(campusNode)

                displayTree.Nodes.Add(headerCampusNode2)
            End If

            'Program
            If RadListBoxPrograms2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim headerNode3 As New RadTreeNode
                headerNode3.Text = "Program Version"
                headerNode3.Value = "ProgramVersion"
                headerNode3.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxPrograms2.Items
                    Dim programVersionNode As New RadTreeNode
                    programVersionNode.Text = item.Text
                    headerNode3.Nodes.Add(programVersionNode)
                Next
                displayTree.Nodes.Add(headerNode3)
            End If

            Return displayTree
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Public Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim radioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                radioButList.SelectedValue = itemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim comboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                If Not itemValue.KeyData = "1900" Then
                                    comboBox.SelectedValue = itemValue.KeyData
                                End If
                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim lstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            lstBox.Items.Clear()
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = itemValue.DisplayText
                                newItem.Value = itemValue.KeyData
                                lstBox.SelectedValue = itemValue.KeyData
                                lstBox.Items.Add(newItem)
                            Next
                        ElseIf TypeOf ctrl Is RadCheckBox Then
                            Dim chkbox As RadCheckBox = DirectCast(ctrl, RadCheckBox)
                            chkbox.Checked = Convert.ToBoolean(setting.ControlValueCollection.FirstOrDefault().KeyData)
                        ElseIf TypeOf ctrl Is CheckBox Then
                            Dim chkbox As CheckBox = DirectCast(ctrl, CheckBox)
                            chkbox.Checked = Convert.ToBoolean(setting.ControlValueCollection.FirstOrDefault().KeyData)
                        Else
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
            End If

            FillProgramVersionsControl()

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

#Region "Others"
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim statusValues As New List(Of String)
        Try
            Dim cKbox As RadCheckBox = DirectCast(Me.FindControl(ctrlName), RadCheckBox)
            If cKbox.Checked = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        Return statusValues
    End Function

    Private Sub SetProperties()
        Try
            'RadDateReportDate.MaxDate = DateTime.Now
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub

    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub

    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub

    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub RadComboReportType_OnItemDataBoundComboBox(sender As Object, e As RadComboBoxItemEventArgs)

    End Sub

    Protected Sub ReportTypeComboBox_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

    End Sub

    Protected Sub ReportNameComboBox_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

    End Sub

    Protected Sub RadComboReportName_OnItemDataBoundComboBox(sender As Object, e As RadComboBoxItemEventArgs)

    End Sub

    Protected Sub YearComboBox_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

    End Sub

    Protected Sub RadComboYear_OnItemDataBoundComboBox(sender As Object, e As RadComboBoxItemEventArgs)

    End Sub

    Protected Sub CampusComboBox_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        RadListBoxPrograms2.Items.Clear()
        FillProgramVersionsControl()
    End Sub

    Protected Sub CampusReportYear_OnItemDataBoundComboBox(sender As Object, e As RadComboBoxItemEventArgs)

    End Sub

    Protected Sub RadComboProgram_OnSelectedIndexChangedComboBox(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

    End Sub

    Protected Sub RadComboCampus_OnItemDataBound(sender As Object, e As RadComboBoxItemEventArgs)
        
    End Sub

    Protected Sub RandComboProgram_OnItemDataBound(sender As Object, e As RadComboBoxItemEventArgs)

    End Sub

    Protected Sub chkInActiveProgramVersions_OnCheckedChangedCheckBox_CheckedChanged(sender As Object, e As EventArgs)
        FillProgramVersionsControl()
    End Sub

    Protected Sub RadListProgramVersionBoxes_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)

    End Sub

    Protected Sub RadListBoxes_ItemDataBound(sender As Object, e As RadListBoxItemEventArgs)

    End Sub
#End Region

#Region "Fill Data"
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()

            CampusId = RadComboCampus.SelectedValue
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Sub

    Public Sub FillReportTypeControl()
        Try
            Dim reportTypes As Dictionary(Of String, String) = New Dictionary(Of String, String) From {{"Preliminary", "Preliminary Annual"}, {"FallAnnual", "Fall Annual"}}
            RadComboReportType.DataTextField = "value"
            RadComboReportType.DataValueField = "key"
            RadComboReportType.DataSource = reportTypes
            RadComboReportType.DataBind()
            RadComboReportType.SelectedValue = "FallAnnual"
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Sub

    Public Sub FillYearControl()
        Try
            Dim currentYear = DateTime.Now.Year
            RadComboYear.DataTextField = "YearId"
            RadComboYear.DataValueField = "YearId"
            Dim dt As New DataTable
            dt.Columns.Add("YearId")
            For index As Integer = 1 To 4
                dt.Rows.Add(currentYear - index)
            Next
            dt.Rows.Add(currentYear)
            For i As Integer = 1 To 2
                dt.Rows.Add(currentYear + i)
            Next
            dt.DefaultView.Sort = "YearId Desc"
            dt = dt.DefaultView.toTable
            RadComboYear.DataSource = dt
            RadComboYear.DataBind()
            RadComboYear.SelectedValue = RadComboYear.Items.FindItemByText(currentYear).Value
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Sub

    Public Sub FillReportNameControl()
        Try
            RadComboReportName.DataTextField = "ReportDescription"
            RadComboReportName.DataValueField = "ResourceId"
            RadComboReportName.DataSource = GetNACCASReports()
            RadComboReportName.DataBind()
            RadComboReportName.SelectedValue = 849
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Sub

    Public Sub FillProgramVersionsControl()
        Try
            RadListBoxPrograms.DataTextField = "ProgramDescription"
            RadListBoxPrograms.DataValueField = "ProgramId"
            Dim selectedCampus = GetCurrentlySelectedCampus()

            If selectedCampus IsNot Nothing AndAlso selectedCampus <> "" Then
                RadListBoxPrograms.Items.Clear()
                RadListBoxPrograms.DataSource = GetProgramsByCampusId(selectedCampus, True)
            End If
            RadListBoxPrograms.DataBind()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Sub
#End Region

#Region "Methods"
    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim da As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As List(Of syCampus)
        Try
            result = da.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), New List(Of String) From {"Active"})
            Return result
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Function

    Private Function GetCurrentYearPlus2AndMinus4Range(currentYear As Integer) As Object
        Dim da As New AcademicYearsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As List(Of saAcademicYear)
        Try
            result = da.GetYearsByRange(currentYear, 4, 2)
            Return result
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Function

    Private Function GetNACCASReports() As Object
        Dim da As New ReportsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As List(Of syReport)
        Try
            result = da.GetNACCASReports()
            Return result
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Function

    Private Function GetProgramsByCampusId(selectedCampus As String, ByVal filteredBySelected As Boolean) As List(Of NaccasProgram)
        Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim campusId As Guid = Guid.Parse(selectedCampus)
        Dim result As New List(Of NaccasProgram)
        Try
            Dim selectedPrograms As List(Of String) = GetCurrentlySelectedPrograms()
            If selectedPrograms Is Nothing Or filteredBySelected = False Then
                result = DA.GetProgramsHavingApprovedNaccasProgramVersionByCampusId(campusId, GetStatusFilters("chkInActiveProgramVersions"))
            Else
                result = DA.GetProgramsHavingApprovedNaccasProgramVersionByCampusId(campusId, GetStatusFilters("chkInActiveProgramVersions"), selectedPrograms)
            End If
            Return result
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Function

    Private Function GetCurrentlySelectedPrograms() As List(Of String)
        Dim selectedProgramVersions As New List(Of String)
        Try
            If RadListBoxPrograms2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxPrograms2.Items
                    Dim selected As String = item.Value
                    selectedProgramVersions.Add(selected)
                Next
                Return selectedProgramVersions
            Else
                Return selectedProgramVersions
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Function

    Private Function GetCurrentlySelectedCampus() As String
        Try
            Return RadComboCampus.SelectedValue
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try
    End Function
#End Region
End Class
