﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamCustomGroupControl1.ascx.vb" Inherits="ParamCustomGroupControl1" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelCustomGroup1" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">

        <div id="captionlabel" class="CaptionLabel" runat="server">
            Layout Options
         
        </div>

        <div id="ShowWorkUnits" class="ShowWorkUnits">

            <asp:RadioButtonList ID="rblShowWorkUnits" runat="server" AutoPostBack="True"
                Width="300px">
                <asp:ListItem Selected="True" Value="True">Do not show work units under each subject</asp:ListItem>
                <asp:ListItem Value="False">Show work units under each subject</asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div id="GroupWorkUnits" class="GroupWorkUnits">
            <asp:Panel ID="WorkUnitsPanel" runat="server" Visible="false">
                <span>Place work units by type under each term</span>
                <asp:CheckBoxList ID="cblGroupByType" runat="server" Height="77px"
                    RepeatColumns="3" RepeatDirection="Horizontal" Width="350px">
                    <asp:ListItem>Exam</asp:ListItem>
                    <asp:ListItem>Externship</asp:ListItem>
                    <asp:ListItem>Final</asp:ListItem>
                    <asp:ListItem>Homework</asp:ListItem>
                    <asp:ListItem>Lab Hours</asp:ListItem>
                    <asp:ListItem>Lab Work</asp:ListItem>
                    <asp:ListItem>Practical Exams</asp:ListItem>
                </asp:CheckBoxList>
            </asp:Panel>
        </div>
        <div class="ResetDiv">
        </div>
        <div id="Div1" class="CaptionLabel" runat="server">Miscellaneous Options</div>
        <div id="OtherCustomOptions" class="OtherCustomOptions">
            <%--    <asp:RadioButtonList ID="rblShowFinancialInfo" runat="server" Width="300px">
       <asp:ListItem Selected="True" Value="True">Yes</asp:ListItem>
        <asp:ListItem Value="False">No</asp:ListItem>
    </asp:RadioButtonList>--%>
            <asp:CheckBoxList ID="cblCustomOptions" runat="server" Height="77px" Width="700px"
                RepeatColumns="3" RepeatDirection="Horizontal">
                <asp:ListItem>Show Financial Information</asp:ListItem>
                <asp:ListItem>Show Student Signature Line</asp:ListItem>
                <asp:ListItem>Show School Official Signature Line</asp:ListItem>
                <asp:ListItem>Show Weekly Schedule</asp:ListItem>
                <asp:ListItem Selected="True">Show Term/Module</asp:ListItem>
                <asp:ListItem>Show All Enrollments</asp:ListItem>
            </asp:CheckBoxList>
        </div>


    </div>

    <div class="ResetDiv">
    </div>
</asp:Panel>
