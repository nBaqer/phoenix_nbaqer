﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports System.Xml.Serialization
Imports Microsoft.Ajax.Utilities

Partial Class ParamSortControl
    Inherits UserControl
    Implements ISortControl
#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _Caption As String
    Private _SqlConn As String
    Private _PostbackCtrlName As String
    Private _SavedSettings As List(Of SortItemInfo)
    Private _SortOptions As List(Of SortItemInfo)
    Public Property ItemDetail() As ParameterDetailItemInfo Implements ISortControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ISortControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ISortControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Private Property PostbackCtrlName() As String
        Get
            Return _PostbackCtrlName
        End Get
        Set(ByVal Value As String)
            _PostbackCtrlName = Value
        End Set
    End Property
    Public Property SortOptions() As List(Of SortItemInfo) Implements ISortControl.SortOptions
        Get
            'Return _SortOptions
            Return CType(Session("_SortOptions" & Me.ID), List(Of SortItemInfo))
        End Get
        Set(ByVal Value As List(Of SortItemInfo))
            '_SortOptions = Value
            Session("_SortOptions" & Me.ID) = Value
        End Set
    End Property
    Public Property SavedSettings() As List(Of SortItemInfo) Implements ISortControl.SavedSettings
        Get
            Return _SavedSettings
        End Get
        Set(ByVal Value As List(Of SortItemInfo))
            _SavedSettings = Value
        End Set
    End Property
    Protected Property SortStore() As IList(Of SortItemInfo)
        Get
            Try
                Dim obj As Object = Session("Sort_VB")
                If obj Is Nothing Then
                    obj = GetSortData()
                    Session("Sort_VB") = obj
                End If
                Return DirectCast(obj, IList(Of SortItemInfo))
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Session("Sort_VB") = Nothing
            End Try
            Return New List(Of SortItemInfo)
        End Get
        Set(ByVal value As IList(Of SortItemInfo))
            Session("Sort_VB") = value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = True Then
            Else
                Session("Sort_VB") = Nothing
                SetProperties()
                Fill()
                Exit Sub
            End If
            SetProperties()
            Fill()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ' lblErrMsg.Text = ex.Message
            Throw ex
        End Try
    End Sub
    Protected Sub RadGridSortSelection_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGridSortSelection.DataSource = SortStore
    End Sub
    Protected Sub RadGridSortSelection_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs)
        If e.CommandName = "SortDirection" Then
            If TypeOf e.Item Is GridDataItem Then
                Dim checkBox As CheckBox = CType(e.Item.FindControl("CheckBox1"), CheckBox)
                Dim myDataItem As GridDataItem = CType(e.Item, GridDataItem)
                Dim SortBtn As LinkButton = CType(myDataItem("DirectionBtn").Controls(0), LinkButton)
                Dim myCheckbox As CheckBox = CType(myDataItem("isdescending").Controls(0), CheckBox)
                Dim SortDisplayText As String = myDataItem("SortDirectionDisplay").Text
                Dim soptions As IList(Of SortItemInfo)
                soptions = SortStore
                Dim tmpSort As SortItemInfo = GetSort(soptions, DirectCast(myDataItem.GetDataKeyValue("fieldname"), String))
                Dim destinationIndex As Integer = soptions.IndexOf(tmpSort)
                Dim tmpSortHold As SortItemInfo = tmpSort

                If tmpSort.IsDecending = False Then
                    SortBtn.ToolTip = "Change to Ascending Order (A to Z)"
                    SortBtn.Text = "Descending Order (Z to A)"
                    myDataItem("SortDirectionDisplay").Text = "Descending Order"
                    checkBox.Checked = True
                    myCheckbox.Checked = True
                    SortDisplayText = "descending order"
                    tmpSort.IsDecending = True
                    soptions.Remove(tmpSort)
                    soptions.Insert(destinationIndex, tmpSortHold)
                    SortStore = soptions
                ElseIf tmpSort.IsDecending = True Then
                    SortBtn.ToolTip = "Change to Descending Order (Z to A)"
                    SortBtn.Text = "Ascending Order (A to Z)"
                    myDataItem("SortDirectionDisplay").Text = "Ascending Order"
                    checkBox.Checked = False
                    myCheckbox.Checked = False
                    SortDisplayText = "ascending order"
                    tmpSortHold.IsDecending = False
                    soptions.Remove(tmpSort)
                    soptions.Insert(destinationIndex, tmpSortHold)
                    SortStore = soptions
                End If
            End If
        End If
    End Sub
    Protected Sub RadGridSortSelection_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs)
        If TypeOf e.Item Is GridDataItem Then
            Dim soptions As IList(Of SortItemInfo)
            soptions = SortStore
            Dim myDataItem As GridDataItem = CType(e.Item, GridDataItem)
            Dim tmpSort As SortItemInfo = GetSort(soptions, DirectCast(myDataItem.GetDataKeyValue("fieldname"), String))
            Dim checkBox As CheckBox = CType(e.Item.FindControl("CheckBox1"), CheckBox)
            Dim SortBtn As LinkButton = CType(myDataItem("DirectionBtn").Controls(0), LinkButton)
            Dim myCheckbox As CheckBox = CType(myDataItem("isdescending").Controls(0), CheckBox)
            Dim SortDisplayText As String = myDataItem("SortDirectionDisplay").Text
            If tmpSort.IsDecending = False Then
                SortBtn.ToolTip = "Change to Descending Order (Z to A)"
                SortBtn.Text = "Ascending Order (A to Z)"
                myDataItem("SortDirectionDisplay").Text = "Ascending Order"
                checkBox.Checked = False
                myCheckbox.Checked = False
            ElseIf tmpSort.IsDecending = True Then
                SortBtn.ToolTip = "Change to Ascending Order (A to Z)"
                SortBtn.Text = "Descending Order (Z to A)"
                myDataItem("SortDirectionDisplay").Text = "Descending Order"
                checkBox.Checked = True
                myCheckbox.Checked = True
            End If
        End If
    End Sub
    Protected Sub RadGridSortSelection_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        If e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = RadGridSortSelection.ClientID Then
            Dim soptions As IList(Of SortItemInfo)
            soptions = SortStore
            Dim sort As SortItemInfo = GetSort(soptions, DirectCast(e.DestDataItem.GetDataKeyValue("fieldname"), String))
            Dim destinationIndex As Integer = soptions.IndexOf(sort)
            If ((e.DropPosition = GridItemDropPosition.Above) _
                    AndAlso (e.DestDataItem.ItemIndex > e.DraggedItems(0).ItemIndex)) Then
                destinationIndex = (destinationIndex - 1)
            End If
            If ((e.DropPosition = GridItemDropPosition.Below) _
                    AndAlso (e.DestDataItem.ItemIndex < e.DraggedItems(0).ItemIndex)) Then
                destinationIndex = (destinationIndex + 1)
            End If
            Dim sortsToMove As New List(Of SortItemInfo)()
            For Each draggedItem As GridDataItem In e.DraggedItems
                Dim tmpSort As SortItemInfo = GetSort(soptions, DirectCast(draggedItem.GetDataKeyValue("fieldname"), String))
                If tmpSort IsNot Nothing Then
                    sortsToMove.Add(tmpSort)
                End If
            Next
            For Each sortToMove As SortItemInfo In sortsToMove
                soptions.Remove(sortToMove)
                soptions.Insert(destinationIndex, sortToMove)
            Next
            'reorder the sequencing
            Dim i As Integer = 1
            For Each item As SortItemInfo In soptions
                item.SortSeq = i
                i = i + 1
            Next
            SortStore = soptions
            RadGridSortSelection.Rebind()
            Dim destinationItemIndex As Integer = destinationIndex - (RadGridSortSelection.PageSize * RadGridSortSelection.CurrentPageIndex)
            e.DestinationTableView.Items(destinationItemIndex).Selected = True
        End If
    End Sub
#End Region
#Region "Methods"
    Protected Function GetSortData() As IList(Of SortItemInfo)
        Dim XmlList As IList(Of SortItemInfo)
        XmlList = SortOptions
        Dim ds As New DataSet
        Dim results As List(Of SortItemInfo) = New List(Of SortItemInfo)
        Try
            For i As Integer = 0 To XmlList.Count - 1
                Dim fieldname As String = DirectCast(XmlList(i).FieldName, String)
                Dim setid As Integer = XmlList(i).SetId
                Dim sortseq As Integer = XmlList(i).SortSeq
                Dim displayname As String = XmlList(i).DisplayName
                Dim isdescending As Boolean = XmlList(i).IsDecending
                results.Add(New SortItemInfo(fieldname, setid, sortseq, displayname, isdescending))
            Next
            Return results
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Protected Function LoadSettings(ByVal SortItems As String) As List(Of SortItemInfo)

        Dim results As List(Of SortItemInfo)
        Dim objStringReader As New StringReader(SortItems)
        Dim serializer As New XmlSerializer(GetType(List(Of SortItemInfo)))
        results = TryCast(serializer.Deserialize(objStringReader), List(Of SortItemInfo))
        objStringReader.Close()
        Return results

    End Function
    Public Sub LoadSavedReportSettings() Implements ISortControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                SortStore = SavedSettings
                Fill()
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Function GetControlSettings() As List(Of SortItemInfo) Implements ISortControl.GetControlSettings
        'Return SortStore.ToList
        Dim SortSettings As List(Of SortItemInfo) = New List(Of SortItemInfo)
        RadGridSortSelection.DataSource = SortStore
        RadGridSortSelection.DataBind()
        Try

            For intCounter As Integer = 0 To SortStore.Count - 1
                Dim fieldname As String = DirectCast(SortStore(intCounter)._FieldName, String)
                Dim setid As Integer = CInt(SortStore(intCounter)._SetId)
                Dim sortseq As Integer = CInt(SortStore(intCounter)._SortSeq)
                Dim displayname As String = DirectCast(SortStore(intCounter)._DisplayName, String)
                Dim isdescending As Boolean = SortStore(intCounter)._IsDecending
                SortSettings.Add(New SortItemInfo(fieldname, setid, sortseq, displayname, isdescending))
            Next

            'For Each item As GridDataItem In RadGridSortSelection.Items
            '    Dim fieldname As String = DirectCast(item("fieldname").Text, String)
            '    Dim setid As Integer = CInt(item("SetId").Text)
            '    Dim sortseq As Integer = CInt(item("sortseq").Text)
            '    Dim displayname As String = DirectCast(item("displayname").Text, String)
            '    Dim isdescending As Boolean = DirectCast(item("isdescending").Controls(0), CheckBox).Checked
            '    SortSettings.Add(New SortItemInfo(fieldname, setid, sortseq, displayname, isdescending))
            'Next
            Return SortSettings
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try

    End Function
    Public Sub Fill()
        Try
            RadGridSortSelection.DataSource = SortStore
            RadGridSortSelection.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)
            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Xml" Then
                p.SetValue(Me, CType(LoadSettings(prop.Value), List(Of SortItemInfo)), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)
            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Xml" Then
                p.SetValue(Me, CType(LoadSettings(prop.Value), List(Of SortItemInfo)), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Shared Function GetSort(ByVal sortToSearchIn As IEnumerable(Of SortItemInfo), ByVal fieldname As String) As SortItemInfo
        For Each sort As SortItemInfo In sortToSearchIn
            If sort.FieldName = fieldname Then
                Return sort
            End If
        Next
        Return Nothing
    End Function
#End Region
End Class

