﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamSortControl.ascx.vb" Inherits="ParamSortControl" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SortOrderPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SortOrderPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<link href="../../css/ParameterSetControl_Default.css" rel="stylesheet" />
<div id="MainContainer" class="MainContainer">

    <div id="captionlabel" class="CaptionLabel" runat="server"><%=Caption%></div>
   
    <div id="SelectedSortOrderGrid" class="SelectedSortOrderGrid" style="padding-left: 10px; padding-top: 10px;">
        <div id="SortOrderInstructions" >
            Select and drag rows to change the report sort order then click on the Sort Direction link to change the sort direction from ascending or descending.
        </div>
        <asp:Panel ID="SortOrderPanel" runat="server">
            <telerik:RadGrid ID="RadGridSortSelection" runat="server" Width="100%"
                AutoGenerateColumns="False" GridLines="None" AllowMultiRowSelection="True"
                OnNeedDataSource="RadGridSortSelection_NeedDataSource"
                OnItemCommand="RadGridSortSelection_ItemCommand"
                OnItemDataBound="RadGridSortSelection_ItemDataBound"
                OnRowDrop="RadGridSortSelection_RowDrop">
                <HeaderContextMenu EnableAutoScroll="True">
                </HeaderContextMenu>
                <ClientSettings AllowRowsDragDrop="True">
                    <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                </ClientSettings>
                <MasterTableView NoMasterRecordsText="You currently have no sorts selected" DataKeyNames="fieldname" ViewStateMode="Enabled">
                    <RowIndicatorColumn>
                        <HeaderStyle Width="50%"></HeaderStyle>
                    </RowIndicatorColumn>

                    <ExpandCollapseColumn>
                        <HeaderStyle Width="50%"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="setid"
                            GroupByExpression="setid" HeaderText="setid"
                            SortExpression="setid" UniqueName="setid" Visible="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="sortseq"
                            GroupByExpression="sortseq" HeaderText="SortSeq"
                            SortExpression="sortseq" UniqueName="sortseq" Visible="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="displayname"
                            GroupByExpression="displayname" HeaderText="Sort Options"
                            SortExpression="displayname" UniqueName="displayname">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="fieldname"
                            GroupByExpression="fieldname" HeaderText="fieldname"
                            SortExpression="fieldname" UniqueName="fieldname" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn
                            GroupByExpression="SortDirection" HeaderText="Sort Direction"
                            SortExpression="SortDirection" UniqueName="SortDirectionDisplay" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="PushButton"
                            UniqueName="DirectionButton" Text="Edit"
                            CommandName="SortDirection" Visible="false">
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ButtonType="LinkButton"
                            UniqueName="DirectionBtn" HeaderText="Sort Direction"
                            GroupByExpression="SortDirection"
                            SortExpression="SortDirection"
                            CommandName="SortDirection">
                        </telerik:GridButtonColumn>
                        <telerik:GridTemplateColumn Visible="false" UniqueName="CheckBox1">
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="CheckBox1" Checked="false" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridCheckBoxColumn
                            UniqueName="isdescending" HeaderText="SortDirectionValue"
                            GroupByExpression="isdescending" DataField="isdescending" Display="false">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <br/>
            <br/>
        </asp:Panel>
    </div>
</div>

<div class="ResetDiv">
</div>

