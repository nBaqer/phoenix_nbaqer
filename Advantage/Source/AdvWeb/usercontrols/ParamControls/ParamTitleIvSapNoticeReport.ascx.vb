﻿
Imports System.Reflection
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports FAME.Parameters.Info
Imports FAME.Parameters.Interfaces
Imports Telerik.Web.UI

Partial Class usercontrols_ParamControls_ParamTitleIvSapNoticeReport
    Inherits UserControl
    Implements ICustomControl
    Protected MyAdvAppSettings As AdvAppSettings

    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    Private _ItemDetail As ParameterDetailItemInfo

#Region "Report Functions"
    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
    Public Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim radioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                radioButList.SelectedValue = itemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim comboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                If Not itemValue.KeyData = "1900" Then
                                    comboBox.SelectedValue = itemValue.KeyData
                                End If
                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim lstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            lstBox.Items.Clear()
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = itemValue.DisplayText
                                newItem.Value = itemValue.KeyData
                                lstBox.SelectedValue = itemValue.KeyData
                                lstBox.Items.Add(newItem)
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else

            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride

            'Campus Group
            Dim ctrlSettingCampusGroup As New ControlSettingInfo
            Dim ctrlValuesCampusGroup As New List(Of ControlValueInfo)
            ctrlSettingCampusGroup.ControlName = RadListBoxCmpGrp2.ID
            For Each item As RadListBoxItem In RadListBoxCmpGrp2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesCampusGroup.Add(objControlValueInfo3)
            Next
            ctrlSettingCampusGroup.ControlValueCollection = ctrlValuesCampusGroup
            settingsCollection.Add(ctrlSettingCampusGroup)


            'Campus
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            ctrlSettingCampus.ControlName = RadListBoxCampus2.ID
            For Each item As RadListBoxItem In RadListBoxCampus2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesCampus.Add(objControlValueInfo3)
            Next
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            settingsCollection.Add(ctrlSettingCampus)


            'Program
            Dim ctrlSettingProgram As New ControlSettingInfo
            Dim ctrlValuesProgram As New List(Of ControlValueInfo)
            ctrlSettingProgram.ControlName = RadListBoxProgram2.ID
            For Each item As RadListBoxItem In RadListBoxProgram2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram.Add(objControlValueInfo3)
            Next
            ctrlSettingProgram.ControlValueCollection = ctrlValuesProgram
            settingsCollection.Add(ctrlSettingProgram)

            'Status
            Dim ctrlSettingStatus As New ControlSettingInfo
            Dim ctrlValuesStatus As New List(Of ControlValueInfo)
            ctrlSettingStatus.ControlName = RadListBoxTitleIvStatuses2.ID
            For Each item As RadListBoxItem In RadListBoxTitleIvStatuses2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesStatus.Add(objControlValueInfo3)
            Next
            ctrlSettingStatus.ControlValueCollection = ctrlValuesStatus
            settingsCollection.Add(ctrlSettingStatus)

            'Enrollments
            Dim ctrlSettingEnrollments As New ControlSettingInfo
            Dim ctrlValuesEnrollments As New List(Of ControlValueInfo)
            ctrlSettingEnrollments.ControlName = RadListBoxActiveEnrollments2.ID
            For Each item As RadListBoxItem In RadListBoxActiveEnrollments2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesEnrollments.Add(objControlValueInfo3)
            Next
            ctrlSettingEnrollments.ControlValueCollection = ctrlValuesEnrollments
            settingsCollection.Add(ctrlSettingEnrollments)

            userSettings.ControlSettingsCollection = settingsCollection

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        Return userSettings

    End Function

    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim displayTree As New RadTreeView
        displayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            'campus groups 
            If RadListBoxCmpGrp2.Items.Count > 0 Then
                Dim headerCampusGroupNode As New RadTreeNode
                headerCampusGroupNode.Text = "Campus Group"
                headerCampusGroupNode.Value = "CampusGroup"
                headerCampusGroupNode.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxCmpGrp2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerCampusGroupNode.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerCampusGroupNode)
            End If

            'campus
            If RadListBoxCampus2.Items.Count > 0 Then
                Dim headerCampusNode As New RadTreeNode
                headerCampusNode.Text = "Campus"
                headerCampusNode.Value = "Campus"
                headerCampusNode.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxCampus2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerCampusNode.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerCampusNode)
            End If

            'Program
            If RadListBoxProgram2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Program"
                headerNode2.Value = "Program"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If

            'Statuses Title IV
            If RadListBoxTitleIvStatuses2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Title IV Status"
                headerNode2.Value = "TitleIVStatus"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxTitleIvStatuses2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If

            'Active Enrollments
            If RadListBoxActiveEnrollments2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Enrollments"
                headerNode2.Value = "Enrollments"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxActiveEnrollments2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If

            Return displayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
#End Region

#Region "Fill Data"
    Public Sub FillCmpGrpControl()
        Try
            RadListBoxCmpGrp.DataTextField = "Campgrpdescrip"
            RadListBoxCmpGrp.DataValueField = "CampgrpId"
            RadListBoxCmpGrp.DataSource = GetCampGrpByUserId()
            RadListBoxCmpGrp.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillCampusControl()
        Try
            RadListBoxCampus.DataTextField = "CampDescrip"
            RadListBoxCampus.DataValueField = "CampusId"
            RadListBoxCampus.DataSource = GetCampusesByUserId()
            RadListBoxCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Public Sub FillProgramsControl()
        Try
            RadListBoxProgram.DataTextField = "ProgDescrip"
            RadListBoxProgram.DataValueField = "ProgId"
            Dim selectedCampusGroups = GetCurrentlySelectedCampusGroups()

            If selectedCampusGroups IsNot Nothing Then
                RadListBoxProgram.DataSource = GetProgramsByCampusGroupId(selectedCampusGroups, True)
            End If
            RadListBoxProgram.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Public Sub FillTitleIvStatusesControl()
        Try
            RadListBoxTitleIvStatuses.DataTextField = "Description"
            RadListBoxTitleIvStatuses.DataValueField = "Id"

            RadListBoxTitleIvStatuses.DataSource = GetTitleIvStatus()

            RadListBoxTitleIvStatuses.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Public Sub FillEnrollemtnsControl()
        Try
            RadListBoxActiveEnrollments.DataTextField = "Description"
            RadListBoxActiveEnrollments.DataValueField = "Id"

            Dim selectedCampuses = GetCurrentlySelectedCampuses()
            Dim selectedPrograms = GetCurrentlySelectedPrograms()
            Dim selectedStatuses = GetCurrentlySelectedTitleIvStatuses()

            If selectedCampuses IsNot Nothing AndAlso selectedPrograms IsNot Nothing Then
                RadListBoxActiveEnrollments.DataSource = GetStudentEnrollments(selectedCampuses, selectedPrograms, selectedStatuses, True)

            End If

            RadListBoxActiveEnrollments.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub




#End Region



#Region "Currently Selected Funcions"
    Private Function GetCurrentlySelectedCampuses() As List(Of String)
        Dim selectedCampuses As New List(Of String)
        Try
            If RadListBoxCampus2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxCampus2.Items
                    Dim selected As String = item.Value
                    selectedCampuses.Add(selected)
                Next
            End If
            Return selectedCampuses

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Private Function GetCurrentlySelectedCampusGroups() As List(Of String)
        Dim selectedCampusGroups As New List(Of String)
        Try
            If RadListBoxCmpGrp2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxCmpGrp2.Items
                    Dim selected As String = item.Value
                    selectedCampusGroups.Add(selected)
                Next
            End If
            Return selectedCampusGroups

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    

    Private Function GetCurrentlySelectedPrograms() As List(Of String)
        Dim selectedPrograms As New List(Of String)
        Try
            If RadListBoxProgram2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim selected As String = item.Value
                    selectedPrograms.Add(selected)
                Next
                Return selectedPrograms
            Else
                Return selectedPrograms
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Private Function GetCurrentlySelectedTitleIvStatuses() As List(Of String)
        Dim selectedPrograms As New List(Of String)
        Try
            If RadListBoxTitleIvStatuses2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxTitleIvStatuses2.Items
                    Dim selected As String = item.Value
                    selectedPrograms.Add(selected)
                Next
                Return selectedPrograms
            Else
                Return selectedPrograms
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Private Function GetCurrentlySelectedEnrollments() As List(Of String)
        Dim selectedPrograms As New List(Of String)
        Try
            If RadListBoxActiveEnrollments2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxActiveEnrollments2.Items
                    Dim selected As String = item.Value
                    selectedPrograms.Add(selected)
                Next
                Return selectedPrograms
            Else
                Return selectedPrograms
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
#End Region

#Region "Data Access Functions"
    Private Function GetStudentEnrollments(selectedCampuses As List(Of String), selectedPrograms As List(Of String),selectedStatuses As List(Of String), ByVal filteredBySelected As Boolean) As List(Of GenericListItem)
        Dim DA As New StudentEnrollmentDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

        Dim result As List(Of GenericListItem)
        Try
            Dim selectedEnrollments As List(Of String) = GetCurrentlySelectedEnrollments()
            If selectedEnrollments Is Nothing Or filteredBySelected = False Then
                result = DA.GetEnrollemnts(selectedCampuses, selectedPrograms, selectedStatuses, GetStatusFilters("chkInActivePrograms"))
            Else
                result = DA.GetEnrollemnts(selectedCampuses, selectedPrograms, selectedStatuses, GetStatusFilters("chkInActivePrograms"), selectedEnrollments)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Private Function GetProgramsByCampusGroupId(selectedCampusGroups As List(Of String), ByVal filteredBySelected As Boolean) As List(Of arProgram)
        Dim DA As New ProgramDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arProgram)
        Try
            Dim selectedPrograms As List(Of String) = GetCurrentlySelectedPrograms()
            If selectedPrograms Is Nothing Or filteredBySelected = False Then
                result = DA.GetProgramsByCampusGroupsId(selectedCampusGroups, GetStatusFilters("chkInActivePrograms"))
            Else
                result = DA.GetProgramsByCampusGroupsId(selectedCampusGroups, GetStatusFilters("chkInActivePrograms"), selectedPrograms)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Private Function GetTitleIvStatus() As List(Of syTitleIVSapStatus)
        Dim DA As New TitleIvSapStatusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syTitleIVSapStatus)
        Dim selectedTitleIvStatuses As List(Of String) = GetCurrentlySelectedTitleIvStatuses()
        Try
            If SelectedTitleIvStatuses Is Nothing Then
                result = DA.GetTitleIvStatuses(GetStatusFilters("chkInActiveTitleIvStatuses"))
            Else
                result = DA.GetTitleIvStatuses(GetStatusFilters("chkInActiveTitleIvStatuses"), selectedTitleIvStatuses)
            End If

            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Private Function GetCampGrpByUserId() As List(Of syCampGrp)
        Dim DA As New CampusGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampGrp)
        Dim SelectedCampusGroups As List(Of String) = GetCurrentlySelectedCampusGroups()
        Try
            If SelectedCampusGroups Is Nothing Then
                result = DA.GetCampusGrpsByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), GetStatusFilters("cbkInactiveCmpGrp"))
            Else
                result = DA.GetCampusGrpsByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), GetStatusFilters("cbkInactiveCmpGrp"), SelectedCampusGroups)
            End If

            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampus)
        Dim SelectedCampuses As List(Of String) = GetCurrentlySelectedCampuses()

        Try
            If SelectedCampuses Is Nothing Then
                result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), GetStatusFilters("checkboxInactiveCampuses"))
            Else
                result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString), GetStatusFilters("checkboxInactiveCampuses"), SelectedCampuses)
            End If

            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    
#End Region

#Region "Others"
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim statusValues As New List(Of String)
        Try
            Dim cKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If cKbox.Checked = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        Return statusValues
    End Function
    Private Sub SetProperties()
        Try
            'RadDateReportDate.MaxDate = DateTime.Now
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub

    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arProgram Then
                Dim myarProgram As arProgram = DirectCast(myItem.DataItem, arProgram)
                If myarProgram.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarProgram.ProgDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is arPrgGrp Then
                Dim myarPrgGrp As arPrgGrp = DirectCast(myItem.DataItem, arPrgGrp)
                If myarPrgGrp.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarPrgGrp.PrgGrpDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
#End Region

#Region "UI Events"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        SetProperties()
        If Not Page.IsPostBack Then
            FillCmpGrpControl()
            FillCampusControl()
            FillProgramsControl()
            FillTitleIvStatusesControl()
            FillEnrollemtnsControl()
        End If
    End Sub

    Protected Sub RadListBoxesCampusGroup_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
        'Pass campuses to the right that belong to the selected campus groups
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim campusesToSelect As New List(Of syCampus)
        Try
            RadListBoxCampus2.Items.Clear()
            RadListBoxCampus.Items.Clear()
            FillCampusControl()
            RadListBoxProgram2.Items.Clear()
            FillProgramsControl()
            RadListBoxActiveEnrollments2.Items.Clear()
            FillEnrollemtnsControl()
            campusesToSelect = DA.GetCampusesForCampusGroups(GetCurrentlySelectedCampusGroups())
            For Each item As syCampus In campusesToSelect
                Dim itemToTransfer = RadListBoxCampus.FindItemByValue(item.CampusId.ToString)
                If itemToTransfer IsNot Nothing Then
                    RadListBoxCampus.Transfer(itemToTransfer, RadListBoxCampus, RadListBoxCampus2)
                End If
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Protected Sub RadListCampusBoxes_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
        RadListBoxProgram2.Items.Clear()
        FillProgramsControl()
        RadListBoxActiveEnrollments2.Items.Clear()
        FillEnrollemtnsControl()
    End Sub

    Protected Sub cbkInactiveCmpGrp_OnCheckedChanged(sender As Object, e As EventArgs)
        FillCmpGrpControl()
        FillCampusControl()
    End Sub

    Protected Sub checkboxInactiveCampuses_OnCheckedChanged(sender As Object, e As EventArgs)
        FillCampusControl()
    End Sub

    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
    
    End Sub

    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ChkBox As CheckBox = DirectCast(sender, CheckBox)
        If ChkBox.ID.Contains("Course") Then

        End If
    End Sub

    Protected Sub ProgramCheckBox_CheckedChanged(sender As Object, e As EventArgs)
        FillProgramsControl()
    End Sub

    Protected Sub TitleIvStatusCheckBox_CheckedChanged(sender As Object, e As EventArgs)
        FillTitleIvStatusesControl()
    End Sub
    Protected Sub RadListProgramBoxes_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
        FillEnrollemtnsControl()
    End Sub

    Protected Sub RadListBoxEnrollments_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
    End Sub
    Protected Sub RadListBoxStatuses_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
        FillEnrollemtnsControl()
    End Sub
#End Region


    
End Class
