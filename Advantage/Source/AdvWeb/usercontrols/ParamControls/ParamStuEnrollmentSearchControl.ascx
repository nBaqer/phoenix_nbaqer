﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamStuEnrollmentSearchControl.ascx.vb"
    Inherits="ParamStuEnrollmentSearchControl" %>
<style type="text/css">
    </style>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridStuEnrollmentSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadGridSelectedStuEnrollments">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadGridStuEnrollmentSearchMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadTextBox1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadTextBox2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadComboBox1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadComboBox2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadComboBox3">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadComboBox4">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="lbnReset">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="lbnResetMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveSelectedUp">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveSelectedDown">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveSelectedUpMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnMoveSelectedDownMRU">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<link href="../../css/ParameterSetControl_Default.css" rel="stylesheet" />
<asp:Panel ID="MainPanel" runat="server" Style="margin-top: 10px; margin-left: 5px;">
    <%--</br>--%>
    <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
        SelectedIndex="0" MultiPageID="RadMultiPage1">
        <Tabs>
            <telerik:RadTab Text="Student Enrollments">
            </telerik:RadTab>
            <telerik:RadTab Text="Most Recently Used">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0" CssClass="SearchmultiPage" ScrollBars="None" Style="width: 98%;">
        <!-- Height="350px" Width="396px" -->
        <telerik:RadPageView runat="server" ID="RadPageView1" CssClass="corporatePageView">
            <div id="MainContainer" runat="server" class="MainContainer" style="margin-bottom: 1em;">
                <div id="captionlabel" class="CaptionLabel" runat="server">
                    <%=Caption%>
                </div>
                <asp:Panel ID="Panel1" runat="server">
                    <div id="StudentSearchFilters" class="StudentSearchFilters" >
                        <div id="SearchNameBox" class="SearchBox">
                            <telerik:RadComboBox ID="RadComboBox1" runat="server" Width="200px" AutoPostBack="true"
                                OnSelectedIndexChanged="FilterDisplay" MarkFirstMatch="true" DropDownWidth="335px">
                            </telerik:RadComboBox>
                            <telerik:RadComboBox ID="RadComboBox2" runat="server" Width="200px" AutoPostBack="true"
                                OnSelectedIndexChanged="FilterDisplay" MarkFirstMatch="true" DropDownWidth="335px">
                            </telerik:RadComboBox>
                            <telerik:RadTextBox ID="RadTextBox1" runat="server" OnTextChanged="FilterDisplay" AutoPostBack="True" CssClass="textbox textbox2" Width="300px"
                                EmptyMessage="Enter Name, Student#, Enrollment ID, or SSN">
                                <ClientEvents OnKeyPress="TBValueChanged_13" />
                            </telerik:RadTextBox>
                            <asp:Button ID="ibSearchStudent" ClientIDMode="Static" runat="Server" Width="60px" Text="Search" OnClick="FilterDisplay" Style="width: 75px;"></asp:Button>
                            <asp:LinkButton ID="lbnReset" runat="server" OnClick="lbnReset_Click" Visible="false" EnableViewState="false">Reset Search</asp:LinkButton>
                        </div>
                        <div class="StudentSearchMessage">
                            <asp:Label ID="lblMessagePanel1" runat="Server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div id="StuEnrollmentSearchGrid" class="StudentSearchGrid">
                        <telerik:RadGrid runat="server" ID="RadGridStuEnrollmentSearch" Width="98%" AllowPaging="True"
                            AllowMultiRowSelection="True" GridLines="None" PageSize="5" AllowSorting="true"
                            EnableLinqExpressions="false"
                            OnNeedDataSource="RadGridStuEnrollmentSearch_NeedDataSource"
                            OnItemCommand="RadGridStuEnrollmentSearch_ItemCommand"
                            OnRowDrop="RadGridStuEnrollmentSearch_RowDrop">
                            <SortingSettings EnableSkinSortStyles="false" />
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <GroupingSettings CaseSensitive="false" />
                            <MasterTableView DataKeyNames="StuEnrollId" AutoGenerateColumns="False" AllowMultiColumnSorting="false">
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <NoRecordsTemplate>
                                    <div style="height: 60px; cursor: pointer;">
                                        You currently have no student enrollments to select for the report
                                    </div>
                                </NoRecordsTemplate>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                                        UniqueName="FirstName" HeaderTooltip="Sort by first name" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StuEnrollId" HeaderText="StuEnrollId" SortExpression="StuEnrollId"
                                        UniqueName="StuEnrollId" Display="False">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName"
                                        UniqueName="LastName" HeaderTooltip="Sort by last name" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentNumber" HeaderText="Student Number" SortExpression="StudentNumber"
                                        UniqueName="StudentNumber" HeaderTooltip="Sort by student number" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentId" DataType="System.Guid" HeaderText="StudentId"
                                        SortExpression="StudentId" UniqueName="StudentId" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSNSearch"
                                        UniqueName="SSN" HeaderTooltip="Sort by SSN" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SSNSearch" HeaderText="SSNSearch" SortExpression="SSNSearch"
                                        UniqueName="SSNSearch" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentType" HeaderText="StudentType" SortExpression="StudentType"
                                        UniqueName="StudentType" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="EnrollmentID" HeaderText="Enrollment ID" SortExpression="EnrollmentID"
                                        UniqueName="EnrollmentID" Visible="true" HeaderTooltip="Sort by enrollment id" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PrgVerDescrip" HeaderText="Program" SortExpression="PrgVerDescrip"
                                        UniqueName="PrgVerDescrip" Visible="true" HeaderTooltip="Sort by program version" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PrgVerId" HeaderText="PrgVerId" SortExpression="PrgVerId"
                                        UniqueName="PrgVerId" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StatusCodeDescrip" HeaderText="Status" SortExpression="StatusCodeDescrip"
                                        UniqueName="StatusCodeDescrip" Visible="true" HeaderTooltip="Sort by status" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StatusCodeId" HeaderText="Status" SortExpression="StatusCodeId"
                                        UniqueName="StatusCodeId" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ShiftDescrip" HeaderText="Shift" SortExpression="ShiftDescrip"
                                        UniqueName="ShiftDescrip" Visible="true" HeaderTooltip="Sort by shift" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ShiftID" HeaderText="Shift" SortExpression="ShiftID"
                                        UniqueName="ShiftID" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IsTransHold" HeaderText="Trans Hold" SortExpression="IsTransHold"
                                        UniqueName="IsTransHold" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" />
                            </MasterTableView>
                            <ClientSettings AllowRowsDragDrop="True" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                            </ClientSettings>
                            <PagerStyle Mode="NumericPages" />
                        </telerik:RadGrid>
                    </div>
                </asp:Panel>
            </div>
            <div style="text-align: center; ">
                <asp:LinkButton  ID="btnMoveSelectedDown" runat="server" OnClick="btnMoveSelectedDown_Click"
                                EnableViewState="false" ToolTip="Move Selected Students Down">
                    <span class="k-icon k-i-arrow-down font-blue k-icon-32"></span>
                </asp:LinkButton>
                <asp:LinkButton ID="btnMoveSelectedUp" runat="server" OnClick="btnMoveSelectedUp_Click"
                    EnableViewState="false" ToolTip="Move Selected Students Up" >
                    <span class="k-icon k-i-arrow-up font-blue k-icon-32"></span>
                </asp:LinkButton>

            </div>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView2" CssClass="corporatePageView" Style="width: 100%;">
            <div id="MainContainer2" runat="server" class="MainContainer" style="margin-bottom: 1em;">
                <div id="Div1" runat="server">
                    <%=Caption3%>
                </div>
                <asp:Panel ID="Panel2" runat="server">
                    <div id="StudentSearchFilters2" class="StudentSearchFilters" style="width: 100%;">
                        <div id="SearchNameBox2" class="SearchBox">
                            <telerik:RadComboBox ID="RadComboBox3" runat="server" Width="200px" AutoPostBack="true"
                                OnSelectedIndexChanged="FilterDisplayMRU" MarkFirstMatch="true" DropDownWidth="335px">
                            </telerik:RadComboBox>
                            <telerik:RadComboBox ID="RadComboBox4" runat="server" Width="200px" AutoPostBack="true"
                                OnSelectedIndexChanged="FilterDisplayMRU" MarkFirstMatch="true" DropDownWidth="335px">
                            </telerik:RadComboBox>
                            <telerik:RadTextBox ID="RadTextBox2" runat="server" OnTextChanged="FilterDisplayMRU" CssClass="textbox textbox2" Width="300px"
                                EmptyMessage="Enter Name, Student#, Enrollment ID, or SSN">
                                <ClientEvents OnKeyPress="TBValueChanged_13" />
                            </telerik:RadTextBox>
                            <asp:Button ID="ibSearchStudentMRU" runat="Server" Text="Search" OnClick="FilterDisplayMRU"></asp:Button>
                            <asp:LinkButton ID="lbnResetMRU" runat="server" OnClick="lbnResetMRU_Click" Visible="false" EnableViewState="false">Reset Search</asp:LinkButton>
                        </div>
                    </div>
                    <div class="StudentSearchMessage">
                        <asp:Label ID="lblMessagePanel2" runat="Server" Text="" Style="height: 20px; vertical-align: central"></asp:Label>
                    </div>
                    <div id="StuEnrollmentSearchGridMRU" class="StudentSearchGrid" style="width: 100%;">
                        <telerik:RadGrid runat="server" ID="RadGridStuEnrollmentSearchMRU" Width="98%" AllowPaging="True"
                            AllowMultiRowSelection="True" GridLines="None" PageSize="5" AllowSorting="true"
                            EnableLinqExpressions="false"
                            OnNeedDataSource="RadGridStuEnrollmentSearchMRU_NeedDataSource"
                            OnItemCommand="RadGridStuEnrollmentSearchMRU_ItemCommand"
                            OnRowDrop="RadGridStuEnrollmentSearchMRU_RowDrop">
                            <SortingSettings EnableSkinSortStyles="false" />
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <GroupingSettings CaseSensitive="false" />
                            <MasterTableView DataKeyNames="StuEnrollId" AutoGenerateColumns="False" AllowMultiColumnSorting="false">
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <NoRecordsTemplate>
                                    <div style="height: 60px; cursor: pointer;">
                                        You currently have no MRU student enrollments to select for the report
                                    </div>
                                </NoRecordsTemplate>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                                        UniqueName="FirstName" HeaderTooltip="Sort by first name" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StuEnrollId" HeaderText="StuEnrollId" SortExpression="StuEnrollId"
                                        UniqueName="StuEnrollId" Display="False">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName"
                                        UniqueName="LastName" HeaderTooltip="Sort by last name" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentNumber" HeaderText="Student Number" SortExpression="StudentNumber"
                                        UniqueName="StudentNumber" HeaderTooltip="Sort by student number" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentId" DataType="System.Guid" HeaderText="StudentId"
                                        SortExpression="StudentId" UniqueName="StudentId" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSNSearch"
                                        UniqueName="SSN" HeaderTooltip="Sort By SSN" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SSNSearch" HeaderText="SSNSearch" SortExpression="SSNSearch"
                                        UniqueName="SSNSearch" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StudentType" HeaderText="StudentType" SortExpression="StudentType"
                                        UniqueName="StudentType" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="EnrollmentID" HeaderText="Enrollment #" SortExpression="EnrollmentID"
                                        UniqueName="EnrollmentID" Visible="true" HeaderTooltip="Sort by enrollment id" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PrgVerDescrip" HeaderText="Program" SortExpression="PrgVerDescrip"
                                        UniqueName="PrgVerDescrip" Visible="true" HeaderTooltip="Sort by program version" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PrgVerId" HeaderText="PrgVerId" SortExpression="PrgVerId"
                                        UniqueName="PrgVerId" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StatusCodeDescrip" HeaderText="Status" SortExpression="StatusCodeDescrip"
                                        UniqueName="StatusCodeDescrip" Visible="true" HeaderTooltip="Sort by status" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StatusCodeId" HeaderText="Status" SortExpression="StatusCodeId"
                                        UniqueName="StatusCodeId" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ShiftDescrip" HeaderText="Shift" SortExpression="ShiftDescrip"
                                        UniqueName="ShiftDescrip" Visible="true" HeaderTooltip="Sort by shift" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ShiftID" HeaderText="Shift" SortExpression="ShiftID"
                                        UniqueName="ShiftID" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IsTransHold" HeaderText="trans Hold" SortExpression="IsTransHold"
                                        UniqueName="IsTransHold" Visible="false" ShowSortIcon="true">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" />
                            </MasterTableView>
                            <ClientSettings AllowRowsDragDrop="True">
                                <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                            </ClientSettings>
                            <PagerStyle Mode="NumericPages" />
                        </telerik:RadGrid>
                    </div>
                </asp:Panel>
            </div>
            <div style="width: 100%; text-align: center;">
                <asp:LinkButton  ID="btnMoveSelectedUpMRU" runat="server" OnClick="btnMoveSelectedUpMRU_Click"
                                 EnableViewState="false" ToolTip="Move Selected Students Down">
                    <span class="k-icon k-i-arrow-up font-blue k-icon-32"></span>
                </asp:LinkButton>
                <asp:LinkButton ID="btnMoveSelectedDownMRU" runat="server" OnClick="btnMoveSelectedDownMRU_Click"
                                EnableViewState="false" ToolTip="Move Selected Students Up" >
                    <span class="k-icon k-i-arrow-down font-blue k-icon-32"></span>
                </asp:LinkButton>

            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>

    <asp:Panel ID="Panel3" runat="server" CssClass="SelectedContainer">
        <div id="CaptionLabel2" class="CaptionLabel2" runat="server"><%=Caption2%></div>
        <div id="StudentSearchFilters3" class="StudentSearchFilters" style="width: 100%; text-align: center">
            <div id="SearchBoxDiv" class="SearchBox">
                <asp:LinkButton ID="lbnResetSelected" runat="server" OnClick="lbnResetSelected_Click" Visible="false" EnableViewState="false">Reset Selected</asp:LinkButton>
            </div>
        </div>
        <div id="SelectedStuEnrollmentsGrid" class="SelectedStudentGrid" runat="server">
            <telerik:RadGrid runat="server" AllowPaging="false" ID="RadGridSelectedStuEnrollments"
                OnNeedDataSource="RadGridSelectedStuEnrollments_NeedDataSource" Width="95%"
                OnRowDrop="RadGridSelectedStuEnrollments_RowDrop" AllowMultiRowSelection="True"
                GridLines="None" AllowSorting="true" PageSize="5" EnableLinqExpressions="false">
                <SortingSettings EnableSkinSortStyles="false" />
                <HeaderContextMenu EnableAutoScroll="True">
                </HeaderContextMenu>
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView DataKeyNames="StuEnrollId" AutoGenerateColumns="False" AllowMultiColumnSorting="false" AllowPaging="false" PageSize="5">
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <NoRecordsTemplate>
                        <div style="height: 60px; cursor: pointer;">
                            You currently have no student enrollments selected for the report
                        </div>
                    </NoRecordsTemplate>
                    <Columns>
                        <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"
                            UniqueName="FirstName" HeaderTooltip="Sort by first name" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StuEnrollId" HeaderText="StuEnrollId" SortExpression="StuEnrollId"
                            UniqueName="StuEnrollId" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName"
                            UniqueName="LastName" HeaderTooltip="Sort by last name" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StudentNumber" HeaderText="Student Number" SortExpression="StudentNumber"
                            UniqueName="StudentNumber" HeaderTooltip="Sort by student number" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StudentId" DataType="System.Guid" HeaderText="StudentId"
                            SortExpression="StudentId" UniqueName="StudentId" Visible="false" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSNSearch"
                            UniqueName="SSN" HeaderTooltip="Sort by SSN" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SSNSearch" HeaderText="SSNSearch" SortExpression="SSNSearch"
                            UniqueName="SSNSearch" Visible="false" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StudentType" HeaderText="StudentType" SortExpression="StudentType"
                            UniqueName="StudentType" Visible="false" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="EnrollmentID" HeaderText="Enrollment ID" SortExpression="EnrollmentID"
                            UniqueName="EnrollmentID" Visible="true" HeaderTooltip="Sort by enrollment id" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PrgVerDescrip" HeaderText="Program" SortExpression="PrgVerDescrip"
                            UniqueName="PrgVerDescrip" Visible="true" HeaderTooltip="Sort by program version" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PrgVerId" HeaderText="PrgVerId" SortExpression="PrgVerId"
                            UniqueName="PrgVerId" Visible="false" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StatusCodeDescrip" HeaderText="Status" SortExpression="StatusCodeDescrip"
                            UniqueName="StatusCodeDescrip" Visible="true" HeaderTooltip="Sort by status" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StatusCodeId" HeaderText="Status" SortExpression="StatusCodeId"
                            UniqueName="StatusCodeId" Visible="false" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ShiftDescrip" HeaderText="Shift" SortExpression="ShiftDescrip"
                            UniqueName="ShiftDescrip" Visible="true" HeaderTooltip="Sort by shift" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ShiftID" HeaderText="Shift" SortExpression="ShiftID"
                            UniqueName="ShiftID" Visible="false" ShowSortIcon="true">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <PagerStyle Mode="NumericPages" />
                </MasterTableView>
                <ClientSettings AllowRowsDragDrop="True">
                    <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                </ClientSettings>
                <PagerStyle Mode="NumericPages" />
            </telerik:RadGrid>
        </div>
    </asp:Panel>


    <!-- start validation panel-->
    <asp:Panel ID="Panel4" runat="server" CssClass="validationsummary"></asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
        ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
        ShowMessageBox="True"></asp:ValidationSummary>
    <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    <!--end validation panel-->


</asp:Panel>

<div class="ResetDiv">
</div>
