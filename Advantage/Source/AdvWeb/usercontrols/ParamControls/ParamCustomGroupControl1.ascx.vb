﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Partial Class ParamCustomGroupControl1
    Inherits UserControl
    Implements ICustomControl
#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = Value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        SetProperties()
        If Not Page.IsPostBack Then
            'WorkUnitsPanel.Visible = True
            For Each cbk As ListItem In cblGroupByType.Items
                cbk.Selected = True
            Next
        End If
    End Sub
    Protected Sub rblShowWorkUnits_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rblShowWorkUnits.SelectedIndexChanged
        HideShowControls()
    End Sub
#End Region
#Region "Methods"
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Try
            Dim DisplayTree As New RadTreeView
            DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
            Dim Selected As ListItem = rblShowWorkUnits.SelectedItem
            Dim ReportOptionsNode As New RadTreeNode
            ReportOptionsNode.Text = "Custom Options"
            ReportOptionsNode.Value = CStr(ItemDetail.DetailId)
            ReportOptionsNode.CssClass = "TreeParentNode"
            Dim LayoutOptionsNode As New RadTreeNode
            Dim ShowWorkUnitsNode As New RadTreeNode
            LayoutOptionsNode.Text = Caption
            ShowWorkUnitsNode.Text = Selected.Text
            LayoutOptionsNode.CssClass = "TreeParentNode"
            If Selected.Value = "True" Then
                For Each item As ListItem In cblGroupByType.Items
                    If item.Selected = True Then
                        Dim SelectedWorkUnitsNode As New RadTreeNode
                        SelectedWorkUnitsNode.Text = item.Text
                        ShowWorkUnitsNode.Nodes.Add(SelectedWorkUnitsNode)
                    End If
                Next
            End If
            LayoutOptionsNode.Nodes.Add(ShowWorkUnitsNode)
            Dim MiscOptionsNode As New RadTreeNode
            MiscOptionsNode.Text = "Misc Options"
            MiscOptionsNode.CssClass = "TreeParentNode"
            For Each item As ListItem In cblCustomOptions.Items
                Dim SelectedMiscOptionNode As New RadTreeNode
                If item.Selected = True Then
                    SelectedMiscOptionNode.Text = item.Text & " - Yes"
                Else
                    SelectedMiscOptionNode.Text = item.Text & " - No"
                End If
                MiscOptionsNode.Nodes.Add(SelectedMiscOptionNode)
            Next
            ReportOptionsNode.Nodes.Add(LayoutOptionsNode)
            ReportOptionsNode.Nodes.Add(MiscOptionsNode)

            DisplayTree.Nodes.Add(ReportOptionsNode)
            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            UserSettings.ItemName = ItemDetail.ItemName
            UserSettings.ItemId = ItemDetail.ItemId
            UserSettings.DetailId = ItemDetail.DetailId
            UserSettings.FriendlyName = ItemDetail.CaptionOverride

            'rblShowWorkUnits Settings
            Dim ctrlSettingShowWorkUnits As New ControlSettingInfo
            Dim ctrlValuesShowWorkUnits As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo

            ctrlSettingShowWorkUnits.ControlName = rblShowWorkUnits.ID

            Dim Selected As ListItem = rblShowWorkUnits.SelectedItem
            objControlValueInfo.DisplayText = Selected.Text
            objControlValueInfo.KeyData = Selected.Value

            ctrlValuesShowWorkUnits.Add(objControlValueInfo)
            ctrlSettingShowWorkUnits.ControlValueCollection = ctrlValuesShowWorkUnits

            SettingsCollection.Add(ctrlSettingShowWorkUnits)

            If Selected.Value = "True" Then
                'cblGroupByType Settings
                Dim ctrlSettingGroupByType As New ControlSettingInfo
                ctrlSettingGroupByType.ControlName = cblGroupByType.ID
                Dim ctrlValuesGroupByType As New List(Of ControlValueInfo)
                For Each item As ListItem In cblGroupByType.Items
                    Dim ValueInfo As New ControlValueInfo
                    ValueInfo.DisplayText = item.Text.ToString
                    ValueInfo.KeyData = item.Selected.ToString
                    ctrlValuesGroupByType.Add(ValueInfo)
                Next
                ctrlSettingGroupByType.ControlValueCollection = ctrlValuesGroupByType
                SettingsCollection.Add(ctrlSettingGroupByType)
            End If
            'cblCustomOptions Settings
            Dim ctrlSettingCustomOptions As New ControlSettingInfo
            ctrlSettingCustomOptions.ControlName = cblCustomOptions.ID
            Dim ctrlValuesCustomOptions As New List(Of ControlValueInfo)
            For Each item As ListItem In cblCustomOptions.Items
                Dim ValueInfo As New ControlValueInfo
                ValueInfo.DisplayText = item.Text.ToString
                ValueInfo.KeyData = item.Selected.ToString
                ctrlValuesCustomOptions.Add(ValueInfo)
            Next
            ctrlSettingCustomOptions.ControlValueCollection = ctrlValuesCustomOptions
            SettingsCollection.Add(ctrlSettingCustomOptions)

            UserSettings.ControlSettingsCollection = SettingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try

        Return UserSettings


    End Function
    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is CheckBoxList Then
                            Dim ChkBoxList As CheckBoxList = DirectCast(ctrl, CheckBoxList)
                            For Each Chk As ListItem In ChkBoxList.Items
                                Chk.Selected = False
                            Next
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim chkitem As ListItem = ChkBoxList.Items.FindByText(ItemValue.DisplayText)
                                If Not chkitem Is Nothing Then
                                    chkitem.Selected = CBool(ItemValue.KeyData)
                                End If

                            Next
                        Else
                            Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
                HideShowControls()
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub HideShowControls()
        Try


            Dim cbkEnabled As Boolean
            If CBool(rblShowWorkUnits.SelectedValue) = False Then
                cbkEnabled = False
                WorkUnitsPanel.Visible = False
            Else
                WorkUnitsPanel.Visible = False 'True
                cbkEnabled = False 'True
            End If
            For Each cbk As ListItem In cblGroupByType.Items
                cbk.Enabled = cbkEnabled
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
#End Region




End Class
