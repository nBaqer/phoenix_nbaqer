﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports FAME.Parameters.Collections
Imports FAME.Parameters.Interfaces
Imports FAME.AdvantageV1.Common
Partial Class ParamSetPanelBarFilterControl
    Inherits UserControl
    Implements IParamSetControl
#Region "Properties"
    Private _Report As ReportInfo
    Private _ParameterSetLookup As String
    Private _SetId As Integer
    Private _SqlConn As String
    Private _DisplayName As String
    Private _ParamSet As ParameterSetInfo
    Public Property ParameterSetLookup() As String Implements IParamSetControl.ParameterSetLookup
        Get
            Return _ParameterSetLookup
        End Get
        Set(ByVal Value As String)
            _ParameterSetLookup = Value
        End Set
    End Property
    Public Property SetId() As Integer Implements IParamSetControl.SetId
        Get
            Return _SetId
        End Get
        Set(ByVal Value As Integer)
            _SetId = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements IParamSetControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    'Public ReadOnly Property ParameterSet() As ParameterSetInfo
    '    Get
    '        Return GetParamSet()
    '    End Get
    'End Property
    Public Property ParamSet() As ParameterSetInfo Implements IParamSetControl.ParamSet
        Get
            Return _ParamSet
        End Get
        Set(ByVal Value As ParameterSetInfo)
            _ParamSet = Value
        End Set
    End Property
    Public Property DisplayName() As String Implements IParamSetControl.DisplayName
        Get
            If String.IsNullOrEmpty(_DisplayName) = True Then
                _DisplayName = ParamSet.SetDisplayName
            End If
            Return _DisplayName

        End Get
        Set(ByVal Value As String)
            _DisplayName = Value
        End Set
    End Property
#End Region
#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            'AddClientScripts()
            If Not IsPostBack Then
                Cache.Remove(ParameterSetLookup)
                Session.Remove(ParamSet.SetName & "_Filters")
                Session.Remove(ParamSet.SetName)
                If Not Session(ParamSet.SetName & "_Filters") Is Nothing Then
                    Dim objMaster As MasterDictionary = CType(Session(ParamSet.SetName & "_Filters"), MasterDictionary)
                End If
                'Session(ParamSet.SetName & "_Filters") = Nothing
                CreatePanelItems()
            End If
            GetMasterFilterSet()
            GetMasterReturnSet()
            GetCollectionSet()
            AddUserControls()
            '
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            lblError.Text = CreateErrorMsg(ex)
        End Try
    End Sub
#End Region
#Region "Methods"
    'Private Sub AddClientScripts()
    '    Try
    '        'Form the script
    '        Dim sb As New StringBuilder
    '        sb.Append("<script type=" & Convert.ToChar(34) & "text/javascript" & Convert.ToChar(34) & ">").AppendLine()

    '        sb.Append("function ")
    '        sb.Append("OnClientTransferredHandlerFor")
    '        sb.Append(ParamSet.SetName)
    '        sb.Append("(sender, e) {").AppendLine()
    '        sb.Append("var myitems = sender;").AppendLine()
    '        sb.Append("var radbox = $find('")
    '        sb.Append(CurrentlyTransferredValues.ClientID)
    '        sb.Append("');").AppendLine()
    '        sb.Append("var radbox2 = $find('")
    '        sb.Append(SourceListBox.ClientID)
    '        sb.Append("');").AppendLine()
    '        sb.Append("myitems = e.get_items();").AppendLine()
    '        sb.Append("radbox.set_value(" & Convert.ToChar(34) & Convert.ToChar(34) & ");").AppendLine()
    '        sb.Append("$telerik.$.each(myitems, function() {").AppendLine()
    '        sb.Append("radbox.set_value(radbox.get_value() + this.get_value() + " & Convert.ToChar(34) & "," & Convert.ToChar(34) & ");").AppendLine()
    '        sb.Append("});").AppendLine()
    '        sb.Append("radbox2.set_value(e.get_sourceListBox().get_id());").AppendLine()
    '        sb.Append("}").AppendLine()
    '        sb.Append("</script>")

    '        If (Not Page.ClientScript.IsClientScriptBlockRegistered("OnClientTransferredHandlerFor" & ParamSet.SetName)) Then
    '            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "OnClientTransferredHandlerFor" & ParamSet.SetName, sb.ToString)
    '        End If
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Sub
    Private Sub LoadParamSet()
        'GetParamSet()
        CreatePanelItems()
    End Sub
    Private Sub GetCollectionSet()
        Dim ObjDetails As DetailDictionary
        If Session("CollectionSet_" & ParamSet.SetName) Is Nothing Then
            ObjDetails = New DetailDictionary
            ObjDetails.Name = ParamSet.SetName
        Else
            ObjDetails = DirectCast(Session("CollectionSet_" & ParamSet.SetName), DetailDictionary)
        End If
        Session("CollectionSet_" & ParamSet.SetName) = ObjDetails
    End Sub
    Private Sub GetMasterFilterSet()
        Dim objMaster As MasterDictionary
        If Session(ParamSet.SetName) Is Nothing Then
            objMaster = New MasterDictionary
            objMaster.Name = ParamSet.SetName & "_Filters"
        Else
            objMaster = DirectCast(Session(ParamSet.SetName & "_Filters"), MasterDictionary)
        End If
        Session(ParamSet.SetName & "_Filters") = objMaster
    End Sub
    'Private Sub GetMasterFilterSet2()
    '    Dim objMaster As ParameterSetFilterInfo
    '    If Session(ParamSet.SetName) Is Nothing Then
    '        objMaster = New ParameterSetFilterInfo
    '        objMaster.ParamSetName = ParamSet.SetName & "_Test"
    '    Else
    '        objMaster = DirectCast(Session(ParamSet.SetName & "_Test"), ParameterSetFilterInfo)
    '    End If
    '    Session(ParamSet.SetName & "_Test") = objMaster
    'End Sub
    Private Sub GetMasterReturnSet()
        Dim objMaster As MasterDictionary
        If Session(ParamSet.SetName) Is Nothing Then
            objMaster = New MasterDictionary
            objMaster.Name = ParamSet.SetName
        Else
            objMaster = DirectCast(Session(ParamSet.SetName), MasterDictionary)
        End If
        Session(ParamSet.SetName) = objMaster
    End Sub
    Private Sub CreatePanelItems()
        Try
            Dim PSet As ParameterSetInfo = ParamSet
            'Dim PSet As ParameterSetInfo = ParamSet
            For Each sectioninfo As ParameterSectionInfo In PSet.ParameterSectionCollection
                Dim RootItem As RadPanelItem = CreateRootItem(sectioninfo)
                RadPanelBarParamSet.Items.Add(RootItem)
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Function CreateRootItem(ByVal sectioninfo As ParameterSectionInfo) As RadPanelItem
        Dim RootItem As New RadPanelItem
        Try
            If sectioninfo.SectionSeq = 1 Then
                RootItem.Expanded = True
            Else
                RootItem.Expanded = False
            End If
            RootItem.Text = sectioninfo.SectionCaption
            RootItem.ToolTip = sectioninfo.SectionDescription
            RootItem.Value = sectioninfo.SectionName + " Root"
            RootItem.Items.Add(CreateChildItem(sectioninfo))

            Return RootItem
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function CreateChildItem(ByVal sectioninfo As ParameterSectionInfo) As RadPanelItem
        Dim ChildItem As New RadPanelItem
        Try
            ChildItem.Value = sectioninfo.SectionName
            ChildItem.ToolTip = sectioninfo.SectionDescription
            Return ChildItem
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Sub AddUserControls()
        Try
            Dim PSet As ParameterSetInfo = ParamSet

            For Each sectioninfo As ParameterSectionInfo In PSet.ParameterSectionCollection
                ' If Not sectioninfo.SectionId = 1 Then
                Dim ChildItem As RadPanelItem = RadPanelBarParamSet.FindItemByValue(sectioninfo.SectionName)

                'Add ParamItem usercontrols to ParamSet usercontrol
                For Each item As ParameterDetailItemInfo In sectioninfo.ParameterDetailItemCollection
                    'Dim divCaption As New LiteralControl
                    Dim userctrl As UserControl
                    Dim myParamItemControl As IParamItemControl
                    '
                    'divCaption.Text = "<div id='captionlabel' class='CaptionLabel' >" & item.Caption.ToString & "</div>"

                    'Load the UserControl and then set the ID property
                    userctrl = CType(LoadControl("~/UserControls/ParamControls/" & item.ControllerClass), UserControl)
                    userctrl.ID = item.ItemName.ToString

                    'convert from UserControl to IParamItemControl to use the properties from the interface
                    myParamItemControl = CType(userctrl, IParamItemControl)
                    myParamItemControl.ItemDetail = item
                    myParamItemControl.SqlConn = SqlConn

                    'convert back to UserControl
                    userctrl = CType(myParamItemControl, UserControl)
                    'ChildItem.Controls.Add(divCaption)
                    ChildItem.Controls.Add(userctrl)
                Next
                '    End If

            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    'Public Function GetParamSet() As ParameterSetInfo
    '    Dim PSet As New ParameterSetInfo
    '    Try
    '        'Step 1: Try to read the value from cache
    '        PSet = CType(Cache(ParameterSetLookup), ParameterSetInfo)
    '        'Step 2 : If nothing is stored in the cache, read the ParameterSetInfo from the DataAccess and store the output in a cache
    '        'The cache is named after the ParameterSetLookup
    '        If PSet Is Nothing Then
    '            Dim DA As New ParamDA(SqlConn)
    '            PSet = DA.GetParamSetBySetName(ParameterSetLookup)
    '            Cache(ParameterSetLookup) = PSet
    '        End If
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        lblError.Text = CreateErrorMsg(ex)
    '    End Try
    '    Return PSet
    'End Function
    Private Function CreateErrorMsg(ByVal ex As Exception) As String
        Dim msg As String
        If ex.InnerException Is Nothing Then
            msg = ex.ToString
        Else
            msg = ex.Message + ex.InnerException.ToString
        End If
        Return msg
    End Function
#End Region

End Class
