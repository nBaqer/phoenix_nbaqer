﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamTSDCDateControl.ascx.vb" Inherits="ParamTSDCDateControl" %>


<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadDatePicker1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadPanelBarParamSet" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

    <div id="MainContainer" class="MainContainer"> 
            <asp:Panel ID="ContainerPanel" runat="server">
          <div id="captionlabel" class="CaptionLabel" runat="server"><%=Caption%></div>
                <asp:Literal ID="InstructText" runat="server"></asp:Literal>   
<div id="DateRow1" class="DateRow1">
 <span id="DateModifier1" class="DateModifier1">
     <telerik:RadComboBox ID="RadComboBox1" runat="server" Width="150px" 
        >
       <Items>
           <telerik:RadComboBoxItem runat="server" Text="IsEqualTo" 
               Value="0" />
           <telerik:RadComboBoxItem runat="server" Text="IsNotEqualTo" 
               Value="1" />
           <telerik:RadComboBoxItem runat="server" Text="GreaterThanOrEqualTo" 
               Value="2" />
           <telerik:RadComboBoxItem runat="server" Text="LessThanOrEqualTo" 
               Value="3" />
           <telerik:RadComboBoxItem runat="server" Text="GreaterThan" 
               Value="4" />
           <telerik:RadComboBoxItem runat="server" Text="LessThan" 
               Value="5" />
           <telerik:RadComboBoxItem runat="server" Text="InList" 
               Value="6" />
           <telerik:RadComboBoxItem runat="server" Text="IsNull" Value="7" />
           <telerik:RadComboBoxItem runat="server" Text="IsNotNull" Value="8" />
           <telerik:RadComboBoxItem runat="server" Text="IsEmpty" Value="9" />
             <telerik:RadComboBoxItem runat="server" Text="IsNotEmpty" Value="10" />
           <telerik:RadComboBoxItem runat="server" Text="Contains" Value="11" />
           <telerik:RadComboBoxItem runat="server" Text="StartsWith" Value="12" />
           <telerik:RadComboBoxItem runat="server" Text="EndsWith" Value="13" />
       </Items>
</telerik:RadComboBox>
 </span>
 <span id="DatePicker1" class="DatePicker1">
 <telerik:RadDatePicker ID="RadDatePicker1" Runat="server" 
    Width="151px" >
<Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

<DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ></DateInput>
</telerik:RadDatePicker>
 </span>
 <span>
     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ErrorMessage="Please enter a Date" ControlToValidate="RadDatePicker1" 
        Enabled="false" ></asp:RequiredFieldValidator>
 </span>
</div>
<div id="DateRow2">
<span id="DateModifier2" class="DateModifier2">
    <telerik:RadComboBox ID="RadComboBox2" runat="server" Width="150px" 
        >
       <Items>
           <telerik:RadComboBoxItem runat="server" Text="LessThanOrEqualTo" 
               Value="3" />
           <telerik:RadComboBoxItem runat="server" Text="LessThan" 
               Value="5" />
       </Items>
</telerik:RadComboBox>
</span>
<span id="DatePicker2" class="DatePicker2">
<telerik:RadDatePicker ID="RadDatePicker2" Runat="server" 
    Width="151px">
<Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

<DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ></DateInput>
</telerik:RadDatePicker>
</span>
 <span>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
        ErrorMessage="Please enter a Date" Display="Static" 
        ControlToValidate="RadDatePicker2" Enabled="false"></asp:RequiredFieldValidator>
 <asp:CompareValidator ID="CompareValidator" runat="Server" ControlToCompare="RadDatePicker1"
ControlToValidate="RadDatePicker2" Operator="GreaterThan" 
        ErrorMessage=" Date range is not valid" Display="Dynamic" Enabled="false" />  
 </span>
</div>
</asp:Panel>
        </div>
    
<div class="ResetDiv">
</div>    