﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common

Partial Class ParamPendingGrad
    Inherits UserControl
    Implements ICustomControl

#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString
    Protected MyAdvAppSettings As AdvAppSettings


    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = Value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        SetProperties()
        If Not Page.IsPostBack Then
            FillCmpGrpControl()
        End If
        FillPrgVerControl()
        ListBoxCounts()
    End Sub


    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
        Dim LBox As RadListBox = DirectCast(sender, RadListBox)

        If LBox.ID.Contains("CmpGrp") Then
            FillPrgVerControl()
        End If
        ListBoxCounts()
    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ChkBox As CheckBox = DirectCast(sender, CheckBox)
        If ChkBox.ID.Contains("CmpGrp") Then
            FillCmpGrpControl()
        ElseIf ChkBox.ID.Contains("PrgVer") Then
            FillPrgVerControl()
        End If
        ListBoxCounts()
    End Sub
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim Lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arPrgVersion Then
                Dim MyarPrgVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                If MyarPrgVersion.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarPrgVersion.PrgVerDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is syCampGrp Then
                Dim MysyCampGrp As syCampGrp = DirectCast(myItem.DataItem, syCampGrp)
                If MysyCampGrp.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampGrp.CampGrpDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub



    Protected Sub ValidateRequired(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        If RadListBoxCmpGrp2.Items.Count > 0 Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

#End Region


#Region "Methods"
    Public Sub FillCmpGrpControl()
        Try
            RadListBoxCmpGrp.DataTextField = "Campgrpdescrip"
            RadListBoxCmpGrp.DataValueField = "CampgrpId"
            RadListBoxCmpGrp.DataSource = GetCampGrpByUserId()
            RadListBoxCmpGrp.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillPrgVerControl()
        Try
            RadListBoxPrgVer.DataTextField = "PrgVerDescrip"
            RadListBoxPrgVer.DataValueField = "PrgVerId"
            RadListBoxPrgVer.DataSource = GetPrgVersionsByCmpGrpId(GetCurrentlySelectedCmpGrps)
            RadListBoxPrgVer.DataBind()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Function GetCurrentlySelectedCmpGrps() As List(Of String)
        Dim SelectedCmpGrps As New List(Of String)
        Try
            If RadListBoxCmpGrp2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxCmpGrp2.Items
                    Dim selected As String = item.Value
                    SelectedCmpGrps.Add(selected)
                Next
                Return SelectedCmpGrps
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCurrentlySelectedPrgVersion() As List(Of String)
        Dim SelectedPrgVersions As New List(Of String)
        Try
            If RadListBoxPrgVer2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxPrgVer2.Items
                    Dim selected As String = item.Value
                    SelectedPrgVersions.Add(selected)
                Next
                Return SelectedPrgVersions
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCampGrpByUserId() As List(Of syCampGrp)
        Dim DA As New CampusGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampGrp)
        Dim SelectedCampus As List(Of String) = GetCurrentlySelectedCmpGrps()
        Try
            If SelectedCampus Is Nothing Then
                result = DA.GetCampusGrpsByUserId(New Guid(AdvantageSession.UserState.UserId.toString), GetStatusFilters("cbkInactiveCmpGrp"))
            Else
                result = DA.GetCampusGrpsByUserId(New Guid(AdvantageSession.UserState.UserId.toString), GetStatusFilters("cbkInactiveCmpGrp"), SelectedCampus)
            End If

            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    'Private Function GetPrgVersionsByCmpGrpId(ByVal SelectedCampus As List(Of String)) As List(Of arPrgVersion)
    '    Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
    '    Dim result As New List(Of arPrgVersion)
    '    Try
    '        Dim SelectedPrgVersion As List(Of String) = GetCurrentlySelectedPrgVersion()
    '        If SelectedPrgVersion Is Nothing Then
    '            result = DA.GetPrgVersionByCmpGrpId(New Guid(AdvantageSession.UserState.UserId.toString), SelectedCampus, GetStatusFilters("cbkInactivePrgVer"))
    '        Else
    '            result = DA.GetPrgVersionByCmpGrpId(New Guid(AdvantageSession.UserState.UserId.toString), SelectedCampus, GetStatusFilters("cbkInactivePrgVer"), SelectedPrgVersion)
    '        End If
    '        Return result
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Function

    Private Function GetPrgVersionsByCmpGrpId(ByVal SelectedCampus As List(Of String)) As List(Of arPrgVersion)
        Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arPrgVersion)
        Try
            Dim SelectedPrgVersion As List(Of String) = GetCurrentlySelectedPrgVersion()
            If SelectedPrgVersion Is Nothing Then
                result = DA.GetPrgVersionByCmpGrpId(New Guid(AdvantageSession.UserState.UserId.toString), SelectedCampus, GetStatusFilters("cbkInactivePrgVer"))
            Else
                result = DA.GetPrgVersionByCmpGrpId(New Guid(AdvantageSession.UserState.UserId.toString), SelectedCampus, GetStatusFilters("cbkInactivePrgVer"), SelectedPrgVersion)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim StatusValues As New List(Of String)

        Try
            Dim CKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If CKbox.Checked = True Then
                StatusValues.Add("Active")
                StatusValues.Add("Inactive")
            Else
                StatusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return StatusValues
    End Function
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim DisplayTree As New RadTreeView
        DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim HeaderNode1 As New RadTreeNode
            HeaderNode1.Text = "Program Version Type"
            HeaderNode1.Value = "Program Version Type"
            HeaderNode1.CssClass = "TreeParentNode"
            Dim PrgVerTypeNode As New RadTreeNode
            PrgVerTypeNode.Text = rblPrgVerType.SelectedItem.Text + "<=" + RadNumericTextBoxAmount.Text + " Amount left to complete"
            HeaderNode1.Nodes.Add(PrgVerTypeNode)
            DisplayTree.Nodes.Add(HeaderNode1)

            If RadListBoxCmpGrp2.Items.Count > 0 Then
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Campus Group"
                HeaderNode2.Value = "CmpGrp"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxCmpGrp2.Items
                    Dim CmpGrpNode As New RadTreeNode
                    CmpGrpNode.Text = item.Text
                    HeaderNode2.Nodes.Add(CmpGrpNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            If RadListBoxPrgVer2.Items.Count > 0 Then
                Dim HeaderNode3 As New RadTreeNode
                HeaderNode3.Text = "Prg Version"
                HeaderNode3.Value = "PrgVer"
                HeaderNode3.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxPrgVer2.Items
                    Dim PrgVersionNode As New RadTreeNode
                    PrgVersionNode.Text = item.Text
                    HeaderNode3.Nodes.Add(PrgVersionNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode3)
            End If
            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            UserSettings.ItemName = ItemDetail.ItemName
            UserSettings.ItemId = ItemDetail.ItemId
            UserSettings.DetailId = ItemDetail.DetailId
            UserSettings.FriendlyName = ItemDetail.CaptionOverride


            'Program Version Type
            Dim ctrlSettingPrgVerType As New ControlSettingInfo
            Dim ctrlValuesPrgVerType As New List(Of ControlValueInfo)
            Dim objControlValueInfo0 As New ControlValueInfo

            ctrlSettingPrgVerType.ControlName = rblPrgVerType.ID
            objControlValueInfo0.DisplayText = rblPrgVerType.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo0.KeyData = rblPrgVerType.SelectedValue.Replace("'", "''")

            ctrlValuesPrgVerType.Add(objControlValueInfo0)
            ctrlSettingPrgVerType.ControlValueCollection = ctrlValuesPrgVerType
            SettingsCollection.Add(ctrlSettingPrgVerType)

            'AmountLeftToComplete
            Dim ctrlSettingAmount As New ControlSettingInfo
            Dim ctrlValuesAmount As New List(Of ControlValueInfo)
            Dim objControlValueInfo10 As New ControlValueInfo

            ctrlSettingAmount.ControlName = RadNumericTextBoxAmount.ID
            If RadNumericTextBoxAmount.Text = "" Then
                objControlValueInfo10.DisplayText = "5"
                objControlValueInfo10.KeyData = "5"
            Else
                objControlValueInfo10.DisplayText = RadNumericTextBoxAmount.Text
                objControlValueInfo10.KeyData = RadNumericTextBoxAmount.Text
            End If
            ctrlValuesAmount.Add(objControlValueInfo10)
            ctrlSettingAmount.ControlValueCollection = ctrlValuesAmount
            SettingsCollection.Add(ctrlSettingAmount)


            'Campus Group
            Dim ctrlSettingCmpGrp As New ControlSettingInfo
            Dim ctrlValuesCmpGrp As New List(Of ControlValueInfo)
            ctrlSettingCmpGrp.ControlName = RadListBoxCmpGrp.ID
            For Each item As RadListBoxItem In RadListBoxCmpGrp.Items
                Dim objControlValueInfo1 As New ControlValueInfo
                objControlValueInfo1.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo1.KeyData = item.Value.Replace("'", "''")
                ctrlValuesCmpGrp.Add(objControlValueInfo1)
            Next
            ctrlSettingCmpGrp.ControlValueCollection = ctrlValuesCmpGrp
            SettingsCollection.Add(ctrlSettingCmpGrp)
            'Campus Group2
            Dim ctrlSettingCmpGrp2 As New ControlSettingInfo
            Dim ctrlValuesCmpGrp2 As New List(Of ControlValueInfo)
            ctrlSettingCmpGrp2.ControlName = RadListBoxCmpGrp2.ID
            For Each item As RadListBoxItem In RadListBoxCmpGrp2.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesCmpGrp2.Add(objControlValueInfo2)
            Next
            ctrlSettingCmpGrp2.ControlValueCollection = ctrlValuesCmpGrp2
            SettingsCollection.Add(ctrlSettingCmpGrp2)

            'Program Version 1
            Dim ctrlSettingPrgVer As New ControlSettingInfo
            Dim ctrlValuesPrgVer As New List(Of ControlValueInfo)
            ctrlSettingPrgVer.ControlName = RadListBoxPrgVer.ID
            For Each item As RadListBoxItem In RadListBoxPrgVer.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesPrgVer.Add(objControlValueInfo3)
            Next
            ctrlSettingPrgVer.ControlValueCollection = ctrlValuesPrgVer
            SettingsCollection.Add(ctrlSettingPrgVer)

            'Program Version 2
            Dim ctrlSettingPrgVer2 As New ControlSettingInfo
            Dim ctrlValuesPrgVer2 As New List(Of ControlValueInfo)
            ctrlSettingPrgVer2.ControlName = RadListBoxPrgVer2.ID
            For Each item As RadListBoxItem In RadListBoxPrgVer2.Items
                Dim objControlValueInfo4 As New ControlValueInfo
                objControlValueInfo4.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo4.KeyData = item.Value.Replace("'", "''")
                ctrlValuesPrgVer2.Add(objControlValueInfo4)
            Next
            ctrlSettingPrgVer2.ControlValueCollection = ctrlValuesPrgVer2
            SettingsCollection.Add(ctrlSettingPrgVer2)


            UserSettings.ControlSettingsCollection = SettingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return UserSettings
    End Function
    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim LstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            LstBox.Items.Clear()
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = ItemValue.DisplayText
                                newItem.Value = ItemValue.KeyData
                                LstBox.SelectedValue = ItemValue.KeyData
                                LstBox.Items.Add(newItem)
                            Next
                        ElseIf TypeOf ctrl Is RadNumericTextBox Then
                            Dim RadNumTextBox As RadNumericTextBox = DirectCast(ctrl, RadNumericTextBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadNumTextBox.Text = ItemValue.KeyData
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadListBoxCmpGrp.Items.Clear()
                RadListBoxCmpGrp2.Items.Clear()
                RadListBoxPrgVer.Items.Clear()
                RadListBoxPrgVer2.Items.Clear()
            End If
            ListBoxCounts()
            ' HideShowInstitutionType()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub ListBoxCounts()
        Dim boxcount1 As Integer = RadListBoxCmpGrp.Items.Count()
        Dim boxcount2 As Integer = RadListBoxCmpGrp2.Items.Count()
        Dim boxcount3 As Integer = RadListBoxPrgVer.Items.Count()
        Dim boxcount4 As Integer = RadListBoxPrgVer2.Items.Count()

        lblCmpGrpCounterAvailable.Text = boxcount1.ToString.Trim & " available"
        lblCmpGrpCounterAvailable.ToolTip = boxcount1.ToString.Trim & " available " & Caption & "(s)"
        lblCmpGrpCounterSelected.Text = boxcount2.ToString.Trim & " selected"
        lblCmpGrpCounterSelected.ToolTip = boxcount2.ToString.Trim & " selected " & Caption & "(s)"

        lblPrgVerCounterAvailable.Text = boxcount3.ToString.Trim & " available"
        lblPrgVerCounterAvailable.ToolTip = boxcount3.ToString.Trim & " available " & Caption & "(s)"
        lblPrgVerCounterSelected.Text = boxcount4.ToString.Trim & " selected"
        lblPrgVerCounterSelected.ToolTip = boxcount4.ToString.Trim & " selected " & Caption & "(s)"

    End Sub

    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
#End Region

End Class
