﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamPendingGrad.ascx.vb" Inherits="ParamPendingGrad" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">

        <div class=" MultiFilterReportContainer">
            <div id="SchoolReportingTypeHeader" class="CaptionLabel" runat="server">Program Version Type</div>
            <div class="ReportLeftMarginInput">
                <table>
                    <tr>
                        <td class="style1">
                            <div id="rblSchoolTypeSelector" runat="server">
                                <asp:RadioButtonList ID="rblPrgVerType" runat="server"
                                    RepeatDirection="Horizontal"
                                    ToolTip="Select the type of Program Version.">
                                    <asp:ListItem Selected="True" Value="Credit">Credits</asp:ListItem>
                                    <asp:ListItem Value="Clock">Hours</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </td>


                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxAmount" Value="5" runat="server">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <div id="radComboBoxInstitutionTypeLabel" runat="server">Amount left to complete </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
        <div id="CmpGrpSelector" class="CmpGrpSelector MultiFilterReportContainer" runat="server">
            <div id="captionlabel2" class="CaptionLabel" runat="server">Campus Group</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxCmpGrp" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxCmpGrp2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxCmpGrp2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveCmpGrp" runat="server"
                        OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true"
                        Text="Show Inactive"
                        ToolTip="Check this box to make inactive programs selectable" />
                    <span id="ProgramCounterAvailable" class="RadListBox1Counter">
                        <asp:Label ID="lblCmpGrpCounterAvailable" runat="server"></asp:Label>
                    </span>
                    <span id="ProgramCounterSelected" class="RadListBox2Counter">
                        <asp:Label ID="lblCmpGrpCounterSelected" runat="server"></asp:Label>
                    </span>
                </div>
            </div>
        </div>

        <div id="RequiredFieldContainer" class="RequiredFieldContainer" runat="server">
            <asp:CustomValidator ID="CustomValidator1" runat="server"
                Enabled="False" OnServerValidate="ValidateRequired" Display="Dynamic"> 
                    </asp:CustomValidator>
            <asp:Label ID="lblSingleSelectMsg" runat="server" Text="" Style="color: Red;"></asp:Label>
        </div>
        <div id="PrgVersionSelector" class="PrgVersionSelector MultiFilterReportContainer" runat="server">
            <div id="Div2" class="CaptionLabel" runat="server">Program Version</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxPrgVer" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxPrgVer2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxPrgVer2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div3" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactivePrgVer" runat="server"
                        OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true"
                        Text="Show Inactive"
                        ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span1" class="RadListBox1Counter">
                        <asp:Label ID="lblPrgVerCounterAvailable" runat="server"></asp:Label>
                    </span>
                    <span id="Span2" class="RadListBox2Counter">
                        <asp:Label ID="lblPrgVerCounterSelected" runat="server"></asp:Label>
                    </span>
                </div>
            </div>
        </div>



        <div class="ResetDiv"></div>
    </div>
</asp:Panel>

