﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamAttendanceSummaryReport.ascx.vb" Inherits="ParamAttendanceSummaryReport" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function checkFilter(sender, args) {
        if (args.get_destinationListBox().get_id().indexOf("RadListBoxActiveEnrollments2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one enrollment is allowed');
        }
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
        //var maxNumberOfItems = 1;
        var dest = args.get_destinationListBox();
        var totalCount = dest.get_items().get_count();
        var itemsToTransferCount = args.get_items().length;
        if (totalCount == maxNumberOfItems) {
            alert(message);
            args.set_cancel(true);
        } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
            while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                itemsToTransferCount--;
                args.get_items().pop();
            }
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>

<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">

        <div id="Div6" class="ProgramSelector MultiFilterReportContainer" runat="server">
            <div id="Div7" class="CaptionLabel" runat="server">Program</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxProgram" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListProgramBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxProgram2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxProgram2" runat="server"
                    OnTransferred="RadListProgramBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="chkInActivePrograms" runat="server" OnCheckedChanged="ProgramCheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>

        <div id="ProgramVersionSelector" class="ProgramVersionSelector MultiFilterReportContainer" runat="server">
            <div id="Div4" class="CaptionLabel" runat="server">Program Version</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxProgramVersions" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListProgramVersionBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxProgramVersions2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxProgramVersions2" runat="server"
                    OnTransferred="RadListProgramVersionBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="chkInActiveProgramVersions" runat="server" OnCheckedChanged="chkInActiveProgramVersions_OnCheckedChangedCheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive program versions selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="lblProgramVersionsCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="lblProgramVersionsCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>

        <div id="Div1" class="StudentGroupSelector MultiFilterReportContainer" runat="server">
            <div id="Div2" class="CaptionLabel" runat="server">Student Groups</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxStudentGroupSelectors" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxStudentGroupSelector_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxStudentGroupSelector2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxStudentGroupSelector2" runat="server"
                    OnTransferred="RadListBoxStudentGroupSelector_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="chkInActiveStudentGroupSelector" runat="server" OnCheckedChanged="StudentGroupSelectorCheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive program versions selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <br />
            </div>
        </div>

        <%--        <div id="Div8" class="ActiveEnrollmentsSelector" runat="server">
            <div id="Div9" class="CaptionLabel" runat="server">Active Enrollments</div>
            <telerik:RadListBox ID="RadListBoxActiveEnrollments" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListBoxEnrollments_OnTransferred"
                OnClientTransferring="checkFilter"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True"
                TransferToID="RadListBoxActiveEnrollments2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="False" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxActiveEnrollments2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="Div5" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="chkInActiveEnrollemnts" CssClass="hidden" runat="server" OnCheckedChanged="TitleIvStatusCheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive enrollments selectable" />
                <span id="ActiveEnrollmentsCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblActiveEnrollmentsCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>--%>
    </div>

</asp:Panel>
