﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamTitleIvSapNoticeReport.ascx.vb" Inherits="usercontrols_ParamControls_ParamTitleIvSapNoticeReport" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function checkFilter(sender, args) {
        if (args.get_destinationListBox().get_id().indexOf("RadListBoxActiveEnrollments2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one enrollment is allowed');
        }
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
            //var maxNumberOfItems = 1;
            var dest = args.get_destinationListBox();
            var totalCount = dest.get_items().get_count();
            var itemsToTransferCount = args.get_items().length;
            if (totalCount == maxNumberOfItems) {
                alert(message);
                args.set_cancel(true);
            } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
                while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                    itemsToTransferCount--;
                    args.get_items().pop();
                }
            }
        }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>

<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">
        <div id="CmpGrpSelector" class="CmpGrpSelector" runat="server">
            <div id="Div3" class="CaptionLabel" runat="server">Campus Group</div>
            <telerik:RadListBox ID="RadListBoxCmpGrp" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListBoxesCampusGroup_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                TransferToID="RadListBoxCmpGrp2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxCmpGrp2" runat="server"
                OnTransferred="RadListBoxesCampusGroup_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="InactiveCampusGroupCheckBoxContainer" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactiveCmpGrp" runat="server"
                    OnCheckedChanged="cbkInactiveCmpGrp_OnCheckedChanged" AutoPostBack="true"
                    Text="Show Inactive"
                    ToolTip="Check this box to make inactive programs selectable" />
                <span id="CampusGroupCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblCmpGrpCounterAvailable" runat="server"></asp:Label>
                </span>
                <span id="CampusGroupCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblCmpGrpCounterSelected" runat="server"></asp:Label>
                </span>
            </div>
        </div>

        <div id="CampusSelectorDiv" class="CampusSelector" runat="server">
            <div id="CampusSelectorLabel" class="CaptionLabel" runat="server">Campus</div>
            <telerik:RadListBox ID="RadListBoxCampus" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListCampusBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                TransferToID="RadListBoxCampus2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxCampus2" runat="server"
                OnTransferred="RadListCampusBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="InactiveCampusCheckBoxContainer" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="checkboxInactiveCampuses" runat="server"
                    OnCheckedChanged="checkboxInactiveCampuses_OnCheckedChanged" AutoPostBack="true"
                    Text="Show Inactive"
                    ToolTip="Check this box to make inactive programs selectable" />
                <span id="CampusCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </span>
                <span id="CampusCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="Label2" runat="server"></asp:Label>
                </span>
            </div>
        </div>


        <div id="Div6" class="ProgramSelector" runat="server">
            <div id="Div7" class="CaptionLabel" runat="server">Program</div>
            <telerik:RadListBox ID="RadListBoxProgram" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListProgramBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True"
                TransferToID="RadListBoxProgram2" SelectionMode="Multiple">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxProgram2" runat="server"
                OnTransferred="RadListProgramBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="Div5" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="chkInActivePrograms" runat="server" OnCheckedChanged="ProgramCheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                <span id="Span3" class="RadListBox1Counter">
                    <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                </span>
                <span id="Span4" class="RadListBox2Counter">
                    <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>

        <%--<div id="ProgramVersionSelector" class="ProgramVersionSelector" runat="server">
            <div id="Div4" class="CaptionLabel" runat="server">Program Version</div>
            <telerik:RadListBox ID="RadListBoxProgramVersions" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True"
                TransferToID="RadListBoxProgramVersions2" SelectionMode="Multiple">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxProgramVersions2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="Div5" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="chkInActiveProgramVersions" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive program versions selectable" />
                <span id="Span3" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramVersionsCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="Span4" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramVersionsCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>--%>

        <div id="Div1" class="TitleIvStatusSelector" runat="server">
            <div id="Div2" class="CaptionLabel" runat="server">Statuses</div>
            <telerik:RadListBox ID="RadListBoxTitleIvStatuses" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListBoxStatuses_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True"
                TransferToID="RadListBoxTitleIvStatuses2" SelectionMode="Multiple">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxTitleIvStatuses2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="Div5" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="chkInActiveTitleIvStatuses" runat="server" OnCheckedChanged="TitleIvStatusCheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive program versions selectable" />
                <span id="Span3" class="RadListBox1Counter">
                    <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                </span>
                <span id="Span4" class="RadListBox2Counter">
                    <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>

        <div id="Div8" class="ActiveEnrollmentsSelector" runat="server">
            <div id="Div9" class="CaptionLabel" runat="server">Active Enrollments</div>
            <telerik:RadListBox ID="RadListBoxActiveEnrollments" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListBoxEnrollments_OnTransferred"
                OnClientTransferring="checkFilter"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True"
                TransferToID="RadListBoxActiveEnrollments2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="False" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxActiveEnrollments2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="Div5" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="chkInActiveEnrollemnts" CssClass="hidden" runat="server" OnCheckedChanged="TitleIvStatusCheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive enrollments selectable" />
                <span id="ActiveEnrollmentsCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblActiveEnrollmentsCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>

    </div>

</asp:Panel>
