﻿Option Strict On
Imports Telerik.Web.UI
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports System
Imports System.Collections.Generic
Imports System.Web.UI
Imports FAME.Parameters.Info
Imports FAME.Parameters.Collections
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Reporting.Info

Partial Class ParamStudentSearchControl
    Inherits UserControl
    Implements ISearchControl

    Private UserId As Guid 'Remove this later!!!
    Private searchRequest As String

#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _PostbackCtrlName As String
    Private _ClientSearchFunction As String
    Private _Mode As ISearchControl.ModeType
    Private _SavedSettings As ParamItemUserSettingsInfo
    Private _SendData As Boolean

    Public Property ItemDetail() As ParameterDetailItemInfo Implements ISearchControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = Value
        End Set
    End Property
    Public Property DAClass() As String Implements ISearchControl.DAClass
        Get
            Return _DAClass
        End Get
        Set(ByVal Value As String)
            _DAClass = Value
        End Set
    End Property
    Public Property DAMethod() As String Implements ISearchControl.DAMethod
        Get
            Return _DAMethod
        End Get
        Set(ByVal Value As String)
            _DAMethod = Value
        End Set
    End Property
    Public Property BindingTextField() As String Implements ISearchControl.BindingTextField
        Get
            Return _BindingTextField
        End Get
        Set(ByVal Value As String)
            _BindingTextField = Value
        End Set
    End Property
    Public Property BindingValueField() As String Implements ISearchControl.BindingValueField
        Get
            Return _BindingValueField
        End Get
        Set(ByVal Value As String)
            _BindingValueField = Value
        End Set
    End Property
    Public Property Caption() As String Implements ISearchControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property Caption2() As String Implements ISearchControl.Caption2
        Get
            'Return _Caption
            Return CType(Session("Caption2_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption2_" & Me.ID) = Value
        End Set
    End Property

    Public Property Caption3() As String Implements ISearchControl.Caption3
        Get
            'Return _Caption
            Return CType(Session("Caption3_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption3_" & Me.ID) = Value
        End Set
    End Property

    Public Property AssemblyFilePathDA() As String Implements ISearchControl.AssemblyFilePathDA
        Get
            Return _AssemblyFilePathDA
        End Get
        Set(ByVal Value As String)
            _AssemblyFilePathDA = Value
        End Set
    End Property
    Private Property AssemblyDA() As Assembly Implements ISearchControl.AssemblyDA
        Get
            Return _AssemblyDA

        End Get
        Set(ByVal Value As Assembly)
            _AssemblyDA = Value

        End Set
    End Property
    Public Property SqlConn() As String Implements ISearchControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Private Property PostbackCtrlName() As String
        Get
            Return _PostbackCtrlName
        End Get
        Set(ByVal Value As String)
            _PostbackCtrlName = Value
        End Set
    End Property
    Public Property ClientSearchFunction() As String
        Get
            'Return _ClientSearchFunction
            Return CType(Session(_ClientSearchFunction), String)
        End Get
        Set(ByVal Value As String)
            '_ClientSearchFunction = Value
            Session(_ClientSearchFunction) = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ISearchControl.SavedSettings
        Get
            Return _SavedSettings
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSettings = value
        End Set
    End Property
    Public Property Mode() As ISearchControl.ModeType Implements ISearchControl.Mode
        Get
            Return _Mode
        End Get
        Set(ByVal value As ISearchControl.ModeType)
            _Mode = value
        End Set
    End Property
    Public Property SendData() As Boolean Implements ISearchControl.SendData
        Get
            Return _SendData
        End Get
        Set(ByVal Value As Boolean)
            _SendData = Value
        End Set
    End Property

    Protected Property StudentSearchStore() As IList(Of StudentInfo)
        Get
            Try
                Dim obj As Object = Session("StudentsSearch_VB")
                If obj Is Nothing Then
                    obj = GetStudents(GetAllDataFilters, "S")
                    Session("StudentsSearch_VB") = obj
                End If
                Return DirectCast(obj, IList(Of StudentInfo))
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Session("StudentsSearch_VB") = Nothing
            End Try
            Return New List(Of StudentInfo)
        End Get
        Set(ByVal value As IList(Of StudentInfo))
            Session("StudentsSearch_VB") = value
        End Set
    End Property

    Protected Property StudentSelectedStore() As IList(Of StudentInfo)
        Get
            Try
                Dim obj As Object = Session("StudentsSelected_VB")
                If obj Is Nothing Then
                    obj = New List(Of StudentInfo)()
                    Session("StudentsSelected_VB") = obj
                End If
                Return DirectCast(obj, IList(Of StudentInfo))
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Session("StudentsSelected_VB") = Nothing
            End Try
            Return New List(Of StudentInfo)
        End Get
        Set(ByVal value As IList(Of StudentInfo))
            Session("StudentsSelected_VB") = value
        End Set
    End Property

    Protected Property MRUStudentSearchStore() As IList(Of StudentInfo)
        Get
            Try
                Dim obj As Object = Session("MRUStudentsSearch_VB")
                If obj Is Nothing Then
                    obj = GetStudents(GetAllDataFilters, "M")
                    Session("MRUStudentsSearch_VB") = obj
                End If
                Return DirectCast(obj, IList(Of StudentInfo))
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Session("MRUStudentsSearch_VB") = Nothing
            End Try
            Return New List(Of StudentInfo)
        End Get
        Set(ByVal value As IList(Of StudentInfo))
            Session("MRUStudentsSearch_VB") = value
        End Set
    End Property

#End Region

#Region "Methods"
    Public Sub ClearControls() Implements ISearchControl.ClearControls
        Dim ClearedData As IList(Of StudentEnrollmentInfoSearch) = New List(Of StudentEnrollmentInfoSearch)
        RadGridSelectedStudents.DataSource = ClearedData
    End Sub
    Private Sub AddTBValueChangedScript()
        'Form the script
        Dim sb As New StringBuilder
        Dim TBValueChanged As String
        sb.Append("<script type=" & Convert.ToChar(34) & "text/javascript" & Convert.ToChar(34) & ">").AppendLine()

        sb.Append("function ")
        
        If (ItemDetail.CaptionOverride = "Student Enrollment Search") Then
            TBValueChanged = "TBValueChanged_13"
        Else
            TBValueChanged = "TBValueChanged_" + ItemDetail.DetailId.ToString()
        End If
        sb.Append("(sender, e) {").AppendLine()
        sb.Append("getEvent = event.keyCode;").AppendLine()
        sb.Append("if (getEvent == ""13"") {").AppendLine()
        sb.Append("var radTextBox = sender;").AppendLine()
        sb.Append("var inputRequest = radTextBox.get_textBoxValue();").AppendLine()
        sb.Append("var searchRequest = inputRequest.replace(/^\s+|\s+$/g, '');").AppendLine()
        sb.Append("if (searchRequest.length == 0) {").AppendLine()
        sb.Append("// ResetPage();").AppendLine()
        sb.Append(" } else {").AppendLine()
        sb.Append("radTextBox.raisePostBackEvent();").AppendLine()
        sb.Append("} } }").AppendLine()
        sb.Append("</script>").AppendLine()

        If (Not Page.ClientScript.IsClientScriptBlockRegistered(TBValueChanged)) Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), TBValueChanged, sb.ToString)
        End If

    End Sub

    Protected Sub RadGridSelectedReportItems_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGridSelectedStudents.DataSource = StudentSelectedStore()
    End Sub

    Protected Sub RadGridReportSearch_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGridStudentSearch.DataSource = StudentSearchStore()
    End Sub

    Protected Sub RadGridReportSearchMRU_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGridStudentSearchMRU.DataSource = MRUStudentSearchStore()
    End Sub

    Public Function GetStudents(ByVal filters As MasterDictionary, ByVal SearchType As String) As IList(Of StudentInfo)
        Dim Query As List(Of arStudent)
        Dim DA As New StudentDA(SqlConn)
        Dim results As List(Of StudentInfo) = New List(Of StudentInfo)

        Try
            If SearchType = "S" Then
                Query = DA.GetStudents
            Else
                'for now we are fudging the user and campus id's
                Dim userGUID, campusGUID As Guid
                userGUID = New Guid("A11CB992-2538-49A6-A726-6FD8DAD050FC")
                campusGUID = New Guid("1BB47DF1-E6F9-4467-A5D6-8FA5495BB571")
                Query = DA.GetMRUStudents(userGUID, campusGUID)
            End If

            Dim i As Integer
            For i = 0 To Query.Count - 1 Step i + 1
                Dim studentid As Guid = Query(i).StudentId
                Dim firstname As String = Query(i).FirstName.Trim
                Dim lastname As String = Query(i).LastName.Trim
                Dim ssn As String = FormatSSN(Query(i).SSN)
                Dim ssnsearch As String = (Query(i).SSN)
                Dim studentnumber = Query(i).StudentNumber
                results.Add(New StudentInfo(studentid, firstname, lastname, studentnumber, ssn, ssnsearch, SearchType))
            Next
            Return results
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function

    Public Sub Fill()
        Try
            RadGridStudentSearch.DataSource = StudentSearchStore
            RadGridStudentSearch.DataBind()
            RadGridSelectedStudents.DataSource = StudentSelectedStore
            RadGridSelectedStudents.DataBind()
            RadGridStudentSearchMRU.DataSource = MRUStudentSearchStore
            RadGridStudentSearchMRU.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Public Function GetStudent(ByVal studentsToSearchIn As IEnumerable(Of StudentInfo), ByVal studentId As Guid) As StudentInfo
        For Each student As StudentInfo In studentsToSearchIn
            If student.StudentID = studentId Then
                Return student
            End If
        Next
        Return Nothing
    End Function

    Private Sub GetReturnCollectionSet()
        Dim ReturnCollectionSet As DetailDictionary = DirectCast(Session("ReturnCollectionSet_" & ItemDetail.SetName), DetailDictionary)
        Dim ReturnValues As ParamValueDictionary
        Try
            ReturnValues = CreateReturnValues()

            If Not ReturnCollectionSet Is Nothing Then
                If ReturnCollectionSet.Contains(ItemDetail.ItemName) Then
                    'take out old values if they exist
                    ReturnCollectionSet.Remove(ItemDetail.ItemName)
                End If
                If ReturnValues.Count > 0 Then
                    'add new values if there are any
                    ReturnCollectionSet.Add(ItemDetail.ItemName, ReturnValues)
                End If
                'store all return values in session
                Session("ReturnCollectionSet_" & ItemDetail.SetName) = ReturnCollectionSet
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Private Function CreateReturnValues() As ParamValueDictionary
        Dim ReturnValues As New ParamValueDictionary
        Try
            ReturnValues = FillValueDictionary()
            ReturnValues.Name = ItemDetail.ReturnValueName
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return ReturnValues
    End Function

    Private Function FillValueDictionary() As ParamValueDictionary
        Dim ValueDictionary As New ParamValueDictionary
        Dim ModDictionary As New ModDictionary
        Dim CurrentlyTransferredValues As List(Of String) = GetCurrentlyTransferredValues()
        Dim SourceListBoxName As String = GetSourceListBox()
        Dim ItemsToAdd As New List(Of String)
        Dim ItemsToRemove As New List(Of String)
        Dim CurrentRadGridValues As New List(Of String)

        For Each item As GridDataItem In RadGridSelectedStudents.Items
            CurrentRadGridValues.Add(item.KeyValues.Substring(item.KeyValues.IndexOf(":") + 2, 36).ToString())
        Next


        If PostbackCtrlName.Contains(ItemDetail.ItemName) Then
            If SourceListBoxName.Contains("RadGridSelectedStuEnrollments") Then
                ItemsToRemove = CurrentlyTransferredValues
                For Each deleted As String In ItemsToRemove
                    CurrentRadGridValues.Remove(deleted)
                Next
            End If
            If SourceListBoxName.Contains("RadGridStuEnrollmentSearch") Then
                ItemsToAdd = CurrentlyTransferredValues
            End If
        End If
        Try
            Select Case ItemDetail.ValueProp
                Case ParameterDetailItemInfo.ItemValueType.String
                    Dim PValues As New ParamValueList(Of String)
                    For Each item As String In CurrentRadGridValues
                        PValues.Add(item)
                    Next
                    For Each str As String In ItemsToAdd
                        PValues.Add(str)
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Long
                    Dim PValues As New ParamValueList(Of Long)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Decimal
                    Dim PValues As New ParamValueList(Of Decimal)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Date
                    Dim PValues As New ParamValueList(Of Date)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsDate(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Integer
                    Dim PValues As New ParamValueList(Of Integer)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsNumeric(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Boolean
                    Dim PValues As New ParamValueList(Of Boolean)
                    For Each item As String In CurrentRadGridValues
                        PValues.AddWithConvert(item)
                    Next
                    For Each str As String In ItemsToAdd
                        If IsBoolean(str) = True Then
                            PValues.AddWithConvert(str)
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case ParameterDetailItemInfo.ItemValueType.Guid
                    Dim PValues As New ParamValueList(Of Guid)
                    For Each item As String In CurrentRadGridValues
                        PValues.Add(New Guid(item))
                    Next
                    For Each str As String In ItemsToAdd
                        If IsGuid(str) = True Then
                            PValues.Add(New Guid(str))
                        End If
                    Next
                    If PValues.Count > 0 Then
                        ModDictionary.Add(ItemDetail.ItemName, PValues)
                    End If
                Case Else
                    Throw New Exception("Can not create parameter value collection: Unknown type")
            End Select
            If ModDictionary.Count > 0 Then
                ValueDictionary.Add(ModDictionary.Modifier.InList, ModDictionary)
            End If
            Return ValueDictionary
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function IsGuid(ByVal value As String) As Boolean
        Dim myGuid As Guid = Nothing
        Try
            myGuid = New Guid(value)
            Return True
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function IsBoolean(ByVal value As String) As Boolean
        Dim myBoolean As Boolean
        Try
            myBoolean = CBool(value)
            Return True
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function GetCurrentlyTransferredValues() As List(Of String)
        Try
            Dim txtbox As RadTextBox = CType(Me.Parent.Parent.Parent.Parent.FindControl("CurrentlyTransferredValues"), RadTextBox)
            Dim TransferredValues As New List(Of String)
            If Not txtbox.Text = String.Empty Then
                Dim transferred As String() = txtbox.Text.Split(New Char() {CChar(",")}, StringSplitOptions.RemoveEmptyEntries)
                For Each tran As String In transferred
                    TransferredValues.Add(tran)
                Next
            End If
            Return TransferredValues
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetSourceListBox() As String
        Try
            Dim txtbox As RadTextBox = CType(Me.Parent.Parent.Parent.Parent.FindControl("SourceListBox"), RadTextBox)
            Return txtbox.Text
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties(prop)
                Else
                    SetChildControlProperties(prop)
                End If
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub

    Private Function GetAllDataFilters() As MasterDictionary
        Dim objFilters As New DetailDictionary
        Dim objDetails As New DetailDictionary
        'objDetails = DirectCast(Session("CollectionSet_" & ItemDetail.SetName), DetailDictionary)
        Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName & "_Filters"), MasterDictionary)
        'Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName), MasterDictionary)
        objDetails.Name = ItemDetail.ItemName

        If objMaster.Contains(ItemDetail.ItemName) Then
            Dim x As String = ""
            objDetails = DirectCast(objMaster.Item(ItemDetail.ItemName), DetailDictionary)
        End If
        Dim StatusValueDictionary As New ParamValueDictionary()
        Dim UserIdValueDictionary As New ParamValueDictionary()
        Dim CurrentlySelectedValueDictionary As New ParamValueDictionary()
        Try
            objFilters.Name = ItemDetail.ItemName
            'StatusValueDictionary = GetStatusFilters(CheckBox1.Checked)
            objFilters.Add("Status", StatusValueDictionary)
            UserIdValueDictionary = GetUserFilters(UserId)
            objFilters.Add("UserId", UserIdValueDictionary)
            If RadGridStudentSearch.Items.Count > 0 Then
                CurrentlySelectedValueDictionary = GetCurrentlySelectedFilters(RadGridStudentSearch)
                objFilters.Add("CurrentlySelected", CurrentlySelectedValueDictionary)
            End If
            For Each d As ParamValueDictionary In objDetails.Values
                objFilters.Add(d.Name, d)
            Next
            If objMaster.Contains(ItemDetail.ItemName) Then
                objMaster.Remove(ItemDetail.ItemName)
            End If
            objMaster.Add(ItemDetail.ItemName, objFilters)
            Session(ItemDetail.SetName & "_Filters") = objMaster
            'Session(ItemDetail.SetName) = objMaster
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '  Throw ex
            Dim x As String = ex.ToString
        End Try
        Return objMaster
    End Function

    Private Function GetCurrentlySelectedFilters(ByVal ctrl As RadGrid) As ParamValueDictionary
        Dim ValueDictionary As New ParamValueDictionary
        Dim ModifierDictionary As New ModDictionary
        Dim RadListBoxValues As New ParamValueList(Of String)
        For Each item As RadListBoxItem In ctrl.Items
            RadListBoxValues.Add(item.Value)
        Next
        ModifierDictionary.Name = "InList"
        ModifierDictionary.Add("CurrentlySelected", RadListBoxValues)
        ValueDictionary.Add(ModDictionary.Modifier.InList, ModifierDictionary)
        ValueDictionary.Name = "CurrentlySelected"

        Return ValueDictionary
    End Function

    Private Function GetStatusFilters(ByVal ShowInActive As Boolean) As ParamValueDictionary
        Dim ValueDictionary As New ParamValueDictionary
        Dim ModifierDictionary As New ModDictionary
        Dim StatusValues As New ParamValueList(Of String)
        Try
            If ShowInActive = True Then
                StatusValues.Add("Active")
                StatusValues.Add("Inactive")
            Else
                StatusValues.Add("Active")
                ' StatusValues.Add("Inactive")
            End If
            ModifierDictionary.Name = "InList"
            ModifierDictionary.Add("Status", StatusValues)
            ValueDictionary.Add(ModDictionary.Modifier.InList, ModifierDictionary)
            ValueDictionary.Name = "Status"
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return ValueDictionary
    End Function
    Private Function GetUserFilters(ByVal UserId As Guid) As ParamValueDictionary
        Dim Valuedictionary As New ParamValueDictionary
        Dim ModifierDictionary As New ModDictionary
        Dim UserIds As New ParamValueList(Of Guid)
        Try
            UserIds.Add(UserId)
            ModifierDictionary.Add("UserId", UserIds)
            Valuedictionary.Add(ModDictionary.Modifier.InList, ModifierDictionary)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return Valuedictionary
    End Function

    Public Function FormatSSN(ByVal SSN As String) As String
        If Not SSN = String.Empty Then
            Return "*****" & Mid(SSN, 6, 4)
        Else
            Return String.Empty
        End If
    End Function

    Protected Sub FilterDisplay()
        Dim rtb As RadTextBox = Me.RadTextBox1
        Dim nameexpression As String
        Dim expression As String = String.Empty

        Me.lbnReset.Visible = True

        If rtb.Text.Trim.Length > 0 Then
            searchRequest = rtb.Text.Trim

            If IsNumeric(searchRequest.Substring(0, 1)) Then
                'on numeric we search the studentnumber the ssn and enrollmentid with an or condition
                nameexpression = "([StudentNumber] LIKE '" & searchRequest & "%' or [SSNSearch] LIKE '" & searchRequest & "%' or [EnrollmentId] LIKE '" & searchRequest & "%')"
            Else
                'on alpha we search the firstname and lastname with an or condition
                If searchRequest.IndexOf(" ") <> -1 Then
                    'we are assuming that a string with a space in the middle is a firstname/lastname request
                    nameexpression = "([FirstName] LIKE '" & searchRequest.Substring(0, searchRequest.IndexOf(" ")) & "%' and [LastName] LIKE '" & searchRequest.Substring(searchRequest.IndexOf(" ") + 1) & "%')"
                Else
                    'on alpha we search the firstname and lastname with an or condition
                    nameexpression = "([FirstName] LIKE '" & searchRequest & "%' or [LastName] LIKE '" & searchRequest & "%')"
                End If
            End If
        Else
            nameexpression = String.Empty
        End If

        If nameexpression <> String.Empty Then
            expression = nameexpression
        End If

        RadGridStudentSearch.MasterTableView.FilterExpression = expression
        Try
            RadGridStudentSearch.MasterTableView.Rebind()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ' lblErrMsg.Text = ex.Message
            Throw ex
        End Try

        'rtb.Text = String.Empty
        'rtb.EmptyMessage = "Search By Name, Student Number, or SSN"
    End Sub

    Protected Sub FilterDisplayMRU()

        Dim rtb As RadTextBox = Me.RadTextBox2
        Dim nameexpression As String
        Dim expression As String= String.Empty

        Me.lbnResetMRU.Visible = True

        If rtb.Text.Trim.Length > 0 Then
            searchRequest = rtb.Text.Trim

            If IsNumeric(searchRequest.Substring(0, 1)) Then
                'on numeric we search the studentnumber the ssn and enrollmentid with an or condition
                nameexpression = "([StudentNumber] LIKE '" & searchRequest & "%' or [SSNSearch] LIKE '" & searchRequest & "%' or [EnrollmentId] LIKE '" & searchRequest & "%')"
            Else
                'on alpha we search the firstname and lastname with an or condition
                If searchRequest.IndexOf(" ") <> -1 Then
                    'we are assuming that a string with a space in the middle is a firstname/lastname request
                    nameexpression = "([FirstName] LIKE '" & searchRequest.Substring(0, searchRequest.IndexOf(" ")) & "%' and [LastName] LIKE '" & searchRequest.Substring(searchRequest.IndexOf(" ") + 1) & "%')"
                Else
                    'on alpha we search the firstname and lastname with an or condition
                    nameexpression = "([FirstName] LIKE '" & searchRequest & "%' or [LastName] LIKE '" & searchRequest & "%')"
                End If
            End If
        Else
            nameexpression = String.Empty
        End If

        If nameexpression <> String.Empty Then
            expression = nameexpression
        End If

        RadGridStudentSearchMRU.MasterTableView.FilterExpression = expression
        RadGridStudentSearchMRU.MasterTableView.Rebind()

        'rtb.Text = String.Empty
        'rtb.EmptyMessage = "Search By Name, Student Number, or SSN"
    End Sub
    Public Function SetFilterMode() As Boolean Implements IFilterControl.SetFilterMode
        Dim RValue As Boolean = False
        Try
            Dim blnQuickFilterMode As Boolean = CType(Session("QuickFilterMode"), Boolean)

            If ItemDetail.SectionType = ParameterSectionInfo.SecType.Quick Then
                If RadGridSelectedStudents.Items.Count > 0 Then
                    blnQuickFilterMode = True
                    RValue = True
                End If
            Else
                If ItemDetail.SectionType = ParameterSectionInfo.SecType.Advanced AndAlso blnQuickFilterMode = True Then
                    RValue = False
                Else
                    RValue = True
                End If
            End If

            Session("QuickFilterMode") = blnQuickFilterMode
            Return RValue
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Sub LoadSavedReportSettings() Implements ISearchControl.LoadSavedReportSettings
        'Try
        '    If Not SavedSettings Is Nothing Then
        '        For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
        '            Dim ctrl As Control = Me.FindControl(Setting.ControlName)
        '            If Not ctrl Is Nothing Then
        '                If TypeOf ctrl Is RadGrid Then
        '                    Dim myradgrid As RadGrid = DirectCast(ctrl, RadGrid)
        '                    myradgrid.MasterTableView.Items.clear()
        '                    Dim StuEnrollIdCollection As New List(Of String)
        '                    For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
        '                        StuEnrollIdCollection.Add(ItemValue.KeyData)
        '                    Next
        '                    'get data to fill grid
        '                    CallForStudEnrollments(StuEnrollIdCollection)
        '                Else
        '                    Throw New Exception("Unknown Saved Control Settings")
        '                End If
        '            End If
        '        Next
        '    Else
        '        myradgrid.MasterTableView.Items.clear()
        '    End If
        '    Call Fill()
        '    ' Call RadListBoxCounts()
        'Catch ex As Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    Throw ex
        'End Try
    End Sub
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ISearchControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Try

            If RadGridSelectedStudents.Items.Count > 0 Then

                Dim SettingsCollection As New List(Of ControlSettingInfo)
                Dim ctrlSetting As New ControlSettingInfo
                Dim ctrlValues As New List(Of ControlValueInfo)

                Dim strDisplay As String

                UserSettings.ItemName = ItemDetail.ItemName
                UserSettings.ItemId = ItemDetail.ItemId
                UserSettings.DetailId = ItemDetail.DetailId
                UserSettings.FriendlyName = ItemDetail.CaptionOverride

                ctrlSetting.ControlName = RadGridSelectedStudents.ID

                For Each item As GridDataItem In RadGridSelectedStudents.Items
                    Dim objControlValue As New ControlValueInfo

                    Dim strItem1 As String = item("FirstName").Text
                    Dim strItem2 As String = item("LastName").Text

                    strDisplay = strItem1 & " " & strItem2

                    objControlValue.DisplayText = strDisplay
                    objControlValue.KeyData = item("StuEnrollId").Text
                    ctrlValues.Add(objControlValue)
                Next
                ctrlSetting.ControlValueCollection = ctrlValues
                SettingsCollection.Add(ctrlSetting)
                UserSettings.ControlSettingsCollection = SettingsCollection

                'Return UserSettings

            End If

            Return Nothing
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        Return UserSettings
    End Function

    Function GetReturnValues() As DetailDictionary Implements ISearchControl.GetReturnValues
        Return Nothing
    End Function

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            UserId = New Guid("A64D1E87-9673-4FA6-B90F-8F7163EFC490")
            If Page.IsPostBack = True Then
                PostbackCtrlName = Page.Request.Params.Get("__EVENTTARGET")
            Else
                SetProperties()
                Fill()
                PostbackCtrlName = String.Empty
                Session("Student_VB") = Nothing
                Session("StudentSelected_VB") = Nothing
                Session("MRUStudentsSearch_VB") = Nothing
            End If
            AddTBValueChangedScript()
            Dim x As String = ClientSearchFunction
            RadTextBox1.ClientEvents.OnKeyPress = ClientSearchFunction
            RadTextBox2.ClientEvents.OnKeyPress = ClientSearchFunction
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ' lblErrMsg.Text = ex.Message
            Throw ex
        End Try
    End Sub

    Protected Sub RadGridStudentSearch_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = RadGridStudentSearch.ClientID Then
                If (e.DestDataItem Is Nothing AndAlso StudentSelectedStore.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = RadGridSelectedStudents.ClientID) Then
                    Dim studentSearch As IList(Of StudentInfo)
                    Dim studentSelected As IList(Of StudentInfo)
                    studentSelected = StudentSelectedStore
                    studentSearch = StudentSearchStore
                    Dim destinationIndex As Int32 = -1
                    If (e.DestDataItem IsNot Nothing) Then
                        Dim StudentGuid As Guid = New Guid(e.DestDataItem.KeyValues.Substring(e.DestDataItem.KeyValues.IndexOf(":") + 2, 36).ToString())
                        Dim student As StudentInfo = GetStudent(studentSearch, StudentGuid)
                        If IsDBNull(student) = False Then
                            destinationIndex = studentSearch.IndexOf(student)
                        End If
                    End If

                    For Each draggedItem As GridDataItem In e.DraggedItems
                        Dim tmpStudent As StudentInfo = GetStudent(studentSearch, DirectCast(draggedItem.GetDataKeyValue("StudentID"), Guid))
                        If tmpStudent IsNot Nothing Then
                            If destinationIndex > -1 Then
                                If e.DropPosition = GridItemDropPosition.Below Then
                                    destinationIndex += 1
                                End If
                                studentSelected.Insert((studentSelected.Count + 1), tmpStudent)
                            Else
                                studentSelected.Add(tmpStudent)
                            End If
                            studentSearch.Remove(tmpStudent)
                        End If
                    Next

                    Dim expressions As New GridSortExpressionCollection

                    Dim expression1 As New GridSortExpression
                    expression1.FieldName = "LastName"
                    expression1.SetSortOrder("Ascending")

                    Dim expression2 As New GridSortExpression
                    expression2.FieldName = "FirstName"
                    expression2.SetSortOrder("Ascending")

                    StudentSelectedStore = studentSelected
                    StudentSearchStore = studentSearch

                    RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
                    RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
                    RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
                    RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

                    RadGridSelectedStudents.MasterTableView.Rebind()
                    RadGridStudentSearch.MasterTableView.Rebind()

                End If
            End If
        End If
    End Sub

    Protected Sub RadGridSelectedStudents_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = RadGridSelectedStudents.ClientID Then
                For Each draggedItem As GridDataItem In e.DraggedItems
                    Dim studentSearch As IList(Of StudentInfo)
                    Dim studentSelected As IList(Of StudentInfo)
                    studentSelected = StudentSelectedStore
                    If draggedItem("StudentType").Text = "S" Then
                        studentSearch = StudentSearchStore
                    Else
                        studentSearch = MRUStudentSearchStore
                    End If
                    If (e.DestDataItem Is Nothing AndAlso studentSearch.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso (e.DestDataItem.OwnerGridID = RadGridStudentSearch.ClientID Or e.DestDataItem.OwnerGridID = RadGridStudentSearchMRU.ClientID)) Then
                        Dim destinationIndex As Int32 = -1
                        If (e.DestDataItem IsNot Nothing) Then
                            Dim StudentGuid As Guid = New Guid(e.DestDataItem.KeyValues.Substring(e.DestDataItem.KeyValues.IndexOf(":") + 2, 36).ToString())
                            Dim student As StudentInfo = GetStudent(studentSelected, DirectCast(e.DestDataItem.GetDataKeyValue("StudentID"), Guid))
                            destinationIndex = studentSelected.IndexOf(student)
                            If IsDBNull(student) = False Then
                                destinationIndex = studentSelected.IndexOf(student)
                            End If
                        End If
                        Dim tmpStudent As StudentInfo = GetStudent(studentSelected, DirectCast(draggedItem.GetDataKeyValue("StudentID"), Guid))
                        If tmpStudent IsNot Nothing Then
                            If destinationIndex > -1 Then
                                If e.DropPosition = GridItemDropPosition.Below Then
                                    destinationIndex += 1
                                End If
                                studentSearch.Insert(destinationIndex, tmpStudent)
                            Else
                                studentSearch.Add(tmpStudent)
                            End If
                            studentSelected.Remove(tmpStudent)
                        End If

                        If draggedItem("StudentType").Text = "S" Then
                            StudentSearchStore = studentSearch
                        Else
                            MRUStudentSearchStore = studentSearch
                        End If
                        StudentSelectedStore = studentSelected
                    End If
                Next

                Dim expressions As New GridSortExpressionCollection

                Dim expression1 As New GridSortExpression
                expression1.FieldName = "LastName"
                expression1.SetSortOrder("Ascending")

                Dim expression2 As New GridSortExpression
                expression2.FieldName = "FirstName"
                expression2.SetSortOrder("Ascending")

                RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
                RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
                RadGridStudentSearch.MasterTableView.Rebind()

                RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
                RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
                RadGridStudentSearchMRU.MasterTableView.Rebind()

                RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
                RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)
                RadGridSelectedStudents.MasterTableView.Rebind()

            End If
        End If
    End Sub

    Protected Sub lbnReset_Click(ByVal sender As Object, ByVal e As EventArgs)

        Me.RadGridStudentSearch.MasterTableView.FilterExpression = String.Empty
        Me.RadGridStudentSearch.DataSource = StudentSearchStore()
        Me.RadGridStudentSearch.DataBind()
        Me.RadTextBox1.Text = String.Empty
        Me.RadTextBox1.EmptyMessage = "Enter Name, Student#, Enrollment#, or SSN"

    End Sub

    Protected Sub lbnResetMRU_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.RadGridStudentSearchMRU.MasterTableView.FilterExpression = String.Empty
        Me.RadGridStudentSearchMRU.DataSource = MRUStudentSearchStore()
        Me.RadGridStudentSearchMRU.DataBind()
        Me.RadTextBox2.Text = String.Empty
        Me.RadTextBox2.EmptyMessage = "Enter Name, Student#, Enrollment#, or SSN"
    End Sub

    Protected Sub lbnResetSelected_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim studentSearch As IList(Of StudentInfo)
        Dim studentSearchMRU As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)

        studentSearch = StudentSearchStore
        studentSearchMRU = MRUStudentSearchStore
        studentSelected = StudentSelectedStore

        Dim count As Integer = RadGridSelectedStudents.MasterTableView.PageCount
        Dim flag As Integer = 0
        For i As Integer = 0 To count - 1
            RadGridSelectedStudents.CurrentPageIndex = i
            RadGridSelectedStudents.Rebind()
            For Each gridItem As GridDataItem In RadGridSelectedStudents.Items
                Dim tmpStudent As StudentInfo = GetStudent(studentSelected, DirectCast(gridItem.GetDataKeyValue("StudentID"), Guid))
                If tmpStudent IsNot Nothing Then
                    If tmpStudent.StudentType = "S" Then
                        studentSearch.Add(tmpStudent)
                        studentSelected.Remove(tmpStudent)
                    Else
                        studentSearchMRU.Add(tmpStudent)
                        studentSelected.Remove(tmpStudent)
                    End If
                End If
            Next
        Next
        RadGridSelectedStudents.CurrentPageIndex = 0
        RadGridSelectedStudents.Rebind()

        StudentSearchStore = studentSearch
        MRUStudentSearchStore = studentSearchMRU
        StudentSelectedStore = studentSelected

        RadGridStudentSearch.Rebind()
        RadGridStudentSearchMRU.Rebind()
    End Sub

    Protected Sub RadGridStudentSearchMRU_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = RadGridStudentSearchMRU.ClientID Then
                If (e.DestDataItem Is Nothing AndAlso StudentSelectedStore.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = RadGridSelectedStudents.ClientID) Then
                    Dim studentSearchMRU As IList(Of StudentInfo)
                    Dim studentSelected As IList(Of StudentInfo)
                    studentSelected = StudentSelectedStore
                    studentSearchMRU = MRUStudentSearchStore
                    Dim destinationIndex As Int32 = -1
                    If (e.DestDataItem IsNot Nothing) Then
                        Dim StudentGuid As Guid = New Guid(e.DestDataItem.KeyValues.Substring(e.DestDataItem.KeyValues.IndexOf(":") + 2, 36).ToString())
                        Dim student As StudentInfo = GetStudent(studentSearchMRU, StudentGuid)
                        'Dim student As StudentInfo = GetStudent(studentSelected, DirectCast(e.DestDataItem.GetDataKeyValue("StudentId"), Guid))
                        destinationIndex = studentSearchMRU.IndexOf(student)
                        'destinationIndex = IIf(student IsNot Nothing, studentSearch.IndexOf(student), -1)
                        If IsDBNull(student) = False Then
                            destinationIndex = studentSearchMRU.IndexOf(student)
                        End If
                    End If

                    For Each draggedItem As GridDataItem In e.DraggedItems
                        Dim tmpStudent As StudentInfo = GetStudent(studentSearchMRU, DirectCast(draggedItem.GetDataKeyValue("StudentID"), Guid))
                        If tmpStudent IsNot Nothing Then
                            If destinationIndex > -1 Then
                                If e.DropPosition = GridItemDropPosition.Below Then
                                    destinationIndex += 1
                                End If
                                studentSelected.Insert((studentSelected.Count + 1), tmpStudent)
                            Else
                                studentSelected.Add(tmpStudent)
                            End If
                            studentSearchMRU.Remove(tmpStudent)
                        End If
                    Next

                    Dim expressions As New GridSortExpressionCollection

                    Dim expression1 As New GridSortExpression
                    expression1.FieldName = "LastName"
                    expression1.SetSortOrder("Ascending")

                    Dim expression2 As New GridSortExpression
                    expression2.FieldName = "FirstName"
                    expression2.SetSortOrder("Ascending")

                    StudentSelectedStore = studentSelected
                    MRUStudentSearchStore = studentSearchMRU

                    RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
                    RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
                    RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
                    RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

                    RadGridSelectedStudents.MasterTableView.Rebind()
                    RadGridStudentSearchMRU.MasterTableView.Rebind()

                End If
            End If
        End If
    End Sub

    Protected Sub RadGridSelectedStudents_ItemEvent(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGridSelectedStudents.ItemEvent
        If RadGridSelectedStudents.Items.Count > 0 Then
            lbnResetSelected.Visible = True
        Else
            lbnResetSelected.Visible = False
        End If
    End Sub

    Protected Sub btnMoveSelectedDown_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim studentSearch As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)
        studentSelected = StudentSelectedStore
        studentSearch = StudentSearchStore

        For Each dataItem As GridDataItem In RadGridStudentSearch.Items
            If dataItem.Selected Then

                Dim tmpStudent As StudentInfo = GetStudent(studentSearch, New Guid(dataItem("StudentId").Text.ToString()))
                studentSelected.Add(tmpStudent)
                studentSearch.Remove(tmpStudent)

            End If
        Next

        Me.RadGridStudentSearch.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StudentSelectedStore = studentSelected
        StudentSearchStore = studentSearch

        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStudents.MasterTableView.Rebind()
        RadGridStudentSearch.MasterTableView.Rebind()

        Me.RadGridStudentSearch.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = False
    End Sub

    Protected Sub btnMoveSelectedDownMRU_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim studentSearchMRU As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)
        studentSelected = StudentSelectedStore
        studentSearchMRU = MRUStudentSearchStore

        For Each dataItem As GridDataItem In RadGridStudentSearchMRU.Items
            If dataItem.Selected Then

                Dim tmpStudent As StudentInfo = GetStudent(studentSearchMRU, New Guid(dataItem("StudentId").Text.ToString()))
                studentSelected.Add(tmpStudent)
                studentSearchMRU.Remove(tmpStudent)

            End If
        Next

        Me.RadGridStudentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StudentSelectedStore = studentSelected
        MRUStudentSearchStore = studentSearchMRU

        RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStudents.MasterTableView.Rebind()
        RadGridStudentSearchMRU.MasterTableView.Rebind()

        Me.RadGridStudentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = False
    End Sub

    Protected Sub btnMoveSelectedUp_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim studentSearch As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)
        studentSelected = StudentSelectedStore
        studentSearch = StudentSearchStore

        For Each dataItem As GridDataItem In RadGridSelectedStudents.Items
            If dataItem.Selected Then

                Dim tmpStudent As StudentInfo = GetStudent(studentSelected, New Guid(dataItem("StudentId").Text.ToString()))
                studentSelected.Remove(tmpStudent)
                studentSearch.Add(tmpStudent)

            End If
        Next

        Me.RadGridStudentSearch.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StudentSelectedStore = studentSelected
        StudentSearchStore = studentSearch

        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStudents.MasterTableView.Rebind()
        RadGridStudentSearch.MasterTableView.Rebind()

        Me.RadGridStudentSearch.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = False

    End Sub

    Protected Sub btnMoveSelectedUpMRU_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim studentSearchMRU As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)
        studentSelected = StudentSelectedStore
        studentSearchMRU = MRUStudentSearchStore

        For Each dataItem As GridDataItem In RadGridSelectedStudents.Items
            If dataItem.Selected Then

                Dim tmpStudent As StudentInfo = GetStudent(studentSelected, New Guid(dataItem("StudentId").Text.ToString()))
                studentSelected.Remove(tmpStudent)
                studentSearchMRU.Add(tmpStudent)

            End If
        Next

        Me.RadGridStudentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StudentSelectedStore = studentSelected
        MRUStudentSearchStore = studentSearchMRU

        RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStudents.MasterTableView.Rebind()
        RadGridStudentSearchMRU.MasterTableView.Rebind()

        Me.RadGridStudentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = False

    End Sub

    Protected Sub btnMoveAllDown_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim studentSearch As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)
        studentSelected = StudentSelectedStore
        studentSearch = StudentSearchStore


        Dim count As Integer = RadGridStudentSearch.PageCount
        Dim flag As Integer = 0
        For i As Integer = 0 To count - 1
            RadGridStudentSearch.CurrentPageIndex = i
            RadGridStudentSearch.Rebind()
            For Each GridDataItem As GridDataItem In RadGridStudentSearch.Items
                Dim tmpStudent As StudentInfo = GetStudent(studentSearch, New Guid(GridDataItem("StudentId").Text.ToString()))
                studentSelected.Add(tmpStudent)
                studentSearch.Remove(tmpStudent)
            Next
        Next

        Me.RadGridStudentSearch.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StudentSelectedStore = studentSelected
        StudentSearchStore = studentSearch

        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStudents.MasterTableView.Rebind()
        RadGridStudentSearch.MasterTableView.Rebind()

        Me.RadGridStudentSearch.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = False


    End Sub

    Protected Sub btnMoveAllDownMRU_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim studentSearchMRU As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)
        studentSelected = StudentSelectedStore
        studentSearchMRU = MRUStudentSearchStore

        Dim count As Integer = RadGridStudentSearchMRU.PageCount
        Dim flag As Integer = 0
        For i As Integer = 0 To count - 1
            RadGridStudentSearchMRU.CurrentPageIndex = i
            RadGridStudentSearchMRU.Rebind()
            For Each GridDataItem As GridDataItem In RadGridStudentSearchMRU.Items
                Dim tmpStudent As StudentInfo = GetStudent(studentSearchMRU, New Guid(GridDataItem("StudentId").Text.ToString()))
                studentSelected.Add(tmpStudent)
                studentSearchMRU.Remove(tmpStudent)
            Next
        Next

        Me.RadGridStudentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StudentSelectedStore = studentSelected
        MRUStudentSearchStore = studentSearchMRU

        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStudents.MasterTableView.Rebind()
        RadGridStudentSearchMRU.MasterTableView.Rebind()

        Me.RadGridStudentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = False


    End Sub

    Protected Sub btnMoveAllUp_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim studentSearch As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)
        studentSelected = StudentSelectedStore
        studentSearch = StudentSearchStore


        Dim count As Integer = RadGridSelectedStudents.PageCount
        Dim flag As Integer = 0
        For i As Integer = 0 To count - 1
            RadGridSelectedStudents.CurrentPageIndex = i
            RadGridSelectedStudents.Rebind()
            For Each GridDataItem As GridDataItem In RadGridSelectedStudents.Items
                Dim tmpStudent As StudentInfo = GetStudent(studentSelected, New Guid(GridDataItem("StudentId").Text.ToString()))
                studentSelected.Remove(tmpStudent)
                studentSearch.Add(tmpStudent)
            Next
        Next

        Me.RadGridStudentSearch.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StudentSelectedStore = studentSelected
        StudentSearchStore = studentSearch

        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStudentSearch.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStudents.MasterTableView.Rebind()
        RadGridStudentSearch.MasterTableView.Rebind()

        Me.RadGridStudentSearch.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = False

    End Sub

    Protected Sub btnMoveAllUpMRU_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim studentSearchMRU As IList(Of StudentInfo)
        Dim studentSelected As IList(Of StudentInfo)
        studentSelected = StudentSelectedStore
        studentSearchMRU = MRUStudentSearchStore


        Dim count As Integer = RadGridSelectedStudents.PageCount
        Dim flag As Integer = 0
        For i As Integer = 0 To count - 1
            RadGridSelectedStudents.CurrentPageIndex = i
            RadGridSelectedStudents.Rebind()
            For Each GridDataItem As GridDataItem In RadGridSelectedStudents.Items
                Dim tmpStudent As StudentInfo = GetStudent(studentSelected, New Guid(GridDataItem("StudentId").Text.ToString()))
                studentSelected.Remove(tmpStudent)
                studentSearchMRU.Add(tmpStudent)
            Next
        Next

        Me.RadGridStudentSearchMRU.MasterTableView.AllowMultiColumnSorting = True
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = True

        Dim expressions As New GridSortExpressionCollection

        Dim expression1 As New GridSortExpression
        expression1.FieldName = "LastName"
        expression1.SetSortOrder("Ascending")

        Dim expression2 As New GridSortExpression
        expression2.FieldName = "FirstName"
        expression2.SetSortOrder("Ascending")

        StudentSelectedStore = studentSelected
        MRUStudentSearchStore = studentSearchMRU

        RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridStudentSearchMRU.MasterTableView.SortExpressions.AddSortExpression(expression2)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression1)
        RadGridSelectedStudents.MasterTableView.SortExpressions.AddSortExpression(expression2)

        RadGridSelectedStudents.MasterTableView.Rebind()
        RadGridStudentSearchMRU.MasterTableView.Rebind()

        Me.RadGridStudentSearchMRU.MasterTableView.AllowMultiColumnSorting = False
        Me.RadGridSelectedStudents.MasterTableView.AllowMultiColumnSorting = False

    End Sub

#End Region

End Class

