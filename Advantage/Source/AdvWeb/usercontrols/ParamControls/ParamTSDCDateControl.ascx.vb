﻿Option Strict On
Imports Telerik.Web.UI
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Parameters.Info
Imports FAME.Parameters.Collections

Partial Class ParamTSDCDateControl
    Inherits UserControl
    Implements IDateControl
#Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _Caption As String
    Private _SqlConn As String
    Private _Mode As IDateControl.ModeType
    Private _ShowDefaultDate As IDateControl.DefaultDateType
    Private _ErrorMsg As List(Of String)
    Private _SavedSettings As ParamItemUserSettingsInfo
    Private _SendData As Boolean

    Public Property ItemDetail() As ParameterDetailItemInfo Implements IDateControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = Value
        End Set
    End Property
    Public Property Caption() As String Implements IDateControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements IDateControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property Mode() As IDateControl.ModeType Implements IDateControl.Mode
        Get
            Return _Mode
        End Get
        Set(ByVal Value As IDateControl.ModeType)
            _Mode = Value
        End Set
    End Property
    Public Property ShowDefaultDate() As IDateControl.DefaultDateType Implements IDateControl.ShowDefaultDate
        Get
            Return _ShowDefaultDate
        End Get
        Set(ByVal Value As IDateControl.DefaultDateType)
            _ShowDefaultDate = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements IDateControl.SavedSettings
        Get
            Return _SavedSettings
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSettings = value
        End Set
    End Property
    Public Property SendData() As Boolean Implements IDateControl.SendData
        Get
            Return _SendData
        End Get
        Set(ByVal Value As Boolean)
            _SendData = Value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        GetLayout()
        SetProperties()
        GetDefaultDate()
        GetMasterCollection()
    End Sub
#End Region
#Region "Methods"
    Public Sub ClearControls() Implements IDateControl.ClearControls
        RadComboBox1.Items.Clear()
        RadComboBox2.Items.Clear()
        RadDatePicker1.SelectedDate = Nothing
        RadDatePicker2.SelectedDate = Nothing
    End Sub
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub GetDefaultDate()
        Select Case CInt(ShowDefaultDate)
            Case 0
                RadDatePicker1.SelectedDate = Nothing
                RadDatePicker2.SelectedDate = Nothing
            Case 1
                RadDatePicker1.SelectedDate = DateTime.Now
                RadDatePicker2.SelectedDate = Nothing
            Case 2
                RadDatePicker1.SelectedDate = Nothing
                RadDatePicker2.SelectedDate = DateTime.Now
            Case 3
                RadDatePicker1.SelectedDate = DateTime.Now
                RadDatePicker2.SelectedDate = DateTime.Now
            Case Else
                RadDatePicker1.SelectedDate = Nothing
                RadDatePicker2.SelectedDate = Nothing
                Throw New Exception("Unknown Default Date Settings")
        End Select
    End Sub
    Private Sub GetLayout()
        Select Case CInt(Mode)
            Case 0
                'RadComboBox1.Visible = True
                RadComboBox2.Visible = False
                RadDatePicker2.Visible = False
                RadComboBox1.SelectedIndex = 0
                RadComboBox2.SelectedIndex = 0
                RadComboBox1.Items.Clear()
                RadComboBox1.Items.Add(New RadComboBoxItem("Is Equal To", "0"))
                'RadComboBox1.Items.Add(New RadComboBoxItem("IsNotEqualTo", "1"))
                RadComboBox1.Items.Add(New RadComboBoxItem("Greater Than Or Equal To", "2"))
                RadComboBox1.Items.Add(New RadComboBoxItem("Less Than Or Equal To", "3"))
                RadComboBox1.Items.Add(New RadComboBoxItem("Greater Than", "4"))
                RadComboBox1.Items.Add(New RadComboBoxItem("Less Than", "5"))
                'RadComboBox1.Items.Add(New RadComboBoxItem("InList", "6"))
                'RadComboBox1.Items.Add(New RadComboBoxItem("IsNull", "7"))
                'RadComboBox1.Items.Add(New RadComboBoxItem("IsNotNull", "8"))
                ' RadComboBox1.Items.Add(New RadComboBoxItem("IsEmpty", "9"))
                'RadComboBox1.Items.Add(New RadComboBoxItem("IsNotEmpty", "10"))
                'RadComboBox1.Items.Add(New RadComboBoxItem("Contains", "11"))
                'RadComboBox1.Items.Add(New RadComboBoxItem("StartsWith", "12"))
                ' RadComboBox1.Items.Add(New RadComboBoxItem("EndsWith", "13"))
            Case 1
                RadComboBox1.Items.Clear()
                RadComboBox1.Items.Add(New RadComboBoxItem("Greater Than Or Equal To", "2"))
                RadComboBox1.Items.Add(New RadComboBoxItem("Greater Than", "4"))
                RadComboBox1.SelectedValue = "2"

                RadComboBox2.Visible = True
                RadComboBox2.SelectedValue = "3"

                RadDatePicker2.Visible = True
            Case Else
                RadComboBox1.SelectedValue = "3"
                RadComboBox1.SelectedIndex = 0
                RadComboBox2.SelectedIndex = 1
                RadDatePicker2.Visible = False
                Throw New Exception("Unknown Search Type")
        End Select
    End Sub
    Public Function GetReturnValues() As DetailDictionary Implements IDateControl.GetReturnValues
        Dim objDetails As New DetailDictionary 
        Dim returnValues As ParamValueDictionary
        Try
            If SetFilterMode() = True Then
                returnValues = CreateValues()
                If returnValues.Count > 0 Then
                    objDetails.Add(ItemDetail.ItemName, returnValues)
                End If
            End If 
            Return objDetails
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Private Sub GetMasterCollection()
        Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName), MasterDictionary)
        Dim objDetails As New DetailDictionary
        Dim returnValues As ParamValueDictionary
        Try
            returnValues = CreateValues()
            If Not objMaster Is Nothing Then
                If objMaster.Contains(ItemDetail.ItemName) Then
                    objDetails = DirectCast(objMaster.Item(ItemDetail.ItemName), DetailDictionary)
                End If
                If Not objDetails Is Nothing Then
                    If objDetails.Contains(ItemDetail.ItemName) Then
                        'take out old values if they exist
                        objDetails.Remove(ItemDetail.ItemName)
                    End If
                    If returnValues.Count > 0 Then
                        'add new values if there are any
                        objDetails.Add(ItemDetail.ItemName, returnValues)
                        objMaster.Remove(ItemDetail.ItemName)
                        objMaster.Add(ItemDetail.ItemName, objDetails)
                    End If

                    'store master collection in session
                    Session(ItemDetail.SetName) = objMaster
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub


    Public Function SetFilterMode() As Boolean Implements IFilterControl.SetFilterMode
        Dim rValue As Boolean
        Try
            Dim blnQuickFilterMode As Boolean = CType(Session("QuickFilterMode"), Boolean)

            If ItemDetail.SectionType = ParameterSectionInfo.SecType.Quick Then
                If Not RadDatePicker1.SelectedDate Is Nothing Or Not RadDatePicker2.SelectedDate Is Nothing Then
                    blnQuickFilterMode = True
                    rValue = True
                Else
                    blnQuickFilterMode = False
                    rValue = False
                End If
            Else
                If ItemDetail.SectionType = ParameterSectionInfo.SecType.Advanced AndAlso blnQuickFilterMode = True Then
                    rValue = False
                Else
                    rValue = True
                End If
            End If

            Session("QuickFilterMode") = blnQuickFilterMode
            Return rValue
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Sub LoadSavedReportSettings() Implements IDateControl.LoadSavedReportSettings
        Try
            RadDatePicker1.SelectedDate = Nothing
            RadDatePicker2.SelectedDate = Nothing
            RadComboBox1.SelectedIndex = 0
            RadComboBox2.SelectedIndex = 0
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadDatePicker Then
                            Dim RadPicker As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If IsDate(ItemValue.KeyData) = True Then
                                    RadPicker.SelectedDate = CDate(ItemValue.KeyData)
                                End If
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim RadBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadBox.SelectedValue = ItemValue.KeyData
                            Next
                        Else
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadDatePicker1.SelectedDate = Nothing
                RadDatePicker2.SelectedDate = Nothing
                RadComboBox1.SelectedIndex = 0
                RadComboBox2.SelectedIndex = 0
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Function HasTermValues() As Boolean
        Try
            Dim objMaster As MasterDictionary = DirectCast(Session(ItemDetail.SetName), MasterDictionary)
            If objMaster.Contains("Term") Then
                Dim objDetailDictionary As DetailDictionary = objMaster.Item("Term")
                If objDetailDictionary.Contains("CurrentlySelected") Then
                    Dim currentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objDetailDictionary.Item("CurrentlySelected"), ParamValueDictionary)
                    For Each di As DictionaryEntry In currentlySelectedValueDictionary
                        'Dim fModDictionary As ModDictionary = DirectCast(currentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                'For Each di2 As DictionaryEntry In FModDictionary
                                '    'Dim SelectedTermList As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))

                                '    'For Each selected As String In mylist
                                '    '    Dim temp As String = selected
                                '    '    ' Predicate = Predicate.And(Function(pv As arTerm) Not pv.TermId.Equals(temp))
                                '    'Next
                                'Next
                        End Select
                                Next

                End If
            End If
            Return True
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements IDateControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)

        Try
            If SetFilterMode() = True Then
                userSettings.ItemName = ItemDetail.ItemName
                userSettings.ItemId = ItemDetail.ItemId
                userSettings.DetailId = ItemDetail.DetailId
                userSettings.FriendlyName = ItemDetail.CaptionOverride

                'If Mode = IDateControl.ModeType.Range Then
                Dim ctrlSetting1 As New ControlSettingInfo
                Dim ctrlSetting2 As New ControlSettingInfo
                Dim ctrlSetting3 As New ControlSettingInfo
                Dim ctrlSetting4 As New ControlSettingInfo

                ctrlSetting1.ControlName = RadComboBox1.ID
                ctrlSetting2.ControlName = RadDatePicker1.ID
                ctrlSetting3.ControlName = RadComboBox2.ID
                ctrlSetting4.ControlName = RadDatePicker2.ID

                Dim ctrlValues1 As New List(Of ControlValueInfo)
                Dim ctrlValues2 As New List(Of ControlValueInfo)
                Dim ctrlValues3 As New List(Of ControlValueInfo)
                Dim ctrlValues4 As New List(Of ControlValueInfo)

                Dim comboItem1 As RadComboBoxItem = RadComboBox1.SelectedItem
                Dim comboItem3 As RadComboBoxItem = RadComboBox2.SelectedItem

                Dim pickerItem2 As RadDateInput = RadDatePicker1.DateInput
                Dim pickerItem4 As RadDateInput = RadDatePicker2.DateInput

                Dim objControlValue1 As New ControlValueInfo
                objControlValue1.DisplayText = comboItem1.Text
                objControlValue1.KeyData = comboItem1.Value

                Dim objControlValue3 As New ControlValueInfo
                objControlValue3.DisplayText = comboItem3.Text
                objControlValue3.KeyData = comboItem3.Value

                Dim objControlValue2 As New ControlValueInfo
                objControlValue2.DisplayText = pickerItem2.SelectedDate.ToString
                objControlValue2.KeyData = pickerItem2.SelectedDate.ToString

                Dim objControlValue4 As New ControlValueInfo
                objControlValue4.DisplayText = pickerItem4.SelectedDate.ToString
                objControlValue4.KeyData = pickerItem4.SelectedDate.ToString

                ctrlValues1.Add(objControlValue1)
                ctrlValues3.Add(objControlValue3)
                ctrlValues2.Add(objControlValue2)
                ctrlValues4.Add(objControlValue4)



                ctrlSetting1.ControlValueCollection = ctrlValues1
                settingsCollection.Add(ctrlSetting1)

                ctrlSetting2.ControlValueCollection = ctrlValues2
                settingsCollection.Add(ctrlSetting2)

                ctrlSetting3.ControlValueCollection = ctrlValues3
                settingsCollection.Add(ctrlSetting3)

                ctrlSetting4.ControlValueCollection = ctrlValues4
                settingsCollection.Add(ctrlSetting4)



                userSettings.ControlSettingsCollection = settingsCollection

                Return userSettings
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw 
        End Try
    End Function
    Private Function CreateValues() As ParamValueDictionary
        Dim returnValues As  ParamValueDictionary
        Try
            returnValues = FillValueDictionary()
            returnValues.Name = ItemDetail.ReturnValueName
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw 
        End Try
        Return returnValues
    End Function
    Private Function FillValueDictionary() As ParamValueDictionary
        Try
            Dim valueDictionary As New ParamValueDictionary


            Select Case Mode
                Case IDateControl.ModeType.Single
                    If IsDate(RadDatePicker1.SelectedDate) = True Then
                        Dim pValues As New ParamValueList(Of Date)
                        pValues.DataType = ValueList.ValueType.Date
                        Dim modifierDictionary As New ModDictionary
                        Dim selectDate As Date = CDate(RadDatePicker1.SelectedDate)
                        Dim mod1 As ModDictionary.Modifier = CType(CInt(RadComboBox1.SelectedValue), ModDictionary.Modifier)
                        pValues.Add(selectDate)
                        modifierDictionary.Add(ItemDetail.ItemName, pValues)
                        valueDictionary.Add(mod1, modifierDictionary)
                    End If
                Case IDateControl.ModeType.Range
                    If IsDate(RadDatePicker1.SelectedDate) = True Then
                        Dim pValues As New ParamValueList(Of Date)
                        pValues.DataType = ValueList.ValueType.Date
                        Dim modifierDictionary As New ModDictionary
                        Dim selectDate As Date = CDate(RadDatePicker1.SelectedDate)
                        Dim mod1 As ModDictionary.Modifier = CType(CInt(RadComboBox1.SelectedValue), ModDictionary.Modifier)
                        pValues.Add(selectDate)
                        modifierDictionary.Add(ItemDetail.ItemName, pValues)
                        valueDictionary.Add(mod1, modifierDictionary)
                    End If
                    If IsDate(RadDatePicker2.SelectedDate) = True Then
                        Dim pValues As New ParamValueList(Of Date)
                        pValues.DataType = ValueList.ValueType.Date
                        Dim modifierDictionary As New ModDictionary
                        Dim selectDate As Date = CDate(RadDatePicker2.SelectedDate)
                        Dim mod2 As ModDictionary.Modifier = CType(CInt(RadComboBox2.SelectedValue), ModDictionary.Modifier)
                        pValues.Add(selectDate)
                        modifierDictionary.Add(ItemDetail.ItemName, pValues)
                        valueDictionary.Add(mod2, modifierDictionary)
                    End If
                Case Else
                    Throw New Exception("Unknown Mode")
            End Select
            Return valueDictionary
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw 
        End Try
    End Function
    Protected Sub ValidateDateRange(ByVal sender As Object, ByVal args As ServerValidateEventArgs)
        If Mode = IDateControl.ModeType.Range Then
            Dim hasValue1 As Boolean = False
            Dim hasValue2 As Boolean = False
            'Check for a date value in both controls
            If IsDate(RadDatePicker1.SelectedDate) = True Then
                hasValue1 = True
            End If
            If IsDate(RadDatePicker2.SelectedDate) = True Then
                hasValue2 = True
            End If
            'if one control has a date value then the other must have a date value
            If hasValue1 = True Or hasValue2 = True Then
                If IsDate(RadDatePicker1.SelectedDate) = True AndAlso IsDate(RadDatePicker2.SelectedDate) = True Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
            End If
        End If
    End Sub
#End Region
End Class
