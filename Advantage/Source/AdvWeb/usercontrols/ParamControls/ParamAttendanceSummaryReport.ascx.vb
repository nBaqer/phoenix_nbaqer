﻿
Imports System.Reflection
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Domain.Lead
Imports FAME.Parameters.Info
Imports FAME.Parameters.Interfaces
Imports Telerik.Web.UI

Partial Class ParamAttendanceSummaryReport
    Inherits System.Web.UI.UserControl
    Implements ICustomControl
    Protected MyAdvAppSettings As AdvAppSettings
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    Private _ItemDetail As ParameterDetailItemInfo
    Protected CampusId As String
    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property

    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride

            'Program Versions
            Dim ctrlSettingProgramVersions As New ControlSettingInfo
            Dim ctrlValuesProgramVersions As New List(Of ControlValueInfo)
            ctrlSettingProgramVersions.ControlName = RadListBoxProgramVersions2.ID
            For Each item As RadListBoxItem In RadListBoxProgramVersions2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgramVersions.Add(objControlValueInfo3)
            Next
            ctrlSettingProgramVersions.ControlValueCollection = ctrlValuesProgramVersions
            settingsCollection.Add(ctrlSettingProgramVersions)


            'StudentGroups
            Dim ctrlSettingStudentGroups As New ControlSettingInfo
            Dim ctrlValuesStudentGroups As New List(Of ControlValueInfo)
            ctrlSettingStudentGroups.ControlName = RadListBoxStudentGroupSelector2.ID
            For Each item As RadListBoxItem In RadListBoxStudentGroupSelector2.Items
                Dim objControlValueInfo4 As New ControlValueInfo
                objControlValueInfo4.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo4.KeyData = item.Value.Replace("'", "''")
                ctrlValuesStudentGroups.Add(objControlValueInfo4)
            Next
            ctrlSettingStudentGroups.ControlValueCollection = ctrlValuesStudentGroups
            settingsCollection.Add(ctrlSettingStudentGroups)


            userSettings.ControlSettingsCollection = settingsCollection
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        Return userSettings


    End Function

    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim displayTree As New RadTreeView
        displayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            'Program
            If RadListBoxProgram2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim headerNode2 As New RadTreeNode
                headerNode2.Text = "Program"
                headerNode2.Value = "Program"
                headerNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim programNode As New RadTreeNode
                    programNode.Text = item.Text
                    headerNode2.Nodes.Add(programNode)
                Next
                displayTree.Nodes.Add(headerNode2)
            End If

            'Program
            If RadListBoxProgramVersions2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim headerNode3 As New RadTreeNode
                headerNode3.Text = "Program Version"
                headerNode3.Value = "ProgramVersion"
                headerNode3.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgramVersions2.Items
                    Dim programVersionNode As New RadTreeNode
                    programVersionNode.Text = item.Text
                    headerNode3.Nodes.Add(programVersionNode)
                Next
                displayTree.Nodes.Add(headerNode3)
            End If

            'Program
            If RadListBoxStudentGroupSelector2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim headerNode4 As New RadTreeNode
                headerNode4.Text = "Student Group"
                headerNode4.Value = "StudentGroup"
                headerNode4.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxStudentGroupSelector2.Items
                    Dim studentGroupNode As New RadTreeNode
                    studentGroupNode.Text = item.Text
                    headerNode4.Nodes.Add(studentGroupNode)
                Next
                displayTree.Nodes.Add(headerNode4)
            End If


            Return displayTree
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Public Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim radioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                radioButList.SelectedValue = itemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim comboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                If Not itemValue.KeyData = "1900" Then
                                    comboBox.SelectedValue = itemValue.KeyData
                                End If
                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim lstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            lstBox.Items.Clear()
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = itemValue.DisplayText
                                newItem.Value = itemValue.KeyData
                                lstBox.SelectedValue = itemValue.KeyData
                                lstBox.Items.Add(newItem)
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else

            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        CampusId = AdvantageSession.UserState.CampusId.ToString
        SetProperties()
        If Not Page.IsPostBack Then
            FillProgramsControl()
            FillProgramVersionsControl()
            FillStudentGroupsControll()
        End If
    End Sub

   

#Region "Currently Selected Funcions"
    Private Function GetCurrentlySelectedPrograms() As List(Of String)
        Dim selectedPrograms As New List(Of String)
        Try
            If RadListBoxProgram2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim selected As String = item.Value
                    selectedPrograms.Add(selected)
                Next
                Return selectedPrograms
            Else
                Return selectedPrograms
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Private Function GetCurrentlySelectedProgramVersion() As List(Of String)
        Dim selectedProgramVersions As New List(Of String)
        Try
            If RadListBoxProgramVersions2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgramVersions2.Items
                    Dim selected As String = item.Value
                    selectedProgramVersions.Add(selected)
                Next
                Return selectedProgramVersions
            Else
                Return selectedProgramVersions
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Private Function GetCurrentlySelectedStudentGroups() As List(Of String)
        Dim selectedStudentGroups As New List(Of String)
        Try
            If RadListBoxStudentGroupSelector2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxStudentGroupSelector2.Items
                    Dim selected As String = item.Value
                    selectedStudentGroups.Add(selected)
                Next
                Return selectedStudentGroups
            Else
                Return selectedStudentGroups
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function


#End Region

#Region "Data Access Functions"
    Private Function GetProgramsByCampusId(selectedCampus As String, ByVal filteredBySelected As Boolean) As List(Of arProgram)
        Dim DA As New ProgramDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

        Dim campusId As Guid = Guid.Parse(selectedCampus)
        Dim result As New List(Of arProgram)
        Try
            Dim selectedPrograms As List(Of String) = GetCurrentlySelectedPrograms()
            If selectedPrograms Is Nothing Or filteredBySelected = False Then
                result = DA.GetProgramsByCampusId(campusId, GetStatusFilters("chkInActivePrograms"))
            Else
                result = DA.GetProgramsByCampusId(campusId, GetStatusFilters("chkInActivePrograms"), selectedPrograms)
            End If
            Return result
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function


    Private Function GetProgramsVersionsByProgramId(selectedCampus As String, ByVal filteredBySelected As Boolean) As List(Of arPrgVersion)
        Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim campusId As Guid = Guid.Parse(selectedCampus)
        Dim result As New List(Of arPrgVersion)
        Try
            Dim selectedProgramVersion As List(Of String) = GetCurrentlySelectedProgramVersion()
            If selectedProgramVersion Is Nothing Or filteredBySelected = False Then
                result = DA.GetProgramVersionsByCampusId(campusId, GetStatusFilters("chkInActiveProgramVersions"))
            Else
                result = DA.GetProgramVersionsByCampusId(campusId, GetStatusFilters("chkInActiveProgramVersions"), selectedProgramVersion)
            End If
            Return result
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function


    
    Private Function GetStudentGroups(selectedCampus As String, filteredBySelected As Boolean) As Object
        Dim DA As New LeadGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim campusId As Guid = Guid.Parse(selectedCampus)
        Dim result As New List(Of adLeadGroup)
        Try
            Dim selectedStudentGroups As List(Of String) = GetCurrentlySelectedStudentGroups()
            If selectedStudentGroups Is Nothing Or filteredBySelected = False Then
                result = DA.GetStudentGroupsByCampusId(campusId, GetStatusFilters("chkInActiveStudentGroupSelector"))
            Else
                result = DA.GetStudentGroupsByCampusId(campusId, GetStatusFilters("chkInActiveStudentGroupSelector"), selectedStudentGroups)
            End If
            Return result
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

#End Region

#Region "Others"

    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim statusValues As New List(Of String)
        Try
            Dim cKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If cKbox.Checked = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        Return statusValues
    End Function

    Private Sub SetProperties()
        Try
            'RadDateReportDate.MaxDate = DateTime.Now
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
#End Region


#Region "Fill Data"
    Public Sub FillProgramsControl()
        Try
            RadListBoxProgram.DataTextField = "ProgDescrip"
            RadListBoxProgram.DataValueField = "ProgId"
            Dim selectedCampus = CampusId

            If selectedCampus IsNot Nothing Then
                RadListBoxProgram.DataSource = GetProgramsByCampusId(selectedCampus, True)
            End If
            RadListBoxProgram.DataBind()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Public Sub FillProgramVersionsControl()
        Try
            RadListBoxProgramVersions.DataTextField = "PrgVerDescrip"
            RadListBoxProgramVersions.DataValueField = "PrgVerId"
            Dim selectedCampus = CampusId

            If selectedCampus IsNot Nothing Then
                RadListBoxProgramVersions.DataSource = GetProgramsVersionsByProgramId(selectedCampus, True)
            End If
            RadListBoxProgramVersions.DataBind()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try

               
    End Sub

    Private Sub FillStudentGroupsControll()
        Try
            RadListBoxStudentGroupSelectors.DataTextField = "Descrip"
            RadListBoxStudentGroupSelectors.DataValueField = "LeadGrpId"
            Dim selectedCampus = CampusId

            If selectedCampus IsNot Nothing Then
                RadListBoxStudentGroupSelectors.DataSource = GetStudentGroups(selectedCampus, True)
            End If
            RadListBoxStudentGroupSelectors.DataBind()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
#End Region

    Protected Sub RadListProgramBoxes_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
        Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim programVersionToSelect As New List(Of arPrgVersion)
        Try
        RadListBoxProgramVersions2.Items.Clear()
        RadListBoxProgramVersions.Items.Clear()
        FillProgramVersionsControl()
            programVersionToSelect = DA.GetProgramVersionForProgram(GetCurrentlySelectedPrograms())
            For Each item As arPrgVersion In programVersionToSelect
                Dim itemToTransfer = RadListBoxProgramVersions.FindItemByValue(item.PrgVerId.ToString)
                If itemToTransfer IsNot Nothing Then
                    RadListBoxProgramVersions.Transfer(itemToTransfer, RadListBoxProgramVersions, RadListBoxProgramVersions2)
                End If
            Next
        Catch ex As Exception
            Dim exTracker = new AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try


    End Sub

    Protected Sub RadListBoxes_ItemDataBound(sender As Object, e As RadListBoxItemEventArgs)

    End Sub

    Protected Sub ProgramCheckBox_CheckedChanged(sender As Object, e As EventArgs)
        FillProgramsControl()
    End Sub

    Protected Sub RadListProgramVersionBoxes_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)

    End Sub

    Protected Sub chkInActiveProgramVersions_OnCheckedChangedCheckBox_CheckedChanged(sender As Object, e As EventArgs)

    End Sub

    Protected Sub RadListBoxStudentGroupSelector_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
       
    End Sub

    Protected Sub StudentGroupSelectorCheckBox_CheckedChanged(sender As Object, e As EventArgs)
      
    End Sub
End Class
