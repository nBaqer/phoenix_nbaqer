﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamTitleIV.ascx.vb" Inherits="ParamTitleIV" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">

        <div id="divAcademicYeardate" class="MultiFilterReportContainer" runat="server">
            <div id="SchoolReportingTypeHeader" class="CaptionLabel" runat="server">Title IV Audit Report Date Range</div>
            <div class="Filterinput">
                <table>
                    <tr>
                        <td class="style1">
                            <div>
                                <asp:Label ID="StartDatetext" class="DatePicker1Text" runat="server"  CssClass="LabelFilterInput">Start Date </asp:Label>
                            </div>
                            <telerik:RadDatePicker ID="RadDateStartDate" runat="server" CssClass="FilterInput"
                                Width="151px" DateInput-ReadOnly="true">
                                <Calendar ID="StartDateCal" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ReadOnly="false" runat="server"></DateInput>
                            </telerik:RadDatePicker>
                        </td>


                        <td>
                            <div>
                                <asp:Label ID="EndDateText" class="DatePicker1Text LabelFilterInput" runat="server">End Date </asp:Label>
                            </div>

                            <telerik:RadDatePicker ID="RadDateEndDate" runat="server"  CssClass="FilterInput"
                                Width="151px" DateInput-ReadOnly="true">
                                <Calendar ID="EndDateCal" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ReadOnly="false" runat="server"></DateInput>
                            </telerik:RadDatePicker>
                        </td>

                        <td>

                            <div id="YearSelector" class="YearSelector" runat="server">

                                <div id="RadComboBoxCohortYearLabel" runat="server">Cohort Year </div>
                                <telerik:RadComboBox ID="RadComboBoxCohortYear" runat="server" ToolTip="Select a Cohort Year" OnSelectedIndexChanged="CohortYearComboBox_SelectedIndexChanged" AutoPostBack="true">
                                </telerik:RadComboBox>
                            </div>
                        </td>
                </table>
            </div>
        </div>
        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer">
            <div id="CampusHeader" class="CaptionLabel" runat="server">Campus</div>

            <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true"
                OnItemDataBound="CampusComboBox_ItemDataBound" CssClass="ManualInput ReportLeftMarginInput"
                Width="350px" ToolTip="Select a campus to run the report">
            </telerik:RadComboBox>
        </div>

    </div>
</asp:Panel>

