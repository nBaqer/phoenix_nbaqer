﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamProgramDetails.ascx.vb" Inherits="ParamProgramDetails" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function checkFilter(sender, args) {
        if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgram2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one program selection is allowed');
        }
        else if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgramVersion2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one program version selection is allowed');
        }
        else if (args.get_destinationListBox().get_id().indexOf("RadListBoxCourse2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one course selection is allowed');
        }
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
        //var maxNumberOfItems = 1;
        var dest = args.get_destinationListBox();
        var totalCount = dest.get_items().get_count();
        var itemsToTransferCount = args.get_items().length;
        if (totalCount == maxNumberOfItems) {
            alert(message);
            args.set_cancel(true);
        } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
            while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                itemsToTransferCount--;
                args.get_items().pop();
            }
        }
    }
 </script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div><font color="red"> *</font>All filters are required and the filters need to be selected in the following order.</div>
    <div id="MainContainer" class="MainContainer">

        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer" runat="server" style="padding-left: 0px;">
            <div id="CampusHeader" class="CaptionLabel" runat="server">1. Campus <font color="red">*</font></div>

            <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged" OnItemDataBound="CampusComboBox_ItemDataBound" CssClass="ReportLeftMarginInput"
                Width="350px" ToolTip="Select a campus to run the report">
            </telerik:RadComboBox>
        </div>
    </div>
    <div id="ProgramSelector" class="ProgramSelector MultiFilterReportContainer" runat="server">
        <div id="captionlabel2" class="CaptionLabel" runat="server">2. Program <font color="red">*</font></div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxProgram" runat="server" Width="360px" Height="100px"
                OnTransferring="RadListBoxes_OnTransferring"
                OnClientTransferring="checkFilter"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                TransferToID="RadListBoxProgram2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" TransferButtons="Common" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxProgram2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                <ButtonSettings ShowDelete="False" ShowReorder="False" TransferButtons="Common"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactivePrograms" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                <span id="ProgramCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="ProgramCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div id="ProgramVersionSelector" class="ProgramVersionSelector MultiFilterReportContainer" runat="server">
        <div id="Div1" class="CaptionLabel" runat="server">3. Program Version <font color="red">*</font></div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxProgramVersion" runat="server" Width="360px" Height="100px"
                AllowTransfer="True"
                OnClientTransferring="checkFilter"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single"
                TransferToID="RadListBoxProgramVersion2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" TransferButtons="Common" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxProgramVersion2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="Div2" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactiveProgramVersions" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive ProgramVersions selectable" />
                <span id="ProgramVersionCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramVersionCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="ProgramVersionCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramVersionCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div id="CourseSelector" class="CourseSelector MultiFilterReportContainer" runat="server">
        <div id="Div3" class="CaptionLabel" runat="server">4. Course <font color="red">*</font></div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxCourse" runat="server" Width="360px" Height="100px"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single"
                TransferToID="RadListBoxCourse2">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxCourse2" runat="server"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Single">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
            </telerik:RadListBox>
            <div id="Div4" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactiveCourses" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive Courses selectable" />
                <span id="CourseCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblCourseCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="CourseCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblCourseCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
</asp:Panel>
