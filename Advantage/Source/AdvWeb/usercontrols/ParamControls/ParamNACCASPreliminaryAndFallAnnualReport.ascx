﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamNACCASPreliminaryAndFallAnnualReport.ascx.vb" Inherits="ParamNACCASPreliminaryAndFallAnnualReport" %>

<style>
   
</style>

<script type="text/javascript">
    function checkFilter(sender, args) {
        //if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgram2") > 1) {
        //    handleSourceToDestinationTransfer(args, 1, 'Only one program selection is allowed');
        //}
        //else if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgramVersion2") > 1) {
        //    handleSourceToDestinationTransfer(args, 1, 'Only one program version selection is allowed');
        //}
        //else if (args.get_destinationListBox().get_id().indexOf("RadListBoxCourse2") > 1) {
        //    handleSourceToDestinationTransfer(args, 1, 'Only one course selection is allowed');
        //}
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
        //var maxNumberOfItems = 1;
        var dest = args.get_destinationListBox();
        var totalCount = dest.get_items().get_count();
        var itemsToTransferCount = args.get_items().length;
        if (totalCount == maxNumberOfItems) {
            alert(message);
            args.set_cancel(true);
        } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
            while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                itemsToTransferCount--;
                args.get_items().pop();
            }
        }
    }

    $(function () {
        var $runReportBtn = $('[id*="BtnView"]');
        var $radComboReportName = $('div[id*="RadComboReportName"]');
        $runReportBtn.on('click', function () {

            var cohortResourceId = "851";
            var exemptedStudentsResourceId = "855";
            var combo1 = $find($radComboReportName.attr('id'));
            eraseCookie("LastReportDownloaded");

            if (combo1._value === cohortResourceId) {

                var interval = setInterval(function () {
                    combo1 = $find($radComboReportName.attr('id'));
                    if (GetCookie('LastReportDownloaded') === "851") {
                        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                        comboItem.set_text("Exempted Students");
                        comboItem.set_value(exemptedStudentsResourceId);
                        combo1.trackChanges();
                        combo1.get_items().add(comboItem);
                        combo1.findItemByValue(exemptedStudentsResourceId).select();
                        $runReportBtn.click();
                        combo1 = $find($radComboReportName.attr('id'));
                        combo1.get_items().remove(combo1.findItemByValue(exemptedStudentsResourceId));
                        combo1 = $find($radComboReportName.attr('id'));
                        combo1.findItemByValue(cohortResourceId).select();
                        eraseCookie("LastReportDownloaded");
                        clearInterval(interval);
                    }
                }, 3000);
            }
        })
    })
</script>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelNACCAS" runat="server">
</telerik:RadAjaxLoadingPanel>

<asp:Panel ID="MainPanel" runat="server" CssClass="content-panel">
    <div id="ReportTypeSelector" class="ReportTypeSelector SingleFilterReportContainter form-row" runat="server" style="padding-left: 2px;">
        <div id="ReportTypeHeader" class="CaptionLabel" runat="server">Report Type<span class="red-color">*</span></div>

        <telerik:RadComboBox ID="RadComboReportType" runat="server" AutoPostBack="true"
            OnSelectedIndexChanged="ReportTypeComboBox_SelectedIndexChanged" OnItemDataBound="RadComboReportType_OnItemDataBoundComboBox"
            Width="350px" Style="display: flex; margin: auto;" ToolTip="Select a report type to run the report" CssClass="FilterInput">
        </telerik:RadComboBox>
    </div>

    <div id="ReportNameSelector" class="ReportNameSelector SingleFilterReportContainter  form-row" runat="server" style="padding-left: 2px;">
        <div id="ReportNameHeader" class="CaptionLabel" runat="server">Report Name<span class="red-color">*</span></div>

        <telerik:RadComboBox ID="RadComboReportName" runat="server" AutoPostBack="true"
            OnSelectedIndexChanged="ReportNameComboBox_SelectedIndexChanged" OnItemDataBound="RadComboReportName_OnItemDataBoundComboBox"
            Width="350px" Style="display: flex; margin: auto;" ToolTip="Select a report name to run the report" CssClass="FilterInput">
        </telerik:RadComboBox>
    </div>

    <div id="YearSelector" class="YearSelector SingleFilterReportContainter" runat="server" style="padding-left: 2px;">
        <div id="YearHeader" class="CaptionLabel" runat="server">Reporting Year<span class="red-color">*</span></div>

        <telerik:RadComboBox ID="RadComboYear" runat="server" AutoPostBack="true"
            OnSelectedIndexChanged="YearComboBox_SelectedIndexChanged" OnItemDataBound="RadComboYear_OnItemDataBoundComboBox"
            Width="350px" Style="display: flex; margin: auto;" ToolTip="Select year to run the report" CssClass="FilterInput">
        </telerik:RadComboBox>
    </div>

    <div id="CampusSelector" class="CampusSelector SingleFilterReportContainter" runat="server" style="padding-left: 2px;">
        <div id="CampusHeader" class="CaptionLabel" runat="server">Campus<span class="red-color">*</span></div>

        <telerik:RadComboBox ID="RadComboCampus" runat="server" AutoPostBack="true"
            OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged" OnItemDataBound="RadComboCampus_OnItemDataBound"
            Width="350px" Style="display: flex; margin: auto;" ToolTip="Select campus to run the report" CssClass="FilterInput">
        </telerik:RadComboBox>
    </div>

    <div id="ProgramVersionSelector" class="ProgramVersionSelector MultiFilterReportContainer" runat="server">
        <div id="Div4" class="CaptionLabel" runat="server">Program</div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxPrograms" runat="server" Width="340px" Height="100px"
                OnTransferred="RadListProgramVersionBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True"
                TransferToID="RadListBoxPrograms2" SelectionMode="Multiple">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
            </telerik:RadListBox>
            <telerik:RadListBox ID="RadListBoxPrograms2" runat="server"
                OnTransferred="RadListProgramVersionBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound"
                Width="340px" Height="100px" AllowTransfer="True"
                AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <div id="Div5" class="InactiveCheckBoxContainer">
                <telerik:RadCheckBox ID="chkInActiveProgramVersions" runat="server" OnCheckedChanged="chkInActiveProgramVersions_OnCheckedChangedCheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive program selectable" />
                <span id="Span3" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramVersionsCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="Span4" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramVersionsCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
        </div>
        <br />
        <br />
    </div>
</asp:Panel>
