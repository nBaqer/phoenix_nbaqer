﻿Option Strict On
Imports System.Reflection
Imports FAME.Parameters.Info
Imports FAME.Parameters.Interfaces
Imports Telerik.Web.UI

Partial Class ConsecutiveAbsenceReportOptionsControl
    Inherits UserControl
    Implements ICustomControl, IParamSetControl

   #Region "Properties"
    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = Value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property

    Public Property ParameterSetLookup As String Implements IParamSetControl.ParameterSetLookup
    Public Property SetId As Integer Implements IParamSetControl.SetId
    Public Property IParamSetControl_SqlConn As String Implements IParamSetControl.SqlConn

    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property

    Public Property ParamSet As ParameterSetInfo Implements IParamSetControl.ParamSet
    Public Property DisplayName As String Implements IParamSetControl.DisplayName

    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        
    End Sub
   
#End Region

    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride

            'rblShowCalendarDays Settings
            Dim ctrlSettingShowCalendarDays As New ControlSettingInfo
            Dim ctrlValuesShowCalendarDays As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo

            ctrlSettingShowCalendarDays.ControlName = rblShowCalendarDays.ID
            ctrlValuesShowCalendarDays.Add(objControlValueInfo)
            ctrlSettingShowCalendarDays.ControlValueCollection = ctrlValuesShowCalendarDays


            Dim Selected As ListItem = rblShowCalendarDays.SelectedItem
            objControlValueInfo.DisplayText = Selected.Text
            objControlValueInfo.KeyData = Selected.Value
            SettingsCollection.Add(ctrlSettingShowCalendarDays)

            UserSettings.ControlSettingsCollection = SettingsCollection
        Catch ex As Exception
            Dim exTracker = new AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        Return UserSettings
    End Function

    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim DisplayTree As New RadTreeView
        DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Dim ShowCalendarDaysSelection As ListItem = rblShowCalendarDays.SelectedItem
        Dim ReportOptionsNode As New RadTreeNode
        ReportOptionsNode.Text = "Custom Options"
        ReportOptionsNode.Value = CStr(ItemDetail.DetailId)
        ReportOptionsNode.CssClass = "TreeParentNode"
        
        Dim ShowWorkUnitsNode As New RadTreeNode
        ShowWorkUnitsNode.Text = ShowCalendarDaysSelection.Text
        
        ReportOptionsNode.Nodes.Add(ShowWorkUnitsNode)
        DisplayTree.Nodes.Add(ReportOptionsNode)
        Return DisplayTree
    End Function
    
    Public Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is CheckBoxList Then
                            Dim ChkBoxList As CheckBoxList = DirectCast(ctrl, CheckBoxList)
                            For Each Chk As ListItem In ChkBoxList.Items
                                Chk.Selected = False
                            Next
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim chkitem As ListItem = ChkBoxList.Items.FindByText(ItemValue.DisplayText)
                                If Not chkitem Is Nothing Then
                                    chkitem.Selected = CBool(ItemValue.KeyData)
                                End If

                            Next
                        Else
                            Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
                'HideShowControls()
            End If
        Catch ex As Exception
            Dim exTracker = new AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
End Class
