﻿
Imports System.Reflection
Imports FAME.Advantage.Common
Imports FAME.Parameters.Info
Imports FAME.Parameters.Interfaces
Imports Telerik.Web.UI

Partial Class ParamDateRange
    Inherits System.Web.UI.UserControl
    Implements ICustomControl
    Protected MyAdvAppSettings As AdvAppSettings
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    Private _ItemDetail As ParameterDetailItemInfo
    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property

    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride

            Dim ctrlSetting1 As New ControlSettingInfo
            Dim ctrlSetting2 As New ControlSettingInfo

            ctrlSetting1.ControlName = RadStartDatePicker.ID
            ctrlSetting2.ControlName = RadEndDatePicker.ID

            Dim ctrlValues2 As New List(Of ControlValueInfo)
            Dim ctrlValues4 As New List(Of ControlValueInfo)

            Dim pickerItem2 As RadDateInput = RadStartDatePicker.DateInput
            Dim pickerItem4 As RadDateInput = RadEndDatePicker.DateInput

            Dim objControlValue2 As New ControlValueInfo
            objControlValue2.DisplayText = pickerItem2.SelectedDate.ToString
            objControlValue2.KeyData = pickerItem2.SelectedDate.ToString

            Dim objControlValue4 As New ControlValueInfo
            objControlValue4.DisplayText = pickerItem4.SelectedDate.ToString
            objControlValue4.KeyData = pickerItem4.SelectedDate.ToString

            ctrlValues2.Add(objControlValue2)
            ctrlValues4.Add(objControlValue4)

            ctrlSetting1.ControlValueCollection = ctrlValues2
            ctrlSetting2.ControlValueCollection = ctrlValues4

            settingsCollection.Add(ctrlSetting1)
            settingsCollection.Add(ctrlSetting2)

            userSettings.ControlSettingsCollection = settingsCollection
        Catch ex As Exception
            Dim exTracker = new AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        Return userSettings

    
    End Function

    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim displayTree As New RadTreeView
        displayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim reportnameDays As String = Page.Title
            Dim headerNode21 As New RadTreeNode
            headerNode21.Text = "Date Range"
            headerNode21.Value = "Date Range"
            headerNode21.CssClass = "TreeParentNode"
            Dim programNode21 As New RadTreeNode

            If RadStartDatePicker.SelectedDate.HasValue And RadEndDatePicker.SelectedDate.HasValue then

            programNode21.Text = RadStartDatePicker.SelectedDate.Value.ToShortDateString() + " - " +RadEndDatePicker.SelectedDate.Value.ToShortDateString()
            End If
            headerNode21.Nodes.Add(programNode21)
            displayTree.Nodes.Add(headerNode21)
            Return displayTree
        Catch ex As Exception
            Dim exTracker = new AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function

    Public Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            RadStartDatePicker.SelectedDate = Nothing
            RadEndDatePicker.SelectedDate = Nothing
            If Not SavedSettings Is Nothing Then
                For Each setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadDatePicker Then
                            Dim radPicker As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                If IsDate(itemValue.KeyData) = True Then
                                    radPicker.SelectedDate = CDate(itemValue.KeyData)
                                End If
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim RadBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each ItemValue As ControlValueInfo In setting.ControlValueCollection
                                RadBox.SelectedValue = ItemValue.KeyData
                            Next
                        Else
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadStartDatePicker.SelectedDate = Nothing
                RadEndDatePicker.SelectedDate = Nothing
            End If
        Catch ex As Exception
            Dim exTracker = new AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        SetProperties()
        If Not Page.IsPostBack Then
            dim now = DateTime.Now
            RadStartDatePicker.SelectedDate = now.AddDays(-7)
            RadEndDatePicker.SelectedDate = now
        End If
    End Sub

    
    Private Sub SetProperties()
        Try
            'RadDateReportDate.MaxDate = DateTime.Now
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
            Dim exTracker = new AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

        Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
End Class
