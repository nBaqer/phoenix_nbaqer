﻿Option Strict On

Imports System.Activities.Statements
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports NHibernate.Hql.Ast.ANTLR
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports System.Runtime.Remoting.Messaging
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common
Partial Class ParamStudentGPADetails
    Inherits UserControl
    Implements ICustomControl

    Private _ItemDetail As ParameterDetailItemInfo
    Private _DAClass As String
    Private _DAMethod As String
    Private _BindingTextField As String
    Private _BindingValueField As String
    Private _Caption As String
    Private _AssemblyFilePathDA As String
    Private _AssemblyDA As Assembly
    Private _SqlConn As String
    Private _SavedSetting As ParamItemUserSettingsInfo
    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString

    Private _StudentIdentifier As String
    Private _ProgramReporterStart As String
    Private _ProgramReporterEnd As String
    Private _AcademicYearStart As String
    Private _AcademicYearEnd As String
    Private _StartDatePartProgram As String
    Private _StartDatePartAcademic As String
    Private _EndDatePartProgram As String
    Private _EndDatePartAcademic As String
    Private _SelectedDatePartProgram As String
    Private _SelectedDatePartAcademic As String

    Protected MyAdvAppSettings As AdvAppSettings

    Public Property ItemDetail() As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return _ItemDetail
        End Get
        Set(ByVal value As ParameterDetailItemInfo)
            _ItemDetail = value
        End Set
    End Property
    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & Me.ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & Me.ID) = Value
        End Set
    End Property
    Public Property FilterMode() As Boolean
        Get
            If (Session("QuickFilterMode") Is Nothing) Then
                Return False
            Else
                Return DirectCast(Session("QuickFilterMode"), Boolean)
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("QuickFilterMode") = value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property
    Public Property SavedSettings() As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings
        Get
            Return _SavedSetting
        End Get
        Set(ByVal value As ParamItemUserSettingsInfo)
            _SavedSetting = value
        End Set
    End Property
    Public Property GradesFormat() As String
        Get
            If (ViewState("GradesFormat") Is Nothing) Then
                Return "letter"
            Else
                Return ViewState("GradesFormat").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("GradesFormat") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        SetProperties()
        'IPEDSControlType = ControlType.GradRates
        If Not Page.IsPostBack Then
            FillCampusControl()
            RadComboCampus.SelectedValue = HttpContext.Current.Request.Params("cmpid").ToString
            FillProgramControls()
            FillRadListBoxProgramVersion1()
            'FillTermControls()
            FillRadListBoxTerm1()
            FillEnrollmentStatusControls()
            FillStudentGroupControls()
            ListBoxCounts()
            RangeFiltersSetUp()
        End If
    End Sub
#Region "Methods"
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillProgramControls()
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxProgram1.DataTextField = "ProgDescrip"
            RadListBoxProgram1.DataValueField = "ProgId"
            RadListBoxProgram1.DataSource = GetProgramsByCampusId(selectedCampus, True)
            RadListBoxProgram1.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    'Public Sub FillProgramVersionControls(Optional ByVal progId As String = Nothing)
    '    Try
    '        Dim selectedCampus As New Guid
    '        Dim selectedPrgIdList As New List(Of Guid)
    '        Dim selectedPrgVersionIdList As New List(Of Guid)
    '        Dim statusValues As New List(Of String)
    '        selectedCampus = Guid.Parse(RadComboCampus.SelectedValue)
    '        'selectedPrgIdList = GetCurrentlySelectedProgIdList(progId)
    '        selectedPrgVersionIdList = GetCurrentlySelectedPrgVersionIdList()
    '        statusValues = GetStatusFilters("cbkInactivePrgVersions")
    '        RadListBoxProgramVersion1.DataTextField = "PrgVerDescrip"
    '        RadListBoxProgramVersion1.DataValueField = "PrgVerId"
    '        'RadListBoxProgramVersion.DataSource = GetProgramVersionByCampusId(selectedCampus, True, progId)
    '        RadListBoxProgramVersion1.DataSource = GetProgramVersionByCampusIdPrgIdList(selectedCampus, selectedPrgIdList, selectedPrgVersionIdList, statusValues)
    '        RadListBoxProgramVersion1.DataBind()
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Sub
    Public Sub FillRadListBoxProgramVersion1()
        Try
            ' Delete from RadListBoxProgramVersion2 items do not belong to selected programs 
            ' Fill RadListBox ProgramVersion1  

            Dim selectedCampus As New Guid
            Dim selectedPrgIdList As New List(Of Guid)
            Dim selectedPrgVersionIdList As New List(Of Guid)
            Dim statusValues As New List(Of String)
            Dim emptyList As New List(Of Guid)
            Dim PrgVersionList As List(Of arPrgVersion)
            Dim selectedPrgVerId As Guid
            Dim RadListBoxProgramVersionItemList As New List(Of RadListBoxItem)
            selectedCampus = Guid.Parse(RadComboCampus.SelectedValue)
            selectedPrgIdList = GetCurrentlySelectedIdsList(RadListBoxProgram2.Items)
            statusValues = GetStatusFilters("cbkInactivePrgVersions")


            ' Delete from RadListBoxProgramVersion2 items do not belong to selected programs
            PrgVersionList = GetProgramVersionByCampusIdPrgIdList(selectedCampus, selectedPrgIdList, emptyList, statusValues)
            For Each item As RadListBoxItem In RadListBoxProgramVersion2.Items
                RadListBoxProgramVersionItemList.Add(item)
            Next
            For Each item As RadListBoxItem In RadListBoxProgramVersionItemList
                selectedPrgVerId = Guid.Parse(item.Value)
                Dim exists = PrgVersionList.Any(Function(x) x.PrgVerId = selectedPrgVerId)
                If (Not exists) Then
                    RadListBoxProgramVersion2.Items.Remove(item)
                End If
            Next

            ' Fill RadListBox ProgramVersion1  
            selectedPrgVersionIdList = GetCurrentlySelectedIdsList(RadListBoxProgramVersion2.Items)
            RadListBoxProgramVersion1.DataTextField = "PrgVerDescrip"
            RadListBoxProgramVersion1.DataValueField = "PrgVerId"
            'RadListBoxProgramVersion.DataSource = GetProgramVersionByCampusId(selectedCampus, True, progId)
            RadListBoxProgramVersion1.DataSource = GetProgramVersionByCampusIdPrgIdList(selectedCampus, selectedPrgIdList, selectedPrgVersionIdList, statusValues)
            RadListBoxProgramVersion1.DataBind()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    'Public Sub FillTermControls(Optional ByVal progId As Nullable(Of Guid) = Nothing)
    '    Try
    '        Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
    '        RadListBoxTerm1.DataTextField = "TermDescrip"
    '        RadListBoxTerm1.DataValueField = "TermId"
    '        RadListBoxTerm1.DataSource = GetTermsByCampusId(selectedCampus, True, progId)
    '        RadListBoxTerm1.DataBind()
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Sub
    Public Sub FillRadListBoxTerm1()
        Try
            ' Delete from RadListBoxrterm2 items do not belong to selected programs 
            ' Fill RadListBox Term1  

            Dim selectedCampus As New Guid
            Dim selectedPrgIdList As New List(Of Guid)
            Dim selectedTermIdList As New List(Of Guid)
            Dim statusValues As New List(Of String)
            Dim emptyList As New List(Of Guid)
            Dim TermList As List(Of arTerm)
            Dim selectedTermId As Guid
            Dim RadListBoxTermItemList As New List(Of RadListBoxItem)
            selectedCampus = Guid.Parse(RadComboCampus.SelectedValue)
            selectedPrgIdList = GetCurrentlySelectedIdsList(RadListBoxProgram2.Items)
            statusValues = GetStatusFilters("cbkInactiveTerms")

            ' Delete from RadListBoxrterm2 items do not belong to selected programs
            TermList = GetTermsByCampusIdPrgIdList(selectedCampus, selectedPrgIdList, selectedTermIdList, statusValues)
            For Each item As RadListBoxItem In RadListBoxTerm2.Items
                RadListBoxTermItemList.Add(item)
            Next
            For Each item As RadListBoxItem In RadListBoxTermItemList
                selectedTermId = Guid.Parse(item.Value)
                Dim exists = TermList.Any(Function(x) x.TermId = selectedTermId)
                If (Not exists) Then
                    RadListBoxTerm2.Items.Remove(item)
                End If
            Next

            ' Fill RadListBox Term1  
            selectedTermIdList = GetCurrentlySelectedIdsList(RadListBoxTerm2.Items)
            RadListBoxTerm1.DataTextField = "TermDescrip"
            RadListBoxTerm1.DataValueField = "TermId"
            RadListBoxTerm1.DataSource = GetTermsByCampusIdPrgIdList(selectedCampus, selectedPrgIdList, selectedTermIdList, statusValues)
            RadListBoxTerm1.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillEnrollmentStatusControls(Optional ByVal progId As Nullable(Of Guid) = Nothing)
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxEnrollmentStatus1.DataTextField = "StatusCodeDescrip"
            RadListBoxEnrollmentStatus1.DataValueField = "StatusCodeId"
            RadListBoxEnrollmentStatus1.DataSource = GetEnrollmentStatusByCampusId(selectedCampus, True, progId)
            RadListBoxEnrollmentStatus1.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Public Sub FillStudentGroupControls(Optional ByVal progId As Nullable(Of Guid) = Nothing)
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxStudentGroup1.DataTextField = "Descrip"
            RadListBoxStudentGroup1.DataValueField = "LeadGrpId"
            RadListBoxStudentGroup1.DataSource = GetStudentGroupByCampusId(selectedCampus, True, progId)
            RadListBoxStudentGroup1.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub


    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim DA As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of syCampus)
        Try
            result = DA.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString))
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetProgramsByCampusId(ByVal SelectedCampus As Guid, ByVal FilteredBySelected As Boolean) As List(Of arProgram)
        Dim DA As New ProgramDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arProgram)
        Try
            Dim SelectedPrograms As List(Of String) = GetCurrentlySelectedPrograms()
            If SelectedPrograms Is Nothing Or FilteredBySelected = False Then
                result = DA.GetProgramsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrograms"))
            Else
                result = DA.GetProgramsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrograms"), SelectedPrograms)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    'Private Function GetProgramVersionByCampusId(ByVal SelectedCampus As Guid _
    '                                            , ByVal FilteredBySelected As Boolean, _
    '                                            Optional ByVal progId As Nullable(Of Guid) = Nothing) As List(Of arPrgVersion)
    '    Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
    '    Dim result As New List(Of arPrgVersion)
    '    Try
    '        Dim SelectedProgramVersions As List(Of String) = GetCurrentlySelectedProgramVersions()
    '        If SelectedProgramVersions Is Nothing Or FilteredBySelected = False Then
    '            If progId Is Nothing Then
    '                'if progId is not selected get all PrgVersions for the selected campus
    '                result = DA.GetProgramVersionsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrgVersions"))
    '            Else
    '                'progId Program was selected so only get the programversions for the given ProgramID 
    '                result = DA.GetProgramVersionsByCampusIdandProgId(SelectedCampus, GetStatusFilters("cbkInactivePrgVersions"), CType(progId, Guid))
    '            End If

    '        Else
    '            'Already exist ProgramVersion selected
    '            If progId = Guid.Empty Then
    '                result = DA.GetProgramVersionsByCampusId(SelectedCampus, GetStatusFilters("cbkInactivePrgVersions"), SelectedProgramVersions)
    '            Else
    '                result = DA.GetProgramVersionsByCampusIdandProgramIdCollection(SelectedCampus, GetStatusFilters("cbkInactivePrgVersions"), _
    '                                                                               SelectedProgramVersions, CType(progId, Guid))
    '            End If

    '        End If
    '        Return result
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Function
    Private Function GetProgramVersionByCampusIdPrgIdList(ByVal selectedCampus As Guid _
                                            , ByVal selectedProgIdList As List(Of Guid) _
                                            , ByVal selectedPrgVersionIdList As List(Of Guid) _
                                            , ByVal statusValues As List(Of String) _
                                            ) As List(Of arPrgVersion)
        Dim DA As New ProgramVersionDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arPrgVersion)
        Try
            result = DA.GetPrgVersionByCmpGrpIdProgIdListPrgVersionIdList(selectedCampus, selectedProgIdList, selectedPrgVersionIdList, statusValues)
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    'Private Function GetTermsByCampusId(ByVal selectedCampus As Guid, ByVal filteredBySelected As Boolean, _
    '                                             Optional ByVal progId As Nullable(Of Guid) = Nothing) As DataTable
    '    Dim DA As New CoursesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
    '    Dim result As DataTable
    '    Dim statusId As String
    '    Dim CourseFacade As New CoursesFacade
    '    Try
    '        Dim SelectedTerms As List(Of String) = GetCurrentlySelectedTerms()
    '        Dim CKbox As CheckBox = DirectCast(Me.FindControl("cbkInactiveTerms"), CheckBox)
    '        Dim boolActive As Boolean = False
    '        If CKbox.Checked = True Then
    '            statusId = "1AF592A6-8790-48EC-9916-5412C25EF49F"
    '            boolActive = False
    '        Else
    '            statusId = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"
    '            boolActive = True
    '        End If
    '        If SelectedTerms Is Nothing Or filteredBySelected = False Then
    '            result = CourseFacade.GetTermsByCampus(selectedCampus.ToString, boolActive)
    '        Else
    '            result = CourseFacade.GetAvailableTermsAndIgnoreSelectedTerms(selectedCampus.ToString, SelectedTerms.ToString(), boolActive)
    '        End If
    '        Return result
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Function
    Private Function GetTermsByCampusIdPrgIdList(ByVal selectedCampus As Guid _
                                        , ByVal selectedProgIdList As List(Of Guid) _
                                        , ByVal selectedTermIdList As List(Of Guid) _
                                        , ByVal statusValues As List(Of String) _
                                        ) As List(Of arTerm)

        Dim DA As New TermDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As New List(Of arTerm)
        Try
            result = DA.GetTermByCmpGrpIdProgIdListTermIdList(selectedCampus, selectedProgIdList, selectedTermIdList, statusValues)
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try

    End Function

    Private Function ConvertListToStringList(selectedCampus As Guid, convertListToStringList1 As String, s1 As String, s As String) As Guid
        Throw New NotImplementedException
    End Function

    Private Function GetEnrollmentStatusByCampusId(ByVal selectedCampus As Guid, ByVal filteredBySelected As Boolean, _
                                                Optional ByVal progId As Nullable(Of Guid) = Nothing) As DataTable
        Dim da As New CoursesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As DataTable
        Dim courseFacade As New CoursesFacade
        Try
            Dim selectedEnrollmentStatus As List(Of String) = GetCurrentlySelectedEnrollmentStatus()
            Dim cKbox As CheckBox = DirectCast(Me.FindControl("cbkInactiveTerms"), CheckBox)
            Dim boolActive As Boolean = False
            If cKbox.Checked = True Then
                boolActive = False
            Else
                boolActive = True
            End If
            If selectedEnrollmentStatus Is Nothing Or filteredBySelected = False Then
                result = courseFacade.GetEnrollmentStatusByCampus(selectedCampus.ToString, boolActive)
            Else
                result = courseFacade.GetAvailableEnrollmentStatusAndIgnoreSelectedStatus(selectedCampus.ToString, selectedEnrollmentStatus.ToString(), boolActive)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetStudentGroupByCampusId(ByVal selectedCampus As Guid, ByVal filteredBySelected As Boolean, _
                                               Optional ByVal progId As Nullable(Of Guid) = Nothing) As DataTable
        Dim da As New CoursesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As DataTable
        Dim courseFacade As New CoursesFacade
        Try
            Dim selectedStudentGroup As List(Of String) = GetCurrentlySelectedStudentGroup()
            Dim cKbox As CheckBox = DirectCast(Me.FindControl("cbkInactiveTerms"), CheckBox)
            Dim boolActive As Boolean = False
            If cKbox.Checked = True Then
                boolActive = False
            Else
                boolActive = True
            End If
            If selectedStudentGroup Is Nothing Or filteredBySelected = False Then
                result = courseFacade.GetStudentGroupByCampus(selectedCampus.ToString, boolActive)
            Else
                result = courseFacade.GetAvailableStudentGroupsAndIgnoreSelectedGroups(selectedCampus.ToString, selectedStudentGroup.ToString(), boolActive)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim statusValues As New List(Of String)
        Try
            Dim CKbox As CheckBox = DirectCast(Me.FindControl(ctrlName), CheckBox)
            If CKbox.Checked = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return statusValues
    End Function
    Private Function GetCurrentlySelectedPrograms() As List(Of String)
        Dim selectedPrograms As New List(Of String)
        Try
            If RadListBoxProgram2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim selected As String = item.Value
                    selectedPrograms.Add(selected)
                Next
                Return selectedPrograms
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    'Private Function GetCurrentlySelectedPrgIdList() As List(Of Guid)
    '    Dim selectedPrograms As New List(Of Guid)
    '    For Each item As RadListBoxItem In RadListBoxProgram2.Items
    '        Dim selected As String = item.Value
    '        selectedPrograms.Add(Guid.Parse(selected))
    '    Next
    '    Return selectedPrograms
    'End Function
    'Private Function GetCurrentlySelectedTerms() As List(Of String)
    '    Dim selectedTerms As New List(Of String)
    '    Try
    '        If RadListBoxTerm2.Items.Count > 0 Then
    '            For Each item As RadListBoxItem In RadListBoxTerm2.Items
    '                Dim selected As String = item.Value
    '                selectedTerms.Add(selected)
    '            Next
    '            Return selectedTerms
    '        Else
    '            Return Nothing
    '        End If

    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Function
    Private Function GetCurrentlySelectedEnrollmentStatus() As List(Of String)
        Dim selectedEnrollmentStatus As New List(Of String)
        Try
            If RadListBoxEnrollmentStatus2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxEnrollmentStatus2.Items
                    Dim selected As String = item.Value
                    selectedEnrollmentStatus.Add(selected)
                Next
                Return selectedEnrollmentStatus
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetCurrentlySelectedStudentGroup() As List(Of String)
        Dim selectedStudentGroup As New List(Of String)
        Try
            If radListBoxStudentGroup2.Items.Count > 0 Then
                For Each item As RadListBoxItem In radListBoxStudentGroup2.Items
                    Dim selected As String = item.Value
                    selectedStudentGroup.Add(selected)
                Next
                Return selectedStudentGroup
            Else
                Return Nothing
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    'Private Function GetCurrentlySelectedPrgVersionIdList() As List(Of Guid)
    '    Dim selectedProgramVersions As New List(Of Guid)
    '    For Each item As RadListBoxItem In RadListBoxProgramVersion2.Items
    '        Dim selected As String = item.Value
    '        selectedProgramVersions.Add(Guid.Parse(selected))
    '    Next
    '    Return selectedProgramVersions
    'End Function
    'Private Function GetCurrentlySelectedProgIdList(ByVal rlbProgram2Items As IList(Of RadListBoxItem)) As List(Of Guid)
    '    Dim selectedPrograms As New List(Of Guid)
    '    For Each item As RadListBoxItem In RadListBoxProgram2.Items
    '        Dim selected As String = item.Value
    '        selectedPrograms.Add(Guid.Parse(selected))
    '    Next
    '    'If Not String.IsNullOrEmpty(progId) Then
    '    '    selectedPrograms.Add(Guid.Parse(progId))
    '    'End If
    '    For Each item As RadListBoxItem In progItems
    '        selectedPrograms.Add(Guid.Parse(item.Value))
    '    Next
    '    Return selectedPrograms
    'End Function
    Private Function GetCurrentlySelectedIdsList(ByVal radListBoxItems As IList(Of RadListBoxItem)) As List(Of Guid)
        Dim selectedIdsList As New List(Of Guid)
        For Each item As RadListBoxItem In radListBoxItems
            Dim selected As String = item.Value
            selectedIdsList.Add(Guid.Parse(selected))
        Next
        Return selectedIdsList
    End Function
    'Private Function GetCurrentlySelectedProgramVersions() As List(Of String)
    '    Dim selectedProgramVersions As New List(Of String)
    '    Try
    '        If RadListBoxProgramVersion2.Items.Count > 0 Then
    '            For Each item As RadListBoxItem In RadListBoxProgramVersion2.Items
    '                Dim selected As String = item.Value
    '                selectedProgramVersions.Add(selected)
    '            Next
    '            Return selectedProgramVersions
    '        Else
    '            Return Nothing
    '        End If

    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw ex
    '    End Try
    'End Function
    'Protected Sub RadListBoxProgram1_Transferred(sender As Object, e As RadListBoxTransferredEventArgs) ' Handles RadListBoxProgram1.Transferred
    '    If RadListBoxProgram2.Items.Count = 0 Then
    '        RadListBoxProgram1.SelectedIndex = -1
    '        FillRadListBoxProgramVersion1()
    '    End If
    '    ListBoxCounts()
    'End Sub
    'Protected Sub RadListBoxProgramVersion1_Transferred(sender As Object, e As RadListBoxTransferredEventArgs) ' Handles RadListBoxProgramVersion1.Transferred
    '    If RadListBoxProgramVersion2.Items.Count = 0 Then
    '        RadListBoxProgramVersion1.SelectedIndex = -1
    '        RadListBoxTerm.Items.Clear()
    '        RadListBoxTerm2.Items.Clear()
    '        FillTermControls()
    '    End If
    '    ListBoxCounts()
    'End Sub
    'Protected Sub RadListBoxProgram2_Transferring(sender As Object, e As RadListBoxTransferringEventArgs) 'Handles RadListBoxProgram2.Transferring
    'End Sub
    'Protected Sub RadListBoxes_OnTransferring(sender As Object, e As RadListBoxTransferringEventArgs)
    '    Dim A As String = String.Empty
    'End Sub
    'Protected Sub RadListBoxProgram_OnTransferring(sender As Object, e As RadListBoxTransferringEventArgs)
    '    'FillRadListBoxProgramVersion1()
    '    'ListBoxCounts()
    'End Sub
    Protected Sub RadListBoxProgram_OnTransferred(sender As Object, e As RadListBoxTransferredEventArgs)
        FillRadListBoxProgramVersion1()
        FillRadListBoxTerm1()
        ListBoxCounts()
    End Sub

    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
        Dim lBox As RadListBox = DirectCast(sender, RadListBox)
        'If lBox.ID.Contains("chkShowGPAbyTerm") Then
        '    If RadListBoxTerm2.Items.Count >= 1 Then
        '        TermCreditRangeOptions.Visible = True
        '        TermGPAAvgRangeOptions.Visible = True
        '    End If
        'End If
        ListBoxCounts()
    End Sub
    'Protected Sub RadListBoxEnrollmentStatus1_OnTransferring(ByVal sender As Object, ByVal e As RadListBoxTransferringEventArgs)
    '    Dim A As String = String.Empty
    'End Sub
    'Protected Sub RadListBoxEnrollmentStatus2_OnTransferring(ByVal sender As Object, ByVal e As RadListBoxTransferringEventArgs)
    '    Dim A As String = String.Empty
    'End Sub
    'Protected Sub RadListBoxProgramVersion1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadListBoxProgramVersion1.SelectedIndexChanged
    '    RadListBoxTerm1.Items.Clear()
    '    RadListBoxTerm2.Items.Clear()
    '    'FillTermControls(New Guid(RadListBoxProgramVersion1.SelectedValue))
    '    FillRadListBoxTerm1()
    '    ListBoxCounts()
    'End Sub
    'Protected Sub RadListBoxTerm_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadListBoxTerm1.SelectedIndexChanged
    '    Dim A As String = String.Empty
    'End Sub
    'Protected Sub RadListBoxTerm_Transferred(sender As Object, e As RadListBoxTransferredEventArgs) Handles RadListBoxTerm1.Transferred
    '    If RadListBoxTerm2.Items.Count = 0 Then
    '        RadListBoxTerm1.SelectedIndex = -1
    '        'FillTermControls()
    '        FillRadListBoxTerm1()
    '    End If
    '    ListBoxCounts()
    'End Sub
    Protected Sub CampusComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        RadListBoxProgram1.Items.Clear()
        RadListBoxProgram2.Items.Clear()
        RadListBoxProgramVersion1.Items.Clear()
        RadListBoxProgramVersion2.Items.Clear()
        RadListBoxTerm1.Items.Clear()
        RadListBoxTerm2.Items.Clear()
        FillProgramControls()
        FillRadListBoxProgramVersion1()
        'FillTermControls()
        FillRadListBoxTerm1()
        ListBoxCounts()
        RangeFiltersSetUp()
    End Sub
    Protected Sub CampusComboBox_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        Dim myItem As RadComboBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is syCampus Then
                Dim MysyCampus As syCampus = DirectCast(myItem.DataItem, syCampus)
                If MysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ChkBox As CheckBox = DirectCast(sender, CheckBox)
        If ChkBox.ID.Contains("Program") Then
            FillProgramControls()
        ElseIf ChkBox.ID.Contains("PrgVersion") Then
            FillRadListBoxProgramVersion1()
        ElseIf ChkBox.ID.Contains("Term") Then
            FillRadListBoxTerm1()
        End If
        ListBoxCounts()
    End Sub
    Private Sub ListBoxCounts()
        Dim boxcount1 As Integer = RadListBoxProgram1.Items.Count()
        Dim boxcount2 As Integer = RadListBoxProgram2.Items.Count()
        Dim boxcount3 As Integer = RadListBoxProgramVersion1.Items.Count()
        Dim boxcount4 As Integer = RadListBoxProgramVersion2.Items.Count()
        Dim boxcount5 As Integer = RadListBoxTerm1.Items.Count()
        Dim boxcount6 As Integer = RadListBoxTerm2.Items.Count()
        Dim boxcount7 As Integer = RadListBoxEnrollmentStatus1.Items.Count()
        Dim boxcount8 As Integer = RadListBoxEnrollmentStatus2.Items.Count()
        Dim boxcount9 As Integer = RadListBoxStudentGroup1.Items.Count()
        Dim boxcount10 As Integer = RadListBoxStudentGroup2.Items.Count()

        lblProgramCounterAvailable.Text = boxcount1.ToString.Trim & " available"
        lblProgramCounterAvailable.ToolTip = boxcount1.ToString.Trim & " available " & Caption & "(s)"
        lblProgramCounterSelected.Text = boxcount2.ToString.Trim & " selected"
        lblProgramCounterSelected.ToolTip = boxcount2.ToString.Trim & " selected " & Caption & "(s)"

        lblProgramVersionCounterAvailable.Text = boxcount3.ToString.Trim & " available"
        lblProgramVersionCounterAvailable.ToolTip = boxcount3.ToString.Trim & " available " & Caption & "(s)"
        lblProgramVersionCounterSelected.Text = boxcount4.ToString.Trim & " selected"
        lblProgramVersionCounterSelected.ToolTip = boxcount4.ToString.Trim & " selected " & Caption & "(s)"

        lblTermCounterAvailable.Text = boxcount5.ToString.Trim & " available"
        lblTermCounterAvailable.ToolTip = boxcount5.ToString.Trim & " available " & Caption & "(s)"
        lblTermCounterSelected.Text = boxcount6.ToString.Trim & " selected"
        lblTermCounterSelected.ToolTip = boxcount6.ToString.Trim & " selected " & Caption & "(s)"

        lblEnrollmentStatusCounterAvailable.Text = boxcount7.ToString.Trim & " available"
        lblEnrollmentStatusCounterAvailable.ToolTip = boxcount7.ToString.Trim & " available " & Caption & "(s)"
        lblEnrollmentStatusCounterSelected.Text = boxcount8.ToString.Trim & " selected"
        lblEnrollmentStatusCounterAvailable.ToolTip = boxcount8.ToString.Trim & " selected " & Caption & "(s)"

        'lblEnrollmentStatusCounterAvailable.Text = boxcount7.ToString.Trim & " available"
        'lblEnrollmentStatusCounterAvailable.ToolTip = boxcount7.ToString.Trim & " available " & Caption & "(s)"
        'lblEnrollmentStatusCounterSelected.Text = boxcount8.ToString.Trim & " selected"
        'lblEnrollmentStatusCounterAvailable.ToolTip = boxcount8.ToString.Trim & " selected " & Caption & "(s)"

        lblStudentGroupAvailable.Text = boxcount9.ToString.Trim & " available"
        lblStudentGroupAvailable.ToolTip = boxcount9.ToString.Trim & " available " & Caption & "(s)"
        lblStudentGroupSelected.Text = boxcount10.ToString.Trim & " selected"
        lblStudentGroupSelected.ToolTip = boxcount10.ToString.Trim & " selected " & Caption & "(s)"

    End Sub
    Private Sub RangeFiltersSetUp()
        'GradeFormat to define Mininmun value and maximun values for Term GPA/Avg Range and Cumulative GPA/Average Range
        GradesFormat = MyAdvAppSettings.AppSettings("GradesFormat", RadComboCampus.SelectedValue).ToLower()
        'Define default values for GPA or Average
        If GradesFormat = "letter" Then
            cumulativeGPA_GT.Text = "0.00"
            cumulativeGPA_LT.Text = "4.00"
            radTermCredits_GT.Text = "0.00"
            radTermCredits_LT.Text = "100.00"
            radTermGPA_GT.Text = "0.00"
            radTermGPA_LT.Text = "4.00"
        Else
            cumulativeGPA_GT.Text = "0.00"
            cumulativeGPA_LT.Text = "100.00"
            radTermCredits_GT.Text = "0.00"
            radTermCredits_LT.Text = "100.00"
            radTermGPA_GT.Text = "0.00"
            radTermGPA_LT.Text = "100.00"
        End If

    End Sub
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim DisplayTree As New RadTreeView
        DisplayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim HeaderNode1 As New RadTreeNode
            HeaderNode1.Text = "Campus"
            HeaderNode1.Value = "Campus"
            HeaderNode1.CssClass = "TreeParentNode"
            Dim CampusNode As New RadTreeNode
            CampusNode.Text = RadComboCampus.SelectedItem.Text
            HeaderNode1.Nodes.Add(CampusNode)
            DisplayTree.Nodes.Add(HeaderNode1)

            'Program
            If RadListBoxProgram2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Program"
                HeaderNode2.Value = "Program"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim ProgramNode As New RadTreeNode
                    ProgramNode.Text = item.Text
                    HeaderNode2.Nodes.Add(ProgramNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Program Version
            If RadListBoxProgramVersion2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "ProgramVersion"
                HeaderNode2.Value = "ProgramVersion"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxProgramVersion2.Items
                    Dim ProgramVersionNode As New RadTreeNode
                    ProgramVersionNode.Text = item.Text
                    HeaderNode2.Nodes.Add(ProgramVersionNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Term
            If RadListBoxTerm2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Terms"
                HeaderNode2.Value = "Terms"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxTerm2.Items
                    Dim CourseNode As New RadTreeNode
                    CourseNode.Text = item.Text
                    HeaderNode2.Nodes.Add(CourseNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Enrollment Status
            If RadListBoxEnrollmentStatus2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Enrollment Status"
                HeaderNode2.Value = "Enrollment Status"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxEnrollmentStatus2.Items
                    Dim CourseNode As New RadTreeNode
                    CourseNode.Text = item.Text
                    HeaderNode2.Nodes.Add(CourseNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Student Group
            If RadListBoxStudentGroup2.Items.Count > 0 Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Student Group"
                HeaderNode2.Value = "Student Group"
                HeaderNode2.CssClass = "TreeParentNode"
                For Each item As RadListBoxItem In RadListBoxStudentGroup2.Items
                    Dim CourseNode As New RadTreeNode
                    CourseNode.Text = item.Text
                    HeaderNode2.Nodes.Add(CourseNode)
                Next
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Cumulative GPA
            If Not String.IsNullOrEmpty(cumulativeGPA_GT.Text) Or Not String.IsNullOrEmpty(cumulativeGPA_LT.Text) _
                Or String.IsNullOrEmpty(radTermCredits_GT.DisplayText) Or Not String.IsNullOrEmpty(cumulativeGPA_LT.DisplayText) Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Cumulative GPA Range"
                HeaderNode2.Value = "Cumulative GPA Range"
                HeaderNode2.CssClass = "TreeParentNode"
                Dim CourseNode As New RadTreeNode
                If Not String.IsNullOrEmpty(cumulativeGPA_GT.Text) Then
                    CourseNode.Text = ">=" & cumulativeGPA_GT.Text.ToString()
                Else
                    CourseNode.Text = ">=" & cumulativeGPA_GT.DisplayText.ToString()
                End If
                If Not String.IsNullOrEmpty(cumulativeGPA_LT.Text) Then
                    If Not String.IsNullOrEmpty(cumulativeGPA_GT.Text) Then
                        CourseNode.Text &= " and "
                    End If
                    CourseNode.Text &= "<=" & cumulativeGPA_LT.Text.ToString()
                Else
                    If Not String.IsNullOrEmpty(cumulativeGPA_GT.Text) Then
                        CourseNode.Text &= " and "
                    End If
                    CourseNode.Text = "<=" & cumulativeGPA_LT.DisplayText.ToString()
                End If
                HeaderNode2.Nodes.Add(CourseNode)
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Term Credits
            If Not String.IsNullOrEmpty(radTermCredits_GT.Text) Or Not String.IsNullOrEmpty(radTermCredits_LT.Text) Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Term Credits GPA Range"
                HeaderNode2.Value = "Term Credits GPA Range"
                HeaderNode2.CssClass = "TreeParentNode"
                Dim CourseNode As New RadTreeNode
                If Not String.IsNullOrEmpty(radTermCredits_GT.Text) Then
                    CourseNode.Text = ">=" & radTermCredits_GT.Text.ToString()
                End If
                If Not String.IsNullOrEmpty(radTermCredits_LT.Text) Then
                    If Not String.IsNullOrEmpty(radTermCredits_GT.Text) Then
                        CourseNode.Text &= " and "
                    End If
                    CourseNode.Text &= "<=" & radTermCredits_LT.Text.ToString()
                End If
                HeaderNode2.Nodes.Add(CourseNode)
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            'Term GPA
            If Not String.IsNullOrEmpty(radTermGPA_GT.Text) Or Not String.IsNullOrEmpty(radTermGPA_LT.Text) Then
                Dim reportname As String = Page.Title
                Dim HeaderNode2 As New RadTreeNode
                HeaderNode2.Text = "Term GPA"
                HeaderNode2.Value = "Term GPA"
                HeaderNode2.CssClass = "TreeParentNode"
                Dim CourseNode As New RadTreeNode
                If Not String.IsNullOrEmpty(radTermGPA_GT.Text) Then
                    CourseNode.Text = ">=" & radTermGPA_GT.Text.ToString()
                End If
                If Not String.IsNullOrEmpty(radTermGPA_GT.Text) Then
                    If Not String.IsNullOrEmpty(radTermGPA_GT.Text) Then
                        CourseNode.Text &= " and "
                    End If
                    CourseNode.Text &= "<=" & radTermGPA_LT.Text.ToString()
                End If
                HeaderNode2.Nodes.Add(CourseNode)
                DisplayTree.Nodes.Add(HeaderNode2)
            End If

            Return DisplayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim UserSettings As New ParamItemUserSettingsInfo
        Dim SettingsCollection As New List(Of ControlSettingInfo)
        Try
            UserSettings.ItemName = ItemDetail.ItemName
            UserSettings.ItemId = ItemDetail.ItemId
            UserSettings.DetailId = ItemDetail.DetailId
            UserSettings.FriendlyName = ItemDetail.CaptionOverride

            'Campus
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo

            ctrlSettingCampus.ControlName = RadComboCampus.ID
            Dim selectedCampus As RadComboBoxItem = RadComboCampus.SelectedItem
            objControlValueInfo.DisplayText = selectedCampus.Text.Replace("'", "''")
            objControlValueInfo.KeyData = selectedCampus.Value.Replace("'", "''")
            ctrlValuesCampus.Add(objControlValueInfo)
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            SettingsCollection.Add(ctrlSettingCampus)

            'Program1
            'Dim ctrlSettingProgram As New ControlSettingInfo
            'Dim ctrlValuesProgram As New List(Of ControlValueInfo)
            'ctrlSettingProgram.ControlName = RadListBoxProgram1.ID
            'For Each item As RadListBoxItem In RadListBoxProgram1.Items
            '    Dim objControlValueInfo2 As New ControlValueInfo
            '    objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
            '    objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
            '    ctrlValuesProgram.Add(objControlValueInfo2)
            'Next
            'ctrlSettingProgram.ControlValueCollection = ctrlValuesProgram
            'SettingsCollection.Add(ctrlSettingProgram)

            'Program2
            Dim ctrlSettingProgram2 As New ControlSettingInfo
            Dim ctrlValuesProgram2 As New List(Of ControlValueInfo)
            ctrlSettingProgram2.ControlName = RadListBoxProgram2.ID
            For Each item As RadListBoxItem In RadListBoxProgram2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram2.Add(objControlValueInfo3)
            Next
            ctrlSettingProgram2.ControlValueCollection = ctrlValuesProgram2
            SettingsCollection.Add(ctrlSettingProgram2)


            'ProgramVersion1
            'Dim ctrlSettingProgramVersion As New ControlSettingInfo
            'Dim ctrlValuesProgramVersion As New List(Of ControlValueInfo)
            'ctrlSettingProgramVersion.ControlName = RadListBoxProgramVersion1.ID
            'For Each item As RadListBoxItem In RadListBoxProgramVersion1.Items
            '    Dim objControlValueInfo2 As New ControlValueInfo
            '    objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
            '    objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
            '    ctrlValuesProgramVersion.Add(objControlValueInfo2)
            'Next
            'ctrlSettingProgramVersion.ControlValueCollection = ctrlValuesProgramVersion
            'SettingsCollection.Add(ctrlSettingProgramVersion)

            'ProgramVersion2
            Dim ctrlSettingProgramVersion2 As New ControlSettingInfo
            Dim ctrlValuesProgramVersion2 As New List(Of ControlValueInfo)
            ctrlSettingProgramVersion2.ControlName = RadListBoxProgramVersion2.ID
            For Each item As RadListBoxItem In RadListBoxProgramVersion2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgramVersion2.Add(objControlValueInfo3)
            Next
            ctrlSettingProgramVersion2.ControlValueCollection = ctrlValuesProgramVersion2
            SettingsCollection.Add(ctrlSettingProgramVersion2)


            'Term2
            Dim ctrlSettingCourse2 As New ControlSettingInfo
            Dim ctrlValuesCourse2 As New List(Of ControlValueInfo)
            ctrlSettingCourse2.ControlName = RadListBoxTerm2.ID
            For Each item As RadListBoxItem In RadListBoxTerm2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesCourse2.Add(objControlValueInfo3)
            Next
            ctrlSettingCourse2.ControlValueCollection = ctrlValuesCourse2
            SettingsCollection.Add(ctrlSettingCourse2)
            UserSettings.ControlSettingsCollection = SettingsCollection


            'Enrollment Status 2
            Dim ctrlSettingStatus2 As New ControlSettingInfo
            Dim ctrlValuesStatus2 As New List(Of ControlValueInfo)
            ctrlSettingStatus2.ControlName = RadListBoxEnrollmentStatus2.ID
            For Each item As RadListBoxItem In RadListBoxEnrollmentStatus2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesStatus2.Add(objControlValueInfo3)
            Next
            ctrlSettingStatus2.ControlValueCollection = ctrlValuesStatus2
            SettingsCollection.Add(ctrlSettingStatus2)
            UserSettings.ControlSettingsCollection = SettingsCollection


            'Student Group 2
            Dim ctrlSettingStudentGroup2 As New ControlSettingInfo
            Dim ctrlValuesStudentGroup2 As New List(Of ControlValueInfo)
            ctrlSettingStudentGroup2.ControlName = RadListBoxStudentGroup2.ID
            For Each item As RadListBoxItem In RadListBoxStudentGroup2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesStudentGroup2.Add(objControlValueInfo3)
            Next
            ctrlSettingStudentGroup2.ControlValueCollection = ctrlValuesStudentGroup2
            SettingsCollection.Add(ctrlSettingStudentGroup2)
            UserSettings.ControlSettingsCollection = SettingsCollection

            'Cumulative GPA Greater than or equal to 
            Dim ctrlSettingCumulativeGpa As New ControlSettingInfo
            Dim objControlValueCumulativeGpa As New ControlValueInfo
            Dim ctrlValuesCumulativeGpa As New List(Of ControlValueInfo)
            ctrlSettingCumulativeGpa.ControlName = cumulativeGPA_GT.ID
            If String.IsNullOrEmpty(cumulativeGPA_GT.Text) Then
                objControlValueCumulativeGpa.DisplayText = cumulativeGPA_GT.DisplayText
                objControlValueCumulativeGpa.KeyData = "cumulativeGPA_GT"
                ctrlValuesCumulativeGpa.Add(objControlValueCumulativeGpa)
            Else
                objControlValueCumulativeGpa.DisplayText = cumulativeGPA_GT.Text
                objControlValueCumulativeGpa.KeyData = "cumulativeGPA_GT"
                ctrlValuesCumulativeGpa.Add(objControlValueCumulativeGpa)
            End If
            ctrlSettingCumulativeGpa.ControlValueCollection = ctrlValuesCumulativeGpa
            SettingsCollection.Add(ctrlSettingCumulativeGpa)
            UserSettings.ControlSettingsCollection = SettingsCollection

            'Cumulative GPA Less than or equal to 
            Dim ctrlSettingCumulativeGpaLt As New ControlSettingInfo
            Dim objControlValueCumulativeGpaLt As New ControlValueInfo
            Dim ctrlValuesCumulativeGpaLt As New List(Of ControlValueInfo)
            ctrlSettingCumulativeGpaLt.ControlName = cumulativeGPA_LT.ID
            If String.IsNullOrEmpty(cumulativeGPA_LT.Text) Then
                objControlValueCumulativeGpaLt.DisplayText = cumulativeGPA_LT.DisplayText
                objControlValueCumulativeGpaLt.KeyData = "cumulativeGPA_LT"
                ctrlValuesCumulativeGpaLt.Add(objControlValueCumulativeGpaLt)
            Else
                objControlValueCumulativeGpaLt.DisplayText = cumulativeGPA_LT.Text
                objControlValueCumulativeGpaLt.KeyData = "cumulativeGPA_LT"
                ctrlValuesCumulativeGpaLt.Add(objControlValueCumulativeGpaLt)
            End If
            ctrlSettingCumulativeGpaLt.ControlValueCollection = ctrlValuesCumulativeGpaLt
            SettingsCollection.Add(ctrlSettingCumulativeGpaLt)
            UserSettings.ControlSettingsCollection = SettingsCollection


            'Term Credits Greater than or equal to 
            Dim ctrlSettingTermCredits As New ControlSettingInfo
            Dim objControlValueTermCredits As New ControlValueInfo
            Dim ctrlValuesTermCredits As New List(Of ControlValueInfo)
            ctrlSettingTermCredits.ControlName = radTermCredits_GT.ID
            If String.IsNullOrEmpty(radTermCredits_GT.Text) Then
                objControlValueTermCredits.DisplayText = radTermCredits_GT.DisplayText
                objControlValueTermCredits.KeyData = "radTermCredits_GT"
                ctrlValuesTermCredits.Add(objControlValueTermCredits)
            Else
                objControlValueTermCredits.DisplayText = radTermCredits_GT.Text
                objControlValueTermCredits.KeyData = "radTermCredits_GT"
                ctrlValuesTermCredits.Add(objControlValueTermCredits)
            End If
            ctrlSettingTermCredits.ControlValueCollection = ctrlValuesTermCredits
            SettingsCollection.Add(ctrlSettingTermCredits)
            UserSettings.ControlSettingsCollection = SettingsCollection

            'Term Credits Less than or equal to 
            Dim ctrlSettingTermCreditsLt As New ControlSettingInfo
            Dim objControlValueTermCreditsLt As New ControlValueInfo
            Dim ctrlValuesTermCreditsLt As New List(Of ControlValueInfo)
            ctrlSettingTermCreditsLt.ControlName = radTermCredits_LT.ID
            If String.IsNullOrEmpty(radTermCredits_LT.Text) Then
                objControlValueTermCreditsLt.DisplayText = radTermCredits_LT.DisplayText
                objControlValueTermCreditsLt.KeyData = "radTermCredits_LT"
                ctrlValuesTermCreditsLt.Add(objControlValueTermCreditsLt)
            Else
                objControlValueTermCreditsLt.DisplayText = radTermCredits_LT.Text
                objControlValueTermCreditsLt.KeyData = "radTermCredits_LT"
                ctrlValuesTermCreditsLt.Add(objControlValueTermCreditsLt)
            End If
            ctrlSettingTermCreditsLt.ControlValueCollection = ctrlValuesTermCreditsLt
            SettingsCollection.Add(ctrlSettingTermCreditsLt)
            UserSettings.ControlSettingsCollection = SettingsCollection


            'Term Credits GPA Greater than or equal to 
            Dim ctrlSettingTermCreditsGpa As New ControlSettingInfo
            Dim objControlValueTermCreditsGpa As New ControlValueInfo
            Dim ctrlValuesTermCreditsGpa As New List(Of ControlValueInfo)
            ctrlSettingTermCreditsGpa.ControlName = radTermGPA_GT.ID
            If String.IsNullOrEmpty(radTermGPA_GT.Text) Then
                objControlValueTermCreditsGpa.DisplayText = radTermGPA_GT.DisplayText
                objControlValueTermCreditsGpa.KeyData = "radTermGPA_GT"
                ctrlValuesTermCreditsGpa.Add(objControlValueTermCreditsGpa)
            Else
                objControlValueTermCreditsGpa.DisplayText = radTermGPA_GT.Text
                objControlValueTermCreditsGpa.KeyData = "radTermGPA_GT"
                ctrlValuesTermCreditsGpa.Add(objControlValueTermCreditsGpa)
            End If
            ctrlSettingTermCreditsGpa.ControlValueCollection = ctrlValuesTermCreditsGpa
            SettingsCollection.Add(ctrlSettingTermCreditsGpa)
            UserSettings.ControlSettingsCollection = SettingsCollection

            'Term Credits Less than or equal to 
            Dim ctrlSettingTermCreditsGpaLt As New ControlSettingInfo
            Dim objControlValueTermCreditsGpaLt As New ControlValueInfo
            Dim ctrlValuesTermCreditsGpaLt As New List(Of ControlValueInfo)
            ctrlSettingTermCreditsGpaLt.ControlName = radTermGPA_LT.ID
            If String.IsNullOrEmpty(radTermGPA_LT.Text) Then
                objControlValueTermCreditsGpaLt.DisplayText = radTermGPA_LT.DisplayText
                objControlValueTermCreditsGpaLt.KeyData = "radTermGPA_LT"
                ctrlValuesTermCreditsGpaLt.Add(objControlValueTermCreditsGpaLt)
            Else
                objControlValueTermCreditsGpaLt.DisplayText = radTermGPA_LT.Text
                objControlValueTermCreditsGpaLt.KeyData = "radTermGPA_LT"
                ctrlValuesTermCreditsGpaLt.Add(objControlValueTermCreditsGpaLt)
            End If
            ctrlSettingTermCreditsGpaLt.ControlValueCollection = ctrlValuesTermCreditsGpaLt
            SettingsCollection.Add(ctrlSettingTermCreditsGpaLt)
            UserSettings.ControlSettingsCollection = SettingsCollection

            ''ShowGPAByTerm
            'Dim ctrlSettingShowGpaByTerm As New ControlSettingInfo
            'Dim objControlValueShowGpaByTerm As New ControlValueInfo
            'Dim ctrlValuesShowGpaByTerm As New List(Of ControlValueInfo)
            'ctrlSettingShowGpaByTerm.ControlName = chkShowGPAbyTerm.ID
            'If chkShowGPAbyTerm.Checked = False Then
            '    objControlValueShowGpaByTerm.DisplayText = "false"
            '    objControlValueShowGpaByTerm.KeyData = "false"
            '    ctrlValuesShowGpaByTerm.Add(objControlValueShowGpaByTerm)
            'Else
            '    objControlValueShowGpaByTerm.DisplayText = "true"
            '    objControlValueShowGpaByTerm.KeyData = "true"
            '    ctrlValuesShowGpaByTerm.Add(objControlValueShowGpaByTerm)
            'End If
            'ctrlSettingShowGpaByTerm.ControlValueCollection = ctrlValuesShowGpaByTerm
            'SettingsCollection.Add(ctrlSettingShowGpaByTerm)
            'UserSettings.ControlSettingsCollection = SettingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return UserSettings
    End Function
    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each Setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = Me.FindControl(Setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim RadioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                RadioButList.SelectedValue = ItemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim ComboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                If Not ItemValue.KeyData = "1900" Then
                                    ComboBox.SelectedValue = ItemValue.KeyData
                                End If
                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim LstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            LstBox.Items.Clear()
                            For Each ItemValue As ControlValueInfo In Setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = ItemValue.DisplayText
                                newItem.Value = ItemValue.KeyData
                                LstBox.SelectedValue = ItemValue.KeyData
                                LstBox.Items.Add(newItem)
                            Next
                            'ElseIf TypeOf ctrl Is CheckBox Then
                            '    For Each itemValue As ControlValueInfo In Setting.ControlValueCollection
                            '        If itemValue.KeyData.ToString.ToLower = "true" Then
                            '            chkShowGPAbyTerm.Checked = True
                            '        Else
                            '            chkShowGPAbyTerm.Checked = False
                            '        End If
                            '    Next
                        ElseIf TypeOf ctrl Is RadNumericTextBox Then
                            For Each itemValue As ControlValueInfo In Setting.ControlValueCollection
                                If itemValue.KeyData = "cumulativeGPA_GT" Then
                                    cumulativeGPA_GT.Text = itemValue.DisplayText
                                    cumulativeGPA_GT.DisplayText = itemValue.DisplayText
                                End If
                                If itemValue.KeyData = "cumulativeGPA_LT" Then
                                    cumulativeGPA_LT.Text = itemValue.DisplayText
                                    cumulativeGPA_LT.DisplayText = itemValue.DisplayText
                                End If
                                If itemValue.KeyData = "radTermCredits_GT" Then
                                    radTermCredits_GT.Text = itemValue.DisplayText
                                    radTermCredits_GT.DisplayText = itemValue.DisplayText
                                End If
                                If itemValue.KeyData = "radTermCredits_LT" Then
                                    radTermCredits_LT.Text = itemValue.DisplayText
                                    radTermCredits_LT.DisplayText = itemValue.DisplayText
                                End If
                                If itemValue.KeyData = "radTermGPA_GT" Then
                                    radTermGPA_GT.Text = itemValue.DisplayText
                                    radTermGPA_GT.DisplayText = itemValue.DisplayText
                                End If
                                If itemValue.KeyData = "radTermGPA_LT" Then
                                    radTermGPA_LT.Text = itemValue.DisplayText
                                    radTermGPA_LT.DisplayText = itemValue.DisplayText
                                End If
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadListBoxProgram1.Items.Clear()
                RadListBoxProgram2.Items.Clear()
                RadListBoxProgramVersion1.Items.Clear()
                RadListBoxProgramVersion2.Items.Clear()
                RadListBoxTerm1.Items.Clear()
                RadListBoxTerm2.Items.Clear()
            End If
            ListBoxCounts()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
#End Region
#Region "Set Properties"
    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next
            'this property as True make the report do not check when the filters selected is less that the requirement.
            FilterMode = True

            'GradeFormat to define Mininmun value and maximun values for Term GPA/Avg Range and Cumulative GPA/Average Range
            GradesFormat = MyAdvAppSettings.AppSettings("GradesFormat", RadComboCampus.SelectedValue).ToLower()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = Me.FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub

#End Region
#Region "Events"
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        Dim Lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arProgram Then
                Dim MyarProgram As arProgram = DirectCast(myItem.DataItem, arProgram)
                If MyarProgram.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = MyarProgram.ProgDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is arPrgVersion Then
                Dim myarPrgVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                Dim myarProgramVersion As arPrgVersion = DirectCast(myItem.DataItem, arPrgVersion)
                If myarProgramVersion.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarPrgVersion.PrgVerDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
#End Region


End Class
