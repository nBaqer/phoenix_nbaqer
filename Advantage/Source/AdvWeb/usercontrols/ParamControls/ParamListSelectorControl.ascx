﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamListSelectorControl.ascx.vb" Inherits="ParamListSelectorControl" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadListBox1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadPanelBarParamSet"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadListBox2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadPanelBarParamSet"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CheckBox1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadPanelBarParamSet"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<div id="MainContainer" class="MainContainer">
    <div class="MultiFilterReportContainer">

        <asp:Panel ID="ContainerPanel" runat="server" CssClass="MultiFilterReportContainer">
            <div id="captionlabel" class="CaptionLabel" runat="server"><%=Caption%></div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBox1" runat="server" Width="360px" Height="100px" CausesValidation="true"
                    OnInserted="RadListBoxes_OnInserted" OnReordered="RadListBoxes_OnReOrdered"
                    OnTransferred="RadListBoxes_OnTransferred" OnItemDataBound="RadListBoxes_ItemDataBound"
                    OnTransferring="RadListBoxes_OnTransferring"
                    OnClientTransferred="OnClientTransferredHandlerFiltersPage">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBox2" runat="server"
                    OnInserted="RadListBoxes_OnInserted"
                    OnReordered="RadListBoxes_OnReOrdered"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" CausesValidation="true">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" />
                    <span id="RadListBox1Counter" class="RadListBox1Counter">
                        <asp:Label ID="lblListBoxCounter1" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="RadListBox2Counter" class="RadListBox2Counter">
                        <asp:Label ID="lblListBoxCounter2" runat="server" Text=""></asp:Label>
                    </span>
                </div>
                <div id="RequiredFieldContainer" class="RequiredFieldContainer" runat="server">
                    <asp:CustomValidator ID="CustomValidator1" runat="server"
                        Enabled="False" OnServerValidate="ValidateRequired" Display="Dynamic"> 
                    </asp:CustomValidator>
                    <asp:CustomValidator ID="CustomValidator2" runat="server"
                        Enabled="False" OnServerValidate="ValidateSingleSelect"
                        Display="Dynamic"> 
                    </asp:CustomValidator>
                    <asp:Label ID="lblSingleSelectMsg" runat="server" Text="" Style="color: Red;"></asp:Label>
                </div>
                <div id="UserNameContainer" class="UserNameContainer">
                    <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </asp:Panel>
    </div>
</div>
<div class="ResetDiv">
</div>


