﻿Option Strict On
Imports Telerik.Web.UI
Imports FAME.Parameters.Info
Imports System.Reflection
Imports FAME.Parameters.Interfaces
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common

Partial Class ParamIPEDSMissingData
    Inherits UserControl
    Implements ICustomControl

    Public Enum IPEDSReportingType
        [ProgramReporter] = 0
        [AcademicYearReporter] = 1
    End Enum
    Public Enum InstitutionType
        [Public] = 0
        [Private] = 1
    End Enum
    Public Enum ControlType
        [Instructions] = 0
        [MissingData] = 1
        [TwelveMonthDetail] = 2
        [TwelveMonthSummary] = 3
        [BDetail] = 4
        [CDetail] = 5
        [BCSummary] = 6
        [COMDetail] = 7
        [COMSummary] = 8
        [Finance] = 9
        [GradRates4] = 10 'For GradRate 4 Year reports
        [FinAidA] = 11 ' FinAid Part A report
        [FinAidBCDE] = 12 'FinAid Part B,C,D,and E
        [GradRates2Years] = 13 'For GradRate 2 Year and less than two year report
        [FallEnrollment] = 14 ' For Fall Enrollment Reports
        [GradRates200Less2Yrs] = 15 'For GradRate 200 Less than 2 Years report
        [COMCIPDetailSumamry] = 16  'For Completion CIP Detail & Summary Report
        [COMAllCompletesDetailSumamry] = 17 ' For Completion All Completer Detail & Summary Report
        [COMCompletesByLevelDetailSumamry] = 18 ' For Completion Completer By level Detail & Summary Report
        [InstB3Summary] = 19
        [GEMissingData] = 20

        [None] = 999 'for testing only
    End Enum
#Region "Properties"
    Private _SqlConn As String
    'Public Shared connectionString As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString
    Private _StartDatePartProgram As String
    Private _StartDatePartAcademic As String
    Private _EndDatePartProgram As String
    Private _EndDatePartAcademic As String
    Private _SelectedDatePartProgram As String
    Private _SelectedDatePartAcademic As String

    Protected MyAdvAppSettings As AdvAppSettings
    Private ipedsControlType1 As ControlType
    Private itemDetail1 As ParameterDetailItemInfo

    Public Property ItemDetail As ParameterDetailItemInfo Implements ICustomControl.ItemDetail
        Get
            Return itemDetail1
        End Get
        Set(value As ParameterDetailItemInfo)
            itemDetail1 = value
        End Set
    End Property

    Public Property Caption() As String Implements ICustomControl.Caption
        Get
            'Return _Caption
            Return CType(Session("Caption_" & ID), String)
        End Get
        Set(ByVal Value As String)
            ' _Caption = Value
            Session("Caption_" & ID) = Value
        End Set
    End Property
    Public Property SqlConn() As String Implements ICustomControl.SqlConn
        Get
            Return _SqlConn
        End Get
        Set(ByVal Value As String)
            _SqlConn = Value
        End Set
    End Property

    Public Property SavedSettings As ParamItemUserSettingsInfo Implements ICustomControl.SavedSettings

    Public Property IPEDSControlType As ControlType
        Get
            Return ipedsControlType1
        End Get
        Set(value As ControlType)
            ipedsControlType1 = value
        End Set
    End Property

    Public Property StudentIdentifier As String

    Public Property ProgramReporterStart As String

    Public Property ProgramReporterEnd As String

    Public Property AcademicYearStart As String

    Public Property AcademicYearEnd As String

    Public Property StartDatePartProgram() As String
        Get
            If String.IsNullOrWhiteSpace(_StartDatePartProgram) Then
                _StartDatePartProgram = "08/01/"
            End If
            Return _StartDatePartProgram
        End Get
        Set(ByVal value As String)
            _StartDatePartProgram = value
        End Set
    End Property

    Public Property StartDatePartAcademic() As String
        Get
            If String.IsNullOrWhiteSpace(_StartDatePartAcademic) Then
                _StartDatePartAcademic = "09/01/"
            End If
            Return _StartDatePartAcademic
        End Get
        Set(ByVal value As String)
            _StartDatePartAcademic = value
        End Set
    End Property

    Public Property EndDatePartProgram() As String
        Get
            If String.IsNullOrWhiteSpace(_EndDatePartProgram) Then
                _EndDatePartProgram = "10/31/"
            End If
            Return _EndDatePartProgram
        End Get
        Set(ByVal value As String)
            _EndDatePartProgram = value
        End Set
    End Property

    Public Property EndDatePartAcademic() As String
        Get
            If String.IsNullOrWhiteSpace(_EndDatePartAcademic) Then
                _EndDatePartAcademic = "12/15/"
            End If
            Return _EndDatePartAcademic
        End Get
        Set(ByVal value As String)
            _EndDatePartAcademic = value
        End Set
    End Property
    Public Property SelectedDatePartProgram() As String
        Get
            If String.IsNullOrWhiteSpace(_SelectedDatePartProgram) Then
                _SelectedDatePartProgram = "08/31/"
            End If
            Return _SelectedDatePartProgram
        End Get
        Set(ByVal value As String)
            _SelectedDatePartProgram = value
        End Set
    End Property

    Public Property SelectedDatePartAcademic() As String
        Get
            If String.IsNullOrWhiteSpace(_SelectedDatePartAcademic) Then
                _SelectedDatePartAcademic = "10/15/"
            End If
            Return _SelectedDatePartAcademic
        End Get
        Set(ByVal value As String)
            _SelectedDatePartAcademic = value
        End Set
    End Property
#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

       MyAdvAppSettings = AdvAppSettings.GetAppSettings()
        SetProperties()
        'IPEDSControlType = ControlType.MissingData
        If IPEDSControlType <> 20 Then Throw New ArgumentException("This should only work for Report Gainful Student with missing data report (ControlType 20)")
        If Not Page.IsPostBack Then
            FillCampusControl()
            RadComboCampus.SelectedValue = HttpContext.Current.Request.Params("cmpid").ToString
            FillProgramControls()
            SetControlDefaults()
        End If
        'FillProgramControls()
        FillAreasofInterestControl()
        ListBoxCounts()
    End Sub
    Protected Sub ReportingYearComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        'If IPEDSControlType = ControlType.COMCIPDetailSumamry Or IPEDSControlType = ControlType.COMAllCompletesDetailSumamry Or IPEDSControlType = ControlType.COMCompletesByLevelDetailSumamry Then
        '    lblDisplayReportDate.Text = "Report Date: " & "07/01/" & Mid(RadComboBoxReportingYear.SelectedItem.Text, 1, 4) & " thru 06/30/" & CStr(CInt(Mid(RadComboBoxReportingYear.SelectedItem.Text, 1, 4)) + 1)
        'End If
    End Sub
    Protected Sub CohortYearComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        Try
            CreateReporterDateRange(CType(rblSchoolType.SelectedIndex, IPEDSReportingType), IPEDSControlType)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
            'Dim x As String = ex.ToString
        End Try
    End Sub
    Protected Sub CampusComboBox_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs)
        RadListBoxProgram.Items.Clear()
        RadListBoxProgram2.Items.Clear()
        FillProgramControls()
        ListBoxCounts()
    End Sub
    Protected Sub CampusComboBox_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        Dim myItem As RadComboBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is syCampus Then
                Dim mysyCampus As syCampus = DirectCast(myItem.DataItem, syCampus)
                If mysyCampus.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = mysyCampus.CampDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    Protected Sub RadListBoxes_OnTransferred(ByVal sender As Object, ByVal e As RadListBoxTransferredEventArgs)
        Dim LBox As RadListBox = DirectCast(sender, RadListBox)
        If LBox.ID.Contains("Program") Then
            FillProgramControls()
        ElseIf LBox.ID.Contains("AreasOfInterest") Then
            FillAreasofInterestControl()
        End If
        ListBoxCounts()
    End Sub
    Protected Sub CheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim chkBox As CheckBox = DirectCast(sender, CheckBox)
        If chkBox.ID.Contains("Program") Then
            FillProgramControls()
        ElseIf chkBox.ID.Contains("AreasOfInterest") Then
            FillAreasofInterestControl()
        End If
        ListBoxCounts()
    End Sub
    Protected Sub RadListBoxes_ItemDataBound(ByVal sender As Object, ByVal e As RadListBoxItemEventArgs)
        'Dim Lbox As RadListBox = DirectCast(sender, RadListBox)
        Dim myItem As RadListBoxItem = e.Item
        Dim myDataItem As Object = myItem.DataItem
        Try
            If TypeOf myDataItem Is arProgram Then
                Dim myarProgram As arProgram = DirectCast(myItem.DataItem, arProgram)
                If myarProgram.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarProgram.ProgDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            ElseIf TypeOf myDataItem Is arPrgGrp Then
                Dim myarPrgGrp As arPrgGrp = DirectCast(myItem.DataItem, arPrgGrp)
                If myarPrgGrp.StatusId = New Guid("1AF592A6-8790-48EC-9916-5412C25EF49F") Then
                    myItem.CssClass = "InactiveListBoxText"
                    myItem.ToolTip = myarPrgGrp.PrgGrpDescrip & " is Inactive"
                    myItem.Text = myItem.Text + " (Inactive)"
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Protected Sub rblSchoolType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        If rblSchoolType.SelectedValue = "Academic" Then
            If Not IPEDSControlType = ControlType.GradRates2Years Or IPEDSControlType = ControlType.GradRates4 Or IPEDSControlType = ControlType.GradRates200Less2Yrs Then
                AcademicYearMsg.Visible = True
            Else
                AcademicYearMsg.Visible = False
            End If
            EnableReportDate(True)
           
            If IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
                LargestProgramText.Visible = False
                RadComboLProgram.Visible = False
                ProgramSelector.Visible = True
            End If
        Else
            Dim reportname As String = Page.Title
            If IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
                If reportname.Contains("Part A") Then
                    RadComboLProgram.Visible = True
                    LargestProgramText.Visible = True
                    ProgramSelector.Visible = True
                End If
                If reportname.Contains("Part D") _
                    Or reportname.Contains("Part E") Then
                    RadComboLProgram.Visible = True
                    LargestProgramText.Visible = True
                    ProgramSelector.Visible = False
                End If
            End If
            AcademicYearMsg.Visible = False
            EnableReportDate(False)
            radComboBoxInstitutionType.Visible = False
            radComboBoxInstitutionTypeLabel.Visible = False
        End If
        HideShowInstitutionType()
        CreateReporterDateRange(CType(rblSchoolType.SelectedIndex, IPEDSReportingType), IPEDSControlType)
    End Sub
#End Region
#Region "Methods"
    Public Sub FillCampusControl()
        Try
            RadComboCampus.DataTextField = "CampDescrip"
            RadComboCampus.DataValueField = "CampusId"
            RadComboCampus.DataSource = GetCampusesByUserId()
            RadComboCampus.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Public Sub FillProgramControls()
        Try
            Dim selectedCampus As Guid = New Guid(RadComboCampus.SelectedValue)
            RadListBoxProgram.DataTextField = "ProgDescrip"
            RadListBoxProgram.DataValueField = "ProgId"
            RadListBoxProgram.DataSource = GetProgramsByCampusId(selectedCampus, True)
            RadListBoxProgram.DataBind()


            RadComboLProgram.DataTextField = "ProgDescrip"
            RadComboLProgram.DataValueField = "ProgId"
            RadComboLProgram.DataSource = GetProgramsByCampusId(selectedCampus, False)
            RadComboLProgram.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Public Sub FillAreasofInterestControl()
        Try
            RadListBoxAreasOfInterest.DataTextField = "PrgGrpDescrip"
            RadListBoxAreasOfInterest.DataValueField = "PrgGrpId"
            RadListBoxAreasOfInterest.DataSource = GetAreasOfInterest()
            RadListBoxAreasOfInterest.DataBind()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Function GetCurrentlySelectedPrograms() As List(Of String)
        Dim selectedPrograms As New List(Of String)
        Try
            If RadListBoxProgram2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxProgram2.Items
                    Dim selected As String = item.Value
                    selectedPrograms.Add(selected)
                Next
                Return selectedPrograms
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetCurrentlySelectedAreasOfInterest() As List(Of String)
        Dim selectedAreasOfInterest As New List(Of String)
        Try
            If RadListBoxAreasOfInterest2.Items.Count > 0 Then
                For Each item As RadListBoxItem In RadListBoxAreasOfInterest2.Items
                    Dim selected As String = item.Value
                    selectedAreasOfInterest.Add(selected)
                Next
                Return selectedAreasOfInterest
            Else
                Return Nothing
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetCampusesByUserId() As List(Of syCampus)
        Dim da As New CampusDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As List(Of syCampus)
        Try
            result = da.GetCampusesByUserId(New Guid(AdvantageSession.UserState.UserId.ToString))
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetAreasOfInterest() As List(Of arPrgGrp)
        Dim DA As New ProgramGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As List(Of arPrgGrp)
        Try
            Dim selectedAreasOfInterest As List(Of String) = GetCurrentlySelectedAreasOfInterest()
            If selectedAreasOfInterest Is Nothing Then
                result = DA.GetProgramGroups(GetStatusFilters("cbkInactiveAreasOfInterest"))
            Else
                result = DA.GetProgramGroups(GetStatusFilters("cbkInactiveAreasOfInterest"), selectedAreasOfInterest)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Private Function GetProgramsByCampusId(ByVal selectedCampus As Guid, ByVal filteredBySelected As Boolean) As List(Of arProgram)
        Dim DA As New ProgramDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim result As List(Of arProgram)
        Try
            Dim selectedPrograms As List(Of String) = GetCurrentlySelectedPrograms()
            If selectedPrograms Is Nothing Or FilteredBySelected = False Then
                result = DA.GetProgramsByCampusId(selectedCampus, GetStatusFilters("cbkInactivePrograms"))
            Else
                result = DA.GetProgramsByCampusId(selectedCampus, GetStatusFilters("cbkInactivePrograms"), selectedPrograms)
            End If
            Return result
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Function GetStatusFilters(ByVal ctrlName As String) As List(Of String)
        Dim statusValues As New List(Of String)

        Try
            Dim cKbox As CheckBox = DirectCast(FindControl(ctrlName), CheckBox)
            If cKbox.Checked = True Then
                statusValues.Add("Active")
                statusValues.Add("Inactive")
            Else
                statusValues.Add("Active")
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        Return statusValues
    End Function
    Public Function GetDisplayData() As Control Implements ICustomControl.GetDisplayData
        Dim displayTree As New RadTreeView
        displayTree.ID = ItemDetail.ItemName & "_DisplaySelected"
        Try
            Dim headerNode1 As New RadTreeNode
            headerNode1.Text = "Campus"
            headerNode1.Value = "Campus"
            headerNode1.CssClass = "TreeParentNode"
            Dim campusNode As New RadTreeNode
            campusNode.Text = RadComboCampus.SelectedItem.Text
            headerNode1.Nodes.Add(campusNode)
            displayTree.Nodes.Add(headerNode1)
            If Not IPEDSControlType = ControlType.Instructions Then
                If RadListBoxProgram2.Items.Count > 0 Then
                    'Dim reportname As String = Page.Title

                    Dim headerNode2 As New RadTreeNode
                    headerNode2.Text = "Program"
                    headerNode2.Value = "Program"
                    headerNode2.CssClass = "TreeParentNode"
                    For Each item As RadListBoxItem In RadListBoxProgram2.Items
                        Dim programNode As New RadTreeNode
                        programNode.Text = item.Text
                        headerNode2.Nodes.Add(programNode)
                    Next
                    displayTree.Nodes.Add(headerNode2)

                End If
                If RadListBoxAreasOfInterest2.Items.Count > 0 Then
                    Dim headerNode3 As New RadTreeNode
                    headerNode3.Text = "Area Of Interest"
                    headerNode3.Value = "Area Of Interest"
                    headerNode3.CssClass = "TreeParentNode"
                    For Each item As RadListBoxItem In RadListBoxAreasOfInterest2.Items
                        Dim interestNode As New RadTreeNode
                        interestNode.Text = item.Text
                        headerNode3.Nodes.Add(interestNode)
                    Next
                    displayTree.Nodes.Add(headerNode3)
                End If
                'If IPEDSControlType = ControlType.BDetail Or _
                '    IPEDSControlType = ControlType.CDetail Or _
                '    IPEDSControlType = ControlType.BCSummary Or _
                '    IPEDSControlType = ControlType.GradRates4 _
                '     Or IPEDSControlType = ControlType.GradRates2Years _
                '     Or IPEDSControlType = ControlType.GradRates200Less2Yrs _
                '    Or IPEDSControlType = ControlType.FinAidA _
                '    Or IPEDSControlType = ControlType.FinAidBCDE _
                '    Or IPEDSControlType = ControlType.FallEnrollment _
                '    Or IPEDSControlType = ControlType.InstB3Summary Then
                '    Dim HeaderNode6 As New RadTreeNode
                '    Dim CohortYearNode As New RadTreeNode
                '    HeaderNode6.Text = "Cohort Year"
                '    HeaderNode6.Value = "Cohort Year"
                '    HeaderNode6.CssClass = "TreeParentNode"
                '    'CohortYearNode.Text = RadComboBoxCohortYear.SelectedItem.Text
                '    HeaderNode6.Nodes.Add(CohortYearNode)
                '    displayTree.Nodes.Add(HeaderNode6)
                'End If
                'If IPEDSControlType = ControlType.COMSummary _
                '    Or IPEDSControlType = ControlType.COMDetail _
                '    Or IPEDSControlType = ControlType.COMCIPDetailSumamry _
                '    Or IPEDSControlType = ControlType.COMAllCompletesDetailSumamry _
                '    Or IPEDSControlType = ControlType.COMCompletesByLevelDetailSumamry Then
                '    Dim HeaderNode7 As New RadTreeNode
                '    Dim ReportingYearNode As New RadTreeNode
                '    HeaderNode7.Text = "Reporting Year"
                '    HeaderNode7.Value = "Reporting Year"
                '    HeaderNode7.CssClass = "TreeParentNode"
                '    ReportingYearNode.Text = RadComboBoxReportingYear.SelectedItem.Text
                '    HeaderNode7.Nodes.Add(ReportingYearNode)
                '    DisplayTree.Nodes.Add(HeaderNode7)
                'End If
                'If IPEDSControlType = ControlType.BDetail Or _
                '    IPEDSControlType = ControlType.CDetail _
                '   Or IPEDSControlType = ControlType.COMDetail _
                '   Or IPEDSControlType = ControlType.TwelveMonthDetail _
                '    Or IPEDSControlType = ControlType.MissingData _
                '      Or IPEDSControlType = ControlType.GradRates4 _
                '     Or IPEDSControlType = ControlType.GradRates2Years _
                '     Or IPEDSControlType = ControlType.GradRates200Less2Yrs _
                '    Or IPEDSControlType = ControlType.FinAidA _
                '     Or IPEDSControlType = ControlType.FinAidBCDE _
                '     Or IPEDSControlType = ControlType.FallEnrollment _
                '      Or IPEDSControlType = ControlType.COMCIPDetailSumamry _
                '      Or IPEDSControlType = ControlType.COMAllCompletesDetailSumamry _
                '      Or IPEDSControlType = ControlType.GEMissingData _
                '      Or IPEDSControlType = ControlType.COMCompletesByLevelDetailSumamry Then
                '    Dim HeaderNode8 As New RadTreeNode
                '    HeaderNode8.Text = "Sort Options"
                '    HeaderNode8.Value = "Sort Options"
                '    HeaderNode8.CssClass = "TreeParentNode"
                '    Dim SortOptionsNode As New RadTreeNode
                '    SortOptionsNode.Text = rblSortBy.SelectedItem.Text
                '    HeaderNode8.Nodes.Add(SortOptionsNode)
                '    displayTree.Nodes.Add(HeaderNode8)
                'End If
                If IPEDSControlType = ControlType.BDetail Or _
                    IPEDSControlType = ControlType.CDetail _
                   Or IPEDSControlType = ControlType.COMDetail _
                   Or IPEDSControlType = ControlType.BCSummary _
                    Or IPEDSControlType = ControlType.GradRates4 _
                     Or IPEDSControlType = ControlType.GradRates2Years _
                     Or IPEDSControlType = ControlType.GradRates200Less2Yrs _
                   Or IPEDSControlType = ControlType.FinAidA _
                    Or IPEDSControlType = ControlType.FinAidBCDE _
                    Or IPEDSControlType = ControlType.FallEnrollment _
                    Or IPEDSControlType = ControlType.InstB3Summary Then  '
                    ' DE8446 - School Reporting whats to be hidden
                    '   Or IPEDSControlType = ControlType.COMCIPDetailSumamry _
                    '   Or IPEDSControlType = ControlType.COMAllCompletesDetailSumamry _
                    '   Or IPEDSControlType = ControlType.COMCompletesByLevelDetailSumamry Then
                    Dim headerNode9 As New RadTreeNode
                    If IPEDSControlType = ControlType.GradRates4 _
                     Or IPEDSControlType = ControlType.GradRates2Years _
                     Or IPEDSControlType = ControlType.GradRates200Less2Yrs Then
                        headerNode9.Text = "Cohort"
                        headerNode9.Value = "Cohort"
                    Else
                        headerNode9.Text = "School Reporting Type"
                        headerNode9.Value = "School Reporting Type"
                    End If
                    headerNode9.CssClass = "TreeParentNode"
                    Dim schoolReportingTypeNode As New RadTreeNode
                    schoolReportingTypeNode.Text = rblSchoolType.SelectedItem.Text
                    headerNode9.Nodes.Add(schoolReportingTypeNode)
                    displayTree.Nodes.Add(headerNode9)
                End If
                If IPEDSControlType = ControlType.FinAidA _
                     Or IPEDSControlType = ControlType.FinAidBCDE Then
                    If rblSchoolType.SelectedIndex = 1 Then
                        Dim headerNode10 As New RadTreeNode
                        headerNode10.Text = "Control Of Institution"
                        headerNode10.Value = "Control Of Institution"
                        headerNode10.CssClass = "TreeParentNode"
                        Dim controlOfInstitutionNode As New RadTreeNode
                        controlOfInstitutionNode.Text = radComboBoxInstitutionType.SelectedItem.Text
                        headerNode10.Nodes.Add(controlOfInstitutionNode)
                        displayTree.Nodes.Add(headerNode10)
                    End If
                End If
                If IPEDSControlType = ControlType.FinAidA _
                    Or IPEDSControlType = ControlType.FinAidBCDE Then
                    ' If rblSchoolType.SelectedIndex = 0 Then
                    Dim reportname As String = Page.Title
                    If reportname.Contains("Part A") Or reportname.Contains("Part D") _
                           Or reportname.Contains("Part E") Then
                        If rblSchoolType.SelectedValue = "Program" Then
                            Dim headerNode11 As New RadTreeNode
                            headerNode11.Text = "Largest Program"
                            headerNode11.Value = "Largest Program"
                            headerNode11.CssClass = "TreeParentNode"
                            Dim largestProgramNode As New RadTreeNode
                            largestProgramNode.Text = RadComboLProgram.SelectedItem.Text
                            headerNode11.Nodes.Add(largestProgramNode)
                            displayTree.Nodes.Add(headerNode11)
                        End If
                    End If
                    'End If
                End If
            End If
            Return displayTree
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Function
    Public Function GetControlSettings() As ParamItemUserSettingsInfo Implements ICustomControl.GetControlSettings
        Dim userSettings As New ParamItemUserSettingsInfo
        Dim settingsCollection As New List(Of ControlSettingInfo)
        Try
            userSettings.ItemName = ItemDetail.ItemName
            userSettings.ItemId = ItemDetail.ItemId
            userSettings.DetailId = ItemDetail.DetailId
            userSettings.FriendlyName = ItemDetail.CaptionOverride

            'Campus
            Dim ctrlSettingCampus As New ControlSettingInfo
            Dim ctrlValuesCampus As New List(Of ControlValueInfo)
            Dim objControlValueInfo As New ControlValueInfo

            ctrlSettingCampus.ControlName = RadComboCampus.ID
            Dim selectedCampus As RadComboBoxItem = RadComboCampus.SelectedItem
            objControlValueInfo.DisplayText = selectedCampus.Text.Replace("'", "''")
            objControlValueInfo.KeyData = selectedCampus.Value.Replace("'", "''")
            ctrlValuesCampus.Add(objControlValueInfo)
            ctrlSettingCampus.ControlValueCollection = ctrlValuesCampus
            settingsCollection.Add(ctrlSettingCampus)

            'Program1
            Dim ctrlSettingProgram As New ControlSettingInfo
            Dim ctrlValuesProgram As New List(Of ControlValueInfo)
            ctrlSettingProgram.ControlName = RadListBoxProgram.ID
            For Each item As RadListBoxItem In RadListBoxProgram.Items
                Dim objControlValueInfo2 As New ControlValueInfo
                objControlValueInfo2.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo2.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram.Add(objControlValueInfo2)
            Next
            ctrlSettingProgram.ControlValueCollection = ctrlValuesProgram
            settingsCollection.Add(ctrlSettingProgram)
            'Program2
            Dim ctrlSettingProgram2 As New ControlSettingInfo
            Dim ctrlValuesProgram2 As New List(Of ControlValueInfo)


            ctrlSettingProgram2.ControlName = RadListBoxProgram2.ID
            For Each item As RadListBoxItem In RadListBoxProgram2.Items
                Dim objControlValueInfo3 As New ControlValueInfo
                objControlValueInfo3.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo3.KeyData = item.Value.Replace("'", "''")
                ctrlValuesProgram2.Add(objControlValueInfo3)
            Next
            ctrlSettingProgram2.ControlValueCollection = ctrlValuesProgram2
            settingsCollection.Add(ctrlSettingProgram2)



            'LargestProgram
            Dim ctrlSettingLargestProgram As New ControlSettingInfo
            Dim ctrlValuesLargestProgram As New List(Of ControlValueInfo)
            Dim objControlValueInfoLP As New ControlValueInfo
            ctrlSettingLargestProgram.ControlName = RadComboLProgram.ID
            Dim selectedLargeProgram As RadComboBoxItem = RadComboLProgram.SelectedItem
            objControlValueInfoLP.DisplayText = selectedLargeProgram.Text.Replace("'", "''")
            objControlValueInfoLP.KeyData = selectedLargeProgram.Value.Replace("'", "''")

            ctrlValuesLargestProgram.Add(objControlValueInfoLP)
            ctrlSettingLargestProgram.ControlValueCollection = ctrlValuesLargestProgram
            settingsCollection.Add(ctrlSettingLargestProgram)

            'AreasOfInterest1
            Dim ctrlSettingAreasOfInterest As New ControlSettingInfo
            Dim ctrlValuesAreasOfInterest As New List(Of ControlValueInfo)


            ctrlSettingAreasOfInterest.ControlName = RadListBoxAreasOfInterest.ID
            For Each item As RadListBoxItem In RadListBoxAreasOfInterest.Items
                Dim objControlValueInfo4 As New ControlValueInfo
                objControlValueInfo4.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo4.KeyData = item.Value.Replace("'", "''")
                ctrlValuesAreasOfInterest.Add(objControlValueInfo4)
            Next

            ctrlSettingAreasOfInterest.ControlValueCollection = ctrlValuesAreasOfInterest
            settingsCollection.Add(ctrlSettingAreasOfInterest)
            'AreasOfInterest2
            Dim ctrlSettingAreasOfInterest2 As New ControlSettingInfo
            Dim ctrlValuesAreasOfInterest2 As New List(Of ControlValueInfo)


            ctrlSettingAreasOfInterest2.ControlName = RadListBoxAreasOfInterest2.ID
            For Each item As RadListBoxItem In RadListBoxAreasOfInterest2.Items
                Dim objControlValueInfo5 As New ControlValueInfo
                objControlValueInfo5.DisplayText = item.Text.Replace("'", "''")
                objControlValueInfo5.KeyData = item.Value.Replace("'", "''")
                ctrlValuesAreasOfInterest2.Add(objControlValueInfo5)
            Next
            ctrlSettingAreasOfInterest2.ControlValueCollection = ctrlValuesAreasOfInterest2
            settingsCollection.Add(ctrlSettingAreasOfInterest2)

            'CohortYear
            Dim ctrlSettingCohortYear As New ControlSettingInfo
            Dim ctrlValuesCohortYear As New List(Of ControlValueInfo)
            Dim objControlValueInfo6 As New ControlValueInfo

            'ctrlSettingCohortYear.ControlName = RadComboBoxCohortYear.ID

            'If RadComboBoxCohortYear.SelectedItem Is Nothing Then
            '    objControlValueInfo6.DisplayText = "1900"
            '    objControlValueInfo6.KeyData = "1900"
            'Else
            '    Dim selectedCohortYear As RadComboBoxItem = RadComboBoxCohortYear.SelectedItem
            '    objControlValueInfo6.DisplayText = selectedCohortYear.Text.Replace("'", "''")
            '    objControlValueInfo6.KeyData = selectedCohortYear.Value.Replace("'", "''")
            'End If

            ctrlValuesCohortYear.Add(objControlValueInfo6)
            ctrlSettingCohortYear.ControlValueCollection = ctrlValuesCohortYear
            settingsCollection.Add(ctrlSettingCohortYear)

            userSettings.ControlSettingsCollection = settingsCollection
            'ReportingYear
            Dim ctrlSettingReportingYear As New ControlSettingInfo
            Dim ctrlValuesReportingYear As New List(Of ControlValueInfo)
            Dim objControlValueInfo7 As New ControlValueInfo

            'ctrlSettingReportingYear.ControlName = RadComboBoxReportingYear.ID
            'If RadComboBoxReportingYear.SelectedItem Is Nothing Then
            '    objControlValueInfo7.DisplayText = "1900"
            '    objControlValueInfo7.KeyData = "1900"
            'Else
            '    Dim selectedReportingYear As RadComboBoxItem = RadComboBoxReportingYear.SelectedItem
            '    objControlValueInfo7.DisplayText = selectedReportingYear.Text.Replace("'", "''")
            '    objControlValueInfo7.KeyData = selectedReportingYear.Value.Replace("'", "''")
            'End If


            ctrlValuesReportingYear.Add(objControlValueInfo7)
            ctrlSettingReportingYear.ControlValueCollection = ctrlValuesReportingYear
            settingsCollection.Add(ctrlSettingReportingYear)

            'School Reporting Type
            Dim ctrlSettingSchoolReportingType As New ControlSettingInfo
            Dim ctrlValuesSchoolReportingType As New List(Of ControlValueInfo)
            Dim objControlValueInfo8 As New ControlValueInfo

            ctrlSettingSchoolReportingType.ControlName = rblSchoolType.ID
            objControlValueInfo8.DisplayText = rblSchoolType.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo8.KeyData = rblSchoolType.SelectedValue.Replace("'", "''")

            ctrlValuesSchoolReportingType.Add(objControlValueInfo8)
            ctrlSettingSchoolReportingType.ControlValueCollection = ctrlValuesSchoolReportingType
            settingsCollection.Add(ctrlSettingSchoolReportingType)

            'Sort Options
            Dim ctrlSettingSortOptions As New ControlSettingInfo
            Dim ctrlValuesSortOptions As New List(Of ControlValueInfo)
            Dim objControlValueInfo9 As New ControlValueInfo

            'ctrlSettingSortOptions.ControlName = rblSortBy.ID
            'objControlValueInfo9.DisplayText = rblSortBy.SelectedItem.Text.Replace("'", "''")
            'objControlValueInfo9.KeyData = rblSortBy.SelectedValue.Replace("'", "''")

            ctrlValuesSortOptions.Add(objControlValueInfo9)
            ctrlSettingSortOptions.ControlValueCollection = ctrlValuesSortOptions
            settingsCollection.Add(ctrlSettingSortOptions)

            'Report Date
            Dim ctrlSettingReportDate As New ControlSettingInfo
            Dim ctrlValuesReportDate As New List(Of ControlValueInfo)
            Dim objControlValueInfo10 As New ControlValueInfo

            'ctrlSettingReportDate.ControlName = RadDateReportDate.ID
            'If RadDateReportDate.SelectedDate Is Nothing Then
            '    objControlValueInfo10.DisplayText = "01/01/1900"
            '    objControlValueInfo10.KeyData = "01/01/1900"
            'Else
            '    objControlValueInfo10.DisplayText = RadDateReportDate.SelectedDate.ToString
            '    objControlValueInfo10.KeyData = RadDateReportDate.SelectedDate.ToString
            'End If
            ctrlValuesReportDate.Add(objControlValueInfo10)
            ctrlSettingReportDate.ControlValueCollection = ctrlValuesReportDate
            settingsCollection.Add(ctrlSettingReportDate)

            'PriorTo
            Dim ctrlSettingPriorTo As New ControlSettingInfo
            Dim ctrlValuesPriorTo As New List(Of ControlValueInfo)
            Dim objControlValueInfo11 As New ControlValueInfo

            'ctrlSettingPriorTo.ControlName = RadDatePriorTo.ID
            'If RadDatePriorTo.SelectedDate Is Nothing Then
            '    objControlValueInfo11.DisplayText = "01/01/1900"
            '    objControlValueInfo11.KeyData = "01/01/1900"
            'Else
            '    objControlValueInfo11.DisplayText = RadDatePriorTo.SelectedDate.ToString
            '    objControlValueInfo11.KeyData = RadDatePriorTo.SelectedDate.ToString
            'End If
            ctrlValuesPriorTo.Add(objControlValueInfo11)
            ctrlSettingPriorTo.ControlValueCollection = ctrlValuesPriorTo
            settingsCollection.Add(ctrlSettingPriorTo)

            'Institution Type
            Dim ctrlSettingInstitutionType As New ControlSettingInfo
            Dim ctrlValuesInstitutionType As New List(Of ControlValueInfo)
            Dim objControlValueInfo12 As New ControlValueInfo

            ctrlSettingInstitutionType.ControlName = radComboBoxInstitutionType.ID
            objControlValueInfo12.DisplayText = radComboBoxInstitutionType.SelectedItem.Text.Replace("'", "''")
            objControlValueInfo12.KeyData = radComboBoxInstitutionType.SelectedValue.Replace("'", "''")

            ctrlValuesInstitutionType.Add(objControlValueInfo12)
            ctrlSettingInstitutionType.ControlValueCollection = ctrlValuesInstitutionType
            settingsCollection.Add(ctrlSettingInstitutionType)


            userSettings.ControlSettingsCollection = settingsCollection
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
        Return userSettings
    End Function
    Private Sub LoadSavedReportSettings() Implements ICustomControl.LoadSavedReportSettings
        Try
            If Not SavedSettings Is Nothing Then
                For Each setting As ControlSettingInfo In SavedSettings.ControlSettingsCollection
                    Dim ctrl As Control = FindControl(setting.ControlName)
                    If Not ctrl Is Nothing Then
                        If TypeOf ctrl Is RadioButtonList Then
                            Dim radioButList As RadioButtonList = DirectCast(ctrl, RadioButtonList)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                radioButList.SelectedValue = itemValue.KeyData
                            Next
                        ElseIf TypeOf ctrl Is RadComboBox Then
                            Dim comboBox As RadComboBox = DirectCast(ctrl, RadComboBox)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                If Not itemValue.KeyData = "1900" Then
                                    comboBox.SelectedValue = itemValue.KeyData
                                End If

                            Next
                        ElseIf TypeOf ctrl Is RadListBox Then
                            Dim lstBox As RadListBox = DirectCast(ctrl, RadListBox)
                            lstBox.Items.Clear()
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                Dim newItem As New RadListBoxItem
                                newItem.Text = itemValue.DisplayText
                                newItem.Value = itemValue.KeyData
                                lstBox.SelectedValue = itemValue.KeyData
                                lstBox.Items.Add(newItem)
                            Next
                        ElseIf TypeOf ctrl Is RadDatePicker Then
                            Dim dPicker As RadDatePicker = DirectCast(ctrl, RadDatePicker)
                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                If Not itemValue.KeyData = "01/01/1900" Then
                                    'Get the current min and max dates
                                    Dim minDate As Date = dPicker.MinDate
                                    Dim maxDate As Date = dPicker.MaxDate
                                    'loosen the validation to allow the population of the date-picker from the saved settings
                                    'since it has already been validated on entry
                                    dPicker.MinDate = dPicker.MinDate.AddYears(-20)
                                    dPicker.MaxDate = dPicker.MaxDate.AddYears(20)
                                    'populate the date picker
                                    dPicker.SelectedDate = CDate(itemValue.KeyData)
                                    'restore the min and max dates to the proper values
                                    dPicker.MinDate = minDate
                                    dPicker.MaxDate = maxDate
                                End If
                            Next
                        Else
                            'Dim x As String = ctrl.GetType.ToString
                            Throw New Exception("Unknown Saved Control Settings")
                        End If
                    End If
                Next
            Else
                RadListBoxProgram.Items.Clear()
                RadListBoxProgram2.Items.Clear()
                RadListBoxAreasOfInterest.Items.Clear()
                RadListBoxAreasOfInterest2.Items.Clear()
            End If
            ListBoxCounts()
            HideShowInstitutionType()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
    Private Sub ListBoxCounts()
        Dim boxcount1 As Integer = RadListBoxProgram.Items.Count()
        Dim boxcount2 As Integer = RadListBoxProgram2.Items.Count()
        Dim boxcount3 As Integer = RadListBoxAreasOfInterest.Items.Count()
        Dim boxcount4 As Integer = RadListBoxAreasOfInterest2.Items.Count()

        lblProgramCounterAvailable.Text = boxcount1.ToString.Trim & " available"
        lblProgramCounterAvailable.ToolTip = boxcount1.ToString.Trim & " available " & Caption & "(s)"
        lblProgramCounterSelected.Text = boxcount2.ToString.Trim & " selected"
        lblProgramCounterSelected.ToolTip = boxcount2.ToString.Trim & " selected " & Caption & "(s)"

        lblAreasOfInterestCounterAvailable.Text = boxcount3.ToString.Trim & " available"
        lblAreasOfInterestCounterAvailable.ToolTip = boxcount3.ToString.Trim & " available " & Caption & "(s)"
        lblAreasOfInterestCounterSelected.Text = boxcount4.ToString.Trim & " selected"
        lblAreasOfInterestCounterSelected.ToolTip = boxcount4.ToString.Trim & " selected " & Caption & "(s)"

    End Sub
  
    Private Sub EnableReportDate(ByVal IsEnabled As Boolean)
      
    End Sub

    Public Function CreateReporterDateRange(ByVal reportingType As IPEDSReportingType, ByVal IPEDSControlType As ControlType) As String
        Dim msg As String = String.Empty



        Try
            Select Case reportingType
                Case IPEDSReportingType.ProgramReporter

                Case IPEDSReportingType.AcademicYearReporter

                Case Else
                    Throw New Exception("Unknown IPEDS Reporting Type")
            End Select
            Return msg
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Function
    Private Sub SetControlDefaults()
        Try

            'StudentIdentifier = MyAdvAppSettings.AppSettings("StudentIdentifier").ToString
            'If StudentIdentifier = "SSN" Then
            '    rblSortBy.Items(0).Value = "SSN"
            '    rblSortBy.Items(0).Text = "SSN"
            'Else
            '    'Fin Aid reports not using Enrollment id for sorting - 11/27/2012
            '    If IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
            '        rblSortBy.Items(0).Value = "Student Number"
            '        rblSortBy.Items(0).Text = "Student Number"
            '    Else
            '        If StudentIdentifier = "EnrollmentId" Then
            '            rblSortBy.Items(0).Value = "EnrollmentId"
            '            rblSortBy.Items(0).Text = "Enrollment ID"
            '        Else
            '            rblSortBy.Items(0).Value = "Student Number"
            '            rblSortBy.Items(0).Text = "Student Number"
            '        End If
            '    End If
            'End If


            Select Case IPEDSControlType
                Case ControlType.Instructions
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = False
                    'DisplaySort.Visible = False
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False

                Case ControlType.GEMissingData
                    RadComboCampus.Visible = True
                    ProgramSelector.Visible = True
                    'DisplaySort.Visible = True
                    AreasOfInterestSelector.Visible = False
                    SchoolReportingTypeHeader.Visible = False
                    rblSchoolType.Visible = False
                    EnableReportDate(False)
                Case ControlType.None

                Case Else
                    Throw New Exception("Unknown Control Type")
            End Select
            CreateReporterDateRange(CType(rblSchoolType.SelectedIndex, IPEDSReportingType), IPEDSControlType)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Sub HideShowInstitutionType()
        If rblSchoolType.SelectedIndex = 1 Then
            If IPEDSControlType = ControlType.GradRates2Years Or IPEDSControlType = ControlType.GradRates4 Or IPEDSControlType = ControlType.GradRates200Less2Yrs Then
                AcademicYearMsg.Visible = False
            End If
            If IPEDSControlType = ControlType.FinAidA Or IPEDSControlType = ControlType.FinAidBCDE Then
                radComboBoxInstitutionType.Visible = True
                radComboBoxInstitutionTypeLabel.Visible = True
            End If
        Else
            radComboBoxInstitutionType.Visible = False
            radComboBoxInstitutionTypeLabel.Visible = False
            AcademicYearMsg.Visible = False
        End If
    End Sub

    Private Sub SetProperties()
        Try
            For Each prop As ParameterItemPropertyInfo In ItemDetail.ParameterItemPropertyCollection
                If prop.ChildControl Is Nothing Then
                    SetControlProperties_Item(prop)
                Else
                    SetChildControlProperties_Item(prop)
                End If
            Next

            For Each prop2 As ParameterDetailPropertyInfo In ItemDetail.ParameterDetailPropertyCollection
                If prop2.ChildControl Is Nothing Then
                    SetControlProperties_Detail(prop2)
                Else
                    SetChildControlProperties_Detail(prop2)
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Sub SetControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim p As PropertyInfo = Me.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(Me, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(Me, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(Me, CInt(prop.Value), Nothing)
            Else
                p.SetValue(Me, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Item(ByVal prop As ParameterItemPropertyInfo)
        Try
            Dim ctrl As Control = FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
    Private Sub SetChildControlProperties_Detail(ByVal prop As ParameterDetailPropertyInfo)
        Try
            Dim ctrl As Control = FindControl(prop.ChildControl)
            Dim p As PropertyInfo = ctrl.GetType().GetProperty(prop.PropName, BindingFlags.Instance Or BindingFlags.Public)

            If prop.ValueType = "Boolean" Then
                p.SetValue(ctrl, CBool(prop.Value), Nothing)
            ElseIf prop.ValueType = "Integer" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            ElseIf prop.ValueType = "Date" Then
                p.SetValue(ctrl, CDate(prop.Value), Nothing)
            ElseIf prop.ValueType = "Enum" Then
                p.SetValue(ctrl, CInt(prop.Value), Nothing)
            Else
                p.SetValue(ctrl, prop.Value, Nothing)
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(prop.ChildControl & " " & prop.PropName & " : " & ex.Message)
        End Try
    End Sub
#End Region


End Class
