﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamDateRange.ascx.vb" Inherits="ParamDateRange" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>


<div id="MainContainer" class="MainContainer">
    <asp:Panel ID="ContainerPanel" runat="server">
        <%--<div id="captionlabel" class="CaptionLabel" runat="server"><%=Caption%></div>--%>
        <asp:Literal ID="InstructText" runat="server"></asp:Literal>
        <span id="StartDatePicker" class="StartDatePicker">
            <label>
                From
            </label>
            <telerik:RadDatePicker ID="RadStartDatePicker" runat="server" CssClass="FilterInput"
                Width="151px">
                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy"></DateInput>
            </telerik:RadDatePicker>
        </span>
        <span id="EndDatePicker" class="EndDatePicker">
            <label>
                To
            </label>
            <telerik:RadDatePicker ID="RadEndDatePicker" runat="server"  CssClass="FilterInput"
                Width="151px">
                <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>

                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy"></DateInput>
            </telerik:RadDatePicker>
        </span>
        <span>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                ErrorMessage="Please enter a Date" Display="Static"
                ControlToValidate="RadStartDatePicker" Enabled="false"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                ErrorMessage="Please enter a Date" Display="Static"
                ControlToValidate="RadEndDatePicker" Enabled="false"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator" runat="Server" ControlToCompare="RadStartDatePicker"
                ControlToValidate="RadEndDatePicker" Operator="GreaterThan"
                ErrorMessage=" Date range is not valid" Display="Dynamic" Enabled="false" />
        </span>
    </asp:Panel>
</div>
