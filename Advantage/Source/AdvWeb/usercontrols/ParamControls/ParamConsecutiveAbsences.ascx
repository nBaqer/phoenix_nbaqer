﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamConsecutiveAbsences.ascx.vb" Inherits="usercontrols_ParamControls_ParamConsecutiveAbsences" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function checkFilter(sender, args) {
        if (args.get_destinationListBox().get_id().indexOf("RadListBoxTerms2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one term selection is allowed');
        }
        else if (args.get_destinationListBox().get_id().indexOf("RadListBoxCourse2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one course selection is allowed');
        }
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
        //var maxNumberOfItems = 1;
        var dest = args.get_destinationListBox();
        var totalCount = dest.get_items().get_count();
        var itemsToTransferCount = args.get_items().length;
        if (totalCount == maxNumberOfItems) {
            alert(message);
            args.set_cancel(true);
        } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
            while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                itemsToTransferCount--;
                args.get_items().pop();
            }
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanelIPEDS" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">
        <div id="CmpGrpSelector" class="CmpGrpSelector MultiFilterReportContainer" runat="server">
            <div id="Div3" class="CaptionLabel" runat="server">Campus Group</div>
            <div class="FilterInput">

                <telerik:RadListBox ID="RadListBoxCmpGrp" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxesCampusGroup_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxCmpGrp2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxCmpGrp2" runat="server"
                    OnTransferred="RadListBoxesCampusGroup_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="InactiveCampusGroupCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveCmpGrp" runat="server"
                        OnCheckedChanged="cbkInactiveCmpGrp_OnCheckedChanged" AutoPostBack="true"
                        Text="Show Inactive"
                        ToolTip="Check this box to make inactive programs selectable" />
                    <span id="CampusGroupCounterAvailable" class="RadListBox1Counter">
                        <asp:Label ID="lblCmpGrpCounterAvailable" runat="server"></asp:Label>
                    </span>
                    <span id="CampusGroupCounterSelected" class="RadListBox2Counter">
                        <asp:Label ID="lblCmpGrpCounterSelected" runat="server"></asp:Label>
                    </span>
                </div>
            </div>
        </div>

        <div id="CampusSelectorDiv" class="CampusSelector MultiFilterReportContainer" runat="server">
            <div id="CampusSelectorLabel" class="CaptionLabel" runat="server">Campus</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxCampus" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListCampusBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxCampus2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxCampus2" runat="server"
                    OnTransferred="RadListCampusBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="InactiveCampusCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="checkboxInactiveCampuses" runat="server"
                        OnCheckedChanged="checkboxInactiveCampuses_OnCheckedChanged" AutoPostBack="true"
                        Text="Show Inactive"
                        ToolTip="Check this box to make inactive programs selectable" />
                    <span id="CampusCounterAvailable" class="RadListBox1Counter">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </span>
                    <span id="CampusCounterSelected" class="RadListBox2Counter">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                    </span>
                </div>
            </div>
        </div>

        <div id="TermSelector" class="TermSelector MultiFilterReportContainer" runat="server">
            <div id="Div4" class="CaptionLabel" runat="server">Term</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxTerms" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True"
                    TransferToID="RadListBoxTerms2" SelectionMode="Multiple">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxTerms2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" />
                </telerik:RadListBox>
                <div id="Div5" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="chkInActiveTerms" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="Span3" class="RadListBox1Counter">
                        <asp:Label ID="lblTermsCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="Span4" class="RadListBox2Counter">
                        <asp:Label ID="lblTermsCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
            </div>
            <br />
        </div>

        <div id="CourseSelector" class="ProgramSelector MultiFilterReportContainer" runat="server">
            <div id="captionlabel2" class="CaptionLabel" runat="server">Course</div>
            <div class="FilterInput">
                <telerik:RadListBox ID="RadListBoxCourse" runat="server" Width="360px" Height="100px"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                    TransferToID="RadListBoxCourse2">
                    <ButtonSettings ShowReorder="true" ShowTransfer="True" ShowTransferAll="True" TransferButtons="All" />
                </telerik:RadListBox>
                <telerik:RadListBox ID="RadListBoxCourse2" runat="server"
                    OnTransferred="RadListBoxes_OnTransferred"
                    OnItemDataBound="RadListBoxes_ItemDataBound"
                    Width="340px" Height="100px" AllowTransfer="True"
                    AllowTransferOnDoubleClick="True" AutoPostBackOnTransfer="True"
                    CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple">
                    <ButtonSettings ShowDelete="False" ShowReorder="False"
                        ShowTransfer="False" ShowTransferAll="False" TransferButtons="Common" />
                </telerik:RadListBox>
                <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                    <asp:CheckBox ID="cbkInactiveCourses" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                    <span id="ProgramCounterAvailable" class="RadListBox1Counter">
                        <asp:Label ID="lblProgramCounterAvailable" runat="server" Text=""></asp:Label>
                    </span>
                    <span id="ProgramCounterSelected" class="RadListBox2Counter">
                        <asp:Label ID="lblProgramCounterSelected" runat="server" Text=""></asp:Label>
                    </span>
                </div>
            </div>
            <br />
        </div>
        <div id="DateOptionsSelector" class="DateOptionsSelector MultiFilterReportContainer" runat="server">
            <div id="DateOptionsHeader" class="CaptionLabel" runat="server">Class Meeting Date</div>

            <div id="DatePopUps" class="DatePops">
                <telerik:RadComboBox ID="radMeetingDateOperator" runat="server" Visible="true" Width="200px" CssClass="LabelFilterInput ReportLeftMarginInput">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="Less Than or Equal To" Value="0" />
                    </Items>
                </telerik:RadComboBox>
                <telerik:RadDatePicker ID="RadDateReportDate" runat="server" DateInput-ReadOnly="true" CssClass="ManualInput">
                    <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                    <DatePopupButton></DatePopupButton>

                    <DateInput runat="server" DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" CssClass="textbox"></DateInput>
                </telerik:RadDatePicker>
            </div>

        </div>
        <div id="ReportDateRangeMsg" runat="server"></div>
        <div id="Div1" class="ProgramSelector MultiFilterReportContainer" runat="server">
            <div id="Div2" class="CaptionLabel" runat="server">Number of days student was consecutively absent</div>
            <telerik:RadComboBox ID="radComboBoxInstitutionType" runat="server" Visible="true" Width="200px" CssClass="LabelFilterInput ReportLeftMarginInput">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="Greater Than or Equal To" Value="0" />
                </Items>
            </telerik:RadComboBox>
            <asp:TextBox ID="txtNumberofDays" runat="server" CssClass="textbox ManualInput" Text="3" />
        </div>
        <br />
    </div>

</asp:Panel>
