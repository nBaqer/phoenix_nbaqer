﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ParamGradCompletion.ascx.vb" Inherits="ParamGradCompletion" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function checkFilter(sender, args) {
        if (args.get_destinationListBox().get_id().indexOf("RadListBoxProgram2") > 1) {
            handleSourceToDestinationTransfer(args, 1, 'Only one program selection is allowed');
        }
    }

    function handleSourceToDestinationTransfer(args, maxNumberOfItems, message) {
        //var maxNumberOfItems = 1;
        var dest = args.get_destinationListBox();
        var totalCount = dest.get_items().get_count();
        var itemsToTransferCount = args.get_items().length;
        if (totalCount === maxNumberOfItems) {
            alert(message);
            args.set_cancel(true);
        } else if (totalCount + itemsToTransferCount > maxNumberOfItems) {
            while (totalCount + itemsToTransferCount > maxNumberOfItems) {
                itemsToTransferCount--;
                args.get_items().pop();
            }
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="MainPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="MainPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxLoadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="MainPanel" runat="server">
    <div id="MainContainer" class="MainContainer">

        <div id="divAcademicYeardate" class="MultiFilterReportContainer" runat="server">
            <div id="SchoolReportingTypeHeader" class="CaptionLabel" runat="server">
                Award Year Date Options
           
            </div>
            <div class="FilterInput">
                <table>
                    <tr>
                        <td class="style1">
                            <div>
                                <asp:Label ID="StartDatetext" class="DatePicker1Text" runat="server">Start Date </asp:Label>
                            </div>
                            <telerik:RadDatePicker ID="RadDateStartDate" runat="server" 
                                Width="151px" DateInput-ReadOnly="true">
                                <Calendar ID="StartDateCal" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server">
                                </Calendar>
                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ReadOnly="false"  runat="server">
                                </DateInput>
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <div>
                                <asp:Label ID="EndDateText" class="DatePicker1Text" runat="server">End Date </asp:Label>
                            </div>
                            <telerik:RadDatePicker ID="RadDateEndDate" runat="server" 
                                Width="151px" DateInput-ReadOnly="true">
                                <Calendar ID="EndDateCal" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server">
                                </Calendar>
                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                <DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" ReadOnly="false" runat="server">
                                </DateInput>
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="CampusSelector" class="CampusSelector MultiFilterReportContainer" runat="server" style="padding-left: 0px;">
            <div id="CampusHeader" class="CaptionLabel" runat="server">
                Campus <font color="red">*</font>
            </div>
            <div class="FileInput">
                <telerik:RadComboBox ID="RadComboCampus" runat="server" Width="350px"
                    AutoPostBack="true" ToolTip="Select a campus to run the report"
                    OnSelectedIndexChanged="CampusComboBox_SelectedIndexChanged"
                    OnItemDataBound="CampusComboBox_ItemDataBound">
                </telerik:RadComboBox>
            </div>
        </div>
    </div>
    <div id="ProgramSelector" class="ProgramSelector MultiFilterReportContainer" runat="server">
        <div id="Div4" class="CaptionLabel" runat="server">Program</div>
        <div class="FilterInput">
            <telerik:RadListBox ID="RadListBoxProgram" runat="server" Width="360px" Height="100px"
                AllowTransfer="True" AutoPostBackOnTransfer="True" AllowTransferOnDoubleClick="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                TransferToID="RadListBoxProgram2"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound">
                <ButtonSettings ShowReorder="true" ShowTransfer="True" />
            </telerik:RadListBox>
            <%-- TransferButtons="Common"  OnClientTransferring="checkFilter"  --%>
            <telerik:RadListBox ID="RadListBoxProgram2" runat="server" Width="340px" Height="100px"
                AllowTransfer="True" AutoPostBackOnTransfer="True" AllowTransferOnDoubleClick="True"
                CausesValidation="False" EnableDragAndDrop="True" SelectionMode="Multiple"
                OnTransferred="RadListBoxes_OnTransferred"
                OnItemDataBound="RadListBoxes_ItemDataBound">
                <ButtonSettings ShowDelete="False" ShowReorder="False"
                    ShowTransfer="False" ShowTransferAll="False" />
            </telerik:RadListBox>
            <%-- TransferButtons="Common"    --%>
            <div id="InactiveCheckBoxContainer" class="InactiveCheckBoxContainer">
                <asp:CheckBox ID="cbkInactivePrograms" runat="server" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" Text="Show Inactive" ToolTip="Check this box to make inactive programs selectable" />
                <span id="ProgramCounterAvailable" class="RadListBox1Counter">
                    <asp:Label ID="lblProgramCounterAvailable" runat="server" Text=""></asp:Label>
                </span>
                <span id="ProgramCounterSelected" class="RadListBox2Counter">
                    <asp:Label ID="lblProgramCounterSelected" runat="server" Text=""></asp:Label>
                </span>
            </div>
            <br />
        </div>
    </div>
    <div id="SchoolReportTypeSelector" class="SchoolReportTypeSelector MultiFilterReportContainer">
        <div id="Div1" class="CaptionLabel" runat="server">On Time Graduation Date By</div>
        <div class="ReportLeftMarginInput">
            <table>
                <tr>
                    <td style="width: 50%">
                        <div id="rblRevGradDateSelector" runat="server">
                            <asp:RadioButtonList ID="rblRevGradDateType" runat="server"
                                RepeatDirection="Horizontal" ToolTip="Select the type of Graduation date calculation to be used." AutoPostBack="False">
                                <asp:ListItem Value="FromProgram" Selected="True">Program Weeks </asp:ListItem>
                                <asp:ListItem Value="GradDateFromEnroll">Graduation Date from Enrollment</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="ResetDiv"></div>
    </div>
</asp:Panel>

