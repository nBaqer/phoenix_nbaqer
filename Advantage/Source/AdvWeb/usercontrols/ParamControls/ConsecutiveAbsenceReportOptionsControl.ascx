﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConsecutiveAbsenceReportOptionsControl.ascx.vb" Inherits="ConsecutiveAbsenceReportOptionsControl" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ConsecutiveAbsencesOptionsGroup">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ConsecutiveAbsencesOptionsGroup" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel
    ID="RadAjaxConsecutivePanelCustomOptionsGroup" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="ConsecutiveAbsencesOptionsGroup" runat="server">
    <div id="MainContainer" class="MainContainer">
        <div id="captionlabel" class="CaptionLabel" runat="server">
            Result Options
         
        </div>
        <div id="ShowCalendarDays" class="ShowCalendarDays ReportLeftMarginInput">

            <asp:RadioButtonList ID="rblShowCalendarDays" runat="server" AutoPostBack="True"
                                 Width="300px">
                <asp:ListItem Selected="True" Value="False">Use Student Scheduled Days</asp:ListItem>
                <asp:ListItem Value="True">Use Calendar Days</asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
</asp:Panel>
