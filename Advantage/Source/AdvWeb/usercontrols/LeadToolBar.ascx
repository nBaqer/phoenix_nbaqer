﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeadToolBar.ascx.vb" Inherits="AdvWeb.usercontrols.UsercontrolsLeadToolBar" %>
<link href="../css/lead_toolbar.css" rel="stylesheet" />
<div id="LeadToolBarWrapper">
    <div id="LeadToolbar" style="width: 100%; min-width: 1280px; border: none; z-index: 0; position: relative"></div>
    <div id="leadToolBarWindow" style="height: 190px;width: 330px" >
        <div style="text-align: left; vertical-align: central">
            <div style="background: gainsboro; height: 100%; margin:0"><p><br/>&nbsp;&nbsp;A new lead has been added to your Lead Queue.<br/><br/>
                &nbsp;&nbsp;A Task or Appointment is due.<br/><br/>
                &nbsp;&nbsp;Select the tab below to access.</p>
            </div>
            
            <div id="alertToolBar"></div>
        </div>
    </div>
</div>

<%-- ReSharper disable UnusedLocals --%>
<script type="text/javascript">
    $(document).ready(function () {
        var leadtoolbar = new AD.LeadToolbar();
    });
</script>
<%-- ReSharper restore UnusedLocals --%>  