﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CampusSwitcher.ascx.vb" Inherits="usercontrols_CampusSwitcher" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<div id="CampusSwitcherContainer" style="width:350px;height:100px;margin:20px; background-color:#ffffff;">
<div id="CSMsg" style="width:200px;margin:auto;">
    <asp:Literal ID="CampusSwitcherMsg" runat="server"></asp:Literal>
        </div>
<div id="CSComboBox" style="width:300px;margin:auto;padding-top:10px;">
    <telerik:RadComboBox ID="ddlCampusSwitcher" runat="server" AutoPostBack="True" 
        Width="300px" style="margin:auto;" ZIndex="15000" DropDownWidth="300px" OnSelectedIndexChanged="ddlCampusSwitcher_OnSelectedIndexChanged">
        <Items>
            <telerik:RadComboBoxItem runat="server" Text="Brighton" 
                Value="RadComboBoxItem1" Owner="ddlCampusSwitcher" />
            <telerik:RadComboBoxItem runat="server" Text="Fort Wayne kdotef hjeoi oejhgitjhffdk Wpjfsjfhf Rs" 
                Value="RadComboBoxItem2" Owner="ddlCampusSwitcher" />
            <telerik:RadComboBoxItem runat="server" Text="IMDT" 
                Value="RadComboBoxItem3" Owner="ddlCampusSwitcher" />
            <telerik:RadComboBoxItem runat="server" Text="Houston" 
                Value="RadComboBoxItem4" Owner="ddlCampusSwitcher" />
            <telerik:RadComboBoxItem runat="server" Text="Miami" 
                Value="RadComboBoxItem5" Owner="ddlCampusSwitcher" />
        </Items>
    </telerik:RadComboBox>
    </div>
</div>
