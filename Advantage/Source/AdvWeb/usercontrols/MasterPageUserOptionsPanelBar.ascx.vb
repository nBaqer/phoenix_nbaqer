﻿
Imports FAME.Advantage.Site.Lib.Infrastruct.Helpers

Namespace AdvWeb.usercontrols
    Partial Class UsercontrolsMasterPageUserOptionsPanelBar
        Inherits UserControl

        Public Property CurrentCampusId() As String
        Public Property CurrentBuildVersion() As String

        Protected Sub Control_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            If Page.IsPostBack Then
                Dim parameter As String = Request("__EVENTARGUMENT")
                Dim control As String = Request("__EVENTTARGET")
                If Not (control Is Nothing) AndAlso control.ToUpper = "MASTERCOMBOBOXS" AndAlso Not (parameter Is Nothing) AndAlso parameter = "5Out" Then
                    'Log Out option in user panel bar.
                    Session.Abandon()
                    FormsAuthentication.SignOut()
                    MultiTenantHostHelper.RedirectToLogin()
                End If
            End If
        End Sub

    End Class
End Namespace