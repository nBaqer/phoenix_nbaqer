﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EntityBarEditControl.ascx.vb" Inherits="usercontrols_EntityBarEditControl" %>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="lstEditSavedPages">
                    <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lstEditSavedPages" />
                    <telerik:AjaxUpdatedControl ControlID="txtTabDisplayName" />
                    <telerik:AjaxUpdatedControl ControlID="btnAddTab" />
                    <telerik:AjaxUpdatedControl ControlID="btnDone" />
                    <telerik:AjaxUpdatedControl ControlID="EditPanelMsg" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="txtTabDisplayName">
                    <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lstEditSavedPages" />
                    <telerik:AjaxUpdatedControl ControlID="txtTabDisplayName" />
                    <telerik:AjaxUpdatedControl ControlID="btnAddTab" />
                    <telerik:AjaxUpdatedControl ControlID="btnDone" />
                    <telerik:AjaxUpdatedControl ControlID="EditPanelMsg" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnAddTab">
                    <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lstEditSavedPages" />
                    <telerik:AjaxUpdatedControl ControlID="txtTabDisplayName" />
                    <telerik:AjaxUpdatedControl ControlID="btnAddTab" />
                    <telerik:AjaxUpdatedControl ControlID="btnDone" />
                    <telerik:AjaxUpdatedControl ControlID="EditPanelMsg" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="EditPanelMsg">
                    <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lstEditSavedPages" />
                    <telerik:AjaxUpdatedControl ControlID="txtTabDisplayName" />
                    <telerik:AjaxUpdatedControl ControlID="btnAddTab" />
                    <telerik:AjaxUpdatedControl ControlID="btnDone" />
                    <telerik:AjaxUpdatedControl ControlID="EditPanelMsg" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        </AjaxSettings>
</telerik:RadAjaxManagerProxy>
    <telerik:RadInputManager ID="RadInputManager1" runat="server">
        <telerik:RegExpTextBoxSetting BehaviorID="RegExpBehavior1" Validation-IsRequired="true" Validation-ValidationGroup="TabDisplayName"
            ValidationExpression="^[a-zA-Z0-9\s.\-\(\)\?!':`~&]{1,35}$"
            SelectionOnFocus="SelectAll">
            <TargetControls>
                <telerik:TargetInput ControlID="txtTabDisplayName" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>
    </telerik:RadInputManager>
<telerik:RadAjaxLoadingPanel ID="EditSavedPagesLoadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<div id="EditP" style="padding:20px 0px 20px 20px;" >
<%--<h2>Favorite Added!</h2>
<h3>Favorites for Students</h3>--%>

<div id="txtTabDisplayNameLabel" style="font-weight:bold;">Tab Display Name</div>
<div style="float:left;margin:0px 20px 20px 0px;">
    <asp:TextBox ID="txtTabDisplayName" runat="server" Width="205px" Text="Students without Financial Aid ..." ></asp:TextBox>
</div>
<div style="float:right;margin:0px 20px 20px 0px;">
    <telerik:RadButton ID="btnAddTab" runat="server" Text="Add" Width="75px" ValidationGroup="TabDisplayName">
    </telerik:RadButton>
</div>
  <div><asp:Literal ID="EditPanelMsg" runat="server"></asp:Literal></div>
<telerik:RadListBox ID="lstEditSavedPages" runat="server" AllowDelete="True" 
    AllowReorder="True" EnableDragAndDrop="true" Width="300px" Height="250px" AutoPostBack="true" 
    AutoPostBackOnDelete="true" AutoPostBackOnReorder="true" CausesValidation="false"
     EmptyMessage="You have no favorites saved">
<ButtonSettings TransferButtons="All" ></ButtonSettings>
    <Items>
        <telerik:RadListBoxItem runat="server" Text="Info"/>
        <telerik:RadListBoxItem runat="server" Text="Exams" />
        <telerik:RadListBoxItem runat="server" Text="Student Ledger" />
        <telerik:RadListBoxItem runat="server" Text="Awards" />
        <telerik:RadListBoxItem runat="server" Text="Attendance" />
        <telerik:RadListBoxItem runat="server" Text="Skills" />
        <telerik:RadListBoxItem runat="server" Text="Academics" />
        <telerik:RadListBoxItem runat="server" Text="Student SAP Check Results" />
        <telerik:RadListBoxItem runat="server" Text="Student Hold Groups" />
        <telerik:RadListBoxItem runat="server" Text="Term Progress" />
    </Items>
</telerik:RadListBox>
</div>
<div style="padding:0px;">
    <telerik:RadButton ID="btnDone" runat="server" Text="Done" Width="75px" 
    CausesValidation="false" style="float:right;margin:0px 20px 20px 0px;"
    Visible="true">
    </telerik:RadButton>
</div>


