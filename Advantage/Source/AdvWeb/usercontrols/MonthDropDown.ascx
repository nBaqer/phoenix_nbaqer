﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MonthDropDown.ascx.vb" Inherits="usercontrols_MonthDropDown" %>

<div id="MonthSelector" class="CampusSelector MultiFilterReportContainer"  runat="server" style="padding-left:2px;">
    <div id="MonthHeader" class="CaptionLabel" runat="server">Month <font color="red">*</font></div>

    <telerik:RadComboBox ID="RadComboMonth" runat="server" AutoPostBack="true" Width="350px" tooltip="Select a month to run the report"  CssClass="ManualInput ReportLeftMarginInput">
    </telerik:RadComboBox>
</div>

