﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MasterPageUserOptionsPanelBar.ascx.vb" Inherits="AdvWeb.usercontrols.UsercontrolsMasterPageUserOptionsPanelBar" %>
<!-- You need to add the jquery and kendo support in the page or master page to use the component -->
<%-- You must put in master page the script: Scripts/UserControls/MasterPageUserOptionsPanelBar.js --%>

<style type="text/css">
    /* Master Page Style*/
    /*#MasterPageUserOptionsPanelBar  .k-content, .k-header, .k-link  {
        height: 29px;
    }*/
</style>

<script>
    // Variables for MasterPageUserOptionsPanelBar
    var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL = '<%= Page.ResolveUrl("~")%>';
    var XMASTER_PAGE_USER_OPTIONS_USERNAME = "<%=Session("UserName") %>";
    var XMASTER_PAGE_USER_OPTIONS_USERID = '<%=Session("UserId")%>';
    var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID = '<%=CurrentCampusId %>';


    var XMASTER_PAGE_USER_OPTIONS_CURRENT_BUILD = '<%=CurrentBuildVersion%>';

    if (XMASTER_PAGE_USER_OPTIONS_CURRENT_BUILD)
        window.insertInSessionCache("BUILDVERSION", XMASTER_PAGE_USER_OPTIONS_CURRENT_BUILD);

</script>

<ul id="MasterPageUserOptionsPanelBar"  style="width: 155px;z-index:9998 ">
    <li data-bind="attr: { id: value }" style="width: 155px; z-index: 9998; position: fixed; ">
        <img data-bind="attr: { src: imageUrl }" alt="Current User" /> 
        <span data-bind="text: text" ></span>
        <ul data-template="ul-template" data-bind="source: items" style="width: 155px"></ul>
    </li>
</ul>

<!-- Kendo template for panel bar internal items. -->
<script id="ul-template" type="text/x-kendo-template">
    <li data-bind="attr:{id:value}">
        <img data-bind="attr: { src: imageUrl }" alt="" />
            <span data-bind="text: text"></span>
    </li>
</script>



