﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MainMenu.ascx.vb" Inherits="usercontrols_MainMenu" %>
<script>
    // Variables for MasterPageUserOptionsPanelBar
    var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL = '<%= Page.ResolveUrl("~")%>';
    var XMASTER_PAGE_USER_OPTIONS_USERNAME = "<%=Session("UserName") %>";
    var XMASTER_PAGE_USER_OPTIONS_USERID = '<%=Session("UserId")%>';
    var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID = '<%=CurrentCampusId %>';

</script>
<div id="menuContent">
    <div id="menuRow1" class="k-widget k-reset k-header k-menu k-menu-horizontal blue-primary">
        <ul id="menu"></ul>

        <div id="helpicons">
            <div class="menu-icons-top-bar">
                <a id="a1" href='~/help/WhatsNew.htm' target='_blank' style="border: 0" runat="server">
                    <span class="k-icon k-i-information font-white"></span>
                </a>
                <a id="a2" href='~/help/AdvantageHelp.htm' target='_blank' runat="server" style="border: 0">
                    <span class="k-icon k-i-question font-white"></span>
                </a>
            </div>
            <ul id="MasterPageUserOptionsPanelBar" style="z-index: 9998;">
                <li id="UserOptionBotton">
                    <span data-bind="attr: { class : imageUrl}"></span>
                    <span data-bind="text: text"></span>
                    <ul id="UserOptionsItems" data-template="ul-template" data-bind="source: items"></ul>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear">
    </div>
    <div id="responsiveRootMenu">
        <div id="menuRow2" class="k-widget k-reset k-header k-menu k-menu-horizontal blue-darken-2">
            <ul id="subMenu"></ul>
            <div id="subMenuLinks">
                <asp:LinkButton ID="FERPA" runat="server" Enabled="false" EnableViewState="false"
                    ClientIDMode="Static" Style="text-decoration: underline; float: left; margin-right: 20px;" Visible="false">FERPA</asp:LinkButton>
                <telerik:RadToolTip ID="rtFERPA" runat="server" TargetControlID="FERPA" RelativeTo="BrowserWindow"
                    Position="Center" RenderInPageRoot="true" Animation="Slide" HideEvent="ManualClose">
                    <h3><b>FERPA Permissions</b></h3>
                    &nbsp;&nbsp;
                        <br />
                    <h4><b>&nbsp;&nbsp;
                            The following entities have permissions to this page.&nbsp;&nbsp;</b></h4>
                    <br />
                    <ul style="padding: 0; margin: 0;">
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <li style="list-style: none;">&nbsp;&nbsp;
                                        <%# Eval("FerPaEntityDescrip") %></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <br />
                </telerik:RadToolTip>
                <asp:LinkButton ID="btnAudit" runat="server" Text="Audit History" OnClientClick="AuditHistory(); return false;"
                    CausesValidation="false" ClientIDMode="Static" ForeColor="white" class="hidden">
                </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="layer1">
        <p class="heading" style="color: white; text-align: right; padding-right: 20px;">Click to Hide</p>
        <div class="content">
            <div id="pagegroupdata">
            </div>
        </div>
        <p class="headerFooter"></p>
    </div>
</div>

<!-- Kendo template for panel bar internal items. -->
<script id="ul-template" type="text/x-kendo-template">
    <li data-bind="attr:{id:value}">
        <span data-bind="attr: { class : imageUrl}"></span>
            <span data-bind="text: text"></span>
    </li>
</script>
<input type="hidden" id="hdnUserName" runat="server" class="hdnUserName" clientidmode="Static" />
<input type="hidden" id="hdnUserId" runat="server" class="hdnUserName" clientidmode="Static" />
<input type="hidden" id="hdnSelectedModule" runat="server" clientidmode="Static" />