﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EmployeeInfoBar.ascx.vb" Inherits="UserControls_EmployeeInfoBar" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy>
<telerik:RadToolTipManager ID="RadToolTipManager2" runat="server" Position="Center"
    Animation="Fade" HideEvent="LeaveTargetAndToolTip" RelativeTo="Element"
    EnableShadow="true" ShowCallout="false"
    RenderInPageRoot="true" AnimationDuration="200">
    <%--        OnAjaxUpdate="OnAjaxUpdate" OnClientHide="OnClientHide"--%>
</telerik:RadToolTipManager>
<script type="text/javascript">
    function itemOpened(s, e) {
        if ($telerik.isIE8) {
            // Fix an IE 8 bug that causes the list bullets to disappear (standards mode only)
            $telerik.$("li", e.get_item().get_element())
                .each(function () { this.style.cssText = this.style.cssText; });
        }
    }

</script>
<script type="text/javascript">

    goToSearchPage = function () {

        location.href = jQuery("#existingStudentsLink").val();
    };

</script>
<div id="wonderbar" style="width: 100%; height: 75px; background-color: #E9EDF2;">
    <div id="infobar" style="width: 100%;">
        <asp:Table ID="tblLeadBar" runat="server" Width="100%" BorderStyle="None" Style="padding-top: 10px; font-family: Verdana; font-size: 12px; margin-left: 10px">
            <asp:TableRow>
                <asp:TableCell Width="60%">
                    <asp:Table ID="tblLeadInfoBar" runat="server" Width="100%" >
                        <asp:TableRow>
                            <asp:TableCell Width="24px">
                                <a href="javascript:void(0);"  onclick="goToSearchPage();" id="img1" runat="server"  title="Search for Employees" >
                                    <span class="k-icon k-i-search font-black"></span>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                <asp:Label ID="lblName" runat="server" Text="Employee:" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblNameValue" runat="server" Style="padding-left: 4px;"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <a href="javascript:void(0);"  onclick="openTaskRadWindow();" id="img2" runat="server"  title="Send Email" >
                                    <span class="k-icon k-i-email font-black"></span>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false">
                                <asp:Label ID="lblIdentifier" runat="server" Text="Address:" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblAddressValue" runat="server"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:HiddenField runat="server" ID="existingStudentsLink" ClientIDMode="Static" />
    </div>
</div>
