﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WonderBar.ascx.vb" Inherits="usercontrols_WonderBar" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy>
 <div id="wonderbar" style="width: 100%; height:90px;background-color:#EFEFEF;">
    <div id="infobar" style="width: 100%;">
                <div style="float: left;">
                    <asp:Image ID="studentphoto" runat="server" ImageUrl="~/images/renee.jpg" ToolTip="view student info page"
                        Style="margin: 5px 5px 0px 10px;" />
                </div>

                <div style="margin-top: 5px;">
                    <%--<span style="margin: 5px 0px 0px 5px; font-size: small;">--%>
                    <span class="wbspan">
                        <telerik:RadComboBox ID="ddlEnrollment" runat="server" Skin="Telerik" Width="620px">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="Medical Assistant Day v2 at the Fort Lauderdale Campus"
                                    Value="RadComboBoxItem1" />
                                <telerik:RadComboBoxItem runat="server" Text="Dental Assistant Day v3 at the West Palm Beach Campus"
                                    Value="RadComboBoxItem2" />
                            </Items>
                        </telerik:RadComboBox>
                    </span>
                </div>

                <div class="wbdivmargins" runat="server" visible="false">

                    <span class="labelsmall">Student Balance:
                        <asp:HyperLink CssClass="advlinkRed" ID="HyperLink5" runat="server" ToolTip="view account details"
                            NavigateUrl="~/studentinfo.aspx">$2,134.50</asp:HyperLink>
                    </span>
                    <span class="labelsmall">GPA:
                        <asp:HyperLink CssClass="advlinkRed" ID="GPA" runat="server" ToolTip="view detailed academics info"
                            NavigateUrl="~/studentinfo.aspx">3.45</asp:HyperLink>
                    </span>
                </div>

                <div class="wbdivmargins">
                    <span class="labelsmall" runat="server" visible="false">Recent Attendance:
                        <asp:HyperLink CssClass="advlinkRed" ID="StudentAttendance" runat="server" ToolTip="view attendance details"
                            NavigateUrl="~/studentinfo.aspx">55%</asp:HyperLink>
                    </span>            
                    <span class="labelsmall" runat="server" visible="false">SAP:
                        <asp:HyperLink CssClass="advlinkRed" ID="SAPStatus" runat="server" ToolTip="student failed last SAP check on 01/30/2011"
                            NavigateUrl="~/studentinfo.aspx">Failed</asp:HyperLink>
                    </span>
                    <span class="labelsmall" >Status:
                        <asp:HyperLink CssClass="advlinkgreen" ID="StudentStatus" runat="server" ToolTip="view student status page"
                            NavigateUrl="~/studentinfo.aspx">Currently Attending</asp:HyperLink>
                    </span>
                    <span class="wbspan">
                        <asp:HyperLink CssClass="advlink" ID="HyperLink2" runat="server" ToolTip="send email"
                            NavigateUrl="~/studentinfo.aspx" Style="margin: 5px;">rjohnson@fameuniversity.edu</asp:HyperLink></span>
                    <span class="wbspan">
                        <asp:HyperLink CssClass="advlink" ID="HyperLink4" runat="server" ToolTip="view detailed student contact info"
                            NavigateUrl="~/studentinfo.aspx" Style="margin: 5px;">954-817-5315</asp:HyperLink>
                    </span>
                </div>

                <div style="margin-top: 5px;">
                    <span class="wbspan">
                        <asp:HyperLink CssClass="advlink" ID="HyperLink234" runat="server" ToolTip="view student info page"
                            NavigateUrl="~/studentinfo.aspx" Style="margin: 5px;">7544 S White Oleander Drive, West Palm Beach, Florida 33415 USA  </asp:HyperLink>
                    </span>
                </div>
        
    </div>
</div>