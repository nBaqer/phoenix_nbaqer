
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Partial Class CrystalRptViewer
    Inherits UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Protected m_ReportObject As ReportDocument
    Protected m_SearchText As String
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


#Region " Public Properties "

    Public Property ReportSource() As ReportDocument
        Get
            Return m_ReportObject
        End Get
        Set(ByVal value As ReportDocument)
            m_ReportObject = Value
        End Set
    End Property

    Public Property SearchText() As String
        Get
            Return m_SearchText
        End Get
        Set(ByVal value As String)
            m_SearchText = Value
        End Set
    End Property

#End Region


#Region " Event Handlers "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        ReportSource = CType(Session("RptObject"), ReportDocument)
        ReportSource.SetDatabaseLogon(CType(Session("DBUserId"), String), CType(Session("DBPassword"), String), CType(Session("ServerName"), String), CType(Session("DBName"), String))
       
        'crViewer.DisplayGroupTree = False
        crViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
        crViewer.DisplayStatusBar = False
        crViewer.ReportSource = ReportSource
        'HideTabs(crViewer)
        If Not IsPostBack Then
            ddlZoom.SelectedIndex = 3   ' Default value => Zoom 100%
        End If

    End Sub

    Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
        If Not (crViewer.ReportSource Is Nothing) Then
            crViewer.ShowFirstPage()
        End If
        'Session("LastPage") = 0
    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        If Not (crViewer.ReportSource Is Nothing) Then
            crViewer.ShowPreviousPage()
        End If
        'Session("LastPage") = 0
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If Not (crViewer.ReportSource Is Nothing) Then
            crViewer.ShowNextPage()
        End If
        'Session("LastPage") = 0
    End Sub

    Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
        If Not (crViewer.ReportSource Is Nothing) Then
            crViewer.ShowLastPage()
        End If
        'Session("LastPage") = 1
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        If Not (crViewer.ReportSource Is Nothing) Then
            Try
                Dim intPg As Integer = Integer.Parse(tbGoToPage.Text)
                crViewer.ShowNthPage(intPg)
                tbGoToPage.Text = ""
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Impossible to convert input into integer.
            End Try
        End If
        'Session("LastPage") = 0
    End Sub

    Private Sub ddlZoom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlZoom.SelectedIndexChanged
        If Not (crViewer.ReportSource Is Nothing) Then
            crViewer.Zoom(CType(ddlZoom.SelectedValue, Integer))
        End If
        'Session("LastPage") = 0
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Dim strPageName As String = "ReportExportViewer.aspx?ExportTo=" & ddlExportTo.SelectedValue.ToString
        'Dim strPageOptions As String = "config='height=300px, width=780px, toolbar=no, menubar=yes, scrollbars=yes, resizable=yes, status=yes'"
        'Dim strNewWinName As String = "ReportExportViewer" & Viewstate("resid")
        'Dim scriptBegin, scriptMiddle, scriptEnd As String
        'scriptBegin = "<SCRIPT LANGUAGE='javascript'>" & vbCrLf & "<!--" & vbCrLf
        'scriptEnd = "-->" & vbCrLf & "</SCRIPT>"
        'scriptMiddle = "window.open ('" & strPageName & "', '" & strNewWinName & "', " & strPageOptions & ")" & vbCrLf
        'Page.RegisterStartupScript("ReportExportViewerPopUp", scriptBegin + scriptMiddle + scriptEnd)

        '   setup the properties of the new window
        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
        Dim name As String = "ReportExportViewer"
        Dim url As String = "../SY/" + name + ".aspx?ExportTo=" & ddlExportTo.SelectedValue.ToString
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
        'Session("LastPage") = 0
    End Sub


    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If Not (crViewer.ReportSource Is Nothing) Then
            ' It only searches for a text. 
            ' It does not position on the text if it's found, but it changes to page that contains the text.
           ' crViewer.SearchForText(tbSearchFor.Text, SearchDirection.Forward)
            crViewer.SearchAndHighlightText(tbSearchFor.Text, SearchDirection.Forward)

        End If
        'Session("LastPage") = 0
    End Sub

    Private Sub btnFriendlyPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFriendlyPrint.Click
        'Dim strPageName As String = "ReportFriendlyPrint.aspx"
        'Dim strPageOptions As String = "config='height=300px, width=780px, toolbar=no, menubar=yes, scrollbars=yes, resizable=yes, status=yes'"
        'Dim strNewWinName As String = "ReportFriendlyPrint" & Viewstate("resid")
        'Dim scriptBegin, scriptMiddle, scriptEnd As String
        'scriptBegin = "<SCRIPT LANGUAGE='javascript'>" & vbCrLf & "<!--" & vbCrLf
        'scriptMiddle = "window.open ('" & strPageName & "', '" & strNewWinName & "', " & strPageOptions & ")" & vbCrLf
        'scriptEnd = "-->" & vbCrLf & "</SCRIPT>"
        'Page.RegisterStartupScript("ReportFriendlyPrintPopUp", scriptBegin + scriptMiddle + scriptEnd)

        '   setup the properties of the new window
        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
        Dim name As String = "ReportFriendlyPrint"
        Dim url As String = "../SY/" + name + ".aspx"
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
        'Session("LastPage") = 0
    End Sub

#End Region

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        ''Added by Saraswathi lakshmanan to fix error 
        ''The report object was not found and it was trying to close the report object
        ''if condition added on July 30 2009
        If Not ReportSource Is Nothing Then
            'ReportSource.Close()
            ReportSource.Dispose()
        End If

    End Sub
End Class
