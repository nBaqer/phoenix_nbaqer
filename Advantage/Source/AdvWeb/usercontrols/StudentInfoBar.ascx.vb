﻿Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.BusinessFacade
Imports Newtonsoft.Json.Linq
Imports Telerik.Web.UI
Imports System.Data

Partial Class UserControls_StudentInfoBar
    Inherits UserControl

#Region "Variables"

    'Private strVID As String
    Private mruProvider As MRURoutines
    Public MasterCampusId As String
    Public Property StudentEnrollmentId As String 
    Public Sub New()
    End Sub

    Public Sub New(ByVal strStudentObjectPointer As String)
        '   strVID = strStudentObjectPointer
    End Sub

    Public Sub New(ByVal strStudentObjectPointer As String, ByVal campus As String)
        'strVID = strStudentObjectPointer
        MasterCampusId = campus
    End Sub

#End Region

#Region "Page Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        mruProvider = New MRURoutines(Context, myAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
    End Sub
  
    
    Public Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'Dim state As AdvantageSessionState
        Dim objStateInfo As AdvantageStateInfo
        Dim studentId As String
        Dim campusId As String
        Dim stuEnrollId As String

        'Dim state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        Dim objStudentState As StudentMRU = CType(Me.Page, BasePage).GetObjStudentState()
        If objStudentState Is Nothing OrElse objStudentState.StudentId = Nothing OrElse objStudentState.StudentId = Guid.Empty Then
            CType(Me.Page, BasePage).RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        If Not Session("RedirectFromMru") Is nothing 
            If(Not string.IsNullOrEmpty(Session("RedirectFromMRU").ToString()))
                Dim redirect = Session("RedirectFromMru")
                Session("RedirectFromMru") = string.empty
                Response.Redirect(redirect)
            End If
        End If
        studentId = objStudentState.StudentId.ToString()

        If not Session("CurrentEnrollmentId") Is Nothing And (Session("ShadowLeadId") = objStudentState.LeadId.ToString or Session("UseSelectedEnrollment") = True)
            stuEnrollId =   Session("CurrentEnrollmentId").ToString()
      
            else
            stuEnrollId = ""
                end if
        HttpContext.Current.Items("Language") = "En-US"

        'leadId = AdvantageSession.MasterLeadId
        'leadName = ""
        campusId = MasterCampusId

        If ( stuEnrollId <> "")

        objStateInfo = mruProvider.BuildStudentStatusBar(studentId, AdvantageSession.UserState.CampusId.ToString(), stuEnrollId)
            else
                objStateInfo = mruProvider.BuildStudentStatusBar(studentId, AdvantageSession.UserState.CampusId.ToString())
        End If
        'objStateInfo = mruProvider.BuildStudentStatusBar(studentId, campusId)

        If objStateInfo.StudentId Is Nothing Then
            CType(Me.Page, BasePage).RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        'Get the shadow lead and store in a hidden field

        ' Dim hiddenLead = CType(FindControl("shadowLead1"), HtmlInputHidden)
        'shadowLead1.Value = objStateInfo.StudentId






        'shadowLead1.Value = objStateInfo.LeadId ' MRUFacade.GetShadowLead(objStateInfo.StudentId).ToString()


        'shadowLead.Value = objStudentState.ShadowLead.ToString()

        ' Load the image
        Dim imageData() As Byte = StudentMasterFacade.GetStudentImage(studentId)
        If (imageData Is Nothing) Then
            studentphotoLt.Text = "<a id=""studentphoto"" href=""javascript:void(0)""><span class=""k-icon k-i-user k-icon-64 font-black""></span></a>"
        Else
            Dim base64String As String = Convert.ToBase64String(imageData, 0, imageData.Length)
            studentphotoLt.Text = "<img id=""studentphoto"" src=""" + Convert.ToString("data:image/png;base64,") & base64String + """ style=""width: 60px; height: 60px;""/>"
        End If
        Dim shadowLeadId = objStateInfo.LeadId
        If (String.IsNullOrWhiteSpace(shadowLeadId)) Then

            shadowLeadId = MRUFacade.GetShadowLead(objStateInfo.StudentId).ToString()
        End If
        Session("ShadowLeadId") = shadowLeadId

        With objStateInfo
            shadowLead1.Text = shadowLeadId
            hdnInfoBarCampusDescrip.Value = .CampusDescrip
            lblInfoBarNameValue.Text = .NameValue
            lblStatusValue.Text = .EnrollmentStatus
            lblIdentifierValue.Text = .StudentIdentifier
            lblProgramVersionValue.Text = .PrgVerDescrip
            lblInfoBarStartDateValue.Text = .StartDate.ToString("d")
            lblGradDateValue.Text = .GradDate.ToString("d")
            stuEnrollId = .StuEnrollId
            lbFaSapStatus.Text = .TitleIVStatustatus
            lbHold.Text = "HOLD"
            studentIdInfo.Value = .StudentId
            studentIdForHOLD.Text = .StudentId
            StudentEnrollmentId = stuEnrollId

            If stuEnrollId IsNot Nothing Then

                currentStudentEnrollment.Value =stuEnrollId
            End If
            Session("CurrentEnrollmentId") = stuEnrollId
        End With

        tcFaSapStatus.Visible = True
        If (lbFaSapStatus.Text = String.Empty) Then
            tcFaSapStatus.Visible = False
        ElseIf String.Compare(lbFaSapStatus.Text, "Title IV Passed") = 0 Then
            lbFaSapStatus.Attributes.Add("class", "greenButton")
        ElseIf String.Compare(lbFaSapStatus.Text, "Title IV Warning") = 0 Or String.Compare(lbFaSapStatus.Text, "Title IV Probation") = 0 Then
            lbFaSapStatus.Attributes.Add("class", "yellowButton")
        Else 'Title IV Ineligible
            lbFaSapStatus.Attributes.Add("class", "redButton")
        End If


        If IsNothing(objStateInfo.EnrollmentDetails) OrElse objStateInfo.EnrollmentDetails.Count < 1 Then
            tcActiveEnrollments.Visible = False

            tcEnrollmentCount.Visible = False
            'lbEnrollmentCountValue.Text = "No Active Enrollments"

        Else
            tcActiveEnrollments.Visible = True

            tcEnrollmentCount.Visible = True
            lbEnrollmentCountValue.Text = " Number of Enrollments: " & objStateInfo.EnrollmentDetails.Count.ToString
            BindActiveEnrollments(objStateInfo.EnrollmentDetails)
            SelectActiveEnrollment(stuEnrollId)
        End If

        Dim strDesc As String = Server.HtmlEncode("View Existing Students")
        existingStudentsLink.Value = "../PL/StudentSearch.aspx?resid=308&mod=AR&cmpid=" + Request.QueryString("cmpid") + "&desc=" + strDesc

        SetTaskRecipientProperties()

        Me.Page.ClientScript.RegisterStartupScript(Me.Page.GetType(), "sendJS", "<script language='javascript'>$(document).ready(function(){ getHold(); });</script>")
    End Sub


    ''' <summary>
    ''' Set properties in session related to task recipient.
    ''' Will be used by Email task window when user clicks the Email link 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetTaskRecipientProperties()

        Dim val As Integer

        val = CInt(System.Enum.Parse(GetType(AdvantageEntityId), AdvantageEntityId.Student.ToString()))

        AdvantageSession.TaskRecipientId = AdvantageSession.MasterStudentId
        AdvantageSession.TaskRecipientTypeId = val.ToString()
        AdvantageSession.TaskRecipientFullName = lblInfoBarNameValue.Text



        If Request.QueryString("mod") IsNot Nothing Then
            Select Case Request.QueryString("mod").ToString().ToUpper()
                Case "AD"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Admissions.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "AR"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Academics.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "FA"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.FinancialAid.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "HR"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.HumanResources.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "PL"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Placement.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "SA"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.StudentAccounts.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case Else
                    AdvantageSession.TaskRecipientModule = Nothing
            End Select
        End If
    End Sub

#End Region

#Region "Click Events"

    Protected Sub lbEnrollmentCountValue_Click(sender As Object, e As EventArgs) Handles lbEnrollmentCountValue.Click
        Dim url As String
        url = "~/AR/StudentEnrollments.aspx?resid=169&mod=AR&cmpid=" + Request.QueryString("cmpid") + "&vid=" + Request.QueryString("vid") + "&desc=Enrollments&vsi=" + Request.QueryString("vsi") + "&type=1"
        Response.Redirect(url)
    End Sub
    Protected Sub lbFaSapStatus_Click(sender As Object, e As EventArgs) Handles lbFaSapStatus.Click
        Dim url As String
        url = "~/AR/FASAPResults.aspx?resid=861&cmpid=" + Request.QueryString("cmpid") + "&desc=Title%20IV%20SAP%20Results&mod=FA"
        Response.Redirect(url)
    End Sub

    Protected Sub lbHold_Click(sender As Object, e As EventArgs) Handles lbHold.Click
        Dim url As String
        url = "~/AD/viewstudentgroups.aspx?resid=628&cmpid=" + Request.QueryString("cmpid") + "&desc=Student+Hold+Groups&mod=AR"
        Response.Redirect(url)
    End Sub

#End Region

#Region "Dropdown Events"

    Protected Sub rcbActiveEnrollments_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles rcbActiveEnrollments.SelectedIndexChanged
        Dim stuEnrollId = e.Value
        
        Dim objStudentState As StudentMRU = CType(Me.Page, BasePage).GetObjStudentState()
        Dim studentId = objStudentState.StudentId.ToString()
       DIm objStateInfo = mruProvider.BuildStudentStatusBar(studentId, AdvantageSession.UserState.CampusId.ToString(), stuEnrollId)

        With objStateInfo
            shadowLead1.Text = objStateInfo.LeadId
            hdnInfoBarCampusDescrip.Value = .CampusDescrip
            lblInfoBarNameValue.Text = .NameValue
            lblStatusValue.Text = .EnrollmentStatus
            lblIdentifierValue.Text = .StudentIdentifier
            lblProgramVersionValue.Text = .PrgVerDescrip
            lblInfoBarStartDateValue.Text = .StartDate.ToString("d")
            lblGradDateValue.Text = .GradDate.ToString("d")
            stuEnrollId = .StuEnrollId
            lbFaSapStatus.Text = .TitleIVStatustatus
            lbHold.Text = "HOLD"
            studentIdInfo.Value = .StudentId
            studentIdForHOLD.Text = .StudentId
            StudentEnrollmentId = stuEnrollId

            If stuEnrollId IsNot Nothing Then

            currentStudentEnrollment.Value =stuEnrollId
            End If
            Session("CurrentEnrollmentId") = stuEnrollId
            Session("UseSelectedEnrollment") = True
        End With

        Response.Redirect(Request.RawUrl)
    End Sub

 

#End Region

#Region "Private Functions"

    Private Sub BindActiveEnrollments(ByVal activeEnrollments As List(Of EnrollmentStateInfo))
        With rcbActiveEnrollments
            .DataSource = activeEnrollments
            .DataValueField = "StuEnrollId"
            .DataTextField = "PrgVerExtendedDescrip"
            .DataBind()
        End With
    End Sub

    Private Sub SelectActiveEnrollment(ByVal stuEnrollId As String)
        If Not rcbActiveEnrollments.IsEmpty Then
            If Not rcbActiveEnrollments.FindItemByValue(stuEnrollId) Is Nothing Then
                rcbActiveEnrollments.SelectedValue = stuEnrollId
                StudentEnrollmentId = stuEnrollId
            End If
        End If
        If stuEnrollId IsNot Nothing And stuEnrollId <> ""
                currentStudentEnrollment.Value = stuEnrollId
            StudentEnrollmentId = stuEnrollId

        End If
    End Sub

    Protected Sub NavigateToSearchPage(ByVal sender As Object, ByVal e As EventArgs)
        Dim strDesc As String = Server.HtmlEncode("View Existing Students")
        Response.Redirect("../PL/StudentSearch.aspx?resid=308&mod=AR&cmpid=" + Request.QueryString("cmpid") + "&desc=" + strDesc, True)
    End Sub


    Public sub SetStuEnrollment(ByVal  stuEnrollId As String)
        currentStudentEnrollment.Value = stuEnrollId
    End sub

    'Public Function GetStudentImagePath(ByVal strStudentId As String) As String
    '    Dim facade As New DocumentManagementFacade
    '    Dim strDocumentType As String = facade.GetLeadPhotoDocumentType(strStudentId, "Photo", "StudentId")
    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
    '    Dim strPath As String = myAdvAppSettings.AppSettings("StudentImagePath").ToString
    '    Dim strFileNameOnly As String = facade.GetStudentPhotoFileName(strStudentId, "Photo")
    '    Dim strFullPath As String = strPath + strDocumentType + "/" + strFileNameOnly
    '    Return strFullPath
    'End Function



#End Region

End Class
