﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Dhdr.ascx.vb" Inherits="usercontrols_Dhdr" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy> 

<div id="header" style="min-width:1015px;width:100%;height:95px;background-color:#FFFFFF">
<div id="othercontrolcontainer" style="height:70px;">
<asp:ImageButton ID="SchoolLogo" runat="server"  AlternateText="Default Dashboard"  PostBackUrl="~/dash.aspx?layout=start"
     ImageUrl="~/images/LogoUni.gif" style="float:left;margin: 10px 10px 10px 10px;"/>
   <div id="test111" style="float:right;margin:10px 15px 0px 0px;">

   <table>
    <tr>
        <td>
            <telerik:RadButton EnableSplitButton="true" ID="SplitButton" AutoPostBack="false" OnClientClicked="OnClientClicked"
            runat="server" Text="Jeffrey Spring"  style="margin:0px 0px 5px 0px;" Width="250px">
            </telerik:RadButton>
            <telerik:RadContextMenu ID="RadContextMenu1" runat="server" OnClientItemClicked="OnClientItemClicked">
            <Items>
            <telerik:RadMenuItem Text="User Settings" ImageUrl="images/gear.png" style="text-align:left;" Width="250px">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="My Roles" ImageUrl="images/hr.png" style="text-align:left;"  Width="250px">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="What's New" ImageUrl="images/InfoCircle.png" style="text-align:left;"  Width="250px">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="Help" ImageUrl="images/HelpIcon.png" style="text-align:left;"  Width="250px">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="Log Out" ImageUrl="images/icon_login.png" style="text-align:left;"  Width="250px" >
            </telerik:RadMenuItem>
            </Items>
            </telerik:RadContextMenu>
        </td>

    </tr>
    <tr>
    <td>
                <telerik:RadComboBox ID="CampusSelector" runat="server" Width="250px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="Fort Lauderdale"  />
                    <telerik:RadComboBoxItem runat="server" Text="Pompano Beach"  />
                    <telerik:RadComboBoxItem runat="server" Text="West Palm Beach"  />
                    <telerik:RadComboBoxItem runat="server" Text="Pembroke Pines"  />
                    <telerik:RadComboBoxItem runat="server" Text="North Miami"  />
                </Items>
            </telerik:RadComboBox>
    </td>
    </tr>
   </table>

        </div>
    <div id="searchdiv"style="float:right;margin:10px 250px 0px 0px;">
       <table>
    <tr>
    <td>
    </td>
        <td>
    <telerik:RadComboBox ID="DDLStudentSearch" Runat="server" ValidationGroup="studentsearch"     
    ShowMoreResultsBox="true" 
    EnableVirtualScrolling="True" ItemsPerRequest="10" AllowCustomText="False" 
    LoadingMessage="Searching..." 
    ShowToggleImage="False" Height="190px"        
    Width="250px" DropDownWidth="325px" OnItemsRequested="DDLStudentSearch_ItemsRequested" 
    onselectedindexchanged="DDLStudentSearch_SelectedIndexChanged" 
    OnItemDataBound="DDLStudentSearch_ItemDataBound"
    AutoPostBack="true"
    EnableLoadOnDemand="True" NoWrap="True"
    EnableEmbeddedScripts="True" 
    MarkFirstMatch="True" HighlightTemplatedItems="True" EmptyMessage="Search Name or SSN">
    <HeaderTemplate>
    <ul>
    <li class="col1">
    <asp:Label ID="hdrlblFullName" runat="server" Text="Full Name"></asp:Label></li>
    <li class="col2">
    <asp:Label ID="hdrlblStuNumType" runat="server" Text="SSN"></asp:Label></li>
    </ul>
    </HeaderTemplate>
    <ItemTemplate>
    <ul>
    <li class="col1">                    
    <asp:Label ID="lblFullName" runat="server"><%# DataBinder.Eval(Container.DataItem, "FullName")%></asp:Label></li>
    <li class="col2">
    <asp:Label ID="lblSSN" runat="server"><%# DataBinder.Eval(Container.DataItem, "SSN")%></asp:Label></li>
    </ul>
    </ItemTemplate>
    </telerik:RadComboBox>
        </td>
    </tr>
   </table>
    </div>

     </div>
    <div id="test44444" style="clear:both;"></div>



                        <telerik:RadToolBar ID="AdvToolBar" runat="server" Width="100%" OnButtonClick="AdvToolBar_ButtonClick"   > 
    <Items>
    <telerik:RadToolBarButton runat="server">
    <ItemTemplate>
    <div id="BreadCrumbs1" style="width:650px;">
    <asp:Image runat="server" ID="bread" ImageUrl="images\dashboard_icon.png" ImageAlign="AbsMiddle" style="margin:0px 5px 0px 0px;"/>
    <asp:literal runat="server" ID="bc1" Text="Dashboards /"></asp:literal>  <asp:HyperLink runat="server" ID="BC2" ForeColor="White">Director of Admissions</asp:HyperLink>
    </div>
    </ItemTemplate>
    </telerik:RadToolBarButton>
<%--    <telerik:RadToolBarSplitButton runat="server" ID="SearchType" CssClass="rtbImageOnly">
    <Buttons>
    <telerik:RadToolBarButton runat="server" Owner=""  Value="1"  ImageUrl="images/hr2.png">
    </telerik:RadToolBarButton>
    <telerik:RadToolBarButton runat="server"  Owner="" Value="2" ImageUrl="images/gear1.png">
    </telerik:RadToolBarButton>
    </Buttons>
    </telerik:RadToolBarSplitButton>--%>
        </Items>
    </telerik:RadToolBar>
       <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function OnClientClicked(sender, args) {

                if (args.IsSplitButtonClick() || !sender.get_commandName()) {
                    var currentLocation = $telerik.getLocation(sender.get_element());
                    var contextMenu = $find("<%=RadContextMenu1.ClientID%>");
                    contextMenu.showAt(currentLocation.x, currentLocation.y + 22);
                } else if (sender.get_commandName() == "TransferRight") {
                    //transferRight();
                }
                else {
                   // transferLeft();
                }
            }

            function OnClientItemClicked(sender, args) {
                var itemText = args.get_item().get_text();
                var splitButton = $find("<%=SplitButton.ClientID%>");
                if (itemText == "Transfer Right") {
                    transferRight();
                    splitButton.set_text("Transfer Right");
                    splitButton.set_commandName("TransferRight");
                }
                else if (itemText == "Transfer Left") {
                    transferLeft();
                    splitButton.set_text("Transfer Left");
                    splitButton.set_commandName("TransferLeft");
                }
            }

        </script>
    </telerik:RadCodeBlock>
<%--      <asp:SqlDataSource ID="StudentSearchDataSource" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DynRptDemoConnectionString %>" SelectCommand="Select Top 10 * from NewStudentSearch"></asp:SqlDataSource>--%>

</div>