﻿Imports System.Diagnostics
Imports FAME.Advantage.Site.Lib.Infrastruct.Helpers
Partial Class usercontrols_MainMenu
    Inherits System.Web.UI.UserControl

    Public ReadOnly Property CurrentCampusId() As String
        Get
            Dim currentcamp As String = Session("CurrentCampus")
            Return currentcamp
        End Get
    End Property

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Page.IsPostBack Then
            AdvantageSession.UserId = AdvantageSession.UserState.UserId.ToString
            AdvantageSession.BuildVersion = GetBuildVersion()

            Dim parameter As String = Request("__EVENTARGUMENT")
            Dim control As String = Request("__EVENTTARGET")
            If Not (control Is Nothing) AndAlso control.ToUpper = "MASTERCOMBOBOXS" AndAlso Not (parameter Is Nothing) AndAlso parameter = "5Out" Then
                'Log Out option in user panel bar.
                Session.Abandon()
                FormsAuthentication.SignOut()
                MultiTenantHostHelper.RedirectToLogin()
            End If
        End If
    End Sub

    Public Function GetBuildVersion() As String
        Dim versionString As String
        Try
            Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Server.MapPath("~/Bin/Common.dll"))
            versionString = myFileVersionInfo.FileVersion.ToString()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            versionString = String.Empty
        End Try
        Return versionString
    End Function


End Class

