﻿Imports FluentNHibernate.Conventions.AcceptanceCriteria
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Logic.Layer

Partial Class UserControls_EmployerInfoBar
    Inherits System.Web.UI.UserControl
    Private strVID As String
    Dim EmployerId As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal strEmployerObjectPointer As String)
        strVID = strEmployerObjectPointer
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    End Sub
    'Public Function GetStudentImagePath(ByVal strStudentId As String) As String
    '    Dim facade As New DocumentManagementFacade
    '    Dim strDocumentType As String = facade.GetLeadPhotoDocumentType(strStudentId, "Photo", "StudentId")
    '    Dim strPath As String = SingletonAppSettings.AppSettings("StudentImagePath") 'SingletonAppSettings.AppSettings("DocumentPath")
    '    Dim strFileNameOnly As String = facade.GetStudentPhotoFileName(strStudentId, "Photo")
    '    Dim strFullPath As String = strPath + strDocumentType + "/" + strFileNameOnly
    '    Return strFullPath

    'End Function
    Protected Sub NavigateToSearchPage(ByVal sender As Object, ByVal e As EventArgs)

        Dim uSearchStr As String = Nothing
        Dim uSearchEntity As HiddenField = CType(Page.Master.FindControl("hdnCurrentEntityId"), HiddenField)
        If uSearchEntity IsNot Nothing Then
            uSearchStr = uSearchEntity.Value
        End If

        Dim strDesc As String = Server.HtmlEncode("View Existing Employers")
        'Response.Redirect("../PL/EmployerSearch.aspx?resid=197&mod=PL&cmpid=" + Request.QueryString("cmpid") + "&desc=" + strDesc + "&Type=2" + "&bar=hidden", True)

        If String.IsNullOrEmpty(uSearchStr) Then
            Response.Redirect("../PL/EmployerSearch.aspx?resid=197&mod=PL&cmpid=" + Request.QueryString("cmpid") + "&desc=" + strDesc, True)
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        HttpContext.Current.Items("Language") = "En-US"

        'objStateInfo = state(strVID)
        EmployerId = AdvantageSession.MasterEmployerId
        lblNameValue.Text = AdvantageSession.MasterEmpName




        Dim strAddress As String = ""
        If Not AdvantageSession.MasterEmpAddress1 Is Nothing Then
            strAddress = AdvantageSession.MasterEmpAddress1
            If AdvantageSession.MasterEmpAddress2 Is Nothing Then
                strAddress &= " " 'Line Break
            End If
        End If
        If Not AdvantageSession.MasterEmpAddress2 Is Nothing Then
            strAddress &= AdvantageSession.MasterEmpAddress2
        End If
        If Not AdvantageSession.MasterEmpCity Is Nothing Then
            If AdvantageSession.MasterEmpAddress1 Is Nothing Or
                Not AdvantageSession.MasterEmpAddress2 Is Nothing Then
                strAddress &= ", " 'Line Break
            End If
            strAddress &= AdvantageSession.MasterEmpCity
        End If
        If Not AdvantageSession.MasterEmpState Is Nothing Then
            If Not AdvantageSession.MasterEmpCity Is Nothing Then
                strAddress &= ", " 'Line Break
            End If
            strAddress &= AdvantageSession.MasterEmpState
        End If
        If Not AdvantageSession.MasterEmpZip Is Nothing Then
            If Not AdvantageSession.MasterEmpCity Is Nothing Or
                Not AdvantageSession.MasterEmpState Is Nothing Then
                strAddress &= " - " 'Line Break
            End If
            strAddress &= AdvantageSession.MasterEmpZip
        End If
        lblAddressValue.Text = strAddress
        If Not AdvantageSession.MasterEmpPhone Is Nothing Then
            If Not AdvantageSession.MasterEmpPhone = String.Empty And AdvantageSession.MasterEmpPhone.Length >= 10 Then
                lblPhone.Text = "(" + AdvantageSession.MasterEmpPhone.Substring(0, 3) + ") " + AdvantageSession.MasterEmpPhone.Substring(3, 3) + "-" + AdvantageSession.MasterEmpPhone.Substring(6, 4)
            Else
                lblPhone.Text = AdvantageSession.MasterEmpPhone
            End If
        Else
            lblPhone.Text = String.Empty
        End If

        SetTaskRecipientProperties()

    End Sub

    ''' <summary>
    ''' Set properties in session related to task recipient.
    ''' Will be used by Email task window when user clicks the Email link 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetTaskRecipientProperties()

        Dim val As Integer
        val = CInt(System.Enum.Parse(GetType(AdvantageEntityId), AdvantageEntityId.Employers.ToString()))

        AdvantageSession.TaskRecipientId = AdvantageSession.MasterEmployerId
        AdvantageSession.TaskRecipientTypeId = val.ToString()
        AdvantageSession.TaskRecipientFullName = lblNameValue.Text



        If Request.QueryString("mod") IsNot Nothing Then
            Select Case Request.QueryString("mod").ToString().ToUpper()
                Case "AD"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Admissions.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "AR"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Academics.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "FA"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.FinancialAid.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "HR"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.HumanResources.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "PL"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Placement.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case "SA"
                    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.StudentAccounts.ToString()))
                    AdvantageSession.TaskRecipientModule = val.ToString()
                Case Else
                    AdvantageSession.TaskRecipientModule = Nothing
            End Select
        End If
    End Sub
End Class

