﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Advantage_Logo.ascx.vb" Inherits="usercontrols_Advantage_Logo" %>
<style type="text/css">
    .paused {
        -webkit-animation-play-state: paused !important; /* Safari 4.0 - 8.0 */
        animation-play-state: paused !important;
    }

    .resetInvis {
        opacity: 0;
    }

    .resetSlide {
        transform: translateX(-100px);
    }

    .resetFall {
        transform: translateY(-100px);
    }

    .running {
        -webkit-animation-play-state: running !important; /* Safari 4.0 - 8.0 */
        animation-play-state: running !important;
    }

    #advantage_logo_svg:hover {
        cursor: pointer;
    }

    #pulse {
        animation: slide 1s ease-in-out forwards;
        animation-delay: 0.6s;
    }

    #letter-A1 {
        animation: fadeIn 1s forwards;
        animation-delay: 0.3s;

    }

    #letter-D1 {
        animation: fadeIn 1s forwards;
        animation-delay: 0.6s;
    }

    #trademark-T {
        animation: fadeIn 1s forwards;
        animation-delay: 3.6s;
    }

    #trademark-M {
        animation: fadeIn 1s forwards;
        animation-delay: 3.6s;
    }

    #triangle-solo-1 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 1.3s,1.6s;
    }

    #triangle-tri-1 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 1.9s,2.2s;
    }

    #triangle-tri-2 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 1.5s,1.8s;
    }

    #triangle-tri-3 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 1.7s,2.0s;
    }

    #triangle-six-1 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 3.1s,3.4s;
    }

    #triangle-six-2 {
        animation: freeFall 0.3s forwards,bounce 0.3s;
        animation-delay: 2.7s,3.0s;
    }

    #triangle-six-3 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 2.9s,3.2s;
    }

    #triangle-six-4 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 2.1s,2.4s;
    }

    #triangle-six-5 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 2.3s,2.6s;
    }

    #triangle-six-6 {
        animation: freeFall 0.3s forwards, bounce 0.3s;
        animation-delay: 2.5s,2.8s;
    }

    @keyframes slide {
        0% {
            transform: translateX(-100px);
        }

        100% {
            transform: translateX(0);
        }
    }

    @keyframes fadeIn {
        0% {
            opacity: 0;
        }

        100% {
            opacity: 1;
        }
    }

    @keyframes freeFall {
        0% {
            transform: translateY(-100px);
        }

        100% {
            transform: translateY(0);
        }
    }

    @keyframes bounce {
        0% {
            transform: translateY(0)
        }

        50% {
            transform: translateY(-4px)
        }

        100% {
            transform: translateY(0)
        }
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {

        var animatedElements = []
        animatedElements.push($("#triangle-group").find(".triangle"));
        animatedElements.push($("#pulse"));
        animatedElements.push($("#letter-A1"));
        animatedElements.push($("#letter-D1"));
        animatedElements.push($("#trademark-T"));
        animatedElements.push($("#trademark-M"));

        var lastElement = animatedElements[animatedElements.length - 1];
        function pauseElements() {
            $.each(animatedElements, function (i, val) {
                var ele = $(val);
                ele.removeClass("running").addClass("paused");
            })

        }

        function resetElements() {
            $.each(animatedElements, function (i, val) {
                var ele = $(val);
                ele.removeClass("paused");
                if (ele.hasClass("triangle")) {
                    ele.addClass("resetFall");
                }
                 else if (ele.hasClass("pulse")) {
                    ele.addClass("resetSlide");
                }
                 else if (ele.hasClass("fade")) {
                    ele.addClass("resetInvis");
                }
            })
        }
        function restartAnimation() {
            var logo = $("#advantage_logo_svg")
            newone = logo.clone(true);
            logo.before(newone);
            logo.remove();
        }


        function homeLink() {
            var currentCampusId = $.QueryString["cmpid"];
            var queryString;

            if (currentCampusId) queryString = "?resid=264&mod=SY&cmpid=" + currentCampusId + "&desc=dashboard";
            else queryString = "?resid=264&mod=SY" + "&desc=dashboard";

            location.href = window.XMASTER_GET_BASE_URL + "/dash.aspx" + queryString;
        };

        $(document).on("mouseover", "#advantage_logo_svg", function () {
            if (lastElement.hasClass("paused")) {
                resetElements();
                restartAnimation();
            }

        })

        $(document).on('click', "#advantage_logo_svg", function (e) {
            homeLink();
        })
        lastElement.on('webkitAnimationEnd', function () {
            pauseElements();
        });

    })

</script>

<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="150" height="50" viewBox="0 0 39.687 16.933" id="advantage_logo_svg">
    <defs id="defs2">
        <linearGradient id="linearGradient4210">
            <stop offset="0" id="stop4212" stop-color="#ececec" />
            <stop offset="1" id="stop4214" stop-color="#ececec" stop-opacity="0" />
        </linearGradient>
        <linearGradient id="linearGradient9117">
            <stop offset="0" id="stop9113" stop-color="#ececec" />
            <stop offset="1" id="stop9115" stop-color="#ececec" stop-opacity="0" />
        </linearGradient>
        <linearGradient xlink:href="#linearGradient9117" id="linearGradient9119" x1="55.058" y1="293.759" x2="73.766" y2="293.759" gradientUnits="userSpaceOnUse" gradientTransform="matrix(.9178 0 0 .55556 -117.415 130.226)" />
        <linearGradient xlink:href="#linearGradient4210" id="linearGradient4216" x1="49.77" y1="293.425" x2="66.941" y2="293.425" gradientUnits="userSpaceOnUse" gradientTransform="matrix(-1 0 0 1 116.71 0)" />
    </defs>
    <g id="layer1" transform="translate(0 -280.067)">
        <g id="g3721" transform="translate(-44.367 -6.749)">
            <text style="line-height: 0%" x="46.008" y="301.811" id="text3781" font-weight="400" font-family="sans-serif" letter-spacing="0" word-spacing="0" stroke-width=".265">
                <tspan id="tspan3779" x="46.008" y="301.811" style="line-height: 1.25" font-size="10.583"></tspan>
            </text>
            <text style="line-height: 0%" x="47.278" y="298.002" id="text3785" font-weight="400" font-family="sans-serif" letter-spacing="0" word-spacing="0" stroke-width=".265">
                <tspan id="tspan3783" x="47.278" y="298.002" style="line-height: 1.25" font-size="10.583"></tspan>
            </text>
            <text style="line-height: 0%" x="45.34" y="296.399" id="text3789" font-weight="400" font-family="sans-serif" letter-spacing="0" word-spacing="0" stroke-width=".265">
                <tspan id="tspan3787" x="45.34" y="296.399" style="line-height: 1.25" font-size="6.35"></tspan>
            </text>
            <text style="line-height: 0%" x="47.077" y="293.459" id="text3793" font-weight="400" font-family="sans-serif" letter-spacing="0" word-spacing="0" stroke-width=".265">
                <tspan id="tspan3791" x="47.077" y="293.459" style="line-height: 1.25" font-size="7.761"></tspan>
            </text>
            <text style="line-height: 0%" x="46.142" y="293.793" id="text3797" font-weight="400" font-family="sans-serif" letter-spacing="0" word-spacing="0" stroke-width=".265">
                <tspan id="tspan3795" x="46.142" y="293.793" style="line-height: 1.25" font-size="5.644"></tspan>
            </text>
            <text style="line-height: 0%" x="48.347" y="294.06" id="text3801" font-weight="400" font-family="sans-serif" letter-spacing="0" word-spacing="0" stroke-width=".265">
                <tspan id="tspan3799" x="48.347" y="294.06" style="line-height: 1.25" font-size="5.644"></tspan>
            </text>

            <g id="g9267" transform="translate(-1.99 .04)">
                <g id="g9461">
                    <g id="g9292" font-weight="400" font-family="sans-serif" letter-spacing="-.397" word-spacing="0" stroke-width=".265">
                        <g id="letters-AD" fill="#ff7f2a">
                            <path d="M51.797 299.071h-.554l-.138-1.154h-1.769l-.673 1.154h-.576l2.442-4.103h.722zm-.752-1.6l-.257-2.027-1.19 2.026z" style="line-height: 1.25" id="letter-A1" class="paused fade" />
                            <path d="M55.97 296.541q0 .673-.342 1.274-.342.6-.934.928-.364.196-.744.262t-.863.066h-1.108l.948-4.103h.954q.446 0 .813.063.37.06.675.264.292.196.447.513.154.314.154.733zm-.576.047q0-.314-.113-.54-.11-.229-.336-.38-.224-.15-.477-.199-.254-.052-.642-.052h-.455l-.744 3.205h.57q.394 0 .703-.063.311-.066.587-.224.444-.253.675-.73.232-.48.232-1.017z" style="line-height: 1.25" id="letter-D1" class="paused fade" />
                        </g>
                        <path class="paused" id="letter-V1" style="line-height: 1.25; -inkscape-font-specification: 'sans-serif Italic'" d="M60.038 294.968l-2.442 4.103h-.728l-.545-4.103h.567l.45 3.605 2.116-3.605z" font-style="italic" fill="#f9f9f9" />
                        <path class="paused" id="letter-A2" style="line-height: 1.25; -inkscape-font-specification: 'sans-serif Italic'" d="M62.644 298.974l-.554.016-.17-1.15-1.769.049-.64 1.173-.576.016 2.327-4.17.722-.02zm-.797-1.58l-.313-2.017-1.133 2.058z" font-style="italic" fill="#f9f9f9" />
                        <path class="paused" id="letter-N1" style="line-height: 1.25; -inkscape-font-specification: 'sans-serif Italic'" d="M65.96 299.071h-.65l-1.122-3.67-.848 3.67h-.516l.948-4.103h.822l1.025 3.351.774-3.351h.516z" font-style="italic" fill="#f9f9f9" />
                        <path class="paused" id="letter-T1" style="line-height: 1.25; -inkscape-font-specification: 'sans-serif Italic'" d="M70.42 295.433h-1.464l-.84 3.638h-.551l.84-3.638h-1.463l.105-.465h3.478z" font-style="italic" fill="#f9f9f9" />
                        <path class="paused" id="letter-A3" style="line-height: 1.25; -inkscape-font-specification: 'sans-serif Italic'" d="M73.019 299.071h-.554l-.138-1.154h-1.77l-.672 1.154h-.576l2.442-4.103h.722zm-.752-1.6l-.257-2.027-1.19 2.026z" font-style="italic" fill="#f9f9f9" />
                        <path class="paused" id="letter-G1" style="line-height: 1.25; -inkscape-font-specification: 'sans-serif Italic'" d="M75.174 299.151q-.849 0-1.325-.419-.477-.419-.477-1.19 0-.568.173-1.05.177-.483.51-.84.331-.356.81-.558.483-.2 1.084-.2.36 0 .719.087.361.086.736.281l-.124.623h-.05q-.306-.286-.667-.413-.358-.13-.771-.13-.455 0-.797.18-.342.179-.576.476-.229.292-.347.681-.116.389-.116.783 0 .592.322.915.326.322.96.322.278 0 .52-.05.246-.052.46-.132l.232-1.05h-1.094l.105-.46h1.642l-.407 1.761-.328.135q-.146.058-.367.122-.19.055-.394.09-.201.036-.433.036z" font-style="italic" fill="#f9f9f9" />
                        <path class="paused" id="letter-E1" style="line-height: 1.25; -inkscape-font-specification: 'sans-serif Italic'" d="M80.83 294.968l-.108.465h-2.158l-.264 1.158h2.158l-.108.463h-2.158l-.358 1.552h2.158l-.108.465h-2.703l.945-4.103z" font-style="italic" fill="#f9f9f9" />
                    </g>
                    <g id="top-group">
                        <path d="M66.94 293.258H49.77v.334h17.17z" id="pulse" class="paused pulse" fill="url(#linearGradient4216)" stroke-width=".189" />
                        <g id="triangle-group" transform="matrix(1.23137 0 0 1.23137 -17.987 -67.907)" fill="#f2f2f2">
                            <g id="triangle-tri" transform="matrix(.11623 0 0 .127 72.414 257.526)">
                                <path d="M12.7 276.619l-7.78-.051 3.935-6.713z" id="triangle-tri-1" class="triangle paused" stroke-width=".265" />
                                <path d="M7.749 284.442l-7.781-.051 3.935-6.713z" id="triangle-tri-2" class="triangle paused" stroke-width=".265" />
                                <path d="M17.942 284.43l-7.78-.052 3.935-6.713z" id="triangle-tri-3" class="triangle paused" stroke-width=".265" />
                                <g id="triangle-solo">
                                    <path class="triangle paused" id="triangle-solo-1" d="M-9.614 284.065l-7.78-.052 3.934-6.712z" stroke-width=".265" />
                                </g>
                            </g>
                            <g id="triangle-tri-5" transform="matrix(.11623 0 0 .127 76.223 256.484)">
                                <g id="triangle-six">
                                    <path class="triangle paused" id="triangle-six-1" d="M12.7 276.619l-7.78-.051 3.935-6.713z" stroke-width=".265" />
                                    <path class="triangle paused" id="triangle-six-2" d="M7.749 284.442l-7.781-.051 3.935-6.713z" stroke-width=".265" />
                                    <path class="triangle paused" id="triangle-six-3" d="M17.942 284.43l-7.78-.052 3.935-6.713z" stroke-width=".265" />
                                    <path class="triangle paused" id="triangle-six-4" d="M2.21 292.368l-7.78-.051 3.934-6.713z" stroke-width=".265" />
                                    <path class="triangle paused" id="triangle-six-5" d="M13.42 292.631l-7.78-.051 3.935-6.713z" stroke-width=".265" />
                                    <path class="triangle paused" id="triangle-six-6" d="M23.625 292.631l-7.78-.051 3.934-6.713z" stroke-width=".265" />
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
            <g transform="translate(.329 -.322)" id="text65" style="line-height: 1.25; -inkscape-font-specification: 'sans-serif Italic'" aria-label="TM" font-style="italic" font-weight="400" font-size="2.117" font-family="sans-serif" letter-spacing="0" word-spacing="0" fill="#f9f9f9" stroke-width=".265">
                <path class="paused fade" id="trademark-T" style="-inkscape-font-specification: 'sans-serif Italic'" d="M80.45 294.4h-.55l-.315 1.364h-.207l.316-1.364h-.55l.04-.175h1.304z" />
                <path class="paused fade" id="trademark-M" style="-inkscape-font-specification: 'sans-serif Italic'" d="M81.714 295.764h-.207l.304-1.308-.625.884h-.122l-.217-.924-.312 1.348h-.193l.355-1.54h.281l.208.858.6-.857h.284z" />
            </g>
        </g>
    </g>
</svg>
