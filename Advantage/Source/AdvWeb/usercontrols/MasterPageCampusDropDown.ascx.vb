﻿Imports System

Namespace AdvWeb.usercontrols

    Partial Public Class UsercontrolsMasterPageCampusDropDown
        Inherits UserControl

#Region " Class event, this event is raised when the DropDownButton change its value. The event is raised inside the Control_Load Control Event"
        ' The MasterPageEventHandler event
        Public Event MasterPageCampusDropDownHandler As EventHandler(Of MasterPageCampusDropDownEventArgs)

        ' Raise the MasterPageCampusDropDown event
        Protected Sub OnMasterPageCampusDropDown(args As MasterPageCampusDropDownEventArgs)

            RaiseEvent MasterPageCampusDropDownHandler(Me, args)
        End Sub

#End Region

#Region "Properties"

        ''' <summary>
        ''' Return Current Campus ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks>Updated from the Init when and the setter of the property is called.</remarks>
        Public Property CurrentCampusId() As String
            Get
                Return MpCurrentSessionId.Value
            End Get
            Set(value As String)
                If (MpPreviousSessionId.Value <> value) Then
                    MpPreviousSessionId.Value = MpCurrentSessionId.Value
                End If
                MpCurrentSessionId.Value = value
            End Set

        End Property

        ''' <summary>
        ''' Return Previous Campus ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks>Read only, this property is updated in the Init Event of Control.</remarks>
        Public ReadOnly Property PreviusCampusId() As String
            Get
                Return MpPreviousSessionId.Value
            End Get
        End Property

        Public Property SearchCampus() As HiddenField

#End Region

#Region "Control Events..."

        ''' <summary>
        ''' Initiate the two hidden fields that hold all the manipulation of the CampusId
        ''' This hidden field should be used through the properties.
        ''' </summary>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnInit(ByVal e As EventArgs)

            ' this assigns Page_PreLoad as the event handler 
            ' for the PreLoad event of the Control's Page property
            AddHandler Page.PreLoad, AddressOf PagePreLoad
            MyBase.OnInit(e)

        End Sub

        Private Sub PagePreLoad(sender As Object, e As EventArgs)


            Dim uSearchCampus As HiddenField = SearchCampus


            Dim campusQueryString As Guid = CampusObjects.GetQueryParameterValueGuidCampusId(Request)
            If campusQueryString = Guid.Empty Then
                'This occurred the first time when CampusId is not in the query string.
                MpCurrentSessionId.Value = AdvantageSession.UserState.CampusId.ToString()
                AdvantageSession.TemporalPreviousCampusId = MpCurrentSessionId.Value
                MpPreviousSessionId.Value = AdvantageSession.TemporalPreviousCampusId
                campusQueryString = AdvantageSession.UserState.CampusId
            End If

            If Page.IsPostBack Then

                If Not String.IsNullOrEmpty(uSearchCampus.Value) Then
                    MpPreviousSessionId.Value = MpCurrentSessionId.Value
                    MpCurrentSessionId.Value = uSearchCampus.Value
                    AdvantageSession.UserState.CampusId = New Guid(uSearchCampus.Value.ToString())

                    Session("hdnSearchCampusId") = uSearchCampus.Value
                    Session("previousCampusId") = MpPreviousSessionId.Value

                End If

                If MpCurrentSessionId.Value Is Nothing OrElse MpCurrentSessionId.Value = String.Empty Then
                    MpCurrentSessionId.Value = AdvantageSession.UserState.CampusId.ToString()
                End If

                If (MpPreviousSessionId.Value <> MpCurrentSessionId.Value) Then
                    AdvantageSession.TemporalPreviousCampusId = MpPreviousSessionId.Value
                End If
            Else
                MpPreviousSessionId.Value = AdvantageSession.TemporalPreviousCampusId

                If Session("hdnSearchCampusId") IsNot Nothing _
                AndAlso (Session("hdnSearchCampusId").ToString() <> String.Empty) Then
                    MpCurrentSessionId.Value = Session("hdnSearchCampusId").ToString()
                    AdvantageSession.TemporalPreviousCampusId = MpCurrentSessionId.Value
                    AdvantageSession.UserState.CampusId = New Guid(MpCurrentSessionId.Value)
                Else
                    MpCurrentSessionId.Value = campusQueryString.ToString()
                    AdvantageSession.TemporalPreviousCampusId = MpCurrentSessionId.Value
                    AdvantageSession.UserState.CampusId = New Guid(MpCurrentSessionId.Value)
                End If

                Session("hdnSearchCampusId") = Nothing
                Session("previousCampusId") = Nothing
            End If


            uSearchCampus.Value = ""



            'If Page.IsPostBack Then

            '    If MpCurrentSessionId.Value Is Nothing OrElse MpCurrentSessionId.Value = String.Empty Then
            '        MpCurrentSessionId.Value = AdvantageSession.UserState.CampusId.ToString()
            '    End If
            '    AdvantageSession.TemporalPreviousCampusId = MpCurrentSessionId.Value
            'Else
            '    Dim campusQueryString As Guid = CampusObjects.GetQueryParameterValueGuidCampusId(Request)
            '    If Not campusQueryString = Guid.Empty Then
            '        MpCurrentSessionId.Value = campusQueryString.ToString()
            '    Else
            '        'This occurred the first time when CampusId is not in the query string.
            '        MpCurrentSessionId.Value = AdvantageSession.UserState.CampusId.ToString()
            '        AdvantageSession.TemporalPreviousCampusId = MpCurrentSessionId.Value 
            '    End If
            '    MpPreviousSessionId.Value = AdvantageSession.TemporalPreviousCampusId
            'End If



        End Sub

        Protected Sub Control_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

            'SetSelectedCampusInSession()
            If Page.IsPostBack Then
                Dim parameter As String = Request("__EVENTARGUMENT")
                Dim control As String = Request("__EVENTTARGET")
                If Not (control Is Nothing) AndAlso control.ToUpper = "MASTERCAMPUSDROPDOWN" Then
                    'Event was raised in DropDownList. Raise then the control event...
                    Dim separator As String() = {","}
                    Dim guids As String() = parameter.Split(separator, 2, StringSplitOptions.None)
                    If guids.GetUpperBound(0) = 1 Then
                        Dim eventargs As MasterPageCampusDropDownEventArgs = New MasterPageCampusDropDownEventArgs()
                        eventargs.CurrentCampusId = Guid.Parse(guids(0))
                        eventargs.OldCampusId = Guid.Parse(guids(1))
                        OnMasterPageCampusDropDown(eventargs)

                        Session("ddlCampusValue") = eventargs.CurrentCampusId.ToString()
                    End If



                End If
            End If
        End Sub

        ''' <summary>
        ''' This eliminate the message condition for change the campus.
        ''' </summary>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnUnload(ByVal e As EventArgs)
            MyBase.OnUnload(e)
        End Sub


        'The value it got from the Query string to considerer that the user has two different dash open with to differents campus....
        'Protected Sub SetSelectedCampusInSession()
        '    Try
        '        Dim queryString As String = String.Empty
        '        If Not Request Is Nothing Then
        '            queryString = Request.QueryString("VSI")
        '        End If

        '        queryString = queryString & "CurrentCampus"
        '        MpRandomSessionId.Value = queryString ' Store the name of the random session plus CurrentCampus in this hidden field.
        '        If Session(queryString) Is Nothing OrElse String.IsNullOrEmpty(Session(queryString).ToString) Then
        '            Session(queryString) = AdvantageSession.UserState.CampusId.ToString
        '        End If
        '    Catch ex As Exception
         '    	Dim exTracker = new AdvApplicationInsightsInitializer()
        '    	exTracker.TrackExceptionWrapper(ex)

        '        Response.Redirect(FormsAuthentication.LoginUrl)
        '    End Try
        'End Sub

#End Region

    End Class

#Region "EventArgs descendant class to be used with the control event"
    ' Event argument class for the MatchFound event
    'This is used for the component event.
    Public Class MasterPageCampusDropDownEventArgs
        Inherits EventArgs

        Public OldCampusId As Guid
        Public CurrentCampusId As Guid

        Public Sub New()
            ' TODO: Insert constructor code here

        End Sub
    End Class

#End Region
End Namespace