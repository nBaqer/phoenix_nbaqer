﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="YearDropDown.ascx.vb" Inherits="usercontrols_YearDropDown" %>

<div id="YearSelector" class="CampusSelector MultiFilterReportContainer"  runat="server" style="padding-left:2px;">
    <div id="YearHeader" class="CaptionLabel" runat="server">Year <font color="red">*</font></div>

    <telerik:RadComboBox ID="RadComboYear" runat="server" AutoPostBack="true" Width="350px" tooltip="Select a year to run the report"  CssClass="ManualInput ReportLeftMarginInput">
    </telerik:RadComboBox>
</div>

