﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StateDropDown.ascx.vb" Inherits="usercontrols_StateDropDown" %>

<div id="StateSelector" class="StateSelector MultiFilterReportContainer" runat="server" style="padding-left: 2px;">
    <div id="StateHeader" class="CaptionLabel" runat="server">State <font color="red">*</font></div>

    <telerik:RadComboBox ID="RadComboState" runat="server" AutoPostBack="true"
        Width="350px" ToolTip="Select a state to run the report" OnSelectedIndexChanged="RadComboState_OnSelectedIndexChanged"  CssClass="ManualInput ReportLeftMarginInput">
    </telerik:RadComboBox>
</div>
