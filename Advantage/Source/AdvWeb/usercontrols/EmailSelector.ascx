<%@ Control Language="vb" AutoEventWireup="false" Inherits="EmailSelector" CodeFile="EmailSelector.ascx.vb" %>
<HTML>
<body>
	<asp:panel id="pnlBigOne" runat="server" CssClass="emailFrame" HorizontalAlign="Center">
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
			<TBODY>
				<TR>
					<TD class="listframetop">
						<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
							<TR>
								<TD class="employersearch">
									<asp:label id="Label1" CssClass="label" runat="server">Type</asp:label></TD>
								<TD class="employersearch2">
									<asp:dropdownlist id="ddlTypes" runat="server" AutoPostBack="True" cssclass="DropDownList">
										<asp:ListItem Value="All Students">All Students</asp:ListItem>
										<asp:ListItem Value="Students">Students</asp:ListItem>
										<asp:ListItem Value="All Employees">All Employees</asp:ListItem>
										<asp:ListItem Value="Employees">Employees</asp:ListItem>
										<asp:ListItem Value="All Employers Contacts">All Employers Contacts</asp:ListItem>
										<asp:ListItem Value="Employers Contacts">Employers Contacts</asp:ListItem>
										<asp:ListItem Value="All Leads">All Leads</asp:ListItem>
										<asp:ListItem Value="Leads">Leads</asp:ListItem>
									</asp:dropdownlist></TD>
							</TR>
						</TABLE>
						<asp:panel id="pnlStudentFilter" CssClass="emailframe" runat="server">
							<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
								<TR>
									<TD class="employersearch">&nbsp;</TD>
									<TD class="employersearch2" style="TEXT-ALIGN: center">
										<asp:label id="lblStudentFilter" CssClass="label" runat="server" Font-Bold="True">Student Filter</asp:label></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblStudentStatus" CssClass="label" runat="server">Status</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlStudentStatusId" CssClass="dropdownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblStudentProgram" CssClass="Label" runat="server">Program</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlStudentPrgVerId" CssClass="DropDownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblStudentCampus" CssClass="Label" runat="server">Campus</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlStudentCampGrpId" CssClass="dropdownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
							</TABLE>
						</asp:panel>
						<asp:panel id="pnlEmployeeFilter" CssClass="emailframe" runat="server" Visible="False">
							<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
								<TR>
									<TD class="employersearch"></TD>
									<TD class="employersearch2" style="TEXT-ALIGN: center">
										<asp:label id="lblEmployeeFilter" CssClass="Label" runat="server" Font-Bold="True">Employee Filter</asp:label></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblEmployeeStatus" CssClass="Label" runat="server">Status</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlEmployeeStatusId" CssClass="dropdownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
							</TABLE>
						</asp:panel>
						<asp:panel id="pnlEmployerFilter" CssClass="emailframe" runat="server" Visible="False">
							<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
								<TR>
									<TD class="employersearch"></TD>
									<TD class="employersearch2" style="TEXT-ALIGN: center">
										<asp:label id="lblEmployerFilter" CssClass="Label" runat="server" Font-Bold="True">Employer Filter</asp:label></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblEmployerStatus" CssClass="Label" runat="server">Status</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlEmployerStatusId" CssClass="dropdownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblEmployerCountry" CssClass="Label" runat="server">Country</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlEmployerCountryId" CssClass="dropdownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
							</TABLE>
						</asp:panel>
						<asp:panel id="pnlLeadFilter" CssClass="emailframe" runat="server">
							<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
								<TR>
									<TD class="employersearch">&nbsp;</TD>
									<TD class="employersearch2" style="TEXT-ALIGN: center">
										<asp:label id="lblLeadFilter" CssClass="label" runat="server" Font-Bold="True">Lead Filter</asp:label></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblLeadStatus" CssClass="label" runat="server">Status</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlLeadStatusId" CssClass="dropdownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblLeadProgram" CssClass="Label" runat="server">Program</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlLeadPrgVerId" CssClass="DropDownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD class="employersearch">
										<asp:label id="lblLeadCampus" CssClass="Label" runat="server">Campus</asp:label></TD>
									<TD class="employersearch2">
										<asp:dropdownlist id="ddlLeadCampGrpId" CssClass="dropdownlist" runat="server"></asp:dropdownlist></TD>
								</TR>
							</TABLE>
						</asp:panel>
						<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
							<TR>
								<TD class="employersearch"></TD>
								<TD class="employersearch2" style="TEXT-ALIGN: center">
									<asp:button id="btnBuildList" CssClass="buttontopfilter" runat="server" Text="Build List"></asp:button></TD>
							</TR>
						</TABLE>
						<asp:Label id="lblNoEmail" CssClass="Label" runat="server">(*) - No Email Address </asp:Label></TD>
				</TR>
				<TR>
					<TD class="listframebottom">
						<DIV class="scrollleftmail">
							<TABLE cellSpacing="0" cellPadding="0" width="95%" border="0">
								<TR>
									<TD class="emailcolumns" align="center">
										<asp:listbox id="lbxAvailable" CssClass="listbox" runat="server" Rows="14" SelectionMode="Multiple"></asp:listbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%; TEXT-ALIGN: center">
										<asp:Button id="btnOK"  runat="server" Text="OK" Width="50px" Enabled="False"></asp:Button></TD>
								</TR>
							</TABLE>
							</DIV>
                        </TD>
                    </TR>
                </TABLE>
	</asp:panel>
	</body>
</HTML>
