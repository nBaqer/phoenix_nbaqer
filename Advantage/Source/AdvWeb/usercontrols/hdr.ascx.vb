﻿Imports Telerik.Web.UI

Partial Class usercontrols_hdr
    Inherits System.Web.UI.UserControl
    Protected Sub DDLStudentSearch_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)

        'Dim Student As NewStudentSearch = DirectCast(e.Item.DataItem, NewStudentSearch)
        ''Dim Student As StudentEnrollmentSearch = DirectCast(e.Item.DataItem, StudentEnrollmentSearch)
        'e.Item.Text = Student.FullName
        'e.Item.Value = Student.StudentId.ToString
        'Dim NameLabel As Label = CType(e.Item.FindControl("lblFullName"), Label)
        'Dim SSNLabel As Label = CType(e.Item.FindControl("lblSSN"), Label)
        ''Dim PrgVerLabel As Label = CType(e.Item.FindControl("lblPrgDescrip"), Label)
        ''Dim StatusLabel As Label = CType(e.Item.FindControl("lblStatusCodeDescrip"), Label)

        'NameLabel.Text = PadDefaultDisplayData(Student.FullName)
        'SSNLabel.Text = PadDefaultDisplayData(FormatSSN(Student.SSN))

        ''PrgVerLabel.Text = PadDefaultDisplayData(Student.PrgVerDescrip)
        ''StatusLabel.Text = PadDefaultDisplayData(Student.StatusCodeDescrip)
    End Sub
    Protected Sub DDLStudentSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        'stuid = e.Value
        'BuildDropDowns(e.Value)
        'ddlEnrollments.Enabled = True
        'ddlTerms.Enabled = True
        'ddlAcademicYears.Enabled = True

    End Sub
    Protected Sub DDLStudentSearch_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)

        'Dim MyStudents = QueryStudents(e.Text)
        'Dim itemOffset As Integer = e.NumberOfItems
        ''Dim endOffset As Integer = Math.Min(itemOffset + ItemsPerRequest, MyStudents.Count)
        ''e.EndOfItems = endOffset = MyStudents.Count

        'Dim MyStudentsDisplay = (From s In MyStudents
        '                   Select New NewStudentSearch With {.FirstName = s.FirstName, .LastName = s.LastName, .FullName = s.FullName, .SSN = s.SSN, .StudentId = s.StudentId}).Distinct

        'Dim MyStudentsDisplaySorted = (From s In MyStudentsDisplay
        '                   Order By s.LastName.Trim Ascending, s.FirstName.Trim Ascending)

        'Dim endOffset As Integer = Math.Min(itemOffset + ItemsPerRequest, MyStudentsDisplaySorted.Count)
        'e.EndOfItems = endOffset = MyStudentsDisplaySorted.Count


        'Dim StudentList As List(Of NewStudentSearch) = MyStudentsDisplaySorted.ToList
        'Dim DisplayedStudents As New List(Of NewStudentSearch)

        'For i As Integer = itemOffset To endOffset - 1
        '    DisplayedStudents.Add(StudentList(i))
        'Next
        'DDLStudentSearch.DataSource = DisplayedStudents
        'DDLStudentSearch.DataTextField = "FullName"
        ''DDLStudentSearch.DataValueField = "StuEnrollId"
        'DDLStudentSearch.DataValueField = "StudentId"
        'DDLStudentSearch.DataBind()

        'e.Message = GetStatusMessage(endOffset, MyStudents.Count)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mylayout As String = Page.Request.Params("layout")

        Select Case mylayout
            Case "start"
                MyWonderBar.Visible = False
                StudentSearchWidgetLayout.Visible = False
                SchoolLogo.Visible = True
            Case "student"
                MyWonderBar.Visible = True
                StudentSearchWidgetLayout.Visible = True
                SchoolLogo.Visible = False
            Case Else

        End Select

    End Sub
End Class
