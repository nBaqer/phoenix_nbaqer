Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade

' ===============================================================================
'
' FAME AdvantageV1
'
' EmailSelector.asxx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Partial Class EmailSelector
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblType As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmployeeCampus As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlEmployeeCampGrpId As System.Web.UI.WebControls.DropDownList


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If Not IsPostBack Then

            '   default settings are for "All Students"
            pnlStudentFilter.Visible = True
            pnlEmployeeFilter.Visible = False
            pnlEmployerFilter.Visible = False
            pnlLeadFilter.Visible = False
            ddlStudentStatusId.Enabled = False
            ddlStudentPrgVerId.Enabled = False
            ddlStudentCampGrpId.Enabled = False

            '   build dropdownlists
            BuildDropDownLists()

        End If
    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDLs()
        BuildCampusGroupsDDLs()
        BuildCountriesDDL()
        BuildPrgVerDDL()
    End Sub
    Private Sub BuildStatusDDLs()
        '   bind the status DDL
        Dim ds As DataSet = (New StatusCodeFacade).GetAllStatusCodesForStudents()

        '   build Student Status ddl
        With ddlStudentStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

        '   bind the status DDL
        ds = (New StatusesFacade).GetAllStatuses()

        '   build Employee Status ddl
        With ddlEmployeeStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = ds
            .DataBind()
        End With

        '   build Employer Status ddl
        With ddlEmployerStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = ds
            .DataBind()
        End With

        '   bind the status DDL
        ds = (New StatusCodeFacade).GetAllStatusCodesForLeads()

        '   build Lead Status ddl
        With ddlLeadStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCampusGroupsDDLs()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        '   get ds from the backend
        Dim ds As DataSet = campusGroups.GetAllCampusesUsedByStudents()

        '   build Student Campus Groups ddl
        With ddlStudentCampGrpId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

        ''   build Employee Campus Groups ddl
        'With ddlEmployeeCampGrpId
        '    .DataTextField = "CampGrpDescrip"
        '    .DataValueField = "CampGrpId"
        '    .DataSource = ds
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With

        ds = campusGroups.GetAllCampusesUsedByLeads()

        '   build Student Campus Groups ddl
        With ddlLeadCampGrpId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountriesDDL()
        '   bind Countries ddl
        Dim countries As New EmployerSearchFacade

        '   build Employer Countries ddl
        With ddlEmployerCountryId
            .DataTextField = "CountryDescrip"
            .DataValueField = "CountryId"
            .DataSource = countries.GetAllCountriesUsedByEmployers()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildPrgVerDDL()
        'Bind the Programs DrowDownList
        With ddlStudentPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = (New StudentSearchFacade).GetAllEnrollmentsUsedByStudents()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
        'Bind the Programs DrowDownList
        With ddlLeadPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = (New StudentSearchFacade).GetAllEnrollmentsUsedByLeads()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub ddlTypes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTypes.SelectedIndexChanged
        '   set one visible panel according to user selection
        Select Case ddlTypes.SelectedValue
            Case "All Students"
                pnlStudentFilter.Visible = True
                pnlEmployeeFilter.Visible = False
                pnlEmployerFilter.Visible = False
                pnlLeadFilter.Visible = False
                ddlStudentStatusId.Enabled = False
                ddlStudentPrgVerId.Enabled = False
                ddlStudentCampGrpId.Enabled = False
            Case "Students"
                pnlStudentFilter.Visible = True
                pnlEmployeeFilter.Visible = False
                pnlEmployerFilter.Visible = False
                pnlLeadFilter.Visible = False
                ddlStudentStatusId.Enabled = True
                ddlStudentPrgVerId.Enabled = True
                ddlStudentCampGrpId.Enabled = True
                'Case "Student Groups"
                '    pnlStudentFilter.Visible = True
                '    pnlEmployeeFilter.Visible = False
                '    pnlEmployerFilter.Visible = False
                '    pnlLeadFilter.Visible = False
                '    ddlStudentStatusId.Enabled = False
                '    ddlStudentPrgVerId.Enabled = False
                '    ddlStudentCampGrpId.Enabled = False
            Case "All Employees"
                pnlStudentFilter.Visible = False
                pnlEmployeeFilter.Visible = True
                pnlEmployerFilter.Visible = False
                pnlLeadFilter.Visible = False
                ddlEmployeeStatusId.Enabled = False
                'ddlEmployeeCampGrpId.Enabled = False
            Case "Employees"
                pnlStudentFilter.Visible = False
                pnlEmployeeFilter.Visible = True
                pnlEmployerFilter.Visible = False
                pnlLeadFilter.Visible = False
                ddlEmployeeStatusId.Enabled = True
                'ddlEmployeeCampGrpId.Enabled = True
                'Case "Employee Groups"
                '    pnlStudentFilter.Visible = False
                '    pnlEmployeeFilter.Visible = True
                '    pnlEmployerFilter.Visible = False
                '    ddlEmployeeStatusId.Enabled = False
                '    'ddlEmployeeCampGrpId.Enabled = False
            Case "All Employers"
                pnlStudentFilter.Visible = False
                pnlEmployeeFilter.Visible = False
                pnlEmployerFilter.Visible = True
                pnlLeadFilter.Visible = False
                ddlEmployerStatusId.Enabled = False
                ddlEmployerCountryId.Enabled = False
            Case "Employers"
                pnlStudentFilter.Visible = False
                pnlEmployeeFilter.Visible = False
                pnlEmployerFilter.Visible = True
                pnlLeadFilter.Visible = False
                ddlEmployerStatusId.Enabled = True
                ddlEmployerCountryId.Enabled = True
            Case "All Leads"
                pnlStudentFilter.Visible = False
                pnlEmployeeFilter.Visible = False
                pnlEmployerFilter.Visible = False
                pnlLeadFilter.Visible = True
                ddlLeadStatusId.Enabled = False
                ddlLeadPrgVerId.Enabled = False
                ddlLeadCampGrpId.Enabled = False
            Case "Leads"
                pnlStudentFilter.Visible = False
                pnlEmployeeFilter.Visible = False
                pnlEmployerFilter.Visible = False
                pnlLeadFilter.Visible = True
                ddlLeadStatusId.Enabled = True
                ddlLeadPrgVerId.Enabled = True
                ddlLeadCampGrpId.Enabled = True
        End Select

        '   init listboxes
        InitListBoxes()

    End Sub

    Private Sub btnBuildList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuildList.Click

        '   init listboxes
        InitListBoxes()

        Select Case ddlTypes.SelectedValue
            Case "All Students"
                '   set listbox properly
                lbxAvailable.Items.Add("All Students")
                lbxAvailable.Items(0).Selected = True
                btnOK.Enabled = True

            Case "Students"
                '   set listbox properly
                lbxAvailable.DataSource = (New StudentSearchFacade).GetAllStudentsForEmail(ddlStudentStatusId.SelectedValue, ddlStudentCampGrpId.SelectedValue, ddlStudentPrgVerId.SelectedValue)
                lbxAvailable.DataTextField = "Name"
                lbxAvailable.DataValueField = "Email"
                lbxAvailable.DataBind()

                'enable OK button only if there are items in the list
                If lbxAvailable.Items.Count > 0 Then btnOK.Enabled = True Else btnOK.Enabled = False

                'Case "Student Groups"
                '    '   set listbox properly
                '    lbxAvailable.DataSource = (New StudentSearchFacade).GetAllGroupsUsedByStudents()
                '    lbxAvailable.DataTextField = "Descrip"
                '    lbxAvailable.DataValueField = "SGroupId"
                '    lbxAvailable.DataBind()

                '    'enable OK button only if there are items in the list
                '    If lbxAvailable.Items.Count > 0 Then btnOK.Enabled = True Else btnOK.Enabled = False

            Case "All Employees"
                '   set listbox properly
                lbxAvailable.Items.Add("All Employees")
                lbxAvailable.Items(0).Selected = True
                btnOK.Enabled = True
            Case "Employees"
                '   set listbox properly
                lbxAvailable.DataSource = (New EmployeesFacade).GetAllEmployeesForEmail(ddlEmployeeStatusId.SelectedValue)
                lbxAvailable.DataTextField = "Name"
                lbxAvailable.DataValueField = "Email"
                lbxAvailable.DataBind()

                'enable OK button only if there are items in the list
                If lbxAvailable.Items.Count > 0 Then btnOK.Enabled = True Else btnOK.Enabled = False

                'Case "Employee Groups"
                '    '   set listbox properly
                '    With New EmployeeSearchFacade
                '        lbxAvailable.DataSource = .GetAllRolesUsedByEmployees()
                '        lbxAvailable.DataTextField = "Role"
                '        lbxAvailable.DataValueField = "RoleId"
                '        lbxAvailable.DataBind()
                '    End With
            Case "All Employers"
                '   set listbox properly
                lbxAvailable.Items.Add("All Employers Contacts")
                lbxAvailable.Items(0).Selected = True
                btnOK.Enabled = True
            Case "Employers"
                '   set listbox properly
                lbxAvailable.DataSource = (New EmployersFacade).GetAllEmployersForEmail(ddlEmployerStatusId.SelectedValue, ddlEmployerCountryId.SelectedValue)
                lbxAvailable.DataTextField = "Name"
                lbxAvailable.DataValueField = "Email"
                lbxAvailable.DataBind()

                'enable OK button only if there are items in the list
                If lbxAvailable.Items.Count > 0 Then btnOK.Enabled = True Else btnOK.Enabled = False
            Case "All Leads"
                '   set listbox properly
                lbxAvailable.Items.Add("All Leads")
                lbxAvailable.Items(0).Selected = True
                btnOK.Enabled = True

            Case "Leads"
                '   set listbox properly
                lbxAvailable.DataSource = (New LeadFacade).GetAllLeadsForEmail(ddlLeadStatusId.SelectedValue, ddlLeadCampGrpId.SelectedValue, ddlLeadPrgVerId.SelectedValue)
                lbxAvailable.DataTextField = "Name"
                lbxAvailable.DataValueField = "Email"
                lbxAvailable.DataBind()

                'enable OK button only if there are items in the list
                If lbxAvailable.Items.Count > 0 Then btnOK.Enabled = True Else btnOK.Enabled = False

        End Select

    End Sub
    Private Sub BuildArrayWithSelectedItems()
        'first we have to count the number of selected items
        Dim j As Integer = 0
        For Each item As ListItem In lbxAvailable.Items
            If item.Selected Then j += 1
        Next

        '   build array with selected items
        Dim emailList(1, j - 1) As String
        j = 0
        For Each item As ListItem In lbxAvailable.Items
            If item.Selected Then
                emailList(0, j) = item.Value
                emailList(1, j) = item.Text
                j += 1
                item.Selected = False
            End If
        Next

        '   save array in Session
        Session("EmailList") = emailList

    End Sub
    Private Sub BuildArrayWithSelectedTable(ByVal table As DataTable)

        '   first we have to count the number of valid emails
        Dim j As Integer = 0
        For i As Integer = 0 To table.Rows.Count - 1
            If table.Rows(i)("HasEmail") = 1 Then j += 1
        Next

        If j = 0 Then
            Session("EmailList") = Nothing
            Return
        End If

        '   build array with selected table
        Dim emailList(1, j - 1) As String
        j = 0
        For i As Integer = 0 To table.Rows.Count - 1
            Dim row As DataRow = table.Rows(i)
            If row("HasEmail") = 1 Then
                emailList(0, j) = CType(row("Email"), String)
                emailList(1, j) = CType(row("Name"), String)
                j += 1
            End If
        Next

        '   save array in Session
        Session("EmailList") = emailList

    End Sub
    Private Sub InitListBoxes()

        '   init listboxes
        lbxAvailable.Items.Clear()

        '   kill any Session value
        Session("EmailList") = Nothing

        '   disable OK Button
        btnOK.Enabled = False

    End Sub
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Select Case ddlTypes.SelectedValue
            Case "All Students"
                '   build table with all Students
                BuildArrayWithSelectedTable((New StudentSearchFacade).GetAllStudentsForEmail(Guid.Empty.ToString, Guid.Empty.ToString, Guid.Empty.ToString).Tables(0))
            Case "Students"
                '   build array with selected items
                BuildArrayWithSelectedItems()
                'Case "Student Groups"
                '    '   get the list of selected groups
                '    BuildArrayWithSelectedItems()
                '    '   Array list contains only the list of Student Groups.
                '    '   Go to the backed to get the complete emailList
                '    BuildArrayWithSelectedTable((New StudentSearchFacade).GetAllStudentEmailsByStudentGroup(Session("EmailList")).Tables(0))
            Case "All Employees"
                '   build table with all Employees
                BuildArrayWithSelectedTable((New EmployeesFacade).GetAllEmployeesForEmail(Guid.Empty.ToString).Tables(0))
            Case "Employees"
                '   build array with selected items
                BuildArrayWithSelectedItems()
            Case "Employee Groups"
                '   build array with selected tables
                BuildArrayWithSelectedItems()
                '   Array list contains only the list of Employee Groups.
                '   Go to the backed to get the complete emailList
                BuildArrayWithSelectedTable((New EmployeeSearchFacade).GetAllEmployeeEmailsByEmployeeRole(Session("EmailList")).Tables(0))
            Case "All Employers"
                '   build table with all Employer Contacts
                BuildArrayWithSelectedTable((New EmployersFacade).GetAllEmployersForEmail(Guid.Empty.ToString, Guid.Empty.ToString).Tables(0))
            Case "Employers"
                '   build array with selected items
                BuildArrayWithSelectedItems()
            Case "All Leads"
                '   build table with all Students
                BuildArrayWithSelectedTable((New LeadFacade).GetAllLeadsForEmail(Guid.Empty.ToString, Guid.Empty.ToString, Guid.Empty.ToString).Tables(0))
            Case "Leads"
                '   build array with selected items
                BuildArrayWithSelectedItems()
        End Select

    End Sub
End Class
