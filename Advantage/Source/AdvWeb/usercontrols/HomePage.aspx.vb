﻿Imports BO = Advantage.Business.Objects
Imports CO = FAME.AdvantageV1.Common

Partial Class SY_HomePage
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim advantageUserState As New BO.User()
            Dim ResourceId As String = Request.QueryString("resid").ToString
            Dim CampusId As String = Request.QueryString("CmpId").ToString
            Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo

            advantageUserState = AdvantageSession.UserState
            lblWelcomeMessage.Text = "Welcome " + advantageUserState.UserName & "<br>"

            'New Pages
            'SetPageLevelPermission(advantageUserState, ResourceId, CampusId)

            'For Old Pages  - only change needs to made is replace this line pObj = Header1.UserPagePermission with the following line
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, CampusId)

            DisableAllActionButtons() 'By Default disable all buttons
            If pObj.HasFull Or pObj.HasEdit Then
                btnSave.Enabled = True
            End If
            If pObj.HasFull Or pObj.HasDelete Then
                btnDelete.Enabled = True
            End If
            If pObj.HasFull Or pObj.HasAdd Then
                btnNew.Enabled = True
            End If
        End If
    End Sub
    Private Sub DisableAllActionButtons()
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub
    Private Sub SetPageLevelPermission(ByVal AdvantageUserState As BO.User, _
                                       ByVal ResourceId As String, _
                                       ByVal CampusId As String)
        Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageUserState, ResourceId, CampusId)
        DisableAllActionButtons() 'By Default disable all buttons
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If
        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        End If
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        End If
    End Sub
End Class
