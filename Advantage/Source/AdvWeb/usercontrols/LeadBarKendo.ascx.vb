﻿
Imports FAME.AdvantageV1.BusinessFacade

Namespace AdvWeb.usercontrols
    ''' <summary>
    ''' Tool bar Info Lead
    ''' </summary>
    ''' <remarks></remarks>
    Partial Class UsercontrolsLeadBarKendo
        Inherits UserControl

        Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
            ' Pass to client side the LeadId
            Dim leadId As String = AdvantageSession.MasterLeadId
            ViewState("leadId") = leadId
            Page.ClientScript.RegisterHiddenField("leadId", CType(ViewState("leadId"), String))
            '   SetTaskRecipientProperties(leadId)

        End Sub

        'Private Sub SetTaskRecipientProperties(leadId As String)

        '    Dim val As Integer
        '    val = CInt(System.Enum.Parse(GetType(AdvantageEntityId), AdvantageEntityId.Lead.ToString()))

        '    AdvantageSession.TaskRecipientId = AdvantageSession.MasterLeadId
        '    AdvantageSession.TaskRecipientTypeId = val.ToString()
        '    Dim db = New LeadFacade()
        '    Dim info = db.GetLeadDataForHeaderInfo(leadId)
        '    AdvantageSession.TaskRecipientFullName = info.Name
        '    val = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Admissions.ToString()))
        '    AdvantageSession.TaskRecipientModule = val.ToString()
        'End Sub

    End Class
End Namespace