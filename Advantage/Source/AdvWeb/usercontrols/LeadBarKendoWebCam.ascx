﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeadBarKendoWebCam.ascx.vb" Inherits="AdvWeb.usercontrols.UsercontrolsLeadBarKendoWebCam" %>
<div id="kendoleadBarWebCamControl">
    <div id="webCamWindow" style="display: block">
       
        <div style="float: left">
            <div id="Advantage_camera" style="width: 240px; height:240px; display: block"></div>
            <canvas id="Advantage_result" width="240" height="240" style="display: none"></canvas>
            <span id="imagefromFile" class="k-icon k-i-image k-icon-256 font-blue-darken-4"></span>
        </div>
        <div style="float: left; margin-left: 10px;">
            <div id="webcamprocessing">
                <button id="takePhoto" class="webcamprocessbutton">Take Photo from Webcam </button>
                <img id="help1" alt="Waiting" style="display: none" src='data:image/gif;base64,R0lGODlhEAAQALMKAH9/f0xMTLKysuXl5RkZGTMzMwAAAMzMzJmZmWZmZv///wAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQJAAAKACwAAAAAEAAQAAAEMlDJqYyhWBJSb6bbJ45TUZCUia5ZEKzD4MIDSyUJeRwSrvO2DABAEQhEQwUCoTCilp8IACH5BAkAAAoALAAAAAAQABAAAAQzUMmpCKFYllKvMZnGTV9oZkFwYunqmknySvFsUwCwDoOU770bBoGgHA4mokIgUBxdzFAEACH5BAkAAAoALAAAAAAQABAAAAQzUMmpSqFYhlAvIZnGTV9oZklyKoYhpWu7hgAwT/WtZwiy97sgRSCgDAYmouJwUBxnzFAEACH5BAkAAAoALAAAAAAQABAAAAQyUMmpQqBYplRvKZnGTV9oZgBwKgQhpWu7hggyK4ZR3/mNCQKfAigsUg4HIVIxGAiboQgAIfkECQAACgAsAAAAABAAEAAABDFQyalSolgCUG8ImcZNX2hmCHIqRSGla7uGgjArBFHf+Y0dh5XBIAEKib7QYJBcJm8RACH5BAkAAAoALAAAAAAQABAAAAQxUMmpAKBYIlRvSpnGTV9oZoJwKkEgpWu7hscxK0VR3/mNDYMVgSABCok+n8GQXCYzEQAh+QQJAAAKACwAAAAAEAAQAAAEMVDJqRCiWApRLwCZxk1faGbHcSpJIqVru4bDMCtBUN/57VOFwi/48xEIM4NBcUwuMxEAIfkECQAACgAsAAAAABAAEAAABDNQyamEoFieUy9CmcZNX2hmw3AqACCla7vOa5LQko3vUhDsPl7GYKAUCiaigkBQHGfMUAQAIfkECQAACgAsAAAAABAAEAAABDNQyanOoViOUa8QmcZNX2ieJ4KglMq+JgDAikxPhoEmiZTvvRuGQKAEAiaiolBQHFnMUAQAIfkECQAACgAsAAAAABAAEAAABDJQyanGoBhbdU7+nAeOJCYI5XSmmGGkCOLCCEsRBAkAEq7ztkyhQEkkQENFIKAwlpafCAAh+QQJAAAKACwAAAAAEAAQAAAEMVDJSautY9ybt6eG8R2HFI7lZxGEqggC68JuVRQfgkh3vteXQIACAGyEikRCUVQpLxEAIfkEBQAACgAsAAAAABAAEAAABDFQyUlrNcZqhbf/BPENgxSO5WcVhaocB+vCbhUEnyBId77XmkSCgkBshAoAQFFUKTURADs=' />
                <div class="clearfix"></div>
                <button id="takePhotoCancel" class="webcamprocessbutton">Cancel </button>
                <button id="takePhotoUpload" class="webcamprocessbutton">Upload</button>
                <div class="clearfix"></div>
                <label id="takePhotoResultLabel"></label>
            </div>
            <p>
                <label>
                    <input id="webCamOrFile" type="checkbox" />
                    Get Photo from File</label></p>
            <input id="uploadphoto" type="file" name="files" />
       </div>
        <div class="clearfix"></div>
        <hr />
        <div style="text-align: right">
            <button id="webCamDone">Done</button>
        </div>
    </div>
    <script type="text/javascript">
        //$(document).ready(function () {
        //   window.AdvantageWebCamControl = new MasterPage.WebCamControl();
        //});
    </script>
</div>
