Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data

' ===============================================================================
'
' FAME AdvantageV1
'
' StudentLedgerPopUp.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.Public Class StudentLedgerPopUp
Partial Class StudentLedgerPopUp
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnNew As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelete As System.Web.UI.WebControls.Button
    Protected studentId As String
    Protected balanceForward As Decimal
    Protected count As Integer
    Protected state As AdvantageSessionState

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        '   For testing purposes only
        'studentId = CType("{F58ADEB8-F734-4465-9F8F-CDBDD657DC65}", String)
        Session("SEARCH") = 0
        ''   get StudentId from the Session
        'If CommonWebUtilities.IsValidGuid(Session("StudentId")) Then
        '    studentId = CType(Session("StudentId"), String)
        'Else
        '    '   redirect to the search page if there is no StudentId in session
        '    Response.Redirect("~/PL/StudentSearch.aspx?resid=231&mod=SA")
        'End If
        Dim strVID As String
        Dim state As AdvantageSessionState
        '        Dim m_Context As HttpContext

        'Get the StudentId from the state object associated with this page.
        strVID = HttpContext.Current.Request.Params("VID")
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        '   get the studentId fgrom the session
        If CommonWebUtilities.IsValidGuid(strVID) Then
            If (state.Contains(strVID)) Then
                studentId = DirectCast(state(strVID), AdvantageStateInfo).StudentId
            Else
                'Send the user to error page requesting a login again...
                Session("Error") = "This is not a valid action. Please login to Advantage again."
                Response.Redirect("../ErrorPage.aspx")
            End If
        Else
            'Send the user to error page requesting a login again...
            Session("Error") = "This is not a valid action. Please login to Advantage again."
            Response.Redirect("../ErrorPage.aspx")
        End If

        'header1.EnableHistoryButton(False)

        If Not IsPostBack Then

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datagrid
            'BindDatagrid("TransDate desc, ModDate desc")
            BindDatagrid(dgrdStudentLedger.Columns(0).SortExpression)

        End If
    End Sub
    Private Sub BuildDropDownLists()
        BuildStudentEnrollmentsDDL(studentId)
        BuildTermsDDL(studentId)
    End Sub
    Private Sub BindDatagrid(ByVal sortExpression As String)

        'this logic is to laternative sort the columns in ascending or descending mode
        'get column to be sorted. Change replace "asc" by "desc". Insert "asc" if necessary
        For Each column As DataGridColumn In dgrdStudentLedger.Columns
            If column.SortExpression = sortExpression Then
                If sortExpression.IndexOf(" asc") > 0 Then
                    sortExpression = sortExpression.Replace(" asc", " desc")
                ElseIf sortExpression.IndexOf(" desc") > 0 Then
                    sortExpression = sortExpression.Replace(" desc", " asc")
                Else
                    sortExpression += " asc"
                End If

                'update sort expression in the column
                column.SortExpression = sortExpression
                Exit For
            End If
        Next

        '   bind datagrid with data from the backend
        With New StudentsAccountsFacade
            dgrdStudentLedger.DataSource = New DataView(.GetStudentLedger(studentId, ddlEnrollmentId.SelectedValue, ddlTermsId.SelectedValue).Tables(0), Nothing, sortExpression, DataViewRowState.CurrentRows)
            dgrdStudentLedger.DataBind()
        End With

        '   if datagrid is sorted by date then display balance column
        If sortExpression.IndexOf("TransDate") > -1 Then
            dgrdStudentLedger.Columns(6).Visible = True
        Else
            dgrdStudentLedger.Columns(6).Visible = False
        End If
    End Sub
    Private Sub BuildTermsDDL(ByVal studentId As String)
        '   bind the Terms DDL
        Dim terms As New StudentsAccountsFacade

        With ddlTermsId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.GetAllTermsPerStudent(studentId)
            .DataBind()
            .Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade

        With ddlEnrollmentId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()
            If Not .Items.Count = 1 Then
                .Items.Insert(0, New ListItem("All Enrollments", Guid.Empty.ToString))
            End If
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub dgrdStudentLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdStudentLedger.ItemDataBound
        '   accept only items (no header or footer records)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            '   get value of TransAmount column
            Dim transAmount As Decimal = CType(e.Item.DataItem, DataRowView).Item("TransAmount")

            '   set amount
            CType(e.Item.FindControl("Amount"), Label).Text = transAmount.ToString("c")

            '   acumulate balance forward
            balanceForward += transAmount
            CType(e.Item.FindControl("Balance"), Label).Text = CType(e.Item.DataItem("Balance"), Decimal).ToString("c")

        End If
    End Sub
    Private Sub btnBuild_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuild.Click

        '   bind datagrid
        BindDatagrid(dgrdStudentLedger.Columns(0).SortExpression)

    End Sub

    Private Sub dgrdStudentLedger_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgrdStudentLedger.SortCommand
        BindDatagrid(e.SortExpression)
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        '   display balance
        lblBalance.Text = balanceForward.ToString("c")

        '   display projected amount only if "All terms" and some Enrollment are selected
        If ddlTermsId.SelectedIndex = 0 And Not ddlEnrollmentId.SelectedValue = Guid.Empty.ToString Then
            lblProjected.Text = (New StudentsAccountsFacade).GetProjectedAmountByEnrollment(ddlEnrollmentId.SelectedValue).ToString("c")

        ElseIf ddlTermsId.SelectedIndex = 0 And ddlEnrollmentId.SelectedValue = Guid.Empty.ToString Then
            lblProjected.Text = (New StudentsAccountsFacade).GetProjectedAmountForStudent(studentId).ToString("c")

        Else
            lblProjected.Text = "Not available"
        End If
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
