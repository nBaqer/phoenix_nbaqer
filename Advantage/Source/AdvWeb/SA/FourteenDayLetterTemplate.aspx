<%@ Page Language="vb" AutoEventWireup="false" Inherits="FourteenDayLetterTemplate" CodeFile="FourteenDayLetterTemplate.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>14 Day Letter</title>
		<STYLE>
     div.page { page-break-after: always}
 
 
		</STYLE>
		<STYLE TYPE='text/css'> .boldline {border-bottom: 1px solid #000000;}
		</STYLE>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<table cellpadding='0' cellspacing='0' width='670' ID="Table1">
			<tr>
				<td valign='top' align="center">
					<table cellpadding='0' cellspacing='0' ID="Table2">
						<tr>
							<td align="center" height="30">
								Acupuncture &amp; Massage College
							</td>
						</tr>
						<tr>
							<td align="center" height="30">
								10506 N. Kendall Drive
							</td>
						</tr>
						<tr>
							<td align="center" height="30">
								Miami, FL 33176
							</td>
						</tr>
						<tr>
							<td align="center" height="30">
								(305) 595-9500 / (305) 595-2622 FAX
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td valign='top' class="boldline">
					<table cellpadding='0' cellspacing='0' width='100%' ID="Table3">
						<tr height=30>
							<td></td> 
						</tr> 
						<tr>
							<td align="left" height=60>
								1/3/2005
							</td>
						</tr>
						<tr>
							<td align="left" height=30>
								Amy Feinerman
							</td>
						</tr>
						<tr>
							<td align="left" height=30>
								6765 Miami Lakes Drive #K443
							</td>
						</tr>
						<tr>
							<td align="left" height=30>
								Miami Lakes, FL 33014
							</td>
						</tr>
						<tr>
							<td align="left" height=15>								
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td valign='top' class="boldline" align="left">
					<table cellpadding='0' cellspacing='0' width='100%' ID="Table4">
						<tr>
							<td align="left" height=60>
								Student Number # 159094588
							</td>
						</tr>
						<tr>
							<td align="left" height=60>
								A payment was credited to your student account for the following student loan 
								disbursement.
							</td>
						</tr>
						<tr>
							<td align="left" height=60>
								Fed Direct Subsidized Staff dispersed in the amount of $1,724.00 on 1/3/2005.
							</td>
						</tr>						
					</table>
				</td>
			</tr>
			<tr>
				<td valign='top'>
					<table cellpadding='0' cellspacing='0' width='100%' ID="Table5">
						<tr height=15>
							<td></td> 
						</tr> 
						<tr>
							<td align="left">
								This receipt is to notify you that we have credited your account the above loan 
								disbursement. The
								
								amount above is applied to tuition that you have incurred. If you want to 
								cancel the above
								
								transaction and make the tuition payment with non-loan funds, you have 14 days 
								to contact the
								
								school in writing to have the transaction reversed. In the future you will 
								receive notice of your
								
								minimum monthly payment. At that time if you are facing a personal or financial 
								hardship and are
							
								unable to make the scheduled payments there are deferments and forbearance. 
								These are ways to
								
								postpone payments when borrowers are unemployed, still in school, or reduce 
								monthly payments.
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</HTML>

