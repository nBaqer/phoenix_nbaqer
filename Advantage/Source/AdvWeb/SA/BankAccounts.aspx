<%@ Register TagPrefix="fame" TagName="header" Src="~/UserControls/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="BankAccounts" CodeFile="BankAccounts.aspx.vb" %>
<%@ Register TagPrefix="fame" TagName="footer" Src="../UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Bank Accounts</title>
      <%--  <LINK href="../css/localhost.css" type="text/css" rel="stylesheet">--%>
        <LINK href="../css/systememail.css" type="text/css" rel="stylesheet">
         <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="../AuditHist.js"></script>
	</HEAD>
	<body runat="server" leftMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<!-- beging header -->
			            <table cellSpacing="0" cellPadding="0" width="100%"  border="0">
                <tr>
                    <td><IMG src="../images/advantage_bank_accounts.jpg"></td>
                    <td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
                </tr>
            </table>
			<!-- end header -->
			<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0" height="100%">
				<!-- begin leftcolumn -->
				<tr>
					<td valign="top" >
						<table id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="listframe" style="height:100%;background-color:rgb(233, 237, 242);">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr  >
											<td width="15%" nowrap align="left"><asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
											<td width="85%" nowrap>
												<asp:radiobuttonlist id="radStatus" CssClass="Label" AutoPostBack="true" Runat="Server" RepeatDirection="Horizontal">
													<asp:ListItem Text="Active" Selected="True" />
													<asp:ListItem Text="Inactive" />
													<asp:ListItem Text="All" />
												</asp:radiobuttonlist>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom">
									<div class="scrollleftpopup" >
										<asp:datalist id="dlstBankAccts" runat="server" DataKeyField="BankAcctId">
											<SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
											<ItemStyle CssClass="NonSelectedItem"></ItemStyle>
											<ItemTemplate>
												<asp:ImageButton id="imgInActive" ImageUrl="../images/Inactive.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>' CausesValidation="False">
												</asp:ImageButton>
												<asp:ImageButton id="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>' CausesValidation="False">
												</asp:ImageButton>
												<asp:Label ID="lblId" Runat="server" Visible="false" text='<%# Container.DataItem("StatusId")%>' />
												<asp:LinkButton id=Linkbutton1 CssClass="NonSelectedItem" CausesValidation="False" Runat="server" CommandArgument='<%# Container.DataItem("BankAcctId")%>' text='<%# Container.DataItem("BankAcctDescrip")%>'>
												</asp:LinkButton>
											</ItemTemplate>
										</asp:datalist>
									</div>
								</td>
							</tr>
						</table>
					</td>
					<!-- end leftcolumn -->
					<!-- begin rightcolumn -->
					<td class="leftside">
						<table id="Table3" cellSpacing="0" cellPadding="0" width="9" border="0">
						</table>
					</td>
					<td class="DetailsFrameTop">
						<table id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="MenuFrame" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"></asp:button></td>
							</tr>
						 </table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						<table id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0" >
							<tr>
								<td class="detailsframe">
									<div class="scrollrightpopup" >
										<!-- begin table content-->
                                      <asp:panel id="pnlRHS" Runat="server">
										<table class="contenttable" cellSpacing="0" cellPadding="0" width="60%" align="center">
											<asp:textbox id="txtBankAcctId" runat="server" Visible="False"></asp:textbox>
                                            <asp:checkbox id="chkIsInDB" runat="server" Visible="False"></asp:checkbox>
                                            <asp:textbox id="txtBankId" runat="server" Visible="False"></asp:textbox>
                                            <asp:textbox id="txtModUser" runat="server" Visible="False">ModUser</asp:textbox>
                                            <asp:textbox id="txtModDate" runat="server" Visible="False">ModDate</asp:textbox>
											<TR>
												<TD class="twocolumnlabelcell">
													<asp:label id="Label1" runat="server" CssClass="label">Bank</asp:label></TD>
												<TD class="twocolumncontentcell" style="text-align: left">
													<asp:Label id="lblBank" runat="server" CssClass="label"></asp:Label></TD>
											</TR>
											<tr>
												<td class="twocolumnlabelcell"><asp:label id="lblBankAcctDescrip" runat="server" CssClass="label">Description</asp:label></td>
												<td class="twocolumncontentcell"><asp:textbox id="txtBankAcctDescrip" runat="server" cssClass="TextBox"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" Display="None" ErrorMessage="Description can not be blank"
														ControlToValidate="txtBankAcctDescrip" Enabled="False">Description can not be blank</asp:requiredfieldvalidator></td>
											</tr>
											<tr>
												<td class="twocolumnlabelcell"><asp:label id="lblStatusId" CssClass="label" Runat="server">Status</asp:label></td>
												<td class="twocolumncontentcell"><asp:dropdownlist id="ddlStatusId" runat="server" CssClass="dropdownlistff"></asp:dropdownlist></td>
											</tr>
											<tr>
												<td class="twocolumnlabelcell"><asp:label id="lblBankAcctNumber" CssClass="label" Runat="server">Account #</asp:label></td>
												<td class="twocolumncontentcell"><asp:textbox id="txtBankAcctNumber" cssClass="TextBox" Runat="server" MaxLength="128"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" Display="None" ErrorMessage="Account Number can not be blank"
														ControlToValidate="txtBankAcctNumber" Enabled="False">Account Number can not be blank</asp:requiredfieldvalidator></td>
											</tr>
											<tr>
												<td class="twocolumnlabelcell"><asp:label id="lblRoutingNumber" runat="server" CssClass="label">Routing Number</asp:label></td>
												<td class="twocolumncontentcell"><asp:textbox id="txtBankRoutingNumber" runat="server" cssClass="TextBox"></asp:textbox></td>
											</tr>
											<tr>
												<td class="twocolumnlabelcell"></td>
												<td class="twocolumncontentcelldate"><asp:checkbox id="chkIsDefaultBankAcct" runat="server" Text="Default Bank Account" CssClass="label"></asp:checkbox></td>
											</tr>
											<tr>
												<td class="twocolumnlabelcell"><asp:label id="lblCampGrpId" CssClass="label" Runat="server">Campus Group</asp:label></td>
												<td class="twocolumncontentcell"><asp:dropdownlist id="ddlCampGrpId" runat="server" CssClass="dropdownlistff"></asp:dropdownlist>
									<%--				<asp:comparevalidator id="CampusGroupCompareValidator" runat="server" ControlToValidate="ddlCampGrpId"
														ErrorMessage="Must Select a Campus Group" Display="None" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Campus Group</asp:comparevalidator>--%>
                                                        </td>
											</tr>
											<TR>
												<TD align="center" colSpan="2" class="twocolumncontentcell">
													<asp:LinkButton id="btnBack" runat="server" CausesValidation="False" CssClass="Label" Visible="False">Go to Bank page</asp:LinkButton></TD>
											</TR>
										</table>
										<!--end table content-->
                                      </asp:panel> 
									</div>
								</td>
							</tr>
						</table>
					</td>
					<!-- end rightcolumn -->
                </tr>
			</table>
			<!-- begin footer -->
			<!-- end footer -->
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="ValidationSummary"></asp:panel><asp:customvalidator id="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
				ShowSummary="False"></asp:validationsummary>
			<!--end validation panel-->
				<div id="footer">Copyright  FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
		</form>
		
	</body>
</HTML>

