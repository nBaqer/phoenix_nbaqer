Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class BankCodes
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Panel4 As System.Web.UI.WebControls.Panel
    Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator3 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lbtBankAccts As System.Web.UI.WebControls.LinkButton
    Protected errorMessage As String


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected InternationalAddressState As Boolean

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", chkForeignZip.Checked)
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   save original hyperlink in the viewstate
            ViewState("OriginalHPL") = hplBankAccounts.NavigateUrl.ToString

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            Dim boolStatus As String
            If radstatus.SelectedItem.Text = "Active" Then
                boolStatus = "True"
            ElseIf radstatus.SelectedItem.Text = "Inactive" Then
                boolStatus = "False"
            Else
                boolStatus = "All"
            End If

            BindDataList(boolStatus)

            '   bind an empty new BankInfo
            BindBankData(New BankInfo)

            'InternationalAddressState = True
        Else

            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", chkForeignZip.Checked)
            'If InternationalAddressState = False Then
            '    objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", False)
            'Else
            '    objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", chkForeignZip.Checked)
            'End If
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        End If

        '   GetInputMaskValue()
        If chkForeignZip.Checked = True Then
            lblOtherState.Enabled = True
            lblOtherState.Visible = True
            txtOtherState.Visible = True
            ddlStateId.Enabled = False
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateId.Enabled = True
        End If

        ValidateStateCaption()

    End Sub
    Private Sub ValidateStateCaption()
        If chkForeignZip.Checked = False Then
            If InStr(lblState.Text, "*") = 0 Then
                lblState.Text = lblState.Text + "<font color=""red"">*</font>"
            End If
        Else
            If InStr(lblState.Text, "*") > 1 Then
                lblState.Text = Replace(lblState.Text, "*", "")
            End If
        End If
    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub BindDataList(ByVal showActiveOnly As String)

        '   bind BankCodes datalist
        With New StudentsAccountsFacade
            dlstBankCodes.DataSource = .GetAllBanks(showActiveOnly)
            dlstBankCodes.DataBind()
        End With

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
        BuildStatesDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildStatesDDL()
        '   bind the State DDL
        Dim states As New StatesFacade

        With ddlStateId
            .DataTextField = "StateDescrip"
            .DataValueField = "StateId"
            .DataSource = states.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dlstBankCodes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstBankCodes.ItemCommand
        '   get the BankCode from the backend and display it
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        Dim strId As String = dlstBankCodes.DataKeys(e.Item.ItemIndex).ToString()
        GetBankCodeId(strId)

        'Dim ctl As Control
        'Dim rfv As New RequiredFieldValidator
        'If chkForeignZip.Checked Then
        '    ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
        '    If Not (ctl Is Nothing) Then
        '        rfv = CType(ctl, RequiredFieldValidator)
        '        rfv.Enabled = False
        '    End If
        '    lblState.Text = "State"
        'Else
        '    ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
        '    If Not (ctl Is Nothing) Then
        '        rfv = CType(ctl, RequiredFieldValidator)
        '        rfv.Enabled = True
        '    End If

        'End If

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstBankCodes, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit(e.CommandArgument)

        'InternationalAddressState = False

        If chkForeignZip.Checked = True Then
            txtOtherState.Visible = True
            lblOtherState.Visible = True
            txtOtherState.Enabled = True
            ddlStateId.Enabled = False
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateId.Enabled = True
        End If

        ValidateStateCaption()

        CommonWebUtilities.RestoreItemValues(dlstBankCodes, e.CommandArgument.ToString)

    End Sub
    Private Sub BindBankData(ByVal bank As BankInfo)

        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        With bank
            chkIsInDB.Checked = .IsInDB
            txtBankId.Text = .BankId
            txtCode.Text = .Code
            ddlStatusId.SelectedValue = .StatusId
            txtBankDescrip.Text = .Name
            ddlCampGrpId.SelectedValue = .CampGrpId
            txtAddress1.Text = .Address1
            txtAddress2.Text = .Address2
            txtCity.Text = .City
            ddlStateId.SelectedValue = .StateId
            txtZip.Text = .Zip
            txtPhone.Text = .Phone
            txtFax.Text = .Fax
            txtEmail.Text = .Email
            txtFirstName.Text = .ContactFirstName
            txtLastName.Text = .ContactLastName
            txtTitle.Text = .ContactTitle
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString

            'bind ForeignHomePhone
            chkForeignPhone.Checked = .ForeignPhone

            'bind ForeignWorkPhone
            chkForeignFax.Checked = .ForeignFax

            'bind ForeignZip
            chkForeignZip.Checked = .ForeignZip

            '   bind WorkPhone
            txtPhone.Text = .Phone
            'If txtPhone.Text <> "" And chkForeignPhone.Checked = False And txtPhone.Text.Length >= 1 Then
            '    txtPhone.Text = facInputMasks.ApplyMask(strMask, txtPhone.Text)
            'End If

            '   bind WorkPhone
            txtFax.Text = .Fax
            'If txtFax.Text <> "" And chkForeignFax.Checked = False And txtFax.Text.Length >= 1 Then
            '    '  txtFax.Text = facInputMasks.ApplyMask(strMask, txtFax.Text)
            'End If


            'If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            '    txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            'End If

            'Foreign Zip
            If .ForeignZip = True Then
                chkForeignZip.Checked = True
                txtOtherState.Visible = True
                lblOtherState.Visible = True
                ddlStateId.Enabled = False
                txtOtherState.Text = .OtherState
                txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
                lblZip.ToolTip = ""
            Else
                chkForeignZip.Checked = False
                txtOtherState.Visible = False
                lblOtherState.Visible = False
                ddlStateId.Enabled = True
                txtzip.Mask = "#####"
                lblZip.ToolTip = zipMask
            End If

            txtzip.Text = .Zip
            txtOtherState.Text = .OtherState
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component

        'If chkForeignPhone.Checked = False Or chkForeignPhone.Checked = False Or chkForeignFax.Checked = False Then
        '    errorMessage = ValidateFieldsWithInputMasks()
        'Else
        '    errorMessage = ""
        'End If

        Dim strValidationMessage As String = ""

        If ddlStateId.SelectedIndex = 0 AndAlso chkForeignZip.Checked = False Then
            strValidationMessage &= "State is required" & vbCrLf
        End If
        If ddlStatusId.SelectedIndex = 0 Then
            strValidationMessage &= "Status is required" & vbCrLf
        End If
        If ddlCampGrpId.SelectedIndex = 0 Then
            strValidationMessage &= "Campus Group is required" & vbCrLf
        End If
        If String.IsNullOrEmpty(txtBankDescrip.Text) Then
            strValidationMessage &= "Bank Description is required" & vbCrLf
        End If

        If Not String.IsNullOrEmpty(strValidationMessage) Then
            DisplayErrorMessage(strValidationMessage)
            Exit Sub
        End If

        If Not errorMessage = "" Then
            DisplayErrorMessage(errorMessage)
            Exit Sub
        End If

        'Check If Mask is successful only if Foreign Is not checked
        If errorMessage = "" Then

            Dim result As String
            With New StudentsAccountsFacade
                '   update bank Info 
                result = .UpdateBankInfo(BuildBankInfo(txtBankId.Text), Session("UserName"))
            End With

            '   bind datalist
            Dim boolStatus As String
            If radStatus.SelectedItem.Text = "Active" Then
                boolStatus = "True"
            ElseIf radstatus.SelectedItem.Text = "Inactive" Then
                boolStatus = "False"
            Else
                boolStatus = "All"
            End If

            BindDataList(boolStatus)
            '   set Style to Selected Item
            CommonWebUtilities.SetStyleToSelectedItem(dlstBankCodes, txtBankId.Text, ViewState)

            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get the BankCode from the backend and display it
                GetBankCodeId(txtBankId.Text)
            End If

            '   if there are no errors bind a new entity and init buttons
            If Page.IsValid Then

                '   set the property IsInDB to true in order to avoid an error if the user
                '   hits "save" twice after adding a record.
                chkIsInDB.Checked = True

                'note: in order to display a new page after "save".. uncomment next lines
                '   bind an empty new BankInfo
                'BindBankData(New BankInfo)

                '   initialize buttons
                'InitButtonsForLoad()
                InitButtonsForEdit(txtBankId.Text)

            End If
        End If
        CommonWebUtilities.RestoreItemValues(dlstBankCodes, txtBankId.Text)
    End Sub
    Private Function BuildBankInfo(ByVal bankId As String) As BankInfo

        '   instantiate class
        Dim bankInfo As New bankInfo

        With bankInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get bankId
            .BankId = bankId

            '   get Code
            .Code = txtCode.Text.Trim

            '   get StatusId
            .StatusId = ddlStatusId.SelectedValue

            '   get bank's name
            .Name = txtBankDescrip.Text.Trim

            '   get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            '   get address1
            .Address1 = txtAddress1.Text.Trim

            '   get address2
            .Address2 = txtAddress2.Text.Trim

            '   get City
            .City = txtCity.Text.Trim

            '   get StateId
            .StateId = ddlStateId.SelectedValue

            '   get Zip
            '     zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
            'If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            '    .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            'Else
            .Zip = txtzip.Text
            ' End If

            '   get Phone
            .Phone = txtPhone.Text.Trim

            '   get Fax
            .Fax = txtFax.Text.Trim

            '   get Email
            .Email = txtEmail.Text.Trim

            '   get Contact's First Name
            .ContactFirstName = txtFirstName.Text.Trim

            '   get Contact's Last Name
            .ContactLastName = txtLastName.Text.Trim

            '   get Contact's Title
            .ContactTitle = txtTitle.Text.Trim

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

            '   get Homephone
            '  phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
            If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
                ' .Phone = facInputMask.RemoveMask(phoneMask, txtPhone.Text)
                .Phone = txtPhone.Text

            Else
                .Phone = txtPhone.Text
            End If

            '   get Workphone
            If txtFax.Text <> "" And chkForeignFax.Checked = False Then
                '.Fax = facInputMask.RemoveMask(phoneMask, txtFax.Text)
                .Fax = txtFax.Text
            Else
                .Fax = txtFax.Text
            End If

            '   get ForeignHomePhone
            .ForeignPhone = chkForeignPhone.Checked

            '   get ForeignCellPhone
            .ForeignFax = chkForeignFax.Checked

            .OtherState = txtOtherState.Text

            .ForeignZip = chkForeignZip.Checked

        End With

        '   return data
        Return bankInfo

    End Function
    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '   bind the datalist
        'BindDataList(chkStatus.Checked)

    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new BankInfo
        BindBankData(New BankInfo)

        '   Reset Style in the Datalist
        CommonWebUtilities.SetStyleToSelectedItem(dlstBankCodes, Guid.Empty.ToString, ViewState)

        '   initialize buttons
        InitButtonsForLoad()

        Dim ctl As Control
        Dim rfv As New RequiredFieldValidator
        If chkForeignZip.Checked Then
            ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
            If Not (ctl Is Nothing) Then
                rfv = CType(ctl, RequiredFieldValidator)
                rfv.Enabled = False
            End If
            lblState.Text = "State"
        Else
            ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
            If Not (ctl Is Nothing) Then
                rfv = CType(ctl, RequiredFieldValidator)
                rfv.Enabled = True
            End If

        End If

        ValidateStateCaption()

        CommonWebUtilities.RestoreItemValues(dlstBankCodes, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        If Not (txtBankId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim saf As New StudentsAccountsFacade

            '   update bank Info 
            Dim result As String = saf.DeleteBankInfo(txtBankId.Text, Date.Parse(txtModDate.Text))
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind the datalist
                Dim boolStatus As String
                If radStatus.SelectedItem.Text = "Active" Then
                    boolStatus = "True"
                ElseIf radstatus.SelectedItem.Text = "Inactive" Then
                    boolStatus = "False"
                Else
                    boolStatus = "All"
                End If

                BindDataList(boolStatus)

                '   bind an empty new BankInfo
                BindBankData(New BankInfo)

                '   initialize buttons
                InitButtonsForLoad()

                InternationalAddressState = False

                ValidateStateCaption()
            End If
            CommonWebUtilities.RestoreItemValues(dlstBankCodes, Guid.Empty.ToString)
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False

        hplBankAccounts.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit(ByVal bankId As String)
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        '   add the campusId and the bankId to the URL
        hplBankAccounts.NavigateUrl = ViewState("OriginalHPL") + "&cmpid=" + campusId + "&bankId=" + bankId
        hplBankAccounts.Enabled = True
    End Sub
    Private Sub GetBankCodeId(ByVal bankCodeId As String)
        '   Create a StudentsFacade Instance
        Dim saf As New StudentsAccountsFacade

        '   bind BankCode properties
        BindBankData(saf.GetBankInfo(bankCodeId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        Dim boolStatus As String

        BindBankData(New BankInfo)
        '   initialize buttons
        InitButtonsForLoad()
        If radstatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radstatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If

        BindDataList(boolStatus)
        'Header1.EnableHistoryButton(False)
    End Sub
    'Private Function GetInputMaskValue() As String
    '    Dim facInputMasks As New InputMasksFacade
    '    '        Dim correctFormat As Boolean
    '    Dim strMask As String
    '    Dim zipMask As String
    '    '    Dim errorMessage As String
    '    Dim strPhoneReq As String
    '    ' Dim strZipReq As String
    '    Dim strFaxReq As String
    '    Dim objCommon As New CommonUtilities

    '    'Get The Input Mask for Phone/Fax and Zip
    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)


    '    'Get The Input Mask for Phone/Fax and Zip
    '    'Apply Mask Only If Its Local Phone Number
    '    If chkForeignPhone.Checked = False Then
    '        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
    '        'accepts only certain characters as mask characters
    '        txtPhone.Mask = Replace(strMask, "#", "9")
    '        lblPhone.ToolTip = strMask
    '    Else
    '        txtPhone.Mask = ""
    '        lblPhone.ToolTip = ""
    '    End If

    '    'If chkForeignFax.Checked = False Then
    '    '    'Get The Input Mask for Phone/Fax and Zip
    '    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    '    'Replace The Mask Character from # to 9 as Masked Edit TextBox 
    '    '    'accepts only certain characters as mask characters
    '    '    '    txtFax.Mask = Replace(strMask, "#", "9")
    '    '    lblFax.ToolTip = strMask
    '    'Else
    '    txtFax.Mask = ""
    '    lblFax.ToolTip = ""
    '    ' End If

    '    'If chkForeignZip.Checked = False Then
    '    '    'Replace The Mask Character from # to 9 as Masked Edit TextBox 
    '    '    'accepts only certain characters as mask characters
    '    '    txtzip.Mask = Replace(zipMask, "#", "9")
    '    '    lblZip.ToolTip = zipMask
    '    'Else
    '    txtzip.Mask = ""
    '    lblFax.ToolTip = ""
    '    '  End If


    '    'If chkForeignFax.Checked = False Then
    '    '    'Get The Input Mask for Phone/Fax and Zip
    '    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    '    'Replace The Mask Character from # to 9 as Masked Edit TextBox 
    '    '    'accepts only certain characters as mask characters
    '    '    txtFax.Mask = Replace(strMask, "#", "9")
    '    '    'Get The Format Of the input masks and display it next to caption
    '    '    'labels
    '    '    lblFax.ToolTip = strMask
    '    'Else
    '    txtFax.Mask = ""
    '    lblFax.ToolTip = ""
    '    ' End If

    '    'Get The RequiredField Value
    '    strPhoneReq = objCommon.SetRequiredColorMask("Phone")
    '    strFaxReq = objCommon.SetRequiredColorMask("Fax")

    '    ''If The Field Is Required Field Then Color The Masked
    '    ''Edit Control
    '    'If strPhoneReq = "Yes" Then
    '    '    txtPhone.BackColor = Color.FromName("#ffff99")
    '    'End If

    '    'If strFaxReq = "Yes" Then
    '    '    txtFax.BackColor = Color.FromName("#ffff99")
    '    'End If

    'End Function

    'Private Function ValidateFieldsWithInputMasks() As String
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim correctFormat As Boolean
    '    Dim strMask As String
    '    Dim zipMask As String
    '    Dim errorMessage As String = ""

    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

    '    'Validate the phone field format. If the field is empty we should not apply the mask
    '    'agaist it.
    '    If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone.Text)
    '        If correctFormat = False Then
    '            errorMessage = "Incorrect format for phone field." & vbCr
    '        End If
    '    End If

    '    If txtFax.Text <> "" And chkForeignFax.Checked = False Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtFax.Text)
    '        If correctFormat = False Then
    '            errorMessage &= "Incorrect format for fax field." & vbCr
    '        End If
    '    End If

    '    If txtZip.Text <> "" And chkForeignZip.Checked = False Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
    '        If correctFormat = False Then
    '            errorMessage &= "Incorrect format for zip field." & vbCr
    '        End If
    '    End If

    '    Return errorMessage

    'End Function
    Private Sub chkForeignFax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignFax.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        ' Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar=""
        If chkForeignFax.Checked = False Then
            txtFax.Mask = "(###)-###-####"
            txtFax.DisplayMask = "(###)-###-####"
            txtFax.DisplayPromptChar = ""
        Else
            txtFax.Text = ""
            txtFax.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtFax.DisplayMask = ""
            txtFax.DisplayPromptChar = ""
        End If

        CommonWebUtilities.SetFocus(Me.Page, txtFax)
    End Sub

    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        ' Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar=""
        If chkForeignPhone.Checked = False Then
            txtPhone.Mask = "(###)-###-####"
            txtPhone.DisplayMask = "(###)-###-####"
            txtPhone.DisplayPromptChar = ""
        Else
            txtPhone.Text = ""
            txtPhone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtPhone.DisplayMask = ""
            txtPhone.DisplayPromptChar = ""
        End If


        CommonWebUtilities.SetFocus(Me.Page, txtPhone)
    End Sub

    Private Sub ddlCampGrpId_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampGrpId.PreRender

    End Sub

    Private Sub ddlCampGrpId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampGrpId.SelectedIndexChanged

    End Sub
    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignZip.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        'CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        txtAddress1.Focus()
        If chkForeignZip.Checked = True Then
            lblOtherState.Enabled = True
            txtOtherState.Enabled = True
            txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtzip.Text = String.Empty
            ddlStateId.SelectedIndex = 0
            txtzip.MaxLength = 20
        Else
            lblOtherState.Enabled = False
            txtOtherState.Enabled = False
            txtzip.Mask = "#####"
            'txtZip.DisplayMask = "#####"
            txtzip.Text = String.Empty
            txtzip.MaxLength = 5
        End If
    End Sub

    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOtherState.TextChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, chkForeignPhone)
    End Sub
    Private Sub txtPhone_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPhone.TextChanged

    End Sub

    Private Sub txtZip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtZip.TextChanged
        If chkForeignZip.Checked = True Then
            Dim objCommon As New CommonWebUtilities
            CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button
        controlsToIgnore.Add(btnSave)

        'Add javascript code to warn the user about non saved changes
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
Public NotInheritable Class ClientMessageBox
    Private Sub New()
    End Sub

    Public Shared Sub Show(message As String, owner As Control)
        Dim page As Page = If(TryCast(owner, Page), owner.Page)
        If page Is Nothing Then
            Return
        End If

        page.ClientScript.RegisterStartupScript(owner.[GetType](), "ShowMessage", String.Format("<script type='text/javascript'>alert('{0}')</script>", message))

    End Sub

End Class
