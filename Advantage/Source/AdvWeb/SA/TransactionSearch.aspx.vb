﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections.Generic
Imports FAME.Advantage.Reporting
Imports BL = Advantage.Business.Logic.Layer

Partial Class TransactionSearch
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected state As AdvantageSessionState
    Protected WithEvents lbtStudentName As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbtAdjust As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbtReverse As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbtVoid As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbtPrint As System.Web.UI.WebControls.ImageButton

    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Private m_Context As HttpContext

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim userId As String
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not IsPostBack Then

            '   build dropdownlists
            BuildDropDownLists()

            '   get start and end dates for Transaction Dates
            GetStartAndEndDates(campusId)

            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False

        End If

        'set code to send a warning to the user in the client
        RegisterJavaScript()

        If IsPostBack Then
            If Not Request.Params("__EVENTTARGET") Is Nothing And Request.Params("btnSearch") Is Nothing Then
                If Request.Params("__EVENTTARGET").ToString = "lbtStudentName" Then
                    lbtStudentName_Click(Me, New System.EventArgs)
                End If
                If Request.Params("__EVENTTARGET").ToString = "lbtAdjust" Then
                    lbtAdjust_Click(Me, New System.Web.UI.ImageClickEventArgs(0, 0))
                End If
                If Request.Params("__EVENTTARGET").ToString = "lbtReverse" Then
                    lbtReverse_Click(Me, New System.Web.UI.ImageClickEventArgs(0, 0))
                End If
                If Request.Params("__EVENTTARGET").ToString = "lbtVoid" Then
                    lbtVoid_Click(Me, New System.Web.UI.ImageClickEventArgs(0, 0))
                End If
                If Request.Params("__EVENTTARGET").ToString = "lbtPrint" Then
                    lbtPrint_Click(Me, New System.Web.UI.ImageClickEventArgs(0, 0))
                End If
            End If
        End If

    End Sub
    Private Sub BuildDropDownLists()
        BuildTermsDDL()
        BuildAcademicYearsDDL()
        BuildCampusGroupsDDL()
        'BuildTransCodesDDL()
        'This is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'TransCodes DDL()
        ddlList.Add(New AdvantageDDLDefinition(ddlTransCodeId, AdvantageDropDownListName.Trans_Codes, campusId, True, True))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub
    Private Sub BindDatagrid()
        With New StudentsAccountsFacade
            ''Modified by Saraswathi On july 20 2009
            ''For mantis case 16590
            ''Include Voids is added to the search list

            ''Include voids modified to Voids list having three options shoow only voids, do not show void and show all
            ''Modified on august 18 2009 by Saraswathi lakshmanan

            dgrdTransactionSearch.DataSource = .SearchTransactions(campusId, ddlTransCodeId.SelectedValue, Date.Parse(txtTransDateFrom.SelectedDate), ConvertToEndOfDay(Date.Parse(txtTransDateTo.SelectedDate)), Decimal.Parse(txtTransAmountFrom.Text), Decimal.Parse(txtTransAmountTo.Text), txtDescription.Text, txtReference.Text, ddlTermsId.SelectedValue, ddlAcademicYearId.SelectedValue, cbxCharges.Checked, cbxPayments.Checked, cbxDebitAdjustments.Checked, cbxCreditAdjustments.Checked, rbtnvoidlist.SelectedValue)
            dgrdTransactionSearch.DataBind()
        End With
    End Sub

    ''Added by Saraswathi lakshmanan on August 18 2009
    ''To hide the adjust transactions links when voided tranasaction is shown
    Public Function Showlink(ByVal strValue As Boolean, ByVal strvalue1 As Boolean) As Boolean
        If strValue = True Then
            Return False
        Else
            Return strvalue1
        End If
    End Function



    ''Added by Saraswathi lakshmanan on NOv 18 2009
    ''To show text as voided when a  voided tranasaction is shown
    ''17990: QA: Voided transactions can be explicitly mentioned as voided.
    Public Function ShowTransType(ByVal strValue As Boolean) As String
        If strValue = True Then
            Return "Voided"
        Else
            Return ""
        End If
    End Function

    Private Function ConvertToEndOfDay(ByVal [date] As Date) As Date
        Return [date].Subtract([date].TimeOfDay).Add(New TimeSpan(23, 59, 59))
    End Function
    Private Sub BuildTermsDDL()
        '   bind the Terms DDL
        Dim terms As New StudentsAccountsFacade

        With ddlTermsId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.GetAllTerms(True, campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildAcademicYearsDDL()
        '   bind the AcademicYearss DDL
        Dim academicYears As New StudentsAccountsFacade

        With ddlAcademicYearId
            .DataTextField = "AcademicYearDescrip"
            .DataValueField = "AcademicYearId"
            .DataSource = academicYears.GetAllAcademicYears()
            .DataBind()
            .Items.Insert(0, New ListItem("All Years", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("All Campus Groups", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildTransCodesDDL()
        '   bind the TransCodes DDL
        Dim transCodes As New StudentsAccountsFacade

        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeId"
            .DataSource = transCodes.GetAllTransCodes(False)
            .DataBind()
            .Items.Insert(0, New ListItem("All Transaction Codes", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub GetStartAndEndDates(ByVal campusId As String)
        '   instantiate facade
        With New StudentsAccountsFacade
            '   get min and max dates from the transactions Table
            Dim dates() As Date = .GetMinAndMaxDatesFromTransactions(campusId)

            '   fill textboxes with min and max dates
            txtTransDateFrom.SelectedDate = dates(0).ToShortDateString
            txtTransDateTo.SelectedDate = dates(1).ToShortDateString
        End With

    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        '   bind datagrid
        BindDatagrid()
    End Sub
    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand

        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String

        '   set session variables to fill header info
        Session("NameCaption") = "Student"
        Session("NameValue") = CType(e.Item.FindControl("StudentNameLinkButton"), LinkButton).Text
        Session("StudentName") = CType(e.Item.FindControl("StudentNameLinkButton"), LinkButton).Text
        Session("StudentId") = CType(e.Item.FindControl("StudentNameLinkButton"), LinkButton).CommandArgument
        Session("StudentName") = CType(e.Item.FindControl("StudentNameLinkButton"), LinkButton).Text
        Session("StudentId") = CType(e.Item.FindControl("StudentNameLinkButton"), LinkButton).CommandArgument

        Select Case e.CommandName
            ' not used - handled with link button and image post backs
            Case "StudentLedger"
                '   save studentId in the session so that it can be used by the StudentLedger program
                'Session("StudentId") = e.CommandArgument

                Dim defaultCampusId As String

                defaultCampusId = HttpContext.Current.Request.Params("cmpid")
                defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString

                'save studentId in the session so that it can be used by the StudentLedger program
                'Session("StudentID") = e.CommandArgument
                'Session("NameCaption") = "Student"
                'Session("NameValue") = StudentName.GetStudentNameByID(Session("StudentID"))
                'Session("IdValue") = ""
                'Session("IdCaption") = ""
                Session("SEARCH") = 1

                'Set relevant properties on the state object
                objStateInfo.StudentId = e.CommandArgument.ToString()
                objStateInfo.NameCaption = "Student : "
                objStateInfo.NameValue = (New StudentSearchFacade).GetStudentNameByID(e.CommandArgument)
                objStateInfo.CampusId = defaultCampusId

                'Create a new guid to be associated with the employer pages
                strVID = Guid.NewGuid.ToString

                'Add an entry to AdvantageSessionState for this guid and object
                'load Advantage state
                state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
                state(strVID) = objStateInfo

                AdvantageSession.MasterStudentId = objStateInfo.StudentId
                AdvantageSession.MasterLeadId = objStateInfo.LeadId
                AdvantageSession.MasterName = objStateInfo.NameValue

                Dim mruProvider As BL.MRURoutines
                mruProvider = New BL.MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
                mruProvider.InsertMRU(1, objStateInfo.StudentId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

                'save current State
                CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

                '   redirect to studentLedger program
                Response.Redirect("StudentLedger.aspx?resid=116&mod=SA&cmpid=" + campusId + "&VID=" + strVID, True)

            Case "Adjust", "Reverse"
                '   save transactionId in the session so that it can be used by redirected program
                Session("TransactionId") = e.CommandArgument

                '   save commandName in the session so that it can be used by redirected program
                Session("CommandName") = e.CommandName

                If CType(e.Item.FindControl("cbxIsPayment"), CheckBox).Checked Then
                    'this is a Payment
                    '   redirect to studentLedger program
                    'Response.Redirect("PostPayments.aspx?resid=85&mod=SA&cmpid=" + campusId, True)

                    '   setup the properties of the new window
                    Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge      '"toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
                    Dim name As String = "PostPayments"
                    Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=85&mod=SA&cmpid=" + campusId
                    CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

                ElseIf CType(e.Item.FindControl("cbxIsRefund"), CheckBox).Checked Then
                    'this is a Refund
                    '   redirect to PostRefunds program
                    'Response.Redirect("PostRefunds.aspx?resid=94&mod=SA&cmpid=" + campusId, True)

                    '   setup the properties of the new window
                    Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
                    Dim name As String = "PostRefunds"
                    Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=94&mod=SA&cmpid=" + campusId
                    CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

                Else
                    'this is other transaction
                    '   redirect to PostCharges program
                    'Response.Redirect("PostCharges.aspx?resid=84&mod=SA&cmpid=" + campusId, True)
                    '   setup the properties of the new window

                    Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
                    Dim name As String = "PostCharges"
                    Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=84&mod=SA&cmpid=" + campusId
                    CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

                End If
            Case "Void"
                lbtVoid_Click(Me, New System.Web.UI.ImageClickEventArgs(0, 0))
            Case "Print"
                lbtPrint_Click(Me, New System.Web.UI.ImageClickEventArgs(0, 0))
        End Select

    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
    End Sub

    Private Sub lbtStudentName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtStudentName.Click
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String

        Dim defaultCampusId As String

        defaultCampusId = HttpContext.Current.Request.Params("cmpid")
        defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString

        'save studentId in the session so that it can be used by the StudentLedger program
        'Session("StudentID") = e.CommandArgument
        'Session("NameCaption") = "Student"
        'Session("NameValue") = StudentName.GetStudentNameByID(Session("StudentID"))
        'Session("IdValue") = ""
        'Session("IdCaption") = ""
        Session("SEARCH") = 1

        'Set relevant properties on the state object
        Dim cmdArgument As String = Request.Params("__EVENTARGUMENT").ToString
        objStateInfo.StudentId = cmdArgument
        objStateInfo.NameCaption = "Student : "
        objStateInfo.NameValue = (New StudentSearchFacade).GetStudentNameByID(cmdArgument)
        objStateInfo.CampusId = defaultCampusId

        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo

        AdvantageSession.MasterStudentId = objStateInfo.StudentId
        AdvantageSession.MasterLeadId = objStateInfo.LeadId
        AdvantageSession.MasterName = objStateInfo.NameValue

        Dim mruProvider As BL.MRURoutines
        mruProvider = New BL.MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
        mruProvider.InsertMRU(1, objStateInfo.StudentId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        '   redirect to studentLedger program
        Response.Redirect("StudentLedger.aspx?resid=116&mod=SA&cmpid=" + campusId + "&VID=" + strVID, True)

    End Sub
    Private Sub lbtAdjust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lbtAdjust.Click

        'get three arguments
        Dim cmdArgument() As String = CType(Request.Params("__EVENTARGUMENT"), String).Split(New Char() {";"c}, 3)


        '   save transactionId in the session so that it can be used by redirected program
        Session("TransactionId") = cmdArgument(0)

        '   save commandName in the session so that it can be used by redirected program
        Session("CommandName") = "Adjust"

        'get student name for the session
        Session("StudentName") = (New StudentSearchFacade).GetStudentNameByTransactionId(Session("TransactionId"))

        If Boolean.Parse(cmdArgument(1)) Then
            'this is a Payment
            '   redirect to studentLedger program
            'Response.Redirect("PostPayments.aspx?resid=85&mod=SA&cmpid=" + campusId, True)


            '   setup the properties of the new window
            Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
            Dim name As String = "PostPayments"
            Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=85&mod=SA&cmpid=" + campusId
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        ElseIf Boolean.Parse(cmdArgument(2)) Then
            'this is a Refund
            '   redirect to PostRefunds program
            'Response.Redirect("PostRefunds.aspx?resid=94&mod=SA&cmpid=" + campusId, True)

            '   setup the properties of the new window
            Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
            Dim name As String = "PostRefunds"
            Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=94&mod=SA&cmpid=" + campusId
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        Else
            'this is other transaction
            '   redirect to PostCharges program
            'Response.Redirect("PostCharges.aspx?resid=84&mod=SA&cmpid=" + campusId, True)
            '   setup the properties of the new window

            Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
            Dim name As String = "PostCharges"
            Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=84&mod=SA&cmpid=" + campusId
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        End If
        BindDatagrid()
    End Sub
    Private Sub lbtReverse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lbtReverse.Click
        'get three arguments
        Dim cmdArgument() As String = CType(Request.Params("__EVENTARGUMENT"), String).Split(New Char() {";"c}, 3)


        '   save transactionId in the session so that it can be used by redirected program
        Session("TransactionId") = cmdArgument(0)

        '   save commandName in the session so that it can be used by redirected program
        Session("CommandName") = "Reverse"

        'get student name for the session
        Session("StudentName") = (New StudentSearchFacade).GetStudentNameByTransactionId(Session("TransactionId"))

        If Boolean.Parse(cmdArgument(1)) Then
            'this is a Payment
            '   redirect to studentLedger program
            'Response.Redirect("PostPayments.aspx?resid=85&mod=SA&cmpid=" + campusId, True)


            '   setup the properties of the new window
            Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
            Dim name As String = "PostPayments"
            Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=85&mod=SA&cmpid=" + campusId
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        ElseIf Boolean.Parse(cmdArgument(2)) Then
            'this is a Refund
            '   redirect to PostRefunds program
            'Response.Redirect("PostRefunds.aspx?resid=94&mod=SA&cmpid=" + campusId, True)

            '   setup the properties of the new window
            Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
            Dim name As String = "PostRefunds"
            Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=94&mod=SA&cmpid=" + campusId
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        Else
            'this is other transaction
            '   redirect to PostCharges program
            'Response.Redirect("PostCharges.aspx?resid=84&mod=SA&cmpid=" + campusId, True)
            '   setup the properties of the new window

            Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=470px"
            Dim name As String = "PostCharges"
            Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/SA/" + name + ".aspx?resid=84&mod=SA&cmpid=" + campusId
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        End If
        BindDatagrid()
    End Sub
    Private Sub lbtVoid_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lbtVoid.Click
        'get one argument
        Dim cmdArgument() As String = CType(Request.Params("__EVENTARGUMENT"), String).Split(New Char() {";"c}, 1)

        '   save transactionId in the session so that it can be used by redirected program
        'Session("TransactionId") = cmdArgument(0)

        Dim result As String = (New StudentsAccountsFacade).VoidTransaction(cmdArgument(0), AdvantageSession.UserState.UserName)

        '   bind datagrid
        BindDatagrid()
    End Sub
    Private Sub RegisterJavaScript()
        'Dim str As String = "<script type='text/javascript'>function WU(){alert('hello Anatoly');If(confirm('Are you sure you want to void this record?')){}else{return false}}</script>"
        Dim str As String = "<script type='text/javascript'>function WU(){alert('The selected record will be VOIDED')}</script>"
        ClientScript.RegisterClientScriptBlock(GetType(String), "WarnUser", str)
    End Sub

    Protected Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                'Dim ib As HyperLink = CType(e.Item.FindControl("HyperLink4"), HyperLink)
                ''ib.Attributes.Add("onclick", "If(confirm('Are you sure you want to void this record?')){}else{return false}")
                'ib.Attributes.Add("onclick", "WU();")
        End Select
    End Sub

    ' DE7793 - 7/18/2012 Janet Robinson switch receipt to ssrs report
    Protected Sub PrintStudentReceipt(ByVal TransactionId As String, UniqueId As String)
        Dim getReportAsBytes As [Byte]()
        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") '"/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
        getReportAsBytes = (New Logic.PrintStudentReceipt).RenderReport("pdf", TransactionId, _
                                                                               UniqueId, _
                                                                               AdvantageSession.UserState.UserName.ToString, _
                                                                               strReportPath, _
                                                                               AdvantageSession.UserState.CampusId.ToString)
        ExportReport("pdf", getReportAsBytes)
    End Sub
    ' DE7793 - 7/18/2012 Janet Robinson switch receipt to ssrs report
    Private Sub ExportReport(ByVal ExportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case ExportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script As String = String.Format("window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub
    ' DE7793 - 7/18/2012 Janet Robinson switch receipt to ssrs report
    Private Sub lbtPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lbtPrint.Click
        'get one argument
        Dim cmdArgument() As String = CType(Request.Params("__EVENTARGUMENT"), String).Split(New Char() {";"c}, 3)

        '   save transactionId in the session so that it can be used by redirected program
        'Session("TransactionId") = cmdArgument(0)

        'get postPaymentInfo for this receipt
        Dim transactionId As String = cmdArgument(0)
        Dim postPaymentInfo As PostPaymentInfo = (New StudentsAccountsFacade).GetPostPaymentInfo(transactionId)


        PrintStudentReceipt(postPaymentInfo.PostPaymentId, postPaymentInfo.UniqueId)

        BindDatagrid()

    End Sub

End Class
