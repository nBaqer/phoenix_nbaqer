Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class RateSchedules
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents txtRateSchedulesId As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents allDataset As System.Data.DataSet
    Protected WithEvents dataGridTable As System.Data.DataTable
    Protected WithEvents dataListTable As System.Data.DataTable

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            'delete any previous session value
            Session("RateScheduleId") = Nothing

            '   build dropdownlists
            BuildDropDownLists()

            '   get allDataset from the DB
            '   bind datalist

            With New StudentsAccountsFacade
                allDataset = .GetRateSchedulesDS()
            End With

            '   save it on the session
            Session("AllDataset") = allDataset

            '   initialize buttons
            'InitButtonsForLoad()
        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            'Call the procedure for edit
            'InitButtonsForEdit()
        End If

        '   create Dataset and Tables
        CreateDatasetAndTables()

        '   the first time we have to bind an empty datagrid
        If Not IsPostBack Then
            PrepareForNewData()
        End If

    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind RateSchedules datalist
        dlstRateSchedules.DataSource = New DataView(dataListTable, rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstRateSchedules.DataBind()

        '   if RateScheduleId exists do an insert else do an update
        If Not Session("RateScheduleId") Is Nothing Then
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstRateSchedules, Session("RateScheduleId"), ViewState, Header1)
        End If

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildTuitionCategoriesDDL(ByVal ddl As DropDownList)

        Dim ddlDS As DataSet
        '   save TuitionCategories in the Viewstate for subsequent use
        If ViewState("TuitionCategoriesDS") Is Nothing Then
            ddlDS = (New StudentsAccountsFacade).GetAllTuitionCategories("True")
            ViewState("TuitionCategoriesDS") = ddlDS
        Else
            ddlDS = ViewState("TuitionCategoriesDS")
        End If

        '   bind the TuitionCategories DDL
        With ddl
            .DataTextField = "TuitionCategoryDescrip"
            .DataValueField = "TuitionCategoryId"
            .DataSource = ddlDS
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dlstRateSchedules_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstRateSchedules.ItemCommand
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        '   this portion of code added to refresh data from the database
        GetRateSchedulesDS()

        '   save RateScheduleId in session
        Dim strId As String = dlstRateSchedules.DataKeys(e.Item.ItemIndex).ToString()
        Session("RateScheduleId") = strId

        '   get the row with the data to be displayed
        Dim row() As DataRow = dataListTable.Select("RateScheduleId=" + "'" + CType(Session("RateScheduleId"), String) + "'")
        'Dim a As Boolean = CType(Container.DataItem, System.Data.DataRowView).Row.RowState Or System.Data.DataRowState.Added + System.Data.DataRowState.Modified

        '   populate controls with row data
        txtRateScheduleId.Text = CType(row(0)("RateScheduleId"), Guid).ToString
        txtRateScheduleCode.Text = row(0)("RateScheduleCode")
        ddlStatusId.SelectedValue = CType(row(0)("StatusId"), Guid).ToString
        txtRateScheduleDescrip.Text = row(0)("RateScheduleDescrip")
        If (Not row(0)("CampGrpId") Is System.DBNull.Value) Then ddlCampGrpId.SelectedValue = CType(row(0)("CampGrpId"), Guid).ToString Else ddlCampGrpId.SelectedIndex = 0

        '   Bind DataGrid
        dgrdRateScheduleDetails.EditItemIndex = -1
        BindDataGrid()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstRateSchedules, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        '   show footer
        dgrdRateScheduleDetails.ShowFooter = True

        CommonWebUtilities.RestoreItemValues(dlstRateSchedules, e.CommandArgument.ToString)
    End Sub
    Private Sub BindRateSchedulesData(ByVal rateSchedule As RateScheduleInfo)
        With rateSchedule
            chkIsInDB.Checked = .IsInDB
            txtRateScheduleId.Text = .RateScheduleId
            txtRateScheduleCode.Text = .Code
            If Not (rateSchedule.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = rateSchedule.StatusId
            txtRateScheduleDescrip.Text = .Description
            If Not (rateSchedule.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = rateSchedule.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   if any of the footer fields of the datagrid is not blank.. send an error message 
        If Not AreFooterFieldsBlank() Then
            '   Display Error Message
            DisplayErrorMessage("To use ""Save"" all fields in the Rate Schedules footer must be blank")
            Exit Sub
        End If

        '   if RateScheduleId does not exist do an insert else do an update
        If Session("RateScheduleId") Is Nothing Then
            '   do an insert
            BuildNewRowInDataList(Guid.NewGuid.ToString)
        Else
            '   do an update
            '   get the row with the data to be displayed
            Dim row() As DataRow = dataListTable.Select("RateScheduleId=" + "'" + CType(Session("RateScheduleId"), String) + "'")

            '   update row data
            UpdateRowData(row(0))

        End If

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors then bind and set selected style to the datalist and
        '   initialize buttons 
        If Customvalidator1.IsValid Then

            '   bind datalist
            BindDataList()

            'dlstRateSchedules.DataSource = New DataView(dataListTable, Nothing, "RateScheduleDescrip asc", DataViewRowState.CurrentRows)
            'dlstRateSchedules.DataBind()

            '   set Style to Selected Item
            ' CommonWebUtilities.SetStyleToSelectedItem(dlstRateSchedules, Session("RateScheduleId"), ViewState, Header1)

            '   initialize buttons
            InitButtonsForEdit()

        End If
        CommonWebUtilities.RestoreItemValues(dlstRateSchedules, CType(Session("RateScheduleId"), String))
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        ''   try to update the DB and show any error message
        'UpdateDB()

        ''   if there were no errors prepare screen for new data
        'If Customvalidator1.IsValid Then
        '    '   Prepare screen for new data
        '    PrepareForNewData()

        '   reset dataset to previous state. delete all changes
        allDataset.RejectChanges()
        PrepareForNewData()

        '   reset Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstRateSchedules, Guid.Empty.ToString, ViewState, Header1)

        CommonWebUtilities.RestoreItemValues(dlstRateSchedules, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        '   get all rows to be deleted
        Dim rows() As DataRow = dataGridTable.Select("RateScheduleId=" + "'" + CType(Session("RateScheduleId"), String) + "'")

        '   delete all rows from dataGrid table
        Dim i As Integer
        If rows.Length > 0 Then
            For i = 0 To rows.Length - 1
                rows(i).Delete()
            Next
        End If

        '   get the row to be deleted in RateSchedules table
        Dim row() As DataRow = dataListTable.Select("RateScheduleId=" + "'" + CType(Session("RateScheduleId"), String) + "'")

        '   delete row from the table
        row(0).Delete()

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   Prepare screen for new data
            PrepareForNewData()
        End If
        CommonWebUtilities.RestoreItemValues(dlstRateSchedules, Guid.Empty.ToString)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Sub CreateDatasetAndTables()

        '   Get dataGrid Dataset from the session and create tables
        allDataset = Session("AllDataset")
        dataGridTable = allDataset.Tables("RateScheduleDetails")
        dataListTable = allDataset.Tables("RateSchedules")

    End Sub
    Private Sub PrepareForNewData()

        '   bind RateSchedules datalist
        BindDataList()
        'dlstRateSchedules.DataSource = dataListTable
        'dlstRateSchedules.DataBind()

        '   save RateScheduleId in session
        Session("RateScheduleId") = Nothing

        '   Bind DataGrid
        BindDataGrid()

        '   bind an empty new RateSchedulesInfo
        BindRateSchedulesData(New RateScheduleInfo)

        '   initialize buttons
        InitButtonsForLoad()

    End Sub
    Private Sub SwapViewOrders(ByVal id1 As String, ByVal id2 As String)

        '   get the rows with each id
        Dim row1(), row2() As DataRow
        row1 = dataGridTable.Select("RateScheduleDetailId=" + "'" + id1 + "'")
        row2 = dataGridTable.Select("RateScheduleDetailId=" + "'" + id2 + "'")

        '   swap value of viewOrder
        Dim viewOrder As Integer
        viewOrder = row1(0)("ViewOrder")
        row1(0)("ViewOrder") = row2(0)("ViewOrder")
        row2(0)("ViewOrder") = viewOrder
    End Sub

    Private Sub dgrdRateScheduleDetails_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdRateScheduleDetails.ItemCommand
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdRateScheduleDetails.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdRateScheduleDetails.ShowFooter = False

                '   user hit "Update" inside the datagrid
            Case "Update"
                '   process only if the edit textboxes are nonblank and valid
                If IsDataInEditTextboxesValid(e) And Not AreEditFieldsBlank(e) Then
                    '   get the row to be updated
                    Dim row() As DataRow = dataGridTable.Select("RateScheduleDetailId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditRateScheduleDetailId"), Label).Text).ToString + "'")
                    '   fill new values in the row
                    If Not CType(e.Item.FindControl("txtEditMinUnits"), TextBox).Text = "" Then row(0)("MinUnits") = Integer.Parse(CType(e.Item.FindControl("txtEditMinUnits"), TextBox).Text) Else row(0)("MinUnits") = 0
                    If Not CType(e.Item.FindControl("txtEditMaxUnits"), TextBox).Text = "" Then row(0)("MaxUnits") = Integer.Parse(CType(e.Item.FindControl("txtEditMaxUnits"), TextBox).Text) Else row(0)("MaxUnits") = 0

                    If Not CType(e.Item.FindControl("ddlEditTuitionCategoryId"), DropDownList).SelectedValue = Guid.Empty.ToString Then
                        row(0)("TuitionCategoryId") = New Guid(CType(e.Item.FindControl("ddlEditTuitionCategoryId"), DropDownList).SelectedValue)
                    Else
                        row(0)("TuitionCategoryId") = System.DBNull.Value
                    End If

                    If Not CType(e.Item.FindControl("txtEditFlatAmount"), TextBox).Text = "" Then row(0)("FlatAmount") = Decimal.Parse(CType(e.Item.FindControl("txtEditFlatAmount"), TextBox).Text) Else row(0)("FlatAmount") = 0
                    If Not CType(e.Item.FindControl("txtEditRate"), TextBox).Text = "" Then row(0)("Rate") = Decimal.Parse(CType(e.Item.FindControl("txtEditRate"), TextBox).Text) Else row(0)("Rate") = 0
                    row(0)("UnitId") = CType(e.Item.FindControl("ddlEditUnitId"), DropDownList).SelectedValue
                    row(0)("Unit") = ConvertUnitIdToUnit(row(0)("UnitId"))
                    row(0)("ModUser") = Session("UserName")
                    row(0)("ModDate") = Date.Now

                    '   no record is selected
                    dgrdRateScheduleDetails.EditItemIndex = -1

                    '   show footer
                    dgrdRateScheduleDetails.ShowFooter = True

                    '   Bind DataList
                    BindDataList()
                Else
                    DisplayErrorMessage("Invalid Data during the edit of an existing row of Rate Schedules")
                    Exit Sub
                End If

                '   user hit "Cancel"
            Case "Cancel"

                '   set no record selected
                dgrdRateScheduleDetails.EditItemIndex = -1

                '   show footer
                dgrdRateScheduleDetails.ShowFooter = True

                '   user hit "Delete" inside the datagrid
            Case "Delete"

                '   get the row to be deleted
                Dim row() As DataRow = dataGridTable.Select("RateScheduleDetailId=" + "'" + CType(e.Item.FindControl("lblEditRateScheduleDetailId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                '   set no record selected
                dgrdRateScheduleDetails.EditItemIndex = -1

                '   show footer
                dgrdRateScheduleDetails.ShowFooter = True

            Case "AddNewRow"
                '   process only if the footer textboxes are nonblank and valid
                If IsDataInFooterTextboxesValid(e) And Not AreFooterFieldsBlank() Then
                    '   If the dataList item doesn't have an Id assigned.. create one and assign it.
                    If Session("RateScheduleId") Is Nothing Then
                        '   build a new row in dataList table
                        BuildNewRowInDataList(Guid.NewGuid.ToString)
                        '   bind datalist
                        BindDataList()
                    End If

                    '   get a new row from the dataGridTable
                    Dim newRow As DataRow = dataGridTable.NewRow

                    '   fill the new row with values
                    newRow("RateScheduleDetailId") = Guid.NewGuid
                    newRow("RateScheduleId") = New Guid(CType(Session("RateScheduleId"), String))
                    If Not CType(e.Item.FindControl("txtFooterMinUnits"), TextBox).Text = "" Then newRow("MinUnits") = Integer.Parse(CType(e.Item.FindControl("txtFooterMinUnits"), TextBox).Text) Else newRow("MinUnits") = 0
                    If Not CType(e.Item.FindControl("txtFooterMaxUnits"), TextBox).Text = "" Then newRow("MaxUnits") = Integer.Parse(CType(e.Item.FindControl("txtFooterMaxUnits"), TextBox).Text) Else newRow("MaxUnits") = 32767
                    If Not CType(e.Item.FindControl("ddlFooterTuitionCategoryId"), DropDownList).SelectedValue = Guid.Empty.ToString Then newRow("TuitionCategoryId") = New Guid(CType(e.Item.FindControl("ddlFooterTuitionCategoryId"), DropDownList).SelectedValue) Else newRow("TuitionCategoryId") = System.DBNull.Value
                    If Not CType(e.Item.FindControl("txtFooterFlatAmount"), TextBox).Text = "" Then newRow("FlatAmount") = Decimal.Parse(CType(e.Item.FindControl("txtFooterFlatAmount"), TextBox).Text) Else newRow("FlatAmount") = 0
                    If Not CType(e.Item.FindControl("txtFooterRate"), TextBox).Text = "" Then newRow("Rate") = Decimal.Parse(CType(e.Item.FindControl("txtFooterRate"), TextBox).Text) Else newRow("Rate") = 0
                    newRow("UnitId") = CType(e.Item.FindControl("ddlFooterUnitId"), DropDownList).SelectedValue
                    newRow("Unit") = ConvertUnitIdToUnit(newRow("UnitId"))
                    newRow("ViewOrder") = GetMaxViewOrder(dataGridTable) + 1
                    newRow("ModUser") = Session("UserName")
                    newRow("ModDate") = Date.Now

                    '   add row to the table
                    newRow.Table.Rows.Add(newRow)
                Else
                    DisplayErrorMessage("Invalid data during adding of a new row of Rate Schedules")
                    Exit Sub
                End If

                '   user hit "Up"
            Case "Up"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(dataGridTable, "RateScheduleId=" + "'" + CType(Session("RateScheduleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim RateScheduleDetailId As String = CType(e.Item.FindControl("lblRateScheduleDetailId"), Label).Text

                '   get the position of this item and swap viewOrder values with prevous item
                Dim i As Integer
                For i = 1 To dv.Count - 1
                    If CType(dv(i).Row("RateScheduleDetailId"), Guid).ToString = RateScheduleDetailId Then
                        SwapViewOrders(CType(dv(i).Row("RateScheduleDetailId"), Guid).ToString, CType(dv(i - 1).Row("RateScheduleDetailId"), Guid).ToString)
                        Exit For
                    End If
                Next
                '   bind datalist
                BindDataList()

                '   user hit "Down" 
            Case "Down"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(dataGridTable, "RateScheduleId=" + "'" + CType(Session("RateScheduleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim RateScheduleDetailId As String = CType(e.Item.FindControl("lblRateScheduleDetailId"), Label).Text

                '   get the position of this item and swap viewOrder values with next item
                Dim i As Integer
                For i = 0 To dv.Count - 2
                    If CType(dv(i).Row("RateScheduleDetailId"), Guid).ToString = RateScheduleDetailId Then
                        SwapViewOrders(CType(dv(i).Row("RateScheduleDetailId"), Guid).ToString, CType(dv(i + 1).Row("RateScheduleDetailId"), Guid).ToString)
                        Exit For
                    End If
                Next
                '   bind datalist
                BindDataList()

        End Select

        '   Bind DataGrid
        BindDataGrid()

    End Sub
    Private Function GetMaxViewOrder(ByVal table As DataTable) As Integer
        Dim i As Integer
        GetMaxViewOrder = 1

        '   if there are no rows just return 1 
        Dim rows() As DataRow = table.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If rows.Length = 0 Then Return 1

        For i = 0 To rows.Length - 1
            If CType(rows(i)("ViewOrder"), Integer) > GetMaxViewOrder Then
                GetMaxViewOrder = CType(rows(i)("ViewOrder"), Integer)
            End If
        Next
    End Function
    Private Sub BuildNewRowInDataList(ByVal theGuid As String)
        '   get a new row
        Dim newRow As DataRow = dataListTable.NewRow

        '   create a Guid for DataList if it has not been created 
        Session("RateScheduleId") = theGuid
        newRow("RateScheduleId") = New Guid(theGuid)

        '   update row with web controls data
        UpdateRowData(newRow)

        '   add row to the table
        newRow.Table.Rows.Add(newRow)

    End Sub
    Private Sub UpdateDB()
        '   update DB
        With New StudentsAccountsFacade
            Dim result As String = .UpdateRateSchedulesDS(allDataset)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get a new version of RateSchedules dataset because some other users may have added,
                '   updated or deleted records from the DB
                allDataset = .GetRateSchedulesDS()
                Session("AllDataset") = allDataset
                CreateDatasetAndTables()
            End If
        End With
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
            If allDataset.HasChanges() Then
                'Set the Delete Button so it prompts the user for confirmation when clicked
                btnnew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
            Else
                btnnew.Attributes.Remove("onclick")
            End If

            '   save dataset systems in session
            Session("AllDataset") = allDataset
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnsave)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            BindToolTip()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub
    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdRateScheduleDetails.Controls(0).Controls(dgrdRateScheduleDetails.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtFooterMinUnits"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterMaxUnits"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterFlatAmount"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterRate"), TextBox).Text = "" Then Return False
        Return True
    End Function
    Private Function AreEditFieldsBlank(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not CType(e.Item.FindControl("txtEditMinUnits"), TextBox).Text = "" Then Return False
        If Not CType(e.Item.FindControl("txtEditMaxUnits"), TextBox).Text = "" Then Return False
        If Not CType(e.Item.FindControl("txtEditFlatAmount"), TextBox).Text = "" Then Return False
        If Not CType(e.Item.FindControl("txtEditRate"), TextBox).Text = "" Then Return False
        Return True
    End Function
    Private Sub UpdateRowData(ByVal row As DataRow)
        '   update row data
        row("RateScheduleCode") = txtRateScheduleCode.Text
        row("StatusId") = New Guid(ddlStatusId.SelectedValue)
        row("RateScheduleDescrip") = txtRateScheduleDescrip.Text
        row("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
        row("ModUser") = Session("UserName")
        row("ModDate") = Date.Parse(Date.Now.ToString)
    End Sub
    Private Function IsDataInFooterTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not IsTextBoxValidInteger(e.Item.FindControl("txtFooterMinUnits")) Then Return False
        If Not IsTextBoxValidInteger(e.Item.FindControl("txtFooterMaxUnits")) Then Return False
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtFooterFlatAmount")) Then Return False
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtFooterRate")) Then Return False
        If Not AreFlatAmountAndRateValid(e.Item.FindControl("txtFooterFlatAmount"), e.Item.FindControl("txtFooterRate")) Then Return False
        If Not IsMaxUnitsGreaterOrEqualThanMinUnits(e.Item.FindControl("txtFooterMinUnits"), e.Item.FindControl("txtFooterMaxUnits")) Then Return False
        Return True
    End Function
    Private Function IsDataInEditTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not IsTextBoxValidInteger(e.Item.FindControl("txtEditMinUnits")) Then Return False
        If Not IsTextBoxValidInteger(e.Item.FindControl("txtEditMaxUnits")) Then Return False
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtEditFlatAmount")) Then Return False
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtEditRate")) Then Return False
        If Not AreFlatAmountAndRateValid(e.Item.FindControl("txtEditFlatAmount"), e.Item.FindControl("txtEditRate")) Then Return False
        If Not IsMaxUnitsGreaterOrEqualThanMinUnits(e.Item.FindControl("txtEditMinUnits"), e.Item.FindControl("txtEditMaxUnits")) Then Return False
        Return True
    End Function
    Private Function IsTextBoxValidInteger(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        Try
            Dim i As Integer = Integer.Parse(textbox.Text)
            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function IsTextBoxValidDecimal(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        Try
            Dim d As Decimal = Decimal.Parse(textbox.Text, System.Globalization.NumberStyles.AllowDecimalPoint + System.Globalization.NumberStyles.AllowThousands)
            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Sub PopulateFooterDropDownLists()
        Dim footer As Control = dgrdRateScheduleDetails.Controls(0).Controls(dgrdRateScheduleDetails.Controls(0).Controls.Count - 1)
        BuildTuitionCategoriesDDL(CType(footer.FindControl("ddlFooterTuitionCategoryId"), DropDownList))
    End Sub
    Protected Function getParentRow(ByVal row As DataRowView) As String
        Return row.Row.GetParentRow("TuitionCategoriesRateScheduleDetails")("TuitionCategoryDescrip")
    End Function
    Private Function ConvertUnitIdToUnit(ByVal id As Integer) As String
        If id = 0 Then Return "Credit Hour" Else Return "Clock Hour"
    End Function

    Private Sub dgrdRateScheduleDetails_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdRateScheduleDetails.ItemDataBound
        '   process only rows in the EditItem section
        Select Case e.Item.ItemType
            Case ListItemType.EditItem
                '   build TuitionCategories dropdownlist
                Dim ddl As DropDownList = CType(e.Item.FindControl("ddlEditTuitionCategoryId"), DropDownList)
                BuildTuitionCategoriesDDL(ddl)
                '   set Selected Value
                If Not CType(e.Item.DataItem, DataRowView).Row("TuitionCategoryId") Is System.DBNull.Value Then
                    ddl.SelectedValue = CType(CType(e.Item.DataItem, DataRowView).Row("TuitionCategoryId"), Guid).ToString
                Else
                    ddl.SelectedIndex = 0
                End If
        End Select
    End Sub
    Private Sub BindDataGrid()
        '   bind datagrid to dataGridTable
        dgrdRateScheduleDetails.DataSource = New DataView(dataGridTable, "RateScheduleId=" + "'" + CType(Session("RateScheduleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
        dgrdRateScheduleDetails.DataBind()

        '   PopulateFooterDropDownLists
        PopulateFooterDropDownLists()
    End Sub
    Private Function AreFlatAmountAndRateValid(ByVal txtFlatAmount As TextBox, ByVal txtRate As TextBox) As Boolean
        '   only one box should be filled: the Flat Amount or the Rate amount but not both
        Return (IsTextBoxBlank(txtFlatAmount) Xor IsTextBoxBlank(txtRate))
    End Function
    Private Function IsMaxUnitsGreaterOrEqualThanMinUnits(ByVal txtMinUnits As TextBox, ByVal txtMaxUnits As TextBox) As Boolean
        '   MaxUnits must be greater or equal than MinUnits
        Try
            Dim i As Integer = Integer.Parse(txtMinUnits.Text)
            Dim j As Integer = Integer.Parse(txtMaxUnits.Text)
            If j >= i Then Return True Else Return False
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function IsTextBoxBlank(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True Else Return False
    End Function

    Private Sub txtRateScheduleDescrip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRateScheduleDescrip.TextChanged, txtRateScheduleCode.TextChanged, ddlStatusId.SelectedIndexChanged, ddlCampGrpId.SelectedIndexChanged
        '   get the row with the data 
        Dim row() As DataRow = dataListTable.Select("RateScheduleId=" + "'" + CType(Session("RateScheduleId"), String) + "'")

        '   update row data
        If row.Length > 0 Then
            UpdateRowData(row(0))
        End If

        '   bind Datalist
        BindDataList()
    End Sub
    Protected Function HasChanges(ByVal id As String) As Boolean
        Dim rows() As DataRow = dataGridTable.Select("RateScheduleId=" + "'" + id + "'", Nothing, DataViewRowState.Added + DataViewRowState.ModifiedCurrent)
        If rows.Length > 0 Then Return True Else Return False
    End Function

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   initialize buttons - present inside Preparefornewdata
            'InitButtonsForLoad()

            '   bind an empty datagrid
            PrepareForNewData()

        End If
        'Header1.EnableHistoryButton(False)
    End Sub
    Private Sub GetRateSchedulesDS()

        '   get allDataset from the DB
        With New StudentsAccountsFacade
            allDataset = .GetRateSchedulesDS()
        End With

        '   save it on the session
        Session("AllDataset") = allDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()

    End Sub
    Protected Function GetTuitionCategoryDescription(ByVal drv As DataRowView) As String
        If Not drv.Row.IsNull("TuitionCategoryId") Then
            Return drv.Row.GetParentRow("TuitionCategoriesRateScheduleDetails")("TuitionCategoryDescrip")
        Else
            Return "No Tuition Category"
        End If

    End Function
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
