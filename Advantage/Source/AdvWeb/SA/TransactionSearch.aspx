﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="TransactionSearch.aspx.vb" Inherits="TransactionSearch" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script type="text/javascript">


        function Adjust(id) {
            var ctl = document.getElementById(id.replace(/Image1/, 'txtParameters'));
            __doPostBack('lbtAdjust', ctl.value);

        }
        function Reverse(id) {
            var ctl = document.getElementById(id.replace(/Image2/, 'txtParameters'));
            __doPostBack('lbtReverse', ctl.value);
        }

        function Void(id) {
            if (confirm('Are you sure you want to VOID this record?')) {
                var ctl = document.getElementById(id.replace(/Image3/, 'txtParameters'))
                __doPostBack('lbtVoid', ctl.value);
            }
            else { return false }
        }

        function Print(id) {
            var ctl = document.getElementById(id.replace(/Image4/, 'txtParameters'));
            __doPostBack('lbtPrint', ctl.value);
        }


    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Both">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2"></td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleft" style="height: expression(document.body.clientHeight - 194 + 'px')">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblCampGrpId" runat="server" CssClass="label" Visible="false">Campus Group</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">
                                        <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Visible="false" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblTransCodeId" runat="server" CssClass="label">Transaction Code</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">
                                        <asp:DropDownList ID="ddlTransCodeId" runat="server" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblTransDateFrom" runat="server" CssClass="label">Start Date</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">


                                        <telerik:RadDatePicker ID="txtTransDateFrom" runat="server" MinDate="1/1/1900" Width="200px">
                                        </telerik:RadDatePicker>

                                        <asp:CompareValidator ID="TransFromDateCompareValidator" runat="server" ErrorMessage="Invalid Transaction From Date"
                                            Display="None" ControlToValidate="txtTransDateFrom" Type="Date" ValueToCompare="2001-01-01"
                                            Operator="GreaterThanEqual">
                                        </asp:CompareValidator>

                                        <asp:RequiredFieldValidator ID="rfvStartDate"
                                            runat="server" ErrorMessage="Start Date can not be blank" Display="None" ControlToValidate="txtTransDateFrom">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <tr>
                                        <td class="transactionsearch">
                                            <asp:Label ID="lblTransDateTo" runat="server" CssClass="label">End Date</asp:Label>
                                        </td>
                                        <td class="transactionsearch2">


                                            <telerik:RadDatePicker ID="txtTransDateTo" runat="server" MinDate="1/1/1900" Width="200px">
                                            </telerik:RadDatePicker>


                                            <asp:CompareValidator ID="TransDateToCompareValidator" runat="server" ErrorMessage="Invalid Transaction From Date"
                                                Display="None" ControlToValidate="txtTransDateTo" Type="Date" Operator="GreaterThanEqual"
                                                ControlToCompare="txtTransDateFrom">
                                            </asp:CompareValidator>

                                            <asp:RequiredFieldValidator
                                                ID="rfvEndDate" runat="server" ErrorMessage="End Date can not be blank" Display="None"
                                                ControlToValidate="txtTransDateTo">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblTransAmountFrom" runat="server" CssClass="label">Minimum Amount</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">
                                        <asp:TextBox ID="txtTransAmountFrom" runat="server" CssClass="textbox">0.0</asp:TextBox><asp:CompareValidator
                                            ID="FromAmountCompareValidator" runat="server" ErrorMessage="Invalid Minimum Amount"
                                            Display="None" ControlToValidate="txtTransAmountFrom" Type="Currency" ValueToCompare="0.00"
                                            Operator="GreaterThanEqual"></asp:CompareValidator><asp:RequiredFieldValidator ID="rfvMinAmount"
                                                runat="server" ErrorMessage="Minimum Amount can not be blank" Display="None"
                                                ControlToValidate="txtTransAmountFrom"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblTransAmountTo" runat="server" CssClass="label">Maximum Amount</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">
                                        <asp:TextBox ID="txtTransAmountTo" runat="server" CssClass="textbox">0.0</asp:TextBox><asp:CompareValidator
                                            ID="ToAmountCompareValidator" runat="server" ErrorMessage="Invalid Maximum Amount"
                                            Display="None" ControlToValidate="txtTransAmountTo" Type="Currency" ValueToCompare="0.00"
                                            Operator="GreaterThanEqual"></asp:CompareValidator><asp:RequiredFieldValidator ID="rfvMaxAmount"
                                                runat="server" ErrorMessage="Maximum Amount can not be blank" Display="None"
                                                ControlToValidate="txtTransAmountTo"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblEnrollmentId" CssClass="label" runat="server">Reference</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">
                                        <asp:TextBox ID="txtReference" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblDescription" runat="server" CssClass="label">Description</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">
                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblTerms" runat="server" CssClass="label">Terms</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">
                                        <asp:DropDownList ID="ddlTermsId" runat="server" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="transactionsearch">
                                        <asp:Label ID="lblAcademicYearId" runat="server" CssClass="label">Academic Year</asp:Label>
                                    </td>
                                    <td class="transactionsearch2">
                                        <asp:DropDownList ID="ddlAcademicYearId" runat="server" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td width="50%">
                                        <asp:CheckBox ID="cbxCharges" runat="server" CssClass="checkbox" Text="Charges" Checked="True"></asp:CheckBox>
                                    </td>
                                    <td width="50%">
                                        <asp:CheckBox ID="cbxDebitAdjustments" runat="server" CssClass="checkbox" Text="Debit Adjust."
                                            Checked="True"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <asp:CheckBox ID="cbxPayments" runat="server" CssClass="checkbox" Text="Payments"
                                            Checked="True"></asp:CheckBox>
                                    </td>
                                    <td width="50%">
                                        <asp:CheckBox ID="cbxCreditAdjustments" runat="server" CssClass="checkbox" Text="Credit Adjust."
                                            Checked="True"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" colspan="2" style="width: 100%">
                                        <asp:RadioButtonList ID="rbtnvoidlist" runat="server" CssClass="radiobuttonlist">
                                            <asp:ListItem Value="0" Selected="True">Do not show voids</asp:ListItem>
                                            <asp:ListItem Value="1">Show all including voids</asp:ListItem>
                                            <asp:ListItem Value="2">Show only voids</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->


                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div style="overflow-x: auto; width: 100%">
                                            <asp:DataGrid ID="dgrdTransactionSearch" runat="server" BorderWidth="1px" BorderColor="#E0E0E0"
                                                Width="100%"
                                                AutoGenerateColumns="False" EnableViewState="False">
                                                <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>

                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Student Name">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="HyperLink1" runat="server" EnableViewState="False" NavigateUrl='<%# "javascript:__doPostBack(""lbtStudentName"",""" + Ctype(Container.DataItem("StudentId"),Guid).ToString + """)" %>'>
																		<%# Container.DataItem("StudentName") %>
                                                            </asp:HyperLink>
                                                            <asp:CheckBox ID="cbxIsrefund" runat="server" CssClass="label" Text="IsRefund" Checked='<%# Container.DataItem("IsRefund") %>'
                                                                EnableViewState="False" Visible="False"></asp:CheckBox>
                                                            <asp:CheckBox ID="cbxIsPayment" runat="server" CssClass="label" Text="IsPayment"
                                                                Checked='<%# Container.DataItem("IsPayment") %>' EnableViewState="False" Visible="False"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="TransDate" SortExpression="TransDate" ReadOnly="True"
                                                        HeaderText="Date" DataFormatString="{0:d}">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TransReference" SortExpression="TransReference" ReadOnly="True"
                                                        HeaderText="Reference">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TransCodeDescrip" SortExpression="TransCodeDescrip" ReadOnly="True"
                                                        HeaderText="Transaction Code">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TransDescrip" SortExpression="TransDescrip" ReadOnly="True"
                                                        HeaderText="Description">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TransAmount" SortExpression="TransAmount" ReadOnly="True"
                                                        HeaderText="Amount" DataFormatString="{0:c}">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="AcademicYearDescrip" SortExpression="AcademicYearDescrip"
                                                        ReadOnly="True" HeaderText="Academic Year">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TermDescrip" SortExpression="TermDescrip" ReadOnly="True"
                                                        HeaderText="Term">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TransTypeDescrip" SortExpression="TransTypeId" ReadOnly="True"
                                                        HeaderText="Trans. Type">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ModUser" SortExpression="ModUser" ReadOnly="True" HeaderText="User">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Adjust Transaction">
                                                        <HeaderStyle CssClass="k-grid-header"></HeaderStyle>

                                                        <ItemTemplate>

                                                            <asp:Image ID="Image1" OnClick="Adjust(this.id);" EnableViewState="False" ImageUrl="../Images/transactionadjust.gif"
                                                                runat="server" Visible='<%# ShowLink(Ctype(Container.DataItem("Voided"), boolean), not(Ctype(Container.DataItem("IsPosted"), boolean) OR Ctype(Container.DataItem("IsPayment"), boolean))) %>' />
                                                            <asp:Image ID="Image2" OnClick="Reverse(this.id);" EnableViewState="False" ImageUrl="../Images/transactionadjust.gif"
                                                                runat="server" Visible='<%# not(Ctype(Container.DataItem("Voided"),boolean)  OR Ctype(Container.DataItem("IsPayment"), boolean)) %>'
                                                                ToolTip="Adjustments" />
                                                            <asp:Image ID="Image3" OnClick="Void(this.id);" EnableViewState="False" ImageUrl="../Images/transactionVoid.gif"
                                                                runat="server" Visible='<%# not(Ctype(Container.DataItem("Voided"),boolean)) %>'
                                                                ToolTip="Void" />
                                                            <asp:Image ID="Image4" OnClick="Print(this.id);"
                                                                EnableViewState="False" ImageUrl="../Images/TransactionPrint.gif" ToolTip="Print Receipt"
                                                                runat="server" Visible='<%# ShowLink(Ctype(Container.DataItem("Voided"), boolean),Ctype(Container.DataItem("IsPayment"), boolean)) %>'
                                                                AlternateText='<%# Integer.Parse(Ctype(Container.DataItem("TransactionId"),Guid).ToString().Substring(0, 6), System.Globalization.NumberStyles.HexNumber).ToString() %>' />
                                                            <asp:Label ID="lblTranstype" Visible="false" runat="server" EnableViewState="false" Text='<%# showTransType(Ctype(Container.DataItem("Voided"), boolean)) %>'></asp:Label>
                                                            <asp:HiddenField runat="server" ID="txtParameters" Value='<%# Ctype(Container.DataItem("TransactionId"),Guid).ToString + ";" + Ctype(Container.DataItem("IsPayment"),Boolean).ToString + ";" + Ctype(Container.DataItem("IsRefund"), Boolean).ToString %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

