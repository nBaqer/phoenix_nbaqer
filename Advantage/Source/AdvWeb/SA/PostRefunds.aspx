﻿<%@ Page Title="Post Refunds" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="PostRefunds.aspx.vb" Inherits="PostRefunds" Async="true" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Post Refunds</title>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function confirmCallBackFn(arg) {

            if (arg == true) {

                var oButton = document.getElementById('<%=Hdn.ClientID%>');
                //alert(oButton);
                oButton.click();

            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->

                        <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none">
                            <tr>
                                <td class="detailsframe">
                                    <asp:Panel runat="server" ID="trStuSearchControl">
                                        <FAME:StudentSearch ID="StudSearch1" runat="server" OnTransferToParent="TransferToParent" />
                                        <div>
                                            <asp:Label ID="lblStudent" runat="server" CssClass="label"></asp:Label>
                                            <asp:TextBox ID="txtStudentName" TabIndex="1" runat="server" CssClass="textboxsearch"
                                                contentEditable="false" Visible="false"></asp:TextBox>
                                        </div>
                                    </asp:Panel>

                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>

                                        <div style="width: 100%;">
                                            <asp:Panel ID="pnlDisplayInfo" runat="server" Visible="true">
                                                <table cellspacing="0" cellpadding="0"
                                                    align="center" style="margin-left: 2px; width: 100%; border: none;">
                                                    <tr>
                                                        <td class="contentcell" nowrap="nowrap">
                                                            <asp:Label ID="lblStudentDisp" CssClass="label" runat="server">Student</asp:Label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td class="contentcell">
                                                            <asp:TextBox ID="txtStudentDisp" CssClass="textbox" Enabled="false" Width="300px"
                                                                runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell" nowrap="nowrap">
                                                            <asp:Label ID="lblEnrollDisp" CssClass="label" runat="server">Enrollment</asp:Label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td class="contentcell">
                                                            <asp:TextBox ID="txtEnrollDisp" CssClass="textbox" Enabled="false" Width="300px"
                                                                runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell" nowrap="nowrap">
                                                            <asp:Label ID="lblTermDisp" CssClass="label" runat="server">Term</asp:Label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td class="contentcell">
                                                            <asp:TextBox ID="txtTermDisp" CssClass="textbox" Enabled="false" Width="300px" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell" nowrap="nowrap">
                                                            <asp:Label ID="lblAcadYearDisp" CssClass="label" runat="server">Academic Year</asp:Label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td class="contentcell">
                                                            <asp:TextBox ID="txtAcadYearDisp" CssClass="textbox" Enabled="false" Width="300px"
                                                                runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                        <br />
                                        <br />
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%;" border="0">
                                            <tr>
                                                <td nowrap="nowrap" class="contentcell" style='width: 150px;'>
                                                    <asp:Label ID="lblFundSourceId" CssClass="label" runat="server">Fund Source <font color="red">*</font></asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlFundSourceId" TabIndex="6" runat="server" CssClass="dropdownlist" AutoPostBack="true" Width="200px">
                                                    </asp:DropDownList>
                                                    <asp:CompareValidator ID="FundSourceCompareValidator" runat="server" Display="None"
                                                        ErrorMessage="Must Select a Fund Source" ControlToValidate="ddlFundSourceId"
                                                        Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Fund Source</asp:CompareValidator>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap="nowrap" class="contentcell" style="width: 150px;">
                                                    <asp:Label ID="lblTransDescrip" CssClass="label" runat="server">Description</asp:Label>


                                                </td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtTransDescrip" TabIndex="3" CssClass="textbox" MaxLength="50"
                                                        runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="DescriptionReqFieldValidator" runat="server" ControlToValidate="txtTransDescrip"
                                                        ErrorMessage="Description can not be blank" Display="None"> Description can not be blank</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap="nowrap" class="contentcell">

                                                    <asp:Label ID="lblTransReference" runat="server" CssClass="label">Reference</asp:Label>


                                                </td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtTransReference" TabIndex="5" runat="server" CssClass="textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap="nowrap" class="contentcell">

                                                    <asp:Label ID="lblTransDate" runat="server" CssClass="label">Date</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <telerik:RadDatePicker ID="txtTransDate" runat="server" Width="200px">
                                                    </telerik:RadDatePicker>
                                                    <asp:RequiredFieldValidator ID="DateReqFieldValidator" runat="server" ControlToValidate="txtTransDate"
                                                        ErrorMessage="Date can not be blank" Display="None">Date can not be blank</asp:RequiredFieldValidator>
                                                    <asp:RangeValidator ID="drvTransactionDate" runat="server" ControlToValidate="txtTransDate"
                                                        ErrorMessage="Date is Invalid or outside allowed range" Display="None" Type="Date"
                                                        MaximumValue="2049/12/31" MinimumValue="2001/01/01">Date is Invalid or outside allowed range</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap="nowrap" class="contentcell">

                                                    <asp:Label ID="lblTransCodeId" CssClass="label" runat="server">Transaction Code</asp:Label>


                                                </td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlTransCodeId" TabIndex="6" runat="server" CssClass="dropdownlist" Width="200px">
                                                    </asp:DropDownList>
                                                    <asp:CompareValidator ID="TransCodeCompareValidator" runat="server" Display="None"
                                                        ErrorMessage="Must Select a Transaction Code" ControlToValidate="ddlTransCodeId"
                                                        Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Transaction Code</asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap="nowrap" class="contentcell">

                                                    <asp:Label ID="lblAmount" runat="server" CssClass="label">Amount</asp:Label>

                                                </td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtTransAmount" TabIndex="8" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox>
                                                    <span class="label">&nbsp;<%=MyAdvAppSettings.AppSettings("Currency")%></span>
                                                    &nbsp;
                                                        <%--<asp:CheckBox ID="cbIsInstCharge" CssClass="checkbox" Text="Inst.Charge?" runat="server" />--%>
                                                    <asp:CheckBox ID="cbIsInstCharge" CssClass="checkbox" runat="server" />
                                                    <asp:Label ID="Label1" runat="server" CssClass="label">Inst.Charge?</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap="nowrap" class="contentcell">

                                                    <asp:Label ID="Label7" CssClass="label" runat="server" Width="105px">Refund to student?</asp:Label>


                                                </td>
                                                <td class="contentcell">
                                                    <asp:CheckBox ID="chkRefundToStudent" TabIndex="6" runat="server" CssClass="checkbox"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap="nowrap">

                                                    <asp:Label ID="lblTermId" Visible="false" runat="server" CssClass="label">Term</asp:Label>

                                                </td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtTerm" Visible="false" TabIndex="6" runat="server" CssClass="textboxreadonly"
                                                        Width="150px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="contentcell">
                                                    <table width="100%" border="0" style="margin-left: 0;">
                                                        <tr>
                                                            <td colspan="5">
                                                                <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" OnItemCommand="RadGrid1_ItemCommand"
                                                                    Width="100%" GridLines="None" MasterTableView-NoMasterRecordsText="No records" Visible="false">
                                                                    <ItemStyle Wrap="true" />
                                                                    <ExportSettings>
                                                                        <Pdf FontType="Subset" PaperSize="Letter" PageLeftMargin="10px" PageRightMargin="10px"
                                                                            PageTopMargin="10px" PageBottomMargin="10px" />
                                                                        <Excel Format="Html" />
                                                                        <Csv ColumnDelimiter="Colon" RowDelimiter="NewLine" />
                                                                    </ExportSettings>
                                                                    <MasterTableView AutoGenerateColumns="False" PageSize="20" EditMode="InPlace" AllowCustomSorting="true"
                                                                        ShowHeadersWhenNoRecords="true" EnableNoRecordsTemplate="true">
                                                                        <NoRecordsTemplate>
                                                                            <div>
                                                                                There are no records to display
                                                                            </div>
                                                                        </NoRecordsTemplate>
                                                                        <NestedViewTemplate>
                                                                            <telerik:RadGrid ID="SubGrid" runat="server" AutoGenerateColumns="false" GridLines="None">
                                                                                <MasterTableView>
                                                                                    <Columns>
                                                                                        <telerik:GridTemplateColumn UniqueName="AwardScheduleIdId" HeaderText="Scheduled Date"
                                                                                            ItemStyle-Width="150px">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="tbAwardScheduleId" runat="server" Visible="false" Text='<%# Container.DataItem("AwardScheduleId") %>'></asp:TextBox>
                                                                                                <asp:Label ID="lScheduleDate" runat="server" Visible="true" Text='<%# If(typeof Eval("ExpectedDate") is DBNull,string.Empty,Ctype(Container.DataItem("ExpectedDate"), Date).ToString("MM/dd/yyyy"))  %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn UniqueName="ScheduleAmount" HeaderText="Net Received Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                                                            ItemStyle-Width="95px">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lScheduleAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount", "${0:n}")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn UniqueName="RefundAmount" HeaderText="Refund Amount"
                                                                                            ItemStyle-Width="100px">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="tbDetRefundAmount" runat="server" CssClass="textboxbutton" AutoPostBack="true" OnTextChanged="tbDetRefundAmount_TextChanged"></asp:TextBox>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn UniqueName="ToStudent" HeaderText="Stipend" ItemStyle-Width="40px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>

                                                                                                <asp:CheckBox ID="cbRefundTo" CssClass="checkbox" runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                    </Columns>
                                                                                </MasterTableView>
                                                                            </telerik:RadGrid>
                                                                        </NestedViewTemplate>
                                                                        <ItemStyle Wrap="true" />
                                                                        <Columns>
                                                                            <telerik:GridTemplateColumn UniqueName="FundSourceId" HeaderText="Fund Source">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="tbStudentAwardId" runat="server" Visible="false" Text='<%# Container.DataItem("StudentAwardId") %>'></asp:TextBox>
                                                                                    <asp:TextBox ID="tbFundSourceId" runat="server" Visible="false" Text='<%# Container.DataItem("AwardTypeId") %>'></asp:TextBox>
                                                                                    <asp:Label ID="lFundSource" runat="server" CssClass="labeldocs" Text='<%# Container.DataItem("AwardTypeDescrip") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn UniqueName="Reference" HeaderText="Start Date">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lReference" runat="server" CssClass="labeldocs" Text='<%# Ctype(Container.DataItem("AwardStartDate"), Date).ToString("MM/dd/yyyy") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn UniqueName="Amount" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lAmount" runat="server" CssClass="labeldocs" Text='<%# DataBinder.Eval(Container, "DataItem.GrossAmount", "${0:n}")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                        </Columns>
                                                                    </MasterTableView>
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                        <asp:Repeater ID="rStudentAwards" runat="server">
                                                            <HeaderTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="label2" CssClass="labelbold" runat="server" Text="Fund Source"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label3" CssClass="labelbold" runat="server" Text="Reference"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label4" CssClass="labelbold" runat="server" Text="Amount"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label5" CssClass="labelbold" runat="server" Text="Refund Amount"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label6" CssClass="labelbold" runat="server" Text="To Student"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbStudentAwardId" runat="server" Visible="false" Text='<%# Container.DataItem("StudentAwardId") %>'></asp:TextBox>
                                                                        <asp:TextBox ID="tbFundSourceId" runat="server" Visible="false" Text='<%# Container.DataItem("AwardTypeId") %>'></asp:TextBox>
                                                                        <asp:Label ID="lFundSource" runat="server" CssClass="labeldocs" Text='<%# Container.DataItem("AwardTypeDescrip") %>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lReference" runat="server" CssClass="labeldocs" Text='<%# Ctype(Container.DataItem("AwardStartDate"), Date).ToShortDateString %>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lAmount" runat="server" CssClass="labeldocs" Text='<%# DataBinder.Eval(Container, "DataItem.GrossAmount","{0:n}") %>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbRefundAmount" runat="server" CssClass="textboxbutton"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="cbRefundTo" CssClass="checkbox" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlUDFHeader" Visible="False" runat="server">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="spacertables"></td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap">
                                                        <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="spacertables"></td>
                                                </tr>
                                            </table>
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="contentcell2">
                                                        <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                            <asp:TextBox ID="txtPostRefundId" runat="server" Visible="False" Wrap="False">txtPostRefundId</asp:TextBox>
                                            <asp:CheckBox ID="chkIsReversal" runat="server" Visible="False"></asp:CheckBox>
                                            <asp:TextBox ID="txtOriginalTransactionId" runat="server" Visible="False" Wrap="False">txtOriginalTransactionId</asp:TextBox>
                                            <asp:CheckBox ID="chkIsPosted" runat="server" Visible="False"></asp:CheckBox>
                                            <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                            <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                            <asp:TextBox ID="txtModUser1" runat="server" Visible="False">ModUser1</asp:TextBox>
                                            <asp:TextBox ID="txtModDate1" runat="server" Visible="False">ModDate1</asp:TextBox>
                                            <asp:TextBox ID="txtTermId" runat="server" Width="0px" Visible="False">TermId</asp:TextBox>
                                            <asp:TextBox ID="txtAcademicYearId" runat="server" CssClass="visibility">AcademicYearId</asp:TextBox>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:TextBox ID="txtAcademicYear" Visible="false" TabIndex="9" runat="server" CssClass="textboxreadonly"></asp:TextBox>
            <asp:TextBox ID="txtStuEnrollment" Visible="false" runat="server" CssClass="textboxreadonly"></asp:TextBox>
            <asp:TextBox ID="txtStuEnrollmentId" Visible="false" runat="server" CssClass="textboxreadonly"></asp:TextBox>
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadWindowManager runat="server" ID="RadWindowManager1"></telerik:RadWindowManager>
    <asp:Button ID="Hdn" runat="server" OnClick="Hdn_Click" />
</asp:Content>
