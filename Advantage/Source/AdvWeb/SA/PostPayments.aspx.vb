﻿Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Xml.Xsl
'Imports org.apache.fop
'Imports org.apache.fop.apps
'Imports org.apache.fop.tools
'Imports org.xml.sax
'Imports java.io
Imports System
Imports FAME.Advantage.Common
Imports FAME.Advantage.Reporting


Partial Class PostPayments
    Inherits BasePage

    Protected WithEvents lblProspect As System.Web.UI.WebControls.Label
    Protected WithEvents CompareValidator1 As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents RangeValidator1 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents lblFundSource As System.Web.UI.WebControls.Label
    Protected WithEvents DescriptionReqFieldValidator As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents DateReqFieldValidator As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents AmountReqFieldValidator As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents lblAwardType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlStudentAwards As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected ResourceId As Integer
    Protected TransactionId As String
    Protected WithEvents TransCodeCompareValidator As System.Web.UI.WebControls.CompareValidator

    Protected campusId, userId As String
    Private pObj As New UserPagePermissionInfo
    Protected ModuleId As String
    Protected userWantsToPrintAReceipt As Boolean
    Protected MyAdvAppSettings As AdvAppSettings

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Session("SearchType") = "PostPayments"

        Dim objCommon As New CommonUtilities
        Dim SDFControls As New SDFComponent

        Dim fac As New UserSecurityFacade


        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        'pObj = fac.GetUserResourcePermissions(userId, ResourceId, campusId)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)
        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        txtAcademicYearId.Attributes.Add("onClick", "needToConfirm = false;__doPostBack('lbtHasDisb','')")

        '   Disable Audit History at all time.
        'Header1.EnableHistoryButton(False)
        userWantsToPrintAReceipt = False

        If Not IsPostBack Then
            lblPaymentApplication.Visible = false 

            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "New")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            InitButtonsForLoad(pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()
            If MyAdvAppSettings.AppSettings("UseDropDownForPostingPayments").ToLower.ToString = "yes" Then
                ddlDescription.Visible = True
                txtTransDescrip.Visible = False
            Else
                ddlDescription.Visible = False
                txtTransDescrip.Visible = True
            End If

            ResourceId = Trim(Request.QueryString("resid"))

            
            dgrdApplyPayment.DataSource = ""
            dgrdApplyPayment.DataBind()

            '   initialize screen with transaction in session or an empty transaction
            If CommonWebUtilities.IsValidGuid(Session("TransactionId")) Then
                '   assign session value and destroy session
                ViewState("TransactionId") = Session("TransactionId")
                Session("TransactionId") = Nothing

                '   bind PostChargeInfo in session
                With New StudentsAccountsFacade
                    'BindPostPaymentData(.GetPostPaymentInfo("BC361A60-0F63-4F1E-B071-DB938B589252"))
                    BindPostPaymentData(.GetPostPaymentInfo(ViewState("TransactionId")))
                End With

                '   bind Disbursements datagrid
                BindDataGrid()

                '   if it is a reversal, create a new transaction
                If CType(Session("CommandName"), String) = "Reverse" Then

                    '   this is a reverse transaction
                    SetControls(False)

                    '   chkIsInDB
                    chkIsInDB.Checked = False

                    '   create a new guid
                    txtPostPaymentId.Text = Guid.NewGuid.ToString

                    '   reverse the amount (don't do anything because it is already reversed)
                    'txtTransAmount.Text = CType(Decimal.Parse(txtTransAmount.Text) * (-1), Decimal).ToString("#,###,###.00")

                    '   transaction type will be an adjustment
                    txtTransTypeId.Text = 1

                    '   change original description
                    If txtTransDescrip.Visible = True Then
                        txtTransDescrip.Text += " - Adjustment For " + txtTransDate.SelectedDate.GetValueOrDefault()
                    End If

                    '   set todays date
                    txtTransDate.SelectedDate = Date.Now.ToShortDateString

                    '   hide the header 
                    'Header1.Visible = False

                    '   hide the footer
                    'footer1.Visible = False

                    '   hide the Header Div
                    'Header.Visible = False

                    '   hide the Footer Div
                    'footer.Visible = False

                    '   in popUp windows the height of the scroll window should be bigger
                    'DIVRHS.Attributes.Add("style", " top:15px; height: expression(document.body.clientHeight + 'px');")

                    '   change the class to make up the space of the search button
                    txtStudentName.CssClass = "TextBox"

                    '   Is Reversal
                    chkIsReversal.Checked = True

                    '   Original TransactionId
                    txtOriginalTransactionId.Text = ViewState("TransactionId")
                End If

                '   set Student Name textbox
                txtStudentName.Text = CType(Session("StudentName"), String)

                '   initialize buttons for edit
                InitButtonsForEdit()

            Else

                '   bind an empty new PostPaymentInfo
                BindPostPaymentData(New PostPaymentInfo)

                ''   bind Disbursements datagrid
                'BindDataGrid()

                '   initialize buttons for load
                'InitButtonsForLoad()
            End If

            '   disable Use Scheduled Payment checkbox until Scheduled Payment is checked
            chkUseSchedPmt.Enabled = False

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")

            If CType(Session("CommandName"), String) = "Reverse" And _
                    Not (ViewState("TransactionId") Is Nothing) Then

                SelectAllDisbursements()
            End If
            InitButtonsForEdit()
        End If

        ''Added by Saraswathi Lakshmanan
        ''to find the studentLedger Balance

        If Not txtStuEnrollmentId.Text = Guid.Empty.ToString Then
            StudentLedgerBalance(txtStuEnrollmentId.Text)
        End If
        ''Added by Saraswathi Lakshmanan on Sept 22 2010
        ''For Document tracking
        ''To show if the student has not met any requitements for Financial Aid disbursements
        'If Not txtStuEnrollmentId.Text = Guid.Empty.ToString Then
        '    Dim fullname As String = CType(Me.FindControl("StudentSearch").FindControl("DDLStudentSearch"), RadComboBox).Text
        '    StudentFinancialAidRequirement(fullname)
        'End If


        If chkUseSchedPmt.Checked Then
            txtTransAmount.ReadOnly = True
        Else
            txtTransAmount.ReadOnly = False
        End If

        'If Trim(txtPostPaymentId.Text) <> "" Then
        '    SDFControls.GenerateControlsEdit(pnlSDF, ResourceId, txtPostPaymentId.Text, ModuleId)
        'Else
        '    SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
        'End If

        AddHandler StudentSearch.StudentIndexChange, AddressOf StudentSearch_SelectedIndexChanged

        headerTitle.Text = Header.Title

    End Sub


     Protected Sub StudentSearch_SelectedIndexChanged(ByVal o As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles StudentSearch.StudentIndexChange
        ddlFundSourceId.Items.Clear()
        ddlFundSourceId.Enabled = False

        'ddlTransCodeId.Items.Clear()
        'ddlTransCodeId.Enabled = False

        BuildDropDownLists()

        txtTransAmount.Text = ".00"
        txtTransReference.Text = ""
    End Sub
    Private Sub BuildDropDownLists()
        BuildPaymentTypesDDL()
        'BuildFundSourcesDDL()
        BuildDisbTypesDDL()
        'BuildTransCodesDDL()

        'This is the list of ddls
        ''  Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'TransCodes DDL()
        ''ddlList.Add(New AdvantageDDLDefinition(ddlTransCodeId, AdvantageDropDownListName.Trans_Codes, campusId, True, True))

        '' CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
        BuildPaymentDescriptionsDDL()
        BuildPaymentCode()
        BuildPaymentDescription_new()

    End Sub
    ''Added by Saraswathi Lakshmanan on April 8 2010
    ''Build payment Code and Charge Codes
    Private Sub BuildPaymentCode()

        With ddlPaymentCode
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeID"
            .DataSource = (New StudentsAccountsFacade).GetAllPaymentDescriptions_new("True", New Guid(campusId))
            ViewState("PaymentCodes") = ddlPaymentCode.DataSource
            .DataBind()
            '.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .Items.Insert(0, New RadComboBoxItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    ''Added by Saraswathi Lakshmanan on April 8 2010
    ''Build payment Code and Charge Codes
    Private Sub BuildPaymentDescription_new()

        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeID"
            .DataSource = (New StudentsAccountsFacade).GetAllChargeDescriptions("True", New Guid(campusId))
            .DataBind()
            '.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))

            .Items.Insert(0, New RadComboBoxItem("Select", Guid.Empty.ToString))

            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildStudentAwardsDDL(ByVal stuEnrollId As String)
        '   the first time get the data from the DB
        If ViewState("ddlStudentAwards") Is Nothing Or Not ViewState("PreviousStuEnrollmentId") = stuEnrollId Then
            '   bind the TransCodes DDL
            ViewState("ddlStudentAwards") = (New StudentAwardFacade).GetAllAwardsPerStudent(stuEnrollId)
            ViewState("PreviousStuEnrollmentId") = stuEnrollId
            'Else
            '    If Not ViewState("PreviousStuEnrollmentId") = stuEnrollId Then
            '        '   bind the TransCodes DDL
            '        ViewState("ddlStudentAwards") = (New StudentAwardFacade).GetAllAwardsPerStudent(stuEnrollId)
            '        ViewState("PreviousStuEnrollmentId") = stuEnrollId
            '    End If
        End If

        '   build Student Awards ddl
        With ddlStudentAwards
            .DataTextField = "AwardDescrip"
            .DataValueField = "StudentAwardId"
            .DataSource = ViewState("ddlStudentAwards")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildPaymentTypesDDL()

        '   build ddl paymentTypes
        Dim names() As String = System.Enum.GetNames(GetType(PaymentType))
        Dim values() As Integer = System.Enum.GetValues(GetType(PaymentType))

        'clear ddl items
        ddlPaymentTypeId.Items.Clear()

        Dim strTemp As String

        For i As Integer = 0 To names.Length - 1
            If names(i).IndexOf("_") <> -1 Then
                strTemp = names(i).Replace("_", " ")
            Else
                strTemp = names(i)
            End If
            'ddlPaymentTypeId.Items.Add(New ListItem(strTemp, values(i)))

            ddlPaymentTypeId.Items.Add(New RadComboBoxItem(strTemp, values(i)))

        Next

        ''   set Cash to be the default PaymentType
        'ddlPaymentTypeId.Items.Insert(0, New ListItem("Select", 0))
        'ddlPaymentTypeId.SelectedIndex = 0

    End Sub
    Private Sub BuildFundSourcesDDL(ByVal stuEnrollId As String)
        '   bind the FundSource DDL
        'Dim stuAccounts As New StudentsAccountsFacade
        'With ddlFundSourceId
        '    .DataTextField = "FundSourceDescrip"
        '    .DataValueField = "FundSourceId"
        '    .DataSource = stuAccounts.GetAllFundSources("True")    'True' retrieves only Active Award Types
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With
        With ddlFundSourceId
            .DataTextField = "FundSourceDescrip"
            .DataValueField = "FundSourceId"

            'New Code Added By Vijay Ramteke On June 11, 2010 For Mantis ID 18950
            '.DataSource = (New StudentsAccountsFacade).GetAvailableAwardsForStudent(stuEnrollId, txtTransDate.SelectedDate)
            If Not ViewState("PaymentCodes") Is Nothing Then
                Dim dsPaymentCode As New DataSet
                dsPaymentCode = ViewState("PaymentCodes")
                Dim dr As DataRow()
                dr = dsPaymentCode.Tables(0).Select("TransCodeID = '" + ddlPaymentCode.SelectedItem.Value + "'")
                If dr.Length > 0 Then
                    For Each row As DataRow In dr
                        If row("SysTransCodeId") = 13 Then
                            .DataSource = (New StudentsAccountsFacade).GetAvailableAwardsForStudent_NoAid(stuEnrollId, txtTransDate.SelectedDate)
                        Else
                            .DataSource = (New StudentsAccountsFacade).GetAvailableAwardsForStudent(stuEnrollId, txtTransDate.SelectedDate)
                        End If
                    Next
                End If
            End If
            'New Code Added By Vijay Ramteke On June 11, 2010 For Mantis ID 18950

            .DataBind()
            '.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))

            .Items.Insert(0, New RadComboBoxItem("Select", Guid.Empty.ToString))

            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildPaymentDescriptionsDDL()
        '   bind the FundSource DDL
        'Dim stuAccounts As New StudentsAccountsFacae
        'With ddlFundSourceId
        '    .DataTextField = "FundSourceDescrip"
        '    .DataValueField = "FundSourceId"
        '    .DataSource = stuAccounts.GetAllFundSources("True")    'True' retrieves only Active Award Types
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With
        With ddlDescription
            .DataTextField = "PmtDescription"
            .DataValueField = "PmtDescriptionId"
            .DataSource = (New StudentsAccountsFacade).GetAllPaymentDescriptions("True", New Guid(campusId))
            .DataBind()
            '.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))

            .Items.Insert(0, New RadComboBoxItem("Select", Guid.Empty.ToString))

            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDisbTypesDDL()

        '   build ddl Disbursement Types
        Dim names() As String = System.Enum.GetNames(GetType(DisbursementType))
        Dim values() As Integer = System.Enum.GetValues(GetType(DisbursementType))

        Dim strTemp As String

        For i As Integer = 0 To names.Length - 1
            If names(i).IndexOf("_") <> -1 Then
                strTemp = names(i).Replace("_", " ")
            Else
                strTemp = names(i)
            End If
            ddlDisbTypeId.Items.Add(New ListItem(strTemp, values(i)))
        Next
    End Sub
    Private Sub BuildTransCodesDDL()
        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeId"
            .DataSource = (New StudentsAccountsFacade).GetAllTransCodes(False)
            .DataBind()
            '.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))

            .Items.Insert(0, New RadComboBoxItem("Select", Guid.Empty.ToString))

            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindPostPaymentData(ByVal PostPaymentInfo As PostPaymentInfo)
        Session("StuEnrollmentId") = ""
        Session("StuEnrollment") = ""
        Session("AcademicYear") = ""
        Session("AcademicYearId") = ""
        Session("TermId") = ""
        Session("Term") = ""
        With PostPaymentInfo
            chkIsInDB.Checked = .IsInDB
            chkIsPosted.Checked = .IsPosted
            'chkIsPosted.Checked = True
            txtPostPaymentId.Text = .PostPaymentId
            txtStuEnrollmentId.Text = .StuEnrollId
            txtStuEnrollment.Text = .EnrollmentDescription
            If txtTransDescrip.Visible = True Then
                txtTransDescrip.Text = .PostPaymentDescription
            Else
                ddlDescription.SelectedIndex = 0
            End If
            txtStudentName.Text = ""
            '   get StudentAwards for the new Student
            'BuildStudentAwardsDDL(txtStuEnrollmentId.Text)
            'ddlStudentAwards.SelectedValue = .StudentAwardId

            '   set blank if the date is null 
            If Not (.PostPaymentDate = Date.MinValue) Then
                txtTransDate.SelectedDate = .PostPaymentDate.ToShortDateString()
            Else
                txtTransDate.SelectedDate = ""
            End If

            txtTransAmount.Text = CType(.Amount * (-1), Decimal).ToString("#,###,###.00")
            txtTransReference.Text = .Reference
            txtAcademicYearId.Text = .AcademicYearId
            txtAcademicYear.Text = .AcademicYearDescrip
            txtTermId.Text = .TermId
            txtTerm.Text = .TermDescrip
            ddlTransCodeId.SelectedValue = .TransCodeId
            ''Added by Saraswathi lakshmanan on April 17 2010
            ''For mantis case 15222
            ddlPaymentCode.SelectedValue = .PaymentCodeID

            txtTransTypeId.Text = .TransTypeId.ToString
            drvTransactionDate.MinimumValue = .EarliestAllowedDate.Date.Month.ToString("0#") + "/" + .EarliestAllowedDate.Date.Day.ToString("0#") + "/" + .EarliestAllowedDate.Date.Year.ToString("000#")

            'maximum allowed date is defined by the value stored in web.config file
            .LatestAllowedDate = Date.Now.AddDays(CType(MyAdvAppSettings.AppSettings("AllowRegisterPaymentsInTheFutureWithUpToThisNumberOfDaysInTheFuture"), Double))
            If .EarliestAllowedDate > .LatestAllowedDate Then
                Dim d As Date = .EarliestAllowedDate
                .EarliestAllowedDate = .LatestAllowedDate
                .LatestAllowedDate = d
            End If
            drvTransactionDate.MaximumValue = .LatestAllowedDate.Date.Month.ToString("0#") + "/" + .LatestAllowedDate.Date.Day.ToString("0#") + "/" + .LatestAllowedDate.Date.Year.ToString("000#")

            drvTransactionDate.ErrorMessage = String.Format("Date must be between {0} and {1}", .EarliestAllowedDate.ToShortDateString, .LatestAllowedDate.ToShortDateString)

            txtReferenceNumber.Text = .CheckNumber
            ddlPaymentTypeId.SelectedValue = .PaymentTypeId
            chkScheduledPayment.Checked = .ScheduledPayment
            chkUseSchedPmt.Checked = .UseScheduledPayment
            'ddlStudentAwards.Enabled = .ScheduledPayment
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            txtModUser1.Text = .ModUser1
            txtModDate1.Text = .ModDate1.ToString
            chkUseSchedPmt.Enabled = False
            ddlFundSourceId.Items.Clear()
            ddlFundSourceId.Enabled = False
            ddlDisbTypeId.Enabled = False
            ddlDisbTypeId.SelectedValue = DisbursementType.Select
            ''Added by Saraswathi Lakshmanan to SHow the ledger balance in the page

            lblstuLedgerBalance.Visible = False

        End With
    End Sub
    Private Sub BindSavePostPaymentData(ByVal PostPaymentInfo As PostPaymentInfo)
        With PostPaymentInfo
            chkIsInDB.Checked = .IsInDB
            chkIsPosted.Checked = .IsPosted
            'chkIsPosted.Checked = True
            txtPostPaymentId.Text = .PostPaymentId
            'txtStuEnrollmentId.Text = .StuEnrollId
            'txtStuEnrollment.Text = .EnrollmentDescription
            If txtTransDescrip.Visible = True Then
                txtTransDescrip.Text = .PostPaymentDescription
            Else
                ddlDescription.SelectedIndex = 0
            End If

            '   get StudentAwards for the new Student
            'BuildStudentAwardsDDL(txtStuEnrollmentId.Text)
            'ddlStudentAwards.SelectedValue = .StudentAwardId

            '   set blank if the date is null 
            If Not (.PostPaymentDate = Date.MinValue) Then
                txtTransDate.SelectedDate = .PostPaymentDate.ToShortDateString()
            Else
                txtTransDate.SelectedDate = ""
            End If

            txtTransAmount.Text = CType(.Amount * (-1), Decimal).ToString("#,###,###.00")
            txtTransReference.Text = .Reference
            'txtAcademicYearId.Text = .AcademicYearId
            'txtAcademicYear.Text = .AcademicYearDescrip
            'txtTermId.Text = .TermId
            'txtTerm.Text = .TermDescrip
            ddlTransCodeId.SelectedValue = .TransCodeId
            ''Added by Saraswathi lakshmanan on April 16 2010
            ''For 15222
            ddlPaymentCode.SelectedValue = .PaymentCodeID

            txtTransTypeId.Text = .TransTypeId.ToString
            drvTransactionDate.MinimumValue = .EarliestAllowedDate.Date.Month.ToString("0#") + "/" + .EarliestAllowedDate.Date.Day.ToString("0#") + "/" + .EarliestAllowedDate.Date.Year.ToString("000#")

            'maximum allowed date is defined by the value stored in web.config file
            .LatestAllowedDate = Date.Now.AddDays(CType(MyAdvAppSettings.AppSettings("AllowRegisterPaymentsInTheFutureWithUpToThisNumberOfDaysInTheFuture"), Double))
            If .EarliestAllowedDate > .LatestAllowedDate Then
                Dim d As Date = .EarliestAllowedDate
                .EarliestAllowedDate = .LatestAllowedDate
                .LatestAllowedDate = d
            End If
            drvTransactionDate.MaximumValue = .LatestAllowedDate.Date.Month.ToString("0#") + "/" + .LatestAllowedDate.Date.Day.ToString("0#") + "/" + .LatestAllowedDate.Date.Year.ToString("000#")

            drvTransactionDate.ErrorMessage = String.Format("Date must be between {0} and {1}", .EarliestAllowedDate.ToShortDateString, .LatestAllowedDate.ToShortDateString)

            txtReferenceNumber.Text = .CheckNumber
            ddlPaymentTypeId.SelectedValue = .PaymentTypeId
            chkScheduledPayment.Checked = .ScheduledPayment
            chkUseSchedPmt.Checked = .UseScheduledPayment
            'ddlStudentAwards.Enabled = .ScheduledPayment
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            txtModUser1.Text = .ModUser1
            txtModDate1.Text = .ModDate1.ToString
            chkUseSchedPmt.Enabled = False
            ''''' Code changes by Kamalesh Ahuja on 25 August to resolve mantis issues id 19438
            ''ddlFundSourceId.Items.Clear()
            '' ddlFundSourceId.Enabled = False
            '''''''''''
            ddlDisbTypeId.Enabled = False
            ddlDisbTypeId.SelectedValue = DisbursementType.Select

            txtStuEnrollmentId.Text = Session("StuEnrollmentId")
            txtStuEnrollment.Text = Session("StuEnrollment")
            txtAcademicYear.Text = Session("AcademicYear")
            txtAcademicYearId.Text = Session("AcademicYearId")
            txtTermId.Text = Session("TermId")
            txtTerm.Text = Session("Term")
            BuildApplyPaymentGrid()
            ''Added by Saraswathi lakshmanan
            ''To Show the ledger balance in the page
            StudentLedgerBalance(txtStuEnrollmentId.Text)

        End With
    End Sub
    Private Sub BindDataGrid()
        Dim ds As New DataSet

        '   instantiate component
        Dim saf As New StudentsAccountsFacade
        If ViewState("TransactionId") Is Nothing Then

            Try
                If ViewState("SelectedPaymentCode") IsNot Nothing AndAlso ViewState("SelectedPaymentCode").ToString IsNot Nothing Then
                    ddlPaymentCode.SelectedValue = ViewState("SelectedPaymentCode").ToString
                End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)
            End Try

            '' New code Added by kamalesh Ahuja on 06 April 2010 to resolve Mantis Issue id 14633

            '''' Code commented by kamalesh ahuja on 14 th Sept 2010 to reslove mantis issue id 19725
            ''If ddlDisbTypeId.SelectedValue = 0 And Not ViewState("DisbType") Is Nothing Then
            '''''''''

            ''BuildFundSourcesDDL(txtStuEnrollmentId.Text)
            '''' Code commented by kamalesh ahuja on 14 th Sept 2010 to reslove mantis issue id 19725
            'BuildFundSourcesDDLNew()
            '''''''

            'Try

            '    ddlDisbTypeId.SelectedValue = ViewState("DisbType").ToString
            '    ' ddlDisbTypeId.Enabled = True

            'Catch ex As Exception
            '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    ddlDisbTypeId.SelectedIndex = -1
            '    '  ddlDisbTypeId.Enabled = False
            'End Try

            Try
                If ViewState("FundSource") IsNot Nothing AndAlso ViewState("FundSource").ToString IsNot Nothing Then
                    ddlPaymentCode.SelectedValue = ViewState("FundSource").ToString
                    ddlFundSourceId.Enabled = True
                End If
            Catch ex As Exception
                Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                '''' Code commented by kamalesh ahuja on 14 th Sept 2010 to reslove mantis issue id 19725
                '' ddlFundSourceId.SelectedIndex = -1
                ''''''''

                '' Code Commented by Kamalesh Ahuja on August 25 2010 to resolve mantis issue id 19438
                ''ddlFundSourceId.Enabled = False
                '''''''
            End Try

            ViewState("SelectedPaymentCode") = Nothing
            ViewState("DisbType") = Nothing
            ViewState("FundSource") = Nothing

            '''' Code commented by kamalesh ahuja on 14 th Sept 2010 to reslove mantis issue id 19725
            ''End If
            ''''''''''

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''' Code commented by kamalesh ahuja on 14 th Sept 2010 to reslove mantis issue id 19725
            'Select Case ddlDisbTypeId.SelectedValue
            '    Case DisbursementType.Payment_Plan
            '        ds = saf.GetPmtPlanScheduledDisb(txtStuEnrollmentId.Text)
            '    Case DisbursementType.Financial_Aid
            ''''''''''

            'New Code Added By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
            'ds = saf.GetAwardScheduledDisb(txtStuEnrollmentId.Text, ddlFundSourceId.SelectedValue, txtTransDate.SelectedDate)

            If Not ViewState("PaymentCodes") Is Nothing Then
                Dim dsPaymentCode As New DataSet
                dsPaymentCode = ViewState("PaymentCodes")
                Dim dr As DataRow()
                dr = dsPaymentCode.Tables(0).Select("TransCodeID = '" + ddlPaymentCode.SelectedItem.Value + "'")
                If dr.Length > 0 Then
                    For Each row As DataRow In dr
                        '''' Code modified by kamalesh ahuja on 14 th Sept 2010 to reslove mantis issue id 19725
                        If row("SysTransCodeId") = 13 Then
                            ds = saf.GetAwardScheduledDisb_NoAid(txtStuEnrollmentId.Text, ddlFundSourceId.SelectedValue, txtTransDate.SelectedDate)
                        ElseIf row("SysTransCodeId") = 11 Then
                            ds = saf.GetAwardScheduledDisb(txtStuEnrollmentId.Text, ddlFundSourceId.SelectedValue, txtTransDate.SelectedDate)
                        ElseIf row("SysTransCodeId") = 12 Then
                            ds = saf.GetPmtPlanScheduledDisb(txtStuEnrollmentId.Text)
                        End If
                        '''''''''
                    Next
                End If
            End If
            'New Code Added By Vijay Ramteke On June 09, 2010 For Mantis Id 18950

            '''' Code commented by kamalesh ahuja on 14 th Sept 2010 to reslove mantis issue id 19725
            ''End Select
            ''''''''

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    dgrdDisbursements.DataSource = ds
                    '' New code Added by kamalesh Ahuja on 06 April 2010 to resolve Mantis Issue id 14633
                    chkScheduledPayment.Checked = True
                    chkUseSchedPmt.Checked = True
                    chkUseSchedPmt.Enabled = True
                    lblPaymentApplication.Visible = True 
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Else
                    '   show nothing, not even the column headers.
                    dgrdDisbursements.DataSource = Nothing

                    '' New code Added by kamalesh Ahuja on 06 April 2010 to resolve Mantis Issue id 14633
                    chkScheduledPayment.Checked = False
                    chkUseSchedPmt.Checked = False
                    chkUseSchedPmt.Enabled = False
                    ddlDisbTypeId.SelectedIndex = 0
                    ddlDisbTypeId.Enabled = False
                    lblPaymentApplication.Visible = false 
                    ''''''

                    '' Code Commented by Kamalesh Ahuja on August 25 2010 to resolve mantis issue id 19438
                    'ddlFundSourceId.Items.Clear()
                    'ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
                    'ddlFundSourceId.Enabled = False
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End If
                dgrdDisbursements.DataBind()
            Else
                'New Code Added By Vijay Ramteke On June 09, 2010
                dgrdDisbursements.DataSource = Nothing
                lblPaymentApplication.Visible = false 

                dgrdDisbursements.DataBind()
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If

        Else
            '   reversing payment
            '
            '   get disbursements/payments
            ds = saf.GetPaymentDisbursements(ViewState("TransactionId"))

            If ds.Tables(0).Rows.Count > 0 Then
                '   Use Scheduled Payment checkbox
                If ds.Tables(0).Compute("SUM(Amount)", "") = -Convert.ToDecimal(txtTransAmount.Text) And _
                        chkScheduledPayment.Checked Then
                    chkUseSchedPmt.Checked = True
                End If

                '   select corresponding value in DisbType DDL
                If ds.Tables(0).Rows(0)("DisbursementType") = DisbursementType.Financial_Aid Then
                    ddlDisbTypeId.SelectedValue = DisbursementType.Financial_Aid
                    '   populate FundSource DDL with values from first row
                    'ddlFundSourceId.Items.Add(New ListItem(ds.Tables(0).Rows(0)("ScheduleDescrip"), ds.Tables(0).Rows(0)("FundSourceId").ToString))

                    ddlFundSourceId.Items.Add(New RadComboBoxItem(ds.Tables(0).Rows(0)("ScheduleDescrip"), ds.Tables(0).Rows(0)("FundSourceId").ToString))

                    ddlFundSourceId.SelectedIndex = 0
                    '
                ElseIf ds.Tables(0).Rows(0)("DisbursementType") = DisbursementType.Payment_Plan Then
                    ddlDisbTypeId.SelectedValue = DisbursementType.Payment_Plan

                Else
                    ddlDisbTypeId.SelectedValue = DisbursementType.Select
                End If
            End If
        End If

        'If ds.Tables.Count > 0 Then
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        dgrdDisbursements.DataSource = ds
        '    Else
        '        '   show nothing, not even the column headers.
        '        dgrdDisbursements.DataSource = Nothing
        '    End If
        '    dgrdDisbursements.DataBind()
        'End If

        '   show/hide Disbursements datagrid
        If Not (txtStuEnrollmentId.Text = Guid.Empty.ToString) Then
            dgrdDisbursements.Visible = True
        Else
            dgrdDisbursements.Visible = False
        End If


    End Sub
    '' Code Added by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
    Private Sub BuildFundSourcesDDLNew()

        With ddlFundSourceId
            .DataTextField = "FundSourceDescrip"
            .DataValueField = "FundSourceId"

            'New Code Added By Vijay Ramteke On June 11, 2010 For Mantis ID 18950
            '.DataSource = (New StudentsAccountsFacade).GetAvailableAwardsForStudent(stuEnrollId, txtTransDate.SelectedDate)
            If Not ViewState("PaymentCodes") Is Nothing Then
                Dim dsPaymentCode As New DataSet
                dsPaymentCode = ViewState("PaymentCodes")
                Dim dr As DataRow()
                dr = dsPaymentCode.Tables(0).Select("TransCodeID = '" + ddlPaymentCode.SelectedItem.Value + "'")
                If dr.Length > 0 Then
                    For Each row As DataRow In dr
                        If row("SysTransCodeId") = 13 Then
                            .DataSource = (New StudentsAccountsFacade).GetAvailableAwardsForStudent_NoAidNew(txtTransDate.SelectedDate, New Guid(campusId))
                        ElseIf row("SysTransCodeId") = 11 Then
                            .DataSource = (New StudentsAccountsFacade).GetAvailableAwardsForStudentNew(txtTransDate.SelectedDate, New Guid(campusId))
                        ElseIf row("SysTransCodeId") = 12 Then
                            .DataSource = (New StudentsAccountsFacade).GetAvailableAwardsForStudentPP(txtTransDate.SelectedDate, New Guid(campusId))
                        End If
                    Next
                End If
            End If
            'New Code Added By Vijay Ramteke On June 11, 2010 For Mantis ID 18950

            .DataBind()
            '.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))

            .Items.Insert(0, New RadComboBoxItem("Select", Guid.Empty.ToString))

            .SelectedIndex = 0
        End With
    End Sub
    '''
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   validate user entries, display proper error message

        Dim result As String
        If txtStudentName.Text = "" Then
            DisplayErrorMessage("Please select a student")
            Exit Sub
        End If
        If ddlDescription.Visible = True Then
            If ddlDescription.SelectedValue = Guid.Empty.ToString() Then
                result = "Please select a description."
                DisplayErrorMessage(result)
                Exit Sub
            End If
        End If
        If ddlPaymentCode.SelectedValue = Guid.Empty.ToString() Then
            result = "Please select a payment Code."
            DisplayErrorMessage(result)
            Exit Sub
        End If
        If ddlFundSourceId.SelectedValue = Guid.Empty.ToString() Then
            result = "Please select a fund source."
            DisplayErrorMessage(result)
            Exit Sub
        End If
        result = ValidateUserEntries()
        If Not result = "" Then
            DisplayErrorMessage(result)
            Exit Sub
        End If
        Dim IsStudentEligible As String

        IsStudentEligible = ViewState("IsStudentEligible")
        If IsStudentEligible = False Then
            DisplayErrorMessage("The student is not eligible to post Financial Aid Disbursements")
            Exit Sub
        End If

        '       if user checked Scheduled Payments, do not allow to save without selecting disbursement.
        Dim DisbArrList As ArrayList = BuildDisbCollection()
        'If chkScheduledPayment.Checked And chkScheduledPayment.Enabled And DisbArrList.Count = 0 Then
        '    result = "Please select a scheduled disbursement/payment."
        '    DisplayErrorMessage(result)
        '    Exit Sub
        'End If
        If chkScheduledPayment.Checked And DisbArrList.Count = 0 Then
            result = "Please select a scheduled disbursement/payment."
            DisplayErrorMessage(result)
            Exit Sub
        End If
        '   total amount of applied disbursements must be equal to transaction amount
        'If chkScheduledPayment.Checked And chkScheduledPayment.Enabled And (Decimal.Parse(txtTransAmount.Text) <> GetAppliedDisbursementsTotal(DisbArrList)) Then
        '    DisplayErrorMessage("Transaction Amount: " + Decimal.Parse(txtTransAmount.Text).ToString("c") + " must be equal to Total Disbursements Applied: " + GetAppliedDisbursementsTotal(DisbArrList).ToString("c"))
        '    Exit Sub
        'End If

        If chkScheduledPayment.Checked And (Decimal.Parse(txtTransAmount.Text) <> GetAppliedDisbursementsTotal(DisbArrList)) Then
            DisplayErrorMessage("Transaction Amount: " + Decimal.Parse(txtTransAmount.Text).ToString("c") + " must be equal to Total Disbursements Applied: " + GetAppliedDisbursementsTotal(DisbArrList).ToString("c"))
            Exit Sub
        End If

        '' Code Added by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports



        If Decimal.Parse(txtTransAmount.Text).ToString("c") <= 0 Then
            result = "Amount must be greater than zero."
            DisplayErrorMessage(result)
            Exit Sub
        End If

        '' code Added by kamalesh Ahuja on 06 April 2010 to resolve Mantis Issue id 14633
        ViewState("FundSource") = ddlFundSourceId.SelectedValue
        ViewState("DisbType") = ddlDisbTypeId.SelectedValue
        ViewState("SelectedPaymentCode") = ddlPaymentCode.SelectedValue
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '   instantiate component
        Dim saf As New StudentsAccountsFacade
        Dim arrApplyPmt As New ArrayList



        '   update PostPayment Info 
        Dim postPaymentInfo As PostPaymentInfo = BuildPostPaymentInfo(txtPostPaymentId.Text)
        Dim boolDup As Boolean = saf.CheckTransDuplicatePayment(postPaymentInfo)
        If boolDup Then

            'ViewState("DisbArrList") = DisbArrList
            'ViewState("arrApplyPmt") = arrApplyPmt
            RadWindowManager1.RadConfirm("Save Record?", "confirmCallBackFn", 300, 100, Nothing, "Duplicate Record Already Exists")
        Else


            arrApplyPmt = BuildAppliedPaymentCollection()
            DisbArrList = BuildDisbCollection()
            result = saf.UpdatePostPaymentInfo(postPaymentInfo, Session("UserName"), DisbArrList, arrApplyPmt)
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                If userWantsToPrintAReceipt Then

                    ' DE7793 - 7/18/2012 Janet Robinson switch receipt to ssrs report
                    PrintStudentReceipt(postPaymentInfo.PostPaymentId, postPaymentInfo.UniqueId)

                    ''Added by Saraswathi lakshmanan to print the address of the student
                    'get StudentAddress for this receipt
                    ''Added on Sept 18 2009
                    ''To fix mantis issue 17600: QA: There is a difference in the payment receipt on the post payments page and transaction search page.
                    'Dim studentAddressInfo As StuAddressInfo = (New StdAddressFacade).GetDefaultStudentAddress(postPaymentInfo.PostPaymentId)

                    'Dim recAddress As ReceiptAddress = GetAddressToBePrintedInReceipts(SingletonAppSettings.AppSettings("AddressToBePrintedInReceipts"))
                    'create receipt xmlDoc
                    'Dim receiptXmlDoc As XmlDocument = CreateReceiptXmlDoc(postPaymentInfo, recAddress, txtStudentName.Text, studentAddressInfo)
                    'print the receipt
                    'PrintAReceipt(receiptXmlDoc)


                End If

                '   If there are no errors bind an empty entity and init buttons
                If Not CommonWebUtilities.IsValidGuid(ViewState("TransactionId")) Then
                    '   bind an empty new PostPaymentInfo
                    BindSavePostPaymentData(New PostPaymentInfo)

                    '' code Commented by kamalesh Ahuja on 06 April 2010 to resolve Mantis Issue id 14633
                    'ddlDisbTypeId.SelectedIndex = 0
                    'ddlDisbTypeId.Enabled = False
                    'ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
                    'ddlFundSourceId.Enabled = False
                    ''''''''''''''''''''''''''''''''''''''''

                    '   bind an empty Disbursements datagrid and hide it
                    BindDataGrid()

                    '   initialize buttons
                    InitButtonsForLoad()
                    'dgrdApplyPayment.DataSource = ""
                    'dgrdApplyPayment.DataBind()
                Else
                    With New StudentsAccountsFacade
                        BindSavePostPaymentData(.GetPostPaymentInfo(ViewState("TransactionId")))
                    End With
                    '   initialize buttons for edit
                    InitButtonsForEdit()
                    '   clear the apply payments datagrid
                    'dgrdApplyPayment.DataSource = ""
                    'dgrdApplyPayment.DataBind()
                End If
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '        Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtPostPaymentId.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtPostPaymentId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '   if we are in a popUp window then close the window
            'If Not Header1.Visible Then
            '    CommonWebUtilities.CloseWindow(Page)
            'End If
        End If
    End Sub
    Private Function GetAppliedDisbursementsTotal(ByVal disbursements As ArrayList) As Decimal
        Dim totalDisbursements As Decimal = 0.0
        For Each disbursement As ScheduledDisbInfo In disbursements
            totalDisbursements += disbursement.Amount
        Next
        Return totalDisbursements
    End Function
    Private Function BuildDisbCollection() As ArrayList
        Dim DisbArrList As New ArrayList
        Dim item As DataGridItem
        Dim dgItems As DataGridItemCollection = dgrdDisbursements.Items

        For i As Integer = 0 To dgItems.Count - 1
            item = dgItems(i)
            If CType(item.FindControl("chkSelectedDisb"), CheckBox).Checked = True Then
                Dim scheduledDisb As New ScheduledDisbInfo
                With scheduledDisb
                    '.PmtDisbRelId = System.Guid.NewGuid.ToString
                    .IsInDB = CType(item.FindControl("chkScheduleIsInDB"), CheckBox).Checked
                    .TransactionId = txtPostPaymentId.Text
                    .Amount = Decimal.Parse(CType(item.FindControl("txtAmountToApply"), TextBox).Text)
                    If CType(item.FindControl("lblIsAwardDisb"), Label).Text() = "1" Then
                        .AwardScheduleId = CType(item.FindControl("lblScheduleId"), Label).Text()
                        '.PayPlanScheduleId = ""
                    Else
                        .PayPlanScheduleId = CType(item.FindControl("lblScheduleId"), Label).Text()
                        '.AwardScheduleId = ""
                    End If
                    .ModUser = Session("UserName")
                    If CType(item.FindControl("chkScheduleIsInDB"), CheckBox).Checked Then
                        .ModDate = DateTime.Parse(CType(item.FindControl("lblModDate"), Label).Text())
                    Else
                        .ModDate = Date.Parse(Date.Now.ToString)
                    End If
                End With
                DisbArrList.Add(scheduledDisb)
            End If
        Next

        Return DisbArrList
    End Function
    Private Function BuildPostPaymentInfo(ByVal PostPaymentId As String) As PostPaymentInfo

        '   instantiate class
        Dim PostPaymentInfo As New PostPaymentInfo

        With PostPaymentInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get IsPosted
            .IsPosted = chkIsPosted.Checked

            '   get PostPaymentId
            .PostPaymentId = txtPostPaymentId.Text

            '   get StuEnrollId
            .StuEnrollId = txtStuEnrollmentId.Text

            '   get CampusId    
            .CampusId = campusId

            '   get PostPayment Description
            If txtTransDescrip.Visible = True Then
                .PostPaymentDescription = txtTransDescrip.Text.Trim
            Else
                .PostPaymentDescription = ddlDescription.SelectedItem.Text
            End If
            '   get PostPaymentDate
            .PostPaymentDate = Date.Parse(txtTransDate.SelectedDate)

            '   get Amount
            .Amount = Decimal.Parse(txtTransAmount.Text) * (-1.0)

            '   get Reference
            .Reference = txtTransReference.Text.Trim

            '   get Academic Year
            .AcademicYearId = txtAcademicYearId.Text

            '   get Term
            .TermId = txtTermId.Text

            '   get TransCode
            .TransCodeId = ddlTransCodeId.SelectedValue

            ''Added by Saraswathi lakshmanan on July 22 2009 
            ''To get the transCode description on the receipt
            If Not ddlTransCodeId.SelectedItem.Text = "Select" Then
                .TransCodeDescription = ddlTransCodeId.SelectedItem.Text
            End If


            '   get TransTypeId
            .TransTypeId = Integer.Parse(txtTransTypeId.Text)

            '   get PaymentTypeId
            .PaymentTypeId = ddlPaymentTypeId.SelectedValue

            '   get Reference Number
            .CheckNumber = txtReferenceNumber.Text.Trim

            '   set IsDeposited flag according to configuration settings
            Select Case .PaymentTypeId
                Case PaymentType.Cash, PaymentType.Check, PaymentType.Money_Order, PaymentType.Non_Cash
                    .IsDeposited = False
                Case PaymentType.Credit_Card
                    '   check if Credit Card Payments should be marked as Deposited automatically
                    .IsDeposited = CType(MyAdvAppSettings.AppSettings("PostCreditCardPaymentsAutomatically"), Boolean)
                Case PaymentType.EFT
                    '   check if EFT Payments should be marked as Deposited automatically
                    .IsDeposited = CType(MyAdvAppSettings.AppSettings("PostEFTPaymentsAutomatically"), Boolean)
            End Select

            '   get ScheduledPayment
            .ScheduledPayment = chkScheduledPayment.Checked

            '   get UseScheduledPayment
            .UseScheduledPayment = chkUseSchedPmt.Checked

            '   get AwardTypeId
            'BuildStudentAwardsDDL(txtStuEnrollmentId.Text)
            'If Not ddlStudentAwards.SelectedValue = Guid.Empty.ToString Then
            '    .StudentAwardId = ddlStudentAwards.SelectedValue
            'End If

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

            '   get ModUser1
            .ModUser1 = txtModUser1.Text

            '   get ModDate1
            .ModDate1 = Date.Parse(txtModDate1.Text)

            '   Is Reversal
            .IsReversal = chkIsReversal.Checked

            '   Original transactionId
            If chkIsReversal.Checked Then .OriginalTransactionId = txtOriginalTransactionId.Text

            ''Added by SAraswathi on April 16 2010
            ''For mantis case 15222
            .PaymentCodeID = ddlPaymentCode.SelectedValue

            Session("StuEnrollmentId") = txtStuEnrollmentId.Text
            Session("AcademicYearId") = txtAcademicYearId.Text
            Session("TermId") = txtTermId.Text
            .FundSourceID = ddlFundSourceId.SelectedValue
        End With

        '   return data
        Return PostPaymentInfo

    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        '   reset previous state
        SetControls(True)

        '   build dropdownlists
        'BuildDropDownLists()

        '   bind an empty new PostPaymentInfo
        BindPostPaymentData(New PostPaymentInfo)

        '   bind an empty Disbursements datagrid and hide it
        BindDataGrid()

        Dim AdvCtrl As AdvControls.IAdvControl = CType(StudentSearch, AdvControls.IAdvControl)
        AdvCtrl.Clear()

        '   initialize buttons
        InitButtonsForLoad()

        '   set session variable to nothing
        Session("PaymentTransactionId") = Nothing

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
        dgrdApplyPayment.DataSource = ""
        dgrdApplyPayment.DataBind()
        btnSave.Enabled = True
        lblfinAidRequirement.Text = ""

    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtPostPaymentId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim saf As New StudentsAccountsFacade

            '   update PostPaymentInfo 
            Dim result As String = saf.DeletePostPaymentInfo(txtPostPaymentId.Text, Date.Parse(txtModDate.Text), Date.Parse(txtModDate1.Text))
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else
                '   bind an empty new PostPaymentInfo
                BindPostPaymentData(New PostPaymentInfo)

                '   bind an empty Disbursements datagrid and hide it
                BindDataGrid()

                '   initialize buttons
                InitButtonsForLoad()
            End If

        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
        'SDF Code Starts Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim SDFControl As New SDFComponent
        SDFControl.DeleteSDFValue(txtPostPaymentId.Text)
        SDFControl.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub
    Private Sub InitButtonsForLoad()
        '   in popup windows new and delete buttons should be disabled
        'If pObj.HasDisplay Then
        '    btnNew.Enabled = True
        '    btnDelete.Enabled = False
        'Else
        '    btnDelete.Enabled = False
        '    btnNew.Enabled = False
        'End If
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnSaveAndPrint.Enabled = True
            btnNew.Enabled = True
        Else
            btnSave.Enabled = False
            btnSaveAndPrint.Enabled = False
            btnNew.Enabled = False
        End If
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForLoad(ByVal pObj As UserPagePermissionInfo)
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnSaveAndPrint.Enabled = True
            btnNew.Enabled = True
        Else
            btnSave.Enabled = False
            btnSaveAndPrint.Enabled = False
            btnNew.Enabled = False
        End If
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        '   in popup windows new and delete buttons should be disabled
        If pObj.HasEdit Then
            btnNew.Enabled = True
            btnDelete.Enabled = True

            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnNew.Enabled = False
            btnDelete.Enabled = False
        End If
    End Sub
    Private Function ValidateUserEntries() As String

        Dim returnMessage As String = ""

        '   If type of payment is "Select" then user must select a payment option
        If ddlPaymentTypeId.SelectedValue = PaymentType.Select Then
            returnMessage += "You must select a Payment Type" + vbCrLf
        Else
            If Not ddlPaymentTypeId.SelectedValue = PaymentType.Cash Then
                If txtReferenceNumber.Text = "" Then
                    returnMessage += " You must enter a " + lblTypeOfReference.Text
                End If
            End If
        End If
        '   applied payments validation if we are not dealing with a reversal
        If Not CType(Session("CommandName"), String) = "Reverse" Then
            returnMessage += ValidateAppliedAmounts()
        End If

        '   return error messasge
        Return returnMessage

    End Function

    Private Function ValidateAppliedAmounts() As String
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim saf As New StudentsAccountsFacade
        Dim errMsg As String
        Dim totalApplied As Decimal
        Dim amtApplied As Decimal
        Dim balance As Decimal
        Dim transDescrip As String
        Dim transDate As String

        iitems = dgrdApplyPayment.Items
        errMsg = ""
        'Loop thru the collection to retrieve the relevant fields
        For i As Integer = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            amtApplied = 0.0
            balance = 0.0

            'We only want the rows that have an amount applied
            If CType(iitem.FindControl("txtAmountToApply"), TextBox).Text <> "" Then
                'We don't want the rows with zeroes
                If CType(iitem.FindControl("txtAmountToApply"), TextBox).Text <> "0.00" And CType(iitem.FindControl("txtAmountToApply"), TextBox).Text <> "0.0" And CType(iitem.FindControl("txtAmountToApply"), TextBox).Text <> "0" Then
                    'Build up the total amount applied
                    amtApplied = CType(iitem.FindControl("txtAmountToApply"), TextBox).Text
                    balance = CType(iitem.FindControl("lblBalance"), Label).Text
                    transDescrip = CType(iitem.FindControl("lblTransaction"), Label).Text
                    transDate = CType(iitem.FindControl("lblDate"), Label).Text
                    totalApplied += amtApplied
                    'Check if the amount being applied is greater than the balance of the transaction
                    If amtApplied > balance Then
                        errMsg += "The amount applied of " + amtApplied.ToString() + " to " + transDescrip + " dated " + transDate + vbCr
                        errMsg += "is greater than the balance of " + balance.ToString + vbCr + vbCr
                    End If
                    'Check if the amount being applied is less than zero
                    If amtApplied < 0.0 Then
                        errMsg += "The amount applied of " + amtApplied.ToString() + " to " + transDescrip + " dated " + transDate + vbCr
                        errMsg += "needs to be greater than 0 " + vbCr + vbCr
                    End If
                End If 'exclude records with zeroes

            End If 'exclude records with empty amounts
        Next

        If totalApplied > Convert.ToDecimal(txtTransAmount.Text) Then
            errMsg += "The total amount applied cannot be greater than the " + vbCrLf
            errMsg += "amount of the payment being posted" + vbCrLf + vbCrLf
        End If

        Return errMsg

    End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub lbtSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtSearch.Click

        '   uncheck Schedule Payment checkbox
        chkScheduledPayment.Checked = False
        chkUseSchedPmt.Checked = False
        chkUseSchedPmt.Enabled = False
        '   disable Disbursement Types DDL
        ddlDisbTypeId.SelectedIndex = 0
        ddlDisbTypeId.Enabled = False
        '   disable Fund Sources DDL
        '' Code Added by Kamalesh Ahuja on August 25 2010 to resolve mantis issue id 19438
        ddlPaymentCode.SelectedIndex = -1
        ''''''
        ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
        ddlFundSourceId.Enabled = False
        '   hide Disbursements datagrid
        dgrdDisbursements.Visible = False

        '   setup the properties of the new window
        Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px"
        Dim name As String = "StudentPopupSearch2"
        ''resourceId modified by Sarswathi on june 10 2009
        ''the Current page's resourceId is passed intead of the popupsearch's resId
        ''Added to fix issue 14829 --changed from 357-85
        Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx?resid=85&mod=SA&cmpid=" + campusId

        '   build list of parameters to be passed to the new window
        Dim parametersArray(6) As String

        '   set StudentName
        parametersArray(0) = txtStudentName.ClientID

        '   set StuEnrollment
        parametersArray(1) = txtStuEnrollmentId.ClientID
        parametersArray(2) = txtStuEnrollment.ClientID

        '   set Terms
        parametersArray(3) = txtTermId.ClientID
        parametersArray(4) = txtTerm.ClientID

        '   set AcademicYears
        parametersArray(5) = txtAcademicYearId.ClientID
        parametersArray(6) = txtAcademicYear.ClientID

        Session("StuEnrollment") = txtStuEnrollment.ClientID
        Session("AcademicYear") = txtAcademicYear.ClientID
        Session("Term") = txtTerm.ClientID


        '   open new window and pass parameters
        CommonWebUtilities.OpenWindowAndPassParameters(Page, url, name, winSettings, parametersArray)

    End Sub
    Private Sub txtStudentName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtStudentName.TextChanged

        '   get StudentAwards for the new Student
        'BuildStudentAwardsDDL(txtStuEnrollmentId.Text)
        'Once the student enrollment has been selected we can call the code to build the datagrid that displays the 
        'outstanding charges against the student's ledger.


    End Sub

    Private Sub chkScheduledPayment_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkScheduledPayment.CheckedChanged

        If txtStuEnrollmentId.Text <> System.Guid.Empty.ToString Then
            If chkScheduledPayment.Checked Then
                ' ddlDisbTypeId.Enabled = True
                chkUseSchedPmt.Enabled = True
                chkUseSchedPmt.Checked = True

                'setup DisbTypeId ddl. If there is only one option... select it automatically
                If ddlDisbTypeId.Items.Count = 2 Then
                    ddlDisbTypeId.SelectedIndex = 1
                    ddlDisbTypeId_SelectedIndexChanged(Me, New System.EventArgs())
                End If
            Else
                ddlDisbTypeId.SelectedIndex = 0
                ddlDisbTypeId.Enabled = False

                'ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
                'ddlFundSourceId.Enabled = False

                dgrdDisbursements.Visible = False

                chkUseSchedPmt.Checked = False
                chkUseSchedPmt.Enabled = False
            End If
        End If

        '   enable or disable DropDownList accordingly
        'ddlStudentAwards.Enabled = chkScheduledPayment.Checked
    End Sub
    Private Sub SetControls(ByVal val As Boolean)
        txtStudentName.Enabled = val
        '   Commented by Michelle R. Rodriguez on 10/12/05
        '   Two Student Search button appeared when the NEW button was clicked
        'lbtSearch.Visible = val 
        btnSaveAndPrint.Enabled = val
        'Img2.Visible = val
        studentSearchIcon.Visible = val
        txtStuEnrollment.Enabled = val
        txtTransDescrip.Enabled = val
        ddlDescription.Enabled = val
        txtTransDate.Enabled = val
        'drvTransactionDate.Enabled = True
        drvTransactionDate.Enabled = val
        txtTransAmount.Enabled = val



        '!!!!!!!!!!!!!!!!!!!
        'AmountRangeValidator.Enabled = val




        txtTransReference.Enabled = val
        txtAcademicYear.Enabled = val
        txtTerm.Enabled = val
        ddlTransCodeId.Enabled = val
        ''Added ofr mantis case 15222
        ddlPaymentCode.Enabled = val
        ' chkScheduledPayment.Enabled = val
        chkUseSchedPmt.Enabled = val
        ddlPaymentTypeId.Enabled = val
        txtReferenceNumber.Enabled = val
        lblTypeOfReference.Enabled = val
        dgrdDisbursements.Enabled = val
    End Sub
    Private Sub ddlPaymentTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPaymentTypeId.SelectedIndexChanged

        '   set title of reference accordingly
        Select Case ddlPaymentTypeId.SelectedValue
            Case PaymentType.Cash
                lblTypeOfReference.Text = "Receipt Number"
            Case PaymentType.Check
                lblTypeOfReference.Text = "Check Number"
            Case PaymentType.Credit_Card
                lblTypeOfReference.Text = "C/C Authorization"
            Case PaymentType.EFT
                lblTypeOfReference.Text = "EFT Number"
            Case PaymentType.Money_Order
                lblTypeOfReference.Text = "Money Order Number"
            Case PaymentType.Non_Cash
                lblTypeOfReference.Text = "Non Cash Reference #"
        End Select

        'set focus to the same control
        CommonWebUtilities.SetFocus(Me.Page, ddlPaymentTypeId)

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        '   set payment type ddl visibility
        Select Case ddlPaymentTypeId.SelectedValue
            Case PaymentType.Cash, PaymentType.Select
                lblTypeOfReference.Visible = False
                txtReferenceNumber.Visible = False
            Case Else
                lblTypeOfReference.Visible = True
                txtReferenceNumber.Visible = True
        End Select
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(lbtHasDisb)
        controlsToIgnore.Add(ddlPaymentTypeId)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    Private Sub ddlFundSourceId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFundSourceId.SelectedIndexChanged
        BindDataGrid()
    End Sub

    Private Sub ddlDisbTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDisbTypeId.SelectedIndexChanged

        Select Case ddlDisbTypeId.SelectedValue

            Case DisbursementType.Financial_Aid
                '   enable Fund Source DDL
                ddlFundSourceId.Enabled = True
                '   bind Fund Sources DDL
                '' Code Commented by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
                ''BuildFundSourcesDDL(txtStuEnrollmentId.Text)
                BuildFundSourcesDDLNew()
                ''''''''''''


                'if there is only one item... select it automatically
                If ddlFundSourceId.Items.Count = 2 Then
                    ddlFundSourceId.SelectedIndex = 1
                    ddlFundSourceId_SelectedIndexChanged(Me, New System.EventArgs())
                End If
                '' New Code Added By Vijay Ramteke On August 18, 2010 For Mantis Id 19537
            Case DisbursementType.Payment_Plan
                '   enable Fund Source DDL
                ddlFundSourceId.Enabled = True
                '   bind Fund Sources DDL
                '' Code Commented by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
                ''BuildFundSourcesDDL(txtStuEnrollmentId.Text)
                BuildFundSourcesDDLNew()
                ''''''''''''


                'if there is only one item... select it automatically
                If ddlFundSourceId.Items.Count = 2 Then
                    ddlFundSourceId.SelectedIndex = 1
                    ddlFundSourceId_SelectedIndexChanged(Me, New System.EventArgs())
                End If
                '' New Code Added By Vijay Ramteke On August 18, 2010 For Mantis Id 19537
            Case Else
                '   disable Fund Source DDL
                ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
                'ddlFundSourceId.Enabled = False
        End Select

        '' Code Commented by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
        ' BindDataGrid()
        ''''

    End Sub

    Private Sub dgrdDisbursements_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdDisbursements.ItemCreated

        'if it is a reversal, check header checkbox as well as all checkboxes in grid.
        If CType(Session("CommandName"), String) = "Reverse" And Not (ViewState("TransactionId") Is Nothing) Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim ctl As CheckBox = CType(e.Item.FindControl("chkSelectedDisb"), CheckBox)
                If Not (ctl Is Nothing) Then
                    ctl.Checked = True
                End If
            End If
        End If

    End Sub

    Private Sub SelectAllDisbursements()
        Dim item As DataGridItem
        Dim dgItems As DataGridItemCollection = dgrdDisbursements.Items
        Dim ctrl As CheckBox

        For i As Integer = 0 To dgItems.Count - 1
            item = dgItems(i)
            ctrl = CType(item.FindControl("chkSelectedDisb"), CheckBox)
            If Not (ctrl Is Nothing) Then
                ctrl.Checked = True
            End If
        Next
    End Sub

    Private Sub chkUseSchedPmt_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkUseSchedPmt.CheckedChanged
        If chkUseSchedPmt.Checked Then
            txtTransAmount.ReadOnly = True
        Else
            txtTransAmount.ReadOnly = False
        End If
    End Sub

    Private Sub dgrdDisbursements_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgrdDisbursements.PreRender
        Dim dgItems As DataGridItemCollection = dgrdDisbursements.Items
        Dim chkAll As CheckBox
        Dim schedAmount As Decimal = 0

        If CType(Session("CommandName"), String) = "Reverse" And Not (ViewState("TransactionId") Is Nothing) Then
            '   DO NOTHING IF IT IS A REVERSAL

        Else
            '   **************************
            '   ***** NOT A REVERSAL *****
            '   **************************

            If dgItems.Count > 0 Then
                '   dgrdDisbursements.Controls(0) => DataGridTable
                '   DataGridTable.Controls(0) => Header
                chkAll = CType(dgrdDisbursements.Controls(0).Controls(0).FindControl("chkAllItems"), CheckBox)

                If Not chkAll Is Nothing Then
                    For i As Integer = 0 To dgItems.Count - 1
                        Dim item As DataGridItem = dgItems(i)
                        Dim ctrl As CheckBox = CType(item.FindControl("chkSelectedDisb"), CheckBox)

                        If Not (ctrl Is Nothing) Then
                            If chkAll.Checked <> ViewState("AllSelected") Or chkAll.Checked Then
                                ctrl.Checked = chkAll.Checked
                            End If
                            Dim txt As TextBox = CType(item.FindControl("txtAmountToApply"), TextBox)
                            If ctrl.Checked And chkUseSchedPmt.Checked Then
                                txt.ReadOnly = True
                            Else
                                txt.ReadOnly = False
                            End If
                        End If
                    Next
                    ViewState("AllSelected") = chkAll.Checked
                Else
                    ViewState("AllSelected") = False
                End If

                'validate user input
                Dim result As String = ValidateAmountsAppliedToDisbursements(dgrdDisbursements)
                If Len(result) > 0 Then
                    DisplayErrorMessage(result)
                    Exit Sub
                Else
                    If chkUseSchedPmt.Checked Then txtTransAmount.Text = GetTotalAmountAppliedToDisbursements(dgrdDisbursements).ToString("#.00")
                End If
            End If
        End If
    End Sub
    Private Function ValidateAmountsAppliedToDisbursements(ByVal dgrdDisbursments As DataGrid) As String
        Dim chkAll As CheckBox = CType(dgrdDisbursements.Controls(0).Controls(0).FindControl("chkAllItems"), CheckBox)
        Dim dgItems As DataGridItemCollection = dgrdDisbursements.Items
        Dim errMsg As String = ""

        If Not chkAll Is Nothing Then
            For i As Integer = 0 To dgItems.Count - 1
                Dim item As DataGridItem = dgItems(i)
                Dim ctrl As CheckBox
                ctrl = CType(item.FindControl("chkSelectedDisb"), CheckBox)

                If Not (ctrl Is Nothing) Then
                    If chkAll.Checked <> ViewState("AllSelected") Or chkAll.Checked Then
                        ctrl.Checked = chkAll.Checked
                    End If
                    If ctrl.Checked And chkUseSchedPmt.Checked Then
                        Dim lbl As Label = CType(item.FindControl("lblBalance"), Label)
                        Dim txt As TextBox = CType(item.FindControl("txtAmountToApply"), TextBox)
                        If Not IsValidAmount(txt.Text) Then
                            errMsg += "Amount: " + txt.Text + "is invalid."
                        Else
                            If Decimal.Parse(txt.Text) > Decimal.Parse(lbl.Text, Globalization.NumberStyles.Currency) Then
                                errMsg += "Amount to apply: " + txt.Text + " is greater than balance: " + lbl.Text
                            End If
                        End If
                    End If
                End If
            Next
        End If

        'return error message
        Return errMsg
    End Function
    Private Function GetTotalAmountAppliedToDisbursements(ByVal dgrdDisbursments As DataGrid) As Decimal
        Dim chkAll As CheckBox = CType(dgrdDisbursements.Controls(0).Controls(0).FindControl("chkAllItems"), CheckBox)
        Dim dgItems As DataGridItemCollection = dgrdDisbursements.Items
        Dim totalAmount As Decimal = 0.0
        If Not chkAll Is Nothing Then
            For i As Integer = 0 To dgItems.Count - 1
                Dim item As DataGridItem = dgItems(i)
                Dim ctrl As CheckBox
                ctrl = CType(item.FindControl("chkSelectedDisb"), CheckBox)

                If Not (ctrl Is Nothing) Then
                    If chkAll.Checked <> ViewState("AllSelected") Or chkAll.Checked Then
                        ctrl.Checked = chkAll.Checked
                    End If
                    If ctrl.Checked And chkUseSchedPmt.Checked Then
                        Dim lbl As Label = CType(item.FindControl("lblBalance"), Label)
                        Dim txt As TextBox = CType(item.FindControl("txtAmountToApply"), TextBox)
                        totalAmount += Decimal.Parse(txt.Text, Globalization.NumberStyles.Currency)
                    End If
                End If
            Next
        End If

        'return error message
        Return totalAmount

    End Function
    Private Function IsValidAmount(ByVal str As String) As Boolean
        Try
            Dim dec As Decimal = Decimal.Parse(str)
            If dec > 0 Then Return True
        Catch
            Return False
        End Try
        Return False
    End Function
    Private Sub lbtHasDisb_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtHasDisb.Click
        '   Modified by Michelle R. Rodriguez on 10/12/2005
        '   Moved from lblSearch_Click because it is now done cliend-side, so it would never be executed.
        '   uncheck Schedule Payment checkbox
        chkScheduledPayment.Checked = False
        chkUseSchedPmt.Checked = False
        chkUseSchedPmt.Enabled = False
        '   disable Disbursement Types DDL
        ddlDisbTypeId.SelectedIndex = 0
        ddlDisbTypeId.Enabled = False
        '   disable Fund Sources DDL
        ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
        ddlFundSourceId.Enabled = False
        '   hide Disbursements datagrid
        dgrdDisbursements.Visible = False

        If txtStuEnrollmentId.Text <> "" Then
            Dim dt As DataTable = (New StudentsAccountsFacade).HasDisbursements(txtStuEnrollmentId.Text)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("PPAmount") > 0 And dt.Rows(0)("AwardAmount") > 0 Then
                    DisplayErrorMessage("Student has both scheduled Payment Plan payments and Financial Aid disbursements")
                    ' chkScheduledPayment.Enabled = True
                    'chkUseSchedPmt.Enabled = True
                    '
                    ddlDisbTypeId.Items.Clear()
                    ddlDisbTypeId.Items.Add(New ListItem("Select", "0"))
                    ddlDisbTypeId.Items.Add(New ListItem("Financial Aid", "1"))
                    ddlDisbTypeId.Items.Add(New ListItem("Payment Plan", "2"))
                    ViewState("HasFinAidPayments") = True
                    ViewState("HasStudentPayments") = True
                    '' Code Commented by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
                    ''BindDDLPaymentCode()
                    '''''''''''

                ElseIf dt.Rows(0)("PPAmount") > 0 Then
                    DisplayErrorMessage("Student has scheduled Payment Plan payments")
                    '  chkScheduledPayment.Enabled = True
                    'chkUseSchedPmt.Enabled = True
                    '
                    ddlDisbTypeId.Items.Clear()
                    ddlDisbTypeId.Items.Add(New ListItem("Select", "0"))
                    ddlDisbTypeId.Items.Add(New ListItem("Payment Plan", "2"))

                    ViewState("HasStudentPayments") = True

                    '' Code Commented by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
                    'If Not ViewState("PaymentCodes") Is Nothing Then
                    '    BindDDLPaymentCode("(11)")
                    'End If
                    ''''''''''''''''''

                ElseIf dt.Rows(0)("AwardAmount") > 0 Then
                    DisplayErrorMessage("Student has scheduled Financial Aid disbursements")
                    '  chkScheduledPayment.Enabled = True
                    'chkUseSchedPmt.Enabled = True
                    '
                    ddlDisbTypeId.Items.Clear()
                    ddlDisbTypeId.Items.Add(New ListItem("Select", "0"))
                    ddlDisbTypeId.Items.Add(New ListItem("Financial Aid", "1"))
                    ViewState("HasFinAidPayments") = True
                    '' Code Commented by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
                    'If Not ViewState("PaymentCodes") Is Nothing Then
                    '    BindDDLPaymentCode("(12)")
                    'End If
                    ''''''''''''''
                Else
                    '' Code Commented by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
                    'If Not ViewState("PaymentCodes") Is Nothing Then
                    '    BindDDLPaymentCode("(11,12)")
                    'End If
                    '''''''''''''''''''
                    chkScheduledPayment.Enabled = False
                    chkUseSchedPmt.Enabled = False
                End If
            End If
        End If
    End Sub

    Private Sub BindDDLPaymentCode(Optional ByVal PaymentTypes As String = "")
        Dim dspaymentcode As New DataSet
        dspaymentcode = ViewState("PaymentCodes")
        Dim drFinal As DataRow()
        Dim i As Integer
        Dim removeListItem As RadComboBoxItem

        'Dim removeListItem As ListItem

        ddlPaymentCode.Items.Clear()
        With ddlPaymentCode
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeID"
            .DataSource = dspaymentcode
            .DataBind()
            .Items.Insert(0, New RadComboBoxItem("Select", Guid.Empty.ToString))

            '.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))

            .SelectedIndex = 0
        End With
        If PaymentTypes <> "" Then
            drFinal = dspaymentcode.Tables(0).Select("SysTransCodeId in " & PaymentTypes)
            For Each dr As DataRow In drFinal
                For i = ddlPaymentCode.Items.Count - 1 To 0 Step -1
                    If ddlPaymentCode.Items(i).Value.ToString = dr("TransCodeId").ToString Then
                        removeListItem = ddlPaymentCode.Items(i)
                        ddlPaymentCode.Items.Remove(removeListItem)
                    End If
                Next
            Next
        End If
    End Sub

    Private Sub BuildApplyPaymentGrid(Optional ByVal stuenrollmentid As String = "")
        '------------------------------------------------------------------------------------------------
        'Get the charges that have a balance greater than zero (0) for the selected stuEnrollId
        '------------------------------------------------------------------------------------------------
        Dim fac As New StudentsAccountsFacade
        Dim dt As New DataTable

        If stuenrollmentid = String.Empty Then
            dt = fac.GetChargesWithBalanceGreaterThanZero(txtStuEnrollmentId.Text)
        Else
            dt = fac.GetChargesWithBalanceGreaterThanZero(stuenrollmentid)
        End If


        dgrdApplyPayment.DataSource = dt
        dgrdApplyPayment.DataBind()
        lblAppliedPmtCaption.Visible = True
    End Sub

    Private Function BuildAppliedPaymentCollection() As ArrayList
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim saf As New StudentsAccountsFacade
        Dim appColl As New ArrayList

        iitems = dgrdApplyPayment.Items

        'Loop thru the collection to retrieve the relevant fields
        For i As Integer = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            'We only want the rows that have an amount applied
            If CType(iitem.FindControl("txtAmountToApply"), TextBox).Text <> "" Then
                'We don't want the rows with zeroes
                If CType(iitem.FindControl("txtAmountToApply"), TextBox).Text <> "0.00" And CType(iitem.FindControl("txtAmountToApply"), TextBox).Text <> "0.0" And CType(iitem.FindControl("txtAmountToApply"), TextBox).Text <> "0" Then
                    Dim applPmtInfo As New AppliedPaymentInfo

                    applPmtInfo.AppliedPaymentId = Guid.NewGuid.ToString
                    applPmtInfo.PaymentId = txtPostPaymentId.Text
                    applPmtInfo.ChargeId = CType(iitem.FindControl("lblTransactionId"), Label).Text
                    applPmtInfo.Amount = CType(iitem.FindControl("txtAmountToApply"), TextBox).Text
                    applPmtInfo.ModUser = Session("UserName")

                    'Add the item to the arraylist
                    appColl.Add(applPmtInfo)

                End If 'exlude records with zeroes

            End If 'exclude records with empty amounts


        Next

        Return appColl

    End Function
    Friend Sub PrintAReceipt(ByVal receiptXmlDoc As XmlDocument)

        ' Load the xsl-fo style sheet.
        Dim xslt As New XslCompiledTransform()
        xslt.Load(Server.MapPath("..") + "/" + Context.Request.Headers.Item("host") + "/Xsl/Receipts.xsl")

        'this is a temp file in which the xsl-fo stream will be created
        Dim tempFOFileName As String = System.IO.Path.GetTempFileName()
        Dim outFileStream As New IO.FileStream(tempFOFileName, IO.FileMode.Create)

        'create xsl-fo file
        xslt.Transform(New XmlNodeReader(receiptXmlDoc), Nothing, outFileStream)
        outFileStream.Close()

        'this is the temp PDF file 
        Dim tempPDFFile As String = System.IO.Path.GetTempFileName()

        'generate the pdf file
        GeneratePDF(tempFOFileName, tempPDFFile)

        'open a child window that will display the PDF file
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium
        Dim url As String = "../SA/PrintReceipts.aspx?pdfFile=" + Server.UrlEncode(tempPDFFile)
        CommonWebUtilities.OpenChildWindow(Page, url, "PrintReceipts", winSettings)

    End Sub
    Private Function GeneratePDF(ByVal foFile As String, ByVal pdfFile As String) As Integer
        'Dim streamFO As New FileInputStream(foFile)
        'Dim src As New InputSource(streamFO)
        'Dim streamOut As New FileOutputStream(pdfFile)
        'Dim driver = New Driver(src, streamOut)
        'driver.setRenderer(org.apache.fop.apps.Driver.RENDER_PDF)
        'driver.run()
        'streamOut.close()
        Return Nothing
    End Function
    Friend Function CreateReceiptXmlDoc(ByVal postPaymentInfo As PostPaymentInfo, ByVal receiptAddress As ReceiptAddress, ByVal name As String, ByVal StudentAddressInfo As StuAddressInfo) As XmlDocument
        Return CreateReceiptXmlDoc(postPaymentInfo, receiptAddress.SchoolName, receiptAddress.SchoolAddress1, receiptAddress.SchoolAddress2, receiptAddress.City, receiptAddress.State, receiptAddress.Zip, name, StudentAddressInfo, receiptAddress.Country)
    End Function
    Friend Function CreateReceiptXmlDoc(ByVal postPaymentInfo As PostPaymentInfo, ByVal schoolName As String, ByVal schoolAddress1 As String, ByVal schoolAddress2 As String, ByVal city As String, ByVal state As String, ByVal zip As String, ByVal name As String, ByVal studentAddressInfo As StuAddressInfo, ByVal country As String) As XmlDocument
        ''modified by Saraswathi lakshmanan on July 22 2009
        ''The Post payment details are passed for printing in the receipts
        ''To fix mantis 16578

        Dim paymentTypeDesc As String
        Select Case postPaymentInfo.PaymentTypeId
            Case PaymentType.Cash
                paymentTypeDesc = "Cash"
            Case PaymentType.Check
                paymentTypeDesc = "Check"
            Case PaymentType.Credit_Card
                paymentTypeDesc = "Credit Card"
            Case PaymentType.EFT
                paymentTypeDesc = "EFT"
            Case PaymentType.Money_Order
                paymentTypeDesc = "Money Order"
            Case PaymentType.Non_Cash
                paymentTypeDesc = "Non Cash"
            Case Else
                paymentTypeDesc = "Other"
        End Select

        Return CreateReceiptXmlDoc(schoolName, schoolAddress1, schoolAddress2, city, state, zip, postPaymentInfo.PostPaymentDate.ToShortDateString, postPaymentInfo.PostPaymentDate.ToShortTimeString, name, postPaymentInfo.UniqueId, -postPaymentInfo.Amount.ToString("###,###.00"), postPaymentInfo.TransCodeDescription, paymentTypeDesc, postPaymentInfo.CheckNumber, studentAddressInfo.Address1, studentAddressInfo.Address2, studentAddressInfo.City, studentAddressInfo.StateText, studentAddressInfo.Zip, country, studentAddressInfo.CountryText)
    End Function
    ''modified by Saraswathi lakshmanan on July 22 2009
    ''The Post payment details are passed for printing in the receipts
    ''To fix mantis 16578
    Friend Function CreateReceiptXmlDoc(ByVal schoolName As String, ByVal schoolAddress1 As String, ByVal schoolAddress2 As String, ByVal city As String, ByVal state As String, ByVal zip As String, ByVal [date] As String, ByVal [time] As String, ByVal name As String, ByVal uniqueId As String, ByVal amount As String, ByVal TransDesc As String, ByVal paymentTypeDesc As String, ByVal CheckNumber As String, ByVal StuAddress1 As String, ByVal StuAddress2 As String, ByVal StuCity As String, ByVal StuState As String, ByVal StuZip As String, ByVal Country As String, ByVal StudentCountry As String) As XmlDocument

        Dim sw As New System.IO.StringWriter()
        Dim ReceiptXml As New XmlTextWriter(sw)

        '   write xml document header
        ReceiptXml.Formatting = System.Xml.Formatting.None
        ReceiptXml.WriteStartDocument(True)
        ReceiptXml.WriteComment("FAME - This document contains a Payments Receipt")

        'output start tag of element "receipts"
        ReceiptXml.WriteStartElement("receipts")

        'output start tag of element "receipt"
        ReceiptXml.WriteStartElement("receipt")

        'output details
        ReceiptXml.WriteElementString("schoolName", schoolName)
        ReceiptXml.WriteElementString("schoolAddress1", schoolAddress1)
        ReceiptXml.WriteElementString("schoolAddress2", schoolAddress2)
        ReceiptXml.WriteElementString("city", city)
        ReceiptXml.WriteElementString("state", state)
        ReceiptXml.WriteElementString("zip", zip)
        ReceiptXml.WriteElementString("country", Country)
        ReceiptXml.WriteElementString("date", [date])
        ReceiptXml.WriteElementString("time", [time])
        ReceiptXml.WriteElementString("name", name)
        ReceiptXml.WriteElementString("uniqueId", uniqueId)
        ReceiptXml.WriteElementString("amount", amount)

        ''Added by Saraswathi lakshmanan on Sept 18 2009 to show the student Address
        ReceiptXml.WriteElementString("studentAddress1", StuAddress1)
        ReceiptXml.WriteElementString("studentAddress2", StuAddress2)
        ReceiptXml.WriteElementString("studentCity", StuCity)
        ReceiptXml.WriteElementString("studentState", StuState)
        ReceiptXml.WriteElementString("studentZip", StuZip)
        ReceiptXml.WriteElementString("studentCountry", StudentCountry)

        ''Added by Saraswathi lakshmanan on July 22 2009
        ''To fix mantis 16578
        ReceiptXml.WriteElementString("TransDesc", TransDesc)
        ReceiptXml.WriteElementString("paymenttypeDesc", paymentTypeDesc)
        ReceiptXml.WriteElementString("CheckNumber", CheckNumber)


        'output end tag of element "receipt"
        ReceiptXml.WriteEndElement()

        'output end tag of element "receipts"
        ReceiptXml.WriteEndElement()

        'close xml writer
        ReceiptXml.Flush()

        'return xmlDoc
        Dim xmlDoc As New XmlDocument()
        xmlDoc.LoadXml(sw.ToString)
        Return xmlDoc

    End Function
    Friend Function GetAddressToBePrintedInReceipts(ByVal userConfiguredValue As String) As ReceiptAddress

        Dim receiptAddress As ReceiptAddress = Nothing

        'address to be printed in the receipt might be the corporate address or campus address
        'according to application settings
        Select Case System.Enum.Parse(GetType(AdvantageCommonValues.AddressToBePrintedInReceipts), userConfiguredValue)
            'use corporate address
            Case AdvantageCommonValues.AddressToBePrintedInReceipts.CorporateAddress
                receiptAddress = New ReceiptAddress()
                '' New Code Added By Vijay Ramteke On January 24, 2011 For Rally Id DE4078
                ''receiptAddress.SchoolName = SingletonAppSettings.AppSettings("CorporateName")
                ''receiptAddress.SchoolAddress1 = SingletonAppSettings.AppSettings("CorporateAddress1")
                ''receiptAddress.SchoolAddress2 = SingletonAppSettings.AppSettings("CorporateAddress2")
                ''receiptAddress.City = SingletonAppSettings.AppSettings("CorporateCity")
                ''receiptAddress.State = SingletonAppSettings.AppSettings("CorporateState")
                ''receiptAddress.Zip = SingletonAppSettings.AppSettings("CorporateZip")
                If Not (MyAdvAppSettings.AppSettings("CorporateName") Is System.DBNull.Value) Then receiptAddress.SchoolName = MyAdvAppSettings.AppSettings("CorporateName")
                If Not (MyAdvAppSettings.AppSettings("CorporateAddress1") Is System.DBNull.Value) Then receiptAddress.SchoolAddress1 = MyAdvAppSettings.AppSettings("CorporateAddress1")
                If Not (MyAdvAppSettings.AppSettings("CorporateAddress2") Is System.DBNull.Value) Then receiptAddress.SchoolAddress2 = MyAdvAppSettings.AppSettings("CorporateAddress2")
                If Not (MyAdvAppSettings.AppSettings("CorporateCity") Is System.DBNull.Value) Then receiptAddress.City = MyAdvAppSettings.AppSettings("CorporateCity")
                If Not (MyAdvAppSettings.AppSettings("CorporateState") Is System.DBNull.Value) Then receiptAddress.State = MyAdvAppSettings.AppSettings("CorporateState")
                If Not (MyAdvAppSettings.AppSettings("CorporateZip") Is System.DBNull.Value) Then receiptAddress.Zip = MyAdvAppSettings.AppSettings("CorporateZip")
                If Not (MyAdvAppSettings.AppSettings("CorporateCountry") Is System.DBNull.Value) Then receiptAddress.Country = MyAdvAppSettings.AppSettings("CorporateCountry")

                '' New Code Added By Vijay Ramteke On January 24, 2011 For Rally Id DE4078
                'use Campus Address
            Case AdvantageCommonValues.AddressToBePrintedInReceipts.CampusAddress
                receiptAddress = (New CampusGroupsFacade).GetCampusDataAsReceiptAddress(campusId)
        End Select

        'return receipt address
        Return receiptAddress

    End Function

    Protected Sub btnSaveAndPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAndPrint.Click
        userWantsToPrintAReceipt = True
        btnSave_Click(Me, New System.EventArgs)
    End Sub
    Private Sub GetPDF(ByVal streamFO As SByte())

        'this is the to InputSource
        'Dim src As InputSource = New InputSource(New ByteArrayInputStream(streamFO))

        ''this is the output stream
        ''Dim streamOut As ByteArrayOutputStream = New ByteArrayOutputStream()
        ''this is the name of the temporary file
        'Dim tempFileName As String = System.IO.Path.GetTempPath() + "test.pdf"
        'Dim streamOut As New java.io.FileOutputStream(tempFileName)

        ''render pdf
        'Dim driver As Driver = New Driver(src, streamOut)
        'driver.setRenderer(org.apache.fop.apps.Driver.RENDER_PDF)
        'driver.run()

        'streamOut.close()
        ''return byte array
        ''Return 1
    End Sub
    Private Function ToSByteArray(ByVal source As Byte()) As SByte()
        Dim sbytes(source.Length - 1) As SByte
        System.Buffer.BlockCopy(source, 0, sbytes, 0, source.Length)
        Return sbytes
    End Function
    Private Function ToByteArray(ByVal source As SByte()) As Byte()
        Dim bytes(source.Length - 1) As Byte
        System.Buffer.BlockCopy(source, 0, bytes, 0, source.Length)
        Return bytes
    End Function
    ''Added by Saraswathi To find the STudent Ledger balance
    ''October 8-2008
    Private Sub StudentLedgerBalance(ByVal StuEnrollId As String)
        Dim saf As New StudentsAccountsFacade
        Dim StudentLedBalance As Double

        StudentLedBalance = saf.GetStudentLedgerBalance(StuEnrollId)
        If StudentLedBalance < 0 Then
            lblstuLedgerBalance.Visible = True
            lblstuLedgerBalance.Text = "Ledger Balance: " & FormatCurrency(StudentLedBalance)
            lblstuLedgerBalance.ForeColor = Color.Red
            'lblstuLedgerBalance.CssClass = "LabelNegativeAmount"
        Else
            lblstuLedgerBalance.Visible = True
            lblstuLedgerBalance.Text = "Ledger Balance: " & FormatCurrency(StudentLedBalance)
            lblstuLedgerBalance.ForeColor = Color.Black
            'lblstuLedgerBalance.CssClass = "LabelAmount"
        End If

    End Sub
    ''Added by Saraswathi lakshmanan on Sept 22 2010
    ''For document tracking
    ''To find out if the student has met all the financial Aid requitrements

    'Private Sub StudentFinancialAidRequirement(ByVal StuEnrollId As String)
    '    Dim saf As New StudentsAccountsFacade
    '    Dim StudentMetFinAidReq As Double

    '    StudentMetFinAidReq = saf.GetStudentLedgerBalance(StuEnrollId)
    '    If StudentMetFinAidReq < 0 Then
    '        lblstuLedgerBalance.Visible = True
    '        lblstuLedgerBalance.Text = "LedgerBalance: " & FormatCurrency(StudentLedBalance)

    '    End If

    'End Sub




    Protected Sub txtStuEnrollmentId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStuEnrollmentId.TextChanged
        BuildApplyPaymentGrid()
    End Sub

    Protected Sub ddlPaymentCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPaymentCode.SelectedIndexChanged

        Dim isFinAidPayment As Boolean = False
        Dim isStudentPayment As Boolean = False

        Dim HasfinAidPayments As Boolean = False
        Dim hasStudentPayments As Boolean = False


        If Not ViewState("PaymentCodes") Is Nothing Then
            Dim dsPaymentCode As New DataSet
            dsPaymentCode = ViewState("PaymentCodes")

            '        int countOfFrenchCustomers =
            '(from c in customers.AsEnumerable()
            ' where c.Field<string>("Country") == "France"
            ' select c).Count();.Field(Of String)("Name")
            ' Dim query = From order In SummaryInfo.AsEnumerable() _
            'Where order.Field(Of DateTime)("ExpGradDate") < gradDate1 Or order.Field(Of DateTime)("ExpGradDate") > gradDate2 Select order Order By order.Field(Of String)(strOrder(0)), order.Field(Of String)(strOrder(1)), order.Field(Of String)(strOrder(2))

            'Dim query As System.Data.OrderedEnumerableRowCollection(Of System.Data.DataRow) = From PayCode In dsPaymentCode.Tables(0).AsEnumerable() _
            ' Where PayCode.Field(Of Integer)("SysTransCodeId") = 11 Select PayCode

            ''    Dim query As System.Data.OrderedEnumerableRowCollection(Of System.Data.DataRow) = From PayCode In dsPaymentCode.Tables(0).AsEnumerable() _
            ''Where PayCode.Field(Of Integer)("SysTransCodeId") = 11 Select PayCode


            'For Each Code As DataRow In query
            '    If ddlPaymentCode.SelectedItem.Value = Code("SysTransCodeID") Then
            '        isFinAidPayment = True
            '    End If
            'Next

            'For Each Code In query
            '    If ddlPaymentCode.SelectedItem.Value = Code Then
            '        isFinAidPayment = True
            '    End If
            'Next

            Dim dr As DataRow()
            dr = dsPaymentCode.Tables(0).Select("TransCodeID = '" + ddlPaymentCode.SelectedItem.Value + "'")
            'Dim removeItem As ListItem
            If dr.Length > 0 Then

                For Each row As DataRow In dr
                    If row("SysTransCodeId") = 11 Then
                        isFinAidPayment = True
                        ddlDisbTypeId.Items.Clear()
                        ddlDisbTypeId.Items.Add(New ListItem("Select", "0"))
                        ddlDisbTypeId.Items.Add(New ListItem("Financial Aid", "1"))
                        'ddlDisbTypeId.Items.Add(New ListItem("Payment Plan", "2"))
                        'removeItem = ddlDisbTypeId.Items.FindByText("Payment Plan")
                        'ddlDisbTypeId.Items.Remove(removeItem)

                    ElseIf row("SysTransCodeId") = 12 Then
                        isStudentPayment = True
                        ddlDisbTypeId.Items.Clear()
                        ddlDisbTypeId.Items.Add(New ListItem("Select", "0"))
                        ddlDisbTypeId.Items.Add(New ListItem("Payment Plan", "2"))
                        'removeItem = ddlDisbTypeId.Items.FindByText("Financial Aid")
                        'ddlDisbTypeId.Items.Remove(removeItem)
                    Else
                        ddlDisbTypeId.Items.Clear()
                        ddlDisbTypeId.Items.Add(New ListItem("Select", "0"))
                        ddlDisbTypeId.Items.Add(New ListItem("Financial Aid", "1"))
                        ddlDisbTypeId.Items.Add(New ListItem("Payment Plan", "2"))
                    End If
                Next
            End If
            If Not ViewState("HasFinAidPayments") Is Nothing Then
                If ViewState("HasFinAidPayments") = True Then
                    HasfinAidPayments = True
                End If
            End If

            If Not ViewState("HasStudentPayments") Is Nothing Then
                If ViewState("HasStudentPayments") = True Then
                    hasStudentPayments = True
                End If
            End If

            '' Code Added by Kamalesh Ahuja on August 25 2010 to resolve mantis issue id 19438
            ddlFundSourceId.Enabled = True
            BuildFundSourcesDDLNew()
            ''''''''''''

            If isFinAidPayment = True Or isStudentPayment = True Then
                If HasfinAidPayments = True Or hasStudentPayments = True Then
                    If txtStuEnrollmentId.Text <> System.Guid.Empty.ToString Then

                        ' ddlDisbTypeId.Enabled = True
                        chkUseSchedPmt.Enabled = True
                        chkUseSchedPmt.Checked = True

                        'setup DisbTypeId ddl. If there is only one option... select it automatically
                        If ddlDisbTypeId.Items.Count = 2 Then
                            ddlDisbTypeId.SelectedIndex = 1
                            ddlDisbTypeId_SelectedIndexChanged(Me, New System.EventArgs())
                        End If

                    End If
                End If

            Else

                ddlDisbTypeId.SelectedIndex = 0
                ddlDisbTypeId.Enabled = False


                ''''' code changes made by Kamalesh Ahuja on 25 August 2010 to resolve mantis issue id 19618
                'ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
                'ddlFundSourceId.Enabled = False
                '''''''''

                dgrdDisbursements.Visible = False

                chkUseSchedPmt.Checked = False
                chkUseSchedPmt.Enabled = False
                chkScheduledPayment.Checked = False
                chkScheduledPayment.Enabled = False
            End If




        End If

        If ddlPaymentCode.SelectedItem.Text = "Select" Then
            ddlFundSourceId.Items.Clear()
            ddlFundSourceId.Enabled = False
        ElseIf (ddlPaymentCode.SelectedItem.Text <> "Select" And txtStuEnrollmentId.Text <> "") And isFinAidPayment = True Then
            StudentFinancialAidRequirement()
        ElseIf (ddlPaymentCode.SelectedItem.Text <> "Select" And txtStuEnrollmentId.Text <> "") And isFinAidPayment = False Then
            lblfinAidRequirement.Visible = False
            ViewState("IsStudentEligible") = True

        End If




    End Sub

    Private Sub StudentFinancialAidRequirement(Optional ByVal fullname As String = "")
        Dim IsStudentEligible As String = isStudentEligibleForFinAid(txtStuEnrollmentId.Text, campusId)
        Dim strStudentName As String

        If fullname Is String.Empty Then
            strStudentName = txtStudentName.Text
        Else
            strStudentName = fullname
        End If

        If Not IsStudentEligible = "" Then
            'lnkEnrollLead.Visible = False
            'lnkOpenRequirements.Visible = False
            If IsStudentEligible = "Eligible" Then
                ViewState("IsStudentEligible") = True
                lblfinAidRequirement.Text = "<strong>" & strStudentName & " </strong> has satisfied all Financial Aid requirements and " & "<strong>" & " is eligible for Posting Financial Aid Disbursements." & "</strong>"
                lblfinAidRequirement.Visible = True
                'btnSave.Enabled = True
                'btnSaveAndPrint.Enabled = True
                'lnkToEnrollLead.Visible = False
                'lblOpenReqs.Visible = False
                'lblOpenReqs.Text = " and to view Admission Requirements, click "
                'lnkToOpenRequirements.Visible = False
                'lnkToOpenRequirements.Font.Bold = True
                'lnkEnrollLead.Enabled = True
                'lnkOpenRequirements.Enabled = True
            Else
                ViewState("IsStudentEligible") = False
                ''Code Added By Vijay Ramteke On January 24 ,2011 For Rally Id DE4062
                ''lblfinAidRequirement.Text = "<strong>" & strStudentName & " </strong> has not satisfied all admission requirements and " & "<strong>" & " is not eligible for Posting Payments.</strong>"
                lblfinAidRequirement.Text = "<strong>" & strStudentName & " </strong> has not satisfied all Financial Aid requirements and " & "<strong>" & " is not eligible for Posting Financial Aid Disbursements.</strong> The requirements are :" & IsStudentEligible
                ''Code Added By Vijay Ramteke On January 24 ,2011 For Rally Id DE4062

                lblfinAidRequirement.Visible = True
                'btnSave.Enabled = False
                'btnSaveAndPrint.Enabled = False
                'lblOpenReqs.Text = " To view Admission Requirements, please click "
                'lblOpenReqs.Visible = False
                'lnkToOpenRequirements.Visible = False
                'lnkToEnrollLead.Visible = False
                'lnkEnrollLead.Enabled = False
                'lnkOpenRequirements.Enabled = True
            End If

        End If

    End Sub

    'Private Function isStudentEligibleForFinAid(ByVal StuEnrollId As String, ByVal CampusId As String) As String
    '    Dim TestRequired As New CheckLeadTest
    '    Dim strEnrollStatus As String = ""

    '    Dim strTestStatus As String = TestRequired.CheckIfRequiredTest_ForFinAid(StuEnrollId)
    '    Dim strDocStatus As String = TestRequired.CheckRequiredDocuments_ForFinAid(StuEnrollId)
    '    Dim strReqsMetStatus As String = (New AdReqsFacade).CheckIfReqGroupMeetsConditions_FinAid_SP(StuEnrollId)
    '    If strTestStatus = "Eligible" And strDocStatus = "Eligible" And strReqsMetStatus = "Eligible" Then
    '        strEnrollStatus = "Eligible"
    '    Else
    '        strEnrollStatus = "Not Eligible"
    '    End If
    '    Return strEnrollStatus

    'End Function
    Private Function isStudentEligibleForFinAid(ByVal StuEnrollId As String, ByVal CampusId As String) As String
        Dim TestRequired As New GraduateAuditFacade
        Dim strEnrollStatus As String = ""

        Dim strTestStatus As String = TestRequired.CheckandListRequiredtest_ForFinAid(StuEnrollId)
        Dim strDocStatus As String = TestRequired.CheckandListRequiredDocuments_ForFinAid(StuEnrollId)
        Dim strReqsMetStatus As String = TestRequired.CheckandListFinAidreqGroups(StuEnrollId)

        If strTestStatus = "" And strDocStatus = "" And strReqsMetStatus = "" Then
            strEnrollStatus = "Eligible"
        Else
            strEnrollStatus = strReqsMetStatus + strDocStatus + strTestStatus
        End If
        Return strEnrollStatus

    End Function


    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, Optional ByVal hourtype As String = "")

        If enrollid = String.Empty Then
            enrollid = txtStuEnrollmentId.Text
        End If

        pnlColumn1.Enabled = True
        pnlColumn2.Enabled = True
        'btnSave.Enabled = True
        'btnNew.Enabled = True

        txtStuEnrollmentId.Text = enrollid
        txtTermId.Text = termid
        txtTerm.Text = termdescrip
        txtAcademicYearId.Text = academicyearid
        txtAcademicYear.Text = academicyeardescrip
        txtStudentName.Text = fullname
        BuildApplyPaymentGrid(enrollid)
        StudentLedgerBalance(enrollid)
        'StudentFinancialAidRequirement(fullname)

        If termid = Nothing Then
            pnlColumn1.Enabled = False
            pnlColumn2.Enabled = False
            'btnSave.Enabled = False
            lblNoTerm.Text = "<strong> This enrollments start date is greater than any term start date in the system</strong>"
            lblNoTerm.Visible = True
        Else
            lblNoTerm.Visible = False

        End If

    End Sub
    ' DE7793 7/18/2012 Janet Robinson Student Receipt using ssrs
    Protected Sub PrintStudentReceipt(ByVal TransactionId As String, UniqueId As String)
        Dim getReportAsBytes As [Byte]()
        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") '"/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
        getReportAsBytes = (New Logic.PrintStudentReceipt).RenderReport("pdf", TransactionId, _
                                                                               UniqueId, _
                                                                               AdvantageSession.UserState.UserName.ToString, _
                                                                               strReportPath, _
                                                                               AdvantageSession.UserState.CampusId.ToString)
        ExportReport("pdf", getReportAsBytes)
    End Sub

    Private Sub ExportReport(ByVal ExportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case ExportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script As String = String.Format("window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub

    Protected Sub Hdn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim saf As New StudentsAccountsFacade
        Dim result As String
        Dim DisbArrList As ArrayList = BuildDisbCollection()
        Dim arrApplyPmt As ArrayList = BuildAppliedPaymentCollection()
        Dim postPaymentInfo As PostPaymentInfo = BuildPostPaymentInfo(txtPostPaymentId.Text)
        result = saf.UpdatePostPaymentInfo(postPaymentInfo, Session("UserName"), DisbArrList, arrApplyPmt)
        userWantsToPrintAReceipt = False
        If result <> "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            If userWantsToPrintAReceipt Then

                ' DE7793 - 7/18/2012 Janet Robinson switch receipt to ssrs report
                PrintStudentReceipt(postPaymentInfo.PostPaymentId, postPaymentInfo.UniqueId)

                ''Added by Saraswathi lakshmanan to print the address of the student
                'get StudentAddress for this receipt
                ''Added on Sept 18 2009
                ''To fix mantis issue 17600: QA: There is a difference in the payment receipt on the post payments page and transaction search page.
                'Dim studentAddressInfo As StuAddressInfo = (New StdAddressFacade).GetDefaultStudentAddress(postPaymentInfo.PostPaymentId)

                'Dim recAddress As ReceiptAddress = GetAddressToBePrintedInReceipts(SingletonAppSettings.AppSettings("AddressToBePrintedInReceipts"))
                'create receipt xmlDoc
                'Dim receiptXmlDoc As XmlDocument = CreateReceiptXmlDoc(postPaymentInfo, recAddress, txtStudentName.Text, studentAddressInfo)
                'print the receipt
                'PrintAReceipt(receiptXmlDoc)


            End If

            '   If there are no errors bind an empty entity and init buttons
            If Not CommonWebUtilities.IsValidGuid(ViewState("TransactionId")) Then
                '   bind an empty new PostPaymentInfo
                BindSavePostPaymentData(New PostPaymentInfo)

                '' code Commented by kamalesh Ahuja on 06 April 2010 to resolve Mantis Issue id 14633
                'ddlDisbTypeId.SelectedIndex = 0
                'ddlDisbTypeId.Enabled = False
                'ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
                'ddlFundSourceId.Enabled = False
                ''''''''''''''''''''''''''''''''''''''''

                '   bind an empty Disbursements datagrid and hide it
                BindDataGrid()

                '   initialize buttons
                InitButtonsForLoad()
                'dgrdApplyPayment.DataSource = ""
                'dgrdApplyPayment.DataBind()
            Else
                With New StudentsAccountsFacade
                    BindSavePostPaymentData(.GetPostPaymentInfo(ViewState("TransactionId")))
                End With
                '   initialize buttons for edit
                InitButtonsForEdit()
                '   clear the apply payments datagrid
                'dgrdApplyPayment.DataSource = ""
                'dgrdApplyPayment.DataBind()
            End If
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControl As New SDFComponent
        Try
            SDFControl.DeleteSDFValue(txtPostPaymentId.Text)
            SDFID = SDFControl.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControl.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                SDFControl.InsertValues(txtPostPaymentId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '   if we are in a popUp window then close the window
        'If Not Header1.Visible Then
        '    CommonWebUtilities.CloseWindow(Page)
        'End If
    End Sub
    Protected Sub HdnSaveAndPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim saf As New StudentsAccountsFacade
        Dim result As String
        Dim DisbArrList As ArrayList = BuildDisbCollection()
        Dim arrApplyPmt As ArrayList = BuildAppliedPaymentCollection()
        Dim postPaymentInfo As PostPaymentInfo = BuildPostPaymentInfo(txtPostPaymentId.Text)
        result = saf.UpdatePostPaymentInfo(postPaymentInfo, Session("UserName"), DisbArrList, arrApplyPmt)
        userWantsToPrintAReceipt = True
        If result <> "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            If userWantsToPrintAReceipt Then

                ' DE7793 - 7/18/2012 Janet Robinson switch receipt to ssrs report
                PrintStudentReceipt(postPaymentInfo.PostPaymentId, postPaymentInfo.UniqueId)

                ''Added by Saraswathi lakshmanan to print the address of the student
                'get StudentAddress for this receipt
                ''Added on Sept 18 2009
                ''To fix mantis issue 17600: QA: There is a difference in the payment receipt on the post payments page and transaction search page.
                'Dim studentAddressInfo As StuAddressInfo = (New StdAddressFacade).GetDefaultStudentAddress(postPaymentInfo.PostPaymentId)

                'Dim recAddress As ReceiptAddress = GetAddressToBePrintedInReceipts(SingletonAppSettings.AppSettings("AddressToBePrintedInReceipts"))
                'create receipt xmlDoc
                'Dim receiptXmlDoc As XmlDocument = CreateReceiptXmlDoc(postPaymentInfo, recAddress, txtStudentName.Text, studentAddressInfo)
                'print the receipt
                'PrintAReceipt(receiptXmlDoc)


            End If

            '   If there are no errors bind an empty entity and init buttons
            If Not CommonWebUtilities.IsValidGuid(ViewState("TransactionId")) Then
                '   bind an empty new PostPaymentInfo
                BindSavePostPaymentData(New PostPaymentInfo)

                '' code Commented by kamalesh Ahuja on 06 April 2010 to resolve Mantis Issue id 14633
                'ddlDisbTypeId.SelectedIndex = 0
                'ddlDisbTypeId.Enabled = False
                'ddlFundSourceId.SelectedValue = System.Guid.Empty.ToString
                'ddlFundSourceId.Enabled = False
                ''''''''''''''''''''''''''''''''''''''''

                '   bind an empty Disbursements datagrid and hide it
                BindDataGrid()

                '   initialize buttons
                InitButtonsForLoad()
                'dgrdApplyPayment.DataSource = ""
                'dgrdApplyPayment.DataBind()
            Else
                With New StudentsAccountsFacade
                    BindSavePostPaymentData(.GetPostPaymentInfo(ViewState("TransactionId")))
                End With
                '   initialize buttons for edit
                InitButtonsForEdit()
                '   clear the apply payments datagrid
                'dgrdApplyPayment.DataSource = ""
                'dgrdApplyPayment.DataBind()
            End If
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControl As New SDFComponent
        Try
            SDFControl.DeleteSDFValue(txtPostPaymentId.Text)
            SDFID = SDFControl.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControl.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                SDFControl.InsertValues(txtPostPaymentId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '   if we are in a popUp window then close the window
        'If Not Header1.Visible Then
        '    CommonWebUtilities.CloseWindow(Page)
        'End If
    End Sub
End Class