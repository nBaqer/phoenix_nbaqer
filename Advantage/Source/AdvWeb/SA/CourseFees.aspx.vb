Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class CourseFees
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Customvalidator1 As System.Web.UI.WebControls.CustomValidator
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel
    Protected WithEvents dlstPrgVersionFees As System.Web.UI.WebControls.DataList
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected courseId As String
    Protected course As String
    Protected campusId As String
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        'Dim campusId As String
        Dim userId As String
        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        If Not Request.Item("CourseId") Is Nothing Then
            courseId = CType(Request.Item("CourseId"), String)
            course = Server.UrlDecode(CType(Request.Item("Course"), String))
            lblCourse.Text = course
        Else
            '   For testing purposes only
            courseId = "20ef2508-48a3-4c7b-8058-9b8279e1a65e"
            course = "Economics"
            lblCourse.Text = course
        End If

        If Not IsPostBack Then
            objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList()

            '   bind an empty fee
            BindNewFeeData()

            '   initialize buttons
            InitButtonsForLoad()
        Else
            objCommon.PageSetup(Form1, "EDIT")
        End If

    End Sub
    Private Sub BindDataList()

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind CourseFees datalist
        dlstCourseFees.DataSource = New DataView((New StudentsAccountsFacade).GetTransCodesByCourse(courseId).Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstCourseFees.DataBind()

    End Sub
    'Private Sub BuildDropDownLists()
    '    BuildStatusDDL()
    '    BuildTransCodesDDL()
    '    BuildTuitionCategoriesDDL()
    '    BuildRateSchedulesDDL()
    'End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = (New StatusesFacade).GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildTransCodesDDL()
        '   bind the TransCodes DDL
        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeId"
            .DataSource = (New StudentsAccountsFacade).GetAllTransCodes(False)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildTuitionCategoriesDDL()
        '   bind the TuitionCategories DDL
        With ddlTuitionCategoryId
            .DataTextField = "TuitionCategoryDescrip"
            .DataValueField = "TuitionCategoryId"
            .DataSource = (New StudentsAccountsFacade).GetAllTuitionCategories("All")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildRateSchedulesDDL()
        '   bind the RateSchedules DDL
        With ddlRateScheduleId
            .DataTextField = "RateScheduleDescrip"
            .DataValueField = "RateScheduleId"
            .DataSource = (New StudentsAccountsFacade).GetAllRateSchedules(False)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub dlstCourseFees_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstCourseFees.ItemCommand
        '   get the CourseFeeId from the backend and display it
        GetCourseFeeId(e.CommandArgument)

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstCourseFees, e.CommandArgument, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstCourseFees, e.CommandArgument)
        '   initialize buttons
        InitButtonsForEdit()

    End Sub

    Private Sub BindCourseFeeData(ByVal CourseFee As CourseFeeInfo)
        With CourseFee
            chkIsInDB.Checked = .IsInDB
            txtCourseFeeId.Text = .CourseFeeId
            txtCourseId.Text = .CourseId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .StatusId, .Status)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTransCodeId, .TransCodeId, .TransCode)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTuitionCategoryId, .TuitionCategoryId, .TuitionCategory)

            'If Not (CourseFee.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = CourseFee.StatusId
            'If Not (CourseFee.TransCodeId = Guid.Empty.ToString) Then ddlTransCodeId.SelectedValue = CourseFee.TransCodeId Else ddlTransCodeId.SelectedIndex = 0
            'If Not (CourseFee.TuitionCategoryId = Guid.Empty.ToString) Then ddlTuitionCategoryId.SelectedValue = CourseFee.TuitionCategoryId Else ddlTuitionCategoryId.SelectedIndex = 0

            If .Amount > 0.0 Then
                txtAmount.Text = .Amount.ToString("###,###.00")
                rbtFlatAmount.Checked = True
                ddlUnitId.SelectedValue = .UnitId
            Else
                txtAmount.Text = Decimal.Parse(0.0).ToString("###,###.00")
                rbtFlatAmount.Checked = False
                ddlUnitId.SelectedIndex = 0
            End If

            If Not (CourseFee.RateScheduleId = Guid.Empty.ToString) Then
                'ddlRateScheduleId.SelectedValue = CourseFee.RateScheduleId
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRateScheduleId, .RateScheduleId, .rateSchedule)
                rbtRateSchedule.Checked = True
            Else
                ddlRateScheduleId.SelectedIndex = 0
                rbtRateSchedule.Checked = False
            End If

            'If Not (CourseFee.StartDate = Date.MinValue) Then txtStartDate.Text = .StartDate.ToShortDateString Else txtStartDate.Text = ""
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With

    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   Check that user input is valid
        If Not IsUserInputDataValid() Then
            '   Display Error Message
            DisplayErrorMessage("Data entered is invalid." + vbCrLf + "You must select a Flat Amount or a Rate Schedule." + vbCrLf + "If you select a Flat Amount you must also enter an amount." + vbCrLf + "If you select the Rate Schedule you have to " + vbCrLf + "select also a Rate Schedule from the dropdownlist.")
            Exit Sub
        End If

        '   update CourseFee Info 
        Dim result As String
        result = (New StudentsAccountsFacade).UpdateCourseFeeInfo(BuildCourseFeeInfo(txtCourseFeeId.Text, courseId), Session("UserName"))

        If result <> "" Then
            '   Display Error Message
            DisplayErrorMessage(result)

        Else
            '   get the CourseFeeId from the backend and display it
            GetCourseFeeId(txtCourseFeeId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then
            '   bind the datalist
            BindDataList()

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstCourseFees, txtCourseFeeId.Text, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstCourseFees, txtCourseFeeId.Text)
            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new CourseFeeInfo
            'BindCourseFeeData(New CourseFeeInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If

    End Sub
    Private Function BuildCourseFeeInfo(ByVal courseFeeId As String, ByVal courseId As String) As CourseFeeInfo

        '   instantiate class
        Dim CourseFeeInfo As New CourseFeeInfo

        With CourseFeeInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get CourseFeeId
            .CourseFeeId = courseFeeId

            '   get CourseId
            .CourseId = courseId

            '   get StatusId
            .StatusId = ddlStatusId.SelectedValue

            '   get Trans. Code
            .TransCodeId = ddlTransCodeId.SelectedValue

            '   get TuitionCategoryId
            .TuitionCategoryId = ddlTuitionCategoryId.SelectedValue

            '   get Amount
            If rbtFlatAmount.Checked Then
                '   get Amount
                .Amount = Decimal.Parse(txtAmount.Text)
                '   get Unit
                .UnitId = ddlUnitId.SelectedValue
            Else
                .Amount = 0.0
            End If

            If rbtRateSchedule.Checked Then
                '   get RateScheduleId
                .RateScheduleId = ddlRateScheduleId.SelectedValue
            Else
                .RateScheduleId = Guid.Empty.ToString
            End If

            '   get StartDate
            .StartDate = Date.Parse(RadDatePicker1.SelectedDate)

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With

        '   return data
        Return CourseFeeInfo

    End Function
    Private Sub chkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged
        '   bind the datalist
        BindDataList()

        '   bind an empty new FeeInfo
        BindNewFeeData()

        '   initialize buttons
        InitButtonsForLoad()
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new CourseFeeInfo
        BindCourseFeeData(New CourseFeeInfo)

        '   Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstCourseFees, Guid.Empty.ToString, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstCourseFees, Guid.Empty.ToString)
        '   initialize buttons
        InitButtonsForLoad()

    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        If Not (txtCourseFeeId.Text = Guid.Empty.ToString) Then

            Dim result As String
            '   update CourseFee Info 
            result = (New StudentsAccountsFacade).DeleteCourseFeeInfo(txtCourseFeeId.Text, Date.Parse(txtModDate.Text))

            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else
                '   bind the datalist
                BindDataList()

                '   bind an empty new FeeInfo
                BindNewFeeData()

                '   initialize buttons
                InitButtonsForLoad()
            End If

        End If
    End Sub
    Private Sub InitButtonsForLoad()
        'btnNew.Enabled = True
        'btnDelete.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        'btnNew.Enabled = True
        'btnDelete.Enabled = True
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    End Sub
    Private Sub GetCourseFeeId(ByVal CourseFeeId As String)
        '   bind CourseFee properties
        BindCourseFeeData((New StudentsAccountsFacade).GetCourseFeeInfo(CourseFeeId))

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        ''   Set error condition
        'Customvalidator1.ErrorMessage = errorMessage
        'Customvalidator1.IsValid = False

        'If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
        '    '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        'End If

    End Sub
    Private Sub BindNewFeeData()

        '   bind an empty new CourseFeeInfo
        BindCourseFeeData(New CourseFeeInfo)

    End Sub
    Private Sub ddlFeeTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '   bind datalist
        BindDataList()

        '   bind an empty fee
        BindNewFeeData()

        '   initialize buttons
        InitButtonsForLoad()
    End Sub
    Private Function IsUserInputDataValid() As Boolean
        If rbtFlatAmount.Checked And Not rbtRateSchedule.Checked Then
            If Not IsTextBoxValidDecimal(txtAmount) Then Return False
            If IsFlatAmountZero() Then Return False
            'If Not ddlRateScheduleId.SelectedIndex = 0 Then Return False
            ddlRateScheduleId.SelectedIndex = 0
        End If
        If rbtRateSchedule.Checked And Not rbtFlatAmount.Checked Then
            If ddlRateScheduleId.SelectedIndex = 0 Then Return False
            'If IsTextBoxValidDecimal(txtAmount) Then Return False
            'If Not IsFlatAmountZero() Then Return False
            txtAmount.Text = "0.00"
        End If
        If (rbtRateSchedule.Checked And rbtFlatAmount.Checked) Or (Not rbtRateSchedule.Checked And Not rbtFlatAmount.Checked) Then Return False
        Return True
    End Function
    Private Function IsTextBoxValidDecimal(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        Try
            Dim d As Decimal = Decimal.Parse(textbox.Text)
            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function IsFlatAmountZero() As Boolean
        If Decimal.Parse(txtAmount.Text) = 0.0 Then Return True Else Return False
    End Function
    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList()
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender 
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList() 
        'add save button 
        controlsToIgnore.Add(btnSave)
       'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        '        Dim SchoolItem As String

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlTransCodeId, AdvantageDropDownListName.Trans_Codes, campusId, True, True, Guid.Empty.ToString))

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, False, False, String.Empty))

        ddlList.Add(New AdvantageDDLDefinition(ddlTuitionCategoryId, AdvantageDropDownListName.Tuition_Cats, campusId, True, True, Guid.Empty.ToString))

        ddlList.Add(New AdvantageDDLDefinition(ddlRateScheduleId, AdvantageDropDownListName.Rate_Schedules, campusId, True, True, Guid.Empty.ToString))


        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub
End Class
