Imports Fame.Common
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

' ===============================================================================
'
' FAME AdvantageV1
'
' BankAccounts.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.

Partial Class BankAccounts
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtRoutingNumber As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkIsDefaultBankAcct As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Panel4 As System.Web.UI.WebControls.Panel
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    'Protected WithEvents Header1 As Header
    'Protected WithEvents footer1 As Footer

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Private bankId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim objCommon As New CommonUtilities
        Dim campusId As String
        Dim userId As String
        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        ''   set Footer and Header to visible=false
        'Header1.Visible = False
        'footer1.Visible = False

        If Not IsPostBack Then
            objCommon.PageSetup(form1, "NEW")
            'objCommon.PopulatePage(form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(form1, "NEW", pObj)
            'ViewState("MODE") = "NEW"

            '   get the bankId from the Querystring
            If CommonWebUtilities.IsValidGuid(Request.QueryString("bankId")) Then
                ViewState("bankId") = CType(Request.QueryString("bankId"), String)
                lblBank.Text = (New StudentsAccountsFacade).GetBankInfo(ViewState("bankId")).Name
            Else
                '   this bankAcctId is only for testing
                ViewState("bankId") = "457937C1-9747-4D18-BB1C-7CFB22145002"
                lblBank.Text = "Test Bank - only for testing"
            End If

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            Dim boolStatus As String
            If radStatus.SelectedItem.Text = "Active" Then
                boolStatus = "True"
            ElseIf radStatus.SelectedItem.Text = "Inactive" Then
                boolStatus = "False"
            Else
                boolStatus = "All"
            End If

            BindDataList(ViewState("bankId"), boolStatus)

            '   bind an empty new BankAcctInfo
            BindBankAcctData(New BankAcctInfo)

            '   initialize buttons
            InitButtonsForLoad()

            '   save url of referrer
            If Not Request.UrlReferrer Is Nothing Then
                ' Store URL Referrer to return to portal
                ViewState("UrlReferrer") = Request.UrlReferrer.ToString()
            Else
                btnBack.Visible = False
            End If
        Else
            objCommon.PageSetup(form1, "EDIT")
        End If

    End Sub
    Private Sub BindDataList(ByVal bankId As String, ByVal showActiveOnly As String)

        '   bind BankCodes datalist
        With New StudentsAccountsFacade
            dlstBankAccts.DataSource = .GetBankAccounts(bankId, showActiveOnly)
            dlstBankAccts.DataBind()
        End With

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BindBankAcctData(ByVal bankAcct As BankAcctInfo)
        With bankAcct
            chkIsInDB.Checked = .IsInDB
            txtBankId.Text = .BankId
            txtBankAcctId.Text = .BankAcctId
            txtBankAcctDescrip.Text = .BankAcctDescrip
            If Not (bankAcct.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = bankAcct.StatusId
            txtBankAcctNumber.Text = .BankAcctNumber
            txtBankRoutingNumber.Text = .BankRoutingNumber
            'If Not (bankAcct.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = bankAcct.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            ddlCampGrpId.SelectedValue = bankAcct.CampGrpId
            chkIsDefaultBankAcct.Checked = .IsDefaultBankAcct
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub dlstBankAccts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstBankAccts.ItemCommand

        '   get the BankAcct from the backend and display it
        Dim strId As String = dlstBankAccts.DataKeys(e.Item.ItemIndex).ToString()
        GetBankAcctId(strId)

        '    '   set Style to Selected Item
        '  CommonWebUtilities.SetStyleToSelectedItem(dlstBankAccts, e.CommandArgument, ViewState)

        '   initialize buttons
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlstBankAccts, e.CommandArgument)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim result As String
        With New StudentsAccountsFacade
            '   update bank Info 
            result = .UpdateBankAcctInfo(BuildBankAcctInfo(txtBankAcctId.Text, ViewState("bankId")), Session("UserName"))
        End With

        '   bind the datalist
        Dim boolStatus As String
        If radStatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If

        BindDataList(ViewState("bankId"), boolStatus)

        '   set Style to SelectedItem
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstBankAccts, txtBankAcctId.Text, ViewState)

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the BankCode from the backend and display it
            GetBankAcctId(txtBankAcctId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new BankAcctInfo
            'BindBankAcctData(New BankAcctInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If
        CommonWebUtilities.RestoreItemValues(dlstBankAccts, txtBankAcctId.Text)
    End Sub
    Private Function BuildBankAcctInfo(ByVal bankAcctId As String, ByVal bankId As String) As BankAcctInfo

        '   instantiate class
        Dim bankAcctInfo As New bankAcctInfo

        With bankAcctInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get bankAcctId
            If Not (bankAcctId = Guid.Empty.ToString) Then .BankAcctId = txtBankAcctId.Text

            '   get bankId
            .BankId = bankId

            '   get StatusId
            .StatusId = ddlStatusId.SelectedValue

            '   get bank account descrip
            .BankAcctDescrip = txtBankAcctDescrip.Text.Trim

            '   get bank account number
            .BankAcctNumber = txtBankAcctNumber.Text.Trim

            '   get Routing Number
            .BankRoutingNumber = txtBankRoutingNumber.Text.Trim

            '   get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            '   get IsDefaultBankAcct
            .IsDefaultBankAcct = chkIsDefaultBankAcct.Checked

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)
        End With

        '   return data
        Return bankAcctInfo

    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new BankInfo
        BindBankAcctData(New BankAcctInfo)

        '   Reset Style in the Datalist
        '  CommonWebUtilities.SetStyleToSelectedItem(dlstBankAccts, Guid.Empty.ToString, ViewState)

        '   initialize buttons
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlstBankAccts, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtBankAcctId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim saf As New StudentsAccountsFacade

            '   update bank Info 
            Dim result As String = saf.DeleteBankAcctInfo(txtBankAcctId.Text, Date.Parse(txtModDate.Text))
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else
                '   bind datalist
                Dim boolStatus As String
                If radStatus.SelectedItem.Text = "Active" Then
                    boolStatus = "True"
                ElseIf radStatus.SelectedItem.Text = "Inactive" Then
                    boolStatus = "False"
                Else
                    boolStatus = "All"
                End If

                BindDataList(ViewState("bankId"), boolStatus)

                '   bind an empty new BankAcctInfo
                BindBankAcctData(New BankAcctInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If

        End If
        CommonWebUtilities.RestoreItemValues(dlstBankAccts, Guid.Empty.ToString)
    End Sub
    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '   bind the datalist
        '   bind datalist
        Dim boolStatus As String
        If radStatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If

        BindDataList(ViewState("bankId"), boolStatus)

    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If
    End Sub
    Private Sub GetBankAcctId(ByVal bankAcctId As String)
        '   Create a StudentsFacade Instance
        Dim saf As New StudentsAccountsFacade

        '   bind BankAcct properties
        BindBankAcctData(saf.GetBankAcctInfo(bankAcctId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        Dim boolStatus As String
        If radStatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If

        BindDataList(ViewState("bankId"), boolStatus)
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        ' Redirect back to the portal home page
        Response.Redirect(CType(ViewState("UrlReferrer"), String))
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button
        controlsToIgnore.Add(btnSave)

        'Add javascript code to warn the user about non saved changes
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
