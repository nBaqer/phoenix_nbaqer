Imports FluentNHibernate.Conventions.AcceptanceCriteria
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports System.Data
Imports System.Globalization
Imports Telerik.Web.UI
Imports FAME.Advantage.Api.Library.Models

Partial Class BillingMethods
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As Label
    Protected WithEvents btnhistory As Button

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Page Properties"
    Protected ResourceId As String
    Protected ModuleId As String
    Protected campusId, userId As String
    Private pObj As New UserPagePermissionInfo
    Public nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat
    Public Const MaxHours As String = "5000.00"
    Public Const MaxCredits As String = "500.00"
    Public Const MinIncrement As String = "0.01"
    Public Const MsgBillingMethodApplyClockHours = "Charges will be applied to active students in the selected Program Version tracking the attendance type of Clock Hour. "
    Public Const MsgBillingMethodApplyCredits = "Charges will be applied to active students in the selected Program Version tracking Credits. "
    Public Const MsgBillingMethodApply = "Program Versions that have charges applied cannot be removed. "
    Private Property BillingMethod() As BillingMethodInfo
        'Get
        '    If (Session("BillingMethodInfo") Is Nothing) Then
        '        Return New BillingMethodInfo()
        '    Else
        '        Return DirectCast(Session("BillingMethodInfo"), BillingMethodInfo)
        '    End If
        'End Get
        'Set(value As BillingMethodInfo)
        '    Session("BillingMethodInfo") = value
        'End Set
        Get
            If (ViewState("BillingMethodInfo") Is Nothing) Then
                Return New BillingMethodInfo()
            Else
                Return DirectCast(ViewState("BillingMethodInfo"), BillingMethodInfo)
            End If
        End Get
        Set(value As BillingMethodInfo)
            ViewState.Add("BillingMethodInfo", value)
        End Set
    End Property
    Private Property Increment() As IncrementInfo
        'Get
        '    If (Session("IncrementInfo") Is Nothing) Then
        '        Return New IncrementInfo()
        '    Else
        '        Return DirectCast(Session("IncrementInfo"), IncrementInfo)
        '    End If
        'End Get
        'Set(value As IncrementInfo)
        '    Session("IncrementInfo") = value
        'End Set
        Get
            If (ViewState("IncrementInfo") Is Nothing) Then
                Return New IncrementInfo()
            Else
                Return DirectCast(ViewState("IncrementInfo"), IncrementInfo)
            End If
        End Get
        Set(value As IncrementInfo)
            ViewState.Add("IncrementInfo", value)
        End Set
    End Property
    Private Property TotalPaymentPeriods As Integer
        'Get
        '    If (Me.Session("TotalPaymentPeriods") Is Nothing) Then
        '        Return 0
        '    Else
        '        Return DirectCast(Session("TotalPaymentPeriods"), Integer)
        '    End If
        'End Get
        'Set(value As Integer)
        '    Session("TotalPaymentPeriods") = value
        'End Set
        Get
            If (ViewState("TotalPaymentPeriods") Is Nothing) Then
                Return 0
            Else
                Return DirectCast(ViewState("TotalPaymentPeriods"), Integer)
            End If
        End Get
        Set(value As Integer)
            ViewState.Add("TotalPaymentPeriods", value)
        End Set
    End Property
    Private Property TotIncreValue() As TotalIncrementValue
        Get
            If (ViewState("TotIncreValue") Is Nothing) Then
                Return New TotalIncrementValue()
            Else
                Return DirectCast(ViewState("TotIncreValue"), TotalIncrementValue)
            End If
        End Get
        Set(value As TotalIncrementValue)
            ViewState.Add("TotIncreValue", value)
        End Set
    End Property
    Private Property CurrentTabIndex As Integer
        'Get
        '    If (Me.Session("CurrentTabIndex") Is Nothing) Then
        '        Return 0
        '    Else
        '        Return DirectCast(Session("CurrentTabIndex"), Integer)
        '    End If
        'End Get
        'Set(value As Integer)
        '    Session("CurrentTabIndex") = value
        'End Set
        Get
            If (ViewState("CurrentTabIndex") Is Nothing) Then
                Return 0
            Else
                Return DirectCast(ViewState("CurrentTabIndex"), Integer)
            End If
        End Get
        Set(value As Integer)
            ViewState.Add("CurrentTabIndex", value)
        End Set
    End Property
    Private Property TabFlag As Boolean
        Get
            If (ViewState("TabFlag") Is Nothing) Then
                Return False
            Else
                Return DirectCast(ViewState("TabFlag"), Boolean)
            End If
        End Get
        Set(value As Boolean)
            ViewState.Add("TabFlag", value)
        End Set
    End Property
    Private Property SaveFlag As Boolean
        Get
            If (ViewState("SaveFlag") Is Nothing) Then
                Return False
            Else
                Return DirectCast(ViewState("SaveFlag"), Boolean)
            End If
        End Get
        Set(value As Boolean)
            ViewState.Add("SaveFlag", value)
        End Set
    End Property
#End Region
#Region "Page "
    'Protected Sub Page_PreInit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreInit
    '    ' Dim customCulture As CultureInfo = New CultureInfo(CurrentCulture.Clone())

    '    Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat
    '    nfi.PercentDecimalDigits = 2
    '    nfi.CurrencyDecimalDigits = 2
    'End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade
        Dim advantageUserState As New User()


        advantageUserState = AdvantageSession.UserState
        ResourceId = HttpContext.Current.Request.Params("resid").ToString()

        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()
            SetTabAndPage()

            MoveToP0(String.Empty)

            'InitButtonsForLoad()
            CommonWebUtilities.RestoreItemValues(dlstBillingMethods, "0")
        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)

        End If
        btnsave.Enabled = False
    End Sub
    Private Sub BindDataList()
        Dim boolStatus As String
        If radstatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radstatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If
        '   bind BillingMethods datalist
        With New StudentsAccountsFacade
            dlstBillingMethods.DataSource = .GetAllBillingMethods(boolStatus)
            dlstBillingMethods.DataBind()
        End With
    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
        BuildBillingMethodDDL()
        BuildIncrementTypeDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildBillingMethodDDL()
        Dim itemsA(3) As ListItem
        itemsA(0) = New ListItem(Utilities.EnumDescription(FAME.AdvantageV1.Common.BillingMethod.ProgramVersion), "0")
        itemsA(1) = New ListItem(Utilities.EnumDescription(FAME.AdvantageV1.Common.BillingMethod.Course), "1")
        itemsA(2) = New ListItem(Utilities.EnumDescription(FAME.AdvantageV1.Common.BillingMethod.Term), "2")
        'check for clock or credit hour schools.
        itemsA(3) = New ListItem(Utilities.EnumDescription(FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod), "3")

        ddlBillingMethod.Items.AddRange(itemsA)
        ddlBillingMethod.DataBind()
        ddlBillingMethod.SelectedIndex = 0 'ProgramVersion
    End Sub
    Private Sub BuildIncrementTypeDDL()
        Dim itemsA(3) As ListItem
        itemsA(0) = New ListItem(Utilities.EnumDescription(IncrementType.ActualHours), "0")
        itemsA(1) = New ListItem(Utilities.EnumDescription(IncrementType.ScheduledHours), "1")
        itemsA(2) = New ListItem(Utilities.EnumDescription(IncrementType.CreditsAttempted), "2")
        itemsA(3) = New ListItem(Utilities.EnumDescription(IncrementType.CreditsEarned), "3")

        ddlIncrementType.Items.AddRange(itemsA)
        ddlIncrementType.DataBind()
        ddlIncrementType.Items.Insert(0, New ListItem("Select", "-1"))
        ddlIncrementType.SelectedIndex = 0 ' Select 
    End Sub
    Private Sub GetBillingMethodAndIncrement(ByVal BillingMentodId As String)
        GetBillingMethod(BillingMentodId)
        GetIncrement(BillingMethod.BillingMethodId)
        PaymentPeriodControl(BillingMethod.BillingMethod)
        BuildGridAPV()
    End Sub
    Private Sub GetBillingMethod(ByVal BillingMethodId As String)
        '   Create a StudentsFacade Instance
        Dim saf As New StudentsAccountsFacade
        If (BillingMethodId = String.Empty) Then
            BillingMethod = New BillingMethodInfo()
        Else
            '   bind BillingMethod properties
            BillingMethod = saf.GetBillingMethodInfo(BillingMethodId)
        End If
        BindBillingMethodData()
    End Sub
    Private Sub GetIncrement(ByVal BillingMethodId As String)
        '   Create a StudentsFacade Instance
        Dim saf As New StudentsAccountsFacade
        If Not (BillingMethod.BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then     '3 --> Payment Period
            '   bind BillingMethod properties
            Increment = New IncrementInfo()
        Else
            Increment = saf.GetIncrementInfo(BillingMethodId)
            'Recalculate all periods CumulateValues (Hours Vs Credits)
            RecalculateCumulativeValues()
        End If
        BindIncrementData()
    End Sub
    Private Sub BindBillingMethodData()
        With BillingMethod
            chkIsInDB.Checked = .IsInDB
            chkBMInUse.Checked = .BillingMethodInUse
            chkITInUse.Checked = .IncrementTypeInUse
            txtBillingMethodId.Text = .BillingMethodId
            txtBillingMethodCode.Text = .Code
            If Not (.StatusId = Guid.Empty.ToString) Then
                ddlStatusId.SelectedValue = .StatusId
            End If
            txtBillingMethodDescrip.Text = .Description
            If Not (.CampGrpId = Guid.Empty.ToString) Then
                ddlCampGrpId.SelectedValue = .CampGrpId
            Else
                ddlCampGrpId.SelectedIndex = 0
            End If
            ddlBillingMethod.SelectedValue = .BillingMethod

            If (.BillingMethodInUse) Then
                ddlBillingMethod.Enabled = False
                txtMessage.Text = "Note: System charge code "
                If .BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod Then
                    txtMessage.Text &= "and Increment type "
                End If
                txtMessage.Text &= "cannot be modified as the selected charging method has already been assigned to a program version."
                txtMessage.Visible = True
                chkAutoBill.Enabled = False
            Else
                ddlBillingMethod.Enabled = True
                txtMessage.Text = ""
                txtMessage.Visible = False
                chkAutoBill.Enabled = True
                'Set autobill Checkbox
                SetAutoBillRules(.BillingMethod, .AutoBill)
            End If
            If (.IncrementTypeInUse) Then
                ddlIncrementType.Enabled = False
            Else
                ddlIncrementType.Enabled = True
            End If

            '   if Term is selected then AUtobill should be disabled
            'ddlBillingMethod_SelectedIndexChanged(Me, New System.EventArgs)

            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString

        End With
    End Sub
    Private Sub BindIncrementData()
        With Increment
            ' IncrementType
            chkIIsInDB.Checked = .IsInDB
            txtIncrementId.Text = .IncrementId

            If (BillingMethod.BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then '3 --> Payment Period
                txtIncrementId.Text = .IncrementId
                ddlIncrementType.SelectedValue = .IncrementType
                txtEffectiveDate.SelectedDate = .EffectiveDate
                ' txtExcAbsencesPercent.Text = .ExcAbsencesPercent.ToString("P", nfi)
                ' txtExcAbsencesPercent.Text = String.Format("{0:0.00%}", .ExcAbsencesPercent)
                txtExcAbsencesPercent.Text = String.Format("{0:P2}", .ExcAbsencesPercent)

            Else
                txtIncrementId.Text = String.Empty
                ddlIncrementType.SelectedValue = "-1"
                txtEffectiveDate.SelectedDate = DateTime.Today
                'txtExcAbsencesPercent.Text = (0.0).ToString("P", nfi)
                txtExcAbsencesPercent.Text = String.Format("{0:P2}", 0.0)
            End If
            txtIModUser.Text = .ModUser
            txtIModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Function SaveBillingMethod() As Boolean
        Dim SaveResult As Boolean = False
        '   instantiate component
        Dim saf As New StudentsAccountsFacade
        '   update BillingMethod Info 
        BuildBillingMethodInfo()
        With BillingMethod
            Dim result As String = saf.UpdateBillingMethodInfo(BillingMethod, Session("UserName").ToString())
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                SaveResult = False
            Else
                .IsInDB = True
                If (.BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then
                    'AndAlso (chkIIsInDB.Checked = True) Then

                    'update Increment Info

                    BuildIncrementInfo()

                    'Update Increment 
                    result = saf.UpdateIncrementInfo(Increment, Session("UserName").ToString())
                    '   get the BillingMethodId from the backend and display it
                    If result <> "" Then
                        '   Display Error Message
                        DisplayErrorMessage(result)
                        SaveResult = False
                    Else
                        Increment.IsInDB = True
                        SaveResult = True
                    End If
                Else
                    'Delete Increment and Periods record if exist 
                    'in case of change of IncrmentType form Payment periods to other one
                    SaveResult = True
                End If

            End If
        End With
        Return SaveResult
    End Function
    Private Sub BuildBillingMethodInfo()
        With BillingMethod
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked
            .BillingMethodInUse = chkBMInUse.Checked
            .IncrementTypeInUse = chkITInUse.Checked
            '   get BillingMethodId
            .BillingMethodId = txtBillingMethodId.Text

            '   get Code
            .Code = txtBillingMethodCode.Text.Trim

            '   get StatusId
            .StatusId = ddlStatusId.SelectedValue

            '   get BillingMethod's name
            .Description = txtBillingMethodDescrip.Text.Trim

            '   get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            '   get Billing Method
            .BillingMethod = CType(ddlBillingMethod.SelectedValue, BillingMethod)

            '   get Auto Bill
            .AutoBill = chkAutoBill.Checked

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
    End Sub
    Private Sub BuildIncrementInfo()
        '   instantiate class
        'Dim IncrementInfo As New IncrementInfo
        With Increment
            .IsInDB = chkIIsInDB.Checked
            .IncrementId = txtIncrementId.Text '
            .BillingMethodId = BillingMethod.BillingMethodId
            .IncrementType = CType(ddlIncrementType.SelectedValue, IncrementType)
            '.IncrementName  is read only -- it depend of IncrementType change
            .EffectiveDate = CType(txtEffectiveDate.SelectedDate, Date)
            Dim ExcAbscencesPercet As Decimal = Decimal.Parse(txtExcAbsencesPercent.Text.Replace("%", ""))
            If (ExcAbscencesPercet <> 0D) Then      ' 0.0 = 0D (decimal)
                .ExcAbsencesPercent = ExcAbscencesPercet / 100
            Else
                .ExcAbsencesPercent = 0D           ' 0.0 = 0D (decimal)
            End If
            .ModUser = txtIModUser.Text
            .ModDate = Date.Parse(txtIModDate.Text)
        End With
    End Sub
#End Region
#Region "Page Butons"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click

        'Select Case RadMultiPage.SelectedIndex
        '    Case "0"
        '        If (SaveP0() = True) Then
        '            MoveToP1()
        '        Else
        '            MoveToP0(BillingMethod.BillingMethodId)
        '            TabFlag = False
        '        End If
        '        Exit Select
        '    Case "1"
        '        If (SaveP1() = True) Then
        '            MoveToP2()
        '        Else
        '            MoveToP1()
        '        End If
        '        Exit Select
        '    Case "2"
        '        If (SaveP2() = True) Then
        '            MoveToP0(BillingMethod.BillingMethodId)
        '            TabFlag = False
        '        Else
        '            MoveToP2()
        '        End If
        '        Exit Select
        'End Select
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnnew.Click
        MoveToP0(String.Empty)
        CommonWebUtilities.RestoreItemValues(dlstBillingMethods, "0")
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btndelete.Click
        Dim result As String
        With BillingMethod
            If Not (.BillingMethodId = Guid.Empty.ToString) Then
                '   instantiate component
                Dim saf As New StudentsAccountsFacade
                ' Validate if the record could be deleted
                Dim ExistPrgVersionsWithGivenBillingMethodId As Boolean
                ExistPrgVersionsWithGivenBillingMethodId = saf.CheckIfPrgVersionsHaveABillingMethodId(.BillingMethodId)
                If (ExistPrgVersionsWithGivenBillingMethodId) Then
                    ' show message
                    Dim Message As String
                    Message = "Note: System charge code "
                    If (.BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then
                        Message &= "and Increment type "
                    End If
                    Message &= "cannot be deleted as the selected charging method has already been assigned to a program version."
                    DisplayErrorMessage(Message)
                    Return
                End If
                'Delete Periods and IncrementType
                If (DeletePeriodsAndIncrementType() = False) Then
                    Return
                End If
                '   Delete BillingMethod Info 
                result = saf.DeleteBillingMethodInfo(.BillingMethodId, Date.Parse(txtModDate.Text))
                If (result <> String.Empty) Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                End If
                MoveToP0(String.Empty)
                InitButtonsForLoad()
                CommonWebUtilities.RestoreItemValues(dlstBillingMethods, "0")
            End If
            CommonWebUtilities.RestoreItemValues(dlstBillingMethods, Guid.Empty.ToString)
        End With
    End Sub
    Private Function DeletePeriodsAndIncrementType() As Boolean
        Dim WasDeleteOK As Boolean = True
        Dim result As String
        Dim saf As New StudentsAccountsFacade
        If (TotalPaymentPeriods > 0) Then
            If (saf.BillingMethodCheckForChargedPaymentPeriods(BillingMethod.BillingMethodId)) Then
                'error deleting PmtPeriods
                DisplayErrorMessage("Current charging method could not be deleted because have Payment Periods already used")
                WasDeleteOK = False
                Return WasDeleteOK
            Else
                'borrar Payment Periods
                result = saf.DeletePmtPeriodsForAIncrementId(Increment.IncrementId)
                If Not (result = String.Empty) Then
                    'error deleting PmtPeriods
                    DisplayErrorMessage(result)
                    WasDeleteOK = False
                    Return WasDeleteOK
                Else
                    TotalPaymentPeriods = 0
                    TotIncreValue = New TotalIncrementValue()
                End If
            End If
        End If
        If (Increment.IsInDB = True) Then
            'Delete  Increment
            result = saf.DeleteIncrementInfo(Increment.BillingMethodId, Increment.ModDate)
            If (result <> String.Empty) Then
                '  Display Error Message
                DisplayErrorMessage(result)
                WasDeleteOK = False
            Else
                WasDeleteOK = True
            End If
        End If
        Return WasDeleteOK
    End Function
#End Region
#Region "Page Events"
    Private Sub dlstBillingMethods_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstBillingMethods.ItemCommand
        '   get the BillingMethodId from the backend and display it
        Dim strId As String = dlstBillingMethods.DataKeys(e.Item.ItemIndex).ToString()
        CommonWebUtilities.RestoreItemValues(dlstBillingMethods, strId)

        TabFlag = False
        SaveFlag = False
        RadTabStrip.SelectedIndex = 0
        MoveToP0(strId)
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlstBillingMethods, BillingMethod.BillingMethodId)
        '   set Style to Selected Item
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstBillingMethods, e.CommandArgument, ViewState, Header1)
    End Sub
    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objCommon As New CommonUtilities
        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
        '   bind datalist
        MoveToP0(BillingMethod.BillingMethodId)
        CommonWebUtilities.RestoreItemValues(dlstBillingMethods, BillingMethod.BillingMethodId)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = False
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = False
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End If
    End Sub
#End Region
#Region " "
    Private Sub ddlBillingMethod_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBillingMethod.SelectedIndexChanged
        If (TotalPaymentPeriods > 0) Then
            ' Message that is going to delete Payment Periods already created
            Const Message As String = "The previous System Charge Code was 'Payment Period' with already periods defined. You should delete them before relocate the System Charge Code. "
            DisplayErrorMessage(Message)
            ddlBillingMethod.SelectedValue = BillingMethod.BillingMethod
            Return
        End If
        BillingMethod.BillingMethod = CType(ddlBillingMethod.SelectedValue, BillingMethod)
        If (BillingMethod.BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then

        End If
        SetIncrementFields(CType(ddlBillingMethod.SelectedValue, BillingMethod))
        SetTabAndPage()
        BindIncrementData()
        PaymentPeriodControl(BillingMethod.BillingMethod)
        BuildGridAPV()
        SyncroTabAndPage(0)
    End Sub
    Private Sub ddlIncrementType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlIncrementType.SelectedIndexChanged
        If (SaveP0() = True) Then
            MoveToP0(BillingMethod.BillingMethodId)
            TabFlag = False
        End If
    End Sub
    Private Sub SetIncrementFields(ByVal pBillingMethod As BillingMethod)
        'AutoBill
        SetAutoBillRules(pBillingMethod, True)
        ' Increment Type

        If (pBillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then 'Payment Period
            btnSaveP0NextToP1.Text = "Save & Continue"
            lblIncrementType.Visible = True
            ddlIncrementType.Visible = True
            chkAutoBill.Visible = True
            If (chkBMInUse.Checked) Then
                ddlIncrementType.Enabled = False
            Else
                ddlIncrementType.Enabled = True
            End If
            txtMessageApplyPrgVersion.Visible = True
        Else
            btnSaveP0NextToP1.Text = "Save"
            lblIncrementType.Visible = False
            ddlIncrementType.Visible = False
            ddlIncrementType.Enabled = False
            ddlIncrementType.SelectedValue = FAME.AdvantageV1.Common.IncrementType.NotApply
            chkAutoBill.Visible = False
            txtMessageApplyPrgVersion.Visible = False
        End If
    End Sub
    Private Sub SetAutoBillRules(ByVal pBillingMethod As FAME.AdvantageV1.Common.BillingMethod, ByVal AutoBill As Boolean)
        Select Case (Integer.Parse(pBillingMethod))
            Case (FAME.AdvantageV1.Common.BillingMethod.ProgramVersion)     '0 --> Program Version
                chkAutoBill.Checked = False
                chkAutoBill.Enabled = True
                Exit Select
            Case (FAME.AdvantageV1.Common.BillingMethod.Course)             '1 --> Course 
                chkAutoBill.Checked = False
                chkAutoBill.Enabled = True
                Exit Select
            Case (FAME.AdvantageV1.Common.BillingMethod.Term)               '2 --> Term
                chkAutoBill.Checked = False
                chkAutoBill.Enabled = False
                Exit Select
            Case (FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod)      '3 --> Payment Period
                chkAutoBill.Checked = AutoBill
                chkAutoBill.Enabled = False
                Exit Select
            Case (FAME.AdvantageV1.Common.BillingMethod.AcademicYear)       '4 --> Academic Year 
                chkAutoBill.Checked = False
                chkAutoBill.Enabled = False
                Exit Select
            Case Else                                                       ' Other case
                chkAutoBill.Checked = False
                chkAutoBill.Enabled = False
                Exit Select
        End Select
        If (chkBMInUse.Checked) Then
            chkAutoBill.Enabled = False
        End If
    End Sub
    Private Sub SetExcAbscene()
        If (ddlIncrementType.SelectedValue > IncrementType.NotApply) Then
            If ddlIncrementType.SelectedValue = IncrementType.CreditsEarned _
            Or ddlIncrementType.SelectedValue = IncrementType.CreditsAttempted Then
                lblExcAbsencesPercent.Visible = False
                txtExcAbsencesPercent.Visible = False
                txtExcAbsencesPercent.Text = String.Format("{0:P2}", 0.0)
            Else
                lblExcAbsencesPercent.Visible = True
                txtExcAbsencesPercent.Visible = True
                'txtExcAbsencesPercent.Text = String.Format("{0:P2}", 1.0)
            End If
        End If
    End Sub
    Private Sub SetlblMessageApplyPrgVersion()
        Select Case (Integer.Parse(Increment.IncrementType))
            Case (IncrementType.NotApply)
                txtMessageApplyPrgVersion.Visible = False
                txtMessageApplyPrgVersion.Text = String.Empty
                Exit Select
            Case (IncrementType.ActualHours)
                txtMessageApplyPrgVersion.Visible = True
                txtMessageApplyPrgVersion.Text = MsgBillingMethodApplyClockHours + MsgBillingMethodApply
                Exit Select
            Case (IncrementType.ScheduledHours)
                txtMessageApplyPrgVersion.Visible = True
                txtMessageApplyPrgVersion.Text = MsgBillingMethodApplyClockHours + MsgBillingMethodApply
                Exit Select
            Case (IncrementType.CreditsAttempted)
                txtMessageApplyPrgVersion.Visible = True
                txtMessageApplyPrgVersion.Text = MsgBillingMethodApplyCredits + MsgBillingMethodApply
                Exit Select
            Case (IncrementType.CreditsEarned)
                txtMessageApplyPrgVersion.Visible = True
                txtMessageApplyPrgVersion.Text = MsgBillingMethodApplyCredits + MsgBillingMethodApply
                Exit Select
            Case Else
                txtMessageApplyPrgVersion.Visible = False
                txtMessageApplyPrgVersion.Text = String.Empty
                Exit Select
        End Select
    End Sub
    Private Sub PaymentPeriodControl(ByVal pBillingMethod As FAME.AdvantageV1.Common.BillingMethod)
        radGridPPI.MasterTableView.IsItemInserted = False
        radGridPPI.MasterTableView.ClearEditItems()
        'Div divPaymentPeriods
        If (pBillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then       '3 --> Payment Period
            ' Load  PmtPeriodIncrements
            BuildGridPPI()
            radGridPPI.MasterTableView.Rebind()
        Else
            '
            radGridPPI.DataSource = Nothing
            TotalPaymentPeriods = 0
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub
#End Region
#Region "PageView Periods and GridPPI  "
    Private Sub BuildGridPPI()
        Dim saf As New StudentsAccountsFacade
        Dim ds As DataSet
        Dim dt As DataTable
        Dim Total As Double
        Try
            ds = saf.GetPmtPeriods(Increment.IncrementId)
            If Not (ds.Tables(0) Is Nothing) Then
                dt = ds.Tables(0)
                TotalPaymentPeriods = dt.Rows.Count()
                TotIncreValue = New TotalIncrementValue()
                TotIncreValue.Total.Add(0.0)
                If (TotalPaymentPeriods > 0) Then
                    For Period As Integer = 1 To TotalPaymentPeriods
                        Total = Convert.ToDouble(dt.Rows(Period - 1).Item("CumulativeValue"))
                        TotIncreValue.Total.Add(Total)
                    Next
                End If
                radGridPPI.DataSource = ds.Tables(0)
            Else
                TotalPaymentPeriods = 0
                TotIncreValue.Total.Add(0.0)
                radGridPPI.DataSource = Nothing
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage(" Error with Dataset")
            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in dataset" & ex.Message & " "
            Else
                Session("Error") = "Error in dataset" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
            Exit Sub
        End Try
    End Sub
    Protected Sub radGridPPI_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles radGridPPI.NeedDataSource
        BuildGridPPI()

    End Sub
    Protected Sub radGridPPI_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles radGridPPI.ItemDataBound
        'if is render the header  Get name for ValueName

        If (TypeOf e.Item Is GridHeaderItem) Then
            DirectCast(e.Item.FindControl("lblHeaderIncrement"), Label).Text = ddlIncrementType.SelectedItem.Text
            If Increment.IncrementType = IncrementType.CreditsEarned _
            Or Increment.IncrementType = IncrementType.CreditsAttempted Then
                DirectCast(e.Item.FindControl("lblHeaderCumulative"), Label).Text = "Cumulative Credits"
            Else
                DirectCast(e.Item.FindControl("lblHeaderCumulative"), Label).Text = "Hours" 'this should be a hide column
            End If
        End If

        If (TypeOf e.Item Is GridDataItem) AndAlso (e.Item.IsInEditMode = False) Then
            Dim DataItem As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim DeleteButton As ImageButton = DirectCast(DataItem("DeleteColumn").Controls(0), ImageButton)
            Dim EditCommandButton As ImageButton = DirectCast(DataItem("EditCommandColumn").Controls(0), ImageButton)
            'Dim PmtPeriodNumber As Integer = Convert.ToInt32(DirectCast(DataItem("PeriodNumber").Controls(1), Label).Text)
            'Dim strAux As String = DirectCast(DataItem("IsCharged").Controls(1), Label).Text
            Dim IsCharged As Boolean = Convert.ToBoolean(DirectCast(DataItem("IsCharged").Controls(1), Label).Text)
            If (IsCharged) Then
                e.Item.Enabled = False
                e.Item.ToolTip = "Payment Period already used. Do not allow update or delete."
                DeleteButton.Enabled = False
                DeleteButton.Visible = False
                EditCommandButton.Enabled = False
                EditCommandButton.Visible = False
            Else
                e.Item.Enabled = True
                e.Item.ToolTip = ""
                EditCommandButton.Enabled = True
                EditCommandButton.Visible = True
                'If (TotalPaymentPeriods > PmtPeriodNumber) Then
                '    DeleteButton.Enabled = False
                '    DeleteButton.Visible = False
                'Else
                DeleteButton.Enabled = True
                DeleteButton.Visible = True
                'End If
            End If
        End If

        If (TypeOf e.Item Is GridDataItem) AndAlso e.Item.IsInEditMode Then
            Dim DataItem As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim rfvIncrementValue As RequiredFieldValidator = DirectCast(e.Item.FindControl("rfvIncrementValue"), RequiredFieldValidator)
            Dim ravIncrementValue As RangeValidator = DirectCast(e.Item.FindControl("ravIncrementValue"), RangeValidator)
            ' adding the logic to bind the transaction code ddl
            Dim token As TokenResponse

            token = CType(Session("AdvantageApiToken"), TokenResponse)

            If (token IsNot Nothing) Then
                Dim transactionCodeRequest as New FAME.Advantage.Api.Library.FinancialAid.TransactionCodeRequest(token.ApiUrl, token.Token)
                dim transactionCodeResult = transactionCodeRequest.GetTransactionCodeForChargingMethod(campusId)
                dim ddlTranscationCode  As DropDownList
                ddlTranscationCode  = DirectCast(e.Item.FindControl("ddlTransactionCode"), DropDownList)
                ddlTranscationCode.DataSource = transactionCodeResult
                ddlTranscationCode.DataTextField = "Text"
                ddlTranscationCode.DataValueField = "Value"
                ddlTranscationCode.DataBind()
                ddlTranscationCode.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
                If Not TypeOf e.Item.DataItem Is GridInsertionObject Then
                    ddlTranscationCode.SelectedValue = DirectCast(e.Item.DataItem,DataRowView).Row.ItemArray(9).ToString()
                End If
            End If
            'Dim Period As Integer = Convert.ToInt32(DirectCast(DataItem("PeriodNumber").Controls(1), Label).Text)
            'If (Period = TotIncreValue.Total.Count) Then
            '    If (Increment.IncrementType = IncrementType.CreditsEarned _
            '    Or Increment.IncrementType = IncrementType.CreditsAttempted _
            '       ) Then
            '        ravIncrementValue.MaximumValue = MaxCredits
            '        ravIncrementValue.MinimumValue = MinIncrement
            '    Else
            '        ravIncrementValue.MaximumValue = MaxHours
            '        ravIncrementValue.MinimumValue = (TotIncreValue.Total(Period - 1) + 0.01).ToString()
            '    End If
            'Else
            '    If (Increment.IncrementType = IncrementType.CreditsEarned _
            '    Or Increment.IncrementType = IncrementType.CreditsAttempted _
            '       ) Then
            '        ravIncrementValue.MaximumValue = MaxCredits
            '        ravIncrementValue.MinimumValue = MinIncrement
            '    Else
            '        If (Period < TotIncreValue.Total.Count - 1) Then
            '            ravIncrementValue.MaximumValue = (TotIncreValue.Total(Period + 1) - 0.01).ToString()
            '        Else
            '            ravIncrementValue.MaximumValue = MaxHours
            '        End If
            '        ravIncrementValue.MinimumValue = (TotIncreValue.Total(Period - 1) + 0.01).ToString()
            '    End If
            'End If
            rfvIncrementValue.ErrorMessage = String.Format("{0} increment is required", ddlIncrementType.SelectedItem.Text)
            ravIncrementValue.ErrorMessage = String.Format("{0} should be between {1} and {2} ", ddlIncrementType.SelectedItem.Text, ravIncrementValue.MinimumValue, ravIncrementValue.MaximumValue)

        End If
        If (TypeOf e.Item Is GridDataItem) AndAlso e.Item.IsInEditMode Then
            'Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
            If (TypeOf e.Item Is GridHeaderItem) Then
            End If
        End If

    End Sub
    Protected Sub radGridPPI_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles radGridPPI.ItemCommand
        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            e.Canceled = True
            Dim newValues As ListDictionary = New ListDictionary()
            'Dim gridEditFormItem As GridCommandItem = DirectCast(e.Item, GridCommandItem)
            'newValues("PeriodNumber") = (TotalPaymentPeriods + 1).ToString()
            newValues("IncrementValue") = ""
            newValues("CumulativeValue") = ""
            newValues("ChargeAmount") = ""
            newValues("TransactionCodeId") = ""
            e.Item.OwnerTableView.InsertItem(newValues)
            TotIncreValue.Total.Add(0.0)
            'TotIncreValue.Total(TotalPaymentPeriods) = 0.0
        End If
    End Sub
    Protected Sub radGridPPI_ItemCreated(sender As Object, e As GridItemEventArgs) Handles radGridPPI.ItemCreated

    End Sub
    Protected Sub radGridPPI_InsertCommand(sender As Object, e As GridCommandEventArgs) Handles radGridPPI.InsertCommand
        Dim PmtPeriod As PmtPeriodInfo = New PmtPeriodInfo()
        Dim InsertedItem As GridDataInsertItem = DirectCast(e.Item, GridDataInsertItem)
        Dim saf As New StudentsAccountsFacade
        ' 1) fill PmtPeriodInfo
        With PmtPeriod
            '.PmtPeriodId  it is new PaymentPeriod already the construtor get the GUID
            .IncrementId = Increment.IncrementId
            '.PeriodNumber = Convert.ToInt32(DirectCast(InsertedItem("PeriodNumber").Controls(1), Label).Text)
            .IncrementValue = Convert.ToDecimal(DirectCast(InsertedItem("IncrementValue").Controls(1), TextBox).Text)
            If (Increment.IncrementType = IncrementType.CreditsAttempted) _
            Or (Increment.IncrementType = IncrementType.CreditsEarned) Then
                '.CumulativeValue = Convert.ToDecimal(TotIncreValue.Total(.PeriodNumber - 1)) + .IncrementValue
            End If
            If (Increment.IncrementType = IncrementType.ActualHours) _
            Or (Increment.IncrementType = IncrementType.ScheduledHours) Then
                .CumulativeValue = .IncrementValue
            End If
            .ChargeAmount = Convert.ToDecimal(DirectCast(InsertedItem("ChargeAmount ").Controls(1), TextBox).Text)
            .ModUser = AdvantageSession.UserName
            .ModDate = DateTime.Now
            .TransactionCodeId = Guid.Parse(DirectCast(InsertedItem("TransactionCode").Controls(1), DropDownList).SelectedValue)
            ' 2) save PmtPeriodINfo
            Const IsInDB As Boolean = False ' No existe it is a new record
            Dim result As String = saf.UpdatePmtPeriodInfo(IsInDB, PmtPeriod, Session("UserName").ToString())
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            End If
            ' 2) Recalculate all periods CumulateValues (Hours Vs Credits)
            ' RecalculateCumulativeValues()
            e.Canceled = True
            radGridPPI.MasterTableView.IsItemInserted = False
            radGridPPI.MasterTableView.ClearEditItems()
            BuildGridPPI()
            radGridPPI.MasterTableView.Rebind()
        End With

    End Sub
    Protected Sub radGridPPI_UpdateCommand(sender As Object, e As GridCommandEventArgs) Handles radGridPPI.UpdateCommand
        Dim PmtPeriod As PmtPeriodInfo = New PmtPeriodInfo()
        Dim EditedItem As GridEditableItem = DirectCast(e.Item, GridEditableItem)
        Dim saf As New StudentsAccountsFacade
        Dim result As String
        ' 1) Update PmtPeriodInfo
        With PmtPeriod
            .PmtPeriodId = DirectCast(EditedItem("PmtPeriodId").Controls(1), Label).Text()
            .IncrementId = Increment.IncrementId
            '.PeriodNumber = Convert.ToInt32(DirectCast(EditedItem("PeriodNumber").Controls(1), Label).Text)
            .IncrementValue = Convert.ToDecimal(DirectCast(EditedItem("IncrementValue").Controls(1), TextBox).Text)
            .CumulativeValue = Convert.ToDecimal(DirectCast(EditedItem("CumulativeValue").Controls(1), Label).Text)
            .ChargeAmount = Convert.ToDecimal(DirectCast(EditedItem("ChargeAmount ").Controls(1), TextBox).Text)
            .ModUser = AdvantageSession.UserName
            .ModDate = DateTime.Now
            .TransactionCodeId = Guid.Parse(TryCast(EditedItem("TransactionCode").Controls(1), DropDownList).SelectedValue)
            Const IsInDB As Boolean = True
            result = saf.UpdatePmtPeriodInfo(IsInDB, PmtPeriod, Session("UserName").ToString())
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            End If
            ' 2) Recalculate all periods CumulateValues (Hours Vs Credits)
            RecalculateCumulativeValues()
            e.Canceled = True
            radGridPPI.MasterTableView.ClearEditItems()
            BuildGridPPI()
            radGridPPI.MasterTableView.Rebind()

        End With
    End Sub
    Protected Sub radGridPPI_DeleteCommand(sender As Object, e As GridCommandEventArgs) Handles radGridPPI.DeleteCommand
        Dim deletedItem As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim PmtPeriodId As String = DirectCast(deletedItem("PmtPeriodId").Controls(1), Label).Text
        Dim saf As New StudentsAccountsFacade
        Dim result As String = saf.DeletePmtPeriodInfo(PmtPeriodId)
        If result <> "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            e.Canceled = True
            BuildGridPPI()
            radGridPPI.MasterTableView.Rebind()
        End If
    End Sub
    Protected Sub radGridPPI_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles radGridPPI.PreRender
        If Increment.IncrementType = IncrementType.CreditsEarned _
        Or Increment.IncrementType = IncrementType.CreditsAttempted Then
            radGridPPI.MasterTableView.Columns.FindByUniqueName("CumulativeValue").Display = True
        Else
            radGridPPI.MasterTableView.Columns.FindByUniqueName("CumulativeValue").Display = False
        End If
        If Not radGridPPI.MasterTableView.FilterExpression Is String.Empty Then
            BuildGridPPI()
            radGridPPI.MasterTableView.Rebind()
        End If
    End Sub
    Private Sub RecalculateCumulativeValues()
        Dim saf As New StudentsAccountsFacade
        Dim ds As DataSet
        Dim dt As DataTable
        Dim Total As Double
        Dim result As String

        ds = saf.GetPmtPeriods(Increment.IncrementId)
        dt = ds.Tables(0)
        TotalPaymentPeriods = dt.Rows.Count()
        If (TotalPaymentPeriods > 0) Then
            TotIncreValue = New TotalIncrementValue()
            TotIncreValue.Total.Add(0.0)
            For Period As Integer = 1 To TotalPaymentPeriods
                If (Increment.IncrementType = IncrementType.CreditsEarned _
                     Or Increment.IncrementType = IncrementType.CreditsAttempted _
                   ) Then
                    Total = TotIncreValue.Total(Period - 1) + Convert.ToDecimal(dt.Rows(Period - 1).Item("IncrementValue"))
                End If
                If (Increment.IncrementType = IncrementType.ActualHours _
                     Or Increment.IncrementType = IncrementType.ScheduledHours _
                   ) Then
                    Total = Convert.ToDecimal(dt.Rows(Period - 1).Item("IncrementValue"))
                End If
                TotIncreValue.Total.Add(Total)
                dt.Rows(Period - 1).Item("CumulativeValue") = TotIncreValue.Total(Period).ToString()
            Next
            result = saf.UpdatePmtPeriodsDS(ds)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                ds.RejectChanges()
            End If
        End If
    End Sub
#End Region
#Region "PageView Assign to Program Verions GridAPV"
    Private Sub BuildGridAPV()
        Dim facade As New ProgVerFacade
        Dim ds As DataSet
        radGridAPV.MasterTableView.IsItemInserted = False
        radGridAPV.MasterTableView.ClearEditItems()
        Try
            'check for increment type null

            ds = facade.PrgVersionsGetListActiveAvailableForABillingMethod(BillingMethod.BillingMethodId)
            If Not (ds.Tables(0) Is Nothing) Then
                radGridAPV.DataSource = ds.Tables(0)
            Else
                radGridAPV.DataSource = Nothing
            End If
            radGridAPV.DataBind()

            CheckItemsInGridAPV()

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage(" Error with Dataset")
            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in dataset" & ex.Message & " "
            Else
                Session("Error") = "Error in dataset" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
            Exit Sub
        End Try
    End Sub
    Protected Sub ToggleSelectedState(ByVal sender As Object, ByVal e As EventArgs)

        Dim headerCheckBox As CheckBox = TryCast(sender, CheckBox)
        If headerCheckBox.Checked Then
            For Each dataItem As GridDataItem In radGridAPV.MasterTableView.Items
                TryCast(dataItem.FindControl("rowChkBox"), CheckBox).Checked = headerCheckBox.Checked
                dataItem.Selected = headerCheckBox.Checked
            Next
        Else
            For Each dataItem As GridDataItem In radGridAPV.MasterTableView.Items
                If Not (DirectCast(dataItem.FindControl("lblIsBillingMethodCharged"), Label).Text = "True") Then
                    TryCast(dataItem.FindControl("rowChkBox"), CheckBox).Checked = headerCheckBox.Checked
                    dataItem.Selected = headerCheckBox.Checked
                End If
            Next

        End If

        'If (CType(sender, CheckBox)).Checked Then
        '    For Each dataItem As GridDataItem In radGridAPV.MasterTableView.Items
        '        CType(dataItem.FindControl("rowChkBox"), CheckBox).Checked = True
        '        dataItem.Selected = True
        '        Dim backColor As Color = ColorTranslator.FromHtml("#e4e4e4")
        '        radGridAPV.ItemStyle.BackColor = backColor
        '        radGridAPV.AlternatingItemStyle.BackColor = backColor
        '        radGridAPV.GridLines = GridLines.None
        '    Next
        'Else
        '    For Each dataItem As GridDataItem In radGridAPV.MasterTableView.Items
        '        CType(dataItem.FindControl("rowChkBox"), CheckBox).Checked = False
        '        dataItem.Selected = False
        '        radGridAPV.ItemStyle.BackColor = Color.White
        '        radGridAPV.AlternatingItemStyle.BackColor = Color.White
        '        radGridAPV.GridLines = GridLines.Both
        '    Next
        'End If
    End Sub
    Protected Sub ToggleRowSelection(ByVal sender As Object, ByVal e As EventArgs)
        'beforde update Verify the program version is not using the charging method 
        If (CType(sender, CheckBox).Checked = False) Then
            Dim dataitem As GridDataItem = TryCast(TryCast(sender, CheckBox).NamingContainer, GridDataItem)
            If (DirectCast(dataitem.FindControl("lblIsBillingMethodCharged"), Label).Text = "True") Then
                CType(sender, CheckBox).Checked = True
                Dim Message As String
                'Message = "Charging method could not be delocated from Program Version because Program Version has been used the charging method."
                Message = "The program version cannot be removed as charges have been posted to students using this charging method."
                Dim scriptstring As String = "radalert('" + Message + " ', 300, 200);"
                ScriptManager.RegisterStartupScript(Me, [GetType](), "radalert", scriptstring, True)
                Return
            End If
            ' Only check for ProgVersions marked as   IsBillingMethodCharged is true
            'If (DirectCast(dataitem.FindControl("lblIsUsed"), Label).Text = "True") Then
            '    CType(sender, CheckBox).Checked = True
            '    Dim Message As String
            '    Message = "Charging method could not be delocated because payment periods are used."
            '    Dim scriptstring As String = "radalert('" + Message + " ', 300, 200);"
            '    ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            '    Return
            'End If
        End If

        'CType(CType(sender, CheckBox).Parent.Parent, GridItem).Selected = CType(sender, CheckBox).Checked
        TryCast(TryCast(sender, CheckBox).NamingContainer, GridItem).Selected = TryCast(sender, CheckBox).Checked
        Dim checkHeader As Boolean = True
        'if some item is not checked the checkbox header should be false
        For Each dataItem As GridDataItem In radGridAPV.MasterTableView.Items
            If Not TryCast(dataItem.FindControl("rowChkBox"), CheckBox).Checked Then
                checkHeader = False
                Exit For
            End If
        Next
        Dim headerItem As GridHeaderItem = TryCast(radGridAPV.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
        TryCast(headerItem.FindControl("headerChkbox"), CheckBox).Checked = checkHeader

    End Sub
    Private Function GetListofCheckedProgramVersions() As DataSet
        Dim tbl As New DataTable("BMPV")        'BillingMethod-ProgramVersion
        Dim strPrgVerId As String
        Dim strBillingMehodId As String
        Dim strBMPVId As String                 'BillingMethod-ProgramVersion
        '        Dim intSelectedCount As Integer = 0
        '        Dim item As GridDataItem

        With tbl
            'Define table schema
            .Columns.Add("BMPVId", GetType(Guid))
            .Columns.Add("BillingMehodId", GetType(Guid))
            .Columns.Add("PrgVerId", GetType(Guid))

            'set field properties
            .Columns("BMPVId").AllowDBNull = False
            .Columns("BillingMehodId").AllowDBNull = False
            .Columns("PrgVerId").AllowDBNull = False

            'set primary key
            .PrimaryKey = New DataColumn() { .Columns("BillingMehodId"), .Columns("PrgVerId")}
        End With

        'GetSelectedItems method brings in a collection of rows that were selected
        For Each dataItem As GridDataItem In radGridAPV.MasterTableView.GetSelectedItems()
            'If the Program was selected, add the course to the datatable using LoadDataRow Method
            'LoadDataRow method helps get control RowState for the new DataRow
            'First Parameter: Array of values, the items in the array correspond to columns in the table. 
            'Second Parameter: AcceptChanges, enables to control the value of the RowState property of the new DataRow.
            '                  Passing a value of False for this parameter causes the new row to have a RowState of Added
            strBMPVId = Guid.NewGuid.ToString
            strBillingMehodId = BillingMethod.BillingMethodId
            strPrgVerId = CType(dataItem.FindControl("txtPrgVerId"), TextBox).Text.ToString
            tbl.LoadDataRow(New Object() {strBMPVId, strBillingMehodId, strPrgVerId}, False)
        Next

        Dim ds As DataSet = New DataSet()
        ds.Tables.Add(tbl)
        Return ds
    End Function
    Private Sub CheckItemsInGridAPV()
        Dim facade As New ProgVerFacade
        Dim ds As DataSet
        ds = facade.PrgVersionsGetListForABillingMethod(BillingMethod.BillingMethodId)
        For Each dr As DataRow In ds.Tables(0).Rows
            For Each dataitem As GridDataItem In radGridAPV.MasterTableView.Items
                If dr("PrgVerId").ToString().Trim().ToUpper = CType(dataitem.FindControl("txtPrgVerId"), TextBox).Text.ToString().Trim().ToUpper() Then
                    dataitem.Selected = True
                    DirectCast(dataitem.FindControl("rowChkBox"), CheckBox).Checked = True
                    DirectCast(dataitem.FindControl("lblIsBillingMethodCharged"), Label).Text = dr("IsBillingMethodCharged").ToString()
                    DirectCast(dataitem.FindControl("lblIsUsed"), Label).Text = dr("IsUsed").ToString()
                    Exit For
                End If
            Next
        Next
    End Sub
    'Private Function SaveBillingMethodPrgVersion() As Boolean
    '    Dim SaveResult As Boolean = False
    '    Dim dsChekedPrgVersions As New DataSet
    '    Dim facade As New ProgVerFacade
    '    '   update BillingMethod Info 
    '    dsChekedPrgVersions = GetListofCheckedProgramVersions()
    '    If Not dsChekedPrgVersions Is Nothing Then
    '        For Each lcol As DataColumn In dsChekedPrgVersions.Tables(0).Columns
    '            lcol.ColumnMapping = MappingType.Attribute
    '        Next
    '        Dim strXML As String = dsChekedPrgVersions.GetXml
    '        facade.UpdatePrgVersionsWithBillingMethod(strXML, BillingMethod.BillingMethodId)
    '    End If

    '    Return SaveResult
    'End Function

#End Region
#Region "Tabs"
    Protected Sub RadTabStrip_TabClick(ByVal sender As Object, ByVal e As RadTabStripEventArgs) Handles RadTabStrip.TabClick
        If (BillingMethod.BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then
            If TabFlag = False Then
                TabFlag = True
                Return
            End If
        End If
        Select Case e.Tab.Value
            Case "1"  'Tab index  = 0 PageBM
                ' Charging Methods
                Select Case CurrentTabIndex
                    Case "0" ' From PageBM P0
                        Exit Select
                    Case "1" ' From PagePP P1
                        If Page.IsValid Then
                            If (SaveFlag = True) Then
                                If (SaveP1() = True) Then
                                    MoveToP0(BillingMethod.BillingMethodId)
                                Else
                                    MoveToP1()
                                End If
                            Else
                                MoveToP0(BillingMethod.BillingMethodId)
                            End If
                        Else
                            MoveToP1()
                        End If
                        Exit Select
                    Case "2" ' From PagePV P2
                        If (SaveFlag = True) Then
                            If (SaveP2() = True) Then
                                MoveToP0(BillingMethod.BillingMethodId)
                            Else
                                MoveToP2()
                            End If
                        Else
                            MoveToP0(BillingMethod.BillingMethodId)
                        End If
                        Exit Select
                End Select
                Exit Select
            Case "2" 'Tab index  = 1 PagePP
                ' Payment Periods Increments
                Select Case CurrentTabIndex
                    Case "0" ' From PageBM P0
                        If (SaveFlag = True) Then
                            If (SaveP0() = True) Then
                                MoveToP1()
                            Else
                                MoveToP0(BillingMethod.BillingMethodId)
                            End If
                        Else
                            MoveToP1()
                        End If
                        Exit Select
                    Case "1" ' From PagePP P1
                        Exit Select
                    Case "2" ' From PagePV P2
                        If (SaveFlag = True) Then
                            If (SaveP2() = True) Then
                                MoveToP1()
                            Else
                                MoveToP2()
                            End If
                        Else
                            MoveToP1()
                        End If
                        Exit Select
                End Select
                Exit Select
            Case "3" 'Tab index  = 2 PagePV
                ' Assign to ProgramVersions
                Select Case CurrentTabIndex
                    Case "0" ' From PageBM P0
                        If (SaveFlag = True) Then
                            If (SaveP0() = True) Then
                                MoveToP2()
                            Else
                                MoveToP0(BillingMethod.BillingMethodId)
                            End If
                        Else
                            MoveToP2()
                        End If
                        Exit Select
                    Case "1" ' From PagePP P1
                        If Page.IsValid Then
                            If (SaveFlag = True) Then
                                If (SaveP1() = True) Then
                                    MoveToP2()
                                Else
                                    MoveToP1()
                                End If
                            Else
                                MoveToP2()
                            End If
                        Else
                            MoveToP1()
                        End If
                        Exit Select
                    Case "2" ' From PagePV P2
                        Exit Select
                End Select
                Exit Select
        End Select
        TabFlag = False
        SaveFlag = False
    End Sub
#End Region
#Region "Navigation Tabs "
    Private Sub btnSaveP0NextToP1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveP0NextToP1.Click
        TabFlag = False
        CurrentTabIndex = 0
        If (BillingMethod.BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then
            If (SaveP0() = True) Then
                SaveFlag = False
                MoveToP1()
            Else
                SaveFlag = True
                MoveToP0(BillingMethod.BillingMethodId)
            End If
        Else
            If (SaveP0() = True) Then
                SaveFlag = False
                MoveToP0(BillingMethod.BillingMethodId)
            Else
                SaveFlag = True
                MoveToP0(BillingMethod.BillingMethodId)
            End If
        End If
    End Sub
    Private Sub btnBackToP0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBackToP0.Click
        TabFlag = False
        If Page.IsValid Then
            SaveFlag = False
            MoveToP0(BillingMethod.BillingMethodId)
        End If
    End Sub
    Private Sub btnSaveP1NextToP2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveP1NextToP2.Click
        TabFlag = False
        If Page.IsValid Then
            CurrentTabIndex = 1
            If (SaveP1() = True) Then
                SaveFlag = False
                MoveToP2()
            Else
                SaveFlag = True
                MoveToP1()
            End If
        End If
    End Sub
    Private Sub btnBackToP1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBackToP1.Click
        TabFlag = False
        SaveFlag = False
        CurrentTabIndex = 2
        If (RadMultiPage.PageViews(1).Enabled = True) Then
            MoveToP1()
        Else
            MoveToP0(BillingMethod.BillingMethodId)
        End If
    End Sub
    Private Sub btnSaveP2BackToP0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveP2BackToP0.Click
        TabFlag = False

        CurrentTabIndex = 2
        If (SaveP2() = True) Then
            SaveFlag = False
            MoveToP0(BillingMethod.BillingMethodId)
        Else
            SaveFlag = True
            MoveToP2()
        End If
    End Sub
    Private Function SaveP0() As Boolean
        Dim saveResult As Boolean = False
        Dim errorMessage As String = ValidateTxtExcAbsencesPercent()
        If errorMessage <> String.Empty Then
            DisplayErrorMessage(errorMessage)
            saveResult = False
        Else
            errorMessage = ValidateIncrementType()
            If errorMessage <> String.Empty Then
                DisplayErrorMessage(errorMessage)
                saveResult = False
            Else
                '   if there are no errors bind a new entity and init buttons
                'If Page.IsValid Then
                If (SaveBillingMethod() = True) Then
                    '   bind datalist
                    '   set the property IsInDB to true in order to avoid an error if the user
                    '   hits "save" twice after adding a record.
                    chkIsInDB.Checked = True
                    saveResult = True

                Else
                    saveResult = False
                End If
            End If
        End If
        Return saveResult
    End Function
    Private Function SaveP1() As Boolean
        Dim saveResult As Boolean = False
        If (SaveP0() = True) Then
            saveResult = True
        Else

        End If

        Return saveResult
    End Function
    Private Function SaveP2() As Boolean
        Dim saveResult As Boolean = False
        Dim facade As New ProgVerFacade
        Dim dsChekedPrgVersions As DataSet
        dsChekedPrgVersions = GetListofCheckedProgramVersions()
        If Not dsChekedPrgVersions Is Nothing Then
            For Each lcol As DataColumn In dsChekedPrgVersions.Tables(0).Columns
                lcol.ColumnMapping = MappingType.Attribute
            Next
            Dim strXML As String = dsChekedPrgVersions.GetXml
            facade.UpdatePrgVersionsWithBillingMethod(strXML, BillingMethod.BillingMethodId)
        End If
        If (SaveP0() = True) Then
            saveResult = True
        Else
            saveResult = False
        End If
        Return saveResult
    End Function
    Private Sub MoveToP0(ByVal pBillingMethodId As String)
        GetBillingMethodAndIncrement(pBillingMethodId)
        SetIncrementFields(BillingMethod.BillingMethod)
        SetExcAbscene()
        SetlblMessageApplyPrgVersion()
        SyncroTabAndPage(0)
        SetTabAndPage()
        '   bind datalist
        BindDataList()
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlstBillingMethods, BillingMethod.BillingMethodId)
    End Sub
    Private Sub MoveToP1()
        PaymentPeriodControl(BillingMethod.BillingMethod)
        SetIncrementFields(BillingMethod.BillingMethod)
        SyncroTabAndPage(1)
    End Sub
    Private Sub MoveToP2()
        BuildGridAPV()
        SyncroTabAndPage(2)
    End Sub
    'Private Sub SetTabAndPageEnable(ByVal BillingMethod As BillingMethod, ByVal ind As Integer)
    '    For Each Tab As RadTab In RadTabStrip.Tabs
    '        If Tab.Index = ind Then
    '            Tab.Enabled = True
    '        Else
    '            Tab.Enabled = False
    '        End If
    '    Next
    '    For Each Page As RadPageView In RadMultiPage.PageViews
    '        If Page.Index = ind Then
    '            Page.Enabled = True
    '        Else
    '            Page.Enabled = False
    '        End If
    '    Next
    '    If (BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then
    '        For Each Tab As RadTab In RadTabStrip.Tabs
    '            Tab.Enabled = True
    '        Next
    '        For Each Page As RadPageView In RadMultiPage.PageViews
    '            Page.Enabled = True
    '        Next

    '    Else
    '        ind = 0
    '        For Each Tab As RadTab In RadTabStrip.Tabs
    '            If Tab.Index = ind Then
    '                Tab.Enabled = True
    '            Else
    '                Tab.Enabled = False
    '            End If
    '        Next
    '        For Each Page As RadPageView In RadMultiPage.PageViews
    '            If Page.Index = ind Then
    '                Page.Enabled = True
    '            Else
    '                Page.Enabled = False
    '            End If
    '        Next
    '    End If
    '    'RadTabStrip.SelectedIndex = ind
    '    RadMultiPage.SelectedIndex = ind
    '    CurrentTabIndex = ind
    'End Sub
    Private Sub SyncroTabAndPage(ByVal ind As Integer)
        RadTabStrip.SelectedIndex = ind
        RadMultiPage.SelectedIndex = ind
        CurrentTabIndex = ind
    End Sub
    Private Sub SetTabAndPage()
        If (BillingMethod.IsInDB = True) Then
            If (BillingMethod.BillingMethod = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then
                RadMultiPage.PageViews(0).Enabled = True
                RadMultiPage.PageViews(1).Enabled = True
                RadMultiPage.PageViews(2).Enabled = True
                RadTabStrip.Tabs(0).Enabled = True
                RadTabStrip.Tabs(1).Enabled = True
                RadTabStrip.Tabs(2).Enabled = True
            Else
                RadMultiPage.PageViews(0).Enabled = True
                RadMultiPage.PageViews(1).Enabled = False
                RadMultiPage.PageViews(2).Enabled = True
                RadTabStrip.Tabs(0).Enabled = True
                RadTabStrip.Tabs(1).Enabled = False
                RadTabStrip.Tabs(2).Enabled = True
            End If
        Else
            RadMultiPage.PageViews(0).Enabled = True
            RadMultiPage.PageViews(1).Enabled = False
            RadMultiPage.PageViews(2).Enabled = False
            RadTabStrip.Tabs(0).Enabled = True
            RadTabStrip.Tabs(1).Enabled = False
            RadTabStrip.Tabs(2).Enabled = False
        End If
    End Sub
#End Region
#Region "Validations"
    Private Function ValidateTxtExcAbsencesPercent() As String
        'Validate percentage
        Dim ErrorMessage As String = String.Empty
        Dim strPercentage As String = txtExcAbsencesPercent.Text.Replace("%", "")
        Dim decValue As Decimal
        Dim intValue As Integer
        'Percent of Excused Absences cannot be grather than 10.00%
        If (Decimal.TryParse(strPercentage, decValue)) Then
            If (decValue <> 0D) Then       ' 0.0 = 0D (decimal)
                decValue = decValue / 100
                intValue = CType(decValue * 10000, Integer)
                'Validate the range
                If (intValue < 0) _
                Or (intValue > 0.1 * 10000) Then
                    ErrorMessage = String.Format("Excused Absences cannot exceed 10% ")
                End If
            End If
            txtExcAbsencesPercent.Text = String.Format("{0:P2}", decValue)
        Else
            ErrorMessage = String.Format("Incorrect format for Percent of Excused Absences: {0:P2} ", decValue)
        End If
        'Validate Date
        If (txtEffectiveDate.SelectedDate Is Nothing) Then
            ErrorMessage &= IIf(ErrorMessage <> String.Empty, Environment.NewLine, String.Empty).ToString() _
                         + String.Format("Payment Period Effective Date should be selected.")
        End If
        Return ErrorMessage
    End Function
    Private Function ValidateIncrementType() As String
        'Validate IncrementValue should be > than before or cero if it is the first
        Dim ErrorMessage As String = String.Empty
        If (ddlBillingMethod.SelectedValue = FAME.AdvantageV1.Common.BillingMethod.PaymentPeriod) Then
            If (ddlIncrementType.SelectedValue = IncrementType.NotApply) Then
                ErrorMessage = String.Format("Increment type is required")
            End If
        Else
            If (ddlIncrementType.SelectedValue > IncrementType.NotApply) Then
                ddlIncrementType.SelectedValue = IncrementType.NotApply
            End If
        End If
        Return ErrorMessage
    End Function
    Protected Sub CumulativeValidation(ByVal source As Object, ByVal args As ServerValidateEventArgs)
        'args.IsValid = (args.Value.Length >= 8)
        If (Increment.IncrementType = IncrementType.CreditsAttempted) _
            Or (Increment.IncrementType = IncrementType.CreditsEarned) Then
            Dim tot As Double = TotIncreValue.Total(TotIncreValue.Total.Count - 2)
            tot = tot + CType(args.Value, Double)
            If tot > 500 Then
                Dim ErrorMessage As String = String.Format("Cumulative value cannot exceed 500 credits")
                'Dim scriptstring As String = "radalert('" + ErrorMessage + " ', 300, 200, null, 'Error message'); return false;"
                'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
                RadWindowManager1.RadAlert(ErrorMessage, 330, 180, "Error Message", "", "True")
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        Else
            args.IsValid = True
        End If

    End Sub

    Private Sub radGridPPI_ItemInserted(sender As Object, e As GridInsertedEventArgs) Handles radGridPPI.ItemInserted

    End Sub
#End Region

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub


    'Dim scriptstring As String = "radalert('Welcome to Rad<b>window</b>!', 330, 210);"
    'Dim scriptstring As String = "radalert('Welcome to Rad<b>window</b>!', 330, 210);"
    'Dim scriptstring As String = "radalert('" + Message + " ', 300, 200);"
    '           ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
    'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openWindow", "alertCallBackFn()", True)
    'RadWindowManager1.RadAlert(Message, 350, 200, "Error Message", "alertCallBackFn", "")
    'DisplayErrorMessage(Message)
    'RadWindowManager1.RadAlert(Message, 300, 100, "Error Message", "alertCallBackFn")
    'Dim sb As New System.Text.StringBuilder()
    '            sb.Append("alert('")
    '            sb.Append(Message)
    '            sb.Append("');")
    'ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())
    'ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('navigateToInserted');", True)

    'CommonWebUtilities.DisplayErrorInClientMessageBox(Me.Page, Message)

End Class


