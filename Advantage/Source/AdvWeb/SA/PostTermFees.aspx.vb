﻿
Imports System.Xml
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports System.IO
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common

Partial Class PostTermFees
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents btnhistory As Button
    Protected WithEvents Label1 As Label
    Protected WithEvents allDataset As DataSet
    Protected WithEvents PostDataset As DataSet
    Private empId As String
    Private employee As String
    Protected totalAmount As Decimal
    Protected totalStudents As Integer
    Protected lastStudent As String

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Private postDate As Date
    Private useCohortFilter As String = "no"
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim userId As String
        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        ''Cohort StartDate is Shown based on the ConfigEntry UseCohortStartDateForPostTermFees
        'useCohortFilter = SingletonAppSettings.AppSettings("UseCohortStartDateForFilters").ToString.ToLower
        ''Commented the above line and added these lines on Jan 7th 2009 by Saraswathi

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If Not MyAdvAppSettings.AppSettings("UseCohortStartDateForPostTermFees") Is Nothing Then
            If MyAdvAppSettings.AppSettings("UseCohortStartDateForPostTermFees").ToString.ToLower = "yes" Then
                useCohortFilter = "yes"
            Else
                useCohortFilter = "no"
            End If
        Else
            useCohortFilter = "no"
        End If

        ' the radio button value Term should be enabled only for non ModuleStart schools
        rblFees.Items(2).Enabled = Not CommonWebUtilities.IsModuleStart()

        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        '   use this empId for testing purposes only
        If CommonWebUtilities.IsValidGuid(Session("EmpId")) Then
            empId = Session("EmpId")
            employee = Session("EmployeeName")
        Else
            empId = "9be9ce82-23a6-4955-be96-b9fe00bf79f3"
            employee = "Nancy Smith"
        End If

        'Header1.EnableHistoryButton(False)

        If Not IsPostBack Then
            '   build dropdownlists
            BuildDropDownLists()

            If pObj.HasFull Or pObj.HasAdd Then
                btnBuildList.Enabled = True
            Else
                btnBuildList.Enabled = False
            End If

            'initialize textbox with todays date
            txtPostFeesDate.SelectedDate = Date.Today.ToShortDateString()

        End If

        '   Post Fees button should be disabled in most cases
        'SetControls(False)
        btnDeletePostedFees.Enabled = False
        dgrdPostTermFees.Columns(11).Visible = True
        dgrdPostTermFees.Columns(8).Visible = False


    End Sub

    Private Sub BuildDropDownLists()
        If useCohortFilter = "yes" Then
            lblTerm.Text = "Cohort Start Date"
            BuildTermsDDLForCohort()
        Else
            lblTerm.Text = "Terms"
            BuildTermsDDL()
        End If

    End Sub
    Private Sub BuildTermsDDL()
        '   bind the Terms DDL
        Dim terms As New StudentsAccountsFacade

        With ddlTermsId
            .Items.Clear()
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            Dim table As DataTable = terms.GetAllTerms(MapIdxToFeeType(rblFees.SelectedIndex), CType(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")), Guid).ToString).Tables(0)
            'Dim table As DataTable = terms.GetAllTerms(True, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString).Tables(0)

            '   populate ddl and save NumberOfTransPosted as an Attribute
            For i As Integer = 0 To table.Rows.Count - 1
                Dim row As DataRow = table.Rows(i)
                Dim listItem As New RadComboBoxItem(row("TermDescrip"), CType(row("TermId"), Guid).ToString)

                '   add an (*) to all items that are already posted
                'If Not row("NumberOfTransactionsPosted") = 0 Then
                '    listItem.Text += "(posted)"
                '    .Items.Insert(0, listItem)
                'Else
                '    '   add the item to the ddl
                '    .Items.Insert(0, listItem)
                'End If
                .Items.Insert(0, listItem)
            Next

            '   insert the "select" item as the first item in the ddl
            .Items.Insert(0, New RadComboBoxItem("Select Term", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildTermsDDLForCohort()
        '   bind the Terms DDL
        Dim terms As New StudentsAccountsFacade

        With ddlTermsId
            .Items.Clear()
            .DataTextField = "CohortStartdate"
            .DataValueField = "CohortStartdate"
            Dim table As DataTable = terms.GetAllCohorts(MapIdxToFeeType(rblFees.SelectedIndex), CType(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")), Guid).ToString).Tables(0)
            'Dim table As DataTable = terms.GetAllTerms(True, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString).Tables(0)

            '   populate ddl and save NumberOfTransPosted as an Attribute
            For i As Integer = 0 To table.Rows.Count - 1
                Dim row As DataRow = table.Rows(i)
                Dim listItem As New RadComboBoxItem(row("CohortStartdate"), row("CohortStartdate"))

                '   add an (*) to all items that are already posted
                'If Not row("NumberOfTransactionsPosted") = 0 Then
                '    listItem.Text += "(posted)"
                '    .Items.Insert(0, listItem)
                'Else
                '    '   add the item to the ddl
                '    .Items.Insert(0, listItem)
                'End If
                .Items.Insert(0, listItem)
            Next

            '   insert the "select" item as the first item in the ddl
            .Items.Insert(0, New RadComboBoxItem("Select Cohort Start Date", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Function MapIdxToFeeType(ByVal idx As Integer) As AdvantageCommonValues.TuitionFeeTypes
        Select Case idx
            Case 0
                Return AdvantageCommonValues.TuitionFeeTypes.Program
            Case 1
                Return AdvantageCommonValues.TuitionFeeTypes.Course
            Case 2
                Return AdvantageCommonValues.TuitionFeeTypes.Term
        End Select
        Return Nothing
    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End If

    End Sub

    Private Sub dgrdPostTermFees_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdPostTermFees.ItemCommand
        '   process postbacks from the datagrid
        Select Case e.CommandName
        End Select

        '   Bind DataGrid
        BindDataGrid()

    End Sub

    Private Sub BindDataGrid()
        '   bind datagrid to dataGridTable
        'dgrdPostTermFees.DataSource = New DataView(allDataset.Tables(0), Nothing, "StudentName asc ", DataViewRowState.CurrentRows)
        dgrdPostTermFees.DataSource = New DataView(allDataset.Tables(0), Nothing, "NumberOfTransactionsPosted,studentname", DataViewRowState.CurrentRows)
        dgrdPostTermFees.DataBind()

    End Sub

    Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
        dgrdPostTermFees.Columns(8).Visible = False
        dgrdPostTermFees.Columns(11).Visible = True
        Dim isPosted As Boolean = False
        'If pObj.HasFull Or pObj.HasAdd Then
        '    '   enable or disable the "post button" accordingly
        '    isPosted = ddlTermsId.SelectedItem.Text.IndexOf("(posted)") > -1
        '    SetControls(isPosted)
        'Else
        '    SetControls(False)
        'End If

        '   display only valid Terms
        If useCohortFilter = "yes" Then

            txtTermsId.Text = ddlTermsId.SelectedValue
            If txtTermsId.Text <> "" Then
                Select Case rblFees.SelectedIndex
                    Case 0
                        '   process Program Version Fees
                        GetPostProgramVersionFeesDS(isPosted)

                        dgrdPostTermFees.Columns(3).HeaderText = "Enrollment"

                        '   bind datalist
                        BindDataGrid()



                    Case 1
                        '   process Course Fees
                        GetPostCourseFeesDS(isPosted)

                        dgrdPostTermFees.Columns(3).HeaderText = "Course"

                        '   bind datalist
                        BindDataGrid()



                    Case 2
                        '   process Term Fees
                        GetPostTermFeesDS(isPosted)

                        dgrdPostTermFees.Columns(3).HeaderText = "Enrollment"

                        '   bind datalist
                        BindDataGrid()



                End Select
                'Else
                '    DisplayErrorMessage("You must select a Cohort Start Date")
            End If
        Else

            If CommonWebUtilities.IsValidGuid(ddlTermsId.SelectedValue) Then

                '   save the selected term
                txtTermsId.Text = ddlTermsId.SelectedValue

                Select Case rblFees.SelectedIndex
                    Case 0
                        '   process Program Version Fees
                        GetPostProgramVersionFeesDS(isPosted)

                        dgrdPostTermFees.Columns(3).HeaderText = "Enrollment"

                        '   bind datalist
                        BindDataGrid()



                    Case 1
                        '   process Course Fees
                        GetPostCourseFeesDS(isPosted)

                        dgrdPostTermFees.Columns(3).HeaderText = "Course"

                        '   bind datalist
                        BindDataGrid()



                    Case 2
                        '   process Term Fees
                        GetPostTermFeesDS(isPosted)

                        dgrdPostTermFees.Columns(3).HeaderText = "Enrollment"

                        '   bind datalist
                        BindDataGrid()



                End Select

            Else
                '   display error message
                If Not dgrdPostTermFees.DataSource = Nothing Then DisplayErrorMessage("You must select a Term")
            End If

        End If

    End Sub
    Private Sub GetPostTermFeesDS(ByVal isPosted As Boolean)

        If isPosted Then
            '   get dataset from the backend
            If useCohortFilter = "yes" Then
                allDataset = (New StudentsAccountsFacade).GetPostedFeesByTermDSForCohortStart(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            Else
                allDataset = (New StudentsAccountsFacade).GetPostedFeesByTermDS(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            End If

        Else
            '   get dataset from the backend
            If useCohortFilter = "yes" Then
                allDataset = (New StudentsAccountsFacade).GetFeesToBePostedByTermDSForCohortStart(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            Else
                allDataset = (New StudentsAccountsFacade).GetFeesToBePostedByTermDS(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            End If

        End If

    End Sub
    Private Sub GetPostCourseFeesDS(ByVal isPosted As Boolean)

        If isPosted Then
            '   get dataset from the backend
            If useCohortFilter = "yes" Then
                allDataset = (New StudentsAccountsFacade).GetPostedFeesByCourseDSForCohortStart(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            Else
                allDataset = (New StudentsAccountsFacade).GetPostedFeesByCourseDS(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            End If


        Else
            '   get dataset from the backend
            If useCohortFilter = "yes" Then
                allDataset = (New StudentsAccountsFacade).GetFeesToBePostedByCourseDSForCohortStart(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            Else
                allDataset = (New StudentsAccountsFacade).GetFeesToBePostedByCourseDS(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            End If

        End If

    End Sub
    Private Sub GetPostProgramVersionFeesDS(ByVal isPosted As Boolean)
        If isPosted Then
            '   get dataset from the backend
            If useCohortFilter = "yes" Then
                allDataset = (New StudentsAccountsFacade).GetPostedFeesByProgramVersionDSForCohortStart(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            Else
                allDataset = (New StudentsAccountsFacade).GetPostedFeesByProgramVersionDS(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            End If

        Else
            '   get dataset from the backend
            If useCohortFilter = "yes" Then
                allDataset = (New StudentsAccountsFacade).GetFeesToBePostedByProgramVersionDSForCohortStart(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            Else
                allDataset = (New StudentsAccountsFacade).GetFeesToBePostedByProgramVersionDS(txtTermsId.Text, AdvantageSession.UserState.UserName, campusId)
            End If

        End If

    End Sub
    Private Sub dgrdPostTermFees_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdPostTermFees.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                '   acumulate balance

                If Not CType(e.Item.DataItem, DataRowView).Item("TransAmount") Is DBNull.Value Then
                    Dim amount As Decimal = CType(e.Item.DataItem, DataRowView).Item("TransAmount")
                    If amount > 0 Then
                        postDate = e.Item.DataItem("TransDate")
                        If CType(e.Item.DataItem, DataRowView).Item("NumberOfTransactionsPosted") = 0 Then
                            totalAmount += amount

                        End If
                        e.Item.Visible = True
                        '   acumulate number of students
                        If totalStudents = 0 Then

                            lastStudent = e.Item.DataItem("StudentName")
                            If CType(e.Item.DataItem, DataRowView).Item("NumberOfTransactionsPosted") = 0 Then totalStudents += 1
                        Else
                            If Not lastStudent = e.Item.DataItem("StudentName") Then

                                lastStudent = e.Item.DataItem("StudentName")
                                If CType(e.Item.DataItem, DataRowView).Item("NumberOfTransactionsPosted") = 0 Then totalStudents += 1
                            End If
                        End If
                    Else
                        e.Item.Visible = False
                    End If
                Else
                    e.Item.Visible = False
                End If
                Dim TransCnt As Integer = CType(e.Item.DataItem, DataRowView).Item("NumberOfTransactionsPosted")
                If TransCnt > 0 Then
                    CType(e.Item.FindControl("chkRemove"), CheckBox).Visible = False
                    e.Item.BackColor = Color.LightGray
                End If

                'If TransCnt>0 then e.Item.DataItem("

            Case ListItemType.Footer
                '   display total in footer
                CType(e.Item.FindControl("lblFooterAmount"), Label).Text = totalAmount.ToString("c")

                '   enable Post Fees button only if there are records in the datagrid
                If totalAmount > 0 And ddlTermsId.SelectedItem.Text.IndexOf("(posted)") < 0 Then
                    SetControls(True)
                    btnDeletePostedFees.Enabled = True
                    'Set the PostFee Button so it prompts the user for confirmation when clicked
                    btnPostFees.Attributes.Add("onclick", "if(confirm('Are you sure you want to post these fees?')){}else{return false}")
                Else
                    SetControls(False)
                    btnDeletePostedFees.Enabled = False
                End If

                '   put totals on top   
                lblTotalStudents.Text = totalStudents.ToString("#####0")
                lblTotalAmount.Text = totalAmount.ToString("c")
        End Select

        If rblFees.SelectedIndex = 0 Then
            dgrdPostTermFees.Columns(3).HeaderText = "Enrollment"
        ElseIf rblFees.SelectedIndex = 1 Then
            dgrdPostTermFees.Columns(3).HeaderText = "Course"
        Else
            dgrdPostTermFees.Columns(3).HeaderText = "Enrollment"
        End If

    End Sub
    Private Sub btnPostFees_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPostFees.Click
        'validate Post Fees Date
        Dim ds As DataSet
        If Not IsDate(txtPostFeesDate.SelectedDate) Then
            '   Display Error Message
            DisplayErrorMessage("Invalid Post Fees Date")
            Exit Sub
        End If
        ds = getPostDataSet()
        AddPostTermFees(ds)
        btnBuildList_Click(Me, New EventArgs())
    End Sub
    Private Sub btnDeletePostedFees_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDeletePostedFees.Click
        Dim result As String
        Select Case rblFees.SelectedIndex
            Case 0
                '   process Program Version Fees
                result = (New StudentsAccountsFacade).DeleteAppliedFeesByProgramVersion(txtTermsId.Text, Session("UserName"), campusId)
                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    '   Display Success message
                    DisplayErrorMessage("Successfully deleted Posted Fees")
                End If
            Case 1
                '   process Course Fees
                result = (New StudentsAccountsFacade).DeleteAppliedFeesByCourse(txtTermsId.Text, Session("UserName"), campusId)
                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    '   Display Success message
                    DisplayErrorMessage("Successfully deleted Posted Fees")
                End If
            Case 2
                '   process Term Fees
                result = (New StudentsAccountsFacade).DeleteAppliedFeesByTerm(txtTermsId.Text, Session("UserName"), campusId)
                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    '   Display Success message
                    DisplayErrorMessage("Successfully deleted Posted Fees")
                End If
        End Select

    End Sub

    Protected Sub rblFees_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rblFees.SelectedIndexChanged
        'rebuild TermsDDL
        If useCohortFilter = "yes" Then
            lblTerm.Text = "Cohort StartDate"
            BuildTermsDDLForCohort()
        Else
            lblTerm.Text = "Terms"
            BuildTermsDDL()
        End If

        '   clear datagrid 
        dgrdPostTermFees.DataSource = Nothing
        dgrdPostTermFees.DataBind()


        btnBuildList_Click(Me, New EventArgs())

        SetControls(False)
        btnDeletePostedFees.Enabled = False
        lblTotalStudents.Text = String.Empty
        lblTotalAmount.Text = String.Empty

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        Return
    End Sub
    Private Shared Sub PrepareControlForExport(control As Control)
        For i As Integer = 0 To control.Controls.Count - 1
            Dim current As Control = control.Controls(i)
            If TypeOf current Is LinkButton Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(TryCast(current, LinkButton).Text))
            ElseIf TypeOf current Is ImageButton Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(TryCast(current, ImageButton).AlternateText))
            ElseIf TypeOf current Is HyperLink Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(TryCast(current, HyperLink).Text))
            ElseIf TypeOf current Is DropDownList Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(TryCast(current, DropDownList).SelectedItem.Text))
            ElseIf TypeOf current Is CheckBox Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(If(TryCast(current, CheckBox).Checked, "True", "False")))
            End If

            If current.HasControls() Then
                PrepareControlForExport(current)
            End If
        Next
    End Sub
    Private Sub btnExportToExcell_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcell.Click
        RememberOldValues()
        Dim oStringWriter As StringWriter = New StringWriter
        Dim oHtmlTextWriter As HtmlTextWriter = New HtmlTextWriter(oStringWriter)
        PrepareControlForExport(dgrdPostTermFees)

        HideAllCheckBoxes(dgrdPostTermFees)
        ClearControls(dgrdPostTermFees)
        dgrdPostTermFees.Columns(8).Visible = True
        dgrdPostTermFees.RenderControl(oHtmlTextWriter)

        'convert html to string
        Dim enc As Encoding = Encoding.UTF8
        Dim s As String = oStringWriter.ToString

        'move html content to a memorystream
        Dim documentMemoryStream As New MemoryStream
        documentMemoryStream.Write(enc.GetBytes(s), 0, s.Length)

        '   save document and document type in sessions variables
        Session("DocumentMemoryStream") = documentMemoryStream
        Session("ContentType") = "application/vnd.ms-excel"

        '   Register a javascript to open the report in another window
        Const javascript As String = "<script>var origwindow=window.self;window.open('PrintAnyReport.aspx');</script>"
        Dim csType As Type = Me.[GetType]()

        ClientScript.RegisterClientScriptBlock(csType, "NewWindow", javascript)
        btnBuildList_Click(Me, New EventArgs())

        RePopulateValues()
    End Sub


    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step i - 1
            ClearControls(control.Controls(i))
        Next
        If control.GetType.ToString = "System.Web.UI.HtmlControls.HtmlTableCell" Then
            If Not control.GetType().GetProperty("SelectedItem") Is Nothing Then
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            Else
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            End If
        End If
        Return
    End Sub

    Protected Sub PrepareGridViewForExport(gv As Control)
        Dim i As Integer
        Dim l As LiteralControl = New LiteralControl
        For i = 0 To gv.Controls.Count - 1 Step i + 1
            If gv.Controls(i).GetType().ToString = "System.Web.UI.WebControls.CheckBox" Then


            End If

            '    l.Text = (gv.Controls(i) as CheckBox).Checked? "True" : "False"

            'gv.Controls.Remove(gv.Controls(i))
            'gv.Controls.AddAt(i, l)
            '}
            'If (gv.Controls(i).HasControls()) Then
            '{
            '    PrepareGridViewForExport(gv.Controls(i))
            '}
        Next
    End Sub

    '    private void PrepareGridViewForExport(Control gv)
    '{

    '    Dim lb As LinkButton = New LinkButton()
    '    Dim l As Literal = New Literal()
    '    Dim name As String = String.Empty
    '    Dim i As Integer
    '    For  i = 0 To  gv.Controls.Count- 1  Step  i + 1
    '      if (gv.Controls(i).GetType() = Type.GetType(CheckBox))
    '        {
    '            l.Text = (gv.Controls(i) as CheckBox).Checked? "True" : "False"

    '            gv.Controls.Remove(gv.Controls(i))
    '            gv.Controls.AddAt(i, l)
    '        }
    '        if (gv.Controls(i).HasControls())
    '        {
    '            PrepareGridViewForExport(gv.Controls(i))
    '        }
    '    Next
    '}


    Private Sub HideAllCheckBoxes(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step i - 1
            HideAllCheckBoxes(control.Controls(i))
        Next
        If control.GetType.ToString = "System.Web.UI.WebControls.CheckBox" Then
            'If control.GetType.ToString = "System.Web.UI.WebControls.TableCell" Then
            Dim literal As LiteralControl = New LiteralControl
            control.Parent.Controls.Add(literal)
            literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
            'control.Parent.Controls.Remove(control)
            control.Visible = False
        End If
        Return
    End Sub

    Private Sub SetControls(ByVal val As Boolean)
        btnPostFees.Enabled = val And (dgrdPostTermFees.Items.Count > 0)
        txtPostFeesDate.Enabled = val
        If val Then txtPostFeesDate.SelectedDate = Date.Today.ToShortDateString() 'Else txtPostFeesDate.SelectedDate = postDate.ToShortDateString()
        'CalButton1.Visible = val
        btnExportToExcell.Enabled = btnPostFees.Enabled
    End Sub



    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'BIndToolTip()
    End Sub
    'Protected Function getStuEnrollId() As String()
    '    Dim rtn(2) As String
    '    Dim stuList As New StringBuilder
    '    Dim transCodeList As New StringBuilder
    '    For i As Integer = 0 To dgrdPostTermFees.Items.Count - 1
    '        If Not CType(dgrdPostTermFees.Items(i).FindControl("Chkremove"), CheckBox).Checked Then
    '            If stuList.Length > 0 And Not stuList.ToString.Contains(CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(4), TableCell).Text) Then stuList.Append(",")

    '            If Not stuList.ToString.Contains(CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(4), TableCell).Text) Then stuList.Append(CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(4), TableCell).Text)


    '            If transCodeList.Length > 0 And Not transCodeList.ToString.Contains(CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(5), TableCell).Text) Then transCodeList.Append(",")


    '            If Not transCodeList.ToString.Contains(CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(5), TableCell).Text) Then transCodeList.Append(CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(5), TableCell).Text)


    '        End If
    '    Next
    '    rtn(0) = stuList.ToString()
    '    rtn(1) = transCodeList.ToString()
    '    Return rtn
    'End Function
    Protected Function getPostDataSet() As DataSet

        Dim iFeeLevelId As Integer = 0
        If rblFees.SelectedIndex = 0 Then iFeeLevelId = 2 'program version
        If rblFees.SelectedIndex = 1 Then iFeeLevelId = 3 'course 
        If rblFees.SelectedIndex = 2 Then iFeeLevelId = 1 'term
        Dim dtRes As DataTable = CreatePostDataTableSchema()
        For i As Integer = 0 To dgrdPostTermFees.Items.Count - 1
            If Not CType(dgrdPostTermFees.Items(i).FindControl("Chkremove"), CheckBox).Checked And CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(8), TableCell).Text = 0 Then
                'PostDataset.Tables(0).Rows.RemoveAt(i)
                Dim drNew As DataRow = dtRes.NewRow()
                drNew("StuEnrollId") = CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(4), TableCell).Text
                drNew("TermId") = CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(12), TableCell).Text
                drNew("CampusId") = campusId
                drNew("TransDate") = Date.Parse(txtPostFeesDate.SelectedDate)
                drNew("TransCodeId") = CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(5), TableCell).Text
                drNew("TransReference") = CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(6), TableCell).Text 'transdescrip
                drNew("TransDescrip") = CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(7), TableCell).Text 'transcodedescrip
                drNew("TransAmount") = CType(dgrdPostTermFees.Items(i).FindControl("lblAmount"), Label).Text.Replace("$", "")
                drNew("ModUser") = Session("UserName")
                drNew("FeeLevelId") = iFeeLevelId
                drNew("FeeId") = CType(CType(dgrdPostTermFees.Items(i), DataGridItem).Controls(9), TableCell).Text 'PrgVerFeeId
                dtRes.Rows.Add(drNew)
            End If
        Next
        Dim ds As New DataSet("dsPostTermFees")
        ds.Tables.Add(dtRes.Copy())
        Return ds
    End Function

    Private Function AddPostTermFees(ByVal dsPostTermFees As DataSet) As String 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document

        Dim rtn As String = String.Empty

        If Not dsPostTermFees Is Nothing Then
            For Each lcol As DataColumn In dsPostTermFees.Tables(0).Columns
                lcol.ColumnMapping = MappingType.Attribute
            Next
            Dim strXml As String = dsPostTermFees.GetXml
            ' rtn = (New studentacc).PostClockAttendance(strXML)
            rtn = (New StudentsAccountsFacade).PostTermFees(strXml)

        End If
        Return ""
    End Function


    Protected Function CreatePostDataTableSchema() As DataTable
        Dim dt As New DataTable("InsertPostTermFees")
        'arStudent.LastName & arStudent.FirstName
        dt.Columns.Add("StuEnrollId", Type.GetType("System.Guid"))
        dt.Columns.Add("TermId", Type.GetType("System.Guid"))
        dt.Columns.Add("CampusId", Type.GetType("System.Guid"))
        dt.Columns.Add("TransDate", Type.GetType("System.DateTime"))
        dt.Columns.Add("TransCodeId", Type.GetType("System.Guid"))
        dt.Columns.Add("TransReference", Type.GetType("System.String")) 'transdescrip
        dt.Columns.Add("TransDescrip", Type.GetType("System.String"))   'transcodedescrip
        dt.Columns.Add("TransAmount", Type.GetType("System.Decimal"))
        dt.Columns.Add("ModUser", Type.GetType("System.String"))
        dt.Columns.Add("FeeLevelId", Type.GetType("System.Int32"))
        dt.Columns.Add("FeeId", Type.GetType("System.Guid"))

        Return dt
    End Function

    ''Added by Sara to maintain the checked state of the grid while exporting to Excel
    Private Sub RememberOldValues()
        Dim categoryIDList As New ArrayList()
        Dim index As Integer = -1
        ViewState("CHECKED_ITEMS") = Nothing
        For Each item As DataGridItem In dgrdPostTermFees.Items
            'Loop through each DataGridItem, and determine which CheckBox controls
            'have been selected.  chkRemove
            index = item.ItemIndex
            Dim myCheckbox As CheckBox = CType(item.FindControl("Chkremove"), CheckBox)
            If myCheckbox.Checked = True Then
                If Not categoryIDList.Contains(index) Then
                    categoryIDList.Add(index)
                End If

            End If

        Next
        If categoryIDList IsNot Nothing AndAlso categoryIDList.Count > 0 Then
            ViewState("CHECKED_ITEMS") = categoryIDList
        End If
    End Sub
    Private Sub RePopulateValues()
        Dim categoryIDList As ArrayList = DirectCast(ViewState("CHECKED_ITEMS"), ArrayList)
        If categoryIDList IsNot Nothing AndAlso categoryIDList.Count > 0 Then
            For Each item As DataGridItem In dgrdPostTermFees.Items
                Dim index As Integer = item.ItemIndex
                If categoryIDList.Contains(index) Then
                    Dim myCheckBox As CheckBox = CType(item.FindControl("Chkremove"), CheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub



End Class




