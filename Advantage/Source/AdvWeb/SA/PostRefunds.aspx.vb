﻿Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports Advantage.AFA.Integration
Imports FAME.Advantage.Api.Library.Models

Partial Class PostRefunds
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents lblProspect As System.Web.UI.WebControls.Label
    Protected WithEvents CompareValidator1 As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents RangeValidator1 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents lblAwardType As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected ResourceId As Integer
    Protected HasShowMessageForPostingToLastDisbursement As Boolean = False

    Protected campusId, userId As String
    Protected ModuleId As String
    Dim pobj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim SDFControls As New SDFComponent

        ResourceId = Trim(Request.QueryString("resid"))

        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId2 As Integer

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId2 = CInt(HttpContext.Current.Request.Params("resid"))
        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        pobj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pobj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        txtAcademicYearId.Attributes.Add("onClick", "needToConfirm = false;__doPostBack('lbtHasDisb','')")
        '   Disable Audit History at all time.
        'header1.EnableHistoryButton(False)

        If Not IsPostBack Then
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            InitButtonsForLoad(pobj)
            ViewState("MODE") = "NEW"
            
        ViewState("NotPostingToLastDisbursement") = HasShowMessageForPostingToLastDisbursement
            '   build dropdownlists
            BuildDropDownLists()

            ''   bind Awards repeater
            'BindStudentAwardsRepeater(txtStuEnrollmentId.Text)

            'Call The SDF Component
            '   initialize screen with transaction in session or an empty transaction
            If CommonWebUtilities.IsValidGuid(Session("TransactionId")) Then
                '   assign session value and destroy session
                ViewState("TransactionId") = Session("TransactionId")
                Session("TransactionId") = Nothing
                RadGrid1.Enabled = "false"

                '   bind PostChargeInfo in session
                With New StudentsAccountsFacade
                    'BindPostRefundData(.GetPostRefundInfo("2106E94F-FF17-4A6F-B327-48FF93A1FAEF"))
                    BindPostRefundData(.GetPostRefundInfo(ViewState("TransactionId")))
                End With

                '   if it is a reversal, create a new transaction
                If CType(Session("CommandName"), String) = "Reverse" Then

                    '   this is a reverse transaction
                    SetControls(False)

                    '   chkIsInDB
                    chkIsInDB.Checked = False

                    '   create a new guid
                    txtPostRefundId.Text = Guid.NewGuid.ToString

                    '   reverse the amount 
                    txtTransAmount.Text = CType(Decimal.Parse(txtTransAmount.Text), Decimal).ToString("#,###,###.00")

                    '   change original description
                    txtTransDescrip.Text += " - Adjustment For " + txtTransDate.SelectedDate.GetValueOrDefault()

                    '   set todays date
                    txtTransDate.SelectedDate = Date.Now.ToShortDateString

                    '   hide the header 
                    'header1.Visible = False

                    '   hide the footer
                    'footer1.Visible = False

                    '   hide the Header Div
                    'header.Visible = False

                    '   hide the Footer Div
                    'footer.Visible = False

                    '   in popUp windows the height of the scroll window should be bigger
                    'DIVRHS.Attributes.Add("style", " top:15px; height: expression(document.body.clientHeight + 'px');")

                    '   change the class to make up the space of the search button
                    'txtStudentName.CssClass = "TextBox"

                    '   Is Reversal
                    chkIsReversal.Checked = True

                    '   Original TransactionId
                    txtOriginalTransactionId.Text = ViewState("TransactionId")

                End If

                '   set Student Name textbox
                txtStudentName.Text = CType(Session("StudentName"), String)

                '   initialize buttons
                'InitButtonsForLoad()

            Else
                '   bind an empty new PostRefundInfo
                BindPostRefundData(New PostRefundInfo)
                '   initialize buttons for edit
                'InitButtonsForEdit()

            End If
            '   initialize buttons
            InitButtonsForLoad()

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            InitButtonsForEdit()
        End If

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = SDFControls.GetSDFExists(ResourceId, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If


        If Trim(txtPostRefundId.Text) <> "" Then
            SDFControls.GenerateControlsEdit(pnlSDF, ResourceId, txtPostRefundId.Text, ModuleId)
        Else
            SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
        End If

        ''Code Commented and Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
        'If CommonWebUtilities.IsValidGuid(txtStuEnrollmentId.Text) Then
        '    If rStudentAwards.Items.Count = 0 Then
        '        BindStudentAwardsRepeater(txtStuEnrollmentId.Text)
        '    End If
        'End If

        If CommonWebUtilities.IsValidGuid(txtStuEnrollmentId.Text) Then
            If RadGrid1.Items.Count = 0 Then
                BindStudentAwardsRepeater(txtStuEnrollmentId.Text)
            End If
        End If
        ''''''''''''''''''''''
    End Sub
    ''Subroutine Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
    Protected Sub RadGrid1_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid1.ItemCommand
        Dim studentAwards As New StuAwardsFacade

        If (e.CommandName = "ExpandCollapse") Then

            Dim rg As RadGrid
            Dim DI As GridDataItem
            DI = DirectCast(e.Item, GridDataItem)
            Dim tb As New TextBox
            rg = DirectCast(DI.ChildItem.FindControl("SubGrid"), RadGrid)
            tb = DirectCast(DI.FindControl("tbStudentAwardId"), TextBox)

            Dim awardScheduleDs = studentAwards.GetStudentAwardScheduleForRefunds(tb.Text)
            With rg
                .DataSource = awardScheduleDs
                .DataBind()

            End With
            If rg.Items.Count = 0 Then
                txtTransAmount.Enabled = True
            Else
                txtTransAmount.Enabled = False
            End If
        End If

    End Sub
    Private Sub BuildDropDownLists()
        'BuildBankAccountsDDL()
        'BuildFundSourcesDDL()
        'BuildTransCodesDDL()

        ''This is the list of ddls
        'Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        ''TransCodes DDL()
        'ddlList.Add(New AdvantageDDLDefinition(ddlTransCodeId, AdvantageDropDownListName.Trans_Codes, campusId, True, True))

        'CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)


        Dim refFAc As New RefundsFacade

        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeId"
            .DataSource = refFAc.GetTransCodesofTypeRefunds(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

        'Fund Sources DDL
        Dim stuAccounts As New StudentsAccountsFacade

        With ddlFundSourceId
            .DataTextField = "FundSourceDescrip"
            .DataValueField = "FundSourceId"
            .DataSource = stuAccounts.GetAllFundSourcesX("True", campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With



    End Sub
    Private Sub BindStudentAwardsRepeater(ByVal stuEnrollId As String)
        '   bind the studentAwards DDL
        Dim studentAwards As New StuAwardsFacade
        ''Code Commented and Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
        'With rStudentAwards
        '    .DataSource = studentAwards.GetAllStudentAwards(stuEnrollId)
        '    .DataBind()
        'End With

        'Dim rg As New RadGrid()
        'rg = DirectCast(Me.FindControl("RadGrid1"), RadGrid)
        ' '' New Code By Vijay Ramteke On August 25, 2010 For Mantis Id 19621
        ' ''rg.DataSource = studentAwards.GetAllStudentAwards(stuEnrollId)
        ' ''''' Code changes by kamalesh ahuja to resolve mantis issue id 19620
        ' ''rg.DataSource = studentAwards.GetAllStudentAwards(stuEnrollId, txtTransDate.Text)
        'rg.DataSource = studentAwards.GetAllStudentAwards(stuEnrollId)
        ' ''''''''
        ' '' New Code By Vijay Ramteke On August 25, 2010 For Mantis Id 19621
        'rg.DataBind()



        With RadGrid1
            .DataSource = studentAwards.GetAllStudentAwards(stuEnrollId, ddlFundSourceId.SelectedValue)
            .DataBind()
        End With

        If RadGrid1.Items.Count > 0 Then
            RadGrid1.Visible = True
            txtTransAmount.BackColor = Color.White
        Else
            ' DE7646 5/24/2012 Janet Robinson keep grid open
            'RadGrid1.Visible = False
        End If

        btnSave.Enabled = True
        If RadGrid1.Items.Count = 0 Then
            txtTransAmount.Enabled = True
        Else
            txtTransAmount.Enabled = False
        End If
        'If rStudentAwards.Items.Count > 0 Then
        '    rStudentAwards.Visible = True
        '    txtTransAmount.BackColor = Color.White
        'Else
        '    rStudentAwards.Visible = False
        'End If
        ''''''''''''''''''''''''''
    End Sub

    Private Sub BuildStudentAwardsDDL(ByVal stuEnrollId As String)
        '    '   bind the AwardType DDL
        '    Dim studentAwards As New StuAwardsFacade

        '    With ddlFundSourceId
        '        .DataTextField = "FundSourceDescrip"
        '        .DataValueField = "FundSourceId"
        '        .DataSource = studentAwards.GetAwardsPerStudent(stuEnrollId)
        '        .DataBind()
        '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '        .SelectedIndex = 0
        '    End With

    End Sub

    Private Sub BuildFundSourcesDDL()
        ''   bind the AwardType DDL
        'Dim stuAccounts As New StudentsAccountsFacade
        'With ddlFundSourceId
        '    .DataTextField = "FundSourceDescrip"
        '    .DataValueField = "FundSourceId"
        '    .DataSource = stuAccounts.GetAllFundSources("True")    'True' retrieves only Active Award Types
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With
    End Sub
    Private Sub BuildTransCodesDDL()

        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeId"
            .DataSource = (New StudentsAccountsFacade).GetAllTransCodes(False)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub BuildBankAccountsDDL()
    '    '   bind the AwardTypes DDL
    '    Dim bankAccounts As New StudentsAccountsFacade

    '    With ddlBankAcctId
    '        .DataTextField = "BankAcctDescrip"
    '        .DataValueField = "BankAcctId"
    '        .DataSource = bankAccounts.GetAllBankAccounts(False)
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BindPostRefundData(ByVal PostRefundInfo As PostRefundInfo)
        With PostRefundInfo
            chkIsInDB.Checked = .IsInDB
            'chkIsPosted.Checked = .IsPosted
            chkIsPosted.Checked = True
            txtPostRefundId.Text = .PostRefundId
            txtStuEnrollmentId.Text = .StuEnrollId
            txtStuEnrollment.Text = .EnrollmentDescription
            txtTransDescrip.Text = .PostRefundDescription
            ddlTransCodeId.SelectedValue = .TransCodeId

            '   set blank if the date is null 
            If Not (.PostRefundDate = Date.MinValue) Then
                txtTransDate.SelectedDate = .PostRefundDate.ToShortDateString()
            Else
                txtTransDate.SelectedDate = ""
            End If

            txtTransAmount.Text = CType(.Amount * (-1), Decimal).ToString("#,###,###.00")
            txtTransReference.Text = .Reference
            txtAcademicYearId.Text = .AcademicYearId
            txtAcademicYear.Text = .AcademicYearDescrip
            txtTermId.Text = .TermId
            txtTerm.Text = .TermDescrip
            cbIsInstCharge.Checked = .IsInstCharge
            'drvTransactionDate.MinimumValue = .EarliestAllowedDate.Date.Year.ToString("000#") + "/" + .EarliestAllowedDate.Date.Month.ToString("0#") + "/" + .EarliestAllowedDate.Date.Day.ToString("0#")
            'drvTransactionDate.MaximumValue = .LatestAllowedDate.Date.Year.ToString("000#") + "/" + .LatestAllowedDate.Date.Month.ToString("0#") + "/" + .LatestAllowedDate.Date.Day.ToString("0#")
            'drvTransactionDate.ErrorMessage = String.Format("Date must be between {0} and {1}", .EarliestAllowedDate.ToShortDateString, .LatestAllowedDate.ToShortDateString)
            'cblRefundTypeId.SelectedValue = .RefundTypeId

            'BuildStudentAwardsDDL(txtStuEnrollmentId.Text)
            'ddlFundSourceId.SelectedValue = .FundSourceId

            'ddlBankAcctId.SelectedValue = .BankAcctId
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            txtModUser1.Text = .ModUser1
            txtModDate1.Text = .ModDate1.ToString

            ' DE7646 5/24/2012 Janet Robinson fill user control fields display only
            If Not ViewState("TransactionId") Is Nothing Then
                txtStudentDisp.Text = .StudentName
                txtEnrollDisp.Text = .EnrollmentDescription
                txtTermDisp.Text = .TermDescrip
                txtAcadYearDisp.Text = .AcademicYearDescrip
                trStuSearchControl.Visible = False
                pnlDisplayInfo.Visible = True
            Else
                trStuSearchControl.Visible = True
                pnlDisplayInfo.Visible = False
            End If

        End With






    End Sub
    Private Async Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   perform local validation. Set proper error message
        If txtTransAmount.Text.Trim = "" Then txtTransAmount.Text = ".00"
        If Not LocalRecordIsValid(Customvalidator1) Then Return

        ''Code Added and commented by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
        Dim postedRefunds As String = String.Empty
        Dim postedRefundsNew As String = String.Empty

        Dim message As String = String.Empty
        HasShowMessageForPostingToLastDisbursement = ViewState("NotPostingToLastDisbursement")
        If HasShowMessageForPostingToLastDisbursement AndAlso Not IsRefundPostedToLastRDisbursement(message) Then
            DisplayWarningMessage(message)
            HasShowMessageForPostingToLastDisbursement = false
            ViewState("NotPostingToLastDisbursement") = HasShowMessageForPostingToLastDisbursement
            Exit Sub
        End If
        'If Not IsInputValid() Then
        '    DisplayErrorMessage(GetErrorMessage())
        '    Exit Sub
        'End If
        Dim returnIsValidMessage = String.Empty
        If Not IsInputValidNew(returnIsValidMessage) Then
            If returnIsValidMessage = String.Empty Then
                DisplayErrorMessage(GetErrorMessageNew())
            Else
                DisplayErrorMessage(returnIsValidMessage)
            End If
            Exit Sub
        End If

        'GetPostedRefunds(postedRefunds)






        '''''''''''''''15 Jan 2010 ''''''''''

        '   instantiate component
        Dim saf As New StudentsAccountsFacade


        Dim PostRefundInfo As PostRefundInfo
        Dim boolAwardDup As Boolean = False
        Dim boolDup As Boolean = False

        '   update PostRefund Info 
        ''Code Added and commented Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425

        'Dim result As String = saf.UpdatePostRefundInfo(BuildPostRefundInfo(txtPostRefundId.Text), postedRefunds, AdvantageSession.UserState.UserName)


        GetPostedRefundsNew(postedRefundsNew)
        PostRefundInfo = BuildPostRefundInfo(txtPostRefundId.Text)
        boolAwardDup = getRadGridDuplicates(postedRefundsNew, PostRefundInfo)

        If boolAwardDup Then
            RadWindowManager1.RadConfirm("Save Record?", "confirmCallBackFn", 300, 100, Nothing, "Duplicate Record Already Exists")

        Else

            boolDup = saf.CheckTransDuplicateRefund(PostRefundInfo)
            If boolDup Then

                'ViewState("DisbArrList") = DisbArrList
                'ViewState("arrApplyPmt") = arrApplyPmt
                RadWindowManager1.RadConfirm("Save Record?", "confirmCallBackFn", 300, 100, Nothing, "Duplicate Record Already Exists")
            Else

                Dim resultNew As String = saf.UpdatePostRefundInfoNew(PostRefundInfo, postedRefundsNew, AdvantageSession.UserState.UserName)

                If resultNew <> "" Then
                    '   Display Error Message
                    DisplayErrorMessage(resultNew)
                Else
                    Try
                        Dim afaIntegrationEnabled = If(Not String.IsNullOrEmpty(MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration, campusId)) _
                            , MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration, campusId) _
                                , MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration).ToString).Trim.ToLower = "yes"

                        If (afaIntegrationEnabled) Then
                            Dim fundSourceId = Guid.Parse(PostRefundInfo.FundSourceId)
                            Dim isTitleIVFundSource = New FundSourcesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString).IsFundSourceTitleIV(fundSourceId)

                            If (isTitleIVFundSource) Then
                                If (Not String.IsNullOrEmpty(postedRefundsNew)) Then
                                    Dim refundsToPost = New List(Of Refund)
                                    Dim guidCampusId = Guid.Parse(campusId)
                                    Dim refunds As String() = postedRefundsNew.Split(":")
                                    For i As Integer = 0 To refunds.Length - 1
                                        If postedRefundsNew.Length > 0 Then
                                            Dim refundField() = refunds(i).Split(";")
                                            Dim refund = New Refund() With {
                                                .StudentEnrollmentId = Guid.Parse(txtStuEnrollmentId.Text),
                                                .CampusId = guidCampusId,
                                                .AdjustmentAmount = Decimal.Parse(refundField(1)),
                                                .AdjustmentDate = PostRefundInfo.PostRefundDate,
                                                .StudentAwardId = Guid.Parse(refundField(0)),
                                                .AwardDisbursementId = Guid.Parse(refundField(4))}

                                            Dim ssn = New LeadDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString) _
                                             .GetSSN(refund.StudentEnrollmentId)

                                            Dim studentAwardDA = New StudentAwardDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

                                            Dim faId = studentAwardDA.GetFinancialAidId(refund.StudentAwardId)
                                            Dim disbursementNumber = studentAwardDA.GetAFADisbursementNumber(refund.AwardDisbursementId)

                                            If (Not String.IsNullOrEmpty(ssn) AndAlso Not String.IsNullOrEmpty(faId) AndAlso Not disbursementNumber = 0) Then
                                                refund.SSN = ssn
                                                refund.FinancialAidId = faId
                                                refund.DisbursementNumber = disbursementNumber
                                                refundsToPost.Add(refund)
                                            End If

                                        End If
                                    Next
                                    If (refundsToPost.Any) Then
                                        Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
                                        Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString

                                        'Send updated to AFA if integration is enabled(handled internally)
                                        Dim helper = New StudentAwardHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, AdvantageSession.UserState.UserName)

                                        Dim res = Await helper.PostDisbursementAdjustment(campusId, refundsToPost)
                                    End If
                                End If
                            End If
                        End If

                    Catch ex As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)


                    End Try

                    'If result <> "" Then
                    '    '   Display Error Message
                    '    DisplayErrorMessage(result)
                    'Else
                    ''''''''''' 15 Jan 2010 ''''''''''''
                    '   If there are no errors bind an empty entity and init buttons
                    If Not CommonWebUtilities.IsValidGuid(ViewState("TransactionId")) Then

                        ' DE7646 5/24/2012 Janet Robinson keep grid open
                        Dim strStuEnrollId As String = txtStuEnrollmentId.Text

                        '   bind an empty new PostRefundInfo
                        BindPostRefundData(New PostRefundInfo)

                        'this will disable the displaying of the refunds grid
                        txtStuEnrollmentId.Text = String.Empty

                        ''Code Added and commented Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
                        'With rStudentAwards
                        '    .DataSource = Nothing
                        '    .DataBind()
                        'End With
                        'rStudentAwards.Visible = False

                        With RadGrid1
                            .DataSource = Nothing
                            .DataBind()
                        End With
                        ' DE7646 5/24/2012 Janet Robinson keep grid open
                        RadGrid1.Visible = True
                        ' DE7646 End
                        '''''''''' 15 Jan 2010 '''''''''''''''''

                        '   initialize buttons
                        InitButtonsForLoad()

                        ' DE7646 5/24/2012 Janet Robinson keep grid open
                        txtStuEnrollmentId.Text = strStuEnrollId
                        If CommonWebUtilities.IsValidGuid(txtStuEnrollmentId.Text) Then
                            If RadGrid1.Items.Count = 0 Then
                                BindStudentAwardsRepeater(txtStuEnrollmentId.Text)
                                RadGrid1.MasterTableView.HierarchyDefaultExpanded = False
                            End If
                        End If
                        ' DE7646 End

                    Else
                        '   bind PostRefundInfo in session
                        With New StudentsAccountsFacade
                            BindPostRefundData(.GetPostRefundInfo(ViewState("TransactionId")))
                        End With
                        '   initialize buttons for edit
                        InitButtonsForEdit()
                    End If
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields When Save Button Is Clicked

                Dim SDFID As ArrayList
                Dim SDFIDValue As ArrayList
                '        Dim newArr As ArrayList
                Dim z As Integer
                Dim SDFControl As New SDFComponent
                Try
                    SDFControl.DeleteSDFValue(txtPostRefundId.Text)
                    SDFID = SDFControl.GetAllLabels(pnlSDF)
                    SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                    For z = 0 To SDFID.Count - 1
                        SDFControl.InsertValues(txtPostRefundId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                    Next
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                End Try
            End If
        End If
        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '   if we are in a popUp window then close the window
        'If Not Header1.Visible Then
        '    CommonWebUtilities.CloseWindow(Page)
        'End If

    End Sub
    Private Function BuildPostRefundInfo(ByVal PostRefundId As String) As PostRefundInfo

        '   instantiate class
        Dim PostRefundInfo As New PostRefundInfo

        With PostRefundInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get IsPosted
            .IsPosted = chkIsPosted.Checked

            '   get PostRefundId
            .PostRefundId = txtPostRefundId.Text

            '   get StuEnrollId
            .StuEnrollId = txtStuEnrollmentId.Text

            '   get CampusId
            .CampusId = campusId

            '   get TransCodeId
            .TransCodeId = ddlTransCodeId.SelectedValue

            '   get PostRefund Description
            .PostRefundDescription = txtTransDescrip.Text.Trim

            '   get PostRefundDate
            '.PostRefundDate = Date.Parse(txtTransDate.Text)
            .PostRefundDate = Date.Parse(txtTransDate.SelectedDate)

            '   get Amount
            .Amount = Decimal.Parse(txtTransAmount.Text) * (-1)

            '   get IsInstCharge
            .IsInstCharge = cbIsInstCharge.Checked

            '   get Reference
            .Reference = txtTransReference.Text.Trim

            '   get Academic Year
            If txtAcademicYearId.Text = "00000000-0000-0000-0000-000000000000" Then
                .AcademicYearId = Session("AcademicYearId")
            Else
                .AcademicYearId = txtAcademicYearId.Text
            End If


            '   get Term
            If txtTermId.Text = "00000000-0000-0000-0000-000000000000" Then
                .TermId = Session("TermId")
            Else
                .TermId = txtTermId.Text
            End If
            '.TermId = txtTermId.Text

            ''   get RefundTypeId
            '.RefundTypeId = cblRefundTypeId.SelectedValue

            ''   get FundSourceId
            '.FundSourceId = ddlFundSourceId.SelectedValue

            '   get BankAcctId
            '.BankAcctId = ddlBankAcctId.SelectedValue

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

            '   get ModUser1
            .ModUser1 = txtModUser1.Text

            '   get ModDate1
            .ModDate1 = Date.Parse(txtModDate1.Text)

            '   Is Reversal
            .IsReversal = chkIsReversal.Checked

            '   Original transactionId
            If chkIsReversal.Checked Then .OriginalTransactionId = txtOriginalTransactionId.Text


        End With

        '   return data
        Return PostRefundInfo

    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        '   bind an empty new PostRefundInfo
        BindPostRefundData(New PostRefundInfo)

        'this will disable the displaying of the refunds grid
        txtStuEnrollmentId.Text = String.Empty

        With RadGrid1
            .DataSource = Nothing
            .DataBind()
        End With
        RadGrid1.Visible = True

        Dim AdvCtrl As AdvControls.IAdvControl = CType(StudSearch1, AdvControls.IAdvControl)
        AdvCtrl.Clear()

        '   initialize buttons
        InitButtonsForLoad()

        '   set session variable to nothing
        Session("RefundTransactionId") = Nothing

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtPostRefundId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim saf As New StudentsAccountsFacade

            '   update PostRefundInfo 
            Dim result As String = saf.DeletePostRefundInfo(txtPostRefundId.Text, Date.Parse(txtModDate.Text), Date.Parse(txtModDate1.Text))
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else
                '   bind an empty new PostRefundInfo
                BindPostRefundData(New PostRefundInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim SDFControl As New SDFComponent
            SDFControl.DeleteSDFValue(txtPostRefundId.Text)
            SDFControl.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        '   in popup windows new and delete buttons should be disabled
        'If Header1.Visible Then
        '    btnNew.Enabled = True
        '    btnDelete.Enabled = False
        '    'cblRefundTypeId.Enabled = True

        'Else
        '    btnDelete.Enabled = False
        '    btnNew.Enabled = False
        '    'cblRefundTypeId.Enabled = True

        'End If

        'txtStudentName.Text = ""
        If pobj.HasFull Or pobj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pobj.HasFull Or pobj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False

        ' DE7646 5/24/2012 Janet Robinson allow save only
        If Not ViewState("TransactionId") Is Nothing Then
            btnDelete.Enabled = False
            btnNew.Enabled = False
        End If

    End Sub
    Private Sub InitButtonsForLoad(ByVal pobj As UserPagePermissionInfo)
        'If pobj.HasFull Or pobj.HasAdd Then
        '    btnSave.Enabled = True
        '    btnNew.Enabled = Header1.Visible
        '    'cblRefundTypeId.Enabled = True

        'Else
        '    btnSave.Enabled = False
        '    btnNew.Enabled = Header1.Visible
        '    'cblRefundTypeId.Enabled = True

        'End If
        'btnDelete.Enabled = False
        If pobj.HasFull Or pobj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pobj.HasFull Or pobj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False

        ' DE7646 5/24/2012 Janet Robinson allow save only
        If Not ViewState("TransactionId") Is Nothing Then
            btnDelete.Enabled = False
            btnNew.Enabled = False
        End If

    End Sub
    Private Sub InitButtonsForEdit()
        '   in popup windows new and delete buttons should be disabled
        'If Header1.Visible Then
        '    btnNew.Enabled = True
        '    btnDelete.Enabled = True
        '    'cblRefundTypeId.Enabled = True


        '    'Set the Delete Button so it prompts the user for confirmation when clicked
        '    btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        'Else
        '    btnNew.Enabled = False
        '    btnDelete.Enabled = False
        '    'cblRefundTypeId.Enabled = True

        'End If
        If pobj.HasFull Or pobj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pobj.HasFull Or pobj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pobj.HasFull Or pobj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        ' DE7646 5/24/2012 Janet Robinson allow save only
        If Not ViewState("TransactionId") Is Nothing Then
            btnDelete.Enabled = False
            btnNew.Enabled = False
        End If

    End Sub
    Private Function LocalRecordIsValid(ByVal customValidator As WebControls.CustomValidator) As Boolean

        '   record is valid
        Return True

    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub


    Private Sub DisplayWarningMessage(ByVal warningMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = warningMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayWarningInMessageBox(Me.Page, warningMessage)
        End If

    End Sub
    'Private Sub lbtSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtSearch.Click

    '    ''   reset Refund Type radio button
    '    'If cblRefundTypeId.SelectedValue = RefundType.ToFinancialAidSource Then
    '    '    cblRefundTypeId.SelectedValue = RefundType.ToStudent
    '    'End If

    '    '   setup the properties of the new window
    '    Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px"
    '    Dim name As String = "StudentPopupSearch2"
    '    'Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx?resid=293&mod=SA&cmpid=" + campusId
    '    ''resourceId modified by Sarswathi on june 10 2009
    '    ''the Current page's resourceId is passed intead of the popupsearch's resId
    '    ''Added to fix issue 14829 --changed from 293- 94
    '    Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx?resid=94&mod=SA&cmpid=" + campusId

    '    '   build list of parameters to be passed to the new window
    '    Dim parametersArray(6) As String

    '    '   set StudentName
    '    parametersArray(0) = txtStudentName.ClientID

    '    '   set StuEnrollment
    '    parametersArray(1) = txtStuEnrollmentId.ClientID
    '    parametersArray(2) = txtStuEnrollment.ClientID

    '    '   set Terms
    '    parametersArray(3) = txtTermId.ClientID
    '    parametersArray(4) = txtTerm.ClientID

    '    '   set AcademicYears
    '    parametersArray(5) = txtAcademicYearId.ClientID
    '    parametersArray(6) = txtAcademicYear.ClientID

    '    '   open new window and pass parameters
    '    CommonWebUtilities.OpenWindowAndPassParameters(Page, url, name, winSettings, parametersArray)

    'End Sub

    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String,
                           ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String,
                           ByVal hourtype As String)


        If enrollid = "" Then enrollid = Session("StuEnrollmentId")
        If fullname = "" Then fullname = Session("StudentName")
        If termid = "" Then termid = Session("TermId")
        If termdescrip = "" Then termdescrip = Session("Term")
        If academicyearid = "" Then academicyearid = Session("AcademicYearId")

        txtStuEnrollmentId.Text = enrollid
        txtTermId.Text = termid
        txtTerm.Text = termdescrip
        txtAcademicYearId.Text = academicyearid
        'txtAcademicYear.Text = academicyeardescrip

        Session("StuEnrollmentId") = txtStuEnrollmentId.Text
        Session("StudentName") = fullname
        Session("TermId") = txtTermId.Text
        Session("Term") = txtTerm.Text
        Session("AcademicYearId") = txtAcademicYearId.Text

        'BindStudentAwardsRepeater(txtStuEnrollmentId.Text)
    End Sub

    Private Sub SetControls(ByVal val As Boolean)
        ddlTransCodeId.Enabled = val
        'txtStudentName.Enabled = val
        'lbtSearch.Visible = val
        'Img2.Visible = val
        'StudentSearchIcon.Visible = val
        txtStuEnrollment.Enabled = val
        txtTransDescrip.Enabled = val
        txtTransDate.Enabled = val
        'drvTransactionDate.Enabled = val
        txtTransAmount.Enabled = val
        'AmountRangeValidator.Enabled = val
        txtTransReference.Enabled = val
        txtAcademicYear.Enabled = val
        txtTerm.Enabled = val
        'ddlBankAcctId.Enabled = val
        'cblRefundTypeId.Enabled = val
        'ddlFundSourceId.Enabled = val
    End Sub

    'Private Sub cblRefundTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblRefundTypeId.SelectedIndexChanged
    '    If cblRefundTypeId.SelectedValue = RefundType.ToFinancialAidSource And _
    '            txtStuEnrollmentId.Text <> System.Guid.Empty.ToString Then
    '        BuildStudentAwardsDDL(txtStuEnrollmentId.Text)
    '        lblFundSourceId.Visible = True
    '        ddlFundSourceId.Visible = True
    '    End If
    'End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

        If txtStuEnrollmentId.Text <> System.Guid.Empty.ToString Then

            'Select Case cblRefundTypeId.SelectedValue
            '    Case RefundType.ToFinancialAidSource
            '        '   show Fund Sources DDL and label
            '        lblFundSourceId.Visible = True
            '        ddlFundSourceId.Visible = True

            '    Case Else
            '        '   hide Fund Sources DDL and label
            '        lblFundSourceId.Visible = False
            '        ddlFundSourceId.Visible = False
            'End Select

        Else
            '   hide Fund Sources DDL and label
            'lblFundSourceId.Visible = False
            'ddlFundSourceId.Visible = False
        End If
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'controlsToIgnore.Add(CalButton)
        'controlsToIgnore.Add(lbtHasDisb)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    'Private Sub lbtHasDisb_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtHasDisb.Click
    '   Modified by Michelle R. Rodriguez on 10/12/2005
    '   Moved from lblSearch_Click because it is now done cliend-side, so it would never be executed.
    '   uncheck Schedule Payment checkbox
    'lblFundSourceId.Visible = False
    'ddlFundSourceId.Visible = False
    'cblRefundTypeId.SelectedValue = RefundType.ToStudent
    'If (txtStuEnrollmentId.Text <> "") Then
    '    If ((New StuAwardsFacade).GetAwardsPerStudent(txtStuEnrollmentId.Text).Tables(0).Rows.Count = 0) Then
    '        cblRefundTypeId.Enabled = False
    '    Else
    '        cblRefundTypeId.Enabled = True<asp:TextBox ID="txtStudentName" TabIndex="1" runat="server" CssClass="textboxsearch"
    'contentEditable="false"></asp:TextBox>
    '    End If

    'End If

    'End Sub
    Private Sub GetPostedRefunds(ByRef postedRefunds As String)
        Dim sb As New StringBuilder()
        Dim list As RepeaterItemCollection = rStudentAwards.Items
        For Each item As RepeaterItem In list
            If item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.Item Then
                Dim tbAmount As TextBox = CType(item.FindControl("tbRefundAmount"), TextBox)
                If Not (tbAmount.Text.Trim = String.Empty) Then
                    Dim tbStudentAwardId As TextBox = CType(item.FindControl("tbStudentAwardId"), TextBox)
                    Dim cbRefundTo As CheckBox = CType(item.FindControl("cbRefundTo"), CheckBox)
                    If sb.Length > 0 Then sb.Append(",")
                    sb.Append(tbStudentAwardId.Text)
                    sb.Append(";")
                    sb.Append(tbAmount.Text)
                    sb.Append(";")
                    Dim tbFundSourceId As TextBox = CType(item.FindControl("tbFundSourceId"), TextBox)
                    sb.Append(tbFundSourceId.Text)
                    sb.Append(";")
                    If cbRefundTo.Checked Then
                        sb.Append("0")
                    Else
                        sb.Append("1")
                    End If
                End If
            End If
        Next
        postedRefunds = sb.ToString()
    End Sub
    ''Function Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
    Private Sub GetPostedRefundsNew(ByRef postedRefunds As String)
        Dim sb As New StringBuilder()
        Dim RdMain As RadGrid
        Dim RdSub As RadGrid
        Dim tbStudentAwardId As New TextBox
        Dim tbFundsourceId As New TextBox

        RdMain = DirectCast(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("RadGrid1"), RadGrid)

        For Each Gr As GridDataItem In RdMain.Items
            If Gr.Expanded = True Then

                tbStudentAwardId = DirectCast(Gr.FindControl("tbStudentAwardId"), TextBox)
                tbFundsourceId = DirectCast(Gr.FindControl("tbFundSourceId"), TextBox)
                RdSub = DirectCast(Gr.ChildItem.FindControl("SubGrid"), RadGrid)

                For Each Sgr As GridDataItem In RdSub.Items
                    Dim tbRefAmt As New TextBox
                    Dim tbAwardScheduleId As TextBox
                    Dim cbRefundTo As CheckBox = DirectCast(Sgr.FindControl("cbRefundTo"), CheckBox)
                    tbRefAmt = DirectCast(Sgr.FindControl("tbDetRefundAmount"), TextBox)
                    tbAwardScheduleId = DirectCast(Sgr.FindControl("tbAwardScheduleId"), TextBox)
                    If Not (tbRefAmt.Text = String.Empty) Then
                        Try
                            If Decimal.Parse(tbRefAmt.Text) > 0 Then
                                If sb.Length > 0 Then sb.Append(":")
                                sb.Append(tbStudentAwardId.Text)
                                sb.Append(";")
                                sb.Append(tbRefAmt.Text)
                                sb.Append(";")
                                sb.Append(tbFundsourceId.Text)
                                sb.Append(";")
                                If cbRefundTo.Checked Then
                                    sb.Append("0")
                                Else
                                    sb.Append("1")
                                End If
                                sb.Append(";")
                                sb.Append(tbAwardScheduleId.Text)
                            End If
                        Catch ex As Exception
                            Dim exTracker = New AdvApplicationInsightsInitializer()
                            exTracker.TrackExceptionWrapper(ex)


                        End Try
                    End If
                Next

            End If
        Next

        If (sb.Length = 0) And (txtTransAmount.Text <> "") Then
            sb.Append("")
            sb.Append(";")
            sb.Append(txtTransAmount.Text)
            sb.Append(";")
            sb.Append(ddlFundSourceId.SelectedValue)
            sb.Append(";")
            If chkRefundToStudent.Checked = True Then
                sb.Append("0")
            Else
                sb.Append("1")
            End If

            sb.Append(";")
            sb.Append("")
        End If

        postedRefunds = sb.ToString()
    End Sub
    ''Function Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
    Private Function IsInputValidNew(ByRef returnMessage As String) As Boolean
        Dim amount As Decimal = 0.0
        Dim RdMain As RadGrid
        Dim RdSub As RadGrid
        Dim itemCount As Integer = 0
        Dim fundSourceSelected = ddlFundSourceId.SelectedValue
        Dim isFundSourceDirectLoan = New FundSourcesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString).IsFundSourceDirectLoan(fundSourceSelected)

        RdMain = DirectCast(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("RadGrid1"), RadGrid)
        For Each Gr As GridDataItem In RdMain.Items
            If Gr.Expanded = True Then

                RdSub = DirectCast(Gr.ChildItem.FindControl("SubGrid"), RadGrid)
                For Each Sgr As GridDataItem In RdSub.Items
                    Dim TbAmount As New Label
                    Dim RefAmt As New TextBox
                    TbAmount = DirectCast(Sgr.FindControl("lScheduleAmount"), Label)
                    RefAmt = DirectCast(Sgr.FindControl("tbDetRefundAmount"), TextBox)
                    If Not (RefAmt.Text = String.Empty) Then
                        If Not Decimal.TryParse(RefAmt.Text, amount) Then RefAmt.Text = "0.00"
                        If (isFundSourceDirectLoan AndAlso Decimal.Truncate(amount) <> amount) Then
                            returnMessage = "Direct Loan cannot be refunded in cents."
                            Return False
                        End If
                        Dim amt As String = TbAmount.Text
                        If TbAmount.Text.Substring(0, 1) = "$" Then
                            amt = TbAmount.Text.Substring(1)
                        End If
                        If Decimal.Parse(amt) < amount Then Return False
                        If Decimal.Parse(RefAmt.Text) > 0 Then
                            itemCount += 1
                        End If
                    End If

                Next

            End If
        Next


        If Not Decimal.TryParse(txtTransAmount.Text, amount) Then
            Return False
        End If

        If amount = 0.0 And itemCount = 0 Then
            Return False
        End If
        If amount > 0 And itemCount > 0 Then
            Return False
        End If
        Return True
    End Function
    Private Function GetErrorMessage() As String
        Dim sb As New StringBuilder()
        Dim amount As Decimal = 0.0
        Dim itemCount As Integer = 0
        Dim list As RepeaterItemCollection = rStudentAwards.Items
        For Each item As RepeaterItem In list
            If item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.Item Then
                Dim failed As Boolean = False
                Dim tbAmount As TextBox = CType(item.FindControl("tbRefundAmount"), TextBox)
                If Not (tbAmount.Text = String.Empty) Then
                    If Not Decimal.TryParse(tbAmount.Text, amount) Then
                        sb.Append("The amount entered for the award ")
                        Dim lFundSource As Label = CType(item.FindControl("lFundSource"), Label)
                        sb.Append(lFundSource.Text)
                        sb.Append(" dated ")
                        Dim lReference As Label = CType(item.FindControl("lReference"), Label)
                        sb.Append(lReference.Text)
                        sb.Append(" with original amount ")
                        Dim lAmount As Label = CType(item.FindControl("lAmount"), Label)
                        sb.Append(lAmount.Text)
                        sb.Append(" is invalid.")
                        sb.Append(vbCrLf)
                        failed = True
                    End If
                    Dim lAmount1 As Label = CType(item.FindControl("lAmount"), Label)
                    If Decimal.Parse(lAmount1.Text) < amount And Not failed Then
                        sb.Append("The amount entered for the award ")
                        Dim lFundSource As Label = CType(item.FindControl("lFundSource"), Label)
                        sb.Append(lFundSource.Text)
                        sb.Append(" dated ")
                        Dim lReference As Label = CType(item.FindControl("lReference"), Label)
                        sb.Append(lReference.Text)
                        sb.Append(" with original amount ")
                        Dim lAmount As Label = CType(item.FindControl("lAmount"), Label)
                        sb.Append(lAmount.Text)
                        sb.Append(" is invalid. It is greater than the award amount.")
                        sb.Append(vbCrLf)
                        failed = True
                    End If
                End If
                If Not failed Then itemCount += 1
            End If
        Next
        If Not Decimal.TryParse(txtTransAmount.Text, amount) Then
            sb.Append("The entered amount ")
            sb.Append(txtTransAmount.Text)
            sb.Append(" is invalid.")
            sb.Append(vbCrLf)
        End If
        If Not (amount = 0.0) And itemCount > 0 Then
            sb.Append("You should not enter a general amount and one or more refund amounts.")
            sb.Append(vbCrLf)
        End If
        If amount = 0.0 And itemCount = 0 Then
            sb.Append("You did not enter any amount.")
            sb.Append(vbCrLf)
        End If
        Return sb.ToString()
    End Function
    ''Function Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
    Private Function GetErrorMessageNew() As String
        Dim sb As New StringBuilder()
        Dim amount As Decimal = 0.0
        Dim itemCount As Integer = 0
        Dim RdMain As RadGrid
        Dim RdSub As RadGrid

        RdMain = DirectCast(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("RadGrid1"), RadGrid)
        For Each Gr As GridDataItem In RdMain.Items
            If Gr.Expanded = True Then
                Dim failed As Boolean = False
                Dim lFundSource As New Label
                Dim lReference As New Label
                Dim lMainAmount As New Label

                RdSub = DirectCast(Gr.ChildItem.FindControl("SubGrid"), RadGrid)
                lFundSource = DirectCast(Gr.FindControl("lFundSource"), Label)
                lReference = DirectCast(Gr.FindControl("lReference"), Label)
                lMainAmount = DirectCast(Gr.FindControl("lAmount"), Label)

                For Each Sgr As GridDataItem In RdSub.Items
                    Dim TbAmount As New TextBox
                    Dim RefAmt As New TextBox
                    Dim tbScheduledAmount As New Label

                    TbAmount = DirectCast(Sgr.FindControl("tbDetRefundAmount"), TextBox)
                    tbScheduledAmount = DirectCast(Sgr.FindControl("lScheduleAmount"), Label)
                    If Not (TbAmount.Text = String.Empty) Then
                        If CDec(TbAmount.Text) > 0 And CDec(txtTransAmount.Text) > 0 Then
                            'sb.Append("You should not enter a general amount and one or more refund amounts.")
                            'sb.Append(vbCrLf)
                            failed = True
                        End If

                        If Not Decimal.TryParse(TbAmount.Text, amount) Then
                            sb.Append("The refund amount entered for the scheduled disbursement ")
                            sb.Append(lFundSource.Text)
                            sb.Append("dated ")
                            sb.Append(lReference.Text)
                            sb.Append("with net received amount ")
                            sb.Append(lMainAmount.Text)
                            sb.Append("is invalid.")
                            sb.Append(vbCrLf)
                            failed = True
                        End If

                        Dim schAmount As String = tbScheduledAmount.Text
                        If schAmount.Substring(0, 1) = "$" Then
                            schAmount = schAmount(1)
                        End If
                        If Decimal.Parse(schAmount) < amount And Not failed Then
                            sb.Append("The refund amount entered for the scheduled disbursement ")
                            sb.Append(lFundSource.Text)
                            sb.Append("dated ")
                            sb.Append(lReference.Text)
                            sb.Append("with net received amount ")
                            sb.Append(tbScheduledAmount.Text)
                            sb.Append(" is invalid. The refund amount is greater than the net received amount.")
                            sb.Append(vbCrLf)
                            failed = True
                        End If
                    End If
                    If failed Then itemCount += 1
                    failed = False
                Next
            End If
        Next

        If Not Decimal.TryParse(txtTransAmount.Text, amount) Then
            sb.Append("The entered amount ")
            sb.Append(txtTransAmount.Text)
            sb.Append(" is invalid.")
            sb.Append(vbCrLf)
        End If
        If Not (amount = 0.0) And itemCount > 0 Then
            sb.Append("You should not enter a general amount and one or more refund amounts.")
            sb.Append(vbCrLf)
        End If
        If amount = 0.0 And itemCount = 0 Then
            sb.Append("You did not enter any amount.")
            sb.Append(vbCrLf)
        End If
        Return sb.ToString()
    End Function
    'Protected Sub txtStuEnrollmentId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStuEnrollmentId.TextChanged
    '    BindStudentAwardsRepeater(txtStuEnrollmentId.Text)
    'End Sub

    Private Function IsRefundPostedToLastRDisbursement(ByRef message As String) As Boolean
        Dim itemCount As Integer = 0
        Dim RdMain As RadGrid
        Dim RdSub As RadGrid
        Dim isToLastDisbursement = False
        RdMain = DirectCast(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("RadGrid1"), RadGrid)
        Dim totalAmount = 0.0
        For Each Gr As GridDataItem In RdMain.Items

            If Gr.Expanded = True Then
                Dim failed As Boolean = False
                Dim lFundSource As New Label
                Dim lReference As New Label
                Dim lMainAmount As New Label

                RdSub = DirectCast(Gr.ChildItem.FindControl("SubGrid"), RadGrid)
                For Each Sgr As GridDataItem In RdSub.Items
                    itemCount = itemCount + 1
                    Dim TbAmount As New TextBox
                    Dim RefAmt As New TextBox
                    Dim tbScheduledAmount As New Label

                    TbAmount = DirectCast(Sgr.FindControl("tbDetRefundAmount"), TextBox)

                    Dim refundAmount = 0.0
                    Dim value = Decimal.TryParse(TbAmount.Text, refundAmount)
                    totalAmount = refundAmount + totalAmount
                    If (isToLastDisbursement = False AndAlso itemCount = 1 AndAlso refundAmount > 0) Then
                        isToLastDisbursement = True

                    Else

                    End If

                Next
            End If
        Next
        If totalAmount = 0 Then
            isToLastDisbursement = True
        End If
        If Not isToLastDisbursement And totalAmount > 0 Then
            message = "Refund amount is not being posted against the latest disbursement. If you wish to proceed, select OK and save again. Otherwise, change the value to the latest disbursement and select Save."
        End If
        'txtTransAmount.Text = totalAmount
        Return isToLastDisbursement
    End Function

    Protected Sub StudSearch1_TransferToParent(enrollid As String, fullname As String, termid As String, termdescrip As String, academicyearid As String, academicyeardescrip As String, hourtype As String) Handles StudSearch1.TransferToParent
        RadGrid1.Visible = True
    End Sub

    Protected Async Sub Hdn_Click(sender As Object, e As EventArgs)
        Dim saf As New StudentsAccountsFacade

        Dim postedRefundsNew As String = String.Empty
        Dim PostRefundInfo As PostRefundInfo = BuildPostRefundInfo(txtPostRefundId.Text)
        GetPostedRefundsNew(postedRefundsNew)
        Dim resultNew As String = saf.UpdatePostRefundInfoNew(PostRefundInfo, postedRefundsNew, AdvantageSession.UserState.UserName)


        If resultNew <> "" Then
            '   Display Error Message
            DisplayErrorMessage(resultNew)
        Else
            Try
                Dim afaIntegrationEnabled = If(Not String.IsNullOrEmpty(MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration, campusId)) _
                    , MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration, campusId) _
                        , MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration).ToString).Trim.ToLower = "yes"

                If (afaIntegrationEnabled) Then
                    Dim fundSourceId = Guid.Parse(PostRefundInfo.FundSourceId)
                    Dim isTitleIVFundSource = New FundSourcesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString).IsFundSourceTitleIV(fundSourceId)

                    If (isTitleIVFundSource) Then
                        If (Not String.IsNullOrEmpty(postedRefundsNew)) Then
                            Dim refundsToPost = New List(Of Refund)
                            Dim guidCampusId = Guid.Parse(campusId)
                            Dim refunds As String() = postedRefundsNew.Split(":")
                            For i As Integer = 0 To refunds.Length - 1
                                If postedRefundsNew.Length > 0 Then
                                    Dim refundField() = refunds(i).Split(";")
                                    Dim refund = New Refund() With {
                                        .StudentEnrollmentId = Guid.Parse(txtStuEnrollmentId.Text),
                                        .CampusId = guidCampusId,
                                        .AdjustmentAmount = Decimal.Parse(refundField(1)),
                                        .AdjustmentDate = PostRefundInfo.PostRefundDate,
                                        .StudentAwardId = Guid.Parse(refundField(0)),
                                        .AwardDisbursementId = Guid.Parse(refundField(4))}

                                    Dim ssn = New LeadDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString) _
                                     .GetSSN(refund.StudentEnrollmentId)

                                    Dim studentAwardDA = New StudentAwardDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

                                    Dim faId = studentAwardDA.GetFinancialAidId(refund.StudentAwardId)
                                    Dim disbursementNumber = studentAwardDA.GetAFADisbursementNumber(refund.AwardDisbursementId)

                                    If (Not String.IsNullOrEmpty(ssn) AndAlso Not String.IsNullOrEmpty(faId) AndAlso Not disbursementNumber = 0) Then
                                        refund.SSN = ssn
                                        refund.FinancialAidId = faId
                                        refund.DisbursementNumber = disbursementNumber
                                        refundsToPost.Add(refund)
                                    End If

                                End If
                            Next
                            If (refundsToPost.Any) Then
                                Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
                                Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString

                                'Send updated to AFA if integration is enabled(handled internally)
                                Dim helper = New StudentAwardHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, AdvantageSession.UserState.UserName)
                                Dim res = Await helper.PostDisbursementAdjustment(campusId, refundsToPost)
                            End If
                        End If
                    End If
                End If

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)


            End Try
            'If result <> "" Then
            '    '   Display Error Message
            '    DisplayErrorMessage(result)
            'Else
            ''''''''''' 15 Jan 2010 ''''''''''''
            '   If there are no errors bind an empty entity and init buttons
            If Not CommonWebUtilities.IsValidGuid(ViewState("TransactionId")) Then

                ' DE7646 5/24/2012 Janet Robinson keep grid open
                Dim strStuEnrollId As String = txtStuEnrollmentId.Text

                '   bind an empty new PostRefundInfo
                BindPostRefundData(New PostRefundInfo)

                'this will disable the displaying of the refunds grid
                txtStuEnrollmentId.Text = String.Empty

                ''Code Added and commented Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
                'With rStudentAwards
                '    .DataSource = Nothing
                '    .DataBind()
                'End With
                'rStudentAwards.Visible = False

                With RadGrid1
                    .DataSource = Nothing
                    .DataBind()
                End With
                ' DE7646 5/24/2012 Janet Robinson keep grid open
                RadGrid1.Visible = True
                ' DE7646 End
                '''''''''' 15 Jan 2010 '''''''''''''''''

                '   initialize buttons
                InitButtonsForLoad()

                ' DE7646 5/24/2012 Janet Robinson keep grid open
                txtStuEnrollmentId.Text = strStuEnrollId
                If CommonWebUtilities.IsValidGuid(txtStuEnrollmentId.Text) Then
                    If RadGrid1.Items.Count = 0 Then
                        BindStudentAwardsRepeater(txtStuEnrollmentId.Text)
                        RadGrid1.MasterTableView.HierarchyDefaultExpanded = False
                    End If
                End If
                ' DE7646 End

            Else
                '   bind PostRefundInfo in session
                With New StudentsAccountsFacade
                    BindPostRefundData(.GetPostRefundInfo(ViewState("TransactionId")))
                End With
                '   initialize buttons for edit
                InitButtonsForEdit()
            End If
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControl As New SDFComponent
        Try
            SDFControl.DeleteSDFValue(txtPostRefundId.Text)
            SDFID = SDFControl.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControl.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                SDFControl.InsertValues(txtPostRefundId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
            Next
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub


    Private Function getRadGridDuplicates(postedRefundsNew As String, PostRefundInfo As PostRefundInfo) As Boolean
        Dim saf As New StudentsAccountsFacade
        Dim RadElements() As String = postedRefundsNew.Split(":")
        Dim RadElementsChild() As String
        Dim RadElementsPreviousChild() As String
        If RadElements.Length > 0 Then
            For i As Integer = 0 To RadElements.Length - 1
                RadElementsChild = RadElements(i).Split(";")
                If i > 0 Then
                    RadElementsPreviousChild = RadElements(i - 1).Split(";")
                    If RadElementsChild(0) = RadElementsPreviousChild(0) And RadElementsChild(1) = RadElementsPreviousChild(1) And RadElementsChild(2) = RadElementsPreviousChild(2) Then
                        Return True
                        Exit Function
                    Else
                        PostRefundInfo.Amount = CDec(RadElementsChild(1).ToString) * (-1)
                        If saf.CheckTransDuplicateRefund(PostRefundInfo) Then
                            Return True
                            Exit Function
                        End If
                    End If
                Else
                    PostRefundInfo.Amount = CDec(RadElementsChild(1).ToString) * (-1)
                    If saf.CheckTransDuplicateRefund(PostRefundInfo) Then
                        Return True
                        Exit Function
                    End If
                End If
            Next

        Else
            Return False
        End If
        Return False
    End Function


    Protected Sub ddlFundSourceId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFundSourceId.SelectedIndexChanged
        BindStudentAwardsRepeater(txtStuEnrollmentId.Text)
    End Sub
    Protected Sub tbDetRefundAmount_TextChanged(sender As Object, e As EventArgs)
        Dim message As String = String.Empty
        If Not IsRefundPostedToLastRDisbursement(message) Then
            'DisplayWarningMessage(message)
            HasShowMessageForPostingToLastDisbursement = True
               ViewState("NotPostingToLastDisbursement") = HasShowMessageForPostingToLastDisbursement
            Else
            ' HasShowMessageForPostingToLastDisbursement = False
        End If
    End Sub
End Class