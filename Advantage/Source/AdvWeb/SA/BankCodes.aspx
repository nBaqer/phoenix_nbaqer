<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="BankCodes.aspx.vb" Inherits="BankCodes" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }


    </script>
    <style type="text/css">
        input[type="checkbox"] {
            vertical-align: top !important;
        }
    </style>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstBankCodes" runat="server" DataKeyField="BankId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("BankId")%>' Text='<%# container.dataitem("BankDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCode" runat="server" class="label">Code</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" TabIndex="1" runat="server" class="textbox"></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblBankDescrip" runat="server" class="label">Description</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtBankDescrip" TabIndex="3" runat="server" class="textbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblStatusId" runat="server" class="label">Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" TabIndex="2" runat="server" class="dropdownlist" Width="200px"></asp:DropDownList>

                                            </td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCampGrpId" runat="server" class="label">Campus Group</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" TabIndex="4" runat="server" class="dropdownlist" Width="200px"></asp:DropDownList>


                                            </td>
                                        </tr>
                                    </table>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" nowrap colspan="6">
                                                <asp:Label ID="labeladd" runat="server" Font-Bold="true" class="label">Address</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap></td>
                                            <td class="contentcell4" align="left">
                                                <asp:CheckBox ID="chkForeignZip" TabIndex="5" Visible="true" CssClass="checkboxinternational" runat="server"
                                                    AutoPostBack="true" Text="International"></asp:CheckBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>&nbsp;</td>
                                            <td class="contentcell4" nowrap>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblAddress1" runat="server" class="label">Address 1</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtAddress1" TabIndex="6" runat="server" class="textbox"></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblState" runat="server" class="label">State</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStateId" TabIndex="9" runat="server" class="dropdownlist" Width="200px"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblAddress2" runat="server" class="label">Address 2</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtAddress2" TabIndex="7" runat="server" class="textbox"></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell">
                                                <asp:Label ID="lblZip" runat="server" class="label">Zip</asp:Label></td>
                                            <td class="contentcell4" align="left">
                                                <telerik:RadMaskedTextBox ID="txtzip" TabIndex="11" runat="server" CssClass="textbox" Width="200px" DisplayPromptChar="" AutoPostBack="false" DisplayFormatPosition="Left"></telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCity" runat="server" class="label">City</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCity" TabIndex="8" runat="server" class="textbox"></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblOtherState" runat="server" class="label" Visible="true">Other State</asp:Label></td>
                                            <td class="contentcell4" nowrap>
                                                <asp:TextBox ID="txtOtherState" class="textbox" TabIndex="11" runat="server" AutoPostBack="True" Enabled="False"
                                                    Visible="true"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" nowrap colspan="6">
                                                <asp:Label ID="labelphone" runat="server" Font-Bold="true" class="label">Phone</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblPhone" runat="server" class="label">Phone</asp:Label></td>
                                            <td class="contentcell4" align="left" style="vertical-align: top">
                                                <telerik:RadMaskedTextBox ID="txtPhone" TabIndex="13" runat="server" CssClass="textbox" DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar=""></telerik:RadMaskedTextBox>

                                                <asp:CheckBox ID="chkForeignPhone" Text="International"
                                                    runat="server" AutoPostBack="true" Visible="true" TabIndex="12"></asp:CheckBox>
                                            </td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblFax" runat="server" class="label">Fax</asp:Label></td>
                                            <td class="contentcell4" align="left" nowrap>
                                                <telerik:RadMaskedTextBox ID="txtFax" TabIndex="13" runat="server" CssClass="textbox" DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar=""></telerik:RadMaskedTextBox>

                                                <asp:CheckBox ID="chkForeignFax" TabIndex="14" CssClass="checkboxinternational" Text="International"
                                                    runat="server" Visible="true" AutoPostBack="true"></asp:CheckBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" nowrap colspan="6">
                                                <asp:Label ID="labelemails" runat="server" Font-Bold="true" class="label">Email</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblEmail" runat="server" class="label">Email</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtEmail" TabIndex="16" runat="server" class="textbox"></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="None" ErrorMessage="Invalid Email Format"
                                                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Invalid Email Format</asp:RegularExpressionValidator></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                    </table>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" nowrap colspan="6">
                                                <asp:Label ID="labelcontact" runat="server" Font-Bold="true" class="label">Contact</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblFirstName" runat="server" class="label">First Name</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtFirstName" TabIndex="17" runat="server" class="textbox"></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblTitle" runat="server" class="label">Title</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTitle" TabIndex="19" runat="server" class="textbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblLastName" runat="server" class="label">LastName</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtLastName" TabIndex="18" runat="server" class="textbox"></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                    </table>
                                    <p></p>
                                    <p></p>
                                    <table width="100%">
                                        <tr>
                                            <td align="center" colspan="5">
                                                <asp:HyperLink ID="hplBankAccounts" runat="server" CssClass="label" Target="_blank" Enabled="false" NavigateUrl="../SA/BankAccounts.aspx?resid=76&amp;mod=SA">Set Up Bank Accounts</asp:HyperLink></td>
                                        </tr>
                                    </table>
                                </div>

                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" />
        <asp:TextBox ID="txtBankId" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtModUser" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtModDate" runat="server" Visible="false"></asp:TextBox>
        <!--end validation panel-->
    </div>
</asp:Content>


