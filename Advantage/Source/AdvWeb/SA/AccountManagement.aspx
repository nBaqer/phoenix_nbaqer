﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="AccountManagement.aspx.vb" Inherits="AccountManagement" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="ContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="PageResized">
    <telerik:RadPane ID="ContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
        <telerik:RadGrid ID="RadGrid1" runat="server"></telerik:RadGrid>
    </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>

