﻿<%@ Page Title="Student Ledger" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentLedger.aspx.vb" Inherits="StudentLedger" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <meta content="False" name="vs_snapToGrid">
    <meta content="True" name="vs_showGrid">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <style>
        @-moz-document url-prefix() {
            .detailsframe .k-grid-header {
                border: black;
                border-width: 1px;
                border-style: solid;
            }

            #ContentMain2_btnPrint { margin-top: 7px !important; }
        }
        #ContentMain2_btnPrint {
            line-height: 13px;

            font-size: 13px;
            margin-top: 5px;
        }

       
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="99%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table cellpadding="0" cellspacing="0" class="maincontenttable" style="width: 98%; border: none;">
                            <tr>
                                <td class="detailsframe">
                                    <div>
                                        <div class="boxContainer">
                                            <h3><%=Header.Title  %></h3>
                                            <table width="100%" border="0px">
                                                <tr>
                                                    <td style="width: 40%">
                                                        <table width="100%" border="0px">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblEnrollmentId" CssClass="label" runat="server">Enrollment</asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlEnrollmentId" runat="server" CssClass="dropdownlist"
                                                                                    AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-top: 15px;">
                                                                                <asp:Label ID="lblBalanceTitle" runat="server" CssClass="label">Balance</asp:Label>
                                                                            </td>
                                                                            <td style="padding-top: 15px;">
                                                                                <asp:Label ID="lblBalance" runat="server" CssClass="labelbold">Balance</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblTerms" runat="server" CssClass="label">Terms</asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlTermsId" runat="server" CssClass="dropdownlist">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-top: 15px;">
                                                                                <asp:Label ID="lblProjectedHdr" runat="server" CssClass="label">Projected</asp:Label>
                                                                            </td>
                                                                            <td style="padding-top: 15px;">
                                                                                <asp:Label ID="lblProjected" runat="server" CssClass="labelbold">Projected Amount</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 20%; vertical-align:top">
                                                    
                                                                <asp:Button ID="btnPrint" runat="server" Text="Print Ledger" ></asp:Button>

                                                        
                                                     
                                                       
                                                    </td>
                                                    <td style="width: 40%" align="left">
                                                        <table>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Label ID="label1" runat="server" Text="label" CssClass="labelbold">Display Format:</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span class="label">
                                                                        <asp:RadioButton ID="rbLedger" runat="server" Text="Balance Forward" GroupName="type"
                                                                            AutoPostBack="True" Checked="true" /></span>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span class="label">
                                                                        <asp:RadioButton ID="rbPending" runat="server" Text="Open Item" GroupName="type"
                                                                            AutoPostBack="True" /></span>
                                                                </td>
                                                                <td>
                                                                    <span class="label">
                                                                        <asp:CheckBox ID="cbShowAll" runat="server" Text="Show All" AutoPostBack="True" /></span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="boxContainer noBorder">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:DataGrid ID="dgrdStudentLedger" runat="server" Width="100%" HorizontalAlign="Center"
                                                            BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0">
                                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                            <Columns>
                                                                <asp:BoundColumn DataField="TransDate" SortExpression="TransDate asc"
                                                                    HeaderText="Date" DataFormatString="{0:d}">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="5%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TransReference" SortExpression="TransReference" ReadOnly="True"
                                                                    HeaderText="Reference">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="15%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="CheckNumber" SortExpression="CheckNumber" ReadOnly="True"
                                                                    HeaderText="Document ID">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="7%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TransCodeDescrip" SortExpression="TransCodeDescrip" ReadOnly="True"
                                                                    HeaderText="Trans. Code">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TransDescrip" SortExpression="TransDescrip" ReadOnly="True"
                                                                    HeaderText="Description">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="15%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn Visible="False" DataField="TransAmount" SortExpression="TransAmount"
                                                                    ReadOnly="True" HeaderText="Amount">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="5%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="AcademicYearDescrip" SortExpression="AcademicYearDescrip"
                                                                    ReadOnly="True" HeaderText="Academic Year">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="5%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TermDescrip" SortExpression="TermDescrip" ReadOnly="True"
                                                                    HeaderText="Term">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TransTypeDescrip" SortExpression="TransTypeId" ReadOnly="True"
                                                                    HeaderText="Trans. Type">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="5%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="ModUser" SortExpression="ModUser" ReadOnly="True" HeaderText="User">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="5%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                                <asp:TemplateColumn HeaderText="Amount">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="5%"></HeaderStyle>

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Amount" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Balance" Visible="false">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="7%"></HeaderStyle>

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Balance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Balance", "{0:c}") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Open Item Balance">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="7%"></HeaderStyle>

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="OpenItemBalance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OpenItemBalance") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="PaymentPeriodNumber" SortExpression="TransTypeId" ReadOnly="True"
                                                                    HeaderText="Payment Period">
                                                                    <HeaderStyle CssClass="k-grid-header" Width="5%"></HeaderStyle>

                                                                </asp:BoundColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

