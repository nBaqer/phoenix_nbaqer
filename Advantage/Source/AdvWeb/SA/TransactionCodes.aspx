<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="TransactionCodes.aspx.vb" Inherits="TransactionCodes" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstTransCodes" runat="server" DataKeyField="TransCodeId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# Container.DataItem("TransCodeId")%>' Text='<%# Container.DataItem("TransCodeDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table cellspacing="0" cellpadding="0" align="center" width="100%">
                                        <asp:TextBox ID="txtTransCodeId" runat="server" Visible="False"></asp:TextBox>
                                        <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                        <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
                                        <!--</td>-->
                                        <tr>
                                            <td class="contentcell" style="width: 30%;">
                                                <asp:Label ID="lblTransCodeCode" runat="server" CssClass="label">Code</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTransCodeCode" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTransCodeDescrip" CssClass="label" runat="server">Description</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTransCodeDescrip" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblSysTransCodeId" CssClass="label" runat="server">System Transaction Code</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlSysTransCodeId" runat="server" CssClass="dropdownlist" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlSysTransCodeId_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblBillTypeId" CssClass="label" runat="server">Bill Type</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlBillTypeId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDefEarnings" CssClass="label" runat="server"></asp:Label></td>
                                            <td class="contentcell4" align="left">
                                                <asp:CheckBox ID="chkDefEarnings" runat="server" CssClass="checkboxinternational" AutoPostBack="true"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblIsInstCharge" CssClass="label" runat="server"></asp:Label></td>
                                            <td class="contentcell4" align="left">
                                                <asp:CheckBox ID="chkIsInstCharge" runat="server" CssClass="checkboxinternational"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblIs1098T" CssClass="label" runat="server"></asp:Label></td>
                                            <td class="contentcell4" align="left">
                                                <asp:CheckBox ID="chkIs1098T" runat="server" CssClass="checkboxinternational"></asp:CheckBox><span id="txt1098T">1098T</span></td>
                                            <telerik:RadToolTip runat="server" TargetControlID="txt1098T" ID="chk1098TToolTip" Width="400px" HideEvent="ManualClose" ShowEvent="OnMouseOver" IsClientID="true" IgnoreAltAttribute="true" RelativeTo="Mouse" Position="BottomRight">
                                                <h3>Qualified Education Expenses</h3>
                                                <h4>Qualified Education Expenses for Education Credits and Tuition and Fees Deduction</h4>
                                                <p>
                                                    Qualified expenses are amounts paid for tuition, fees and other related expense for an eligible student that are required
                                    for enrollment or attendance at an eligible educational institution. You must pay the expenses for an academic period that
                                    starts during the tax year or the first three months of the next tax year.
                                                </p>
                                                <p>
                                                    Eligible expenses also include student activity fees you are required to pay to enroll or attend the school. For example,
                                    an activity fee that all students are required to pay to fund all on-campus student organizations and activities.
                                                </p>
                                                <h4>Expenses that Do Not Qualify</h4>
                                                <p>
                                                    Even if you pay the following expenses to enroll or attend the school, the following are not qualified education expenses:
                                        <ul>
                                            <li>Room and board</li>
                                            <li>Insurance</li>
                                            <li>Medical expenses (including student health fees)</li>
                                            <li>Transportation</li>
                                            <li>Similar personal, living or family expenses</li>
                                        </ul>
                                                </p>
                                            </telerik:RadToolTip>
                                            <telerik:RadToolTip runat="server" TargetControlID="chkIs1098T" ID="RadToolTip1" Width="400px" HideEvent="ManualClose" ShowEvent="OnMouseOver" IsClientID="false" IgnoreAltAttribute="true" RelativeTo="Mouse" Position="BottomRight">
                                                <h3>Qualified Education Expenses</h3>
                                                <h4>Qualified Education Expenses for Education Credits and Tuition and Fees Deduction</h4>
                                                <p>
                                                    Qualified expenses are amounts paid for tuition, fees and other related expense for an eligible student that are required
                                    for enrollment or attendance at an eligible educational institution. You must pay the expenses for an academic period that
                                    starts during the tax year or the first three months of the next tax year.
                                                </p>
                                                <p>
                                                    Eligible expenses also include student activity fees you are required to pay to enroll or attend the school. For example,
                                    an activity fee that all students are required to pay to fund all on-campus student organizations and activities.
                                                </p>
                                                <h4>Expenses that Do Not Qualify</h4>
                                                <p>
                                                    Even if you pay the following expenses to enroll or attend the school, the following are not qualified education expenses:
                                        <ul>
                                            <li>Room and board</li>
                                            <li>Insurance</li>
                                            <li>Medical expenses (including student health fees)</li>
                                            <li>Transportation</li>
                                            <li>Similar personal, living or family expenses</li>
                                        </ul>
                                                </p>
                                            </telerik:RadToolTip>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" align="center" width="100%">
                                        <tr>
                                            <td class="label" align="center" colspan="2" style="padding: 20px">
                                                <h3>GL Percentage Distribution</h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:DataGrid ID="dgrdTransCodeGLAccounts" runat="server" EditItemStyle-Wrap="false"
                                                    HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid"
                                                    ShowFooter="True" Width="100%" BorderWidth="1px" BorderColor="#E0E0E0">
                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                    <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="GL Account">
                                                            <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGLAccount" runat="server" Text='<%# Container.DataItem("GLAccount") %>'>GLAccount</asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtFooterGLAccount" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditGLAccount" runat="server" Text='<%# Container.DataItem("GLAccount") %>'
                                                                    CssClass="textbox" Width="90%">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvGLAccount" runat="server" ErrorMessage="GL Account can not be blank"
                                                                    Display="None" ControlToValidate="txtEditGLAccount">GL Account field can not be blank</asp:RequiredFieldValidator>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Percentage">
                                                            <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPercentage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Percentage", "{0:####.##}") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtFooterPercentage" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditPercentage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Percentage", "{0:####.##}") %>'
                                                                    CssClass="textbox" Width="90%">
                                                                </asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Type">
                                                            <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUnit" runat="server" Text='<%# Container.DataItem("Type") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList ID="ddlFooterTypeId" runat="server" CssClass="dropdownlist" Width="90%">
                                                                    <asp:ListItem Value="0">Debit</asp:ListItem>
                                                                    <asp:ListItem Value="1">Credit</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlEditTypeId" runat="server" CssClass="dropdownlist" Width="90%"
                                                                    SelectedValue='<%# Container.DataItem("TypeId") %>'>
                                                                    <asp:ListItem Value="0">Debit</asp:ListItem>
                                                                    <asp:ListItem Value="1">Credit</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                                            <ItemStyle Wrap="False"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
                                                                    CausesValidation="False" runat="server" CommandName="Edit">
														<img border="0" src="../images//im_edit.gif" alt="edit"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkButUp" Text="<img border=0 src=../images/up.gif alt=MoveUp>"
                                                                    CausesValidation="False" runat="server" CommandName="Up"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkButDown" Text="<img border=0 src=../images/down.gif alt=MoveDown>"
                                                                    CausesValidation="False" runat="server" CommandName="Down"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterStyle Wrap="False" HorizontalAlign="Center"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                                <asp:Label ID="lblBalanced" runat="server" CssClass="label"></asp:Label>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                                    runat="server" CommandName="Update">
														<img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                                    CausesValidation="False" runat="server" CommandName="Delete"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
                                                                    CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTransCodeGLAccountId" runat="server" Text='<%# Container.DataItem("TransCodeGLAccountId") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle Wrap="False"></FooterStyle>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditTransCodeGLAccountId" runat="server" Text='<%# Container.DataItem("TransCodeGLAccountId") %>'
                                                                    Visible="False"></asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid></td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="DRPanel" runat="server">
                                        <table cellspacing="0" cellpadding="0" align="center" width="100%">
                                            <tr>
                                                <td class="label" align="center" colspan="2" style="padding: 20px">
                                                    <h3>Deferred Revenue GL Percentage Distribution</h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:DataGrid ID="dgrdTransCodeDRGLAccounts" runat="server" EditItemStyle-Wrap="false"
                                                        HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid"
                                                        ShowFooter="True" Width="100%" BorderWidth="1px" BorderColor="#E0E0E0">
                                                        <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="GL Account">
                                                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGLAccount" runat="server" Text='<%# Container.DataItem("GLAccount") %>'>GLAccount</asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:TextBox ID="txtFooterGLAccount" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                                                                </FooterTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtEditGLAccount" runat="server" Text='<%# Container.DataItem("GLAccount") %>'
                                                                        CssClass="textbox" Width="90%">
                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvGLAccount" runat="server" ErrorMessage="GL Account can not be blank"
                                                                        Display="None" ControlToValidate="txtEditGLAccount">GL Account field can not be blank</asp:RequiredFieldValidator>
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Percentage">
                                                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPercentage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Percentage", "{0:####.##}") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:TextBox ID="txtFooterPercentage" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                                                                </FooterTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtEditPercentage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Percentage", "{0:####.##}") %>'
                                                                        CssClass="textbox" Width="90%">
                                                                    </asp:TextBox>
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Type">
                                                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUnit" runat="server" Text='<%# Container.DataItem("Type") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="ddlFooterTypeId" runat="server" CssClass="dropdownlist" Width="90%">
                                                                        <asp:ListItem Value="2">Debit</asp:ListItem>
                                                                        <asp:ListItem Value="3">Credit</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </FooterTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="ddlEditTypeId" runat="server" CssClass="dropdownlist" Width="90%"
                                                                        SelectedValue='<%# Container.DataItem("TypeId") %>'>
                                                                        <asp:ListItem Value="2">Debit</asp:ListItem>
                                                                        <asp:ListItem Value="3">Credit</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn>
                                                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                                                <ItemStyle Wrap="False"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
                                                                        CausesValidation="False" runat="server" CommandName="Edit">
														<img border="0" src="../images//im_edit.gif" alt="edit"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkButUp" Text="<img border=0 src=../images/up.gif alt=MoveUp>"
                                                                        CausesValidation="False" runat="server" CommandName="Up"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkButDown" Text="<img border=0 src=../images/down.gif alt=MoveDown>"
                                                                        CausesValidation="False" runat="server" CommandName="Down"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <FooterStyle Wrap="False" HorizontalAlign="Center"></FooterStyle>
                                                                <FooterTemplate>
                                                                    <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                                    <asp:Label ID="lblBalanced" runat="server" CssClass="label"></asp:Label>
                                                                </FooterTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                                        runat="server" CommandName="Update">
														<img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                                        CausesValidation="False" runat="server" CommandName="Delete"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
                                                                        CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTransCodeGLAccountId" runat="server" Text='<%# Container.DataItem("TransCodeGLAccountId") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <FooterStyle Wrap="False"></FooterStyle>
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="lblEditTransCodeGLAccountId" runat="server" Text='<%# Container.DataItem("TransCodeGLAccountId") %>'
                                                                        Visible="False"></asp:Label>
                                                                </EditItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <br />
                                    <%--	<asp:Panel ID="pnlReportingAgenciesHeader" runat="server" Visible="True">
						<table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center"
							style="border: 1px solid #ebebeb;">
							<tr>
								<td class="contentcellheader" nowrap colspan="6" style="border-top: 0px; border-right: 0px;
									border-left: 0px">
									<asp:Label ID="lblReportingAgencies" runat="server" Font-Bold="true" CssClass="label">Accrediting Agencies Mapping</asp:Label></td>
							</tr>
							<tr>
								<td class="contentcellcitizenship" colspan="6" style="padding-top: 12px">
									<asp:Panel ID="pnlReportingAgencies" TabIndex="12" runat="server" EnableViewState="false">
									</asp:Panel>
								</td>
							</tr>
						</table>
					</asp:Panel>--%>
                                </div>

                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                runat="server">
            </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>

</asp:Content>


