﻿
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports System.IO
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ

Partial Class StudentLedger
    Inherits BasePage

    Protected studentId, leadid As String
    Protected balanceForward As Decimal
    Protected count As Integer
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected state As AdvantageSessionState
    Protected campusId, userid As String
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#End Region


    Dim resourceId As Integer
    Protected boolSwitchCampus As Boolean = False
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try
            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                studentId = Guid.Empty.ToString()
            Else
                studentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                leadid = Guid.Empty.ToString()
            Else
                leadid = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(studentId)
                .LeadId = New Guid(leadid)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)



        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Session("SEARCH") = 0


        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = getStudentFromStateObject(116) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            studentId = .StudentId.ToString
            leadid = .LeadId.ToString

        End With

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If
            'build dropdownlists
            BuildDropDownLists()

            'bind datagrid
            If rbLedger.Checked Then
                BindDatagridLedger("TransDate asc")
            Else
                BindDatagridPending()
            End If
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, studentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, studentId, resourceId.ToString)
            'End If
            MyBase.uSearchEntityControlId.Value = ""
        End If

        cbShowAll.Visible = rbPending.Checked

        If ddlEnrollmentId.SelectedIndex = 0 Then
            btnPrint.Visible = false
        End If
    End Sub
    Private Sub BuildDropDownLists()
        BuildStudentEnrollmentsDDL(studentId)
        BuildTermsDDL(studentId)
    End Sub

    Private Sub SortGrid(ByVal sender As System.Object, ByVal e As DataGridSortCommandEventArgs) Handles dgrdStudentLedger.SortCommand
        'dgrdStudentLedger.Columns(0).SortExpression = e.SortExpression
    End Sub
    Private Sub BindDatagridLedger(ByVal sortExpression As String)

        '   bind datagrid with data from the backend
        With New StudentsAccountsFacade
            Dim myDataView = New DataView(.GetStudentLedger(studentId, ddlEnrollmentId.SelectedValue, ddlTermsId.SelectedValue).Tables(0), "RecordType>=0", sortExpression, DataViewRowState.CurrentRows)
            dgrdStudentLedger.DataSource = myDataView
            dgrdStudentLedger.DataBind()
        End With
        
        ''Since document Id was introduced the column numbers were not corrected.
        ''so, the column numbers are chnaged from 10 to 11 and 11 to 12
        ''Modified by Saraswathi lakshmanan on oct 23 2009
        '   if datagrid is sorted by date then display balance column

        For Each column As DataGridColumn In dgrdStudentLedger.Columns
            If column.HeaderText = "Balance" Then
                If sortExpression.IndexOf("TransDate asc") > -1 Then
                    column.Visible = True
                Else
                    column.Visible = False
                End If

            ElseIf column.HeaderText = "Open Item Balance" Then
                column.Visible = False
            End If

        Next

        'this logic is to laternative sort the columns in ascending or descending mode
        'get column to be sorted. Change replace "asc" by "desc". Insert "asc" if necessary
        For Each column As DataGridColumn In dgrdStudentLedger.Columns
            If column.SortExpression = sortExpression Then
                If sortExpression.IndexOf(" asc") > 0 Then
                    sortExpression = sortExpression.Replace(" asc", " desc")
                ElseIf sortExpression.IndexOf(" desc") > 0 Then
                    sortExpression = sortExpression.Replace(" desc", " asc")
                Else
                    sortExpression += " asc"
                End If

                'update sort expression in the column
                column.SortExpression = sortExpression
                Exit For
            End If
        Next

    End Sub
    Private Sub BindDatagridPending()
        Dim filter As String = "RecordType<=0 "
        If Not cbShowAll.Checked Then filter += " and IsItemClosed=false"

        '   bind datagrid with data from the backend
        With New StudentsAccountsFacade
            dgrdStudentLedger.DataSource = New DataView(.GetStudentLedger(studentId, ddlEnrollmentId.SelectedValue, ddlTermsId.SelectedValue).Tables(0), filter, "ChargeDate asc, ApplyToTransId asc, TransDate asc", DataViewRowState.CurrentRows)
            dgrdStudentLedger.DataBind()
        End With
        ''Since document Id was introduced the column numbers were not corrected.
        ''so, the column numbers are chnaged from 10 to 11 and 11 to 12
        ''Modified by Saraswathi lakshmanan on oct 23 2009
        dgrdStudentLedger.Columns(11).Visible = False
        dgrdStudentLedger.Columns(12).Visible = True

    End Sub
    Private Sub BuildTermsDDL(ByVal studentId As String)
        '   bind the Terms DDL
        Dim terms As New StudentsAccountsFacade

        With ddlTermsId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.GetAllTermsPerStudent(studentId)
            .DataBind()
            .Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildTermsDDLEnrollment(ByVal stuEnrollId As String)
        '   bind the Terms DDL
        Dim terms As New StudentsAccountsFacade

        With ddlTermsId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.GetTermsPerStudentEnrollment(stuEnrollId)
            .DataBind()
            .Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade

        With ddlEnrollmentId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()
            .Items.Insert(0, New ListItem("All Enrollments", Guid.Empty.ToString))

            If .Items.Count > 1 Then
                'Since enrollemnts are orders based on start date, and count of enrollments is greater than one, it is safe to preselect index = 1
                .SelectedIndex = 1
            Else
                .SelectedIndex = 0
            End If

        End With
    End Sub

    Private Sub dgrdStudentLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdStudentLedger.ItemDataBound
        '   accept only items (no header or footer records)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            '   get value of TransAmount column
            Dim transAmount As Decimal = CType(e.Item.DataItem, DataRowView).Item("TransAmount")

            '   set amount
            CType(e.Item.FindControl("Amount"), Label).Text = transAmount.ToString("c")

            '   acumulate balance forward
            If Not CType(e.Item.DataItem("Voided"), Boolean) Then balanceForward += transAmount
            CType(e.Item.FindControl("Balance"), Label).Text = CType(e.Item.DataItem("Balance"), Decimal).ToString("c")

        End If
    End Sub
   
    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim print = New FAME.Advantage.Reporting.StudentLedger()
        Dim rpt = print.BuildReportFromPage(ddlEnrollmentId.SelectedValue.ToString, "", ConfigurationManager.AppSettings("Reports.ReportsFolder").ToString.Trim)

        ExportReport(rpt)
    End Sub

    Private Sub dgrdStudentLedger_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgrdStudentLedger.SortCommand
        '   bind datagrid
        If rbLedger.Checked Then
            BindDatagridLedger(e.SortExpression)
        Else
            BindDatagridPending()
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        '   display balance
        lblBalance.Text = balanceForward.ToString("c")

        '   display projected amount only if "All terms" and some Enrollment are selected
        If ddlTermsId.SelectedIndex = 0 And Not ddlEnrollmentId.SelectedValue = Guid.Empty.ToString Then
            lblProjected.Text = (New StudentsAccountsFacade).GetProjectedAmountByEnrollment(ddlEnrollmentId.SelectedValue).ToString("c")

        ElseIf ddlTermsId.SelectedIndex = 0 And ddlEnrollmentId.SelectedValue = Guid.Empty.ToString Then
            lblProjected.Text = (New StudentsAccountsFacade).GetProjectedAmountForStudent(studentId).ToString("c")

        Else
            lblProjected.Text = "Not available"
        End If
    End Sub

    Protected Sub ddlEnrollmentId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
        If ddlEnrollmentId.SelectedIndex <> 0 Then
            BuildTermsDDLEnrollment(ddlEnrollmentId.SelectedValue.ToString)
            btnPrint.Visible = True
        Else
            btnPrint.Visible = false
            BuildTermsDDL(studentId)
        End If
        If rbLedger.Checked Then
            BindDatagridLedger("TransDate asc")
        Else
            BindDatagridPending()
        End If
    End Sub

    Protected Sub cbShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbShowAll.CheckedChanged, rbPending.CheckedChanged, rbLedger.CheckedChanged
        '   bind datagrid
        If rbLedger.Checked Then
            BindDatagridLedger(dgrdStudentLedger.Columns(0).SortExpression)
        Else
            BindDatagridPending()
        End If
    End Sub

    Private Sub ExportReport(ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String

        strExtension = "pdf"
        strMimeType = "application/pdf"

        HttpContext.Current.Response.Clear()
        Response.ContentType = "application/octet-stream"
        Dim dateStr = DateTime.Now().ToString("MM/dd/yyyy").Replace("/", "")

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim leadDA = New LeadDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

        Dim stuID = leadDA.GetLeadStudentNumber(ddlEnrollmentId.SelectedValue.ToString, MyAdvAppSettings.AppSettings("StudentIdentifier", campusId).ToString.ToLower)
        HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=" + stuID + "_Ledger_" + dateStr + ".") + strExtension)


        Dim stream As New MemoryStream(getReportAsBytes)
        stream.CopyTo(HttpContext.Current.Response.OutputStream)
    End Sub
End Class