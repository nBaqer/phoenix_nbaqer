﻿Imports System.Diagnostics
Imports Fame.Common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports System.IO
Imports System.Xml.Xsl
Imports System.Xml.XPath

Partial Class PrintDeposits
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        InitializeComponent()
    End Sub

#End Region

#Region "Class Events"

    ''' <summary>
    ''' page load event - set up the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        Dim objCommon As New CommonUtilities
        Dim resourceId As Integer

        chkSelectAll.Font.Size = FontUnit.Point(9)

        If HttpContext.Current.Request.Params("resid") IsNot Nothing Then
            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        End If

        Dim campusId As String = Master.CurrentCampusId
        ViewState("CampusId") = campusId

        Dim userId As String = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        Dim pObj As New UserPagePermissionInfo
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId.ToString(), campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            SetupPageData(objCommon)
        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")
        End If

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

    End Sub

    ''' <summary>
    ''' event fired when user clicks the select all checkbox
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub chkSelectAll_CheckChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkSelectAll.CheckedChanged

        Dim chkBox As CheckBox = CType(sender, CheckBox)

        For Each item As DataGridItem In dgrdAvailableDeposits.Items
            CType(item.FindControl("chkSelectedA"), CheckBox).Checked = chkBox.Checked
        Next

    End Sub

    ''' <summary>
    ''' Called when user clicks btnSelect button to show deposits
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSelect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSelect.Click

        'Create datatable from AvailableDeposits, Bind Datagrids and save table in viewstate
        InitSessions()

        Dim errMsg = ValidateFields()
        If Not String.IsNullOrEmpty(errMsg) Then
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errMsg)
        Else
            GetDepositsTable(False)
        End If

    End Sub


    ''' <summary>
    ''' Event called when user has chosen the deposits to process and clicks to save
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        Dim selectedDepositItems As Integer = GetSelectedDepositGridRow(0)
        Dim selectedLeadDepositItems As Integer = GetSelectedDepositGridRow(1)

        If selectedDepositItems = 0 And selectedLeadDepositItems = 0 Then
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "A Deposit must be selected")
            Exit Sub
        End If

        'create an array string with Selected student and lead Deposits TransactionsIds
        Dim selectedDeposits As String() = New String(selectedDepositItems - 1) {}
        Dim selectedLeadDeposits As String() = New String(selectedLeadDepositItems - 1) {}

        'get the Deposits dataTable
        Dim dtNewDepositsTable As DataTable = CType(ViewState("DepositsTable"), DataTable)

        Dim i As Integer = 0
        For Each dataRow As DataRow In dtNewDepositsTable.Rows
            If dataRow.Item("Selected") = 1 And dataRow.Item("IsLead") = 0 Then
                selectedDeposits.SetValue(CType(dataRow.Item("TransactionId"), Guid).ToString, i)
                i += 1
            End If
        Next

        i = 0
        For Each dataRow As DataRow In dtNewDepositsTable.Rows
            If dataRow.Item("Selected") = 1 And dataRow.Item("IsLead") = 1 Then
                selectedLeadDeposits.SetValue(CType(dataRow.Item("TransactionId"), Guid).ToString, i)
                i += 1
            End If
        Next

        'update selected SelectedDeposits (Deposits)
        Dim studentsAccounts As New StudentsAccountsFacade
        Dim result As String = studentsAccounts.UpdateSelectedDeposits(ddlBankAcctId.SelectedValue, AdvantageSession.UserState.UserName, selectedDeposits)
        If Not result = "" Then
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, result)
            Exit Sub
        End If

        'update selected SelectedLeadDeposits (Deposits)
        result = studentsAccounts.UpdateSelectedLeadDeposits(AdvantageSession.UserState.UserName, selectedLeadDeposits)
        If Not result = "" Then
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, result)
            Exit Sub
        End If

        'Create datatable from AvailableDeposits, Bind Datagrids and save table in viewstate
        GetDepositsTable(False)


    End Sub



    ''' <summary>
    ''' event called when user chooses to print deposits to html
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnPrintHtml_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imgExport.Click

        If IsNothing(CType(ViewState("DepositsTable"), DataTable)) Then
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "No deposits selected")
            Exit Sub
        Else

            Dim selectedDepositItems As Integer = GetSelectedDepositGridRow(0)
            Dim selectedLeadDepositItems As Integer = GetSelectedDepositGridRow(1)

            If selectedDepositItems = 0 And selectedLeadDepositItems = 0 Then
                CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "A deposit must be selected")
                Exit Sub
            End If

            'build Xml document with deposit information
            Dim xmlStream As MemoryStream = CreateXmlStreamToPrint(CType(ViewState("DepositsTable"), DataTable))

            'get XslStyleSheet 
            Dim templateFilePath As String = Server.MapPath("..") + "/" + Context.Request.Headers.Item("host") + "/Xsl/DepositsReportHtml.xsl"

            If (Not File.Exists(templateFilePath)) Then
                templateFilePath = Server.MapPath("..") + "/localhost" + "/Xsl/DepositsReportHtml.xsl"
                If (Not File.Exists(templateFilePath)) Then
                    Throw New FileNotFoundException("Could not find file 'DepositsReportHtml.xsl' at the following path '" + templateFilePath + "'")
                End If
            End If

            'generate html document
            Dim xslt As New XslTransform
            xslt.Load(templateFilePath)
            Dim xpathdocument As New XPathDocument(xmlStream)
            Dim documentMemoryStream As New MemoryStream
            Dim writer As New XmlTextWriter(documentMemoryStream, Nothing)
            writer.Formatting = Formatting.Indented
            xslt.Transform(xpathdocument, Nothing, writer, Nothing)

            'save document and document type in sessions variables
            Session("DocumentMemoryStream") = documentMemoryStream
            Session("ContentType") = "text/html"

            Const javascript As String = "<script>var origwindow=window.self;window.open('PrintAnyReport.aspx');</script>"

            'Register a javascript to open the report in another window
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NewWindow", javascript, False)

        End If

    End Sub

#End Region

#Region "Class Methods"

    Private Sub SetupPageData(objCommon As CommonUtilities)

        objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")

        BuildBankAccountsDdl()

        'get starting and ending dates
        GetStartAndEndDates()

        'Create datatable from AvailableDeposits, Bind Datagrids and save table in viewstate
        GetDepositsTable(True)

        InitSessions()
    End Sub
    ''' <summary>
    ''' set viewstate and session objects to null
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InitSessions()

        'initialize session variables
        ViewState("selectedDepositItems") = Nothing

        Session("NameCaption") = Nothing
        Session("NameValue") = Nothing
        Session("AddAll") = Nothing

        Session("PageCountDepos") = Nothing
        Session("PageCountselect") = Nothing
        Session("RemoveALL") = Nothing
        Session("ButtonAllEnable") = Nothing
        Session("SelectDepositsTable") = Nothing
        Session("selectedDepositRemove") = Nothing

    End Sub

    ''' <summary>
    ''' Get bank accounts for dropdown lists
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BuildBankAccountsDdl()

        'bind the bank accounts DDL
        Dim bankAccounts As New StudentsAccountsFacade

        With ddlBankAcctId
            .DataTextField = "BankAcctDescrip"
            .DataValueField = "BankAcctId"
            .DataSource = bankAccounts.GetAllBankAccounts(False)
            .DataBind()
            If .Items.Count = 0 Then
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            End If
            .SelectedIndex = 0
        End With

    End Sub

    ''' <summary>
    ''' Set the min and max dates for the page
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetStartAndEndDates()

        'instantiate facade
        Dim minAndMaxDates As New StudentsAccountsFacade

        'get min and max dates from the transactions Table
        Dim campusId As String = CType(ViewState("CampusId"), String)
        Dim dates() As Date = minAndMaxDates.GetMinAndMaxDatesFromStudentAndLeadTransactions(campusId)

        'fill textboxes with min and max dates
        txtStartDate.SelectedDate = DateAdd(DateInterval.Day, -60, CDate(dates(1).ToShortDateString))

        txtEndDate.SelectedDate = CType(dates(1).ToShortDateString, Date?)

    End Sub

    ''' <summary>
    ''' Validate the fields entered before getting deposits
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateFields() As String

        Dim errMsg As String = String.Empty
        If txtStartDate.SelectedDate Is Nothing Then
            errMsg = "Start Date is required" + vbCrLf
        End If
        If txtEndDate.SelectedDate Is Nothing Then
            errMsg += "End Date is required"
        End If

        If Not String.IsNullOrEmpty(errMsg) Then
            Return errMsg
        Else
            If CommonWebUtilities.IsValidDate(txtStartDate.SelectedDate) Then
                If txtStartDate.SelectedDate > Today Then
                    errMsg += "Start Date cannot be greater than current date" + vbCrLf
                End If
            Else
                errMsg += "Invalid Start Date" + vbCrLf
            End If
            If CommonWebUtilities.IsValidDate(txtEndDate.SelectedDate) Then
                If txtEndDate.SelectedDate > Today Then
                    errMsg += "End Date cannot be greater than current date" + vbCrLf
                End If
            Else
                errMsg += "Invalid End Date" + vbCrLf
            End If
            If CommonWebUtilities.IsValidDate(txtStartDate.SelectedDate) AndAlso CommonWebUtilities.IsValidDate(txtEndDate.SelectedDate) Then
                If txtEndDate.SelectedDate < txtStartDate.SelectedDate Then
                    errMsg += "Start Date cannot be greater than End Date" + vbCrLf
                End If
            End If
            If ddlBankAcctId.SelectedValue = Guid.Empty.ToString() Then
                errMsg += "Must select a bank account." + vbCrLf
            End If

            Return errMsg

        End If

    End Function

    ''' <summary>
    ''' Get the deposits from the database and bind the
    ''' datagrid to the data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetDepositsTable(init As Boolean)

        Dim campusId As String = CType(ViewState("CampusId"), String)
        Dim bDeposited As Boolean = CType(ddlShowDeposited.SelectedValue, Boolean)


        'get the original AvailableDeposits dataTable
        Dim depositsTable As DataTable = (New StudentsAccountsFacade).GetAvailableDeposits(txtStartDate.SelectedDate, txtEndDate.SelectedDate, ddlBankAcctId.SelectedValue, campusId, bDeposited).Tables(0)

        dgrdAvailableDeposits.DataSource = depositsTable
        dgrdAvailableDeposits.DataBind()

        ViewState("DepositsTable") = depositsTable

        If depositsTable.Rows.Count > 0 Then
            lblNumRecords.Text = " Number of Records: " & depositsTable.Rows.Count.ToString()
        Else
            If init = False Then
                CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, "No Records found!")
            End If
        End If

        If bDeposited = True Then
            btnSave.Enabled = False
        Else
            btnSave.Enabled = True
        End If

    End Sub


    ''' <summary>
    ''' get the selected grid row
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetSelectedDepositGridRow(nLead As Integer) As Integer

        Dim selectedDepositItems As Integer

        Dim depositsTable As DataTable = UnSelectAllItemsInDepositsTable(CType(ViewState("DepositsTable"), DataTable), nLead)

        Dim strEntity As String
        If nLead = 1 Then
            strEntity = "Lead"
        Else
            strEntity = "Student"
        End If


        For Each item As DataGridItem In dgrdAvailableDeposits.Items
            If CType(item.FindControl("chkSelectedA"), CheckBox).Checked = True And CType(item.FindControl("EntityTypeA"), Label).Text = strEntity Then
                UpdateSelectedItemsInDepositsTable(depositsTable, CType(item.FindControl("lblTransactionIdA"), Label).Text, 1)
                selectedDepositItems += 1
            End If
        Next

        Return selectedDepositItems

    End Function


    ''' <summary>
    ''' update the selected deposits in the database
    ''' </summary>
    ''' <param name="transactionId"></param>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Private Sub UpdateSelectedItemsInDepositsTable(dtNewDepositsTable As DataTable, ByVal transactionId As String, ByVal value As Integer)

        '   loop through out the whole table
        Dim i As Integer


        For i = 0 To dtNewDepositsTable.Rows.Count - 1

            '   select the record with the given transactionId
            If CType(dtNewDepositsTable.Rows(i).Item("TransactionId"), Guid).ToString = transactionId Then
                '   set item to proper value
                dtNewDepositsTable.Rows(i).Item("Available") = value
                '   set item to proper value
                dtNewDepositsTable.Rows(i).Item("Selected") = value
            End If
        Next

        ViewState("DepositsTable") = dtNewDepositsTable

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Function UnSelectAllItemsInDepositsTable(dtNewDepositsTable As DataTable, nLead As Integer) As DataTable

        '   loop through out the whole table
        Dim i As Integer

        For i = 0 To dtNewDepositsTable.Rows.Count - 1
            If dtNewDepositsTable.Rows(i).Item("IsLead") = nLead Then
                '   set item to proper value
                dtNewDepositsTable.Rows(i).Item("Available") = 0
                '   set item to proper value
                dtNewDepositsTable.Rows(i).Item("Selected") = 0
            End If
        Next

        Return dtNewDepositsTable

    End Function




    ''' <summary>
    ''' Get the total deposit amount for the selected deposits
    ''' </summary>
    ''' <param name="depositsTable"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTotalDepositsAmount(ByVal depositsTable As DataTable) As String

        Dim total As Decimal = 0

        'loop through out the whole table
        Dim i As Integer
        For i = 0 To depositsTable.Rows.Count - 1
            '   select only selected deposits
            If depositsTable.Rows(i).Item("Selected") = 1 Then
                If (Not depositsTable.Rows(i).Item("TransAmount") Is DBNull.Value) Then
                    total += CType(depositsTable.Rows(i).Item("TransAmount"), Decimal)
                End If
            End If
        Next

        'return total amount
        Return total.ToString("c")

    End Function


    ''' <summary>
    ''' Create the xml stream object for the deposits to print to excel
    ''' </summary>
    ''' <param name="depositsTable"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateXmlStreamToPrint(ByVal depositsTable As DataTable) As MemoryStream

        Dim myMemoryStream As New MemoryStream

        Dim printXmlTextWriter As New XmlTextWriter(myMemoryStream, Nothing)

        'write xml document header
        printXmlTextWriter.Formatting = Formatting.Indented
        printXmlTextWriter.WriteStartDocument(False)
        printXmlTextWriter.WriteComment("FAME - This document contains a list of bank deposits")

        'output start of element "document"
        printXmlTextWriter.WriteStartElement("document")

        'output start of element "deposits"
        printXmlTextWriter.WriteStartElement("deposits")

        'output totalAmount
        printXmlTextWriter.WriteAttributeString("totalAmount", GetTotalDepositsAmount(depositsTable))

        'loop through out all deposits
        Dim i As Integer
        For i = 0 To depositsTable.Rows.Count - 1

            'print only selected deposits
            If depositsTable.Rows(i).Item("Selected") = 1 Then
                Dim row As DataRow = depositsTable.Rows(i)

                'output start of element "deposit"
                printXmlTextWriter.WriteStartElement("deposit")

                'output StudentName
                If Not DBNull.Value.Equals(row.Item("Name")) Then
                    printXmlTextWriter.WriteAttributeString("Name", CType(row.Item("Name"), String))
                Else
                    printXmlTextWriter.WriteAttributeString("Name", String.Empty)
                End If

                If Not DBNull.Value.Equals(row.Item("StudentNumber")) Then
                    printXmlTextWriter.WriteAttributeString("StudentNumber", CType(row.Item("StudentNumber"), String))
                Else
                    printXmlTextWriter.WriteAttributeString("StudentNumber", "")
                End If

                'output Bank
                printXmlTextWriter.WriteAttributeString("Lead-Student", CType(row.Item("EntityType"), String))

                'output Bank
                printXmlTextWriter.WriteAttributeString("Description", CType(row.Item("Description"), String))


                'output BankAccount
                If (Not row.Item("BankAcctDescrip") Is DBNull.Value) Then
                    printXmlTextWriter.WriteAttributeString("BankAccount", CType(row.Item("BankAcctDescrip"), String))
                Else
                    printXmlTextWriter.WriteAttributeString("BankAccount", String.Empty)
                End If

                'output Date
                If (Not row.Item("TransDate") Is DBNull.Value) Then
                    printXmlTextWriter.WriteAttributeString("Date", CType(row.Item("TransDate"), Date).ToShortDateString)
                Else
                    printXmlTextWriter.WriteAttributeString("Date", String.Empty)
                End If

                'output Reference
                If (Not row.Item("TransReference") Is DBNull.Value) Then
                    printXmlTextWriter.WriteAttributeString("Reference", CType(row.Item("TransReference"), String))
                Else
                    printXmlTextWriter.WriteAttributeString("Reference", String.Empty)
                End If


                'output Amount
                If (Not row.Item("TransAmount") Is DBNull.Value) Then
                    printXmlTextWriter.WriteAttributeString("Amount", CType(row.Item("TransAmount"), Decimal).ToString("c"))
                Else
                    printXmlTextWriter.WriteAttributeString("Amount", String.Empty)
                End If


                'output Reference
                If (Not row.Item("DepositState") Is DBNull.Value) Then
                    printXmlTextWriter.WriteAttributeString("Deposited", CType(row.Item("DepositState"), String))
                Else
                    printXmlTextWriter.WriteAttributeString("Deposited", String.Empty)
                End If

                'output end tag of element "deposit"
                printXmlTextWriter.WriteEndElement()
            End If

        Next

        'output end tag of element "deposits"
        printXmlTextWriter.WriteEndElement()

        'output end tag of element "document"
        printXmlTextWriter.WriteEndElement()

        'Write the XML to file and close the writer
        printXmlTextWriter.Flush()
        myMemoryStream.Position = 0

        'return memoryStream
        Return myMemoryStream

    End Function



#End Region

End Class