<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="LateFees.aspx.vb" Inherits="LateFees" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstLateFees">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstLateFees" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstLateFees" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstLateFees" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">

                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstLateFees" runat="server" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server"
                                            CommandArgument='<%# container.dataitem("LateFeesId")%>' Text='<%# Container.DataItem("LateFeesDescrip") + " " + Ctype(Container.DataItem("EffectiveDate"), Date).ToShortDateString %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both"
                orientation="horizontaltop">
                <asp:Panel ID="pnlRHS" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table width="100%" align="center">
                                        <asp:TextBox ID="txtLateFeesId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox
                                            ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtModUser"
                                                runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate"
                                                    runat="server" Visible="False">ModDate</asp:TextBox><tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblLateFeesCode" runat="server" CssClass="label">Code</asp:Label>
                                                        </td>
                                                        <td class="contentcell4" align="left">
                                                            <asp:TextBox ID="txtLateFeesCode" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="revCode" runat="server" ControlToValidate="txtLateFeesCode"
                                                                Display="None" ErrorMessage="too many characters" ValidationExpression=".{0,12}"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server" Visible="false">Status</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist" Visible="false" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblLateFeesDescrip" CssClass="label" runat="server">Description</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:TextBox ID="txtLateFeesDescrip" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ErrorMessage="Must Select a Campus Group"
                                                    Display="None" ControlToValidate="ddlCampGrpId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Campus Group</asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTransCodeId" CssClass="label" runat="server">Transaction Code</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:DropDownList ID="ddlTransCodeId" runat="server" CssClass="dropdownlist" Width="200px">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="TransCodeCompareValidator" runat="server" Display="None"
                                                    ErrorMessage="Must Select a Transaction Code" ControlToValidate="ddlTransCodeId"
                                                    Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Transaction Code</asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblEffectiveDate" CssClass="label" runat="server">Effective Date</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <telerik:RadDatePicker ID="txtEffectiveDate" MinDate="1/1/1945" runat="server" Width="200px">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:RadioButton ID="rbFlatAmount" runat="server" Text="Flat Amount" GroupName="Only" CssClass="label" />
                                            </td>

                                            <td class="contentcell4" align="left">
                                                <asp:TextBox ID="txtFlatAmount" CssClass="textbox" MaxLength="10"  Width="176px" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="None"
                                                    ErrorMessage="Date can not be blank" ControlToValidate="txtFlatAmount">Amount can not be blank</asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="AmountRangeValidator" runat="server" Display="None" ErrorMessage="Invalid Flat Amount"
                                                    ControlToValidate="txtFlatAmount" MinimumValue="0.00" MaximumValue="10000" Type="Currency">Invalid Flat Amount</asp:RangeValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:RadioButton ID="rbRate" runat="server" Text="Percent Rate %" GroupName="Only"
                                                    CssClass="label" Checked="true" />
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:TextBox ID="txtRate" CssClass="textbox" MaxLength="10"  Width="176px" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="None"
                                                    ErrorMessage="Percent Rate can not be blank" ControlToValidate="txtRate">Amount can not be blank</asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="RateRangevalidator" runat="server" Display="None" ErrorMessage="Invalid Percent Rate"
                                                    ControlToValidate="txtRate" MinimumValue="0.00" MaximumValue="100" Type="Currency">Invalid Percent Rate</asp:RangeValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblGracePeriod" CssClass="label" runat="server">Grace Period (days)</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left">
                                                <asp:TextBox ID="txtGracePeriod" CssClass="textbox" MaxLength="10"  Width="176px" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="None"
                                                    ErrorMessage="Grace Period can not be blank" ControlToValidate="txtGracePeriod">Amount can not be blank</asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="GracePeriodRangevalidator" runat="server" Display="None"
                                                    ErrorMessage="Invalid Grace Period" ControlToValidate="txtGracePeriod" MinimumValue="0"
                                                    MaximumValue="365" Type="Integer">Invalid Grace Period</asp:RangeValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:TextBox ID="txtRowIds" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                    <!--end table content-->
                                </div>

                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>

</asp:Content>


