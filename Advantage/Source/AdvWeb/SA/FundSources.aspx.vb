'Imports System.Xml


' ===============================================================================
'
' FAME AdvantageV1
'
' FundSources.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
'Imports FAME.Common
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class FundSources
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Dim cmpCallReportingAgencies As New ReportingAgencies
    Protected intRptFldId As Integer
    Protected boolRptAgencyAppToSchool As Boolean



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ViewState("IsUserSa") = advantageUserState.IsUserSA

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '' New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 17676
            If (Not advantageUserState.IsUserSA And Not advantageUserState.IsUserSuper And Not advantageUserState.IsUserSupport) Then
                txtCutOffDate.Enabled = False
            End If

            BindDataList()

            '   bind an empty new FundSourceInfo
            BindFundSourceData(New FundSourceInfo)

        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            'Call the procedure for edit
            ' InitButtonsForEdit()
        End If
        boolRptAgencyAppToSchool = AdvantageCommonValues.CheckIfReportingAgenciesExistForSchool
        If boolRptAgencyAppToSchool = True Then
            pnlReportingAgenciesHeader.Visible = True
            intRptFldId = AdvantageCommonValues.FundSources
            cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtFundSourceId.Text)
        Else
            pnlReportingAgenciesHeader.Visible = False
        End If
    End Sub

    Private Sub BindDataList()

        Dim boolStatus As String

        If radstatus.SelectedItem.Text.ToLower = "active" Then
            boolStatus = "True"
        ElseIf radstatus.SelectedItem.Text.ToLower = "inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If

        '   bind FundSources datalist
        With New StudentsAccountsFacade
            dlstFundSources.DataSource = .GetAllFundSources(boolStatus)
            dlstFundSources.DataBind()
        End With

    End Sub

    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
        BuildAwardTypesDDL()
        BuildAdvantageFundSourcesDDL()
    End Sub

    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub

    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub BuildAwardTypesDDL()
        '   bind the status DDL
        Dim AwardTypes As New StudentsAccountsFacade

        With ddlAwardTypeId
            .DataTextField = "Descrip"
            .DataValueField = "AwardTypeId"
            .DataSource = AwardTypes.GetAllAwardTypes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildAdvantageFundSourcesDDL()
        '   bind the status DDL
        Dim AwardTypes As New StudentsAccountsFacade
        With ddlAdvFundSourceId
            .DataTextField = "Descrip"
            .DataValueField = "AdvFundSourceId"
            .DataSource = AwardTypes.GetAdvantageFundSources()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub dlstFundSources_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstFundSources.ItemCommand
        '   get the FundSource from the backend and display it
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()
        Dim strGuid As String = ""

        Dim strId As String = dlstFundSources.DataKeys(e.Item.ItemIndex).ToString()
        strGuid = dlstFundSources.DataKeys(e.Item.ItemIndex).ToString()

        GetFundSourceId(strId)

        cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, strId)
        '   set Style to Selected Item
        CommonWebUtilities.RestoreItemValues(dlstFundSources, strId)
        '   initialize buttons
        InitButtonsForEdit()

    End Sub

    Private Sub BindFundSourceData(ByVal FundSource As FundSourceInfo)
        With FundSource
            chkIsInDB.Checked = .IsInDB
            txtFundSourceId.Text = .FundSourceId
            txtFundSourceCode.Text = .Code
            If Not (FundSource.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = FundSource.StatusId
            txtFundSourceDescrip.Text = .Description
            If Not (FundSource.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = FundSource.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            If Not (FundSource.AwardTypeId = 0) Then
                ddlAwardTypeId.SelectedValue = FundSource.AwardTypeId
            Else
                ddlAwardTypeId.SelectedIndex = 0
            End If
            If Not FundSource.AdvFundSourceId = "" Then ddlAdvFundSourceId.SelectedValue = FundSource.AdvFundSourceId Else ddlAdvFundSourceId.SelectedValue = ""
            If .TitleIV = True Then
                chkTitleIV.Checked = True
            Else
                chkTitleIV.Checked = False
            End If

            'New Code Added By Vijay Ramteke On June 09, 2010
            If .CutoffDate = Nothing Then
                txtCutOffDate.SelectedDate = Nothing
            Else
                txtCutOffDate.SelectedDate = CDate(.CutoffDate.ToShortDateString)
            End If
            'New Code Added By Vijay Ramteke On June 09, 2010

            Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)

            If Not isUserSa Then
                chkTitleIV.Enabled = Not .IsUsedInFA
            End If
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim result As String
        With New StudentsAccountsFacade
            '   update FundSource Info 
            result = .UpdateFundSourceInfo(BuildFundSourceInfo(txtFundSourceId.Text), Session("UserName"))
        End With


        BindDataList()

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the FundSource from the backend and display it
            GetFundSourceId(txtFundSourceId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new FundSourceInfo
            'BindFundSourceData(New FundSourceInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked
        Dim arrReportingAgencyValue As ArrayList
        Dim z As Integer
        'Try
        arrReportingAgencyValue = cmpCallReportingAgencies.GetAllValues(pnlReportingAgencies)
        ' DE 1131 Janet Robinson 04/25/2011
        'DE8804
        'z = cmpCallReportingAgencies.InsertValues(txtFundSourceId.Text, arrReportingAgencyValue, "saFundSources")
        z = cmpCallReportingAgencies.InsertValues(txtFundSourceId.Text, arrReportingAgencyValue, "saFundSources", False)

        Dim sReportAgencies As String
        Dim iResult As Integer
        sReportAgencies = AdvantageCommonValues.GetReportingAgenciesApplicableToSchool
        If sReportAgencies.Contains(1) Then '1 is Agency value for IPEDS 
            Dim BuildIPEDsDB As New ReportingAgencyFacade
            iResult = BuildIPEDsDB.UpdateIPEDSValues(arrReportingAgencyValue(0).ToString, txtFundSourceId.Text, "saFundSources")
        End If
        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        ' End Try
        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CommonWebUtilities.RestoreItemValues(dlstFundSources, txtFundSourceId.Text)
    End Sub

    Private Function BuildFundSourceInfo(ByVal FundSourceId As String) As FundSourceInfo

        '   instantiate class
        Dim FundSourceInfo As New FundSourceInfo

        With FundSourceInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get FundSourceId
            .FundSourceId = FundSourceId

            '   get Code
            .Code = txtFundSourceCode.Text.Trim

            '   get StatusId
            .StatusId = ddlStatusId.SelectedValue

            '   get FundSource's name
            .Description = txtFundSourceDescrip.Text.Trim

            '   get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

            If ddlAwardTypeId.SelectedValue = "" Then
                .AwardTypeId = 0
            Else
                .AwardTypeId = ddlAwardTypeId.SelectedValue
            End If

            .AdvFundSourceId = ddlAdvFundSourceId.SelectedValue

            If (chkTitleIV.Checked) Then
                .TitleIV = 1
            Else
                .TitleIV = 0
            End If

            .IsUsedInFA = Not chkTitleIV.Enabled

            'New Code Added By Vijay Ramteke On June 09, 2010
            Try
                .CutoffDate = Date.Parse(txtCutOffDate.SelectedDate)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                .CutoffDate = Nothing
            End Try
            'New Code Added By Vijay Ramteke On June 09, 2010

        End With

        '   return data
        Return FundSourceInfo

    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new FundSourceInfo
        BindFundSourceData(New FundSourceInfo)

        cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtFundSourceId.Text)

        '   initialize buttons
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlstFundSources, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim z As Integer
        If Not (txtFundSourceId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim saf As New StudentsAccountsFacade
            z = cmpCallReportingAgencies.InsertValues(txtFundSourceId.Text, Nothing)
            '   update FundSource Info 
            Dim result As String = saf.DeleteFundSourceInfo(txtFundSourceId.Text, Date.Parse(txtModDate.Text))
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else

                BindDataList()

                '   bind an empty new FundSourceInfo
                BindFundSourceData(New FundSourceInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If

        End If
        cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtFundSourceId.Text)
        CommonWebUtilities.RestoreItemValues(dlstFundSources, Guid.Empty.ToString)
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If
    End Sub
    Private Sub GetFundSourceId(ByVal FundSourceId As String)
        '   Create a StudentsFacade Instance
        Dim saf As New StudentsAccountsFacade

        '   bind FundSource properties
        BindFundSourceData(saf.GetFundSourceInfo(FundSourceId))
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        Dim objCommon As New CommonUtilities

        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
        BindFundSourceData(New FundSourceInfo)

        BindDataList()

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

End Class
