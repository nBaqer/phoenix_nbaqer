﻿<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ApplyPayments.aspx.vb" Inherits="ApplyPayments" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False" OnClick="btnSave_Click"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>


                                        <!-- Begin Page Content-->
                                        <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" border="0">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgrdPaymentsToApply" Width="100%" runat="server" AutoGenerateColumns="False"
                                                        BorderWidth="1px" BorderColor="#E0E0E0" BorderStyle="Solid" AllowPaging="true" PageSize="10" AutoGenerateSelectButton="true" DataKeyNames="TransactionId">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Student Name">
                                                                <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbStudentName" Text='<%# Bind("StudentName") %>' runat="server" />

                                                                    <asp:HiddenField ID="hdnUnAppliedAmount" runat="server" Value='<%# Bind("UnAppliedAmount") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="PrgVerDescrip" HeaderStyle-CssClass="datagridheaderstyle"
                                                                ItemStyle-CssClass="datagriditemstyle" HeaderText="Program" />
                                                            <asp:BoundField DataField="TransDate" HeaderStyle-CssClass="datagridheaderstyle"
                                                                ItemStyle-CssClass="datagriditemstyle" HeaderText="Payment Date" DataFormatString="{0:d}"
                                                                HtmlEncode="false" />
                                                            <asp:BoundField DataField="TransAmount" HeaderStyle-CssClass="datagridheaderstyle"
                                                                ItemStyle-CssClass="datagriditemstyle" HeaderText="Payment Amount" DataFormatString="{0:c}"
                                                                HtmlEncode="false" />
                                                            <asp:BoundField DataField="UnAppliedAmount" HeaderStyle-CssClass="datagridheaderstyle"
                                                                ItemStyle-CssClass="datagriditemstyle" HeaderText="UnApplied" DataFormatString="{0:c}"
                                                                HtmlEncode="false" />
                                                            <asp:BoundField DataField="TransDescrip" HeaderStyle-CssClass="datagridheaderstyle"
                                                                ItemStyle-CssClass="datagriditemstyle" HeaderText="Payment Description" />
                                                        </Columns>
                                                        <SelectedRowStyle Font-Bold="true" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                  
                                    <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" border="0">
                                        <tr>
                                            <td>
                                                <div style="width: 100%; margin-top: 25px;">
                                                    <asp:DataGrid ID="dgrdApplyPayment" runat="server" EditItemStyle-Wrap="false" HeaderStyle-Wrap="true"
                                                        AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="True"
                                                        Width="100%" BorderWidth="1px" BorderColor="#E0E0E0" OnItemDataBound="dgrdApplyPayment_ItemDataBound">
                                                        <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Transaction">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPaymentId" runat="server" Visible="false" Text='<%# Container.DataItem("PaymentId") %>'>
                                                                    </asp:Label>
                                                                    <asp:Label ID="lblTotalAmountToApply" runat="server" Visible="false">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lblTransactionId" runat="server" Visible="false" Text='<%# Container.DataItem("TransactionId") %>'>
                                                                    </asp:Label>
                                                                    <asp:Label ID="lblTransaction" runat="server" Text='<%# Container.DataItem("TransCodeDescrip") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransDate", "{0:d}") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Original Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOriginalAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransAmount", "{0:c}") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Balance">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBalance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Balance", "{0:c}") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Amount to Apply">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtAmountToApply" runat="server" CssClass="textbox">0.00</asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>


                                    </div>


                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>

