﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class StudentRevenueLedger
    Inherits BasePage

    Protected StudentId As String
    Protected boolSwitchCampus As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected state As AdvantageSessionState
    Protected LeadId As String
    Dim resourceId As Integer



    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try
            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)



        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim campusId As String
        Session("SEARCH") = 0
        resourceId = HttpContext.Current.Items("ResourceId")
        campusid = Master.CurrentCampusId

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(177) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With


        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            'header1.EnableHistoryButton(False)

            '   build dropdownlists
            BuildDropDownLists()

            '   bind Datalist
            BindDataList()
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            'End If
            MyBase.uSearchEntityControlId.Value = ""
        End If
    End Sub
    Private Sub BuildDropDownLists()
        BuildStudentEnrollmentsDDL(StudentId)
    End Sub
    Private Sub BindDatagrid(ByVal StuEnrollId As String, ByVal transCodeId As String)
        Dim ds As New DataSet
        Dim saf As New StudentsAccountsFacade
        Dim drRows() As DataRow
        Dim strFilter As String


        ds = saf.GetDeferredRevenueDetailsByEnrollment(StuEnrollId, transCodeId)
        'Get the parent records. Those are the ones with Source = 0.
        strFilter = "Source = 0"
        drRows = ds.Tables("RevenueLedgerDS").Select(strFilter)

        dgrdStudentRevenueLedger.MasterTableView.DataSource = drRows
        dgrdStudentRevenueLedger.DataBind()

        Session("RevenueLedgerDS") = ds
    End Sub

    Protected Sub dgrdStudentRevenueLedger_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles dgrdStudentRevenueLedger.NeedDataSource
        If Not e.IsFromDetailTable Then
            If Not IsNothing(Session("RevenueLedgerDS")) Then
                Dim drRows() As DataRow
                Dim strFilter As String
                Dim ds As DataSet

                ds = CType(Session("RevenueLedgerDS"), DataSet)
                strFilter = "Source = 0"

                drRows = ds.Tables("RevenueLedgerDS").Select(strFilter)

                dgrdStudentRevenueLedger.MasterTableView.DataSource = drRows

            End If

        End If
    End Sub

    Protected Sub dgrdStudentRevenueLedger_DetailTableDataBind(ByVal source As Object, ByVal e As GridDetailTableDataBindEventArgs) Handles dgrdStudentRevenueLedger.DetailTableDataBind
        Dim dataItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)
        Select Case e.DetailTableView.Name
            Case "Earnings"
                Dim drRows() As DataRow
                Dim strFilter As String
                Dim ds As DataSet
                Dim TransactionId As String

                ds = CType(Session("RevenueLedgerDS"), DataSet)

                TransactionId = dataItem.GetDataKeyValue("TransactionId").ToString()
                strFilter = "Source = 1 and TransactionId = '" + TransactionId + "'"

                drRows = ds.Tables("RevenueLedgerDS").Select(strFilter)

                e.DetailTableView.DataSource = drRows

        End Select
    End Sub

    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade

        With ddlEnrollmentId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()
        End With
    End Sub
    'Private Sub dgrdStudentRevenueLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdStudentRevenueLedger.ItemDataBound
    '    '   accept only items (no header or footer records)
    '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '        '   get value of Amount column
    '        Dim amount As Decimal = CType(e.Item.DataItem, DataRowView).Item("Amount")
    '        If amount < 0 Then
    '            '   set Fees amount
    '            CType(e.Item.FindControl("lblFees"), Label).Text = ((-1) * amount).ToString("###,###.00")
    '        Else
    '            '   set Earnings amount
    '            CType(e.Item.FindControl("lblEarnings"), Label).Text = amount.ToString("###,###.00")
    '        End If
    '    End If
    'End Sub
    Private Sub BindDataList()
        '   bind dlstStudentRevenueLedger datalist
        With New StudentsAccountsFacade
            dlstStudentRevenueLedger.DataSource = .GetDeferredRevenueTotalsByEnrollment(ddlEnrollmentId.SelectedValue)
            dlstStudentRevenueLedger.DataBind()
        End With

        '   bind an empty datagrid
        BindDatagrid(ddlEnrollmentId.SelectedValue, Guid.Empty.ToString)

    End Sub

    Private Sub dlstStudentRevenueLedger_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstStudentRevenueLedger.ItemCommand
        '   bind datagrid
        BindDatagrid(ddlEnrollmentId.SelectedValue, e.CommandArgument)

        '   set Style to Selected Item
        CommonWebUtilities.SetStyleToSelectedItem(dlstStudentRevenueLedger, e.CommandArgument, ViewState)

    End Sub
    Protected Function ConvertBooleanToYesNo(ByVal booleanValue As Boolean) As String
        If booleanValue Then Return "Yes" Else Return "No"
    End Function
    Protected Function ConvertSourceToVerbose(ByVal source As Integer) As String
        Select Case source
            Case 0
                Return "Student Ledger"
            Case 1
                Return "Revenue Calc."
            Case 2
                Return "Adjustment"
        End Select
        Return String.Empty
    End Function

    Private Sub ddlEnrollmentId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
        '   bind Datalist
        BindDataList()
    End Sub

End Class
