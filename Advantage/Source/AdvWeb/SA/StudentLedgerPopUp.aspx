<%@ Reference Page="~/AR/StudentEnrollments.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="StudentLedgerPopUp" CodeFile="StudentLedgerPopUp.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Student Ledger</title>
		<meta content="False" name="vs_snapToGrid">
		<meta content="True" name="vs_showGrid">
		<LINK href="../CSS/localhost.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
  </HEAD>
	<body runat="server" leftMargin="0" topMargin="0" ID="Body1">
		<form id="Form1" method="post" runat="server">
			<table class="SAtable" id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<td class="detailsframe">
					<div class="scrollright" style="LEFT: 0px; TOP: 10px; HEIGHT: 480px;">
							<!-- begin table content-->
							<table class="contenttable" cellSpacing="0" cellPadding="0" width="100%">
								<tr>
									<TD noWrap width="30%"></TD>
									<td noWrap width="20%" colSpan="2"><asp:label id="lblEnrollmentId" CssClass="Label" Runat="server">Enrollment</asp:label></td>
									<TD width="20%"><asp:dropdownlist id="ddlEnrollmentId" runat="server" CssClass="DropDownList"></asp:dropdownlist></TD>
									<td vAlign="middle" width="30%" rowSpan="3"><asp:button id="btnBuild" runat="server"  Text="Show Ledger"></asp:button></td>
								</tr>
								<tr>
									<td colSpan="5" height="5"></td>
								</tr>
								<tr>
									<TD></TD>
									<TD colSpan="2"><asp:label id="lblTerms" runat="server" CssClass="Label">Terms</asp:label></TD>
									<TD><asp:dropdownlist id="ddlTermsId" runat="server" CssClass="DropDownList"></asp:dropdownlist></TD>
								</tr>
								<tr>
									<td colSpan="5" height="5"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD colSpan="2"><asp:label id="lblBalanceTitle" runat="server" CssClass="Label">Balance</asp:label></TD>
									<TD><asp:label id="lblBalance" runat="server" CssClass="labelBold">Balance</asp:label></TD>
									<td></td>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="2"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="2">
										<asp:Label id="lblProjectedHdr" runat="server" CssClass="label">Projected</asp:Label></TD>
									<TD>
										<asp:Label id="lblProjected" runat="server" CssClass="labelBold">Projected Amount</asp:Label></TD>
									<TD></TD>
								</TR>
								<tr>
									<td colSpan="5" height="5"></td>
								</tr>
							</table>
							<table class="contenttable" cellSpacing="0" cellPadding="0" width="100%">
								<tr>
									<td><asp:datagrid id="dgrdStudentLedger" runat="server" width="100%" HorizontalAlign="Center" BorderStyle="Solid" 
											AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0">
											<AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
											<ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
											<HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="TransDate" SortExpression="TransDate desc, ModDate desc" HeaderText="Date" 
 DataFormatString="{0:d}">
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="TransReference" SortExpression="TransReference" ReadOnly="True" HeaderText="Reference"></asp:BoundColumn>
												<asp:BoundColumn DataField="TransCodeDescrip" SortExpression="TransCodeDescrip" ReadOnly="True" HeaderText="Trans. Code"></asp:BoundColumn>
												<asp:BoundColumn DataField="TransDescrip" SortExpression="TransDescrip" ReadOnly="True" HeaderText="Description"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="TransAmount" SortExpression="TransAmount" ReadOnly="True" 
 HeaderText="Amount"></asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Amount">
													<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Amount" runat="server"></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Balance">
													<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=Balance runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Balance", "{0:c}") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="AcademicYearDescrip" SortExpression="AcademicYearDescrip" ReadOnly="True" 
 HeaderText="Academic Year"></asp:BoundColumn>
												<asp:BoundColumn DataField="TermDescrip" SortExpression="TermDescrip" ReadOnly="True" HeaderText="Term"></asp:BoundColumn>
												<asp:BoundColumn DataField="TransTypeDescrip" SortExpression="TransTypeId" ReadOnly="True" HeaderText="Trans. Type">
													<HeaderStyle Width="40px"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ModUser" SortExpression="ModUser" ReadOnly="True" HeaderText="User"></asp:BoundColumn>
											</Columns>
										</asp:datagrid>
									</td>
								</tr>
							</table>
							<!--end table content-->
							</div>
					</td>
				</TR>				
			</table><!-- start validation panel--><asp:panel id="Panel1" runat="server" CssClass="ValidationSummary"></asp:panel><asp:customvalidator id="Customvalidator1" runat="server" CssClass="ValidationSummary" ErrorMessage="CustomValidator"
					Display="None"></asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
					ShowSummary="False"></asp:validationsummary>
				<!--end validation panel-->
		</form>
	</body>
</HTML>
