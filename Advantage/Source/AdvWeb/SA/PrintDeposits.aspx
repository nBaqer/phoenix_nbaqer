﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PrintDeposits.aspx.vb" Inherits="PrintDeposits" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <style type="text/css">
        .RadForm.rfdLabel label {
            vertical-align: middle !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->

                        <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <table cellpadding="0" cellspacing="0" class="contenttable" align="center" style="width: 100%">
                                            <tr>
                                                <td class="contentcell" style="width: 20%">
                                                    <asp:Label ID="lblStartDate" runat="server" CssClass="label">Start Date</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <telerik:RadDatePicker ID="txtStartDate" runat="server" Width="200px">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" style="width: 20%">
                                                    <asp:Label ID="lblEndDate" runat="server" CssClass="label">End Date</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <telerik:RadDatePicker ID="txtEndDate" runat="server" Width="200px">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" style="width: 20%">
                                                    <asp:Label ID="lblBankAcctId" runat="server" CssClass="label">Bank Codes/Bank Accounts</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlBankAcctId" runat="server" CssClass="dropdownlist" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" style="width: 20%">
                                                    <asp:Label ID="Label1" runat="server" CssClass="label">Show Deposited/Not-Deposited</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlShowDeposited" runat="server" CssClass="dropdownlist" Width="200px">
                                                        <asp:ListItem Text="Show Undeposited Items" Value="0" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Show Deposited Items (Save Disabled)" Value="1"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell"></td>
                                                <td class="contentcell">
                                                    <asp:Button ID="btnSelect" runat="server" Text="Select"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table cellspacing="0" cellpadding="0" width="100%" align="center" style="margin: 0px;">
                                            <tr>
                                                <td width="25%" height="15px">&nbsp;<asp:Label ID="lblNumRecords" runat="server" CssClass="label"></asp:Label></td>
                                                <td width="50%" height="15px" class="printdepheader" nowrap align="left" valign="baseline">
                                                    <asp:Label ID="lblAvailableDeposits" runat="server" CssClass="label" Font-Bold="True">Deposits Available </asp:Label>
                                                </td>
                                                <td width="25%" height="15px" nowrap align="right">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="right">
                                                                <asp:LinkButton runat="server" ID="imgExport" AlternateText="Export to Excel" ToolTip="Export to Excel">
                                                                    <span class="k-icon k-i-file-excel font-light-green-darken-4"></span>
                                                                </asp:LinkButton>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="checkbox" Text="Select All" OnCheckedChanged="chkSelectAll_CheckChanged" AutoPostBack="true"></asp:CheckBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td class="printdepheader" nowrap align="center" colspan="3">
                                                    <div style="overflow: auto; width: 100%; height: 450px">
                                                        <asp:DataGrid ID="dgrdAvailableDeposits" PagerStyle-Mode="NumericPages" AllowPaging="false" runat="server" AutoGenerateColumns="False"
                                                            BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0" Width="100%">
                                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTransactionIdA" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransactionId") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="Name" ReadOnly="True" HeaderStyle-CssClass="datalistheader"
                                                                    ItemStyle-CssClass="datalist" HeaderText="Name"></asp:BoundColumn>
                                                                <asp:TemplateColumn HeaderStyle-CssClass="datalistheader" ItemStyle-CssClass="datalist" Visible="True"
                                                                    HeaderText="Lead/Student">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="EntityTypeA" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EntityType") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="65px"></ItemStyle>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="StudentNumber" ReadOnly="True" HeaderStyle-CssClass="datalistheader"
                                                                    ItemStyle-CssClass="datalist" HeaderText="Student Number">
                                                                    <ItemStyle Width="75px"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="DESCRIPTION" HeaderStyle-CssClass="datalistheader" ItemStyle-CssClass="datalist"
                                                                    HeaderText="Description"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Bankdescrip" HeaderStyle-CssClass="datalistheader" ItemStyle-CssClass="datalist"
                                                                    ReadOnly="True" HeaderText="Bank" Visible="false"></asp:BoundColumn>

                                                                <asp:BoundColumn DataField="BankAcctDescrip" ReadOnly="True" HeaderStyle-CssClass="datalistheader"
                                                                    ItemStyle-CssClass="datalist" HeaderText="Bank Account"></asp:BoundColumn>

                                                                <asp:BoundColumn DataField="TransDate" HeaderStyle-CssClass="datalistheader" ItemStyle-CssClass="datalist"
                                                                    ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}">
                                                                    <ItemStyle Width="75px"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TransReference" HeaderStyle-CssClass="datalistheader"
                                                                    ItemStyle-CssClass="datalist" ReadOnly="True" HeaderText="Reference"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="DepositState" HeaderStyle-CssClass="datalistheader" ItemStyle-CssClass="datalist"
                                                                    HeaderText="Deposited?">
                                                                    <ItemStyle Width="25px"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TransAmount" HeaderStyle-CssClass="datalistheader" ItemStyle-CssClass="datalist"
                                                                    HeaderText="Amount" DataFormatString="{0:c}">
                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:TemplateColumn HeaderStyle-CssClass="datalistheader" ItemStyle-CssClass="datalist"
                                                                    HeaderText="Selected">
                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelectedA" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Selected") %>'></asp:CheckBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"></td>
                                            </tr>
                                        </table>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

