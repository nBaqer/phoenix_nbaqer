Imports System.Diagnostics
Imports FAME.Advantage.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common

Partial Class Report
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim student, address, cityStateZip, program, status, studentNumber, studentNumberDesc As String
    '-----corporate placeholders
    Dim corporateName, corporateAddress, corporateCityStateZip, corporatePhone, corporateFax As String
    Dim campusid As String
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here

        'Dim advantageUserState As New Advantage.Business.Objects.User()
        'advantageUserState = AdvantageSession.UserState
        campusid = AdvantageSession.UserState.CampusId.ToString


        GenerateInvoice()
    End Sub
    Private Sub GenerateInvoice()
        Dim ds As DataSet
        Dim dr As DataRow
        Dim recCount As Integer ', SumOfInvoice
        'Dim sb As New System.Text.StringBuilder
        Dim stuEnrollId As String = String.Empty
        Dim total As Double = 0
        Const bodySize As Integer = 21
        Dim facInputMasks As New InputMasksFacade
        Dim stuIden As String = CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String)

        'Get the mask for phone numbers and zip
        Dim ssnMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            ds = (New StuDebitInvoiceFacade).GetInvoiceDS(campusid, CType(Session("StuEnrollIdList"), String), Request("Date"))

            SetUpReport()


            For Each dr In ds.Tables(0).Rows
                Student = dr("Student").ToString()
                Address = dr("Address").ToString()
                CityStateZip = dr("City").ToString() & " " & dr("StateDescrip").ToString() & " " & dr("Zip").ToString()
                Program = dr("PrgVerDescrip").ToString()
                Status = dr("StatusCodeDescrip").ToString()
                If (stuIden.ToUpper() = "SSN") Then
                    StudentNumberDesc = "SSN"
                    'Dim ssnLength As Integer = dr("SSN").ToString().Length
                    'If (ssnLength > 4) Then StudentNumber = dr("SSN").ToString().Substring(ssnLength - 4)
                    If dr("SSN").ToString.Length >= 1 Then
                        Dim temp As String = CType(dr("SSN"), String)
                        StudentNumber = facInputMasks.ApplyMask(ssnMask, "*****" & temp.Substring(5))
                    Else
                        StudentNumber = ""
                    End If

                Else
                    StudentNumberDesc = "Student Number"
                    If (Not dr("StudentNumber") Is Nothing) Then
                        StudentNumber = dr("StudentNumber").ToString()
                    End If
                End If


                'if its the first page of the report
                If recCount = 0 Then
                    WriteHeader()
                    stuEnrollId = dr("StuEnrollId").ToString()
                Else
                    'if the the enrollment has changed 
                    If stuEnrollId <> dr("StuEnrollId").ToString() Then
                        WriteSpacers(bodySize - recCount)
                        WriteFooter(total)
                        BreakPage()
                        WriteHeader()
                        recCount = 0
                        total = 0
                        stuEnrollId = dr("StuEnrollId").ToString()
                    Else

                        'if the the page has run out of space
                        If recCount >= bodySize Then
                            WriteFooter(total)
                            BreakPage()
                            WriteHeader()
                            recCount = 0
                        End If
                    End If
                End If
                recCount += 1
                total += CType(dr("Amount"), Double)

                WriteBody(CType(recCount, String), dr("ExpectedDate").ToString, CType(("Payment Due - " & dr("PayPlanDescrip")), String), String.Format("{0:c}", dr("Amount")))
            Next
            'if no more records but still need spaces
            If recCount <= bodySize Then
                WriteSpacers(bodySize - recCount)
            End If

            WriteFooter(total)
            EndReport()

            ds.Dispose()
            'ds = Nothing

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub GenerateInvoice" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub GenerateInvoice" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")

        End Try
    End Sub
    Private Sub SetUpReport()
        GetCorporateInfo()
        Response.Write("<html><head><title>Student Debit Invoice Report</title>")
        Response.Write("<link rel='stylesheet' type='text/css' href='../css/LetterForScreen.css' media='screen' />")
        Response.Write("<link rel='stylesheet' type='text/css' href='../css/LetterForPrinting.css' media='print' />")
        Response.Write("<style type='text/css'>")
        Response.Write(".table1 {border: 1px solid #000000;}")
        Response.Write(".leftheadercell { border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; font-family: Arial, Helvetica, sans-serif;font-size: 14;}")
        Response.Write(".headercells {border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000;font-family: Arial, Helvetica, sans-serif;font-size: 14;}")
        Response.Write(".leftinnercell {border-left: 1px solid #000000;font-family: Arial, Helvetica, sans-serif;font-size: 14;}")
        'Response.Write(".leftinnercell {border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; }")
        'Response.Write(".innercells {border-right: 1px solid #000000; border-bottom: 1px solid #000000;}")
        Response.Write(".spacerleft {border-left: 1px solid #000000;font-family: Arial, Helvetica, sans-serif;font-size: 14;}")
        Response.Write(".spacerright {border-right: 1px solid #000000;font-family: Arial, Helvetica, sans-serif;font-size: 14;}")
        Response.Write(".spacertotal {border-right: 1px solid #000000;font-family: Arial, Helvetica, sans-serif;font-size: 14;}")
        Response.Write("</style></head><body class='letterbase'>")
        'Response.Write("<table class='lettertable' cellpadding='0' cellspacing='0' width='740'><tr><td valign='top' align='center'>")
    End Sub
    Private Sub WriteHeader()
        Response.Write("<table class='lettertable' cellpadding='0' cellspacing='0' width='670' align='center'><tr><td valign='top' align='center'>")
        Response.Write("<table ID='Table1' width='100%'><tr><td><IMG SRC='../SY/getschoollogo.aspx' height='60' width='60'>")
        Response.Write("</td><td><FONT size='5'; font-family: Arial, Helvetica, sans-serif>" & MyAdvAppSettings.AppSettings("SchoolName") & "</FONT></td><td class='ArialTD'>Invoice No. <br>" & GetRandomNumber() & "</FONT>")
        Response.Write("</td></tr></table><table width='640' ID='Table9' align='center'><tr><td>")
        Response.Write("<table align='center' ID='Table2' width='100%'><tr><td class='ArialTD'>Bill To:</td></tr><tr><td width='50%'>")
        Response.Write("<table ID='Table3' width='100%' cellpadding='2' cellspacing='0' class='table1'><tr>	<td class='ArialTD'>")
        Response.Write(Student & "</td></tr><tr><td class='ArialTD'>" & Address & "&nbsp;</td></tr><tr><td class='ArialTD'>")
        Response.Write(CityStateZip & "&nbsp;</td></tr></table></td><td width='5%'>&nbsp;</td><td width='45%'>")
        Response.Write("<table ID='Table4' width='100%' cellpadding='2' cellspacing='0' class='table1'>")
        Response.Write("<tr><td class='ArialTD'>Date:</td><td class='ArialTD'>" & Date.Now.Date & "</td></tr><tr><td class='ArialTD'>")
        Response.Write("Program:</td><td class='ArialTD'>" & Program & "</td></tr><tr><td class='ArialTD'>Status:</td>")
        Response.Write("<td class='ArialTD'>" & Status & "</td></tr>")
        Response.Write("<tr><td class='ArialTD'>" & StudentNumberDesc & ":</td><td class='ArialTD'>" & StudentNumber & "</td></tr>")
        Response.Write("</table></td></tr></table><br>")
        Response.Write("<table align='center' ID='Table10' width='100%'><tr><td>")
        Response.Write("<table align='center' ID='Table5' width='100%' cellpadding='2' cellspacing='0' border='0'>")
        Response.Write("<tr><td class='leftheadercell'>Item</td><td class='headercells'>Due Date</td>")
        Response.Write("<td class='headercells'>Description</td><td class='headercells'>Total</td></tr>")

    End Sub
    Private Sub WriteBody(ByVal item As String, ByVal dueDate As Date, ByVal description As String, ByVal paymentDueTotal As String)
        Response.Write("<tr><td class='leftinnercell'>" & item & "</td>")
        Response.Write("<td class='ArialTD'>" & FormatDateTime(dueDate, DateFormat.ShortDate) & "</td>")
        Response.Write("<td class='ArialTD'>" & description & "</td>")
        Response.Write("<td class='spacerright'>" & paymentDueTotal & "</td></tr>")
    End Sub
    Private Sub WriteFooter(ByVal totalAmount As Double)
        Response.Write("<tr><td colspan='3' align='right' class='leftheadercell'>")
        Response.Write("Total:</td><td class='headercells'>" & String.Format("{0:c}", totalAmount) & "</td></tr></table></td></tr></table>")
        Response.Write("<br><table align='center' ID='Table6' width='100%' border='0'><tr><td colspan='2' class='ArialTD'>")
        Response.Write("Comment:</td></tr><tr><td valign='top' width='40%'>")
        Response.Write("<table ID='Table7' cellpadding='2' cellspacing='0' class='table1' width='100%'>")
        Response.Write("<tr><td valign='center' align='center' height='130'>")
        Response.Write("<textarea rows='7' cols='27'></textarea>")
        Response.Write("</td></tr></table></td><td width='60%'>")
        Response.Write("<table ID='Table8' cellpadding='2' cellspacing='0' class='table1' width='100%'>")
        Response.Write("<tr><td valign='top' height='130' class='ArialTD'>Please remit " & String.Format("{0:c}", totalAmount) & " upon receipt of this invoice to:")
        Response.Write("<br><br>" & CorporateName & "<br>" & CorporateAddress & "<br>" & CorporateCityStateZip & "")
        Response.Write("<br>" & CorporatePhone & CorporateFax & "</td></tr></table></td></tr></table></td></tr></table>")

        Response.Write("</td></tr></table>")
    End Sub
    Private Sub GetCorporateInfo()
        Dim corporateData As CorporateInfo
        ''Modified by saraswathi lakshmanan on June 17 2009
        ''The invoice address info is printed in the recipts
        ''to fix mantis case 13365
        'CorporateData = (New CorporateFacade).GetCorporateInfo
        corporateData = (New CorporateFacade).GetInvoiceAddressInfo(campusid)
        With corporateData
            CorporateName = .CorporateName
            CorporateAddress = .Address1 & " " & .Address2
            CorporateCityStateZip = .City & ", " & .State & " " & .Zip
            CorporatePhone = .Phone
            If .Fax.ToString <> "" Then
                CorporateFax = " / " & .Fax & " FAX"
            End If
        End With
    End Sub
    Private Sub WriteSpacers(ByVal numOfSpacers As Integer)
        Dim x As Integer
        For x = 1 To numOfSpacers
            Response.Write("<tr><td class='leftinnercell'>&nbsp;")
            Response.Write("</td><td>&nbsp;</td>")
            Response.Write("<td>&nbsp;</td>")
            Response.Write("<td class='spacerright'>&nbsp;</td></tr>")
        Next
    End Sub

    Private Sub EndReport()
        'Response.Write("</td></tr></table></body></html>")
        Response.Write("</body></html>")
    End Sub

    Private Sub BreakPage()
        Response.Write("<p style='page-break-after: always;'></p>")
    End Sub

    Private Function GetRandomNumber() As String
        Dim i As Integer
        Dim sChar As String
        Dim sId As String = String.Empty

        Randomize()

        For i = 1 To 4
            sChar = Chr(CType((Int((90 - 65 + 1) * Rnd()) + 65), Integer))
            sId &= sChar
        Next

        Return sID & Month(Now) & Day(Now) & Year(Now) & Hour(Now) & Minute(Now) & Second(Now)
    End Function
End Class
