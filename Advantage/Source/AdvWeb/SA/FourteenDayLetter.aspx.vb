Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports Fame.AdvantageV1.Common

Partial Class FourteenDayLetter
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    'letter place holders
    Dim Student, Address, CityStateZip, SSN, TransDate, FundSourceDescription As String
    Dim Amount As Decimal = 0
    '-----corporate placeholders
    Dim CorporateName, CorporateAddress, CorporateCityStateZip, CorporatePhone, CorporateFax As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        GenerateLetter()
    End Sub
    Private Sub GenerateLetter()
        Dim ds As New DataSet
        Dim dr As DataRow
        Dim RecCount As Integer = 0
        '        Dim MyTotal As String
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            ds = (New FourteenDayLetterFacade).GetLetterDS(Session("FourteenDayLetterTranID"))

            SetUpLetter()

            For Each dr In ds.Tables(0).Rows

                If RecCount <> 0 Then
                    BreakPage()
                End If

                Student = dr("firstname").ToString() & " " & dr("middlename").ToString() & " " & dr("lastname").ToString()
                Address = dr("Address1").ToString() & " " & dr("Address2").ToString()
                CityStateZip = dr("City").ToString() & ", " & dr("StateDescrip").ToString() & " " & dr("Zip").ToString()
                Amount = String.Format("{0:c}", (dr("transamount") * -1))
                TransDate = String.Format("{0:d}", dr("TransDate"))
                FundSourceDescription = dr("fundsourcedescrip").ToString()
                'SSN = dr("SSN").ToString()
                If dr("SSN").ToString.Length >= 1 Then
                    Dim temp As String = dr("SSN")
                    SSN = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                    'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                Else
                    SSN = ""
                End If
                RecCount += 1

                WriteLetter()

            Next

            EndLetter()

            ds.Dispose()
            ds = Nothing

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub GenerateLetter" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub GenerateLetter" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")

        End Try
    End Sub
    Private Sub SetUpLetter()
        GetCorporateInfo()
        Response.Write("<HTML><HEAD><title>14 Day Letter</title><STYLE>BR.page { page-break-after: always }</STYLE>")
        Response.Write("<link rel='stylesheet' type='text/css' href='../css/LetterForScreen.css' media='screen' />")
        Response.Write("<link rel='stylesheet' type='text/css' href='../css/LetterForPrinting.css' media='print' />")
        Response.Write("</HEAD><body class='letterbase'>")
    End Sub
    Private Sub WriteLetter()
        Response.Write("<table class='lettertable' width='670' ID='Table1' align='center'>")
        Response.Write("<tr><td valign='top' align='center' class='tdpadding'><table cellpadding='0' cellspacing='0' ID='Table2'>")
        Response.Write("<tr><td align='center' height='30' class='ArialTD'>" & CorporateName & "</td></tr><tr>")
        Response.Write("<td align='center' height='30' class='ArialTD'>" & CorporateAddress & "</td></tr><tr>")
        Response.Write("<td align='center' height='30' class='ArialTD'>" & CorporateCityStateZip & "</td></tr><tr><td align='center' height='30' class='ArialTD'>")
        Response.Write(CorporatePhone & CorporateFax & "</td></tr></table></td></tr><tr>")
        Response.Write("<td valign='top' class='tdpadding' style='BORDER-BOTTOM: 1px solid #000000'><table cellpadding='0' cellspacing='0' width='100%' ID='Table3'>")
        Response.Write("<tr height=30><td></td></tr><tr><td align='left' height=60 class='ArialTD'>" & TransDate & "</td></tr><tr>")
        Response.Write("<td align='left' height=30 class='ArialTD'>" & Student & "</td></tr><tr><td align='left' height=30 class='ArialTD'>")
        Response.Write(Address & "</td></tr><tr><td align='left' height=30 class='ArialTD'>" & CityStateZip)
        Response.Write("</td></tr><tr><td align='left' height=15 class='ArialTD'></td></tr></table></td></tr><tr>")
        Response.Write("<td valign='top' class='tdpadding' style='BORDER-BOTTOM: 1px solid #000000' align='left'>")
        Response.Write("<table cellpadding='0' cellspacing='0' width='100%' ID='Table4'><tr>")
        Response.Write("<td align='left' height=60 class='ArialTD'>Student Number # " & SSN & "</td></tr><tr><td align='left' height=60 class='ArialTD'>")
        Response.Write("A payment was credited to your student account for the following student loan disbursement.")
        Response.Write("</td></tr><tr><td align='left' height=60 class='ArialTD'>" & FundSourceDescription & " disbursed ")
        Response.Write("in the amount of &nbsp;$" & Amount & " on " & TransDate & ".</td></tr></table></td></tr><tr><td valign='top' class='tdpadding'>")
        Response.Write("<table cellpadding='0' cellspacing='0' width='100%' ID='Table5'><tr height=15><td></td>")
        Response.Write("</tr><tr><td align='left' class='ArialTD'>This receipt is to notify you that we have credited ")
        Response.Write("your account the above loan disbursement. The amount above is applied to tuition that ")
        Response.Write("you have incurred. If you want to cancel the above transaction and make the tuition ")
        Response.Write("payment with non-loan funds, you have 14 days to contact the school in writing to have ")
        Response.Write("the transaction reversed. In the future you will receive notice of your minimum monthly ")
        Response.Write("payment. At that time if you are facing a personal or financial hardship and are ")
        Response.Write("unable to make the scheduled payments there are deferments and forbearance. These are ways to ")
        Response.Write("postpone payments when borrowers are unemployed, still in school, or reduce monthly payments.")
        Response.Write("</td></tr></table></td></tr></table>")

    End Sub
    Private Sub GetCorporateInfo()
        Dim CorporateData As New CorporateInfo
        CorporateData = (New CorporateFacade).GetCorporateInfo
        With CorporateData
            CorporateName = .CorporateName
            CorporateAddress = .Address1 & " " & .Address2
            CorporateCityStateZip = .City & ", " & .State & " " & .Zip
            CorporatePhone = .Phone
            If .Fax.ToString <> "" Then
                CorporateFax = " / " & .Fax & " FAX"
            End If
        End With
    End Sub
    Private Sub WriteSpacers()
        Dim x As Integer
        For x = 1 To 6
            Response.Write("<br>")
        Next
    End Sub
    Private Sub EndLetter()
        Response.Write("</body></HTML>")
    End Sub

    Private Sub BreakPage()
        WriteSpacers()
        Response.Write("<div CLASS=page  style='break-after: page;'></div>")
    End Sub
End Class
