
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Collections

' ===============================================================================
'
' FAME AdvantageV1
'
' BankAccounts.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.

Partial Class ApplyPayments
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtRoutingNumber As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkIsDefaultBankAcct As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Panel4 As System.Web.UI.WebControls.Panel
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    'Protected WithEvents Header1 As Header
    'Protected WithEvents footer1 As Footer

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private amountToApply As Decimal = 0.0
    Private totalAmountToApply As Decimal = 0.0
    Private paymentId As String
    Private campusId As String
    Private pObj As New UserPagePermissionInfo
    Protected StudentId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim userId As String
        '   disable History button at all time
        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        'ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString

        Dim advantageUserState As New Advantage.Business.Objects.User()

        advantageUserState = AdvantageSession.UserState


        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        If Not IsPostBack Then
            'Bind Payments To Apply Datagrid
            BindPaymentsToApply()
        End If
    End Sub
    Private Sub BuildApplyPaymentGrid(ByVal lnkButton As LinkButton)
        amountToApply = Decimal.Parse(lnkButton.CommandName)
        totalAmountToApply = amountToApply
        paymentId = lnkButton.CommandArgument

        'Bind Apply Payments DataGrid
        BindApplyPaymentsDataGrid()
    End Sub
    Private Sub BindApplyPaymentsDataGrid()
        dgrdApplyPayment.DataSource = (New ApplyPaymentsFacade).GetAvailableCharges(paymentId)
        dgrdApplyPayment.DataBind()
    End Sub

    Protected Sub lbStudentName_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'BuildApplyPaymentGrid

        BuildApplyPaymentGrid(CType(sender, LinkButton))

        'set cssClass to selected LinkButton

        SetCssClassToAllLinkButtons(Me.Controls, "ItemStyle")
        'CType(sender, LinkButton).CssClass = "SelectedItemStyle"
        CType(sender, LinkButton).Font.Bold = True
        'strGUID = dgrdPaymentsToApply.DataKeys(e.Item.ItemIndex).ToString()
        'CommonWebUtilities.RestoreItemValues(dlstTerm, strGUID)

        'enable save button
        EnableSaveButton()

    End Sub
    Private Sub BindPaymentsToApply()
        dgrdPaymentsToApply.DataSource = (New ApplyPaymentsFacade).GetPaymentsToApply(campusId)
        dgrdPaymentsToApply.DataBind()
    End Sub
    Protected Sub dgrdApplyPayment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim lblPaymentId As Label = e.Item.FindControl("lblPaymentId")
                lblPaymentId.Text = paymentId
                Dim lblTotalAmountToApply As Label = e.Item.FindControl("lblTotalAmountToApply")
                lblTotalAmountToApply.Text = totalAmountToApply

                Dim txtbx As TextBox = e.Item.FindControl("txtAmountToApply")
                If amountToApply >= CType(e.Item.DataItem("Balance"), Decimal) Then
                    txtbx.Text = CType(e.Item.DataItem("Balance"), Decimal).ToString()
                    amountToApply -= e.Item.DataItem("Balance")
                Else
                    If amountToApply > 0 Then
                        txtbx.Text = amountToApply.ToString()
                        amountToApply = 0.0
                    Else
                        txtbx.Text = ""
                    End If
                End If
        End Select
    End Sub
    Private Function BuildAppliedPaymentsCollection() As ArrayList
        Dim appColl As New ArrayList

        'Loop thru the collection to retrieve the relevant fields
        For i As Integer = 0 To dgrdApplyPayment.Items.Count - 1
            Dim iitem As DataGridItem = dgrdApplyPayment.Items(i)

            'We only want the rows that have an amount applied
            Dim txtBoxAmountToApply As TextBox = iitem.FindControl("txtAmountToApply")

            'do not process empty rows
            If Len(txtBoxAmountToApply.Text.Trim) > 0 Then

                'do not process 0.00 amounts
                If Decimal.Parse(txtBoxAmountToApply.Text) > 0.0 Then
                    Dim applPmtInfo As New AppliedPaymentInfo

                    applPmtInfo.AppliedPaymentId = Guid.NewGuid.ToString
                    applPmtInfo.PaymentId = CType(iitem.FindControl("lblPaymentId"), Label).Text
                    applPmtInfo.ChargeId = CType(iitem.FindControl("lblTransactionId"), Label).Text
                    applPmtInfo.Amount = CType(iitem.FindControl("txtAmountToApply"), TextBox).Text
                    If Not Session("UserName") Is Nothing Then applPmtInfo.ModUser = Session("UserName") Else applPmtInfo.ModUser = "No Valid User"

                    'Add the item to the arraylist
                    appColl.Add(applPmtInfo)

                End If 'exlude records with zeroes
            End If
        Next

        Return appColl

    End Function
    Private Function ValidateUserEntries() As String
        Dim totalApplied As Decimal = 0.0
        Dim errMsg As String = ""
        Dim totalToBeApplied As Decimal

        'Loop thru the collection to retrieve the relevant fields
        For i As Integer = 0 To dgrdApplyPayment.Items.Count - 1

            'get
            Dim iitem As DataGridItem = dgrdApplyPayment.Items(i)
            Dim balance As Decimal = CType(iitem.FindControl("lblBalance"), Label).Text
            Dim transDescrip As String = CType(iitem.FindControl("lblTransaction"), Label).Text
            Dim transDate As String = CType(iitem.FindControl("lblDate"), Label).Text
            totalToBeApplied = Decimal.Parse(CType(iitem.FindControl("lblTotalAmountToApply"), Label).Text)

            'We only want the rows that have an amount applied
            Dim txtBoxAmountToApply As TextBox = iitem.FindControl("txtAmountToApply")

            'ignore empty rows
            If Len(txtBoxAmountToApply.Text.Trim) > 0 Then

                'amount to apply must be a valid decimal greater than zero
                If IsValidDecimalGreaterThanZero(txtBoxAmountToApply.Text) Then
                    'Build up the total amount applied
                    Dim amtApplied As Decimal = Decimal.Parse(txtBoxAmountToApply.Text)
                    'Check if the amount being applied is greater than the balance of the transaction
                    If amtApplied > balance Then
                        errMsg += "The amount applied of " + amtApplied.ToString() + " to " + transDescrip + " dated " + transDate + vbCr
                        errMsg += "is greater than the balance of " + balance.ToString + vbCr + vbCr
                    Else
                        totalApplied += amtApplied
                    End If
                Else
                    errMsg += "The applied amount: " + txtBoxAmountToApply.Text + " to " + transDescrip + " dated " + transDate + vbCr
                    errMsg += "is invalid." + vbCr + vbCr
                End If
            End If

        Next

        If totalApplied > totalToBeApplied Then
            errMsg += "The total amount applied cannot be greater than the " + vbCr
            errMsg += "amount of the payment being posted" + vbCr + vbCr
        End If

        Return errMsg

    End Function
    Private Function IsValidDecimalGreaterThanZero(ByVal str As String) As Boolean
        Try
            Dim dec As Decimal = Decimal.Parse(str)
            If dec > 0.0 Then Return True Else Return False
        Catch
            Return False
        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        '   validate user entries, display proper error message
        Dim result As String = ValidateUserEntries()
        If Len(result) > 0 Then
            DisplayErrorMessage(result)
            Exit Sub
        Else
            '   update AppliedPayments 
            result = (New ApplyPaymentsFacade).SaveNewAppliedPayments(BuildAppliedPaymentsCollection())
            If Len(result) > 0 Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub
            Else
                'Bind Payments To Apply Datagrid
                BindPaymentsToApply()

                'clear apply payments datagrid
                dgrdApplyPayment.DataSource = Nothing
                dgrdApplyPayment.DataBind()

                'disable save button
                btnSave.Enabled = False
                dgrdPaymentsToApply.SelectedIndex = -1
            End If
        End If

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub
    Private Sub EnableSaveButton()
        'Enable the Save button if the user has permission
        If pObj.HasFull Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button
        controlsToIgnore.Add(btnSave)

        'Add javascript code to warn the user about non saved changes
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Private Sub SetCssClassToAllLinkButtons(ByVal controls As ControlCollection, ByVal cssClass As String)
        For Each ctrl As Control In controls
            'check if the control is a container (repeater, datalist or datagrid)
            If ctrl.GetType().Name = "LinkButton" Then
                'CType(ctrl, LinkButton).CssClass = cssClass
                CType(ctrl, LinkButton).Font.Bold = False
            End If

            'iterate over other controls
            If ctrl.HasControls Then
                SetCssClassToAllLinkButtons(ctrl.Controls, cssClass)
            End If
        Next
    End Sub

    Protected Sub dgrdPaymentsToApply_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles dgrdPaymentsToApply.PageIndexChanging
        BindPaymentsToApply()
        dgrdPaymentsToApply.PageIndex = e.NewPageIndex
        dgrdPaymentsToApply.DataBind()
        dgrdPaymentsToApply.SelectedIndex = -1
        dgrdApplyPayment.DataSource = Nothing
        dgrdApplyPayment.DataBind()
        btnSave.Enabled = False
    End Sub

    Protected Sub dgrdPaymentsToApply_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles dgrdPaymentsToApply.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
        '    e.Row.Attributes("onmouseout") = "this.style.textDecoration='none';"
        '    ' Set the last parameter to True 
        '    ' to register for event validation. 
        '    e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(dgrdPaymentsToApply, "Select$" + e.Row.DataItemIndex, True)
        'End If
    End Sub

    Protected Sub dgrdPaymentsToApply_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dgrdPaymentsToApply.SelectedIndexChanged
        'Dim str As String = String.Empty
        paymentId = dgrdPaymentsToApply.SelectedDataKey.Value.ToString

        amountToApply = Decimal.Parse(CType(dgrdPaymentsToApply.SelectedRow.FindControl("hdnUnAppliedAmount"), HiddenField).Value)
        totalAmountToApply = amountToApply

        'BuildApplyPaymentGrid(CType(sender, Button))
        BindApplyPaymentsDataGrid()
        EnableSaveButton()
    End Sub
End Class