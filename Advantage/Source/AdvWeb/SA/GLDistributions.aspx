﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="GLDistributions.aspx.vb" Inherits="GLDistributions" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"> </script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table id="Table2" class="Table100">
                <tr>
                    <td class="listframetop">Select GL Distributions
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleft">
                            <table class="Table100">
                                <tr>
                                    <td width="30%" style="padding: 3px;">
                                        <asp:Label ID="lblCampGrpId" runat="server" CssClass="label">Campus Groups</asp:Label>
                                    </td>
                                    <td width="70%" style="padding: 3px;">
                                        <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%" style="padding: 3px;">
                                        <asp:Label ID="lblTransDateFrom" runat="server" CssClass="label">From Date</asp:Label>
                                    </td>
                                    <td width="70%" style="padding: 3px;">

                                        <telerik:RadDatePicker ID="txtTransDateFrom" runat="server" MinDate="1/1/1900" Width="200px">
                                        </telerik:RadDatePicker>

                                        <asp:CompareValidator ID="TransFromDateCompareValidator" runat="server" Operator="GreaterThanEqual" ValueToCompare="2001-01-01"
                                            Type="Date" ControlToValidate="txtTransDateFrom" Display="None" ErrorMessage="Invalid Transaction From Date"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%" style="padding: 3px;">
                                        <asp:Label ID="lblTransDateTo" runat="server" CssClass="label">To Date</asp:Label>
                                    </td>
                                    <td width="70%" style="padding: 3px;">


                                        <telerik:RadDatePicker ID="txtTransDateTo" runat="server" Width="200px">
                                        </telerik:RadDatePicker>

                                        <asp:CompareValidator ID="TransDateToCompareValidator" runat="server" Operator="GreaterThanEqual" Type="Date"
                                            ControlToValidate="txtTransDateTo" Display="None" ErrorMessage="Invalid Transaction From Date" ControlToCompare="txtTransDateFrom"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="padding: 3px;">
                                        <asp:CheckBox ID="chkSummary" Text="Summary Report" runat="server" CssClass="checkbox" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                                <tr>
                                    <td nowrap align="right" width="100%" style="padding-right: 30px;" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" Text="Build List"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table class="Table100">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->
                            <table class="contenttable" style="width: 100%">
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnExportToExcell" runat="server" Width="115px"
                                            Text="Export To Excel"></asp:Button>
                                    </td>
                                </tr>
                                <tr height="5px">
                                    <td>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdGLDistributions" runat="server" Width="100%" AutoGenerateColumns="False"
                                            BorderColor="#E0E0E0" BorderWidth="1px" ShowFooter="True">
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="CreateDate" HeaderText="From" Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="EndDate" HeaderText="To" Visible="False" DataFormatString="{0:d}">
                                                    <ItemStyle HorizontalAlign="Justify"></ItemStyle>
                                                </asp:BoundColumn>

                                                <asp:BoundColumn DataField="CreateDate" SortExpression="CreateDate" ReadOnly="True" HeaderText="Date"
                                                    DataFormatString="{0:d}">
                                                    <ItemStyle HorizontalAlign="Justify"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Transaction" SortExpression="Transaction" HeaderText="Transaction"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="GLAccount" SortExpression="TransAmount" ReadOnly="True"
                                                    HeaderText="GL Account">
                                                    <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="TransactionDescrip" HeaderText="Transaction Code"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Debit">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDebits" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Debit")%>'
                                                            CssClass="label"></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblTotalDebits" runat="server" CssClass="labelbold">Total Debit</asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Credit">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCredits" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Credit") %>'
                                                            CssClass="label"></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblTotalCredits" runat="server" CssClass="labelbold">Total Credit</asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>

                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>


                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

