<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master"
    Inherits="FAFSARenewalLetterSelector" CodeFile="FAFSARenewalLetterSelector.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>FAFSA Renewal Letter</title>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">
            <table class="SAtable" id="Table5" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <!-- begin rightcolumn -->
                    <td class="DetailsFrameTop">
                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Enabled="False" CssClass="save" Text="Save">
                                    </asp:Button><asp:Button ID="btnNew" runat="server" Enabled="False" CssClass="new"
                                        Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server"
                                            Enabled="False" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                    </td>
                </tr>
                <tr>
                    <td class="detailsframe">
                        <!-- begin table content-->
                        <div class="scrollsingleframe">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                 <tr>
                                    <td> &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 45%; text-align: right">
                                        <asp:Label ID="lblStartDate" runat="server" CssClass="label">Enter start date<font runat="server" color="red">*</font> &nbsp; &nbsp; &nbsp;</asp:Label>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 55%; text-align: left">
                                        <%-- <asp:TextBox ID="txtStartDate" runat="server" CssClass="DateTextBox" BackColor="#ffff99"></asp:TextBox>
                                        <img height="1" src="../images/1x1.gif" width="10">
                                        <a id="CalButton" onclick="javascript:OpenCalendar('Form1', 'txtStartDate', false, 2001)"
                                            runat="server">
                                            <img id="Img2" alt="Graphic Calendar" src="../UserControls/Calendar/PopUpCalendar.gif"
                                                align="absMiddle" border="0" runat="server"></a>--%>
                                        <telerik:RadDatePicker ID="txtStartDate" MinDate="1/1/1945" runat="server">
                                        </telerik:RadDatePicker>
                                        <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="Start Date is required"
                                            ControlToValidate="txtStartDate" Display="None" runat="server">Start Date is required</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvStartDate" ErrorMessage="Start Date is invalid" ControlToValidate="txtStartDate"
                                            Display="None" runat="server" Type="Date" Operator="DataTypeCheck"></asp:CompareValidator>
                                    </td>
                                </tr>                               
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 45%; text-align: right">
                                        <asp:Label ID="Label2" runat="server" CssClass="label">Enter cut-off date<font runat="server" color="red">*</font> &nbsp; &nbsp; &nbsp;</asp:Label>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 55%; text-align: left">
                                        <%-- <asp:TextBox ID="txtCutOffDate" runat="server" CssClass="DateTextBox" BackColor="#ffff99"></asp:TextBox><img
                                            height="1" src="../images/1x1.gif" width="10">
                                        <a id="A2" onclick="javascript:OpenCalendar('Form1', 'txtCutOffDate', false, 2001)"
                                            runat="server">
                                            <img id="Img1" alt="Graphic Calendar" src="../UserControls/Calendar/PopUpCalendar.gif"
                                                align="absMiddle" border="0" runat="server"></a>--%>
                                        <telerik:RadDatePicker ID="txtEndDate" MinDate="1/1/1945" runat="server">
                                        </telerik:RadDatePicker>
                                        <asp:RequiredFieldValidator ID="rfvEndDate" ErrorMessage="Cut Off Date is required" ControlToValidate="txtEndDate"
                                            Display="None" runat="server">Cut Off Date is required</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvEndDate" ErrorMessage="Cut Off Date is invalid" ControlToValidate="txtEndDate"
                                                Display="None" runat="server" Type="Date" Operator="DataTypeCheck"></asp:CompareValidator>
                                        <asp:CompareValidator ID="cvEndDate1" ErrorMessage="Cut Off Date must be greater than or equal to Start Date"
                                                ControlToValidate="txtEndDate" Display="None" runat="server" Type="Date" Operator="GreaterThanEqual"
                                                ControlToCompare="txtStartDate"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td> &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 45%; text-align: right"> &nbsp;
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 55%; text-align: left">
                                        <asp:Button ID="btnGetList" runat="server" Text="Get List" Width="160px"></asp:Button>
                                    </td>                                    
                                    <td> &nbsp;</td>                               
                                </tr>
                            </table>
                            <br />
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 45%; text-align: right"> &nbsp;
                                    </td>
                                    <td style="width: 55%; text-align: left">
                                        <asp:Button ID="btnGenerateLetter" runat="server" Text="Generate Letter" Width="160px"
                                            Visible="False"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td> &nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:DataGrid ID="dgrdStudentDetails" runat="server" Width="100%" HorizontalAlign="Center"
                                            BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0">
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center"></HeaderStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Left" HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" CssClass="label">
															<%# Container.DataItem("firstname") + " " + Container.DataItem("middlename") + " " + Container.DataItem("lastname") %>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="SSN" HeaderText="SSN"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="AwardEndDate" HeaderText="Award End Date" DataFormatString="{0:d}">
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Left" HeaderText="Default Address">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkDefaultAddress" runat="server" Text='' Enabled="False" Checked='<%# Container.DataItem("DefaultAddress") %>' />
                                                        <asp:TextBox ID="Textbox1" runat="server" CssClass="ardatalistcontent" Visible="False"
                                                            Text='<%# Container.DataItem("studentId") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderTemplate>
                                                        <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkCopy',
															document.forms[0].chkAllItems.checked)" />Check All
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkCopy" runat="server" Text='' />
                                                        <asp:TextBox ID="txtStudentID" runat="server" CssClass="ardatalistcontent" Visible="False"
                                                            Text='<%# Container.DataItem("studentId") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <!--end table content-->
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <!-- end footer -->
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
</asp:Content>
