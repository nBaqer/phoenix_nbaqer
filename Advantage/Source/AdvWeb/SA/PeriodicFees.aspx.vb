Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class PeriodicFees
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblFeeType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFeeTypeId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCourseId As System.Web.UI.WebControls.Label

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected TermId As String
    Protected Term As String
    Protected campusId As String
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        'Dim campusId As String
        Dim userId As String
        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Not Request.Item("TermId") Is Nothing Then
            TermId = CType(Request.Item("TermId"), String)
            Term = CType(Request.Item("Term"), String)
            lblTerm.Text = Term
        Else
            '   For testing purposes only
            TermId = "662335B1-987B-4C68-AAE4-37AFD0AA4B68"
            Term = "Computer Science - 2001"
            lblTerm.Text = Term
        End If

        If Not IsPostBack Then
            objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList()

            '   bind an empty fee
            BindNewFeeData()

            '   initialize buttons
            InitButtonsForLoad()

        Else
            objCommon.PageSetup(Form1, "EDIT")
            InitButtonsForEdit()
        End If

    End Sub
    Private Sub BindDataList()

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind PeriodicFees datalist
        dlstPeriodicFees.DataSource = New DataView((New StudentsAccountsFacade).GetTransCodesByTerm(TermId).Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstPeriodicFees.DataBind()

    End Sub
    'Private Sub BuildDropDownLists()
    '    BuildStatusDDL()
    '    BuildTransCodesDDL()
    '    BuildTuitionCategoriesDDL()
    '    BuildRateSchedulesDDL()
    '    BuildProgramVersionsDDL()
    '    BuildProgramTypesDDL()
    'End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = (New StatusesFacade).GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildTransCodesDDL()
        '   bind the TransCodes DDL
        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeId"
            .DataSource = (New StudentsAccountsFacade).GetAllTransCodes(False)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildTuitionCategoriesDDL()
        '   bind the TuitionCategories DDL
        With ddlTuitionCategoryId
            .DataTextField = "TuitionCategoryDescrip"
            .DataValueField = "TuitionCategoryId"
            .DataSource = (New StudentsAccountsFacade).GetAllTuitionCategories("All")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildRateSchedulesDDL()
        '   bind the RateSchedules DDL
        With ddlRateScheduleId
            .DataTextField = "RateScheduleDescrip"
            .DataValueField = "RateScheduleId"
            .DataSource = (New StudentsAccountsFacade).GetAllRateSchedules(False)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramVersionsDDL()
        Dim dt As New DataTable
        dt = (New StuEnrollFacade).GetAllPrgVersionsForCampus(campusId).Tables(0)
        Cache.Insert("myProgVersions", dt)
        With ddlPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramTypesDDL()
        With ddlProgTypeId
            .DataTextField = "Description"
            .DataValueField = "ProgTypId"
            .DataSource = (New StuEnrollFacade).GetAllProgramTypes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub dlstPeriodicFees_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstPeriodicFees.ItemCommand
        '   get the PeriodicFeeId from the backend and display it
        GetPeriodicFeeId(e.CommandArgument)

        'bind futureterms control
        If rblApplyTo.SelectedValue = "2" Then
            BindFutureTermsControlForPrgVersion()
        Else
            BindFutureTermsControl()
        End If

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstPeriodicFees, e.CommandArgument, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstPeriodicFees, e.CommandArgument)

        '   initialize buttons
        InitButtonsForEdit()

    End Sub
    Private Sub BindPeriodicFeeData(ByVal PeriodicFee As PeriodicFeeInfo)
        With PeriodicFee
            chkIsInDB.Checked = .IsInDB
            txtPeriodicFeeId.Text = .PeriodicFeeId
            txtTermId.Text = .TermId
            'If Not (PeriodicFee.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = PeriodicFee.StatusId
            'If Not (PeriodicFee.TransCodeId = Guid.Empty.ToString) Then ddlTransCodeId.SelectedValue = PeriodicFee.TransCodeId Else ddlTransCodeId.SelectedIndex = 0
            'If Not (PeriodicFee.TuitionCategoryId = Guid.Empty.ToString) Then ddlTuitionCategoryId.SelectedValue = PeriodicFee.TuitionCategoryId Else ddlTuitionCategoryId.SelectedIndex = 0

            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .StatusId, .Status)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTransCodeId, .TransCodeId, .TransCode)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTuitionCategoryId, .TuitionCategoryId, .TuitionCategory)

            If .Amount > 0.0 Then
                txtAmount.Text = .Amount.ToString("###,###.00")
                rbtFlatAmount.Checked = True
                ddlUnitId.SelectedValue = .UnitId
            Else
                txtAmount.Text = Decimal.Parse(0.0).ToString("###,###.00")
                rbtFlatAmount.Checked = False
                ddlUnitId.SelectedIndex = 0
            End If

            If Not (PeriodicFee.RateScheduleId = Guid.Empty.ToString) Then
                'ddlRateScheduleId.SelectedValue = PeriodicFee.RateScheduleId
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRateScheduleId, .RateScheduleId, .rateSchedule)
                rbtRateSchedule.Checked = True
            Else
                ddlRateScheduleId.SelectedIndex = 0
                rbtRateSchedule.Checked = False
            End If

            ddlUnitId.SelectedValue = .UnitId
            rblApplyTo.SelectedIndex = .ApplyTo

            If Not (.TermStartDate = Date.MinValue) Then
                tbTermStartDate.SelectedDate = .TermStartDate.ToShortDateString
            Else
                tbTermStartDate.Clear()
            End If

            If Not (PeriodicFee.ProgTypeId = Guid.Empty.ToString) Then
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlProgTypeId, .ProgTypeId, .ProgType)
            End If

            If Not (PeriodicFee.PrgVerId = Guid.Empty.ToString) Then
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrgVerId, .PrgVerId, .PrgVerDescrip)
            End If

            'ddlProgTypeId.SelectedValue = .ProgTypeId
            'ddlPrgVerId.SelectedValue = .PrgVerId
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   Check that user input is valid
        If Not IsUserInputDataValid() Then
            '   Display Error Message
            DisplayErrorMessage("Data entered is invalid." + vbCrLf + "You must select a Flat Amount or a Rate Schedule." + vbCrLf + "If you select a Flat Amount you must also enter an amount." + vbCrLf + "If you select the Rate Schedule you have to " + vbCrLf + "select also a Rate Schedule from the dropdownlist.")
            Exit Sub
        End If

        '   update PeriodicFee Info 
        Dim result As String

        result = (New StudentsAccountsFacade).UpdatePeriodicFeeInfo(BuildPeriodicFeeInfo(txtPeriodicFeeId.Text, TermId), Session("UserName"), GetTermsFromControl(cblFutureTerms.Items))
        If result <> "" Then
            '   Display Error Message
            DisplayErrorMessage(result)

        Else
            '   get the PeriodicFeeId from the backend and display it
            GetPeriodicFeeId(txtPeriodicFeeId.Text)
            BindFutureTermsControl()
            'bind futureterms control
            If rblApplyTo.SelectedValue = "2" Then
                BindFutureTermsControlForPrgVersion()
            End If

        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then
            '   bind the datalist
            BindDataList()

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPeriodicFees, txtPeriodicFeeId.Text, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstPeriodicFees, txtPeriodicFeeId.Text)
            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new PeriodicFeeInfo
            'BindPeriodicFeeData(New PeriodicFeeInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If

    End Sub
    Private Function GetTermsFromControl(ByVal listItemCollection As ListItemCollection) As String()
        Dim list As New List(Of String)
        For Each item As ListItem In listItemCollection
            If item.Selected Then list.Add(item.Value)
        Next
        Return list.ToArray()
    End Function
    Private Function BuildPeriodicFeeInfo(ByVal periodicFeeId As String, ByVal termId As String) As PeriodicFeeInfo

        '   instantiate class
        Dim PeriodicFeeInfo As New PeriodicFeeInfo

        With PeriodicFeeInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get PeriodicFeeId
            .PeriodicFeeId = periodicFeeId

            '   get StatusId
            .StatusId = ddlStatusId.SelectedValue

            '   TermId
            .TermId = termId

            '   get Trans. Code
            .TransCodeId = ddlTransCodeId.SelectedValue

            '   get TuitionCategoryId
            .TuitionCategoryId = ddlTuitionCategoryId.SelectedValue

            '   get Amount
            .Amount = Decimal.Parse(txtAmount.Text)

            '   get RateScheduleId
            .RateScheduleId = ddlRateScheduleId.SelectedValue

            '   get Unit
            .UnitId = ddlUnitId.SelectedValue

            '   get ApplyTo
            .ApplyTo = rblApplyTo.SelectedIndex

            Select Case rblApplyTo.SelectedIndex
                Case 0
                    .ProgTypeId = Guid.Empty.ToString
                    .PrgVerId = Guid.Empty.ToString
                Case 1
                    .ProgTypeId = ddlProgTypeId.SelectedValue
                    .PrgVerId = Guid.Empty.ToString
                Case 2
                    .ProgTypeId = Guid.Empty.ToString
                    .PrgVerId = ddlPrgVerId.SelectedValue
            End Select

            ' TermStartDate must be blank or a valid date
            If Not tbTermStartDate.SelectedDate Is Nothing Then
                .TermStartDate = Date.Parse(tbTermStartDate.SelectedDate)
            Else
                .TermStartDate = Date.MinValue
            End If

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With

        '   return data
        Return PeriodicFeeInfo

    End Function
    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '   bind the datalist
        BindDataList()

    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new PeriodicFeeInfo
        BindPeriodicFeeData(New PeriodicFeeInfo)

        pFutureTerms.Visible = False
        lApplyToTheseFutureTerms.Visible = False

        '   Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstPeriodicFees, Guid.Empty.ToString, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstPeriodicFees, Guid.Empty.ToString)
        '   initialize buttons
        InitButtonsForLoad()

    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        If Not (txtPeriodicFeeId.Text = Guid.Empty.ToString) Then

            Dim result As String
            '   update PeriodicFee Info 
            result = (New StudentsAccountsFacade).DeletePeriodicFeeInfo(txtPeriodicFeeId.Text, Date.Parse(txtModDate.Text))
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPeriodicFees, Guid.Empty.ToString, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstPeriodicFees, Guid.Empty.ToString)
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else
                '   bind the datalist
                BindDataList()

                '   bind an empty new FeeInfo
                BindNewFeeData()

                '   initialize buttons
                InitButtonsForLoad()
                BindFutureTermsControl()
            End If

        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    End Sub
    Private Sub GetPeriodicFeeId(ByVal PeriodicFeeId As String)

        '   bind PeriodicFee properties
        BindPeriodicFeeData((New StudentsAccountsFacade).GetPeriodicFeeInfo(PeriodicFeeId))

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Sub BindNewFeeData()

        '   bind an empty new PeriodicFeeInfo
        BindPeriodicFeeData(New PeriodicFeeInfo)

    End Sub

    Private Sub ddlFeeTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlFeeTypeId.SelectedIndexChanged
        '   bind datalist
        BindDataList()

        '   bind an empty fee
        BindNewFeeData()

        '   initialize buttons
        InitButtonsForLoad()
    End Sub
    Private Function IsUserInputDataValid() As Boolean
        If rbtFlatAmount.Checked And Not rbtRateSchedule.Checked Then
            If Not IsTextBoxValidDecimal(txtAmount) Then Return False
            If IsFlatAmountZero() Then Return False
            'If Not ddlRateScheduleId.SelectedIndex = 0 Then Return False
            ddlRateScheduleId.SelectedIndex = 0
        End If
        If rbtRateSchedule.Checked And Not rbtFlatAmount.Checked Then
            If ddlRateScheduleId.SelectedIndex = 0 Then Return False
            'If IsTextBoxValidDecimal(txtAmount) Then Return False
            'If Not IsFlatAmountZero() Then Return False
            txtAmount.Text = "0.00"
        End If
        If (rbtRateSchedule.Checked And rbtFlatAmount.Checked) Or (Not rbtRateSchedule.Checked And Not rbtFlatAmount.Checked) Then Return False
        If rblApplyTo.SelectedIndex = 1 And ddlProgTypeId.SelectedIndex = 0 Then Return False
        If rblApplyTo.SelectedIndex = 2 And ddlPrgVerId.SelectedIndex = 0 Then Return False

        If Not tbTermStartDate.SelectedDate Is Nothing And Not IsDate(tbTermStartDate.SelectedDate) Then Return False
        Return True
    End Function
    Private Function IsTextBoxValidDecimal(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        Try
            Dim d As Decimal = Decimal.Parse(textbox.Text)
            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function IsFlatAmountZero() As Boolean
        If Decimal.Parse(txtAmount.Text) = 0.0 Then Return True Else Return False
    End Function
    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList()

        '   bind an empty new PeriodicFeeInfo
        BindPeriodicFeeData(New PeriodicFeeInfo)

        pFutureTerms.Visible = False
        lApplyToTheseFutureTerms.Visible = False

        '   Reset Style in the Datalist
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstPeriodicFees, Guid.Empty.ToString, ViewState)

        '   initialize buttons
        InitButtonsForLoad()
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        '        Dim SchoolItem As String

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlTransCodeId, AdvantageDropDownListName.Trans_Codes, campusId, True, True, Guid.Empty.ToString))

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, False, False, String.Empty))

        ddlList.Add(New AdvantageDDLDefinition(ddlTuitionCategoryId, AdvantageDropDownListName.Tuition_Cats, campusId, True, True, Guid.Empty.ToString))

        ddlList.Add(New AdvantageDDLDefinition(ddlRateScheduleId, AdvantageDropDownListName.Rate_Schedules, campusId, True, True, Guid.Empty.ToString))

        ddlList.Add(New AdvantageDDLDefinition(ddlProgTypeId, AdvantageDropDownListName.Program_Types, campusId, True, True, Guid.Empty.ToString))

        'ddlList.Add(New AdvantageDDLDefinition(ddlPrgVerId, AdvantageDropDownListName.ProgramVersions, campusId, True, False, Guid.Empty.ToString))

        BuildProgramVersionsDDL()
        '    BuildProgramTypesDDL()
        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub

    Protected Sub ddlTransCodeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTransCodeId.SelectedIndexChanged
        'bind futureterms control
        BindFutureTermsControl()
    End Sub
    Private Sub BindFutureTermsControl()
        'populate Future Terms panel
        Dim dt As New DataTable
        dt = (New StudentsAccountsFacade).GetFutureTermsInWhichToApplyThisTransCodeId(TermId, _
                                                                    ddlTransCodeId.SelectedValue, _
                                                                    campusId, _
                                                                    rblApplyTo.SelectedValue, _
                                                                    ddlPrgVerId.SelectedValue).Tables(0)
        Cache.Insert("myTerm", dt)
        cblFutureTerms.DataSource = dt
        cblFutureTerms.DataValueField = "TermId"
        cblFutureTerms.DataTextField = "TermDescrip"
        cblFutureTerms.DataBind()

        ' Future Terms panel should be visible only if there are terms
        If cblFutureTerms.Items.Count > 0 Then
            pFutureTerms.Visible = True
            lApplyToTheseFutureTerms.Visible = True
        Else
            pFutureTerms.Visible = False
            lApplyToTheseFutureTerms.Visible = False
        End If
    End Sub
    Private Sub BindFutureTermsControlForPrgVersion()
        'populate Future Terms panel
        'Commenting this as the cache is not refreshing the data and 
        'is not showing up the future terms
        'commented by Balaji on 02/06/2009
        'Dim dt As DataTable = CType(Cache("myTerm"), DataTable)
        Dim dt As New DataTable 

        Dim progId As String
        'Dim dt1 As New DataTable("GetFutureTermsByProgramVersion")
        'dt1.Columns.Add(New DataColumn("TermId", System.Type.GetType("System.String")))
        'dt1.Columns.Add(New DataColumn("TermDescrip", System.Type.GetType("System.String")))
        If ddlPrgVerId.SelectedValue <> "" Then
            Dim drPrg() As DataRow = CType(Cache("myProgVersions"), DataTable).Select("PrgVerId= '" & ddlPrgVerId.SelectedValue & "'")
            If drPrg.Length > 0 Then
                progId = drPrg(0)("ProgId").ToString
                'dt = CType(Cache("myTerm"), DataTable).Clone()
                dt = (New StudentsAccountsFacade).GetFutureTermsInWhichToApplyThisTransCodeId(TermId, _
                                                                    ddlTransCodeId.SelectedValue, _
                                                                    campusId, _
                                                                    rblApplyTo.SelectedValue, _
                                                                    ddlPrgVerId.SelectedValue).Tables(0)
                'Dim drs() As DataRow = dt.Select("ProgId='" & progId & "'" & " OR ProgId is NULL")
                'For Each dr As DataRow In drs
                '    Dim newrow As DataRow
                '    newrow = dt1.NewRow
                '    newrow("TermId") = dr("TermId").ToString
                '    newrow("TermDescrip") = dr("TermDescrip").ToString
                '    dt1.Rows.Add(newrow)
                'Next
            End If
        End If

        'Cache.Insert("mydata", dt)
        cblFutureTerms.DataSource = dt
        cblFutureTerms.DataValueField = "TermId"
        cblFutureTerms.DataTextField = "TermDescrip"
        cblFutureTerms.DataBind()

        ' Future Terms panel should be visible only if there are terms
        If cblFutureTerms.Items.Count > 0 Then
            pFutureTerms.Visible = True
            lApplyToTheseFutureTerms.Visible = True
        Else
            pFutureTerms.Visible = False
            lApplyToTheseFutureTerms.Visible = False
        End If

    End Sub
    Protected Sub ddlPrgVerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrgVerId.SelectedIndexChanged
        If rblApplyTo.SelectedValue = "2" Then
            BindFutureTermsControlForPrgVersion()
        End If
    End Sub
    Protected Sub rblApplyTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblApplyTo.SelectedIndexChanged
        If rblApplyTo.SelectedValue = "0" Then
            ddlPrgVerId.SelectedIndex = 0
        End If
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub
End Class

