﻿'Imports Fame.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports FAME.Advantage.Common

Partial Class DeferredEarningsPosting
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblEnrollment As System.Web.UI.WebControls.Label
    Private accruedTotal, toAccrueTotal, totalTotal As Decimal
    Private campusid, userId As String
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'header1.EnableHistoryButton(False)
        campusid = Master.CurrentCampusId

        Dim resourceId As Integer

        'disable history button
        'Header1.EnableHistoryButton(False)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)

      If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusid.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        If Not IsPostBack Then
            ''Modified By Saraswathi lakshmanan on may 19 2009
            ''The deferred revenue Date populated on the ddl should be based on the ccampus 
            ''Deferred revenue date vary for each and every campus
            '   populate dropdownlist with posting dates
            BuildReportDateDDL(campusid)

            '   disable PostDeferredRevenues button
            btnPostDeferredRevenues.Enabled = False
        End If
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False

        'Set the Post Deferred Revenue Button so it prompts the user for confirmation when clicked
        btnPostDeferredRevenues.Attributes.Add("onclick", "if(confirm('Are you sure you want to Post Deferred Revenues? This action can not be undone.')){}else{return false}")

    End Sub
    Private Sub BuildReportDateDDL(ByVal Campusid As String)
        ''Campus ID Added by saraswathi on may 19 2009
        With ddlReportDate
            ddlReportDate.DataTextField = "EndOfPeriodDate"
            ddlReportDate.DataValueField = "EndOfPeriodDate"
            ddlReportDate.DataSource = (New StudentsAccountsFacade).GetEndOfMonthDatesForPostingDeferredRevenueCampus(Campusid)
            ddlReportDate.DataBind()
            ''Changing the selected index to 1 from 6
            ''Since only dates till the current date will be displayed.
            If ddlReportDate.Items.Count >= 1 Then
                ddlReportDate.SelectedIndex = 0
            End If

        End With

    End Sub
    Private Sub BindDatagrid(ByVal transCodeId As String)
        Dim drMode As DeferredRevenuePostingModesEnumerator.DeferredRevenuePostingMode
        Dim studentIdentifierType As String

        drMode = DeferredRevenuePostingModesEnumerator.DeferredRevenuePostingMode.BindTransCodeDetails
        studentIdentifierType = MyAdvAppSettings.AppSettings("StudentIdentifier")

        If Not txtTransDate.SelectedDate Is Nothing Then
            If IsDate(txtTransDate.SelectedDate) = False Then
                'DisplayErrorMessage("InCorrect datatype for transaction date")
                DisplayRADAlert(CallbackType.Postback, "IncorrectDatatype1", "InCorrect datatype for transaction date", "Incorrect Datatype")
                Exit Sub
            Else
                With New StudentsAccountsFacade
                    'dgrdStudentRevenueLedger.DataSource = .GetDeferredRevenueDetailsByTransCode(transCodeId, Date.Parse(ddlReportDate.SelectedValue))
                    'dgrdStudentRevenueLedger.DataSource = New DataView(.GetDeferredRevenueDetailsByTransCodeDBI(transCodeId, Date.Parse(ddlReportDate.SelectedValue), campusid, txtTransDate.SelectedDate), Nothing, "StudentName asc", DataViewRowState.CurrentRows)
                    dgrdStudentRevenueLedger.DataSource = New DataView(.GetDeferredRevenueDetailsForTransCode(transCodeId, Date.Parse(ddlReportDate.SelectedValue), campusid, studentIdentifierType, drMode, Session("UserName"), txtTransDate.SelectedDate), Nothing, "StudentName asc", DataViewRowState.CurrentRows)
                    dgrdStudentRevenueLedger.DataBind()
                End With
            End If
            ''19323: QA: Not entering a date and building the revenue list is giving an error page. 
        Else
            With New StudentsAccountsFacade
                'dgrdStudentRevenueLedger.DataSource = .GetDeferredRevenueDetailsByTransCode(transCodeId, Date.Parse(ddlReportDate.SelectedValue))
                dgrdStudentRevenueLedger.DataSource = New DataView(.GetDeferredRevenueDetailsForTransCode(transCodeId, Date.Parse(ddlReportDate.SelectedValue), campusid, studentIdentifierType, drMode, Session("UserName"), ""), Nothing, "StudentName asc", DataViewRowState.CurrentRows)
                dgrdStudentRevenueLedger.DataBind()
            End With
        End If

    End Sub

    Private Sub dgrdStudentRevenueLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdStudentRevenueLedger.ItemDataBound
        '   accept only items (no header or footer records)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            '   get value of Accrued column
            accruedTotal += CType(CType(e.Item.DataItem, DataRowView).Item("Accrued"), Decimal)
            Dim toAccrue As Decimal = CType(CType(e.Item.DataItem, DataRowView).Item("ToAccrue"), Decimal)
            toAccrueTotal += toAccrue
            totalTotal += CType(CType(e.Item.DataItem, DataRowView).Item("Total"), Decimal)

            'zero values should be displayed according to the checkbox
            If (toAccrue = 0 Or (toAccrue > 0 And toAccrue < 0.01) Or (toAccrue < 0 And toAccrue > -0.01)) And Not cbxShowZeroAmounts.Checked Then e.Item.Visible = False Else e.Item.Visible = True
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("lblFooterAccruedTotal"), Label).Text = accruedTotal.ToString("#,###,###.00")
            CType(e.Item.FindControl("lblFooterToAccrueTotal"), Label).Text = toAccrueTotal.ToString("#,###,###.00")
            CType(e.Item.FindControl("lblFooterTotalTotal"), Label).Text = totalTotal.ToString("#,###,###.00")
        End If
    End Sub
    Private Sub BindDataList(Optional ByVal ds As DataSet = Nothing)
        Dim userId As String
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim pobj As UserPagePermissionInfo
        Dim facCGF As New CampusGroupsFacade
        Dim studentIdentifierType As String

        studentIdentifierType = MyAdvAppSettings.AppSettings("StudentIdentifier")

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString

        pobj = fac.GetUserResourcePermissions(userId, resourceId, campusid)
        'We only want to execute this code if there are deferred revenue items posting for the campus selected
        If facCGF.CampusHasDeferredRevenuePostings(campusid) Then
            ''Modiifed by saraswathi on June 29 2009
            ''To catch the student exceptions and print the exception if exist and donot allow them to 
            Dim dsDefRev As New DataSet
            Dim stuAccFacade As New StudentsAccountsFacade
            ''Modified by Saraswathi Lakshmanan on May 18 2010
            ''18962: Deferred Revenue Posting issue:Negative Tuition Amounts 
            ''Proposed a new method to calculate the deferred revenue based on the remaimng amount and remaining Duration

            'Troy: 5/29/2012 If a DataSet is passed in then it should be used instead of going back to the backend.
            If Not IsNothing(ds) Then
                dsDefRev = ds
            Else
                ''Modified by Saraswathi lakshmanan on June 30 2010
                ''to run the deferred revenue starting from the given transdate
                Dim drMode As DeferredRevenuePostingModesEnumerator.DeferredRevenuePostingMode

                drMode = DeferredRevenuePostingModesEnumerator.DeferredRevenuePostingMode.BuildList

                If txtTransDate.SelectedDate <> txtTransDate.MinDate Then
                    dsDefRev = stuAccFacade.GetDeferredRevenueResults(Date.Parse(ddlReportDate.SelectedValue), campusid, studentIdentifierType, drMode, Session("UserName"), txtTransDate.SelectedDate)
                    ''19323: QA: Not entering a date and building the revenue list is giving an error page. 
                Else
                    dsDefRev = stuAccFacade.GetDeferredRevenueResults(Date.Parse(ddlReportDate.SelectedValue), campusid, studentIdentifierType, drMode, Session("UserName"))

                End If
            End If


            'The dataset returned by the sproc has one table called DeferredRevenueResults table. The structure and contents can be either for the exceptions or trans codes.
            'There is a field called TableName that can either be StudentExceptions or TransCodes. We therefore have to check the first row TableName to know which result set
            'was returned by the sproc.
            If dsDefRev.Tables("DeferredRevenueResultsTable").Rows(0)("TableName") = "StudentExceptions" Then
                btnPostDeferredRevenues.Enabled = False
                DisplayErrorMessage("The students listed in the grid have exceptions. Please correct the records to post deferred revenue for all the students.")
                DgExceptions.Visible = True
                DgExceptions.DataSource = dsDefRev.Tables("DeferredRevenueResultsTable")
                DgExceptions.DataBind()
                dgrdStudentRevenueLedger.Visible = False
                dlstDefRevenueByTransCode.Visible = False


            ElseIf dsDefRev.Tables("DeferredRevenueResultsTable").Rows(0)("TableName") = "TransCodes" Then
                dlstDefRevenueByTransCode.Visible = True
                dlstDefRevenueByTransCode.DataSource = dsDefRev.Tables("DeferredRevenueResultsTable")
                dlstDefRevenueByTransCode.DataBind()
                DgExceptions.Visible = False
                dgrdStudentRevenueLedger.Visible = True
                '   bind an empty datagrid
                BindDatagrid(Guid.Empty.ToString)

                '   enable PostDeferredRevenues button
                If pobj.HasFull Then
                    btnPostDeferredRevenues.Enabled = True
                End If
            End If
        Else
            'Display message to the user that there are no deferred revenue postings for the selected campus.
            Dim errMsg As String
            errMsg = "The deferred revenue posting could not be run for this campus " + vbCrLf + "because there are no deferred revenue transactions."
            DisplayErrorMessage(errMsg)

        End If


        'End With


    End Sub

    Private Sub dlstDefRevenueByTransCode_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstDefRevenueByTransCode.ItemCommand
        '   bind datagrid
        BindDatagrid(e.CommandArgument)

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstDefRevenueByTransCode, e.CommandArgument, ViewState, header1)
        CommonWebUtilities.RestoreItemValues(dlstDefRevenueByTransCode, e.CommandArgument)
    End Sub

    Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        '   bind datalist
        BindDataList()
        HandleMonthNotEnded()
    End Sub
    Protected Function ConvertBooleanToYesNo(ByVal booleanValue As Boolean) As String
        If booleanValue Then Return "Yes" Else Return "No"
    End Function
    Protected Function ConvertSourceToVerbose(ByVal source As Integer) As String
        Select Case source
            Case 0
                Return "Student Ledger"
            Case 1
                Return "Revenue Calc."
            Case 2
                Return "Adjustment"
        End Select
        Return String.Empty
    End Function

    Private Sub btnPostDeferredRevenues_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPostDeferredRevenues.Click
        Dim ds As DataSet
        Dim saf As New StudentsAccountsFacade
        Dim studentIdentifierType As String
        Dim drMode As DeferredRevenuePostingModesEnumerator.DeferredRevenuePostingMode

        drMode = DeferredRevenuePostingModesEnumerator.DeferredRevenuePostingMode.PostDeferredRevenue
        studentIdentifierType = CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String)


        Try
            If txtTransDate.SelectedDate <> txtTransDate.MinDate Then
                ds = saf.GetDeferredRevenueResults(Date.Parse(ddlReportDate.SelectedValue), campusid, studentIdentifierType, drMode, Session("UserName"), txtTransDate.SelectedDate)
            Else
                ds = saf.GetDeferredRevenueResults(Date.Parse(ddlReportDate.SelectedValue), campusid, studentIdentifierType, drMode, Session("UserName"))
            End If



            ' populate dropdownlist with posting dates
            ''Modified by saraswathi lakshmana on may 19 2009
            ''The deferred revenue date is fetched based on the campus Id
            BuildReportDateDDL(campusid)
            'Let user know that posting was successful
            DisplayMessage("Posting of Deferred Revenues for this date has been successful")
            'Bind DataList
            BindDataList(ds)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            If Not IsNothing(ex.InnerException) Then
                DisplayErrorMessage(ex.InnerException.Message.ToString)
            Else
                DisplayErrorMessage(ex.Message.ToString)
            End If
        End Try

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub DisplayMessage(ByVal message As String)

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display message box in the client
            CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, message)
        End If

    End Sub

    Private Sub HandleMonthNotEnded()
        Dim strMsg As String

        'To fix DE12009: 
        'If the selected month end date has not ended as yet
        '(1) Disable the Post Deferred Revenues button
        '(2)Let the user know that they won't be able to post 
        If Date.Now <= Date.Parse(ddlReportDate.SelectedValue) Then
            btnPostDeferredRevenues.Enabled = False
            strMsg = "You won't be able to run the process because the selected month has not yet ended." + vbCr + "You can still preview the calculated amounts."
            DisplayErrorMessage(strMsg)
        End If
    End Sub
End Class

