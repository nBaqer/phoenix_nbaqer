<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="BillingMethods.aspx.vb" Inherits="BillingMethods" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
        $(function () {
            $(':text').bind('keydown', function (e) {
                //on keydown for all textboxes
                if (e.target.className != "searchtextbox") {
                    //if (e.target.id == "ContentMain2_txtExcAbsencesPercent") {
                    if (e.keyCode == 13) { //if this is enter key
                        e.preventDefault();
                        return false;
                    } else
                        return true;
                    //} else
                    //    return true;
                } else
                    return true;
            });
        });
        function ClietCumulativeValidation(source, arguments) {
            arguments.IsValid = false;
        }
        function alertCallBackFn(arg) {

            radalert(arg, 350, 250, "Error Message");

        }

    </script>
    <style>
        .tooltipPaymentPeriod {
            background-color: #424242 !important;
            border: none !important;
        }
      .RadForm_Material.rfdTextarea textarea.riTextBox{
          border:none !important;
      }
    </style>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="radGridPPI">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radGridPPI" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="radGridAPV">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radGridAPV" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="75px" Width="75px" Transparency="25">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/loading3.gif" AlternateText="loading" />
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
    </telerik:RadWindowManager>
    <!-- do not use this type of comments -->

    <%--  
        <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstBillingMethods">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstBillingMethods" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstBillingMethods" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstBillingMethods" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSaveNextToP2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pagePP" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSaveP0NextP1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pageBM" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    --%>

    <div style="overflow: auto;" class="billingMethodsPage">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="300" Scrolling="Y">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap="nowrap" align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap="nowrap">
                                        <asp:RadioButtonList ID="radstatus" runat="server" CssClass="radiobutton"
                                            AutoPostBack="true" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstBillingMethods" runat="server" DataKeyField="BillingMethodId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("BillingMethodId")%>' Text='<%# Container.DataItem("BillingMethodDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <asp:Panel ID="pnlRHS" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save" Enabled="false"></asp:Button>
                                <asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" width="100%" style="padding-left: 10px; padding-top: 10px;">
                        <tr>
                            <td class="detailsframe">
                                <asp:TextBox ID="txtBillingMethodId" runat="server" Visible="False"></asp:TextBox>
                                <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                <asp:CheckBox ID="chkBMInUse" runat="server" Visible="False"></asp:CheckBox>
                                <asp:CheckBox ID="chkITInUse" runat="server" Visible="False"></asp:CheckBox>
                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                <asp:TextBox ID="txtIncrementId" runat="server" Visible="False"></asp:TextBox>
                                <asp:CheckBox ID="chkIIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                <asp:TextBox ID="txtIModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                <asp:TextBox ID="txtIModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                <!-- begin content table-->
                                <telerik:RadTabStrip ID="RadTabStrip" runat="server" MultiPageID="RadMultiPage" Width="690px"
                                    SelectedIndex="0"
                                    OnTabClick="RadTabStrip_TabClick">
                                    <Tabs>
                                        <telerik:RadTab PageViewID="pageBM" runat="server"
                                            Text=" 1. Add Charging Method " Value="1"
                                            Enable="True" Selected="True">
                                        </telerik:RadTab>
                                        <telerik:RadTab PageViewID="pagePP" runat="server"
                                            Text=" 2. Setup Payment Period Increments " Value="2"
                                            Enable="False">
                                        </telerik:RadTab>
                                        <telerik:RadTab PageViewID="pagePV" runat="server" Value="3"
                                            Text=" 3. Apply Charging Method to Program Versions "
                                            Enable="False">
                                        </telerik:RadTab>
                                    </Tabs>
                                </telerik:RadTabStrip>
                                <telerik:RadMultiPage ID="RadMultiPage" runat="server" SelectedIndex="0" CssClass="contentleadmastertable" Height="400px" Width="690px">
                                    <telerik:RadPageView ID="pageBM" runat="server"
                                        Enable="True">
                                        <div class="scrollright9">
                                            <table width="100%" align="center">
                                                <tr>
                                                    <td class="contentcell" colspan="2">
                                                        <telerik:RadTextBox ID="txtMessage" runat="server" Visible="false"
                                                            Width="100%" Rows="2" TextMode="MultiLine" Wrap="true"
                                                            CssClass="labelBoldBillingMethodsPage"
                                                            Font-Names="Arial" Font-Bold="True" ForeColor="#000066" Font-Size="12px"
                                                            ReadOnly="True"
                                                            ReadOnlyStyle-CssClass="labelBoldBillingMethodsPage"
                                                            ReadOnlyStyle-BorderStyle="None" />
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblBillingMethodCode" runat="server" CssClass="label">Code</asp:Label></td>
                                                    <td class="contentcell4">
                                                        <asp:TextBox ID="txtBillingMethodCode" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvBillingMethodCode1" runat="server" ErrorMessage="Code can not be blank" Display="None"
                                                            ControlToValidate="txtBillingMethodCode">Code can not be blank</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label>

                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblBillingMethodDescrip" CssClass="label" runat="server">Description</asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:TextBox ID="txtBillingMethodDescrip" CssClass="textbox" MaxLength="50" runat="server" Width="250px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvBillingMethodDescrip1" runat="server" ErrorMessage="Description can not be blank"
                                                            Display="None" ControlToValidate="txtBillingMethodDescrip">Description can not be blank</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        <asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ErrorMessage="Must Select a Campus Group"
                                                            Display="None" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000"
                                                            ControlToValidate="ddlCampGrpId">Must Select a Campus Group
                                                        </asp:CompareValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblBillingMethod" runat="server" CssClass="label">System Charge Code</asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlBillingMethod" runat="server" CssClass="dropdownlist" AutoPostBack="true" ValidateRequestMode="Enabled" CausesValidation="True">
                                                        </asp:DropDownList>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:CheckBox ID="chkAutoBill" runat="server" CssClass="label" Visible="True" TextAlign="left" Text="AutoBill "></asp:CheckBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblIncrementType" runat="server" CssClass="label" Visible="False">Increment Type</asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlIncrementType" runat="server" CssClass="dropdownlist" Visible="False" AutoPostBack="true" ValidateRequestMode="Enabled" CausesValidation="True">
                                                        </asp:DropDownList>
                                                        <asp:RangeValidator ID="rgvIncrementType" runat="server"
                                                            ControlToValidate="ddlIncrementType"
                                                            ErrorMessage="Select correct Increment Type" Display="None"
                                                            Type="Integer" MinimumValue="0" MaximumValue="3">
                                                        </asp:RangeValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="width: 90%; padding-right: 30px;" align="right">
                                                        <asp:Button ID="btnSaveP0NextToP1" runat="server" Text="Save"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pagePP" runat="server"
                                        Enable="False">
                                        <div class="scrollright9">
                                            <table width="100%" align="center">
                                                <tr>
                                                    <td class="contentcellheader">
                                                        <asp:Label ID="lblTitle01" runat="server" CssClass="labelBoldBillingMethodsPage"
                                                            Text="Define Payment Period Increments for this Charging Method"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblEfectiveDate" runat="server" CssClass="labelBillingMethodsPage"
                                                            Text="Payment Period Effective Date "></asp:Label>
                                                        <telerik:RadDatePicker ID="txtEffectiveDate" runat="server" MinDate="1/1/2001" Width="130"
                                                            ToolTip="This charge method will apply to all students enrolled on or after this date">
                                                        </telerik:RadDatePicker>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblExcAbsencesPercent" runat="server" CssClass="labelBillingMethodsPage"
                                                            Text="Percentage of Excused Absences to include in calculation"></asp:Label>
                                                        <telerik:RadToolTip ID="totExcAbsencesPercent" runat="server" class="tooltipPaymentPeriod"
                                                            TargetControlID="lblExcAbsencesPercent"
                                                            RenderInPageRoot="true" Animation="Slide"
                                                            ShowEvent="OnMouseOver" IgnoreAltAttribute="true" RelativeTo="Mouse" Position="BottomRight"
                                                            HideEvent="ManualClose" IsClientID="false">
                                                            <div class="tooltipPaymentPeriod">
                                                                <h4>Excused absences in Clock-hour programs</h4>
                                                                <p>
                                                                    To be counted for Federal Student Aid (FSA) purposes, 
                                                                    excused absences must be permitted in your school's 
                                                                    written policies. Under FSA regulations, <span style="font-weight: bold;">no more than 10%</span>
                                                                    of the clock-hours in a payment period may be consider 
                                                                    excused absences. If your school's accrediting agency or 
                                                                    the state agency that legally authorizes your school to 
                                                                    operate allows fewer hours to be counted as excused 
                                                                    absences, you must follow the stricter standard rather 
                                                                    than the FSA standard.(2013-2014 FSA Handbook)
                                                                </p>
                                                            </div>

                                                        </telerik:RadToolTip>
                                                        <asp:TextBox ID="txtExcAbsencesPercent" runat="server" CssClass="textbox" Width="60px"
                                                            Text="0.01">
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" align="center">
                                                <tr>
                                                    <td>
                                                        <br />
                                                        <telerik:RadGrid ID="radGridPPI" runat="server"
                                                            Visible="true"
                                                            GridLines="Both"
                                                            ShowStatusBar="True" PageSize="5"
                                                            AllowPaging="True"
                                                            AllowSorting="True"
                                                            AllowMultiRowEdit="false"
                                                            AutoGenerateColumns="True"
                                                            AllowAutomaticInserts="True"
                                                            AllowAutomaticUpdates="True"
                                                            AllowAutomaticDeletes="True">
                                                            <PagerStyle Mode="NumericPages" AlwaysVisible="False" />
                                                            <MasterTableView CommandItemDisplay="Top"
                                                                HorizontalAlign="NotSet"
                                                                EditMode="InPlace"
                                                                AutoGenerateColumns="false"
                                                                AllowAutomaticInserts="True"
                                                                DataKeyNames="PmtPeriodId"
                                                                InsertItemPageIndexAction="ShowItemOnLastPage"
                                                                CommandItemSettings-AddNewRecordText="Add New Period"
                                                                NoMasterRecordsText="No Increment Periods to display">
                                                                <CommandItemSettings
                                                                    AddNewRecordText="Add New Period"
                                                                    RefreshText="View All"
                                                                    ShowRefreshButton="false" />
                                                                <CommandItemTemplate>
                                                                    <div style="height: 20px;">
                                                                        <div style="float: left; margin-left:2px;margin-top:2px; vertical-align: top;">
                                                                            <asp:ImageButton ImageUrl="~/images/icon/icon_add.png" runat="server" ID="btn1" CommandName="InitInsert" CssClass="rgAdd" Text=" " />
                                                                            <asp:LinkButton runat="server" ID="linkbuttionInitInsert" CommandName="InitInsert"
                                                                                Text="Add New Period"></asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </CommandItemTemplate>
                                                                <Columns>
                                                                    <telerik:GridTemplateColumn UniqueName="PmtPeriodId"
                                                                        ReadOnly="true"
                                                                        Display="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPmtPeriodId" runat="server"
                                                                                Text='<%# Eval("PmtPeriodId")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Right" CssClass="contentCellPeriod" />
                                                                        <EditItemTemplate>
                                                                            <asp:Label ID="lblPmtPeriodId" runat="server"
                                                                                Text='<%# Eval("PmtPeriodId")%>'></asp:Label>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <%--<telerik:GridTemplateColumn UniqueName="PeriodNumber"
                                                                        ReadOnly="true"
                                                                        SortExpression="PeriodNumber"
                                                                        AllowFiltering="false"
                                                                        HeaderStyle-Width="25px"
                                                                        HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-Font-Names="Arial, sans-serif"
                                                                        ItemStyle-Font-Size="11px"
                                                                        ItemStyle-HorizontalAlign="Center"
                                                                        ItemStyle-CssClass="contentCellPeriod">
                                                                        <HeaderStyle Width="25px"
                                                                            Font-Bold="true" ForeColor="White" BackColor="#99CCFF" HorizontalAlign="Center" />
                                                                        <HeaderTemplate>
                                                                            <asp:Label ID="lblHeaderPeriod" runat="server" CssClass="labelboldmaintenance"
                                                                                Text='Period'></asp:Label>
                                                                        </HeaderTemplate>
                                                                        <ItemStyle CssClass="contentCellPeriod" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPeriod" runat="server" CssClass="contentCellPeriod"
                                                                                Text='<%# Eval("PeriodNumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:Label ID="lblPeriodNumber" runat="server" CssClass="contentCellPeriod"
                                                                                Text='<%# Eval("PeriodNumber")%>'></asp:Label>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>--%>
                                                                    <telerik:GridTemplateColumn UniqueName="IncrementValue"
                                                                        AllowFiltering="false"
                                                                        HeaderStyle-Width="110px"
                                                                        HeaderStyle-HorizontalAlign="Right"
                                                                        ItemStyle-Font-Names="Arial, sans-serif"
                                                                        ItemStyle-Font-Size="11px"
                                                                        ItemStyle-HorizontalAlign="Right"
                                                                        ItemStyle-CssClass="contentCellPeriodInfo">
                                                                        <HeaderStyle Width="110px"
                                                                            Font-Bold="true" ForeColor="White" BackColor="#99CCFF" HorizontalAlign="Right" />
                                                                        <HeaderTemplate>
                                                                            <asp:Label ID="lblHeaderIncrement" runat="server" CssClass="labelboldmaintenance"
                                                                                Text='<%# ddlIncrementType.SelectedItem.Text %>'></asp:Label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblIncrementValue" runat="server" CssClass="contentCellPeriodInfo"
                                                                                Text='<%# Eval("IncrementValue", "{0:N2}")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtIncrementValue" runat="server" CssClass="contentCellPeriodInfo"
                                                                                Text='<%# Eval("IncrementValue")%>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvIncrementValue" runat="server"
                                                                                ControlToValidate="txtIncrementValue" Display="None" SetFocusOnError="True"
                                                                                ErrorMessage="">
                                                                            </asp:RequiredFieldValidator>
                                                                            <asp:RangeValidator ID="ravIncrementValue" runat="server"
                                                                                ControlToValidate="txtIncrementValue" Display="None" SetFocusOnError="True"
                                                                                Type="Double" MinimumValue="0.01" MaximumValue="5000.00"
                                                                                ErrorMessage="">
                                                                                <asp:CustomValidator ID="cuvCumulativeValue" runat="server"
                                                                                    ControlToValidate="txtIncrementValue" Display="Static" SetFocusOnError="True"
                                                                                    ErrorMessage="Cumulative value cannot exceed 500 credits."
                                                                                    OnServerValidate="CumulativeValidation" Text="Cumulative value cannot exceed 500 credits">
                                                                                </asp:CustomValidator>
                                                                            </asp:RangeValidator>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="CumulativeValue"
                                                                        ReadOnly="true"
                                                                        AllowFiltering="false"
                                                                        HeaderStyle-Width="105px"
                                                                        HeaderStyle-HorizontalAlign="Right"
                                                                        ItemStyle-Font-Names="Arial, sans-serif"
                                                                        ItemStyle-Font-Size="11px"
                                                                        ItemStyle-HorizontalAlign="Right"
                                                                        ItemStyle-CssClass="contentCellPeriod">
                                                                        <HeaderStyle Width="105px"
                                                                            Font-Bold="true" ForeColor="White" BackColor="#99CCFF" />
                                                                        <HeaderTemplate>
                                                                            <asp:Label ID="lblHeaderCumulative" runat="server" HorizontalAlign="Right"
                                                                                Text='<%#"Cumulative " & ddlIncrementType.SelectedItem.Text%>'></asp:Label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCumulativeValue" runat="server" HorizontalAlign="Right" CssClass="contentCellPeriod"
                                                                                Text='<%# Eval("CumulativeValue", "{0:N2}")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:Label ID="lblCumulativeValue" runat="server" HorizontalAlign="Right" CssClass="contentCellPeriod"
                                                                                Text='<%# Eval("CumulativeValue")%>'></asp:Label>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="ChargeAmount"
                                                                        AllowFiltering="false"
                                                                        HeaderStyle-Width="100px"
                                                                        HeaderStyle-HorizontalAlign="Right"
                                                                        ItemStyle-Font-Names="Arial, sans-serif"
                                                                        ItemStyle-Font-Size="11px"
                                                                        ItemStyle-HorizontalAlign="Right"
                                                                        ItemStyle-CssClass="contentCellPeriodInfo">
                                                                        <HeaderStyle Width="100px"
                                                                            Font-Bold="true" ForeColor="White" BackColor="#99CCFF" HorizontalAlign="Right" />
                                                                        <HeaderTemplate>
                                                                            <asp:Label ID="lblHeaderChargeAmount" runat="server" CssClass="labelboldmaintenance"
                                                                                Text="Charge Amount"></asp:Label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblChargeAmount" runat="server" CssClass="contentCellPeriodInfo"
                                                                                Text='<%# Eval("ChargeAmount", "{0:C2}")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtChargeAmount" runat="server" CssClass="contentCellPeriodInfo"
                                                                                Text='<%# Eval("ChargeAmount")%>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvChargeAmount" runat="server"
                                                                                ControlToValidate="txtChargeAmount" Display="None" SetFocusOnError="True"
                                                                                ErrorMessage="Charge amount is required">
                                                                            </asp:RequiredFieldValidator>
                                                                            <asp:RangeValidator ID="ravChargeAmount" runat="server"
                                                                                ControlToValidate="txtChargeAmount" Display="None" SetFocusOnError="True"
                                                                                Type="Double" MinimumValue="0.01" MaximumValue="1000000.00"
                                                                                ErrorMessage="The charge amount should be greater than zero">
                                                                            </asp:RangeValidator>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="TransactionCode"
                                                                        AllowFiltering="false"
                                                                        HeaderStyle-Width="100px"
                                                                        HeaderStyle-HorizontalAlign="Right"
                                                                        ItemStyle-Font-Names="Arial, sans-serif"
                                                                        ItemStyle-Font-Size="11px"
                                                                        ItemStyle-HorizontalAlign="Right"
                                                                        ItemStyle-CssClass="contentCellPeriodInfo">
                                                                        <HeaderStyle Width="100px"
                                                                            Font-Bold="true" ForeColor="White" BackColor="#99CCFF" HorizontalAlign="Right" />
                                                                        <HeaderTemplate>
                                                                            <asp:Label ID="lblHeaderTranscationCode" runat="server" CssClass="labelboldmaintenance"
                                                                                Text="Transaction Code"></asp:Label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTransactionCode" runat="server" Text='<%# Eval("TransCodeDescrip")%>' CssClass="contentCellPeriodInfo"
                                                                                ></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:DropDownList ID="ddlTransactionCode" runat="server" Width="110px" CssClass="contentCellPeriodInfo">
                                                                            </asp:DropDownList>
                                                                            <asp:CompareValidator ID="cfvTransactionCode" runat="server" ControlToValidate="ddlTransactionCode"
                                                                                                  ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual"
                                                                                                  ErrorMessage="Transaction Code is Required" Display="None"></asp:CompareValidator>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="IsCharged"
                                                                        ReadOnly="true"
                                                                        Display="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblIsCharged" runat="server"
                                                                                Text='<%# Eval("IsCharged")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Right" CssClass="contentCellPeriod" />
                                                                        <EditItemTemplate>
                                                                            <asp:Label ID="lblIsCharged" runat="server"
                                                                                Text='<%# Eval("IsCharged")%>'></asp:Label>
                                                                        </EditItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" InsertImageUrl="../images/icon/icon_add.png" 
                                                                        CancelImageUrl="../images/icon/icon_cancel.png" EditImageUrl="../images/icon/icon_edit.png" UpdateImageUrl="../images/icon/icon_save.png" 
                                                                        HeaderStyle-Width="55px">
                                                                        <HeaderStyle Width="55px"
                                                                            Font-Bold="true" ForeColor="White" BackColor="#99CCFF" />
                                                                        <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" Width="60px" />
                                                                    </telerik:GridEditCommandColumn>
                                                                    <telerik:GridButtonColumn ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="../images/icon/icon_delete.png"
                                                                        CommandName="Delete"
                                                                        ConfirmDialogType="Classic"
                                                                        ConfirmText="Delete this item?"
                                                                        ConfirmTitle="Delete"
                                                                        Text="Delete">
                                                                        <HeaderStyle Width="25px"
                                                                            Font-Bold="true" ForeColor="White" BackColor="#99CCFF" />
                                                                        <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                                                                    </telerik:GridButtonColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>
                                                <tr>
                                                </tr>
                                                <tr>
                                                    <td style="width: 90%; padding-right: 30px;" align="right">
                                                        <br />
                                                        <asp:Button ID="btnBackToP0" runat="server" Text="Back"></asp:Button>
                                                        <asp:Button ID="btnSaveP1NextToP2" runat="server" Text="Save & Continue"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pagePV" runat="server"
                                        Enable="False">
                                        <div class="scrollright9">
                                            <table width="100%" align="center">
                                                <tr>
                                                    <td class="contentcellheader">
                                                        <asp:Label ID="Label6" runat="server" CssClass="labelBoldBillingMethodsPage"
                                                            Text="Apply charging method to the selected Program Version"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <telerik:RadTextBox ID="txtMessageApplyPrgVersion" runat="server"
                                                            Width="100%" Rows="2" TextMode="MultiLine" Wrap="true"
                                                            CssClass="labelBoldBillingMethodsPage"
                                                            Font-Names="Arial" Font-Size="12px"
                                                            ReadOnly="True"
                                                            ReadOnlyStyle-CssClass="labelBoldBillingMethodsPage"
                                                            ReadOnlyStyle-Border="None"
                                                            ReadOnlyStyle-BorderStyle="None" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 20px;" class="contentcell">
                                                        <telerik:RadAjaxPanel runat="server" EnableViewState="true" EnableEmbeddedScripts="true" EnableAJAX="false">
                                                            <telerik:RadGrid ID="radGridAPV" runat="server"
                                                                AutoGenerateColumns="False" Width="500px" Height="220px" GridLines="None"
                                                                AllowSorting="false"
                                                                AllowMultiRowSelection="True">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <AlternatingItemStyle HorizontalAlign="Left" />
                                                                <MasterTableView NoMasterRecordsText="No Program Version were found available for the current Sytem Charge Code">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn"
                                                                            HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderStyle Width="15%" />
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="headerChkbox" runat="server" Text=""
                                                                                    AutoPostBack="True" CssClass="checkbox" ForeColor="White"
                                                                                    OnCheckedChanged="ToggleSelectedState" />
                                                                            </HeaderTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="rowChkBox" runat="server"
                                                                                    AutoPostBack="True"
                                                                                    OnCheckedChanged="ToggleRowSelection" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn Visible="false" Display="false">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox runat="server" ID="txtPrgVerId" Text='<%# Bind("PrgVerId")%>'></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn
                                                                            HeaderStyle-HorizontalAlign="Left"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblPrgVerDescrip" runat="server" CssClass="labelboldmaintenance"
                                                                                    Text='Program Version'></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPrgVerDescrip" runat="server" Text='<%# Bind("PrgVerDescrip")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn UniqueName="IsBillingMethodCharged"
                                                                            ReadOnly="true"
                                                                            Display="false">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblIsBillingMethodCharged" runat="server" CssClass="labelboldmaintenance"
                                                                                    Text='IsBillingMethodCharged'></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemStyle CssClass="contentCellPeriod" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblIsBillingMethodCharged" runat="server" CssClass="contentCellPeriod"
                                                                                    Text='<%# Eval("IsBillingMethodCharged")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn UniqueName="IsUsed"
                                                                            ReadOnly="true"
                                                                            Display="false">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblIsUsed" runat="server" CssClass="labelboldmaintenance"
                                                                                    Text='IsUsed'></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemStyle CssClass="contentCellPeriod" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblIsUsed" runat="server" CssClass="contentCellPeriod"
                                                                                    Text='<%# Eval("IsUsed")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                                <ClientSettings EnablePostBackOnRowClick="True">
                                                                    <Scrolling AllowScroll="True" />
                                                                    <Selecting AllowRowSelect="True"></Selecting>
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                        </telerik:RadAjaxPanel>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 90%; padding-right: 30px;" align="right">
                                                        <br />
                                                        <asp:Button ID="btnBackToP1" runat="server" Text="Back"></asp:Button>
                                                        <asp:Button ID="btnSaveP2BackToP0" runat="server" Text="Save"></asp:Button>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        <!--end validation panel-->
    </div>

</asp:Content>

