﻿<%@ Page Title="Post Payments" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostPayments.aspx.vb" Inherits="PostPayments" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Post Payments</title>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function confirmCallBackFn(arg) {

            if (arg == true) {

                if ('<%=userWantsToPrintAReceipt.ToString().ToLower()%>' == 'true') {
                    var oButton = document.getElementById('<%=HdnSaveAndPrint.ClientID%>');
                    //alert('true')
                    oButton.click();
                }
                else {
                    var oButton = document.getElementById('<%=Hdn.ClientID%>');
                    //alert('false')
                    oButton.click();
                }

            }

        }

    </script>
    <style type="text/css">
        .borderRight {
            border-right: 1px solid #ebebeb;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" Orientation="HorizontalTop">
            <div style="overflow-x: auto; width: 100%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                    <tr>
                        <td class="detailsframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                                <tr>
                                    <td class="menuframe" align="right">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                            Enabled="False"></asp:Button>
                                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                            Enabled="False"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none">
                                <tr>
                                    <td class="detailsframe">
                                        <div>
                                            <asp:Panel runat="server" ID="MainPanel">
                                                <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                                <asp:CheckBox ID="chkIsReversal" runat="server" Visible="False"></asp:CheckBox>
                                                <asp:TextBox ID="txtPostPaymentId" runat="server" Visible="False" Wrap="False">txtPostPaymentId</asp:TextBox>
                                                <asp:TextBox ID="txtOriginalTransactionId" runat="server" Visible="False" Wrap="False">OriginalTransactionId</asp:TextBox>
                                                <asp:CheckBox ID="chkIsPosted" runat="server" Visible="False"></asp:CheckBox>
                                                <asp:TextBox ID="txtModUser1" runat="server" Visible="False">ModUser1</asp:TextBox>
                                                <asp:TextBox ID="txtModDate1" runat="server" Visible="False">ModDate1</asp:TextBox>
                                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                                <asp:LinkButton ID="lbtHasDisb" runat="server" Width="0px"></asp:LinkButton>
                                                <br />

                                                <FAME:StudentSearch ID="StudentSearch" runat="server" OnTransferToParent="TransferToParent" />
                                                <div class="boxContainer">
                                                    <h3>
                                                        <asp:Label ID="headerTitle" runat="server"></asp:Label></h3>
                                                    <table style="width: 100%; border: none; padding: 0px;">

                                                        <tr>
                                                            <td valign="top" style="width: 49%; padding-right: 1%;">
                                                                <asp:Panel runat="server" ID="pnlColumn1" CssClass="borderRight">
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblTransDescrip" runat="server" CssClass="label">Description<font color="red">*</font></asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <asp:TextBox ID="txtTransDescrip" CssClass="textbox" runat="server" TextMode="MultiLine"
                                                                                Rows="2" TabIndex="3" Width="325px">
                                                                            </asp:TextBox>
                                                                            <telerik:RadComboBox ID="ddlDescription" runat="server" TabIndex="7" AutoPostBack="True" ForeColor="Black" BackColor="White"
                                                                                Width="325px">
                                                                            </telerik:RadComboBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblTransReference" runat="server" CssClass="label">Reference</asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <asp:TextBox ID="txtTransReference" runat="server" TabIndex="4" Width="300px" CssClass="textbox"></asp:TextBox>

                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblPaymentCode" runat="server" CssClass="label">Payment Code<span  
                                                                                             style="font-size: xx-small; color: #b71c1c; font-family: v, Verdana">*</span></asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <telerik:RadComboBox ID="ddlPaymentCode" runat="server" TabIndex="7" AutoPostBack="True" ForeColor="Black" BackColor="White"
                                                                                Width="325px">
                                                                            </telerik:RadComboBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblFundSourceId" runat="server" CssClass="label">Fund Source<font color="red">*</font></asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <telerik:RadComboBox ID="ddlFundSourceId" runat="server" AutoPostBack="True" Enabled="False"
                                                                                TabIndex="14" Width="325px">
                                                                            </telerik:RadComboBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblTransCodeId" runat="server" CssClass="label">Charge Code</asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <telerik:RadComboBox ID="ddlTransCodeId" TabIndex="8" runat="server" Width="325px" ForeColor="Black" BackColor="White">
                                                                            </telerik:RadComboBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblPaymentTypeId" runat="server" CssClass="label">Payment Type<font color="red">*</font></asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <telerik:RadComboBox ID="ddlPaymentTypeId" runat="server" Width="325px" AutoPostBack="True" ForeColor="Black" BackColor="White"
                                                                                TabIndex="11">
                                                                            </telerik:RadComboBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblTypeOfReference" runat="server" CssClass="label">Reference Type</asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <asp:TextBox ID="txtReferenceNumber" runat="server" CssClass="textbox" MaxLength="50"
                                                                                TabIndex="13" Width="320px"></asp:TextBox>
                                                                            <asp:TextBox ID="txtTransTypeId" runat="server" Visible="False"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblTransDate" runat="server" CssClass="label">Transaction Date<font color="red">*</font></asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <telerik:RadDatePicker runat="server" ID="txtTransDate" DatePopupButton-ToolTip="" Width="325px"
                                                                                Calendar-ShowRowHeaders="false">
                                                                            </telerik:RadDatePicker>
                                                                            <asp:RangeValidator ID="drvTransactionDate" runat="server" Display="None" ErrorMessage="Date is Invalid or outside allowed range"
                                                                                ControlToValidate="txtTransDate" Type="Date" MaximumValue="2049/12/31" MinimumValue="1945/01/01">Date is Invalid or outside allowed range</asp:RangeValidator>
                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <div class="searchformlabel" style='width: 128px;'>
                                                                            <asp:Label ID="lblTransAmount" runat="server" CssClass="label">Amount<font color="red">*</font></asp:Label>
                                                                        </div>
                                                                        <div class="searchformtext">
                                                                            <asp:TextBox ID="txtTransAmount" runat="server" CssClass="textbox" Width="311px"
                                                                                AutoPostBack="true" TabIndex="9"></asp:TextBox>
                                                                            <br/>
                                                                            <span class="label">
                                                                                <%=MyAdvAppSettings.AppSettings("Currency")%>
                                                                            </span>
                                                                            <asp:CheckBox ID="chkUseSchedPmt" runat="server" Text="Use Projected Amount" CssClass="checkboxinternational"
                                                                                AutoPostBack="True" TabIndex="10"></asp:CheckBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="searchformrow" style="text-align: left; margin-top: 10px;">
                                                                        <asp:Button ID="btnSaveAndPrint" runat="server" Text="Save and Print Receipt" CssClass="save"/>
                                                                    </div>
                                                                    <div class="searchformrow">
                                                                        <asp:Label ID="lblfinAidRequirement" runat="server" CssClass="label" Text="Financial Aid Requirement"
                                                                            Visible="false" Width="450px"></asp:Label>
                                                                    </div>
                                                                </asp:Panel>
                                                                <div class="searchformrow">
                                                                    <asp:Label ID="lblNoTerm" runat="server" CssClass="label" Visible="false" Width="450px"></asp:Label>
                                                                </div>
                                                                <div class="searchformrow">
                                                                    <asp:Label ID="lblstuLedgerBalance" runat="server" CssClass="labelamount" Height="32px"
                                                                        Text="Student Ledger Balance" Visible="False"></asp:Label>
                                                                </div>
                                                                <div class="searchformrow">
                                                                    <asp:TextBox ID="txtAcademicYearId" runat="server" CssClass="visibility">AcademicYearId</asp:TextBox>
                                                                    <asp:TextBox ID="txtTermId" runat="server" CssClass="visibility">TermId</asp:TextBox>
                                                                </div>
                                                            </td>
                                                            <td valign="top" style="width: 50%;">
                                                                <asp:Panel runat="server" ID="pnlColumn2">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" border="0">
                                                                        <tr>
                                                                            <td style="width: 38%; text-align: left">
                                                                                <asp:CheckBox ID="chkScheduledPayment" runat="server" Text="Scheduled Payment" CssClass="checkbox"
                                                                                    AutoPostBack="True" TabIndex="12" Enabled="false" Visible="false"></asp:CheckBox>
                                                                            </td>
                                                                            <td style="width: 20%; white-space: nowrap;">
                                                                                <asp:Label ID="lblDisbTypeId" runat="server" CssClass="label" Visible="false">Source</asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; white-space: nowrap;">
                                                                                <asp:DropDownList ID="ddlDisbTypeId" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                                                                    Enabled="False" TabIndex="13" Visible="false">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 100%; text-align: left" colspan="3">
                                                                                <asp:Panel runat="server" ID="pnlDataGrid">
                                                                                    <div>
                                                                                        <asp:Label ID="lblPaymentApplication" runat="server"  CssClass="font-blue-darken-3">Payment Application</asp:Label>
                                                                                    </div>
                                                                                    <div style="overflow: auto; width: 100%;">
                                                                                        <asp:DataGrid ID="dgrdDisbursements" TabIndex="15" runat="server" Width="100%" Height="200px" AutoGenerateColumns="False"
                                                                                            AllowSorting="True" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0"
                                                                                            DataKeyField="ScheduleId">
                                                                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                                                            <Columns>
                                                                                                <asp:TemplateColumn>
                                                                                                    <HeaderTemplate>
                                                                                                        <asp:CheckBox ID="chkAllItems" runat="server" CssClass="checkboxbold" AutoPostBack="True"></asp:CheckBox><asp:Label ID="labelselect" Width="68%" runat="server">Select All</asp:Label>
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="chkSelectedDisb" CssClass="checkboxbold" runat="server" Text=""
                                                                                                            AutoPostBack="True"></asp:CheckBox>
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle Width="22%" />
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn HeaderText="Scheduled Date">
                                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblDatePeriod" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ExpectedDate", "{0:d}") %>'> </asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle HorizontalAlign="Center" Width="18%" />
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn HeaderText="Disbursement">
                                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblDisbursement" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScheduleDescrip") %>'> </asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle HorizontalAlign="Center" Width="35%" />
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn HeaderText="Original Amount">
                                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblProjectedAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount", "{0:c}") %>'> </asp:Label>
                                                                                                        <asp:Label ID="lblScheduleId" runat="server" Text='<%# Container.DataItem("ScheduleId") %>'
                                                                                                            Visible="False"> </asp:Label>
                                                                                                        <asp:Label ID="lblIsAwardDisb" runat="server" Text='<%# Container.DataItem("IsAwardDisb") %>'
                                                                                                            Visible="False"> </asp:Label>
                                                                                                        <asp:CheckBox ID="chkScheduleIsInDB" runat="server" Checked='<%# Container.DataItem("IsInDB") %>'
                                                                                                            Visible="False"></asp:CheckBox>
                                                                                                        <asp:Label ID="lblModDate" runat="server" Text='<%# Container.DataItem("ModDate") %>'
                                                                                                            Visible="false"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn HeaderText="Balance">
                                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblBalance" runat="server" Text='<%# Databinder.Eval(Container, "DataItem.Balance", "{0:c}") %>'> </asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:TemplateColumn HeaderText="Amount To Pay">
                                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:TextBox ID="txtAmountToApply" runat="server" CssClass="textbox" Text='<%# Databinder.Eval(Container, "DataItem.Balance", "{0:#.00}") %>'> </asp:TextBox>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                            </Columns>
                                                                                        </asp:DataGrid>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="1000px" colspan="4" style="padding-top: 20px">
                                                                                <table cellspacing="0" cellpadding="0" width="100%" align="center">
                                                                                    <tr>
                                                                                        <td class="spacertables"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="label" align="center" colspan="2">
                                                                                            <asp:Label ID="lblAppliedPmtCaption" runat="server"  CssClass="font-blue-darken-3">Apply payment to charge</asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="spacertables"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <asp:DataGrid ID="dgrdApplyPayment" runat="server" EditItemStyle-Wrap="false" HeaderStyle-Wrap="true"
                                                                                                AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="True" GridLines="Both"
                                                                                                Width="100%" BorderWidth="1px" BorderColor="#E0E0E0">
                                                                                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                                                                <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                                                                <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                                                                <Columns>
                                                                                                    <asp:TemplateColumn HeaderText="Transaction">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTransactionId" runat="server" Visible="false" Text='<%# Container.DataItem("TransactionId") %>'>
                                                                                                            </asp:Label>
                                                                                                            <asp:Label ID="lblTransaction" runat="server" Text='<%# Container.DataItem("TransCodeDescrip") %>'>
                                                                                                            </asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="Date">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransDate", "{0:d}") %>'>
                                                                                                            </asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="Original Amount">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblOriginalAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransAmount", "{0:c}") %>'>
                                                                                                            </asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="Balance">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblBalance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Balance", "{0:c}") %>'>
                                                                                                            </asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="Amount to Pay">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="txtAmountToApply" runat="server" CssClass="textbox">0.00</asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="visibility: hidden;">
                                                                            <td class="style1" style="visibility: hidden; white-space: nowrap;">
                                                                                <asp:Label ID="lblStudent" runat="server" CssClass="label">Student</asp:Label>
                                                                            </td>
                                                                            <td class="twocolumncontentcell" style="text-align: left; visibility: hidden; white-space: nowrap; margin-left: 10px;">
                                                                                <asp:TextBox ID="txtStudentName" runat="server" CssClass="textboxsearch" contentEditable="False"
                                                                                    TabIndex="1"></asp:TextBox>
                                                                                <a href="#" onclick="openwin()">
                                                                                    <img id="studentSearchIcon" runat="server" src="../images/top_search3.gif" border="0" /></a>
                                                                                <asp:LinkButton ID="lbtSearch" runat="server" CausesValidation="False" CssClass="searchlinkbutton"
                                                                                    TabIndex="2" Visible="False"> <img border="0" src="../images/top_search3.gif" alt="Search Student" align="absmiddle"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="visibility: hidden;">
                                                                            <td class="style1" style="visibility: hidden; white-space: nowrap;">
                                                                                <asp:Label ID="lblEnrollmentId" CssClass="label" runat="server">Enrollment</asp:Label>
                                                                            </td>
                                                                            <td class="twocolumncontentcell" style="visibility: hidden; white-space: nowrap;">
                                                                                <asp:TextBox ID="txtStuEnrollment" runat="server" CssClass="textbox" contentEditable="False"
                                                                                    TabIndex="2"></asp:TextBox>
                                                                                <asp:TextBox ID="txtStuEnrollmentId" runat="server" CssClass="textbox" Width="0px">StuEnrollmentId</asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="visibility: hidden;">
                                                                            <td class="style1" style="visibility: hidden; white-space: nowrap;">
                                                                                <asp:Label ID="lblAcademicYearId" runat="server" CssClass="label">Academic Year</asp:Label>
                                                                            </td>
                                                                            <td class="twocolumncontentcell" style="visibility: hidden; white-space: nowrap;">
                                                                                <asp:TextBox ID="txtAcademicYear" runat="server" CssClass="textbox" contentEditable="False"
                                                                                    TabIndex="5"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="visibility: hidden;">
                                                                            <td class="style1" style="visibility: hidden; white-space: nowrap;">
                                                                                <asp:Label ID="lblTermId" runat="server" CssClass="label">Term</asp:Label>
                                                                            </td>
                                                                            <td class="twocolumncontentcell" style="visibility: hidden; white-space: nowrap;">
                                                                                <asp:TextBox ID="txtTerm" TabIndex="6" runat="server" CssClass="textbox" contentEditable="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                            <asp:DataGrid ID="dgrdScrollableGrid" runat="server" HorizontalAlign="Center">
                                            </asp:DataGrid>
                                            <!--
                                        <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" border="1">
                                            <TR>
                                                <TD class="spacertables"></TD>
                                            </TR>
                                            <TR>
                                                <TD class="contentcellheader" noWrap colSpan="6">
                                                    <asp:label id="lblSDF" cssclass="Label" Runat="server" Font-Bold="true">School Defined Fields</asp:label></TD>
                                            </TR>
                                            <TR>
                                                <TD class="spacertables"></TD>
                                            </TR>
                                        </TABLE>
                                        <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" border="1">
                                            <TR>
                                                <TD class="contentcell2" colSpan="6">
                                                    <asp:panel id="pnlSDF" tabIndex="12" Runat="server" enableviewstate="false"></asp:panel></TD>
                                            </TR>
                                        </TABLE> 
                                        -->

                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- end rightcolumn -->
                    </tr>
                </table>
                <!-- start validation panel-->
                <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
                </asp:Panel>
                <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                    Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
                <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
                </asp:Panel>
                <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                    ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
                <!--end validation panel-->
            </div>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadWindowManager runat="server" ID="RadWindowManager1"></telerik:RadWindowManager>
    <asp:Button ID="Hdn" runat="server" OnClick="Hdn_Click" style="display: none"  />
    <asp:Button ID="HdnSaveAndPrint" runat="server" OnClick="HdnSaveAndPrint_Click"  style="display: none" />
    <script type="text/javascript">
        $(document).keypress(function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode === 13) {
                var btnId = $(document.activeElement).attr("id");
                if (btnId === "btnSaveAndPrint") {
                    return true;
                }
                else {
                    return false;
                }
            }
            return true;
        });
    </script>
</asp:Content>
