﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections.Generic
Partial Class PostLateFees
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents allDataset As System.Data.DataSet
    Private empId As String
    Private employee As String
    Protected totalAmount As Decimal
    Protected totalStudents As Integer
    Protected lastStudent As String

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Private studentName As String = String.Empty


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim userId As String
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
      If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
        If Not IsPostBack Then
            Dim lateFeesInfo As LateFeesInfo = (New LateFeesFacade).GetCurrentLateFeesInfo(campusId)
            If lateFeesInfo Is Nothing Then
                'DisplayErrorMessage("There is no Late Fees information for this Campus Group")
                DisplayRADAlert(CallbackType.Postback, "NoLateFeesInfo", "There is no Late Fees information for this Campus Group", "No Late Fee Info")
                btnBuildList.Enabled = False
                Exit Sub
            Else
                btnBuildList.Enabled = True
                With lateFeesInfo
                    ViewState("FlatAmount") = .FlatAmount
                    ViewState("Rate") = .Rate
                    ViewState("GracePeriod") = .GracePeriod
                    ViewState("TransCodeId") = .TransCodeId
                    ViewState("EffectiveDate") = .EffectiveDate
                End With
            End If

            Dim lastPostedDate As Date = (New StudentsAccountsFacade).GetDateOfLastLateFeesPosting(campusId)
            If Not lastPostedDate = Date.MinValue Then
                txtLastPostedDate.Text = lastPostedDate.ToShortDateString
            Else
                txtLastPostedDate.Text = String.Empty
            End If
            If lastPostedDate.AddMonths(1) >= Date.Today Then
                txtCutoffDate.SelectedDate = Date.Today.ToShortDateString
            Else
                If Not (lastPostedDate = Date.MinValue) Then
                    txtCutoffDate.SelectedDate = lastPostedDate.AddMonths(1).ToShortDateString
                Else
                    txtCutoffDate.SelectedDate = Date.Now.ToShortDateString
                End If
            End If

            If pObj.HasFull Or pObj.HasAdd Then
                btnBuildList.Enabled = True
            Else
                btnBuildList.Enabled = False
            End If

            'initialize textbox with todays date
            txtPostFeesDate.SelectedDate = Date.Today.ToShortDateString()
        End If

        '   Post Fees button should be disabled in most cases
        SetControls(False)
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub dgrdPostLateFees_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdPostLateFees.ItemCommand
        '   process postbacks from the datagrid

        '   Bind DataGrid
        BindDataGrid(e)

    End Sub

    Private Sub BindDataGrid()

        '   bind datagrid to dataGridTable
        dgrdPostLateFees.Visible = True
        dgrdPostLateFees.DataSource = New DataView(allDataset.Tables(0), Nothing, "StudentName asc ", DataViewRowState.CurrentRows)
        dgrdPostLateFees.DataBind()

        'unhide all checkboxes from the datagrid
        dgrdPostLateFees.Columns(6).Visible = True

        'register a javascript to check or uncheck all checkboxes at once
        CommonWebUtilities.CheckUncheckAllCheckBoxes(Me.GetType(), Me.Page, CType(dgrdPostLateFees.Controls(0).Controls(0).FindControl("cbAll"), CheckBox), "cbApplyFee")

    End Sub
    Private Sub BindDataGrid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

        allDataset = ViewState("LateFeesDS")

        '   bind datagrid to dataGridTable
        dgrdPostLateFees.Visible = True
        dgrdPostLateFees.DataSource = New DataView(allDataset.Tables(0), Nothing, "StudentName asc ", DataViewRowState.CurrentRows)
        dgrdPostLateFees.DataBind()

        'unhide all checkboxes from the datagrid
        dgrdPostLateFees.Columns(6).Visible = True

        'register a javascript to check or uncheck all checkboxes at once
        CommonWebUtilities.CheckUncheckAllCheckBoxes(Me.GetType(), Me.Page, CType(dgrdPostLateFees.Controls(0).Controls(0).FindControl("cbAll"), CheckBox), "cbApplyFee")

        Session("LateFeesDS") = allDataset
        Session("StuEnrollId") = CType(e.Item.FindControl("txtStuEnrollId"), TextBox).Text
        Dim url As String = "../SA/StudentPPDetails.aspx"
        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium
        Dim name As String = "StudentPPDetails"
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub
    Private Sub btnBuildList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuildList.Click

        If pObj.HasFull Or pObj.HasAdd Then
            '   enable or disable the "post button" accordingly
            SetControls(True)
        Else
            SetControls(False)
        End If

        'validate Process Date
        If Not IsDate(txtCutoffDate.SelectedDate) Then
            '   Display Error Message
            DisplayErrorMessage("Invalid Process Date")
            Exit Sub
        End If

        '   get dataset from the backend
        allDataset = (New StudentAwardFacade).GetLateFeesToBePostedDS(Date.Parse(txtCutoffDate.SelectedDate), ViewState("GracePeriod"), AdvantageSession.UserState.UserName, campusId, ViewState("EffectiveDate").ToString)
        ViewState("LateFeesDS") = allDataset

        '   bind datagrid
        BindDataGrid()

    End Sub

    Private Sub dgrdPostLateFees_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdPostLateFees.ItemDataBound
        Dim applyOnlyFlatAmount As Boolean = CType(ViewState("Rate"), Decimal) = 0.0
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Select Case applyOnlyFlatAmount
                    Case True
                        '   acumulate balance
                        Dim lblAmount As Label = CType(e.Item.FindControl("lblAmount"), Label)
                        Dim cbApplyFee As CheckBox = CType(e.Item.FindControl("cbApplyFee"), CheckBox)
                        If Not lastStudent = e.Item.DataItem("StudentName") Then
                            totalStudents += 1
                            lastStudent = e.Item.DataItem("StudentName")
                            Dim txtPayPlanScheduleId As TextBox = CType(e.Item.FindControl("txtPayPlanScheduleId"), TextBox)
                            txtPayPlanScheduleId.Text = CType(e.Item.DataItem("PayPlanScheduleId"), Guid).ToString()
                            Dim amount As Decimal = ViewState("FlatAmount") + ViewState("Rate") * e.Item.DataItem("Balance") / 100.0
                            lblAmount.Text = amount.ToString("c")
                            totalAmount += amount
                            cbApplyFee.Checked = True
                            cbApplyFee.Visible = True
                            e.Item.Visible = True
                            If (totalStudents Mod 2) = 0 Then
                                e.Item.CssClass = "DataGridAlternatingStyle"
                            Else
                                e.Item.CssClass = "DataGridItemStyle"
                            End If
                        Else
                            'e.Item.Visible = (studentName = e.Item.DataItem("StudentName"))
                            e.Item.Visible = False
                            cbApplyFee.Visible = False
                        End If
                    Case Else
                        '   acumulate balance
                        Dim lblAmount As Label = CType(e.Item.FindControl("lblAmount"), Label)
                        Dim cbApplyFee As CheckBox = CType(e.Item.FindControl("cbApplyFee"), CheckBox)
                        If Not lastStudent = e.Item.DataItem("StudentName") Then
                            totalStudents += 1
                            lastStudent = e.Item.DataItem("StudentName")
                        End If
                        Dim txtPayPlanScheduleId As TextBox = CType(e.Item.FindControl("txtPayPlanScheduleId"), TextBox)
                        txtPayPlanScheduleId.Text = CType(e.Item.DataItem("PayPlanScheduleId"), Guid).ToString()
                        Dim amount As Decimal = ViewState("FlatAmount") + ViewState("Rate") * e.Item.DataItem("Balance") / 100.0
                        lblAmount.Text = amount.ToString("c")
                        totalAmount += amount
                        cbApplyFee.Checked = True
                        cbApplyFee.Visible = True
                        e.Item.Visible = True
                End Select
            Case ListItemType.Footer
                '   display total in footer
                CType(e.Item.FindControl("lblFooterAmount"), Label).Text = totalAmount.ToString("c")

                '   enable Post Fees button only if there are records in the datagrid
                If totalAmount > 0 Then
                    SetControls(True)
                    'Set the PostFee Button so it prompts the user for confirmation when clicked
                    btnPostFees.Attributes.Add("onclick", "if(confirm('Are you sure you want to post these fees?. After posting you will not be allowed to export these fees to Excel.')){}else{return false}")
                Else
                    SetControls(False)
                End If

                '   put totals on top   
                lblTotalStudents.Text = totalStudents.ToString("#####0")
                lblTotalAmount.Text = totalAmount.ToString("c")
        End Select
    End Sub
    Private Sub btnPostFees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPostFees.Click
        'validate Post Fees Date
        If Not IsDate(txtPostFeesDate.SelectedDate) Then
            '   Display Error Message
            DisplayErrorMessage("Invalid Post Late Fees Date")
            Exit Sub
        End If
        Dim postFeesDate = Date.Parse(txtPostFeesDate.SelectedDate)

        Dim lateFees() As String = GetLateFeesToApply()

        'apply Late Fees
        Dim result As String = (New StudentsAccountsFacade).ApplyLateFees(lateFees, ViewState("TransCodeId"), postFeesDate, campusId, AdvantageSession.UserState.UserName)

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            ProcessSuccesfulPosting()
        End If
    End Sub
    Private Sub ProcessSuccesfulPosting()
        'clear the datagrid
        dgrdPostLateFees.Visible = False
        'dgrdPostLateFees.DataSource = Nothing
        'dgrdPostLateFees.DataBind()

        '   Display Success message
        DisplayErrorMessage("Posting was succesfull")
        SetControls(False)

    End Sub

    Private Sub btnExportToExcell_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcell.Click
        'hide all checkboxes from the datagrid
        dgrdPostLateFees.Columns(6).Visible = False
        'convert all link buttons to labels
        ConvertLinkButtonsToLabels(dgrdPostLateFees)

        Dim oStringWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim oHtmlTextWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(oStringWriter)

        'delete from the datagrid all controls that are not literals
        ClearControls(dgrdPostLateFees)

        'render datagrid
        dgrdPostLateFees.RenderControl(oHtmlTextWriter)

        'convert html to string
        Dim enc As System.Text.Encoding = System.Text.Encoding.UTF8
        Dim s As String = oStringWriter.ToString

        'move html content to a memorystream
        Dim documentMemoryStream As New System.IO.MemoryStream
        documentMemoryStream.Write(enc.GetBytes(s), 0, s.Length)

        '   save document and document type in sessions variables
        Session("DocumentMemoryStream") = documentMemoryStream
        Session("ContentType") = "application/vnd.ms-excel"

        '   Register a javascript to open the report in another window
        Dim javascript As String = "<script>var origwindow=window.self;window.open('PrintAnyReport.aspx');</script>"
        Dim csType As Type = Me.[GetType]()


        ClientScript.RegisterClientScriptBlock(csType, "NewWindow", javascript)

        'hide the datagrid
        dgrdPostLateFees.Visible = False

    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step i - 1
            ClearControls(control.Controls(i))
        Next
        If control.GetType.ToString = "System.Web.UI.HtmlControls.HtmlTableCell" Then
            If Not control.GetType().GetProperty("SelectedItem") Is Nothing Then
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            Else
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            End If
        End If
        Return
    End Sub
    Private Sub SetControls(ByVal val As Boolean)
        btnPostFees.Enabled = val And (dgrdPostLateFees.Items.Count > 0)
        txtPostFeesDate.Enabled = val
        txtPostFeesDate.Visible = val
        btnExportToExcell.Enabled = btnPostFees.Enabled
    End Sub
    Private Function GetLateFeesToApply() As String()
        Dim lateFees As New List(Of String)
        Dim list As DataGridItemCollection = dgrdPostLateFees.Items
        For Each item As DataGridItem In list
            If item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.Item Then
                If item.Visible Then
                    Dim cbControl As CheckBox = item.FindControl("cbApplyFee")
                    If Not cbControl Is Nothing Then
                        If cbControl.Checked Then
                            Dim sb As New StringBuilder()
                            sb.Append(CType(item.FindControl("txtPayPlanScheduleId"), TextBox).Text)
                            sb.Append(";")
                            sb.Append(CType(item.FindControl("lblAmount"), Label).Text)
                            lateFees.Add(sb.ToString())
                        End If
                    End If
                End If
            End If
        Next
        Return lateFees.ToArray()
    End Function
    Private Sub ConvertLinkButtonsToLabels(ByVal dgrd As DataGrid)
        For i As Integer = 0 To dgrd.Items.Count - 1
            Dim lb As LinkButton = CType(dgrd.Items(i).FindControl("lbStudentName"), LinkButton)
            Dim lab As Label = New Label()
            lab.Text = lb.Text
            Dim parent As TableCell = lb.Parent
            parent.Controls.Remove(lb)
            parent.Controls.AddAt(0, lab)
        Next
    End Sub

End Class
