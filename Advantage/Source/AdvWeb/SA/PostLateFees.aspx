﻿<%@ Page Title="Post Late Fees" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostLateFees.aspx.vb" Inherits="PostLateFees" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <table id="Table1" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <br />
                                <td class="contentcellblue">
                                    <asp:Label ID="lblLastPostedDate" runat="server" CssClass="label">Last Posted Date</asp:Label>
                                </td>
                                <td class="contentcell4blue" align="left">
                                    <asp:TextBox ID="txtLastPostedDate" runat="server" ReadOnly="true" CssClass="textbox"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblCutoffDate" runat="server" CssClass="label">Process Date</asp:Label>
                                </td>

                                <td class="contentcell4blue" align="left">

                                    <telerik:RadDatePicker ID="txtCutoffDate" MinDate="1/1/1945" Width="200px" runat="server">
                                    </telerik:RadDatePicker>

                                </td>
                            </tr>

                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblShowAllPendingAmounts" runat="server" Visible="false" CssClass="label">Show All</asp:Label>
                                </td>
                                <td class="contentcell4blue">
                                    <asp:CheckBox ID="cbShowAllPendingAmounts" runat="server" Visible="false" CssClass="checkbox"></asp:CheckBox>
                                <td>
                            </tr>

                            <tr>
                                <div>
                                    <td class="contentcellblue"></td>
                                    <td class="contentcell4blue" align="left">
                                        <br />
                                        <asp:Button ID="btnBuildList" runat="server" CausesValidation="False" Text="Show Late Fees"></asp:Button>
                                    </td>
                                </div>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="ScrollLeftFltr4Rows">
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->


                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr>
                                    <td class="contentcell" style="width: 20%">
                                        <asp:Label ID="lblTotalStudentsHdr" runat="server" CssClass="labelbold">Total Students:</asp:Label>
                                    </td>
                                    <td class="contentcell4" style="text-align: left">
                                        <asp:Label ID="lblTotalStudents" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblTotalAmountHdr" runat="server" CssClass="labelbold">Total Amount:</asp:Label>
                                    </td>
                                    <td class="contentcell4" style="padding-bottom: 10px; text-align: left">
                                        <asp:Label ID="lblTotalAmount" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="label2" runat="server" CssClass="labelbold">Late Fees Transaction Date:</asp:Label>
                                    </td>
                                    <td class="contentcell4" style="padding-bottom: 10px">


                                        <telerik:RadDatePicker ID="txtPostFeesDate" MaxDate="2049/12/31" MinDate="2001/01/01" runat="server">
                                        </telerik:RadDatePicker>

                                    </td>
                                </tr>
                                <tr>
                                    <td  style="padding-bottom: 10px; text-align: right" colspan="2">
                                        <asp:Button ID="btnPostFees" runat="server" Text="Post Late Fees"
                                            Enabled="False"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="padding-bottom: 10px; text-align: right" colspan="2">
                                        <asp:Button ID="btnExportToExcell" runat="server" 
                                            Text="Export to Excel" Enabled="False"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <asp:TextBox ID="txtBatchPaymentId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox
                                    ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtModUser"
                                        runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate"
                                            runat="server" Visible="False">ModDate</asp:TextBox><tr>
                                                <td align="center" colspan="4">
                                                    <asp:DataGrid ID="dgrdPostLateFees" Width="100%" BorderStyle="Solid" AutoGenerateColumns="False"
                                                        HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" runat="server" BorderColor="#E0E0E0"
                                                        BorderWidth="1px">
                                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Student Name">
                                                                <HeaderStyle  Width="15%" />
                                                                
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbStudentName" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.StudentName") %>'
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.StudentName") %>' runat="server"></asp:LinkButton>
                                                                    <asp:TextBox ID="txtStuEnrollId" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.StuEnrollId") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="StudentIdentifier" ReadOnly="True" HeaderText="Student Id.">
                                                                <HeaderStyle  Width="15%" />
                                                                
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="ExpectedDate" HeaderText="Due Date" DataFormatString="{0:d}"
                                                                ReadOnly="True">
                                                                <HeaderStyle  Width="10%" />
                                                                
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="GraceDate" HeaderText="Grace Date" DataFormatString="{0:d}"
                                                                ReadOnly="True">
                                                                <HeaderStyle  Width="10%" />
                                                                
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Balance" HeaderText="Amount Due" DataFormatString="{0:c}"
                                                                ReadOnly="True">
                                                                <HeaderStyle  Width="10%" />
                                                                
                                                            </asp:BoundColumn>
                                                            <asp:TemplateColumn HeaderText="Late Fee">
                                                                <HeaderStyle  Width="10%" />
                                                                <ItemStyle CssClass="datagriditemstyle" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Right"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAmount" runat="server" CssClass="labelamount"></asp:Label>&nbsp;
                                                    <asp:TextBox ID="txtPayPlanScheduleId" runat="server" Visible="False"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lblFooterAmount" runat="server" CssClass="labelamount"></asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn>
                                                                <HeaderStyle  Width="10%" Font-Bold="False" Font-Italic="False"
                                                                    Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                                                
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="cbApplyFee" runat="server" CssClass="checkbox" />
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="cbAll" Text="Apply Fee" CssClass="checkbox" Style="font-weight: bold"
                                                                        Checked="true" runat="server" />
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

