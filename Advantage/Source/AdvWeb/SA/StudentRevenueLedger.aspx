﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentRevenueLedger.aspx.vb" Inherits="StudentRevenueLedger" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="450" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>

            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblEnrollment" runat="server" CssClass="labelbold">Enrollment</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:DropDownList ID="ddlEnrollmentId" runat="server" CssClass="dropdownlist" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftledger">
                            <table cellspacing="0" cellpadding="0" style="width: 100%; border: none;">
                                <tr>
                                    <td class="threecolumndatalist" style="background-color: #FFF; border-right: #EBEBEB 1px solid; border-top: #EBEBEB 1px solid; border-left: #EBEBEB 1px solid; width: 55%; ">
                                        <asp:Label ID="lblTransCodeHeader" CssClass="LabelBold" runat="server">Trans Code</asp:Label></td>
                                    <td class="threecolumndatalist" style="background-color: #FFF; border-right: #EBEBEB 1px solid; border-top: #EBEBEB 1px solid; border-left: #EBEBEB 1px solid; width: 15%;">
                                        <asp:Label ID="lblTransAmount" CssClass="LabelBold" runat="server">Charged</asp:Label></td>
                                    <td class="threecolumndatalist" style="background-color: #FFF; border-right: #EBEBEB 1px solid; border-top: #EBEBEB 1px solid; width: 15%; ">
                                        <asp:Label ID="lblEarnedAmountHeader" CssClass="LabelBold" runat="server">Earned</asp:Label></td>
                                    <td class="threecolumndatalist" style="background-color: #FFF; border-right: #EBEBEB 1px solid; border-top: #EBEBEB 1px solid; width: 15%; ">
                                        <asp:Label ID="lblUnearnedAmountHeader" CssClass="LabelBold" runat="server">Unearned</asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:DataList ID="dlstStudentRevenueLedger" runat="server" GridLines="Both" RepeatDirection="Vertical"
                                                      BorderWidth="1px" BorderColor="#EBEBEB"
                                            RepeatLayout="Table" Width="100%">
                                            <ItemStyle CssClass="itemstyle"></ItemStyle>
                                            <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                            <ItemTemplate>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="threecolumndatalist" style="width: 55%;">
                                                            <asp:LinkButton ID="lnkTransCode" runat="server" CssClass="itemstyle" Text='<%# Container.DataItem("TransCodeDescrip") %>'
                                                                CommandArgument='<%# Container.DataItem("TransCodeId") %>'> </asp:LinkButton></td>
                                                        <td class="threecolumndatalist" style="width: 15%;">
                                                            <asp:Label ID="lblFeeAmount" runat="server" CssClass="ItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.TransAmount", "{0:###,###.00}") %>'></asp:Label></td>
                                                        <td class="threecolumndatalist" style="width: 15%;">
                                                            <asp:Label ID="lblEarnedAmount" runat="server" CssClass="ItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Earned", "{0:###,###.00}") %>'></asp:Label></td>
                                                        <td class="threecolumndatalist" style="width: 15%;">
                                                            <asp:Label ID="lblUnearnedAmount" runat="server" CssClass="ItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Unearned", "{0:###,###.00}") %>'></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="false"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->

                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td nowrap>
                                        <telerik:RadGrid ID="dgrdStudentRevenueLedger" runat="server" Width="100%" AutoGenerateColumns="false" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0"
                                            OnNeedDataSource="dgrdStudentRevenueLedger_NeedDataSource" OnDetailTableDataBind="dgrdStudentRevenueLedger_DetailTableDataBind">

                                            <MasterTableView Width="100%" Name="Fees" AllowMultiColumnSorting="false" DataKeyNames="TransactionId" ShowFooter="true">
                                                <DetailTables>
                                                    <telerik:GridTableView DataKeyNames="TransactionId" Name="Earnings" Width="99%" HorizontalAlign="Center" ShowFooter="true">
                                                        <Columns>
                                                            <telerik:GridBoundColumn HeaderText="Date" DataField="DefRevenueDate" DataFormatString="{0:d}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn HeaderText="Earned" DataField="Amount" DataFormatString="{0:n}" Aggregate="Sum"></telerik:GridBoundColumn>
                                                        </Columns>

                                                    </telerik:GridTableView>
                                                </DetailTables>
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="DefRevenueDate" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}">
                                                        <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="TransCodeDescrip" HeaderText="Trans Code"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="TransDescrip" HeaderText="Description"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="FeeLevel" HeaderText="Fee Level"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Charged" DataFormatString="{0:n}" Aggregate="Sum"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Earned" HeaderText="Earned" DataFormatString="{0:n}" Aggregate="Sum"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Unearned" HeaderText="Unearned" DataFormatString="{0:n}" Aggregate="Sum"></telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

