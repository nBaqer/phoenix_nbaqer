﻿Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports System.Xml

Partial Class AccountManagement
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Private m_Context As HttpContext
    Protected state As AdvantageSessionState
#Region "Page Events"
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ''Put user code to initialize the page here
        Dim userId As String
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        CampusSecurityCheck()
    End Sub
    Private Sub page_prerender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'save current state
        commonwebutilities.saveadvantagesessionstate(httpcontext.current, state)
    End Sub
#End Region
#Region "Switch Campus Logic"
    Public Sub CampusSecurityCheck()
        'Check if this page still exists in the menu while switching campus
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
    End Sub
#End Region
End Class
