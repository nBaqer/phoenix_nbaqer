
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects

Partial Class FourteenDayLetterSelector
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Protected userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString

        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageUserState, resourceId, campusId)

    End Sub

    Private Sub GetList()
        With dgrdStudentDetails
            .DataSource = (New FourteenDayLetterFacade).GetFourteenDayDS(txtStartDate.SelectedDate, txtEndDate.SelectedDate, campusId)
            .DataBind()
        End With

        btnGenerateLetter.Visible = True
    End Sub

    Private Sub btnGetList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetList.Click
        GetList()
    End Sub

    Private Sub GetCheckedTransactions()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim ErrStr As String = ""
        Dim selected As Boolean = False
        Dim TransId As String = String.Empty

        ' Save the datagrid items in a collection.
        iitems = dgrdStudentDetails.Items
        Try

            'Loop thru the collection to retrieve the chkbox value
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)

                If (CType(iitem.FindControl("chkCopy"), CheckBox).Checked = True) Then
                    'get the hidden values
                    TransId += "'" & (CType(iitem.FindControl("txtTransactionID"), TextBox).Text) & "'" & ","
                    'set flag to true if option is selected
                    selected = True
                End If
            Next

            If selected = False Then
                DisplayErrorMessage("Please select one or more student transactions in order to generate the invoice")
                Exit Sub
            Else
                Session("FourteenDayLetterTranID") = (TransId.Substring(0, (TransId.Trim.Length - 1)))
                PopUpReport()
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub GetCheckedTransactions" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub GetCheckedTransactions" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")

        End Try
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        'need to uncheck this when porting over to live
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub PopUpReport()
        Dim popupScript As String = "<script language='javascript'>" & _
            "window.open('FourteenDayLetter.aspx?resid=264&mod=AR" & "', 'CustomPopUp', " & _
            "'width=750px, height=400px, menubar=yes, resizable=no, toolbar=no, scrollbars=yes, resizable=yes, status=yes')" & _
            "</script>"

        Dim csType As Type = Me.[GetType]()


        ClientScript.RegisterStartupScript(csType, "PopupScript", popupScript)

    End Sub

    Private Sub btnGenerateLetter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateLetter.Click
        GetCheckedTransactions()
    End Sub
End Class
