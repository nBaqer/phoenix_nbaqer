﻿Imports System.Web.UI.WebControls.Expressions
Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common
Partial Class PostCharges
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents lblProspect As System.Web.UI.WebControls.Label
    Protected WithEvents CompareValidator1 As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents RangeValidator1 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents A1 As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents A2 As System.Web.UI.HtmlControls.HtmlAnchor
    Protected ResourceId As Integer
    Protected campusId, userId As String
    Protected moduleid As String
    Protected FeeTypeInteger As Integer
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'controlsToIgnore.Add(CalButton)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Session("SearchType") = "PostCharges"

        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim SDFControls As New SDFComponent

        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim pobj As New UserPagePermissionInfo

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        moduleid = HttpContext.Current.Request.Params("Mod").ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pobj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pobj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If



        'header1.EnableHistoryButton(False)
        If Not IsPostBack Then
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            If pobj.HasFull Or pobj.HasAdd Then
                btnSave.Enabled = True
                btnNew.Enabled = True
            Else
                btnSave.Enabled = False
                btnNew.Enabled = True
            End If
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   initialize screen with transaction in session or an empty transaction
            If CommonWebUtilities.IsValidGuid(Session("TransactionId")) Then
                '   assign session value and destroy session
                ViewState("TransactionId") = Session("TransactionId")
                Session("TransactionId") = Nothing
                btnNew.Enabled = False

                '   bind PostChargeInfo in session
                With New StudentsAccountsFacade
                    'BindPostChargeData(.GetPostChargeInfo("425aa61c-3823-40c8-90d7-cd8d029eb692"))
                    BindPostChargeData(.GetPostChargeInfo(ViewState("TransactionId")))
                End With

                '   if it is a reversal, create a new transaction
                If CType(Session("CommandName"), String) = "Reverse" Then

                    '   this is a reverse transaction
                    SetControls(False)

                    '   chkIsInDB
                    chkIsInDB.Checked = False

                    '   create a new guid
                    txtPostChargeId.Text = Guid.NewGuid.ToString

                    '   reverse the amount
                    txtTransAmount.Text = CType(Decimal.Parse(txtTransAmount.Text) * (-1), Decimal).ToString("#,###,###.00")

                    '   change original description
                    txtTransDescrip.Text += " - Adjustment For " + txtTransDate.SelectedDate.GetValueOrDefault()

                    '   set todays date
                    txtTransDate.SelectedDate = CType(Date.Now.ToShortDateString, Date?)

                    '   transaction type will be an adjustment
                    ddlTransTypeId.SelectedIndex = 1

                    '   hide the header and footer
                    'DisableHeaderAndFooter()

                    '   change the class to make up the space of the search button
                    'txtStudentName.CssClass = "TextBox"

                    '   Is Reversal
                    chkIsReversal.Checked = True

                    '   Original TransactionId
                    txtOriginalTransactionId.Text = ViewState("TransactionId")

                    ' disable fee type validation on reveral
                    Requiredfieldvalidator1.Enabled = False

                ElseIf CType(Session("CommandName"), String) = "Adjust" Then
                    '   hide the header and footer
                    'DisableHeaderAndFooter()
                End If

                ' hide student search user control and enable student panel
                StudSearch1.Visible = False
                pnlStudent.Visible = True

                '   set Student Name textbox
                'txtStudentName.Text = CType(Session("StudentName"), String)

                '   initialize buttons for edit
                'InitButtonsForEdit()

            Else
                '   if this is a popWindow without a valid transaction then close the window
                If ViewState("IsPopUp") = True Then
                    CommonWebUtilities.CloseWindow(Me.Page, Me.GetType())
                    Exit Sub
                End If

                '   bind an empty new PostChargeInfo
                BindPostChargeData(New PostChargeInfo)

                '   initialize buttons for load
                'InitButtonsForLoad()
            End If
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            'BindPostChargeData(New PostChargeInfo)

        End If

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = SDFControls.GetSDFExists(ResourceId, moduleid)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtTransDescrip.Text) <> "" Then
            SDFControls.GenerateControlsEdit(pnlSDF, ResourceId, txtPostChargeId.Text, moduleid)
        Else
            SDFControls.GenerateControlsNew(pnlSDF, ResourceId, moduleid)
        End If

        'Preserve the selection for use in Save
        If rdnfeelevel.SelectedValue <> "" Then
            FeeTypeInteger = rdnfeelevel.SelectedValue
        End If

    End Sub
    Private Sub BuildDropDownLists()
        BuildTransCodesDDL()

        ''This is the list of ddls
        'Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        ''TransCodes DDL()
        'ddlList.Add(New AdvantageDDLDefinition(ddlTransCodeId, AdvantageDropDownListName.Trans_Codes, campusId, True, True))

        'CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub

    Private Sub BuildTransCodeadjustments()
        If ddlTransCodeId.Items.Count > 0 Then
            ddlTransCodeId.Items.Clear()
        End If
        Dim dsTransTypes As DataSet
        dsTransTypes = (New StudentsAccountsFacade).GetAllAdjustmentsDescriptions("True")
        ViewState("dsTransTypes") = dsTransTypes

        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeID"
            .DataSource = dsTransTypes
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildTransCodesDDL()
        'With ddlTransCodeId
        '    .DataTextField = "TransCodeDescrip"
        '    .DataValueField = "TransCodeId"
        '    .DataSource = (New StudentsAccountsFacade).GetAllTransCodes(False)
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With
        If ddlTransCodeId.Items.Count > 0 Then
            ddlTransCodeId.Items.Clear()
        End If

        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeID"
            .DataSource = (New StudentsAccountsFacade).GetAllChargeDescriptions("True", New Guid(campusId))
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BindPostChargeData(ByVal PostChargeInfo As PostChargeInfo)
        Session("StuEnrollmentId") = ""
        Session("StuEnrollment") = ""
        Session("AcademicYear") = ""
        Session("AcademicYearId") = ""
        Session("TermId") = ""
        Session("Term") = ""
        Session("TransCode") = ""
        Session("TransType") = ""
        With PostChargeInfo
            chkIsInDB.Checked = .IsInDB
            chkIsPosted.Checked = .IsPosted
            'chkIsPosted.Checked = True
            txtPostChargeId.Text = .PostChargeId
            txtStuEnrollmentId.Text = .StuEnrollId
            'txtStuEnrollment.Text = .EnrollmentDescription
            ddlTransCodeId.SelectedValue = .TransCodeId
            txtTransDescrip.Text = .PostChargeDescription
            'txtStudentName.Text = ""
            '   set blank if the date is null 
            If Not (.PostChargeDate = Date.MinValue) Then
                txtTransDate.SelectedDate = .PostChargeDate.ToShortDateString()
            Else
                txtTransDate.SelectedDate = Nothing
            End If
            txtTransAmount.Text = .Amount.ToString("#,###,###.00")
            txtTransReference.Text = .Reference
            txtAcademicYearId.Text = .AcademicYearId
            txtAcademicYear.Text = .AcademicYearDescrip
            txtTermId.Text = .TermId
            txtTerm.Text = .TermDescrip
            drvTransactionDate.MinimumValue = .EarliestAllowedDate.Date.Year.ToString("000#") + "/" + .EarliestAllowedDate.Date.Month.ToString("0#") + "/" + .EarliestAllowedDate.Date.Day.ToString("0#")
            drvTransactionDate.MaximumValue = .LatestAllowedDate.Date.Year.ToString("000#") + "/" + .LatestAllowedDate.Date.Month.ToString("0#") + "/" + .LatestAllowedDate.Date.Day.ToString("0#")
            drvTransactionDate.ErrorMessage = String.Format("Date must be between {0} and {1}", .EarliestAllowedDate.ToShortDateString, .LatestAllowedDate.ToShortDateString)
            ddlTransTypeId.SelectedIndex = .TransTypeId
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            ''Added by saraswathi lakshmanan on may 18 2009
            ''To show the fee level
            If .FeeLevelId <> 0 Then
                rdnfeelevel.SelectedValue = .FeeLevelId
            Else
                rdnfeelevel.SelectedIndex = -1
            End If

            txtStudentName.Text = .StudentName
            txtStuEnrollmentDesc.Text = .EnrollmentDescription
            txtTermDesc.Text = .TermDescrip
            txtAcademicYearDesc.Text = .AcademicYearDescrip

        End With
    End Sub
    Private Sub BindSavePostChargeData(ByVal PostChargeInfo As PostChargeInfo)
        With PostChargeInfo
            chkIsInDB.Checked = .IsInDB
            chkIsPosted.Checked = .IsPosted
            'chkIsPosted.Checked = True
            txtPostChargeId.Text = .PostChargeId
            'txtStuEnrollmentId.Text = .StuEnrollId
            'txtStuEnrollment.Text = .EnrollmentDescription
            If Not IsNothing(Session("TransCode")) Then
                ddlTransCodeId.SelectedValue = Session("TransCode").ToString()
            End If
            ddlTransTypeId.SelectedIndex = Session("TransType")
            txtTransDescrip.Text = .PostChargeDescription
            '   set blank if the date is null 
            If Not (.PostChargeDate = Date.MinValue) Then
                txtTransDate.SelectedDate = .PostChargeDate.ToShortDateString()
            Else
                txtTransDate.SelectedDate = Nothing
            End If
            txtTransAmount.Text = .Amount.ToString("#,###,###.00")
            txtTransReference.Text = .Reference
            'txtAcademicYearId.Text = .AcademicYearId
            'txtAcademicYear.Text = .AcademicYearDescrip
            'txtTermId.Text = .TermId
            'txtTerm.Text = .TermDescrip
            drvTransactionDate.MinimumValue = .EarliestAllowedDate.Date.Year.ToString("000#") + "/" + .EarliestAllowedDate.Date.Month.ToString("0#") + "/" + .EarliestAllowedDate.Date.Day.ToString("0#")
            drvTransactionDate.MaximumValue = .LatestAllowedDate.Date.Year.ToString("000#") + "/" + .LatestAllowedDate.Date.Month.ToString("0#") + "/" + .LatestAllowedDate.Date.Day.ToString("0#")
            drvTransactionDate.ErrorMessage = String.Format("Date must be between {0} and {1}", .EarliestAllowedDate.ToShortDateString, .LatestAllowedDate.ToShortDateString)


            ''ddlTransTypeId.SelectedIndex = .TransTypeId

            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString

            txtStuEnrollmentId.Text = Session("StuEnrollmentId")
            txtStuEnrollment.Text = Session("StuEnrollment")
            txtAcademicYear.Text = Session("AcademicYear")
            txtAcademicYearId.Text = Session("AcademicYearId")
            txtTermId.Text = Session("TermId")
            txtTerm.Text = Session("Term")
            ''Added by saraswathi lakshmanan on may 18 2009
            ''To show the fee level
            If .FeeLevelId <> 0 Then
                rdnfeelevel.SelectedValue = .FeeLevelId
            Else
                rdnfeelevel.SelectedIndex = -1
            End If


        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim saf As New StudentsAccountsFacade

        'Recall the preserved selected value
        If FeeTypeInteger > 0 Then
            rdnfeelevel.SelectedValue = FeeTypeInteger
        End If

        'If DateDiff(DateInterval.Day, CDate(txtTransDate.Text), CDate(Date.Now)) < 1 Then
        'End If

        'Troy: 9/4/2013 Commented out the following lines as the user should be able to post fees into the future.
        'If DateDiff(DateInterval.Day, CDate(txtTransDate.SelectedDate), CDate(Date.Now)) < 0 Then
        '    DisplayErrorMessage("The transaction date should not be greater than today's date")
        '    Exit Sub
        'End If

        If Not Page.IsValid Then
            Exit Sub
        End If

        'Amount must be between 0.01 and 1000000 for TransType of Charge.
        'Amount must be between -1000000 and 1000000 for TransType of Adjustment
        Dim valMsg = ValidateAmount()

        If valMsg <> "" Then
            DisplayErrorMessage(valMsg)
            Exit Sub
        End If


        '   update PostCharge Info 
        Dim postChargeInfo As PostChargeInfo = BuildPostChargeInfo(txtPostChargeId.Text)
        Dim boolDup As Boolean = saf.CheckTransDuplicateCharge(postChargeInfo)
        If boolDup Then

            'ViewState("DisbArrList") = DisbArrList
            'ViewState("arrApplyPmt") = arrApplyPmt
            RadWindowManager1.RadConfirm("Save Record?", "confirmCallBackFn", 300, 100, Nothing, "Duplicate Record Already Exists")
        Else
            Dim result As String = saf.UpdatePostChargeInfo(postChargeInfo, AdvantageSession.UserState.UserName)
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   If there are no errors bind an empty entity and init buttons
                If Not CommonWebUtilities.IsValidGuid(ViewState("TransactionId")) Then

                    '   bind an empty new PostChargeInfo
                    BindSavePostChargeData(New PostChargeInfo)

                    '   initialize buttons
                    'InitButtonsForLoad()
                Else
                    '   bind PostChargeInfo in session
                    With New StudentsAccountsFacade
                        BindSavePostChargeData(.GetPostChargeInfo(ViewState("TransactionId")))
                    End With
                    '   initialize buttons for edit
                    'InitButtonsForEdit()
                End If

            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '        Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtPostChargeId.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtPostChargeId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End If


    End Sub
    Private Function BuildPostChargeInfo(ByVal postChargeId As String) As PostChargeInfo

        '   instantiate class
        Dim PostChargeInfo As New PostChargeInfo
        Dim saf As New StudentsAccountsFacade
        Dim adjustmentamount As Integer = 1
        With PostChargeInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get IsPosted
            .IsPosted = chkIsPosted.Checked

            '   get PostChargeId
            .PostChargeId = txtPostChargeId.Text

            '   get StuEnrollId
            .StuEnrollId = txtStuEnrollmentId.Text

            '   get CampusId
            .CampusId = campusId

            '   get TransCodeId
            .TransCodeId = ddlTransCodeId.SelectedValue

            '   get PostCharge Description
            .PostChargeDescription = txtTransDescrip.Text.Trim

            '   get DepositDate
            '.PostChargeDate = Date.Parse(txtTransDate.Text)
            .PostChargeDate = Date.Parse(txtTransDate.SelectedDate)

            If Not ViewState("dsTransTypes") Is Nothing Then
                Dim dr() As DataRow
                Dim dstranstypes As DataSet = ViewState("dsTransTypes")
                dr = dstranstypes.Tables(0).Select("TransCodeId='" + ddlTransCodeId.SelectedValue + "'")
                If dr.Length > 0 Then
                    ''15 is credit adjustment
                    If dr(0)("SysTransCodeID") = 15 Then
                        adjustmentamount = -1
                    End If

                End If

            End If


            '   get Amount
            .Amount = Decimal.Parse(txtTransAmount.Text) * adjustmentamount

            '   get Reference
            .Reference = txtTransReference.Text.Trim

            '   get Academic Year
            .AcademicYearId = txtAcademicYearId.Text

            '   get Term
            .TermId = txtTermId.Text

            '   get Transaction Type
            .TransTypeId = ddlTransTypeId.SelectedIndex
33:
            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

            '   Is Reversal
            .IsReversal = chkIsReversal.Checked

            '   Original transactionId
            If chkIsReversal.Checked Then .OriginalTransactionId = txtOriginalTransactionId.Text

            ' ''   FeeLevelId
            ''.FeeLevelId = saf.GetFeeLevelId(txtStuEnrollmentId.Text, ddlTransCodeId.SelectedValue)
            ''Fee level id is calculated based on the radio button with options Term, program Version and Course
            ''Modified by saraswathi on may 18 2009          
            rdnfeelevel.Enabled = True
            .FeeLevelId = rdnfeelevel.SelectedValue
            'rdnfeelevel.Enabled = False


            Session("StuEnrollmentId") = txtStuEnrollmentId.Text
            Session("AcademicYearId") = txtAcademicYearId.Text
            Session("TermId") = txtTermId.Text
            Session("TransCode") = ddlTransCodeId.SelectedValue
            Session("TransType") = ddlTransTypeId.SelectedIndex
        End With

        '   return data
        Return PostChargeInfo

    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        '   reset previous state
        SetControls(True)

        '   bind an empty new PostChargeInfo
        BindPostChargeData(New PostChargeInfo)


        Dim AdvCtrl As AdvControls.IAdvControl = CType(StudSearch1, AdvControls.IAdvControl)
        AdvCtrl.Clear()

        '   set session variable to nothing
        Session("ChargeTransactionId") = Nothing

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, ResourceId, moduleid)

    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtPostChargeId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim saf As New StudentsAccountsFacade

            '   update PostChargeInfo 
            Dim result As String = saf.DeletePostChargeInfo(txtPostChargeId.Text, Date.Parse(txtModDate.Text))
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else
                '   bind an empty new PostChargeInfo
                BindPostChargeData(New PostChargeInfo)

                '   initialize buttons
                'InitButtonsForLoad()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtPostChargeId.Text)
                SDFControl.GenerateControlsNew(pnlSDF, ResourceId, moduleid)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If

        End If
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String, _
                               ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String, _
                               ByVal hourtype As String)


        txtStuEnrollmentId.Text = enrollid
        Session("StudentName") = fullname
        txtTermId.Text = termid
        txtTerm.Text = termdescrip
        txtAcademicYearId.Text = academicyearid
        'txtAcademicYear.Text = academicyeardescrip

        Session("StuEnrollment") = fullname
        Session("StuEnrollmentId") = txtStuEnrollmentId.Text
        Session("AcademicYearId") = txtAcademicYearId.Text
        Session("TermId") = txtTermId.Text
        'Session("StudentName") = txtStudentName.Text
        'Session("TermId") = txtTermId.Text
        Session("Term") = txtTerm.Text
        'Session("AcademicYearID") = txtAcademicYearId.Text
        'Session("AcademicYear") = txtAcademicYear.Text
    End Sub
    Private Sub SetControls(ByVal val As Boolean)
        'txtStudentName.Enabled = val
        'lbtSearch.Visible = val
        'Img2.Visible = val
        txtStuEnrollment.Enabled = val
        ddlTransCodeId.Enabled = val
        TransCodeCompareValidator.Enabled = val
        txtTransDescrip.Enabled = val
        txtTransDate.Enabled = val
        drvTransactionDate.Enabled = val
        txtTransAmount.Enabled = val
        'AmountRangeValidator.Enabled = val
        txtTransReference.Enabled = val
        txtAcademicYear.Enabled = val
        txtTerm.Enabled = val
        ddlTransTypeId.Enabled = val
        ''Added by saraswathi lakshmanan on may 18 2009
        rdnfeelevel.Enabled = val
        'rdnfeelevel.ClearSelection()
    End Sub

    Protected Sub ddlTransCodeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTransCodeId.SelectedIndexChanged
        Dim dsDefaultCharges As DataSet
        Dim defperiod As Integer
        Dim Override As Boolean
        Dim facade As New StudentsAccountsFacade
        dsDefaultCharges = facade.GetDefaultChargesForTransCodeandStudent(txtStuEnrollmentId.Text, ddlTransCodeId.SelectedValue)
        rdnfeelevel.Enabled = True
        If dsDefaultCharges.Tables.Count > 0 Then
            If dsDefaultCharges.Tables(0).Rows.Count > 0 Then
                defperiod = dsDefaultCharges.Tables(0).Rows(0)("FeeLevelID")
                Override = dsDefaultCharges.Tables(0).Rows(0)("PeriodCanChange")

                rdnfeelevel.SelectedValue = defperiod
                If Override = False Then
                    rdnfeelevel.Enabled = False
                Else
                    rdnfeelevel.Enabled = True
                End If
            Else
                rdnfeelevel.SelectedIndex = -1
                rdnfeelevel.Enabled = True

            End If
        Else
            rdnfeelevel.SelectedIndex = -1
            rdnfeelevel.Enabled = True
        End If

        ''Added by Saraswathi lakshmanan on may 24 2010
        ''To fix mantis case: 19075

        txtStuEnrollment.Text = Session("StuEnrollment")
        txtAcademicYear.Text = Session("AcademicYear")
        txtTerm.Text = Session("Term")
        Session("TransCode") = ""
    End Sub

    'Protected Sub ddlTransTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTransTypeId.SelectedIndexChanged
    '    ''0 Charge
    '    ''1- Adjustment
    '    If ddlTransTypeId.SelectedValue = 0 Then

    '        BuildTransCodesDDL()
    '    ElseIf ddlTransTypeId.SelectedValue = 1 Then
    '        BuildTransCodeadjustments()
    '    End If

    '    txtStuEnrollment.Text = Session("StuEnrollment")
    '    txtAcademicYear.Text = Session("AcademicYear")
    '    txtTerm.Text = Session("Term")
    '    Session("TransType") = ""
    'End Sub

    Protected Sub Hdn_Click(sender As Object, e As EventArgs)
        Dim saf As New StudentsAccountsFacade
        Dim postChargeInfo As PostChargeInfo = BuildPostChargeInfo(txtPostChargeId.Text)

        Dim result As String = saf.UpdatePostChargeInfo(postChargeInfo, AdvantageSession.UserState.UserName)
        If result <> "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   If there are no errors bind an empty entity and init buttons
            If Not CommonWebUtilities.IsValidGuid(ViewState("TransactionId")) Then

                '   bind an empty new PostChargeInfo
                BindSavePostChargeData(New PostChargeInfo)

                '   initialize buttons
                'InitButtonsForLoad()
            Else
                '   bind PostChargeInfo in session
                With New StudentsAccountsFacade
                    BindSavePostChargeData(.GetPostChargeInfo(ViewState("TransactionId")))
                End With
                '   initialize buttons for edit
                'InitButtonsForEdit()
            End If

        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControl As New SDFComponent
        Try
            SDFControl.DeleteSDFValue(txtPostChargeId.Text)
            SDFID = SDFControl.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControl.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                SDFControl.InsertValues(txtPostChargeId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

    End Sub

    Private Function ValidateAmount() As String
        Dim msg As String = ""

        If ddlTransTypeId.SelectedIndex = 0 Then
            If CDec(txtTransAmount.Text) >= 0.01 And CDec(txtTransAmount.Text) <= 1000000 Then
                msg = ""
            Else
                msg = "Charge amount must be between 0.01 and 1000000."
            End If
        Else
            If CDec(txtTransAmount.Text) >= -1000000 And CDec(txtTransAmount.Text) <= 1000000 Then
                msg = ""
            Else
                msg = "Adjustment amount must be between -1000000 and 1000000."
            End If
        End If

        Return msg
    End Function
End Class