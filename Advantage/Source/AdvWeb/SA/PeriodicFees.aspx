
<%@ Reference Page="~/AR/Term.aspx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeriodicFees" CodeFile="PeriodicFees.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Term Fees Setup</title>
<%--    <link rel="stylesheet" type="text/css" href="../css/localhost.css" />--%>
    <link href="../css/systememail.css" type="text/css" rel="stylesheet" />
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <script language="javascript" src="../AuditHist.js" type="text/javascript"></script>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>

    
</head>
<body runat="server" leftmargin="0" topmargin="0" id="Body1" name="Body1">
    <form id="Form1" method="post" runat="server">
    <!-- beging header -->
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="toppopupheader">
                <img src="../images/advantage_term_fees.jpg">
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
    </table>
    <!-- end header -->
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" />
    <asp:ScriptManager ID="scriptmanager1" runat="server">
    </asp:ScriptManager>
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" height="100%" border="0">
        <tr>
            <td class="listframe">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table2">
                    <tr>
                        <td class="listframetop2">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="15%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server"><b class="label">Show</b></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftcoursefees">
                                <asp:DataList ID="dlstPeriodicFees" runat="server">
                                    <SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
                                    <HeaderTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="threecolumndatalist" style="width: 100%;">
                                                    <asp:Label ID="lblTransCode" runat="server" CssClass="labelbold">Transaction Code</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <SelectedItemTemplate>
                                    </SelectedItemTemplate>
                                    <ItemStyle CssClass="NonSelectedItem"></ItemStyle>
                                    <ItemTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="threecolumndatalist" style="width: 100%; padding: 0 10px 0 10px">
                                                    <asp:ImageButton ID="imgInActive" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
                                                        ImageUrl="../images/Inactive.gif" CommandArgument='<%# Container.DataItem("PeriodicFeeId") %>'
                                                        CausesValidation="False" />
                                                    <asp:ImageButton ID="imgActive" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>'
                                                        ImageUrl="../images/Active.gif" CommandArgument='<%# Container.DataItem("PeriodicFeeId") %>'
                                                        CausesValidation="False" />
                                                    <asp:LinkButton ID="Linkbutton1" CssClass="NonSelectedItem" CausesValidation="False" runat="server"
                                                        Text='<%# Container.DataItem("TransCodeDescrip") %>' CommandArgument='<%# Container.DataItem("PeriodicFeeId") %>'>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList></div>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="leftside">
                <table cellspacing="0" cellpadding="0" width="9" border="0" id="Table3">
                </table>
            </td>
            <td class="detailsframetop">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                            <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False">
                            </asp:Button>
                        </td>>
                    </tr>
                </table>
                <table width="95%" border="0" cellpadding="0" cellspacing="0" id="Table5">
                    <tr>
                        <td class="detailsframeff">
                            <div>
                                <table cellspacing="2" cellpadding="2" width="100%" align="center">
                                    <asp:TextBox ID="txtPeriodicFeeId" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                                        ID="txtTermId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB"
                                            runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtModUser" runat="server"
                                                Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate" runat="server"
                                                    Visible="False">ModDate</asp:TextBox>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblTermId" runat="server" CssClass="label">Term</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcelldate">
                                            <asp:Label ID="lblTerm" runat="server" CssClass="label">Term</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblStatusId" CssClass="label" runat="server" Visible="false">Status</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlistff" BackColor="#FFFF99"
                                                Visible="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblTransCodeId" runat="server" CssClass="label">Transaction Code<font color="red">*</font></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlTransCodeId" runat="server" CssClass="dropdownlistff" 
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="cvTransCode" runat="server" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000"
                                                ErrorMessage="Must Select a Transaction Code" Display="None" ControlToValidate="ddlTransCodeId">Must Select a Transaction Code</asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <span class="label">Apply this amount</span>
                                        </td>
                                        <td class="twocolumncontentcelldate">
                                            <asp:Panel ID="pnlRate" runat="server" CssClass="panel" BorderWidth="1">
                                                <table id="Table6" cellspacing="1" cellpadding="6" width="100%" border="0">
                                                    <tr>
                                                        <td nowrap colspan="3">
                                                            <asp:RadioButton ID="rbtFlatAmount" runat="server" CssClass="label" Text="Flat Amount"
                                                                Checked="True" GroupName="RateSchedule"></asp:RadioButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img alt="" src="../images/1x1.gif" width="25">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblAmount" runat="server" CssClass="label">Amount</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="textbox" BackColor="White"
                                                                Width="100px" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblUnit" runat="server" CssClass="label">Unit</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlUnitId" runat="server" CssClass="dropdownlistff">
                                                                <asp:ListItem Value="0">Credit Hour</asp:ListItem>
                                                                <asp:ListItem Value="2">Term</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblTuitionCategoryId" runat="server" CssClass="label">Tuition Category</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTuitionCategoryId" runat="server" CssClass="dropdownlistff">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <hr class="HR" width="100%" size="1" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap colspan="3">
                                                            <asp:RadioButton ID="rbtRateSchedule" runat="server" CssClass="label" Text="Rate Schedule"
                                                                GroupName="RateSchedule"></asp:RadioButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblRateScheduleId" runat="server" CssClass="label">Rate Schedule</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlRateScheduleId" runat="server" CssClass="dropdownlistff">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <span class="label">Apply to these programs</span>
                                        </td>
                                        <td class="twocolumncontentcelldate">
                                            <asp:Panel ID="PanelApplyTo" runat="server" CssClass="panel" BorderWidth="1">
                                                <table cellspacing="1" cellpadding="6" width="100%" align="center">
                                                    <tr>
                                                        <td rowspan="3">
                                                            <asp:RadioButtonList ID="rblApplyTo" runat="server" CssClass="label" AutoPostBack="true">
                                                                <asp:ListItem Value="0" Selected="True">All Programs</asp:ListItem>
                                                                <asp:ListItem Value="1">Program Type</asp:ListItem>
                                                                <asp:ListItem Value="2">Program Version</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:DropDownList ID="ddlProgTypeId" runat="server" Width="150" CssClass="dropdownlistff">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:DropDownList ID="ddlPrgVerId" runat="server" Width="150" CssClass="dropdownlistff"
                                                                AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <span class="label">Apply to this Start Date</span>
                                        </td>
                                        <td class="twocolumncontentcelldate">
                                            <asp:Panel ID="Panel2" runat="server" CssClass="panel" BorderWidth="1">
                                                <table cellspacing="1" cellpadding="6" width="100%" align="center">
                                                    <tr>
                                                        <td align="left">
                                                            <%--<asp:TextBox ID="tbTermStartDate" runat="server" Width="122" CssClass="TextBox"></asp:TextBox>&nbsp;
                                                            <a id="A1" href="javascript:OpenCalendar('Form1', 'tbTermStartDate', false, 2006)"
                                                                runat="server">
                                                                <img id="Img1" alt="Graphic Calendar" src="../UserControls/Calendar/PopUpCalendar.gif"
                                                                    border="0" align="absMiddle" runat="server"></a>--%>
                                                            <telerik:RadDatePicker ID="tbTermStartDate" MinDate="1/1/1945" runat="server">
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lApplyToTheseFutureTerms" CssClass="label" runat="server" Visible="false">Apply to these Future Terms</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcelldate">
                                            <asp:Panel ID="pFutureTerms" runat="server" CssClass="panel" BorderWidth="1" Visible="false">
                                                <table cellspacing="1" cellpadding="6" width="100%" align="center">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:CheckBoxList ID="cblFutureTerms" runat="server" CssClass="label" RepeatDirection="Horizontal"
                                                                RepeatColumns="4">
                                                            </asp:CheckBoxList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
   <%-- <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
    </form>
</body>
</html>
