Imports System.Data

Partial Class StudentPPDetails
    Inherits System.Web.UI.Page
    Private firstTime As Boolean = True
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("LateFeesDS") Is Nothing Then
            Response.Redirect(FormsAuthentication.LoginUrl)
        End If

        BindGridView(Session("StuEnrollId"))
    End Sub
    Private Sub BindGridView(ByVal stuEnrollId As String)
        Dim allDataset As DataSet = Session("LateFeesDS")
        dgrdPostLateFees.DataSource = New DataView(allDataset.Tables(0), "StuEnrollId='" + stuEnrollId + "'", Nothing, DataViewRowState.CurrentRows)
        dgrdPostLateFees.DataBind()
    End Sub

    Protected Sub dgrdPostLateFees_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdPostLateFees.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                If firstTime Then
                    lblUserName.Text = e.Item.DataItem("StudentName")
                    lblUserIdentifier.Text = e.Item.DataItem("StudentIdentifier")
                End If
        End Select
    End Sub
End Class
