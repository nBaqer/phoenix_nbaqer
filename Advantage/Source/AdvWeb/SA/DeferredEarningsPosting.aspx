﻿<%@ Page Title="Deferred Earnings Posting" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="DeferredEarningsPosting.aspx.vb" Inherits="DeferredEarningsPosting" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="400" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <table cellspacing="0" cellpadding="2" width="100%" border="0">
                            <tr>
                                <br />
                                <td class="contentcellblue">
                                    <asp:Label ID="lblReportDate" runat="server" CssClass="label">Report Date</asp:Label>
                                </td>
                                <td class="contentcell4blue">
                                    <asp:DropDownList ID="ddlReportDate" Width="200px" runat="server" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">
                                    <asp:Label ID="lblReportDate0" runat="server" CssClass="label" >Earliest Transaction date</asp:Label>
                                </td>
                                <td class="contentcell4blue">

                                    <telerik:RadDatePicker ID="txtTransDate" MinDate="1/1/1945" runat="server" Width="200px" >
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcellblue">&nbsp;
                                </td>
                                <td class="contentcell4blue">
                                    <asp:CheckBox ID="cbxShowZeroAmounts" runat="server" CssClass="checkbox" Text="Show Zero Amounts"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell4blue" style="text-align: right; padding-right: 15px;" colspan="2" >
                                    <asp:Button ID="btnBuildList" runat="server"  Text="Build List"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftdeferred">
                                <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
                                    <tr>
                                        <td class="threecolumndatalist" >
                                            <asp:Label ID="lblTransCodeHeader" runat="server" CssClass="labelbold">Transaction</asp:Label>
                                        </td>
                                        <td class="threecolumndatalist">
                                            <asp:Label ID="lblToAccrueAmountHeader" runat="server" CssClass="labelbold">To Earn</asp:Label>
                                        </td>

                                    </tr>
                                </table>
                                <asp:DataList ID="dlstDefRevenueByTransCode" runat="server" Width="100%" ShowFooter="False"
                                    GridLines="Both">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                    <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                    <ItemTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
                                            <tr>
                                                <td  style="width: 70%">
                                                    <asp:LinkButton ID="lnkTransCode" runat="server" CssClass="itemstyle" Text='<%# Container.DataItem("TransCodeDescrip") %>'
                                                        CommandArgument='<%# Container.DataItem("TransCodeId") %>'>
                                                    </asp:LinkButton>
                                                </td>
                                                <td  style="width: 30%">
                                                    <asp:Label ID="lblToAccrueAmount" runat="server" CssClass="itemstyle" Text='<%# DataBinder.Eval(Container, "DataItem.DeferredRevenueAmount", "{0:##,###,###.00}") %>'>To Earn Amount</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                                <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
                                    <tr>
                                        <td class="spacertables"></td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnPostDeferredRevenues" runat="server" Text="Post Deferred Revenues"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->

                            <table width="100%" cellspacing="0" cellpadding="0" align="center" class="contenttable">
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdStudentRevenueLedger" runat="server" ShowFooter="True" Width="97%"
                                            AutoGenerateColumns="False" CellPadding="0" BorderStyle="Solid" BorderWidth="1px"
                                            BorderColor="#E0E0E0">
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="StudentName" HeaderText="Student Name">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="StudentIdentifier" ReadOnly="True" HeaderText="Student Id."></asp:BoundColumn>
                                                <asp:BoundColumn DataField="DefRevenueDate" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn Visible="False" HeaderText="Source">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSource" runat="server" Text='<%# ConvertSourceToVerbose(Container.DataItem("Source")) %>'
                                                            CssClass="label">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn Visible="False" HeaderText="Accrued">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="label1" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Accrued", "{0:##,###,###.00}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterAccruedTotal" runat="server" CssClass="label">Accrued Total</asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Amount to Earn">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ToAccrue", "{0:##,###,###.00}") %>'
                                                            CssClass="label">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterToAccrueTotal" runat="server" CssClass="labelbold">To Earn Total</asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn Visible="False" HeaderText="Total">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total", "{0:##,###,###.00}") %>'
                                                            CssClass="label">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterTotalTotal" runat="server" CssClass="label">Amount to Earn Total</asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="DgExceptions" runat="server" ShowFooter="True" Width="97%" AutoGenerateColumns="False"
                                            CellPadding="0" BorderStyle="Solid" BorderWidth="1px" BorderColor="#E0E0E0">
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="StudentName" HeaderText="Student Name">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="TransCode" HeaderText="Trans Code"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="TransAmount" HeaderText="Trans Amount"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="FeeLevelID" HeaderText="Fee Level ID">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="TermStart" ReadOnly="True" HeaderText="Term Start Date"
                                                    DataFormatString="{0:d}">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="TermEnd" ReadOnly="True" HeaderText="Term End Date" DataFormatString="{0:d}">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Exception" HeaderText="Exception"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

