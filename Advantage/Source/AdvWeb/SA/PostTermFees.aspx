﻿<%@ Page Title="Post Term Fees" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostTermFees.aspx.vb" Inherits="PostTermFees" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <table cellspacing="0" cellpadding="5px" width="100%">
                            <tr>
                                <td>
                                    <div style="padding: 0px 0px 10px 0px;">
                                        <asp:Label ID="lblTerm" runat="server" CssClass="label">Terms</asp:Label>
                                    </div>
                                    <telerik:RadComboBox ID="ddlTermsId" Width="98%" BackColor="White" ForeColor="Black" runat="server">
                                    </telerik:RadComboBox>
                                    <asp:TextBox ID="txtTermsId" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rblFees" runat="server" CssClass="radiobuttonlist" Width="134px"
                                        AutoPostBack="True">
                                        <asp:ListItem Value="Program Version" Selected="True">Program Version</asp:ListItem>
                                        <asp:ListItem Value="Course">Course</asp:ListItem>
                                        <asp:ListItem Value="Term">Term</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadButton ID="btnBuildList" runat="server" CausesValidation="False"
                                        Text="Show Fees">
                                    </telerik:RadButton>
                                </td>
                            </tr>
                        </table>


                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfltr4rows">
                        </div>

                    </td>
                </tr>
            </table>




        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="false"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false"></asp:Button>
                    </td>
                </tr>
            </table>

              <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%;border:none">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin table content-->
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="twocolumnlabelcell">
                                        <asp:Label ID="lblTotalStudentsHdr" runat="server" CssClass="labelbold">Total Students:</asp:Label></td>
                                    <td class="twocolumncontentcell" style="text-align: left">
                                        <asp:Label ID="lblTotalStudents" runat="server" CssClass="Label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell">
                                        <asp:Label ID="lblTotalAmountHdr" runat="server" CssClass="labelbold">Total Amount:</asp:Label></td>
                                    <td class="twocolumncontentcell" style="padding-bottom: 10px; text-align: left">
                                        <asp:Label ID="lblTotalAmount" runat="server" CssClass="Label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell">
                                        <asp:Label ID="Label2" runat="server" CssClass="labelbold">Post Fees Date:</asp:Label></td>
                                    <td class="twocolumncontentcell" style="padding-bottom: 10px">

                                        <telerik:RadDatePicker ID="txtPostFeesDate" MinDate="1/1/1945" runat="server">
                                        </telerik:RadDatePicker>
                                        <asp:RangeValidator ID="drvPostFeesDate" runat="server" MinimumValue="2001/01/01"
                                            MaximumValue="2049/12/31" Type="Date" Display="None" ErrorMessage="Date is invalid or outside allowed range"
                                            ControlToValidate="txtPostFeesDate">Date is invalid or outside allowed range</asp:RangeValidator>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="twocolumnlabelcell">
                                        <telerik:RadButton ID="btnPostFees" runat="server" CssClass="button"  Text="Post Fees"
                                            Enabled="False">
                                        </telerik:RadButton>
                                        <asp:Button ID="btnDeletePostedFees" runat="server" Text="Delete Posted Fees (Only for Testing)"
                                            Enabled="False" Visible="False"></asp:Button></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadButton ID="btnExportToExcell" runat="server" CssClass="button"  Text="Export to Excel" Enabled="False"></telerik:RadButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table id="tblLegend" runat="server" visible="true">
                                <tr align="center">
                                    <td style="text-align: right" class="legendlabel">Legend :</td>
                                    <td style="height: 5px; vertical-align: middle; background-color: White; border: solid 1px #ebebeb; width: 150px">
                                        <span class="legendlabel">Unposted Fees</span></td>
                                    <td style="height: 5px; vertical-align: middle; background-color: LightGrey; border: solid 1px #ebebeb; width: 150px">
                                        <span class="legendlabel">Posted Fees</span></td>

                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <asp:TextBox ID="txtBatchPaymentId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox><tr>
                                    <td align="left" colspan="4">
                                        <asp:DataGrid ID="dgrdPostTermFees" Width="68%"
                                            BorderStyle="Solid" AutoGenerateColumns="False"
                                            HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" runat="server" BorderColor="#E0E0E0" BorderWidth="1px">
                                            <EditItemStyle Wrap="False"></EditItemStyle>
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="StudentName" ReadOnly="True" HeaderText="Student Name" HeaderStyle-Width="25%" HeaderStyle-CssClass="k-grid-header"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="StudentIdentifier" ReadOnly="True" HeaderText="Student ID" HeaderStyle-Width="10%" HeaderStyle-CssClass="k-grid-header"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="FeeDescription" HeaderText="Description" HeaderStyle-Width="25%" HeaderStyle-CssClass="k-grid-header"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="FeeDetailDescription" HeaderText="Enrollment" HeaderStyle-Width="20%" HeaderStyle-CssClass="k-grid-header"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="StuEnrollId" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Transcodeid" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="TransDescrip" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="TransCodeDescrip" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NumberOfTransactionsPosted" HeaderText="Posted" HeaderStyle-Width="15%" HeaderStyle-CssClass="k-grid-header" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="FeeId" Visible="false"></asp:BoundColumn>

                                                <asp:TemplateColumn HeaderText="Amount">
                                                    <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" CssClass="labelamount" Text='<%# DataBinder.Eval(Container, "DataItem.TransAmount", "{0:c}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterAmount" runat="server" CssClass="labelamount"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Do Not Post" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRemove" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="TermId" Visible="false"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                            </table>
                            <!--end table content-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

