<%@ Page Title="Student Invoices" Language="vb" AutoEventWireup="false" Inherits="StudentDebitInvoice"
    MasterPageFile="~/NewSite.master" CodeFile="StudentDebitInvoice.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">
            <table class="SAtable" id="Table5" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <!-- begin rightcolumn -->
                    <td class="detailsframetop">
                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False">
                                    </asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" Enabled="False"
                                        CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" Text="Delete"
                                            CssClass="delete" Enabled="False" CausesValidation="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                    </td>
                </tr>
                <tr>
                    <td class="detailsframe">
                        <!-- begin table content-->
                        <div class="scrollsingleframe">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="70%" align="center">
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 43%; text-align: right">
                                        <asp:Label ID="Label3" runat="server" CssClass="label">Enter cut-off date for the projection<span style="COLOR: red">*</span></asp:Label>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 17%; text-align: left">
                                        <telerik:RadDatePicker ID="txtDate" MinDate="1/1/1945" runat="server" AutoPostBack="true" OnSelectedDateChanged="txtDate_SelectedDateChanged">                                                                           
                                        </telerik:RadDatePicker>
                                        <%--   <asp:textbox id="txtDate" runat="server" CssClass="DateTextBox" BackColor="#ffff99"></asp:textbox>
                                        <IMG height="1" src="../images/1x1.gif" width="10">
										<A id="A2" onclick="javascript:OpenCalendar('Form1', 'txtDate', false, 1945)" runat="server">
											<IMG id="Img3" alt="Graphic Calendar" src="../UserControls/Calendar/PopUpCalendar.gif"
												align="absMiddle" border="0" runat="server"></A>--%>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 15%; text-align: left">
                                        <asp:RequiredFieldValidator ID="rfvDate" runat="server" Display="None" ControlToValidate="txtDate"
                                            ErrorMessage="Cut-Off date is required">Cut-Off date is required</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvDate" runat="server" Display="None" ControlToValidate="txtDate"
                                            Operator="DataTypeCheck" Type="Date" ErrorMessage="Invalid date"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 43%; text-align: right">
                                        <asp:Label ID="Label4" runat="Server" CssClass="Label">Enrollment Status</asp:Label>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 17%; text-align: left">
                                        <asp:DropDownList ID="ddlStatusId" runat="Server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 15%; text-align: left">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 43%; text-align: right">
                                        <asp:Label ID="Label5" runat="Server" CssClass="label">Program Version</asp:Label>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 17%; text-align: left">
                                        <asp:DropDownList ID="ddlProgram" runat="Server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 15%; text-align: left">
                                    </td>
                                    <td style="width: 40%; text-align: left">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 43%; text-align: right">
                                        <asp:Label ID="Label6" runat="server" CssClass="label">Expected Date</asp:Label>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 17%; text-align: left">
                                        <telerik:RadDatePicker ID="txtExpectDate" MinDate="1/1/1945" runat="server">
                                        </telerik:RadDatePicker>
                                        <%--  <asp:TextBox ID="txtExpectDate" runat="server" BackColor="White" CssClass="DateTextBox"></asp:TextBox>
                                        <IMG height="1" src="../images/1x1.gif" width="10">
                                        	<A id="A3" onclick="javascript:OpenCalendar('Form1', 'txtExpectDate', false, 1945)" runat="server">
                                        <IMG id="Img4" alt="Graphic Calendar" src="../UserControls/Calendar/PopUpCalendar.gif"
												align="absMiddle" border="0" runat="server"></A>--%>
                                    </td>
                                    <td class="twocolumncontentcell" style="width: 15%; text-align: left">
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="txtExpectDate"
                                            Operator="DataTypeCheck" Type="Date" ErrorMessage="Invalid date"></asp:CompareValidator>
                                    </td>
                                    <td style="width: 40%; text-align: left">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 43%">
                                    </td>
                                    <td class="twocolumncontentcell" style="padding-left: 2em; text-align: left; width: 17%;">
                                        <asp:Button ID="btnGetList" runat="server" CssClass="button" Text="Get List" Width="130px"
                                            CausesValidation="True"></asp:Button>
                                    </td>
                                    <td class="twocolumncontentcell" style="padding-left: 2em; text-align: left">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell" style="width: 43%">
                                    </td>
                                    <td class="twocolumncontentcell" style="padding-left: 2em; text-align: left; width: 17%;">
                                         <asp:Button ID="btnGenerateInv" runat="server" CssClass="button" Text="Generate Invoice"
                                            Width="130px" Visible="False"></asp:Button>
                                    </td>
                                    <td class="twocolumncontentcell" style="padding-left: 2em; text-align: left">
                                    </td>
                                </tr>
                            </table>
                            <br>                           
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdStudentDetails" runat="server" BorderColor="#E0E0E0" AllowSorting="True"
                                            AutoGenerateColumns="False" BorderStyle="Solid" HorizontalAlign="Center" Width="100%">
                                            <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            <HeaderStyle CssClass="datagridheaderstyle" HorizontalAlign="Center"></HeaderStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Student">
                                                    <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "javascript:__doPostBack(""lbtStudentName"",""" + Ctype(Container.DataItem("StudentId"),Guid).ToString + """)" %>'>
															<%# Container.DataItem("Student") %>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="SSN" HeaderText="SSN" SortExpression="SSN" ReadOnly="True">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="PrgVerDescrip" HeaderText="Program Version" SortExpression="PrgVerDescrip"
                                                    ReadOnly="True"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="StatusCodeDescrip" SortExpression="StatusCodeDescrip"
                                                    ReadOnly="True" HeaderText="Status"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:d}"
                                                    SortExpression="StartDate" ReadOnly="True">
                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="DateDetermined" HeaderText="Date Determined" DataFormatString="{0:d}"
                                                    SortExpression="DateDetermined" ReadOnly="True"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ExpGradDate" HeaderText="Revised Grad Date" DataFormatString="{0:d}"
                                                    SortExpression="ExpGradDate" ReadOnly="True"></asp:BoundColumn>
                                                <asp:BoundColumn Visible="True" DataField="Balance" HeaderText="Payment Plan Balance" DataFormatString="{0:c}"
                                                    SortExpression="Balance" ReadOnly="True"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderTemplate>
                                                        <input id="chkAllItems" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkCopy',
															document.forms[0].chkAllItems.checked)" />Check All
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkCopy" runat="server" Text='' />
                                                        <asp:TextBox ID="txtstuenrollid" runat="server" CssClass="ardatalistcontent" Visible="False"
                                                            Text='<%# Container.DataItem("stuenrollid") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <!--end table content-->
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
</asp:Content>
