<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudentPPDetails.aspx.vb"
    Inherits="StudentPPDetails" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Unpaid Payment Plan Charges</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../css/localhost.css" type="text/css" rel="stylesheet">
    <link href="../css/systememail.css" type="text/css" rel="stylesheet">
    <link href="../css/BulletedList.css" type="text/css" rel="stylesheet">
    <style>
        .pageheader
{
  background-color:#E9EDF2;
  background-image:url(images/header_bg.gif);
  color:#504C39;
  font-family:Verdana;
  font-size:11px;
  text-align:center;
  border-top:solid 1px #ebebeb;
  border-left:solid 1px #ebebeb;
  border-bottom:solid 1px #ebebeb;
  border-right:solid 1px #ebebeb;
  padding: 2px;
}
.topemail
{
	font-family: verdana;
	font-size: xx-small;
	color: #FFFFFF;
	background-color: #164D7F;
	text-align: left;
	padding: 6px;
	width: 100%;
	vertical-align: top;
}
h2, h3 {font-size: 11px}
h1 {font-size:13px}

    </style>
</head>
<body runat="server" id="Body1" name="Body1">
    <form id="Form1" method="post" runat="server">
		  
              <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table2">
            <tr>
                <td nowrap class="topemail" width="30%" align=left> 
                    <img src="../images/advantage_popup_header.jpg">
                    <font name="Arial" size="4">Unpaid Payment Plan Charges</font>    
                </td>
                <td class="topemail" width="40%" nowrap>&nbsp;</td>
                <td width="30%" class="topemail" nowrap>
                    <a class="close" onclick="top.close()" href="javascript:">X Close</a>
                </td>
            </tr>
        </table>
		<p></p>
        <!--begin right column-->
        <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%">
            <tbody>
                <tr>
                    <td class="detailsFrame">
                        <div class="scrollwholedocs">
                            <!-- begin content table-->
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="50%" border="0"
                                align="center">
                                <tr>
                                    <td class="twocolumnlabelcell" style="text-align: right; padding-right: 20px">
                                        <asp:Label ID="lblUser" runat="server" CssClass="Label">Student Name</asp:Label></td>
                                    <td class="2columnconetntcell" nowrap style="padding-right: 20px">
                                        <asp:Label ID="lblUserName" runat="server" CssClass="TextBox"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="twocolumnlabelcell" style="text-align: right; padding-right: 20px">
                                        <asp:Label ID="lblUserId" runat="server" CssClass="Label">Student Identifier</asp:Label></td>
                                    <td class="2columnconetntcell" nowrap style="padding-right: 20px">
                                        <asp:Label ID="lblUserIdentifier" runat="server" CssClass="TextBox"></asp:Label></td>
                                </tr>
                            </table>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td style="padding-top: 20px">
                                        <asp:DataGrid ID="dgrdPostLateFees" Width="100%" BorderStyle="Solid" AutoGenerateColumns="False"
                                            HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" runat="server" BorderColor="#E0E0E0"
                                            BorderWidth="1px">
                                            <EditItemStyle Wrap="False"></EditItemStyle>
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="DataGridHeaderStyle" Wrap="True"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="ExpectedDate" HeaderText="Due Date" DataFormatString="{0:d}"
                                                    ReadOnly="True">
                                                    <HeaderStyle CssClass="DataGridHeaderStyle" Width="10%" />
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="GraceDate" HeaderText="Grace Date" DataFormatString="{0:d}"
                                                    ReadOnly="True">
                                                    <HeaderStyle CssClass="DataGridHeaderStyle" Width="10%" />
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Balance" HeaderText="Amount Due" DataFormatString="{0:c}"
                                                    ReadOnly="True">
                                                    <HeaderStyle CssClass="DataGridHeaderStyle" Width="10%" />
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                </asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <!-- end content table -->
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="footer">
            Copyright FAME 2005 - 2019. All rights reserved.</div>
    </form>
</body>
</html>
