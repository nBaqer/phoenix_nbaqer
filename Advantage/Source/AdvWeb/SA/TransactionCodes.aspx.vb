Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Drawing

Partial Class TransactionCodes
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents txtTransCodesId As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents allDataset As System.Data.DataSet
    Protected WithEvents dataGridTable As System.Data.DataTable
    Protected WithEvents dataListTable As System.Data.DataTable
    Protected WithEvents dlstTransCode As System.Web.UI.WebControls.DataList
    Protected debits As Decimal = 0.0
    Protected credits As Decimal = 0.0
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim cmpCallReportingAgencies As New ReportingAgencies
    Protected intRptFldId As Integer
    Protected boolRptAgencyAppToSchool As Boolean
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            With New StudentsAccountsFacade
                allDataset = .GetTransCodesDS(fsGetStatusFilter())
            End With

            '   save it on the session
            Session("AllDataset") = allDataset

        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
        End If

        '   create Dataset and Tables
        CreateDatasetAndTables()

        '   the first time we have to bind an empty datagrid
        If Not IsPostBack Then
            PrepareForNewData()
        End If
        'Commented for the Defect DE5406
        'boolRptAgencyAppToSchool = AdvantageCommonValues.CheckIfReportingAgenciesExistForSchool
        'If boolRptAgencyAppToSchool = True Then
        '    pnlReportingAgenciesHeader.Visible = True
        '    intRptFldId = AdvantageCommonValues.TransactionCodes
        '    cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtTransCodeId.Text)
        'Else
        '    pnlReportingAgenciesHeader.Visible = False
        'End If

    End Sub
    Private Sub BuildDropDownLists()

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, True, True))

        'system Transaction Codes
        ddlList.Add(New AdvantageDDLDefinition(ddlSysTransCodeId, AdvantageDropDownListName.System_Transaction_Codes, campusId, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub

    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub

    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub dlstTransCodes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstTransCodes.ItemCommand

        '   this portion of code added to refresh data from the database
        '**********************************************************************************
        '   get allDataset from the DB
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        With New StudentsAccountsFacade
            allDataset = .GetTransCodesDS(fsGetStatusFilter())
        End With

        '   save it on the session
        Session("AllDataset") = allDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()
        '**********************************************************************************

        '   save TransCodeId in session
        Dim strId As String = dlstTransCodes.DataKeys(e.Item.ItemIndex).ToString()
        Session("TransCodeId") = strId

        '   get the row with the data to be displayed
        Dim row() As DataRow = dataListTable.Select("TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'")

        '   populate controls with row data
        txtTransCodeId.Text = CType(row(0)("TransCodeId"), Guid).ToString
        txtTransCodeCode.Text = row(0)("TransCodeCode")
        ddlStatusId.SelectedValue = CType(row(0)("StatusId"), Guid).ToString
        txtTransCodeDescrip.Text = row(0)("TransCodeDescrip")
        If (Not row(0)("CampGrpId") Is System.DBNull.Value) Then ddlCampGrpId.SelectedValue = CType(row(0)("CampGrpId"), Guid).ToString Else ddlCampGrpId.SelectedIndex = 0
        ddlBillTypeId.SelectedValue = CType(row(0)("BillTypeId"), Guid).ToString
        chkDefEarnings.Checked = row(0)("DefEarnings")
        chkIsInstCharge.Checked = row(0)("IsInstCharge")
        chkIs1098T.Checked = row(0)("Is1098T")
        If (Not row(0)("SysTransCodeId") Is System.DBNull.Value) Then ddlSysTransCodeId.SelectedValue = row(0)("SysTransCodeId") Else ddlSysTransCodeId.SelectedIndex = 0
        'DE8921
        If ddlSysTransCodeId.SelectedItem.Text = "Applicant Fee" Then
            ddlBillTypeId.SelectedValue = ddlBillTypeId.Items.FindByText("Applicant").Value
            'DE8921
            chkDefEarnings.Enabled = False
            chkDefEarnings.Checked = False
            chkIs1098T.Checked = False
            chkIs1098T.Enabled = False
        ElseIf ddlSysTransCodeId.SelectedItem.Text = "Tuition" Then
            chkDefEarnings.Checked = True
            chkDefEarnings.Enabled = False
            DRPanel.Visible = True
            chkIs1098T.Checked = True
            chkIs1098T.Enabled = False
            chkIsInstCharge.Checked = True
            chkIsInstCharge.Enabled = False
        Else
            chkDefEarnings.Enabled = True
            DRPanel.Visible = False
            chkIs1098T.Enabled = True
            chkIsInstCharge.Enabled = True
        End If

        '   Bind DataGrid
        dgrdTransCodeGLAccounts.EditItemIndex = -1
        BindDataGrid(dgrdTransCodeGLAccounts)

        '   Bind DRDataGrid
        dgrdTransCodeDRGLAccounts.EditItemIndex = -1
        BindDataGrid(dgrdTransCodeDRGLAccounts)

        'Commented for the Defect DE5406
        'cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtTransCodeId.Text)
        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstTransCodes, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        '   show footer
        dgrdTransCodeGLAccounts.ShowFooter = True

        CommonWebUtilities.RestoreItemValues(dlstTransCodes, CType(Session("TransCodeId"), String))
    End Sub
    Private Sub BindTransCodesData(ByVal TransCode As TransCodeInfo)
        With TransCode
            chkIsInDB.Checked = .IsInDB
            txtTransCodeId.Text = .TransCodeId
            txtTransCodeCode.Text = .Code
            If Not (TransCode.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = TransCode.StatusId
            txtTransCodeDescrip.Text = .Description
            If Not (TransCode.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = TransCode.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            If Not (TransCode.BillTypeId = Guid.Empty.ToString) Then ddlBillTypeId.SelectedValue = TransCode.BillTypeId Else ddlBillTypeId.SelectedIndex = 0
            If Not (TransCode.SysTransCodeId = 0) Then ddlSysTransCodeId.SelectedValue = TransCode.SysTransCodeId.ToString() Else ddlSysTransCodeId.SelectedIndex = 0
            chkDefEarnings.Checked = .DeferEarnings
            chkIsInstCharge.Checked = .IsInstCharge()
            chkIs1098T.Checked = .Is1098T
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        '   if any of the footer fields of the datagrid is not blank.. send an error message 
        If Not (AreFooterFieldsBlank(dgrdTransCodeGLAccounts) And AreFooterFieldsBlank(dgrdTransCodeDRGLAccounts)) Then
            '   Display Error Message
            DisplayErrorMessage("To use ""Save"" all fields in the GL Account Distribution footer must be blank.")
            Exit Sub
        End If

        '   if the percentage distribution of debits and credits is not in balance.. send an error message 
        If Not (IsDatagridBalanced(dgrdTransCodeGLAccounts) And IsDatagridBalanced(dgrdTransCodeDRGLAccounts)) Then
            '   Display Error Message
            DisplayErrorMessage("The percentage distribution of Debits and Credits is not in balance")
            Exit Sub
        End If

        '   if TransCodeId does not exist do an insert else do an update
        If Session("TransCodeId") Is Nothing Then
            '   do an insert
            BuildNewRowInDataList(Guid.NewGuid.ToString)
        Else
            '   do an update

            '   get the row with the data to be displayed
            Dim row() As DataRow = dataListTable.Select("TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'")

            '   update row data
            UpdateRowData(row(0))

        End If

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors then bind and set selected style to the datalist and
        '   initialize buttons 
        If Customvalidator1.IsValid Then

            '   bind datalist
            dlstTransCodes.DataSource = New DataView(dataListTable, Nothing, "TransCodeDescrip asc", DataViewRowState.CurrentRows)
            dlstTransCodes.DataBind()

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstTransCodes, Session("TransCodeId"), ViewState, Header1)

            '   initialize buttons
            InitButtonsForEdit()

        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked
        'Commented for the Defect DE5406
        'Dim arrReportingAgencyValue As ArrayList
        'Dim z As Integer
        ''Try
        'arrReportingAgencyValue = cmpCallReportingAgencies.GetAllValues(pnlReportingAgencies)
        'z = cmpCallReportingAgencies.InsertValues(txtTransCodeId.Text, arrReportingAgencyValue)
        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        ' End Try
        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CommonWebUtilities.RestoreItemValues(dlstTransCodes, CType(Session("TransCodeId"), String))
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click

        '   reset dataset to previous state. delete all changes
        allDataset.RejectChanges()
        PrepareForNewData()

        'Commented for the Defect DE5406
        'cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtTransCodeId.Text)

        '   reset Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstTransCodes, Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstTransCodes, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click

        '   get all rows to be deleted
        Dim z As Integer
        Dim rows() As DataRow = dataGridTable.Select("TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'")

        z = cmpCallReportingAgencies.InsertValues(CType(Session("TransCodeId"), String), Nothing)

        '   delete all rows from dataGrid table
        Dim i As Integer
        If rows.Length > 0 Then
            For i = 0 To rows.Length - 1
                rows(i).Delete()
            Next
        End If

        '   get the row to be deleted in TransCodes table
        Dim row() As DataRow = dataListTable.Select("TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'")

        '   delete row from the table
        row(0).Delete()

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   Prepare screen for new data
            PrepareForNewData()
            CommonWebUtilities.RestoreItemValues(dlstTransCodes, Guid.Empty.ToString)

        End If
        'Commented for the Defect DE5406
        'cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtTransCodeId.Text)



    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False

        'set Deferred Revenue panel
        DRPanel.Visible = chkDefEarnings.Checked
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        'set Deferred Revenue panel
        DRPanel.Visible = chkDefEarnings.Checked

    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub CreateDatasetAndTables()

        '   Get dataGrid Dataset from the session and create tables
        allDataset = Session("AllDataset")
        dataGridTable = allDataset.Tables("TransCodeGLAccounts")
        dataListTable = allDataset.Tables("TransCodes")

    End Sub

    Private Sub PrepareForNewData()

        '   bind TransCodes datalist
        dlstTransCodes.DataSource = dataListTable
        dlstTransCodes.DataBind()

        '   save TransCodeId in session
        Session("TransCodeId") = Nothing

        '   Bind DataGrid
        BindDataGrid(dgrdTransCodeGLAccounts)

        '   Bind DataGrid
        BindDataGrid(dgrdTransCodeDRGLAccounts)

        '   bind an empty new TransCodesInfo
        BindTransCodesData(New TransCodeInfo)

        '   initialize buttons
        InitButtonsForLoad()

        'DE8921
        chkDefEarnings.Enabled = True

    End Sub

    Private Sub SwapViewOrders(ByVal id1 As String, ByVal id2 As String)

        '   get the rows with each id
        Dim row1(), row2() As DataRow
        row1 = dataGridTable.Select("TransCodeGLAccountId=" + "'" + id1 + "'")
        row2 = dataGridTable.Select("TransCodeGLAccountId=" + "'" + id2 + "'")

        '   swap value of viewOrder
        Dim viewOrder As Integer
        viewOrder = row1(0)("ViewOrder")
        row1(0)("ViewOrder") = row2(0)("ViewOrder")
        row2(0)("ViewOrder") = viewOrder
    End Sub

    Private Sub dgrdTransCodeGLAccounts_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdTransCodeGLAccounts.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdTransCodeGLAccounts.ShowFooter = False

                '   user hit "Update" inside the datagrid
            Case "Update"
                '   process only if the footer textboxes are nonblank and valid
                If IsDataInEditTextboxesValid(e) And Not AreEditFieldsBlank(source, e) Then
                    '   get the row to be updated
                    Dim row() As DataRow = dataGridTable.Select("TransCodeGLAccountId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditTransCodeGLAccountId"), Label).Text).ToString + "'")
                    '   fill new values in the row
                    If Not CType(e.Item.FindControl("txtEditGLAccount"), TextBox).Text = "" Then row(0)("GLAccount") = CType(e.Item.FindControl("txtEditGLAccount"), TextBox).Text Else row(0)("GLAccount") = ""
                    If Not CType(e.Item.FindControl("txtEditPercentage"), TextBox).Text = "" Then row(0)("Percentage") = Decimal.Parse(CType(e.Item.FindControl("txtEditPercentage"), TextBox).Text) Else row(0)("Percentage") = 0
                    row(0)("TypeId") = CType(e.Item.FindControl("ddlEditTypeId"), DropDownList).SelectedValue
                    row(0)("Type") = ConvertTypeIdToType(row(0)("TypeId"))
                    row(0)("ModUser") = Session("UserName")
                    row(0)("ModDate") = Date.Parse(Date.Now.ToString)

                    '   no record is selected
                    dgrdTransCodeGLAccounts.EditItemIndex = -1

                    '   show footer
                    dgrdTransCodeGLAccounts.ShowFooter = True
                Else
                    DisplayErrorMessage("Invalid data during the edit of an existing GL Account Percentage Distribution")
                    Exit Sub
                End If

                '   user hit "Cancel"
            Case "Cancel"

                '   set no record selected
                dgrdTransCodeGLAccounts.EditItemIndex = -1

                '   show footer
                dgrdTransCodeGLAccounts.ShowFooter = True

                '   user hit "Delete" inside the datagrid
            Case "Delete"

                '   get the row to be deleted
                Dim row() As DataRow = dataGridTable.Select("TransCodeGLAccountId=" + "'" + CType(e.Item.FindControl("lblEditTransCodeGLAccountId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                '   set no record selected
                dgrdTransCodeGLAccounts.EditItemIndex = -1

                '   show footer
                dgrdTransCodeGLAccounts.ShowFooter = True

            Case "AddNewRow"
                '   process only if the footer textboxes are nonblank and valid
                If IsDataInFooterTextboxesValid(e) And Not AreFooterFieldsBlank(dgrdTransCodeGLAccounts) Then
                    '   If the dataList item doesn't have an Id assigned.. create one and assign it.
                    If Session("TransCodeId") Is Nothing Then
                        '   build a new row in dataList table
                        BuildNewRowInDataList(Guid.NewGuid.ToString)
                    End If

                    '   get a new row from the dataGridTable
                    Dim newRow As DataRow = dataGridTable.NewRow

                    '   fill the new row with values
                    newRow("TransCodeGLAccountId") = Guid.NewGuid
                    newRow("TransCodeId") = New Guid(CType(Session("TransCodeId"), String))
                    If Not CType(e.Item.FindControl("txtFooterGLAccount"), TextBox).Text = "" Then newRow("GLAccount") = CType(e.Item.FindControl("txtFooterGLAccount"), TextBox).Text
                    If Not CType(e.Item.FindControl("txtFooterPercentage"), TextBox).Text = "" Then newRow("Percentage") = Decimal.Parse(CType(e.Item.FindControl("txtFooterPercentage"), TextBox).Text) Else newRow("Percentage") = 100.0
                    newRow("TypeId") = CType(e.Item.FindControl("ddlFooterTypeId"), DropDownList).SelectedValue
                    newRow("Type") = ConvertTypeIdToType(newRow("TypeId"))
                    newRow("ViewOrder") = GetMaxViewOrder(dataGridTable) + 1
                    newRow("ModUser") = Session("UserName")
                    newRow("ModDate") = Date.Parse(Date.Now.ToString)

                    '   add row to the table
                    newRow.Table.Rows.Add(newRow)
                Else
                    DisplayErrorMessage("Invalid data during adding a new GL Account Distribution")
                    Exit Sub
                End If

                '   user hit "Up"
            Case "Up"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(dataGridTable, "TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim TransCodeGLAccountId As String = CType(e.Item.FindControl("lblTransCodeGLAccountId"), Label).Text

                '   get the position of this item and swap viewOrder values with prevous item
                Dim i As Integer
                For i = 1 To dv.Count - 1
                    If CType(dv(i).Row("TransCodeGLAccountId"), Guid).ToString = TransCodeGLAccountId Then
                        SwapViewOrders(CType(dv(i).Row("TransCodeGLAccountId"), Guid).ToString, CType(dv(i - 1).Row("TransCodeGLAccountId"), Guid).ToString)
                        Exit For
                    End If
                Next

                '   user hit "Down" 
            Case "Down"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(dataGridTable, "TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim TransCodeGLAccountId As String = CType(e.Item.FindControl("lblTransCodeGLAccountId"), Label).Text

                '   get the position of this item and swap viewOrder values with next item
                Dim i As Integer
                For i = 0 To dv.Count - 2
                    If CType(dv(i).Row("TransCodeGLAccountId"), Guid).ToString = TransCodeGLAccountId Then
                        SwapViewOrders(CType(dv(i).Row("TransCodeGLAccountId"), Guid).ToString, CType(dv(i + 1).Row("TransCodeGLAccountId"), Guid).ToString)
                        Exit For
                    End If
                Next

        End Select

        '   Bind DataGrid
        BindDataGrid(dgrdTransCodeGLAccounts)

    End Sub

    Private Sub dgrdTransCodeDRGLAccounts_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransCodeDRGLAccounts.ItemCommand, dgrdTransCodeGLAccounts.ItemCommand
        Dim dgrd As DataGrid = CType(source, DataGrid)
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrd.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrd.ShowFooter = False

                '   user hit "Update" inside the datagrid
            Case "Update"
                '   process only if the footer textboxes are nonblank and valid
                If IsDataInEditTextboxesValid(e) And Not AreEditFieldsBlank(source, e) Then
                    '   get the row to be updated
                    Dim row() As DataRow = dataGridTable.Select("TransCodeGLAccountId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditTransCodeGLAccountId"), Label).Text).ToString + "'")
                    '   fill new values in the row
                    If Not CType(e.Item.FindControl("txtEditGLAccount"), TextBox).Text = "" Then row(0)("GLAccount") = CType(e.Item.FindControl("txtEditGLAccount"), TextBox).Text Else row(0)("GLAccount") = ""
                    If Not CType(e.Item.FindControl("txtEditPercentage"), TextBox).Text = "" Then row(0)("Percentage") = Decimal.Parse(CType(e.Item.FindControl("txtEditPercentage"), TextBox).Text) Else row(0)("Percentage") = 0
                    row(0)("TypeId") = CType(e.Item.FindControl("ddlEditTypeId"), DropDownList).SelectedValue
                    row(0)("Type") = ConvertTypeIdToType(row(0)("TypeId"))
                    row(0)("ModUser") = Session("UserName")
                    row(0)("ModDate") = Date.Parse(Date.Now.ToString)

                    '   no record is selected
                    dgrd.EditItemIndex = -1

                    '   show footer
                    dgrd.ShowFooter = True
                Else
                    DisplayErrorMessage("Invalid data during the edit of an existing GL Account Percentage Distribution")
                    Exit Sub
                End If

                '   user hit "Cancel"
            Case "Cancel"

                '   set no record selected
                dgrd.EditItemIndex = -1

                '   show footer
                dgrd.ShowFooter = True

                '   user hit "Delete" inside the datagrid
            Case "Delete"

                '   get the row to be deleted
                Dim row() As DataRow = dataGridTable.Select("TransCodeGLAccountId=" + "'" + CType(e.Item.FindControl("lblEditTransCodeGLAccountId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                '   set no record selected
                dgrd.EditItemIndex = -1

                '   show footer
                dgrd.ShowFooter = True

            Case "AddNewRow"
                '   process only if the footer textboxes are nonblank and valid
                If IsDataInFooterTextboxesValid(e) And Not AreFooterFieldsBlank(dgrd) Then
                    '   If the dataList item doesn't have an Id assigned.. create one and assign it.
                    If Session("TransCodeId") Is Nothing Then
                        '   build a new row in dataList table
                        BuildNewRowInDataList(Guid.NewGuid.ToString)
                    End If

                    '   get a new row from the dataGridTable
                    Dim newRow As DataRow = dataGridTable.NewRow

                    '   fill the new row with values
                    newRow("TransCodeGLAccountId") = Guid.NewGuid
                    newRow("TransCodeId") = New Guid(CType(Session("TransCodeId"), String))
                    If Not CType(e.Item.FindControl("txtFooterGLAccount"), TextBox).Text = "" Then newRow("GLAccount") = CType(e.Item.FindControl("txtFooterGLAccount"), TextBox).Text
                    If Not CType(e.Item.FindControl("txtFooterPercentage"), TextBox).Text = "" Then newRow("Percentage") = Decimal.Parse(CType(e.Item.FindControl("txtFooterPercentage"), TextBox).Text) Else newRow("Percentage") = 100.0
                    newRow("TypeId") = CType(e.Item.FindControl("ddlFooterTypeId"), DropDownList).SelectedValue
                    newRow("Type") = ConvertTypeIdToType(newRow("TypeId"))
                    newRow("ViewOrder") = GetMaxViewOrder(dataGridTable) + 1
                    newRow("ModUser") = Session("UserName")
                    newRow("ModDate") = Date.Parse(Date.Now.ToString)

                    '   add row to the table
                    newRow.Table.Rows.Add(newRow)
                Else
                    DisplayErrorMessage("Invalid data during adding a new GL Account Distribution")
                    Exit Sub
                End If

                '   user hit "Up"
            Case "Up"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(dataGridTable, "TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim TransCodeGLAccountId As String = CType(e.Item.FindControl("lblTransCodeGLAccountId"), Label).Text

                '   get the position of this item and swap viewOrder values with prevous item
                Dim i As Integer
                For i = 1 To dv.Count - 1
                    If CType(dv(i).Row("TransCodeGLAccountId"), Guid).ToString = TransCodeGLAccountId Then
                        SwapViewOrders(CType(dv(i).Row("TransCodeGLAccountId"), Guid).ToString, CType(dv(i - 1).Row("TransCodeGLAccountId"), Guid).ToString)
                        Exit For
                    End If
                Next

                '   user hit "Down" 
            Case "Down"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(dataGridTable, "TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim TransCodeGLAccountId As String = CType(e.Item.FindControl("lblTransCodeGLAccountId"), Label).Text

                '   get the position of this item and swap viewOrder values with next item
                Dim i As Integer
                For i = 0 To dv.Count - 2
                    If CType(dv(i).Row("TransCodeGLAccountId"), Guid).ToString = TransCodeGLAccountId Then
                        SwapViewOrders(CType(dv(i).Row("TransCodeGLAccountId"), Guid).ToString, CType(dv(i + 1).Row("TransCodeGLAccountId"), Guid).ToString)
                        Exit For
                    End If
                Next

        End Select

        '   Bind DRDataGrid
        BindDataGrid(dgrd)

    End Sub

    Private Function GetMaxViewOrder(ByVal table As DataTable) As Integer
        Dim i As Integer
        GetMaxViewOrder = 1

        '   if there are no rows just return 1 
        Dim rows() As DataRow = table.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If rows.Length = 0 Then Return 1

        For i = 0 To rows.Length - 1
            If CType(rows(i)("ViewOrder"), Integer) > GetMaxViewOrder Then
                GetMaxViewOrder = CType(rows(i)("ViewOrder"), Integer)
            End If
        Next
    End Function

    Private Sub BuildNewRowInDataList(ByVal theGuid As String)
        '   get a new row
        Dim newRow As DataRow = dataListTable.NewRow

        '   create a Guid for DataList if it has not been created 
        Session("TransCodeId") = theGuid
        newRow("TransCodeId") = New Guid(theGuid)

        '   update row with web controls data
        UpdateRowData(newRow)

        '   add row to the table
        newRow.Table.Rows.Add(newRow)

    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   initialize buttons
            InitButtonsForLoad()

            '   bind an empty datagrid
            PrepareForNewData()
        End If

    End Sub

    Private Sub UpdateDB()
        '   update DB
        With New StudentsAccountsFacade
            Dim result As String = .UpdateTransCodesDS(allDataset)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get a new version of TransCodes dataset because some other users may have added,
                '   updated or deleted records from the DB
                Dim boolStatus As String

                If ddlStatusId.SelectedItem.Text.ToLower = "active" Then
                    boolStatus = "True"
                    radstatus.SelectedIndex = 0
                ElseIf ddlStatusId.SelectedItem.Text.ToLower = "inactive" Then
                    radstatus.SelectedIndex = 1
                    boolStatus = "False"
                Else
                    boolStatus = "All"
                End If
                allDataset = .GetTransCodesDS(boolStatus)
                Session("AllDataset") = allDataset
                CreateDatasetAndTables()
            End If
        End With
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
            If allDataset.HasChanges() Then
                'Set the Delete Button so it prompts the user for confirmation when clicked
                btnnew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
            Else
                btnnew.Attributes.Remove("onclick")
            End If

            '   save dataset systems in session
            Session("AllDataset") = allDataset

            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnsave)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            BindToolTip()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub

    Private Function AreFooterFieldsBlank(ByVal source As DataGrid) As Boolean
        Dim footer As Control = source.Controls(0).Controls(source.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtFooterGLAccount"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterPercentage"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function AreEditFieldsBlank(ByVal source As DataGrid, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not CType(e.Item.FindControl("txtEditGLAccount"), TextBox).Text = "" Then Return False
        If Not CType(e.Item.FindControl("txtEditPercentage"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function IsDatagridBalanced(ByVal source As DataGrid) As Boolean
        Dim footer As Control = source.Controls(0).Controls(source.Controls(0).Controls.Count - 1)
        If CType(footer.FindControl("lblBalanced"), Label).Text = "Bal" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub UpdateRowData(ByVal row As DataRow)
        '   update row data
        row("TransCodeCode") = txtTransCodeCode.Text.Trim
        row("StatusId") = New Guid(ddlStatusId.SelectedValue)
        row("TransCodeDescrip") = txtTransCodeDescrip.Text.Trim
        row("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
        row("BillTypeId") = New Guid(ddlBillTypeId.SelectedValue)
        row("DefEarnings") = chkDefEarnings.Checked

        row("IsInstCharge") = chkIsInstCharge.Checked
        row("Is1098T") = chkIs1098T.Checked
        row("SysTransCodeId") = ddlSysTransCodeId.SelectedValue
        row("ModUser") = Session("UserName")
        row("ModDate") = Date.Parse(Date.Now.ToString)
    End Sub

    Private Function IsDataInFooterTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not IsTextBoxValidAccount(e.Item.FindControl("txtFooterGLAccount")) Then Return False
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtFooterPercentage")) Then Return False
        Return True
    End Function

    Private Function IsDataInEditTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not IsTextBoxValidAccount(e.Item.FindControl("txtEditGLAccount")) Then Return False
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtEditPercentage")) Then Return False
        Return True
    End Function

    Private Function IsTextBoxValidAccount(ByVal textbox As TextBox) As Boolean
        If Len(textbox.Text) > 20 Then Return False
        Return True
    End Function

    Private Function DeleteDashes(ByVal str As String) As String
        Do While str.IndexOf("-") > -1
            str = str.Remove(str.IndexOf("-"), 1)
        Loop
        Return str
    End Function

    Private Function IsTextBoxValidDecimal(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        Try
            Dim d As Decimal = Decimal.Parse(textbox.Text)
            Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    Private Function ConvertTypeIdToType(ByVal id As Integer) As String
        Select Case id
            Case 0, 2
                Return "Debit"
            Case 1, 3
                Return "Credit"
        End Select
        Return String.Empty
    End Function

    Private Sub BindDataGrid(ByVal dgrd As DataGrid)

        'set the rowfilter according to the datagrid to be used
        Dim rowFilter As String = "TransCodeId=" + "'" + CType(Session("TransCodeId"), String) + "'"
        If dgrd.ID = "dgrdTransCodeGLAccounts" Then
            rowFilter += " AND (TypeId=0 OR TypeId=1)"
        Else
            rowFilter += " AND (TypeId=2 OR TypeId=3)"
        End If

        '   bind datagrid to dataGridTable
        dgrd.DataSource = New DataView(dataGridTable, rowFilter, "ViewOrder asc", DataViewRowState.CurrentRows)
        debits = 0.0
        credits = 0.0
        dgrd.DataBind()
    End Sub

    Private Function IsTextBoxBlank(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True Else Return False
    End Function

    Private Sub SetBalancedFlag(ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Dim lab As Label = CType(e.Item.FindControl("lblBalanced"), Label)
        If IsBalanced() Then
            '   set word "BAL" in green color
            lab.Text = "Bal"
            lab.ForeColor = Color.Green
            lab.Font.Bold = True
        Else
            '   set word "Unb" in red color
            lab.Text = "Unb"
            lab.ForeColor = Color.Red
            lab.Font.Bold = True
        End If
    End Sub

    Private Function IsBalanced() As Boolean
        '   check that all types are balanced
        If debits = credits Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub dgrdTransCodeGLAccounts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdTransCodeGLAccounts.ItemDataBound, dgrdTransCodeDRGLAccounts.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Select Case CType(e.Item.DataItem, DataRowView).Item("TypeId")
                    Case 0, 2
                        debits += CType(e.Item.DataItem, DataRowView).Item("Percentage")
                    Case 1, 3
                        credits += CType(e.Item.DataItem, DataRowView).Item("Percentage")
                End Select
            Case ListItemType.Footer
                '   if percentages are 100.00 set the balance flag to green
                SetBalancedFlag(e)
        End Select
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged

        With New StudentsAccountsFacade
            allDataset = .GetTransCodesDS(fsGetStatusFilter())
            Session("AllDataset") = allDataset
            CreateDatasetAndTables()
        End With

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   initialize buttons
            InitButtonsForLoad()

            '   bind an empty datagrid
            PrepareForNewData()
        End If
    End Sub

    Private Function fsGetStatusFilter() As String
        Dim sValue As String = ""

        If radstatus.SelectedItem.Text.ToLower = "active" Then
            sValue = "True"
        ElseIf radstatus.SelectedItem.Text.ToLower = "inactive" Then
            sValue = "False"
        Else
            sValue = "All"
        End If

        Return sValue
    End Function

    Protected Sub chkDefEarnings_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDefEarnings.CheckedChanged
        'set visibility of Deferred Revenue Panel
        DRPanel.Visible = chkDefEarnings.Checked
    End Sub
    'US3269 9/19/2012 
    Protected Sub ddlSysTransCodeId_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlSysTransCodeId.SelectedIndexChanged
        If ddlSysTransCodeId.SelectedItem.Text = "Applicant Fee" Then
            ddlBillTypeId.SelectedValue = ddlBillTypeId.Items.FindByText("Applicant").Value
            'DE8921
            chkDefEarnings.Enabled = False
            chkDefEarnings.Checked = False
            chkIs1098T.Checked = True
            chkIs1098T.Enabled = False
        ElseIf ddlSysTransCodeId.SelectedItem.Text = "Tuition" Then
            chkDefEarnings.Checked = True
            chkDefEarnings.Enabled = False
            DRPanel.Visible = True
            chkIs1098T.Checked = True
            chkIs1098T.Enabled = False
            chkIsInstCharge.Checked = True
            chkIsInstCharge.Enabled = False
        Else
            chkDefEarnings.Checked = False
            chkDefEarnings.Enabled = True
            DRPanel.Visible = False
            chkIs1098T.Checked = False
            chkIs1098T.Enabled = True
            chkIsInstCharge.Checked = False
            chkIsInstCharge.Enabled = True
        End If

    End Sub
End Class
