
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.Globalization
Imports BO = Advantage.Business.Objects

Partial Class LateFees
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New FAME.Common.CommonUtilities
        Dim fac As New UserSecurityFacade
        Dim m_Context As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList()

            '   bind an empty new LateFeesInfo
            BindLateFeesData(New LateFeesInfo)

            '   initialize buttons
            'InitButtonsForLoad()
        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            'Call the procedure for edit
            'InitButtonsForEdit()
        End If

    End Sub
    Private Sub BindDataList()

        '   bind LateFees datalist
        dlstLateFees.DataSource = New DataView((New LateFeesFacade).GetAllLateFees().Tables(0), Nothing, Nothing, DataViewRowState.CurrentRows)
        dlstLateFees.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        'BuildStatusDDL()
        'BuildCampusGroupsDDL()

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, False, True))

        'Campus Groups DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCampGrpId, AdvantageDropDownListName.CampGrps, Nothing, True, True))

        ''Transaction Codes
        'ddlList.Add(New AdvantageDDLDefinition(ddlTransCodeId, AdvantageDropDownListName.Trans_Codes, campusId, True, True, Guid.Empty.ToString))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        'build TransCodesDDL
        BuildTransCodesDDL()
    End Sub
    'Private Sub BuildStatusDDL()
    '    '   bind the status DDL
    '    Dim statuses As New StatusesFacade

    '    With ddlStatusId
    '        .DataTextField = "Status"
    '        .DataValueField = "StatusId"
    '        .DataSource = statuses.GetAllStatuses()
    '        .DataBind()
    '    End With

    'End Sub
    'Private Sub BuildCampusGroupsDDL()
    '    '   bind the CampusGroups DDL
    '    Dim campusGroups As New CampusGroupsFacade

    '    With ddlCampGrpId
    '        .DataTextField = "CampGrpDescrip"
    '        .DataValueField = "CampGrpId"
    '        .DataSource = campusGroups.GetAllCampusGroups()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With

    'End Sub
    Private Sub BuildTransCodesDDL()
        '   bind the TransCodes DDL
        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeId"
            .DataSource = (New StudentsAccountsFacade).GetAllTransCodes(7, False)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub dlstLateFees_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstLateFees.ItemCommand
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        '   get the LateFeesId from the backend and display it
        GetLateFeesId(e.CommandArgument)

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstLateFees, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlstLateFees, e.CommandArgument.ToString)
    End Sub
    Private Sub BindLateFeesData(ByVal LateFees As LateFeesInfo)
        With LateFees
            chkIsInDB.Checked = .IsInDB
            txtLateFeesId.Text = .LateFeesId
            txtLateFeesCode.Text = .Code
            If Not (.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = LateFees.StatusId
            txtLateFeesDescrip.Text = .Description
            If Not (.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = LateFees.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            If Not (.TransCodeId = Guid.Empty.ToString) Then ddlTransCodeId.SelectedValue = LateFees.TransCodeId Else ddlTransCodeId.SelectedIndex = 0
            txtEffectiveDate.SelectedDate = CDate(.EffectiveDate.ToShortDateString)
            If .FlatAmount > 0.0 Then
                txtFlatAmount.Text = .FlatAmount.ToString("##0.00")
                rbFlatAmount.Checked = True
            Else
                txtFlatAmount.Text = "0.00"
                rbFlatAmount.Checked = False
            End If
            If .Rate > 0.0 Then
                txtRate.Text = .Rate.ToString("##0.00")
                rbRate.Checked = True
            Else
                txtRate.Text = "0.00"
                rbRate.Checked = False
            End If
            If .Rate = 0.0 And .FlatAmount = 0 Then rbFlatAmount.Checked = True

            txtGracePeriod.Text = .GracePeriod.ToString("d")
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim result As String
        With New LateFeesFacade
            '   update LateFees Info 
            result = .UpdateLateFeesInfo(BuildLateFeesInfo(txtLateFeesId.Text), Context.User.Identity.Name)
        End With


        '   bind datalist
        BindDataList()

        '   set Style to Selected Item
        CommonWebUtilities.SetStyleToSelectedItem(dlstLateFees, txtLateFeesId.Text, ViewState)

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the LateFeesId from the backend and display it
            GetLateFeesId(txtLateFeesId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new LateFeesInfo
            'BindLateFeesData(New LateFeesInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If
        CommonWebUtilities.RestoreItemValues(dlstLateFees, txtLateFeesId.Text)
    End Sub
    Private Function BuildLateFeesInfo(ByVal LateFeesId As String) As LateFeesInfo
        'instantiate class
        Dim LateFeesInfo As New LateFeesInfo

        With LateFeesInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get LateFeesId
            .LateFeesId = LateFeesId

            'get Code
            .Code = txtLateFeesCode.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get LateFees's name
            .Description = txtLateFeesDescrip.Text.Trim

            'get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            'Transaction Code
            .TransCodeId = ddlTransCodeId.SelectedValue

            'Effective Date
            .EffectiveDate = Date.Parse(txtEffectiveDate.SelectedDate)

            'Flat Amount
            If rbFlatAmount.Checked Then
                .FlatAmount = Decimal.Parse(txtFlatAmount.Text, NumberStyles.Currency)
            Else
                .FlatAmount = Decimal.Parse("0.00")
            End If

            'Rate
            If rbRate.Checked Then
                .Rate = Decimal.Parse(txtRate.Text, NumberStyles.AllowDecimalPoint + NumberStyles.AllowLeadingWhite + NumberStyles.AllowTrailingWhite)
            Else
                .Rate = Decimal.Parse("0.00")
            End If

            'Grace Period
            .GracePeriod = Integer.Parse(txtGracePeriod.Text, NumberStyles.AllowLeadingWhite + NumberStyles.AllowTrailingWhite)

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return LateFeesInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new LateFeesInfo
        BindLateFeesData(New LateFeesInfo)

        'Reset Style in the Datalist
        CommonWebUtilities.SetStyleToSelectedItem(dlstLateFees, Guid.Empty.ToString, ViewState)

        'initialize buttons
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlstLateFees, Guid.Empty.ToString)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtLateFeesId.Text = Guid.Empty.ToString) Then

            'update LateFees Info 
            Dim result As String = (New LateFeesFacade).DeleteLateFeesInfo(txtLateFeesId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new LateFeesInfo
                BindLateFeesData(New LateFeesInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If
            CommonWebUtilities.RestoreItemValues(dlstLateFees, Guid.Empty.ToString)
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If
    End Sub
    Private Sub GetLateFeesId(ByVal LateFeesId As String)

        '   bind LateFees properties
        BindLateFeesData((New LateFeesFacade).GetLateFeesInfo(LateFeesId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)


        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    'Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
    '    '   bind datalist
    '    BindDataList()
    'End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
End Class
