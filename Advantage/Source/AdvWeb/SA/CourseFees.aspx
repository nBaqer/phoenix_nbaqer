<%@ Page Language="vb" AutoEventWireup="false" Inherits="CourseFees" CodeFile="CourseFees.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Course Fees</title>
 <%--   <link href="../css/localhost_lowercase.css" type="text/css" rel="stylesheet" />--%>
    <link href="../css/systememail.css" type="text/css" rel="stylesheet" />
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
</head>
<body runat="server" leftmargin="0" topmargin="0" id="Body1" name="Body1">
    <form id="Form1" method="post" runat="server">
    <!-- beging header -->
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true" AsyncPostBackTimeout="3600">
                <Scripts>
                    <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
                    <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
                </Scripts>
            </telerik:RadScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <img src="../images/advantage_course_fees.jpg">
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
    </table>
    <!-- end header -->
    <table cellspacing="0" cellpadding="0" width="100%" height="100%" border="0">
        <tr>
            <td class="listframe">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table2">
                    <tr>
                        <td class="listframetop2">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="15%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server"><b class="label">Show</b></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radStatus" CssClass="Label" AutoPostBack="true" runat="Server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftcoursefees">
                                <asp:DataList ID="dlstCourseFees" runat="server" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
                                    <HeaderTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td style="width: 50%; border: 1px solid #999">
                                                    <asp:Label ID="lblTransCode" runat="server" CssClass="label">Transaction Code</asp:Label>
                                                </td>
                                                <td style="width: 50%; border-bottom: 1px solid #999; border-right: 1px solid #999;
                                                    border-top: 1px solid #999">
                                                    <asp:Label ID="lblStartDateTitle" runat="server" CssClass="label">Start Date</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="NonSelectedItem"></ItemStyle>
                                    <ItemTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td width="50%">
                                                    <asp:ImageButton ID="imgInActive" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
                                                        CausesValidation="False" CommandArgument='<%# Container.DataItem("CourseFeeId") %>'
                                                        ImageUrl="../images/Inactive.gif" />
                                                    <asp:ImageButton ID="imgActive" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>'
                                                        CausesValidation="False" CommandArgument='<%# Container.DataItem("CourseFeeId") %>'
                                                        ImageUrl="../images/Active.gif" />
                                                    <asp:LinkButton ID="Linkbutton1" CssClass="NonSelectedItem" CausesValidation="False"
                                                        runat="server" Text='<%# Container.DataItem("TransCodeDescrip") %>' CommandArgument='<%# Container.DataItem("CourseFeeId") %>'>
                                                    </asp:LinkButton>
                                                </td>
                                                <td width="50%">
                                                    <asp:Label ID="lblStartDateDL" runat="server" CssClass="NonSelectedItem" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate", "{0:d}") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="leftside">
                <table cellspacing="0" cellpadding="0" width="9" border="0" id="Table3">
                </table>
            </td>
            <td class="DetailsFrameTop">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                    <tr>
                        <td class="MenuFrame" align="right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                            <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
                <table width="90%" border="0" cellpadding="0" cellspacing="0" id="Table5">
                    <tr>
                        <td class="detailsframeff">
                            <div>
                                <table cellspacing="0" cellpadding="0" align="center" width="90%">
                                    <asp:TextBox ID="txtCourseFeeId" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                                        ID="txtCourseId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB"
                                            runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtModUser" runat="server"
                                                Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate" runat="server"
                                                    Visible="False">ModDate</asp:TextBox>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblCourseId" runat="server" CssClass="label">Course</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell" style="text-align: left">
                                            <asp:Label ID="lblCourse" runat="server" CssClass="label" Enabled="False">Course</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblStatusId" CssClass="label" runat="server" Visible="false">Status</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell" style="text-align: left">
                                            <asp:DropDownList ID="ddlStatusId" runat="server" Width="50%" CssClass="dropdownlistff"
                                                Visible="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblTransCodeId" runat="server" CssClass="label">Transaction Code<font color="red">*</font></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell" style="text-align: left">
                                            <asp:DropDownList ID="ddlTransCodeId" runat="server" Width="50%" CssClass="dropdownlistff"></asp:DropDownList>
                                            <asp:CompareValidator ID="cvTransCode" runat="server" ControlToValidate="ddlTransCodeId"
                                                Display="None" ErrorMessage="Must Select a Transaction Code" ValueToCompare="00000000-0000-0000-0000-000000000000"
                                                Operator="NotEqual">Must Select a Transaction Code</asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblStartDate" runat="server" CssClass="label">Effective Date<font color="red">*</font></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcelldate">

                                            <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="RadDatePicker1"
                                                Display="None" ErrorMessage="Start Date can not be blank">Start Date can not be blank</asp:RequiredFieldValidator>
                                            <telerik:RadDatePicker ID="RadDatePicker1" runat="server"  ></telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                        </td>
                                        <td class="twocolumncontentcelldate" >
                                            <asp:Panel ID="pnlRate" runat="server" CssClass="panel" BorderWidth="1" style="margin:10px 0px;">
                                                <table id="Table1" cellspacing="1" cellpadding="6" width="100%" border="0">
                                                    <tr>
                                                        <td nowrap colspan="3">
                                                            <asp:RadioButton ID="rbtFlatAmount" runat="server" CssClass="checkbox" Text="Flat Rate"
                                                                Checked="True" GroupName="RateSchedule"></asp:RadioButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img alt="" src="../images/1x1.gif" width="25">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblAmount" runat="server" CssClass="label">Amount</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="textboxAmount" Width="100px"
                                                                BackColor="White" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblUnit" runat="server" CssClass="label">Unit</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlUnitId" runat="server" CssClass="dropdownlistff">
                                                                <asp:ListItem Value="0">Credit Hour</asp:ListItem>
                                                                <asp:ListItem Value="2">Course</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblTuitionCategoryId" runat="server" CssClass="label">Tuition Category</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTuitionCategoryId" runat="server" CssClass="dropdownlistff">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <hr class="HR" width="100%" size="1">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap colspan="3">
                                                            <asp:RadioButton ID="rbtRateSchedule" runat="server" CssClass="checkbox" Text="Rate Schedule"
                                                                GroupName="RateSchedule"></asp:RadioButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblRateScheduleId" runat="server" CssClass="label">Rate Schedule</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlRateScheduleId" runat="server" CssClass="dropdownlistff">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server" Width="50px">
    </asp:Panel>
    &nbsp;&nbsp;
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False"
        ShowMessageBox="True"></asp:ValidationSummary>
    <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    </form>
</body>
</html>
