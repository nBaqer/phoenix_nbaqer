<%@ Page Language="vb" AutoEventWireup="false" Inherits="ProgramVersionFees" CodeFile="ProgramVersionFees.aspx.vb" %>

<%@ Register TagPrefix="fame" TagName="header" Src="~/UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Program Version Fees</title>
    <link href="../CSS/systememail.css" type="text/css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="../css/popup.css" />
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script language="javascript" src="../AuditHist.js"></script>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
</head>
<body runat="server" leftmargin="0" topmargin="0" id="Body1" name="Body1">
    <form id="Form1" method="post" runat="server">
    <!-- beging header -->
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <img src="../images/advantage_setup_fees.jpg">
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
    </table>
    <!-- end header -->
      
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" height="100%" border="0">
        <tr>
            <td class="ListFrame">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table2">
                    <tr>
                        <td class="listframetop2">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="15%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server"><b class="label">Show</b></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radStatus" CssClass="Label" AutoPostBack="true" runat="Server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftcoursefees">
                                <asp:DataList ID="dlstProgramVersionFees" runat="server" Width="100%">
                                    <SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
                                    <HeaderTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="threecolumndatalist" style="width: 100%; border: 1px solid #999">
                                                    <asp:Label ID="lblTransCode" runat="server" CssClass="labelBold">Transaction Code</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="NonSelectedItem"></ItemStyle>
                                    <ItemTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="threecolumndatalist" style="width: 100%; padding: 0 10px 0 10px">
                                                    <asp:ImageButton ID="imgInActive" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
                                                        CausesValidation="False" CommandArgument='<%# Container.DataItem("PrgVerFeeId") %>'
                                                        ImageUrl="../images/Inactive.gif" />
                                                    <asp:ImageButton ID="imgActive" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>'
                                                        CausesValidation="False" CommandArgument='<%# Container.DataItem("PrgVerFeeId") %>'
                                                        ImageUrl="../images/Active.gif" />
                                                    <asp:LinkButton ID="Linkbutton1" CssClass="NonSelectedItem" CausesValidation="False"
                                                        runat="server" CommandArgument='<%# Container.DataItem("PrgVerFeeId") %>' Text='<%# Container.DataItem("TransCodeDescrip") %>'>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList></div>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="leftside">
                <table cellspacing="0" cellpadding="0" width="9" border="0" id="Table3">
                </table>
            </td>
            <td class="DetailsFrameTop">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                    <tr>
                        <td class="MenuFrame" align="right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                            <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
                <div id="contentnew2">
                        <center><h2 style="margin:10px;"><%Response.Write(lblProgramVersion.Text)%></h2></center>
                     <div class="rowform"><asp:Label ID="lblPrgVerId" runat="server" CssClass="label" Visible="false">Program Version</asp:Label></div>
                    <div class="rowform">                                    
                        <asp:TextBox ID="txtPrgVerFeeId" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                        ID="txtPrgVerId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB"
                        runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtModUser" runat="server"
                        Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate" runat="server"
                        Visible="False">ModDate</asp:TextBox>
                    </div>
                    <div class="rowform"> <asp:Label ID="lblProgramVersion" runat="server" CssClass="label" Enabled="False" Visible="false">ProgramVersion</asp:Label></div>
                    <div class="rowform"><asp:Label ID="lblStatusId" CssClass="label" runat="server" Visible="false">Status</asp:Label></div>
                    <div class="rowform">
                    <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist2"
                        Visible="false">
                    </asp:DropDownList>
                    </div>
                    <div class="rowform"><asp:Label ID="lblTransCodeId" runat="server" CssClass="label">Transaction Code</asp:Label></div>
                    <div class="rowform">
                    <asp:DropDownList ID="ddlTransCodeId" runat="server" CssClass="dropdownlist2">
                    </asp:DropDownList>
                    <asp:CompareValidator ID="cvTransCode" runat="server" ControlToValidate="ddlTransCodeId"
                    Display="None" ErrorMessage="Must Select a Transaction Code" ValueToCompare="00000000-0000-0000-0000-000000000000"
                    Operator="NotEqual">Must Select a Transaction Code</asp:CompareValidator>
                    </div>
                    <div class="rowform">
                    <asp:Panel ID="pnlRate" runat="server" CssClass="panel" BorderWidth="1">
                        <table id="Table6" cellspacing="1" cellpadding="6" width="100%" border="0">
                            <tr>
                                <td nowrap colspan="3">
                                    <asp:RadioButton ID="rbtFlatAmount" runat="server" CssClass="checkbox" Text="Flat Amount"
                                        GroupName="RateSchedule" Checked="True"></asp:RadioButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="../images/1x1.gif" width="25">
                                </td>
                                <td>
                                    <asp:Label ID="lblAmount" runat="server" CssClass="label">Amount</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="textboxAmount" BackColor="White"
                                        MaxLength="50" Width="100px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="lblUnit" runat="server" CssClass="label">Unit</asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlUnitId" runat="server" CssClass="dropdownlist2">
                                        <asp:ListItem Value="0">Credit Hour</asp:ListItem>
                                        <asp:ListItem Value="1">Clock Hour</asp:ListItem>
                                        <asp:ListItem Value="2">Program</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="lblTuitionCategoryId" runat="server" CssClass="label">Tuition Category</asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTuitionCategoryId" runat="server" CssClass="dropdownlist2">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr width="100%" size="1">
                                </td>
                            </tr>
                            <tr>
                                <td nowrap colspan="3">
                                    <asp:RadioButton ID="rbtRateSchedule" runat="server" CssClass="checkbox" Text="Rate Schedule"
                                        GroupName="RateSchedule"></asp:RadioButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="lblRateScheduleId" runat="server" CssClass="label">Rate Schedule</asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlRateScheduleId" runat="server" CssClass="dropdownlist2">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    </div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                    <div class="rowform"></div>
                </div>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table5" style="visibility:hidden;">
                    <tr>
                        <td class="detailsframe">
                            <div class="scrollrightcoursefees" style="visibility:hidden;">
                                <table cellspacing="2" cellpadding="2" align="center" width="100%">

                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            
                                        </td>
                                        <td class="twocolumncontentcelldate">
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            
                                        </td>
                                        <td class="twocolumncontentcell">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            
                                        </td>
                                        <td class="twocolumncontentcell">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                        </td>
                                        <td class="twocolumncontentcell">

                                        </td>
                                    </tr>
                                </table>
                            </div>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
  <%--  <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
    </form>
</body>
</html>
