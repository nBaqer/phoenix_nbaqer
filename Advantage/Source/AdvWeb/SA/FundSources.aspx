<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="FundSources.aspx.vb" Inherits="FundSources" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>

    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstFundSources" runat="server" DataKeyField="FundSourceId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("FundSourceId")%>' Text='<%# container.dataitem("FundSourceDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                    </tr>
                </table>
                <div class="boxContainer">
                    <h3><%=Header.Title  %></h3>
                    <asp:Panel ID="pnlRHS" runat="server">
                        <table width="60%" align="center">
                            <asp:TextBox ID="txtFundSourceId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                            <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                            <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblFundSourceCode" runat="server" CssClass="label">Code</asp:Label></td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtFundSourceCode" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCommonFeeCode" runat="server" ErrorMessage="Code can not be blank" Display="None"
                                        ControlToValidate="txtFundSourceCode">Code can not be blank</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label></td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblFundSourceDescrip" CssClass="label" runat="server">Description</asp:Label></td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtFundSourceDescrip" CssClass="textbox" runat="server" MaxLength="150"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label></td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList>
                                    <asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ErrorMessage="Must Select a Campus Group"
                                        Display="None" ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual" ControlToValidate="ddlCampGrpId">Must Select a Campus Group</asp:CompareValidator></td>
                            </tr>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblAwardTypeId" runat="server" CssClass="label"></asp:Label></td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlAwardTypeId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblAdvFundSourceId" runat="server" CssClass="label"></asp:Label></td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlAdvFundSourceId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="contentcell"></td>
                                <td class="contentcell4" align="left">
                                    <asp:CheckBox ID="chkTitleIV" runat="server" CssClass="checkboxinternational" Text="Title IV"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblCutoffDate" runat="server" CssClass="label"></asp:Label></td>
                                <td class="contentcell4date" align="left">
                                    <telerik:RadDatePicker ID="txtCutOffDate" MinDate="1/1/1945" runat="server" Width="200px">
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Panel ID="pnlReportingAgenciesHeader" runat="server" Visible="True">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center" style="border: 1px solid #ebebeb;">
                                <tr>
                                    <td class="contentcellheader" nowrap colspan="6" style="border-top: 0px; border-right: 0px; border-left: 0px">
                                        <asp:Label ID="lblReportingAgencies" runat="server" Font-Bold="true" CssClass="label">Accrediting Agencies Mapping</asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="contentcellcitizenship" colspan="6" style="padding-top: 12px">
                                        <asp:Panel ID="pnlReportingAgencies" TabIndex="12" runat="server" EnableViewState="false"></asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <p></p>
                        </asp:Panel>
                        <!--end table content-->
                    </asp:Panel>
                </div>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        <!--end validation panel-->
    </div>

</asp:Content>


