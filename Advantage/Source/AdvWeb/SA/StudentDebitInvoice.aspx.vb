
Imports System.Diagnostics
Imports Advantage.Business.Objects
Imports Telerik.Web.UI.Calendar
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports CrystalDecisions.Shared.Json
Imports FAME.Advantage.Api.Library.Models
Imports FAME.AdvantageV1.BusinessFacade

Partial Class StudentDebitInvoice
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblStartDate As Label
    Protected WithEvents Img6 As HtmlImage
    Protected state As AdvantageSessionState
    Protected WithEvents lblDate As Label
    Protected WithEvents lbtStudentName As LinkButton


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    'Private m_Context As HttpContext
    Private DateChg As Boolean = False
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim userId As String
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        Dim facCorp As New CorporateFacade
        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        'userId = AdvantageSession.UserState.UserId.ToString


        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Request.Params("__EVENTTARGET") Is Nothing Then
            If Request.Params("__EVENTTARGET").ToString = "lbtStudentName" Then
                lbtStudentName_Click(Me, New EventArgs)
            End If
        End If
        If Not Page.IsPostBack Then
            BuildStatusDDL()
            BuildProgramsDDL()
            ''validation added by saraswathi to find if invoice address in available for the campus
            ''Added on June 18 2009
            If facCorp.ValidateIfInvoiceAddressAvialable(campusId) = False Then
                DisplayErrorMessage("Enter the Invoice Address information (Address1, City, State and Zip)in Campus page for the Student Invoice Report")
            End If
        End If
    End Sub
    Private Sub GetList()
        With dgrdStudentDetails
            Dim expectedDate As String = ""
            If Not txtExpectDate.SelectedDate Is Nothing Then
                expectedDate = CType(txtExpectDate.SelectedDate, String)
            End If
            .DataSource = (New StuDebitInvoiceFacade).GetStudentDebitInvoices(CType(txtDate.SelectedDate, String), campusId, ddlStatusId.SelectedValue, ddlProgram.SelectedValue, expectedDate)
            .DataBind()
        End With

        btnGenerateInv.Visible = True
        btnGenerateInv.Enabled = True
    End Sub
    Private Sub BindDatagrid(ByVal sortExpression As String)

        'this logic is to laternative sort the columns in ascending or descending mode
        'get column to be sorted. Change replace "asc" by "desc". Insert "asc" if necessary
        For Each column As DataGridColumn In dgrdStudentDetails.Columns
            If column.SortExpression = sortExpression Then
                If sortExpression.IndexOf(" asc") > 0 Then
                    sortExpression = sortExpression.Replace(" asc", " desc")
                ElseIf sortExpression.IndexOf(" desc") > 0 Then
                    sortExpression = sortExpression.Replace(" desc", " asc")
                Else
                    sortExpression += " asc"
                End If

                'update sort expression in the column
                column.SortExpression = sortExpression
                Exit For
            End If
        Next

        '   bind datagrid with data from the backend
        With New StuDebitInvoiceFacade

            Dim expectedDate As String = ""
            If Not txtExpectDate.SelectedDate Is Nothing Then
                expectedDate = CType(txtExpectDate.SelectedDate, String)
            End If
            dgrdStudentDetails.DataSource = New DataView(.GetStudentDebitInvoices(CType(txtDate.SelectedDate, String), campusId, ddlStatusId.SelectedValue, ddlProgram.SelectedValue, expectedDate).Tables(0), Nothing, sortExpression, DataViewRowState.CurrentRows)
            dgrdStudentDetails.DataBind()
        End With

    End Sub

    Private Sub GetCheckedStudents()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        'Dim ErrStr As String = ""
        Dim selected As Boolean = False
        Dim stuEnrollIdList As String = String.Empty
        Dim StuEnrollGuid As New List(Of Guid)

        ' Save the datagrid items in a collection.
        iitems = dgrdStudentDetails.Items
        Try

            'Loop thru the collection to retrieve the chkbox value
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)

                If (CType(iitem.FindControl("chkCopy"), CheckBox).Checked = True) Then
                    'get the hidden values
                    'stuEnrollIdList += "'" & (CType(iitem.FindControl("txtstuenrollid"), TextBox).Text) & "'" & ","
                    stuEnrollIdList += (CType(iitem.FindControl("txtstuenrollid"), TextBox).Text) & ","
                    StuEnrollGuid.Add(New Guid(CType(iitem.FindControl("txtstuenrollid"), TextBox).Text))
                    'set flag to true if option is selected
                    selected = True
                End If
            Next

            If selected = False Then
                DisplayErrorMessage("Please select one or more student enrollments in order to generate the invoice")
                'Response.Write("Please select one or more student enrollments in order to generate the invoice")
                Exit Sub
            Else
                Session("StuEnrollIdList") = (stuEnrollIdList.Substring(0, (stuEnrollIdList.Trim.Length - 1)))
                'PopUpReport()
                'call the api url to get the report
                Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
                Dim invoiceReport = New FAME.Advantage.Api.Library.Reports.InvoiceReport(tokenResponse.ApiUrl, tokenResponse.Token)
                Dim model = New FAME.Advantage.Api.Library.Models.Reports.InvoiceReport
                model.CampusId = New Guid(campusId)
                model.RefDate = txtDate.SelectedDate
                model.StuEnrollId = StuEnrollGuid
                'code to generate pdf
                Dim result = invoiceReport.getInvoiceReport(model)
                If Not result Is Nothing Then
                    Response.ContentType = "application/octet-stream"
                    Response.AddHeader("Content-Disposition", "attachment;filename=InvoiceReport.pdf")
                    result.CopyTo(Response.OutputStream)
                    Response.Flush()
                Else
                    DisplayErrorMessage("No data found.")
                End If

            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub GetCheckedStudents" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub GetCheckedStudents" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")

        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        'need to uncheck this when porting over to live
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub btnGenerateInv_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerateInv.Click
        'DE9010 2/12/2013 Janet Robinson
        If Not DateChg Then
            GetCheckedStudents()
        End If
    End Sub

    Private Sub btnGetList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetList.Click
        GetList()
    End Sub

    Private Sub PopUpReport()
        'If Not (Session("ReportDS") Is Nothing) Then        
        Dim popupScript As String = "<script language='javascript'>" &
            "window.open('rptStuDebitInv.aspx?Date=" & txtDate.SelectedDate & "&cmpid=" & campusId & "', 'CustomPopUp', " &
            "'width=780px, height=400px, menubar=yes, resizable=no, toolbar=no, scrollbars=yes, resizable=yes, status=yes')" &
            "</script>"

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", popupScript, False)

    End Sub

    Private Sub lbtStudentName_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtStudentName.Click
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String

        'Dim defaultCampusId As String

        'defaultCampusId = campusId
        '    defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString
        '
        Session("SEARCH") = 1

        'Set relevant properties on the state object
        Dim cmdArgument As String = Request.Params("__EVENTARGUMENT").ToString
        objStateInfo.StudentId = cmdArgument
        objStateInfo.NameCaption = "Student : "
        objStateInfo.NameValue = (New StudentSearchFacade).GetStudentNameByID(cmdArgument)

        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo

        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)


        '   setup the properties of the new window
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
        Dim name As String = "StudentLedgerPopUp"
        Dim url As String = "../SA/" + name + ".aspx?resid=116&mod=SA&cmpid=" + campusId + "&VID=" + strVID
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)


    End Sub

    Private Sub dgrdStudentDetails_SortCommand(ByVal source As Object, ByVal e As DataGridSortCommandEventArgs) Handles dgrdStudentDetails.SortCommand
        BindDatagrid(e.SortExpression)
    End Sub
    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusCodeFacade
        With ddlStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = statuses.GetAllStatusCodesForStudents(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramsDDL()
        'Bind the CampusGroups DrowDownList
        Dim programs As New StatusesFacade
        With ddlProgram
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = programs.GetAllProgramVersions(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    'DE9010 2/12/2013 Janet Robinson
    Protected Sub txtDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs)
        DateChg = False
        If e.NewDate = e.OldDate Then
            btnGenerateInv.Enabled = True
        Else
            btnGenerateInv.Enabled = False
            DateChg = True
        End If

    End Sub
End Class
