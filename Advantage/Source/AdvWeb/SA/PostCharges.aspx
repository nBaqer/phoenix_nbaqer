﻿<%@ Page Title="Post Charges" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="PostCharges.aspx.vb" Inherits="PostCharges" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Post Charges</title>
    <script language="JavaScript" type="text/JavaScript">

        function setRange(control, validator) {
            var x = document.getElementById(control)
            var y = document.getElementById(validator)
            if (x.selectedIndex == 0) { y.minimumvalue = '0.01' } else { y.minimumvalue = '-1000000' }

        }
        function confirmCallBackFn(arg) {

            if (arg == true) {

                var oButton = document.getElementById('<%=Hdn.ClientID%>');
                //alert(oButton);
                oButton.click();

            }

        }
    </script>

    <style>
        .contentcell {
            width: 115px !important;
            padding: 5px;
            vertical-align: middle !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none;">
                            <tr>
                                <td class="detailsframe" style="width: 100%;">
                                    <FAME:StudentSearch ID="StudSearch1" runat="server" OnTransferToParent="TransferToParent" />
                                    <div class="boxContainer" id="DIVRHS" runat="server">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                        <asp:TextBox ID="txtPostChargeId" runat="server" Visible="False" Wrap="False">txtPostChargeId</asp:TextBox>
                                        <asp:CheckBox ID="chkIsReversal" runat="server" Visible="False"></asp:CheckBox><asp:TextBox
                                            ID="txtOriginalTransactionId" runat="server" Visible="False" Wrap="False">txtOriginalTransactionId</asp:TextBox>
                                        <asp:CheckBox ID="chkIsPosted" runat="server" Visible="False"></asp:CheckBox><asp:TextBox
                                            ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                        <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" border="0">
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="pnlStudent" runat="server" Visible="false">
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblStudent" runat="server" CssClass="label">Student</asp:Label>&nbsp;
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:TextBox ID="txtStudentName" TabIndex="1" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <div>
                                                                        <asp:Label ID="lblEnrollmentId" CssClass="lebel" runat="server" >Enrollment</asp:Label>&nbsp;
                                                                       
                                                                    </div>
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:TextBox ID="txtStuEnrollmentDesc" TabIndex="2" runat="server" CssClass="textbox"
                                                                        Enabled="false">StuEnrollment</asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblTermId" runat="server" CssClass="label">Term</asp:Label>&nbsp;
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:TextBox ID="txtTermDesc" TabIndex="3" runat="server" CssClass="textbox" Enabled="false">Term</asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="contentcell">
                                                                    <asp:Label ID="lblAcademicYearId" runat="server" CssClass="label">Academic Year</asp:Label>&nbsp;
                                                                </td>
                                                                <td class="contentcell">
                                                                    <asp:TextBox ID="txtAcademicYearDesc" TabIndex="4" runat="server" CssClass="textbox"
                                                                        Enabled="false">AcademicYear</asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTransactionTypeId" runat="server" CssClass="label">Transaction Type</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlTransTypeId" Style="margin-left: 9px;" Width="200px"
                                                        TabIndex="7" runat="server" BackColor="White" ForeColor="Black"
                                                        OnChange="javascript: setRange('ContentMain1_ddlTransTypeId','ContentMain1_AmountRangeValidator');"
                                                        AutoPostBack="False">
                                                        <Items>
                                                            <asp:ListItem Value="0" Text="Charge" />
                                                            <asp:ListItem Value="1" Text="Adjustment" />
                                                        </Items>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTransCode" runat="server" CssClass="label" >Transaction Code<font color="red">*</font></asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlTransCodeId" TabIndex="7" runat="server" Width="200px"
                                                        BackColor="White" ForeColor="Black" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    <asp:CompareValidator ID="TransCodeCompareValidator" runat="server" Display="None"
                                                        ErrorMessage="Transaction Code is Required" ControlToValidate="ddlTransCodeId"
                                                        Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Transaction Code is Required</asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTransDescrip" CssClass="label" runat="server">Description</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtTransDescrip" TabIndex="3" 
                                                        CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTransReference" runat="server" CssClass="label">Reference</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <div>
                                                        <asp:TextBox ID="txtTransReference" TabIndex="4"
                                                            runat="server" CssClass="textbox"></asp:TextBox>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTransDate" runat="server" CssClass="label" >Date</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <telerik:RadDatePicker ID="txtTransDate" MinDate="1/1/2001" Width="200px"
                                                        runat="server">
                                                    </telerik:RadDatePicker>
                                                    <asp:RequiredFieldValidator ID="DateReqFieldValidator" runat="server" Display="None"
                                                        ErrorMessage="Date is Required" ControlToValidate="txtTransDate">Date is Required</asp:RequiredFieldValidator><asp:RangeValidator
                                                            ID="drvTransactionDate" runat="server" Display="None" ErrorMessage="Date is Invalid or outside allowed range"
                                                            ControlToValidate="txtTransDate" MinimumValue="2001/01/01" MaximumValue="2049/12/31"
                                                            Type="Date">Date is Invalid or outside allowed range</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTransAmount" runat="server" CssClass="label">Amount</asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtTransAmount" TabIndex="9" runat="server" 
                                                      CssClass="textbox" MaxLength="12"></asp:TextBox>
                                                    <span class="label">&nbsp;<%=MyAdvAppSettings.AppSettings("Currency")%></span><asp:RequiredFieldValidator
                                                        ID="AmountReqFieldValidator" runat="server" Display="None" ErrorMessage="Amount is Required"
                                                        ControlToValidate="txtTransAmount">Amount is Required</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblFeeType" runat="server" CssClass="label">Fee Type:<font color="red">*</font></asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:RadioButtonList ID="rdnfeelevel" runat="server" CssClass="radiobuttonlist" RepeatDirection="Horizontal" 
                                                       >
                                                        <asp:ListItem Value="1">Term </asp:ListItem>
                                                        <asp:ListItem Value="2">Program Version</asp:ListItem>
                                                        <asp:ListItem Value="3">Course</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="None"
                                                        ErrorMessage="Fee Type is Required" ControlToValidate="rdnfeelevel">Fee Type is Required</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <asp:TextBox ID="txtStuEnrollmentId" runat="server" CssClass="textbox" Width="0px"
                                Visible="false">StuEnrollmentId</asp:TextBox><asp:TextBox ID="txtAcademicYearId"
                                    runat="server" CssClass="textbox" Width="0px" Visible="false">AcademicYearId</asp:TextBox><asp:TextBox
                                        ID="txtTermId" runat="server" CssClass="textbox" Width="0px" Visible="false">TermId</asp:TextBox><tr>
                                            <td class="contentcell" style="font-size: xx-small; color: #b71c1c; font-family: v, Verdana"></td>
                                            <td class="contentcell4"></td>
                                            <td class="emptycell">&nbsp;
                                            </td>
                                        </tr>
                        </table>
                        <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                                <tr>
                                    <td class="contentcellheader">
                                        <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                            </table>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="contentcell2">
                                        <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false">
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <!--end table content-->
                    </td>
                </tr>
            </table>

            <!-- end rightcolumn -->

            <asp:TextBox ID="txtTerm" Visible="false" TabIndex="6" runat="server" CssClass="textbox"
                ReadOnly="True">Term</asp:TextBox>
            <asp:TextBox ID="txtStuEnrollment" Visible="false" TabIndex="2" runat="server" CssClass="textbox"
                ReadOnly="True">StuEnrollment</asp:TextBox>
            <asp:TextBox ID="txtAcademicYear" Visible="false" TabIndex="5" runat="server" CssClass="textbox"
                ReadOnly="True">AcademicYear</asp:TextBox>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadWindowManager runat="server" ID="RadWindowManager1"></telerik:RadWindowManager>
    <asp:Button ID="Hdn" runat="server" OnClick="Hdn_Click" />
</asp:Content>
