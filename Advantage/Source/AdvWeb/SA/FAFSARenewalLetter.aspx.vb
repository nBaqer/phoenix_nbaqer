Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Partial Class FAFSARenewalLetter
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    'letter place holders
    Dim Student, Address, CityStateZip, StudentFirstName As String
    Dim Amount As Decimal = 0

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        GenerateLetter()
    End Sub
    Private Sub GenerateLetter()
        Dim ds As New DataSet
        Dim dr As DataRow
        Dim RecCount As Integer = 0
        '        Dim MyTotal As String

        Try
            ds = (New FAFSARenewalLetterFacade).GetFAFSALetterDS(Session("FAFSAStudentID"))

            SetUpLetter()

            For Each dr In ds.Tables(0).Rows

                If RecCount <> 0 Then
                    BreakPage()
                End If

                Student = dr("firstname").ToString() & " " & dr("middlename").ToString() & " " & dr("lastname").ToString()
                StudentFirstName = dr("firstname").ToString()
                Address = dr("Address1").ToString() & " " & dr("Address2").ToString()
                CityStateZip = dr("City").ToString() & ", " & dr("StateDescrip").ToString() & " " & dr("Zip").ToString()

                RecCount += 1

                WriteLetter()

            Next

            EndLetter()

            ds.Dispose()
            ds = Nothing

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub GenerateLetter" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub GenerateLetter" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")

        End Try
    End Sub
    Private Sub SetUpLetter()
        Response.Write("<HTML><HEAD><title>FAFSA Renewal Letter</title>")
        Response.Write("<link rel='stylesheet' type='text/css' href='../css/LetterForScreen.css' media='screen' />")
        Response.Write("<link rel='stylesheet' type='text/css' href='../css/LetterForPrinting.css' media='print' />")
        Response.Write("</HEAD><body class='letterbase'>")
    End Sub
    Private Sub WriteLetter()
        Response.Write("<table class='lettertable' width='670' ID='Table1' border=0 align='center'>")
        Response.Write("<tr><td class='tdpadding'>")
        Response.Write("<IMG SRC='../SY/getschoollogo.aspx' height='140' width='140'>")  '?schoolId=1
        Response.Write("<table cellspacing='0' cellpadding='0' width='100%' border=0>")
        Response.Write("<tr height='30'><td></td></tr><tr><td align='left' height='65' class='ArialTD'>" & Date.Now.Date)
        Response.Write("</td></tr><tr><td align='left' height='30' valign=bottom class='ArialTD'>" & Student & "</td>")
        Response.Write("</tr><tr><td align='left' class='ArialTD'>" & Address & "</td></tr><tr><td align='left' class='ArialTD'>")
        Response.Write(CityStateZip & "</td></tr><tr><td align='left' height='60' class='ArialTD'>Dear " & StudentFirstName)
        Response.Write(":</td></tr><tr><td align='left' height='100' valign='top' class='ArialTD'>")
        Response.Write("It is time to renew your Financial Aid due to a new award year starting on _______________.")
        Response.Write(" Please go to the following website:&nbsp;")
        Response.Write("<a href='http://www.fafsa.ed.gov'>www.fafsa.ed.gov</a>")
        Response.Write(" and complete the ____________ Free Application for Federal Student Aid(FAFSA). You will need ")
        Response.Write("to have your PIN number, ________ tax returns and if you had to provide your ")
        Response.Write("parents' taxes last award year, please remember to have that information as ")
        Response.Write("well.</td></tr><tr><td align='left' height='80' valign='top' class='ArialTD'>")
        Response.Write("Once you complete the application, AMCollege will receive your application ")
        Response.Write("information. We will then call you to setup an appointment to review your ")
        Response.Write("application and determine your award. If you wish to receive Financial Aid for ")
        Response.Write("the ____________, you must submit the application no later than ________________________.")
        Response.Write("</td></tr><tr height='40' valign='top' class='ArialTD'><td><br>We look forward to seeing you soon.")
        Response.Write("</td></tr><tr height='40' valign='top' class='ArialTD'><td><br>Sincerely,</td></tr><tr height='80' valign='top'>")
        Response.Write("<td class='ArialTD'><br><br>Financial Aid Department</td></tr><tr height='15'><td class='ArialTD'>")
        Response.Write("<br>P.S. If you do not remember your PIN, please go to ")
        Response.Write("<a href='http://www.pin.ed.gov'>www.pin.ed.gov</a></td></tr></tr></table>")
        Response.Write("</td></tr></table>")
    End Sub
    Private Sub WriteSpacers()
        Dim x As Integer
        For x = 1 To 12
            Response.Write("<br>")
        Next
    End Sub
    Private Sub EndLetter()
        Response.Write("</body></HTML>")
    End Sub

    Private Sub BreakPage()
        WriteSpacers()
        Response.Write("<BR CLASS=page><br>")
    End Sub
End Class
