﻿

Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.IO

Partial Class GLDistributions
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents btnhistory As Button
    Protected totalDebits As Decimal
    Protected totalCredits As Decimal
    Private campusId As String
    Dim pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        campusId = Master.CurrentCampusId
        Dim resourceId As Integer
        Dim advantageUserState As User = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        'userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then

            '   build drop-down lists
            BuildDropDownLists()

            '   get start and end dates for GLDistribution Dates
            GetStartAndEndDates()

            'initially the exportToExcell button should be off
            btnExportToExcell.Visible = False

            ' Configure Grid for not summary report
            dgrdGLDistributions.Columns(0).Visible = False  'From
            dgrdGLDistributions.Columns(1).Visible = False  'To 
            dgrdGLDistributions.Columns(2).Visible = True 'Date 
            dgrdGLDistributions.Columns(3).Visible = True 'Transaction 
            dgrdGLDistributions.Columns(5).Visible = False  'Transaction Description
            Session("GLSummaryActive") = False

        End If
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

    Private Sub BuildDropDownLists()
        BuildCampusGroupsDDL()
    End Sub
    Private Sub BindDatagrid()
        With New StudentsAccountsFacade
            Dim ds = .SearchGLDistributions(ddlCampGrpId.SelectedValue, Date.Parse(CType(txtTransDateFrom.SelectedDate, String)), Date.Parse(CType(txtTransDateTo.SelectedDate, String)), chkSummary.Checked)
            'ds.Tables(0).DefaultView.Sort = "Date"
            'dgrdGLDistributions.DataSource = ds.Tables(0).DefaultView
            'dgrdGLDistributions.DataBind()
            dgrdGLDistributions.DataSource = ds
            dgrdGLDistributions.DataBind()
        End With
    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            '.DataSource = campusGroups.GetAllCampusGroups()
            .DataSource = campusGroups.GetAllCampGroupsByCampusId(campusId)
            .DataBind()
        End With

    End Sub
    Private Sub GetStartAndEndDates()
        '   instantiate facade
        With New StudentsAccountsFacade
            '   get min and max dates from the GLDistributions Table
            Dim dates() As Date = .GetMinAndMaxDatesFromGLDistributions()

            '   fill text-boxes with min and max dates
            txtTransDateFrom.SelectedDate = CType(dates(0).ToShortDateString, Date?)
            txtTransDateTo.SelectedDate = CType(dates(1).ToShortDateString, Date?)
        End With

    End Sub

    Protected Sub dgrdGLDistributions_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdGLDistributions.ItemCreated
        Dim isChecked As Boolean
        If Session("GLSummaryActive") <> Nothing Then
            isChecked = CType(Session("GLSummaryActive"), Boolean)
        Else
            isChecked = chkSummary.Checked
        End If

        If isChecked Then
            dgrdGLDistributions.Columns(0).Visible = True  'From
            dgrdGLDistributions.Columns(1).Visible = True  'To 
            dgrdGLDistributions.Columns(2).Visible = False 'Date 
            dgrdGLDistributions.Columns(3).Visible = False 'Transaction 
            dgrdGLDistributions.Columns(5).Visible = True  'Transaction Description
        Else
            dgrdGLDistributions.Columns(0).Visible = False  'From
            dgrdGLDistributions.Columns(1).Visible = False  'To 
            dgrdGLDistributions.Columns(2).Visible = True 'Date 
            dgrdGLDistributions.Columns(3).Visible = True 'Transaction 
            dgrdGLDistributions.Columns(5).Visible = False  'Transaction Description
        End If
    End Sub

    Private Sub dgrdGLDistributions_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdGLDistributions.ItemDataBound
        '   accept only items (no header or footer records)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim cramount As Decimal
                Dim dbamount As Decimal
                Dim credit = CType(e.Item.DataItem, GeneralLedgerInfo).Credit
                Dim debit = CType(e.Item.DataItem, GeneralLedgerInfo).Debit


                cramount = CType(If(IsNothing(credit), 0, credit), Decimal)
                dbamount = CType(If(IsNothing(debit), 0, debit), Decimal)

                If (dbamount <> 0) Then
                    CType(e.Item.FindControl("lblDebits"), Label).Text = dbamount.ToString("c")
                End If

                If (cramount <> 0) Then
                    CType(e.Item.FindControl("lblCredits"), Label).Text = cramount.ToString("c")
                End If

                totalCredits += cramount
                totalDebits += dbamount

                btnExportToExcell.Visible = True
            Case ListItemType.Footer
                '   set debits amount
                CType(e.Item.FindControl("lblTotalDebits"), Label).Text = totalDebits.ToString("c")

                '   set credits amount
                CType(e.Item.FindControl("lblTotalCredits"), Label).Text = totalCredits.ToString("c")

        End Select
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        dgrdGLDistributions.EditItemIndex = -1
        BindDatagrid()
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step i - 1
            ClearControls(control.Controls(i))
        Next
        If control.GetType.ToString = "System.Web.UI.HtmlControls.HtmlTableCell" Then
            If Not control.GetType().GetProperty("SelectedItem") Is Nothing Then
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            Else
                Dim literal As LiteralControl = New LiteralControl
                control.Parent.Controls.Add(literal)
                literal.Text = CType(control.GetType().GetProperty("Text").GetValue(control, Nothing), String)
                control.Parent.Controls.Remove(control)
            End If
        End If
        Return
    End Sub

    Private Sub btnExportToExcell_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcell.Click


        Dim oStringWriter As StringWriter = New StringWriter
        Dim oHtmlTextWriter As HtmlTextWriter = New HtmlTextWriter(oStringWriter)

        'delete from the data grid all controls that are not literals
        ClearControls(dgrdGLDistributions)

        'render data grid
        dgrdGLDistributions.RenderControl(oHtmlTextWriter)

        'convert html to string
        Dim enc As Encoding = Encoding.UTF8
        Dim s As String = oStringWriter.ToString

        'move html content to a memory stream
        Dim documentMemoryStream As New MemoryStream
        documentMemoryStream.Write(enc.GetBytes(s), 0, s.Length)

        '   save document and document type in sessions variables
        Session("DocumentMemoryStream") = documentMemoryStream
        Session("ContentType") = "application/vnd.ms-excel"

        '   Register a javascript to open the report in another window
        Dim javascript As String = "<script>var origwindow=window.self;window.open('PrintAnyReport.aspx');</script>"
        Dim csType As Type = [GetType]()


        ClientScript.RegisterClientScriptBlock(csType, "NewWindow", javascript)

    End Sub

    Protected Sub chkSummary_CheckedChanged(sender As Object, e As EventArgs) Handles chkSummary.CheckedChanged
        Session("GLSummaryActive") = chkSummary.Checked
    End Sub
End Class
