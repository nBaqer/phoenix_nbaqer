<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="RateSchedules.aspx.vb" Inherits="RateSchedules" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstRateSchedules">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstRateSchedules" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstRateSchedules" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstRateSchedules" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters" style="position: relative; top: 5px; height: expression(document.body.clientHeight - 179 + 'px')">
                                <asp:DataList ID="dlstRateSchedules" runat="server" DataKeyField="RateScheduleId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# Container.DataItem("RateScheduleId")%>' Text='<%# Container.DataItem("RateScheduleDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <asp:Panel ID="pnlRHS" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table cellspacing="0" cellpadding="0" align="center" width="100%">
                                        <asp:TextBox ID="txtRateScheduleId" runat="server" Visible="False"></asp:TextBox>
                                        <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                        <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="label1" runat="server" CssClass="label">Code</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtRateScheduleCode" runat="server" CssClass="textbox"></asp:TextBox><asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="Code can not be blank" Display="None"
                                                    ControlToValidate="txtRateScheduleCode">Code can not be blank</asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblRateScheduleDescrip" CssClass="label" runat="server">Description</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtRateScheduleDescrip" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="rfvDescription" runat="server" ErrorMessage="Description can not be blank" Display="None"
                                                    ControlToValidate="txtRateScheduleDescrip">Description can not be blank</asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList><asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ErrorMessage="Must Select a Campus Group"
                                                    Display="None" ControlToValidate="ddlCampGrpId" ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual">Must Select a Campus Group</asp:CompareValidator></td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="labelbold" align="center" colspan="2" style="padding: 20px">Rate Schedule Details</td>
                                        </tr>
                                        <tr>
                                            <td align="center" width="100%">
                                                <asp:DataGrid ID="dgrdRateScheduleDetails" runat="server" EditItemStyle-Wrap="false" HeaderStyle-Wrap="true"
                                                    AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="Solid" ShowFooter="True" BorderColor="#E0E0E0" CellPadding="0"
                                                    BorderWidth="1px">
                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <AlternatingItemStyle HorizontalAlign="Center" CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <ItemStyle HorizontalAlign="Center" CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="datagridheaderstyle"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Min Units">
                                                            <HeaderStyle HorizontalAlign="Center" Width="11%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMinUnits" runat="server" CssClass="label" Text='<%# Container.DataItem("MinUnits") %>'>MinUnits</asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtFooterMinUnits" runat="server" CssClass="textbox" MaxLength="5" Width="80%"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditMinUnits" runat="server" CssClass="textbox" Text='<%# Container.DataItem("MinUnits") %>' Width="80%">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMinUnits" runat="server" ControlToValidate="txtEditMinUnits" Display="None"
                                                                    ErrorMessage="Min Units can not be blank">Min Units field can not be blank</asp:RequiredFieldValidator>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Max Units">
                                                            <HeaderStyle HorizontalAlign="Center" Width="11%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMaxUnits" CssClass="label" runat="server" Text='<%# Container.DataItem("MaxUnits") %>' Width="80%">MaxUnits</asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtFooterMaxUnits" runat="server" CssClass="textbox" MaxLength="5"
                                                                    Width="80%"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditMaxUnits" runat="server" CssClass="textbox" Text='<%# Container.DataItem("MaxUnits") %>' Width="80%">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvMaxUnits" runat="server" ControlToValidate="txtEditMaxUnits" Display="None"
                                                                    ErrorMessage="Max Units can not be blank">Max Units field can not be blank</asp:RequiredFieldValidator>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Tuition Category">
                                                            <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTuitionCategoryId" CssClass="label" runat="server" Text='<%# GetTuitionCategoryDescription(Container.DataItem) %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList ID="ddlFooterTuitionCategoryId" runat="server" CssClass="dropdownlist" Width="80%"></asp:DropDownList>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlEditTuitionCategoryId" runat="server" CssClass="dropdownlist" Width="80%"></asp:DropDownList>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Flat Amount">
                                                            <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFlatAmount" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FlatAmount", "{0:#,###.00}") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtFooterFlatAmount" runat="server" CssClass="textbox" MaxLength="9"
                                                                    Width="80%"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditFlatAmount" runat="server" CssClass="textbox" Text='<%# DataBinder.Eval(Container, "DataItem.FlatAmount", "{0:#,###.##}") %>' Width="80%">
                                                                </asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Rate">
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Rate", "{0:#,###.00}") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:TextBox ID="txtFooterRate" runat="server" CssClass="textbox" MaxLength="9"
                                                                    Width="80%"></asp:TextBox>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtEditRate" runat="server" CssClass="textbox" Text='<%# DataBinder.Eval(Container, "DataItem.Rate", "{0:#,###.##}") %>' Width="80%">
                                                                </asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Unit">
                                                            <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUnit" CssClass="label" runat="server" Text='<%# Container.DataItem("Unit") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:DropDownList ID="ddlFooterUnitId" runat="server" CssClass="dropdownlist" Width="80%">
                                                                    <asp:ListItem Value="0">Credit Hour</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlEditUnitId" runat="server" CssClass="dropdownlist" Width="80%" SelectedIndex='<%# Container.DataItem("UnitId") %>'>
                                                                    <asp:ListItem Value="0" Selected="True">Credit Hour</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                                            <ItemStyle Wrap="False" HorizontalAlign="center" Width="15%"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
                                                                    CausesValidation="False" runat="server" CommandName="Edit">
																		<img border="0" src="../images//im_edit.gif" alt="edit"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkButUp" Text="<img border=0 src=../images/up.gif alt=MoveUp>" CausesValidation="False"
                                                                    runat="server" CommandName="Up"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkButDown" Text="<img border=0 src=../images/down.gif alt=MoveDown>" CausesValidation="False"
                                                                    runat="server" CommandName="Down"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>" runat="server"
                                                                    CommandName="Update">
																		<img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>" CausesValidation="False"
                                                                    runat="server" CommandName="Delete"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
                                                                    CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="0" ItemStyle-Width="0" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" Width="0"></HeaderStyle>
                                                            <ItemStyle Width="0"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRateScheduleDetailId" runat="server" Text='<%# Container.DataItem("RateScheduleDetailId") %>' Visible="False">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblEditRateScheduleDetailId" runat="server" Text='<%# Container.DataItem("RateScheduleDetailId") %>' Visible="False">
                                                                </asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="customvalidator"
            Display="none"></asp:CustomValidator>
        <asp:Panel ID="pnlrequiredfieldvalidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="true"
            ShowSummary="false"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>

</asp:Content>


