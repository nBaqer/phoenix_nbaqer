' ===============================================================================
'
' FAME AdvantageV1
'
' PrintAnyReport.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports System.IO
Imports System.Diagnostics

Partial Class PrintAnyReport
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        '   Get document memory stream
        Dim documentMemoryStream As MemoryStream = CType(Session("DocumentMemoryStream"), MemoryStream)
        '   send report to the browser
        'Response.Clear()
        'Response.ContentType = CType(Session("ContentType"), String)
        'Response.AppendHeader("Content-Length", Convert.ToString(documentMemoryStream.Length))
        'Response.BinaryWrite(documentMemoryStream.ToArray())
        'Response.End()
        Const strFileName As String = "PrintReport.xls"
        HttpContext.Current.Response.Buffer = True
        HttpContext.Current.Response.Clear()
        Response.AppendHeader("Pragma", "no-cache")
        Response.AppendHeader("Cache-Control", "no-cache")
        Response.AppendHeader("max-age", "0")
        HttpContext.Current.Response.ClearHeaders()

        HttpContext.Current.Response.ContentType = "application/vnd.xls"


        HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + strFileName)

        HttpContext.Current.Response.Charset = ""
        EnableViewState = False
        'Dim oStringWriter As New StringWriter()
        'Dim oHtmlTextWriter As New HtmlTextWriter(oStringWriter)


        HttpContext.Current.Response.BinaryWrite(documentMemoryStream.ToArray())
        HttpContext.Current.Response.End()
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub

End Class
