Imports FAME.Common
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class TuitionEarnings
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents pnlEarningOptions As System.Web.UI.WebControls.Panel
    Protected WithEvents allDataset As System.Data.DataSet
    Protected WithEvents dataGridTable As System.Data.DataTable
    Protected WithEvents dataListTable As System.Data.DataTable


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

      If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   get allDataset from the DB
            allDataset = (New StudentsAccountsFacade).GetTuitionEarningsDS()

            '   save it on the session
            Session("AllDataset") = allDataset

            '   initialize buttons
            InitButtonsForLoad()

        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
        End If

        '   create Dataset and Tables
        CreateDatasetAndTables()

        '   the first time we have to bind an empty datagrid
        If Not IsPostBack Then
            PrepareForNewData()
        End If

    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind TuitionEarnings datalist
        'dlstTuitionEarnings.DataSource = New DataView((New StudentsAccountsFacade).GetAllTuitionEarnings().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstTuitionEarnings.DataSource = New DataView(dataListTable, rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstTuitionEarnings.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dlstTuitionEarnings_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstTuitionEarnings.ItemCommand

        '   this portion of code added to refresh data from the database
        '**********************************************************************************
        '   get allDataset from the DB
        '   bind datalist
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        BindDataList()

        allDataset = (New StudentsAccountsFacade).GetTuitionEarningsDS()

        '   save it on the session
        Session("AllDataset") = allDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()
        '**********************************************************************************

        '   save TuitionEarningId in session
        Dim strId As String = dlstTuitionEarnings.DataKeys(e.Item.ItemIndex).ToString()
        Session("TuitionEarningId") = strId

        '   get the row with the data to be displayed
        Dim row() As DataRow = dataListTable.Select("TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'")

        '   populate controls with row data
        txtTuitionEarningId.Text = CType(row(0)("TuitionEarningId"), Guid).ToString
        txtTuitionEarningsCode.Text = row(0)("TuitionEarningsCode")
        ddlStatusId.SelectedValue = CType(row(0)("StatusId"), Guid).ToString
        txtTuitionEarningsDescrip.Text = row(0)("TuitionEarningsDescrip")
        If (Not row(0)("CampGrpId") Is System.DBNull.Value) Then ddlCampGrpId.SelectedValue = CType(row(0)("CampGrpId"), Guid).ToString Else ddlCampGrpId.SelectedIndex = 0
        rblRevenueBasis.SelectedIndex = row(0)("RevenueBasisIdx")
        rblPercentageRangeBasis.SelectedIndex = row(0)("PercentageRangeBasisIdx")
        rblPercentToEarn.SelectedIndex = row(0)("PercentToEarnIdx")
        ''Added by Saraswathi on July 1 2009
        ''Take Holidays into account
        If Not row(0)("TakeHolidays") Is DBNull.Value Then
            chkHols.Checked = row(0)("TakeHolidays")
        Else
            chkHols.Checked = False
        End If

        ''Added by Saraswathi on May 19 2010
        ''To get the method for deferred revenue calculation
        If Not row(0)("RemainingMethod") Is DBNull.Value Then
            ChkDefRevenueMethod.Checked = row(0)("RemainingMethod")
        Else
            ChkDefRevenueMethod.Checked = False
        End If

        pnlPercentageRanges.Visible = ShouldDataGridBeVisible(rblPercentToEarn.SelectedIndex)
        ChkDefRevenueMethod.Enabled = ShouldChkDefRevenueMethodBeEnabled(rblPercentToEarn.SelectedIndex)
        chkHols.Enabled = ShouldChkTakeHolidaysBeEnabled(rblPercentToEarn.SelectedIndex)
        'If ShouldChkDefRevenueMethodBeEnabled(rblPercentToEarn.SelectedIndex) = True Then
        '    ChkDefRevenueMethod.Enabled = True
        'Else
        '    ChkDefRevenueMethod.Enabled = False
        '    ChkDefRevenueMethod.Checked = False
        'End If

        'If ShouldChkTakeHolidaysBeEnabled(rblPercentToEarn.SelectedIndex) = True Then
        '    chkHols.Enabled = True
        'Else
        '    chkHols.Enabled = False
        '    chkHols.Checked = False
        'End If


        If (Not row(0)("FullMonthBeforeDay") Is System.DBNull.Value) Then txtEarnAFullMonth.Text = row(0)("FullMonthBeforeDay") Else txtEarnAFullMonth.Text = ""
        If (Not row(0)("HalfMonthBeforeDay") Is System.DBNull.Value) Then txtEarnAHalfMonth.Text = row(0)("HalfMonthBeforeDay") Else txtEarnAHalfMonth.Text = ""
        If (Not row(0)("IsAttendanceRequired") Is System.DBNull.Value) Then chkIsAttendanceRequired.Checked = row(0)("IsAttendanceRequired") Else chkIsAttendanceRequired.Checked = False
        If (Not row(0)("RequiredAttendancePercent") Is System.DBNull.Value) Then txtRequiredAttendancePercent.Text = row(0)("RequiredAttendancePercent") Else txtRequiredAttendancePercent.Text = ""
        If (Not row(0)("RequiredCumulativeHoursAttended") Is System.DBNull.Value) Then txtRequiredCumulativeHoursAttended.Text = row(0)("RequiredCumulativeHoursAttended") Else txtRequiredCumulativeHoursAttended.Text = ""
        'If (Not row(0)("ModUser") Is System.DBNull.Value) Then txtModUser.Text = row(0)("ModUser") Else txtModUser.Text = ""
        'If (Not row(0)("ModDate") Is System.DBNull.Value) Then txtModDate.Text = CType(row(0)("ModDate"), Date).ToString Else txtModDate.Text = Date.Now

        '   bind datagrid
        dgrdTuitionEarningsPercentageRanges.DataSource = New DataView(dataGridTable, "TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
        dgrdTuitionEarningsPercentageRanges.DataBind()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstTuitionEarnings, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        '   show footer
        dgrdTuitionEarningsPercentageRanges.ShowFooter = True

        CommonWebUtilities.RestoreItemValues(dlstTuitionEarnings, CType(Session("TuitionEarningId"), String))
    End Sub
    Private Sub BindTuitionEarningsData(ByVal TuitionEarnings As TuitionEarningsInfo)
        With TuitionEarnings
            chkIsInDB.Checked = .IsInDB
            txtTuitionEarningId.Text = .TuitionEarningId
            txtTuitionEarningsCode.Text = .Code
            If Not (TuitionEarnings.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = TuitionEarnings.StatusId
            txtTuitionEarningsDescrip.Text = .Description
            If Not (TuitionEarnings.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = TuitionEarnings.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            rblRevenueBasis.SelectedIndex = TuitionEarnings.RevenueBasis
            rblPercentageRangeBasis.SelectedIndex = TuitionEarnings.PercentageRangeBasis
            rblPercentToEarn.SelectedIndex = TuitionEarnings.PercentToEarn

            ''Added by Saraswathi on July 1 2009
            ''Take Holidays into account
            chkHols.Checked = TuitionEarnings.TakeHolidays
            ''Added by Saraswathi onMay 19 2010
            ChkDefRevenueMethod.Checked = TuitionEarnings.defRevenueMethod


            pnlPercentageRanges.Visible = ShouldDataGridBeVisible(rblPercentToEarn.SelectedIndex)
            ChkDefRevenueMethod.Enabled = ShouldChkDefRevenueMethodBeEnabled(rblPercentToEarn.SelectedIndex)
            chkHols.Enabled = ShouldChkTakeHolidaysBeEnabled(rblPercentToEarn.SelectedIndex)


            txtEarnAFullMonth.Text = TuitionEarnings.FullMonthBeforeDay
            txtEarnAHalfMonth.Text = TuitionEarnings.HalfMonthBeforeDay
            chkIsAttendanceRequired.Checked = TuitionEarnings.IsAttendanceRequired
            txtRequiredAttendancePercent.Text = TuitionEarnings.RequiredAttendancePercent
            txtRequiredCumulativeHoursAttended.Text = TuitionEarnings.RequiredCumulativeHoursAttended
            'txtModUser.Text = .ModUser
            'txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   if any of the footer fields of the datagrid is not blank.. send an error message 
        If Not AreFooterFieldsBlank() Then
            '   Display Error Message
            DisplayErrorMessage("To use ""Save"" the fields ""Up To"" and ""Earn Percent"" must be blank.")
        End If
        '   if TuitionEarningId does not exist do an insert else do an update
        If Session("TuitionEarningId") Is Nothing Then
            '   do an insert
            BuildNewRowInDataList(Guid.NewGuid.ToString)
        Else
            '   do an update

            '   get the row with the data to be displayed
            Dim row() As DataRow = dataListTable.Select("TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'")

            '   update row data
            UpdateRowData(row(0))

        End If

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors then bind and set selected style to the datalist and
        '   initialize buttons 
        If Customvalidator1.IsValid Then

            ''   bind GradeSystems datalist
            'dlstTuitionEarnings.DataSource = New DataView(dataListTable, Nothing, "TuitionEarningsDescrip asc", DataViewRowState.CurrentRows)
            'dlstTuitionEarnings.DataBind()
            BindDataList()

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstTuitionEarnings, Session("TuitionEarningId"), ViewState, Header1)

            '   initialize buttons
            InitButtonsForEdit()

        End If

        CommonWebUtilities.RestoreItemValues(dlstTuitionEarnings, CType(Session("TuitionEarningId"), String))
    End Sub
    'Private Function BuildTuitionEarningsInfo(ByVal TuitionEarningId As String) As TuitionEarningsInfo

    '    '   instantiate class
    '    Dim TuitionEarningsInfo As New TuitionEarningsInfo

    '    With TuitionEarningsInfo
    '        '   get IsInDB
    '        .IsInDB = chkIsInDB.Checked

    '        '   get TuitionEarningId
    '        .TuitionEarningId = TuitionEarningId

    '        '   get Code
    '        .Code = txtTuitionEarningsCode.Text.Trim

    '        '   get StatusId
    '        .StatusId = ddlStatusId.SelectedValue

    '        '   get TuitionEarnings's name
    '        .Description = txtTuitionEarningsDescrip.Text.Trim

    '        '   get Campus Group
    '        .CampGrpId = ddlCampGrpId.SelectedValue

    '        '   get Revenue Basis
    '        .RevenueBasis = rblRevenueBasis.SelectedIndex

    '        '   get Percent To earn
    '        .PercentToEarn = rblPercentToEarn.SelectedIndex

    '        '   get Apply Full Month after this date
    '        .FullMonthBeforeDay = txtEarnAFullMonth.Text.Trim

    '        '   get Apply Full Month after this date
    '        .HalfMonthBeforeDay = txtEarnAHalfMonth.Text.Trim

    '        '   get IsAttendanceRequired
    '        .IsAttendanceRequired = chkIsAttendanceRequired.Checked

    '        '   get Required Attendace Percent
    '        .RequiredAttendancePercent = txtRequiredAttendancePercent.Text

    '        '   get Required Cumulative Hours Attended
    '        .RequiredCumulativeHoursAttended = txtRequiredCumulativeHoursAttended.Text.Trim

    '        '   get ModUser
    '        .ModUser = txtModUser.Text

    '        '   get ModDate
    '        .ModDate = Date.Parse(txtModDate.Text)

    '    End With

    '    '   return data
    '    Return TuitionEarningsInfo

    'End Function
    'Private Sub chkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged
    '    '   bind the datalist
    '    BindDataList(chkStatus.Checked)

    'End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        ''   try to update the DB and show any error message
        'UpdateDB()

        ''   if there were no errors prepare screen for new data
        'If Customvalidator1.IsValid Then
        '    '   Prepare screen for new data
        '    PrepareForNewData()

        '   reset dataset to previous state. delete all changes
        allDataset.RejectChanges()
        PrepareForNewData()

        '   reset Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstTuitionEarnings, Guid.Empty.ToString, ViewState, Header1)

        CommonWebUtilities.RestoreItemValues(dlstTuitionEarnings, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        '   get all rows to be deleted
        Dim rows() As DataRow = dataGridTable.Select("TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'")

        '   delete all rows from dataGrid table
        Dim i As Integer
        If rows.Length > 0 Then
            For i = 0 To rows.Length - 1
                rows(i).Delete()
            Next
        End If

        '   get the row to be deleted in TuitionEarnings table
        Dim row() As DataRow = dataListTable.Select("TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'")

        '   delete row from the table
        row(0).Delete()

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   Prepare screen for new data
            PrepareForNewData()
        End If
        CommonWebUtilities.RestoreItemValues(dlstTuitionEarnings, Guid.Empty.ToString)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    'Private Sub GetTuitionEarningId(ByVal TuitionEarningId As String)
    '    '   Create a StudentsFacade Instance
    '    Dim saf As New StudentsAccountsFacade

    '    '   bind TuitionEarnings properties
    '    BindTuitionEarningsData(saf.GetTuitionEarningsInfo(TuitionEarningId))
    'End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Sub CreateDatasetAndTables()

        '   Get dataGrid Dataset from the session and create tables
        allDataset = Session("AllDataset")
        dataGridTable = allDataset.Tables("TuitionEarningsPercentageRanges")
        dataListTable = allDataset.Tables("TuitionEarnings")

    End Sub
    Private Sub PrepareForNewData()

        ''   bind TuitionEarnings datalist
        'dlstTuitionEarnings.DataSource = dataListTable
        'dlstTuitionEarnings.DataBind()
        BindDataList()

        '   save TuitionEarningId in session
        Session("TuitionEarningId") = Nothing

        '   bind an empty TuitionEarningsPercentageRanges datagrid
        dgrdTuitionEarningsPercentageRanges.DataSource = New DataView(dataGridTable, "TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
        dgrdTuitionEarningsPercentageRanges.DataBind()

        '   bind an empty new TuitionEarningsInfo
        BindTuitionEarningsData(New TuitionEarningsInfo)

        '   initialize buttons
        InitButtonsForLoad()

    End Sub
    Private Sub SwapViewOrders(ByVal id1 As String, ByVal id2 As String)

        '   get the rows with each id
        Dim row1(), row2() As DataRow
        row1 = dataGridTable.Select("TuitionEarningsPercentageRangeId=" + "'" + id1 + "'")
        row2 = dataGridTable.Select("TuitionEarningsPercentageRangeId=" + "'" + id2 + "'")

        '   swap value of viewOrder
        Dim viewOrder As Integer
        viewOrder = row1(0)("ViewOrder")
        row1(0)("ViewOrder") = row2(0)("ViewOrder")
        row2(0)("ViewOrder") = viewOrder
    End Sub

    Private Sub dgrdTuitionEarningsPercentageRanges_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTuitionEarningsPercentageRanges.ItemCommand
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdTuitionEarningsPercentageRanges.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdTuitionEarningsPercentageRanges.ShowFooter = False

                '   user hit "Update" inside the datagrid
            Case "Update"

                '   get the row to be updated
                Dim row() As DataRow = dataGridTable.Select("TuitionEarningsPercentageRangeId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditTuitionEarningsPercentageRangeId"), Label).Text).ToString + "'")

                '   fill new values in the row
                row(0)("UpTo") = CType(e.Item.FindControl("txtEditUpTo"), TextBox).Text
                row(0)("EarnPercent") = CType(e.Item.FindControl("txtEditEarnPercent"), TextBox).Text
                row(0)("ModUser") = Session("UserName")
                row(0)("ModDate") = Date.Now

                '   no record is selected
                dgrdTuitionEarningsPercentageRanges.EditItemIndex = -1

                '   show footer
                dgrdTuitionEarningsPercentageRanges.ShowFooter = True

                '   user hit "Cancel"
            Case "Cancel"

                '   set no record selected
                dgrdTuitionEarningsPercentageRanges.EditItemIndex = -1

                '   show footer
                dgrdTuitionEarningsPercentageRanges.ShowFooter = True

                '   user hit "Delete" inside the datagrid
            Case "Delete"

                '   get the row to be deleted
                Dim row() As DataRow = dataGridTable.Select("TuitionEarningsPercentageRangeId=" + "'" + CType(e.Item.FindControl("lblEditTuitionEarningsPercentageRangeId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                '   set no record selected
                dgrdTuitionEarningsPercentageRanges.EditItemIndex = -1

                '   show footer
                dgrdTuitionEarningsPercentageRanges.ShowFooter = True

            Case "AddNewRow"
                '   process only if UpTo and EarnPercent have some data
                If IsDataInFooterTextboxesValid(e.Item.FindControl("txtFooterUpTo"), e.Item.FindControl("txtFooterEarnPercent")) Then
                    '   If the dataList item doesn't have an Id assigned.. create one and assign it.
                    If Session("TuitionEarningId") Is Nothing Then
                        '   build a new row in dataList table
                        BuildNewRowInDataList(Guid.NewGuid.ToString)
                    End If

                    '   get a new row from the dataGridTable
                    Dim newRow As DataRow = dataGridTable.NewRow

                    '   fill the new row with values
                    newRow("TuitionEarningsPercentageRangeId") = Guid.NewGuid
                    newRow("TuitionEarningId") = New Guid(CType(Session("TuitionEarningId"), String))
                    newRow("UpTo") = CType(e.Item.FindControl("txtFooterUpTo"), TextBox).Text
                    newRow("EarnPercent") = CType(e.Item.FindControl("txtFooterEarnPercent"), TextBox).Text
                    newRow("ViewOrder") = GetMaxViewOrder(dataGridTable) + 1
                    newRow("ModUser") = Session("UserName")
                    newRow("ModDate") = Date.Now

                    '   add row to the table
                    newRow.Table.Rows.Add(newRow)
                Else
                    DisplayErrorMessage("Invalid data during adding a new row of Percentage Ranges")
                    Exit Sub
                End If

                '   user hit "Up"
            Case "Up"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(dataGridTable, "TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim TuitionEarningsPercentageRangeId As String = CType(e.Item.FindControl("lblTuitionEarningsPercentageRangeId"), Label).Text

                '   get the position of this item and swap viewOrder values with prevous item
                Dim i As Integer
                For i = 1 To dv.Count - 1
                    If CType(dv(i).Row("TuitionEarningsPercentageRangeId"), Guid).ToString = TuitionEarningsPercentageRangeId Then
                        SwapViewOrders(CType(dv(i).Row("TuitionEarningsPercentageRangeId"), Guid).ToString, CType(dv(i - 1).Row("TuitionEarningsPercentageRangeId"), Guid).ToString)
                        Exit For
                    End If
                Next

                '   user hit "Down" 
            Case "Down"
                '   get a view with data ordered by ViewOrder
                Dim dv As New DataView(dataGridTable, "TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                '   get the id of the item selected by the user
                Dim TuitionEarningsPercentageRangeId As String = CType(e.Item.FindControl("lblTuitionEarningsPercentageRangeId"), Label).Text

                '   get the position of this item and swap viewOrder values with next item
                Dim i As Integer
                For i = 0 To dv.Count - 2
                    If CType(dv(i).Row("TuitionEarningsPercentageRangeId"), Guid).ToString = TuitionEarningsPercentageRangeId Then
                        SwapViewOrders(CType(dv(i).Row("TuitionEarningsPercentageRangeId"), Guid).ToString, CType(dv(i + 1).Row("TuitionEarningsPercentageRangeId"), Guid).ToString)
                        Exit For
                    End If
                Next

        End Select

        '   bind datagrid to dataGridTable only if there were no validation errors
        dgrdTuitionEarningsPercentageRanges.DataSource = New DataView(dataGridTable, "TuitionEarningId=" + "'" + CType(Session("TuitionEarningId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)
        dgrdTuitionEarningsPercentageRanges.DataBind()

    End Sub
    Private Function GetMaxViewOrder(ByVal table As DataTable) As Integer
        Dim i As Integer
        GetMaxViewOrder = 1

        '   if there are no rows just return 1 
        Dim rows() As DataRow = table.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If rows.Length = 0 Then Return 1

        For i = 0 To rows.Length - 1
            If CType(rows(i)("ViewOrder"), Integer) > GetMaxViewOrder Then
                GetMaxViewOrder = CType(rows(i)("ViewOrder"), Integer)
            End If
        Next
    End Function
    Private Sub BuildNewRowInDataList(ByVal theGuid As String)
        '   get a new row
        Dim newRow As DataRow = dataListTable.NewRow

        '   create a Guid for DataList if it has not been created 
        Session("TuitionEarningId") = theGuid
        newRow("TuitionEarningId") = New Guid(theGuid)

        '   update row with web controls data
        UpdateRowData(newRow)

        '   add row to the table
        newRow.Table.Rows.Add(newRow)

    End Sub
    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   initialize buttons
            InitButtonsForLoad()

            '   bind an empty datagrid
            PrepareForNewData()
        End If

    End Sub
    Private Sub UpdateDB()
        '   update DB
        With New StudentsAccountsFacade
            Dim result As String = .UpdateTuitionEarningsDS(allDataset)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                allDataset.RejectChanges()
            Else
                '   get a new version of TuitionEarnings dataset because some other users may have added,
                '   updated or deleted records from the DB
                '   bind datalist

                allDataset = .GetTuitionEarningsDS()
                Session("AllDataset") = allDataset
                CreateDatasetAndTables()
            End If
        End With
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
            If allDataset.HasChanges() Then
                'Set the Delete Button so it prompts the user for confirmation when clicked
                btnnew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
            Else
                btnnew.Attributes.Remove("onclick")
            End If

            '   save dataset systems in session
            Session("AllDataset") = allDataset

            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnsave)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            BindToolTip()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub
    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdTuitionEarningsPercentageRanges.Controls(0).Controls(dgrdTuitionEarningsPercentageRanges.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtFooterUpTo"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterEarnPercent"), TextBox).Text = "" Then Return False
        Return True
    End Function
    Private Sub UpdateRowData(ByVal row As DataRow)
        '   update row data
        row("TuitionEarningsCode") = txtTuitionEarningsCode.Text
        row("StatusId") = New Guid(ddlStatusId.SelectedValue)
        row("TuitionEarningsDescrip") = txtTuitionEarningsDescrip.Text
        row("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
        row("RevenueBasisIdx") = rblRevenueBasis.SelectedIndex
        row("PercentageRangeBasisIdx") = rblPercentageRangeBasis.SelectedIndex
        row("PercentToEarnIdx") = rblPercentToEarn.SelectedIndex
        ''Added by Saraswathi On july 1 2009
        row("TakeHolidays") = chkHols.Checked
        ''Added by Saraswathi Lakshmanan on May 19 2010
        row("RemainingMethod") = ChkDefRevenueMethod.Checked

        row("FullMonthBeforeDay") = txtEarnAFullMonth.Text
        row("HalfMonthBeforeDay") = txtEarnAHalfMonth.Text
        row("IsAttendanceRequired") = CType(chkIsAttendanceRequired.Checked, Boolean)
        row("RequiredAttendancePercent") = txtRequiredAttendancePercent.Text
        row("RequiredCumulativeHoursAttended") = txtRequiredCumulativeHoursAttended.Text
        row("ModUser") = Session("UserName")
        row("ModDate") = Date.Parse(Date.Now.ToString)
    End Sub
    Private Function IsDataInFooterTextboxesValid(ByVal textBoxUpTo As TextBox, ByVal textBoxEarnPercent As TextBox) As Boolean
        If Not IsFooterTextBoxValid(textBoxUpTo) Then Return False
        If Not IsFooterTextBoxValid(textBoxEarnPercent) Then Return False
        Return True
    End Function
    Private Function IsFooterTextBoxValid(ByVal textbox As TextBox) As Boolean
        Try
            Dim i As Integer = Double.Parse(textbox.Text)
            If i >= 0 And i <= 100 Then Return True
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
        Return False
    End Function
    ''Added by Saraswathi lakshmanan on may 19 2010
    Protected Function ShouldChkTakeHolidaysBeEnabled(ByVal idx As Integer) As Boolean
        If idx = 2 Or idx = 0 Then Return True Else Return False
    End Function
    ''Added by Saraswathi lakshmanan on may 19 2010
    Protected Function ShouldChkDefRevenueMethodBeEnabled(ByVal idx As Integer) As Boolean
        If idx = 1 Or idx = 0 Then Return True Else Return False
    End Function

    Protected Function ShouldDataGridBeVisible(ByVal idx As Integer) As Boolean
        'If idx = 1 Then Return True Else Return False
        ''Modified by Saraswathi lakshmanan on July 1 2009
        If idx = 2 Then Return True Else Return False
    End Function

    Private Sub rblPercentToEarn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rblPercentToEarn.SelectedIndexChanged
        pnlPercentageRanges.Visible = ShouldDataGridBeVisible(rblPercentToEarn.SelectedIndex)

        If ShouldChkDefRevenueMethodBeEnabled(rblPercentToEarn.SelectedIndex) = True Then
            ChkDefRevenueMethod.Enabled = True
        Else
            ChkDefRevenueMethod.Enabled = False
            ChkDefRevenueMethod.Checked = False
        End If

        If ShouldChkTakeHolidaysBeEnabled(rblPercentToEarn.SelectedIndex) = True Then
            chkHols.Enabled = True
        Else
            chkHols.Enabled = False
            chkHols.Checked = False
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        allDataset.RejectChanges()
        PrepareForNewData()
        '   bind datalist
        BindDataList()
        'Header1.EnableHistoryButton(False)
    End Sub

End Class
