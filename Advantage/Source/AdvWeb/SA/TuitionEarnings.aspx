<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="TuitionEarnings.aspx.vb" Inherits="TuitionEarnings" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstTuitionEarnings">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstTuitionEarnings" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstTuitionEarnings" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstTuitionEarnings" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstTuitionEarnings" runat="server" DataKeyField="TuitionEarningId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# Container.DataItem("TuitionEarningId")%>' Text='<%# Container.DataItem("TuitionEarningsDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                        <tr>
                                            <td>
                                                <table class="contenttable" cellspacing="2" cellpadding="2" width="100%" align="center">
                                                    <asp:TextBox ID="txtTuitionEarningId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="label1" runat="server" CssClass="label">Code</asp:Label></td>
                                                        <td class="contentcell4">
                                                            <asp:TextBox ID="txtTuitionEarningsCode" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox><asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtTuitionEarningsCode" Display="None"
                                                                ErrorMessage="Code can not be blank">Code can not be blank</asp:RequiredFieldValidator></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblStatusId" runat="server" CssClass="label">Status</asp:Label></td>
                                                        <td class="contentcell4">
                                                            <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblTuitionEarningsDescrip" runat="server" CssClass="label">Description</asp:Label></td>
                                                        <td class="contentcell4">
                                                            <asp:TextBox ID="txtTuitionEarningsDescrip" runat="server" CssClass="textbox" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtTuitionEarningsDescrip"
                                                                Display="None" ErrorMessage="Description can not be blank">Description can not be blank</asp:RequiredFieldValidator></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell">
                                                            <asp:Label ID="lblCampGrpId" runat="server" CssClass="label">Campus Group</asp:Label></td>
                                                        <td class="contentcell4">
                                                            <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList><asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ControlToValidate="ddlCampGrpId"
                                                                Display="None" ErrorMessage="Must Select a Campus Group" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Campus Group</asp:CompareValidator></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Panel ID="pnlRevenueBasis" runat="server" CssClass="label" Visible="False" BorderWidth="1px">
                                                                Revenue Basis 
                                                                <asp:RadioButtonList ID="rblRevenueBasis" runat="server" CssClass="label" RepeatDirection="Horizontal"
                                                                    Width="380px">
                                                                    <asp:ListItem Value="Months" Selected="True">Months</asp:ListItem>
                                                                    <asp:ListItem Value="Lessons/Exams">Lessons/Exams</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Panel ID="pnlPercentToEarn" runat="server" CssClass="label">
                                                                <h3>Earn Revenue By: </h3>
                        <table>
                            <tr>
                                <td colspan="2">
                                    <asp:RadioButtonList ID="rblPercentToEarn" runat="server" CssClass="label" AutoPostBack="True" >
                                        <asp:ListItem Value="Percentage based on calendar days in a month" Selected="True">Percentage based on calendar days in a month</asp:ListItem>
                                        <asp:ListItem Value="Equal Percentage every month [without holidays]">Equal Percentage every month [without holidays]</asp:ListItem>
                                        <asp:ListItem Value="Specify Percentage Range">Specify Percentage Range</asp:ListItem>

                                    </asp:RadioButtonList></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkHols" Text="Take Holidays into account" CssClass="checkbox" runat="server" /></td>
                                <td>
                                    <asp:CheckBox ID="ChkDefRevenueMethod" Text="Calculate revenue based on remaining method" CssClass="checkbox" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEarnFullMonth" runat="server" CssClass="label">Cut-off day to earn full month of revenue</asp:Label>
                                    <asp:RangeValidator ID="rvFullMonth" runat="server" ErrorMessage="Day in Earn a Full Month ... must be between 0 and 31"
                                        Display="None" ControlToValidate="txtEarnAFullMonth" Type="Integer" MinimumValue="0" MaximumValue="31"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvEarnAFullMonth" runat="server" ErrorMessage="Cut-off day to earn full month ... can not be blank"
                                        Display="None" ControlToValidate="txtEarnAFullMonth">Cut-off day to earn earn full month ... can not be blank</asp:RequiredFieldValidator></td>
                                <td>
                                    <asp:TextBox ID="txtEarnAFullMonth" runat="server" CssClass="textbox" Width="25px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEarnHalfMonth" runat="server" CssClass="label">Cut-off day to earn half month of revenue</asp:Label>
                                    <asp:RangeValidator ID="rvHalfMonth" runat="server" ErrorMessage="Cut-off day to earn half month ... must be between 0 and 31"
                                        Display="None" ControlToValidate="txtEarnAHalfMonth" Type="Integer" MinimumValue="0" MaximumValue="31"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvEarnAHalfMonth" runat="server" ErrorMessage="Cut-off day to earn earn half month ... can not be blank"
                                        Display="None" ControlToValidate="txtEarnAHalfMonth">Cut-off day to earn earn half month ... can not be blank</asp:RequiredFieldValidator></td>
                                <td>
                                    <asp:TextBox ID="txtEarnAHalfMonth" runat="server" CssClass="textbox" Width="25px"></asp:TextBox></td>
                            </tr>
                        </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Panel ID="AttendanceParameters" runat="server" CssClass="label" Visible="False" BorderWidth="1px">
                                                                Attendance Parameters 
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAttendanceRequired" runat="server" CssClass="label">Attendance Required</asp:Label></td>
                                <td>
                                    <asp:CheckBox ID="chkIsAttendanceRequired" runat="server" CssClass="label"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblRequiredAttendancePercent" runat="server" CssClass="label">Required Attendance Percent</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtRequiredAttendancePercent" runat="server" CssClass="textbox" Width="25px"></asp:TextBox>
                                    <asp:RangeValidator ID="rvRequiredAttendance" runat="server" ErrorMessage="Required Attendance Percentage must be between 0 and 100"
                                        Display="None" ControlToValidate="txtRequiredAttendancePercent" Type="Integer" MinimumValue="0"
                                        MaximumValue="100"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required Attendance Percent can not be blank"
                                        Display="None" ControlToValidate="txtRequiredAttendancePercent">Required Attendance Percent can not be blank</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblRequiredCumulativeHoursAttended" runat="server" CssClass="label">Required Cumulative Hours Attended</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtRequiredCumulativeHoursAttended" runat="server" CssClass="textbox" Width="50px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvRequiredCumulativeHours" runat="server" ErrorMessage="Required Cumulative Hours can not be blank"
                                        Display="None" ControlToValidate="txtRequiredCumulativeHoursAttended">Required Cumulative Hours can not be blank</asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rvRequiredCumulativeHours" runat="server" ErrorMessage="Required Cumulative Hours must be between 0 and 10000"
                                        Display="None" ControlToValidate="txtRequiredCumulativeHoursAttended" Type="Integer" MinimumValue="0"
                                        MaximumValue="10000"></asp:RangeValidator></td>
                            </tr>
                        </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center">
                                                <asp:Panel ID="pnlPercentageRanges" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButtonList ID="rblPercentageRangeBasis" runat="server" CssClass="radiobuttonlist" Width="160px">
                                                                    <asp:ListItem Value="0" Selected="True">Program Length</asp:ListItem>
                                                                    <asp:ListItem Value="1">Credits Earned</asp:ListItem>
                                                                    <asp:ListItem Value="2">Credits Attempted</asp:ListItem>
                                                                    <asp:ListItem Value="3">Courses Completed</asp:ListItem>
                                                                    <asp:ListItem Value="4">Hours Scheduled</asp:ListItem>
                                                                    <asp:ListItem Value="5">Hours Completed</asp:ListItem>
                                                                </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:DataGrid ID="dgrdTuitionEarningsPercentageRanges" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    BorderStyle="Solid" ShowFooter="True">
                                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="Up To %">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblUpTo" runat="server" Text='<%# Container.DataItem("UpTo") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="txtFooterUpTo" runat="server" CssClass="textbox" Width="50px"></asp:TextBox>
                                                                                <asp:RangeValidator ID="rvFooterUpTo" runat="server" ControlToValidate="txtFooterUpTo" Display="None"
                                                                                    ErrorMessage='"Up To" value must be between 0 and 100 ' MaximumValue="100" MinimumValue="0" Type="Double">"Up To" value must be between 0 and 100 </asp:RangeValidator>
                                                                                <asp:RegularExpressionValidator ID="regexValidator" runat="server" ControlToValidate="txtFooterUpTo" Display="None"
                                                                                    ErrorMessage='"Up To" value accepts only 2 decimal places ' ValidationExpression="(?!^0*$)(?!^0*\.0*$)^\d{1,3}(\.\d{1,2})?$"></asp:RegularExpressionValidator>
                                                                            </FooterTemplate>
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtEditUpTo" Style="text-align: center" runat="server" CssClass="textbox" Text='<%# Container.DataItem("UpTo") %>' Width="50px">
                                                                                </asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvUpTo" runat="server" ControlToValidate="txtEditUpTo" Display="None" ErrorMessage="Up To field can not be blank">Up To field can not be blank</asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="rvUpTo" runat="server" ControlToValidate="txtEditUpTo" Display="None" ErrorMessage='"Up To" value must be between 0 and 100 '
                                                                                    MaximumValue="100" MinimumValue="0" Type="Double">"Up To" value must be between 0 and 100 </asp:RangeValidator>
                                                                                <asp:RegularExpressionValidator ID="regexValidator2" runat="server" ControlToValidate="txtEditUpTo" Display="None"
                                                                                    ErrorMessage='"Up To" value accepts only 2 decimal places ' ValidationExpression="(?!^0*$)(?!^0*\.0*$)^\d{1,3}(\.\d{1,2})?$"></asp:RegularExpressionValidator>
                                                                            </EditItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Earn %">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEarnPercent" runat="server" Text='<%# Container.DataItem("EarnPercent") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="txtFooterEarnPercent" runat="server" CssClass="textbox" Width="50px"></asp:TextBox>
                                                                                <asp:RangeValidator ID="rvFooterEarnPercent" runat="server" ControlToValidate="txtFooterEarnPercent"
                                                                                    Display="None" ErrorMessage='"Earn Percent" value must be between 0 and 100 ' MaximumValue="100"
                                                                                    MinimumValue="0" Type="Double">"Earn Percent" value must be between 0 and 100 </asp:RangeValidator>
                                                                                <asp:RegularExpressionValidator ID="regexValidator1" runat="server" ControlToValidate="txtFooterEarnPercent" Display="None"
                                                                                    ErrorMessage='"Earn Percent" value accepts only 2 decimal places ' ValidationExpression="(?!^0*$)(?!^0*\.0*$)^\d{1,3}(\.\d{1,2})?$"></asp:RegularExpressionValidator>
                                                                            </FooterTemplate>
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtEditEarnPercent" Style="text-align: center" runat="server" CssClass="textbox" Text='<%# Container.DataItem("EarnPercent") %>' Width="50px">
                                                                                </asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvEarnPercent" runat="server" ControlToValidate="txtEditEarnPercent" Display="None"
                                                                                    ErrorMessage="Earn Percent field can not be blank">Earn Percent field can not be blank</asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="rvEarnPercent" runat="server" ControlToValidate="txtEditEarnPercent" Display="None"
                                                                                    ErrorMessage='"Earn Percent" value must be between 0 and 100 ' MaximumValue="100" MinimumValue="0"
                                                                                    Type="Double">"Earn Percent" value must be between 0 and 100 </asp:RangeValidator>
                                                                                <asp:RegularExpressionValidator ID="regexValidator1" runat="server" ControlToValidate="txtEditEarnPercent" Display="None"
                                                                                    ErrorMessage='"Earn Percent" value accepts only 2 decimal places ' ValidationExpression="(?!^0*$)(?!^0*\.0*$)^\d{1,3}(\.\d{1,2})?$"></asp:RegularExpressionValidator>
                                                                            </EditItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn>
                                                                            <ItemStyle Wrap="False"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
                                                                                    CausesValidation="False" runat="server" CommandName="Edit">
																						<img border="0" src="../images//im_edit.gif" alt="edit"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkButUp" Text="<img border=0 src=../images/up.gif alt=MoveUp>" CausesValidation="False"
                                                                                    runat="server" CommandName="Up"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkButDown" Text="<img border=0 src=../images/down.gif alt=MoveDown>" CausesValidation="False"
                                                                                    runat="server" CommandName="Down"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                                            </FooterTemplate>
                                                                            <EditItemTemplate>
                                                                                <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>" runat="server"
                                                                                    CommandName="Update">
																						<img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>" CausesValidation="False"
                                                                                    runat="server" CommandName="Delete"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
                                                                                    CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
                                                                            </EditItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTuitionEarningsPercentageRangeId" runat="server" Text='<%# Container.DataItem("TuitionEarningsPercentageRangeId") %>' Visible="False">
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="lblEditTuitionEarningsPercentageRangeId" runat="server" Text='<%# Container.DataItem("TuitionEarningsPercentageRangeId") %>' Visible="False">
                                                                                </asp:Label>
                                                                            </EditItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="CustomValidator"
            Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="True"
            ShowSummary="False"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        <!--end validation panel-->
    </div>

</asp:Content>


