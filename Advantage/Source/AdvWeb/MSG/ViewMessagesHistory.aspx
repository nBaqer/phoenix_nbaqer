
<%@ Register TagPrefix="fame" TagName="header" Src="~/UserControls/Header.ascx" %>
<%@ Register TagName="SysHeader" TagPrefix="Header" Src="MSGHeader.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewMessagesHistory.aspx.vb"
    Inherits="ViewMessagesHistory" %>


<%@ Register TagName="MessagingMenu" TagPrefix="Menu" Src="Menu_Main.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head id="Head1" runat="server">
    <title>View Messages</title>
    <meta http-equiv="pragma" content="NO-CACHE">
    <link href="../css/localhost.css" type="text/css" rel="stylesheet">
    <link href="../css/messagescreen.css" type="text/css" rel="stylesheet">
    <!-- begin tooltip style -->
    <style type="text/css">
		#dhtmltooltip { BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: black 1px solid; PADDING-LEFT: 2px; Z-INDEX: 100; FILTER: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135); LEFT: -300px; VISIBILITY: hidden; PADDING-BOTTOM: 2px; BORDER-LEFT: black 1px solid; WIDTH: 150px; PADDING-TOP: 2px; BORDER-BOTTOM: black 1px solid; POSITION: absolute; BACKGROUND-COLOR: lightyellow }
		#dhtmlpointer { Z-INDEX: 101; LEFT: -300px; VISIBILITY: hidden; POSITION: absolute }
	</style>
    <!-- end tooltip style -->
</head>
<body>
    <form id="form1" runat="server">

       <telerik:RadScriptManager ID="RadScriptManager1" Runat="server" EnablePageMethods="true" AsyncPostBackTimeout="3600" >
             <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
        </Scripts>
       </telerik:RadScriptManager>

        <div id="header">
            <fame:header ID="Header1" runat="server"></fame:header>
        </div>

        <script language="javascript" src="ToolTip.js" type="text/javascript"></script>

        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="DetailsFrameTop">
                    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="MenuFrame" align="right">
                                <asp:Button ID="btnSave" runat="server" Enabled="False" CssClass="save" Text="Save">
                                </asp:Button><asp:Button ID="btnNew" runat="server" Enabled="False" CssClass="new"
                                    Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server"
                                        Enabled="False" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%"
                        border="0">
                        <tr>
                            <td class="detailsframe">
                                <div class="scrollwhole">
                                    <!--begin content-->
                                    <table width="100%" summary="Content Table" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 50%; vertical-align: top">
                                                <table width="100%" summary="Options and Message Table" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                                <asp:Repeater ID="rptQueue" runat="server" OnItemDataBound="rptQueue_OnItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table width="100%" border="0" bgcolor="white" cellpadding="0" cellspacing="0">
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr onmouseover="this.style.backgroundColor='#e7edf3';" onmouseout="this.style.backgroundColor='';">
                                                                            <td style="border-style: solid; border-width: 1px; border-color: #e7edf3; padding: 0px">
                                                                                <table width="100%" class="Label">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:ImageButton ID="ibView" runat="server" AlternateText="View details" ImageUrl="../Images/MSG/Queue_View.gif"
                                                                                                CommandName="View" CommandArgument='<%# Container.DataItem("MessageId") %>' /></td>
                                                                                        <td>
                                                                                            To:</td>
                                                                                        <td colspan="2" align="left">
                                                                                            <b>
                                                                                                <%# Container.DataItem("RecipientType") %>
                                                                                                :&nbsp;<%# Container.DataItem("RecipientName") %></b></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:ImageButton ID="ibDeliver" runat="server" AlternateText="Deliver now" ImageUrl="../Images/MSG/Queue_Deliver.gif"
                                                                                                CommandName="Deliver" CommandArgument='<%# Container.DataItem("MessageId") %>' /></td>
                                                                                        <td>
                                                                                            Re:</td>
                                                                                        <td colspan="2" align="left">
                                                                                            <%# Container.DataItem("TemplateDescrip") %>
                                                                                            &nbsp;{<%# Container.DataItem("ReName") %>}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:ImageButton ID="ibDelete" runat="server" AlternateText="Delete" ImageUrl="../Images/MSG/Queue_Delete.gif"
                                                                                                CommandName="Delete" CommandArgument='<%# Container.DataItem("MessageId") %>' /></td>
                                                                                        <td>
                                                                                            Via:</td>
                                                                                        <td align="left">
                                                                                            <%# Container.DataItem("DeliveryType") %>
                                                                                            &nbsp;on&nbsp;<%# Container.DataItem("CreatedDate") %></td>
                                                                                        <td>
                                                                                            <asp:ImageButton ID="imInfo" runat="server" ImageUrl="../Images/MSG/Queue_Info.gif" /></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                                <asp:LinkButton ID="lnkPrev" CssClass="Label" runat="server" Text="Previous" />&nbsp;
                                                                <asp:LinkButton ID="lnkNext" CssClass="Label" runat="server" Text="Next" />
                                                           
                                                        </td>
                                                    </tr>
                                                </table>
                                                <td align="left" valign="top" height="100%">
                                                    <!--This style keeps reverting!-->
                                                    <!--<div id="fckdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 100%; BORDER-BOTTOM: black 1px solid; 
			                    HEIGHT: expression(document.body.clientHeight - 80 + 'px'); BACKGROUND-COLOR: silver">-->
                                                    <!--Note to graphic designer: you may need to adjust the height to fit in with the header and footer -->
                                                    <div id="fckdiv" style="border-right: black 1px solid; border-top: black 1px solid;
                                                        border-left: black 1px solid; width: 100%; border-bottom: black 1px solid; height: expression(document.body.clientHeight - 80 + 'px');
                                                        background-color: silver">
                                                        
<telerik:RadEditor runat="server" ID="fck" ToolsFile="~/MSG/xml/RadEditorToolsFile.xml" Height="400px" Width="550px">
    <Modules>
        <telerik:EditorModule Name="RadEditorHtmlInspector" Enabled="true" Visible="false" />
        <telerik:EditorModule Name="RadEditorNodeInspector" Enabled="true" Visible="false" />
        <telerik:EditorModule Name="RadEditorDomInspector" Enabled="false" />
        <telerik:EditorModule Name="RadEditorStatistics" Enabled="false" />
    </Modules>
    <ImageManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <MediaManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <FlashManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <TemplateManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <DocumentManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
</telerik:RadEditor>
 <script type="text/javascript">
     
     if (Telerik.Web.UI.Editor !== undefined) {
         Telerik.Web.UI.Editor.CommandList["FameTags"] = function (commandName, editor, args) {
             var elem = editor.getSelectedElement(); //returns the selected element.

             if (elem.tagName == "A") {
                 editor.selectElement(elem);
                 argument = elem;
             }
             else {
                 //remove links if present from the current selection - because of JS error thrown in IE
                 editor.fire("Unlink");

                 //remove Unlink command from the undo/redo list
                 var commandsManager = editor.get_commandsManager();
                 var commandIndex = commandsManager.getCommandsToUndo().length - 1;
                 commandsManager.removeCommandAt(commandIndex);

                 var content = editor.getSelectionHtml();

                 var link = editor.get_document().createElement("A");

                 link.innerHTML = content;
                 argument = link;
             }

             var myCallbackFunction = function (sender, args) {
                 editor.pasteHtml(String.format("{0}", args.innerHTML))
             }

             editor.showExternalDialog(
                  'SpecialFieldsPopupRadEditor.aspx?categoryName=' + args.get_value().replace('&amp;', '[amp]'),
                  argument,
                  370,
                  300,
                  myCallbackFunction,
                  null,
                  'Insert [Field]',
                  false,
                  Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
                  false,
                  true);
         };
     }
    </script>
                                                    </div>
                                                </td>
                                        </tr><asp:HiddenField ID="txtConfirm" runat="server" />
                                    </table>
                                    
                                    <!--end content-->
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- end rightcolumn -->
            </tr>
        </table>
        <!-- begin footer -->
       <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    </form>
</body>
</html>
