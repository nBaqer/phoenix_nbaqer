'
' FAME
' Copyright (c) 2005, 2006
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdvMessaging
' Description: Settings for AdvMessaging

Imports System.Text
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.MSG
Imports FAME.AdvantageV1.Common.MSG

Partial Class AdvantageApp_MSG_Settings
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MSGCommon.AdvInit(Me)
        If Not Page.IsPostBack Then
            ' Clear whatever is in the status bar
            MSGCommon.SetBrowserStatusBar(Me, " ")

            ' add javascript to select a Template
            Me.btnSelectTemplate.Attributes.Add("onclick", "var strReturn; strReturn=window.showModalDialog('Open.aspx',null,'resizable:yes;status:no;dialogWidth:475px;dialogHeight:500px;dialogHide:true;help:no;scroll:yes'); if (strReturn != null) document.getElementById('txtTemplateFile').value=strReturn;")

            ' Edit Categories
            edAddCategory.Text = ""
            BindCategories()

            ' show the value for the scheduler and admin email
            txtScheduler.Text = MSGCommon.GetWorkflowSchedule()
            txtAdminEmail.Text = MSGCommon.GetAdvAdminEmail()

            txtConfirm.Text = "false"
            btnReloadRules.Attributes.Add("onclick", "document.getElementById('txtConfirm').value = window.confirm('This action will clear all existing rules \rdefined in Workflow and restore them to the \roriginalinstallation.\rDo you want to continue?');")
        End If
    End Sub


    ' Method: btnReloadRules_Click
    ' Description: Reloads all rules fro MsgRules.xml.  This wipes out any settings the user
    '   may have had.
    Private Sub btnReloadRules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReloadRules.Click
        If txtConfirm.Text = "false" Then
            Return
        End If
        Dim xmlFileName As String = MapPath("xml/MsgRules.xml")
        Dim ds As New DataSet
        ds.ReadXml(xmlFileName, XmlReadMode.InferSchema)

        ' wipe out all the rules
        RulesFacade.DeleteAllRules()

        Dim dr As DataRow
        For Each dr In ds.Tables("AdvMessaging_Rules").Rows
            Dim rInfo As New RuleInfo
            rInfo.Descrip = dr("RuleDescrip")
            rInfo.Type = dr("RuleType")
            rInfo.RuleSql = dr("RuleSql")
            rInfo.RecipientType = dr("RecipientType")
            rInfo.ReType = dr("ReType")
            rInfo.DeliveryType = dr("DeliveryType")
            RulesFacade.UpdateRule(rInfo, MSGCommon.GetCurrentUserId())
        Next
        MsgCommon.SetBrowserStatusBar(Page, "All rules have been added.")
    End Sub

    ' Method: btnSelectTemplate_Click
    ' Description: Selects the default template that is loaded automatically when creating a new tempalte
    '   and composing a new message
    Private Sub btnSelectTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectTemplate.Click
        If Me.txtTemplateFile.Text <> "" Then
            Dim tInfo As TemplateInfo = TemplatesFacade.GetTemplateInfo(Me.txtTemplateFile.Text)
            Me.lblDefTemplate.Text = tInfo.Descrip
        End If
    End Sub

    ' Method: btnLoadDefTemplats_Click
    ' Description: Loads all default templates from MsgTemplates.xml
    Private Sub btnLoadDefTemplates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadDefTemplates.Click
        Dim xmlFileName As String = MapPath("xml/MsgTemplates.xml")
        Dim ds As New DataSet
        ds.ReadXml(xmlFileName, XmlReadMode.InferSchema)

        ' start by deleting all the groups
        GroupsFacade.DeleteAllGroups()

        Dim dsGroups As DataSet = GroupsFacade.GetGroups(True, True)
        Dim dr As DataRow
        For Each dr In ds.Tables("AdvMessaging_DefTemplates").Rows
            Try
                Dim tInfo As New TemplateInfo

                Dim drs() As DataRow = dsGroups.Tables(0).Select("Descrip='" & dr("Category") & "'")
                Dim mgId As String
                If drs.Length = 0 Then
                    Dim tgInfo As New GroupInfo
                    tgInfo.CampGroupId = MSGCommon.GetCampusID()
                    tgInfo.Descrip = dr("Category")
                    GroupsFacade.UpdateGroup(tgInfo, MSGCommon.GetCurrentUserId())
                    mgId = tInfo.GroupId
                    ' refresh the dsGroups dataset to reflect the added group
                    dsGroups = GroupsFacade.GetGroups(True, True)
                Else
                    mgId = drs(0)("GroupId").ToString()
                End If

                tInfo.GroupId = mgId
                tInfo.Data = dr("TemplateData")
                tInfo.Descrip = dr("Name")
                tInfo.CampGroupId = MSGCommon.GetCampusID()
                TemplatesFacade.UpdateTemplate(tInfo, MSGCommon.GetCurrentUserId())
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)


            End Try
        Next

        MsgCommon.SetBrowserStatusBar(Page, "All templates have been added.")
    End Sub

#Region " Edit Categories functions "
    Private Sub BindCategories(ByVal campusID As String)
        Try
            dgCategory.DataSource = GroupsFacade.GetGroups(True, True)
            dgCategory.DataBind()
            If dgCategory.Items.Count = 0 Then
                dgCategory.Visible = False
                lblMsg.Text = "No categories have been defined found"
            Else
                dgCategory.Visible = True
                lblMsg.Text = ""
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    Private Sub BindCategories()
        Dim campusId As String = MsgCommon.GetCampusID()
        '        If Not campusId = "" Then
        BindCategories(campusId)
        '       Else
        '          MsgCommon.DisplayErrorMessage("No campusId")
        'End If
    End Sub
    ' Called for each item in the datagrid.  Gives us a chance to add a javascript handler for each
    ' hyperlink in the grid.  This allows the proper value to be returned to the caller.
    Public Sub dgCategory_OnItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Try
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
            End Select
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    ' Mapped to DataGrid.OnDeleteCommand 
    '
    Public Sub DeleteCategory(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            'strItem = dgCategory.DataKeys(e.Item.ItemIndex)
            ' get the columns and the data from it, there have to be a quicker way....
            Dim lb As Label = e.Item.FindControl("lbldgCategoryId")
            Dim MessageGroupId As String = lb.Text
            ' call the database function to delete the item and then delete this item
            ' remove from syMessageGroups table
            'GroupsFacade.DeleteGroup(MessageGroupId)
            ' remove from syMessageTemplates table
            'MessagingFacade.UnlinkMessageTemplateInfo(MessageGroupId)
            ' hide the row
            e.Item().Visible() = False
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub
    ' Mapped to DataGrid.OnUpdateCommand
    '
    Public Sub UpdateCategory(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            ' get the edit box value and update the db
            Dim ed As TextBox = CType(e.Item.FindControl("editCategory"), TextBox)
            Dim strValue As String = ed.Text
            ' get the MessageTemplateId of the row that is being edited
            Dim edCategoryId As TextBox = CType(e.Item.FindControl("editCategoryId"), TextBox)
            Dim strGroupId As String = edCategoryId.Text
            ' get the MessageTemplateInfo, modify the description and save it            
            Dim msgGroupInfo As GroupInfo = GroupsFacade.GetGroupInfo(strGroupId)
            msgGroupInfo.IsInDB = True
            msgGroupInfo.Descrip = strValue
            GroupsFacade.UpdateGroup(msgGroupInfo, MSGCommon.GetCurrentUserId())
            ' now update the database
            ' disable edit mode
            dgCategory.EditItemIndex = -1
            BindCategories()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ' Mapped to DataGrid.OnCancelCommand
    '
    Public Sub CancelEditCategory(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        ' disable edit mode
        dgCategory.EditItemIndex = -1
        BindCategories()
    End Sub
    ' Mapped to DataGrid.OnEditCommand 
    '
    Public Sub EditCategory(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            dgCategory.EditItemIndex = e.Item.ItemIndex
            BindCategories()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub
    '
    Public Sub AssignField(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            Dim lb As Label = e.Item.FindControl("lbldgCategoryId")
            Dim MessageGroupId As String = lb.Text
            Dim sb As New StringBuilder
            sb.Append("<script language='javascript'>")
            sb.Append("strReturn=window.showModalDialog(")
            sb.Append("'TemplateFilter.aspx?GroupId=" + MessageGroupId + "',")
            sb.Append("null,'resizable:yes;status:no;dialogWidth:475px;dialogHeight:500px;dialogHide:true;help:no;scroll:yes')")
            sb.Append("</script>")
            ClientScript.RegisterStartupScript(Me.GetType(), "AssignFields", sb.ToString())
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try

    End Sub
    '
    Private Sub lbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbAdd.Click
        Try
            ' only try to add no empty values
            edAddCategory.Text.TrimEnd()
            edAddCategory.Text.TrimStart()
            If edAddCategory.Text.Length > 0 Then
                Dim msgGroupInfo As New GroupInfo
                msgGroupInfo.IsInDB = False
                msgGroupInfo.Descrip = edAddCategory.Text
                msgGroupInfo.CampGroupId = MSGCommon.GetCampusID()
                msgGroupInfo.ModUser = MSGCommon.GetCurrentUserId()
                Dim iError As Integer = GroupsFacade.UpdateGroup(msgGroupInfo, MSGCommon.GetCurrentUserId())
                ' if iError < 0, that is an error 
                If iError < 0 Then
                    ' show message box here 
                    ' here we should improve the message
                    Dim strMsg As String = "Failed to insert < " + edAddCategory.Text
                    strMsg += " >. Make sure is not a duplicated value"
                    Dim strScript As String = ""
                    strScript = "<script>"
                    strScript = strScript & "alert('" + strMsg + "');"
                    strScript = strScript & "</script>"
                    Response.Write(strScript)
                    Return
                End If

                BindCategories()
                edAddCategory.Text = ""

                ' TODO: add a status showing that the new category was added correctly
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub
#End Region


    Private Sub dgCategory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCategory.ItemCommand
        If e.CommandName = "AssignField" Then
            Try
                Dim lb As Label = e.Item.FindControl("lbldgCategoryId")
                Dim MessageGroupId As String = lb.Text
                Dim sb As New StringBuilder
                sb.Append("<script language='javascript'>")
                sb.Append("     strReturn=window.showModalDialog(")
                sb.Append("'TemplateFilter.aspx?GroupId=" + MessageGroupId + "',")
                sb.Append("null,'resizable:no;status:no;dialogWidth:250px;dialogHeight:400px;dialogHide:true;help:no;scroll:yes')")
                sb.Append("</script>")
                ClientScript.RegisterStartupScript(Me.GetType(), "AssignFields", sb.ToString())
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                MSGCommon.DisplayErrorMessage(Me, ex.Message)
            End Try
        End If
    End Sub
End Class
