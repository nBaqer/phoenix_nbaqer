' ===============================================================================
' SetupSpecialFields
' Messaging code behind SetupSpecialFields.aspx
' Assigns permissions for which special fields can be shown for a given entity
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.AdvantageV1.BusinessFacade.MSG
Imports FAME.AdvantageV1.Common.MSG

Partial Class MSG_SetupSpecialFields
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim fac As New UserSecurityFacade
        Dim m_Context As HttpContext

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = resourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim

        MSGCommon.AdvInit(Me)

        If Not Page.IsPostBack Then
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)

            ' load the form with its default values
            ResetForm()

            ' show the entities in the left pane
            BindEntities()

            ' Bind the EntityFieldGroup to the form
            Dim EntityId As String = Request.Params("EntityId")
            If Not EntityId Is Nothing Then
                BindEntityFieldGroup(EntityId)
            End If
        Else
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title
    End Sub
#Region "Advantage Permissions"
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            'btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            'btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If
        '   btnnew.Enabled = True

    End Sub
#End Region

    ''' <summary>
    ''' Clears the form to its initial state.  DropDownLists are reset
    ''' with values from the database.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ResetForm()
        ' clear out the form
        Me.lbFieldGroups.Items.Clear()
        Me.lbSysFieldGroups.Items.Clear()
        MSGCommon.BuildFieldGroupsLB(lbSysFieldGroups)
    End Sub

    ''' <summary>
    ''' Displays the list of entities
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindEntities()
        dlEntities.DataSource = EntitiesFieldGroupsFacade.GetEntities()
        dlEntities.DataBind()
    End Sub

    ''' <summary>
    ''' Fills the form with values queried from the database
    ''' using the supplied entityid
    ''' </summary>
    ''' <param name="EntityId"></param>
    ''' <remarks></remarks>
    Protected Sub BindEntityFieldGroup(ByVal EntityId As String)
        Try
            ' Add all the FieldGroups for the given entityid
            lbFieldGroups.DataSource = EntitiesFieldGroupsFacade.GetAll(EntityId, True, False)
            lbFieldGroups.DataValueField = "EntityFieldGroupId"
            lbFieldGroups.DataTextField = "FieldGroupName"
            lbFieldGroups.DataBind()

            ' now, we need to remove all the results that at in lbResults
            ' from the lbSysResults so that the user cannot add results twice.
            Dim lb As ListItem
            For Each lb In lbFieldGroups.Items
                Dim lb2 As ListItem = lbSysFieldGroups.Items.FindByText(lb.Text)
                If Not lb2 Is Nothing Then lbSysFieldGroups.Items.Remove(lb2)
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist containing all the users.
    ''' In response, this handler should populate the info for a task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlEntities_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlEntities.ItemCommand
        ' get the selected userid and store it in the viewstate object
        Dim entityid As String = e.CommandArgument.ToString
        ViewState("entityid") = entityid
        dlEntities.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If entityid Is Nothing Or entityid = "" Then
            MSGCommon.Alert(Me, "There was a problem displaying the details for this entity.")
            Return
        End If

        ' show the controls that allow the user to set the task to inactive/active
        'ddlActive.Visible = True
        'lblActivePrompt.Visible = True
        ResetForm()

        ' bind the task to the form
        BindEntityFieldGroup(entityid)

        ' Advantage specific stuff
        'CommonWebUtilities.SetStyleToSelectedItem(dlEntities, entityid, ViewState, Header1)

        CommonWebUtilities.RestoreItemValues(dlEntities, entityid)

        InitButtonsForEdit()
    End Sub

    ''' <summary>
    ''' Move an item from the SysFieldGroups to the FieldGroups listbox.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddFieldGroup.Click
        Dim i As Integer = lbSysFieldGroups.SelectedIndex
        If i = -1 Then Return
        Dim Name As String = lbSysFieldGroups.SelectedItem.Text
        lbSysFieldGroups.Items.RemoveAt(i)
        lbFieldGroups.Items.Add(Name)
    End Sub

    ''' <summary>
    ''' Move an item from the FieldGroups to the SysFieldGroups listbox.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelFieldGroup.Click
        Dim i As Integer = lbFieldGroups.SelectedIndex
        If i = -1 Then Return
        Dim Name As String = lbFieldGroups.SelectedItem.Text
        lbFieldGroups.Items.RemoveAt(i)
        lbSysFieldGroups.Items.Add(Name)
    End Sub

    ''' <summary>
    ''' User wants to save the task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim entityid As String = ViewState("entityid")

            ' first remove all field groups for the given entity
            EntitiesFieldGroupsFacade.DeleteAll(entityid)

            ' now, add a new field group mapping for each entry in the listbox
            Dim opOk As Boolean = True
            Dim lb As ListItem
            For Each lb In lbFieldGroups.Items
                Dim info As New EntityFieldGroupInfo
                info.EntityId = entityid
                info.FieldGroupName = lb.Text
                info.Active = True
                If Not EntitiesFieldGroupsFacade.Update(info, MSGCommon.GetCurrentUserId()) Then
                    opOk = False
                End If
            Next

            ' first remove all field groups for the given entity
            If Not opOk Then
                MSGCommon.Alert(Me, "Changes could not be saved.")
                Return
            End If
            MSGCommon.Alert(Page, "Changes were saved successfully")

            ' Advantage specific stuff
            'CommonWebUtilities.SetStyleToSelectedItem(dlEntities, entityid, ViewState, Header1)
            InitButtonsForEdit()
            CommonWebUtilities.RestoreItemValues(dlEntities, entityid)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' This just clears the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ViewState("entityid") = Nothing
        ResetForm()

        ' Advantage specific stuff
        ' CommonWebUtilities.SetStyleToSelectedItem(dlEntities, Guid.Empty.ToString, ViewState, Header1)
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlEntities, Guid.Empty.ToString)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnAddFieldGroup)
        controlsToIgnore.Add(btnDelFieldGroup)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
