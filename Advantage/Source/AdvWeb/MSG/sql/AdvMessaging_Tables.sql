GO഍਍⼀⨀⨀⨀⨀⨀⨀ 伀戀樀攀挀琀㨀  吀愀戀氀攀 嬀搀戀漀崀⸀嬀猀礀䴀攀猀猀愀最攀䜀爀漀甀瀀猀崀    匀挀爀椀瀀琀 䐀愀琀攀㨀 　㄀⼀㈀㜀⼀㈀　　㘀 　㤀㨀㐀㔀㨀㈀㤀 ⨀⨀⨀⨀⨀⨀⼀ഀഀ഍
SET ANSI_NULLS ON഍਍䜀伀ഀഀ഍
SET QUOTED_IDENTIFIER ON഍਍䜀伀ഀഀ഍
SET ANSI_PADDING ON഍਍䜀伀ഀഀ഍
CREATE TABLE [dbo].[syMessageGroups](഍਍ऀ嬀䴀攀猀猀愀最攀䜀爀漀甀瀀䤀搀崀 嬀甀渀椀焀甀攀椀搀攀渀琀椀昀椀攀爀崀 刀伀圀䜀唀䤀䐀䌀伀䰀  一伀吀 一唀䰀䰀Ⰰഀഀ഍
	[CampGrpId] [uniqueidentifier] NULL,഍਍ऀ嬀䜀爀漀甀瀀䐀攀猀挀爀椀瀀崀 嬀瘀愀爀挀栀愀爀崀⠀㔀　⤀ 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀Ⰰഀഀ഍
	[ModDate] [datetime] NULL CONSTRAINT [DF_syMessageGroups_ModDate]  DEFAULT (2005 - 1 - 1),഍਍ऀ嬀䴀漀搀唀猀攀爀崀 嬀瘀愀爀挀栀愀爀崀⠀㔀　⤀ 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀 䌀伀一匀吀刀䄀䤀一吀 嬀䐀䘀开猀礀䴀攀猀猀愀最攀䜀爀漀甀瀀猀开䴀漀搀唀猀攀爀崀  䐀䔀䘀䄀唀䰀吀 ⠀✀渀漀 甀猀攀爀 猀攀氀攀挀琀攀搀✀⤀Ⰰഀഀ഍
 CONSTRAINT [PK_syMessageGroups] PRIMARY KEY CLUSTERED ഍਍⠀ഀഀ഍
	[MessageGroupId] ASC഍਍⤀ 伀一 嬀倀刀䤀䴀䄀刀夀崀ഀഀ഍
) ON [PRIMARY]഍਍ഀഀ഍
GO഍਍匀䔀吀 䄀一匀䤀开倀䄀䐀䐀䤀一䜀 伀䘀䘀ഀഀ഍
഍਍ഀഀ഍
/****** Object:  Table [dbo].[syMessageRules]    Script Date: 01/27/2006 09:46:04 ******/഍਍匀䔀吀 䄀一匀䤀开一唀䰀䰀匀 伀一ഀഀ഍
GO഍਍匀䔀吀 儀唀伀吀䔀䐀开䤀䐀䔀一吀䤀䘀䤀䔀刀 伀一ഀഀ഍
GO഍਍匀䔀吀 䄀一匀䤀开倀䄀䐀䐀䤀一䜀 伀一ഀഀ഍
GO഍਍䌀刀䔀䄀吀䔀 吀䄀䈀䰀䔀 嬀搀戀漀崀⸀嬀猀礀䴀攀猀猀愀最攀刀甀氀攀猀崀⠀ഀഀ഍
	[RuleId] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_syMessageRules_RuleId]  DEFAULT (newid()),഍਍ऀ嬀刀甀氀攀吀礀瀀攀崀 嬀猀洀愀氀氀椀渀琀崀 一唀䰀䰀Ⰰഀഀ഍
	[RuleDescrip] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,഍਍ऀ嬀刀甀氀攀匀焀氀崀 嬀琀攀砀琀崀 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀Ⰰഀഀ഍
	[TemplateId] [uniqueidentifier] NULL,഍਍ऀ嬀刀攀挀椀瀀椀攀渀琀吀礀瀀攀崀 嬀瘀愀爀挀栀愀爀崀⠀㄀㔀⤀ 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀Ⰰഀഀ഍
	[ReType] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,഍਍ऀ嬀䐀攀氀椀瘀攀爀礀吀礀瀀攀崀 嬀瘀愀爀挀栀愀爀崀⠀㄀　⤀ 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀Ⰰഀഀ഍
	[Schedule] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,഍਍ऀ嬀䰀愀猀琀刀甀渀崀 嬀搀愀琀攀琀椀洀攀崀 一唀䰀䰀Ⰰഀഀ഍
	[LastRunError] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,഍਍ऀ嬀䴀漀搀䐀愀琀攀崀 嬀猀洀愀氀氀搀愀琀攀琀椀洀攀崀 一唀䰀䰀Ⰰഀഀ഍
	[ModUser] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,഍਍ऀ嬀䄀挀琀椀瘀攀崀 嬀琀椀渀礀椀渀琀崀 一唀䰀䰀Ⰰഀഀ഍
 CONSTRAINT [PK_syMessageRules] PRIMARY KEY CLUSTERED ഍਍⠀ഀഀ഍
	[RuleId] ASC഍਍⤀ 伀一 嬀倀刀䤀䴀䄀刀夀崀ഀഀ഍
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]഍਍ഀഀ഍
GO഍਍匀䔀吀 䄀一匀䤀开倀䄀䐀䐀䤀一䜀 伀䘀䘀ഀഀ഍
഍਍ഀഀ഍
/****** Object:  Table [dbo].[syMessages]    Script Date: 01/27/2006 09:46:41 ******/഍਍匀䔀吀 䄀一匀䤀开一唀䰀䰀匀 伀一ഀഀ഍
GO഍਍匀䔀吀 儀唀伀吀䔀䐀开䤀䐀䔀一吀䤀䘀䤀䔀刀 伀一ഀഀ഍
GO഍਍匀䔀吀 䄀一匀䤀开倀䄀䐀䐀䤀一䜀 伀一ഀഀ഍
GO഍਍䌀刀䔀䄀吀䔀 吀䄀䈀䰀䔀 嬀搀戀漀崀⸀嬀猀礀䴀攀猀猀愀最攀猀崀⠀ഀഀ഍
	[MessageId] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_syMessages_MessageId]  DEFAULT (newid()),഍਍ऀ嬀䴀攀猀猀愀最攀吀攀洀瀀氀愀琀攀䤀搀崀 嬀甀渀椀焀甀攀椀搀攀渀琀椀昀椀攀爀崀 一唀䰀䰀Ⰰഀഀ഍
	[FromId] [uniqueidentifier] NULL,഍਍ऀ嬀䐀攀氀椀瘀攀爀礀吀礀瀀攀崀 嬀瘀愀爀挀栀愀爀崀⠀㄀　⤀ 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀Ⰰഀഀ഍
	[RecipientType] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,഍਍ऀ嬀刀攀挀椀瀀椀攀渀琀䤀搀崀 嬀甀渀椀焀甀攀椀搀攀渀琀椀昀椀攀爀崀 一唀䰀䰀Ⰰഀഀ഍
	[ReType] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,഍਍ऀ嬀刀攀䤀搀崀 嬀甀渀椀焀甀攀椀搀攀渀琀椀昀椀攀爀崀 一唀䰀䰀Ⰰഀഀ഍
	[ChangeId] [uniqueidentifier] NULL,഍਍ऀ嬀䴀猀最䌀漀渀琀攀渀琀崀 嬀琀攀砀琀崀 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀Ⰰഀഀ഍
	[CreatedDate] [datetime] NULL,഍਍ऀ嬀䐀攀氀椀瘀攀爀礀䐀愀琀攀崀 嬀搀愀琀攀琀椀洀攀崀 一唀䰀䰀Ⰰഀഀ഍
	[LastDeliveryAttempt] [datetime] NULL,഍਍ऀ嬀䰀愀猀琀䐀攀氀椀瘀攀爀礀䴀猀最崀 嬀瘀愀爀挀栀愀爀崀⠀㄀　　⤀ 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀Ⰰഀഀ഍
	[DateDelivered] [datetime] NULL,഍਍ऀ嬀䴀漀搀䐀愀琀攀崀 嬀搀愀琀攀琀椀洀攀崀 一唀䰀䰀 䌀伀一匀吀刀䄀䤀一吀 嬀䐀䘀开猀礀䴀攀猀猀愀最攀猀开䴀漀搀䐀愀琀攀崀  䐀䔀䘀䄀唀䰀吀 ⠀✀㈀　　㔀ⴀ　㄀ⴀ　㄀✀⤀Ⰰഀഀ഍
	[ModUser] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,഍਍ 䌀伀一匀吀刀䄀䤀一吀 嬀猀礀䴀攀猀猀愀最攀猀开倀䬀崀 倀刀䤀䴀䄀刀夀 䬀䔀夀 䌀䰀唀匀吀䔀刀䔀䐀 ഀഀ഍
(഍਍ऀ嬀䴀攀猀猀愀最攀䤀搀崀 䄀匀䌀ഀഀ഍
) ON [PRIMARY]഍਍⤀ 伀一 嬀倀刀䤀䴀䄀刀夀崀 吀䔀堀吀䤀䴀䄀䜀䔀开伀一 嬀倀刀䤀䴀䄀刀夀崀ഀഀ഍
഍਍䜀伀ഀഀ഍
SET ANSI_PADDING OFF഍਍ഀഀ഍
഍਍⼀⨀⨀⨀⨀⨀⨀ 伀戀樀攀挀琀㨀  吀愀戀氀攀 嬀搀戀漀崀⸀嬀猀礀䴀攀猀猀愀最攀吀攀洀瀀氀愀琀攀猀崀    匀挀爀椀瀀琀 䐀愀琀攀㨀 　㄀⼀㈀㜀⼀㈀　　㘀 　㤀㨀㐀㜀㨀　㐀 ⨀⨀⨀⨀⨀⨀⼀ഀഀ഍
SET ANSI_NULLS ON഍਍䜀伀ഀഀ഍
SET QUOTED_IDENTIFIER ON഍਍䜀伀ഀഀ഍
SET ANSI_PADDING ON഍਍䜀伀ഀഀ഍
CREATE TABLE [dbo].[syMessageTemplates](഍਍ऀ嬀䴀攀猀猀愀最攀吀攀洀瀀氀愀琀攀䤀搀崀 嬀甀渀椀焀甀攀椀搀攀渀琀椀昀椀攀爀崀 刀伀圀䜀唀䤀䐀䌀伀䰀  一伀吀 一唀䰀䰀 䌀伀一匀吀刀䄀䤀一吀 嬀䐀䘀开猀礀䴀攀猀猀愀最攀吀攀洀瀀氀愀琀攀猀开䴀攀猀猀愀最攀吀攀洀瀀氀愀琀攀䤀搀崀  䐀䔀䘀䄀唀䰀吀 ⠀渀攀眀椀搀⠀⤀⤀Ⰰഀഀ഍
	[MessageGroupId] [uniqueidentifier] NULL,഍਍ऀ嬀䌀愀洀瀀䜀爀瀀䤀搀崀 嬀甀渀椀焀甀攀椀搀攀渀琀椀昀椀攀爀崀 一唀䰀䰀Ⰰഀഀ഍
	[TemplateDescrip] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,഍਍ऀ嬀吀攀洀瀀氀愀琀攀䐀愀琀愀崀 嬀琀攀砀琀崀 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀Ⰰഀഀ഍
	[ModDate] [datetime] NULL CONSTRAINT [DF_syMessageTemplates_ModDate]  DEFAULT (2005 - 1 - 1),഍਍ऀ嬀䴀漀搀唀猀攀爀崀 嬀瘀愀爀挀栀愀爀崀⠀㔀　⤀ 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀 一唀䰀䰀 䌀伀一匀吀刀䄀䤀一吀 嬀䐀䘀开猀礀䴀攀猀猀愀最攀吀攀洀瀀氀愀琀攀猀开䴀漀搀唀猀攀爀崀  䐀䔀䘀䄀唀䰀吀 ⠀✀渀漀 甀猀攀爀 猀攀氀攀挀琀攀搀✀⤀Ⰰഀഀ഍
	[Active] [smallint] NULL,഍਍ 䌀伀一匀吀刀䄀䤀一吀 嬀猀礀䴀攀猀猀愀最攀吀攀洀瀀氀愀琀攀猀开倀䬀崀 倀刀䤀䴀䄀刀夀 䬀䔀夀 䌀䰀唀匀吀䔀刀䔀䐀 ഀഀ഍
(഍਍ऀ嬀䴀攀猀猀愀最攀吀攀洀瀀氀愀琀攀䤀搀崀 䄀匀䌀ഀഀ഍
) ON [PRIMARY]഍਍⤀ 伀一 嬀倀刀䤀䴀䄀刀夀崀 吀䔀堀吀䤀䴀䄀䜀䔀开伀一 嬀倀刀䤀䴀䄀刀夀崀ഀഀ഍
഍਍䜀伀ഀഀ഍
SET ANSI_PADDING OFF