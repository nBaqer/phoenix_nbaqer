'
' FAME
' Copyright (c) 2005
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdvMessaging
' Description: Popup window which allows the user to open a template for editing.
'   This popup return the TemplateId to the caller through javascript.
' The parameter "print" can be passed to the url.  If it is set to "1", then
' we will print all messages (regardless of deliverytype).  If not "1", then
' we print all messages that are have "Printer" deliverytype and we deliver
' all other messages in their normal fashion (ie, email).

Imports System.Text
Imports FAME.AdvantageV1.BusinessFacade.MSG
Imports FAME.AdvantageV1.Common.MSG

Partial Class AdvantageApp_MSG_Print
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            ' check the parameter "print".  If set to "1", we will Print all messages in the current view
            ViewState("print") = Request.Params("print")

            ' chage the "Print All" button to "Process All"
            If ViewState("print") Is Nothing Or ViewState("print") = "" Or ViewState("print") = "0" Then
                Me.btnPrint.Text = "Process All"
            End If

            lblHeader.Text = "The operation you have requested may take a long time depending on the number of messages to print.  Are you sure you want to proceed?"
            btnCancel.Attributes.Add("onclick", "window.close();")

            Dim js As New StringBuilder
            js.Append("document.getElementById('btnPrint').visible=false;" & vbCrLf)
            js.Append("document.getElementById('btnCancel').visible=false;" & vbCrLf)
            js.Append("document.getElementById('lblHeader').value='Loading selected messages...';" & vbCrLf)
            js.Append("bar1.showBar();" & vbCrLf)
            btnPrint.Attributes.Add("onclick", js.ToString)
        End If
    End Sub


    ' Retrieves all the messages that were selected and saved in Session("curMsgGUIDS").
    ' This session entry is an ArrayList of guids.
    Private Function GetAllMessagesHTML(ByVal bOnlyPrint As Boolean) As String
        Dim sb As New StringBuilder(256 * 1024) ' start at 256k
        ' Load the array from session
        Dim oArray As Collections.ArrayList = Session("curMsgGUIDS")
        If oArray Is Nothing Then
            Return ""
        End If

        ' iterate through each message and add their message content
        sb.Append("<div><span style=""DISPLAY: none"">&nbsp;</span></div>")
        For Each MessageId As String In oArray
            Dim msgInfo As MessageInfo = MessagingFacade.GetMessageInfo(MessageId)

            If bOnlyPrint Or msgInfo.DeliveryType = "Printer" Then
                sb.Append(msgInfo.MsgContent)
                ' add a page break before the start of each message
                If MessageId <> oArray.Item(oArray.Count - 1) Then
                    ' BEN: 10-10-06 Fix for new version of fckeditor 2.2.1
                    sb.Append("<div style=""PAGE-BREAK-BEFORE: always"" clear=""all""><span style=""DISPLAY: none"">&nbsp;</span></div>")                    
                End If
            End If
        Next

        Return sb.ToString
    End Function

    Private Sub SendAllEmailMsgs()
        ' Load the array from session
        Dim oArray As Collections.ArrayList = Session("curMsgGUIDS")
        If oArray Is Nothing Then
            Return
        End If

        ' deliver each message
        Dim strRes As String
        For Each MessageId As String In oArray
            ' we can safely use this version of DeliverMessage because it
            ' does not support the ability to print.  Thus, we can be assured that
            ' all messages will get delivered except for printing.
            strRes = MessagingFacade.DeliverMessage(MessageId, True, MSGCommon.GetCurrentUserId())
        Next
    End Sub

    ' Command handler for the user clicking the print button
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim bOnlyPrint As Boolean = True
        If ViewState("print") Is Nothing Or ViewState("print") = "" Or ViewState("print") = "0" Then
            bOnlyPrint = False
        End If

        ' Change the Cancel button to Close 
        Me.btnCancel.Text = "Close"

        ' retrieve all the messages and print them
        Dim sb As New StringBuilder(GetAllMessagesHTML(bOnlyPrint))
        If sb.Length > 0 Then MsgCommon.PrintDocument(Page, sb.ToString)

        ' check if we are supposed to send the messages too
        If bOnlyPrint = False Then
            SendAllEmailMsgs()
        End If
        ' clear out the session var
        Session("curMsgGUIDS") = Nothing
    End Sub
End Class
