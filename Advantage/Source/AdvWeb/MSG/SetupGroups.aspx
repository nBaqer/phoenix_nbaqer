<%@ page title="" language="vb" MasterPageFile="~/NewSite.master" autoeventwireup="false" codefile="SetupGroups.aspx.vb" inherits="AdvantageApp_MSG_SetupGroups" %>
<%@ mastertype virtualpath="~/NewSite.master"%> 
<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlGroups">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlGroups" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlGroups" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlGroups" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlGroups" runat="server" DataKeyField="GroupId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="~/images/Inactive.gif" runat="server" Visible='<%# (Not Ctype(Container.DataItem("Active"), Boolean)).ToString %>' CommandArgument='<%# Container.DataItem("GroupId")%>' CausesValidation="False" />
                                        <asp:ImageButton ID="imgActive" ImageUrl="~/images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Active"), Boolean).ToString %>' CommandArgument='<%# Container.DataItem("GroupId")%>' CausesValidation="False" />
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("Active")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("GroupId")%>' Text='<%# container.dataitem("Descrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button>
                                <asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" style="width: 60%;" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCode" runat="server" CssClass="label">Code <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" runat="server" CssClass="textbox" TabIndex="1" MaxLength="20"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblActivePrompt" runat="server" CssClass="label">Status <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlActive" runat="server" CssClass="dropdownlist" TabIndex="2" /></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDescrip" runat="server" CssClass="label">Description <span style="color: red">*</span></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="textbox" TabIndex="3" MaxLength="100"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" align="left" valign="top">
                                                <font class="Label">Templates in this Group</font></td>
                                            <td height="85" align="left" class="contentcell4">
                                                <asp:ListBox ID="lbSysTemplates" runat="server" CssClass="listboxes"
                                                     TabIndex="4" /></td>
                                        </tr>
                                    </table>
                                </div>
                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="customvalidator"
            Display="none"></asp:CustomValidator>
        <asp:Panel ID="pnlrequiredfieldvalidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="true"
            ShowSummary="false"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>

</asp:Content>


