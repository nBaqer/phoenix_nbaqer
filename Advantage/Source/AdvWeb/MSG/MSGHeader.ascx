<%@ Control Language="VB" ClassName="Header" %>
<%@ Import Namespace="FAME.AdvantageV1.BusinessFacade.MSG" %>

<script runat="server">
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        ' set the title to be whatever the parent is
        MSGCommon.SetBrowserTitle(Parent.Page, Parent.Page.Title)
        ' update the header bar
        lblHeaderText.Text = Parent.Page.Title
        lblUserName.Text = Session("username")
        lblDate.Text = Date.Now.ToString()
    End Sub
</script>

<!-- begin header -->
<table cellSpacing="0" cellPadding="0" width="100%" summary="Header Table" border="0">
<tr>
    <td class="headerimage" width="100" align="center" valign="middle"><IMG src="../images/MSG/header.jpg" /></td>
    <td class="headertitle" align="left"><asp:Label ID="lblHeaderText" runat="server" /></td>
    <td class="headerlabel" align="right">
        <table>
            <tr><td><asp:Label ID="lblUserName" runat="server" /></td></tr>
            <tr><td><asp:Label ID="lblDate" runat="server" /></td></tr>
        </table>
    </td>
</tr>
</table>
<!-- end header -->						
