<%@ Register TagName="SysHeader" TagPrefix="HeaderSimple" Src="MSGHeaderSimple.ascx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FilterMessagesPopup.aspx.vb" Inherits="AdvantageApp_MSG_QueueFilter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Filter Messages</title>
    <meta http-equiv="pragma" content="NO-CACHE"/>
    <LINK href="../css/MSG.css" type="text/css" rel="stylesheet"/>
	<base target="_self"/>
</head>
<body bgColor="#e9edf2">
    <form id="form1" runat="server">
    <div>
        <headersimple:SysHeader ID="sysHeader" runat="server" />
        <p />
		<table width="90%" align="center" cellpadding="4">
			<tr>
				<td align="left" class="Label">Recipient type:</td>
				<td align="left"><asp:dropdownlist id="ddlRecipientType" runat="server" CssClass="DropDownList" Width="150px" AutoPostBack="True" /></td>
			</tr>
			<tr>
				<td align="left" class="Label">Recipient</td>
				<td align="left"><asp:TextBox ID="txtRecipientName" Runat="server" CssClass="textbox" />
					<asp:imagebutton id="ibStudentLookup" Width="14px" Height="14px" ImageUrl="../Images/MSG/RecipLookup.gif" Runat="server" />
					<asp:linkbutton id="lbStudentLookup" runat="server" CssClass="Label" Text="Lookup" /></td>				
			</tr>
			<tr>
				<td align="left" class="Label">Delivery type</td>
				<td align="left"><asp:dropdownlist id="ddlDeliveryType" runat="server" CssClass="DropDownList" AutoPostBack="True" /></td>
			</tr>
			<tr>
				<td align="left" class="Label">Template group</td>
				<td align="left"><asp:dropdownlist id="ddlCategory" runat="server" CssClass="DropDownList" AutoPostBack="True" /></td>
			</tr>
			<tr>
				<td align="left" class="Label">Template</td>
				<td align="left"><asp:dropdownlist id="ddlTemplate" runat="server" CssClass="DropDownList" /></td>
			</tr>
			<tr>
				<td align="left" class="Label">Show errors only?</td>
				<td align="left"><asp:CheckBox ID="cbShowErrors" Runat="server" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<asp:Button ID="btnOK" Runat="server"  Text="Ok" Width="80px" />&nbsp;
					<asp:Button ID="btnCancel" Runat="server"  Text="Cancel" Width="80px" />
					<asp:Button ID="btnReset" Runat="server"  Text="Reset" Width="80px" />
				</td>
			</tr>
		</table>
		<!-- start Hidden fields -->				
        <asp:HiddenField ID="txtRecipientId" runat="server" />
        <asp:HiddenField ID="hfRecipientType" runat="server" />
		<!-- end Hidden fields -->
    </div>
    </form>
</body>
</html>
