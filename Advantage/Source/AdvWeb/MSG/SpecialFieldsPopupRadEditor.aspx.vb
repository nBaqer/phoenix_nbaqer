﻿
Imports System.Data
'<summary>
'Based on received parameter - CATEGORY NAME  - 
'page loops though MSG/xml/MsgFields.xml file 
'and selects for DDL values matching received category name.
'Clicking on the button - closes the page, and returns selected data via javascript callback to the parent page (rad editor)
'</summary>

Partial Class MSG_SpecialFieldsPopupRadEditor
    Inherits System.Web.UI.Page
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If Not Page.IsPostBack Then
            Dim categoryName As String = Server.HtmlDecode(Trim(Request.QueryString("categoryName"))).Replace("[amp]", "&")
            lblCategory.Text = categoryName
            If categoryName <> "" Then
                BindFields(categoryName)
            Else
                lblMsg.Text = "Error"
            End If

        End If
    End Sub

    Private Sub BindFields(ByVal Category As String)

        Dim ds As New DataSet
        Try
            Dim xmlFileName As String = Page.MapPath("xml/MsgFields.xml")
            ds.ReadXml(xmlFileName)

            Dim ds2 As DataSet = ds.Clone()
            ds2.Tables.Add()
            Dim drs As DataRow() = ds.Tables(0).Select("category='" & Category & "'")
            Dim dr As DataRow
            For Each dr In drs
                ds2.Tables(0).ImportRow(dr)
            Next

            Dim dv As New DataView(ds2.Tables(0))
            dv.Sort = "description"

            lbFields.DataSource = dv
            lbFields.DataTextField = "description"
            lbFields.DataValueField = "name"
            lbFields.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        ds.Dispose()
    End Sub


End Class
