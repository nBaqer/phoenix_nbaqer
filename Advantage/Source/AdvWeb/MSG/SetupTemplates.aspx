<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SetupTemplates.aspx.vb" Inherits="MSG_SetupTemplates" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>

</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlTemplates" runat="server" DataKeyField="TemplateId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="~/images/Inactive.gif" runat="server"
                                            Visible='<%# (Not Ctype(Container.DataItem("Active"), Boolean)).ToString %>'
                                            CommandArgument='<%# Container.DataItem("TemplateId")%>' CausesValidation="False" />
                                        <asp:ImageButton ID="imgActive" ImageUrl="~/images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Active"), Boolean).ToString %>'
                                            CommandArgument='<%# Container.DataItem("TemplateId")%>' CausesValidation="False" />
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("Active")%>' />
                                        <asp:LinkButton Text='<%# Container.DataItem("Descrip")%>' runat="server" CssClass="itemstyle"
                                            CommandArgument='<%# Container.DataItem("TemplateId")%>' ID="Linkbutton2" CausesValidation="False" />

                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="y" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCode" runat="server" CssClass="label">Code <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" runat="server" CssClass="textbox" TabIndex="1"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblActivePrompt" runat="server" CssClass="label">Status <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlActive" runat="server" CssClass="dropdownlist" TabIndex="2" /></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDescrip" runat="server" CssClass="label">Description <span style="color: red">*</span></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="textbox" TabIndex="3" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGroup" runat="server" CssClass="label">Campus Groups <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGroup" runat="server" CssClass="dropdownlist" TabIndex="4">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblGroup" runat="server" CssClass="label">Group<span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdownlist" Width="50%" TabIndex="5">
                                                </asp:DropDownList>
                                                <asp:Button ID="btnAddGroup" runat="server" Text="<- Add" />
                                                <asp:TextBox ID="txtGroup" runat="server" CssClass="textbox" Width="30%" TabIndex="6"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" align="left" valign="top">
                                                <asp:Label ID="LblModule" runat="server" CssClass="label">Module<span style="color: #ff0000">*</span></asp:Label></td>
                                            <td align="left" class="contentcell4">
                                                <asp:DropDownList ID="ddlModule" runat="server" CssClass="dropdownlist" AutoPostBack="True" TabIndex="7">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" align="left" valign="top">
                                                <asp:Label ID="LblEntity" runat="server" CssClass="label">Entity<span style="color: #ff0000">*</span></asp:Label></td>
                                            <td align="left" class="contentcell4">
                                                <asp:DropDownList ID="ddlEntity" runat="server" CssClass="dropdownlist" TabIndex="8" AutoPostBack="True" />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <div style="width: 100%">


                                        <telerik:RadEditor runat="server" ID="fck" ToolsFile="~/MSG/xml/RadEditorToolsFile.xml" Width="100%">
                                            <Modules>
                                                <telerik:EditorModule Name="RadEditorHtmlInspector" Enabled="true" Visible="false" />
                                                <telerik:EditorModule Name="RadEditorNodeInspector" Enabled="true" Visible="false" />
                                                <telerik:EditorModule Name="RadEditorDomInspector" Enabled="false" />
                                                <telerik:EditorModule Name="RadEditorStatistics" Enabled="false" />
                                            </Modules>
                                            <ImageManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                            <MediaManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                            <FlashManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                            <TemplateManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                            <DocumentManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                        </telerik:RadEditor>
                                        <script type="text/javascript">
                                            Telerik.Web.UI.Editor.CommandList["FameTags"] = function (commandName, editor, args) {
                                                var elem = editor.getSelectedElement(); //returns the selected element.
                                                if (elem.tagName == "A") {
                                                    editor.selectElement(elem);
                                                    argument = elem;
                                                }
                                                else {
                                                    var content = editor.getSelectionHtml();
                                                    var link = editor.get_document().createElement("A");
                                                    link.innerHTML = content;
                                                    argument = link;
                                                }

                                                var myCallbackFunction = function (sender, args) {
                                                    if (args != null) {
                                                        editor.pasteHtml(String.format("{0}", args.innerHTML))
                                                    }
                                                    else {
                                                        editor.pasteHtml(String.format("{0}", null))
                                                    }
                                                }

                                                editor.showExternalDialog(
                                                    'SpecialFieldsPopupRadEditor.aspx?categoryName=' + args.get_value().replace('&amp;', '[amp]'),
                                                    argument,
                                                    370,
                                                    300,
                                                    myCallbackFunction,
                                                    null,
                                                    'Insert [Field]',
                                                    true,
                                                    Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
                                                    true,
                                                    true);
                                            };
                                        </script>
                                    </div>
                                </div>
                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="customvalidator"
            Display="none"></asp:CustomValidator>
        <asp:Panel ID="pnlrequiredfieldvalidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="true"
            ShowSummary="false"></asp:ValidationSummary>
        <!-- Link Buttons and other hidden controls -->
        <asp:LinkButton ID="lbNewTemplate" runat="server" />
        <asp:LinkButton ID="lbOpenTemplate" runat="server" />
        <asp:LinkButton ID="lbSaveTemplate" runat="server" />
        <asp:LinkButton ID="lbImportWord" runat="server" />
        <asp:LinkButton ID="lbPreview" runat="server" />
        <asp:HiddenField ID="txtTemplateFile" runat="server" />
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>

</asp:Content>


