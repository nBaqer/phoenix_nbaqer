<%@ Control Language="VB" ClassName="MessagingMenu" %>
<%@ Import Namespace="FAME.AdvantageV1.BusinessFacade.MSG" %>
<link href="../css/TM.css" rel="stylesheet" type="text/css" />
<link href="../css/MSG.css" rel="stylesheet" type="text/css" />

<script runat="server">    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsPostBack Then
            'BindFilterJS()
            ' hide the process all button if we are in the outbox
            If Request.Params("source") = "1" Then
                Menu3.Visible = False
            End If
        End If
        BindFilterJS()
        
        ShowFilterOnStatusBar()
    End Sub
    
    Protected Sub ShowFilterOnStatusBar()
        Dim sb as New StringBuilder
        if Session("source") <> "1" Then
            sb.Append("[Outbox]")
        Else
            sb.Append("[Sent Items]")
        End If               
        MSGCommon.SetBrowserStatusBar(Me.Page, sb.ToString)
    End Sub    

    ' Install the javascript handler for the filter button
    Protected Sub BindFilterJS()
        Dim popUrl As New StringBuilder
        popUrl.Append("FilterMessagesPopup.aspx?rtype=" & Session("rtype"))
        popUrl.Append("&rid=" & Request.Params("rid"))
        popUrl.Append("&rtxt=" & Request.Params("rtxt"))
        popUrl.Append("&dtype=" & Request.Params("dtype"))
        popUrl.Append("&mgid=" & Request.Params("mgid"))
        popUrl.Append("&tid=" & Request.Params("tid"))
        popUrl.Append("errors=" & Request.Params("errors"))
        Menu2.Attributes.Add("onclick", MSGCommon.GetJavsScriptPopup(popUrl.ToString, 400, 320, "msgMenu_hfResult"))
    End Sub
    
    Protected Sub Menu2_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)
        If hfResult.Value.Length = 0 Then Return
        Dim p() As String = hfResult.Value.Split("|")
        hfResult.Value = ""
        
        ' only apply the filter if it is properly formatted
        If p.Length = 7 Then
            Session("rtype") = p(0)
            Session("rid") = p(1)
            Session("rtxt") = p(2)
            Session("dtype") = p(3)
            Session("mgid") = p(4)
            Session("tid") = p(5)
            Session("errors") = p(6)
            Dim sb As New StringBuilder
            sb.Append("ViewMessages.aspx?source=" + Request.Params("source"))
            If Not Session("rtype") Is Nothing AndAlso Session("rtype") <> "" Then
                sb.Append("&rtype=" + Session("rtype"))
            End If
            If Not Session("rid") Is Nothing AndAlso Session("rid") <> "" Then
                sb.Append("&rid=" + Session("rid"))
            End If
            If Not Session("rtxt") Is Nothing AndAlso Session("rtxt") <> "" Then
                sb.Append("&rtxt=" + Session("rtxt"))
            End If
            If Not Session("dtype") Is Nothing AndAlso Session("dtype") <> "" Then
                sb.Append("&dtype=" + Session("dtype"))
            End If
            If Not Session("mgid") Is Nothing AndAlso Session("mgid") <> "" Then
                sb.Append("&mgid=" + Session("mgid"))
            End If
            If Not Session("tid") Is Nothing AndAlso Session("tid") <> "" Then
                sb.Append("&tid=" + Session("tid"))
            End If
            If Not Session("errors") Is Nothing AndAlso Session("errors") <> "" Then
                sb.Append("&errors=" + Session("errors"))
            End If
            Response.Redirect(sb.ToString)
        End If
    End Sub

    ' Process all the messages in the current filter
    Protected Sub Menu3_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)
        Dim url As String = String.Format("ViewMessages.aspx?source={0}&rid={1}&dtype={2}&mgid={3}&tid={4}&error={5}&processall=1&rtype={6}", _
                                            Session("source"), Session("rid"), Session("dtype"), Session("mgid"), _
                                            Session("tid"), Session("errors"), Session("rtype"))
        Response.Redirect(url)
    End Sub

    ' Print all the messages in the current filter
    Protected Sub Menu4_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)
        Dim url As String = String.Format("ViewMessages.aspx?source={0}&rid={1}&dtype={2}&mgid={3}&tid={4}&error={5}&printall=1&rtype={6}", _
                                                    Session("source"), Session("rid"), Session("dtype"), Session("mgid"), _
                                                    Session("tid"), Session("errors"), Session("rtype"))
        Response.Redirect(url)
    End Sub
</script>

<div style="margin: 0; padding: 0">
    <table width="100%" summary="SysMenu" cellpadding="0" cellspacing="0">
        <tr>
            <td class="MenuFrame" valign="middle" nowrap>
                                 <div class="calendarmenuitems">
                                    <asp:Menu ID="Menu1" runat="server" DynamicHorizontalOffset="0" StaticMenuItemStyle-ItemSpacing="0"
                                        DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false" Orientation="Horizontal">
                                        <StaticMenuStyle />
                                        <StaticMenuItemStyle CssClass="taskbuttons" />
                                        <DynamicMenuStyle />
                                        <DynamicMenuItemStyle />
                                        <DynamicHoverStyle />
                                        <Items>
                                            <asp:MenuItem NavigateUrl="AddMessage.aspx" Text="New Message" Value="New Message" />
                                        </Items>
                                    </asp:Menu>
                                </div>
                                <div class="calendarmenuitems">
                                    <asp:Menu ID="Menu2" runat="server" DynamicHorizontalOffset="0" StaticMenuItemStyle-ItemSpacing="0"
                                        DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false" Orientation="Horizontal"
                                        OnMenuItemClick="Menu2_MenuItemClick">
                                        <StaticMenuStyle />
                                        <StaticMenuItemStyle CssClass="taskbuttons" />
                                        <DynamicMenuStyle />
                                        <DynamicMenuItemStyle />
                                        <DynamicHoverStyle />
                                        <Items>
                                            <asp:MenuItem Text="Filter" Value="Filter" />
                                        </Items>
                                    </asp:Menu>
                                </div>
                                <div class="calendarmenuitems">
                                    <asp:Menu ID="Menu3" runat="server" DynamicHorizontalOffset="0" StaticMenuItemStyle-ItemSpacing="0"
                                        DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false" Orientation="Horizontal"
                                        OnMenuItemClick="Menu3_MenuItemClick">
                                        <StaticMenuStyle />
                                        <StaticMenuItemStyle CssClass="taskbuttons"/>
                                        <DynamicMenuStyle />
                                        <DynamicMenuItemStyle />
                                        <Items>
                                            <asp:MenuItem Text="Process All" Value="Process All" />
                                        </Items>
                                    </asp:Menu>
                                </div>
                              <div class="calendarmenuitems">
                    <asp:Menu ID="Menu4" runat="server" DynamicHorizontalOffset="0" StaticMenuItemStyle-ItemSpacing="0"
                        DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false" Orientation="Horizontal"
                        OnMenuItemClick="Menu4_MenuItemClick">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons"/>
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle />
                        <Items>
                            <asp:MenuItem Text="Print All" Value="Print All" />
                        </Items>
                    </asp:Menu>
                </div>
                             <div class="taskbgrightdiv"></div>
                            </td>
                        </tr>
                    </table>
    <asp:HiddenField ID="hfResult" runat="server" />
</div>
