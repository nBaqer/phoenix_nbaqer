<%@ Register TagName="SysHeader" TagPrefix="Header" Src="MSGHeader.ascx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SpecialFieldsPopup.aspx.vb" Inherits="Fields" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Insert Special Field</title>
    <meta http-equiv="pragma" content="NO-CACHE"/>
    <LINK href="../css/MSG.css" type="text/css" rel="stylesheet"/>
	<base target="_self" />
	<!--<script type="text/javascript" src="./js/insertfield.js"></script> -->
		<script type="text/javascript">
		//<![CDATA[
		var oEditor = window.parent.InnerDialogLoaded() ;
		window.onload = function ()
		{
			// Show the "Ok" button
			window.parent.SetOkButton( true ) ;
			window.parent.document.getElementById('btnOk').value = "Insert Field";
		}
		function Ok ()
		{
			//var strSelection=document.getElementById('lbFields').value; 
			var ddl = document.getElementById('lbFields');
			//var rep_str ="";
			var strSelection = "";
			for (i=0; i < ddl.options.length; i++) {
				if (ddl.options[i].selected)
					strSelection += ddl.options[i].value + "&nbsp";      
			}			
    
			if(strSelection != '') 
			{
				if(oEditor == null) 
					alert('error'); 
				else 
					oEditor.FCK.InsertHtml(strSelection);
			}
			window.close(); /* and close the window */
		}		
		//]]>
		</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellPadding="4" width="100%" align="center" summary="Header Table" border="0">
		<tr>
			<td align="left">
				<asp:label id="lblCategory" runat="server" CssClass="Label" Width="100px">Category:</asp:label>&nbsp;
			</td>
		</tr>
		<tr>
			<td align="left">
				<asp:DropDownList id="ddlFieldCategories" runat="server" Width="220" CssClass="DropDownList" AutoPostBack="True"></asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td align="left">
				<asp:ListBox id="lbFields" runat="server" Width="220" CssClass="ListBox" Height="220px" SelectionMode="Multiple"></asp:ListBox>
			</td>
		</tr>
	    </table>
	    <table width="100%" align="center" summary="Content Table" border="0">
		<tr>
			<td valign="middle" align="center">
				<asp:Label id="lblMsg" runat="server" CssClass="errormsg1"></asp:Label></td>
		</tr>
	    </table>
    </div>
    </form>
</body>
</html>
