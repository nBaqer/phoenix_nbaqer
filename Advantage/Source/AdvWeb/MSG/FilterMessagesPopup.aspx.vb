'
' FAME
' Copyright (c) 2006
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdvMessaging
' Description: Allows the user to select a filter for the Outbox and Sent Items views
'   This form is used by Queue.aspx
' Parameters can be passed to this to select initial values in the filter.  Params are as follows:
'   rtype, rid, dtype, mgid, tid, errors

Imports System.Text
Imports FAME.AdvantageV1.BusinessFacade.MSG

Partial Class AdvantageApp_MSG_QueueFilter
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            ' Build the drop down lists
            MsgCommon.BuildRecipientTypeDDL(ddlRecipientType)
            MsgCommon.BuildDeliveryTypeDDL(ddlDeliveryType)
            MSGCommon.BuildCategoryDDL(MSGCommon.GetCampusID(), ddlCategory)
            MSGCommon.BuildAvailableTemplates(MSGCommon.GetCurrentUserId(), MSGCommon.GetCampusID(), Nothing, Nothing, ddlTemplate)

            ' Load the parameters into the form
            LoadParams()

            ' put javascript handler on the ok button
            ' The ok button return a string with this format:
            '       rtype|rid|dtype|mgid|tid|errors
            Dim js As New StringBuilder
            js.Append("window.returnValue=document.getElementById('ddlRecipientType').value + '|' + ")
            js.Append("document.getElementById('txtRecipientId').value + '|' + ")
            js.Append("document.getElementById('txtRecipientName').value + '|' + ")
            js.Append("document.getElementById('ddlDeliveryType').value + '|' + ")
            js.Append("document.getElementById('ddlCategory').value + '|' + ")
            js.Append("document.getElementById('ddlTemplate').value + '|' + ")
            js.Append("document.getElementById('cbShowErrors').value;" & vbCrLf)
            js.Append("window.close();")
            btnOK.Attributes.Add("onclick", js.ToString)

            ' put javascript handler on the cancel button
            btnCancel.Attributes.Add("onclick", "window.returnValue=''; window.close();")

            Dim js2 As String = MSGCommon.GetRecipientLookupJS("txtRecipientName", "hfRecipientType", "txtRecipientId", True)
            Me.lbStudentLookup.Attributes.Add("onclick", js2)
        End If
    End Sub

    ' Method: LoadParams
    ' Description: Loads parameters passed to the form on loading the page
    '   Paramteres include:
    '       rtype   = recipient type
    '       rid     = recipient id
    '       rtxt    = recipient name
    '       dtype   = delivery type
    '       mgid    = message group id / category
    '       tid     = template id
    '       errors  = show errors
    Private Sub LoadParams()
        Try
            ddlRecipientType.SelectedValue = Request.Params("rtype")
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlRecipientType.SelectedIndex = 0
        End Try
        ' this call is ????
        txtRecipientId.Value = Request.Params("rid")
        txtRecipientName.Text = Request.Params("rtxt")

        Try
            ddlDeliveryType.SelectedValue = Request.Params("dtype")
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlDeliveryType.SelectedIndex = 0
        End Try

        Try
            ddlCategory.SelectedValue = Request.Params("mgid")
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlCategory.SelectedIndex = 0
        End Try

        'ViewState("tid") = Request.Params("tid")

        If Request.Params("errors") = "1" Then cbShowErrors.Checked = True
    End Sub

    ' Method: ddlCategory_SelectedIndexChanged
    ' Description: Handler for the user selecting a template category.  This displays all the templates for 
    '   the selected category.
    Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Dim MessageGroupid As String = ddlCategory.SelectedValue
        'ddlTemplate.DataSource = TemplatesFacade.GetAllTemplates(Nothing, Nothing, MSGCommon.GetCampusID(), MessageGroupid, True, False)
        ddlTemplate.DataTextField = "TemplateDescrip"
        ddlTemplate.DataValueField = "MessageTemplateId"
        ddlTemplate.DataBind()
        ddlTemplate.Items.Insert(0, New ListItem("--Select--", ""))
        ddlTemplate.SelectedIndex = 0        
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.ddlTemplate.SelectedIndex = 0
        Me.cbShowErrors.Checked = False
        Me.ddlCategory.SelectedIndex = 0
        Me.txtRecipientName.Text = ""
        Me.txtRecipientId.Value = ""
        Me.ddlRecipientType.SelectedIndex = 0
        Me.ddlDeliveryType.SelectedIndex = 0
    End Sub

    Protected Sub ddlRecipientType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRecipientType.SelectedIndexChanged
        hfRecipientType.Value = ddlRecipientType.SelectedValue
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click

    End Sub
End Class
