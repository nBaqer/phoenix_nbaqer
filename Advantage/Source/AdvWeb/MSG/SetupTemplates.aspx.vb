' ===============================================================================
' SetupTemplates
' Handles adding, editing and deleting (make inactive) of templates
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.BusinessFacade.MSG
Imports FAME.AdvantageV1.Common.MSG
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class MSG_SetupTemplates
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")


        Dim fac As New UserSecurityFacade
        Dim m_Context As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        MSGCommon.AdvInit(Me)
        If Not Page.IsPostBack Then
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)
            ' clear out the form with original values
            ResetForm()
            ' display the templates defined in the system
            BindTemplates()

            ' Bind the task to the form
            Dim TemplateId As String = Request.Params("templateid")
            If Not TemplateId Is Nothing Then
                BindTemplate(TemplateId)
            End If
        Else
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title
    End Sub

#Region "Advantage Permissions"
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub
#End Region

    ''' <summary>
    ''' Fills the form with a template given a TemplateId
    ''' </summary>
    ''' <param name="TemplateId"></param>
    ''' <remarks></remarks>
    Protected Sub BindTemplate(ByVal TemplateId As String)
        Try
            Dim info As TemplateInfo = TemplatesFacade.GetTemplateInfo(TemplateId)
            ViewState("moddate") = info.ModDate ' need this to do a proper delete
            ddlActive.SelectedValue = info.Active.ToString()
            txtCode.Text = info.Code
            txtName.Text = info.Descrip
            CommonWebUtilities.SetupRadEditor(fck, "AdvMessaging_MsgCompose", info.EntityId)
            fck.Content = info.Data 'Server.HtmlEncode(
            ' select the module
            Try
                ddlModule.SelectedValue = info.ModuleId
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try
            ' select the entity
            Try
                If ddlModule.SelectedValue <> "" Then
                    MSGCommon.BuildModuleEntitiesDDL(ddlModule.SelectedValue, ddlEntity)
                    ddlEntity.SelectedValue = info.ModuleEntityId
                Else
                    ddlEntity.Items.Clear()
                End If
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try
            ' select the category
            Try
                ddlGroup.SelectedValue = info.GroupId
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try
            Try
                ddlCampGroup.SelectedValue = info.CampGroupId
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try

            ' set the special field parameter
            MSGCommon.SetSpecialFieldPopupParam(info.EntityId)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Binds all templates
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindTemplates()
        If radstatus.SelectedIndex = 0 Then
            dlTemplates.DataSource = TemplatesFacade.GetTemplates(Nothing, Nothing, True, False)
        ElseIf radstatus.SelectedIndex = 1 Then
            dlTemplates.DataSource = TemplatesFacade.GetTemplates(Nothing, Nothing, False, True)
        Else
            dlTemplates.DataSource = TemplatesFacade.GetTemplates(Nothing, Nothing, True, True)
        End If
        dlTemplates.DataBind()
    End Sub

    ''' <summary>
    ''' Resets the form to the intial state.  All controls that are databound are initialized
    ''' from their datasource.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetForm()
        ' clear out the form
        txtTemplateFile.Value = ""
        ' Change the toolbar so that it displays the full edit set
        ' MSGCommon.SetupEditor(fck, "AdvMessaging_MsgDesigner") 'NZ 11/21/2012
        fck.Content = ""
        fck.Tools.Clear()

        MSGCommon.BuildStatusDDL(ddlActive)
        MSGCommon.BuildCampusGroups(ddlCampGroup)
        If ddlCampGroup.SelectedValue <> "" Then
            MSGCommon.BuildCategoryDDL(ddlCampGroup.SelectedValue, ddlGroup)
        End If

        ddlActive.SelectedValue = True
        txtCode.Text = ""
        txtName.Text = ""

        MSGCommon.BuildModulesDDL(Nothing, Me.ddlModule)
        ddlEntity.Items.Clear()

        MSGCommon.SetSpecialFieldPopupParam("")
    End Sub

    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist containing all the users.
    ''' In response, this handler should populate the info for a template
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlTemplates_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlTemplates.ItemCommand
        ' get the selected groupid and store it in the viewstate object
        Dim templateid As String = e.CommandArgument.ToString
        ViewState("id") = templateid
        dlTemplates.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If templateid Is Nothing Or templateid = "" Then
            MSGCommon.Alert(Me, "There was a problem displaying the details for this template.")
            Return
        End If

        ' bind the result to the form
        BindTemplate(templateid)

        ' Advantage specific stuff
        'CommonWebUtilities.SetStyleToSelectedItem(dlTemplates, templateid, ViewState, Header1)
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlTemplates, templateid)
    End Sub
    ''' <summary>
    ''' Checks the form's contents for validity.  If any control is
    ''' not valid, an alert is made and the function returns false.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As Boolean
        ' Check the task drop down
        If Me.txtCode.Text.Length = 0 Then
            MSGCommon.Alert(Me, "Code is required.")
            Return False
        End If

        If txtCode.Text.Length > 12 Then
            MSGCommon.DisplayErrorMessage(Me, "Code cannot be greater than 12 characters.")
            Return False
        End If

        If Me.txtName.Text.Length = 0 Then
            MSGCommon.Alert(Me, "Description is required.")
            Return False
        End If

        If Me.ddlCampGroup.SelectedValue = "" Then
            MSGCommon.Alert(Me, "Campus Group is required.")
            Return False
        End If

        If Me.ddlGroup.SelectedIndex = 0 Then
            MSGCommon.Alert(Me, "Group is required.")
            Return False
        End If

        If ddlEntity.SelectedValue = "" Then
            MSGCommon.Alert(Me, "Entity is required.")
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            ' check that the user has entered in all the right values
            If Not ValidateForm() Then Return

            Dim templateid As String = ViewState("id")
            Dim info As New TemplateInfo
            ' determine if this is an update or a new by looking
            ' at the ViewState which is set by passing a parameter to the page.
            If templateid Is Nothing Or templateid = "" Then
                info.IsInDB = False
                ViewState("id") = info.TemplateId
            Else
                info.IsInDB = True
                info.TemplateId = templateid
            End If

            info.Active = CType(ddlActive.SelectedValue, Boolean)
            info.Code = txtCode.Text
            info.Descrip = txtName.Text
            info.Data = fck.Content
            info.GroupId = ddlGroup.SelectedValue
            info.ModuleEntityId = ddlEntity.SelectedValue
            info.CampGroupId = ddlCampGroup.SelectedValue

            Dim opOk As Boolean = TemplatesFacade.UpdateTemplate(info, MSGCommon.GetCurrentUserId())
            If opOk Then
                MSGCommon.Alert(Page, "Template was saved successfully")
            Else
                MSGCommon.Alert(Page, "Errors ocurred while saving the template")
            End If

            ' update the left hand side
            BindTemplates()
            InitButtonsForEdit()
            CommonWebUtilities.RestoreItemValues(dlTemplates, templateid)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Page, "Template could not be saved. Error=" + ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Simply reset the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        ViewState("id") = Nothing
        ViewState("moddate") = Nothing
        ResetForm()

        ' Advantage specific stuff
        'CommonWebUtilities.SetStyleToSelectedItem(Me.dlTemplates, Guid.Empty.ToString, ViewState, Header1)
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlTemplates, Guid.Empty.ToString)
    End Sub

    ''' <summary>
    ''' Deletes a template
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            Dim id As String = ViewState("id")
            If Not id Is Nothing AndAlso id <> "" Then
                Dim modDate As DateTime = ViewState("moddate")
                Dim res As String = TemplatesFacade.DeleteTemplate(id, modDate)
                If res <> "" Then
                    MSGCommon.DisplayErrorMessage(Me, res)
                Else
                    ViewState("id") = Nothing
                    ResetForm()
                    ' update the left pane
                    BindTemplates()

                    ' Advantage specific stuff
                    ' CommonWebUtilities.SetStyleToSelectedItem(Me.dlTemplates, Guid.Empty.ToString, ViewState, Header1)
                    InitButtonsForLoad()
                End If
                CommonWebUtilities.RestoreItemValues(dlTemplates, Guid.Empty.ToString)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, "Error.  Record could not be deleted.")
        End Try
    End Sub

    ''' <summary>
    ''' Display active, inactive or all based on the user checking the radio box option
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        ResetForm()
        BindTemplates()
    End Sub

    ''' <summary>
    ''' Build a list of entities for the selected module
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
        fck.Content = ""
        fck.Tools.Clear()
        If ddlModule.SelectedValue <> "" Then
            MSGCommon.BuildModuleEntitiesDDL(ddlModule.SelectedValue, ddlEntity)
        End If
    End Sub


    ''' <summary>
    ''' Set the parameter for the special fields popup
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlEntity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEntity.SelectedIndexChanged
        ' need to get the entityid from moduleentityid
        Dim EntityId As String = EntitiesFieldGroupsFacade.GetEntityId(ddlEntity.SelectedValue)
        MSGCommon.SetSpecialFieldPopupParam(EntityId)
        ' Load the template into the editor     
        fck.Content = ""
        fck.Tools.Clear()
        CommonWebUtilities.SetupRadEditor(fck, "AdvMessaging_MsgCompose", EntityId)
    End Sub

    ''' <summary>
    ''' Allows the user to quickly add a group
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddGroup.Click
        ' first checkf that the user entered a group
        If Me.txtGroup.Text = "" Then
            MSGCommon.Alert(Me, "Please enter the group description in the textbox before clicking the Add button.")
            Return
        End If

        ' build the info class and add it to the db
        Dim info As New GroupInfo
        info.Code = "[NONE]" ' assign a default code
        info.Descrip = txtGroup.Text
        'info.CampGroupId = TMCommon.GetCampusID()
        If GroupsFacade.UpdateGroup(info, MSGCommon.GetCurrentUserId()) Then
            ' category was added correctly
            ' clear the textbox and add it to the drop down list and make it the selected one
            txtGroup.Text = ""
            ddlGroup.Items.Add(New ListItem(info.Descrip, info.GroupId))
            ddlGroup.SelectedValue = info.GroupId
        Else
            MSGCommon.DisplayErrorMessage(Me, "Failed to add group")
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(btnAddGroup)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
