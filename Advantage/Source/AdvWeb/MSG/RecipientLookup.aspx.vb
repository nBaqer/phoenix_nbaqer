'
' FAME
' Copyright (c) 2005, 2006
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdvMessaging
' Description: Popup window which allows the user to open a template for editing.
'   This popup return the TemplateId to the caller through javascript.
' This form allows for an inital search term to be passed
'   RecipientLookup.aspx?s={MyInitialSearchTerm}
' Search terms can optionally specify the recipient type...
'   RecipientLookup.aspx?s={RecipientType:MyInitialSearchTerm}
'   Ex: RecipientLookup.aspx?s=Student:Amy
' New paramter to enable or disable the RETYPE dropdownlist
'   RecipientLookup.aspx?enableretype=false|true

' History:
' 8/21/06 - Ben: Changed initretype to reflect the entityid rather than the description

Imports System.Text
Imports System.Web.UI.WebControls
Imports FAME.AdvantageV1.BusinessFacade.MSG

Partial Class AdvantageApp_MSG_RecipientLookup
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'MSGCommon.AdvInit(Me)
            If Not Page.IsPostBack Then
                ' fill the drop down list
                MSGCommon.BuildRecipientTypeDDL(ddlRecipientType)

              
                ' set the initial search string
                Dim initReId As String = Request.Params("initreid")
                If Not initReId Is Nothing And initReId <> "" Then
                    Dim i As Integer = initReId.IndexOf(":")
                    If i > 0 Then
                        txtSearch.Text = initReId.Substring(i + 1)
                    Else
                        txtSearch.Text = initReId
                    End If
                End If

                ' set the initial retype
                Dim initReType As String = Request.Params("initretype")
                If Not initReType Is Nothing Then
                    ddlRecipientType.SelectedValue = initReType
                End If

                ' disable control is no ReType param
                Dim strDoReType As String = Request.Params("enableretype")
                If strDoReType Is Nothing Then strDoReType = "true"
                If strDoReType.ToLower() = "true" Then
                    ddlRecipientType.Enabled = True
                Else
                    ddlRecipientType.Enabled = False
                End If

                ' add javascript to the check if the user has entered search criteria
                Dim js As New StringBuilder
                js.Append("var strData=document.getElementById('txtSearch').value;" & vbCrLf)
                'js.Append("bar1.showBar();" & vbCrLf)
                btnSearch.Attributes.Add("onclick", js.ToString)

                ' add javascript to the cancel button
                btnCancel.Attributes.Add("onclick", "window.returnValue=''; window.close();")

                ' Do the search if the contact type and initial search criteria have
                ' been specified
                If Not initReType Is Nothing AndAlso initReType <> "" AndAlso txtSearch.Text.Length > 0 Then
                    DoSearch()
                End If

                If ddlRecipientType.SelectedIndex = 0 Then
                    ddlRecipientType.SelectedValue = 394
                End If

            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        
    End Sub


    ' Method: BindItems
    ' Description: Binds the search results for a recipient lookup
    Private Sub BindItems()
        Try
			rptResults.DataSource = ContactSearchFacade.RecipientSearch(ddlRecipientType.SelectedValue, txtSearch.Text, MSGCommon.GetCampusID())
            rptResults.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    Private Sub DoSearch()
        ' get the data from the controsl and query the db
        Dim strSearch As String = Me.txtSearch.Text
        strSearch.TrimStart(" ")
        strSearch.TrimEnd(" ")
        If strSearch.Length > 0 Then
            BindItems()
            If rptResults.Items.Count = 0 Then
                MSGCommon.Alert(Page, "No results found.")
            End If
        Else
            MSGCommon.Alert(Page, "No search criteria entered!")
        End If
    End Sub

    ' Method: btnSearch_Click
    ' Description: Performs the search for a recipient in response to the user clicking the "Search" button
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        DoSearch()
    End Sub

    Public Sub rptResults_OnItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptResults.ItemDataBound
        Try
            Dim lb As LinkButton = e.Item.FindControl("lbFullName")
            If Not lb Is Nothing Then
                Dim RecipientId As String = lb.CommandArgument.ToString
                Dim FullName As String = lb.CommandName.ToString
                Dim js As New StringBuilder
                js.Append("if (navigator.appName == 'Microsoft Internet Explorer') window.returnValue='")
                js.Append(ddlRecipientType.SelectedValue)
                js.Append(":" & FullName)
                js.Append("|" & RecipientId)
                js.Append("'; else window.opener.returnValue='")
                js.Append(ddlRecipientType.SelectedValue)
                js.Append(":" & FullName)
                js.Append("|" & RecipientId)
                js.Append("';  window.close();")
                lb.Attributes.Add("onclick", js.ToString())
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
End Class
