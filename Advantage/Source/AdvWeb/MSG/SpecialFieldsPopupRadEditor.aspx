﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SpecialFieldsPopupRadEditor.aspx.vb" Inherits="MSG_SpecialFieldsPopupRadEditor" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Insert Special Field</title>
    <meta http-equiv="pragma" content="NO-CACHE" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellpadding="4" width="100%" align="center" summary="Header Table" border="0">
                <tr>
                    <td align="left">Please select from the available fields below
                        <br />
                        for category:&nbsp;<b><asp:Label ID="lblCategory" runat="server" CssClass="Label"></asp:Label></b>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:ListBox ID="lbFields" runat="server" CssClass="ListBox" Height="120px" Width="100%" SelectionMode="Single"></asp:ListBox>
                    </td>
                </tr>
            </table>
            <table width="100%" align="center" summary="Content Table" border="0">
                <tr>
                    <td valign="middle" align="center">
                        <input type="button" onclick="javascript: insertLink();" value="Insert Field" />
                        <asp:Label ID="lblMsg" runat="server" CssClass="errormsg1"></asp:Label>
                    </td>
                </tr>
            </table>

            <script type="text/javascript">
                if (window.attachEvent)
                {
                    window.attachEvent("onload", initDialog);
                }
                else if (window.addEventListener)
                {
                    window.addEventListener("load", initDialog, false);
                }

                var linkName = document.getElementById("lbFields");

                var workLink = null;

                function getRadWindow()
                {
                    if (window.radWindow)
                    {
                        return window.radWindow;
                    }
                    if (window.frameElement && window.frameElement.radWindow)
                    {
                        return window.frameElement.radWindow;
                    }
                    return null;
                }

                function initDialog()
                {
                    var clientParameters = getRadWindow().ClientParameters; //return the arguments supplied from the parent page
                    if (clientParameters != null)
                    {
                        linkName.value = clientParameters.innerHTML;
                    }
                    workLink = clientParameters;
                }

                function insertLink() //fires when the Insert Link button is clicked
                {
                    //create an object and set some custom properties to it 
                    var lb = document.getElementById("lbFields");
                    ///workLink.innerHTML = lb.value;
                    if (lb != null)
                    {
                        var options = lb.options;
                        for (var i = options.length - 1; i > 0; i--)
                        {
                            if (options[i].selected == true)
                            {
                                workLink.innerHTML = options[i].value;
                            }
                        }

                    }
                    getRadWindow().close(workLink); //use the close function of the getRadWindow to close the dialog and pass the arguments from the dialog to the callback function on the main page.
                }
            </script>


        </div>
    </form>
</body>
</html>
