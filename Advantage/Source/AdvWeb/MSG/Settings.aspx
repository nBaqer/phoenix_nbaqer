<%@ Register TagName="SysHeader" TagPrefix="Header" Src="MSGHeader.ascx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Settings.aspx.vb" Inherits="AdvantageApp_MSG_Settings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Advantage Messaging - Settings</title>
    <LINK href="../css/MSG.css" type="text/css" rel="stylesheet"/>
</head>
<body bgColor="#e9edf2">
    <form id="form1" runat="server">
    <div>
        <header:SysHeader ID="sysHeader" runat="server" />
		<p></p>
		<table width="80%" summary="General Settings Table">
			<tr>
				<td align="left" class="LabelBold">General Settings</td>
			</tr>
			<tr>
				<td align="left" class="Label">Administrator email:&nbsp;
					<asp:TextBox id="txtAdminEmail" runat="server" Width="250px" CssClass="textbox" ReadOnly="True"></asp:TextBox>
				</td>
			</tr>
		</table>
		<p></p>
		<table width="80%" summary="Workflow Settings Table">
			<tr>
				<td align="left" class="LabelBold">Workflow Settings</td>
			</tr>
			<tr>
				<td align="left" class="Label">Scheduler run time (HH:MMPM):&nbsp;
					<asp:TextBox ID="txtScheduler" Runat="server" CssClass="textbox" Width="65px" ReadOnly="True">11:15PM</asp:TextBox>
				</td>
			</tr>
			<tr>
				<td align="left" class="Label">Reload Workflow Rules:
					<asp:Button ID="btnReloadRules" Runat="server"  Text="Reload"></asp:Button>
					<asp:TextBox ID="txtConfirm" Runat="server" Width="0" Height="0" Visible=false></asp:TextBox>
				</td>
			</tr>
		</table>
		<p></p>
		<table width="80%" summary="Template Settings Table">
			<tr>
				<td align="left" class="LabelBold" colspan="2">Template Settings</td>
			</tr>
			<tr>
				<td align="left" class="Label">Default template used in Messaging Designer:&nbsp;
					<asp:Label id="lblDefTemplate" runat="server" CssClass="label" Width="200px">{None Defined}</asp:Label>&nbsp;
				</td>
			</tr>
			<tr>
				<td align="left"><asp:Button ID="btnSelectTemplate" Runat="server"  Text="Select Template"></asp:Button></td>
			</tr>
			<tr>
				<td align="left" class="Label">Load Default Templates:
					<asp:Button ID="btnLoadDefTemplates" Runat="server"  Text="Load"></asp:Button>
				</td>
			</tr>
		</table>
		<asp:textbox id="txtTemplateFile" runat="server" Width="0" EnableViewState="False" Visible="False" Height="0px"></asp:textbox>
		<table width="80%" align="center" summary="Header Table" border="0">
			<tr>
				<td align="left" class="LabelBold" colspan="2">Manage Template Categories</td>
			</tr>
			<tr>
				<td align="left">
					<asp:TextBox Runat="server" ID="edAddCategory" Width="200px" CssClass="TextBox"></asp:TextBox>
					<asp:linkbutton id="lbAdd" runat="server" CssClass="Label" Width="150px" CausesValidation="False">Add New Category</asp:linkbutton>
				</td>
			</tr>
		</table>
		<div style="OVERFLOW:auto;               ; HEIGHT:expression(document.body.clientHeight - 96 + 'px')">
			<asp:datagrid CellPadding="4" id="dgCategory" runat="server" Width="80%" OnCancelCommand="CancelEditCategory"
				OnUpdateCommand="UpdateCategory" OnEditCommand="EditCategory" OnDeleteCommand="DeleteCategory"
				AutoGenerateColumns="False" OnItemDataBound="dgCategory_OnItemDataBound" BackColor="White">
				<AlternatingItemStyle CssClass="Label"></AlternatingItemStyle>
				<ItemStyle CssClass="Label"></ItemStyle>
				<HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeader"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Category Name" ItemStyle-HorizontalAlign="Left">
						<ItemTemplate>
							<asp:Label Runat="server" Text='<%# Container.DataItem("Descrip") %>' ID="lbldgCategory">
								<%# Container.DataItem("GroupId") %>
							</asp:Label>
							<asp:Label Runat=server Visible=False Text='<%# Container.DataItem("GroupId") %>' ID="lbldgCategoryId">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox Runat="server" ID="editCategory" Text='<%# Container.DataItem("Descrip") %>'>
							</asp:TextBox>
							<asp:TextBox Runat="server" ID="editCategoryId" Text='<%# Container.DataItem("GroupId") %>' Visible=False>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="ModDate" HeaderText="Mod Date" ReadOnly="True"></asp:BoundColumn>
					<asp:ButtonColumn ItemStyle-HorizontalAlign="Left" ButtonType="LinkButton" CommandName="delete" HeaderText="" Text="Delete"></asp:ButtonColumn>
					<asp:EditCommandColumn ItemStyle-HorizontalAlign="Left" ButtonType="LinkButton" CancelText="Cancel" EditText="Rename" UpdateText="Done"
						HeaderText=""></asp:EditCommandColumn>
					<asp:ButtonColumn ItemStyle-HorizontalAlign="Left" ButtonType="LinkButton" CommandName="AssignField" HeaderText="" Text="Assign Special Fields"></asp:ButtonColumn>
				</Columns>
			</asp:datagrid>
			<asp:Label id="lblMsg" runat="server" CssClass="errormsg1"></asp:Label>
		</div>
		<div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
		<!-- end footer -->
    </div>
    </form>
</body>
</html>
