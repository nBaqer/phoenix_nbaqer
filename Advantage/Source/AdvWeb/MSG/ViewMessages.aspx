<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewMessages.aspx.vb" Inherits="AdvantageApp_MSG_Queue" EnableEventValidation="false" %>

<%@ Register TagName="MessagingMenu" TagPrefix="Menu" Src="Menu_Main.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Messages</title>
    <meta http-equiv="pragma" content="NO-CACHE" />
    <link href="../css/MSG.css" type="text/css" rel="stylesheet" />
    <!-- begin tooltip style -->
    <style type="text/css">
        #dhtmltooltip {
            BORDER-RIGHT: black 1px solid;
            PADDING-RIGHT: 2px;
            BORDER-TOP: black 1px solid;
            PADDING-LEFT: 2px;
            Z-INDEX: 100;
            FILTER: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);
            LEFT: -300px;
            VISIBILITY: hidden;
            PADDING-BOTTOM: 2px;
            BORDER-LEFT: black 1px solid;
            WIDTH: 150px;
            PADDING-TOP: 2px;
            BORDER-BOTTOM: black 1px solid;
            POSITION: absolute;
            BACKGROUND-COLOR: lightyellow;
        }

        #dhtmlpointer {
            Z-INDEX: 101;
            LEFT: -300px;
            VISIBILITY: hidden;
            POSITION: absolute;
        }
    </style>
    <!-- end tooltip style -->
</head>
<body>
    <script language="javascript" src="ToolTip.js" type="text/javascript"></script>
    <form id="form1" runat="server">

        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true" AsyncPostBackTimeout="3600">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            </Scripts>
        </telerik:RadScriptManager>

        <div>
            <Menu:MessagingMenu ID="msgMenu" runat="server" />
            <div class="AlignedDivCalendar">
                <table width="100%" summary="Content Table" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 325px" valign="top">
                            <table width="100%" summary="Options and Message Table" border="0">
                                <tr>
                                    <td>
                                        <div style="border: #83929f 1px solid; overflow: auto; height: expression(document.body.clientHeight - 80 + 'px')">
                                            <asp:Repeater ID="rptQueue" runat="server" OnItemDataBound="rptQueue_OnItemDataBound">
                                                <HeaderTemplate>
                                                    <table class="tablecontent" bgcolor="white">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr onmouseover="this.style.backgroundColor='lightblue';" onmouseout="this.style.backgroundColor='';">
                                                        <td style="border: solid 1px lightblue; padding: 0">
                                                            <table width="100%" class="Label">
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="ibView" runat="server" AlternateText="View details" ImageUrl="../Images/MSG/Queue_View.gif"
                                                                            CommandName="View" CommandArgument='<%# Container.DataItem("MessageId") %>' /></td>
                                                                    <td>To:</td>
                                                                    <td colspan="2" align="left"><b><%# Container.DataItem("RecipientType") %>:&nbsp;<%# Container.DataItem("RecipientName") %></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="ibDeliver" runat="server" AlternateText="Deliver now" ImageUrl="../Images/MSG/Queue_Deliver.gif"
                                                                            CommandName="Deliver" CommandArgument='<%# Container.DataItem("MessageId") %>' /></td>
                                                                    <td>Re:</td>
                                                                    <td colspan="2" align="left"><%# Container.DataItem("TemplateDescrip") %>&nbsp;{<%# Container.DataItem("ReName") %>}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="ibDelete" runat="server" AlternateText="Delete" ImageUrl="../Images/MSG/Queue_Delete.gif"
                                                                            CommandName="Delete" CommandArgument='<%# Container.DataItem("MessageId") %>' /></td>
                                                                    <td>Via:</td>
                                                                    <td align="left"><%# Container.DataItem("DeliveryType") %>&nbsp;on&nbsp;<%# Container.DataItem("CreatedDate") %></td>
                                                                    <td>
                                                                        <asp:ImageButton ID="imInfo" runat="server" ImageUrl="../Images/MSG/Queue_Info.gif" /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:LinkButton ID="lnkPrev" CssClass="Label" runat="server" Text="Previous" />
                                            <asp:LinkButton ID="lnkNext" CssClass="Label" runat="server" Text="Next" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <td align="left" valign="top" height="100%">
                                <!--This style keeps reverting!-->
                                <!--<div id="fckdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 100%; BORDER-BOTTOM: black 1px solid; 
			                    HEIGHT: expression(document.body.clientHeight - 80 + 'px'); BACKGROUND-COLOR: silver">-->
                                <!--Note to graphic designer: you may need to adjust the height to fit in with the header and footer -->
                                <div id="fckdiv" style="border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; width: 100%; border-bottom: black 1px solid; height: expression(document.body.clientHeight - 80 + 'px'); background-color: silver">



                                    <telerik:RadEditor runat="server" ID="fck" ToolsFile="~/MSG/xml/RadEditorToolsFile.xml" Height="400px" Width="550px">
                                        <Modules>
                                            <telerik:EditorModule Name="RadEditorHtmlInspector" Enabled="true" Visible="false" />
                                            <telerik:EditorModule Name="RadEditorNodeInspector" Enabled="true" Visible="false" />
                                            <telerik:EditorModule Name="RadEditorDomInspector" Enabled="false" />
                                            <telerik:EditorModule Name="RadEditorStatistics" Enabled="false" />
                                        </Modules>
                                        <ImageManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                        <MediaManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                        <FlashManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                        <TemplateManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                        <DocumentManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
                                    </telerik:RadEditor>
                                    <script type="text/javascript">
                                        if (Telerik.Web.UI.Editor !== undefined) {
                                            Telerik.Web.UI.Editor.CommandList["FameTags"] = function (commandName, editor, args) {
                                                var elem = editor.getSelectedElement(); //returns the selected element.

                                                if (elem.tagName == "A") {
                                                    editor.selectElement(elem);
                                                    argument = elem;
                                                }
                                                else {
                                                    //remove links if present from the current selection - because of JS error thrown in IE
                                                    editor.fire("Unlink");

                                                    //remove Unlink command from the undo/redo list
                                                    var commandsManager = editor.get_commandsManager();
                                                    var commandIndex = commandsManager.getCommandsToUndo().length - 1;
                                                    commandsManager.removeCommandAt(commandIndex);

                                                    var content = editor.getSelectionHtml();

                                                    var link = editor.get_document().createElement("A");

                                                    link.innerHTML = content;
                                                    argument = link;
                                                }

                                                var myCallbackFunction = function (sender, args) {
                                                    editor.pasteHtml(String.format("{0}", args.innerHTML))
                                                }

                                                editor.showExternalDialog(
                                                     'SpecialFieldsPopupRadEditor.aspx?categoryName=' + args.get_value().replace('&amp;', '[amp]'),
                                                     argument,
                                                     370,
                                                     300,
                                                     myCallbackFunction,
                                                     null,
                                                     'Insert [Field]',
                                                     false,
                                                     Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
                                                     false,
                                                     true);
                                            };
                                        }
                                    </script>

                                </div>
                            </td>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="txtConfirm" runat="server" />
                <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
            </div>
        </div>
    </form>
</body>
</html>
