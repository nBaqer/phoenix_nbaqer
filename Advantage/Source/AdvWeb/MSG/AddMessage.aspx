<%@ Register TagName="SysHeader" TagPrefix="Header" Src="MSGHeader.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddMessage.aspx.vb" Inherits="AdvantageApp_MSG_Compose" EnableViewState="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Compose Message</title>
    <link href="../css/tm.css" type="text/css" rel="stylesheet"/>
    <link href="../css/msg.css" type="text/css" rel="stylesheet"/>
</head>
<body style="background-color: White">
    <form id="form1" runat="server">

        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server" EnablePageMethods="true" AsyncPostBackTimeout="3600" >
             <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
        </Scripts>
       </telerik:RadScriptManager>
        <!-- message options -->
      <%--  <div class="MenuFramediv">
            &nbsp;</div>--%>
        <div class="ScrolledDiv" style="border:1px yellowgreen solid; height:750px; overflow:auto;">
            <table summary="Message Options Table" width="100%" class="tableborder" cellpadding="0"
                cellspacing="0">
                <tr>
                    <th colspan="2" nowrap>
                        <div style="width: 30%; text-align: left; float: left; background-color: Transparent; padding-top: 5px">
                            <h1>Compose Message</h1>
                        </div>
                        
                    </th>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <table summary="Add User Task table" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="labelcell">
                                    Module/Entity
                                </td>
                                <td class="contentcell">
                                    <asp:DropDownList ID="ddlModules" runat="server" CssClass="DropDownList" Width="70%" AutoPostBack="True" />&nbsp;/
                                    <asp:Label ID="lbEntity" runat="server" CssClass="Label" Text="{N/A}" />                                    
                                </td>
                            </tr>
                            <tr>
                                <td class="labelcell">
                                    Template<font style="color: Red">*</font>
                                </td>
                                <td class="contentcell">
                                    <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="DropDownListReq" Width="70%" AutoPostBack="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="labelcell">
                                    AdHoc Report
                                </td>
                                <td class="contentcell">
                                   <asp:DropDownList ID="ddlAdHocReport" runat="server" CssClass="DropDownList" Width="70%" AutoPostBack="True" />
                                </td>
                            </tr>
                            <tr id="trReportParam" runat="server" visible="false">
                                <td class="labelcell"></td>
                                <td class="contentcell" style="border: 1px solid #ebebeb;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="labelcell">AdHoc Report Parameter(s)</td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Panel ID="pnlRHS" runat="server" Width="100%" Visible="False">
                                        <div style="text-align:center; vertical-align:top">
                                            <h3><asp:Label ID="lblChooseFilter" runat="server" CssClass="LabelBold" Visible="True" Text="Choose Filter(s)" /></h3>
                                        </div>
                                        <div style="text-align:center">
                                            <asp:Label ID="lblNoFilters" runat="server" CssClass="LabelBold" Visible="False" Text="There are no filters for this report" />
                                        </div>
                                        <div id="divMain" runat="server" style="height: expression(document.body.clientHeight - 325 + 'px'); text-align: left; padding-left: 5px">					
                                            <asp:Wizard ID="ReportWizard" runat="server" Width="100%" ActiveStepIndex="0" DisplaySideBar="False" DisplayCancelButton="False" Height="90%" BorderWidth="0" BackColor="White">
                                                <StepStyle BackColor="White" ForeColor="Black" />
                                                <NavigationStyle Height="0px" Width="0px" />
                                                <NavigationButtonStyle Height="0px" Width="0px" />
                                                <SideBarButtonStyle CssClass="LabelBold" />
                                                <SideBarStyle VerticalAlign="Middle" Width="150px" />
                                                <HeaderStyle CssClass="ListFrametop" BorderColor="#E6E2D8" BorderStyle="Solid" BorderWidth="2px" HorizontalAlign="Center" />                    
                                                <WizardSteps>
                                                <asp:TemplatedWizardStep ID="Step1" runat="server" StepType="Start" Title="Filters">
                                                <ContentTemplate>
                                                <div style="vertical-align:top; height:90%">
                                                <!-- Repeater with the list of report parameters -->
                                                <asp:Repeater ID="rptFilters" runat="server">
                                                <HeaderTemplate>
                                                    <div style="height: 150px; overflow-y:auto">
                                                    <table width="95%" cellpadding="0" cellspacing="0" align="center">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr valign="top">
                                                        <td nowrap style="border: 1px solid #ebebeb;" valign="top">
                                                            <asp:HiddenField ID="hfParamId" runat="server" Value='<%# Container.DataItem("ParamId")%>' />
                                                            <asp:HiddenField ID="hfTblFldId" runat="server" Value='<%# Container.DataItem("TblFldsId")%>' />
                                                            <asp:HiddenField ID="hfTblName" runat="server" Value='<%# Container.DataItem("TableName")%>' />
                                                            <asp:HiddenField ID="hfFldName" runat="server" Value='<%# Container.DataItem("FieldName")%>' />
                                                            <asp:HiddenField ID="hfDDLid" runat="server" Value='<%# Container.DataItem("DDLId")%>' />
                                                            <asp:HiddenField ID="hfFldType" runat="server" Value='<%# Container.DataItem("FldTypeId")%>' />
                                                            <asp:Label ID="lblFldName" runat="server" Text='<%# Container.DataItem("Header")%>' CssClass="EasyLabel" />
                                                            <asp:Label ID="lblReq" runat="server" Text="*" Visible='<%# CType(Container.DataItem("IsRequired"),Boolean) %>' CssClass="EasyLabel" ForeColor="Red" />
                                                        </td>
                                                        <td  style="border: 1px solid #ebebeb; background-color:#fafafa"> 
                                                            <!-- Displays filter/values that come from the database -->
                                                            <asp:Label ID="lblDiv1" runat="server" Visible="False" Text="<div style='overflow-y:auto; height: 100px'>" />
                                                            <asp:DataList ID="dlValues" runat="server" RepeatDirection="Vertical" RepeatLayout="Table" RepeatColumns="2" Width="100%" DataKeyField="ValueId" >
                                                            <SelectedItemStyle CssClass="SelectedItemStyle" />
                                                            <ItemStyle CssClass="ItemStyle" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:Label id="lblId" runat="server" Visible="False" Width="0px" Text='<%# Container.DataItem("ValueId")%>' />
                                                                <asp:CheckBox ID="cbValue" runat="server" CssClass="CheckBoxStyle" Text='<%# Container.DataItem("Descrip")%>' />
                                                            </ItemTemplate>
                                                            </asp:DataList>                                        
                                                            <asp:Label ID="lblDiv2" runat="server" Visible="False" Text="</div>" />
                                                            <!-- End: Displays filter/values that come from the database -->
                                                            <!-- Calendar filter -->
                                                            <table>
                                                            <tr>
                                                            <td valign="middle" align="center">
                                                                <asp:Label ID="lbFilterEqual" runat="server" Visible="False" Text="Exact value" CssClass="EasyLabel" />
                                                                <asp:TextBox ID="txtFilterEqual" runat="server" Visible="False" CssClass="TextBox" Width="80px" />
                                                                <img align="middle" src="../images/TM/PopUpCalendar.gif" id="ibEqualDate" runat="server" Visible="False" />                                            
                                                            </td>
                                                            <td valign="middle" align="center">
                                                                <asp:Label ID="lbFilterMin" runat="server" Visible="False" Text="Min" CssClass="EasyLabel" />
                                                                <asp:TextBox ID="txtFilterMin" runat="server" Visible="False" CssClass="TextBox" Width="80px" />
                                                                <img align="middle" src="../images/TM/PopUpCalendar.gif" id="ibMinDate" runat="server" Visible="False" />                                            
                                                            </td>
                                                            <td valign="middle" align="center">
                                                                <asp:Label ID="lbFilterMax" runat="server" Visible="False" Text="Max" CssClass="EasyLabel" />
                                                                <asp:TextBox ID="txtFilterMax" runat="server" Visible="False" CssClass="TextBox" Width="80px" />
                                                                <img align="middle" src="../images/TM/PopUpCalendar.gif" id="ibMaxDate" runat="server" Visible="False" />
                                                            </td>
                                                            </tr>
                                                            </table>
                                                            <!-- End: Calendar filter -->
                                                            <!-- Regular user entered via textbox filter -->
                                                            <table>
                                                            <tr valign="middle">
                                                            <td valign="middle" align="center">
                                                                <asp:DropDownList ID="ddlRegFilterOp" runat="server" CssClass="dropdownlist" Width="135px" Visible="False" />
                                                                <asp:TextBox ID="txtRegFilter" runat="server" Visible="False" CssClass="TextBox" Width="135px" />
                                                            </td>
                                                            </tr>
                                                            </table>
                                                            <!-- End: Calendar filter -->
                                                        </td>
                                                        </tr>
                                                        <tr height="10px"><td></td></tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                </table>
                                                </div>
                                                </FooterTemplate>
                                                </asp:Repeater>
                                                <!-- End: Repeater with the list of report parameters -->
                                                </div>
                                                </ContentTemplate>
                                                </asp:TemplatedWizardStep>
                                                </WizardSteps>                        
                                                </asp:Wizard>
                                                </div>
                                                </asp:Panel>    
                                </td>
                                </tr>
                                <tr>
                                <td class="contentcell" style="text-align:right;"><asp:Button ID="btnMergeData" runat="server" Text="Merge Data" Width="100px"  /></td>
                                </tr>
                                </table>   
                                </td>
                            </tr>
                            <tr id="tr1" runat="server" visible="true">
                                <td class="labelcell">Recipient Lookup<font style="color: Red">*</font></td>
                                <td class="contentcell">
                                    <asp:DropDownList id="EntityList" AutoPostBack="True" runat="server" Width="85px" CssClass="DropDownList">
                                          <asp:ListItem Selected="True" Value="394"> Student </asp:ListItem>
                                          <asp:ListItem Value="395"> Lead </asp:ListItem>
                                          <asp:ListItem Value="397"> Employer </asp:ListItem>
                                          <asp:ListItem Value="396">  Employee</asp:ListItem>
                                          <asp:ListItem Value="434"> Lender </asp:ListItem>
                                       </asp:DropDownList>  
                                    <asp:TextBox ID="txtSearch" runat="server" Width="227px" CssClass="textboxreq" />&nbsp;&nbsp;
                                    <asp:ImageButton runat="server" ID="lookupEntity" OnClick="lookupEntity_OnClick" ImageUrl="../Images/MSG/RecipLookup.gif"/>
                                    
                                </td>
                            </tr>
                            <tr id="tr2" runat="server" visible="false" height="150px">
                                <td>&nbsp;</td>
                                <td class="contentcell" >
                                     <telerik:RadGrid ID="rgEntities" runat="server" Width="90%" AllowSorting="false" style="min-height:35px;" 
                                            AutoGenerateColumns="False" 
                                            AllowFilteringByColumn="false" GroupingSettings-CaseSensitive="false" AllowPaging="false" OnItemCommand="rgEntities_ItemCommand"
                                             >
                                          <ClientSettings>
                                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true" ScrollHeight="150px" ></Scrolling>
                                              <Selecting AllowRowSelect="True"></Selecting>
                                        </ClientSettings>
                                            
                                            <MasterTableView Width="100%"  AllowMultiColumnSorting="false" DataKeyNames="RecipientId, Name" >
                                                <Columns>
                                                    <telerik:GridButtonColumn Text="Select" CommandName="Select" >
                                                       <HeaderStyle Width="50px"></HeaderStyle> 
                                                        
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="StatusCodeDescrip"
                                                        HeaderText="RecipientId" DataField="RecipientId" display="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="Details" HeaderText="Name"
                                                         DataField="Name">
                                                    </telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                </td>
                            </tr>
                            <tr id="trRecipient" runat="server" visible="true">
                                <td class="labelcell">
                                    Recipient<font style="color: Red">*</font></td>
                                <td class="contentcell">
                                    <asp:TextBox ID="txtRecipient" runat="server" Width="316px" CssClass="textboxreq" ReadOnly="true" BackColor="#E7F3F8" />&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr id="tr3" runat="server" visible="true">
                                <td class="labelcell">Regarding Lookup</td>
                                <td class="contentcell">
                                    <asp:DropDownList id="RegardingList" AutoPostBack="True" runat="server" Width="85px" CssClass="DropDownList">
                                          <asp:ListItem Selected="True" Value="394"> Student </asp:ListItem>
                                          <asp:ListItem Value="395"> Lead </asp:ListItem>
                                          <asp:ListItem Value="397"> Employer </asp:ListItem>
                                          <asp:ListItem Value="396">  Employee</asp:ListItem>
                                          <asp:ListItem Value="434"> Lender </asp:ListItem>
                                       </asp:DropDownList>  
                                    <asp:TextBox ID="txtReSearch" runat="server" Width="227px" CssClass="textboxreq" />&nbsp;&nbsp;
                                    <asp:ImageButton runat="server" ID="reSearch" OnClick="lookupReEntity_OnClick" ImageUrl="../Images/MSG/RecipLookup.gif"/>
                                    
                                </td>
                            </tr>
                            <tr id="tr4" runat="server" visible="false" height="150px">
                                <td>&nbsp;</td>
                                <td class="contentcell" >
                                     <telerik:RadGrid ID="rgReEntities" runat="server" Width="90%" AllowSorting="false" style="min-height:35px;" 
                                            AutoGenerateColumns="False" 
                                            AllowFilteringByColumn="false" GroupingSettings-CaseSensitive="false" AllowPaging="false" OnItemCommand="rgReEntities_ItemCommand"
                                             >
                                          <ClientSettings>
                                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true" ScrollHeight="150px" ></Scrolling>
                                              <Selecting AllowRowSelect="True"></Selecting>
                                        </ClientSettings>
                                            
                                            <MasterTableView Width="100%"  AllowMultiColumnSorting="false" DataKeyNames="RecipientId, Name" >
                                                <Columns>
                                                    <telerik:GridButtonColumn Text="Select" CommandName="Select" >
                                                       <HeaderStyle Width="50px"></HeaderStyle> 
                                                        
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="StatusCodeDescrip"
                                                        HeaderText="RecipientId" DataField="RecipientId" display="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="Details" HeaderText="Name"
                                                         DataField="Name">
                                                    </telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                </td>
                            </tr>


                            <tr>
                                <td class="labelcell">
                                    Regarding</td>
                                <td class="contentcell">
                                    <asp:TextBox ID="txtRe" runat="server" Width="316px" CssClass="textbox" ReadOnly="true" BackColor="#E7F3F8" />&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="labelcell">
                                    Delivery<font style="color: Red">*</font></td>
                                <td class="contentcell">
                                    <asp:DropDownList ID="ddlDelivery" runat="server" Width="70%" CssClass="DropDownListReq" />&nbsp;<asp:Button ID="btnSendNow" runat="server" Text="Send" Width="100px"  />
                                </td>
                            </tr>
                        </table>
                        </td></tr> </table>
            <!-- end message options -->
            <table summary="Message Options Table" width="100%" class="tableborder" cellpadding="0"
                cellspacing="0"><tr><td valign="top">

                        <!--This style keeps reverting!-->
                        <!--<div style="HEIGHT: expression(document.body.clientHeight - 160 + 'px')">-->
                        <div>
<telerik:RadEditor runat="server" ID="fck" ToolsFile="~/MSG/xml/RadEditorToolsFile.xml" Width="100%" AutoResizeHeight="true">
    <Modules>
        <telerik:EditorModule Name="RadEditorHtmlInspector" Enabled="true" Visible="false" />
        <telerik:EditorModule Name="RadEditorNodeInspector" Enabled="true" Visible="false" />
        <telerik:EditorModule Name="RadEditorDomInspector" Enabled="false" />
        <telerik:EditorModule Name="RadEditorStatistics" Enabled="false" />
    </Modules>
    <ImageManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <MediaManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <FlashManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <TemplateManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <DocumentManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
</telerik:RadEditor>
 <script type="text/javascript">
     Telerik.Web.UI.Editor.CommandList["FameTags"] = function (commandName, editor, args) {
         var elem = editor.getSelectedElement(); //returns the selected element.

         if (elem.tagName == "A") {
             editor.selectElement(elem);
             argument = elem;
         }
         else {
             //remove links if present from the current selection - because of JS error thrown in IE
             editor.fire("Unlink");

             //remove Unlink command from the undo/redo list
             var commandsManager = editor.get_commandsManager();
             var commandIndex = commandsManager.getCommandsToUndo().length - 1;
             commandsManager.removeCommandAt(commandIndex);

             var content = editor.getSelectionHtml();

             var link = editor.get_document().createElement("A");

             link.innerHTML = content;
             argument = link;
         }

         var myCallbackFunction = function (sender, args) {
             editor.pasteHtml(String.format("{0}", args.innerHTML))
         }

         editor.showExternalDialog(
              'SpecialFieldsPopupRadEditor.aspx?categoryName=' + args.get_value().replace('&amp;', '[amp]'),
              argument,
              370,
              300,
              myCallbackFunction,
              null,
              'Insert [Field]',
              false,
              Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
              false,
              true);
     };
    </script>


                        </div>
                   </td></tr>


            </table>

            <!-- Link Buttons and other hidden controls -->
            <!-- end Link Buttons and other hidden controls -->
           
        </div>
        <asp:HiddenField ID="hfRecipientId" runat="server" />
        <asp:HiddenField ID="hfRecipientType" runat="server" />
        <asp:HiddenField ID="hfReId" runat="server" />
        <asp:HiddenField ID="hfReType" runat="server" />
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </form>
</body>
</html>
