'
' FAME
' Copyright (c) 2005
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdvMessaging
' Description: Display messages that are either pending or already sent.  User
'   can print/email messages that are pending.  User can also apply a filter.

Imports System.Text
Imports FAME.AdvantageV1.BusinessFacade.MSG
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common

Partial Class AdvantageApp_MSG_Queue
    Inherits System.Web.UI.Page
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private _CurrentPage As Integer

    Private Sub Page_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = "SY"
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId

        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        MSGCommon.AdvInit(Me)
        If Not Page.IsPostBack Then
            CommonWebUtilities.SetupRadEditor(fck, "AdvMessaging_MsgCompose")  ', AdvantageSession.UserState.CampusId.ToString
            ' MSGCommon.SetupEditor(fck, "AdvMessaging_ProcessMsgs")
            fck.Content = ""
            fck.Visible = False ' editor should be hidden initialy


            ' Get and save the paramters
            Session("rtype") = Request.Params("rtype")
            Session("rid") = Request.Params("rid")
            Session("rtxt") = Request.Params("rtxt")
            Session("dtype") = Request.Params("dtype")
            Session("mgid") = Request.Params("mgid")
            Session("tid") = Request.Params("tid")
            Session("errors") = Request.Params("errors")
            Session("source") = Request.Params("source")
            Session("messageid") = Request.Params("messageid")
            If Session("source") Is Nothing Or Session("source") = "" Then
                Session("source") = "0"
            End If

            ' Set the title bar to either Sent Items or Outbox
            If Session("source") <> "0" Then
                Me.Title = "Sent Items"
            Else
                Me.Title = "Outbox"
            End If

            ' display the messages
            BindMessages()

            ' check the params for printall or processall requests
            If Request.Params("printall") = "1" Then
                Me.PrintAll()
            ElseIf Request.Params("processall") = "1" Then
                Me.ProcessAll()
            End If
        Else
            ' display the messages
            'BindMessages()
            fck.Content = ""
            fck.Visible = False
        End If
    End Sub


    ''' <summary>
    ''' Fills the repeater with a list of messages.
    ''' Messages can be filtered.  The filters are stored in Session object.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindMessages()
        Try
            ' get the filters
            Dim strFilterSource = CType(Session("source"), String)
            Dim filterRecipientType = CType(Session("rtype"), String)
            Dim filterRecipientId = CType(Session("rid"), String)
            Dim filterDeliveryType = CType(Session("dtype"), String)
            Dim filterMessageGroupId = CType(Session("mgid"), String)
            Dim filterTemplateId = CType(Session("tid"), String)
            Dim filterErrors = CType(Session("errors"), String)
            Dim filterShowOnlyErrors = False
            Dim filterMessageId = CType(Session("messageid"), String)


            Dim ds As Data.DataSet
            ' Populate the repeater control with the Items DataSet
            Dim dsPaged As New PagedDataSource
            dsPaged.AllowPaging = True
            dsPaged.PageSize = 10

            ' check which source we want (0=Outbox, 1=Sent Items)
            If strFilterSource = "0" Then
                ds = MessagingFacade.GetAllMessages_OutBox(filterRecipientType,
                                                                            filterRecipientId,
                                                                            filterDeliveryType,
                                                                            filterMessageGroupId,
                                                                            filterTemplateId,
                                                                            filterShowOnlyErrors,
                                                                            filterMessageID)
            Else
                ds = MessagingFacade.GetAllMessages_Sent(filterRecipientType,
                                                                          filterRecipientId,
                                                                          filterDeliveryType,
                                                                          filterMessageGroupId,
                                                                          filterTemplateId,
                                                                          filterShowOnlyErrors)
            End If

            dsPaged.DataSource = ds.Tables(0).DefaultView
            dsPaged.CurrentPageIndex = CurrentPage

            'Disable Previous or Next buttons if necessary
            lnkPrev.Enabled = Not dsPaged.IsFirstPage
            lnkNext.Enabled = Not dsPaged.IsLastPage

            rptQueue.DataSource = dsPaged
            rptQueue.DataBind()
            ds.Dispose()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Called for each item in the Repeater.  Gives us a chance to install javascript handlers.
    ''' Handlers are added for Info, Deliver, and Delete
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub rptQueue_OnItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptQueue.ItemDataBound
        Try
            'Dim ib = CType(e.Item.FindControl("ibView"), ImageButton)
            'If Not ib Is Nothing Then
            'End If

            ' Put the javascript for Info
            Dim ib = CType(e.Item.FindControl("imInfo"), ImageButton)
            If Not ib Is Nothing Then
                ' change the image if there is an error
                Dim lastMsg = String.Empty
                If TypeOf e.Item.DataItem("LastDeliveryMsg") Is DBNull Then
                    lastMsg = String.Empty
                Else
                    lastMsg = CType(e.Item.DataItem("LastDeliveryMsg"), String)
                End If

                'Try

                lastMsg = lastMsg.Replace(vbCrLf.ToString(), " ")
                '    Catch ex As System.Exception
                 '    	Dim exTracker = new AdvApplicationInsightsInitializer()
                '    	exTracker.TrackExceptionWrapper(ex)

                'End Try
                If lastMsg <> "" AndAlso lastMsg <> "Ok" Then
                    ib.ImageUrl = "../Images/MSG/Queue_error.gif"
                End If

                ' build of a javascript string to display a popup
                Dim js As New StringBuilder
                js.Append("ddrivetip('")
                If e.Item.DataItem("ReName").ToString() <> "" Then
                    js.Append("<li>Re: " & e.Item.DataItem("ReName") & "</li>")
                End If
                js.Append("<li>To: " & e.Item.DataItem("MailTo") & "</li>")
                js.Append("<li>From: " & e.Item.DataItem("MailFrom") & "</li>")
                ' Do specific info based on if this is the outbox or sent items
                If Session("source") <> 1 Then
                    js.Append("<li>Delivery date: " & e.Item.DataItem("DeliveryDate") & "</li>")
                    If e.Item.DataItem("LastDeliveryAttempt").ToString() <> "" Then
                        js.Append("<li>Last delivery attempt: " & e.Item.DataItem("LastDeliveryAttempt") & "</li>")
                    End If
                Else
                    js.Append("<li>Date delivered: " & e.Item.DataItem("DateDelivered") & "</li>")
                End If
                ' Add the last error message
                If lastMsg <> "" Then
                    js.Append("<li>Last error msg: " & lastMsg.Replace("'", "") & "</li>")
                End If
                js.Append("', 300);")

                ib.Attributes.Add("onmouseover", js.ToString)
                ib.Attributes.Add("onmouseout", "hideddrivetip()")
            End If

            ' Javascript for Deliver
            ib = e.Item.FindControl("ibDeliver")
            If Not ib Is Nothing Then
                If Session("source") <> 1 Then
                    ib.Visible = True
                    ib.Attributes.Add("onclick", "document.getElementById('txtConfirm').value=window.confirm('Deliver now?')")
                Else
                    ib.Visible = False ' hide if we are in the Sent Items view
                End If
            End If

            ' Javascript for Delete
            ib = e.Item.FindControl("ibDelete")
            If Not ib Is Nothing Then
                If Session("source") <> 1 Then
                    ib.Visible = True
                    ib.Attributes.Add("onclick", "document.getElementById('txtConfirm').value=window.confirm('Permanantly delete this message?')")
                Else
                    ib.Visible = False ' hide if we are in the Sent Items view
                End If

            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Various button handlers within the Message Queue repeater control
    ''' Handle, "View", "Deliver" and "Delete"
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rptQueue_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptQueue.ItemCommand
        Try
            Dim messageId As String = e.CommandArgument.ToString
            Select Case (e.CommandName)
                Case "View"
                    If Session("source") <> 1 Then
                       fck.Enabled = True
                    Else
                      fck.Enabled = False
                    End If

                    fck.ToolbarMode = Telerik.Web.UI.EditorToolbarMode.ShowOnFocus
                    Dim msgInfo As FAME.AdvantageV1.Common.MSG.MessageInfo = MessagingFacade.GetMessageInfo(messageId)
                    fck.Visible = True
                    fck.Content = msgInfo.MsgContent

                    ' handle the case that the user want to deliver a single message
                Case "Deliver"
                    ' check the result of the popup before continuing
                    If txtConfirm.Value <> "true" Then Return
                    Dim strRes As String = MessagingFacade.DeliverMessage(Page, messageId, MSGCommon.GetCurrentUserId())
                    If strRes = "" Then
                        'MSGCommon.Alert(Me, "Message has been delivered.")                        
                    Else
                        MSGCommon.Alert(Me, "Message could not be delivered")
                        MSGCommon.SetBrowserStatusBar(Me, strRes)
                    End If
                    BindMessages()

                    ' Handle the case when the user clicks the delete button
                Case "Delete"
                    Try
                        ' check the result of the popup before continuing
                        If txtConfirm.Value <> "true" Then Return
                        If MessagingFacade.DeleteMessage(MessageId) Then
                            MSGCommon.SetBrowserStatusBar(Page, "Message has been deleted.")
                            BindMessages()
                        Else
                            MSGCommon.SetBrowserStatusBar(Page, "Message could not be deleted.")
                        End If
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        MSGCommon.DisplayErrorMessage(Me, ex.Message)
                    End Try

            End Select
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Print all messages that are in the current filter.
    ''' This is done by showing a popup (Print.aspx).  The current list of MessageIds are stored
    ''' in cached under "curMsgGUIDS" which is then referenced within Print.aspx.    
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PrintAll()
        ' Iterate through the repeater and get the MessageId of all messages contained within it.
        ' These messageIds are stored in an ArrayList which will then be put into session.
        Dim item As RepeaterItem
        Dim oArray = New ArrayList
        For Each item In rptQueue.Items
            Dim lb = CType(item.FindControl("ibView"), ImageButton)
            If Not lb Is Nothing Then
                oArray.Add(lb.CommandArgument)
            End If
        Next

        ' store all the messageIds into session
        Session("curMsgGUIDS") = oArray
        ' Display the popup
        Dim popupScript As String = "<script language='javascript'>" & _
                  "window.showModalDialog('Print.aspx?print=1',null,'resizable:yes;status:no;dialogWidth:375px;dialogHeight:175px;dialogHide:true;help:no;scroll:yes');" & _
                  "</script>"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", popupScript)
    End Sub

    ''' <summary>
    ''' Process all messages that are in the current filter
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ProcessAll()
        ' Iterate through the repeater and get the MessageId of all messages contained within it.
        ' These messageIds are stored in an ArrayList which will then be put into session.
        Dim item As RepeaterItem
        Dim oArray = New ArrayList
        For Each item In rptQueue.Items
            Dim lb As ImageButton = item.FindControl("ibView")
            If Not lb Is Nothing Then
                oArray.Add(lb.CommandArgument)
            End If
        Next

        ' store all the messageIds into session
        Session("curMsgGUIDS") = oArray
        ' Display the popup
        Dim popupScript As String = "<script language='javascript'>" & _
                  "window.showModalDialog('Print.aspx?print=0',null,'resizable:yes;status:no;dialogWidth:375px;dialogHeight:175px;dialogHide:true;help:no;scroll:yes');" & _
                  "</script>"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", popupScript)
        BindMessages()
    End Sub

#Region " Paging support "
    Public Property CurrentPage As Integer
        Get
            'look for current page in ViewState
            _CurrentPage = CInt(ViewState("CurrentPage"))
            Return _CurrentPage
        End Get
        Set(value As Integer)
            ViewState("CurrentPage") = Value
        End Set
    End Property

    Sub lnkNext_Click(sender As System.Object, e As System.EventArgs) Handles lnkNext.Click
        ' Set viewstate variable to the previous page
        CurrentPage += 1

        ' Reload control
        Me.BindMessages()
    End Sub
    Private Sub lnkPrev_Click(sender As Object, e As System.EventArgs) Handles lnkPrev.Click
        ' Set viewstate variable to the previous page
        CurrentPage -= 1

        ' Reload control
        Me.BindMessages()

    End Sub
#End Region

End Class

