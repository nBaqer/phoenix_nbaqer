'
' FAME
' Copyright (c) 2005
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdvMessaging
Imports Fame.AdvantageV1.BusinessFacade.MSG
Imports Fame.AdvantageV1.Common.MSG
Imports Fame.AdvantageV1.Common.Reports
Imports Fame.AdvantageV1.BusinessFacade.Reports
Imports System.Data
Imports Advantage.Business.Objects
Imports Fame.AdvantageV1.BusinessFacade
Imports Fame.AdvantageV1.Common
Imports Telerik.Web.UI


Partial Class AdvantageApp_MSG_Compose
    Inherits Page
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Dim advantageUserState As User = AdvantageSession.UserState
            ResourceId = HttpContext.Current.Request.Params("resid")
            campusId = AdvantageSession.UserState.CampusId.ToString
            userId = AdvantageSession.UserState.UserId.ToString
            ModuleId = "SY"
            Dim mContext As HttpContext
            mContext = HttpContext.Current
            txtResourceId.Text = ResourceId

            Session("UserId") = userId
            Session("UserName") = AdvantageSession.UserState.UserName.ToString
            Session("cmpid") = campusId
            Session("resid") = txtResourceId.Text.Trim
            Try
                mContext.Items("Language") = "En-US"
                mContext.Items("ResourceId") = ResourceId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Dim err As String = ex.Message
            End Try
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
            MSGCommon.AdvInit(Me)

            If Not Page.IsPostBack Then
                ' add javascript to Deliver the message
                btnSendNow.Attributes.Add("onclick", "window.confirm('Deliver message?');")

                ' add javascript for the contact lookup handlers                
                Dim js As String = MSGCommon.GetRecipientLookupJS("txtRecipient", "hfRecipientType", "hfRecipientId", False)


                js = MSGCommon.GetRecipientLookupJS("txtRe", "hfReType", "hfReId", True)


                ResetForm()

                SetRecipientFromSession()

            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim err As String = ex.Message

        End Try
    End Sub

    ''' <summary>
    ''' if we are coming from a link in the info bar, the recipient information will
    ''' be in the session. Check if the addnew query string parameter is present.
    ''' If so, populate the various controls from the session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetRecipientFromSession()

        If Request.QueryString("entityId") IsNot Nothing Then
            'Config the module using the entity Id, if not use the session
            Dim entityId As String = Request.QueryString("entityId")
            Dim modulo = CType(Request.QueryString("mod"), Integer)
            If (modulo = 4) Then
                'Only four (Lead entity) is configured for now
                'if you need other entity, please service your self! :-)
                Dim val = CInt(System.Enum.Parse(GetType(AdvantageEntityId), AdvantageEntityId.Lead.ToString()))
                Dim admission = CInt(System.Enum.Parse(GetType(AdvantageSystemModuleId), AdvantageSystemModuleId.Admissions.ToString()))
                Dim db = New LeadFacade()
                Dim info = db.GetLeadDataForHeaderInfo(entityId)
                ddlModules.SelectedValue = admission.ToString()
                hfRecipientType.Value = val.ToString()
                hfRecipientId.Value = entityId
                txtRecipient.Text = info.Name

                EntityList.SelectedValue = val.ToString()
                RegardingList.SelectedValue = val.ToString()

                Return
            End If
        End If

        If Request.QueryString("addnew") IsNot Nothing Then
            ddlModules.SelectedValue = AdvantageSession.TaskRecipientModule
            hfRecipientType.Value = AdvantageSession.TaskRecipientTypeId
            hfRecipientId.Value = AdvantageSession.TaskRecipientId
            txtRecipient.Text = AdvantageSession.TaskRecipientFullName

            EntityList.SelectedValue = AdvantageSession.TaskRecipientTypeId
            RegardingList.SelectedValue = AdvantageSession.TaskRecipientTypeId

        End If


    End Sub

    ''' <summary>
    ''' Sets the forms back to its intial state
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ResetForm()

        'MSGCommon.SetupEditor(fck, "AdvMessaging_MsgCompose")
        MSGCommon.BuildModulesDDL(MSGCommon.GetCurrentUserId(), ddlModules)


        MSGCommon.BuildDeliveryTypeDDL(ddlDelivery)
        MSGCommon.BuildAvailableTemplates(MSGCommon.GetCurrentUserId(), MSGCommon.GetCampusID(), Nothing, Nothing, Me.ddlTemplate)
        'New Code Added By Vijay Ramteke On May 05, 2010
        MSGCommon.BuildAdHocDDL(MSGCommon.GetCurrentUserId(), "0", MSGCommon.GetCampusID(), ddlAdHocReport)
        lblChooseFilter.Visible = False
        lblNoFilters.Visible = False
        trReportParam.Visible = False
        trRecipient.Visible = True
        Dim rpt As Repeater = Step1.ContentTemplateContainer.FindControl("rptFilters")
        rpt.DataSource = Nothing
        rpt.DataBind()
        Session("dsAdHoc") = Nothing
        'New Code Added By Vijay Ramteke On May 05, 2010
        hfRecipientType.Value = ""
        hfRecipientId.Value = ""
        txtRecipient.Text = "" ' ?
        hfReType.Value = ""
        hfReId.Value = ""

        If ddlTemplate.SelectedIndex <> 0 Then
            CommonWebUtilities.SetupRadEditor(fck, "AdvMessaging_MsgCompose", TemplatesFacade.GetTemplateInfo(ddlTemplate.SelectedValue).EntityId)
        End If
        fck.Content = ""


    End Sub



    ''' <summary>
    ''' Checks the form's contents for validity.  If any control is
    ''' not valid, an alert is made and the function returns false.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As Boolean
        If ddlTemplate.SelectedIndex = 0 Then
            MSGCommon.Alert(Me, "Template is required")
            Return False
        End If
        If ddlAdHocReport.SelectedValue.ToString = "0" Then
            If txtRecipient.Text = "" Then
                MSGCommon.Alert(Page, "You must select a recipient.")
                Return False
            End If
        End If
        If ddlDelivery.SelectedValue = "" Then
            MSGCommon.Alert(Page, "You must select a delivery type.")
            Return False
        End If
        Return True
    End Function




    Public Sub lookupEntity_OnClick(ByVal sender As Object, ByVal e As EventArgs) Handles lookupEntity.Click

        If String.IsNullOrEmpty(txtSearch.Text) Then
            MSGCommon.Alert(Page, "No search criteria entered.")
        Else
            Dim ds As DataSet = ContactSearchFacade.RecipientSearch(EntityList.SelectedValue, txtSearch.Text, MSGCommon.GetCampusID())
            rgEntities.DataSource = ds
            rgEntities.DataBind()
            tr2.Visible = True
        End If




    End Sub

    Public Sub lookupReEntity_OnClick(ByVal sender As Object, ByVal e As EventArgs) Handles reSearch.Click

        If String.IsNullOrEmpty(txtSearch.Text) Then
            MSGCommon.Alert(Page, "No search criteria entered.")
        Else
            Dim ds As DataSet = ContactSearchFacade.RecipientSearch(RegardingList.SelectedValue, txtReSearch.Text, MSGCommon.GetCampusID())
            rgReEntities.DataSource = ds
            rgReEntities.DataBind()
            tr4.Visible = True
        End If




    End Sub

    Public Sub rgEntities_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rgEntities.ItemCommand
        'need to put there here. The navigation paging controls will generate an ItemCommand, 
        'we don't want any trouble
        If e.CommandName = "Select" And TypeOf e.Item Is GridDataItem Then

            'requires that the referenced field be in the DataKeyValues in the grid
            Dim item As GridDataItem = (DirectCast(e.Item, GridDataItem))

            Dim recId As String = item.GetDataKeyValue("RecipientId").ToString()
            Dim name As String = item.GetDataKeyValue("Name").ToString()
            txtRecipient.Text = name

            AdvantageSession.TaskRecipientId = recId
            AdvantageSession.TaskRecipientTypeId = EntityList.SelectedValue
            AdvantageSession.TaskRecipientFullName = txtRecipient.Text


            tr2.Visible = False


        End If


    End Sub

    Public Sub rgReEntities_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rgReEntities.ItemCommand
        'need to put there here. The navigation paging controls will generate an ItemCommand, 
        'we don't want any trouble
        If e.CommandName = "Select" And TypeOf e.Item Is GridDataItem Then

            'requires that the referenced field be in the DataKeyValues in the grid
            Dim item As GridDataItem = (DirectCast(e.Item, GridDataItem))

            Dim recId As String = item.GetDataKeyValue("RecipientId").ToString()
            Dim name As String = item.GetDataKeyValue("Name").ToString()
            txtRe.Text = name

            ViewState("ReId") = recId


            tr4.Visible = False


        End If


    End Sub


    ''' <summary>
    ''' Handler for the Deliver Now button.  An attempt to deliver
    ''' the message happens in this method.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSendNow_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSendNow.Click
        If Not ValidateForm() Then Return

        ' Build the MessageInfo object that will get added to syMessages (the Queue)
        Dim msgInfo As New Fame.AdvantageV1.Common.MSG.MessageInfo

        Dim res As String = String.Empty
        Dim resFinal As String = String.Empty
        Dim i As Integer = 0
        'Dim MessageIds As String = ""
        Dim messageIdsDelete As String = ""
        If ddlAdHocReport.SelectedValue.ToString = "0" Then
            msgInfo = New Fame.AdvantageV1.Common.MSG.MessageInfo
            msgInfo.IsInDB = False
            msgInfo.TemplateId = ddlTemplate.SelectedValue
            msgInfo.FromId = MSGCommon.GetCurrentUserId()
            msgInfo.DeliveryType = ddlDelivery.SelectedValue
            If AdvantageSession.TaskRecipientTypeId IsNot Nothing Then
                msgInfo.RecipientType = AdvantageSession.TaskRecipientTypeId
            Else
                msgInfo.RecipientType = hfRecipientType.Value
            End If
            If AdvantageSession.TaskRecipientId IsNot Nothing Then
                msgInfo.RecipientId = AdvantageSession.TaskRecipientId
            Else
                msgInfo.RecipientId = hfRecipientId.Value
            End If

            If ViewState("ReId") IsNot Nothing Then
                msgInfo.ReType = RegardingList.SelectedValue
                msgInfo.ReId = ViewState("ReId").ToString()
            End If

            msgInfo.DeliveryDate = Date.Now
            msgInfo.GroupId = TemplatesFacade.GetTemplateInfo(ddlTemplate.SelectedValue).GroupId
            msgInfo.MsgContent = fck.Content 'Server.HtmlDecode(
            ' add the message to the database and try to deliver it
            If Not Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.AddMessage(msgInfo, MSGCommon.GetCurrentUserId()) Then
                MSGCommon.Alert(Page, "Message could not be sent.")
                Return
            End If
            res = Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.DeliverMessage(Page, msgInfo.MessageId, MSGCommon.GetCurrentUserId())
            res = res.Replace(vbCrLf.ToString(), " ") ' cr/lf causes problems in the html, just remove it
        Else
            Dim dsAdHoc As DataSet
            dsAdHoc = CType(Session("dsAdHoc"), DataSet)
            Dim objInfos(dsAdHoc.Tables(0).Rows.Count - 1) As Object
            For Each drAH As DataRow In dsAdHoc.Tables(0).Rows
                If Not drAH("RecipientId").ToString = Guid.Empty.ToString Then
                    msgInfo = New Fame.AdvantageV1.Common.MSG.MessageInfo
                    msgInfo.IsInDB = False
                    msgInfo.TemplateId = ddlTemplate.SelectedValue
                    msgInfo.FromId = MSGCommon.GetCurrentUserId()
                    msgInfo.DeliveryType = ddlDelivery.SelectedValue
                    msgInfo.RecipientType = hfRecipientType.Value
                    msgInfo.RecipientId = drAH("RecipientId").ToString
                    msgInfo.ReType = hfReType.Value
                    msgInfo.ReId = Guid.Empty.ToString
                    msgInfo.DeliveryDate = Date.Now
                    Dim info As TemplateInfo = TemplatesFacade.GetTemplateInfo(ddlTemplate.SelectedValue)
                    msgInfo.GroupId = info.GroupId
                    Dim params As SpecialFields_Params

                    If AdvantageSession.TaskRecipientTypeId IsNot Nothing Then
                        params = New SpecialFields_Params(MSGCommon.GetCampusID, AdvantageSession.TaskRecipientTypeId, AdvantageSession.TaskRecipientId, hfReType.Value, hfReId.Value, "")
                    Else
                        params = New SpecialFields_Params(MSGCommon.GetCampusID, hfRecipientType.Value, hfRecipientId.Value, hfReType.Value, hfReId.Value, "")
                    End If

                    msgInfo.MsgContent = SpecialFields.SubSpecialFields(info.Data, params)
                    ' add the message to the database and try to deliver it
                    If Not Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.AddMessage(msgInfo, MSGCommon.GetCurrentUserId()) Then
                        MSGCommon.Alert(Page, "Message could not be sent.")
                        'Return
                    End If
                    res = Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.DeliverMessageAdHoc(Page, msgInfo.MessageId, MSGCommon.GetCurrentUserId())

                    Try
                        If res = "" Then
                            msgInfo.IsInDB = True
                            objInfos(i) = msgInfo
                            i = i + 1
                        Else
                            messageIdsDelete = messageIdsDelete + "'" + msgInfo.MessageId + "',"
                            resFinal += res.Replace(vbCrLf.ToString(), " ") + ":" + drAH("FirstName").ToString + " " + drAH("LastName").ToString + "\\n" ' cr/lf causes problems in the html, just remove it
                        End If
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        resFinal += res.Replace(vbCrLf.ToString(), " ") + "\\n" ' cr/lf causes problems in the html, just remove it
                    End Try
                    If ddlDelivery.SelectedValue.ToLower = "printer" Then
                        Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.DeliverMessageAdHocPrinter(Page, fck.Content) 'Server.HtmlEncode(
                        For Each obj As Object In objInfos
                            Dim msgInfoUpdate As New Fame.AdvantageV1.Common.MSG.MessageInfo
                            Try
                                If Not obj Is Nothing Then
                                    msgInfoUpdate = CType(obj, Fame.AdvantageV1.Common.MSG.MessageInfo)
                                    Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.UpdateMessage(msgInfoUpdate, MSGCommon.GetCurrentUserId())
                                End If
                            Catch ex As Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.DeleteMessage(msgInfoUpdate.MessageId)
                            End Try
                        Next
                    End If
                Else
                    If resFinal + "" = "" Then
                        resFinal = "No relevant fields found in the AdHoc report"
                    End If
                End If

                ''resFinal += res.Replace(vbCrLf.ToString(), " ") + ":" + drAH("FirstName").ToString + " " + drAH("LastName").ToString + "\\n" ' cr/lf causes problems in the html, just remove it
            Next

        End If

        ' if delivery type anything but printer, redirect to the outbox
        ' if delivery type is printer, then we need to stay on this page in order for the
        ' print process to complete

        ' Dim tempInfo As New FAME.AdvantageV1.Common.MSG.TemplateInfo

        Dim qString As String = ""
        If Not String.IsNullOrEmpty(msgInfo.RecipientType) Then
            qString += "&rtype=" + msgInfo.RecipientType
        End If

        If Not String.IsNullOrEmpty(msgInfo.RecipientId) Then
            qString += "&rid=" + msgInfo.RecipientId
        End If

        If Not String.IsNullOrEmpty(msgInfo.DeliveryType) Then
            qString += "&dtype=" + msgInfo.DeliveryType
        End If

        If Not String.IsNullOrEmpty(msgInfo.GroupId) Then
            qString += "&mgid=" + msgInfo.GroupId
        End If

        If Not String.IsNullOrEmpty(msgInfo.TemplateId) Then
            qString += "&tid=" + msgInfo.TemplateId
        End If
        If Not String.IsNullOrEmpty(msgInfo.MessageId) Then
            qString += "&messageid=" + msgInfo.MessageId
        End If
        If ddlDelivery.SelectedValue.ToLower <> "printer" Then
            If ddlAdHocReport.SelectedValue.ToString = "0" Then
                If res = "" Then
                    Me.Response.Redirect("ViewMessages.aspx?source=0" + qString)
                Else
                    Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.DeleteMessage(msgInfo.MessageId)
                    MSGCommon.DisplayErrorMessage(Me, "Message could Not be sent.  Reason=" + res)
                    MSGCommon.SetBrowserStatusBar(Me, res)
                End If
            Else
                If resFinal = "" Then
                    Me.Response.Redirect("ViewMessages.aspx?source=0")
                Else
                    MSGCommon.DisplayErrorMessage(Me, "Message could Not be sent.  Reason=" + resFinal)
                    If Not messageIdsDelete + "" = "" Then
                        Fame.AdvantageV1.BusinessFacade.MSG.MessagingFacade.DeleteMessageAdHoc(messageIdsDelete)
                    End If
                    MSGCommon.SetBrowserStatusBar(Me, res)
                End If
            End If

        Else
            If Not resFinal = "" Then
                If ddlDelivery.SelectedValue.ToLower <> "printer" Then
                    MSGCommon.DisplayErrorMessage(Me, "Message could Not be sent.  Reason=" + resFinal)
                Else
                    MSGCommon.DisplayErrorMessage(Me, "Document could Not be print.  Reason=" + resFinal)
                End If
            Else
                ResetForm()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Handler for the button Send to Outbox. 
    ''' Adds a new message into the queue.  Does not delivery immediately.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
    '    ' If Not ValidateForm() Then Return

    '    ' '' Build the MessageInfo object that will get added to syMessages (the Queue)
    '    ''Dim msgInfo As New MessageInfo
    '    ''msgInfo.IsInDB = False
    '    ''msgInfo.TemplateId = ddlTemplate.SelectedValue
    '    ''msgInfo.FromId = MSGCommon.GetCurrentUserId()
    '    ''msgInfo.DeliveryType = ddlDelivery.SelectedValue
    '    ''msgInfo.RecipientType = hfRecipientType.Value
    '    ''msgInfo.RecipientId = hfRecipientId.Value
    '    ''msgInfo.ReType = hfReType.Value
    '    ''msgInfo.ReId = hfReId.Value
    '    ''msgInfo.DeliveryDate = Date.Now
    '    ''msgInfo.MsgContent = fck.Value
    '    ' '' add the message to the database
    '    ''If Not MessagingFacade.AddMessage(msgInfo, MSGCommon.GetCurrentUserId()) Then
    '    ''    MSGCommon.Alert(Page, "Message could Not be sent.")
    '    ''Else
    '    ''    'MSGCommon.Alert(Page, "Message sent to Outbox.")
    '    ''    Me.Response.Redirect("ViewMessages.aspx?source=0")
    '    ''End If

    '    ' Build the MessageInfo object that will get added to syMessages (the Queue)
    '    Dim msgInfo As Fame.AdvantageV1.Common.MSG.MessageInfo

    '    '   Dim res As String
    '    Dim resFinal As String
    '    Dim i As Integer = 0
    '    Dim MessageIds As String = ""
    '    Dim MessageIdsDelete As String = ""
    '    If ddlAdHocReport.SelectedValue.ToString = "0" Then
    '        msgInfo = New Fame.AdvantageV1.Common.MSG.MessageInfo
    '        msgInfo.IsInDB = False
    '        msgInfo.TemplateId = ddlTemplate.SelectedValue
    '        msgInfo.FromId = MSGCommon.GetCurrentUserId()
    '        msgInfo.DeliveryType = ddlDelivery.SelectedValue
    '        msgInfo.RecipientType = hfRecipientType.Value
    '        msgInfo.RecipientId = hfRecipientId.Value
    '        msgInfo.ReType = hfReType.Value
    '        msgInfo.ReId = hfReId.Value
    '        msgInfo.DeliveryDate = Date.Now
    '        msgInfo.MsgContent = fck.Content 'Server.HtmlDecode(
    '        ' add the message to the database and try to deliver it
    '        If Not MessagingFacade.AddMessage(msgInfo, MSGCommon.GetCurrentUserId()) Then
    '            MSGCommon.Alert(Page, "Message could Not be sent.")
    '        Else
    '            'MSGCommon.Alert(Page, "Message sent to Outbox.")
    '            Me.Response.Redirect("ViewMessages.aspx?source=0")
    '        End If
    '    Else
    '        Dim dsAdHoc As New DataSet
    '        dsAdHoc = CType(Session("dsAdHoc"), DataSet)
    '        Dim objInfos(dsAdHoc.Tables(0).Rows.Count - 1) As Object
    '        For Each drAH As DataRow In dsAdHoc.Tables(0).Rows
    '            If Not drAH("RecipientId").ToString = Guid.Empty.ToString Then
    '                msgInfo = New Fame.AdvantageV1.Common.MSG.MessageInfo
    '                msgInfo.IsInDB = False
    '                msgInfo.TemplateId = ddlTemplate.SelectedValue
    '                msgInfo.FromId = MSGCommon.GetCurrentUserId()
    '                msgInfo.DeliveryType = ddlDelivery.SelectedValue
    '                msgInfo.RecipientType = hfRecipientType.Value
    '                msgInfo.RecipientId = drAH("RecipientId").ToString
    '                msgInfo.ReType = hfReType.Value
    '                msgInfo.ReId = Guid.Empty.ToString
    '                msgInfo.DeliveryDate = Date.Now
    '                Dim info As TemplateInfo = TemplatesFacade.GetTemplateInfo(ddlTemplate.SelectedValue)
    '                Dim params As New SpecialFields_Params(MSGCommon.GetCampusID, hfRecipientType.Value, hfRecipientId.Value, hfReType.Value, hfReId.Value, "")
    '                msgInfo.MsgContent = SpecialFields.SubSpecialFields(info.Data, params)
    '                ' add the message to the database and try to deliver it
    '                If Not MessagingFacade.AddMessage(msgInfo, MSGCommon.GetCurrentUserId()) Then
    '                    MSGCommon.Alert(Page, "Message could Not be sent.")
    '                End If
    '            Else
    '                If resFinal + "" = "" Then
    '                    resFinal = "No relevant fields found in the AdHoc report"
    '                End If
    '            End If
    '        Next
    '        If Not resFinal + "" = "" Then
    '            MSGCommon.Alert(Page, resFinal)
    '        Else
    '            Me.Response.Redirect("ViewMessages.aspx?source=0")
    '        End If
    '    End If
    'End Sub

    ''' <summary>
    ''' User has selected a module.  
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlModules_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlModules.SelectedIndexChanged
        ' Mantis 7627: Event was not getting fired when "All" was selected because the ddlModules[0] was the
        ' same as ddlModules[1].  I.e., the value for both was "".  Now, the value for the 0 index "None" so 
        ' it needs to be cleared before building the template list for the ddl.
        Dim ModuleId As String = ddlModules.SelectedValue
        If ddlModules.SelectedIndex = 0 Then
            ModuleId = ""
        End If
        MSGCommon.BuildAvailableTemplates(MSGCommon.GetCurrentUserId(), MSGCommon.GetCampusID(), ModuleId, Nothing, ddlTemplate)
        'New Code Added By Vijay Ramteke on May 05, 2010
        MSGCommon.BuildAdHocDDL(MSGCommon.GetCurrentUserId(), ModuleId, MSGCommon.GetCampusID(), ddlAdHocReport)
        'New Code Added By Vijay Ramteke on May 05, 2010

        fck.Content = ""
        fck.Tools.Clear()
    End Sub

    ''' <summary>
    ''' User has selected a template.  Fill up the form.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTemplate.SelectedIndexChanged
        If ddlTemplate.SelectedIndex > 0 Then


            Dim info As TemplateInfo = TemplatesFacade.GetTemplateInfo(ddlTemplate.SelectedValue)

            Dim params As New SpecialFields_Params()
          
            params = New SpecialFields_Params(MSGCommon.GetCampusID, AdvantageSession.TaskRecipientTypeId, AdvantageSession.TaskRecipientId, hfReType.Value, hfReId.Value, "")

            info.Data = SpecialFields.SubSpecialFields(info.Data, params)


            ' set this value so the special field popup has the right values
            MSGCommon.SetSpecialFieldPopupParam(info.EntityId)
            ddlModules.SelectedValue = info.ModuleId
            lbEntity.Text = info.EntityName
            hfRecipientType.Value = info.EntityId
            ' Load the template into the editor     
            CommonWebUtilities.SetupRadEditor(fck, "AdvMessaging_MsgCompose", info.EntityId)
            fck.Content = info.Data 'Server.HtmlEncode(
            'New Code Added By Vijay Ramteke on May 05, 2010
            MSGCommon.BuildAdHocDDL(MSGCommon.GetCurrentUserId(), ddlModules.SelectedValue, MSGCommon.GetCampusID(), ddlAdHocReport)
            'New Code Added By Vijay Ramteke on May 05, 2010
        Else

            lbEntity.Text = "{N/A}"
            hfRecipientType.Value = ""
            hfReType.Value = ""
            fck.Content = ""
            fck.Tools.Clear()
        End If
    End Sub
    ''' <summary>
    ''' Do special field substitutions
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub DoPreview()
        Try
            If ddlTemplate.SelectedValue = "" Then Return

            Dim info As TemplateInfo = TemplatesFacade.GetTemplateInfo(ddlTemplate.SelectedValue)
            ' do the special fields substitutions
            Dim params As New SpecialFields_Params(MSGCommon.GetCampusID, hfRecipientType.Value, hfRecipientId.Value, hfReType.Value, hfReId.Value, "")
            fck.Content = SpecialFields.SubSpecialFields(info.Data, params) 'Server.HtmlEncode(
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub


    Protected Sub ddlAdHocReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAdHocReport.SelectedIndexChanged
        If Not ddlAdHocReport.SelectedValue.ToString = "0" Then
            ViewState("rid") = ddlAdHocReport.SelectedValue.ToString
            ''New Code Added By Vijay Ramteke On June 27, 2010 For Mantis 19269
            If Not MSGCommon.GetCampusGroupDescFromReportId(ddlAdHocReport.SelectedValue.ToString).ToLower = "all" Then
                ViewState("CampGrpId") = MSGCommon.GetCampusID()
            Else
                ViewState("CampGrpId") = ""
            End If
            ''New Code Added By Vijay Ramteke On June 27, 2010 For Mantis 19269
            pnlRHS.Visible = True
            Dim info As AdHocRptInfo = AdHocRptFacade.GetReportInfo(ddlAdHocReport.SelectedValue.ToString)
            Dim dsFields As DataSet = Fame.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetReportParams(ddlAdHocReport.SelectedValue.ToString, True)
            BindFilters(dsFields)
            trReportParam.Visible = True
            trRecipient.Visible = False
        Else
            ''New Code Added By Vijay Ramteke On June 27, 2010 For Mantis 19269
            ViewState("rid") = ""
            ViewState("CampGrpId") = ""
            ViewState("CampGrp") = ""
            ''New Code Added By Vijay Ramteke On June 27, 2010 For Mantis 19269
            trReportParam.Visible = False
            trRecipient.Visible = True
        End If
    End Sub
    Protected Sub BindFilters(ByVal ds As DataSet)
        ' first, get all the rows with IsParam = 1 and put it in a new datatable called "dt"
        Dim dt As DataTable = ds.Tables(0).Clone()
        Dim drs() As DataRow = ds.Tables(0).Select("IsParam=1")
        For Each dr As DataRow In drs
            dt.Rows.Add(dr.ItemArray)
        Next
        ' Bind the resulting datatable to the repeater
        Dim rpt As Repeater = Step1.ContentTemplateContainer.FindControl("rptFilters")
        rpt.DataSource = dt
        rpt.DataBind()
        Dim ReportId As String = ViewState("rid")
        For Each ri As RepeaterItem In rpt.Items
            Dim DDLId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
            Dim fldType As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)

            If Not DDLId Is Nothing AndAlso DDLId <> "" Then
                _SetupFilter_DDL(ri)
            ElseIf fldType = FieldType._DataTime Then
                _SetupFilter_Calendar(ri)
            Else
                _SetupFilter_RegularInput(ri)
            End If
        Next
        ' Display a message if there are no filters for this report
        lblChooseFilter.Visible = (rpt.Items.Count > 0)
        lblNoFilters.Visible = (rpt.Items.Count = 0)
        divMain.Visible = (rpt.Items.Count > 0)

    End Sub
    Protected Sub _SetupFilter_DDL(ByVal ri As RepeaterItem)
        Dim DDLId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
        Dim dl As DataList = ri.FindControl("dlValues")

        If ViewState("CampGrpId") Is Nothing Then
            ViewState("CampGrpId") = ""
        End If
        dl.DataSource = Fame.AdvantageV1.BusinessFacade.Reports.ReportFacade.GetValuesForField(DDLId, ViewState("CampGrpId").ToString, AdvantageSession.UserState.UserId.ToString)
        dl.DataBind()
        If dl.Items.Count > 4 Then
            ri.FindControl("lblDiv1").Visible = True
            ri.FindControl("lblDiv2").Visible = True
        End If
    End Sub
    Protected Sub _SetupFilter_Calendar(ByVal ri As RepeaterItem)
        ' special case for datetime field.  We need to show controls that allow the user
        ' to enter in an exact date, or a max/min date
        ri.FindControl("lbFilterEqual").Visible = True
        ri.FindControl("txtFilterEqual").Visible = True
        ri.FindControl("ibEqualDate").Visible = True
        ri.FindControl("lbFilterMin").Visible = True
        ri.FindControl("txtFilterMin").Visible = True
        ri.FindControl("ibMinDate").Visible = True
        ri.FindControl("lbFilterMax").Visible = True
        ri.FindControl("txtFilterMax").Visible = True
        ri.FindControl("ibMaxDate").Visible = True

        ' add calendar javascript for the Equal image button
        Dim ctlID As String = ri.FindControl("txtFilterEqual").ClientID
        Dim ibID As String = ri.FindControl("ibEqualDate").ClientID
        Dim img As HtmlImage = ri.FindControl("ibEqualDate")
        img.Attributes.Add("onload", String.Format("setup_calendar('{0}','{1}')", ctlID, ibID))

        ' add calendar javascript for the Min image button
        ctlID = ri.FindControl("txtFilterMin").ClientID
        ibID = ri.FindControl("ibMinDate").ClientID
        img = ri.FindControl("ibMinDate")
        img.Attributes.Add("onload", String.Format("setup_calendar('{0}','{1}')", ctlID, ibID))

        ' add calendar javascript for the Min image button
        ctlID = ri.FindControl("txtFilterMax").ClientID
        ibID = ri.FindControl("ibMaxDate").ClientID
        img = ri.FindControl("ibMaxDate")
        img.Attributes.Add("onload", String.Format("setup_calendar('{0}','{1}')", ctlID, ibID))
    End Sub
    Protected Sub _SetupFilter_RegularInput(ByVal ri As RepeaterItem)
        ri.FindControl("txtRegFilter").Visible = True
        Dim ddl As DropDownList = ri.FindControl("ddlRegFilterOp")
        ddl.Visible = True

        ' Build the dropdownlist with a list of possible operators
        ddl.Items.Add(New ListItem("-- Select --", ""))
        ddl.Items.Add(New ListItem("Equal", "="))
        ddl.Items.Add(New ListItem("Contains", "like"))
        ddl.Items.Add(New ListItem("Not Equal", "<>"))
        ddl.Items.Add(New ListItem("Greater Than", ">"))
        ddl.Items.Add(New ListItem("Less Than", "<"))
        ddl.Items.Add(New ListItem("Greater Than or Equal", ">="))
        ddl.Items.Add(New ListItem("Less Than or Equal", "<="))
    End Sub

    Protected Sub btnMergeData_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMergeData.Click
        Dim rid As String = ViewState("rid")
        If rid Is Nothing Or rid = "" Then
            MSGCommon.DisplayErrorMessage(Me, "Please select a report.")
            Return
        End If
        ' ensure that all required filters have been specified
        Dim res As String = ValidateFilters()
        If res <> "" Then
            MSGCommon.DisplayErrorMessage(Me, res)
            Return
        End If
        Session("rptparams") = GetReportFilters()
        Try
            Dim info As AdHocRptInfo = AdHocRptFacade.GetReportInfo(rid)
            If info Is Nothing Then Return
            ' set the filter in the info object so it gets added when we run the report
            info.Filter = Session("rptparams")

            Dim ds As DataSet = AdHocRptFacade.RunReportAdHoc(info, hfRecipientType.Value, False, False)
            If ds.Tables(0).Rows.Count > 0 Then
                Session("dsAdHoc") = ds
                DoMerge(ds)
            Else
                Session("dsAdHoc") = Nothing
                MSGCommon.DisplayErrorMessage(Me, "No Data To Merge.")
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Session("dsAdHoc") = Nothing
            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub
    Protected Function ValidateFilters() As String
        Dim rpt As Repeater = Step1.ContentTemplateContainer.FindControl("rptFilters")
        For Each ri As RepeaterItem In rpt.Items
            If ri.FindControl("lblReq").Visible Then
                Dim DDLId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
                Dim fldType As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)
                If Not DDLId Is Nothing AndAlso DDLId <> "" Then
                    ' check that at least one value has been specfied in the DDL filter form
                    Dim dl As DataList = ri.FindControl("dlValues")
                    Dim bFound As Boolean = False
                    For i As Integer = 0 To dl.Items.Count - 1
                        Dim dli As DataListItem = dl.Items(i)
                        If CType(dli.FindControl("cbValue"), CheckBox).Checked Then
                            bFound = True
                        End If
                    Next
                    If Not bFound Then
                        Return "At least one required filter has not been supplied"
                    End If
                ElseIf fldType = FieldType._DataTime Then
                    ' check that at least one value has been specfied in the calendar filter form
                    If CType(ri.FindControl("txtFilterEqual"), TextBox).Text = "" AndAlso
                        CType(ri.FindControl("txtFilterMin"), TextBox).Text = "" AndAlso
                        CType(ri.FindControl("txtFilterMax"), TextBox).Text = "" Then
                        Return "At least one required filter has not been supplied"
                    End If
                Else
                    ' check that at least one value has been specfied in the regular filter form
                    Dim txt As TextBox = ri.FindControl("txtRegFilter")
                    Dim ddl As DropDownList = ri.FindControl("ddlRegFilterOp")
                    If txt.Text = "" Or ddl.SelectedValue = "" Then
                        Return "At least one required filter has not been supplied"
                    End If
                End If
            End If
        Next
        Return ""
    End Function
    Protected Function GetReportFilters() As String
        Dim sb As New StringBuilder()
        Dim rpt As Repeater = Step1.ContentTemplateContainer.FindControl("rptFilters")
        Dim ReportId As String = ViewState("rid")
        ' iterate through each item in the repeater
        ' each item contains a TblFldId along with a checkbox list of values
        For Each ri As RepeaterItem In rpt.Items
            Dim DDLId As String = CType(ri.FindControl("hfDDLId"), HiddenField).Value
            Dim fldType As FieldType = CType(CType(ri.FindControl("hfFldType"), HiddenField).Value, FieldType)

            Dim s As String = ""
            If Not DDLId Is Nothing AndAlso DDLId <> "" Then ' DDL driven form
                s = _GetReportFilter_DDL(ri)
            ElseIf fldType = FieldType._DataTime Then ' Datetime/Calendar driven form
                s = _GetReportFilter_Calendar(ri)
            Else ' The filters came from the user manually entering an operation and a value
                s = _GetReportFilter_RegularInput(ri)
            End If

            If s.Length > 0 Then
                sb.Append(String.Format("{0} AND {1}", s, vbCrLf))
            End If
        Next

        ' remove the last "AND"
        If sb.Length > 6 Then sb.Remove(sb.Length - 6, 6)

        Return sb.ToString()
    End Function
    Protected Function _GetReportFilter_DDL(ByVal ri As RepeaterItem) As String
        Dim fldTypeId As FieldType = CType(ri.FindControl("hfFldType"), HiddenField).Value
        Dim dl As DataList = ri.FindControl("dlValues")
        Dim sb As New StringBuilder()
        For i As Integer = 0 To dl.Items.Count - 1
            Dim dli As DataListItem = dl.Items(i)
            If CType(dli.FindControl("cbValue"), CheckBox).Checked Then
                Dim Value As String = CType(dli.FindControl("lblId"), Label).Text
                sb.Append(GetValueForSql(fldTypeId, Value) + ",")
            End If
        Next

        ' only add to the filter list if there is length to it
        If sb.Length = 0 Then
            Return ""
        End If

        sb.Remove(sb.Length - 1, 1) ' remove the last comma

        Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
        Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
        Dim s As String = String.Format("{0}.{1} in ({2})", tblName, fldName, sb.ToString())
        Return s
    End Function
    Protected Function _GetReportFilter_Calendar(ByVal ri As RepeaterItem) As String
        ' special case for datetime field.  We need to show controls that allow the user
        ' to enter in an exact date, or a max/min date        
        Dim txtMin As TextBox = ri.FindControl("txtFilterMin")
        Dim txtMax As TextBox = ri.FindControl("txtFilterMax")
        Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
        Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value
        Dim sb As New StringBuilder()


        Dim txt As TextBox = ri.FindControl("txtFilterEqual")
        If txt.Text.Length > 0 Then
            Try
                Dim d As DateTime = CType(txt.Text, DateTime)
                sb.AppendFormat("{0}.{1} = '{2}' ", tblName, fldName, d.ToShortDateString())
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End If

        txt = ri.FindControl("txtFilterMin")
        If txt.Text.Length > 0 Then
            Try
                Dim d As DateTime = CType(txt.Text, DateTime)
                If sb.Length = 0 Then
                    sb.AppendFormat("{0}.{1} >= '{2}' ", tblName, fldName, d.ToShortDateString())
                Else
                    sb.AppendFormat(" AND {0}.{1} >= '{2}' ", tblName, fldName, d.ToShortDateString())
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End If

        txt = ri.FindControl("txtFilterMax")
        If txt.Text.Length > 0 Then
            Try
                Dim d As DateTime = CType(txt.Text, DateTime)
                If sb.Length = 0 Then
                    sb.AppendFormat("{0}.{1} <= '{2}' ", tblName, fldName, d.ToShortDateString())
                Else
                    sb.AppendFormat(" AND {0}.{1} <= '{2}' ", tblName, fldName, d.ToShortDateString())
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End If
        Return sb.ToString()
    End Function
    Protected Function _GetReportFilter_RegularInput(ByVal ri As RepeaterItem) As String
        Dim fldTypeId As FieldType = CType(ri.FindControl("hfFldType"), HiddenField).Value
        Dim txt As TextBox = ri.FindControl("txtRegFilter")
        Dim ddl As DropDownList = ri.FindControl("ddlRegFilterOp")
        If ddl.SelectedValue <> "" Then
            Dim value As String = txt.Text
            If ddl.SelectedValue = "like" Then value = "%" + value + "%"
            value = GetValueForSql(fldTypeId, value) ' convert the value to a sql type value (i.e., add single quotes for a string value)

            Dim tblName As String = CType(ri.FindControl("hfTblName"), HiddenField).Value
            Dim fldName As String = CType(ri.FindControl("hfFldName"), HiddenField).Value

            Return String.Format("{0}.{1} {2} {3}", tblName, fldName, ddl.SelectedValue, value)
        End If
        Return "" ' operator was not selected
    End Function
    Protected Function GetValueForSql(ByVal fldTypeId As FieldType, ByVal value As String) As String
        Select Case fldTypeId
            Case FieldType._Bit
                Return value
            Case FieldType._Char
                Return "'" + value + "'"
            Case FieldType._DataTime
                Return "'" + value + "'"
            Case FieldType._Decimal
                Return value
            Case FieldType._Float
                Return value
            Case FieldType._Int
                Return value
            Case FieldType._Money
                Return value
            Case FieldType._Smallint
                Return value
            Case FieldType._TinyInt
                Return value
            Case FieldType._Uniqueidentifier
                Return "'" + value + "'"
            Case FieldType._Unknown
                Return "'" + value + "'"
            Case FieldType._Varchar
                Return "'" + value + "'"
        End Select
        Return "'" + value + "'"
    End Function

    Protected Sub DoMerge(ByVal ds As DataSet)
        If ddlTemplate.SelectedValue = "" Then Return
        Dim info As TemplateInfo = TemplatesFacade.GetTemplateInfo(ddlTemplate.SelectedValue)
        fck.Content = SpecialFields.SubSpecialFieldsAdHoc(info.Data, ds) 'Server.HtmlEncode(
    End Sub
End Class