﻿Imports Fame.common
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Fame.AdvantageV1.BusinessFacade.MSG
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports BMsg = FAME.AdvantageV1.BusinessFacade.MSG

Partial Class ViewMSGStudentMessages
    Inherits BasePage


    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected StudentId As String
    Protected resourceId As Integer
    Protected boolStatus As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String
    Protected modCode As String
    Protected state As AdvantageSessionState

    Protected LeadId As String = Guid.Empty.ToString
    Protected boolSwitchCampus As Boolean = False



    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try
            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)




        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region
    Private Sub Page_Load1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim userId, campusId, moduleId As String
        Dim advantageUserState As New BO.User()
        Dim pObj As New UserPagePermissionInfo

        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        advantageUserState = AdvantageSession.UserState
        userId = AdvantageSession.UserState.UserId.ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        ViewState("CampusId") = campusId
        moduleId = AdvantageSession.UserState.ModuleCode.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = getStudentFromStateObject(334) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString


        End With


        advantageUserState = AdvantageSession.UserState
        MSGCommon.AdvInit(Me)

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If
            CommonWebUtilities.SetupRadEditor(fck, "AdvMessaging_MsgCompose")
            ' MSGCommon.SetupEditor(fck, "AdvMessaging_ProcessMsgs")
            InitButtonsForLoad()
            fck.Content = ""
            fck.Visible = False ' editor should be hidden initialy
            ' fck.ToolbarStartExpanded = False
            fck.ToolbarMode = Telerik.Web.UI.EditorToolbarMode.ShowOnFocus

            ' Get and save the paramters
            Session("rtype") = Request.Params("rtype")
            Session("rid") = StudentId
            Session("dtype") = Request.Params("dtype")
            Session("mgid") = Request.Params("mgid")
            Session("tid") = Request.Params("tid")
            Session("errors") = Request.Params("errors")
            Session("source") = "1"
            If Session("source") Is Nothing Or Session("source") = "" Then
                Session("source") = "0"
            End If

            ' Set the title bar to either Sent Items or Outbox
            'If Session("source") <> "0" Then
            '    Me.Title = "Sent Items"
            'Else
            '    Me.Title = "Outbox"
            'End If

            ' display the messages
            BindMessages()
            fck.Visible = False
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            MyBase.uSearchEntityControlId.Value = ""
        End If
        btnNew.Enabled = False
        btnSave.Enabled = False
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If
        btnNew.Enabled = True
    End Sub
    Private Sub BindMessages()
        Try
            ' get the filters
            Dim strFilterSource As String = Session("source")
            Dim filterRecipientType As String = Session("rtype")
            Dim filterRecipientId As String = Session("rid")
            Dim filterDeliveryType As String = Session("dtype")
            Dim filterMessageGroupId As String = Session("mgid")
            Dim filterTemplateId As String = Session("tid")
            Dim filterErrors As String = Session("errors")
            Dim filterShowOnlyErrors As Boolean = False
            Dim filterMessageId As String = Session("messageid")
            'Dim msg As FAME.AdvantageV1.BusinessFacade.MSG.MessagingFacade
            Dim ds As New System.Data.DataSet
            ' Populate the repeater control with the Items DataSet
            ' check which source we want (0=Outbox, 1=Sent Items)
            If strFilterSource = "0" Then
                ds = BMsg.MessagingFacade.GetAllMessages_OutBox(filterRecipientType,
                                                                            filterRecipientId,
                                                                            filterDeliveryType,
                                                                            filterMessageGroupId,
                                                                            filterTemplateId,
                                                                            filterShowOnlyErrors,
                                                                            filterMessageId)
            Else
                ds = BMsg.MessagingFacade.GetAllMessages_Sent(filterRecipientType, _
                                                                          filterRecipientId, _
                                                                          filterDeliveryType, _
                                                                          filterMessageGroupId, _
                                                                          filterTemplateId, _
                                                                          filterShowOnlyErrors)
            End If

            rptQueue.DataSource = ds
            rptQueue.DataBind()
            ds.Dispose()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Protected Sub rptQueue_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles rptQueue.ItemCommand
        Try
            Dim MessageId As String = e.CommandArgument.ToString
            'Dim msg As FAME.AdvantageV1.BusinessFacade.MSG.MessagingFacade
            fck.Visible = True
            If Session("source") <> 1 Then
                ' MSGCommon.SetupEditor(fck, "AdvMessaging_ProcessMsgs")
                fck.Enabled = True
            Else
                'MSGCommon.SetupEditor(fck, "AdvMessaging_Disabled")
                fck.Enabled = False
            End If
            fck.ToolbarMode = Telerik.Web.UI.EditorToolbarMode.ShowOnFocus
            ' fck.ToolbarStartExpanded = False
            Dim msgInfo As FAME.AdvantageV1.Common.MSG.MessageInfo = BMsg.MessagingFacade.GetMessageInfo(MessageId)
            fck.Visible = True
            fck.Content = msgInfo.MsgContent
            '  CommonWebUtilities.SetStyleToSelectedItem(rptQueue, MessageId, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(rptQueue, MessageId)
            ' handle the case that the user want to deliver a single message
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
