/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2004 Frederico Caldeira Knabben
 * 
 * Licensed under the terms of the GNU Lesser General Public License:
 * 		http://www.opensource.org/licenses/lgpl-license.php
 * 
 * For further information visit:
 * 		http://www.fckeditor.net/
 * 
 * File Name: en.js
 * 	Dutch language file for the OpenWord plugin.
 * 
 * Version:  2.0
 * Modified: 2004-11-22 11:20:42
 * 
 * File Authors:
 * 		Koen Willems (kwillems@zonnet.nl)
 */
FCKLang.OpenWord = 'Open Word' ;
FCKLang.OpenWordDlgTitle = 'Open from Word' ;
FCKLang.OpenWordDlgTitle_Tip = 'Open from a Word document (.doc)' ;
FCKLang.OpenWordAlert = 'The file you\'ve selected is not a Word-document.\nIt should end with the extension \'.doc\'';
FCKLang.OpenWordClean = 'Clean the Word-document' ;
