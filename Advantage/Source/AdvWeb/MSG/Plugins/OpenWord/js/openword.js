function Ok ()
{
	var content;
	window.clipboardData.clearData();					/* clean the clipboard, before we start */
	var pathlength	= document.getElementById('filepath').value.length - 4;
	var pathisdoc	= document.getElementById('filepath').value.lastIndexOf('.doc');
	if (pathlength !== pathisdoc)
	{
		alert (FCKLang.OpenWordAlert);					/* It's not a Word-document! */
	}
	else												/* Wow, it's a Word-document! */
	{
		var file = unescape( document.getElementById('filepath').value )
		try{
			var word = new ActiveXObject('Word.Application');
			//var word = new ActiveXObject('Word.Application.6');
		/*									
              try {										/* What kind of Word-document do we have? 
                   word=new ActiveXObject('Word.Application.9') // word 2k
                   } catch (e) {
              try {
                   word=new ActiveXObject('Word.Application.8') // word 97
                   } catch (e) {
              try {
                   word=new ActiveXObject('Word.Application.7') // word 95
                   } catch (e) {
				}
				}
				}
		*/
			if (word != null)								/* the client-computer has Word installed */
			{
				word.Documents.Open(file);					/* So let's open the file to see what's in it */
				word.Documents(file).Range(0).Copy();		/* and put the content of the file on the clipboard */
				content = oEditor.FCK.GetClipboardHTML();	/* grab the content from the clipboard */
				var bCleanWord = document.getElementById('chkCleanWord').checked ;
				if (bCleanWord)
				{
					content=CleanWord(content);				/* clean the mess Word has made of it */
				}
				oEditor.FCK.InsertHtml( content ) ;			/* put the content into FCK */
				window.clipboardData.clearData('Text');		/* clean the clipboard, we don't need it anymore */
				word.Quit();								/* close Word */
				window.close();								/* and close the window */
			}
		}
		catch(e)
		{ 
			document.write(e.Message);
		}
	}
}

function checkCleanWord()								/* check if we have to clean the document */
{	if(document.getElementById('chkCleanWord').checked)
	{
		document.getElementById('indent').disabled='';
	}
	else
	{
		document.getElementById('indent').disabled='disabled';
	}
}

function CleanWord( html )
{
	var bIgnoreFont = document.getElementById('chkRemoveFont').checked ;
	var bRemoveStyles = document.getElementById('chkRemoveStyles').checked ;

	html = html.replace(/<o:p>\s*<\/o:p>/g, "") ;
	html = html.replace(/<o:p>.*?<\/o:p>/g, "&nbsp;") ;

	// Remove mso-xxx styles.
	html = html.replace( /\s*mso-[^:]+:[^;"]+;?/gi, "" ) ;

	// Remove margin styles.
	html = html.replace( /\s*MARGIN: 0cm 0cm 0pt\s*;/gi, "" ) ;
	html = html.replace( /\s*MARGIN: 0cm 0cm 0pt\s*"/gi, "\"" ) ;

	html = html.replace( /\s*TEXT-INDENT: 0cm\s*;/gi, "" ) ;
	html = html.replace( /\s*TEXT-INDENT: 0cm\s*"/gi, "\"" ) ;

	html = html.replace( /\s*TEXT-ALIGN: [^\s;]+;?"/gi, "\"" ) ;

	html = html.replace( /\s*PAGE-BREAK-BEFORE: [^\s;]+;?"/gi, "\"" ) ;

	html = html.replace( /\s*FONT-VARIANT: [^\s;]+;?"/gi, "\"" ) ;

	html = html.replace( /\s*tab-stops:[^;"]*;?/gi, "" ) ;
	html = html.replace( /\s*tab-stops:[^"]*/gi, "" ) ;

	// Remove FONT face attributes.
	if ( bIgnoreFont )
	{
		html = html.replace( /\s*face="[^"]*"/gi, "" ) ;
		html = html.replace( /\s*face=[^ >]*/gi, "" ) ;

		html = html.replace( /\s*FONT-FAMILY:[^;"]*;?/gi, "" ) ;
	}
	
	// Remove Class attributes
	html = html.replace(/<(\w[^>]*) class=([^ |>]*)([^>]*)/gi, "<$1$3") ;

	// Remove styles.
	if ( bRemoveStyles )
		html = html.replace( /<(\w[^>]*) style="([^\"]*)"([^>]*)/gi, "<$1$3" ) ;

	// Remove empty styles.
	html =  html.replace( /\s*style="\s*"/gi, '' ) ;

	html = html.replace( /<SPAN\s*[^>]*>\s*&nbsp;\s*<\/SPAN>/gi, '&nbsp;' ) ;

	html = html.replace( /<SPAN\s*[^>]*><\/SPAN>/gi, '' ) ;

	// Remove Lang attributes
	html = html.replace(/<(\w[^>]*) lang=([^ |>]*)([^>]*)/gi, "<$1$3") ;

	html = html.replace( /<SPAN\s*>(.*?)<\/SPAN>/gi, '$1' ) ;

	html = html.replace( /<FONT\s*>(.*?)<\/FONT>/gi, '$1' ) ;

	// Remove XML elements and declarations
	html = html.replace(/<\\?\?xml[^>]*>/gi, "") ;

	// Remove Tags with XML namespace declarations: <o:p></o:p>
	html = html.replace(/<\/?\w+:[^>]*>/gi, "") ;

	html = html.replace( /<H\d>\s*<\/H\d>/gi, '' ) ;

	html = html.replace( /<H1([^>]*)>/gi, '<div$1><b><font size="6">' ) ;
	html = html.replace( /<H2([^>]*)>/gi, '<div$1><b><font size="5">' ) ;
	html = html.replace( /<H3([^>]*)>/gi, '<div$1><b><font size="4">' ) ;
	html = html.replace( /<H4([^>]*)>/gi, '<div$1><b><font size="3">' ) ;
	html = html.replace( /<H5([^>]*)>/gi, '<div$1><b><font size="2">' ) ;
	html = html.replace( /<H6([^>]*)>/gi, '<div$1><b><font size="1">' ) ;

	html = html.replace( /<\/H\d>/gi, '</font></b></div>' ) ;

	html = html.replace( /<(U|I|STRIKE)>&nbsp;<\/\1>/g, '&nbsp;' ) ;

	// Remove empty tags (three times, just to be sure).
	html = html.replace( /<([^\s>]+)[^>]*>\s*<\/\1>/g, '' ) ;
	html = html.replace( /<([^\s>]+)[^>]*>\s*<\/\1>/g, '' ) ;
	html = html.replace( /<([^\s>]+)[^>]*>\s*<\/\1>/g, '' ) ;

	// Transform <P> to <DIV>
	var re = new RegExp("(<P)([^>]*>.*?)(<\/P>)","gi") ;	// Different because of a IE 5.0 error
	html = html.replace( re, "<div$2</div>" ) ;

	return html ;
}