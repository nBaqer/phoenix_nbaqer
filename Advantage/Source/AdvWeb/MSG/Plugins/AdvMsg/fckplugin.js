/*
**************************************************************************************
	FAME
	Copyright (c) 2005
	by Benjamin Strum of ThinkTron Corporation (www.thinktron.com)
	Description: Registers toolbars and the command handlers needed to integrate
		AdvMessaging with the FCKEditor
***************************************************************************************	
*/

/*
	Advantage Messaging Open Dialog plugin methods FCKEditor
*/
var FCKAdvMsgDialogCommand = function( name, title, url, width, height, getStateFunction, getStateParam )
{ this.name = name; }

FCKAdvMsgDialogCommand.prototype.Execute = function()
{
	var lb = parent.document.getElementById(this.name)
	if (lb != null)	lb.click()	
}

FCKAdvMsgDialogCommand.prototype.GetState = function()
{
	if ( this.GetStateFunction )	return this.GetStateFunction( this.GetStateParam ) ;
	else							return FCK_TRISTATE_OFF ;
}

/*
	main entry point functions for FCKEditor plugins
*/
function regNewCommandButton() 
{
	FCKCommands.RegisterCommand( 'AdvNew', new FCKAdvMsgDialogCommand('lbNewTemplate'));	
	var oItem		= new FCKToolbarButton( 'AdvNew', 
		FCKLang['DlgAdvNewTitle'], FCKLang['DlgAdvNewTitle_Tip'], FCK_TOOLBARITEM_ICONTEXT ) ;
	
	oItem.IconPath	= FCKConfig.PluginsPath + 'AdvMsg/AdvNew.gif' ;
	FCKToolbarItems.RegisterItem( 'AdvNew', oItem ) ;
}

function regOpenCommandButton() 
{
	FCKCommands.RegisterCommand( 'AdvOpen', new FCKAdvMsgDialogCommand('lbOpenTemplate') );	
	var oItem		= new FCKToolbarButton( 'AdvOpen', 
		FCKLang['DlgAdvOpenTitle'],  FCKLang['DlgAdvOpenTitle_Tip'], FCK_TOOLBARITEM_ICONTEXT ) ;
	
	oItem.IconPath	= FCKConfig.PluginsPath + 'AdvMsg/AdvOpen.gif' ;
	FCKToolbarItems.RegisterItem( 'AdvOpen', oItem ) ;
}

function regSaveCommandButton() 
{
	FCKCommands.RegisterCommand( 'AdvSave', new FCKAdvMsgDialogCommand('lbSaveTemplate'));	
	var oItem		= new FCKToolbarButton( 'AdvSave', 
		FCKLang['DlgAdvSaveTitle'], FCKLang['DlgAdvSaveTitle_Tip'], FCK_TOOLBARITEM_ICONTEXT ) ;
	
	oItem.IconPath	= FCKConfig.PluginsPath + 'AdvMsg/AdvSave.gif' ;
	FCKToolbarItems.RegisterItem( 'AdvSave', oItem ) ;
}

function regSaveAsCommandButton() 
{
	FCKCommands.RegisterCommand( 'AdvSaveAs', new FCKAdvMsgDialogCommand('lbSaveAsTemplate'));	
	var oItem		= new FCKToolbarButton( 'AdvSaveAs', 
		FCKLang['DlgAdvSaveAsTitle'], FCKLang['DlgAdvSaveAsTitle_Tip'], FCK_TOOLBARITEM_ICONTEXT ) ;
	
	oItem.IconPath	= FCKConfig.PluginsPath + 'AdvMsg/AdvSaveAs.gif' ;
	FCKToolbarItems.RegisterItem( 'AdvSaveAs', oItem ) ;
}
/*
function regCategoriesCommandButton() 
{
	FCKCommands.RegisterCommand( 'AdvCategories', new FCKAdvMsgDialogCommand('lbEditCategories'));
	var oItem		= new FCKToolbarButton( 'AdvCategories', 
		FCKLang['DlgAdvCategoriesTitle'], FCKLang['DlgAdvCategoriesTitle_Tip'], FCK_TOOLBARITEM_ICONTEXT ) ;
	
	oItem.IconPath	= FCKConfig.PluginsPath + 'AdvMsg/AdvCategories.gif' ;
	FCKToolbarItems.RegisterItem( 'AdvCategories', oItem ) ;
}
*/
function regInsertFieldCommandButton() 
{
	FCKCommands.RegisterCommand( 'InsertField', new FCKDialogCommand( 'InsertField', FCKLang['DlgAdvInsertFieldTitle'],
		FCKConfig.PluginsPath + '../SpecialFieldsPopup.aspx', 280, 410 ) ) ; 

	var oItem = new FCKToolbarButton( 'InsertField', 
		FCKLang['DlgAdvInsertFieldTitle'], FCKLang['DlgAdvInsertFieldTitle_Tip'], FCK_TOOLBARITEM_ICONTEXT ) ;
	
	oItem.IconPath	= FCKConfig.PluginsPath + 'AdvMsg/AdvInsertField.gif' ;	
	FCKToolbarItems.RegisterItem( 'InsertField', oItem ) ;
}

function regPreviewCommandButton() 
{
	FCKCommands.RegisterCommand( 'Preview', new FCKAdvMsgDialogCommand('lbPreview'));
	var oItem = new FCKToolbarButton( 'Preview', 
		FCKLang['DlgPreviewTitle'], FCKLang['DlgPreviewTitle_Tip'], FCK_TOOLBARITEM_ICONTEXT ) ;
	
	oItem.IconPath	= FCKConfig.PluginsPath + 'AdvMsg/Preview.gif' ;
	FCKToolbarItems.RegisterItem( 'Preview', oItem ) ;
}

// Call all the entry point functions
regNewCommandButton()
regOpenCommandButton();
regSaveCommandButton();
regSaveAsCommandButton();
//regCategoriesCommandButton();
regInsertFieldCommandButton();
regPreviewCommandButton() ;