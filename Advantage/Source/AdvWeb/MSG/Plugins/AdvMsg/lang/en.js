

FCKLang['DlgAdvNewTitle']			= 'New' ;
FCKLang['DlgAdvOpenTitle']			= 'Open Template' ;
FCKLang['DlgAdvSaveTitle']			= 'Save Template' ;
FCKLang['DlgAdvSaveAsTitle']		= 'Save Template As' ;

FCKLang['DlgAdvNewTitle_Tip']		= 'New' ;
FCKLang['DlgAdvOpenTitle_Tip']		= 'Open Template' ;
FCKLang['DlgAdvSaveTitle_Tip']		= 'Save Template' ;
FCKLang['DlgAdvSaveAsTitle_Tip']	= 'Save As Template' ;
FCKLang['DlgAdvCategoriesTitle_Tip']= 'Manage Categories' ;

FCKLang['DlgAdvInsertField']		= 'Insert Field' ;
FCKLang['DlgAdvInsertFieldTitle']	= 'Insert Special Field' ;
FCKLang['DlgAdvInsertFieldTitle_Tip'] = 'Insert Special Field' ;
FCKLang['DlgAdvInsertFieldAlert']	= 'Need to make a valid Selection';

FCKLang['Preview'] = 'Preview' ;
FCKLang['DlgPreviewTitle'] = 'Preview' ;
FCKLang['DlgPreviewTitle_Tip'] = 'Preview' ;
FCKLang['PreviewdAlert'] = 'Failed to create the Preview';