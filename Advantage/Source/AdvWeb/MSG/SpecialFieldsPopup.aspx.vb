'
' FAME
' Copyright (c) 2006
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdvMessaging
' Description: Popup window which allows the user to open a template for editing.
'   This popup return the TemplateId to the caller through javascript.

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.MSG

Partial Class Fields
    Inherits System.Web.UI.Page
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

    End Sub

    ' display the templates for a given category
    Private Sub BindTemplates(ByVal campusID As String, ByVal MessageGroupId As String)
        Try
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If Not Page.IsPostBack Then
            Dim id As String = Session("CurrentEntityId")
            If id Is Nothing Then id = ""
            BuildFieldCategories(ddlFieldCategories, id)
        End If
    End Sub
    ' here we had to make changes to show(filter) only category fields allowed for a given template
    Private Sub BuildFieldCategories(ByRef ddl As DropDownList, ByVal EntityId As String)
        Dim ds As New DataSet
        Try
            ' this method seems to get called twice se we have to 
            ' clear out the dropdownlist before doing anything
            ddl.Items.Clear()

            ' read from the xml file all the categories
            Dim xmlFileName As String = ddl.Page.MapPath("xml/MsgCategories.xml")
            ds.ReadXml(xmlFileName)

            If EntityId = "" Then
                'ddl.DataTextField = "Category"
                'ddl.DataValueField = "Category"
                'ddl.DataSource = ds
                'ddl.DataBind()
                MSGCommon.DisplayErrorMessage(Me, "Special fields cannot be displayed because no entity is associated with the current message.")
            Else
                ' filter the fields categories by group id
                ddl.DataSource = EntitiesFieldGroupsFacade.GetAll(EntityId, True, False)
                ddl.DataTextField = "FieldGroupName"
                ddl.DataValueField = "FieldGroupName"
                ddl.DataBind()
            End If
            ddl.Items.Insert(0, "---Select---")
            ddl.SelectedIndex = 0
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        ds.Dispose()
    End Sub

    Private Sub BindFields(ByVal Category As String)
        ' return if we are on the "---Select---" value
        If ddlFieldCategories.SelectedIndex = 0 Then
            Return
        End If

        Dim ds As New DataSet
        Try
            Dim xmlFileName As String = Page.MapPath("xml/MsgFields.xml")
            ds.ReadXml(xmlFileName)

            Dim ds2 As DataSet = ds.Clone()
            ds2.Tables.Add()
            Dim drs As DataRow() = ds.Tables(0).Select("category='" & Category & "'")
            Dim dr As DataRow
            For Each dr In drs
                ds2.Tables(0).ImportRow(dr)
            Next

            Dim dv As New DataView(ds2.Tables(0))
            dv.Sort = "description"

            lbFields.DataSource = dv
            lbFields.DataTextField = "description"
            lbFields.DataValueField = "name"
            lbFields.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        ds.Dispose()
    End Sub

    Private Sub ddlFieldCategories_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlFieldCategories.SelectedIndexChanged
        Dim Category As String = ddlFieldCategories.SelectedValue
        BindFields(Category)
    End Sub

End Class
