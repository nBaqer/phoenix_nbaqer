' ===============================================================================
' SetupGroups
' Handles adding, editing and deleting (make inactive) of groups
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.AdvantageV1.BusinessFacade.MSG
Imports FAME.AdvantageV1.Common.MSG

Partial Class AdvantageApp_MSG_SetupGroups
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'Dim fac As New UserSecurityFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId

        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        MSGCommon.AdvInit(Me)


        If Not Page.IsPostBack Then
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)
            ' clear out the form with original values
            ResetForm()
            ' display the groups defined in the system
            BindGroups()

            ' Bind the task to the form
            Dim GroupId As String = Request.Params("groupid")
            If Not GroupId Is Nothing Then
                BindGroup(GroupId)
            End If
        Else
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title
    End Sub

#Region "Advantage Permissions"
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub
#End Region

    ''' <summary>
    ''' Fills the form with a Group given a GroupId
    ''' </summary>
    ''' <param name="GroupId"></param>
    ''' <remarks></remarks>
    Protected Sub BindGroup(ByVal GroupId As String)
        Try
            Dim info As GroupInfo = GroupsFacade.GetGroupInfo(GroupId)
            ddlActive.SelectedValue = info.Active.ToString()
            txtCode.Text = info.Code
            txtName.Text = info.Descrip
            ViewState("moddate") = info.ModDate ' need this to do a proper delete

            BindTemplatesForGroup(GroupId)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Show all the templates that belong to this group
    ''' </summary>
    ''' <param name="GroupId"></param>
    ''' <remarks></remarks>
    Protected Sub BindTemplatesForGroup(ByVal GroupId As String)
        ' get all the tasks from cache so that we can do a select on it
        Dim ds As DataSet = ViewState("AllGroups")
        If ds Is Nothing Then
            ds = TemplatesFacade.GetTemplates(Nothing, Nothing, True, False)
            ViewState("AllGroups") = ds
        End If

        ' get the subset of rows
        Dim ds2 As New DataTable
        Dim drs As DataRow() = ds.Tables(0).Select("GroupId='" + GroupId + "'")
        Dim dr As DataRow
        ' insert columns into table
        For Each dc As DataColumn In ds.Tables(0).Columns
            ds2.Columns.Add(New DataColumn(dc.ColumnName, dc.DataType, dc.Expression))
        Next
        ' import the rows now
        For Each dr In drs
            ds2.ImportRow(dr)
        Next

        lbSysTemplates.DataSource = ds2
        lbSysTemplates.DataValueField = "TemplateId"
        lbSysTemplates.DataTextField = "Descrip"
        lbSysTemplates.DataBind()
    End Sub

    ''' <summary>
    ''' Binds all groups
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindGroups()
        If radStatus.SelectedIndex = 0 Then
            dlGroups.DataSource = GroupsFacade.GetGroups(True, False)
        ElseIf radStatus.SelectedIndex = 1 Then
            dlGroups.DataSource = GroupsFacade.GetGroups(False, True)
        Else
            dlGroups.DataSource = GroupsFacade.GetGroups(True, True)
        End If
        dlGroups.DataBind()
    End Sub

    ''' <summary>
    ''' Resets the form to the intial state.  All controls that are databound are initialized
    ''' from their datasource.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetForm()
        ' clear out the form
        MSGCommon.BuildStatusDDL(ddlActive)
        ddlActive.SelectedValue = True

        txtCode.Text = ""
        txtName.Text = ""

        lbSysTemplates.Items.Clear()
    End Sub

    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist containing all the users.
    ''' In response, this handler should populate the info for a group
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlGroups_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlGroups.ItemCommand
        ' get the selected groupid and store it in the viewstate object
        Dim groupid As String = e.CommandArgument.ToString
        ViewState("id") = groupid
        dlGroups.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If groupid Is Nothing Or groupid = "" Then
            MSGCommon.Alert(Me, "There was a problem displaying the details for this group.")
            Return
        End If

        ' bind the result to the form
        BindGroup(groupid)

        ' Advantage specific stuff
        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlGroups, ViewState("id").ToString)
    End Sub

    ''' <summary>
    ''' Checks the form's contents for validity.  If any control is
    ''' not valid, an alert is made and the function returns false.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As Boolean
        ' Check the task drop down
        If Me.txtCode.Text.Length = 0 Then
            MSGCommon.Alert(Me, "Code is required.")
            Return False
        End If

        If txtCode.Text.Length > 12 Then
            MSGCommon.DisplayErrorMessage(Me, "Code cannot be greater than 12 characters.")
            Return False
        End If

        If Me.txtName.Text.Length = 0 Then
            MSGCommon.Alert(Me, "Description is required.")
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            ' check that the user has entered in all the right values
            If Not ValidateForm() Then Return

            Dim groupid As String = ViewState("id")
            Dim info As New GroupInfo
            ' determine if this is an update or a new by looking
            ' at the ViewState which is set by passing a parameter to the page.
            If groupid Is Nothing Or groupid = "" Then
                info.IsInDB = False
                ViewState("id") = info.GroupId
            Else
                info.IsInDB = True
                info.GroupId = groupid
            End If

            info.Active = CType(ddlActive.SelectedValue, Boolean)
            info.Code = txtCode.Text
            info.Descrip = txtName.Text
            'info.CampGroupId = MSGCommon.GetCampusID()

            Dim opOk As Boolean = True ' TODO: tie this to the update operation
            GroupsFacade.UpdateGroup(info, MSGCommon.GetCurrentUserId())

            If opOk Then
                MSGCommon.Alert(Page, "Group was saved successfully")
            Else
                MSGCommon.Alert(Page, "Errors ocurred while saving the group")
            End If

            ' update the left hand side
            BindGroups()
            InitButtonsForEdit()
            CommonWebUtilities.RestoreItemValues(dlGroups, ViewState("id").ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Page, "Group could not be saved. Error=" + ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Simply reset the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ViewState("id") = Nothing
        ViewState("moddate") = Nothing
        ResetForm()

        ' Advantage specific stuff
        InitButtonsForLoad()


        CommonWebUtilities.RestoreItemValues(dlGroups, Guid.Empty.ToString)
    End Sub

    ''' <summary>
    ''' Deletes a message grouop
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim id As String = ViewState("id")
            If Not id Is Nothing AndAlso id <> "" Then
                Dim modDate As DateTime = ViewState("moddate")
                Dim res As String = GroupsFacade.DeleteGroup(id, modDate)
                If res <> "" Then
                    MSGCommon.DisplayErrorMessage(Me, res)
                Else
                    ViewState("id") = Nothing
                    ResetForm()
                    ' update the left pane
                    BindGroups()

                    ' Advantage specific stuff
                    'CommonWebUtilities.SetStyleToSelectedItem(Me.dlGroups, Guid.Empty.ToString, ViewState, Header1)
                    InitButtonsForLoad()
                End If
                CommonWebUtilities.RestoreItemValues(dlGroups, Guid.Empty.ToString)
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MSGCommon.DisplayErrorMessage(Me, "Error.  Record could not be deleted.")
        End Try
    End Sub

    ''' <summary>
    ''' Display active, inactive or all based on the user checking the radio box option
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        ResetForm()
        BindGroups()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

End Class
