﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ViewMSGStudentMessages.aspx.vb" Inherits="ViewMSGStudentMessages" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 


<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script type="text/javascript">
   </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>   
     
           
                    <%--LEFT PANE CONTENT HERE--%>

                      <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="listframetop">
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td nowrap align="center" width="100%">
                                            <asp:Label ID="lblShow" runat="server">Filter Not Applicable</asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="listframebottom">
                                <div class="scrollleftviewmsg">
                                    <table id="AutoNumber5" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="threecolumndatalist" style="border: #e0e0e0 1px solid; width: 25%; white-space: normal">
                                                <asp:Label ID="lblTerm1" CssClass="labelbold" runat="server">Message</asp:Label></td>
                                            <td class="threecolumndatalist" style="border-right: #e0e0e0 1px solid; border-top: #e0e0e0 1px solid;
                                                width: 30%; border-bottom: #e0e0e0 1px solid; white-space: normal">
                                                <asp:Label ID="lblCourse1" CssClass="labelbold" runat="server">Delivery Type</asp:Label></td>
                                            <td class="threecolumndatalist" style="border-right: #e0e0e0 1px solid; border-top: #e0e0e0 1px solid;
                                                width: 45%; border-bottom: #e0e0e0 1px solid; white-space: normal">
                                                <asp:Label ID="lblSection1" CssClass="labelbold" runat="server">Date</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:DataList ID="rptQueue" runat="server" GridLines="Both" repeatlayout="Table" Width="100%">
                                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td class="threecolumndatalist" style="width: 25%; padding: 3px">
                                                                    <asp:LinkButton ID="Linkbutton1" CssClass="itemstyle" CausesValidation="False" runat="server"
                                                                        CommandArgument='<%# Container.DataItem("MessageId")%>' Text='<%# Container.DataItem("TemplateDescrip")%>'>
                                                                    </asp:LinkButton></td>
                                                                <td class="threecolumndatalist" style="width: 30%; ; padding: 3pxl">
                                                                    <asp:LinkButton ID="Linkbutton2" CssClass="itemstyle" CausesValidation="False" runat="server"
                                                                        CommandArgument='<%# Container.DataItem("MessageId")%>'
                                                                        Text='<%# Container.DataItem("DeliveryType") %>'>
                                                                    </asp:LinkButton></td>
                                                                <td class="threecolumndatalist" style="width: 45%; ; padding: 3px">
                                                                    <asp:LinkButton ID="Linkbutton3" CssClass="itemstyle" CausesValidation="False" runat="server"
                                                                        CommandArgument='<%# Container.DataItem("MessageId")%>'
                                                                        Text='<%# Container.DataItem("CreatedDate") %>'>
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:DataList></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>

    </telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <!-- begin top menu (save,new,reset,delete,history)-->
            <tr>
                <td class="menuframe" align="right">
                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                        ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                            ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                        </asp:Button>
                        </td>
            </tr>
        </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2">
        <!-- begin content table-->

                   <%-- MAIN CONTENT HERE--%>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div id="fckdiv" style="height: expression(document.body.clientHeight - 80 + 'px')">

<telerik:RadEditor runat="server" ID="fck" ToolsFile="~/MSG/xml/RadEditorToolsFile.xml" Height="400px" Width="550px">
    <Modules>
        <telerik:EditorModule Name="RadEditorHtmlInspector" Enabled="true" Visible="false" />
        <telerik:EditorModule Name="RadEditorNodeInspector" Enabled="true" Visible="false" />
        <telerik:EditorModule Name="RadEditorDomInspector" Enabled="false" />
        <telerik:EditorModule Name="RadEditorStatistics" Enabled="false" />
    </Modules>
    <ImageManager ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <MediaManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <FlashManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <TemplateManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
    <DocumentManager  ViewPaths="~/MSG/UserFiles" UploadPaths="~/MSG/UserFiles" DeletePaths="~/MSG/UserFiles" />
</telerik:RadEditor>
 <script type="text/javascript">
     if (Telerik.Web.UI.Editor !== undefined) {
         Telerik.Web.UI.Editor.CommandList["FameTags"] = function (commandName, editor, args) {
             var elem = editor.getSelectedElement(); //returns the selected element.

             if (elem.tagName == "A") {
                 editor.selectElement(elem);
                 argument = elem;
             }
             else {
                 //remove links if present from the current selection - because of JS error thrown in IE
                 editor.fire("Unlink");

                 //remove Unlink command from the undo/redo list
                 var commandsManager = editor.get_commandsManager();
                 var commandIndex = commandsManager.getCommandsToUndo().length - 1;
                 commandsManager.removeCommandAt(commandIndex);

                 var content = editor.getSelectionHtml();

                 var link = editor.get_document().createElement("A");

                 link.innerHTML = content;
                 argument = link;
             }

             var myCallbackFunction = function (sender, args) {
                 editor.pasteHtml(String.format("{0}", args.innerHTML))
             }

             editor.showExternalDialog(
                  'SpecialFieldsPopupRadEditor.aspx?categoryName=' + args.get_value().replace('&amp;', '[amp]'),
                  argument,
                  370,
                  300,
                  myCallbackFunction,
                  null,
                  'Insert [Field]',
                  false,
                  Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
                  false,
                  true);
         };
     }
    </script>

                                </div>
                            </td>
                        </tr>
                    </table>
                    <asp:TextBox ID="txtMessageId" runat="server" Visible="False"></asp:TextBox>
                    <asp:HiddenField ID="txtConfirm" runat="server" />

        <!-- end content table-->
        </div>
        </td>
        </tr>
        </table>

    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

