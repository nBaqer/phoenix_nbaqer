<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Print.aspx.vb" Inherits="AdvantageApp_MSG_Print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Messages</title>
    <LINK href="../css/MSG.css" type="text/css" rel="stylesheet"/>
    <meta http-equiv="pragma" content="NO-CACHE"/>
	<base target="_self" />
	<script language="javascript" src="xp_progress.js"></script>
</head>
<body bgColor="#e9edf2">
    <form id="form1" runat="server">
    <div>
        <table>
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td align="center">
					<script type="text/javascript">
						// create the progress bar initially hidden
						var bar1 = createBar(300,15,'white',1,'black','blue',85,7,3,"");
						bar1.hideBar();
					</script>
				</td>
			</tr>
			<tr>
				<td align="center" class="Label"><asp:Label Runat="server" ID="lblHeader"></asp:Label>
				</td>
			</tr>
			<tr>
				<td align="center">
					<asp:Button id="btnPrint" runat="server"  Text="Print All" Width="100px"></asp:Button>&nbsp;
					<asp:Button id="btnCancel" runat="server"  Text="Cancel" Width="100px"></asp:Button>
				</td>
			</tr>
		</table>			
    </div>
    </form>
</body>
</html>
