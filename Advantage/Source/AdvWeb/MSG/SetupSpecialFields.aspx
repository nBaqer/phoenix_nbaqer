<%@ page title="" language="vb" MasterPageFile="~/NewSite.master" autoeventwireup="false" codefile="SetupSpecialFields.aspx.vb" inherits="MSG_SetupSpecialFields" %>
<%@ mastertype virtualpath="~/NewSite.master"%> 
<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlEntities">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlEntities" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlEntities" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlEntities" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlEntities" runat="server" DataKeyField="EntityId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="~/images/Inactive.gif" runat="server" Visible="False"
                                            CommandArgument='<%# Container.DataItem("EntityId")%>' CausesValidation="False" />
                                        <asp:ImageButton ID="imgActive" ImageUrl="~/images/Active.gif" runat="server" Visible="True"
                                            CommandArgument='<%# Container.DataItem("EntityId")%>' CausesValidation="False" />
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text="True" />
                                        <asp:LinkButton Text='<%# Container.DataItem("Descrip")%>' runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("EntityId")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false" Enabled="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="2" width="80%" align="center">
                                        <tr>
                                            <td align="center" style="padding-bottom: 16px">
                                                <font class="Label"><i>Choose which special fields can be displayed for templates belonging
                                                        to the specified entity.</i></font>
                                            </td>
                                        </tr>
                                        <tr valign="middle">
                                            <td align="left" colspan="3">
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                    <tr>
                                                        <td class="threecolumnheader1">
                                                            <asp:Label ID="lblAvailFieldGroups" CssClass="label" runat="server" Font-Bold="True">Available Field Groups</asp:Label></td>
                                                        <td class="threecolumnspacerreg"></td>
                                                        <td class="threecolumnheader1">
                                                            <asp:Label ID="lblFieldGroups" CssClass="label" runat="server" Font-Bold="True">Field Groups</asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="threecolumncontentreg" style="text-align: center">
                                                            <asp:ListBox ID="lbSysFieldGroups" runat="server" CssClass="listboxesheight"></asp:ListBox></td>
                                                        <td class="threecolumnbuttonsreg" align="center" style="margin-top:-25px;">
                                                            <div style="margin: 10px;">
                                                                <telerik:RadButton ID="btnAddFieldGroup" runat="server" Text="Add -->" Width="100px" />

                                                            </div>
                                                            <div style="margin:10px;">
                                                                <telerik:RadButton ID="btnDelFieldGroup" runat="server" Text="<-- Remove" Width="100px" />

                                                            </div>
                                                        </td>
                                                        <td class="threecolumncontentreg" style="text-align: center">
                                                            <asp:ListBox ID="lbFieldGroups" runat="server" CssClass="listboxesheight"></asp:ListBox></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:HiddenField ID="hfResult" runat="server" />
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>

</asp:Content>


