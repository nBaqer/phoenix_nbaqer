﻿Imports System.Diagnostics
Imports FAME.Advantage.Common

Partial Class PageNotFound
    Inherits BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ErrMessage As String = ""
        lblApplicationHeaderVersion.Text = "Advantage - " & GetBuildVersion()
        Dim ex As New System.Exception
        ' Send Email option is enabled when the client Mode is set to Yes and when the Error email from and To address  are set in manage configurations
        If UCase(ConfigurationManager.AppSettings("ClientMode").ToString) = "YES" Then
            lblErrorheader.Visible = False
            lblerror.Visible = False

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            If Not MyAdvAppSettings.AppSettings("ErrorEmailToAddress") Is Nothing And Not MyAdvAppSettings.AppSettings("ErrorEmailFromAddress") Is Nothing Then
                If MyAdvAppSettings.AppSettings("ErrorEmailToAddress").ToString = String.Empty Or MyAdvAppSettings.AppSettings("ErrorEmailFromAddress").ToString = String.Empty Then
                    btnSendEmail.Visible = False
                Else
                    btnSendEmail.Visible = True
                End If

            Else
                btnSendEmail.Visible = False
            End If
        Else
            lblErrorheader.Visible = True
            lblerror.Visible = True
            btnSendEmail.Visible = False
        End If

        ''Application ErrorCode is saved in Global.asax Application error
        If Not Application("ErrorCode") Is Nothing Then
            If Application("ErrorCode").ToString = "404" Then
                lblerror.Text = "Error 404:Page Not available"
                lblErrorReason.Text = "The resource you are looking for has been removed, had its name changed, or is temporarily unavailable"
            ElseIf Application("ErrorCode").ToString = "403" Then
                lblerror.Text = "Error 403:Forbidden"
                lblErrorReason.Text = "Error 403:Forbidden"
            ElseIf Application("ErrorCode").ToString = "500" Then
                lblerror.Text = "Error 500:Internal Server Error"
                lblErrorReason.Text = "Error 500:Internal Server Error"
            Else
                lblErrorReason.Text = ""
                lblerror.Text = "Error on page"
            End If

        Else
            lblerror.Text = "Unhandled error"

        End If

        If Not Application("ErrorPage") Is Nothing Then
            lblerror.Text = "Error on page:" + Application("ErrorPage").ToString + "<br/>" + "*** The Error is : " + lblerror.Text
        End If
        ''Get the previous url to go back to that page
        If Not Page.IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                hdnpreviousURL.Value = Request.UrlReferrer.ToString()
            End If
        End If

    End Sub
    Protected Sub lnkLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogin.Click
        Session.Abandon()
        Response.Redirect(FormsAuthentication.LoginUrl, True)
    End Sub
    Private Function GetBuildVersion() As String
        Dim VersionString As String = String.Empty
        Try
            'Dim MyAssemblyName As AssemblyName = AssemblyName.GetAssemblyName(Server.MapPath("~/Bin/Common.dll"))
            'VersionString = MyAssemblyName.Version.ToString()
            Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Server.MapPath("~/Bin/Common.dll"))
            VersionString = myFileVersionInfo.FileVersion.ToString()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            VersionString = String.Empty
        End Try
        Return VersionString
    End Function

    Protected Sub lnkPreviousPage_Click(sender As Object, e As System.EventArgs) Handles lnkPreviousPage.Click
        If hdnpreviousURL.Value.Contains("ErrorPage.aspx") Or hdnpreviousURL.Value.Contains("PageNotFound.aspx") Then
            Session.Abandon()
            Response.Redirect(FormsAuthentication.LoginUrl, True)
        Else
            Response.Redirect(hdnpreviousURL.Value.ToString)
        End If

    End Sub

    Protected Sub btnSendEmail_Click(sender As Object, e As System.EventArgs) Handles btnSendEmail.Click

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim MessageBody As New StringBuilder
        With MessageBody
            .AppendLine("Customer Support,")
            .AppendLine()
            .AppendLine(" There is an error in Advantage:" & GetBuildVersion())
            .AppendLine()
            If Not Application("ErrorPage") Is Nothing Then
                .AppendLine("Error Page URL: " & Application("ErrorPage").ToString)
            End If
            If Not MyAdvAppSettings.AppSettings("SchoolName") Is Nothing Then
                .AppendLine("School Name: " & MyAdvAppSettings.AppSettings("SchoolName").ToString)
            End If
            If Not AdvantageSession.UserName Is Nothing Then
                .AppendLine("Logged In user name: " & AdvantageSession.UserName.ToString)
                .AppendLine("Logged In Campus : " & AdvantageSession.UserState.CampusId.ToString)
            End If

            .AppendLine()
            .AppendLine(lblerror.Text)
            .AppendLine()
            .AppendLine("Thank You")
            .AppendLine()
        End With

        Dim FromAddress As String
        Dim ToAddress As String

        If Not MyAdvAppSettings.AppSettings("ErrorEmailFromAddress") Is Nothing Then
            FromAddress = MyAdvAppSettings.AppSettings("ErrorEmailFromAddress")
        Else
            FromAddress = "Donotreply@fameinc.com"

        End If
        If Not MyAdvAppSettings.AppSettings("ErrorEmailToAddress") Is Nothing Then
            ToAddress = MyAdvAppSettings.AppSettings("ErrorEmailToAddress")
        Else
            ToAddress = "AdvantageHelp@fameinc.com"

        End If

        BusinessLogicUtilities.Utilities.SendEmailregardingErrorPage(FromAddress, ToAddress, "Advantage Error - " & GetBuildVersion(), MessageBody.ToString, "")

        Application("ErrorPage") = Nothing
        Application("ErrorCode") = Nothing
        DisplayNotificationMessage("Notification was sent to Advantage Customer Support." + vbCrLf + "Contact Support for further information.")
    End Sub
    Private Sub DisplayNotificationMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
End Class
