﻿
<%--<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>--%>
<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EmployeeContactInfo.aspx.vb" Inherits="EmployeeContactInfo" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<tr>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
					    <table cellpadding="0" cellspacing="0" class="maincontenttable" style="width: 98%; border: none;">
					        <tr>
					            <td class="detailsframe">
					                <div class="boxContainer">
					                    <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <asp:panel id="pnlRHS" Runat="server">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="40%" align="center">
                                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                                <asp:DropDownList ID="ddlEmpId" runat="server" Visible="False">
                                                </asp:DropDownList>
                                                <asp:CheckBox ID="cbxIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                                <asp:TextBox ID="txtEmpContactInfoId" runat="server" Visible="false" DESIGNTIMEDRAGDROP="307"
                                                    MaxLength="128"></asp:TextBox>
                                                <asp:TextBox ID="txtEmpId" runat="server" Visible="False"></asp:TextBox>
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap" colspan="6" style="border-top: 0; border-left: 0; border-right: 0">
                                                       <span class="label" style="font-weight: bold">Phones</span> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell" style="padding-top: 16px">
                                                        <asp:Label ID="lblWorkPhone" CssClass="label" runat="server">Work</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" style="padding-top: 16px">
                                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 60%">
                                                                    <%--<ew:MaskedTextBox ID="txtWorkPhone" TabIndex="2" runat="server" CssClass="textbox">
                                                                    </ew:MaskedTextBox>--%>
                                                                    <telerik:RadMaskedTextBox ID="txtWorkPhone" TabIndex="2" runat="server" CssClass="textbox" Width="200px" 
                                                                        DisplayFormatPosition="Left" DisplayPromptChar="">
                                                                     </telerik:RadMaskedTextBox>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:CheckBox ID="chkForeignWorkPhone" TabIndex="1" CssClass="checkboxinternational"
                                                                        Text="International" runat="server" AutoPostBack="true"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblPassword" CssClass="label" runat="server">Home</asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 60%">
                                                                 <%--   <ew:MaskedTextBox ID="txtHomePhone" TabIndex="4" runat="server" CssClass="textbox">
                                                                    </ew:MaskedTextBox>--%>
                                                                     <telerik:RadMaskedTextBox ID="txtHomePhone" TabIndex="4" runat="server" CssClass="textbox" Width="200px" 
                                                                        DisplayFormatPosition="Left" DisplayPromptChar="">
                                                                     </telerik:RadMaskedTextBox>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:CheckBox ID="chkForeignHomePhone" TabIndex="3" CssClass="CheckboxInternational"
                                                                        Text="International" runat="server" AutoPostBack="true"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblCell" runat="server" CssClass="label">Cell</asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 60%">
                                                                   <%-- <ew:MaskedTextBox ID="txtCellPhone" TabIndex="6" runat="server" CssClass="textbox">
                                                                    </ew:MaskedTextBox>--%>
                                                                     <telerik:RadMaskedTextBox ID="txtCellPhone" TabIndex="6" runat="server" CssClass="textbox" Width="200px" 
                                                                        DisplayFormatPosition="Left" DisplayPromptChar="">
                                                                     </telerik:RadMaskedTextBox>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:CheckBox ID="chkForeignCellPhone" TabIndex="5" CssClass="checkboxinternational"
                                                                        Text="International" runat="server" AutoPostBack="true"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell" style="padding-bottom: 16px">
                                                        <asp:Label ID="lblBeeper" runat="server" CssClass="label">Beeper</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" style="padding-bottom: 16px">
                                                        <asp:TextBox ID="txtBeeper" TabIndex="7" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" Enabled="False"
                                                            ControlToValidate="txtBeeper" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}(\sx\d(\d)?(\d)?(\d)?(\d)?)?"
                                                            ErrorMessage="Invalid Beeper Number" Display="None"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap" colspan="6" style="border-top: 0; border-left: 0; border-right: 0">
                                                        <span class="label" style="font-weight: bold">E-mails</span> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell" style="padding-top: 16px">
                                                        <asp:Label ID="lblWork" runat="server" CssClass="label">Work</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" style="padding-top: 16px">
                                                        <asp:TextBox ID="txtWorkEmail" TabIndex="8" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtWorkEmail"
                                                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid Work Email Address"
                                                            Display="None"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblHome" runat="server" CssClass="label">Home</asp:Label>
                                                    </td>
                                                    <td class="contentcell4">
                                                        <asp:TextBox ID="txtHomeEmail" TabIndex="9" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtHomeEmail"
                                                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid Home Email Address"
                                                            Display="None"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                            </table>
									</asp:panel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
					<!-- end rightcolumn --></tr>
			</table>
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

