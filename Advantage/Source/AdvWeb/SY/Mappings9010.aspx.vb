﻿Imports System.Data
Imports System.IO
Imports System.Web
Imports System.Collections.Generic
Imports Telerik.Web.UI.com.hisoftware.api2
Imports NHibernate.Linq
Imports Telerik.Charting.Styles
Imports Telerik.Web.UI.Widgets
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.BusinessFacade
Imports Ionic.Zip
Imports System.Data.OleDb
Imports FAME.Advantage.Common
Imports FAME.Parameters.Info
Imports Telerik.Web.UI

Partial Class Mappings9010
    Inherits BasePage
    Dim strRootFolderName As String
    Dim datefile As String = Guid.NewGuid().ToString()

    Protected MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            If Not Page.IsPostBack Then
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            DisplayRadNotification("Error in Page Load event", ex.Message)
        End Try

        headerTitle.Text = Header.Title
    End Sub
End Class