﻿Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports System.Collections
Imports System.Collections.Generic
Imports Advantage.Business.Logic.Layer
Imports FAME.Advantage.Common

Partial Class EmployeeEmergencyContacts
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblName As Label
    Protected WithEvents btnhistory As Button
    Protected WithEvents CampusGroupCompareValidator As CompareValidator
    Protected WithEvents RegularExpressionValidator3 As RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator4 As RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator5 As RegularExpressionValidator
    Protected WithEvents cCampusGroupCompareValidator As CompareValidator

    ' This variable holds empId. The value assigned is for testing only
    Protected empId As String '= "EC633761-20D9-4A9E-AA18-3486B8F42AA8"
    Protected state As AdvantageSessionState
    Protected strVID As String

#End Region




    Private pObj As New UserPagePermissionInfo

    Protected resourceId As Integer
    Protected moduleid As String
    Protected sdfcontrols As New SDFComponent
    Private campusId As String
    Protected userId As String

    Private mruProvider As MRURoutines
    Protected EmployeeId As String
    Protected boolSwitchCampus As Boolean = False
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function GetEmployeeFromStateObject() As EmployeeMRU

        Dim objStateInfo As New AdvantageStateInfo
        Dim objEmployeeState As New EmployeeMRU


        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        MyBase.GlobalSearchHandler(3)

        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            EmployeeId = Guid.Empty.ToString()
        Else
            EmployeeId = AdvantageSession.MasterEmployeeId
        End If


        txtEmpId.Text = EmployeeId


        Dim objGetStudentStatusBar As AdvantageStateInfo

        mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(AdvantageSession.MasterEmployeeId)
        With objGetStudentStatusBar
            AdvantageSession.MasterName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmployeeName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmployeeAddress1 = objGetStudentStatusBar.Address1
            AdvantageSession.MasterEmployeeAddress2 = objGetStudentStatusBar.Address2
            AdvantageSession.MasterEmployeeCity = objGetStudentStatusBar.City
            AdvantageSession.MasterEmployeeState = objGetStudentStatusBar.State
            AdvantageSession.MasterEmployeeZip = objGetStudentStatusBar.Zip

        End With

        If Not String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            Master.ShowHideStatusBarControl(True)
            Master.PageObjectId = EmployeeId
            Master.PageResourceId = Request.QueryString("resid")
            Master.setHiddenControlForAudit()
        Else
            Master.ShowHideStatusBarControl(False)
        End If

        With objEmployeeState
            .EmployeeId = New Guid(EmployeeId)
            .Name = objStateInfo.NameValue
        End With

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            Return Nothing
        Else
            Return objEmployeeState
        End If


    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        'Dim strVID As String
        'Dim state As AdvantageSessionState
        '   Dim m_Context As HttpContext

        Dim advantageUserState As User = AdvantageSession.UserState

        campusId = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'Get the StudentId from the state object associated with this page
        Dim objEmployeeState As EmployeeMRU
        objEmployeeState = getEmployeeFromStateObject() 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objEmployeeState Is Nothing Then
            MyBase.RedirectToEmployeeSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objEmployeeState
            empId = .EmployeeId.ToString

        End With


        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(3, AdvantageSession.MasterName)
            End If
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            'objCommon.SetCaptionsAndColorRequiredFields(Form1)

            '   bind datalist
            BindDataList(empId)

            '   bind an empty new EmployeeEmergencyContactInfo
            BindEmployeeEmergencyContactData(New EmployeeEmergencyContactInfo(empId))

            '   initialize buttons
            InitButtonsForLoad()

            '   disable History button the first time
            'Header1.EnableHistoryButton(False)
            MyBase.uSearchEntityControlId.Value = ""
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
        End If

        GetInputMaskValue()

        moduleid = HttpContext.Current.Request.Params("Mod").ToString
        'resourceId = CInt(HttpContext.Current.Request.Params("resid"))


        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(resourceId, moduleid)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtEmployeeEmergencyContactName.Text) <> "" Then
            sdfcontrols.GenerateControlsEditSingleCell(pnlSDF, resourceId, txtEmployeeEmergencyContactId.Text, moduleid)
        Else
            sdfcontrols.GenerateControlsNewSingleCell(pnlSDF, resourceId, moduleid)
        End If

    End Sub
    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        '     Dim correctFormat As Boolean
        Dim strMask As String
        Dim zipMask As String
        '   Dim errorMessage As String
        Dim strHomePhoneReq As String
        Dim strCellPhoneReq As String
        Dim strWorkPhoneReq As String
        Dim strZipReq As String
        '  Dim strFaxReq As String
        Dim objCommon As New CommonUtilities

        'Get The Input Mask for Phone/Fax and Zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        PhoneMask(chkForeignCellPhone.Checked, txtCellPhone)
        PhoneMask(chkForeignHomePhone.Checked, txtHomePhone)
        PhoneMask(chkForeignWorkPhone.Checked, txtWorkPhone)

        'Get The RequiredField Value
        strHomePhoneReq = objCommon.SetRequiredColorMask("HomePhone")
        strCellPhoneReq = objCommon.SetRequiredColorMask("CellPhone")
        strWorkPhoneReq = objCommon.SetRequiredColorMask("WorkPhone")

        strZipReq = objCommon.SetRequiredColorMask("Zip")

    End Sub
    Private Sub BindDataList(ByVal empId As String)

        '   bind EmployeeEmergencyContacts datalist
        With New EmployeesFacade
            dlstEmployeeEmergencyContacts.DataSource = .GetAllEmployeeEmergencyContacts(empId, radStatus.SelectedIndex)
            dlstEmployeeEmergencyContacts.DataBind()
        End With
    End Sub
    Private Sub BuildDropDownLists()
        'BuildStatusDDL()
        'BuildRelationDDL()

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, False, True))

        'Relations DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlRelation, AdvantageDropDownListName.Relations, Nothing, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub
    'Private Sub BuildStatusDDL()
    '    '   bind the status DDL
    '    Dim statuses As New StatusesFacade

    '    With ddlStatusId
    '        .DataTextField = "Status"
    '        .DataValueField = "StatusId"
    '        .DataSource = statuses.GetAllStatuses()
    '        .DataBind()
    '    End With

    'End Sub
    'Private Sub BuildRelationDDL()
    '    Dim names() As String = System.Enum.GetNames(GetType(Relationship))
    '    Dim values() As Integer = System.Enum.GetValues(GetType(Relationship))

    '    For i As Integer = 0 To names.Length - 1
    '        ddlRelation.Items.Add(New ListItem(names(i), values(i)))
    '    Next

    '    '   set Select to be the default relationship
    '    ddlRelation.Items.Insert(0, New ListItem("Select", 0))
    '    ddlRelation.SelectedIndex = 0
    'End Sub
    Private Sub dlstEmployeeEmergencyContacts_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstEmployeeEmergencyContacts.ItemCommand
        '   get the EmployeeEmergencyContactId from the backend and display it
        Dim strId As String = dlstEmployeeEmergencyContacts.DataKeys(e.Item.ItemIndex).ToString()

        GetEmployeeEmergencyContactId(strId)

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = resourceId
        Master.setHiddenControlForAudit()
        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployeeEmergencyContacts, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEditSingleCell(pnlSDF, resourceId, txtEmployeeEmergencyContactId.Text, moduleid)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CommonWebUtilities.RestoreItemValues(dlstEmployeeEmergencyContacts, strId)
    End Sub
    Private Sub BindEmployeeEmergencyContactData(ByVal EmployeeEmergencyContact As EmployeeEmergencyContactInfo)
        With EmployeeEmergencyContact
            txtEmpId.Text = .EmpId
            chkIsInDB.Checked = .IsInDB
            txtEmployeeEmergencyContactId.Text = .EmployeeEmergencyContactId
            txtEmployeeEmergencyContactName.Text = .Name

            Dim facInputMasks As New InputMasksFacade
            Dim strMask As String
            Dim zipMask As String

            'Get the mask for phone numbers and zip
            strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
            zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)


            If .ForeignHomePhone = True Then
                chkForeignHomePhone.Checked = True
            Else
                chkForeignHomePhone.Checked = False
            End If
            If .ForeignCellPhone = True Then
                chkForeignCellPhone.Checked = True
            Else
                chkForeignCellPhone.Checked = False
            End If
            If .ForeignWorkPhone = True Then
                chkForeignWorkPhone.Checked = True
            Else
                chkForeignWorkPhone.Checked = False
            End If

            If Not (EmployeeEmergencyContact.StatusId = Guid.Empty.ToString) Then
                'ddlStatusId.SelectedValue = EmployeeEmergencyContact.StatusId
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .StatusId, .Status)
            Else
                ddlStatusId.SelectedIndex = 0
            End If

            'ddlRelation.SelectedValue = .RelationId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRelation, .RelationId, .Relation)

            ''Get HomePhone
            txtHomePhone.Text = .HomePhone
            'If txtHomePhone.Text <> "" And chkForeignHomePhone.Checked = False Then
            '    txtHomePhone.Text = facInputMasks.ApplyMask(strMask, txtHomePhone.Text)
            'End If


            ''Get WorkPhone
            txtWorkPhone.Text = .WorkPhone
            'If txtWorkPhone.Text <> "" And chkForeignWorkPhone.Checked = False Then
            '    txtWorkPhone.Text = facInputMasks.ApplyMask(strMask, txtWorkPhone.Text)
            'End If


            ''Get CellPhone
            txtCellPhone.Text = .CellPhone
            'If txtCellPhone.Text <> "" And chkForeignCellPhone.Checked = False Then
            '    txtCellPhone.Text = facInputMasks.ApplyMask(strMask, txtCellPhone.Text)
            'End If

            PhoneMask(chkForeignCellPhone.Checked, txtCellPhone)
            PhoneMask(chkForeignHomePhone.Checked, txtHomePhone)
            PhoneMask(chkForeignWorkPhone.Checked, txtWorkPhone)


            txtBeeper.Text = .Beeper
            txtWorkEmail.Text = .WorkEmail
            txtHomeEmail.Text = .HomeEmail
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString


        End With
    End Sub
    'Private Function ValidateFieldsWithInputMasks() As String
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim correctFormat, correctFormat1, correctFormat2 As Boolean ' , correctFormat3
    '    Dim strMask As String
    '    Dim zipMask As String
    '    Dim errorMessage As String

    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

    '    'Validate the phone field format. If the field is empty we should not apply the mask
    '    'agaist it.
    '    errorMessage = ""

    '    If txtWorkPhone.Text <> "" And chkForeignWorkPhone.Checked = False Then
    '        correctFormat1 = facInputMasks.ValidateStringWithInputMask(strMask, txtWorkPhone.Text)
    '        If correctFormat1 = False Then
    '            errorMessage &= "Incorrect format for work phone field." & vbLf
    '        End If
    '    End If


    '    If txtHomePhone.Text <> "" And chkForeignHomePhone.Checked = False Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtHomePhone.Text)
    '        If correctFormat = False Then
    '            errorMessage &= "Incorrect format for home phone field." & vbLf
    '        End If
    '    End If


    '    If txtCellPhone.Text <> "" And chkForeignCellPhone.Checked = False Then
    '        correctFormat2 = facInputMasks.ValidateStringWithInputMask(strMask, txtCellPhone.Text)
    '        If correctFormat2 = False Then
    '            errorMessage &= "Incorrect format for cell phone field." & vbLf
    '        End If
    '    End If


    '    Return Trim(errorMessage)

    'End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        '   update EmployeeEmergencyContact Info 
        Dim errorMessage As String

        'Check If Mask is successful only if Foreign Is not checked
        'If chkForeignHomePhone.Checked = False Or chkForeignCellPhone.Checked = False Or chkForeignWorkPhone.Checked = False Then
        '    errorMessage = ValidateFieldsWithInputMasks()
        'Else
        '    errorMessage = ""
        'End If
        errorMessage = ""
        If errorMessage = "" Then
            Dim result As String = (New EmployeesFacade).UpdateEmployeeEmergencyContactInfo(BuildEmployeeEmergencyContactInfo(empId), Session("UserName"))

            '   bind datalist
            BindDataList(empId)

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployeeEmergencyContacts, txtEmployeeEmergencyContactId.Text, ViewState, Header1)

            If Not result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error1", result, "Save Error")
            Else
                '   get the EmployeeEmergencyContactId from the backend and display it
                GetEmployeeEmergencyContactId(txtEmployeeEmergencyContactId.Text)
            End If

            '   if there are no errors bind a new entity and init buttons
            If Page.IsValid Then

                '   set the property IsInDB to true in order to avoid an error if the user
                '   hits "save" twice after adding a record.
                chkIsInDB.Checked = True

                'note: in order to display a new page after "save".. uncomment next lines
                '   bind an empty new EmployeeEmergencyContactInfo
                'BindEmployeeEmergencyContactData(New EmployeeEmergencyContactInfo)

                '   initialize buttons
                'InitButtonsForLoad()
                InitButtonsForEdit()


                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields When Save Button Is Clicked

                Dim SDFID As ArrayList
                Dim SDFIDValue As ArrayList
                '  Dim newArr As ArrayList
                Dim z As Integer
                Dim SDFControl As New SDFComponent
                Try
                    SDFControl.DeleteSDFValue(txtEmployeeEmergencyContactId.Text)
                    SDFID = SDFControl.GetAllLabels(pnlSDF)
                    SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                    For z = 0 To SDFID.Count - 1
                        SDFControl.InsertValues(txtEmployeeEmergencyContactId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                    Next
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try

                'SchoolDefined Fields Code Ends Here 
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If
        Else
            DisplayErrorMessageMask(errorMessage)
        End If
        CommonWebUtilities.RestoreItemValues(dlstEmployeeEmergencyContacts, txtEmployeeEmergencyContactId.Text)
    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Function BuildEmployeeEmergencyContactInfo(ByVal empId As String) As EmployeeEmergencyContactInfo
        'instantiate class
        Dim EmployeeEmergencyContactInfo As New EmployeeEmergencyContactInfo(empId)
        'Dim facInputMask As New InputMasksFacade
        'Dim phoneMask As String
        '  Dim zipMask As String



        With EmployeeEmergencyContactInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get EmployeeEmergencyContactId
            .EmployeeEmergencyContactId = txtEmployeeEmergencyContactId.Text

            'get Name
            .Name = txtEmployeeEmergencyContactName.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get EmpId
            .EmpId = txtEmpId.Text

            'get RelationId
            .RelationId = ddlRelation.SelectedValue

            ' phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
            'If txtHomePhone.Text <> "" And chkForeignHomePhone.Checked = False Then
            '    .HomePhone = facInputMask.RemoveMask(phoneMask, txtHomePhone.Text)
            'Else
            '    .HomePhone = txtHomePhone.Text
            'End If


            ''Get WorkPhone
            'If txtWorkPhone.Text <> "" And chkForeignWorkPhone.Checked = False Then
            '    .WorkPhone = facInputMask.RemoveMask(phoneMask, txtWorkPhone.Text)
            'Else
            '    .WorkPhone = txtWorkPhone.Text
            'End If

            ''Get CellPhone
            'If txtCellPhone.Text <> "" And chkForeignCellPhone.Checked = False Then
            '    .CellPhone = facInputMask.RemoveMask(phoneMask, txtCellPhone.Text)
            'Else
            '    .CellPhone = txtCellPhone.Text
            'End If

            .WorkPhone = txtWorkPhone.Text
            .HomePhone = txtHomePhone.Text
            .CellPhone = txtCellPhone.Text
            '   get Beeper
            .Beeper = txtBeeper.Text.Trim

            '   get WorkEmail
            .WorkEmail = txtWorkEmail.Text.Trim

            '   get HomeEmail
            .HomeEmail = txtHomeEmail.Text.Trim

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

            If chkForeignHomePhone.Checked = True Then
                .ForeignHomePhone = 1
            Else
                .ForeignHomePhone = 0
            End If

            If chkForeignWorkPhone.Checked = True Then
                .ForeignWorkPhone = 1
            Else
                .ForeignWorkPhone = 0
            End If

            If chkForeignCellPhone.Checked = True Then
                .ForeignCellPhone = 1
            Else
                .ForeignCellPhone = 0
            End If
        End With
        'return data
        Return EmployeeEmergencyContactInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        '   bind an empty new EmployeeEmergencyContactInfo
        BindEmployeeEmergencyContactData(New EmployeeEmergencyContactInfo(empId))

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployeeEmergencyContacts, Guid.Empty.ToString, ViewState, Header1)

        'initialize buttons
        InitButtonsForLoad()

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, moduleid)
        CommonWebUtilities.RestoreItemValues(dlstEmployeeEmergencyContacts, Guid.Empty.ToString)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (txtEmployeeEmergencyContactId.Text = Guid.Empty.ToString) Then
            'instantiate component
            Dim saf As New EmployeesFacade
            'update EmployeeEmergencyContact Info 
            Dim result As String = saf.DeleteEmployeeEmergencyContactInfo(txtEmployeeEmergencyContactId.Text, Date.Parse(txtModDate.Text))
            If result <> "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error2", result, "Save Error")
            Else

                '   bind the datalist
                BindDataList(empId)

                '   bind an empty new EmployeeEmergencyContactInfo
                BindEmployeeEmergencyContactData(New EmployeeEmergencyContactInfo(empId))

                '   initialize buttons
                InitButtonsForLoad()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtEmployeeEmergencyContactId.Text)
                SDFControl.GenerateControlsNewSingleCell(pnlSDF, resourceId, moduleid)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If
        End If
        CommonWebUtilities.RestoreItemValues(dlstEmployeeEmergencyContacts, Guid.Empty.ToString)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub GetEmployeeEmergencyContactId(ByVal EmployeeEmergencyContactId As String)
        '   Create a StudentsFacade Instance
        Dim saf As New EmployeesFacade

        '   bind EmployeeEmergencyContact properties
        BindEmployeeEmergencyContactData(saf.GetEmployeeEmergencyContactInfo(EmployeeEmergencyContactId))
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Set error condition
    '    Customvalidator1.ErrorMessage = errorMessage
    '    Customvalidator1.IsValid = False

    '    If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If
    'End Sub
    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList(empId)

        BindEmployeeEmergencyContactData(New EmployeeEmergencyContactInfo(empId))
        InitButtonsForLoad()
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(chkForeignCellPhone)
        controlsToIgnore.Add(chkForeignWorkPhone)
        controlsToIgnore.Add(chkForeignHomePhone)

        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Private Sub chkForeignCellPhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignCellPhone.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        PhoneMask(chkForeignCellPhone.Checked, txtCellPhone)
        CommonWebUtilities.SetFocus(Me.Page, txtCellPhone)
    End Sub

    Private Sub chkForeignHomePhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignHomePhone.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        PhoneMask(chkForeignHomePhone.Checked, txtHomePhone)
        CommonWebUtilities.SetFocus(Me.Page, txtHomePhone)
    End Sub

    Private Sub chkForeignWorkPhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignWorkPhone.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        PhoneMask(chkForeignWorkPhone.Checked, txtWorkPhone)
        CommonWebUtilities.SetFocus(Me.Page, txtWorkPhone)
    End Sub


    Private Sub PhoneMask(ByVal bchecked As Boolean, ByVal txtPhone As RadMaskedTextBox)
        If bchecked = False Then
            txtPhone.Mask = "(###)-###-####"
            txtPhone.DisplayMask = "(###)-###-####"
            txtPhone.DisplayPromptChar = ""
        Else
            txtPhone.Text = ""
            txtPhone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtPhone.DisplayMask = ""
            txtPhone.DisplayPromptChar = ""
        End If
    End Sub
End Class
