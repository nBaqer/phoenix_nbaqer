﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports BL = Advantage.Business.Logic.Layer
Imports FAME.Advantage.Common

Partial Class EmployeeSearch
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblStudentID As System.Web.UI.WebControls.Label
    Protected WithEvents txtStudentID As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEnrollment As System.Web.UI.WebControls.Label
    Protected WithEvents txtEnrollment As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCampGrpID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCampGrpID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents dgrdStudentSearch As System.Web.UI.WebControls.DataGrid
    Protected WithEvents StudentLNameLinkButton As System.Web.UI.WebControls.LinkButton
    Protected state As AdvantageSessionState

    Private mruProvider As BL.MRURoutines

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


        mruProvider = New BL.MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))

        'load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)

        'reset header information and save state
        'ResetHeaderInfoAndSaveState()

        'save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'Call mru component for employees
        'Dim objMRUFac As New MRUFacade
        'Dim ds As New DataSet

        'ds = objMRUFac.LoadMRU("Employees", Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))

        ''load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        'state("MRUDS") = ds

        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
    End Sub

#End Region

    Private campusId As String
    Private pObj As New UserPagePermissionInfo
    Protected resourceId As Integer
    Protected userId As String
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dim resId As Integer = 0
        'Try
        '    resId = CInt(HttpUtility.ParseQueryString(Request.UrlReferrer.Query())("resid"))
        'Catch ex As Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    resId = 0
        'End Try
        'If Request.QueryString.ToString.Contains("&mruemps") AndAlso resId > 0 AndAlso Not resId = 309 Then 'Happens only when user switches campus and page is either not part of new campus/insufficient permission
        '   CampusObjects.ShowNotificationWhileSwitchingCampus(9, "")
        'End If

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Me.Form.DefaultButton = btnSearch.UniqueID
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusid = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

      If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            Session("StudentID") = Nothing
            Session("EmployerID") = Nothing
            BuildStatusDDL()
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state("empInfo") = ""
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
            'BuildCampusGroupsDDL()
        End If
        DisableAllButtons()


    End Sub
    Private Sub DisableAllButtons()
        btnNew.Enabled = False
        btnDelete.Enabled = False
        btnSave.Enabled = False

    End Sub
    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub BuildCampusGroupsDDL()
    '    'Bind the CampusGroups DrowDownList
    '    Dim campusGroups As New CampusGroupsFacade
    '    With ddlCampGrpId
    '        .DataTextField = "CampGrpDescrip"
    '        .DataValueField = "CampGrpId"
    '        .DataSource = campusGroups.GetAllCampusGroups()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BindDataList()
        'Bind The First,Middle,LastName based on EmployerContactId
        Dim SearchFacade As New EmployeeSearchFacade
        dgrdTransactionSearch.DataSource = SearchFacade.EmployeeSearchResults(txtEmpId.Text, txtLastName.Text, txtFirstName.Text, txtSSN.Text, ddlStatusId.SelectedValue, campusId)
        dgrdTransactionSearch.DataBind()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtLastName.Text = "" And txtFirstName.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And txtSSN.Text = "" Then
            'DisplayErrorMessage("Please enter a value to search")
            DisplayRADAlert(CallbackType.Postback, "Error1", "Please enter a value to search", "Save Error")
            Exit Sub
        End If
        dgrdTransactionSearch.Visible = True
        BindDataList()
    End Sub
    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        '   define a new GUID
        Dim strVID As String = Guid.NewGuid.ToString
        Dim defaultCampusId As String
        Dim fac As New UserSecurityFacade
        Dim arrUPP As New ArrayList
        Dim pURL As String

        defaultCampusId = HttpContext.Current.Request.Params("cmpid")

        If e.CommandName = "StudentSearch" Then
            Dim intSchoolOption As Integer = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))
            '   save employeeId in the session so that it can be used by the employeeInfo program
            'state("empId") = e.CommandArgument.ToString
            '   redirect to studentLedger program
            Session("SEARCH") = 1


            strVID = BuildEmployeeStateObject(e.CommandArgument.ToString)

            AdvantageSession.MasterEmployeeId = e.CommandArgument.ToString()

            Dim strCampusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString()
            arrUPP = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 192, strCampusId, 396, intSchoolOption)
            If arrUPP.Count = 0 Then
                'User does not have permission to any resource for this submodule
                'Session("Error") = "You do not have permission to any of the pages for existing student<br> for the campus that you are logged in to."
                'Response.Redirect("../ErrorPage.aspx")
                RadNotification1.Show()
                RadNotification1.Text = "You do not have permission to access any of the existing employee pages in the current campus"
            ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 52) Then
                AdvantageSession.EmployeeMRU = Nothing
                Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                Dim strVSI As String = R.Next()
                mruProvider.InsertMRU(3, txtEmployeeId.Text, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                Response.Redirect("../SY/EmployeeInfo.aspx?resid=52&mod=HR&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=3" + "&VSI=" + strVSI, True)
            Else
                'redirect to the first page that the user has permission to for the submodule
                pURL = BuildPartialURL(arrUPP(0))
                mruProvider.InsertMRU(3, txtEmployeeId.Text, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                Response.Redirect(pURL & "&mod=HR&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=3", True)
            End If
        End If

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnDisable()
        btnNew.Enabled = False
        btnSave.Enabled = False
        btnDelete.Enabled = False
        btnhistory.Enabled = False
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'save current State
        '  CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'set btnSearch as default click
        'CommonWebUtilities.SetButtonAsDefaultClick(btnSearch)
        'BIndToolTip()
    End Sub
    Private Sub ResetHeaderInfoAndSaveState()

        '   save information in state
        state("empFirstName") = Nothing
        state("empLastName") = Nothing

        '   set values for the header
        Session("NameCaption") = Nothing
        Session("NameValue") = Nothing
        Session("IdCaption") = Nothing
        Session("IdValue") = Nothing

    End Sub
    Private Function BuildPartialURL(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.URL & "?resid=" & uppInfo.ResourceId
    End Function

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click

        txtLastName.Text = ""
        txtFirstName.Text = ""
        txtSSN.Text = ""
        ddlStatusId.SelectedIndex = 0
        dgrdTransactionSearch.Visible = False

    End Sub
    Public Function BuildEmployeeStateObject(ByVal EmployeeId As String) As String
        Dim objStateInfo As New AdvantageStateInfo
        Dim objGetStudentStatusBar As New AdvantageStateInfo
        Dim strVID As String
        Dim facInputMasks As New InputMasksFacade
        Dim strEmployeeId As String = ""

        If EmployeeId.ToString.Trim = "" Then 'No Student was selected from MRU
            strEmployeeId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   3, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
        Else
            strEmployeeId = EmployeeId
        End If

        objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(strEmployeeId)
        With objStateInfo
            .EmployeeId = objGetStudentStatusBar.EmployeeId
            .NameValue = objGetStudentStatusBar.NameValue
            .Address1 = objGetStudentStatusBar.Address1
        End With

        'Create a new guid to be associated with the Employee pages
        strVID = Guid.NewGuid.ToString

        Session("EmployeeObjectPointer") = strVID

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        txtEmployeeId.Text = strEmployeeId

        'UpdateMRUTable
        'Reason: We need to keep track of the last student record the user worked with
        'Scenario1 : User can log in and click on a student page without using MRU 
        'and we need to display the data of the last student the user worked with
        'If the user is a first time user, we will load the student who was last added to advantage
        'mruProvider.UpdateMRUList(EmployeeId, AdvantageSession.UserState.UserId.ToString, _
        '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

        'mruProvider.InsertMRU(3, strEmployeeId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

        Return strVID


    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub
End Class
