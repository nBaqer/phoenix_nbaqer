﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="UserImpersonation.aspx.vb" Inherits="SY_UserImpersonation" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:content id="content4" contentplaceholderid="additional_head" runat="server">
</asp:content>
<asp:content id="content1" contentplaceholderid="contentmain2" runat="server">
    <script src="../Scripts/Storage/storageCache.js"></script>
    <script src="../Scripts/common-util.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/dataSourceCommon.js"></script>                           
	<script type="text/javascript" src="../Scripts/DataSources/usersDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/userImpersonationLogDataSource.js"></script>
	<script type="text/javascript" src="../Scripts/viewModels/userImpersonation.js"></script>
    <script type="text/javascript"  src="../Scripts/common/kendoControlSetup.js"></script>
	<div id='frame'>
		<div id='body'>
			<telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
		<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			
            <br/>
            <div style="margin:10px;">
            <div id="tabstrip" style='display:table;width:100%;background-color: white;' >
                <ul >
                    <li id="singleStudentTab" class="k-state-active">Impersonate User</li>
                    <li >Impersonation Log</li>
                </ul>
                <div >
                    <br/>
				    <input id="userDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="UserAndName"  data-bind="source: users, value: selectedUsers" style="width:250px" />
				    <input id="sImp" type="submit" data-bind="click: impersonateUser" value="Impersonate" class="k-button" /><input id="resetImp" type="submit" data-bind="click: resetImpersonation" value="Reset to Support" class="k-button" />                            				
			        <br/><br/>
                </div>
                <div>
                    <br/>
                    
                    <span class="tblLabels">Start Date:&nbsp;&nbsp;</span><input id='startDatePicker' data-role='datepicker' style="width:175px" />&nbsp;&nbsp;<span class="tblLabels">End Date:&nbsp;&nbsp;</span><input id="endDatePicker" data-role='datepicker' style="width:175px" />&nbsp;
                    <input type="button" data-bind="click: searchLog" value="Search" class="k-button" />
                    <br/><br/>
                   <div id="userImpersonationGrid"  data-bind="source: userImpersonationLog"></div>
                    <asp:Button runat="server" ID="hiddenButton" ClientIDMode="Static" Visible="false"/>
                </div>
            </div>
            </div>
            <br/>
            
			
			<div id="errorDetails" style="display: none"></div>
			<input type="hidden" id="hdnImpUserName" runat="server" class="hdnUserName" ClientIDMode="Static"/>
			<input type="hidden" id="hdnImpUserId" runat="server" class="hdnUserName" ClientIDMode="Static"/>
            <input type="hidden" id="hdnImpIsImpersonatingLocal" runat="server" class="hdnUserName" ClientIDMode="Static"/>
            <input type="hidden" id="hdnImpUserAndName" runat="server" class="hdnUserName" ClientIDMode="Static"/>
            <input type="hidden" id="hdnNonImpersonatingUserId" runat="server" class="hdnUserName" ClientIDMode="Static"/>
            <input type="hidden" id="hdnUserImpersonationLogId" runat="server"  ClientIDMode="Static"/>
            
		</telerik:RadPane>
		</telerik:RadSplitter>
		</div>    
	</div>
	
	
	

</asp:content>  


