<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SummaryOfRolesAndCampus.aspx.vb" Inherits="SY_SummaryOfRolesAndCampus" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Roles Assigned</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
		<LINK href="../css/systememail.css" type="text/css" rel="stylesheet">
		<LINK href="../css/BulletedList.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body runat="server" ID="Body1" NAME="Body1">
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><IMG src="../images/advantage_roles_assigned.jpg"></td>
					<td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
				</tr>
			</table>
			<!--begin right column-->
			<table class="maincontenttable" id="Table5" cellSpacing="0" cellPadding="0" width="100%">
				<TBODY>
					<tr>
						<td class="detailsFrame"><div class="scrollwholedocs">
								<!-- begin content table-->
								<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="50%" border="0" align="center">
									<TR>
										<TD class="contentcell" style="text-align: right; padding-right: 20px"><asp:label id="lblUser" Runat="server" CssClass="label">User</asp:label></TD>
										<TD class="2columnconetntcell" noWrap style="PADDING-RIGHT: 20px"><asp:label id="lblUserName" Runat="server" CssClass="textbox"></asp:label></TD>
										
									</TR>
								</TABLE>
								<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<TR>
										<TD style="padding-top: 20px">
										          <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CampGrpId"
                                                     cellpadding="0" BorderWidth="1px" BorderColor="#E0E0E0" width="100%">
			                                        <HeaderStyle cssClass="datagridheader" Wrap="True"></HeaderStyle>
			                                        <RowStyle cssClass="datagriditemstyle"/>
			                                        <AlternatingRowStyle CssClass="datagridalternatingstyle" />
                                                    <Columns>
                                                        <asp:BoundField DataField="CampGrpDescrip" HeaderText="Campus Groups" SortExpression="CampGrpDescrip" HeaderStyle-CssClass="datagridheader" ItemStyle-CssClass="datagriditemstyle">
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Campus" HeaderStyle-CssClass="datagridheader" ItemStyle-CssClass="datagriditemstyle">
                                                            <ItemTemplate>
                                                                <asp:BulletedList ID="BlCampus" DataTextField="CampDescrip" CausesValidation="False" BulletStyle="Square"  runat="server">
                                                                </asp:BulletedList>
                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="datagriditemstyle" />
                                                            <HeaderStyle CssClass="datagridheader" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Roles" HeaderStyle-CssClass="datagridheader" ItemStyle-CssClass="datagriditemstyle">
                                                            <ItemTemplate>
                                                                <asp:BulletedList ID="BlRole"  DataTextField="Role" CausesValidation="False" BulletStyle="Square"  runat="server">
                                                                </asp:BulletedList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
										
										</TD>
									</TR>
								</TABLE>
								<!-- end content table -->
							</div>
						</td>
					</tr>
				</TBODY>
			</table>
			<div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
		</form>
	</body>
</HTML>

