' ===============================================================================
'
' FAME AdvantageV1
'
' StudentContactAddresses.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.Drawing
Imports Advantage.Business.Objects
Imports System.Collections

Partial Class StudentContactAddresses
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As Label
    Protected WithEvents btnhistory As Button
    Protected studentContactId As String
    Protected campusId As String
    Private pObj As New UserPagePermissionInfo


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim resourceId As Integer
        Dim userId As String

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim advantageUserState As  User = AdvantageSession.UserState
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        '   get the StudentContactId from the querystring
        If CommonWebUtilities.IsValidGuid(CType(Request.Item("StudentContactId"), String)) Then
            studentContactId = Request.Item("StudentContactId")
        Else
            '   for testing purpose only
            studentContactId = "BF239870-89D8-43C5-AB5A-B76971A5500B".ToLower ' Anabella Alton
            'end of testing code
        End If

        If Not IsPostBack Then
            objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"

            '   initialize the name of the StudentContact
            If Not Request.Item("StudentContact") Is Nothing Then
                lblStudentContactName.Text = Request.Item("StudentContact")
            Else
                lblStudentContactName.Text = "Invalid Student Contact Name"
            End If

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList(studentContactId)

            '   bind an empty new StudentContactAddressInfo
            BindStudentContactAddressData(New StudentContactAddressInfo(studentContactId))

            '   initialize buttons
            InitButtonsForLoad()
        Else
            objCommon.PageSetup(Form1, "EDIT")
        End If
        GetInputMaskValue()
        If chkForeignZip.Checked = True Then
            ddlStateId.Enabled = False
            txtOtherState.Visible = True
            lblOtherState.Visible = True
        Else
            ddlStateId.Enabled = True
            txtOtherState.Visible = False
            lblOtherState.Visible = False
        End If
    End Sub
    Private Sub BindDataList(ByVal studentContactId As String)

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind StudentContactAddresses datalist
        dlstStudentContactAddresses.DataSource = New DataView((New StudentContactsFacade).GetAllStudentContactAddresses(studentContactId).Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstStudentContactAddresses.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDdl()
        BuildStatesDdl()
        BuildAddressTypesDdl()
        BuildCountriesDdl()
    End Sub
    Private Sub BuildStatusDdl()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildStatesDdl()
        Dim state As New StatesFacade
        With ddlStateId
            .DataTextField = "StateDescrip"
            .DataValueField = "StateId"
            .DataSource = state.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountriesDdl()
        '   bind Countries ddl
        Dim countries As New PrefixesFacade

        '   build Employer Countries ddl
        With ddlCountryId
            .DataTextField = "CountryDescrip"
            .DataValueField = "CountryId"
            .DataSource = countries.GetAllCountries()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            'selected index must be the default country if it is defined in configuration file
            .SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)
        End With
    End Sub
    Private Sub BuildAddressTypesDdl()
        Dim addressType As New PrefixesFacade
        With ddlAddressTypeId
            .DataTextField = "AddressDescrip"
            .DataValueField = "AddressTypeId"
            .DataSource = addressType.GetAllAddressType()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub DlstStudentContactAddressesItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstStudentContactAddresses.ItemCommand

        '   get the StudentContactAddressId from the backend and display it
        GetStudentContactAddressId(e.CommandArgument)

        If chkForeignZip.Checked = True Then
            ddlStateId.Enabled = False
            txtOtherState.Visible = True
            lblOtherState.Visible = True
        Else
            ddlStateId.Enabled = True
            txtOtherState.Visible = False
            lblOtherState.Visible = False
        End If

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContactAddresses, e.CommandArgument, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstStudentContactAddresses, e.CommandArgument)

        '   initialize buttons
        InitButtonsForEdit()

    End Sub
    Private Sub BindStudentContactAddressData(ByVal studentContactAddress As StudentContactAddressInfo)

        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String

        'Get the mask for phone numbers and zip
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        With studentContactAddress
            If .ForeignZip = True Then
                chkForeignZip.Checked = True
            Else
                chkForeignZip.Checked = False
            End If

            chkIsInDB.Checked = .IsInDB
            txtStudentContactAddressId.Text = .StudentContactAddressId
            ddlStatusId.SelectedValue = .StatusId
            txtStudentContactId.Text = .StudentContactId
            ddlAddressTypeId.SelectedValue = .AddrTypId
            txtAddress1.Text = .Address1
            txtAddress2.Text = .Address2
            txtCity.Text = .City
            ddlStateId.SelectedValue = .StateId
            ddlCountryId.SelectedValue = .CountryId
            'txtZip.Text = .Zip

            'Get Zip
            txtZip.Text = .Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            End If

            txtOtherState.Text = .OtherState
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub BtnSaveClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim result As String
        With New StudentContactsFacade
            '   update StudentContactAddress Info 
            result = .UpdateStudentContactAddressInfo(BuildStudentContactAddressInfo(txtStudentContactAddressId.Text, studentContactId), AdvantageSession.UserState.UserName)
        End With

        '   bind datalist
        BindDataList(studentContactId)

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContactAddresses, txtStudentContactAddressId.Text, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstStudentContactAddresses, txtStudentContactAddressId.Text)


        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the StudentContactAddressId from the backend and display it
            GetStudentContactAddressId(txtStudentContactAddressId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new StudentContactAddressInfo
            'BindStudentContactAddressData(New StudentContactAddressInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If
    End Sub
    Private Function BuildStudentContactAddressInfo(ByVal studentContactAddressId As String, ByVal studentContactId As String) As StudentContactAddressInfo
        'instantiate class
        Dim studentContactAddressInfo As New StudentContactAddressInfo(studentContactId)

        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String

        'Get the mask for phone numbers and zip
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        With studentContactAddressInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get ForeignZip
            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            'get StudentContactAddressId
            .StudentContactAddressId = studentContactAddressId

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'studentContactId
            '.studentContactId = txtstudentContactId.Text

            'AddrTypId
            .AddrTypId = ddlAddressTypeId.SelectedValue

            'First Name
            .Address1 = txtAddress1.Text

            'Address2
            .Address2 = txtAddress2.Text

            'City
            .City = txtCity.Text

            'StateId
            .StateId = ddlStateId.SelectedValue

            'CountryId
            .CountryId = ddlCountryId.SelectedValue

            'Zip
            '.Zip = txtZip.Text

            'Get Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                .Zip = facInputMasks.RemoveMask(zipMask, txtZip.Text)
            Else
                .Zip = txtZip.Text
            End If

            'OtherState
            .OtherState = txtOtherState.Text

            'ModUser
            .ModUser = txtModUser.Text

            'ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return studentContactAddressInfo
    End Function
    Private Sub BtnNewClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        '   bind an empty new StudentContactAddressInfo
        BindStudentContactAddressData(New StudentContactAddressInfo(studentContactId))

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContactAddresses, Guid.Empty.ToString, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstStudentContactAddresses, Guid.Empty.ToString)
        'initialize buttons
        InitButtonsForLoad()
    End Sub
    Private Sub BtnDeleteClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (txtStudentContactAddressId.Text = Guid.Empty.ToString) Then

            'update StudentContactAddress Info 
            Dim result As String = (New StudentContactsFacade).DeleteStudentContactAddressInfo(txtStudentContactAddressId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub
            Else
                '   bind datalist
                BindDataList(studentContactId)

                '   bind an empty new StudentContactAddressInfo
                BindStudentContactAddressData(New StudentContactAddressInfo(studentContactId))

                '   initialize buttons
                InitButtonsForLoad()
            End If

        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    End Sub

    Private Sub GetStudentContactAddressId(ByVal studentContactAddressId As String)
        '   bind StudentContactAddress properties
        BindStudentContactAddressData((New StudentContactsFacade).GetStudentContactAddressInfo(studentContactAddressId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub RadStatusSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindStudentContactAddressData(New StudentContactAddressInfo(studentContactId))
        BindDataList(studentContactId)

        InitButtonsForLoad()

    End Sub
    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        '        Dim correctFormat As Boolean
        Dim strMask As String
        Dim zipMask As String
        '  Dim errorMessage As String
        '  Dim strPhoneReq As String
        Dim strZipReq As String

        '  Dim strFaxReq As String
        Dim objCommon As New CommonUtilities

        'Get The Input Mask for Phone/Fax and Zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters

        If chkForeignZip.Checked = False Then
            txtZip.Mask = Replace(zipMask, "#", "9")
            lblZip.ToolTip = zipMask
        Else
            txtZip.Mask = ""
            lblZip.ToolTip = ""
        End If

        'Get The RequiredField Value
        strZipReq = objCommon.SetRequiredColorMask("Zip")


        'If The Field Is Required Field Then Color The Masked
        'Edit Control
        If strZipReq = "Yes" Then
            txtZip.BackColor = Color.FromName("#ffff99")
        End If
    End Sub

    'Private Function ValidateFieldsWithInputMasks() As String
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim correctFormat1 As Boolean ' correctFormat,, correctFormat2, correctFormat3
    '    Dim strMask As String
    '    Dim zipMask As String
    '    Dim errorMessage As String

    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

    '    'Validate the phone field format. If the field is empty we should not apply the mask
    '    'agaist it.
    '    errorMessage = ""

    '    If txtZip.Text <> "" And chkForeignZip.Checked = False Then
    '        correctFormat1 = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
    '        If correctFormat1 = False Then
    '            errorMessage &= "Incorrect format for zip field." & vbLf
    '        End If
    '    End If

    '    Return Trim(errorMessage)
    'End Function
    Private Sub ChkForeignZipCheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignZip.CheckedChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
    End Sub
    Private Sub TxtOtherStateTextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtOtherState.TextChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtZip)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender 
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList() 
        'add save button 
        controlsToIgnore.Add(btnSave)
       'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
