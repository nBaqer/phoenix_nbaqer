
' ===============================================================================
'
' FAME AdvantageV1
'
' StudentContactPhones.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class StudentContactPhones
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblRelation As System.Web.UI.WebControls.Label
    Protected WithEvents txtStudentContactPhoneHead As System.Web.UI.WebControls.TextBox
    Protected StudentContactId As String
    Protected WithEvents rfvPhone As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvBestTime As System.Web.UI.WebControls.RequiredFieldValidator
    Protected errorMessage As String
    Protected campusId As String
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim resourceId As Integer
        Dim userId As String
        'Dim fac As New UserSecurityFacade

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim advantageUserState  As User = AdvantageSession.UserState
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        '   get the StudentContactId from the querystring
        If CommonWebUtilities.IsValidGuid(CType(Request.Item("StudentContactId"), String)) Then
            StudentContactId = Request.Item("StudentContactId")
        Else
            '   for testing purpose only
            StudentContactId = "BF239870-89D8-43C5-AB5A-B76971A5500B".ToLower ' Anabella Alton
            'end of testing code
        End If

        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString


        If Not IsPostBack Then
            objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"

            '   initialize the name of the StudentContact
            If Not Request.Item("StudentContact") Is Nothing Then
                lblStudentContactName.Text = Request.Item("StudentContact")
            Else
                lblStudentContactName.Text = "Invalid Student Contact Name"
            End If

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList(StudentContactId)

            '   bind an empty new StudentContactPhoneInfo
            BindStudentContactPhoneData(New StudentContactPhoneInfo)

            '   initialize buttons
            InitButtonsForLoad()
        Else
            objCommon.PageSetup(Form1, "EDIT")
            InitButtonsForEdit()

        End If
        ' GetInputMaskValue()
    End Sub

    Private Sub BindDataList(ByVal StudentContactId As String)

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind StudentContacts datalist
        dlstStudentContactPhones.DataSource = New DataView((New StudentContactsFacade).GetAllStudentContactPhones(StudentContactId).Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstStudentContactPhones.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        'BuildStatusDDL()
        'BuildPhoneTypesDDL()

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Status DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing))

        'PhoneTypes DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneTypeID, AdvantageDropDownListName.Phone_Types, campusId, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildPhoneTypesDDL()
        Dim PhoneType As New LeadFacade
        With ddlPhoneTypeID
            .DataTextField = "PhoneTypeDescrip"
            .DataValueField = "PhoneTypeId"
            .DataSource = PhoneType.GetPhoneTypes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub dlstStudentContactPhones_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstStudentContactPhones.ItemCommand

        '   get the StudentContactPhoneId from the backend and display it
        GetStudentContactPhoneId(e.CommandArgument)

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContactPhones, e.CommandArgument, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstStudentContactPhones, e.CommandArgument)

        '   initialize buttons
        InitButtonsForEdit()

    End Sub
    Private Sub BindStudentContactPhoneData(ByVal StudentContactPhone As StudentContactPhoneInfo)

        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String

        '        Dim zipMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        With StudentContactPhone
            chkIsInDB.Checked = .IsInDB
            txtStudentContactPhoneId.Text = .StudentContactPhoneId
            ddlStatusId.SelectedValue = .StatusId
            txtStudentContactId.Text = .StudentContactId
            'ddlPhoneTypeID.SelectedValue = .PhoneTypeId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneTypeID, .PhoneTypeId, .PhoneType)
            'txtPhone.Text = .Phone
            txtPhone.Text = .Phone
            txtExt.Text = .Ext
            txtBestTime.Text = .BestTime

            'bind ForeignHomePhone
            chkForeignPhone.Checked = .ForeignPhone

            If .ForeignPhone = True Then
                txtPhone.Mask = "aaaaaaaaaaaaaaaaaaaaaaaaa"
                txtPhone.MaxLength = 25
            Else
                txtPhone.Mask = "(###) ###-####"
            End If
           

            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        If Not chkForeignPhone.Checked And txtPhone.Text.Length <> 10 Then
            errorMessage = "Phone must be 10 digits"
        End If

        If Not errorMessage = "" Then
            DisplayErrorMessage(errorMessage)
            Exit Sub
        End If

        'Check If Mask is successful only if Foreign Is not checked
        If errorMessage = "" Then

            Dim result As String
            With New StudentContactsFacade
                '   update StudentContactPhone Info 
                result = .UpdateStudentContactPhoneInfo(BuildStudentContactPhoneInfo(txtStudentContactPhoneId.Text, StudentContactId), AdvantageSession.UserState.UserName)
            End With

            '   bind datalist
            BindDataList(StudentContactId)

           
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get the StudentContactPhoneId from the backend and display it
                GetStudentContactPhoneId(txtStudentContactPhoneId.Text)
            End If

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContactPhones, txtStudentContactPhoneId.Text, ViewState)
            CommonWebUtilities.RestoreItemValues(dlstStudentContactPhones, txtStudentContactPhoneId.Text)

            '   if there are no errors bind a new entity and init buttons
            If Page.IsValid Then

                '   set the property IsInDB to true in order to avoid an error if the user
                '   hits "save" twice after adding a record.
                chkIsInDB.Checked = True

                'note: in order to display a new page after "save".. uncomment next lines
                '   bind an empty new StudentContactPhoneInfo
                'BindStudentContactPhoneData(New StudentContactPhoneInfo)

                '   initialize buttons
                'InitButtonsForLoad()
                InitButtonsForEdit()

            End If
        End If
    End Sub
    Private Function BuildStudentContactPhoneInfo(ByVal StudentContactPhoneId As String, ByVal StudentContactId As String) As StudentContactPhoneInfo

        'Dim facInputMask As New InputMasksFacade
        'Dim phoneMask As String
        '        Dim zipMask As String

        'instantiate class
        Dim studentContactPhoneInfo As New StudentContactPhoneInfo(StudentContactId)

        With studentContactPhoneInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get StudentContactPhoneId
            .StudentContactPhoneId = StudentContactPhoneId

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'StudentContactId
            '.StudentContactId = txtStudentContactId.Text

            'PhoneTypeId
            .PhoneTypeId = ddlPhoneTypeID.SelectedValue

            'First Name
            '.Phone = txtPhone.Text
            .Phone = txtPhone.Text

            'Ext
            .Ext = txtExt.Text

            'BestTime
            .BestTime = txtBestTime.Text

            '   get ForeignHomePhone
            .ForeignPhone = chkForeignPhone.Checked

            'ModUser
            .ModUser = txtModUser.Text

            'ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return studentContactPhoneInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new StudentContactPhoneInfo
        BindStudentContactPhoneData(New StudentContactPhoneInfo(StudentContactId))

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContactPhones, Guid.Empty.ToString, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstStudentContactPhones, Guid.Empty.ToString)

        'initialize buttons
        InitButtonsForLoad()
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtStudentContactPhoneId.Text = Guid.Empty.ToString) Then

            'update StudentContactPhone Info 
            Dim result As String = (New StudentContactsFacade).DeleteStudentContactPhoneInfo(txtStudentContactPhoneId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub
            Else
                '   bind datalist
                BindDataList(StudentContactId)

                '   bind an empty new StudentContactPhoneInfo
                BindStudentContactPhoneData(New StudentContactPhoneInfo(StudentContactId))

                '   initialize buttons
                InitButtonsForLoad()
            End If

        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If


        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    End Sub
    Private Sub GetStudentContactPhoneId(ByVal StudentContactPhoneId As String)
        '   bind StudentContactPhone properties
        BindStudentContactPhoneData((New StudentContactsFacade).GetStudentContactPhoneInfo(StudentContactPhoneId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList(StudentContactId)
        '   bind an empty new StudentContactPhoneInfo
        BindStudentContactPhoneData(New StudentContactPhoneInfo(StudentContactId))
    End Sub
  
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone.CheckedChanged

        'Dim objCommon As New CommonWebUtilities
        ' txtPhone.Text = ""
        CommonWebUtilities.SetFocus(Me.Page, txtPhone)

        If chkForeignPhone.Checked Then
            txtPhone.Mask = "aaaaaaaaaaaaaaaaaaaaaaaaa"
            txtPhone.MaxLength = 25
        Else
            txtPhone.Mask = "(###) ###-####"
        End If

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
