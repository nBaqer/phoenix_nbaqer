Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class LeadStatusChangePermissions
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlHeader As System.Web.UI.WebControls.Panel
    Protected WithEvents lblCampGrpDescrip As System.Web.UI.WebControls.Label
    Protected WithEvents allDataset As System.Data.DataSet
    Protected WithEvents dataGridTable As System.Data.DataTable
    Protected WithEvents dataListTable As System.Data.DataTable
    Protected WithEvents ddlRolesTable As System.Data.DataTable
    Protected WithEvents ddlStatusesTable As System.Data.DataTable

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        '   Dim sStatusId As String
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        'Header1.EnableHistoryButton(False)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        Try
            If Not Page.IsPostBack Then
                ViewState("MODE") = "NEW"
                objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

                Session("LeadStatusChangeId") = Nothing

                '   build dropdownlists
                BuildDropDownLists()

                '   get allDataset from the DB
                With New LeadStatusChangeFacade
                    allDataset = .GetLeadStatusChangesDS()
                End With

                '   save it on the session
                Session("AllDataset") = allDataset

                '   initialize buttons
                InitButtonsForLoad()

            Else
                objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                'Call the procedure for edit
                InitButtonsForEdit()
            End If

            '   create Dataset and Tables
            CreateDatasetAndTables()

            '   the first time we have to bind an empty datagrid
            If Not IsPostBack Then PrepareForNewData()

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub

    Private Sub CreateDatasetAndTables()
        '   Get dataGrid dataset from the session and create tables
        allDataset = Session("AllDataset")
        dataListTable = allDataset.Tables("LeadStatusChanges")
        dataGridTable = allDataset.Tables("LeadStatusChangePermissions")
        ddlRolesTable = allDataset.Tables("Roles")
        ddlStatusesTable = allDataset.Tables("Statuses")
    End Sub
    Private Sub PrepareForNewData()

        '   save LeadStatusChangeId in session
        Session("LeadStatusChangeId") = Nothing

        ''   bind datalist
        'BindDataList()

        '   bind DataGrid
        BindDataGrid()

        '   bind an empty new LeadStatusChangeInfo
        BindLeadStatusChangeData(New LeadStatusChangeInfo)

        '   initialize buttons
        InitButtonsForLoad()
    End Sub

    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String

        'If ddlCampusGroup.SelectedIndex > 0 Then
        rowFilter = "AND CampGrpId='" & ddlCampusGroup.SelectedValue & "'"
        'End If
        If ddlOrigStatus.SelectedIndex > 0 Then
            rowFilter &= "AND OrigStatusId='" & ddlOrigStatus.SelectedValue & "' "
        End If
        If ddlNewStatus.SelectedIndex > 0 Then
            rowFilter &= "AND NewStatusId='" & ddlNewStatus.SelectedValue & "' "
        End If

        If rowFilter <> "" Then rowFilter = rowFilter.Remove(0, 4)

        sortExpression = "CampGrpDescrip,OrigStatusDescrip,NewStatusDescrip"

        '   bind dlstLeadStatusChanges datalist
        dlstLeadStatusChanges.DataSource = New DataView(dataListTable, rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstLeadStatusChanges.DataBind()

        'If Not Session("LeadStatusChangeId") Is Nothing Then
        '    '   set Style to Selected Item
        '    'CommonWebUtilities.SetStyleToSelectedItem(dlstLeadStatusChanges, Session("LeadStatusChangeId"), ViewState, Header1)
        'Else
        '    '   reset Style to Selected Item
        '    ' CommonWebUtilities.SetStyleToSelectedItem(dlstLeadStatusChanges, Guid.Empty.ToString, ViewState, Header1)
        'End If
    End Sub
    Private Sub BuildDropDownLists()
        BuildLeadStatusDDL()
        BuildCampGrpDDL()
    End Sub
    Private Sub BuildLeadStatusDDL()
        '   populate status DDLs on RHS with all lead statuses, active and inactive ones.
        Dim status As New LeadFacade
        Dim ds As DataSet = status.GetReassignLeadStatus("AllStatuses")
        With ddlOrigStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = ds.Tables(0)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
        With ddlNewStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = ds.Tables(0)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildLeadStatusDDL(ByVal campGrpId As String, ByVal IsFilterDDL As Boolean)
        '   populate status DDLs with all lead statuses, active and inactive ones, for a particular campusGrp.
        '   IsFilterDDL argument indicates if DDLs to populate are those on LHS or RHS
        Dim status As New LeadFacade
        Dim ds As DataSet = status.GetReassignLeadStatus("AllStatuses", campGrpId)
        If IsFilterDDL Then
            '   LHS DDLs
            With ddlOrigStatus
                .DataTextField = "StatusCodeDescrip"
                .DataValueField = "StatusCodeID"
                .DataSource = ds.Tables(0)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
            With ddlNewStatus
                .DataTextField = "StatusCodeDescrip"
                .DataValueField = "StatusCodeID"
                .DataSource = ds.Tables(0)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
        Else
            '   RHS DDLs
            With ddlOrigStatusId
                .DataTextField = "StatusCodeDescrip"
                .DataValueField = "StatusCodeID"
                .DataSource = ds.Tables(0)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
            With ddlNewStatusId
                .DataTextField = "StatusCodeDescrip"
                .DataValueField = "StatusCodeID"
                .DataSource = ds.Tables(0)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
        End If
    End Sub

    Private Sub BuildCampGrpDDL()
        '   bind the Campus Group DropDownList
        Dim facade As New CampusGroupsFacade
        Dim ds As DataSet = facade.GetAllCampusGroups()
        With ddlCampusGroup
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", System.Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", System.Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub GetLeadStatusChangesDS()
        '   get allDataset from the DB
        With New LeadStatusChangeFacade
            allDataset = .GetLeadStatusChangesDS()
        End With

        '   save it on the session
        Session("AllDataset") = allDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()
    End Sub
    Private Sub dlstLeadStatusChanges_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstLeadStatusChanges.ItemCommand
        '   save AdLeadStatusChangeId in session
        Dim strId As String = e.CommandArgument   'dlstLeadStatusChanges.DataKeys(e.Item.ItemIndex).ToString()

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.SetHiddenControlForAudit()
        Session("LeadStatusChangeId") = strId

        '   this portion of code added to refresh data from the database
        GetLeadStatusChangesDS()

        '   add all known lead status so page does not fails
        BuildLeadStatusDDL()

        ''   save AdLeadStatusChangeId in session
        'Dim strId As String = e.CommandArgument   'dlstLeadStatusChanges.DataKeys(e.Item.ItemIndex).ToString()
        'Session("LeadStatusChangeId") = strId

        '   get the row with the data to be displayed
        Dim row() As DataRow = dataListTable.Select("LeadStatusChangeId=" + "'" + CType(Session("LeadStatusChangeId"), String) + "'")

        '   populate controls with row data
        txtLeadStatusChangeId.Text = CType(row(0)("LeadStatusChangeId"), Guid).ToString
        ddlOrigStatusId.SelectedValue = CType(row(0)("OrigStatusId"), Guid).ToString
        ddlNewStatusId.SelectedValue = CType(row(0)("NewStatusId"), Guid).ToString
        ddlCampGrpId.SelectedValue = CType(row(0)("CampGrpId"), Guid).ToString
        chkIsReversal.Checked = row(0)("IsReversal")
        txtModUser.Text = row(0)("ModUser")
        txtModDate.Text = row(0)("ModDate").ToString

        '   bind DataGrid
        dgrdRoles.EditItemIndex = -1
        BindDataGrid()

        '   show footer
        dgrdRoles.ShowFooter = True

        '   disable ReqGrpLeadGrp link button
        dgrdRoles.Enabled = True

        '   initialize buttons
        InitButtonsForEdit()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstLeadStatusChanges, e.CommandArgument, ViewState, Header1)

        CommonWebUtilities.RestoreItemValues(dlstLeadStatusChanges, CType(Session("LeadStatusChangeId"), String))
    End Sub
    Private Sub BindLeadStatusChangeData(ByVal LeadStatusChange As LeadStatusChangeInfo)
        With LeadStatusChange
            chkIsInDB.Checked = .IsInDB
            txtLeadStatusChangeId.Text = .LeadStatusChangeId
            ddlOrigStatusId.SelectedValue = .OrigStatusId
            ddlNewStatusId.SelectedValue = .NewStatusId
            ddlCampGrpId.SelectedValue = .CampGrpId
            chkIsReversal.Checked = .IsReversal
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        If Me.ddlNewStatusId.SelectedValue = Me.ddlOrigStatusId.SelectedValue Then
            '   Display Error Message
            DisplayErrorMessage("- Statuses cannot be equal")
            Exit Sub
        End If

        '   if any of the footer fields of the datagrid is not blank.. send an error message 
        If Not AreFooterFieldsBlank() Then
            '   Display Error Message
            DisplayErrorMessage("To use ""Save"" all fields in the Authorized Roles footer must be blank")
            Exit Sub
        End If

        '   if AdLeadStatusChangeId does not exist do an insert else do an update
        If Session("LeadStatusChangeId") Is Nothing Then
            '   do an insert
            BuildNewRowInDataList(Guid.NewGuid.ToString)

        Else
            '   do an update
            '   get the row with the data to be displayed
            Dim row() As DataRow = dataListTable.Select("LeadStatusChangeId=" + "'" + CType(Session("LeadStatusChangeId"), String) + "'")

            '   update row data
            UpdateRowData(row(0))

        End If

        ' Do the unique index validation
        If AreAuthorizedRolesValid() Then

            '   try to update the DB and show any error message
            UpdateDB()

            '   if there were no errors then bind and set selected style to the datalist and initialize buttons 
            If Customvalidator1.IsValid Then

                '   bind datalist
                BindDataList()

                '   initialize buttons
                InitButtonsForEdit()
            End If

        Else
            DisplayErrorMessage("A permission with this role already exists in the database.")
        End If
        CommonWebUtilities.RestoreItemValues(dlstLeadStatusChanges, CType(Session("LeadStatusChangeId"), String))
    End Sub

    Private Function BuildLeadStatusChangeInfo(ByVal LeadStatusChangeId As String) As LeadStatusChangeInfo
        'instantiate class
        Dim LeadStatusChange As New LeadStatusChangeInfo

        With LeadStatusChange
            'IsInDB
            .IsInDB = chkIsInDB.Checked

            'LeadStatusChangeId
            .LeadStatusChangeId = txtLeadStatusChangeId.Text

            'CampGrpId
            .CampGrpId = ddlCampGrpId.SelectedValue

            'ModUser
            .ModUser = txtModUser.Text

            'ModDate
            .ModDate = Date.Parse(txtModDate.Text)
        End With

        'return data
        Return LeadStatusChange
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        '   reset dataset to previous state. delete all changes
        allDataset.RejectChanges()
        PrepareForNewData()

        '   initialize buttons
        InitButtonsForLoad()

        '   reset Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstLeadStatusChanges, Guid.Empty.ToString, ViewState, Header1)


        CommonWebUtilities.RestoreItemValues(dlstLeadStatusChanges, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        '   get all rows to be deleted
        Dim rows() As DataRow = dataGridTable.Select("LeadStatusChangeId=" + "'" + CType(Session("LeadStatusChangeId"), String) + "'")

        '   delete all rows from dataGrid table
        Dim i As Integer
        If rows.Length > 0 Then
            For i = 0 To rows.Length - 1
                rows(i).Delete()
            Next
        End If

        '   get the row to be deleted in LeadStatusChangeId table
        Dim row() As DataRow = dataListTable.Select("LeadStatusChangeId=" + "'" + CType(Session("LeadStatusChangeId"), String) + "'")

        '   delete row from the table
        row(0).Delete()

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   Prepare screen for new data
            PrepareForNewData()
        End If

        '   bind datalist
        BindDataList()

        '   initialize buttons
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlstLeadStatusChanges, Guid.Empty.ToString)
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
            dgrdRoles.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
            dgrdRoles.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub

    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdRoles.Controls(0).Controls(dgrdRoles.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("ddlFooterRoleId"), DropDownList).SelectedValue = System.Guid.Empty.ToString And
                Not CType(footer.FindControl("ddlFooterStatusId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then
            Return False
        End If
        ''If Not CType(footer.FindControl("ddlFooterRoleId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then Return False
        ''If Not CType(footer.FindControl("ddlFooterStatusId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then Return False
        Return True
    End Function

    Private Sub BuildNewRowInDataList(ByVal theGuid As String)
        '   get a new row
        Dim newRow As DataRow = dataListTable.NewRow

        '   create a Guid for DataList if it has not been created 
        Session("LeadStatusChangeId") = theGuid
        newRow("LeadStatusChangeId") = New Guid(theGuid)

        '   update row with web controls data
        UpdateRowData(newRow)

        '   add row to the table
        newRow.Table.Rows.Add(newRow)
    End Sub

    Private Sub UpdateRowData(ByVal row As DataRow)
        '   update row data
        row("OrigStatusId") = New Guid(ddlOrigStatusId.SelectedValue)
        row("NewStatusId") = New Guid(ddlNewStatusId.SelectedValue)
        row("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
        row("IsReversal") = chkIsReversal.Checked
        row("ModUser") = Session("UserName")
        row("ModDate") = Date.Parse(Date.Now.ToString)
    End Sub

    Private Sub UpdateDB()
        '   update DB
        With New LeadStatusChangeFacade
            Dim result As String = .UpdateLeadStatusChangesDS(allDataset)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get a new version of LeadStatusChanges dataset because some other users may have added,
                '   updated or deleted records from the DB
                allDataset = .GetLeadStatusChangesDS()
                Session("AllDataset") = allDataset
                CreateDatasetAndTables()
            End If
        End With
    End Sub

    Private Function AreAuthorizedRolesValid() As Boolean
        Dim arrRows() As DataRow
        Dim lastRole As String = ""
        Dim result As Boolean = True

        arrRows = dataGridTable.Select("LeadStatusChangeId=" + "'" + CType(Session("LeadStatusChangeId"), String) + "'", "RoleId")

        If arrRows.GetLength(0) > 0 Then
            For Each row As DataRow In arrRows
                If lastRole = "" Or lastRole <> row("RoleId").ToString Then
                    lastRole = row("RoleId").ToString
                Else
                    result = False
                    Exit For
                End If
            Next
        End If

        Return result

    End Function

    Private Sub dgrdRoles_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdRoles.ItemCommand
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdRoles.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdRoles.ShowFooter = False


                '   user hit "Update" inside the datagrid
            Case "Update"
                '   process only if the edit dropdownlist are valid
                If Not AreEditFieldsBlank(e) Then
                    '   get the row to be updated
                    Dim row() As DataRow = dataGridTable.Select("LeadStatusChangePermissionId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditLeadStatusChangePermissionId"), Label).Text).ToString + "'")
                    '   fill new values in the row
                    If Not CType(e.Item.FindControl("ddlEditRoleId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then row(0)("RoleId") = New Guid(CType(e.Item.FindControl("ddlEditRoleId"), DropDownList).SelectedValue)
                    If Not CType(e.Item.FindControl("ddlEditStatusId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then row(0)("StatusId") = New Guid(CType(e.Item.FindControl("ddlEditStatusId"), DropDownList).SelectedValue)
                    row(0)("ModUser") = Session("UserName")
                    row(0)("ModDate") = Date.Parse(Date.Now.ToString)

                    '   no record is selected
                    dgrdRoles.EditItemIndex = -1

                    '   show footer
                    dgrdRoles.ShowFooter = True

                    '   Bind DataList
                    BindDataList()
                Else
                    DisplayErrorMessage("Invalid data during the edit of an existing row of Authorized Roles")
                    Exit Sub
                End If


                '   user hit "Cancel"
            Case "Cancel"
                '   set no record selected
                dgrdRoles.EditItemIndex = -1

                '   show footer
                dgrdRoles.ShowFooter = True


                '   user hit "Delete" inside the datagrid
            Case "Delete"
                '   get the row to be deleted
                Dim row() As DataRow = dataGridTable.Select("LeadStatusChangePermissionId=" + "'" + CType(e.Item.FindControl("lblEditLeadStatusChangePermissionId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                '   set no record selected
                dgrdRoles.EditItemIndex = -1

                '   show footer
                dgrdRoles.ShowFooter = True


            Case "AddNewRow"
                '   process only if the footer dropdownlists are valid
                If Not AreFooterFieldsBlank() Then
                    '   If the dataList item doesn't have an Id assigned.. create one and assign it.
                    If Session("LeadStatusChangeId") Is Nothing Then
                        '   build a new row in dataList table
                        BuildNewRowInDataList(Guid.NewGuid.ToString)
                        '   bind datalist
                        BindDataList()
                    End If

                    '   get a new row from the dataGridTable
                    Dim newRow As DataRow = dataGridTable.NewRow

                    '   fill the new row with values
                    newRow("LeadStatusChangePermissionId") = Guid.NewGuid
                    newRow("LeadStatusChangeId") = New Guid(CType(Session("LeadStatusChangeId"), String))
                    If Not CType(e.Item.FindControl("ddlFooterRoleId"), DropDownList).SelectedValue = Guid.Empty.ToString Then newRow("RoleId") = New Guid(CType(e.Item.FindControl("ddlFooterRoleId"), DropDownList).SelectedValue)
                    If Not CType(e.Item.FindControl("ddlFooterStatusId"), DropDownList).SelectedValue = Guid.Empty.ToString Then newRow("StatusId") = New Guid(CType(e.Item.FindControl("ddlFooterStatusId"), DropDownList).SelectedValue)
                    newRow("ModUser") = Session("UserName")
                    newRow("ModDate") = Date.Parse(Date.Now.ToString)

                    '   add row to the table
                    newRow.Table.Rows.Add(newRow)

                Else
                    DisplayErrorMessage("Invalid data during adding of a new row of Authorized Roles")
                    Exit Sub
                End If
        End Select

        '   Bind DataGrid
        BindDataGrid()
    End Sub

    Private Sub BindDataGrid()
        '   bind datagrid to dataGridTable
        dgrdRoles.DataSource = New DataView(dataGridTable, "LeadStatusChangeId=" + "'" + CType(Session("LeadStatusChangeId"), String) + "'", "RoleDescrip ASC", DataViewRowState.CurrentRows)
        dgrdRoles.DataBind()
    End Sub

    Private Function AreEditFieldsBlank(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not CType(e.Item.FindControl("ddlEditRoleId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then Return False
        If Not CType(e.Item.FindControl("ddlEditStatusId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then Return False
        Return True
    End Function

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
            If allDataset.HasChanges() Then
                'Set the Delete Button so it prompts the user for confirmation when clicked
                btnnew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
            Else
                btnnew.Attributes.Remove("onclick")
            End If

            '   save dataset systems in session
            Session("AllDataset") = allDataset
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add buttons 
            controlsToIgnore.Add(btnsave)
            controlsToIgnore.Add(btnBuildList)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            BindToolTip()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try


    End Sub

    Private Sub dgrdRoles_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdRoles.ItemDataBound

        '   process only rows in the items sections
        Select Case e.Item.ItemType
            Case ListItemType.EditItem
                '   bind RoleId dropdownlist
                Dim ddl As DropDownList = CType(e.Item.FindControl("ddlEditRoleId"), DropDownList)
                With ddl
                    .DataTextField = "RoleDescrip"
                    .DataValueField = "RoleId"
                    .DataSource = New DataView(ddlRolesTable, "", "RoleDescrip ASC ", DataViewRowState.CurrentRows)
                    .DataBind()
                    .SelectedValue = CType(e.Item.FindControl("lblEditRoleId"), Label).Text
                End With

                '   bind StatusId dropdownlist
                ddl = CType(e.Item.FindControl("ddlEditStatusId"), DropDownList)
                With ddl
                    .DataTextField = "Status"
                    .DataValueField = "StatusId"
                    .DataSource = New DataView(ddlStatusesTable, "", "Status ASC ", DataViewRowState.CurrentRows)
                    .DataBind()
                    .SelectedValue = CType(e.Item.FindControl("lblEditStatusId"), Label).Text
                End With


            Case ListItemType.Footer
                '   bind RoleId dropdownlist
                Dim ddl As DropDownList = CType(e.Item.FindControl("ddlFooterRoleId"), DropDownList)
                With ddl
                    .DataTextField = "RoleDescrip"
                    .DataValueField = "RoleId"
                    .DataSource = New DataView(ddlRolesTable, "", "RoleDescrip ASC ", DataViewRowState.CurrentRows)
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", System.Guid.Empty.ToString))
                    .SelectedIndex = 0
                End With

                '   bind StatusId dropdownlist
                ddl = CType(e.Item.FindControl("ddlFooterStatusId"), DropDownList)
                With ddl
                    .DataTextField = "Status"
                    .DataValueField = "StatusId"
                    .DataSource = New DataView(ddlStatusesTable, "", "Status ASC ", DataViewRowState.CurrentRows)
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", System.Guid.Empty.ToString))
                    .SelectedIndex = 0
                End With
        End Select
    End Sub

    Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        PrepareForNewData()

        '   bind datalist
        BindDataList()
    End Sub

    Private Sub ddlCampGrpId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampGrpId.SelectedIndexChanged
        '   populate Lead Status dropdownlists depending on selected Campus Group
        BuildLeadStatusDDL(ddlCampGrpId.SelectedValue, False)
    End Sub

    Private Sub ddlCampusGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampusGroup.SelectedIndexChanged
        '   populate Lead Status dropdownlists depending on selected Campus Group
        If ddlCampusGroup.SelectedValue <> System.Guid.Empty.ToString Then
            BuildLeadStatusDDL(ddlCampusGroup.SelectedValue, True)
        Else
            ddlOrigStatus.Items.Clear()
            ddlNewStatus.Items.Clear()
        End If
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class

