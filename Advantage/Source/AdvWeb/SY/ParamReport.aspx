﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="ParamReport.aspx.vb" Inherits="ParamReport" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="fame" TagName="ParamPanelBarSet" Src="~/UserControls/ParamControls/ParamSetPanelBarControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../CSS/NewAdvantage.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/ParameterSetControl_Default.css" />
    <style type="text/css">
        .hiddencol {
            display: none;
        }

        .SingleFilterReportContainter {
            width: 100%;
            min-height: 60px;
            height: auto;
            padding: 2px;
        }

        .SingleFilterReportContainter > .CaptionLabel {
            float: left;
            display: inline;
            width: 40%;
           
        }

        .SingleFilterReportContainter > .FilterInput {
            float: left;
            display: inline !important;
            width: 30% !important;
            margin-top: 4px !important;
        }

        .MultiFilterReportContainer > .CaptionLabel {
            width: 30%;
            margin-bottom: 5px;
        }

        .MultiFilterReportContainer > .FilterInput {
            width: 100%;
            padding: 10px;
        }

        .MultiFilterReportContainer  .LabelFilterInput {
            width: 30% !important;
        }

        
        .MultiFilterReportContainer  .ManualInput {
            width: 30% !important;
        }

        .ReportLeftMarginInput {
            margin-left: 10px;
        }

        div.CaptionLabel {
            border-left: #1565c0 2px solid !important;
            border-bottom: #1565c0 2px solid !important;
            /*border: #1565c0 2px solid !important;*/
            /*border-bottom-left-radius: 10px;*/
            /*border-radius: 10px;*/
            padding-left: 10px !important;
            font-size: 15px !important

        }

        .RadListBox {
            width: 500px !important;
            height: 150px !important;
            margin-bottom: 10px;
           
        }

        .RadListBox.rlbFixedHeight .rlbGroup {
            border-radius: 10px !important;
            padding: 10px !important;
        }

    </style>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <%--REM: Assign the Data Source for the grid along with commands--%>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
        DeleteCommand="DELETE FROM syReportUserSettings WHERE [UserSettingId] = @old_UserSettingId"
        UpdateCommand="UPDATE [syReportUserSettings] SET [PrefName] = @PrefName, [PrefDescrip] = @PrefDescrip WHERE [UserSettingId] = @old_UserSettingId AND [PrefName] = @old_PrefName AND [PrefDescrip] = @old_PrefDescrip"
        OldValuesParameterFormatString="old_{0}"
        ConflictDetection="CompareAllValues">
        <DeleteParameters>
            <asp:Parameter Name="UserSettingId" />
            <asp:Parameter Name="old_UserSettingId" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="UserSettingId" />
            <asp:Parameter Name="old_UserSettingId" />
            <asp:Parameter Name="PrefName" />
            <asp:Parameter Name="old_PrefName" />
            <asp:Parameter Name="PrefDescrip" />
            <asp:Parameter Name="old_PrefDescrip" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <%--REM: Telerik Managers--%>
    <asp:ScriptManagerProxy ID="TestProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function OnClientTransferredHandlerFiltersPage(sender, e) {
                //alert("hi");
                var myitems = sender;
                var radbox = $find('<%= CurrentlyTransferredValuesPage.ClientID %>');
                var radbox2 = $find('<%= SourceListBoxPage.ClientID %>');
                myitems = e.get_items();
                radbox.set_value("");
                $telerik.$.each(myitems, function () {
                    radbox.set_value(radbox.get_value() + this.get_value() + ",");
                });
                radbox2.set_value(e.get_sourceListBox().get_id());
            }


        </script>
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(requestEndHandler);

            // This function will handle the end request event
            function requestEndHandler(sender, args) {
                if (args.get_error()) {
                    alert(args.get_error().description);
                }
            }

        </script>
        <script type="text/javascript">
            //Allows only a preset number of characters to be entered in a textbox. 
            //Also supplies a countdown counter.
            var count = "500";   //Max chars allowed. Example: var count = "500";
            function limiter(textboxname) {
                var tex = textboxname.value;
                var len = tex.length;
                if (len > count) {
                    tex = tex.substring(0, count);
                    textboxname.value = tex;
                    return false;
                }
                limit = count - len
                var limitlabel = $find('<%= DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtLimitLabel"), RadTextBox).ClientID %>');
                limitlabel.set_value(limit);
            }
        </script>
    </telerik:RadCodeBlock>
    <%-- ************************************************** --%>
    <!--REM:  Place the content of the pane here   -->
    <telerik:RadSplitter ID="ContentSplitter" runat="server" Height="100%" Width="100%" HeightOffset="164" VisibleDuringInit="false">
        <!--REM:  Content for the left panel -->
        <telerik:RadPane ID="LeftPane" runat="server" Width="280px" Scrolling="both" MaxWidth="385">
            <asp:Button ID="btnNew" runat="server" Text="New Setting"
                Width="243px" CausesValidation="false"
                ToolTip="Clears all settings and returns you to the report landing page."
                OnClick="btnNew_Click" Style="margin-left: 10px; margin-top: 20px; margin-bottom: 20px;" />
            <telerik:RadPanelBar ID="RadRptPanelBarRadGrid" runat="server" ExpandMode="FullExpandedItem" Height="100%" Width="100%">
                <Items>
                    <telerik:RadPanelItem Expanded="True" Text="Saved Reports" Font-Size="10pt">
                        <Items>
                            <telerik:RadPanelItem Value="PrefGrid">
                                <ItemTemplate>
                                    <telerik:RadGrid ID="RadGridReportSettings" runat="server" Height="100%" BorderWidth="0px"
                                        AllowMultiRowSelection="false" AllowMultiRowEdit="false"
                                        Visible="true" ShowStatusBar="true" ShowHeader="true"
                                        OnItemDataBound="RadGridReportSettings_ItemDataBound"
                                        OnItemCommand="RadGridReportSettings_OnItemCommand"
                                        OnNeedDataSource="RadGridReportSettings_NeedDataSource">

                                        <ClientSettings EnableRowHoverStyle="true" EnableAlternatingItems="false">
                                            <Selecting AllowRowSelect="True" />
                                        </ClientSettings>


                                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="UserSettingId" AllowCustomSorting="true"
                                            GridLines="Horizontal" ShowHeader="False" AllowAutomaticUpdates="true"
                                            AllowAutomaticDeletes="true" EditMode="PopUp">
                                            <RowIndicatorColumn>
                                                <HeaderStyle Width="20px" />
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn>
                                                <HeaderStyle Width="20px" />
                                            </ExpandCollapseColumn>
                                            <Columns>

                                              
                                                <telerik:GridButtonColumn UniqueName="EditCommandColumn" ButtonType="ImageButton"
                                                    ImageUrl="~/Images/icon/edit.gif" Text="Edit Setting"
                                                    CommandName="EditSettings">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </telerik:GridButtonColumn>
                                                <telerik:GridBoundColumn DataField="UserSettingId" DataType="System.Guid" Display="false"
                                                    HeaderText="UserSettingId" UniqueName="UserSettingId" Visible="False" ReadOnly="true">
                                                </telerik:GridBoundColumn>


                                                <telerik:GridBoundColumn DataField="PrefName" DataType="System.String"
                                                    MaxLength="350" UniqueName="PrefName" ReadOnly="False"
                                                    AllowSorting="True" GroupByExpression="PrefName"
                                                    SortExpression="PrefName" Visible="false" HeaderText="Name">
                                                    <ItemStyle Wrap="true" Width="350px" />
                                                </telerik:GridBoundColumn>

                                                <telerik:GridButtonColumn ButtonType="LinkButton" ButtonCssClass="PrefNameLinkBtn"
                                                    UniqueName="PrefNameLink" CommandName="LoadPrefs"
                                                    DataTextField="PrefName" DataType="System.String"
                                                    GroupByExpression="PrefName"
                                                    SortExpression="PrefName">
                                                    <ItemStyle Wrap="true" Width="200px" />
                                                </telerik:GridButtonColumn>

                                                <telerik:GridBoundColumn DataField="PrefDescrip" DataType="System.String"
                                                    HeaderText="Description (500 char max)" MaxLength="500" UniqueName="PrefDescrip" ItemStyle-CssClass="hiddencol" ReadOnly="False">
                                                    <ItemStyle Wrap="true" />
                                                </telerik:GridBoundColumn>


                                                <telerik:GridButtonColumn UniqueName="DeleteColumn" ButtonType="ImageButton"
                                                    ImageUrl="~/Images/icon/action_delete.png" Text="Delete Setting"
                                                    ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ConfirmText="Are you sure you would like to delete this report setting?"
                                                    CommandName="Delete">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </telerik:GridButtonColumn>

                                            </Columns>

                                        </MasterTableView>
                                        <PagerStyle Mode="Slider" />
                                        <ClientSettings>
                                            <Scrolling AllowScroll="false" />
                                        </ClientSettings>
                                    </telerik:RadGrid>

                                </ItemTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>

                </Items>
            </telerik:RadPanelBar>

        </telerik:RadPane>

        <telerik:RadSplitBar ID="RadRptSplitBar1" runat="server" CollapseMode="Forward" />

        <!--REM: Content for the Main Panel Width="100%"-->
        <telerik:RadPane ID="MainPane" runat="server" Scrolling="both" BorderWidth="0px">
            <asp:Panel ID="PagePanel" runat="server">

                <div id="ReportTitleSect" class="font-blue-darken-4">
                    <div id="ReportLiteral" class="RptLit">
                        <asp:Panel ID="PanelRptLit" runat="server">
                            <br />
                            <asp:Literal ID="RptDescrip" runat="server"></asp:Literal>
                        </asp:Panel>
                    </div>


                    <telerik:RadTabStrip ID="RadRptTabStrip" runat="server" Style="margin-top: 25px; margin-left: 20px;" OnTabClick="RadRptTabStrip_TabClick"
                        MultiPageID="RadRptMultiPage" SelectedIndex="0">
                        <Tabs>
                            <telerik:RadTab Text="Filtering" Selected="True" Enabled="false" ToolTip="Filtering not available for this report"></telerik:RadTab>
                            <telerik:RadTab Text="Grouping" Enabled="false" ToolTip="Grouping not available for this report"></telerik:RadTab>
                            <telerik:RadTab Text="Sorting" Enabled="false" ToolTip="Sorting not available for this report"></telerik:RadTab>
                            <telerik:RadTab Text="Options" Enabled="false" ToolTip="Options not available for this report"></telerik:RadTab>
                            <telerik:RadTab Text="Run Report" ToolTip="Save report settings & run report"></telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>

                </div>

                <telerik:RadMultiPage ID="RadRptMultiPage" runat="server" CssClass="RadMultiPage" SelectedIndex="0">
                    <%-- REM: Filtering Tab--%>
                    <telerik:RadPageView ID="RadRptPage_Filtering" runat="server" CssClass="RadPageView">
                    </telerik:RadPageView>
                    <%-- REM: Grouping Tab--%>
                    <telerik:RadPageView ID="RadRptPage_Grouping" runat="server" CssClass="RadPageView">
                    </telerik:RadPageView>
                    <%-- REM: Sorting Tab--%>
                    <telerik:RadPageView ID="RadRptPage_Sorting" runat="server" CssClass="RadPageView">
                    </telerik:RadPageView>
                    <%-- REM: Options Tab--%>
                    <telerik:RadPageView ID="RadRptPage_Options" runat="server" CssClass="RadPageView">
                    </telerik:RadPageView>
                    <%-- REM: View & Save Tab--%>
                    <telerik:RadPageView ID="RadRptPage_View" runat="server" CssClass="RadPageView">
                        <telerik:RadPanelBar ID="RadPanelBar_ViewSave" runat="server" ExpandMode="SingleExpandedItem"
                            Width="100%">
                            <Items>
                                <telerik:RadPanelItem Expanded="True" Text="Report Summary" Value="ReportSaveViewRoot">
                                    <Items>
                                        <telerik:RadPanelItem Expanded="True" Value="ReportViewSave">
                                            <ItemTemplate>


                                                <%--REM: Left Panel - Summary information--%>
                                                <div id="ReportSettingsSummary" class="ReportSettingsSummary">

                                                    <div id="SelectionSummary" class="SettingsLabel">
                                                        Current Settings
                                                    </div>
                                                    <asp:Panel ID="PanelOptionWarning" runat="server" Height="363px" ScrollBars="auto">
                                                        <div id="OptLiteralWarning" class="OptWarning">
                                                            <asp:PlaceHolder ID="PlcOptionWarning" runat="server"></asp:PlaceHolder>
                                                        </div>

                                                        <div id="OptLiteralSummary" class="OptSummary">
                                                            <asp:PlaceHolder ID="PlcOptionSummary" runat="server"></asp:PlaceHolder>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                                <%--REM: Right Side - Setting information--%>
                                                <div id="ReportSettingMgt" class="ReportSettingMgt">
                                                    <div id="SettingsLabel" class="SettingsLabel">
                                                        Custom Report
                                                    </div>
                                                    <asp:Panel ID="Panel1" runat="server" ScrollBars="auto">
                                                        <div id="OptLiteralSaveReports" class="OptSummary">
                                                            <asp:PlaceHolder ID="PlcSaveReports" runat="server">
                                                                <asp:Literal ID="Literal1" runat="server">
                                                                   To save current report Filtering, Grouping, Sorting and Options, enter the Name of the report, an optional Description and select Save Settings. The report template will be saved and accessible on the left, under Saved Reports.
                                                                    <br /> <br />
                                                                    For example, you can create and save a custom Transcript for each program offered at your institution.
                                                                </asp:Literal>
                                                            </asp:PlaceHolder>
                                                    </asp:Panel>
                                                </div>
                                                <%--REM: We want a panel here for the textboxes so the update animation shows 1 animation for both boxes.--%>
                                                <div style="position: inherit;">
                                                    <%--      <asp:Panel ID="PanelViewSave" runat="server" Height="210px" Width="325px"> --%>
                                                    <asp:Label ID="lblSettingName" AssociatedControlID="txtSettingName" runat="server" Text="Name" CssClass="SettingNameLabel"></asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtSettingName" MaxLength="28" ValidationGroup="Save" runat="server" Width="290px" CssClass="textbox">
                                                        </asp:TextBox>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblSettingDescription" AssociatedControlID="" runat="server" Text="Description" CssClass="SettingDescripLabel"></asp:Label>
                                                    </div>

                                                    <div id="SettingsDescripTextbox" class="SettingsDescripTextbox">
                                                        <asp:TextBox ID="txtSettingDescription" runat="server" TextMode="MultiLine" Width="315px" Style="font-family: Verdana; font-size: small;"
                                                            Height="50px" CssClass="SettingsDescripTextbox" onclick="limiter(this);" onkeyup="limiter(this);" onkeydown="limiter(this);">
                                                        </asp:TextBox>
                                                    </div>


                                                    <br>
                                                    <div id="buttonSave" class="buttonSave">
                                                        <asp:Button ID="btnSave" runat="server" Text="Save Settings" ValidationGroup="Save" OnClick="btnSave_Click" CausesValidation="true" ToolTip="Saves report settings." />
                                                    </div>
                                                    <div id="CharRemainingLabel" class="CharRemainingLabel">
                                                        <asp:Label ID="Label1" runat="server" Text="Characters remaining: " BorderStyle="None"></asp:Label>
                                                        <telerik:RadTextBox ID="txtLimitLabel" BorderStyle="None" runat="server" Width="60px" />
                                                    </div>



                                                    <%-- <asp:Image ID="imgCheck" CssClass="imgSucessful" runat="server" Visible="false" ImageUrl="~/Images/icon/action_check.png" /> --%>
                                                    <div id="SavedSuccesss" class="SavedSuccess">
                                                        <asp:ImageButton ID="ImgbtnCheck" runat="server" Visible="false" ImageAlign="Bottom" ImageUrl="~/Images/icon/action_check.png" />
                                                        <asp:Label ID="lblSuccessful" runat="server" Text="Report Settings Saved Successfully" Visible="false"></asp:Label>
                                                    </div>

                                                    <div id="SettingValidation" class="SettingValidation">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" ControlToValidate="txtSettingName" runat="server"
                                                            ErrorMessage="Please provide a setting name before saving." ValidationGroup="Save" />
                                                    </div>

                                                    <%--         </asp:Panel>  --%>
                                                </div>

                                                <br />
                                                </div>
                                                 <%--REM: Right Side - Run information--%>
                                                <div id="ReportRunSection" class="ReportRunSection">
                                                    <%--  <div id="ViewLabel" class="ViewLabel"> --%>
                                                    <div id="ViewLabel" class="SettingsLabel">
                                                        Run Report
                                                    </div>

                                                    <%--           <asp:Panel ID="PanelViewReport" runat="server" Height="135px" Width="325px">--%>
                                                    <br />

                                                    <div>
                                                        <asp:Label ID="lblViewOptions" runat="server" Text="Export Type " CssClass="ViewOptionsLabel"></asp:Label>
                                                    </div>
                                                    <div class="web20RadComboBox">
                                                        <telerik:RadComboBox ID="RadComboBoxView" AllowCustomText="false" runat="server" CssClass="ViewOptionsDDL">
                                                          
                                                        </telerik:RadComboBox>
                                                    </div>
                                                    <div id="ExportBtn" class="ExportBtn">
                                                        <asp:Button ID="BtnView" runat="server" Text="Run Report" OnClick="BtnView_Click" ValidationGroup="Export" CausesValidation="false" />
                                                    </div>

                                                    <div id="ViewReportSuccess" class="lblViewSuccess">
                                                        <asp:ImageButton ID="ImgbtnCheckView" runat="server" Visible="False" ImageAlign="Bottom" ImageUrl="~/Images/icon/action_check.png" />
                                                        <asp:Label ID="lblViewSuccess" runat="server" Text="Report exported successfully." Visible="False"></asp:Label>
                                                    </div>

                                                    <div id="ViewError" class="ViewExportError">
                                                        <asp:Label ID="lblViewExportError" Width="80%" runat="server" Font-Bold="true" ForeColor="Red" Text="An error has occured during the report view." Visible="False"></asp:Label>
                                                    </div>
                                                    <%--           </asp:Panel>   --%>
                                                </div>

                                            </ItemTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>

                                <telerik:RadPanelItem ID="radPanelReportDisplayOption" runat="server" Expanded="false" Text="Report Display Options" Value="ReportOptionsRoot" Visible="False">
                                    <Items>
                                        <telerik:RadPanelItem Expanded="false" Value="ReportOptions">
                                            <ItemTemplate>


                                                <%--REM: Splitter for the Summary section under View & Save tab --%>
                                                <%--REM: Single Pane - Contains the Report Options information --%>
                                                <div id="ReportOptLabel" class="ReportOptLabel">
                                                    Report Display Options
                                                </div>
                                                <div id="GeneralRptOptions" class="GenRptOpt">

                                                    <asp:RadioButtonList Visible="false" ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="ReportDisplayOptions_SelectedIndexChanged">
                                                    </asp:RadioButtonList>

                                                    <asp:RadioButtonList ID="rblPageNumberStyle" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="ReportDisplayOptions_SelectedIndexChanged">
                                                        <asp:ListItem Selected="True" Text="Page Numbers" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="No Page Numbers" Value="2"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                 
                                                    <asp:RadioButtonList ID="rblDisplayDate" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="ReportDisplayOptions_SelectedIndexChanged">
                                                        <asp:ListItem Value="0" Selected="True" Text="Do Not Display Report Date"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Display Report Date"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
                <div id="hiddencontrols" style="display: none;">
                    <telerik:RadTextBox ID="CurrentlyTransferredValuesPage" runat="server" Width="0px" Height="0px" BorderWidth="0px" Style="display: none;">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox ID="SourceListBoxPage" runat="server" Width="0px" Height="0px" BorderWidth="0px" Style="display: none;">
                    </telerik:RadTextBox>
                    <!-- Width="0px" Height="0px" BorderWidth="0px" -->
                </div>

            </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:HiddenField ID="txtResId" runat="server" />

    <script type="text/javascript">
            $(function () {
                var resId = ($('[id*="txtResId"]').val());

                switch (resId) {
                    case "846":
                        $('[id*="OptLiteralSaveReports"]').html('To save current report Options, enter the Name of the report, an optional Description and select Save Settings. The report template will be saved and accessible on the left, under Saved Reports. <br /> <br />For example, you can create and save a custom NACCAS Report of any type and name for a specific year, campus and program/s.')
                        break;
                }
            })
    </script>
</asp:Content>


