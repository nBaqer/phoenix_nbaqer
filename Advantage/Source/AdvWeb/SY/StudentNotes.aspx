﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentNotes.aspx.vb" Inherits="AdvWeb.SY.StudentNotes" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../css/AD/LeadNotes.css" rel="stylesheet" />
    <script src="../Scripts/Advantage.Client.AD.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain2" runat="Server">
    <div class="boxContainer">
        <h3><%=Header.Title  %> <a id="notesPlusImage" href="javascript:void(0);">
                        <span class="k-icon k-i-plus-circle font-green"></span>
                    </a></h3>

        <!-- Use this to put content -->
        <div id="notesWrapper">
            <div id="notesFirstPanel" class="flex-container">
              
                <div id="notesItem1" class="flex-item">
                    <label>Module</label>
                    <div id="notesModulesDdl"></div>
                </div>
                <div id="notesItem3" class="flex-item">
                    <button id="notesPrintbutton" type="button" class="k-button" style="font-size: 12px; font-weight: bold; width: 75px">Print</button>
                </div>
            </div>

            <div id="notesMainPanel" style="margin: 10px">
                <div id="notesGrid"></div>
            </div>
            <div id="Edit"></div>
        </div>
        <script type="text/javascript">
            var XSTUDENT_GET_SHADOW_LEAD = "<%=ObjStudentState.ShadowLead%>";
            $(document).ready(function () {
                var manager = new AD.LeadNotes(XSTUDENT_GET_SHADOW_LEAD, "AR");
                manager.initOperation();
            });
        </script>
        <script type="text/x-kendo-template" id="notesTypeGridStyleTemplate">
        #if( Type == "Confidential"){#
            <span style="color: ##b71c1c;">  #: Type #  </span>
        #}else{#
            #: Type # 
        #}#
        </script>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

