﻿<%@ Page Title="Setup Roles" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Roles.aspx.vb" Inherits="Roles" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"/>
   <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <%--    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OldContentSplitter"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlRoles"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
   <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="OldContentSplitter"  />
                     <telerik:AjaxUpdatedControl ControlID="dlRoles"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="OldContentSplitter"  />
                     <telerik:AjaxUpdatedControl ControlID="dlRoles"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings> 
</telerik:RadAjaxManagerProxy>--%>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="listframetop2">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="15%" nowrap align="left">
                                    <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                <td width="85%" nowrap>
                                    <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Active" Selected="True" />
                                        <asp:ListItem Text="Inactive" />
                                        <asp:ListItem Text="All" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftnofilter">
                            <asp:DataList ID="dlRoles" DataKeyField="RoleId" runat="server" Width="100%">
                                <SelectedItemStyle CssClass="selecteditemstyle" />
                                <ItemStyle CssClass="itemstyle" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                        Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'
                                        CausesValidation="False" />
                                    <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'
                                        CausesValidation="False" />
                                    <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId") %>' />
                                    <asp:LinkButton Text='<%# Container.DataItem("Role") %>' runat="server" CssClass="itemstyle"
                                        CommandArgument='<%# Container.DataItem("RoleId")%>' ID="Linkbutton1" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="98%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                        <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width:98%;" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <!-- begin content table-->
                            <h3>
                                <asp:Label ID="headerTitle" runat="server"></asp:Label>
                            </h3>

                            <table class="contenttable" cellspacing="0" cellpadding="3" style="width: 80%;" align="center">
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblStatusId" runat="server" CssClass="label">Status<font color="red">*</font></asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblCode" runat="server" CssClass="label">Code<font color="red">*</font></asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtCode" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblDescrip" runat="server" CssClass="label">Description<font color="red">*</font></asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtDescrip" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblSysRoleId" runat="server" CssClass="label">Advantage Role<font color="red">*</font></asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:DropDownList ID="ddlSysRoleId" runat="server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table class="contenttable" cellspacing="0" cellpadding="0" style="width: 80%;" align="center">
                                <tr>
                                    <td nowrap class="threecolumnheader">
                                        <asp:Label ID="lblAvailModules" runat="server" CssClass="labelbold">Available Modules</asp:Label>
                                    </td>
                                    <td nowrap class="threecolumnbuttons"></td>
                                    <td nowrap class="threecolumnheader">
                                        <asp:Label ID="lblModules" runat="server" CssClass="labelbold">Modules</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="threecolumncontent">
                                        <asp:ListBox ID="lbSysModules" runat="server" CssClass="listboxes" Height="150px"></asp:ListBox>
                                    </td>
                                    <td nowrap class="threecolumnbuttons">
                                        <div style="margin:10px;">
                                            <asp:Button ID="btnAddModule" runat="server" Text="Add -->" Width="100px"
                                                CausesValidation="false" /><br />
                                        </div>

                                        <div style="margin:10px;">
                                            <asp:Button ID="btnDelModule" runat="server" Text="<-- Remove" Width="100px"
                                                CausesValidation="false" />
                                        </div>

                                    </td>
                                    <td nowrap class="threecolumncontent">
                                        <asp:ListBox ID="lbModules" runat="server" CssClass="listboxes" Height="150px"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table class="contenttable" cellspacing="0" cellpadding="0" style="width: 80%;" align="center">
                                <tr>
                                    <td nowrap class="threecolumnheader">
                                        <asp:Label ID="label1" runat="server" CssClass="labelbold">Users in This Role</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="threecolumncontent">
                                        <asp:ListBox ID="lbUsers" runat="server" CssClass="listboxes" Height="150px"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>


            <asp:TextBox ID="txtModUser" runat="server" Visible="False" Text="ModUser" />
            <asp:TextBox ID="txtRoleId" runat="server" Visible="False" />
            <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False" />
            <asp:TextBox ID="txtModDate" runat="server" Visible="False" Text="ModDate" />

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        <asp:RequiredFieldValidator ID="rfvCode" runat="server" Display="None" ErrorMessage="Code is required"
            ControlToValidate="txtCode" />
        <asp:RequiredFieldValidator ID="rfvDescrip" runat="server" Display="None" ErrorMessage="Description is required"
            ControlToValidate="txtDescrip" />
        <asp:RequiredFieldValidator ID="rfvStatus" runat="server" Display="None" ErrorMessage="Status is required"
            ControlToValidate="ddlStatusId" InitialValue="" />
        <asp:RequiredFieldValidator ID="rfvAdvRole" runat="server" Display="None" ErrorMessage="Advantage role is required"
            ControlToValidate="ddlSysRoleId" InitialValue="" />
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

