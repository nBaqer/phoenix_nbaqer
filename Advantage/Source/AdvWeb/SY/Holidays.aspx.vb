Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class Holidays
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Protected ResourceId As String
    Protected ModuleId As String
    Protected campusId, userId As String
    Private validatetxt As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents CalButton As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents DateReqFieldValidator As System.Web.UI.WebControls.RequiredFieldValidator

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList()

            '   bind an empty new HolidayInfo
            BindHolidayData(New HolidayInfo)

            '   initialize buttons
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)
        Else
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BindDataList()

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind Holidays datalist
        dlstHolidays.DataSource = New DataView((New HolidayFacade).GetAllHolidays().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstHolidays.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
        BuildTimeIntervalsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildTimeIntervalsDDL()
        '   bind the TimeIntervals DDL
        Dim ds As DataSet = (New StudentsAccountsFacade).GetAllTimeIntervals()

        With ddlStartTimeId
            .DataTextField = "TimeIntervalDescrip"
            .DataTextFormatString = "{0:hh:mm tt}"
            .DataValueField = "TimeIntervalId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

        With ddlEndTimeId
            .DataTextField = "TimeIntervalDescrip"
            .DataTextFormatString = "{0:hh:mm tt}"
            .DataValueField = "TimeIntervalId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dlstHolidays_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstHolidays.ItemCommand

        '   get the HolidayId from the backend and display it
        GetHolidayId(e.CommandArgument)

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()
        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstHolidays, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlstHolidays, e.CommandArgument.ToString)
    End Sub
    Private Sub BindHolidayData(ByVal Holiday As HolidayInfo)
        With Holiday
            chkIsInDB.Checked = .IsInDB
            txtHolidayId.Text = .HolidayId
            txtHolidayCode.Text = .Code
            If Not (Holiday.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = Holiday.StatusId
            txtHolidayDescrip.Text = .Description
            If Not (Holiday.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = Holiday.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            txtHolidayStartDate.SelectedDate = .HolidayStartDate.ToShortDateString
            txtHolidayEndDate.SelectedDate = .HolidayEndDate.ToShortDateString
            cbxAllDay.Checked = .AllDay
            ddlStartTimeId.SelectedValue = .StartTimeId
            ddlEndTimeId.SelectedValue = .EndTimeId
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If Not IsDataValidForSave Then
            DisplayErrorMessage(GetErrorMessageForSave())
            Exit Sub
        End If

        '   instantiate component
        Dim result As String
        With New HolidayFacade
            '   update Holiday Info 
            result = .UpdateHolidayInfo(BuildHolidayInfo(txtHolidayId.Text), Session("UserName"))
        End With

        '   bind datalist
        BindDataList()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstHolidays, txtHolidayId.Text, ViewState, Header1)

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the HolidayId from the backend and display it
            GetHolidayId(txtHolidayId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new HolidayInfo
            'BindHolidayData(New HolidayInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If
        CommonWebUtilities.RestoreItemValues(dlstHolidays, txtHolidayId.Text)
    End Sub
    Private Function BuildHolidayInfo(ByVal HolidayId As String) As HolidayInfo
        'instantiate class
        Dim HolidayInfo As New HolidayInfo

        With HolidayInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get HolidayId
            .HolidayId = HolidayId

            'get Code
            .Code = txtHolidayCode.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get Holiday's name
            .Description = txtHolidayDescrip.Text.Trim

            'get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            'get Holiday Start Date
            .HolidayStartDate = Date.Parse(txtHolidayStartDate.SelectedDate)

            'get Holiday end Date
            .HolidayEndDate = Date.Parse(txtHolidayEndDate.SelectedDate.GetValueOrDefault() + " 11:59:59 PM")

            'get All Day ?
            .AllDay = cbxAllDay.Checked

            'get StartTimeId
            .StartTimeId = ddlStartTimeId.SelectedValue

            'get EndTimeId
            .EndTimeId = ddlEndTimeId.SelectedValue

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return HolidayInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        '   bind an empty new HolidayInfo
        BindHolidayData(New HolidayInfo)

        'Reset Style in the Datalist
        CommonWebUtilities.SetStyleToSelectedItem(dlstHolidays, Guid.Empty.ToString, ViewState)

        'initialize buttons
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlstHolidays, Guid.Empty.ToString)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If Not (txtHolidayId.Text = Guid.Empty.ToString) Then

            'update Holiday Info 
            Dim result As String = (New HolidayFacade).DeleteHolidayInfo(txtHolidayId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new HolidayInfo
                BindHolidayData(New HolidayInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If
            CommonWebUtilities.RestoreItemValues(dlstHolidays, Guid.Empty.ToString)
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        btnDelete.Enabled = False

        'btnNew.Enabled = False
        'btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        'btnNew.Enabled = True
        'btnDelete.Enabled = True

        ''Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    End Sub
    Private Sub GetHolidayId(ByVal HolidayId As String)

        '   bind Holiday properties
        BindHolidayData((New HolidayFacade).GetHolidayInfo(HolidayId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        BindHolidayData(New HolidayInfo)
        '   bind datalist
        BindDataList()
    End Sub
    Private Function IsDataValidForSave() As Boolean

        If txtHolidayCode.Text = String.Empty Then Return False
        If txtHolidayDescrip.Text = String.Empty Then Return False
        If ddlCampGrpId.SelectedItem.Text = "Select" Then Return False

        Dim bothTimesAreNull As Boolean = ddlStartTimeId.SelectedValue = Guid.Empty.ToString And ddlEndTimeId.SelectedValue = Guid.Empty.ToString
        If cbxAllDay.Checked And Not bothTimesAreNull Then Return False

        Dim anyTimeIsNull = ddlStartTimeId.SelectedValue = Guid.Empty.ToString Or ddlEndTimeId.SelectedValue = Guid.Empty.ToString
        If Not cbxAllDay.Checked And anyTimeIsNull Then Return False

        If Not cbxAllDay.Checked And bothTimesAreNull Then Return False

        If Not cbxAllDay.Checked And Not anyTimeIsNull Then
            If Date.Parse(ddlStartTimeId.SelectedItem.Text).CompareTo(Date.Parse(ddlEndTimeId.SelectedItem.Text)) >= 0 Then Return False
        End If

        'validate start date
        If Not IsDate(txtHolidayStartDate.SelectedDate) Then Return False

        'validate end date
        If Not IsDate(txtHolidayEndDate.SelectedDate) Then Return False

        'end date must be later that start date
        If Date.Parse(txtHolidayStartDate.SelectedDate).CompareTo(Date.Parse(txtHolidayEndDate.SelectedDate)) > 0 Then Return False

        Return True
    End Function
    Private Function GetErrorMessageForSave() As String
        Dim errorMessage As String = ""

        If txtHolidayCode.Text = String.Empty Then
            errorMessage += "Holiday Code is required" + vbCrLf
        End If

        If txtHolidayDescrip.Text = String.Empty Then
            errorMessage += "Holiday Description is required" + vbCrLf
        End If
        If ddlCampGrpId.SelectedItem.Text = "Select" Then
            errorMessage += "Campus Group is required" + vbCrLf
        End If

        'validate start date
        'B. Shanblatt 12/4/2012
        'if the holiday start date is empty or invalid start date is required message
        'DE8792
        If txtHolidayStartDate.SelectedDate Is Nothing Then
            errorMessage += "A valid Start Date is required" + vbCrLf
        End If

        'validate end date
        'B. Shanblatt 12/4/2012
        'if the holiday end date is empty or invalid return end date is required message
        'DE8792
        If txtHolidayEndDate.SelectedDate Is Nothing Then
            errorMessage += "A valid End Date is required" + vbCrLf
        End If


        'end date must be later that start date
        'B. Shanblatt 12/4/2012
        'added if statement to make sure the start and end dates exist and are valid dates for comparision
        'without this check the code would proceed with the comparision and fail causing defect 
        'DE8792
        If (Not txtHolidayStartDate.SelectedDate Is Nothing And IsDate(txtHolidayStartDate.SelectedDate)) And (Not txtHolidayEndDate.SelectedDate Is Nothing And IsDate(txtHolidayEndDate.SelectedDate)) Then
            If Date.Parse(txtHolidayStartDate.SelectedDate).CompareTo(Date.Parse(txtHolidayEndDate.SelectedDate)) > 0 Then
                'errorMessage += "'Start Time' must be earlier than 'End Time'." + vbCrLf
                errorMessage += "The holiday end date cannot be less than the holiday start date" + vbCrLf
                Return errorMessage
            End If
        End If

        Dim bothTimesAreNull As Boolean = ddlStartTimeId.SelectedValue = Guid.Empty.ToString And ddlEndTimeId.SelectedValue = Guid.Empty.ToString
        If cbxAllDay.Checked And Not bothTimesAreNull Then
            'errorMessage += "'All Day' and 'Start Time'/'End Time' are selected. These are conflicting options." + vbCrLf
            errorMessage += "Close is required - choose All Day or select a Start and End Time" + vbCrLf
            Return errorMessage
        End If

        Dim anyTimeIsNull = ddlStartTimeId.SelectedValue = Guid.Empty.ToString Or ddlEndTimeId.SelectedValue = Guid.Empty.ToString
        If Not cbxAllDay.Checked And anyTimeIsNull Then
            'errorMessage += "If 'All Day' is not selected you must select a Start and End Time." + vbCrLf
            errorMessage += "Close is required - choose All Day or select a Start and End Time" + vbCrLf
            Return errorMessage
        End If

        If Not cbxAllDay.Checked And bothTimesAreNull Then
            'errorMessage += "You must select a value. You must check 'All Day ?' or select 'Start Time' and 'End Time'." + vbCrLf
            errorMessage += "Close is required - choose All Day or select a Start and End Time" + vbCrLf
            Return errorMessage
        End If

        If Not cbxAllDay.Checked And Not anyTimeIsNull Then
            If Date.Parse(ddlStartTimeId.SelectedItem.Text).CompareTo(Date.Parse(ddlEndTimeId.SelectedItem.Text)) >= 0 Then
                errorMessage += "Start Time must be earlier than End Time" + vbCrLf
            End If
        End If

        Return errorMessage
    End Function
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Public Sub CloseFieldsValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)

        validatetxt = False

        Dim bothTimesAreNull As Boolean = ddlStartTimeId.SelectedValue = Guid.Empty.ToString And ddlEndTimeId.SelectedValue = Guid.Empty.ToString
        If cbxAllDay.Checked And Not bothTimesAreNull Then
            validatetxt = True
            DisplayErrorMessage("Close is required - chose All Day or select a start and end time")
            Exit Sub
        End If


        Dim anyTimeIsNull = ddlStartTimeId.SelectedValue = Guid.Empty.ToString Or ddlEndTimeId.SelectedValue = Guid.Empty.ToString
        If Not cbxAllDay.Checked And anyTimeIsNull Then
            validatetxt = True
            DisplayErrorMessage("Close is required - chose All Day or select a start and end time")
            Exit Sub
        End If

        If Not cbxAllDay.Checked And bothTimesAreNull Then
            validatetxt = True
            DisplayErrorMessage("Close is required - chose All Day or select a start and end time")
            Exit Sub
        End If

        If Not cbxAllDay.Checked And Not anyTimeIsNull Then
            If Date.Parse(ddlStartTimeId.SelectedItem.Text).CompareTo(Date.Parse(ddlEndTimeId.SelectedItem.Text)) >= 0 Then
                validatetxt = True
                DisplayErrorMessage("Close is required - chose All Day or select a start and end time")
                Exit Sub
            End If
        End If

    End Sub

End Class
