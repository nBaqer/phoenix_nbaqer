﻿<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="reportparamstranscript.aspx.vb" Inherits="AdvWeb.SY.SyReportParamsTranscript" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register Src="~/usercontrols/AdvantageRadAjaxLoadingControl.ascx" TagPrefix="uc1" TagName="AdvantageRadAjaxLoadingControl" %>

<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <title>
        <%# pagetitle %>
    </title>

    <script type="text/javascript" src="../scripts/scrollsaver.min.js"></script>
    <script type="text/javascript">
        function oldpageresized(sender)
        {
            window.$telerik.repaintChildren(sender);
        }
    </script>
    <script src="../js/EnterKey.js" type="text/javascript"></script>
    <%--<script src="../usercontrols/fullcalendar/script.js" type="text/javascript"></script>--%>
    <%--<script src="../scripts/jquery-1.2.6.js" type="text/javascript"></script> 
	<script src="../scripts/jquery-1.2.6-vsdoc.js" type="text/javascript"></script> 
	<script type="text/javascript" src="../scripts/scrollsaver.min.js"></script>--%>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <%-- <asp:scriptmanagerproxy id="scriptmanagerproxy1" runat="server">
 </asp:scriptmanagerproxy>--%>
    <uc1:AdvantageRadAjaxLoadingControl runat="server" ID="AdvantageRadAjaxLoadingControl1" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="AdvantageRadAjaxLoadingControl1">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="oldpageresized(this)">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="250" Scrolling="Y">

                <table class="maincontenttable">
                    <tr>
                        <td class="listframetop">
                            <asp:Label ID="lblpreferences" runat="server" CssClass="label" Text="saved user reports">Saved Report Selections</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstprefs" runat="server" Width="100%" DataKeyField="prefid">
                                    <SelectedItemStyle CssClass="selecteditemstyle" />
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton Text='<%# container.dataitem("prefname")%>' runat="server"
                                            CssClass="itemstyle" CommandArgument='<%# container.dataitem("prefid")%>'
                                            ID="linkbutton1" CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <!-- end leftcolumn -->
            <telerik:RadPane ID="radpane1" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop" left="250px">
                <!-- begin rightcolumn -->
                <table style="width: 100%; padding: 0; border-collapse: collapse" id="Table4">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" style="text-align: right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                            <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                Enabled="False"></asp:Button></td>
                    </tr>

                </table>

                <table class="maincontenttable" id="table5" style="padding-left: 10px; width: 80%">
                    <tr>
                        <td class="detailsframe">
                            <div>
                                <!-- begin table content-->
                                <table class="maincontenttable" style="padding-top: 10px;">
                                    <!-- begin top panel -->
                                    <tr>
                                        <td>
                                            <asp:Panel ID="pnlpreference" CssClass="textbox" runat="server" Width="80%" Visible="true" Style="padding-left: 2px" Height="28px">
                                                <asp:Table ID="table2" CssClass="label" runat="server" Width="300px">
                                                    <asp:TableRow>
                                                        <asp:TableCell Width="125px" Wrap="false">
                                                            <asp:Label ID="lblprefname" runat="server" CssClass="labelbold">Preference name</asp:Label>
                                                        </asp:TableCell>
                                                        <asp:TableCell Width="175px">
                                                            <asp:TextBox ID="txtpreferencename" runat="server" CssClass="textbox" MaxLength="50" Width="175px"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:Panel>
                                            <p></p>
                                            <asp:Panel ID="pnl5" CssClass="textbox" runat="server" Visible="false" Width="80%" Height="35px">
                                                <asp:Table ID="table3" CssClass="label" runat="server" Width="300px">
                                                    <asp:TableRow>
                                                        <asp:TableCell Width="125px" Wrap="false">
                                                            <asp:Label ID="lbl4" CssClass="label" runat="server" Font-Bold="true">Required filter(s)</asp:Label>
                                                        </asp:TableCell>
                                                        <asp:TableCell Width="250px" Wrap="false">
                                                            <asp:Label ID="lblrequiredfilters" CssClass="label" runat="server">Student, Enrollment</asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                                <!-- end top panel -->
                                <table class="contenttable">
                                    <tr>
                                        <td class="lsreport">
                                            <!-- begin left side report params -->
                                            <table class="maincontenttable">
                                                <tr>
                                                    <td colspan="3" class="lsheaderreport">
                                                        <asp:Label ID="lblselectsort" runat="server" CssClass="label" Font-Bold="true">Step 1: Please select the fields to sort the report by:</asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="lscontentreport1">Available Sort Order</td>
                                                    <td class="lscontentreport2">&nbsp;</td>
                                                    <td class="lscontentreport1">Selected Sort Order</td>
                                                </tr>
                                                <tr>
                                                    <td class="lscontentreport3" style="width: 40%">
                                                        <asp:ListBox ID="lbxsort" runat="server" Rows="7" CssClass="todocumentskills2"></asp:ListBox></td>
                                                    <td class="lscontentreport2" style="width: 20%">
                                                        <asp:Button ID="btnadd" Text="Add >" runat="server" CausesValidation="false"></asp:Button><br>
                                                        <asp:Button ID="btnremove" Text="< Remove" runat="server" CausesValidation="false"></asp:Button></td>
                                                    <td class="lscontentreport3" style="width: 40%">
                                                        <asp:ListBox ID="lbxselsort" runat="server" Rows="7" CssClass="todocumentskills2"></asp:ListBox></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="lsheaderreport">
                                                        <asp:Label ID="lblselectfilters" runat="server" CssClass="label" Font-Bold="true">Step 2: Please select the filters to apply to the report:</asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="lscontentreport1">Filter</td>
                                                    <td class="lscontentreport2">&nbsp;</td>
                                                    <td class="lscontentreport1">Values</td>
                                                </tr>
                                                <tr>
                                                    <td class="lscontentreport3">
                                                        <asp:ListBox ID="lbxfilterlist" runat="server" Rows="8" CssClass="todocumentlist1" AutoPostBack="true"></asp:ListBox></td>
                                                    <td class="lscontentreport2">&nbsp;</td>
                                                    <td class="lscontentreport3">
                                                        <asp:ListBox ID="lbxselfilterlist" runat="server" Rows="8" CssClass="todocumentlist1" AutoPostBack="true"
                                                            SelectionMode="multiple"></asp:ListBox><br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="lsfooterreport">
                                                        <br />
                                                        <br />
                                                        <asp:Panel ID="pnl4" CssClass="textbox" runat="server" Visible="false" Width="100%" Height="160px">
                                                            <table class="maincontenttable">
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                            <table class="maincontenttable">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Table ID="table1" CssClass="label" runat="server" Width="425px">
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Width="75px">
                                                                                    <asp:Label ID="lblstudent" CssClass="label" runat="server">Student</asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="300px">
                                                                                    <asp:TextBox ID="txtStudentId" CssClass="textbox" runat="server" Width="300px" contenteditable="true"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="50px" HorizontalAlign="left">
                                                                                    <asp:RequiredFieldValidator ID="requiredfldvalstudent" runat="server" Display="none" ControlToValidate="txtStudentId"
                                                                                        ErrorMessage="Student is a required filter"></asp:RequiredFieldValidator>
                                                                                    <asp:LinkButton ID="lbtsearch" runat="server" CausesValidation="false" >
																								<img style="border:0" src="../images/top_search.gif" alt="search student"></asp:LinkButton>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Width="75px">
                                                                                    <asp:Label ID="lblenrollmentid" CssClass="label" runat="server" Visible="true">Enrollment</asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="300px">
                                                                                    <asp:TextBox ID="txtstuenrollment" runat="server" CssClass="textbox" Width="300px" contenteditable="false"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="requiredfldvalstuenroll" runat="server" Display="none" ControlToValidate="txtstuenrollment"
                                                                                        ErrorMessage="Enrollment is a required filter"></asp:RequiredFieldValidator>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:HiddenField ID="txtstuenrollmentid" runat="server" />
                                                                                    <asp:HiddenField ID="txttermid" runat="server" />
                                                                                    <asp:HiddenField ID="txtterm" runat="server" />
                                                                                    <asp:HiddenField ID="txtacademicyearid" runat="server" />
                                                                                    <asp:HiddenField ID="txtacademicyear" runat="server" />
                                                                                    <asp:HiddenField ID="txtstudentidentifier" runat="server" />
                                                                                    <asp:HiddenField ID="txtstudentguidid" runat="server" />

                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Width="75px">
                                                                                    <asp:Label ID="lbltermstartcutoff" CssClass="label" runat="server" Visible="false">Term start cutoff</asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="300px">
                                                                                    <asp:TextBox ID="txttermstart" runat="server" CssClass="textbox" Width="200px" Visible="false" Text='<%# date.today.toshortdatestring() %>'></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:CheckBox ID="chkstudentsign" runat="server" Visible="false" Text="show student signature line" />
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:CheckBox ID="chkschoolsign" runat="server" Visible="false" Text="show school official signature line" />
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                        <asp:Table ID="tblfilterothers" CssClass="label" runat="server" Width="525px">
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Width="75px"></asp:TableCell>
                                                                                <asp:TableCell Width="150px"></asp:TableCell>
                                                                                <asp:TableCell Width="150px"></asp:TableCell>
                                                                                <asp:TableCell Width="150px"></asp:TableCell>
                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                        <asp:Table ID="tblstudentsearch" CssClass="label" runat="server" Width="100%">
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Width="40%">
                                                                                    <asp:CheckBox ID="chktotalcost" runat="server" Visible="false" Text="show total cost" Checked="true" />
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="40%">
                                                                                    <asp:CheckBox ID="chkcurrentbalance" runat="server" Visible="false" Text="show current balance" Checked="true" />
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell ColumnSpan="2">
                                                                                    <asp:RadioButtonList ID="rdolstchkunits" runat="server" CssClass="label" AutoPostBack="true" RepeatColumns="2" Visible="false">
                                                                                        <asp:ListItem Value="true" Selected="true">Show work units under each subject</asp:ListItem>
                                                                                        <asp:ListItem Value="false">Do not show work units under each subject</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
																						&nbsp;
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
																						&nbsp;
                                                                                </asp:TableCell>
                                                                                <asp:TableCell ColumnSpan="2">
                                                                                    <asp:Panel ID="pnlrow" runat="server" Enabled="false" Visible="false">
                                                                                        <table class="label">
                                                                                            <tr>
                                                                                                <td colspan="3">Group work units by type under each term
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chk501" runat="server" Text=" exam" Checked="true" /></td>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chk544" runat="server" Text=" externships" Checked="true" /></td>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chk502" runat="server" Text=" final" Checked="true" /></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chk499" runat="server" Text=" homework" Checked="true" /></td>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chk503" runat="server" Text=" lab hours" Checked="true" /></td>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chk500" runat="server" Text=" lab work" Checked="true" /></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chk533" runat="server" Text=" practical exams" Checked="true" /></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:Panel>

                                                                                </asp:TableCell>

                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" rowspan="6"></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- end left side report params -->
                                        </td>
                                        <td class="rsspacerreport"></td>
                                        <td class="rsreport">
                                            <!-- begin right side -->
                                            <table class="maincontenttable">
                                                <tr>
                                                    <td>
                                                        <!-- begin right side top -->
                                                        <table class="maincontenttable">
                                                            <tr>
                                                                <td class="rtheaderreport">
                                                                    <asp:Label ID="lbladditionalinfo" runat="server" CssClass="label" Font-Bold="true">Step 3: Additional information to print on the report:</asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="rtcontentreport">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptfilters" runat="server" Text="Selected Filter Criteria" CssClass="label"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptsort" runat="server" Text="Selected Sort Order" CssClass="label"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptdescrip" runat="server" Text="Report Description" CssClass="label"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptinstructions" runat="server" Text="Report Instructions" CssClass="label"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptnotes" runat="server" Text="Report Notes" CssClass="label"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptdateissue" runat="server" Text="Date Issued" CssClass="label"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptsuppressheader" runat="server" Text="Suppress Header" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkUseSignatureForAttendance" runat="server" Text="Suppress Signature Lines" Enabled="false" CssClass="label" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkShowOffTrans" runat="server" Text="Show Official Transcript" CssClass="label" Enabled="false" Visible="false" Checked="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptshowclassdates" runat="server" Text="Show Class Dates" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chktermmodule" runat="server" Visible="false" Text="Show Term/Module" CssClass="label" Checked="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkshowlegaldisc" runat="server" Text="Show Legal Disclaimer" Enabled="false" CssClass="label" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkshowTermProgressDescription" runat="server" Text="Show Term Progress Description" Enabled="true" Checked="false" CssClass="label" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptiswttdc" runat="server" Text="Include Students Who Transfer<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Different Campus" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptiswttdp" runat="server" Text="Include Students Who Transfer<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Different Program" CssClass="label" Enabled="false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkrptdonotshowlda" runat="server" Text="Do Not Show LDA" Enabled="false" CssClass="label" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- end right side top -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <!-- begin right side bottom -->
                                                        <table class="maincontenttable">
                                                            <tr>
                                                                <td colspan="2" class="rtheaderreport2">
                                                                    <asp:Label ID="lblstep4" runat="server" CssClass="label" Font-Bold="true">Step 4: </asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" class="rtcontentreport">
                                                                    <asp:Button ID="btnsearch" Text="View Report" runat="server" Width="130px"></asp:Button><br>
                                                                    <asp:Button ID="btnfriendlyprint" Visible="false" Text=" Printer Friendly Report" runat="server"></asp:Button></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="rbdropdownreport">
                                                                    <asp:DropDownList ID="ddlexportto" runat="server" CssClass="dropdownlist">
                                                                        <asp:ListItem Value="pdf">Adobe Acrobat</asp:ListItem>
                                                                        <asp:ListItem Value="xls">Microsoft Excel</asp:ListItem>
                                                                        <asp:ListItem Value="doc">Microsoft Word</asp:ListItem>
                                                                        <asp:ListItem Value="xls1">Microsoft Excel (Data) </asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td>
                                                                    <asp:Button ID="btnexport" Text="Export" runat="server"></asp:Button></td>
                                                            </tr>
                                                        </table>
                                                        <!-- end right side bottom-->
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- end right side -->
                                        </td>
                                    </tr>
                                </table>
                                <asp:LinkButton ID="lbtprompt" runat="server" Width="0px"></asp:LinkButton>

                                <asp:Label ID="lblerrormessage" runat="server" Font-Bold="true" EnableViewState="false" Font-Size="medium"
                                    ForeColor="#ff8000"></asp:Label>
                                <asp:HiddenField ID="txtresid" runat="server" />
                                <asp:HiddenField ID="txtprefid" runat="server" />
                                <!--end table content-->
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
    </telerik:RadAjaxPanel>
    <asp:Panel ID="panel1" runat="server"></asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" Display="None"
        ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" ShowSummary="false" ShowMessageBox="true"></asp:ValidationSummary>



</asp:Content>

